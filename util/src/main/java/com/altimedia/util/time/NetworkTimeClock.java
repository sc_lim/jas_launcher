/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package com.altimedia.util.time;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.SystemClock;

import com.altimedia.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;

/**
 * Created by mc.kim on 21,07,2020
 */
public class NetworkTimeClock implements Clock {
    public static final String NTP_TIME_SET = "kr.altimedia.intent.action.NTP_TIME_SET";
    private static final long DEFAULT_PERIOD = 1 * 60 * 60 * 1000;
    private final String TAG = NetworkTimeClock.class.getSimpleName();
    private final long currentPeriod;
    private final Context mContext;
    Timer mTimer = null;
    List<OnTimeSetListener> mTimeListener = new ArrayList<>();
    private NTPTimerTask mNTPTimerTask = null;
    private long mNtpTimeDifferTime = 0;
    private boolean mIsSetTime = false;
    private static boolean DEBUG = false;

    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getAction()) {
                case Intent.ACTION_TIMEZONE_CHANGED:
                case Intent.ACTION_TIME_CHANGED:

                    if (Log.INCLUDE) {
                        Log.d(TAG, "intent get : " + intent.getAction());
                    }
                    sync();
                    break;
            }
        }
    };


    ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback() {
        @Override
        public void onAvailable(@NonNull Network network) {
            super.onAvailable(network);
            if (Log.INCLUDE) {
                Log.d(TAG, "onAvailable");
            }
            sync(network);
        }

        @Override
        public void onLost(@NonNull Network network) {
            super.onLost(network);
            if (Log.INCLUDE) {
                Log.d(TAG, "onLost");
            }
            releaseTimer();
        }

        @Override
        public void onUnavailable() {
            super.onUnavailable();
            if (Log.INCLUDE) {
                Log.d(TAG, "onUnavailable");
            }
            releaseTimer();
        }
    };

    public NetworkTimeClock(Context context) {
        this(context, DEFAULT_PERIOD);
    }

    public NetworkTimeClock(Context context, long period) {
        this.mContext = context;
        this.currentPeriod = period;
        ConnectivityManager connectivityManager = mContext.getSystemService(ConnectivityManager.class);
        connectivityManager.registerDefaultNetworkCallback(networkCallback);
        registerReceiver();
    }


    private void registerReceiver() {
        final IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        this.mContext.registerReceiver(mIntentReceiver, filter);
    }

    private void unregisterReceiver() {
        this.mContext.unregisterReceiver(mIntentReceiver);
    }


    private void releaseTimer() {
        if (mNTPTimerTask != null && mTimer != null) {
            if (DEBUG && Log.INCLUDE) {
                Log.d(TAG, "onAvailable mNTPTimerTask != null : so cancel previous timer");
            }
            mTimer.cancel();
            mTimer = null;
            mNTPTimerTask = null;
        }
    }

    public void sync() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call sync");
        }
        ConnectivityManager connectivityManager = mContext.getSystemService(ConnectivityManager.class);
        sync(connectivityManager.getActiveNetwork());
    }

    public void sync(Network network) {
        if (network == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "call sync network null so return");
            }
            return;
        }
        if (DEBUG && Log.INCLUDE) {
            Log.d(TAG, "call sync network");
        }
        releaseTimer();
        mNTPTimerTask = new NTPTimerTask(network, new OnTimeSetCallback() {
            @Override
            public void notifySetTime(long ntpTime) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifySetTime : " + ntpTime);
                }
                mIsSetTime = true;
                mNtpTimeDifferTime = ntpTime - System.currentTimeMillis();
                fireTimeSetCallback(ntpTime);
            }
        });
        mTimer = new Timer();
        mTimer.schedule(mNTPTimerTask, 0, currentPeriod);
    }

    public void release() {
        ConnectivityManager connectivityManager = mContext.getSystemService(ConnectivityManager.class);
        connectivityManager.unregisterNetworkCallback(networkCallback);
        unregisterReceiver();
        mTimeListener.clear();
        releaseTimer();
    }

    @Override
    public boolean isTimeSet() {
        return mIsSetTime;
    }

    @Override
    public long currentTimeMillis() {

        if (isTimeSet()) {
            if (DEBUG && Log.INCLUDE) {
                Log.d(TAG, "isTimeSet so return mNtpTimeDifferTime : " + mNtpTimeDifferTime);
                Log.d(TAG, "isTimeSet so return System.currentTimeMillis() : " + System.currentTimeMillis());
                Log.d(TAG, "isTimeSet so return mNtpTime : " + (mNtpTimeDifferTime + System.currentTimeMillis()));
            }
            return mNtpTimeDifferTime + System.currentTimeMillis();
        } else {
            if (DEBUG && Log.INCLUDE) {
                Log.d(TAG, "isTimeSet not set so return SystemTime : " + System.currentTimeMillis());
            }
            return System.currentTimeMillis();
        }
    }

    @Override
    public long elapsedRealtime() {
        return SystemClock.elapsedRealtime();
    }

    @Override
    public long uptimeMillis() {
        return SystemClock.uptimeMillis();
    }

    @Override
    public boolean addClockUpdateListener(OnTimeSetListener onTimeSetListener) {
        if (mTimeListener.contains(onTimeSetListener)) {
            return false;
        }
        return mTimeListener.add(onTimeSetListener);
    }

    @Override
    public boolean removeClockUpdateListener(OnTimeSetListener onTimeSetListener) {
        if (!mTimeListener.contains(onTimeSetListener)) {
            return false;
        }
        return mTimeListener.remove(onTimeSetListener);
    }

    private void fireTimeSetCallback(long ntpTime) {
        for (OnTimeSetListener onTimeSetCallback : mTimeListener) {
            onTimeSetCallback.notifyTimeSetChanged(ntpTime);
        }
        if (this.mContext != null) {
            this.mContext.sendBroadcast(new Intent(NTP_TIME_SET));
        }
    }

    public interface OnTimeSetCallback {
        void notifySetTime(long ntpTime);
    }

    private static class NTPTimerTask extends TimerTask {
        private static final String TAG = NTPTimerTask.class.getSimpleName();
        private static final int NTP_PACKET_SIZE = 48;
        private static final long OFFSET_1900_TO_1970 = ((365L * 70L) + 17L) * 24L * 60L * 60L;
        private static final int NTP_MODE_CLIENT = 3;
        private static final int NTP_VERSION = 3;
        private static final int TRANSMIT_TIME_OFFSET = 40;
        private static final int ORIGINATE_TIME_OFFSET = 24;
        private static final int RECEIVE_TIME_OFFSET = 32;
        private static final int NTP_LEAP_NOSYNC = 3;
        private static final int NTP_STRATUM_DEATH = 0;
        private static final int NTP_STRATUM_MAX = 15;
        private static final int NTP_MODE_SERVER = 4;
        private static final int NTP_MODE_BROADCAST = 5;
        private static final int NTP_PORT = 123;
        private final Network mNetwork;
        private final OnTimeSetCallback mOnTimeSetCallback;

        public NTPTimerTask(@NonNull Network network, @NonNull OnTimeSetCallback onTimeSetCallback) {
            mNetwork = network;
            mOnTimeSetCallback = onTimeSetCallback;
        }

        private static void checkValidServerReply(byte leap, byte mode, int stratum, long transmitTime)
                throws Exception {
            if (leap == NTP_LEAP_NOSYNC) {
                throw new Exception("unsynchronized server");
            }
            if ((mode != NTP_MODE_SERVER) && (mode != NTP_MODE_BROADCAST)) {
                throw new Exception("untrusted mode: " + mode);
            }
            if ((stratum == NTP_STRATUM_DEATH) || (stratum > NTP_STRATUM_MAX)) {
                throw new Exception("untrusted stratum: " + stratum);
            }
            if (transmitTime == 0) {
                throw new Exception("zero transmitTime");
            }
        }

        @Override
        public void run() {
            if (DEBUG && Log.INCLUDE) {
                Log.d(TAG, "Timer Alert");
            }
            long time = requestTime("time.android.com", 3 * 1000, mNetwork);
            if (DEBUG && Log.INCLUDE) {
                Log.d(TAG, "Timer Alert : " + time);
            }
            if (time != -1) {
                mOnTimeSetCallback.notifySetTime(time);
            }
        }

        public long requestTime(String host, int timeout, final Network networkForResolv) {
            InetAddress address = null;
            try {
                address = networkForResolv.getByName(host);
            } catch (Exception e) {
                return -1;
            }
            return requestTime(address, NTP_PORT, timeout, networkForResolv);
        }

        public long requestTime(InetAddress address, int port, int timeout, Network network) {
            DatagramSocket socket = null;
            try {
                socket = new DatagramSocket();
                network.bindSocket(socket);
                socket.setSoTimeout(timeout);
                byte[] buffer = new byte[NTP_PACKET_SIZE];
                DatagramPacket request = new DatagramPacket(buffer, buffer.length, address, port);

                buffer[0] = NTP_MODE_CLIENT | (NTP_VERSION << 3);
                final long requestTime = System.currentTimeMillis();
                final long requestTicks = SystemClock.elapsedRealtime();
                writeTimeStamp(buffer, TRANSMIT_TIME_OFFSET, requestTime);
                socket.send(request);
                DatagramPacket response = new DatagramPacket(buffer, buffer.length);
                socket.receive(response);
                final long responseTicks = SystemClock.elapsedRealtime();
                final long responseTime = requestTime + (responseTicks - requestTicks);
                final byte leap = (byte) ((buffer[0] >> 6) & 0x3);
                final byte mode = (byte) (buffer[0] & 0x7);
                final int stratum = buffer[1] & 0xff;
                final long originateTime = readTimeStamp(buffer, ORIGINATE_TIME_OFFSET);
                final long receiveTime = readTimeStamp(buffer, RECEIVE_TIME_OFFSET);
                final long transmitTime = readTimeStamp(buffer, TRANSMIT_TIME_OFFSET);
                checkValidServerReply(leap, mode, stratum, transmitTime);
                long roundTripTime = responseTicks - requestTicks - (transmitTime - receiveTime);
                long clockOffset = ((receiveTime - originateTime) + (transmitTime - responseTime)) / 2;
                return responseTime + clockOffset;
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            } finally {
                if (socket != null) {
                    socket.close();
                }
            }
        }

        private long read32(byte[] buffer, int offset) {
            byte b0 = buffer[offset];
            byte b1 = buffer[offset + 1];
            byte b2 = buffer[offset + 2];
            byte b3 = buffer[offset + 3];

            // convert signed bytes to unsigned values
            int i0 = ((b0 & 0x80) == 0x80 ? (b0 & 0x7F) + 0x80 : b0);
            int i1 = ((b1 & 0x80) == 0x80 ? (b1 & 0x7F) + 0x80 : b1);
            int i2 = ((b2 & 0x80) == 0x80 ? (b2 & 0x7F) + 0x80 : b2);
            int i3 = ((b3 & 0x80) == 0x80 ? (b3 & 0x7F) + 0x80 : b3);

            return ((long) i0 << 24) + ((long) i1 << 16) + ((long) i2 << 8) + (long) i3;
        }

        private long readTimeStamp(byte[] buffer, int offset) {
            long seconds = read32(buffer, offset);
            long fraction = read32(buffer, offset + 4);
            // Special case: zero means zero.
            if (seconds == 0 && fraction == 0) {
                return 0;
            }
            return ((seconds - OFFSET_1900_TO_1970) * 1000) + ((fraction * 1000L) / 0x100000000L);
        }

        private void writeTimeStamp(byte[] buffer, int offset, long time) {
            // Special case: zero means zero.
            if (time == 0) {
                Arrays.fill(buffer, offset, offset + 8, (byte) 0x00);
                return;
            }

            long seconds = time / 1000L;
            long milliseconds = time - seconds * 1000L;
            seconds += OFFSET_1900_TO_1970;

            // write seconds in big endian format
            buffer[offset++] = (byte) (seconds >> 24);
            buffer[offset++] = (byte) (seconds >> 16);
            buffer[offset++] = (byte) (seconds >> 8);
            buffer[offset++] = (byte) (seconds >> 0);

            long fraction = milliseconds * 0x100000000L / 1000L;
            // write fraction in big endian format
            buffer[offset++] = (byte) (fraction >> 24);
            buffer[offset++] = (byte) (fraction >> 16);
            buffer[offset++] = (byte) (fraction >> 8);
            // low order bits should be random data
            buffer[offset++] = (byte) (Math.random() * 255.0);
        }
    }


}