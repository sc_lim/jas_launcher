/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package com.altimedia.util;

import android.content.Context;
import android.os.Build;
import android.os.PowerManager;

/**
 * Created by mc.kim on 15,05,2020
 */
public class DeviceUtil {

    private static final String TAG = DeviceUtil.class.getSimpleName();

    public static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getModel() {
        return Build.MODEL;
    }

    public static void reboot(Context context) {
        reboot(context, null);
    }

    public static void rebootQuiescent(Context context) {
        reboot(context, "quiescent");
    }

    public static void reboot(Context context, String reason) {
        if (context != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "reboot, reason=" + reason);
            }

            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            pm.reboot(reason);
        }
    }

}
