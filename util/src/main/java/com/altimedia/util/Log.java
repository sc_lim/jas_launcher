/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.util;

public class Log {

    public static final boolean INCLUDE = BuildConfig.DEBUG;

    public static void v(String tag, String message) {
        if (INCLUDE) {
            String[] tags = getTags(tag);

            for (String str : tags) {
                android.util.Log.v(str, message);
            }
        }
    }

    public static void i(String tag, String message) {
        if (INCLUDE) {
            String[] tags = getTags(tag);

            for (String str : tags) {
                android.util.Log.i(str, message);
            }
        }
    }

    public static void d(String tag, String message) {
        if (INCLUDE) {
            String[] tags = getTags(tag);

            for (String str : tags) {
                android.util.Log.d(str, message);
            }
        }
    }

    public static void w(String tag, String message) {
        if (INCLUDE) {
            String[] tags = getTags(tag);

            for (String str : tags) {
                android.util.Log.w(str, message);
            }
        }
    }

    public static void e(String tag, String message) {
        if (INCLUDE) {
            String[] tags = getTags(tag);

            for (String str : tags) {
                android.util.Log.e(str, message);
            }
        }
    }

    private static String[] getTags(String tag) {
        if (tag != null) {
            if (tag.indexOf("|") > 0) {
                String[] tags = tag.split("\\|");
                return tags;
            } else {
                return new String[]{tag};
            }
        }

        return null;
    }

}
