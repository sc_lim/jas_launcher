/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.common;

/**
 * Interface for the fine-grained memory management. The class which wants to release memory based
 * on the system constraints should inherit this interface and implement {@link #performTrimMemory}.
 */
public interface MemoryManageable {
    /** For more information, see {@link android.content.ComponentCallbacks2#onTrimMemory}. */
    void performTrimMemory(int level);
}
