/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.common.concurrent;


import androidx.annotation.NonNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/** A thread factory that creates threads with named <code>prefix-##</code>. */
public class NamedThreadFactory implements ThreadFactory {
    private final AtomicInteger mCount = new AtomicInteger(0);
    private final ThreadFactory mDefaultThreadFactory;
    private final String mPrefix;

    public NamedThreadFactory(final String prefix) {
        mDefaultThreadFactory = Executors.defaultThreadFactory();
        mPrefix = prefix + "-";
    }

    @Override
    public Thread newThread(@NonNull final Runnable runnable) {
        final Thread thread = mDefaultThreadFactory.newThread(runnable);
        thread.setName(mPrefix + mCount.getAndIncrement());
        return thread;
    }

    public boolean namedWithPrefix(Thread thread) {
        return thread.getName().startsWith(mPrefix);
    }
}
