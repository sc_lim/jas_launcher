/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.common;

import android.content.Context;

public interface HasSingletons<C> {

    @SuppressWarnings("unchecked") // injection
    static <C> C get(Class<C> clazz, Context context) {
        return ((HasSingletons<C>) context).singletons();
    }

    /**
     * Returns the strongly typed singleton.
     */
    C singletons();
}
