/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.common;

public class CommonConstants {
    @Deprecated // TODO(b/121158110) refactor so this is not needed.
    public static final String BASE_PACKAGE =
// AOSP_Comment_Out             !BuildConfig.AOSP
// AOSP_Comment_Out                     ? "com.google.android.tv"
// AOSP_Comment_Out                     :
            "com.android.tv";
    /**
     * A constant for the key of the extra data for the app linking intent.
     */
    public static final String EXTRA_APP_LINK_CHANNEL_URI = "app_link_channel_uri";

    /**
     * Video is unavailable because the source is not physically connected, for example the HDMI
     * cable is not connected.
     */
    public static final int VIDEO_UNAVAILABLE_REASON_NOT_CONNECTED = 5;

    private CommonConstants() {
    }
}
