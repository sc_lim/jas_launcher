/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */
package com.altimedia.tvmodule.common;

/** Flags for Cloud EPG */
public interface CloudEpgFlags {

    /**
     * Whether or not this feature is compiled into this build.
     *
     * <p>This returns true by default, unless the is_compiled_selector parameter was set during
     * code generation.
     */
    boolean compiled();

    /** Is the device in a region supported by Cloud Epg */
    boolean supportedRegion();

    /** List of input ids that the TV app will update their EPG. */
    String thirdPartyEpgInputsCsv();
}
