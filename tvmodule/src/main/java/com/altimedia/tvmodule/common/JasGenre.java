/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.common;

import android.content.Context;

import com.altimedia.tvmodule.R;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelRingStorage;

public class JasGenre {
    public static final String DIGITAL_TV         = "1:1";
    public static final String SPORT              = "1:2";
    public static final String NEWS               = "1:3";
    public static final String KIDS               = "1:4";
    public static final String DOCUMENTARY        = "1:5";
    public static final String ENTERTAINMENT      = "1:6";
    public static final String MOVIE              = "1:7";
    public static final String EDUCATION          = "1:8";
    public static final String ADULT              = "1:9";

    public static final int ID_NO_GENRE = -1;

    private static final String[] CANONICAL_GENRES = {
            DIGITAL_TV,
            MOVIE,
            KIDS,
            SPORT,
            NEWS,
            ENTERTAINMENT,
            EDUCATION,
            DOCUMENTARY,
            ADULT
    };

    private static boolean filter(Channel ch) {
        return ch != null && !ch.isHiddenChannel() && !ch.isMosaicChannel() && !ch.isAudioChannel();
    }

    public static final ChannelRingStorage.RingFunction RING_GENRE_DIGITAL_TV = new ChannelRingStorage.RingFunction(DIGITAL_TV, (ch) -> ch.getGenreId() == 0 && filter(ch));
    public static final ChannelRingStorage.RingFunction RING_GENRE_MOVIE = new ChannelRingStorage.RingFunction(MOVIE, (ch) -> ch.getGenreId() == 1 && filter(ch));
    public static final ChannelRingStorage.RingFunction RING_GENRE_KIDS = new ChannelRingStorage.RingFunction(KIDS, (ch) -> ch.getGenreId() == 2 && filter(ch));
    public static final ChannelRingStorage.RingFunction RING_GENRE_SPORT = new ChannelRingStorage.RingFunction(SPORT, (ch) -> ch.getGenreId() == 3 && filter(ch));
    public static final ChannelRingStorage.RingFunction RING_GENRE_NEWS = new ChannelRingStorage.RingFunction(NEWS, (ch) -> ch.getGenreId() == 4 && filter(ch));
    public static final ChannelRingStorage.RingFunction RING_GENRE_ENTERTAINMENT = new ChannelRingStorage.RingFunction(ENTERTAINMENT, (ch) -> ch.getGenreId() == 5 && filter(ch));
    public static final ChannelRingStorage.RingFunction RING_GENRE_EDUCATION = new ChannelRingStorage.RingFunction(EDUCATION, (ch) -> ch.getGenreId() == 6 && filter(ch));
    public static final ChannelRingStorage.RingFunction RING_GENRE_DOCUMENTARY = new ChannelRingStorage.RingFunction(DOCUMENTARY, (ch) -> ch.getGenreId() == 7 && filter(ch));
    public static final ChannelRingStorage.RingFunction RING_GENRE_ADULT = new ChannelRingStorage.RingFunction(ADULT, (ch) -> ch.getGenreId() == 8 && filter(ch));

    private JasGenre() {
    }

    public static String[] getLabels(Context context) {
        String[] items = context.getResources().getStringArray(R.array.jas_genre_labels);
        if (items.length != CANONICAL_GENRES.length) {
            throw new IllegalArgumentException("Genre data mismatch");
        }
        return items;
    }

    public static int getGenreCount() {
        return CANONICAL_GENRES.length;
    }

    public static String getCanonicalGenre(int id) {
        if (id < 0 || id >= CANONICAL_GENRES.length) {
            return null;
        }
        return CANONICAL_GENRES[id];
    }

    public static int getId(String canonicalGenre) {
        if (canonicalGenre == null) {
            return ID_NO_GENRE;
        }
        for (int i = 0; i < CANONICAL_GENRES.length; ++i) {
            if (CANONICAL_GENRES[i].equals(canonicalGenre)) {
                return i;
            }
        }
        return ID_NO_GENRE;
    }

    public static String getGenreLabel(Context context, int id) {
        if (id < 0 || id >= CANONICAL_GENRES.length) {
            return null;
        }
        return getLabels(context)[id];
    }
}
