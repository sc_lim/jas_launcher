/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */
package com.altimedia.tvmodule.common;

import android.content.Context;

/** Static utilities for Has interfaces. */
public final class HasUtils {

    /** Returns the application context. */
    public static Context getApplicationContext(Context context) {
        Context appContext = context.getApplicationContext();
        return appContext != null ? appContext : context;
    }

    private HasUtils() {}
}
