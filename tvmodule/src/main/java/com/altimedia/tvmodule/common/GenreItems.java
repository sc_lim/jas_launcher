/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.common;

import android.content.Context;
import android.media.tv.TvContract.Programs.Genres;

import com.altimedia.tvmodule.R;

public class GenreItems {
    /**
     * Genre ID indicating all channels.
     */
    public static final int ID_ALL_CHANNELS = 0;

    private static final String[] CANONICAL_GENRES = {
            null, // All channels
            Genres.FAMILY_KIDS,
            Genres.SPORTS,
            Genres.SHOPPING,
            Genres.MOVIES,
            Genres.COMEDY,
            Genres.TRAVEL,
            Genres.DRAMA,
            Genres.EDUCATION,
            Genres.ANIMAL_WILDLIFE,
            Genres.NEWS,
            Genres.GAMING,
            Genres.ARTS,
            Genres.ENTERTAINMENT,
            Genres.LIFE_STYLE,
            Genres.MUSIC,
            Genres.PREMIER,
            Genres.TECH_SCIENCE
    };

    private GenreItems() {
    }

    public static String[] getLineUpLabel(Context context) {
        String[] items = context.getResources().getStringArray(R.array.lineup_labels);
        return items;
    }

    public static int getLineUpId(Context context, String lineup) {
        String[] lineupList = getLineUpLabel(context);
        for (int i = 0; i < lineupList.length; i++) {
            String id = lineupList[i];
            if (id.equalsIgnoreCase(lineup)) {
                return i;
            }
        }
        return 0;
    }


    public static String[] getLabels(Context context) {
        String[] items = context.getResources().getStringArray(R.array.genre_labels);
        if (items.length != CANONICAL_GENRES.length) {
            throw new IllegalArgumentException("Genre data mismatch");
        }
        return items;
    }

    public static int getGenreCount() {
        return CANONICAL_GENRES.length;
    }

    /**
     * Returns the canonical genre for the given id. If the id is invalid, {@code null} will be
     * returned instead.
     */
    public static String getCanonicalGenre(int id) {
        if (id < 0 || id >= CANONICAL_GENRES.length) {
            return null;
        }
        return CANONICAL_GENRES[id];
    }

    /**
     * Returns id for the given canonical genre. If the genre is invalid, {@link #ID_ALL_CHANNELS}
     * will be returned instead.
     */
    public static int getId(String canonicalGenre) {
        if (canonicalGenre == null) {
            return ID_ALL_CHANNELS;
        }
        for (int i = 1; i < CANONICAL_GENRES.length; ++i) {
            if (CANONICAL_GENRES[i].equals(canonicalGenre)) {
                return i;
            }
        }
        return ID_ALL_CHANNELS;
    }
}
