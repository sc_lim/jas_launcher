/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;

import java.util.List;

import com.altimedia.tvmodule.service.EpgSyncJobService;

public class TifController {
    private static final TifController ourInstance = new TifController();

    public static TifController getInstance() {
        return ourInstance;
    }

    private TifController() {
    }

    public void init(Context context) {
        JobScheduler jobScheduler =
                (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        // If there are not pending jobs. Create a sync job and schedule it.
        List<JobInfo> pendingJobs = jobScheduler.getAllPendingJobs();
        if (pendingJobs.isEmpty()) {
            String inputId = context.getSharedPreferences(com.google.android.media.tv.companionlibrary.EpgSyncJobService.PREFERENCE_EPG_SYNC,
                    Context.MODE_PRIVATE).getString(com.google.android.media.tv.companionlibrary.EpgSyncJobService.BUNDLE_KEY_INPUT_ID, null);
            if (inputId != null) {
                // Set up periodic sync only when input has set up.
                com.google.android.media.tv.companionlibrary.EpgSyncJobService.setUpPeriodicSync(context, inputId,
                        new ComponentName(context, EpgSyncJobService.class));
            }
            return;
        }
        // On L/L-MR1, reschedule the pending jobs.
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            for (JobInfo job : pendingJobs) {
                if (job.isPersisted()) {
                    jobScheduler.schedule(job);
                }
            }
        }
    }

    private final long ONE_HOUR = 1000 * 60 * 60;
    private final long ONE_DAY = 24 * ONE_HOUR;

    public void startScan(Context context, String mInputId) {
        com.google.android.media.tv.companionlibrary.EpgSyncJobService.cancelAllSyncRequests(context);
        com.google.android.media.tv.companionlibrary.EpgSyncJobService.requestImmediateSync(
                context,
                mInputId, ONE_DAY * 7,
                new ComponentName(context, EpgSyncJobService.class));
    }
}
