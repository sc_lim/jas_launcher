/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule;

import android.content.Context;

import java.util.concurrent.Executor;

import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.TvInputManagerHelper;


/**
 * Interface with getters for application scoped singletons.
 */
public interface TvSingletons extends BaseSingletons {

    static TvSingletons getSingletons(Context context) {
        return (TvSingletons) BaseApplication.getSingletons(context);
    }

    ChannelDataManager getChannelDataManager();

    ProgramDataManager getProgramDataManager();

    boolean isRunningInMainProcess();

    TvInputManagerHelper getTvInputManagerHelper();

    Executor getDbExecutor();

    BackendKnobsFlags getBackendKnobs();
}
