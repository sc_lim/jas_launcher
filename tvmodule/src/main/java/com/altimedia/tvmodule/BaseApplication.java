/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule;

import android.app.Application;
import android.content.Context;

import com.altimedia.util.time.Clock;
import com.altimedia.util.time.NetworkTimeClock;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import com.altimedia.util.Log;


/**
 * The base application class for TV applications.
 */
public abstract class BaseApplication extends Application implements BaseSingletons, LifecycleObserver {
    private final String TAG = BaseApplication.class.getSimpleName();
    protected static BaseSingletons sSingletons;

    public static BaseSingletons getSingletons(Context context) {
        if (sSingletons == null) {
            sSingletons = (BaseSingletons) context.getApplicationContext();
        }
        return sSingletons;
    }

    private NetworkTimeClock mNetworkTimeClock = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mNetworkTimeClock = new NetworkTimeClock(getApplicationContext());
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        ProcessLifecycleOwner.get().getLifecycle().removeObserver(this);
        if (mNetworkTimeClock != null) {
            mNetworkTimeClock.release();
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        //App in background
        if (Log.INCLUDE) {
            Log.d(TAG, "onAppBackgrounded");
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        // App in foreground
        if (Log.INCLUDE) {
            Log.d(TAG, "onAppForegrounded");
        }
        if (mNetworkTimeClock != null) {
            mNetworkTimeClock.sync();
        }
    }


    @Override
    public Clock getClock() {
        return mNetworkTimeClock;
    }
}
