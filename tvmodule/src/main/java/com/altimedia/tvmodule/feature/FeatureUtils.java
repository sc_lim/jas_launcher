/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.feature;

import android.content.Context;

import java.util.Arrays;

import com.altimedia.tvmodule.util.CommonUtils;

public class FeatureUtils {

    /**
     * Returns a feature that is enabled if any of {@code features} is enabled.
     *
     * @param features the features to or
     */
    public static Feature or(final Feature... features) {
        return new Feature() {
            @Override
            public boolean isEnabled(Context context) {
                for (Feature f : features) {
                    if (f.isEnabled(context)) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public String toString() {
                return "or(" + Arrays.asList(features) + ")";
            }
        };
    }

    /**
     * Returns a feature that is enabled if all of {@code features} is enabled.
     *
     * @param features the features to and
     */
    public static Feature and(final Feature... features) {
        return new Feature() {
            @Override
            public boolean isEnabled(Context context) {
                for (Feature f : features) {
                    if (!f.isEnabled(context)) {
                        return false;
                    }
                }
                return true;
            }

            @Override
            public String toString() {
                return "and(" + Arrays.asList(features) + ")";
            }
        };
    }

    /**
     * Returns a feature that is opposite of the given {@code feature}.
     *
     * @param feature the feature to invert
     */
    public static Feature not(final Feature feature) {
        return new Feature() {
            @Override
            public boolean isEnabled(Context context) {
                return !feature.isEnabled(context);
            }

            @Override
            public String toString() {
                return "not(" + feature + ")";
            }
        };
    }

    /** A feature that is always enabled. */
    public static final Feature ON =
            new Feature() {
                @Override
                public boolean isEnabled(Context context) {
                    return true;
                }

                @Override
                public String toString() {
                    return "on";
                }
            };

    /** A feature that is always disabled. */
    public static final Feature OFF =
            new Feature() {
                @Override
                public boolean isEnabled(Context context) {
                    return false;
                }

                @Override
                public String toString() {
                    return "off";
                }
            };

    /** True if running in robolectric. */
    public static final Feature ROBOLECTRIC =
            new Feature() {
                @Override
                public boolean isEnabled(Context context) {
                    return CommonUtils.isRoboTest();
                }

                @Override
                public String toString() {
                    return "isRobolecteric";
                }
            };

    private FeatureUtils() {}
}
