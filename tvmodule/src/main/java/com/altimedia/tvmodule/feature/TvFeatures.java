/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.feature;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import static com.altimedia.tvmodule.feature.FeatureUtils.OFF;
import static com.altimedia.tvmodule.feature.FeatureUtils.ON;

public class TvFeatures extends CommonFeatures {

    public static final Feature EPG_SEARCH = OFF;

    /** A flag which indicates that LC app is unhidden even when there is no input. */

    public static final Feature PICTURE_IN_PICTURE =
            new Feature() {
                private Boolean mEnabled;

                @Override
                public boolean isEnabled(Context context) {
                    if (mEnabled == null) {
                        mEnabled =
                                Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
                                        && context.getPackageManager()
                                        .hasSystemFeature(
                                                PackageManager.FEATURE_PICTURE_IN_PICTURE);
                    }
                    return mEnabled;
                }
            };

    /** Enable a conflict dialog between currently watched channel and upcoming recording. */
    public static final Feature SHOW_UPCOMING_CONFLICT_DIALOG = OFF;

    /** Use input blacklist to disable partner's tuner input. */
    public static final Feature USE_PARTNER_INPUT_BLACKLIST = ON;

    public static final Feature USE_ALTI_TIS_INPUT_ONLY = ON;

    private TvFeatures() {}
}
