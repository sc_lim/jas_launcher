/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.feature;

import android.content.Context;

import com.altimedia.tvmodule.util.CommonUtils;

public class TestableFeature implements Feature {
    private static final String TAG = "TestableFeature";
    private static final String DETAIL_MESSAGE =
            "TestableFeatures should only be changed in tests.";

    private final Feature mDelegate;
    private Boolean mTestValue = null;

    /** Creates testable feature. */
    public static TestableFeature createTestableFeature(Feature delegate) {
        return new TestableFeature(delegate);
    }

    /** Creates testable feature with initial value. */
    public static TestableFeature createTestableFeature(Feature delegate, Boolean initialValue) {
        return new TestableFeature(delegate, initialValue);
    }

    private TestableFeature(Feature delegate) {
        mDelegate = delegate;
    }

    private TestableFeature(Feature delegate, Boolean initialValue) {
        mDelegate = delegate;
        mTestValue = initialValue;
    }


    @Override
    public boolean isEnabled(Context context) {
        if (CommonUtils.isRunningInTest() && mTestValue != null) {
            return mTestValue;
        }
        return mDelegate.isEnabled(context);
    }

    @Override
    public String toString() {
        String msg = mDelegate.toString();
        if (CommonUtils.isRunningInTest()) {
            if (mTestValue == null) {
                msg = "Testable Feature is unchanged: " + msg;
            } else {
                msg = "Testable Feature is " + (mTestValue ? "on" : "off") + " was " + msg;
            }
        }
        return msg;
    }
}
