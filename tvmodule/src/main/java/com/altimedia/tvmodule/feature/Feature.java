/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.feature;

import android.content.Context;

/**
 * A feature is elements of code that are turned off for most users until a feature is fully
 * launched.
 *
 * <p>Expected usage is:
 *
 * <pre>{@code
 * if (MY_FEATURE.isEnabled(context) {
 *     showNewCoolUi();
 * } else{
 *     showOldBoringUi();
 * }
 * }</pre>
 */
public interface Feature {
    boolean isEnabled(Context context);
}
