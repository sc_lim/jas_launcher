/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package com.altimedia.tvmodule.dao;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.Nullable;

import kr.altimedia.agent.epg.data.ExtensionInfo;

public interface Channel {
    long INVALID_ID = -1;
    int LOAD_IMAGE_TYPE_CHANNEL_LOGO = 1;
    int LOAD_IMAGE_TYPE_APP_LINK_ICON = 2;
    int LOAD_IMAGE_TYPE_APP_LINK_POSTER_ART = 3;
    /**
     * When a TIS doesn't provide any information about app link, and it doesn't have a leanback
     * launch intent, there will be no app link card for the TIS.
     */
    int APP_LINK_TYPE_NONE = -1;
    /**
     * When a TIS provide a specific app link information, the app link card will be {@code
     * APP_LINK_TYPE_CHANNEL} which contains all the provided information.
     */
    int APP_LINK_TYPE_CHANNEL = 1;
    /**
     * When a TIS doesn't provide a specific app link information, but the app has a leanback launch
     * intent, the app link card will be {@code APP_LINK_TYPE_APP} which launches the application.
     */
    int APP_LINK_TYPE_APP = 2;
    /**
     * Channel number delimiter between major and minor parts.
     */
    char CHANNEL_NUMBER_DELIMITER = '-';

    long getId();

    String getServiceId();

    Uri getUri();

    String getPackageName();

    String getInputId();

    String getType();

    String getServiceType();

    String getDisplayNumber();

    @Nullable
    String getDisplayName();

    String getDescription();

    String getVideoFormat();

    boolean isPassthrough();

    String getDisplayText();

    String getAppLinkText();

    int getAppLinkColor();

    String getAppLinkIconUri();

    String getAppLinkPosterArtUri();

    String getAppLinkIntentUri();

    String getNetworkAffiliation();

    String getLogoUri();

    boolean isRecordingProhibited();

    boolean isPhysicalTunerChannel();

    boolean isBrowsable();

    boolean isSearchable();

    boolean isLocked();

    boolean hasSameReadOnlyInfo(Channel mCurrentChannel);

    void setChannelLogoExist(boolean result);

    void setBrowsable(boolean browsable);

    void setLocked(boolean locked);

    void copyFrom(Channel channel);

    void setLogoUri(String logoUri);

    void setNetworkAffiliation(String networkAffiliation);

    boolean channelLogoExists();

    byte[] getInternalProviderDataByteArray();

    ExtensionInfo.JasChannelExtension getExtension();

    long getTimeshiftService();

    String getGenre();

    int getGenreId();

    boolean isAdultChannel();

    String getCentralUrl();

    String getLocalUrl();

    int getAppLinkType(Context context);

    Intent getAppLinkIntent(Context context);

    void prefetchImage(
            Context mContext,
            int loadImageTypeChannelLogo,
            int mPosterArtWidth,
            int mPosterArtHeight);

    boolean isFavoriteChannel();

    void setFavoriteChannel(boolean favoriteChannel);

    boolean isPaidChannel();

    boolean isHiddenChannel();

    boolean isPromoChannel();

    boolean isAudioChannel();

    boolean isMosaicChannel();

    boolean isUHDChannel();

    boolean isSubscribedChannel();

    void setSubscribedChannel(boolean subscribedChannel);
}
