/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.dao;

import android.content.Context;
import android.media.tv.TvContentRating;

import androidx.annotation.IntDef;
import androidx.annotation.Nullable;

import com.google.common.collect.ImmutableList;

import java.util.Comparator;
import java.util.Objects;

public interface BaseProgram {
    int VIDEO_TYPE_UNKNOWN = 0;
    int VIDEO_TYPE_SD = 1;
    int VIDEO_TYPE_HD = 2;
    int VIDEO_TYPE_UHD = 3;
    @IntDef({VIDEO_TYPE_UNKNOWN, VIDEO_TYPE_SD, VIDEO_TYPE_HD, VIDEO_TYPE_UHD})
    @interface VideoType{}

    /**
     * Comparator used to compare {@link BaseProgram} according to its season and episodes number.
     * If a program's season or episode number is null, it will be consider "smaller" than programs
     * with season or episode numbers.
     */
    Comparator<BaseProgram> EPISODE_COMPARATOR = new EpisodeComparator(false);
    /**
     * Comparator used to compare {@link BaseProgram} according to its season and episodes number
     * with season numbers in a reversed order. If a program's season or episode number is null, it
     * will be consider "smaller" than programs with season or episode numbers.
     */
    Comparator<BaseProgram> SEASON_REVERSED_EPISODE_COMPARATOR = new EpisodeComparator(true);

    String COLUMN_SERIES_ID = "series_id";
    String COLUMN_STATE = "state";

    /** Compares two strings represent season numbers or episode numbers of programs. */
    static int numberCompare(String s1, String s2) {
        if (Objects.equals(s1, s2)) {
            return 0;
        } else if (s1 == null) {
            return -1;
        } else if (s2 == null) {
            return 1;
        } else if (s1.equals(s2)) {
            return 0;
        }
        try {
            return Integer.compare(Integer.parseInt(s1), Integer.parseInt(s2));
        } catch (NumberFormatException e) {
            return s1.compareTo(s2);
        }
    }

    /** Generates the series ID for the other inputs than the tuner TV input. */
    static String generateSeriesId(String packageName, String title) {
        return packageName + "/" + title;
    }

    /** Returns ID of the program. */
    long getId();

    /** Returns the title of the program. */
    String getTitle();

    /** Returns the episode title. */
    String getEpisodeTitle();

    /** Returns the displayed title of the program episode. */
    @Nullable
    String getEpisodeDisplayTitle(Context context);

    /**
     * Returns the content description of the program episode, suitable for being spoken by an
     * accessibility service.
     */
    String getEpisodeContentDescription(Context context);

    /** Returns the description of the program. */
    String getDescription();

    /** Returns the long description of the program. */
    String getLongDescription();

    /** Returns the start time of the program in Milliseconds. */
    long getStartTimeUtcMillis();

    /** Returns the end time of the program in Milliseconds. */
    long getEndTimeUtcMillis();

    /** Returns the duration of the program in Milliseconds. */
    long getDurationMillis();

    /** Returns the series ID. */
    @Nullable
    String getSeriesId();

    /** Returns the season number. */
    String getSeasonNumber();

    /** Returns the episode number. */
    String getEpisodeNumber();

    /** Returns URI of the program's poster. */
    String getPosterArtUri();

    /** Returns URI of the program's thumbnail. */
    String getThumbnailUri();

    /** Returns the array of the ID's of the canonical genres. */
    int[] getCanonicalGenreIds();

    /** Returns the array of content ratings. */
    ImmutableList<TvContentRating> getContentRatings();

    /** Returns channel's ID of the program. */
    long getChannelId();

    /** Returns if the program is valid. */
    boolean isValid();

    /** Checks whether the program is episodic or not. */
    boolean isEpisodic();

    /** Generates the series ID for the other inputs than the tuner TV input. */
    class EpisodeComparator implements Comparator<BaseProgram> {
        private final boolean mReversedSeason;

        EpisodeComparator(boolean reversedSeason) {
            mReversedSeason = reversedSeason;
        }

        @Override
        public int compare(BaseProgram lhs, BaseProgram rhs) {
            if (lhs == rhs) {
                return 0;
            }
            int seasonNumberCompare = numberCompare(lhs.getSeasonNumber(), rhs.getSeasonNumber());
            if (seasonNumberCompare != 0) {
                return mReversedSeason ? -seasonNumberCompare : seasonNumberCompare;
            } else {
                return numberCompare(lhs.getEpisodeNumber(), rhs.getEpisodeNumber());
            }
        }
    }

    /** Returns ProgramId of the program, from the extensionData */
    String getProgramId();

    /** Returns EventId of the program, from the extensionData */
    int getEventId();

    /** Returns the ServiceId of the channel to which the program belongs, from the extensionData */
    int getServiceId();

    /** Returns VideoType of the program, from the extensionData */
    @VideoType int getVideoType();
}
