/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package com.altimedia.tvmodule.dao;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.tv.TvContract;
import android.media.tv.TvInputInfo;
import android.net.Uri;
import android.text.TextUtils;

import com.altimedia.tvmodule.common.CommonConstants;
import com.altimedia.tvmodule.common.JasGenre;
import com.altimedia.tvmodule.util.CommonUtils;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.tvmodule.util.Utils;
import com.altimedia.util.Log;

import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import kr.altimedia.agent.epg.data.ExtensionInfo;
import kr.altimedia.jasmine.tis.util.EPGExtendedInfo;

public class ChannelImpl implements Channel {
    private static final String TAG = "ChannelImpl";

    /**
     * Compares the channel numbers of channels which belong to the same input.
     */
    public static final Comparator<Channel> CHANNEL_NUMBER_COMPARATOR =
            new Comparator<Channel>() {
                @Override
                public int compare(Channel lhs, Channel rhs) {
                    return ChannelNumber.compare(lhs.getDisplayNumber(), rhs.getDisplayNumber());
                }
            };

    private static final int APP_LINK_TYPE_NOT_SET = 0;
    private static final String INVALID_PACKAGE_NAME = "packageName";

    public static final String[] PROJECTION = {
            // Columns must match what is read in ChannelImpl.fromCursor()
            TvContract.Channels._ID,
            TvContract.Channels.COLUMN_PACKAGE_NAME,
            TvContract.Channels.COLUMN_INPUT_ID,
            TvContract.Channels.COLUMN_TYPE,
            TvContract.Channels.COLUMN_DISPLAY_NUMBER,
            TvContract.Channels.COLUMN_DISPLAY_NAME,
            TvContract.Channels.COLUMN_DESCRIPTION,
            TvContract.Channels.COLUMN_VIDEO_FORMAT,
            TvContract.Channels.COLUMN_SERVICE_TYPE,
            TvContract.Channels.COLUMN_BROWSABLE,
            TvContract.Channels.COLUMN_SEARCHABLE,
            TvContract.Channels.COLUMN_LOCKED,
            TvContract.Channels.COLUMN_APP_LINK_TEXT,
            TvContract.Channels.COLUMN_APP_LINK_COLOR,
            TvContract.Channels.COLUMN_APP_LINK_ICON_URI,
            TvContract.Channels.COLUMN_APP_LINK_POSTER_ART_URI,
            TvContract.Channels.COLUMN_APP_LINK_INTENT_URI,
            TvContract.Channels.COLUMN_INTERNAL_PROVIDER_DATA,
            TvContract.Channels.COLUMN_NETWORK_AFFILIATION,
            TvContract.Channels.COLUMN_INTERNAL_PROVIDER_FLAG2, // Only used in bundled input
    };

    /**
     * Creates {@code ChannelImpl} object from cursor.
     *
     * <p>The query that created the cursor MUST use {@link #PROJECTION}
     */
    public static ChannelImpl fromCursor(Cursor cursor) {
        // Columns read must match the order of {@link #PROJECTION}
        ChannelImpl channel = new ChannelImpl();
        int index = 0;
        channel.mId = cursor.getLong(index++);
        channel.mPackageName = Utils.intern(cursor.getString(index++));
        channel.mInputId = Utils.intern(cursor.getString(index++));
        channel.mType = Utils.intern(cursor.getString(index++));
        channel.mDisplayNumber = normalizeDisplayNumber(cursor.getString(index++));
        channel.mDisplayName = cursor.getString(index++);
        channel.mDescription = cursor.getString(index++);
        channel.mVideoFormat = Utils.intern(cursor.getString(index++));
        channel.mServiceType = Utils.intern(cursor.getString(index++));
        channel.mBrowsable = cursor.getInt(index++) == 1;
        channel.mSearchable = cursor.getInt(index++) == 1;
        channel.mLocked = cursor.getInt(index++) == 1;
        channel.mAppLinkText = cursor.getString(index++);
        channel.mAppLinkColor = cursor.getInt(index++);
        channel.mAppLinkIconUri = cursor.getString(index++);
        channel.mAppLinkPosterArtUri = cursor.getString(index++);
        channel.mAppLinkIntentUri = cursor.getString(index++);
        channel.mInternalProviderData = (cursor.getBlob(index++));
        channel.mNetworkAffiliation = cursor.getString(index++);
        if (CommonUtils.isBundledInput(channel.mInputId)) {
            channel.mRecordingProhibited = cursor.getInt(index++) != 0;
        }
        return channel;
    }

    /**
     * Replaces the channel number separator with dash('-').
     */
    public static String normalizeDisplayNumber(String string) {
        if (!TextUtils.isEmpty(string)) {
            int length = string.length();
            for (int i = 0; i < length; i++) {
                char c = string.charAt(i);
                if (c == '.'
                        || Character.isWhitespace(c)
                        || Character.getType(c) == Character.DASH_PUNCTUATION) {
                    StringBuilder sb = new StringBuilder(string);
                    sb.setCharAt(i, CHANNEL_NUMBER_DELIMITER);
                    return sb.toString();
                }
            }
        }
        return string;
    }

    /**
     * ID of this channel. Matches to BaseColumns._ID.
     */
    private long mId;

    private String mPackageName;
    private String mInputId;
    private String mType;
    private String mDisplayNumber;
    private String mDisplayName;
    private String mDescription;
    private String mVideoFormat;
    private String mServiceType;
    private boolean mBrowsable;
    private boolean mSearchable;
    private boolean mLocked;
    private boolean mIsPassthrough;
    private String mAppLinkText;
    private int mAppLinkColor;
    private String mAppLinkIconUri;
    private String mAppLinkPosterArtUri;
    private String mAppLinkIntentUri;
    private Intent mAppLinkIntent;
    private byte[] mInternalProviderData;
    private ExtensionInfo.JasChannelExtension mExtension;
    private String mNetworkAffiliation;
    private int mAppLinkType;
    private String mLogoUri;
    private boolean mRecordingProhibited;
    private boolean mFavoriteChannel;
    private boolean mSubscribedChannel;

    private boolean mChannelLogoExist;

    private ExtensionInfo.JasChannelExtension NULL_EXTENSION = ExtensionInfo.JasChannelExtension.getDefaultInstance();

    private ChannelImpl() {
        // Do nothing.
    }

    @Override
    public boolean isFavoriteChannel() {
        return mFavoriteChannel;
    }

    @Override
    public void setFavoriteChannel(boolean favoriteChannel) {
        mFavoriteChannel = favoriteChannel;
    }

    @Override
    public boolean isPaidChannel() {
        return getExtension().getIsServicePaid() && !isSubscribedChannel();
    }

    @Override
    public boolean isHiddenChannel() {
        return getExtension().hasServiceVisibility() ? getExtension().getServiceVisibility() == false : false /* by default */;
    }

    @Override
    public boolean isPromoChannel() {
        return getExtension().hasPromoService() ? getExtension().getPromoService() == 1 : false /* by default */;
    }

    //TODO:update all TYPE_OF_SERVICE & move constants to another class
    private static final String TYPE_OF_SERVICE_VIDEO = "1";
    private static final String TYPE_OF_SERVICE_MOSAIC = "6";
    private static final String TYPE_OF_SERVICE_DATA = "C";
    private static final String TYPE_OF_SERVICE_HD = "11";
    private static final String TYPE_OF_SERVICE_AUDIO = "90";
    private static final String TYPE_OF_SERVICE_UHD = "94";

    public static final String typeToString(String typeOfService) {
        if (TYPE_OF_SERVICE_VIDEO.equalsIgnoreCase(typeOfService)) {
            return "VIDEO";
        } else if (TYPE_OF_SERVICE_MOSAIC.equalsIgnoreCase(typeOfService)) {
            return "MOSAIC";
        } else if (TYPE_OF_SERVICE_DATA.equalsIgnoreCase(typeOfService)) {
            return "DATA";
        } else if (TYPE_OF_SERVICE_HD.equalsIgnoreCase(typeOfService)) {
            return "HD";
        } else if (TYPE_OF_SERVICE_AUDIO.equalsIgnoreCase(typeOfService)) {
            return "AUDIO";
        } else if (TYPE_OF_SERVICE_UHD.equalsIgnoreCase(typeOfService)) {
            return "UHD";
        } else {
            return typeOfService;
        }
    }

    @Override
    public boolean isAudioChannel() {
        String typeOfService = getExtension().getTypeOfService();
        return !TextUtils.isEmpty(typeOfService) && TYPE_OF_SERVICE_AUDIO.equals(typeOfService);
    }

    @Override
    public boolean isMosaicChannel() {
        String typeOfService = getExtension().getTypeOfService();
        return !TextUtils.isEmpty(typeOfService) && TYPE_OF_SERVICE_MOSAIC.equals(typeOfService);
    }

    @Override
    public boolean isUHDChannel() {
        String typeOfService = getExtension().getTypeOfService();
        return !TextUtils.isEmpty(typeOfService) && TYPE_OF_SERVICE_UHD.equals(typeOfService);
    }

    @Override
    public boolean isSubscribedChannel() {
        return mSubscribedChannel;
    }

    @Override
    public void setSubscribedChannel(boolean subscribedChannel) {
        mSubscribedChannel = subscribedChannel;
    }

    @Override
    public long getId() {
        return mId;
    }

    @Override
    public String getServiceId() {
        return getExtension().getServiceId();
    }

    @Override
    public Uri getUri() {
        if (isPassthrough()) {
            return TvContract.buildChannelUriForPassthroughInput(mInputId);
        } else {
            return TvContract.buildChannelUri(mId);
        }
    }

    @Override
    public String getPackageName() {
        return mPackageName;
    }

    @Override
    public String getInputId() {
        return mInputId;
    }

    @Override
    public String getType() {
        return mType;
    }

    public String getServiceType() {
        return mServiceType;
    }

    @Override
    public String getDisplayNumber() {
        return mDisplayNumber;
    }

    @Override
    @Nullable
    public String getDisplayName() {
        return mDisplayName;
    }

    @Override
    public String getDescription() {
        return mDescription;
    }

    @Override
    public String getVideoFormat() {
        return mVideoFormat;
    }

    @Override
    public boolean isPassthrough() {
        return mIsPassthrough;
    }

    /**
     * Gets identification text for displaying or debugging. It's made from Channels' display number
     * plus their display name.
     */
    @Override
    public String getDisplayText() {
        return TextUtils.isEmpty(mDisplayName)
                ? mDisplayNumber
                : mDisplayNumber + " " + mDisplayName;
    }

    @Override
    public String getAppLinkText() {
        return mAppLinkText;
    }

    @Override
    public int getAppLinkColor() {
        return mAppLinkColor;
    }

    @Override
    public String getAppLinkIconUri() {
        return mAppLinkIconUri;
    }

    @Override
    public String getAppLinkPosterArtUri() {
        return mAppLinkPosterArtUri;
    }

    @Override
    public String getAppLinkIntentUri() {
        return mAppLinkIntentUri;
    }

    @Override
    public String getNetworkAffiliation() {
        return mNetworkAffiliation;
    }

    /**
     * Returns channel logo uri which is got from cloud, it's used only for ChannelLogoFetcher.
     */
    @Override
    public String getLogoUri() {
        return mLogoUri;
    }

    @Override
    public boolean isRecordingProhibited() {
        return mRecordingProhibited;
    }

    /**
     * Checks whether this channel is physical tuner channel or not.
     */
    @Override
    public boolean isPhysicalTunerChannel() {
        return !TextUtils.isEmpty(mType) && !TvContract.Channels.TYPE_OTHER.equals(mType);
    }

    /**
     * Checks if two channels equal by checking ids.
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ChannelImpl)) {
            return false;
        }
        ChannelImpl other = (ChannelImpl) o;
        // All pass-through TV channels have INVALID_ID value for mId.
        return mId == other.mId
                && TextUtils.equals(mInputId, other.mInputId)
                && mIsPassthrough == other.mIsPassthrough;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mId, mInputId, mIsPassthrough);
    }

    @Override
    public boolean isBrowsable() {
        return mBrowsable;
    }

    /**
     * Checks whether this channel is searchable or not.
     */
    @Override
    public boolean isSearchable() {
        return mSearchable;
    }

    @Override
    public boolean isLocked() {
        return mLocked;
    }

    public void setBrowsable(boolean browsable) {
        mBrowsable = browsable;
    }

    public void setLocked(boolean locked) {
        mLocked = locked;
    }

    /**
     * Sets channel logo uri which is got from cloud.
     */
    public void setLogoUri(String logoUri) {
        mLogoUri = logoUri;
    }

    @Override
    public void setNetworkAffiliation(String networkAffiliation) {
        mNetworkAffiliation = networkAffiliation;
    }

    @Override
    public byte[] getInternalProviderDataByteArray() {
        return mInternalProviderData;
    }

    @NonNull
    public ExtensionInfo.JasChannelExtension getExtension() {
        if (mExtension == null) {
            mExtension = NULL_EXTENSION;
            if (mInternalProviderData != null) {
                try {
                    mExtension = EPGExtendedInfo.Channel.decode(mInternalProviderData);
                    //Log.d(TAG, "extended data:"+mExtension);
                } catch (EPGExtendedInfo.ParseException e) {
                    //Log.d(TAG, "getExtension() parse exception. e:"+e);
                    e.printStackTrace();
                }
            }
        }
        return mExtension;
    }

    public long getTimeshiftService() {
        String hours = getExtension().hasTimeshiftService() ? getExtension().getTimeshiftService() : null;
        long ms = 0;
        if (hours != null) {
            try {
                int h = Integer.parseInt(hours);
                ms = h * 60 * 60 * 1000;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return ms;
    }

    public String getGenre() {
        return getExtension().hasServiceGenre() ? getExtension().getServiceGenre() : null;
    }

    public int getGenreId() {
        return JasGenre.getId(getGenre());
    }

    public boolean isAdultChannel() {
        return JasGenre.ADULT.equalsIgnoreCase(getGenre());
    }

    @Override
    public String getCentralUrl() {
        return getExtension().getCentralUrl();
    }

    @Override
    public String getLocalUrl() {
        return getExtension().getLocalUrl();
    }

    /**
     * Check whether {@code other} has same read-only channel info as this. But, it cannot check two
     * channels have same logos. It also excludes browsable and locked, because two fields are
     * changed by TV app.
     */
    @Override
    public boolean hasSameReadOnlyInfo(Channel other) {
        return other != null
                && Objects.equals(mId, other.getId())
                && Objects.equals(mPackageName, other.getPackageName())
                && Objects.equals(mInputId, other.getInputId())
                && Objects.equals(mType, other.getType())
                && Objects.equals(mDisplayNumber, other.getDisplayNumber())
                && Objects.equals(mDisplayName, other.getDisplayName())
                && Objects.equals(mDescription, other.getDescription())
                && Objects.equals(mVideoFormat, other.getVideoFormat())
                && mIsPassthrough == other.isPassthrough()
                && Objects.equals(mAppLinkText, other.getAppLinkText())
                && mAppLinkColor == other.getAppLinkColor()
                && Objects.equals(mAppLinkIconUri, other.getAppLinkIconUri())
                && Objects.equals(mAppLinkPosterArtUri, other.getAppLinkPosterArtUri())
                && Objects.equals(mAppLinkIntentUri, other.getAppLinkIntentUri())
                && Objects.equals(mRecordingProhibited, other.isRecordingProhibited())
                && Objects.equals(mInternalProviderData, other.getInternalProviderDataByteArray());
    }

    @Override
    public String toString() {
        ExtensionInfo.JasChannelExtension ext = getExtension();
        return "Channel [" + getDisplayNumber() + ":"+getDisplayName()+"] "
                + "{serviceId=" + getServiceId()
                + ", browsable=" + isBrowsable()
                + ", paidCh=" + ext.getIsServicePaid()
                + ", subscribed=" + isSubscribedChannel()
                + ", locked=" + isLocked()
                + ", fav=" + isFavoriteChannel()
                + ", audio=" + isAudioChannel()
                + ", mosaic=" + isMosaicChannel()
                + ", type=" + typeToString(ext.getTypeOfService())
                + ", genre=" + (ext.hasServiceGenre() ? ext.getServiceGenre() : "none")
                + ", localUrl=" + getLocalUrl()
                + ", centralUrl=" + getCentralUrl()
                + ", timeshift=" + (getTimeshiftService() / 3600000L) + "h"
                + "} "
                + "{id=" + mId
                + ", packageName=" + mPackageName
                + ", inputId=" + mInputId
                + ", type=" + mType
                + ", description=\"" + mDescription + "\""
                + ", videoFormat=" + mVideoFormat
                + ", isPassthrough=" + mIsPassthrough
                + ", searchable=" + mSearchable
                + ", appLinkText=" + mAppLinkText
                + ", recordingProhibited=" + mRecordingProhibited
                + "}";
    }

    @Override
    public void copyFrom(Channel channel) {
        if (channel instanceof ChannelImpl) {
            copyFrom((ChannelImpl) channel);
        } else {
            // copy what we can
            mId = channel.getId();
            mPackageName = channel.getPackageName();
            mInputId = channel.getInputId();
            mType = channel.getType();
            mDisplayNumber = channel.getDisplayNumber();
            mDisplayName = channel.getDisplayName();
            mDescription = channel.getDescription();
            mVideoFormat = channel.getVideoFormat();
            mIsPassthrough = channel.isPassthrough();
            mBrowsable = channel.isBrowsable();
            mSearchable = channel.isSearchable();
            mLocked = channel.isLocked();
            mAppLinkText = channel.getAppLinkText();
            mAppLinkColor = channel.getAppLinkColor();
            mAppLinkIconUri = channel.getAppLinkIconUri();
            mAppLinkPosterArtUri = channel.getAppLinkPosterArtUri();
            mAppLinkIntentUri = channel.getAppLinkIntentUri();
            mInternalProviderData = channel.getInternalProviderDataByteArray();
            mNetworkAffiliation = channel.getNetworkAffiliation();
            mRecordingProhibited = channel.isRecordingProhibited();
            mChannelLogoExist = channel.channelLogoExists();
            mNetworkAffiliation = channel.getNetworkAffiliation();
        }
    }

    @SuppressWarnings("ReferenceEquality")
    public void copyFrom(ChannelImpl channel) {
        ChannelImpl other = channel;
        if (this == other) {
            return;
        }
        mId = other.mId;
        mPackageName = other.mPackageName;
        mInputId = other.mInputId;
        mType = other.mType;
        mDisplayNumber = other.mDisplayNumber;
        mDisplayName = other.mDisplayName;
        mDescription = other.mDescription;
        mVideoFormat = other.mVideoFormat;
        mIsPassthrough = other.mIsPassthrough;
        mBrowsable = other.mBrowsable;
        mSearchable = other.mSearchable;
        mLocked = other.mLocked;
        mAppLinkText = other.mAppLinkText;
        mAppLinkColor = other.mAppLinkColor;
        mAppLinkIconUri = other.mAppLinkIconUri;
        mAppLinkPosterArtUri = other.mAppLinkPosterArtUri;
        mAppLinkIntentUri = other.mAppLinkIntentUri;
        mInternalProviderData = channel.mInternalProviderData;
        mExtension = null;
        mNetworkAffiliation = channel.mNetworkAffiliation;
        mAppLinkIntent = other.mAppLinkIntent;
        mAppLinkType = other.mAppLinkType;
        mRecordingProhibited = other.mRecordingProhibited;
        mChannelLogoExist = other.mChannelLogoExist;
    }

    /**
     * Creates a channel for a passthrough TV input.
     */
    public static ChannelImpl createPassthroughChannel(Uri uri) {
        if (!TvContract.isChannelUriForPassthroughInput(uri)) {
            throw new IllegalArgumentException("URI is not a passthrough channel URI");
        }
        String inputId = uri.getPathSegments().get(1);
        return createPassthroughChannel(inputId);
    }

    /**
     * Creates a channel for a passthrough TV input with {@code inputId}.
     */
    public static ChannelImpl createPassthroughChannel(String inputId) {
        return new Builder().setInputId(inputId).setPassthrough(true).build();
    }

    /**
     * Checks whether the channel is valid or not.
     */
    public static boolean isValid(Channel channel) {
        return channel != null && (channel.getId() != INVALID_ID || channel.isPassthrough());
    }

    /**
     * Builder class for {@code ChannelImpl}. Suppress using this outside of ChannelDataManager so
     * Channels could be managed by ChannelDataManager.
     */
    public static final class Builder {
        private final ChannelImpl mChannel;

        public Builder() {
            mChannel = new ChannelImpl();
            // Fill initial data.
            mChannel.mId = INVALID_ID;
            mChannel.mPackageName = INVALID_PACKAGE_NAME;
            mChannel.mInputId = "inputId";
            mChannel.mType = "type";
            mChannel.mDisplayNumber = "0";
            mChannel.mDisplayName = "name";
            mChannel.mDescription = "description";
            mChannel.mBrowsable = true;
            mChannel.mSearchable = true;
        }

        public Builder(Channel other) {
            mChannel = new ChannelImpl();
            mChannel.copyFrom(other);
        }

        @VisibleForTesting
        public Builder setId(long id) {
            mChannel.mId = id;
            return this;
        }

        @VisibleForTesting
        public Builder setPackageName(String packageName) {
            mChannel.mPackageName = packageName;
            return this;
        }

        public Builder setInputId(String inputId) {
            mChannel.mInputId = inputId;
            return this;
        }

        public Builder setType(String type) {
            mChannel.mType = type;
            return this;
        }

        @VisibleForTesting
        public Builder setDisplayNumber(String displayNumber) {
            mChannel.mDisplayNumber = normalizeDisplayNumber(displayNumber);
            return this;
        }

        @VisibleForTesting
        public Builder setDisplayName(String displayName) {
            mChannel.mDisplayName = displayName;
            return this;
        }

        @VisibleForTesting
        public Builder setDescription(String description) {
            mChannel.mDescription = description;
            return this;
        }

        public Builder setVideoFormat(String videoFormat) {
            mChannel.mVideoFormat = videoFormat;
            return this;
        }

        public Builder setBrowsable(boolean browsable) {
            mChannel.mBrowsable = browsable;
            return this;
        }

        public Builder setSearchable(boolean searchable) {
            mChannel.mSearchable = searchable;
            return this;
        }

        public Builder setLocked(boolean locked) {
            mChannel.mLocked = locked;
            return this;
        }

        public Builder setPassthrough(boolean isPassthrough) {
            mChannel.mIsPassthrough = isPassthrough;
            return this;
        }

        @VisibleForTesting
        public Builder setAppLinkText(String appLinkText) {
            mChannel.mAppLinkText = appLinkText;
            return this;
        }

        @VisibleForTesting
        public Builder setNetworkAffiliation(String networkAffiliation) {
            mChannel.mNetworkAffiliation = networkAffiliation;
            return this;
        }

        public Builder setAppLinkColor(int appLinkColor) {
            mChannel.mAppLinkColor = appLinkColor;
            return this;
        }

        public Builder setAppLinkIconUri(String appLinkIconUri) {
            mChannel.mAppLinkIconUri = appLinkIconUri;
            return this;
        }

        public Builder setAppLinkPosterArtUri(String appLinkPosterArtUri) {
            mChannel.mAppLinkPosterArtUri = appLinkPosterArtUri;
            return this;
        }

        @VisibleForTesting
        public Builder setAppLinkIntentUri(String appLinkIntentUri) {
            mChannel.mAppLinkIntentUri = appLinkIntentUri;
            return this;
        }

        public Builder setRecordingProhibited(boolean recordingProhibited) {
            mChannel.mRecordingProhibited = recordingProhibited;
            return this;
        }

        public ChannelImpl build() {
            ChannelImpl channel = new ChannelImpl();
            channel.copyFrom(mChannel);
            return channel;
        }
    }

    /**
     * Prefetches the images for this channel.
     */
    public void prefetchImage(Context context, int type, int maxWidth, int maxHeight) {
        String uriString = getImageUriString(type);
        if (!TextUtils.isEmpty(uriString)) {
//            ImageLoader.prefetchBitmap(context, uriString, maxWidth, maxHeight);
        }
    }

//    @UiThread
//    public void loadBitmap(
//            Context context,
//            final int type,
//            int maxWidth,
//            int maxHeight,
//            ImageLoader.ImageLoaderCallback callback) {
//        String uriString = getImageUriString(type);
//        ImageLoader.loadBitmap(context, uriString, maxWidth, maxHeight, callback);
//    }

    //    /**
//     * Sets if the channel logo exists. This method should be only called from {@link
//     * ChannelDataManager}.
//     */
    @Override
    public void setChannelLogoExist(boolean exist) {
        mChannelLogoExist = exist;
    }

    /**
     * Returns if channel logo exists.
     */
    public boolean channelLogoExists() {
        return mChannelLogoExist;
    }

    /**
     * Returns the type of app link for this channel. It returns {@link
     * Channel#APP_LINK_TYPE_CHANNEL} if the channel has a non null app link text and a valid app
     * link intent, it returns {@link Channel#APP_LINK_TYPE_APP} if the input service which holds
     * the channel has leanback launch intent, and it returns {@link Channel#APP_LINK_TYPE_NONE}
     * otherwise.
     */
    public int getAppLinkType(Context context) {
        if (mAppLinkType == APP_LINK_TYPE_NOT_SET) {
            initAppLinkTypeAndIntent(context);
        }
        return mAppLinkType;
    }

    /**
     * Returns the app link intent for this channel. If the type of app link is {@link
     * Channel#APP_LINK_TYPE_NONE}, it returns {@code null}.
     */
    public Intent getAppLinkIntent(Context context) {
        if (mAppLinkType == APP_LINK_TYPE_NOT_SET) {
            initAppLinkTypeAndIntent(context);
        }
        return mAppLinkIntent;
    }

    private void initAppLinkTypeAndIntent(Context context) {
        mAppLinkType = APP_LINK_TYPE_NONE;
        mAppLinkIntent = null;
        PackageManager pm = context.getPackageManager();
        if (!TextUtils.isEmpty(mAppLinkText) && !TextUtils.isEmpty(mAppLinkIntentUri)) {
            try {
                Intent intent = Intent.parseUri(mAppLinkIntentUri, Intent.URI_INTENT_SCHEME);
                if (intent.resolveActivityInfo(pm, 0) != null) {
                    mAppLinkIntent = intent;
                    mAppLinkIntent.putExtra(
                            CommonConstants.EXTRA_APP_LINK_CHANNEL_URI, getUri().toString());
                    mAppLinkType = APP_LINK_TYPE_CHANNEL;
                    return;
                } else {
                    if (Log.INCLUDE) {
                        Log.w(TAG, "No activity exists to handle : " + mAppLinkIntentUri);
                    }
                }
            } catch (URISyntaxException e) {
                if (Log.INCLUDE) {
                    Log.w(TAG, "Unable to set app link for " + mAppLinkIntentUri);
                }
                // Do nothing.
            }
        }
        if (mPackageName.equals(context.getApplicationContext().getPackageName())) {
            return;
        }
        mAppLinkIntent = pm.getLeanbackLaunchIntentForPackage(mPackageName);
        if (mAppLinkIntent != null) {
            mAppLinkIntent.putExtra(
                    CommonConstants.EXTRA_APP_LINK_CHANNEL_URI, getUri().toString());
            mAppLinkType = APP_LINK_TYPE_APP;
        }
    }

    private String getImageUriString(int type) {
        switch (type) {
            case LOAD_IMAGE_TYPE_CHANNEL_LOGO:
                return TvContract.buildChannelLogoUri(mId).toString();
            case LOAD_IMAGE_TYPE_APP_LINK_ICON:
                return mAppLinkIconUri;
            case LOAD_IMAGE_TYPE_APP_LINK_POSTER_ART:
                return mAppLinkPosterArtUri;
        }
        return null;
    }

    public static class DefaultComparator implements Comparator<Channel> {
        private final Context mContext;
        private final TvInputManagerHelper mInputManager;
        private final Map<String, String> mInputIdToLabelMap = new HashMap<>();
        private boolean mDetectDuplicatesEnabled;

        public DefaultComparator(Context context, TvInputManagerHelper inputManager) {
            mContext = context;
            mInputManager = inputManager;
        }

        public void setDetectDuplicatesEnabled(boolean detectDuplicatesEnabled) {
            mDetectDuplicatesEnabled = detectDuplicatesEnabled;
        }

        @SuppressWarnings("ReferenceEquality")
        @Override
        public int compare(Channel lhs, Channel rhs) {
            if (lhs == rhs) {
                return 0;
            }
            // Put channels from OEM/SOC inputs first.
            boolean lhsIsPartner = mInputManager.isPartnerInput(lhs.getInputId());
            boolean rhsIsPartner = mInputManager.isPartnerInput(rhs.getInputId());
            if (lhsIsPartner != rhsIsPartner) {
                return lhsIsPartner ? -1 : 1;
            }
            // Compare the input labels.
            String lhsLabel = getInputLabelForChannel(lhs);
            String rhsLabel = getInputLabelForChannel(rhs);
            int result =
                    lhsLabel == null
                            ? (rhsLabel == null ? 0 : 1)
                            : rhsLabel == null ? -1 : lhsLabel.compareTo(rhsLabel);
            if (result != 0) {
                return result;
            }
            // Compare the input IDs. The input IDs cannot be null.
            result = lhs.getInputId().compareTo(rhs.getInputId());
            if (result != 0) {
                return result;
            }
            // Compare the channel numbers if both channels belong to the same input.
            result = ChannelNumber.compare(lhs.getDisplayNumber(), rhs.getDisplayNumber());
            if (mDetectDuplicatesEnabled && result == 0) {
                if (Log.INCLUDE) {
                    Log.w(
                            TAG,
                            "Duplicate channels detected! - \""
                                    + lhs.getDisplayText()
                                    + "\" and \""
                                    + rhs.getDisplayText()
                                    + "\"");
                }
            }
            return result;
        }

        @VisibleForTesting
        String getInputLabelForChannel(Channel channel) {
            String label = mInputIdToLabelMap.get(channel.getInputId());
            if (label == null) {
                TvInputInfo info = mInputManager.getTvInputInfo(channel.getInputId());
                if (info != null) {
//                    label = Utils.loadLabel(mContext, info);
                    if (label != null) {
                        mInputIdToLabelMap.put(channel.getInputId(), label);
                    }
                }
            }
            return label;
        }
    }


}
