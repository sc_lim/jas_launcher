/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

public class ChannelRing extends ArrayList<Channel> {
    class ChannelComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            Channel c1 = (Channel) o1;
            Channel c2 = (Channel) o2;
            int num1 = parseInt(c1.getDisplayNumber());
            int num2 = parseInt(c2.getDisplayNumber());
            return (num1 > num2) ? 1 : -1;
        }
    }
    private ChannelComparator comparator = new ChannelComparator();

    public ChannelRing(Collection<? extends Channel> collection) {
        super(collection);
    }

    public boolean add(Channel channel) {
        Channel ch;
        int index = 0;
        for (index = 0 ; index < size() ; index++) {
            ch = get(index);
            if (comparator.compare(ch, channel) >= 0) {
                super.add(index, channel);
                return true;
            }
        }
        super.add(index, channel);
        return true;
    }

    public Channel getFirst() {
        if (size() > 0) {
            return get(0);
        } else {
            return null;
        }
    }

    public Channel getLast() {
        if (size() > 0) {
            return get(size() - 1);
        } else {
            return null;
        }
    }

    public Channel getChannel(long channelId) {
        for (int i = 0 ; i < size() ; i++) {
            if (get(i).getId() == channelId) {
                return get(i);
            }
        }
        return null;
    }

    public Channel getChannel(String serviceId) {
        for (int i = 0 ; i < size() ; i++) {
            if (get(i).getServiceId().equals(serviceId)) {
                return get(i);
            }
        }
        return null;
    }

    public Channel getPrevious(Channel channel) {
        int index = indexOf(channel);
        index--;
        if (index < 0) {
            index = size() - 1;
        }
        return get(index);
    }

    public Channel getNext(Channel channel) {
        int index = indexOf(channel);
        index++;
        if (index >= size()) {
            index = 0;
        }
        return get(index);
    }

    public boolean belongsTo(Channel channel) {
        return contains(channel);
    }

    private static int parseInt(String number) {
        int num = -1;
        try {
            num = Integer.parseInt(number);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return num;
    }

    public Channel findAdjacent(String inputNumber) {
        return findAdjacent(parseInt(inputNumber));
    }

    public Channel findAdjacent(int inputNumber) {
        int prev = -1, now;
        for (int i = 0 ; i < size() ; i++) {
            now = parseInt(get(i).getDisplayNumber());
            if (now == inputNumber) {
                return get(i);
            }
            if (i != 0 && now > inputNumber) {
                if (now - inputNumber > inputNumber - prev) {
                    return get(i - 1);
                } else {
                    return get(i);
                }
            }
            prev = now;
        }
        return getLast();
    }

    public String toString() {
        String str = "";
        str += "channels.size="+size();
        for (int i = 0 ; i < size() ; i++) {
            str += "\n channel["+i+"]:"+chToString(get(i));
        }
        return str;
    }

    private static String chToString(Channel ch) {
        if (ch == null) {
            return null;
        } else {
            String str = "[" + ch.getDisplayNumber() + "] " + ch.getDisplayName()
                    + ", id:" + ch.getId()
//                    + ", locked:" + ch.isLocked()
//                    + ", fav:" + ch.isFavoriteChannel()
//                    + ", paid:" + ch.isPaidChannel()
//                    + ", audio:" + ch.isAudioChannel()
//                    + ", mosaic:" + ch.isMosaicChannel()
//                    + ", type:" + ch.getType()
//                    + ", desc:\"" + ch.getDescription() + "\""
//                    + ", browsable:" + ch.isBrowsable()
//                    + ", recordingProhibited:" + ch.isRecordingProhibited()
//                    + ", uri:" + ch.getUri()
                    ;
            return str;
        }
    }
}
