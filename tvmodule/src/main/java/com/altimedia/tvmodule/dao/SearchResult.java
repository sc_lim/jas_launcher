/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.dao;


public class SearchResult {
    private long channelId;
    private String channelNumber;
    private String title;
    private String description;
    private String imageUri;
    private String intentAction;
    private String intentData;
    private String intentExtraData;
    private String contentType;
    private boolean isLive;
    private int videoWidth;
    private int videoHeight;
    private long duration;
    private int progressPercentage;

    public long getChannelId() {
        return channelId;
    }

    public String getChannelNumber() {
        return channelNumber;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUri() {
        return imageUri;
    }

    public String getIntentAction() {
        return intentAction;
    }

    public String getIntentData() {
        return intentData;
    }

    public String getContentType() {
        return contentType;
    }

    public boolean isLive() {
        return isLive;
    }

    public int getVideoWidth() {
        return videoWidth;
    }

    public int getVideoHeight() {
        return videoHeight;
    }

    public long getDuration() {
        return duration;
    }

    public int getProgressPercentage() {
        return progressPercentage;
    }


    private SearchResult(Builder builder) {
        channelId = builder.channelId;
        channelNumber = builder.channelNumber;
        title = builder.title;
        description = builder.description;
        imageUri = builder.imageUri;
        intentAction = builder.intentAction;
        intentData = builder.intentData;
        intentExtraData = builder.intentExtraData;
        contentType = builder.contentType;
        isLive = builder.isLive;
        videoWidth = builder.videoWidth;
        videoHeight = builder.videoHeight;
        duration = builder.duration;
        progressPercentage = builder.progressPercentage;
    }


    public static Builder builder() {
        return new Builder()
                .setChannelId(0)
                .setIsLive(false)
                .setVideoWidth(0)
                .setVideoHeight(0)
                .setDuration(0)
                .setProgressPercentage(0);

    }

    public static class Builder {

        private long channelId;
        private String channelNumber;
        private String title;
        private String description;
        private String imageUri;
        private String intentAction;
        private String intentData;
        private String intentExtraData;
        private String contentType;
        private boolean isLive;
        private int videoWidth;
        private int videoHeight;
        private long duration;
        private int progressPercentage;


        public Builder setChannelId(long value) {
            this.channelId = value;
            return this;
        }

        public Builder setChannelNumber(String value) {
            this.channelNumber = value;
            return this;
        }

        public Builder setTitle(String value) {
            this.title = value;
            return this;
        }

        public Builder setDescription(String value) {
            this.description = value;
            return this;
        }

        public Builder setImageUri(String value) {
            this.imageUri = value;
            return this;
        }

        public Builder setIntentAction(String value) {
            this.intentAction = value;
            return this;
        }

        public Builder setIntentData(String value) {
            this.intentData = value;
            return this;
        }

        public Builder setIntentExtraData(String value) {
            this.intentExtraData = value;
            return this;
        }

        public Builder setContentType(String value) {
            this.contentType = value;
            return this;
        }

        public Builder setIsLive(boolean value) {
            this.isLive = value;
            return this;
        }

        public Builder setVideoWidth(int value) {
            this.videoWidth = value;
            return this;
        }

        public Builder setVideoHeight(int value) {
            this.videoHeight = value;
            return this;
        }

        public Builder setDuration(long value) {
            this.duration = value;
            return this;
        }

        public Builder setProgressPercentage(int value) {
            this.progressPercentage = value;
            return this;
        }

        public SearchResult build() {
            return new SearchResult(this);
        }
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "channelId=" + channelId +
                ", channelNumber='" + channelNumber + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", imageUri='" + imageUri + '\'' +
                ", intentAction='" + intentAction + '\'' +
                ", intentData='" + intentData + '\'' +
                ", intentExtraData='" + intentExtraData + '\'' +
                ", contentType='" + contentType + '\'' +
                ", isLive=" + isLive +
                ", videoWidth=" + videoWidth +
                ", videoHeight=" + videoHeight +
                ", duration=" + duration +
                ", progressPercentage=" + progressPercentage +
                '}';
    }
}
