/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.dao;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.altimedia.tvmodule.R;

public abstract class BaseProgramImpl implements BaseProgram {

    @Override
    @Nullable
    public String getEpisodeDisplayTitle(Context context) {
        String episodeNumber = getEpisodeNumber();
        String episodeTitle = getEpisodeTitle();
        if (!TextUtils.isEmpty(episodeNumber)) {
            episodeTitle = episodeTitle == null ? "" : episodeTitle;
            String seasonNumber = getSeasonNumber();
            if (TextUtils.isEmpty(seasonNumber) || TextUtils.equals(seasonNumber, "0")) {
                // Do not show "S0: ".
                return context.getResources()
                        .getString(
                                R.string.display_episode_title_format_no_season_number,
                                episodeNumber,
                                episodeTitle);
            } else {
                return context.getResources()
                        .getString(
                                R.string.display_episode_title_format,
                                seasonNumber,
                                episodeNumber,
                                episodeTitle);
            }
        }
        return episodeTitle;
    }

    @Override
    public String getEpisodeContentDescription(Context context) {
        String episodeNumber = getEpisodeNumber();
        String episodeTitle = getEpisodeTitle();
        if (!TextUtils.isEmpty(episodeNumber)) {
            episodeTitle = episodeTitle == null ? "" : episodeTitle;
            String seasonNumber = getSeasonNumber();
            if (TextUtils.isEmpty(seasonNumber) || TextUtils.equals(seasonNumber, "0")) {
                // Do not list season if it is empty or 0
                return context.getResources()
                        .getString(
                                R.string.content_description_episode_format_no_season_number,
                                episodeNumber,
                                episodeTitle);
            } else {
                return context.getResources()
                        .getString(
                                R.string.content_description_episode_format,
                                seasonNumber,
                                episodeNumber,
                                episodeTitle);
            }
        }
        return episodeTitle;
    }

    @Override
    public boolean isEpisodic() {
        return !TextUtils.isEmpty(getSeriesId());
    }
}
