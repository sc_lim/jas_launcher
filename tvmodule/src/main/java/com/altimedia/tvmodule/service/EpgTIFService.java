/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.service;

import android.content.ComponentName;
import android.content.Context;
import android.media.tv.TvContentRating;
import android.media.tv.TvInputManager;
import android.media.tv.TvInputService;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.ViewGroup;
import android.view.accessibility.CaptioningManager;

import androidx.annotation.Nullable;

import com.google.android.media.tv.companionlibrary.BaseTvInputService;
import com.google.android.media.tv.companionlibrary.TvPlayer;
import com.google.android.media.tv.companionlibrary.model.Program;
import com.google.android.media.tv.companionlibrary.model.RecordedProgram;

public class EpgTIFService extends BaseTvInputService {

    private static final long EPG_SYNC_DELAYED_PERIOD_MS = 1000 * 2; // 2 Seconds
    private final String TAG = EpgTIFService.class.getSimpleName();
    public static final String INPUT_ID = EpgTIFService.class.getPackage().getName() + "/." +
            EpgTIFService.class.getSimpleName();
    private CaptioningManager mCaptioningManager;

    @Nullable
    @Override
    public TvInputService.Session onCreateSession(String inputId) {

        TaurusTvInputSessionImpl session = new TaurusTvInputSessionImpl(this, inputId);
        session.setOverlayViewEnabled(true);
        return session;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mCaptioningManager = (CaptioningManager) getSystemService(Context.CAPTIONING_SERVICE);

    }

    @Override
    public Session sessionCreated(Session session) {

        return super.sessionCreated(session);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public TvInputService.RecordingSession onCreateRecordingSession(String inputId) {
        return super.onCreateRecordingSession(inputId);
    }


    class TaurusTvInputSessionImpl extends BaseTvInputService.Session {
        private static final float CAPTION_LINE_HEIGHT_RATIO = 0.0533f;
        private static final int TEXT_UNIT_PIXELS = 0;
        private static final String UNKNOWN_LANGUAGE = "und";

        private int mSelectedSubtitleTrackIndex;
        private String mInputId;
        private Context mContext;

        TaurusTvInputSessionImpl(Context context, String inputId) {
            super(context, inputId);
            mContext = context;
            mInputId = inputId;
        }

        @Override
        public boolean onPlayProgram(Program program, long startPosMs) {
            if (program == null) {
                requestEpgSync(getCurrentChannelUri());
                notifyVideoUnavailable(TvInputManager.VIDEO_UNAVAILABLE_REASON_TUNING);
                return false;
            }
            createPlayer(program.getInternalProviderData().getVideoType(),
                    Uri.parse(program.getInternalProviderData().getVideoUrl()));
            if (startPosMs > 0) {
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                notifyTimeShiftStatusChanged(TvInputManager.TIME_SHIFT_STATUS_AVAILABLE);
            }
            return true;
        }


        @Override
        public TvPlayer getTvPlayer() {
            return null;
        }

        @Override
        public boolean onPlayRecordedProgram(RecordedProgram recordedProgram) {
            return false;
        }

        @Override
        public void onSetCaptionEnabled(boolean b) {

        }


        @Override
        public boolean onTune(Uri channelUri) {
            notifyVideoUnavailable(TvInputManager.VIDEO_UNAVAILABLE_REASON_TUNING);
            releasePlayer();
            return super.onTune(channelUri);
        }

        private ViewGroup getViewGroup(Context mContext) {
            return null;
        }

        private void createPlayer(int videoType, Uri videoUrl) {
            releasePlayer();
        }


        private void releasePlayer() {
        }

        @Override
        public void onRelease() {
            super.onRelease();
            releasePlayer();
        }

        @Override
        public void onBlockContent(TvContentRating rating) {
            super.onBlockContent(rating);
            releasePlayer();
        }


        public void requestEpgSync(final Uri channelUri) {
            EpgSyncJobService.requestImmediateSync(EpgTIFService.this, mInputId,
                    new ComponentName(EpgTIFService.this, EpgSyncJobService.class));
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    onTune(channelUri);
                }
            }, EPG_SYNC_DELAYED_PERIOD_MS);
        }

    }


}