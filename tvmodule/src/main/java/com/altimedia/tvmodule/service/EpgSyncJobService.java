/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.service;

import android.app.job.JobParameters;
import android.content.Intent;
import android.net.Uri;

import com.altimedia.util.Log;
import com.google.android.media.tv.companionlibrary.model.Channel;
import com.google.android.media.tv.companionlibrary.model.InternalProviderData;
import com.google.android.media.tv.companionlibrary.model.Program;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;


/**
 * Created by mc.kim on 2017-08-30.
 */

public class EpgSyncJobService extends com.google.android.media.tv.companionlibrary.EpgSyncJobService {
    public static final String ACTION_SYNC_STATE_CHANGE = "alticast.intent.action.ACTION_SYNC_STATE_CHANGE";
    public static final String KEY_STATE = "state";
    public static final int STATE_START = 1;
    public static final int STATE_END = 2;
    private final String TAG = EpgSyncJobService.class.getSimpleName();
    private final long HOUR = 60 * 60 * 1000;
    private final long DAY = 24 * HOUR;
    private final long FULL_TIME = 1 * DAY;

    private int[] DUMMY_CHANNEL_NUM = {1, 2, 4, 7, 8, 9, 10, 12, 15, 18, 21, 27, 29, 41, 46, 48, 58, 59, 100};

    private ArrayList<String> mList = new ArrayList();

    @Override
    public List<Channel> getChannels() {

        List<Channel> channelList = new LinkedList<>();
        for (int chNum : DUMMY_CHANNEL_NUM) {
            Random random = new Random();
            int index = random.nextInt(mList.size());
            Channel.Builder builder = new Channel.Builder();
            InternalProviderData providerData = new InternalProviderData();
            providerData.setVideoUrl(mList.get(index));
            builder.setInternalProviderData(providerData);
            builder.setServiceId(chNum);
            builder.setInputId(EpgTIFService.INPUT_ID);
            builder.setDisplayName(chNum + "번 채널");
            builder.setDisplayNumber(String.valueOf(chNum));
            builder.setOriginalNetworkId(chNum);
            channelList.add(builder.build());
        }

        return channelList;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Log.INCLUDE) {
            Log.d(TAG, "call onCreate");
        }

        mList.add(("https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8"));
        mList.add(("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"));
        mList.add(("https://mnmedias.api.telequebec.tv/m3u8/29880.m3u8"));
        mList.add(("http://184.72.239.149/vod/smil:BigBuckBunny.smil/playlist.m3u8"));
        mList.add(("http://www.streambox.fr/playlists/test_001/stream.m3u8"));
    }

    private Intent getIntent(int state) {
        Intent intent = new Intent(ACTION_SYNC_STATE_CHANGE);
        intent.putExtra(KEY_STATE, state);
        return intent;
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onStartJob");
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(getIntent(STATE_START));
        return super.onStartJob(params);
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onStopJob");
        }
        return super.onStopJob(params);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onStartCommand");
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onDestroy");
        }
        super.onDestroy();
    }

    private final long ONE_HOUR = 60 * 60 * 1000;
    private final long ONE_MIN = 10 * 60 * 1000;
    private final int PROGRAM_DUMMY_SIZE = 200;

    private long getRandomProgramTime() {
        int radom = (int) (Math.random() * 10) + 1;
        return ONE_MIN * radom;
    }


    @Override
    public List<Program> getProgramsForChannel(Uri channelUri, Channel channel, long startMs, long endMs) {
        List<Program> programList = new LinkedList<>();
        long previousEndTime = (System.currentTimeMillis() - (1 * ONE_HOUR));
        int index = 0;
        for (index = 0; index < PROGRAM_DUMMY_SIZE; index++) {
            Program.Builder builder = new Program.Builder();
            builder.setChannelId(channel.getServiceId());
            builder.setVideoHeight(1920);
            builder.setVideoWidth(1080);
            builder.setDescription("");
            builder.setLongDescription("");
            builder.setPosterArtUri("");
            builder.setEpisodeTitle("");

            long startTime = previousEndTime;
            long endTime = startTime + getRandomProgramTime();


            previousEndTime = endTime;

            builder.setStartTimeUtcMillis(startTime);
            builder.setEndTimeUtcMillis(endTime);


            builder.setTitle(channel.getDisplayNumber() + "의 " + index + "번째 프로그램");

            Program program = builder.build();

            programList.add(program);
        }
        return programList;
    }

}
