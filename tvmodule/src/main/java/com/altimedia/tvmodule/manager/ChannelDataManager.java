/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */
package com.altimedia.tvmodule.manager;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.database.ContentObserver;
import android.media.tv.TvContract;
import android.media.tv.TvInputManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.ArraySet;
import android.util.MutableInt;

import com.altimedia.tvmodule.common.JasGenre;
import com.altimedia.tvmodule.common.WeakHandler;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.ChannelImpl;
import com.altimedia.tvmodule.dao.ChannelRing;
import com.altimedia.tvmodule.feature.TvFeatures;
import com.altimedia.tvmodule.util.AsyncDbTask;
import com.altimedia.tvmodule.util.PermissionUtils;
import com.altimedia.tvmodule.util.SharedPreferencesUtils;
import com.altimedia.tvmodule.util.SoftPreconditions;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.tvmodule.util.Utils;
import com.altimedia.util.Log;
import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executor;

import androidx.annotation.AnyThread;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

@AutoFactory
@AnyThread
public class ChannelDataManager {
    private static final String TAG = "ChannelDataManager";
    private static final int MSG_UPDATE_CHANNELS = 1000;
    private final boolean TEST = false;
    private final Context mContext;
    private final TvInputManagerHelper mInputManager;
    private final Executor mDbExecutor;
    private boolean mStarted;
    private boolean mDbLoadFinished;
    private QueryAllChannelsTask mChannelsUpdateTask;
    private final List<Runnable> mPostRunnablesAfterChannelUpdate = new ArrayList<>();
    private final Set<Listener> mListeners = new CopyOnWriteArraySet<>();

    // Use container class to support multi-thread safety. This value can be set only on the main
    // thread.
    private volatile UnmodifiableChannelData mData = new UnmodifiableChannelData();
    private final ChannelRingStorage mChannelRingStorage = new ChannelRingStorage(this);
    private final ChannelImpl.DefaultComparator mChannelComparator;
    private final Handler mHandler;
    private final Set<Long> mBrowsableUpdateChannelIds = new HashSet<>();
    private final Set<Long> mLockedUpdateChannelIds = new HashSet<>();
    private final ContentResolver mContentResolver;
    private final ContentObserver mChannelObserver;
    private final boolean mStoreBrowsableInSharedPreferences;
    private final SharedPreferences mBrowsableSharedPreferences;
    private final TvInputManager.TvInputCallback mTvInputCallback =
            new TvInputManager.TvInputCallback() {
                @Override
                public void onInputAdded(String inputId) {
                    boolean channelAdded = false;
                    ChannelData data = new ChannelData(mData);
                    for (ChannelWrapper channel : mData.channelWrapperMap.values()) {
                        if (channel.mChannel.getInputId().equals(inputId)) {
                            channel.mInputRemoved = false;
                            addChannel(data, channel.mChannel);
                            channelAdded = true;
                        }
                    }
                    if (channelAdded) {
                        Collections.sort(data.channels, mChannelComparator);
                        mData = new UnmodifiableChannelData(data);
                        notifyChannelListUpdated();
                    }
                }

                @Override
                public void onInputRemoved(String inputId) {
                    boolean channelRemoved = false;
                    ArrayList<ChannelWrapper> removedChannels = new ArrayList<>();
                    for (ChannelWrapper channel : mData.channelWrapperMap.values()) {
                        if (channel.mChannel.getInputId().equals(inputId)) {
                            channel.mInputRemoved = true;
                            channelRemoved = true;
                            removedChannels.add(channel);
                        }
                    }
                    if (channelRemoved) {
                        ChannelData data = new ChannelData();
                        data.channelWrapperMap.putAll(mData.channelWrapperMap);
                        for (ChannelWrapper channelWrapper : data.channelWrapperMap.values()) {
                            if (!channelWrapper.mInputRemoved) {
                                addChannel(data, channelWrapper.mChannel);
                            }
                        }
                        Collections.sort(data.channels, mChannelComparator);
                        mData = new UnmodifiableChannelData(data);
                        notifyChannelListUpdated();
                        for (ChannelWrapper channel : removedChannels) {
                            channel.notifyChannelRemoved();
                        }
                    }
                }
            };

    private final ChannelController mChannelController;

    @MainThread
    public ChannelDataManager(@Provided Context context,
                              @Provided TvInputManagerHelper inputManager,
                              @Provided @AsyncDbTask.DbExecutor Executor executor,
                              @Provided ContentResolver contentResolver) {
        mContext = context;
        mInputManager = inputManager;
        mDbExecutor = executor;
        mContentResolver = contentResolver;
        mChannelComparator = new ChannelImpl.DefaultComparator(context, inputManager);
        // Detect duplicate channels while sorting.
        mChannelComparator.setDetectDuplicatesEnabled(true);
        mHandler = new ChannelDataManagerHandler(this);
        mChannelObserver =
                new ContentObserver(mHandler) {
                    @Override
                    public void onChange(boolean selfChange) {
                        boolean hasUpdateMessage = mHandler.hasMessages(MSG_UPDATE_CHANNELS);

                        if (Log.INCLUDE) {
                            Log.d(TAG, "onChange, selfChange=" + selfChange
                                    + ", hasUpdateMessage=" + hasUpdateMessage);
                        }

                        if (!hasUpdateMessage) {
                            mHandler.sendEmptyMessageDelayed(MSG_UPDATE_CHANNELS, 3 * 1000);
                        }
                    }
                };
        mStoreBrowsableInSharedPreferences = !PermissionUtils.hasAccessAllEpg(mContext);
        mBrowsableSharedPreferences =
                context.getSharedPreferences(
                        SharedPreferencesUtils.SHARED_PREF_BROWSABLE, Context.MODE_PRIVATE);

        mChannelController = new ChannelController();
    }

    @VisibleForTesting
    ContentObserver getContentObserver() {
        return mChannelObserver;
    }

    /**
     * Starts the manager. If data is ready, {@link Listener#onLoadFinished()} will be called.
     */
    @MainThread
    public void start() {
        if (mStarted) {
            return;
        }
        mStarted = true;
        // Should be called directly instead of posting MSG_UPDATE_CHANNELS message to the handler.
        // If not, other DB tasks can be executed before channel loading.
        handleUpdateChannels();
        mContentResolver.registerContentObserver(
                TvContract.Channels.CONTENT_URI, true, mChannelObserver);
        mInputManager.addCallback(mTvInputCallback);
    }

    /**
     * Stops the manager. It clears manager states and runs pending DB operations. Added listeners
     * aren't automatically removed by this method.
     */
    @MainThread
    @VisibleForTesting
    public void stop() {
        if (!mStarted) {
            return;
        }
        mStarted = false;
        mDbLoadFinished = false;
        mInputManager.removeCallback(mTvInputCallback);
        mContentResolver.unregisterContentObserver(mChannelObserver);
        mHandler.removeCallbacksAndMessages(null);
        clearChannels();
        mPostRunnablesAfterChannelUpdate.clear();
        if (mChannelsUpdateTask != null) {
            mChannelsUpdateTask.cancel(true);
            mChannelsUpdateTask = null;
        }
        applyUpdatedValuesToDb();
    }

    /**
     * Adds a {@link Listener}.
     */
    public void addListener(Listener listener) {
        if (Log.INCLUDE) Log.d(TAG, "addListener " + listener);
        SoftPreconditions.checkNotNull(listener);
        if (listener != null) {
            mListeners.add(listener);
        }
    }

    public boolean listenerContains(Listener listener) {
        if (mListeners != null && listener != null) {
            return mListeners.contains(listener);
        }
        return false;
    }

    /**
     * Removes a {@link Listener}.
     */
    public void removeListener(Listener listener) {
        if (Log.INCLUDE) Log.d(TAG, "removeListener " + listener);
        SoftPreconditions.checkNotNull(listener);
        if (listener != null) {
            mListeners.remove(listener);
        }
    }

    /**
     * Adds a {@link ChannelListener} for a specific channel with the channel ID {@code channelId}.
     */
    public void addChannelListener(Long channelId, ChannelListener listener) {
        ChannelWrapper channelWrapper = mData.channelWrapperMap.get(channelId);
        if (channelWrapper == null) {
            return;
        }
        channelWrapper.addListener(listener);
    }

    /**
     * Removes a {@link ChannelListener} for a specific channel with the channel ID {@code
     * channelId}.
     */
    public void removeChannelListener(Long channelId, ChannelListener listener) {
        ChannelWrapper channelWrapper = mData.channelWrapperMap.get(channelId);
        if (channelWrapper == null) {
            return;
        }
        channelWrapper.removeListener(listener);
    }

    /**
     * Checks whether data is ready.
     */
    public boolean isDbLoadFinished() {
        return mDbLoadFinished;
    }

    /**
     * Returns the number of channels.
     */
    public int getChannelCount() {
        return mData.channels.size();
    }

    /**
     * Returns a list of channels.
     */
    public List<Channel> getChannelList() {
        return new ArrayList<>(mData.channels);
    }

    public List<Channel> getBlockChannelList() {
        ArrayList<Channel> blockChannelList = new ArrayList<>();
        for (Channel ch : mData.channels) {
            if (ch.isLocked()) {
                blockChannelList.add(ch);
            }
        }
        return blockChannelList;
    }

    public void initEpgChannelRing(Channel initChannel) {
        if (Log.INCLUDE) Log.d(TAG, "initEpgChannelRing() ch:" + (initChannel != null ? initChannel.getDisplayNumber() : null));
        ChannelRing epgRing = mChannelRingStorage.getChannelRing(ChannelRingStorage.RING_MINIEPG);
        epgRing.clear();
        epgRing.addAll(mChannelRingStorage.getChannelRing(ChannelRingStorage.RING_BROWSABLE));
        if (!epgRing.contains(initChannel)) {
            epgRing.add(initChannel);
        }
    }

    public boolean containsEpgChannelRing(Channel channel) {
        return mChannelRingStorage.getChannelRing(ChannelRingStorage.RING_MINIEPG).contains(channel);
    }

    public void addChannelToEpgChannelRing(Channel channel) {
        if (Log.INCLUDE) Log.d(TAG, "addChannelToEpgChannelRing() ch:" + (channel != null ? channel.getDisplayNumber() : null));
        if (!mChannelRingStorage.getChannelRing(ChannelRingStorage.RING_MINIEPG).contains(channel)) {
            mChannelRingStorage.getChannelRing(ChannelRingStorage.RING_MINIEPG).add(channel);
            if (Log.INCLUDE) Log.d(TAG, "addChannelToEpgChannelRing() channel added.");
        }
    }

    public void removeChannelFromEpgChannelRing(Channel channel) {
        if (Log.INCLUDE) Log.d(TAG, "removeChannelFromEpgChannelRing() ch:" + (channel != null ? channel.getDisplayNumber() : null));
        if (mChannelRingStorage.getChannelRing(ChannelRingStorage.RING_HIDDEN).contains(channel)) {
            mChannelRingStorage.getChannelRing(ChannelRingStorage.RING_MINIEPG).remove(channel);
            if (Log.INCLUDE) Log.d(TAG, "removeChannelFromEpgChannelRing() channel removed.");
        }
    }

    public Channel getNextEpgChannel(Channel channel, boolean up) {
        ChannelRing browsableRing = mChannelRingStorage.getChannelRing(ChannelRingStorage.RING_MINIEPG);
        Channel nextChannel = up ? browsableRing.getNext(channel) : browsableRing.getPrevious(channel);
        return nextChannel;
    }

    public Channel getNextChannel(Channel channel, boolean up) {
        ChannelRing browsableRing = mChannelRingStorage.getChannelRing(ChannelRingStorage.RING_BROWSABLE);
        Channel nextChannel = up ? browsableRing.getNext(channel) : browsableRing.getPrevious(channel);
        return nextChannel;
    }

    /**
     * Returns a list of browsable channels.
     */
    public List<Channel> getBrowsableChannelList() {
        List<Channel> channels = new ArrayList<>();
        for (Channel channel : mData.channels) {
            if (channel.isBrowsable()) {
                channels.add(channel);
            }
        }
        return channels;
    }

    /**
     * Returns the total channel count for a given input.
     *
     * @param inputId The ID of the input.
     */
    public int getChannelCountForInput(String inputId) {
        MutableInt count = mData.channelCountMap.get(inputId);
        return count == null ? 0 : count.value;
    }

    /**
     * Checks if the channel exists in DB.
     *
     * <p>Note that the channels of the removed inputs can not be obtained from {@link #getChannel}.
     * In that case this method is used to check if the channel exists in the DB.
     */
    public boolean doesChannelExistInDb(long channelId) {
        return mData.channelWrapperMap.get(channelId) != null;
    }

    /**
     * Returns true if and only if there exists at least one channel and all channels are hidden.
     */
    public boolean areAllChannelsHidden() {
        for (Channel channel : mData.channels) {
            if (channel.isBrowsable()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gets the channel with the channel ID {@code channelId}.
     */
    public Channel getChannel(Long channelId) {
        ChannelWrapper channelWrapper = mData.channelWrapperMap.get(channelId);
        if (channelWrapper == null || channelWrapper.mInputRemoved) {
            return null;
        }
        return channelWrapper.mChannel;
    }

    public Channel getChannelByServiceId(String serviceId) {
        if (serviceId == null) {
            return null;
        }
        for (Channel ch : mData.channels) {
            if (ch.getServiceId().equals(serviceId)) {
                return ch;
            }
        }
        return null;
    }

    public Channel getChannelByDisplayNumber(String displayNumber) {
        for (Channel ch : mData.channels) {
            if (ch.getDisplayNumber().equals(displayNumber)) {
                return ch;
            }
        }
        return null;
    }

    /**
     * The value change will be applied to DB when applyPendingDbOperation is called.
     */
    public void updateBrowsable(Long channelId, boolean browsable) {
        updateBrowsable(channelId, browsable, false);
    }

    /**
     * The value change will be applied to DB when applyPendingDbOperation is called.
     *
     * @param skipNotifyChannelBrowsableChanged If it's true, {@link Listener
     *                                          #onChannelBrowsableChanged()} is not called, when this method is called. {@link
     *                                          #notifyChannelBrowsableChanged} should be directly called, once browsable update is
     *                                          completed.
     */
    public void updateBrowsable(
            Long channelId, boolean browsable, boolean skipNotifyChannelBrowsableChanged) {
        ChannelWrapper channelWrapper = mData.channelWrapperMap.get(channelId);
        if (channelWrapper == null) {
            return;
        }
        if (channelWrapper.mChannel.isBrowsable() != browsable) {
            channelWrapper.mChannel.setBrowsable(browsable);
            if (browsable == channelWrapper.mBrowsableInDb) {
                mBrowsableUpdateChannelIds.remove(channelWrapper.mChannel.getId());
            } else {
                mBrowsableUpdateChannelIds.add(channelWrapper.mChannel.getId());
            }
            channelWrapper.notifyChannelUpdated();
            // When updateBrowsable is called multiple times in a method, we don't need to
            // notify Listener.onChannelBrowsableChanged multiple times but only once. So
            // we send a message instead of directly calling onChannelBrowsableChanged.
            if (!skipNotifyChannelBrowsableChanged) {
                notifyChannelBrowsableChanged();
            }
        }
    }

    public void notifyChannelBrowsableChanged() {
        for (Listener l : mListeners) {
            l.onChannelBrowsableChanged();
        }
    }

    private void notifyChannelListUpdated() {
        for (Listener l : mListeners) {
            l.onChannelListUpdated();
        }
    }

    public void notifyUserBasedChannelListUpdated() {
        for (Listener l : mListeners) {
            l.onUserBasedChannelListUpdated();
        }
    }

    private void notifyLoadFinished() {
        for (Listener l : mListeners) {
            l.onLoadFinished();
        }
    }

    /**
     * Updates channels from DB. Once the update is done, {@code postRunnable} will be called.
     */
    public void updateChannels(Runnable postRunnable) {
        if (mChannelsUpdateTask != null) {
            mChannelsUpdateTask.cancel(true);
            mChannelsUpdateTask = null;
        }
        mPostRunnablesAfterChannelUpdate.add(postRunnable);
        if (!mHandler.hasMessages(MSG_UPDATE_CHANNELS)) {
            mHandler.sendEmptyMessage(MSG_UPDATE_CHANNELS);
        }
    }

    /**
     * The value change will be applied to DB when applyPendingDbOperation is called.
     */
    public void updateLocked(Long channelId, boolean locked) {
        ChannelWrapper channelWrapper = mData.channelWrapperMap.get(channelId);
        if (channelWrapper == null) {
            return;
        }
        if (channelWrapper.mChannel.isLocked() != locked) {
            channelWrapper.mChannel.setLocked(locked);
            if (locked == channelWrapper.mLockedInDb) {
                mLockedUpdateChannelIds.remove(channelWrapper.mChannel.getId());
            } else {
                mLockedUpdateChannelIds.add(channelWrapper.mChannel.getId());
            }
            channelWrapper.notifyChannelUpdated();
        }
    }

    /**
     * Applies the changed values by {@link #updateBrowsable} and {@link #updateLocked} to DB.
     */
    public void applyUpdatedValuesToDb() {
        ChannelData data = mData;
        ArrayList<Long> browsableIds = new ArrayList<>();
        ArrayList<Long> unbrowsableIds = new ArrayList<>();
        for (Long id : mBrowsableUpdateChannelIds) {
            ChannelWrapper channelWrapper = data.channelWrapperMap.get(id);
            if (channelWrapper == null) {
                continue;
            }
            if (channelWrapper.mChannel.isBrowsable()) {
                browsableIds.add(id);
            } else {
                unbrowsableIds.add(id);
            }
            channelWrapper.mBrowsableInDb = channelWrapper.mChannel.isBrowsable();
        }
        String column = TvContract.Channels.COLUMN_BROWSABLE;
        if (mStoreBrowsableInSharedPreferences) {
            SharedPreferences.Editor editor = mBrowsableSharedPreferences.edit();
            for (Long id : browsableIds) {
                editor.putBoolean(getBrowsableKey(getChannel(id)), true);
            }
            for (Long id : unbrowsableIds) {
                editor.putBoolean(getBrowsableKey(getChannel(id)), false);
            }
            editor.apply();
        } else {
            if (!browsableIds.isEmpty()) {
                updateOneColumnValue(column, 1, browsableIds);
            }
            if (!unbrowsableIds.isEmpty()) {
                updateOneColumnValue(column, 0, unbrowsableIds);
            }
        }
        mBrowsableUpdateChannelIds.clear();
        ArrayList<Long> lockedIds = new ArrayList<>();
        ArrayList<Long> unlockedIds = new ArrayList<>();
        for (Long id : mLockedUpdateChannelIds) {
            ChannelWrapper channelWrapper = data.channelWrapperMap.get(id);
            if (channelWrapper == null) {
                continue;
            }
            if (channelWrapper.mChannel.isLocked()) {
                lockedIds.add(id);
            } else {
                unlockedIds.add(id);
            }
            channelWrapper.mLockedInDb = channelWrapper.mChannel.isLocked();
        }
        column = TvContract.Channels.COLUMN_LOCKED;
        if (!lockedIds.isEmpty()) {
            updateOneColumnValue(column, 1, lockedIds);
        }
        if (!unlockedIds.isEmpty()) {
            updateOneColumnValue(column, 0, unlockedIds);
        }
        mLockedUpdateChannelIds.clear();
        if (Log.INCLUDE) {
            Log.d(
                    TAG,
                    "applyUpdatedValuesToDb"
                            + "\n browsableIds size:"
                            + browsableIds.size()
                            + "\n unbrowsableIds size:"
                            + unbrowsableIds.size()
                            + "\n lockedIds size:"
                            + lockedIds.size()
                            + "\n unlockedIds size:"
                            + unlockedIds.size());
        }
    }

    @MainThread
    private void clearChannels() {
        mData = new UnmodifiableChannelData();
    }

    @MainThread
    private void addChannel(ChannelData data, Channel channel) {
        data.channels.add(channel);
        String inputId = channel.getInputId();
        MutableInt count = data.channelCountMap.get(inputId);
        if (count == null) {
            data.channelCountMap.put(inputId, new MutableInt(1));
        } else {
            count.value++;
        }
    }

    @MainThread
    private void handleUpdateChannels() {
        if (Log.INCLUDE) {
            Log.d(TAG, "handlerUpdatechannels");
        }

        if (mChannelsUpdateTask != null) {
            mChannelsUpdateTask.cancel(true);
        }
        mChannelsUpdateTask = new QueryAllChannelsTask();
        mChannelsUpdateTask.executeOnDbThread();
    }

    /**
     * Reloads channel data.
     */
    public void reload() {
        if (mDbLoadFinished && !mHandler.hasMessages(MSG_UPDATE_CHANNELS)) {
            mHandler.sendEmptyMessage(MSG_UPDATE_CHANNELS);
        }
    }

    /**
     * A listener for ChannelDataManager. The callbacks are called on the main thread.
     */
    public interface Listener {
        /**
         * Called when data load is finished.
         */
        void onLoadFinished();

        /**
         * Called when channels are added, deleted, or updated. But, when browsable is changed, it
         * won't be called. Instead, {@link #onChannelBrowsableChanged} will be called.
         */
        void onChannelListUpdated();

        /**
         * Called when browsable of channels are changed.
         */
        void onChannelBrowsableChanged();


        void onUserBasedChannelListUpdated();
    }

    /**
     * A listener for individual channel change. The callbacks are called on the main thread.
     */
    public interface ChannelListener {
        /**
         * Called when the channel has been removed in DB.
         */
        void onChannelRemoved(Channel channel);

        /**
         * Called when values of the channel has been changed.
         */
        void onChannelUpdated(Channel channel);
    }

    private class ChannelWrapper {
        final Set<ChannelListener> mChannelListeners = new ArraySet<>();
        final Channel mChannel;
        boolean mBrowsableInDb;
        boolean mLockedInDb;
        boolean mInputRemoved;

        ChannelWrapper(Channel channel) {
            mChannel = channel;
            mBrowsableInDb = channel.isBrowsable();
            mLockedInDb = channel.isLocked();
            mInputRemoved = !mInputManager.hasTvInputInfo(channel.getInputId());
        }

        void addListener(ChannelListener listener) {
            mChannelListeners.add(listener);
        }

        void removeListener(ChannelListener listener) {
            mChannelListeners.remove(listener);
        }

        void notifyChannelUpdated() {
            for (ChannelListener l : mChannelListeners) {
                l.onChannelUpdated(mChannel);
            }
        }

        void notifyChannelRemoved() {
            for (ChannelListener l : mChannelListeners) {
                l.onChannelRemoved(mChannel);
            }
        }
    }

    private class CheckChannelLogoExistTask extends AsyncTask<Void, Void, Boolean> {
        private final Channel mChannel;

        CheckChannelLogoExistTask(Channel channel) {
            mChannel = channel;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try (AssetFileDescriptor f =
                         mContext.getContentResolver()
                                 .openAssetFileDescriptor(
                                         TvContract.buildChannelLogoUri(mChannel.getId()), "r")) {
                return true;
            } catch (FileNotFoundException e) {
                // no need to log just return false
            } catch (Exception e) {
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            ChannelWrapper wrapper = mData.channelWrapperMap.get(mChannel.getId());
            if (wrapper != null) {
                wrapper.mChannel.setChannelLogoExist(result);
            }
        }
    }

    private void initChannels() {
        if (Log.INCLUDE) Log.d(TAG, "initChannels()");

        //update browsable channel
        updateBrowsableChannels();

        //merge MBS channel information
        updateFavoriteChannels();
        updateBlockChannels();
        updateSubscribedChannels();

        if (TEST) {
            for (Channel ch : mData.channels) {
                switch (ch.getDisplayNumber() != null ? ch.getDisplayNumber() : "") {
                    case "260":
                        ch.setFavoriteChannel(true);
                        break;
                    case "262":
                        ch.setLocked(true);
                        break;
                }
            }
        }

        if (Log.INCLUDE) {
            List<Channel> channels = mData.channels;
            Log.d(TAG, "initChannels() channels.size:" + (channels == null ? null : channels.size()));
            for (Channel ch : channels) {
                Log.d(TAG, "initChannels() ch:" + chToString(ch));
            }

            Log.d(TAG, "initChannels() lwc:" + chToString(mChannelController.getLastWatchedChannel()));
        }

        mChannelRingStorage.update();

        if (Log.INCLUDE) {
            Log.d(TAG, "initChannels() promo:" + chToString(getPromoChannel()));
        }
    }

    public ChannelRingStorage getChannelRingStorage() {
        return mChannelRingStorage;
    }

    private void updateOneColumnValue(
            final String columnName, final int columnValue, final List<Long> ids) {
        if (!PermissionUtils.hasAccessAllEpg(mContext)) {
            return;
        }
        mDbExecutor.execute(
                new Runnable() {
                    @Override
                    public void run() {
                        String selection = Utils.buildSelectionForIds(TvContract.Channels._ID, ids);
                        ContentValues values = new ContentValues();
                        values.put(columnName, columnValue);
                        mContentResolver.update(
                                TvContract.Channels.CONTENT_URI, values, selection, null);
                    }
                });
    }

    private String getBrowsableKey(Channel channel) {
        return channel.getInputId() + "|" + channel.getId();
    }

    @MainThread
    private static class ChannelDataManagerHandler extends WeakHandler<ChannelDataManager> {
        public ChannelDataManagerHandler(ChannelDataManager channelDataManager) {
            super(Looper.getMainLooper(), channelDataManager);
        }

        @Override
        public void handleMessage(Message msg, @NonNull ChannelDataManager channelDataManager) {
            if (msg.what == MSG_UPDATE_CHANNELS) {
                channelDataManager.handleUpdateChannels();
            }
        }
    }

    /**
     * Container class which includes channel data that needs to be synced. This class is modifiable
     * and used for changing channel data. e.g. TvInputCallback, or AsyncDbTask.onPostExecute.
     */
    @MainThread
    private static class ChannelData {
        final Map<Long, ChannelWrapper> channelWrapperMap;
        final Map<String, MutableInt> channelCountMap;
        final List<Channel> channels;

        ChannelData() {
            channelWrapperMap = new HashMap<>();
            channelCountMap = new HashMap<>();
            channels = new ArrayList<>();
        }

        ChannelData(ChannelData data) {
            channelWrapperMap = new HashMap<>(data.channelWrapperMap);
            channelCountMap = new HashMap<>(data.channelCountMap);
            channels = new ArrayList<>(data.channels);
        }

        ChannelData(
                Map<Long, ChannelWrapper> channelWrapperMap,
                Map<String, MutableInt> channelCountMap,
                List<Channel> channels) {
            this.channelWrapperMap = channelWrapperMap;
            this.channelCountMap = channelCountMap;
            this.channels = channels;
        }
    }

    /**
     * Unmodifiable channel data.
     */
    @MainThread
    private static class UnmodifiableChannelData extends ChannelData {
        UnmodifiableChannelData() {
            super(Collections.unmodifiableMap(new HashMap<Long, ChannelWrapper>()),
                    Collections.unmodifiableMap(new HashMap<String, MutableInt>()),
                    Collections.unmodifiableList(new ArrayList<Channel>()));
        }

        UnmodifiableChannelData(ChannelData data) {
            super(
                    Collections.unmodifiableMap(data.channelWrapperMap),
                    Collections.unmodifiableMap(data.channelCountMap),
                    Collections.unmodifiableList(data.channels));
        }
    }

    public void setFavoriteChannel(Channel channel, boolean setFavorite) {
        channel.setFavoriteChannel(setFavorite);
        ChannelRing favRing = mChannelRingStorage.getChannelRing(ChannelRingStorage.RING_FAVORITE);
        if (setFavorite) {
            favRing.add(channel);
        } else {
            favRing.remove(channel);
        }
        mChannelRingStorage.printChannelRing();
    }

    public void setFavoriteChannelList(List<String> favoriteChannelList) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setFavoriteChannelList() favoriteChannelList.size:" + (favoriteChannelList == null ? null : favoriteChannelList.size()));
            for (int i = 0; favoriteChannelList != null && i < favoriteChannelList.size(); i++) {
                Log.d(TAG, "setFavoriteChannelList() favoriteChannelList[" + i + "]:" + favoriteChannelList.get(i));
            }
        }

        mChannelController.setFavoriteChannelList((ArrayList<String>) favoriteChannelList);
        updateFavoriteChannels();
        mChannelRingStorage.update();
    }

    public void setBlockChannelList(List<String> blockChannelList) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setBlockChannelList() blockChannelList.size:" + (blockChannelList == null ? null : blockChannelList.size()));
            for (int i = 0; blockChannelList != null && i < blockChannelList.size(); i++) {
                Log.d(TAG, "setBlockChannelList() blockChannelList[" + i + "]:" + blockChannelList.get(i));
            }
        }

        mChannelController.setBlockChannelList((ArrayList<String>) blockChannelList);
        updateBlockChannels();
        mChannelRingStorage.update();
    }

    public void setSubscribedChannelList(List<String> subscribedChannelList) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setSubscribedChannelList() subscribedChannelList.size:" + (subscribedChannelList == null ? null : subscribedChannelList.size()));
            for (int i = 0; subscribedChannelList != null && i < subscribedChannelList.size(); i++) {
                Log.d(TAG, "setSubscribedChannelList() subscribedChannelList[" + i + "]:" + subscribedChannelList.get(i));
            }
        }

        mChannelController.setSubscribedChannelList((ArrayList<String>) subscribedChannelList);
        updateSubscribedChannels();
        mChannelRingStorage.update();
    }

    private void updateBrowsableChannels() {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateBrowsableChannels()");
        }

        for (Channel ch : mData.channels) {
            ch.setBrowsable(!ch.isHiddenChannel() && !ch.isMosaicChannel());
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "updateBrowsableChannels() channels.size:" + (mData.channels == null ? null : mData.channels.size()));
            for (Channel ch : mData.channels) {
                Log.d(TAG, "updateBrowsableChannels() ch:" + chToString(ch));
            }
        }
    }

    private void updateFavoriteChannels() {
        ArrayList<String> favoriteChannelList = mChannelController.getFavoriteChannelList();
        if (Log.INCLUDE) {
            Log.d(TAG, "updateFavoriteChannels() favoriteChannelList:" + favoriteChannelList);
        }

        for (Channel ch : mData.channels) {
            if (ch.isHiddenChannel()) {
                ch.setFavoriteChannel(false);
            } else {
                boolean contains = favoriteChannelList.contains(ch.getServiceId());
                ch.setFavoriteChannel(contains);
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "updateFavoriteChannels() channels.size:" + (mData.channels == null ? null : mData.channels.size()));
            for (Channel ch : mData.channels) {
                Log.d(TAG, "updateFavoriteChannels() ch:" + chToString(ch));
            }
        }
    }

    private void updateBlockChannels() {
        ArrayList<String> blockChannelList = mChannelController.getBlockChannelList();
        if (Log.INCLUDE) {
            Log.d(TAG, "updateBlockChannels() blockChannelList:" + blockChannelList);
        }

        for (Channel ch : mData.channels) {
            boolean contains = blockChannelList.contains(ch.getServiceId());
            ch.setLocked(contains);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "updateBlockChannels() channels.size:" + (mData.channels == null ? null : mData.channels.size()));
            for (Channel ch : mData.channels) {
                Log.d(TAG, "updateBlockChannels() ch:" + chToString(ch));
            }
        }
    }

    private void updateSubscribedChannels() {
        ArrayList<String> subscribedChannelList = mChannelController.getSubscribedChannelList();
        if (Log.INCLUDE) {
            Log.d(TAG, "updateSubscribedChannels() subscribedChannelList:" + subscribedChannelList);
        }

        for (Channel ch : mData.channels) {
            boolean contains = subscribedChannelList.contains(ch.getServiceId());
            ch.setSubscribedChannel(contains);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "updateSubscribedChannels() channels.size:" + (mData.channels == null ? null : mData.channels.size()));
            for (Channel ch : mData.channels) {
                Log.d(TAG, "updateSubscribedChannels() ch:" + chToString(ch));
            }
        }
    }

    public String chToString(Channel ch) {
        if (ch == null) {
            return null;
        } else {
            String str = "Channel [" + ch.getDisplayNumber() + ":" + ch.getDisplayName() +"]"
                    + " serviceId:" + (ch.getServiceId())
                    + ", browsable:" + ch.isBrowsable()
                    + ", paid:" + ch.getExtension().getIsServicePaid()
                    + ", subscribed:" + ch.isSubscribedChannel()
                    + ", locked:" + ch.isLocked()
                    + ", fav:" + ch.isFavoriteChannel()
                    + ", audio:" + ch.isAudioChannel()
                    + ", mosaic:" + ch.isMosaicChannel()
                    + ", typeOfService:" + (ChannelImpl.typeToString(ch.getExtension().getTypeOfService()))
                    + ", timeshift:" + (ch.getTimeshiftService() / 3600000L) +"h"
                    + ", hasGenre:" + ch.getExtension().hasServiceGenre()
                    + ", getGenre:[" + (ch.getExtension().hasServiceGenre() ? ch.getExtension().getServiceGenre() : "none") +"]"
                    + ", genre:" + (ch.getGenreId() != -1 ? "\"" + JasGenre.getGenreLabel(mContext, ch.getGenreId()) + "\"" : "null")
                    + ", desc:\"" + ch.getDescription() + "\""
                    + ", id:" + ch.getId()
                    + ", type:" + ch.getType()
                    + ", mServiceType:" + ch.getServiceType()
                    + ", videoFormat:" + ch.getVideoFormat()
                    + ", uri:" + ch.getUri()
                    + ", searchable:" + ch.isSearchable()
                    + ", recordingProhibited:" + ch.isRecordingProhibited()
                    + ", isHidden:" + ch.isHiddenChannel()
                    + ", isPromo:" + ch.isPromoChannel()
                    + ", isAdult:" + ch.isAdultChannel()
                    ;
            return str;
        }
    }

    public Channel getGuideChannel() {
        Channel channel = mChannelController.getLastWatchedChannel();
        if (Log.INCLUDE) {
            Log.d(TAG, "getGuideChannel() guideChannel:" + channel);
        }
        if (channel == null) {
            if (mData != null && mData.channels != null && mData.channels.size() > 0) {
                channel = mData.channels.get(0);
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "getGuideChannel() firstChannel:" + channel);
            }
        }
        return channel;
    }

    public Channel getLowestChannel() {
        if (mData != null && mData.channels != null && mData.channels.size() > 0) {
            Channel channel = mData.channels.get(0);
            return channel;
        }
        return null;
    }

    public Channel getPromoChannel() {
        return mChannelRingStorage.getPromoChannel();
    }

    private static class ChannelController {
        private final ArrayList<String> mBlockChannelList = new ArrayList();
        private final ArrayList<String> mFavoriteChannelList = new ArrayList();
        private final ArrayList<String> mSubscribedChannelList = new ArrayList();
        private Channel mLastWatchedChannel;

        private Channel getLastWatchedChannel() {
            return mLastWatchedChannel;
        }

        private void setLastWatchedChannel(Channel channel) {
            mLastWatchedChannel = channel;
        }

        private void setBlockChannelList(ArrayList<String> blockChannels) {
            mBlockChannelList.clear();
            if (blockChannels != null) {
                mBlockChannelList.addAll(blockChannels);
            }
        }

        private ArrayList<String> getBlockChannelList() {
            return mBlockChannelList;
        }

        private void setFavoriteChannelList(ArrayList<String> favoriteChannelList) {
            mFavoriteChannelList.clear();
            if (favoriteChannelList != null) {
                mFavoriteChannelList.addAll(favoriteChannelList);
            }
        }

        private ArrayList<String> getFavoriteChannelList() {
            return mFavoriteChannelList;
        }

        private void setSubscribedChannelList(ArrayList<String> subscribedChannelList) {
            mSubscribedChannelList.clear();
            if (subscribedChannelList != null) {
                mSubscribedChannelList.addAll(subscribedChannelList);
            }
        }

        private ArrayList<String> getSubscribedChannelList() {
            return mSubscribedChannelList;
        }
    }

    public Channel getLastWatchedChannel() {
        return mChannelController.getLastWatchedChannel();
    }

    public void setLastWatchedChannel(Channel channel) {
        mChannelController.setLastWatchedChannel(channel);
    }

    private final class QueryAllChannelsTask extends AsyncDbTask.AsyncChannelQueryTask {
        QueryAllChannelsTask() {
            super(mDbExecutor, mContext);
        }

        @Override
        protected void onPostExecute(List<Channel> channels) {
            mChannelsUpdateTask = null;

            if (channels == null) {
                if (Log.INCLUDE) Log.e(TAG, "onPostExecute with null channels");
                return;
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "onPostExecute, channels size=" + channels.size());
            }

            ChannelData data = new ChannelData();
            data.channelWrapperMap.putAll(mData.channelWrapperMap);
            Set<Long> removedChannelIds = new HashSet<>(data.channelWrapperMap.keySet());
            List<ChannelWrapper> removedChannelWrappers = new ArrayList<>();
            List<ChannelWrapper> updatedChannelWrappers = new ArrayList<>();
            boolean channelAdded = false;
            boolean channelUpdated = false;
            boolean channelRemoved = false;
            Map<String, ?> deletedBrowsableMap = null;
            if (mStoreBrowsableInSharedPreferences) {
                deletedBrowsableMap = new HashMap<>(mBrowsableSharedPreferences.getAll());
            }
            for (Channel channel : channels) {
                if (TvFeatures.USE_ALTI_TIS_INPUT_ONLY.isEnabled(mContext)) {
                    if (channel.getInputId() != TvInputManagerHelper.ALTI_TIS) {
                        continue;
                    }
                }

                if (mStoreBrowsableInSharedPreferences) {
                    String browsableKey = getBrowsableKey(channel);
                    channel.setBrowsable(
                            mBrowsableSharedPreferences.getBoolean(browsableKey, false));
                    deletedBrowsableMap.remove(browsableKey);
                }
                long channelId = channel.getId();
                boolean newlyAdded = !removedChannelIds.remove(channelId);
                ChannelWrapper channelWrapper;
                if (newlyAdded) {
                    new CheckChannelLogoExistTask(channel)
                            .executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                    channelWrapper = new ChannelWrapper(channel);
                    data.channelWrapperMap.put(channel.getId(), channelWrapper);
                    if (!channelWrapper.mInputRemoved) {
                        channelAdded = true;
                    }
                } else {
                    channelWrapper = data.channelWrapperMap.get(channelId);
                    if (!channelWrapper.mChannel.hasSameReadOnlyInfo(channel)) {
                        // Channel data updated
                        Channel oldChannel = channelWrapper.mChannel;
                        // We assume that mBrowsable and mLocked are controlled by only TV app.
                        // The values for mBrowsable and mLocked are updated when
                        // {@link #applyUpdatedValuesToDb} is called. Therefore, the value
                        // between DB and ChannelDataManager could be different for a while.
                        // Therefore, we'll keep the values in ChannelDataManager.
                        channel.setBrowsable(oldChannel.isBrowsable());
                        channel.setLocked(oldChannel.isLocked());
                        channelWrapper.mChannel.copyFrom(channel);
                        if (!channelWrapper.mInputRemoved) {
                            channelUpdated = true;
                            updatedChannelWrappers.add(channelWrapper);
                        }
                    }
                }
            }
            if (mStoreBrowsableInSharedPreferences
                    && !deletedBrowsableMap.isEmpty()
                    && PermissionUtils.hasReadTvListings(mContext)) {
                // If hasReadTvListings(mContext) is false, the given channel list would
                // empty. In this case, we skip the browsable data clean up process.
                SharedPreferences.Editor editor = mBrowsableSharedPreferences.edit();
                for (String key : deletedBrowsableMap.keySet()) {
                    if (Log.INCLUDE) Log.d(TAG, "remove key: " + key);
                    editor.remove(key);
                }
                editor.apply();
            }
            for (long id : removedChannelIds) {
                ChannelWrapper channelWrapper = data.channelWrapperMap.remove(id);
                if (!channelWrapper.mInputRemoved) {
                    channelRemoved = true;
                    removedChannelWrappers.add(channelWrapper);
                }
            }
            for (ChannelWrapper channelWrapper : data.channelWrapperMap.values()) {
                if (!channelWrapper.mInputRemoved) {
                    addChannel(data, channelWrapper.mChannel);
                }
            }
            Collections.sort(data.channels, mChannelComparator);
            mData = new UnmodifiableChannelData(data);

            initChannels();

            if (!mDbLoadFinished) {
                mDbLoadFinished = true;
                notifyLoadFinished();
            } else if (channelAdded || channelUpdated || channelRemoved) {
                notifyChannelListUpdated();
            }
            for (ChannelWrapper channelWrapper : removedChannelWrappers) {
                channelWrapper.notifyChannelRemoved();
            }
            for (ChannelWrapper channelWrapper : updatedChannelWrappers) {
                channelWrapper.notifyChannelUpdated();
            }
            for (Runnable r : mPostRunnablesAfterChannelUpdate) {
                r.run();
            }
            mPostRunnablesAfterChannelUpdate.clear();
        }
    }
}