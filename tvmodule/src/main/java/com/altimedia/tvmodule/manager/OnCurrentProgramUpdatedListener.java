/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.manager;


import com.altimedia.tvmodule.dao.Program;

public interface OnCurrentProgramUpdatedListener {
    /** Called when the current program is updated. */
    void onCurrentProgramUpdated(long channelId, Program program);
}
