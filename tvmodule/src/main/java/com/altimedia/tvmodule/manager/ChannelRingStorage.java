/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.manager;

import java.util.Hashtable;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.ChannelRing;
import com.altimedia.util.Log;
import com.google.common.base.Predicates;

public class ChannelRingStorage {
    private static final String TAG = ChannelRingStorage.class.getSimpleName();

    public static class RingFunction {
        private String ringName;
        private Predicate<Channel> predicate;

        public RingFunction(String ringName, Predicate<Channel> predicate) {
            this.ringName = ringName;
            this.predicate = predicate;
        }

        @Override
        public String toString() {
            return "RingName='" + ringName;
        }
    }

    //predefined channel ring
    //you can get custom channel ring by getChannelRing(RingFunction)
    //eg) getChannelRing(new ChannelRingStorage.RingFunction("RING_PAID", Channel::isPaidChannel));
    public static final String RING_ALL = "RING_ALL";
    public static final String RING_BROWSABLE = "RING_BROWSABLE";
    public static final String RING_MINIEPG = "RING_MINIEPG";
    public static final String RING_HIDDEN = "RING_HIDDEN";
    public static final String RING_VIDEO = "RING_VIDEO";
    public static final String RING_AUDIO = "RING_AUDIO";
    public static final String RING_FAVORITE = "RING_FAVORITE";
    public static final String RING_BLOCK = "RING_BLOCK";
    public static final String RING_MOSAIC = "RING_MOSAIC";

    //predefined ring function
    private static final RingFunction FUNCTION_ALL = new RingFunction(RING_ALL, ch -> true);
    private static final RingFunction FUNCTION_BROWSABLE = new RingFunction(RING_BROWSABLE, Channel::isBrowsable);
    private static final RingFunction FUNCTION_HIDDEN = new RingFunction(RING_HIDDEN, Predicates.not(Channel::isBrowsable));
    private static final RingFunction FUNCTION_VIDEO = new RingFunction(RING_VIDEO, Predicates.not(Channel::isAudioChannel));
    private static final RingFunction FUNCTION_AUDIO = new RingFunction(RING_AUDIO, Channel::isAudioChannel);
    private static final RingFunction FUNCTION_FAVORITE = new RingFunction(RING_FAVORITE, Channel::isFavoriteChannel);
    private static final RingFunction FUNCTION_BLOCK = new RingFunction(RING_BLOCK, Channel::isLocked);
    private static final RingFunction FUNCTION_MOSAIC = new RingFunction(RING_MOSAIC, Channel::isMosaicChannel);

    private ChannelDataManager channelDataManager;
    private Hashtable<String, ChannelRing> table;

    ChannelRingStorage(ChannelDataManager channelDataManager) {
        this.channelDataManager = channelDataManager;
        table = new Hashtable<>();
        initRings();
    }

    void update() {
        table.clear();
        initRings();

        if (Log.INCLUDE) {
            printChannelRing();
        }
    }

    private void initRings() {
        List<Channel> channels = channelDataManager.getChannelList();

        table.put(FUNCTION_ALL.ringName, getChannelRingFromStream(channels.stream().filter(FUNCTION_ALL.predicate)));
        table.put(FUNCTION_BROWSABLE.ringName, getChannelRingFromStream(channels.stream().filter(FUNCTION_BROWSABLE.predicate)));
        table.put(FUNCTION_HIDDEN.ringName, getChannelRingFromStream(channels.stream().filter(FUNCTION_HIDDEN.predicate)));
        table.put(FUNCTION_VIDEO.ringName, getChannelRingFromStream(channels.stream().filter(FUNCTION_VIDEO.predicate)));
        table.put(FUNCTION_AUDIO.ringName, getChannelRingFromStream(channels.stream().filter(FUNCTION_AUDIO.predicate)));
        table.put(FUNCTION_FAVORITE.ringName, getChannelRingFromStream(channels.stream().filter(FUNCTION_FAVORITE.predicate)));
        table.put(FUNCTION_BLOCK.ringName, getChannelRingFromStream(channels.stream().filter(FUNCTION_BLOCK.predicate)));
        table.put(FUNCTION_MOSAIC.ringName, getChannelRingFromStream(channels.stream().filter(FUNCTION_MOSAIC.predicate)));

        //for navigate from mini epg.
        table.put(RING_MINIEPG, getChannelRingFromStream(channels.stream().filter(FUNCTION_BROWSABLE.predicate)));
    }

    private static ChannelRing getChannelRingFromStream(Stream<Channel> stream) {
        List<Channel> list = stream.collect(Collectors.toList());
        ChannelRing channelRing = new ChannelRing(list);
        return channelRing;
    }

    public ChannelRing getChannelRing(String ringName) {
        if (ringName == null) {
            return null;
        }
        ChannelRing channelRing = table.get(ringName);
        return channelRing;
    }

    public ChannelRing getChannelRing(RingFunction ringFunction) {
        if (ringFunction == null) {
            return null;
        }
        if (table.contains(ringFunction.ringName)) {
            return table.get(ringFunction.ringName);
        } else {
            List<Channel> channels = channelDataManager.getChannelList();
            ChannelRing channelRing = getChannelRingFromStream(channels.stream().filter(ringFunction.predicate));
            table.put(ringFunction.ringName, channelRing);
            return channelRing;
        }
    }

    void printChannelRing() {
        if (Log.INCLUDE) {
            Log.d(TAG, "table.size:"+table.size());
            table.forEach((k, v) -> {
                Log.d(TAG, "ringName:"+k+", ring:"+v);
            });
        }
    }

    public Channel getPromoChannel() {
        return getFirstChannel(RING_ALL, Channel::isPromoChannel);
    }

    //getFirstChannel(RING_ALL, Channel::isPromo);
    public Channel getFirstChannel(String ringName, Predicate<Channel> predicate) {
        return getChannelRing(ringName).stream().filter(predicate).findFirst().orElse(null);
    }

    //getChannels(RING_ALL, Channel::isPromo);
    public ChannelRing getChannels(String ringName, Predicate<Channel> predicate) {
        return getChannelRingFromStream(getChannelRing(ringName).stream().filter(predicate));
    }
}
