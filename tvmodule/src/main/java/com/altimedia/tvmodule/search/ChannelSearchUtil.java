/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.search;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.media.tv.TvContract;
import android.media.tv.TvInputManager;
import android.net.Uri;

import com.altimedia.tvmodule.dao.SearchResult;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ChannelSearchUtil {
    private static final String TAG = ChannelSearchUtil.class.getSimpleName();

    private final TvInputManager mTvInputManager;
    private final ContentResolver mContentResolver;


    public ChannelSearchUtil(TvInputManager mTvInputManager, ContentResolver mContentResolver) {
        this.mTvInputManager = mTvInputManager;
        this.mContentResolver = mContentResolver;
    }

    public List<SearchResult> searchChannels(Uri uri) {

        String[] projection = {
                TvContract.Channels._ID,
                TvContract.Channels.COLUMN_DISPLAY_NUMBER,
                TvContract.Channels.COLUMN_DISPLAY_NAME,
                TvContract.Channels.COLUMN_DESCRIPTION
        };
        List<SearchResult> searchResults = new ArrayList<>();
        try (Cursor c = mContentResolver.query(
                uri, projection, null, null, null)) {
            if (c != null) {
                int count = 0;
                while (c.moveToNext()) {
                    long id = c.getLong(0);
                    // Filter out the channel which has been already searched.
                    SearchResult.Builder result = SearchResult.builder();
                    result.setChannelId(id);
                    result.setChannelNumber(c.getString(1));
                    result.setTitle(c.getString(2));
                    result.setDescription(c.getString(3));
                    result.setImageUri(TvContract.buildChannelLogoUri(id).toString());
                    result.setIntentAction(Intent.ACTION_VIEW);
                    result.setIntentData(buildIntentData(id));
                    result.setContentType(TvContract.Programs.CONTENT_ITEM_TYPE);
                    result.setIsLive(true);
                    result.setProgressPercentage(0);
                    if (Log.INCLUDE) {
                        Log.d(TAG, " search result : " + result.toString());
                    }
                    searchResults.add(result.build());
                }
            }
        }
        return searchResults;
    }

    private String buildIntentData(long channelId) {
        return TvContract.buildChannelUri(channelId).toString();
    }

    private void appendSelectionString(
            StringBuilder sb, String[] columnForExactMatching, String[] columnForPartialMatching) {
        boolean firstColumn = true;
        if (columnForExactMatching != null) {
            for (String column : columnForExactMatching) {
                if (!firstColumn) {
                    sb.append(" OR ");
                } else {
                    firstColumn = false;
                }
                sb.append(column).append("=?");
            }
        }
        if (columnForPartialMatching != null) {
            for (String column : columnForPartialMatching) {
                if (!firstColumn) {
                    sb.append(" OR ");
                } else {
                    firstColumn = false;
                }
                sb.append(column).append(" LIKE ?");
            }
        }
    }

    private void insertSelectionArgumentStrings(
            String[] selectionArgs,
            int pos,
            String query,
            String[] columnForExactMatching,
            String[] columnForPartialMatching) {
        if (columnForExactMatching != null) {
            int until = pos + columnForExactMatching.length;
            for (; pos < until; ++pos) {
                selectionArgs[pos] = query;
            }
        }
        String selectionArg = "%" + query + "%";
        if (columnForPartialMatching != null) {
            int until = pos + columnForPartialMatching.length;
            for (; pos < until; ++pos) {
                selectionArgs[pos] = selectionArg;
            }
        }
    }

    private SearchResult buildSearchResultForInput(String inputId) {
        SearchResult.Builder result = SearchResult.builder();
        result.setIntentAction(Intent.ACTION_VIEW);
        result.setIntentData(TvContract.buildChannelUriForPassthroughInput(inputId).toString());
        return result.build();
    }
}
