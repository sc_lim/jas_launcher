/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.util;

import android.util.ArraySet;
import android.util.LongSparseArray;

import androidx.annotation.VisibleForTesting;

import java.util.Collections;
import java.util.Set;

/**
 * Uses a {@link LongSparseArray} to hold sets of {@code T}.
 *
 * <p>This has the same memory and performance trade offs listed in {@link LongSparseArray}.
 */
public class MultiLongSparseArray<T> {
    @VisibleForTesting
    static final int DEFAULT_MAX_EMPTIES_KEPT = 4;
    private final LongSparseArray<Set<T>> mSparseArray;
    private final Set<T>[] mEmptySets;
    private int mEmptyIndex = -1;

    public MultiLongSparseArray() {
        mSparseArray = new LongSparseArray<>();
        mEmptySets = new Set[DEFAULT_MAX_EMPTIES_KEPT];
    }

    public MultiLongSparseArray(int initialCapacity, int emptyCacheSize) {
        mSparseArray = new LongSparseArray<>(initialCapacity);
        mEmptySets = new Set[emptyCacheSize];
    }

    /**
     * Adds a mapping from the specified key to the specified value, replacing the previous mapping
     * from the specified key if there was one.
     */
    public void put(long key, T value) {
        Set<T> values = mSparseArray.get(key);
        if (values == null) {
            values = getEmptySet();
            mSparseArray.put(key, values);
        }
        values.add(value);
    }

    /** Removes the value at the specified index. */
    public void remove(long key, T value) {
        Set<T> values = mSparseArray.get(key);
        if (values != null) {
            values.remove(value);
            if (values.isEmpty()) {
                mSparseArray.remove(key);
                cacheEmptySet(values);
            }
        }
    }

    /**
     * Gets the set of Objects mapped from the specified key, or an empty set if no such mapping has
     * been made.
     */
    public Iterable<T> get(long key) {
        Set<T> values = mSparseArray.get(key);
        return values == null ? Collections.EMPTY_SET : values;
    }

    /** Clears cached empty sets. */
    public void clearEmptyCache() {
        while (mEmptyIndex >= 0) {
            mEmptySets[mEmptyIndex--] = null;
        }
    }

    @VisibleForTesting
    int getEmptyCacheSize() {
        return mEmptyIndex + 1;
    }

    private void cacheEmptySet(Set<T> emptySet) {
        if (mEmptyIndex < DEFAULT_MAX_EMPTIES_KEPT - 1) {
            mEmptySets[++mEmptyIndex] = emptySet;
        }
    }

    private Set<T> getEmptySet() {
        if (mEmptyIndex < 0) {
            return new ArraySet<>();
        }
        Set<T> emptySet = mEmptySets[mEmptyIndex];
        mEmptySets[mEmptyIndex--] = null;
        return emptySet;
    }

    @Override
    public String toString() {
        return mSparseArray.toString() + "(emptyCacheSize=" + getEmptyCacheSize() + ")";
    }
}
