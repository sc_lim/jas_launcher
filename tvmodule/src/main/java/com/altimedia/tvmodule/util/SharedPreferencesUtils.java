/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.util;

import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.altimedia.tvmodule.common.CommonConstants;

public class SharedPreferencesUtils {
    // Note that changing the preference name will reset the preference values.
    public static final String SHARED_PREF_FEATURES = "sharePreferencesFeatures";
    public static final String SHARED_PREF_BROWSABLE = "browsable_shared_preference";
    public static final String SHARED_PREF_WATCHED_HISTORY = "watched_history_shared_preference";
    public static final String SHARED_PREF_DVR_WATCHED_POSITION =
            "dvr_watched_position_shared_preference";
    public static final String SHARED_PREF_AUDIO_CAPABILITIES =
            CommonConstants.BASE_PACKAGE + ".audio_capabilities";
    public static final String SHARED_PREF_RECURRING_RUNNER = "sharedPreferencesRecurringRunner";
    public static final String SHARED_PREF_EPG = "epg_preferences";
    public static final String SHARED_PREF_SERIES_RECORDINGS = "seriesRecordings";
    /** No need to pre-initialize. It's used only on the worker thread. */
    public static final String SHARED_PREF_CHANNEL_LOGO_URIS = "channelLogoUris";
    /** Stores the UI related settings */
    public static final String SHARED_PREF_UI_SETTINGS = "ui_settings";

    public static final String SHARED_PREF_PREVIEW_PROGRAMS = "previewPrograms";

    private static boolean sInitializeCalled;

    public static synchronized void initialize(final Context context, final Runnable postTask) {
        if (!sInitializeCalled) {
            sInitializeCalled = true;
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    PreferenceManager.getDefaultSharedPreferences(context);
                    context.getSharedPreferences(SHARED_PREF_FEATURES, Context.MODE_PRIVATE);
                    context.getSharedPreferences(SHARED_PREF_BROWSABLE, Context.MODE_PRIVATE);
                    context.getSharedPreferences(SHARED_PREF_WATCHED_HISTORY, Context.MODE_PRIVATE);
                    context.getSharedPreferences(
                            SHARED_PREF_DVR_WATCHED_POSITION, Context.MODE_PRIVATE);
                    context.getSharedPreferences(
                            SHARED_PREF_AUDIO_CAPABILITIES, Context.MODE_PRIVATE);
                    context.getSharedPreferences(
                            SHARED_PREF_RECURRING_RUNNER, Context.MODE_PRIVATE);
                    context.getSharedPreferences(SHARED_PREF_EPG, Context.MODE_PRIVATE);
                    context.getSharedPreferences(
                            SHARED_PREF_SERIES_RECORDINGS, Context.MODE_PRIVATE);
                    context.getSharedPreferences(SHARED_PREF_UI_SETTINGS, Context.MODE_PRIVATE);
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    postTask.run();
                }
            }.execute();
        }
    }

    private SharedPreferencesUtils() {}
}
