/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.util;

import android.content.Context;
import android.content.pm.PackageManager;

public class PermissionUtils {
    /**
     * Permission to read the TV listings.
     */
    public static final String PERMISSION_READ_TV_LISTINGS = "android.permission.READ_TV_LISTINGS";

    private static Boolean sHasAccessAllEpgPermission;
    private static Boolean sHasAccessWatchedHistoryPermission;
    private static Boolean sHasModifyParentalControlsPermission;
    private static Boolean sHasChangeHdmiCecActiveSource;
    private static Boolean sHasReadContentRatingSystem;


    public static boolean hasAccessAllEpg(Context context) {
//        if (sHasAccessAllEpgPermission == null) {
//            sHasAccessAllEpgPermission =
//                    context.checkSelfPermission(
//                            "com.android.providers.tv.permission.ACCESS_ALL_EPG_DATA")
//                            == PackageManager.PERMISSION_GRANTED;
//        }
//        return sHasAccessAllEpgPermission;
        return true;
    }

    public static boolean hasAccessWatchedHistory(Context context) {
        if (sHasAccessWatchedHistoryPermission == null) {
            sHasAccessWatchedHistoryPermission =
                    context.checkSelfPermission(
                            "com.android.providers.tv.permission.ACCESS_WATCHED_PROGRAMS")
                            == PackageManager.PERMISSION_GRANTED;
        }
        return sHasAccessWatchedHistoryPermission;
    }

    public static boolean hasModifyParentalControls(Context context) {
        if (sHasModifyParentalControlsPermission == null) {
            sHasModifyParentalControlsPermission =
                    context.checkSelfPermission("android.permission.MODIFY_PARENTAL_CONTROLS")
                            == PackageManager.PERMISSION_GRANTED;
        }
        return sHasModifyParentalControlsPermission;
    }

    public static boolean hasReadTvListings(Context context) {
        return context.checkSelfPermission(PERMISSION_READ_TV_LISTINGS)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasInternet(Context context) {
        return context.checkSelfPermission("android.permission.INTERNET")
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasWriteExternalStorage(Context context) {
        return context.checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE")
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasChangeHdmiCecActiveSource(Context context) {
        if (sHasChangeHdmiCecActiveSource == null) {
            sHasChangeHdmiCecActiveSource =
                    context.checkSelfPermission(
                            "android.permission.CHANGE_HDMI_CEC_ACTIVE_SOURCE")
                            == PackageManager.PERMISSION_GRANTED;
        }
        return sHasChangeHdmiCecActiveSource;
    }

    public static boolean hasReadContetnRatingSystem(Context context) {
        if (sHasReadContentRatingSystem == null) {
            sHasReadContentRatingSystem =
                    context.checkSelfPermission(
                            "android.permission.READ_CONTENT_RATING_SYSTEMS")
                            == PackageManager.PERMISSION_GRANTED;
        }
        return sHasReadContentRatingSystem;
    }
}
