/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.util;

public class StringUtils {
    private StringUtils() {
    }

    /**
     * Returns compares two strings lexicographically and handles null values quietly.
     */
    public static int compare(String a, String b) {
        if (a == null) {
            return b == null ? 0 : -1;
        }
        if (b == null) {
            return 1;
        }
        return a.compareTo(b);
    }

    /** Returns {@code s} or {@code ""} if {@code s} is {@code null} */
    public static final String nullToEmpty(String s) {
        return s == null ? "" : s;
    }
}
