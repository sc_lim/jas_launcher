/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.util;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

public final class SoftPreconditions {
    private static final String TAG = "SoftPreconditions";

    public static boolean checkArgument(
            final boolean expression,
            String tag,
            @Nullable String errorMessageTemplate,
            @Nullable Object... errorMessageArgs) {
        if (!expression) {
            String msg = format(errorMessageTemplate, errorMessageArgs);
            warn(tag, "Illegal argument", new IllegalArgumentException(msg), msg);
        }
        return expression;
    }

    public static boolean checkArgument(final boolean expression) {
        checkArgument(expression, null, null);
        return expression;
    }

    public static <T> T checkNotNull(
            final T reference,
            String tag,
            @Nullable String errorMessageTemplate,
            @Nullable Object... errorMessageArgs) {
        if (reference == null) {
            String msg = format(errorMessageTemplate, errorMessageArgs);
            warn(tag, "Null Pointer", new NullPointerException(msg), msg);
        }
        return reference;
    }

    public static <T> T checkNotNull(final T reference) {
        return checkNotNull(reference, null, null);
    }

    public static boolean checkState(
            final boolean expression,
            String tag,
            @Nullable String errorMessageTemplate,
            @Nullable Object... errorMessageArgs) {
        if (!expression) {
            String msg = format(errorMessageTemplate, errorMessageArgs);
            warn(tag, "Illegal State", new IllegalStateException(msg), msg);
        }
        return expression;
    }

    public static boolean checkState(final boolean expression) {
        checkState(expression, null, null);
        return expression;
    }

    public static void warn(String tag, String prefix, Exception e, String msg)
            throws RuntimeException {
        if (TextUtils.isEmpty(tag)) {
            tag = TAG;
        }
        String logMessage;
        if (TextUtils.isEmpty(msg)) {
            logMessage = prefix;
        } else if (TextUtils.isEmpty(prefix)) {
            logMessage = msg;
        } else {
            logMessage = prefix + ": " + msg;
        }

        if (!CommonUtils.isRunningInTest()) {
            throw new RuntimeException(msg, e);
        } else {
            Log.w(tag, logMessage, e);
        }
    }

    static String format(@Nullable String template, @Nullable Object... args) {
        template = String.valueOf(template); // null -> "null"

        args = args == null ? new Object[]{"(Object[])null"} : args;

        // start substituting the arguments into the '%s' placeholders
        StringBuilder builder = new StringBuilder(template.length() + 16 * args.length);
        int templateStart = 0;
        int i = 0;
        while (i < args.length) {
            int placeholderStart = template.indexOf("%s", templateStart);
            if (placeholderStart == -1) {
                break;
            }
            builder.append(template, templateStart, placeholderStart);
            builder.append(args[i++]);
            templateStart = placeholderStart + 2;
        }
        builder.append(template, templateStart, template.length());

        // if we run out of placeholders, append the extra args in square braces
        if (i < args.length) {
            builder.append(" [");
            builder.append(args[i++]);
            while (i < args.length) {
                builder.append(", ");
                builder.append(args[i++]);
            }
            builder.append(']');
        }

        return builder.toString();
    }

    private SoftPreconditions() {
    }
}
