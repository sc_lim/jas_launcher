/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.util;

import android.content.Context;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.WorkerThread;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.Boolean.TRUE;

public class TvProviderUtils {
    private static final String TAG = "TvProviderUtils";

    public static final String EXTRA_PROGRAM_COLUMN_SERIES_ID = "series_id";
    public static final String EXTRA_PROGRAM_COLUMN_STATE  = "state";

    /**
     * Possible extra columns in TV provider.
     */
    @Retention(RetentionPolicy.SOURCE)
    public @interface TvProviderExtraColumn {
    }

    private static boolean sProgramHasSeriesIdColumn;
    private static boolean sRecordedProgramHasSeriesIdColumn;
    private static boolean sRecordedProgramHasStateColumn;

    /**
     * Checks whether a table contains a series ID column.
     *
     * <p>This method is different from {@link #getProgramHasSeriesIdColumn()} and {@link
     * #getRecordedProgramHasSeriesIdColumn()} because it may access to database, so it should be
     * run in worker thread.
     *
     * @return {@code true} if the corresponding table contains a series ID column; {@code false}
     * otherwise.
     */
    @WorkerThread
    public static synchronized boolean checkSeriesIdColumn(Context context, Uri uri) {
        boolean canCreateColumn = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O);
        if (!canCreateColumn) {
            return false;
        }
        return (Utils.isRecordedProgramsUri(uri)
        )
                || (Utils.isProgramsUri(uri) );
    }


    /**
     * Checks whether a table contains a state column.
     *
     * <p>This method is different from {@link #getRecordedProgramHasStateColumn()} because it may
     * access to database, so it should be run in worker thread.
     *
     * @return {@code true} if the corresponding table contains a state column; {@code false}
     * otherwise.
     */
    @WorkerThread
    public static synchronized boolean checkStateColumn(Context context, Uri uri) {
        boolean canCreateColumn = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O);
        if (!canCreateColumn) {
            return false;
        }
        return (Utils.isRecordedProgramsUri(uri)
        );
    }


    public static synchronized boolean getProgramHasSeriesIdColumn() {
        return TRUE.equals(sProgramHasSeriesIdColumn);
    }

    public static synchronized boolean getRecordedProgramHasSeriesIdColumn() {
        return TRUE.equals(sRecordedProgramHasSeriesIdColumn);
    }

    public static synchronized boolean getRecordedProgramHasStateColumn() {
        return TRUE.equals(sRecordedProgramHasStateColumn);
    }

    public static String[] addExtraColumnsToProjection(
            String[] projection, @TvProviderExtraColumn String column) {
        List<String> projectionList = new ArrayList<>(Arrays.asList(projection));
        if (!projectionList.contains(column)) {
            projectionList.add(column);
        }
        projection = projectionList.toArray(projection);
        return projection;
    }


    private TvProviderUtils() {
    }
}
