/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.tvmodule.util;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.dao.ProgramImpl;

/**
 * A utility class to parse and store data from the {@link
 * android.media.tv.TvContract.Programs#COLUMN_INTERNAL_PROVIDER_DATA} field in the {@link
 * android.media.tv.TvContract.Programs}.
 */
public final class InternalDataUtils {
    private static final boolean DEBUG = false;
    private static final String TAG = "InternalDataUtils";

    private InternalDataUtils() {
        // do nothing
    }

    /**
     * Deserializes a byte array into objects to be stored in the Program class.
     *
     * <p>Series ID and critic scores are loaded from the bytes.
     *
     * @param bytes   the bytes to be deserialized
     * @param builder the builder for the Program class
     */
    public static void deserializeInternalProviderData(byte[] bytes, ProgramImpl.Builder builder) {
        if (bytes == null || bytes.length == 0) {
            return;
        }
        try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bytes))) {
            builder.setSeriesId((String) in.readObject());
            builder.setCriticScores((List<Program.CriticScore>) in.readObject());
        } catch (NullPointerException e) {
            Log.e(TAG, "no bytes to deserialize");
        } catch (IOException e) {
            Log.e(TAG, "Could not deserialize internal provider contents");
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "class not found in internal provider contents");
        }
    }

    /**
     * Convenience method for converting relevant data in Program class to a serialized blob type
     * for storage in internal_provider_data field.
     *
     * @param program the program which contains the objects to be serialized
     * @return serialized blob-type data
     */
    @Nullable
    public static byte[] serializeInternalProviderData(Program program) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try (ObjectOutputStream out = new ObjectOutputStream(bos)) {
            if (!TextUtils.isEmpty(program.getSeriesId()) || program.getCriticScores() != null) {
                out.writeObject(program.getSeriesId());
                out.writeObject(program.getCriticScores());
                return bos.toByteArray();
            }
        } catch (IOException e) {
            Log.e(
                    TAG,
                    "Could not serialize internal provider contents for program: "
                            + program.getTitle());
        }
        return null;
    }

}
