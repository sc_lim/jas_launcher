/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.player.voplayer;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.player.AltiPlayer;
import com.altimedia.player.BuildConfig;
import com.altimedia.player.PlayerConstants;
import com.altimedia.player.PlayerImpl;
import com.altimedia.player.QMEventListener;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.database.DatabaseProvider;
import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoListener;
import com.visualon.android.exoplayer2.VOConfiguration;
import com.visualon.android.exoplayer2.VOEventListener;
import com.visualon.android.exoplayer2.VOExoPlayer;
import com.visualon.android.exoplayer2.VOLicenseChecker;
import com.visualon.android.exoplayer2.ui.VOSubtitleView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class VOPlayerImpl extends PlayerImpl {
    private static final String TAG = VOPlayerImpl.class.getSimpleName();
    private static final boolean INCLUDE = BuildConfig.DEBUG;
    private static final boolean VOLOG_ENABLED = false;
    private static boolean licenseInitialized = false;

    public static final String PREFER_EXTENSION_DECODERS_EXTRA = "prefer_extension_decoders";
    public static final String ABR_ALGORITHM_EXTRA = "abr_algorithm";
    public static final String ABR_ALGORITHM_DEFAULT = "default";
    public static final String ABR_ALGORITHM_RANDOM = "random";

    private VOExoPlayer mPlayer = null;
    private DefaultTrackSelector trackSelector;
    private VOSubtitleView subtitleView;

    private VideoInfoUpdater mVideoInfoUpdater;

    private static final String DOWNLOAD_CONTENT_DIRECTORY = "downloads";
    private File downloadDirectory;

    private EventLogger mVOEventLogger;
    private QMEventListener mQMEventListener;
    private QMEventListenerWrapper mQMEventListenerWrapper = new QMEventListenerWrapper();
    private class QMEventListenerWrapper implements VOEventListener, Player.EventListener {
        @Override
        public void onMessageReceived(int messageType, Object message) {
            if (mQMEventListener != null && message != null) {
                switch (messageType) {
                    case VOEventListener.VO_MSG_Initial_Delay:
                        long initTime = (Long) message;
                        if (INCLUDE) {
                            Log.d(TAG, "VO_MSG_Initial_Delay, time:" + initTime);
                        }
                        mQMEventListener.onInitialDelay(initTime);
                        break;
                    case VOEventListener.VO_MSG_Buffer_Underruns:
                        Pair<Long, Long> pair = (Pair<Long, Long>) message;
                        if (INCLUDE) {
                            Log.d(TAG, "VO_MSG_Buffer_Underruns, player_buffering_amount:" + pair.first + ", player_buffer_threshold:" + pair.second);
                        }
                        mQMEventListener.onBufferUnderruns(pair.first, pair.second);
                        break;
                    case VOEventListener.VO_MSG_Quality_Switch:
                        Format format = (Format) message;
                        if (INCLUDE) {
                            Log.d(TAG, "VO_MSG_Quality_Switch, type:" + format.sampleMimeType + ", resolution:" + format.width + "*" + format.height + ", bitrate:" + format.bitrate);
                        }
                        if (format.sampleMimeType.contains("video")) {
                            mQMEventListener.onQualitySwitchVideo(format.bitrate, format.width, format.height);
                        } else if (format.sampleMimeType.contains("audio")) {
                            mQMEventListener.onQualitySwitchAudio(format.bitrate);
                        }
                        mVideoInfoUpdater.update(format);
                        break;
                    case VOEventListener.VO_MSG_Media_Throughput_VIDEO:
                        Pair<Long, Long> videoDownloaded = (Pair<Long, Long>) message;
                        if (INCLUDE) {
                            Log.d(TAG, "VO_MSG_Media_Throughput_VIDEO, size:" + videoDownloaded.first + ", duration:" + videoDownloaded.second);
                        }
                        mQMEventListener.onMediaThroughputVideo(videoDownloaded.first, videoDownloaded.second);
                        mVideoInfoUpdater.update(videoDownloaded.first, videoDownloaded.second);
                        break;
                    case VOEventListener.VO_MSG_Media_Throughput_AUDIO:
                        Pair<Long, Long> audioDownloaded = (Pair<Long, Long>) message;
                        if (INCLUDE) {
                            Log.d(TAG, "VO_MSG_Media_Throughput_AUDIO, size:" + audioDownloaded.first + ", duration:" + audioDownloaded.second);
                        }
                        mQMEventListener.onMediaThroughputAudio(audioDownloaded.first, audioDownloaded.second);
                        break;
                    case VOEventListener.VO_MSG_ACTION_SEEK:
                        if (INCLUDE) {
                            Log.d(TAG, "VO_MSG_ACTION_SEEK");
                        }
                        mQMEventListener.onSeek();
                        break;
                    case VOEventListener.VO_MSG_ACTION_SEEK_COMPLETE:
                        if (INCLUDE) {
                            Log.d(TAG, "VO_MSG_ACTION_SEEK_COMPLETE");
                        }
                        mQMEventListener.onSeekComplete();
                        break;
                    default:
                        break;
                }
            }
        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            if (INCLUDE) {
                Log.d(TAG, "onTracksChanged() trackGroups:" + trackGroups + ", trackSelections:" + trackSelections);
            }
            if (mQMEventListener != null) {
                for (TrackSelection trackSelection : trackSelections.getAll()) {
                    if (trackSelection == null) {
                        continue;
                    }
                    Format format = trackSelection.getSelectedFormat();
                    String mimeType = format.containerMimeType;
                    assert mimeType != null;
                    if (mimeType.contains("video")) {
                        mQMEventListener.onVideoTracksChanged(format.bitrate, format.width, format.height);
                        mVideoInfoUpdater.update(format);
                    } else if (mimeType.contains("audio")) {
                        mQMEventListener.onAudioTracksChanged(format.bitrate);
                    }
                }
            }
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (INCLUDE) {
                Log.d(TAG, "onPlayerStateChanged() playWhenReady:" + playWhenReady + ", playbackState:" + playbackState);
            }
            if (mQMEventListener != null) {
                mQMEventListener.onPlayerStateChanged(playWhenReady, playbackState);
            }
        }
    };

    public VOPlayerImpl(Context context) {
        if (INCLUDE) {
            Log.d(TAG, "VOPlayerImpl()");
        }
        initializeLicense(context);

        VOConfiguration voConfiguration = new VOConfiguration();
        //Add the line below to disable the Codec Checker.
        voConfiguration.setEnableCodecAndFormatCheck(false);
        TrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory(voConfiguration);
        trackSelector = new DefaultTrackSelector(context, trackSelectionFactory);

        DefaultTrackSelector.ParametersBuilder parametersBuilder = new DefaultTrackSelector.ParametersBuilder(context);
        //Add the line below to use AVC/HEVC in one mpd file.
        parametersBuilder.setAllowVideoMixedMimeTypeAdaptiveness(true)
                //Add the line below to switching to UHD Content automatically.
                .setAllowVideoNonSeamlessAdaptiveness(true)
                .clearViewportSizeConstraints();
        DefaultTrackSelector.Parameters trackSelectorParameters = parametersBuilder.build();
        trackSelector.setParameters(trackSelectorParameters);

        mVideoInfoUpdater = new VideoInfoUpdater();

        mPlayer = new VOExoPlayer.Builder(context, voConfiguration)
                .setTrackSelector(trackSelector)
                .build();

        if (VOLOG_ENABLED && INCLUDE) {
            mVOEventLogger = new EventLogger(trackSelector);
            mPlayer.addAnalyticsListener(mVOEventLogger);
        }

        mPlayer.addListener(mQMEventListenerWrapper);
        mPlayer.addVOEventListener(mQMEventListenerWrapper);
    }

    private static synchronized void initializeLicense(Context context) {
        if (licenseInitialized == false) {
            if (INCLUDE) {
                Log.d(TAG, "initializeLicense()");
            }
            byte[] b = new byte[32 * 1024];
            try {
                InputStream is = context.getAssets().open("volicense.dat");
                is.read(b);
                is.close();
            } catch (IOException e) {
                if (INCLUDE) {
                    Log.d(TAG, "initializeLicense() cannot read license file.");
                }
                e.printStackTrace();
            }

            boolean result = VOLicenseChecker.getInstance().init(b, "");
            if (INCLUDE) {
                Log.d(TAG, "initializeLicense() result : " + result);
            }
//            if (INCLUDE) {
//                Log.d(TAG, "initializeLicense() License Remaining Days : " + VOLicenseChecker.getInstance().getRemainingDays() + " days");
//            }
//            VOLicenseChecker.getInstance().initEventAdapter(Looper.getMainLooper());
//            VOLicenseChecker.getInstance().addEventListener(new VOLicenseChecker.EventListener() {
//                @Override
//                public void onLicenseChecked(String feature, boolean result) {
//                    Log.d(TAG, "onLicenseChecked() feature:" + feature + ", result:" + result);
//                    if (!result) {
//                        Toast toast = Toast.makeText(context, "VOPlayer License Fail!", Toast.LENGTH_LONG);
//                        toast.show();
//                        new Handler().postDelayed(toast::cancel, 1000);
//                    }
//                }
//            });
            if (INCLUDE) {
                Log.d(TAG, "initializeLicense() done.");
            }

            licenseInitialized = true;
        }
    }

    private static RenderersFactory buildRenderersFactory(Context context, boolean preferExtensionRenderer) {
        if (INCLUDE) {
            Log.d(TAG, "buildRenderersFactory()");
        }
        @DefaultRenderersFactory.ExtensionRendererMode
        int extensionRendererMode =
                useExtensionRenderers()
                        ? (preferExtensionRenderer
                        ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        return new DefaultRenderersFactory(context)
                .setExtensionRendererMode(extensionRendererMode);
    }

    private static boolean useExtensionRenderers() {
        if (INCLUDE) {
            Log.d(TAG, "useExtensionRenderers()");
        }
        return true;
    }

    public SimpleExoPlayer getPlayer() {
        return mPlayer;
    }

    public void setVideoSurface(Surface surface) {
        if (INCLUDE) {
            Log.d(TAG, "setVideoSurface() surface:" + surface);
        }
        Player player = this.mPlayer;
        if (player != null) {
            player.getVideoComponent().setVideoSurface(surface);
        }
    }

    public void setVideoSurfaceView(SurfaceView surfaceView) {
        if (INCLUDE) {
            Log.d(TAG, "setVideoSurfaceView() surfaceView:" + surfaceView);
        }
        Player player = this.mPlayer;
        if (player != null) {
            player.getVideoComponent().setVideoSurfaceView(surfaceView);
        }
    }

    public void setVideoSurfaceHolder(SurfaceHolder surfaceHolder) {
        if (INCLUDE) {
            Log.d(TAG, "setVideoSurfaceHolder() surfaceHolder:" + surfaceHolder);
        }
        Player player = this.mPlayer;
        if (player != null) {
            player.getVideoComponent().setVideoSurfaceHolder(surfaceHolder);
        }
    }

    public void insertSubtitleView(ViewGroup viewGroup) {
        if (INCLUDE) {
            Log.d(TAG, "insertSubtitleView()");
        }
        if (subtitleView == null) {
            subtitleView = new VOSubtitleView(viewGroup.getContext());
            if (INCLUDE) {
                Log.d(TAG, "insertSubtitleView() created...");
            }
            viewGroup.addView(subtitleView);
            mPlayer.addTextOutput(subtitleView);
        }
    }

    public void setSubtitleStyle(String jsonString) {
        if (INCLUDE) {
            Log.d(TAG, "setSubtitleStyle()");
        }
        if (subtitleView != null) {
            if (INCLUDE) {
                Log.d(TAG, "setSubtitleStyle() setStyle called...");
            }
            subtitleView.setStyle(jsonString, false);
        }
    }

    public void setVideoInfoView(TextView videoInfoView) {
        mVideoInfoUpdater.setVideoInfoView(videoInfoView);
    }

    public void setVideoInfoEnabled(boolean enabled) {
        mVideoInfoUpdater.setVideoInfoEnabled(enabled);
    }

    public void setRepeatMode(int repeatMode) {
        if (INCLUDE) {
            Log.d(TAG, "setRepeatMode() repeatMode:" + AltiPlayer.EventListener.repeatModeToString(repeatMode));
        }
        if (mPlayer != null) {
            mPlayer.setRepeatMode(repeatMode);
        }
    }

    public void prepare(MediaSource mediaSource, boolean resetPosition, boolean resetState) {
        if (INCLUDE) {
            Log.d(TAG, "prepare() mediaSource:" + mediaSource + ", resetPosition:" + resetPosition + ", resetState:" + resetState);
        }
        mPlayer.prepare(mediaSource, resetPosition, resetState);
    }

    public void prepare(String url) {
        if (INCLUDE) {
            Log.d(TAG, "prepare() url:" + url);
        }
    }

    public void prepare(String url, Context context) {
        if (INCLUDE) {
            Log.d(TAG, "prepare() url:" + url + ", context:" + context);
        }
        File downloadContentDirectory = new File(getDownloadDirectory(context), DOWNLOAD_CONTENT_DIRECTORY);
        String userAgent = Util.getUserAgent(context, PlayerConstants.getApplicationName());
        DefaultDataSourceFactory upstreamFactory =
                new DefaultDataSourceFactory(context, new DefaultHttpDataSourceFactory(userAgent));
        DatabaseProvider databaseProvider = new ExoDatabaseProvider(context);
        Cache downloadCache =
                new SimpleCache(downloadContentDirectory, new NoOpCacheEvictor(), databaseProvider);
        DataSource.Factory dataSourceFactory = buildReadOnlyCacheDataSource(upstreamFactory, downloadCache);

        MediaSource mediaSource = new DashMediaSource.Factory(dataSourceFactory)
                .setDrmSessionManager(DrmSessionManager.getDummyDrmSessionManager())
                .createMediaSource(Uri.parse(url));

        mPlayer.prepare(mediaSource, false, false);
    }

    public void setPlayWhenReady(boolean playWhenReady) {
        if (INCLUDE) {
            Log.d(TAG, "setPlayWhenReady() playWhenReady:" + playWhenReady);
        }
        mPlayer.setPlayWhenReady(playWhenReady);
    }

    public void stop(boolean reset) {
        if (INCLUDE) {
            Log.d(TAG, "stop() reset:" + reset);
        }
        mPlayer.stop(reset);
    }

    private File getDownloadDirectory(Context context) {
        if (INCLUDE) {
            Log.d(TAG, "getDownloadDirectory() context:" + context);
        }
        if (downloadDirectory == null) {
            downloadDirectory = context.getExternalFilesDir(null);
            if (downloadDirectory == null) {
                downloadDirectory = context.getFilesDir();
            }
        }
        return downloadDirectory;
    }

    private static CacheDataSourceFactory buildReadOnlyCacheDataSource(
            DataSource.Factory upstreamFactory, Cache cache) {
        if (INCLUDE) {
            Log.d(TAG, "buildReadOnlyCacheDataSource() upstreamFactory:" + upstreamFactory + ", cache:" + cache);
        }
        return new CacheDataSourceFactory(
                cache,
                upstreamFactory,
                new FileDataSource.Factory(),
                /* cacheWriteDataSinkFactory= */ null,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                /* eventListener= */ null);
    }

    public void releasePlayer() {
        if (INCLUDE) {
            Log.d(TAG, "releasePlayer() mPlayer:" + mPlayer);
        }
        if (mPlayer != null) {
            if (subtitleView != null) {
                mPlayer.removeTextOutput(subtitleView);
                subtitleView = null;
            }
            setQMEventListener(null);

            mPlayer.removeVOEventListener(mQMEventListenerWrapper);
            mPlayer.removeListener(mQMEventListenerWrapper);
            if (mVOEventLogger != null) {
                mPlayer.removeAnalyticsListener(mVOEventLogger);
                mVOEventLogger = null;
            }

            mPlayer.release();
            mPlayer = null;
        }

        if (mVideoInfoUpdater != null) {
            mVideoInfoUpdater.release();
            mVideoInfoUpdater = null;
        }
    }

    public ArrayList<String> getTrackList(@AltiPlayer.TrackType int trackType) {
        ArrayList<String> list = new ArrayList<>();
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        DefaultTrackSelector.Parameters parameters = trackSelector.getParameters();
        for (int i = 0; mappedTrackInfo != null && i < mappedTrackInfo.getRendererCount(); i++) {
            int currentTrackType = mappedTrackInfo.getRendererType(i);
            if (currentTrackType == trackType) {
                TrackGroupArray trackGroupArray = mappedTrackInfo.getTrackGroups(i);
                for (int j = 0; j < trackGroupArray.length; j++) {
                    TrackGroup trackGroup = trackGroupArray.get(j);
                    for (int k = 0; k < trackGroup.length; k++) {
                        if (INCLUDE) {
                            Log.d(TAG, "getTrackList(), Type: " + trackType + " GroupIndex: " + j + " TrackIndex: " + k + " lang: " + trackGroup.getFormat(k).language);
                        }
                        list.add(trackGroup.getFormat(k).language);
                    }
                }
            }
        }

        if (INCLUDE) {
            Log.d(TAG, "getTrackList() trackType:" + trackType + ", list.size:" + (list != null ? list.size() : "null"));
            for (int i = 0; list != null && i < list.size(); i++) {
                Log.d(TAG, "getTrackList(), list[" + i + "]:" + list.get(i));
            }
        }
        return list;
    }

    public int getSelectedTrackIndex(@AltiPlayer.TrackType int trackType) {
        TrackSelectionArray trackSelectionArray = mPlayer.getCurrentTrackSelections();

        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        DefaultTrackSelector.Parameters parameters = trackSelector.getParameters();
        for (int i = 0; mappedTrackInfo != null && i < mappedTrackInfo.getRendererCount(); i++) {
            int currentTrackType = mappedTrackInfo.getRendererType(i);
            if (INCLUDE) {
                Log.d(TAG, "getSelectedTrackIndex() i:" + i + ", trackType:" + trackType);
            }
            if (currentTrackType == trackType) {
                TrackGroupArray trackGroupArray = mappedTrackInfo.getTrackGroups(i);
                TrackSelection selection = trackSelectionArray.get(i);
                TrackGroup selectedTrackGroup = (selection != null ? selection.getTrackGroup() : null);
                for (int j = 0; j < trackGroupArray.length; j++) {
                    TrackGroup trackGroup = trackGroupArray.get(j);
                    for (int k = 0; k < trackGroup.length; k++) {
                        if (INCLUDE) {
                            Log.d(TAG, "getSelectedTrackIndex() Type: " + trackType + " GroupIndex: " + j + " TrackIndex: " + k + " lang: " + trackGroup.getFormat(k).language);
                        }
                        if (trackGroup == selectedTrackGroup) {
                            if (INCLUDE) {
                                Log.d(TAG, "getSelectedTrackIndex() found selectedTrackGroup, index:" + j);
                            }
                            return j;
                        }
                    }
                }
            }
        }
        return -1;
    }

    /**
     * set selected track index
     * @param index - the track index to select or -1 to clear the track selection
     */
    public void setSelectedTrackIndex(@AltiPlayer.TrackType int trackType, int index) {
        MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
        DefaultTrackSelector.Parameters parameters = trackSelector.getParameters();
        DefaultTrackSelector.ParametersBuilder builder = parameters.buildUpon();
        for (int i = 0; mappedTrackInfo != null && i < mappedTrackInfo.getRendererCount(); i++) {
            int currentTrackType = mappedTrackInfo.getRendererType(i);
            if (currentTrackType == trackType) {
                builder.clearSelectionOverrides(/* rendererIndex= */ i).setRendererDisabled(/* rendererIndex= */ i, false);
                if (index != -1) {
                    DefaultTrackSelector.SelectionOverride override = new DefaultTrackSelector.SelectionOverride(index, new int[]{0}, 2, 0);
                    builder.setSelectionOverride(/* rendererIndex= */ i, mappedTrackInfo.getTrackGroups(/* rendererIndex= */ i), override);
                    if (INCLUDE) {
                        Log.d(TAG, "setSelectedTrackIndex() setSelectionOverride:" + i + ", trackType:" + trackType);
                    }
                } else {
                    builder.clearSelectionOverrides(/* rendererIndex= */ i).setRendererDisabled(/* rendererIndex= */ i, true);
                    if (INCLUDE) {
                        Log.d(TAG, "setSelectedTrackIndex() disable track:" + i + ", trackType:" + trackType);
                    }
                }
            }
        }
        trackSelector.setParameters(builder);
    }

    public void addEventListener(Player.EventListener eventListener) {
        mPlayer.addListener(eventListener);
    }

    public void removeEventListener(Player.EventListener eventListener) {
        mPlayer.removeListener(eventListener);
    }

    public void addVideoListener(VideoListener videoListener) {
        mPlayer.addVideoListener(videoListener);
    }

    public void removeVideoListener(VideoListener videoListener) {
        mPlayer.removeVideoListener(videoListener);
    }

    @Override
    public void setVideoScalingMode(@AltiPlayer.ScalingMode int scaleMode) {
        mPlayer.setVideoScalingMode(scaleMode);
    }

    public void setQMEventListener(QMEventListener qmEventListener) {
        mQMEventListener = qmEventListener;
    }

    private class VideoInfoUpdater implements Runnable {
        private static final String TAG = "VideoInfoUpdater";
        private Handler mHandler = new Handler();

        private TextView mVideoInfoView;
        private boolean mVideoInfoEnabled = false;
        private String formatInfo = "";
        private String speedInfo = "";

        private VideoInfoUpdater() {
        }

        private void release() {
            mVideoInfoView = null;
            formatInfo = null;
        }

        private void setVideoInfoView(TextView videoInfoView) {
            if (INCLUDE) {
                Log.d(TAG, "setVideoInfoView() videoInfoView:" + videoInfoView);
            }
            mVideoInfoView = videoInfoView;
            update();
        }

        private void setVideoInfoEnabled(boolean enabled) {
            if (INCLUDE) {
                Log.d(TAG, "setVideoInfoEnabled() enabled:" + enabled);
            }
            if (mVideoInfoEnabled == enabled) {
                return;
            }
            mVideoInfoEnabled = enabled;
            update();
        }

        private void update() {
            if (INCLUDE) {
                Log.d(TAG, "update() formatInfo:"+formatInfo);
            }
            mHandler.post(this);
        }

        private void update(Format format) {
            formatInfo = getVideoInfo(format);
            if (INCLUDE) {
                Log.d(TAG, "update() formatInfo:"+formatInfo);
            }
            mHandler.post(this);
        }

        private void update(long size, long duration) {
            speedInfo = "Download : " + (long)(size / (duration * 0.001)) + " bps";
            if (INCLUDE) {
                Log.d(TAG, "update() speedInfo:"+speedInfo);
            }
            mHandler.post(this);
        }

        private String getVideoInfo(Format format) {
            return format == null ? null : "Resolution : " + format.width + "x" + format.height + "\n"
                    + "CodecType : " + format.sampleMimeType + "\n"
                    + "Bitrate : " + format.bitrate + " bps" + "\n"
                    + "FrameRate : " + format.frameRate + "\n";
        }

        public void run() {
            if (mVideoInfoEnabled && mVideoInfoView != null) {
                if (mVideoInfoView.getVisibility() != View.VISIBLE) {
                    mVideoInfoView.setVisibility(View.VISIBLE);
                }
                mVideoInfoView.setText(formatInfo + speedInfo);
            } else {
                if (mVideoInfoView != null && mVideoInfoView.getVisibility() != View.GONE) {
                    mVideoInfoView.setVisibility(View.GONE);
                }
            }
        }
    }
}
