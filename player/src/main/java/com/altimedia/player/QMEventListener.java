package com.altimedia.player;

public interface QMEventListener {
    void onInitialDelay(long initTime);
    void onBufferUnderruns(long playerBufferingAmount, long playerBufferThreshold);
    void onQualitySwitchVideo(int bitrate, int width, int height);
    void onQualitySwitchAudio(int bitrate);
    void onMediaThroughputVideo(long size, long duration);
    void onMediaThroughputAudio(long size, long duration);
    void onVideoTracksChanged(int bitrate, int width, int height);
    void onAudioTracksChanged(int bitrate);
    void onSeek();
    void onSeekComplete();
    void onPlayerStateChanged(boolean playWhenReady, int playbackState);
}
