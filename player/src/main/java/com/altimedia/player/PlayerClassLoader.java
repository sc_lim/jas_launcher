/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.player;

import android.content.Context;
import android.util.Log;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

class PlayerClassLoader {
    private static final String TAG = PlayerClassLoader.class.getSimpleName();
    private static final boolean INCLUDE = BuildConfig.DEBUG;
    private static final String PLAYER_IMPL_CLASS = "com.altimedia.player.voplayer.VOPlayerImpl";

    static PlayerImpl createPlayerImpl(Context context) {
        if (INCLUDE) {
            Log.d(TAG, "createPlayerImpl()");
        }
        PlayerImpl playerImpl = null;
        try {
            Class<?> c = Class.forName(PLAYER_IMPL_CLASS);
            Constructor<?> constructor = c.getConstructor(Context.class);
            playerImpl = (PlayerImpl) constructor.newInstance(context);
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        if (INCLUDE) {
            Log.d(TAG, "createPlayerImpl() playerImpl:" + playerImpl);
        }
        return playerImpl;
    }
}
