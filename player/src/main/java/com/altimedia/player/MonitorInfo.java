package com.altimedia.player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MonitorInfo {
    private String said;
    private String accountId;
    private String profileId;
    private String transactionId;

    public MonitorInfo(String said, String accountId, String profileId) {
        this.said = said;
        this.accountId = accountId;
        this.profileId = profileId;
        this.transactionId = UUID.randomUUID().toString();
    }

    public MonitorInfo(String said, String accountId, String profileId, String transactionId) {
        this.said = said;
        this.accountId = accountId;
        this.profileId = profileId;
        this.transactionId = transactionId;
    }

    Map<String, String> createMap() {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("x-account-id", accountId);
        headerMap.put("x-profile-id", profileId);
        headerMap.put("x-device-id", said);
        headerMap.put("x-transaction-id", transactionId);
        return headerMap;
    }

    @Override
    public String toString() {
        return "MonitorInfo{" +
                "said='" + said + '\'' +
                ", accountId='" + accountId + '\'' +
                ", profileId='" + profileId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                '}';
    }
}
