/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.player;

import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.ExoMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.drm.MediaDrmCallback;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;

import java.util.Objects;
import java.util.UUID;

public class AltiMediaSource {
    private String userAgent = null;
    private DataSource.Factory dataSourceFactory;
    private MediaSource mediaSource = null;
    private final String playUrl;
    private final String laUrl;

    private AltiMediaSource(String playUrl, String laUrl, Context context, MonitorInfo monitorInfo) {
        this.playUrl = playUrl;
        this.laUrl = laUrl;
        userAgent = Util.getUserAgent(context, PlayerConstants.getApplicationName());
        dataSourceFactory = new DefaultHttpDataSourceFactory(userAgent);
        if (monitorInfo != null) {
            ((DefaultHttpDataSourceFactory)dataSourceFactory).getDefaultRequestProperties().set(monitorInfo.createMap());
        }
    }

    public AltiMediaSource(AltiMediaSource... altiMediaSources) {
        this(null, null, (Context)null, null);
        if (altiMediaSources != null && altiMediaSources.length > 0) {
            MediaSource[] mediaSources = new MediaSource[altiMediaSources.length];
            for (int i = 0; i < altiMediaSources.length ; i++) {
                mediaSources[i] = altiMediaSources[i].getMediaSource();
            }
            mediaSource = new MergingMediaSource(mediaSources);
        }
    }

    public AltiMediaSource(Context context, String url, MonitorInfo monitorInfo) {
        this(url, null, context, monitorInfo);
        mediaSource = new DashMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(url));
    }

    public AltiMediaSource(Context context, String playUrl, String laUrl, MonitorInfo monitorInfo) {
        this(playUrl, laUrl, context, monitorInfo);
        mediaSource = createLeafMediaSource(playUrl, laUrl);
    }

    public MediaSource getMediaSource() {
        return mediaSource;
    }

    public boolean equals(AltiMediaSource that) {
        if ((this.playUrl == null && this.laUrl == null) && (that.playUrl == null && that.laUrl == null)) {
            //cannot compare two AltiMediaSource - return false
            return false;
        } else {
            return Objects.equals(this.playUrl, that.playUrl)
                    && Objects.equals(this.laUrl, that.laUrl);
        }
    }

    private MediaSource createLeafMediaSource(String playUrl, String laUrl) {
        String drmType = "widevine";
        UUID drmScheme = Util.getDrmUuid(drmType);
        String drmLicenseUrl = laUrl;
        boolean drmMultiSession = true;
        String[] drmKeyRequestProperties = null;

        DrmInfo drmInfo = new DrmInfo(drmType, drmScheme, drmLicenseUrl, drmKeyRequestProperties, drmMultiSession);
        MediaDrmCallback mediaDrmCallback =
                createMediaDrmCallback(drmInfo.drmLicenseUrl, drmInfo.drmKeyRequestProperties);
        DrmSessionManager<ExoMediaCrypto> drmSessionManager =
                new DefaultDrmSessionManager.Builder()
                        .setUuidAndExoMediaDrmProvider(drmInfo.drmScheme, FrameworkMediaDrm.DEFAULT_PROVIDER)
                        .setMultiSession(drmInfo.drmMultiSession)
                        .build(mediaDrmCallback);

        String parameters_extension = null;
        Uri parameters_uri = Uri.parse(playUrl);

        return createLeafMediaSource(parameters_uri, parameters_extension, drmSessionManager);
    }

    private MediaSource createLeafMediaSource(
            Uri uri, String extension, DrmSessionManager<ExoMediaCrypto> drmSessionManager) {
        @C.ContentType int type = Util.inferContentType(uri, extension);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(dataSourceFactory)
                        .setDrmSessionManager(drmSessionManager)
                        .createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(dataSourceFactory)
                        .setDrmSessionManager(drmSessionManager)
                        .createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(dataSourceFactory)
                        .setDrmSessionManager(drmSessionManager)
                        .createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ProgressiveMediaSource.Factory(dataSourceFactory)
                        .setDrmSessionManager(drmSessionManager)
                        .createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }

    private HttpMediaDrmCallback createMediaDrmCallback(
            String licenseUrl, String[] keyRequestPropertiesArray) {
        HttpDataSource.Factory licenseDataSourceFactory = new DefaultHttpDataSourceFactory(userAgent);
        HttpMediaDrmCallback drmCallback =
                new HttpMediaDrmCallback(licenseUrl, licenseDataSourceFactory);
        if (keyRequestPropertiesArray != null) {
            for (int i = 0; i < keyRequestPropertiesArray.length - 1; i += 2) {
                drmCallback.setKeyRequestProperty(keyRequestPropertiesArray[i],
                        keyRequestPropertiesArray[i + 1]);
            }
        }
        return drmCallback;
    }

    private class DrmInfo {
        public final String drmType;
        public final UUID drmScheme;
        public final String drmLicenseUrl;
        public final String[] drmKeyRequestProperties;
        public final boolean drmMultiSession;

        public DrmInfo(
                String drmType,
                UUID drmScheme,
                String drmLicenseUrl,
                String[] drmKeyRequestProperties,
                boolean drmMultiSession) {
            this.drmType = drmType;
            this.drmScheme = drmScheme;
            this.drmLicenseUrl = drmLicenseUrl;
            this.drmKeyRequestProperties = drmKeyRequestProperties;
            this.drmMultiSession = drmMultiSession;
        }

        public String toString() {
            return "DrmInfo() drmType:" + drmType + ", drmScheme:" + drmScheme
                    + ", drmLicenseUrl:" + drmLicenseUrl + ", drmKeyRequestProperties:" + drmKeyRequestProperties
                    + ", drmMultiSession:" + drmMultiSession;
        }
    }
}
