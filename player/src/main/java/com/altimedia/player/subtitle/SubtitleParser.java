/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.player.subtitle;


import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mc.kim on 03,06,2020
 */
interface SubtitleParser {
    boolean canParse(String mimeType);

    Subtitle parse(InputStream inputStream, String inputEncoding, long startTimeUs)
            throws IOException;
}
