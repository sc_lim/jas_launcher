/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.player;

import android.content.Context;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.video.VideoListener;

import java.util.ArrayList;

public abstract class PlayerImpl {
    public abstract void setVideoSurface(Surface surface);

    public abstract void setVideoSurfaceView(SurfaceView surfaceView);

    public abstract void setVideoSurfaceHolder(SurfaceHolder surfaceHolder);

    public abstract void setRepeatMode(int repeatMode);

    public abstract void prepare(String url);

    public abstract void prepare(String url, Context context);

    public abstract void prepare(MediaSource mediaSource, boolean resetPosition, boolean resetState);

    public abstract void setPlayWhenReady(boolean playWhenReady);

    public abstract void stop(boolean reset);

    public abstract void releasePlayer();

    public abstract SimpleExoPlayer getPlayer();

    public abstract void insertSubtitleView(ViewGroup viewGroup);

    public abstract void setSubtitleStyle(String jsonString);

    public abstract void setVideoInfoView(TextView videoInfoTextView);

    public abstract void setVideoInfoEnabled(boolean enabled);

    public abstract ArrayList<String> getTrackList(@AltiPlayer.TrackType int trackType);

    public abstract int getSelectedTrackIndex(@AltiPlayer.TrackType int trackType);

    public abstract void setSelectedTrackIndex(@AltiPlayer.TrackType int trackType, int index);

    public abstract void addEventListener(Player.EventListener eventListener);

    public abstract void removeEventListener(Player.EventListener eventListener);

    public abstract void addVideoListener(VideoListener videoListener);

    public abstract void removeVideoListener(VideoListener videoListener);

    public abstract void setQMEventListener(QMEventListener qmEventListener);

    public void togglePlayPause() {
        Player player = getPlayer();
        if (player != null) {
            boolean isPlay = player.getPlayWhenReady();
            player.setPlayWhenReady(!isPlay);
        }
    }

    public void seekTo(long position) {
        Player player = getPlayer();
        if (player != null) {
            player.seekTo(position);
        }
    }

    public void setVideoScalingMode(@AltiPlayer.ScalingMode int scaleMode) {
        Player player = getPlayer();
        if (player != null) {
            player.getVideoComponent().setVideoScalingMode(scaleMode);
        }
    }

    public void fastRewind(long rewindTime) {
        Player player = getPlayer();
        if (player != null) {
            long seekPositionMillis = player.getContentPosition() - rewindTime;
            seekPositionMillis = Math.max(0, seekPositionMillis);

            player.seekTo(seekPositionMillis);
        }
    }

    public void fastForward(long forwardTime) {
        Player player = getPlayer();
        if (player != null) {
            long durationMillis = getDuration();
            long seekPositionMillis = player.getContentPosition() + forwardTime;
            seekPositionMillis = Math.max(0, seekPositionMillis);

            if (durationMillis == C.TIME_UNSET) {
                seekPositionMillis = Math.min(player.getBufferedPosition(), seekPositionMillis);
            } else {
                seekPositionMillis = Math.min(durationMillis, seekPositionMillis);
            }
            player.seekTo(seekPositionMillis);
        }
    }

    public long getDuration() {
        Player player = getPlayer();
        if (player != null) {
            return player.getDuration();
        } else {
            return 0;
        }
    }

    public long getCurrentPosition() {
        Player player = getPlayer();
        if (player != null) {
            return player.getCurrentPosition();
        } else {
            return -1;
        }
    }

    public long getBufferedPosition() {
        Player player = getPlayer();
        if (player != null) {
            return player.getBufferedPosition();
        } else {
            return -1;
        }
    }

    public boolean isPlaying() {
        Player player = getPlayer();
        if (player != null) {
            return player.getPlayWhenReady();
        } else {
            return false;
        }
    }
}