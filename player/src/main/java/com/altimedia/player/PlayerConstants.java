/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.player;

public class PlayerConstants {
    private static String APPLICATION_NAME = "JasLauncher";
    private static final String PACKAGE_NAME = "kr.altimedia.launcher.jasmin";

    public static String getApplicationName() {
        return APPLICATION_NAME;
    }

    public static String getPackageName() {
        return PACKAGE_NAME;
    }
}
