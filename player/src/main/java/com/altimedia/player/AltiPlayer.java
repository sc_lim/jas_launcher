/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package com.altimedia.player;

import android.content.Context;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.IntDef;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;

import java.util.ArrayList;

/**
 * AltiPlayer
 *
 * AltiPlayer play sequence
 *
 * 1.initialize
 *      //create
 *      altiPlayer = new AltiPlayer.Builder(context).build();
 *      altiPlayer.setVideoSurfaceView(surfaceView);
 *
 *      //add event listener (optional)
 *      altiPlayer.setEventListener(eventListener);
 *      altiPlayer.setVideoListener(videoListener);
 *
 * 2.prepare media
 *      //prepare media
 *      altiPlayer.setPlayWhenReady(true);//play when media ready (auto play)
 *      altiPlayer.setPlayWhenReady(false);//play manually
 *      altiPlayer.prepare(mediaSource);
 *
 * 3.playback
 *      //play media
 *      altiPlayer.play();
 *      altiPlayer.pause();
 *      altiPlayer.togglePlayPause();
 *      altiPlayer.fastRewind(10000L);
 *      altiPlayer.fastForward(10000L);
 *
 * 4.stop
 *      //stop media
 *      altiPlayer.stop();
 *
 *      //go to step 2
 *
 * 5.release
 *      //remove event listener (optional)
 *      altiPlayer.setEventListener(null);
 *      altiPlayer.setVideoListner(null);
 *
 *      //release player
 *      altiPlayer.releasePlayer();
 *
 *      //go to step 1
 */
public class AltiPlayer {
    private static final String TAG = AltiPlayer.class.getSimpleName();
    private static final boolean INCLUDE = BuildConfig.DEBUG;

    public static final int STATE_IDLE = Player.STATE_IDLE;
    public static final int STATE_BUFFERING = Player.STATE_BUFFERING;
    public static final int STATE_READY = Player.STATE_READY;
    public static final int STATE_ENDED = Player.STATE_ENDED;

    @IntDef({STATE_IDLE, STATE_BUFFERING, STATE_READY, STATE_ENDED})
    public @interface PlaybackStatus {}

    public static final int REPEAT_MODE_OFF = Player.REPEAT_MODE_OFF;
    public static final int REPEAT_MODE_ON = Player.REPEAT_MODE_ONE;
    public static final int REPEAT_MODE_ALL = Player.REPEAT_MODE_ALL;

    public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT = C.VIDEO_SCALING_MODE_SCALE_TO_FIT;
    public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING;

    @IntDef({VIDEO_SCALING_MODE_SCALE_TO_FIT, VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING})
    public @interface ScalingMode {}

    public static final int PLAYER_ERROR_TYPE_SOURCE = 0;
    public static final int PLAYER_ERROR_TYPE_RENDERER = 1;
    public static final int PLAYER_ERROR_TYPE_UNEXPECTED = 2;
    public static final int PLAYER_ERROR_TYPE_REMOTE = 3;
    public static final int PLAYER_ERROR_TYPE_OUT_OF_MEMORY = 4;
    public static final int PLAYER_ERROR_TYPE_USER_DEFINED_1 = 5;
    public static final int PLAYER_ERROR_TYPE_USER_DEFINED_2 = 6;
    public static final int PLAYER_ERROR_TYPE_USER_DEFINED_3 = 7;

    @IntDef({PLAYER_ERROR_TYPE_SOURCE, PLAYER_ERROR_TYPE_RENDERER, PLAYER_ERROR_TYPE_UNEXPECTED, PLAYER_ERROR_TYPE_REMOTE, PLAYER_ERROR_TYPE_OUT_OF_MEMORY, PLAYER_ERROR_TYPE_USER_DEFINED_1, PLAYER_ERROR_TYPE_USER_DEFINED_2, PLAYER_ERROR_TYPE_USER_DEFINED_3})
    public @interface PlayerErrorType {}

    public static final int TRACK_TYPE_UNKNOWN = C.TRACK_TYPE_UNKNOWN;
    public static final int TRACK_TYPE_DEFAULT = C.TRACK_TYPE_DEFAULT;
    public static final int TRACK_TYPE_AUDIO = C.TRACK_TYPE_AUDIO;
    public static final int TRACK_TYPE_VIDEO = C.TRACK_TYPE_VIDEO;
    public static final int TRACK_TYPE_TEXT = C.TRACK_TYPE_TEXT;
    public static final int TRACK_TYPE_METADATA = C.TRACK_TYPE_METADATA;
    public static final int TRACK_TYPE_CAMERA_MOTION = C.TRACK_TYPE_CAMERA_MOTION;
    public static final int TRACK_TYPE_NONE = C.TRACK_TYPE_NONE;

    @IntDef({TRACK_TYPE_UNKNOWN, TRACK_TYPE_DEFAULT, TRACK_TYPE_AUDIO, TRACK_TYPE_VIDEO, TRACK_TYPE_TEXT, TRACK_TYPE_METADATA, TRACK_TYPE_CAMERA_MOTION, TRACK_TYPE_NONE})
    public @interface TrackType {}

    public static final int SUBTITLE_FONT_LARGE = 0;
    public static final int SUBTITLE_FONT_MEDIUM = 1;
    public static final int SUBTITLE_FONT_SMALL = 2;

    @IntDef({SUBTITLE_FONT_LARGE, SUBTITLE_FONT_MEDIUM, SUBTITLE_FONT_SMALL})
    public @interface SubtitleFontStyle {}

    private static final String SUBTITLE_STYLE = "{\n" +
//            "        \"horizontal_position\": 0, \n" +
//            "        \"vertical_position\": 0, \n" +
            "        \"font_size\": %d, \n" +
//            "        \"image_size\": 100, \n" +
            "        \"font_color_opacity\": 100, \n" +
            "        \"font_color_list\": 16777215, \n" +
            "        \"background_color_opacity\": 0, \n" +
            "        \"background_color_list\": 0, \n" +
//            "        \"edge_color_opacity\": 0, \n" +
//            "        \"edge_color_list\": 255, \n" +
//            "        \"edge_type_list\": 0, \n" +
//            "        \"font_list\": 7, \n" +
            "        \"window_background_color_opacity\": 0, \n" +
//            "        \"window_background_color_list\": 0, \n" +
//            "        \"underline_font\": false, \n" +
//            "        \"bold_font\": true, \n" +
//            "        \"italic_font\": true, \n" +
//            "        \"subtitle_auto_adjustment\": true, \n" +
            "        \"subtitle_trim\": \"\" \n" +
            "}";

    private static final String SUBTITLE_STYLE_LARGE = String.format(SUBTITLE_STYLE, 120);
    private static final String SUBTITLE_STYLE_MEDIUM = String.format(SUBTITLE_STYLE, 100);
    private static final String SUBTITLE_STYLE_SMALL = String.format(SUBTITLE_STYLE, 80);

    public static final class Builder {
        private Context context;

        public Builder(Context context) {
            this.context = context;
        }

        public AltiPlayer build() {
            PlayerImpl playerImpl = PlayerClassLoader.createPlayerImpl(context);
            if (playerImpl != null) {
                return new AltiPlayer(playerImpl);
            } else {
                return null;
            }
        }
    }

    private PlayerImpl playerImpl = null;
    private EventListener mEventListener;
    private VideoListener mVideoListener;
    private Player.EventListener eventListenerWrapper = new Player.EventListener() {
        public void onTimelineChanged(Timeline timeline, int reason) {
            if (INCLUDE) {
                Log.d(TAG, "onTimelineChanged() timeline:" + timeline + ", reason:" + EventListener.timelineChangeReasonToString(reason));
            }
            if (mEventListener != null) {
                mEventListener.onTimelineChanged(timeline, reason);
            }
        }

        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            if (INCLUDE) {
                Log.d(TAG, "onTracksChanged() trackGroups:" + trackGroups + ", trackSelections:" + trackSelections);
            }
            if (mEventListener != null) {
                mEventListener.onTracksChanged(trackGroups, trackSelections);
            }
        }

        public void onLoadingChanged(boolean isLoading) {
            if (INCLUDE) {
                Log.d(TAG, "onLoadingChanged() isLoading:" + isLoading);
            }
            if (mEventListener != null) {
                mEventListener.onLoadingChanged(isLoading);
            }
        }

        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (INCLUDE) {
                Log.d(TAG, "onPlayerStateChanged() playWhenReady:" + playWhenReady + ", playbackState:" + EventListener.stateToString(playbackState));
            }
            if (mEventListener != null) {
                mEventListener.onPlayerStateChanged(playWhenReady, playbackState);
            }
        }

        public void onPlaybackSuppressionReasonChanged(int playbackSuppressionReason) {
            if (INCLUDE) {
                Log.d(TAG, "onPlaybackSuppressionReasonChanged() playbackSuppressionReason:" + EventListener.playbackSuppressionReasonToString(playbackSuppressionReason));
            }
            if (mEventListener != null) {
                mEventListener.onPlaybackSuppressionReasonChanged(playbackSuppressionReason);
            }
        }

        public void onIsPlayingChanged(boolean isPlaying) {
            if (INCLUDE) {
                Log.d(TAG, "onIsPlayingChanged() isPlaying:" + isPlaying);
            }
            if (mEventListener != null) {
                mEventListener.onIsPlayingChanged(isPlaying);
            }
        }

        public void onRepeatModeChanged(int repeatMode) {
            if (INCLUDE) {
                Log.d(TAG, "onRepeatModeChanged() repeatMode:" + EventListener.repeatModeToString(repeatMode));
            }
            if (mEventListener != null) {
                mEventListener.onRepeatModeChanged(repeatMode);
            }
        }

        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
            if (INCLUDE) {
                Log.d(TAG, "onShuffleModeEnabledChanged() shuffleModeEnabled:" + shuffleModeEnabled);
            }
            if (mEventListener != null) {
                mEventListener.onShuffleModeEnabledChanged(shuffleModeEnabled);
            }
        }

        public void onPlayerError(ExoPlaybackException error) {
            if (INCLUDE) {
                Log.d(TAG, "onPlayerError() error:" + error);
            }
            if (mEventListener != null) {
                //mEventListener.onPlayerError(error);
                int type = -1;
                String msg = error.getMessage();
                Exception exception = null;
                switch (error.type) {
                    case ExoPlaybackException.TYPE_SOURCE:
                        type = PLAYER_ERROR_TYPE_SOURCE;
                        exception = error.getSourceException();
                        break;
                    case ExoPlaybackException.TYPE_RENDERER:
                        type = PLAYER_ERROR_TYPE_RENDERER;
                        exception = error.getRendererException();
                        break;
                    case ExoPlaybackException.TYPE_UNEXPECTED:
                        type = PLAYER_ERROR_TYPE_UNEXPECTED;
                        exception = error.getUnexpectedException();
                        break;
                    case ExoPlaybackException.TYPE_REMOTE:
                        type = PLAYER_ERROR_TYPE_REMOTE;
                        break;
                    case ExoPlaybackException.TYPE_OUT_OF_MEMORY:
                        type = PLAYER_ERROR_TYPE_OUT_OF_MEMORY;
                        exception = new Exception(error.getOutOfMemoryError());
                        break;
                }
                mEventListener.onPlayerError(type, msg, exception);
            }
        }

        public void onPositionDiscontinuity(int discontinuityReason) {
            if (INCLUDE) {
                Log.d(TAG, "onPositionDiscontinuity() reason:" + EventListener.discontinuityReasonToString(discontinuityReason));
            }
            if (mEventListener != null) {
                mEventListener.onPositionDiscontinuity(discontinuityReason);
            }
        }

        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            if (INCLUDE) {
                Log.d(TAG, "onPlaybackParametersChanged() playbackParameters:" + playbackParameters);
            }
            if (mEventListener != null) {
                mEventListener.onPlaybackParametersChanged(playbackParameters);
            }
        }

        public void onSeekProcessed() {
            if (INCLUDE) {
                Log.d(TAG, "onSeekProcessed()");
            }
            if (mEventListener != null) {
                mEventListener.onSeekProcessed();
            }
        }
    };
    private com.google.android.exoplayer2.video.VideoListener videoListenerWrapper = new com.google.android.exoplayer2.video.VideoListener() {
        public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
            if (mVideoListener != null) {
                mVideoListener.onVideoSizeChanged(width, height, unappliedRotationDegrees, pixelWidthHeightRatio);
            }
        }

        public void onSurfaceSizeChanged(int width, int height) {
            if (mVideoListener != null) {
                mVideoListener.onSurfaceSizeChanged(width, height);
            }
        }

        public void onRenderedFirstFrame() {
            if (mVideoListener != null) {
                mVideoListener.onRenderedFirstFrame();
            }
        }
    };

    private AltiPlayer(PlayerImpl player) {
        playerImpl = player;
        playerImpl.addEventListener(eventListenerWrapper);
        playerImpl.addVideoListener(videoListenerWrapper);
    }

    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    public void setVideoListener(VideoListener videoListener) {
        mVideoListener = videoListener;
    }

    public void setQMEventListener(QMEventListener qmEventListener) {
        playerImpl.setQMEventListener(qmEventListener);
    }

    public void setVideoSurface(Surface surface) {
        playerImpl.setVideoSurface(surface);
    }

    public void setVideoSurfaceView(SurfaceView surfaceView) {
        playerImpl.setVideoSurfaceView(surfaceView);
    }

    public void setVideoSurfaceHolder(SurfaceHolder surfaceHolder) {
        playerImpl.setVideoSurfaceHolder(surfaceHolder);
    }

    public void setRepeatMode(int repeatMode) {
        playerImpl.setRepeatMode(repeatMode);
    }

    public SimpleExoPlayer getPlayer() {
        return playerImpl.getPlayer();
    }

    public void prepare(String url, Context context) {
        playerImpl.prepare(url, context);
    }

    public void prepare(AltiMediaSource mediaSource) {
        this.prepare2(mediaSource.getMediaSource());
    }

    public void prepare2(MediaSource mediaSource) {
        this.prepare2(mediaSource, true, true);
    }

    public void prepare2(MediaSource mediaSource, boolean resetPosition, boolean resetState) {
        playerImpl.prepare(mediaSource, resetPosition, resetState);
    }

    public void setPlayWhenReady(boolean playWhenReady) {
        playerImpl.setPlayWhenReady(playWhenReady);
    }

    public void togglePlayPause() {
        playerImpl.togglePlayPause();
    }

    public void pause() {
        setPlayWhenReady(false);
    }

    public void play() {
        setPlayWhenReady(true);
    }

    public void stop() {
        this.stop(false);
    }

    public void stop(boolean reset) {
        playerImpl.stop(reset);
    }

    public void setVideoScalingMode(@ScalingMode int scaleMode){
        playerImpl.setVideoScalingMode(scaleMode);
    }

    public void seekTo(long position) {
        playerImpl.seekTo(position);
    }

    public void fastRewind(long rewindTime) {
        playerImpl.fastRewind(rewindTime);
    }

    public void fastForward(long forwardTime) {
        playerImpl.fastForward(forwardTime);
    }

    public long getDuration() {
        return playerImpl.getDuration();
    }

    public long getCurrentPosition() {
        return playerImpl.getCurrentPosition();
    }

    public long getBufferedPosition() {
        return playerImpl.getBufferedPosition();
    }

    public boolean isPlaying() {
        return playerImpl.isPlaying();
    }

    public void releasePlayer() {
        setEventListener(null);
        setVideoListener(null);

        playerImpl.removeVideoListener(videoListenerWrapper);
        playerImpl.removeEventListener(eventListenerWrapper);
        playerImpl.releasePlayer();
    }

    public ArrayList<String> getTrackList(@TrackType int trackType) {
        return playerImpl.getTrackList(trackType);
    }

    /**
     * Returns the selected track index
     * @param trackType
     * @return The selected track index or -1 if there is no selection
     */
    public int getSelectedTrackIndex(@TrackType int trackType) {
        return playerImpl.getSelectedTrackIndex(trackType);
    }

    /**
     * set selected track index
     * @param index - the track index to select or -1 to clear the track selection
     */
    public void setSelectedTrackIndex(@TrackType int trackType, int index) {
        playerImpl.setSelectedTrackIndex(trackType, index);
    }

    public void insertSubtitleView(ViewGroup viewGroup) {
        playerImpl.insertSubtitleView(viewGroup);
    }

    public void setSubtitleStyle(String jsonString) {
        playerImpl.setSubtitleStyle(jsonString);
    }

    public void setSubtitleStyle(@SubtitleFontStyle int fontStyle) {
        String jsonString;
        switch (fontStyle) {
            case SUBTITLE_FONT_LARGE:
                jsonString = SUBTITLE_STYLE_LARGE;
                break;
            case SUBTITLE_FONT_SMALL:
                jsonString = SUBTITLE_STYLE_SMALL;
                break;
            case SUBTITLE_FONT_MEDIUM:
            default:
                jsonString = SUBTITLE_STYLE_MEDIUM;
                break;
        }
        playerImpl.setSubtitleStyle(jsonString);
    }

    public void setVideoInfoView(TextView videoInfoTextView) {
        playerImpl.setVideoInfoView(videoInfoTextView);
    }

    public void setVideoInfoEnabled(boolean enabled) {
        playerImpl.setVideoInfoEnabled(enabled);
    }

    public interface EventListener {
        default void onTimelineChanged(Timeline timeline, int reason) {}

        default void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {}

        default void onLoadingChanged(boolean isLoading) {}

        default void onPlayerStateChanged(boolean playWhenReady, @PlaybackStatus int playbackState) {}

        default void onPlaybackSuppressionReasonChanged(int playbackSuppressionReason) {}

        default void onIsPlayingChanged(boolean isPlaying) {}

        default void onRepeatModeChanged(int repeatMode) {}

        default void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {}

        default void onPlayerError(@PlayerErrorType int type, String msg, Exception exception) {}

        default void onPositionDiscontinuity(int reason) {}

        default void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {}

        default void onSeekProcessed() {}

        static String stateToString(int state) {
            switch (state) {
                case Player.STATE_IDLE:
                    return "STATE_IDLE";
                case Player.STATE_BUFFERING:
                    return "STATE_BUFFERING";
                case Player.STATE_READY:
                    return "STATE_READY";
                case Player.STATE_ENDED:
                    return "STATE_ENDED";
                default:
                    return "UNDEFINED_STATE:"+state;
            }
        }

        static String playbackSuppressionReasonToString(int reason) {
            switch (reason) {
                case Player.PLAYBACK_SUPPRESSION_REASON_NONE:
                    return "PLAYBACK_SUPPRESSION_REASON_NONE";
                case Player.PLAYBACK_SUPPRESSION_REASON_TRANSIENT_AUDIO_FOCUS_LOSS:
                    return "PLAYBACK_SUPPRESSION_REASON_TRANSIENT_AUDIO_FOCUS_LOSS";
                default:
                    return "UNDEFINED_PLAYBACK_SUPPRESSION_REASON:"+reason;
            }
        }

        static String repeatModeToString(int repeatMode) {
            switch (repeatMode) {
                case Player.REPEAT_MODE_OFF:
                    return "REPEAT_MODE_OFF";
                case Player.REPEAT_MODE_ONE:
                    return "REPEAT_MODE_ONE";
                case Player.REPEAT_MODE_ALL:
                    return "REPEAT_MODE_ALL";
                default:
                    return "UNDEFINED_REPEAT_MODE:"+repeatMode;
            }
        }

        static String discontinuityReasonToString(int discontinuityReason) {
            switch (discontinuityReason) {
                case Player.DISCONTINUITY_REASON_PERIOD_TRANSITION:
                    return "DISCONTINUITY_REASON_PERIOD_TRANSITION";
                case Player.DISCONTINUITY_REASON_SEEK:
                    return "DISCONTINUITY_REASON_SEEK";
                case Player.DISCONTINUITY_REASON_SEEK_ADJUSTMENT:
                    return "DISCONTINUITY_REASON_SEEK_ADJUSTMENT";
                case Player.DISCONTINUITY_REASON_AD_INSERTION:
                    return "DISCONTINUITY_REASON_AD_INSERTION";
                case Player.DISCONTINUITY_REASON_INTERNAL:
                    return "DISCONTINUITY_REASON_INTERNAL";
                default:
                    return "UNDEFINED_DISCONTINUITY_REASON:"+discontinuityReason;
            }
        }

        static String timelineChangeReasonToString(int reason) {
            switch (reason) {
                case Player.TIMELINE_CHANGE_REASON_PREPARED:
                    return "TIMELINE_CHANGE_REASON_PREPARED";
                case Player.TIMELINE_CHANGE_REASON_RESET:
                    return "TIMELINE_CHANGE_REASON_RESET";
                case Player.TIMELINE_CHANGE_REASON_DYNAMIC:
                    return "TIMELINE_CHANGE_REASON_DYNAMIC";
                default:
                    return "UNDEFINED_TIMELINE_CHANGE_REASON:"+reason;
            }
        }
    }

    public interface VideoListener {
        default void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {}

        default void onSurfaceSizeChanged(int width, int height) {}

        default void onRenderedFirstFrame() {}
    }
}
