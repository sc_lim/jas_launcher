/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.message.remote;


import com.altimedia.util.Log;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.MbsUtil;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.message.api.MessageApi;
import kr.altimedia.launcher.jasmin.dm.message.obj.Message;
import kr.altimedia.launcher.jasmin.dm.message.obj.response.MessageListResponse;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mc.kim on 11,05,2020
 */
public class MessageRemote {
    private static final String TAG = MessageRemote.class.getSimpleName();
    private MessageApi mMessageApi;

    public MessageRemote(MessageApi messageApi) {
        mMessageApi = messageApi;
    }

    public List<Message> getMessageList(String lang, String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<MessageListResponse> call = mMessageApi.getMessageList(lang, said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<MessageListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        MessageListResponse mMessageListResponse = response.body();
        return mMessageListResponse.getMessageList();
    }

    public boolean postMessageView(String accountId, String profileId, String messageId) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("accountId", accountId);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("messageId", messageId);

        Call<BaseResponse> call = mMessageApi.postMessageView(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);

        BaseResponse mBaseResponse = response.body();
        return mBaseResponse.getReturnCode().equalsIgnoreCase("s");
    }
}
