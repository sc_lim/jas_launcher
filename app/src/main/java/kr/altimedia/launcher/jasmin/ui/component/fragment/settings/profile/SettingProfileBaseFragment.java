package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile;

import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;

public class SettingProfileBaseFragment extends SettingBaseFragment {
    protected OnPushProfileChangeMessage mOnPushProfileChangeMessage;

    @Override
    protected int initLayoutResourceId() {
        return 0;
    }

    public void setOnPushProfileChangeMessage(OnPushProfileChangeMessage mOnPushProfileChangeMessage) {
        this.mOnPushProfileChangeMessage = mOnPushProfileChangeMessage;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mOnPushProfileChangeMessage = null;
    }

    public interface OnPushProfileChangeMessage {
        void onPushProfile(PushMessageReceiver.ProfileUpdateType profileUpdateType, ProfileInfo profileInfo);
    }
}
