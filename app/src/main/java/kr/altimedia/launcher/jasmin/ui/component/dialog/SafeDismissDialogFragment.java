/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.content.Context;
import android.os.PowerManager;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import kr.altimedia.launcher.jasmin.search.JasSearchActivity;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

/**
 * Provides the safe dismiss feature regardless of the DialogFragment's life cycle.
 */
public abstract class SafeDismissDialogFragment extends DialogFragment {
    protected TvOverlayManager mTvOverlayManager;
    private boolean mStateLossAttached = false;
    private boolean mAttached = false;
    private boolean mDismissPending = false;
    private final String TAG = SafeDismissDialogFragment.class.getSimpleName();


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mStateLossAttached = !isScreenOn(context);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttached : mStateLossAttached : " + mStateLossAttached);
        }
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof JasSearchActivity) {
            mTvOverlayManager = ((JasSearchActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }

    }
    private boolean isScreenOn(Context context){
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        return pm.isInteractive();
    }
    @Override
    public void onStart() {
        super.onStart();
        if (Log.INCLUDE) {
            Log.d(TAG, "onStart");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Log.INCLUDE) {
            Log.d(TAG, "onResume");
        }
        mAttached = isScreenOn(getContext());
        if (mDismissPending) {
            mDismissPending = false;
            dismiss();
        }
    }

    @Override
    public void show(@NonNull FragmentManager manager, @Nullable String tag) {
        if (!manager.isStateSaved()) {
            super.show(manager, tag);
        } else {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitAllowingStateLoss();
        }
    }

    public String getClassName() {
        return getClass().getName();
    }

    public TvOverlayManager getTvOverlayManager() {
        return mTvOverlayManager;
    }


    public abstract int getOverlayType();

    @Override
    public void onDestroy() {
        if (mTvOverlayManager != null) {
            mTvOverlayManager.onSafeDismissDialogDestroyed(getOverlayType(), this);
        }
        super.onDestroy();
    }

    /**
     * Dismiss safely regardless of the DialogFragment's life cycle.
     */
    @Override
    public void dismiss() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call dismiss : " + mAttached);
        }
        if (mStateLossAttached) {
            super.dismissAllowingStateLoss();
        } else {
            if (!mAttached) {
                mDismissPending = true;
            } else {
                if (!getFragmentManager().isStateSaved()) {
                    super.dismiss();
                } else {
                    super.dismissAllowingStateLoss();
                }
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mAttached = false;
        if (Log.INCLUDE) {
            Log.d(TAG, "onDetach ");
        }
    }
}
