/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.ArraySet;
import android.util.Pair;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.ChannelRing;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.dao.ProgramImpl;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ChannelRingStorage;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.tvmodule.util.Utils;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.util.GenreUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Manages the channels and programs for the program guide.
 */
@MainThread
public class ProgramManager {
    private static final boolean DEBUG = false;
    private static final String TAG = "ProgramManager";
    public static final int VIEW_TYPE_CHANNEL_LIST = 0;
    public static final int VIEW_TYPE_FAV_LIST = 1;
    public static final int VIEW_TYPE_POP_LIST = 2;
    public static final int VIEW_TYPE_AUDIO = 3;
    public static final long RESTRICT_CHANNEL_ID = -1;
    /**
     * If the first entry's visible duration is shorter than this value, we clip the entry out.
     * Note: If this value is larger than 1 min, it could cause mismatches between the entry's
     * position and detailed view's time range.
     */
    static final long FIRST_ENTRY_MIN_DURATION = TimeUnit.HOURS.toMillis(LiveTvActivity.TIMESHIFT_DISABLED ? 0 : 72);
    private static final long HOUR_IN_MILLIS = TimeUnit.HOURS.toMillis(1);
    private static final long HALF_HOUR_IN_MILLIS = HOUR_IN_MILLIS / 2;
    private static final long INVALID_ID = -1;

    private final TvInputManagerHelper mTvInputManagerHelper;
    private final ChannelDataManager mChannelDataManager;
    private final ProgramDataManager mProgramDataManager;
    private final ProgramGuide mProgramGuide;

    private final long mTimeShiftDifferTime = 3 * 24 * TimeUtil.HOUR;
    private long mStartUtcMillis;
    private long mEndUtcMillis;
    private long mFromUtcMillis;
    private long mToUtcMillis;
    final long MAX_TIME = TimeUtil.HOUR + (TimeUtil.MIN * 30);
    private List<Channel> mChannels = new ArrayList<>();
    private final Map<Long, List<TableEntry>> mChannelIdEntriesMap = new HashMap<>();
    private final List<List<Channel>> mGenreChannelList = new ArrayList<>();
    private final List<Integer> mFilteredGenreIds = new ArrayList<>();

    private Channel mBackChannelForMosaic = null;

    // Position of selected genre to filter channel list.
    private int mSelectedGenreId = GenreUtil.ID_ALL_CHANNELS;
    // Channel list after applying genre filter.
    // Should be matched with mSelectedGenreId always.
    private List<Channel> mFilteredChannels = mChannels;
    private boolean mChannelDataLoaded;

    private final Set<Listener> mListeners = new ArraySet<>();
    private final Set<TableEntriesUpdatedListener> mTableEntriesUpdatedListeners = new ArraySet<>();

    private final Set<TableEntryChangedListener> mTableEntryChangedListeners = new ArraySet<>();
    private final Context mContext;
    private final ChannelDataManager.Listener mChannelDataManagerListener =
            new ChannelDataManager.Listener() {
                @Override
                public void onLoadFinished() {
                    if (Log.INCLUDE) Log.d(TAG, "ChannelDataManager. onLoadFinished()");
                    mChannelDataLoaded = true;
                    updateChannels(false);
                }

                @Override
                public void onChannelListUpdated() {
                    if (Log.INCLUDE) Log.d(TAG, "ChannelDataManager. onChannelListUpdated()");
                    updateChannels(false);
                }

                @Override
                public void onChannelBrowsableChanged() {
                    if (Log.INCLUDE) Log.d(TAG, "ChannelDataManager. onChannelBrowsableChanged()");
                    updateChannels(false);
                }

                @Override
                public void onUserBasedChannelListUpdated() {
                    if (Log.INCLUDE)
                        Log.d(TAG, "ChannelDataManager. onUserBasedChannelListUpdated()");
                }
            };
    private final ProgramDataManager.Callback mProgramDataManagerCallback =
            new ProgramDataManager.Callback() {
                @Override
                public void onProgramUpdated() {
                    if (Log.INCLUDE) Log.d(TAG, "ProgramDataManager. onProgramUpdated()");
                    updateTableEntries(true);
                }

                @Override
                public void onChannelUpdated() {
                    if (Log.INCLUDE) Log.d(TAG, "ProgramDataManager.onChannelUpdated()");
                    updateTableEntriesWithoutNotification(false);
                    notifyTableEntriesUpdated();
                }

                @Override
                public void onChannelTuned(long tuneChannelId) {
                    if (Log.INCLUDE)
                        Log.d(TAG, "ProgramDataManager.tuneChannelId() : " + tuneChannelId);
                    updateTableEntriesWithoutNotification(false);
                    notifyTableEntriesUpdated();
                }
            };

    public ProgramManager(Context context,
                          TvInputManagerHelper tvInputManagerHelper,
                          ChannelDataManager channelDataManager,
                          ProgramDataManager programDataManager, ProgramGuide programGuide) {
        if (Log.INCLUDE) Log.d(TAG, "ProgramManager()");
        mContext = context;
        mTvInputManagerHelper = tvInputManagerHelper;
        mChannelDataManager = channelDataManager;
        mProgramDataManager = programDataManager;
        mProgramGuide = programGuide;
    }


    private BaseBubbleProvider.OnInformationInteractor mOnInformationInteractor = null;

    public BaseBubbleProvider.OnInformationInteractor getOnInformationInteractor() {
        return mOnInformationInteractor;
    }

    public void setOnInformationInteractor(BaseBubbleProvider.OnInformationInteractor mOnInformationInteractor) {
        this.mOnInformationInteractor = mOnInformationInteractor;
    }

    private HashMap<Integer, Integer> mViewTypeMap = new HashMap<>();
    private int mLastViewType = 0;

    void programGuideVisibilityChanged(boolean visible) {
        if (Log.INCLUDE) Log.d(TAG, "programGuideVisibilityChanged() visible:" + visible);
//        mProgramDataManager.setPauseProgramUpdate(visible);
        if (visible) {
            mChannelDataManager.addListener(mChannelDataManagerListener);
            mProgramDataManager.addCallback(mProgramDataManagerCallback);
        } else {
            mChannelDataManager.removeListener(mChannelDataManagerListener);
            mProgramDataManager.removeCallback(mProgramDataManagerCallback);
        }
    }

    public int mSelectedGenreId() {
        return mSelectedGenreId;
    }

    /**
     * Adds a {@link Listener}.
     */
    void addListener(Listener listener) {
        if (Log.INCLUDE) Log.d(TAG, "addListener() listener:" + listener);

        if (mListeners.contains(listener)) {
            return;
        }
        mListeners.add(listener);
    }

    /**
     * Registers a listener to be invoked when table entries are updated.
     */
    void addTableEntriesUpdatedListener(TableEntriesUpdatedListener listener) {
        if (Log.INCLUDE) Log.d(TAG, "addTableEntriesUpdatedListener() listener:" + listener);
        mTableEntriesUpdatedListeners.add(listener);
    }

    /**
     * Registers a listener to be invoked when a table entry is changed.
     */
    void addTableEntryChangedListener(TableEntryChangedListener listener) {
        if (Log.INCLUDE) Log.d(TAG, "addTableEntryChangedListener() listener:" + listener);
        mTableEntryChangedListeners.add(listener);
    }

    /**
     * Removes a {@link Listener}.
     */
    void removeListener(Listener listener) {
        if (!mListeners.contains(listener)) {
            return;
        }
        if (Log.INCLUDE) Log.d(TAG, "removeListener() listener:" + listener);
        mListeners.remove(listener);
    }

    /**
     * Removes a previously installed table entries update listener.
     */
    void removeTableEntriesUpdatedListener(TableEntriesUpdatedListener listener) {
        if (Log.INCLUDE) Log.d(TAG, "removeTableEntriesUpdatedListener() listener:" + listener);
        mTableEntriesUpdatedListeners.remove(listener);
    }

    /**
     * Removes a previously installed table entry changed listener.
     */
    void removeTableEntryChangedListener(TableEntryChangedListener listener) {
        if (Log.INCLUDE) Log.d(TAG, "removeTableEntryChangedListener() listener:" + listener);
        mTableEntryChangedListeners.remove(listener);
    }

    public void setViewTypeMap(HashMap<Integer, Integer> viewTypeMap) {
        this.mViewTypeMap = viewTypeMap;
    }

    /**
     * Resets channel list with given genre. Caller should call {@link #buildGenreFilters(Context)} prior
     * to call this API to make This notifies channel updates to listeners.
     */
    void resetChannelListWithGenre(int genreId) {
        resetChannelListWithGenre(genreId, true);
    }

    void resetChannelListWithGenre(int genreId, boolean withCallback) {
        if (Log.INCLUDE) Log.d(TAG, "resetChannelListWithGenre() genreId:" + genreId);
//        if (genreId == mSelectedGenreId) {
//            return;
//        }
        mFilteredChannels = mGenreChannelList.get(genreId);
        mSelectedGenreId = genreId;
        if (Log.INCLUDE) {
            Log.d(
                    TAG,
                    "resetChannelListWithGenre: "
                            + GenreUtil.getCanonical(genreId)
                            + " has "
                            + mFilteredChannels.size()
                            + " channels out of "
                            + mChannels.size() + "withCallback : " + withCallback);
        }
        if (mGenreChannelList.get(mSelectedGenreId) == null) {
            throw new IllegalStateException("Genre filter isn't ready.");
        }
        boolean viewTypeChanged = setViewType(genreId);
        if (withCallback) {
            notifyChannelsUpdated();
            notifyGenresUpdated();
            if (viewTypeChanged) {
                checkViewType(genreId);
            }
        }

    }

    private boolean setViewType(int genreId) {
        if (!mViewTypeMap.containsKey(genreId)) {
            return false;
        }
        int viewType = mViewTypeMap.get(genreId);
        if (/*mLastViewType != VIEW_TYPE_POP_LIST || */viewType != mLastViewType) {
            mLastViewType = viewType;
            return true;
        }
        return false;
    }

    private void checkViewType(int genreId) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                notifyViewTypeUpdated(mLastViewType);
            }
        });
    }

    public int getLastViewType() {
        return mLastViewType;
    }

    /**
     * Update the initial time range to manage. It updates program entries and genre as well.
     */
    void updateInitialTimeRange(long startUtcMillis, long endUtcMillis) {
        updateInitialTimeRange(startUtcMillis, endUtcMillis, startUtcMillis, endUtcMillis);
    }

    private final ArrayList<Pair<Long, Long>> mTimeViewPair = new ArrayList<>();
    private long mViewPortTime = 0;

//    private static final long TOTAL_TIME_MILLIS = TimeUnit.DAYS.toMillis(11);

    void updateInitialTimeRange(long startUtcMillis, long endUtcMillis, long fromUtcMillis, long toUtcMillis) {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateInitialTimeRange() startUtcMillis:" + startUtcMillis + ", endUtcMillis:" + endUtcMillis);
            Log.d(TAG, "updateInitialTimeRange() startUtcMillis:" + startUtcMillis + ", mEndUtcMillis:" + mEndUtcMillis);
        }
//        long viewPortTime = endUtcMillis - startUtcMillis;
        mStartUtcMillis = startUtcMillis;
        mEndUtcMillis = endUtcMillis;
//        if (endUtcMillis > mEndUtcMillis) {
//            mEndUtcMillis = endUtcMillis;
//        }
        initializeTimeViewPort(startUtcMillis, mEndUtcMillis, toUtcMillis - fromUtcMillis);

        mProgramDataManager.setPrefetchTimeRange(mStartUtcMillis);
        updateChannels(true);
        setTimeRange(fromUtcMillis, toUtcMillis, true, false);
    }

    private final ArrayList<ProgramManager.TableEntry> mTimeViewList = new ArrayList<>();
    private void initializeTimeViewPort(long startUtcMillis, long endUtcMillis, long viewPort) {
        mTimeViewList.clear();
        mViewPortTime = viewPort;
        mTimeViewPair.clear();
        long viewPortStartTime = startUtcMillis;
        while (viewPortStartTime < endUtcMillis) {
            long startTime = viewPortStartTime;
            long endTime = viewPortStartTime + viewPort;
            Pair<Long, Long> timeTable = new Pair<>(startTime, endTime);
            mTimeViewPair.add(timeTable);
            if (Log.INCLUDE) {
                Log.d(TAG, "initializeTimeViewPort | viewPortStartTime : " + startTime);
                Log.d(TAG, "initializeTimeViewPort | viewPortendTime : " + endTime);
            }
            mTimeViewList.add(new TableEntry(RESTRICT_CHANNEL_ID, startTime, endTime));
            viewPortStartTime += viewPort;
        }
    }


    private Pair<Long, Long> getViewPortTime(long startTime) {
        for (Pair<Long, Long> longPair : mTimeViewPair) {
            long portStartTime = longPair.first;
            long portEndTime = longPair.second;
            if (portStartTime <= startTime
                    && portEndTime >= startTime) {
                return longPair;
            }
        }
        return null;
    }


    /**
     * Shifts the time range by the given time. Also makes ProgramGuide scroll the views.
     */
    void shiftTime(long timeMillisToScroll, boolean keepFocus) {
        if (Log.INCLUDE) Log.d(TAG, "shiftTime() timeMillisToScroll:" + timeMillisToScroll);
        long fromUtcMillis = mFromUtcMillis + timeMillisToScroll;
        long toUtcMillis = mToUtcMillis + timeMillisToScroll;
        if (fromUtcMillis < mStartUtcMillis) {
            toUtcMillis += mStartUtcMillis - fromUtcMillis;
            fromUtcMillis = mStartUtcMillis;
        }
        if (toUtcMillis > mEndUtcMillis) {
            fromUtcMillis -= toUtcMillis - mEndUtcMillis;
            toUtcMillis = mEndUtcMillis;
        }
        setTimeRange(fromUtcMillis, toUtcMillis, false, keepFocus);
    }

    /**
     * Returned the scrolled(shifted) time in milliseconds.
     */
    long getShiftedTime() {
        if (Log.INCLUDE) Log.d(TAG, "getShiftedTime()");
        return mFromUtcMillis - mStartUtcMillis;
    }

    /**
     * Returns the start time set by {@link #updateInitialTimeRange}.
     */
    long getStartTime() {
        if (Log.INCLUDE) Log.d(TAG, "getStartTime()");
        return mStartUtcMillis;
    }

    /**
     * Returns the program index of the program with {@code entryId} or -1 if not found.
     */
    int getProgramIdIndex(long channelId, long entryId) {
        if (Log.INCLUDE)
            Log.d(TAG, "getProgramIdIndex() channelId:" + channelId + ", entryId:" + entryId);

        List<TableEntry> entries = mChannelIdEntriesMap.get(channelId);
        if (entries != null) {
            for (int i = 0; i < entries.size(); i++) {
                if (entries.get(i).getId() == entryId) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the program index of the program at {@code time} or -1 if not found.
     */
    int getProgramIndexAtTime(long channelId, long time) {
        if (Log.INCLUDE)
            Log.d(TAG, "getProgramIndexAtTime() channelId:" + channelId + ", time:" + time);
        List<TableEntry> entries = mChannelIdEntriesMap.get(channelId);
        if (entries != null) {
            for (int i = 0; i < entries.size(); ++i) {
                TableEntry entry = entries.get(i);
                if (entry.entryStartUtcMillis <= time && time < entry.entryEndUtcMillis) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the start time of currently managed time range, in UTC millisecond.
     */
    long getFromUtcMillis() {
        if (Log.INCLUDE) Log.d(TAG, "getFromUtcMillis()");
        return mFromUtcMillis;
    }

    /**
     * Returns the end time of currently managed time range, in UTC millisecond.
     */
    long getToUtcMillis() {
        if (Log.INCLUDE) Log.d(TAG, "getToUtcMillis()");

        return mToUtcMillis;
    }

    public long getStartUtcMillis() {
        if (Log.INCLUDE) Log.d(TAG, "getStartUtcMillis()");
        return mStartUtcMillis;
    }

    public long getEndUtcMillis() {
        if (Log.INCLUDE) Log.d(TAG, "getEndUtcMillis()");
        return mEndUtcMillis;
    }

    /**
     * Returns the number of the currently managed channels.
     */
    int getChannelCount() {
        if (Log.INCLUDE) Log.d(TAG, "getChannelCount()");

        return mFilteredChannels.size();
    }


    public List<Channel> getFilteredChannels() {
        return mFilteredChannels;
    }

    public Channel getBackChannelForMosaic() {
        return mBackChannelForMosaic;
    }

    /**
     * Returns a {@link Channel} at a given {@code channelIndex} of the currently managed channels.
     * Returns {@code null} if such a channel is not found.
     */
    Channel getChannel(int channelIndex) {
        if (Log.INCLUDE) Log.d(TAG, "getChannel() channelIndex:" + channelIndex);

        if (channelIndex < 0 || channelIndex >= getChannelCount()) {
            return null;
        }
        return mFilteredChannels.get(channelIndex);
    }

    /**
     * Returns the index of provided {@link Channel} within the currently managed channels. Returns
     * -1 if such a channel is not found.
     */
    int getChannelIndex(Channel channel) {
        if (Log.INCLUDE) Log.d(TAG, "getChannelIndex() channel:" + channel);

        return mFilteredChannels.indexOf(channel);
    }

    /**
     * Returns the index of channel with {@code channelId} within the currently managed channels.
     * Returns -1 if such a channel is not found.
     */
    int getChannelIndex(long channelId) {
        if (Log.INCLUDE) Log.d(TAG, "getChannelIndex() channelId:" + channelId);
        return getChannelIndex(mChannelDataManager.getChannel(channelId));
    }

    /**
     * Returns the number of "entries", which lies within the currently managed time range, for a
     * given {@code channelId}.
     */
    int getTableEntryCount(long channelId) {
        if (Log.INCLUDE) Log.d(TAG, "getTableEntryCount() channelId:" + channelId);

        return mChannelIdEntriesMap.isEmpty() ? 0 : (mChannelIdEntriesMap.get(channelId) != null
                && !mChannelIdEntriesMap.get(channelId).isEmpty()) ? mChannelIdEntriesMap.get(channelId).size() : 0;
    }

    List<TableEntry> getTableEntryList(long channelId) {
        if (Log.INCLUDE) Log.d(TAG, "getTableEntryList() channelId:" + channelId);
        return mChannelIdEntriesMap.get(channelId);
    }

    /**
     * Returns an entry as {@link ProgramImpl} for a given {@code channelId} and {@code index} of
     * entries within the currently managed time range. Returned {@link ProgramImpl} can be a dummy
     * one (e.g., whose channelId is INVALID_ID), when it corresponds to a gap between programs.
     */
    TableEntry getTableEntry(long channelId, int index) {
        if (Log.INCLUDE) Log.d(TAG, "getTableEntry() channelId:" + channelId + ", index:" + index);

//        mProgramDataManager.prefetchChannel(channelId, index);
        return mChannelIdEntriesMap.get(channelId).get(index);
    }

    /**
     * Returns list genre ID's which has a channel.
     */
    List<Integer> getFilteredGenreIds() {
        if (Log.INCLUDE) Log.d(TAG, "getFilteredGenreIds()");

        return mFilteredGenreIds;
    }

    int getSelectedGenreId() {
        if (Log.INCLUDE) Log.d(TAG, "getSelectedGenreId()");

        return mSelectedGenreId;
    }

    // Note that This can be happens only if program guide isn't shown
    // because an user has to select channels as browsable through UI.
    private void updateChannels(boolean clearPreviousTableEntries) {
        if (Log.INCLUDE)
            Log.d(TAG, "updateChannels() clearPreviousTableEntries:" + clearPreviousTableEntries);

        if (Log.INCLUDE) Log.d(TAG, "updateChannels");
        mChannels = mChannelDataManager.getBrowsableChannelList();
        if (Log.INCLUDE) {
            Log.d(TAG, "updateChannels : channel Size : " + (mChannels != null ? mChannels.size() : 0));
        }
//        mFilteredChannels = getChannelList(GenreUtil.getCanonical(mSelectedGenreId));
        updateTableEntriesWithoutNotification(clearPreviousTableEntries);
        // Channel update notification should be called after updating table entries, so that
        // the listener can get the entries.
        notifyChannelsUpdated();
        notifyTableEntriesUpdated();
        buildGenreFilters(mContext);
    }

    /**
     * Sets the channel list for testing
     */
    void setChannels(List<Channel> channels) {
        if (Log.INCLUDE)
            Log.d(TAG, "setChannels() channels.size:" + (channels == null ? "null" : channels.size()));

        mChannels = new ArrayList<>(channels);
        mSelectedGenreId = GenreUtil.ID_ALL_CHANNELS;
        mFilteredChannels = mChannels;
        buildGenreFilters(mContext);
    }

    public void setBackChannelForMosaic(Channel channel) {
        mBackChannelForMosaic = channel;
    }

    private void updateTableEntries(boolean clear) {
        if (Log.INCLUDE) Log.d(TAG, "updateTableEntries() clear:" + clear);
        updateTableEntriesWithoutNotification(clear);
        notifyTableEntriesUpdated();
        buildGenreFilters(mContext);
    }

    private ArrayList<ProgramManager.TableEntry> cloneList(ArrayList<ProgramManager.TableEntry> list) {
        ArrayList<ProgramManager.TableEntry> newList = new ArrayList<>(list.size());
        for (ProgramManager.TableEntry p : list) {
            newList.add(p.clone());
        }
        return newList;
    }

    /**
     * Updates the table entries without notifying the change.
     */
    private synchronized void updateTableEntriesWithoutNotification(boolean clear) {
        if (Log.INCLUDE) Log.d(TAG, "updateTableEntriesWithoutNotification() clear:" + clear);
        if (clear) {
            mChannelIdEntriesMap.clear();
        }
        for (Channel channel : mChannels) {
            long channelId = channel.getId();
            // Inline the updating of the mChannelIdEntriesMap here so we can only call
            // getParentalControlSettings once.

            List<TableEntry> entries = createProgramEntries(channelId, false);

            if (mProgramGuide.isRestrictedChannel(channel)) {
                mChannelIdEntriesMap.put(channelId, cloneList(mTimeViewList));
            } else if (mProgramGuide.isLockedChannel(channel)) {
                mChannelIdEntriesMap.put(channelId, cloneList(mTimeViewList));
            } else {
                mChannelIdEntriesMap.put(channelId, entries);
            }

            int size = entries.size();
            if (Log.INCLUDE) {
                Log.d(
                        TAG,
                        "Programs are loaded for channel "
                                + channel.getId()
                                + ", loaded size = "
                                + size);
            }
            if (size == 0) {
                continue;
            }

//            TableEntry lastEntry = entries.get(size - 1);
//            if (mEndUtcMillis < lastEntry.entryEndUtcMillis
//                    && lastEntry.entryEndUtcMillis != Long.MAX_VALUE) {
//                mEndUtcMillis = lastEntry.entryEndUtcMillis;
//            }
//            if (Log.INCLUDE) {
//                Log.d(TAG,
//                        "updateTableEntriesWithoutNotification | mEndUtcMillis : "
//                                + mEndUtcMillis);
//            }
        }
        if (mEndUtcMillis > mStartUtcMillis) {
            for (Channel channel : mChannels) {
                long channelId = channel.getId();
                List<TableEntry> entries = mChannelIdEntriesMap.get(channelId);

                if (entries.isEmpty()) {
                    long limitTime = mEndUtcMillis;
                    long startTime = mStartUtcMillis;
                    long programEndTime = startTime + MAX_TIME;
                    while (true) {
                        entries.add(new TableEntry(channelId, startTime, programEndTime, false));
                        startTime = programEndTime;
                        programEndTime += MAX_TIME;
                        if (startTime >= limitTime) {
                            break;
                        }
                    }
                } else {
                    TableEntry lastEntry = entries.get(entries.size() - 1);
                    if (mEndUtcMillis > lastEntry.entryEndUtcMillis) {
                        entries.add(
                                new TableEntry(
                                        channelId, lastEntry.entryEndUtcMillis, mEndUtcMillis));
                    } else if (lastEntry.entryEndUtcMillis == Long.MAX_VALUE) {
                        entries.remove(entries.size() - 1);
                        entries.add(
                                new TableEntry(
                                        lastEntry.channelId,
                                        lastEntry.program,
                                        lastEntry.entryStartUtcMillis,
                                        mEndUtcMillis,
                                        lastEntry.mIsBlocked, lastEntry.mIsAudioChannel));
                    }
                }
            }
        }
    }

    /**
     * Build genre filters based on the current programs. This categories channels by its current
     * program's canonical genres and subsequent @{link resetChannelListWithGenre(int)} calls will
     * reset channel list with built channel list. This is expected to be called whenever program
     * guide is shown.
     */

    private List<Channel> getChannelList(String canonicalId) {
        List<Channel> channelList = new ArrayList<>();
        switch (canonicalId) {
            case GenreUtil.ALL_CHANNELS: {
                for (Channel channel : mChannels) {
                    channelList.add(channel);
                }
            }
            break;
            case GenreUtil.FAVORITE_CHANNELS: {
                ChannelRing channels =
                        mChannelDataManager.getChannelRingStorage().getChannelRing(ChannelRingStorage.RING_FAVORITE);

                if (channels == null) {
                    break;
                }

                for (Channel channel : channels) {
                    channelList.add(channel);
                }
            }
            break;
            case GenreUtil.POP_CHANNELS: {
                ChannelRing channels =
                        mChannelDataManager.getChannelRingStorage().getChannelRing(ChannelRingStorage.RING_MOSAIC);

                if (channels == null) {
                    break;
                }

                if (Log.INCLUDE) {
                    Log.d(TAG, "pop channel  exist : " + channels.size());
                    for (Channel channel : channels) {
                        Log.d(TAG, "channel " + channel.toString());
                    }
                }
                channelList.addAll(channels);
            }
            break;
            case GenreUtil.MULTI_VIEW_CHANNELS:
                break;
            case GenreUtil.AUDIO_CHANNELS:
                ChannelRing channelRing = mChannelDataManager.getChannelRingStorage().getChannelRing(ChannelRingStorage.RING_AUDIO);
                if (channelRing == null) {
                    break;
                }
                channelList.addAll(channelRing);
                break;
            default:
                ChannelRingStorage.RingFunction function = GenreUtil.getRingFunction(canonicalId);
                if (function != null) {
                    ChannelRing channels =
                            mChannelDataManager.getChannelRingStorage().getChannelRing(function);
                    channelList.addAll(channels);
//                    for (Channel channel : channels) {
//                        if (!channel.isMosaicChannel() && !channel.isAudioChannel()) {
//                            channelList.add(channel);
//                        }
//                    }

                }
                break;
        }
        return channelList;
    }

    private void buildGenreFilters(Context context) {
        if (Log.INCLUDE) Log.d(TAG, "buildGenreFilters()");
        if (Log.INCLUDE) Log.d(TAG, "buildGenreFilters");

        int count = GenreUtil.getCount();
        mGenreChannelList.clear();
        for (int i = 0; i < count; i++) {
            mGenreChannelList.add(new ArrayList<>());
        }

        for (int i = 0; i < count; i++) {
            int id = i;
            String canonicalId = GenreUtil.getCanonical(id);
            mGenreChannelList.set(id, getChannelList(canonicalId));
//            switch (canonicalId) {
//                case GenreUtil.ALL_CHANNELS:
//                    mGenreChannelList.set(id, mChannels);
//                    break;
//                case GenreUtil.FAVORITE_CHANNELS: {
//                    ChannelRing channels =
//                            mChannelDataManager.getChannelRingStorage().getChannelRing(ChannelRingStorage.RING_FAVORITE);
//                    if (channels != null) {
//                        mGenreChannelList.get(id).addAll(channels);
//                    }
//                }
//                break;
//                case GenreUtil.POP_CHANNELS: {
//                    ChannelRing channels =
//                            mChannelDataManager.getChannelRingStorage().getChannelRing(ChannelRingStorage.RING_MOSAIC);
//                    if (channels != null) {
//                        if (Log.INCLUDE) {
//                            Log.d(TAG, "pop channel  exist : " + channels.size());
//                            for (Channel channel : channels) {
//                                Log.d(TAG, "channel " + channel.toString());
//                            }
//                        }
//                        mGenreChannelList.get(id).addAll(channels);
//                    } else {
//                        if (Log.INCLUDE) {
//                            Log.d(TAG, "pop channel not exist");
//                        }
//                    }
//                }
//                break;
//                case GenreUtil.MULTI_VIEW_CHANNELS:
//                    break;
//                default:
//                    ChannelRingStorage.RingFunction function = GenreUtil.getRingFunction(canonicalId);
//                    if (function != null) {
//                        ChannelRing channels =
//                                mChannelDataManager.getChannelRingStorage().getChannelRing(function);
//                        mGenreChannelList.get(id).addAll(channels);
//                    }
//
//                    break;
//            }
        }


        mFilteredGenreIds.clear();
        mFilteredGenreIds.add(0);
        for (int i = 1; i < GenreUtil.getCount(); i++) {
            if (mGenreChannelList.get(i).size() > 0) {
                mFilteredGenreIds.add(i);
            }
        }
//        mSelectedGenreId = GenreUtil.ID_ALL_CHANNELS;
        mFilteredChannels = getChannelList(GenreUtil.getCanonical(mSelectedGenreId));
        notifyGenresUpdated();
    }


    @Nullable
    private TableEntry getTableEntry(long channelId, long entryId) {
        if (Log.INCLUDE)
            Log.d(TAG, "getTableEntry() channelId:" + channelId + ", entryId:" + entryId);
        if (mChannelIdEntriesMap.isEmpty()) {
            return null;
        }
        List<TableEntry> entries = mChannelIdEntriesMap.get(channelId);
        if (entries != null) {
            for (TableEntry entry : entries) {
                if (entry.getId() == entryId) {
                    return entry;
                }
            }
        }
        return null;
    }

    private void updateEntry(TableEntry old, TableEntry newEntry) {
        if (Log.INCLUDE) Log.d(TAG, "updateEntry() old:" + old + ", newEntry:" + newEntry);

        List<TableEntry> entries = mChannelIdEntriesMap.get(old.channelId);
        int index = entries.indexOf(old);
        entries.set(index, newEntry);
        notifyTableEntryUpdated(newEntry);
    }

    public void setTimeRange(long fromUtcMillis, long toUtcMillis) {
        setTimeRange(fromUtcMillis, toUtcMillis, false, false);
    }

    public void setTimeRange(long fromUtcMillis, long toUtcMillis, boolean force, boolean keepFocus) {
        if (Log.INCLUDE)
            Log.d(TAG, "setTimeRange() fromUtcMillis:" + fromUtcMillis + ", toUtcMillis:" + toUtcMillis);

        if (Log.INCLUDE) {
            Log.d(
                    TAG,
                    "setTimeRange. {FromTime="
                            + Utils.toTimeString(fromUtcMillis)
                            + ", ToTime="
                            + Utils.toTimeString(toUtcMillis)
                            + "}");
        }
        if (force || (mFromUtcMillis != fromUtcMillis || mToUtcMillis != toUtcMillis)) {
            mFromUtcMillis = fromUtcMillis;
            mToUtcMillis = toUtcMillis;
            notifyTimeRangeUpdated(keepFocus);
        }
    }


    private List<TableEntry> createProgramEntries(long channelId, boolean parentalControlsEnabled) {
        if (Log.INCLUDE)
            Log.d(TAG, "createProgramEntries() channelId:" + channelId + ", parentalControlsEnabled:" + parentalControlsEnabled);

        List<TableEntry> entries = new ArrayList<>();
        boolean channelLocked =
                parentalControlsEnabled && mChannelDataManager.getChannel(channelId).isLocked();
        boolean audioChannel = mChannelDataManager.getChannel(channelId).isAudioChannel();
        if (audioChannel) {
            long limitTime = mEndUtcMillis;
            long startTime = mStartUtcMillis;
            long programEndTime = startTime + MAX_TIME;
            while (true) {
                entries.add(new TableEntry(channelId, startTime, programEndTime, false, true));
                startTime = programEndTime;
                programEndTime += MAX_TIME;
                if (startTime >= limitTime) {
                    break;
                }
            }
        } else {
            long lastProgramEndTime = mStartUtcMillis;
            if (Log.INCLUDE)
                Log.d(TAG, "  createProgramEntries() mStartUtcMillis:" + mStartUtcMillis);
            List<Program> programs = mProgramDataManager.getPrograms(channelId, mStartUtcMillis);
            if (Log.INCLUDE) {
                Log.d(TAG, "  createProgramEntries() channelID:" + channelId + ", programs.size:" + (programs != null ? programs.size() : "null"));
            }
            for (Program program : programs) {
                if (program.getChannelId() == INVALID_ID) {
                    // Dummy program.
                    continue;
                }

                long programStartTime = Math.max(program.getStartTimeUtcMillis(), mStartUtcMillis);

                if (DEBUG) {
                    Log.d(TAG, "  createProgramEntries() channelID:" + channelId + ", program.getStartTimeUtcMillis() :" + program.getStartTimeUtcMillis());
                    Log.d(TAG, "  createProgramEntries() channelID:" + channelId + ", mStartUtcMillis :" + mStartUtcMillis);
                    Log.d(TAG, "  createProgramEntries() channelID:" + channelId + ", programStartTime :" + programStartTime);
                }

                long programEndTime = program.getEndTimeUtcMillis();
                if (programStartTime > lastProgramEndTime) {
                    // Gap since the last program.
//                    entries.add(new TableEntry(channelId, lastProgramEndTime, programStartTime));
//                    lastProgramEndTime = programStartTime;
                    if (Log.INCLUDE) {
                        Log.d(TAG, "make gap");
                    }
                    Pair<Long, Long> pairView = getViewPortTime(lastProgramEndTime);
                    if (pairView == null) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "gap pairView == null");
                        }
                        entries.add(
                                new TableEntry(
                                        channelId,
                                        lastProgramEndTime,
                                        programStartTime, channelLocked));
                        lastProgramEndTime = programStartTime;
                        continue;
                    }

                    long viewPortEndTime = pairView.second;

                    if (viewPortEndTime >= programStartTime) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "gap pairView != null including | viewPortEndTime : " + viewPortEndTime + ", programEndTime : " + programEndTime);
                        }
                        entries.add(
                                new TableEntry(
                                        channelId,
                                        lastProgramEndTime,
                                        programStartTime, channelLocked));
                        lastProgramEndTime = programStartTime;
                    } else {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "gap pairView != null including over | viewPortEndTime : " + viewPortEndTime + ", programEndTime : " + programEndTime);
                        }
                        long checkDummyEndTime = viewPortEndTime;
                        boolean isRunning = true;
                        while (true) {

                            entries.add(
                                    new TableEntry(
                                            channelId,
                                            lastProgramEndTime,
                                            checkDummyEndTime, channelLocked));

                            if (DEBUG) {
                                Log.d(TAG, "gap createProgramEntries | program : " + program.getTitle());
                                Log.d(TAG, "gap createProgramEntries | programEndTime : " + programEndTime);
                                Log.d(TAG, "gap createProgramEntries | lastProgramEndTime : " + lastProgramEndTime);
                                Log.d(TAG, "gap createProgramEntries | checkDummyEndTime : " + checkDummyEndTime);
                            }

                            lastProgramEndTime = checkDummyEndTime;

                            if (!isRunning) {
                                break;
                            }

                            checkDummyEndTime += mViewPortTime;
                            if (checkDummyEndTime >= programStartTime) {
                                checkDummyEndTime = programStartTime;
                                isRunning = false;
                            }
                        }
                    }
                }

                if (programEndTime > lastProgramEndTime) {
                    Pair<Long, Long> pairView = getViewPortTime(lastProgramEndTime);
                    if (pairView == null) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "pairView == null");
                        }
                        entries.add(
                                new TableEntry(
                                        channelId,
                                        program,
                                        lastProgramEndTime,
                                        programEndTime,
                                        channelLocked));
                        lastProgramEndTime = programEndTime;
                        continue;
                    }

                    long viewPortEndTime = pairView.second;

                    if (viewPortEndTime >= programEndTime) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "pairView != null including | viewPortEndTime : " + viewPortEndTime + ", programEndTime : " + programEndTime);
                        }
                        entries.add(
                                new TableEntry(
                                        channelId,
                                        program,
                                        lastProgramEndTime,
                                        programEndTime,
                                        channelLocked));
                        lastProgramEndTime = programEndTime;
                    } else {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "pairView != null including over | viewPortEndTime : " + viewPortEndTime + ", programEndTime : " + programEndTime);
                        }
                        long checkDummyEndTime = viewPortEndTime;
                        boolean isRunning = true;
                        while (true) {

                            entries.add(
                                    new TableEntry(
                                            channelId,
                                            program,
                                            lastProgramEndTime,
                                            checkDummyEndTime,
                                            channelLocked));

                            if (DEBUG) {
                                Log.d(TAG, "createProgramEntries | program : " + program.getTitle());
                                Log.d(TAG, "createProgramEntries | programEndTime : " + programEndTime);
                                Log.d(TAG, "createProgramEntries | lastProgramEndTime : " + lastProgramEndTime);
                                Log.d(TAG, "createProgramEntries | checkDummyEndTime : " + checkDummyEndTime);
                            }

                            lastProgramEndTime = checkDummyEndTime;

                            if (!isRunning) {
                                break;
                            }

                            checkDummyEndTime += mViewPortTime;
                            if (checkDummyEndTime >= programEndTime) {
                                checkDummyEndTime = programEndTime;
                                isRunning = false;
                            }
                        }
                    }
                }
            }
        }


        if (entries.size() > 1) {
            int size = entries.size();

            TableEntry lastEntry = entries.get(size - 1);


            long limitTime = mEndUtcMillis;
            long startTime = lastEntry.entryEndUtcMillis;
            if (Log.INCLUDE) {
                Log.d(TAG, "margin program startTime : " + startTime + ", limitTime : " + limitTime);
            }
            Pair<Long, Long> pairView = getViewPortTime(lastEntry.entryEndUtcMillis);
            if (pairView == null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "margin program null return");
                }
                return entries;
            }
            long programEndTime = pairView.second;
            while (true) {
                entries.add(new TableEntry(channelId, startTime, programEndTime, false));
                startTime = programEndTime;
                programEndTime += MAX_TIME;
                if (startTime >= limitTime) {
                    break;
                }
            }

//            TableEntry secondEntry = entries.get(1);
//            if (secondEntry.entryStartUtcMillis < mStartUtcMillis + FIRST_ENTRY_MIN_DURATION) {
//                // If the first entry's width doesn't have enough width, it is not good to show
//                // the first entry from UI perspective. So we clip it out.
//                entries.remove(0);
//                entries.set(
//                        0,
//                        new TableEntry(
//                                secondEntry.channelId,
//                                secondEntry.program,
//                                mStartUtcMillis,
//                                secondEntry.entryEndUtcMillis,
//                                secondEntry.mIsBlocked));
//            }
        }
        return entries;
    }

    private void notifyGenresUpdated() {
        if (Log.INCLUDE) Log.d(TAG, "notifyGenresUpdated()");

        for (Listener listener : mListeners) {
            listener.onGenresUpdated(mSelectedGenreId);
        }
    }

    private void notifyViewTypeUpdated(int ViewType) {
        if (Log.INCLUDE) Log.d(TAG, "notifyViewTypeUpdated()");

        for (Listener listener : mListeners) {
            listener.onListViewTypeChanged(ViewType, mFilteredChannels);
        }
    }

    private void notifyChannelsUpdated() {
        if (Log.INCLUDE) Log.d(TAG, "notifyChannelsUpdated()");
        for (Listener listener : mListeners) {
            listener.onChannelsUpdated();
        }
    }

    private void notifyTimeRangeUpdated(boolean keepFocus) {
        if (Log.INCLUDE) Log.d(TAG, "notifyTimeRangeUpdated()");
        for (Listener listener : mListeners) {
            listener.onTimeRangeUpdated(keepFocus);
        }
    }

    private void notifyTableEntriesUpdated() {
        if (Log.INCLUDE) Log.d(TAG, "notifyTableEntriesUpdated()");
        for (TableEntriesUpdatedListener listener : mTableEntriesUpdatedListeners) {
            listener.onTableEntriesUpdated();
        }
    }

    private void notifyTableEntryUpdated(TableEntry entry) {
        if (Log.INCLUDE) Log.d(TAG, "notifyTableEntryUpdated() entry:" + entry);
        for (TableEntryChangedListener listener : mTableEntryChangedListeners) {
            listener.onTableEntryChanged(entry);
        }
    }

    /**
     * Entry for program guide table. An "entry" can be either an actual program or a gap between
     * programs. This is needed for {@link ProgramListAdapter} because {@link
     * androidx.leanback.widget.HorizontalGridView} ignores margins between items.
     */
    static class TableEntry {
        /**
         * Channel ID which this entry is included.
         */
        long channelId;

        /**
         * Program corresponding to the entry. {@code null} means that this entry is a gap.
         */
        final Program program;


        /**
         * Start time of entry in UTC milliseconds.
         */
        final long entryStartUtcMillis;

        /**
         * End time of entry in UTC milliseconds
         */
        final long entryEndUtcMillis;

        private final boolean mIsBlocked;
        private final boolean mIsAudioChannel;

        private TableEntry(long channelId, long startUtcMillis, long endUtcMillis) {
            this(channelId, startUtcMillis, endUtcMillis, false);
        }

        private TableEntry(
                long channelId, long startUtcMillis, long endUtcMillis, boolean blocked) {
            this(channelId, null, startUtcMillis, endUtcMillis, blocked);
        }

        private TableEntry(
                long channelId, long startUtcMillis, long endUtcMillis, boolean blocked, boolean audioChannel) {
            this(channelId, null, startUtcMillis, endUtcMillis, blocked, audioChannel);
        }

        private TableEntry(
                long channelId,
                Program program,
                long entryStartUtcMillis,
                long entryEndUtcMillis,
                boolean isBlocked) {
            this(channelId, program, entryStartUtcMillis, entryEndUtcMillis, isBlocked, false);
        }

        private TableEntry(
                long channelId,
                Program program,
                long entryStartUtcMillis,
                long entryEndUtcMillis,
                boolean isBlocked,
                boolean isAudioChannel) {
            this.channelId = channelId;
            this.program = program;
            this.entryStartUtcMillis = entryStartUtcMillis;
            this.entryEndUtcMillis = entryEndUtcMillis;
            this.mIsBlocked = isBlocked;
            this.mIsAudioChannel = isAudioChannel;
        }

        /**
         * A stable id useful for {@link androidx.recyclerview.widget.RecyclerView.Adapter}.
         */
        long getId() {
            // using a negative entryEndUtcMillis keeps it from conflicting with program Id
            return program != null ? program.getId() : -entryEndUtcMillis;
        }


        /**
         * Returns true if this is a gap.
         */
        boolean isGap() {
            return !Program.isProgramValid(program);
        }

        /**
         * Returns true if this channel is blocked.
         */
        boolean isBlocked() {
            return channelId == ProgramManager.RESTRICT_CHANNEL_ID;
        }

        /**
         * Returns true if this channel is audio.
         */
        public boolean isIsAudioChannel() {
            return mIsAudioChannel;
        }

        /**
         * Returns true if this program is on the air.
         */
        boolean isCurrentProgram() {
            long current = JasmineEpgApplication.SystemClock().currentTimeMillis();
            if (program != null) {
                return program.getStartTimeUtcMillis() <= current && program.getEndTimeUtcMillis() > current;
            }
            return entryStartUtcMillis <= current && entryEndUtcMillis > current;
        }

        boolean isFutureProgram() {
            long current = JasmineEpgApplication.SystemClock().currentTimeMillis();
            if (program != null) {
                return program.getStartTimeUtcMillis() > current;
            }
            return entryStartUtcMillis > current;
        }

        boolean isPastProgram() {
            long current = JasmineEpgApplication.SystemClock().currentTimeMillis();
            if (program != null) {
                return program.getEndTimeUtcMillis() < current;
            }
            return entryEndUtcMillis < current;
        }

        /**
         * Returns if this program has the genre.
         */
        boolean hasGenre(int genreId) {
            return !isGap() && program.hasGenre(genreId);
        }

        /**
         * Returns the width of table entry, in pixels.
         */
        int getWidth() {
            return getWidth(entryStartUtcMillis, entryEndUtcMillis);
        }

        int getWidth(long entryStartUtcMillis, long entryEndUtcMillis) {
//            return Math.abs(GuideUtils.convertMillisToPixel(entryStartUtcMillis, entryEndUtcMillis));
            return GuideUtils.convertMillisToPixel(entryStartUtcMillis, entryEndUtcMillis);
        }

        public void setChannelId(long channelId) {
            this.channelId = channelId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TableEntry that = (TableEntry) o;

            if (channelId != that.channelId) return false;
            if (entryStartUtcMillis != that.entryStartUtcMillis) return false;
            return entryEndUtcMillis == that.entryEndUtcMillis;
        }

        @Override
        public int hashCode() {
            int result = (int) (channelId ^ (channelId >>> 32));
            result = 31 * result + (int) (entryStartUtcMillis ^ (entryStartUtcMillis >>> 32));
            result = 31 * result + (int) (entryEndUtcMillis ^ (entryEndUtcMillis >>> 32));
            return result;
        }

        @Override
        public String toString() {
            return "TableEntry{"
                    + "hashCode="
                    + hashCode()
                    + ", channelId="
                    + channelId
                    + ", program="
                    + program
                    + ", startTime="
                    + Utils.toTimeString(entryStartUtcMillis)
                    + ", endTimeTime="
                    + Utils.toTimeString(entryEndUtcMillis)
                    + "}";
        }


        public TableEntry clone() {
            TableEntry p = new TableEntry(this.channelId, this.program, this.entryStartUtcMillis, this.entryEndUtcMillis, this.mIsBlocked, this.mIsAudioChannel);
            return p;
        }
    }


    interface Listener {
        void onGenresUpdated(int selectedGenreId);

        void onChannelsUpdated();

        void onTimeRangeUpdated(boolean keepFocus);

        void onListViewTypeChanged(int viewType, List<Channel> channels);
    }

    interface TableEntriesUpdatedListener {
        void onTableEntriesUpdated();
    }

    interface TableEntryChangedListener {
        void onTableEntryChanged(TableEntry entry);
    }

    static class ListenerAdapter implements Listener {
        @Override
        public void onGenresUpdated(int selectedGenreId) {
        }

        @Override
        public void onChannelsUpdated() {
        }

        @Override
        public void onTimeRangeUpdated(boolean keepFocus) {
        }

        @Override
        public void onListViewTypeChanged(int viewType, List<Channel> channels) {

        }
    }
}
