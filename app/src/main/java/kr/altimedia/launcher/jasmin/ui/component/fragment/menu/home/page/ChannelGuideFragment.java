/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.page;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import kr.altimedia.launcher.jasmin.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChannelGuideFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChannelGuideFragment extends Fragment {
    public static final String CLASS_NAME = ChannelGuideFragment.class.getName();


    public ChannelGuideFragment() {
        // Required empty public constructor
    }

    public static ChannelGuideFragment newInstance() {
        ChannelGuideFragment fragment = new ChannelGuideFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_channel_guide, container, false);
    }

}
