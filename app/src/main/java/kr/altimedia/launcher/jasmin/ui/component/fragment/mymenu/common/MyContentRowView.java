/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class MyContentRowView extends LinearLayout {
    private final String TAG = MyContentRowView.class.getSimpleName();

    private HorizontalGridView mGridView;
    private OnDispatchKeyListener mOnDispatchKeyListener = null;

    public MyContentRowView(Context context) {
        this(context, null);
    }

    public MyContentRowView(Context context, int resourceId) {
        this(context, null, 0, resourceId);
    }

    public MyContentRowView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public MyContentRowView(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs, defStyle, R.layout.list_mymenu_menu);
    }

    public MyContentRowView(Context context, AttributeSet attrs, int defStyle, int resourceId) {
        super(context, attrs, defStyle);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(resourceId, this);

        mGridView = (HorizontalGridView) findViewById(R.id.row_content);
        mGridView.setHasFixedSize(false);
        mGridView.setHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.contents_horizontal_space));

        setOrientation(LinearLayout.VERTICAL);
        setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (mOnDispatchKeyListener != null) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if(mOnDispatchKeyListener.onKeyDown(event)){
                    return true;
                }
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void setOnDispatchKeyListener(OnDispatchKeyListener onDispatchKeyListener) {
        this.mOnDispatchKeyListener = onDispatchKeyListener;
    }

    public void disposeListener() {
        this.mOnDispatchKeyListener = null;
    }

    public HorizontalGridView getGridView() {
        return mGridView;
    }

    public interface OnDispatchKeyListener {
        boolean onKeyDown(KeyEvent keyCode);
    }
}
