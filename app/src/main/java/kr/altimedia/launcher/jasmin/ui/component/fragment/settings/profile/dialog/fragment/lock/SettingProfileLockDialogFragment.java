/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.lock;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseProfileDialogFragment;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment.KEY_PROFILE;

public class SettingProfileLockDialogFragment extends SettingBaseProfileDialogFragment {
    public static final String CLASS_NAME = SettingProfileLockDialogFragment.class.getName();
    private final String TAG = SettingProfileLockDialogFragment.class.getSimpleName();

    private final float DEFAULT_ALPHA = 1.0f;
    private final float DIM_ALPHA = 0.5f;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private ProfileInfo profileInfo;
    private PasswordView passwordView;
    private TextView saveButton;

    private MbsDataTask profileLockTask;
    private SettingTaskManager settingTaskManager;

    private OnLockChangeListener onLockChangeListener;

    private SettingProfileLockDialogFragment() {
    }

    public static SettingProfileLockDialogFragment newInstance(Bundle bundle) {
        SettingProfileLockDialogFragment fragment = new SettingProfileLockDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {
                onLockChangeListener.onLockChange(false);
            }
        };
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    public void setOnLockChangeListener(OnLockChangeListener onLockChangeListener) {
        this.onLockChangeListener = onLockChangeListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_setting_profile_lock, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        profileInfo = getArguments().getParcelable(KEY_PROFILE);
        settingTaskManager = new SettingTaskManager(getFragmentManager(), mTvOverlayManager);

        initView(view);
    }

    private void initView(View view) {
        initProgressbar(view);
        initProfile(view);
        initPonCode(view);
        initButtons(view);
    }

    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void showProgress() {
        mProgressBarManager.show();
    }

    private void hideProgress() {
        if (mProgressBarManager != null) {
            mProgressBarManager.hide();
        }
    }

    private void initProfile(View view) {
        ImageView pic = view.findViewById(R.id.profile_pic);
        TextView name = view.findViewById(R.id.profile_name);

        ProfileInfo profileInfo = getArguments().getParcelable(KEY_PROFILE);
        try {
            profileInfo.setProfileIcon(pic);
        }catch (Exception e){
        }
        name.setText(profileInfo.getProfileName());
    }

    private void initPonCode(View view) {
        passwordView = view.findViewById(R.id.pin_code);
        passwordView.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (isFull) {
                    setEnableButton(true);
                    saveButton.requestFocus();
                } else {
                    setEnableButton(false);
                }
            }
        });
    }

    private void setEnableButton(boolean isEnable) {
        if (isEnable != saveButton.isEnabled()) {
            float alpha = isEnable ? DEFAULT_ALPHA : DIM_ALPHA;
            saveButton.setAlpha(alpha);
            saveButton.setEnabled(isEnable);
        }
    }

    private boolean isRequest = false;
    private void initButtons(View view) {
        saveButton = view.findViewById(R.id.saveButton);
        TextView cancelButton = view.findViewById(R.id.cancelButton);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isRequest) {
                    return;
                }

                isRequest = true;
                onProfileLockCode();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLockChangeListener.onLockChange(false);
            }
        });

        setEnableButton(false);
    }

    private void onProfileLockCode() {
        profileLockTask = settingTaskManager.putProfileLock(
                profileInfo.getProfileId(), "Y", passwordView.getPassword(),
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "needLoading");
                        }

                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed");
                        }

                        isRequest = false;
                        settingTaskManager.showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSuccess, result : " + result);
                        }

                        isRequest = false;
                        if (result) {
                            onLockChangeListener.onLockChange(true);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        settingTaskManager.showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingProfileLockDialogFragment.this.getContext();
                    }
                });
    }

    @Override
    public ProfileInfo getProfileInfo() {
        return getArguments().getParcelable(KEY_PROFILE);
    }

    @Override
    protected void updateProfile(PushMessageReceiver.ProfileUpdateType type) {
        super.updateProfile(type);

        if (Log.INCLUDE) {
            Log.d(TAG, "updateProfile, type : " + type);
        }

        dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        onLockChangeListener = null;
        if (profileLockTask != null) {
            profileLockTask.cancel(true);
        }
    }

    public interface OnLockChangeListener {
        void onLockChange(boolean isLock);
    }
}
