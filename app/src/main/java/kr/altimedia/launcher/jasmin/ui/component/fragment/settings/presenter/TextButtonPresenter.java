/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;

public class TextButtonPresenter extends Presenter {
    private final float BUTTON_DEFAULT_ALPHA = 1.0f;
    private final float BUTTON_DIM_ALPHA = 0.5f;

    private int resourceId = R.layout.presenter_button;

    public TextButtonPresenter() {
    }

    private int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(getResourceId(), parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        TextButtonItem buttonItem = (TextButtonItem) item;

        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData(buttonItem);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    private class ViewHolder extends Presenter.ViewHolder {
        private TextView button;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            button = view.findViewById(R.id.button);
        }

        public void setData(TextButtonItem buttonItem) {
            String title = button.getContext().getString(buttonItem.getTitle());
            button.setText(title);

            boolean isEnable = buttonItem.isEnabled();
            setButtonEnable(isEnable);
        }

        public void setButtonEnable(boolean isEnable) {
            float alpha = isEnable ? BUTTON_DEFAULT_ALPHA : BUTTON_DIM_ALPHA;

            button.setAlpha(alpha);
            button.setEnabled(isEnable);
            button.setFocusable(isEnable);
            button.setFocusableInTouchMode(isEnable);
        }
    }

    public interface TextButtonItem {
        int getType();

        int getTitle();

        boolean isEnabled();
    }
}
