/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.data.MenuItem;

public class MenuItemPresenter extends Presenter {

    private final int ALPHA_NORMAL = 153;
    private final int ALPHA_FOCUS = 255;

    private OnKeyListener onKeyListener;
    private OnFocusListener onFocusListener;

    public MenuItemPresenter(OnKeyListener onKeyListener, OnFocusListener onFocusListener) {
        this.onKeyListener = onKeyListener;
        this.onFocusListener = onFocusListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        MenuItemViewHolder mVodItemViewHolder = new MenuItemViewHolder(inflater.inflate(R.layout.item_mymenu_menu, null, false));
        return mVodItemViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        MenuItemViewHolder itemViewHolder = (MenuItemViewHolder) viewHolder;
        MenuItem menuItem = (MenuItem) item;

        itemViewHolder.setItem(menuItem);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        MenuItemViewHolder itemViewHolder = (MenuItemViewHolder) viewHolder;
        itemViewHolder.dispose();
    }

    public OnKeyListener getOnKeyListener() {
        return onKeyListener;
    }

    public OnFocusListener getOnFocusListener() {
        return onFocusListener;
    }

    public class MenuItemViewHolder extends ViewHolder implements View.OnFocusChangeListener {
        private final String TAG = MenuItemViewHolder.class.getSimpleName();

        private MenuItem menuItem;

        private ImageView menuIcon;
        private TextView menuTitle;

        public MenuItemViewHolder(View view) {
            super(view);

            menuIcon = view.findViewById(R.id.menuIcon);
            menuTitle = view.findViewById(R.id.menuTitle);

            view.setOnFocusChangeListener(this);
        }

        public void dispose() {

        }

        public void setItem(MenuItem item) {
            menuItem = item;

            menuIcon.setImageResource(item.getIcon());
            menuIcon.setImageAlpha(ALPHA_NORMAL);
            menuTitle.setText(item.getTitle());
        }

        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if(hasFocus){
                menuTitle.setSelected(true);
                menuIcon.setImageAlpha(ALPHA_FOCUS);
            }else{
                menuTitle.setSelected(false);
                menuIcon.setImageAlpha(ALPHA_NORMAL);
            }
            if(onFocusListener != null) {
                int index = (int) view.getTag(R.id.KEY_INDEX);
                onFocusListener.onFocus(index, menuItem);
            }
        }
    }

    public interface OnKeyListener {
        boolean onKey(int keyCode, int index, Object item, ViewHolder viewHolder);
    }

    public interface OnFocusListener {
        void onFocus(int index, Object item);
    }
}