/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.widget.TextClock;

public class LocaleAwareTextClock extends TextClock {
    private static final String TAG = "LocaleAwareTextClock";

    public LocaleAwareTextClock(Context context) {
        this(context, null);
    }

    public LocaleAwareTextClock(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LocaleAwareTextClock(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Note: This assumes that locale cannot be changed while TV is showing.
        String pattern12 = DateFormat.getBestDateTimePattern(getTextLocale(), "hm MMMd");
        String pattern24 = DateFormat.getBestDateTimePattern(getTextLocale(), "Hm MMMd");
        setFormat12Hour(pattern12);
        setFormat24Hour(pattern24);
    }
}
