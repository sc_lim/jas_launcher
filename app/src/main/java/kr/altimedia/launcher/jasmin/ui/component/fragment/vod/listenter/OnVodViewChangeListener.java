/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.listenter;

import android.os.Bundle;
import android.os.Parcelable;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public interface OnVodViewChangeListener {
    void replaceDetail(Content content);

    void showPackage(String categoryId, ArrayList<Product> packageMainList);

    void watchMainVideo(Content content);

    void watchPreviewVideo(Parcelable contentData);

    void watchTrailerVideo(Parcelable contentData);

    void requestTuneVod(Bundle bundle);

    void onVodLikeChange(boolean isLike);

    TvOverlayManager getTvOverlayManager();
}
