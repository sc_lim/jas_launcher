package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public class ChannelErrorDialogFragment extends SafeDismissDialogFragment implements ChannelDataManager.Listener {
    public static final String CLASS_NAME = ChannelErrorDialogFragment.class.getName();
    private final String TAG = ChannelErrorDialogFragment.class.getSimpleName();

    private static final String KEY_FROM_CLASS_NAME = "FROM_CLASS_NAME";
    private static final String KEY_ERROR_CODE = "ERROR_CODE";
    private static final String KEY_CANCELABLE = "CANCELABLE";
    private static final String KEY_CODE = "KEY_CODE";

    private DialogInterface.OnDismissListener mOnDismissListener = null;


    private ChannelErrorDialogFragment() {
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public static ChannelErrorDialogFragment newInstance(ChannelDataManager channelDataManager, int keyCode, String fromClassName, String errorCode, BackendKnobsFlags defaultBackendKnobsFlags) {
        return newInstance(channelDataManager, keyCode, fromClassName, errorCode, defaultBackendKnobsFlags, false);
    }

    public static ChannelErrorDialogFragment newInstance(ChannelDataManager channelDataManager, int keyCode, String fromClassName, String errorCode, BackendKnobsFlags defaultBackendKnobsFlags, boolean cancelable) {

        Bundle args = new Bundle();
        args.putString(KEY_FROM_CLASS_NAME, fromClassName);
        args.putString(KEY_ERROR_CODE, errorCode);
        args.putBoolean(KEY_CANCELABLE, cancelable);
        args.putInt(KEY_CODE, keyCode);

        ChannelErrorDialogFragment fragment = new ChannelErrorDialogFragment();
        fragment.setChannelManager(channelDataManager, defaultBackendKnobsFlags);
        fragment.setArguments(args);
        return fragment;
    }

    private ChannelDataManager mChannelDataManager = null;
    private BackendKnobsFlags mDefaultBackendKnobsFlags = null;

    private void setChannelManager(ChannelDataManager channelDataManager, BackendKnobsFlags defaultBackendKnobsFlags) {
        this.mChannelDataManager = channelDataManager;
        this.mDefaultBackendKnobsFlags = defaultBackendKnobsFlags;
        this.mChannelDataManager.addListener(this);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        boolean isCancelable = bundle == null || bundle.getBoolean(KEY_CANCELABLE, true);
        Dialog dialog = generateDialog(isCancelable, savedInstanceState);
        dialog.setOnDismissListener(this);
        return dialog;
    }

    private Dialog generateDialog(boolean isCancelable, Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }


    public void setOnDismissListener(@NonNull DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_error_channel, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initVieW(view);
    }

    private int mKeyCode = 0;

    private void initVieW(View view) {
        Bundle mBundle = getArguments();
        String fromClass = mBundle.getString(KEY_FROM_CLASS_NAME);
        String errorCode = mBundle.getString(KEY_ERROR_CODE);
        mKeyCode = mBundle.getInt(KEY_CODE, 0);
        boolean isUserBaseChannelNotLoaded = mDefaultBackendKnobsFlags.isEpgReady(false);
        String description = isUserBaseChannelNotLoaded ?
                getString(R.string.error_message_lnc_0009_1) : getString(R.string.error_message_lnc_0009);

        if (Log.INCLUDE) {
            Log.d(TAG, "error code : " + errorCode + ", fromClass : " + fromClass);
        }

        TextView popupTitleView = view.findViewById(R.id.popupTitle);
        TextView errorCodeView = view.findViewById(R.id.error_code);
        TextView errorMessageView = view.findViewById(R.id.error_message);

        String popupTitle = ErrorMessageManager.getInstance().getPopUpTitle(view.getContext(), errorCode);
        if (!StringUtils.nullToEmpty(popupTitle).isEmpty()) {
            popupTitleView.setText(popupTitle);
        }
        errorCodeView.setText(errorCode);
        errorMessageView.setText(description);

    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
            mOnDismissListener = null;
        }
        if (mChannelDataManager != null) {
            mChannelDataManager.removeListener(this);
        }
    }


    @Override
    public void onLoadFinished() {
        dismiss();

        if (mDefaultBackendKnobsFlags.isEpgReady(true)) {
            startLiveTv(mKeyCode);
        }

    }

    @Override
    public void onChannelListUpdated() {
        dismiss();
        if (mDefaultBackendKnobsFlags.isEpgReady(true)) {
            startLiveTv(mKeyCode);
        }
    }

    @Override
    public void onChannelBrowsableChanged() {

    }

    @Override
    public void onUserBasedChannelListUpdated() {
        dismiss();
        if (mDefaultBackendKnobsFlags.isEpgReady(true)) {
            startLiveTv(mKeyCode);
        }
    }

    private void startLiveTv(int keyCode) {
        if (keyCode == KeyEvent.KEYCODE_GUIDE) {
            CWMPServiceProvider.getInstance().notifyHotKeyClicked(keyCode);
            Intent intent = new Intent(PlaybackUtil.ACTION_START_GUIDE);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if (keyCode == KeyEvent.KEYCODE_TV || keyCode == 0) {
            Intent intent = new Intent(PlaybackUtil.ACTION_START_LIVE);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (keyCode == KeyEvent.KEYCODE_CHANNEL_UP || keyCode == KeyEvent.KEYCODE_CHANNEL_DOWN) {
            Channel currentChannel = mChannelDataManager.getGuideChannel();
            Channel nextChannel = mChannelDataManager.getNextChannel(currentChannel, keyCode == KeyEvent.KEYCODE_CHANNEL_UP);
            if (nextChannel == null) {
                nextChannel = currentChannel;
            }
            PlaybackUtil.startLiveTvActivityWithChannel(getContext(), nextChannel.getId());
        }
    }

}
