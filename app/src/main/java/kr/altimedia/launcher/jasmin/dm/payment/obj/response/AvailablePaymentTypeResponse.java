/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class AvailablePaymentTypeResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private AvailablePaymentTypeResult availablePaymentTypeResult;

    protected AvailablePaymentTypeResponse(Parcel in) {
        super(in);
        availablePaymentTypeResult = in.readParcelable(AvailablePaymentTypeResult.class.getClassLoader());
    }

    public static final Creator<AvailablePaymentTypeResponse> CREATOR = new Creator<AvailablePaymentTypeResponse>() {
        @Override
        public AvailablePaymentTypeResponse createFromParcel(Parcel in) {
            return new AvailablePaymentTypeResponse(in);
        }

        @Override
        public AvailablePaymentTypeResponse[] newArray(int size) {
            return new AvailablePaymentTypeResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public List<PaymentType> getAvailablePaymentTypeList() {
        return availablePaymentTypeResult.getPaymentTypes();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(availablePaymentTypeResult, flags);
    }

    private static class AvailablePaymentTypeResult implements Parcelable {
        @Expose
        @SerializedName("paymentMethod")
        private List<PaymentType> paymentTypes;

        protected AvailablePaymentTypeResult(Parcel in) {
            in.readList(paymentTypes, PaymentType.class.getClassLoader());
        }

        public static final Creator<AvailablePaymentTypeResult> CREATOR = new Creator<AvailablePaymentTypeResult>() {
            @Override
            public AvailablePaymentTypeResult createFromParcel(Parcel in) {
                return new AvailablePaymentTypeResult(in);
            }

            @Override
            public AvailablePaymentTypeResult[] newArray(int size) {
                return new AvailablePaymentTypeResult[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(paymentTypes);
        }

        public List<PaymentType> getPaymentTypes() {
            return paymentTypes;
        }
    }
}
