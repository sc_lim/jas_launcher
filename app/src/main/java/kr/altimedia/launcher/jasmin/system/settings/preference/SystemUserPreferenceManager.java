/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.system.settings.preference;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by mc.kim on 11,02,2020
 */
abstract class SystemUserPreferenceManager implements SharedPreferences.OnSharedPreferenceChangeListener {
    protected final String PREF_USER = "USER_PREF";
    protected final String PREF_USER_PRIVATE = "USER_PREF_PRIVATE";

    public static final String KEY_SERIES_AUTO_PLAY = "sf.series_auto_play";
    public static final String KEY_LAST_WATCHED_CHANNEL = "sf.last_watched_channel";
    public static final String KEY_CHANNEL_PREVIEW_MAP = "sf.channel_preview_map";
    public static final String KEY_SYSTEM_UPDATE_ENABLE = "sf.system_update_enable";
    public static final String KEY_AUTO_REBOOT_ENABLE = "sf.system_auto_reboot";
    public static final String KEY_NOTIFICATIONS = "sf.notification";
    public static final String KEY_REMINDER_PROGRAM_LIST = "sf.reminder_program_list";
    public static final String KEY_PIP_INFO = "sf.pip_info";

    protected HashMap<String, String> defaultValueMap = new HashMap<>();

    private SharedPreferences mUserPreferences = null;
    private SharedPreferences mUserPrivatePreferences = null;

    protected Context context;

    protected SystemUserPreferenceManager(Context context) {
        this.context = context;
        initDefaultValue();

        mUserPreferences = context.getSharedPreferences(PREF_USER, Activity.MODE_PRIVATE);
        mUserPrivatePreferences = context.getSharedPreferences(PREF_USER_PRIVATE, Activity.MODE_PRIVATE);
        mUserPreferences.registerOnSharedPreferenceChangeListener(this);
        mUserPrivatePreferences.registerOnSharedPreferenceChangeListener(this);
    }

    private void initDefaultValue() {
        defaultValueMap.put(KEY_NOTIFICATIONS, "");
        defaultValueMap.put(KEY_SERIES_AUTO_PLAY, Boolean.toString(false));
        defaultValueMap.put(KEY_LAST_WATCHED_CHANNEL, null);
        defaultValueMap.put(KEY_CHANNEL_PREVIEW_MAP, "{}");
        defaultValueMap.put(KEY_REMINDER_PROGRAM_LIST, "[]");
        defaultValueMap.put(KEY_SYSTEM_UPDATE_ENABLE, Boolean.toString(true));
        defaultValueMap.put(KEY_AUTO_REBOOT_ENABLE, Boolean.toString(true));
        defaultValueMap.put(KEY_PIP_INFO, "{\"position\":0,\"size\":1}");
    }

    protected boolean checkDefaultValue(String key) {
        return defaultValueMap.containsKey(key);
    }

    protected String getDefaultString(String key) {
        if (!defaultValueMap.containsKey(key)) {
            return "";
        } else {
            return defaultValueMap.get(key);
        }
    }

    protected boolean getDefaultBoolean(String key) {
        if (!defaultValueMap.containsKey(key)) {
            return false;
        } else {
            return Boolean.parseBoolean(defaultValueMap.get(key));
        }
    }

    protected int getDefaultInt(String key) {
        if (!defaultValueMap.containsKey(key)) {
            return -1;
        } else {
            return Integer.parseInt(defaultValueMap.get(key));
        }
    }

    protected long getDefaultLong(String key) {
        if (!defaultValueMap.containsKey(key)) {
            return -1;
        } else {
            return Long.parseLong(defaultValueMap.get(key));
        }
    }

    private SharedPreferences getUserPreferences(String upType) {
        switch (upType) {
            case PREF_USER_PRIVATE:
                return mUserPrivatePreferences;
            default:
                return mUserPreferences;
        }
    }

    protected String getString(String upType, String key, String defaultValue) {
        SharedPreferences preferences = getUserPreferences(upType);

        return preferences.getString(key, defaultValue);
    }

    protected Set<String> getStringSet(String upType, String key) {
        SharedPreferences preferences = getUserPreferences(upType);
        Set<String> defaultSet = new HashSet<>();
        return preferences.getStringSet(key, defaultSet);
    }

    protected int getInt(String upType, String key, int defaultValue) {
        SharedPreferences preferences = getUserPreferences(upType);
        return preferences.getInt(key, defaultValue);
    }

    protected long getLong(String upType, String key, long defaultValue) {
        SharedPreferences preferences = getUserPreferences(upType);
        return preferences.getLong(key, defaultValue);
    }

    protected boolean getBoolean(String upType, String key, boolean defaultValue) {
        SharedPreferences preferences = getUserPreferences(upType);
        return preferences.getBoolean(key, defaultValue);
    }


    protected boolean putString(String upType, String key, String value) {
        SharedPreferences preferences = getUserPreferences(upType);
        SharedPreferences.Editor editor = preferences.edit().putString(key, value);
        return editor.commit();
    }

    protected boolean putInt(String upType, String key, int value) {
        SharedPreferences preferences = getUserPreferences(upType);
        SharedPreferences.Editor editor = preferences.edit().putInt(key, value);
        return editor.commit();
    }

    protected boolean putLong(String upType, String key, long value) {
        SharedPreferences preferences = getUserPreferences(upType);
        SharedPreferences.Editor editor = preferences.edit().putLong(key, value);
        return editor.commit();
    }

    protected boolean putBoolean(String upType, String key, boolean value) {
        SharedPreferences preferences = getUserPreferences(upType);
        SharedPreferences.Editor editor = preferences.edit().putBoolean(key, value);
        return editor.commit();
    }

    protected boolean putStringSet(String upType, String key, Set<String> stringSet) {
        SharedPreferences preferences = getUserPreferences(upType);
        SharedPreferences.Editor editor = preferences.edit().putStringSet(key, stringSet);
        return editor.commit();
    }


    protected boolean removeFile(String upType, String key) {
        SharedPreferences preferences = getUserPreferences(upType);
        SharedPreferences.Editor editor = preferences.edit().remove(key);
        return editor.commit();
    }


    protected void registUpChangedListener() {

    }

    abstract public String readString(String key);

    abstract public boolean readBoolean(String key);

    abstract public int readInt(String key);

    abstract public long readLong(String key);

    abstract public boolean writeString(String key, String value);

    abstract public boolean writeBoolean(String key, boolean value);

    abstract public boolean writeInt(String key, int value);

    abstract public boolean writeLong(String key, long value);

    abstract public Set<String> getStringSet(String key);

    abstract public boolean putStringSet(String key, Set<String> stringSet);

    abstract public boolean removeFile(String key);

    @Override
    abstract public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key);

}
