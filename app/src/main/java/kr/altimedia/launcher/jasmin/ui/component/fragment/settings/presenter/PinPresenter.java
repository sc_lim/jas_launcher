/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;

public class PinPresenter extends Presenter {
    private static final String TAG = PinPresenter.class.getSimpleName();

    public static final float PASSWORD_DEFAULT_ALPHA = 1.0f;
    public static final float PASSWORD_DIM_ALPHA = 0.8f;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_pin, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        PinButtonItem buttonItem = (PinButtonItem) item;
        vh.onBindViewHolder(buttonItem);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
    }

    public class ViewHolder extends Presenter.ViewHolder {
        private PasswordView password;
        private TextView passwordName;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            password = view.findViewById(R.id.pin);
            passwordName = view.findViewById(R.id.password_name);
        }

        public void onBindViewHolder(PinButtonItem item) {
            boolean isEnable = item.isEnable();
            setEnablePassword(isEnable);

            Context context = view.getContext();
            passwordName.setText(context.getString(item.getTitle()));
            password.forceInputPassword(item.getPassword());
        }

        public void setOnKeyListener(View.OnKeyListener onKeyListener) {
            password.setOnKeyListener(onKeyListener);
        }

        public void setOnPasswordComplete(PasswordView.OnPasswordComplete onPasswordComplete) {
            password.setOnPasswordComplete(onPasswordComplete);
        }

        public void setEnablePassword(boolean isEnable) {
            float dimAlpha = isEnable ? PASSWORD_DEFAULT_ALPHA : PASSWORD_DIM_ALPHA;
            password.setEnabled(isEnable, dimAlpha);
        }

        public boolean isFullPassword() {
            return password.isFull();
        }

        public void clearPassword() {
            password.clear();
        }
    }

    public interface PinButtonItem {
        int getType();

        int getTitle();

        String getPassword();

        boolean isEnable();

        void setEnable(boolean isEnable);

        void setPassword(String password);
    }
}
