/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonListInfo;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodDetailsDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter.VodSeasonPresenter;
import kr.altimedia.launcher.jasmin.ui.view.drawer.EdgeDrawerLayout;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class VodSeasonSelectDialogFragment extends SafeDismissDialogFragment
        implements View.OnKeyListener, EdgeDrawerLayout.DrawerListener {
    public static final String CLASS_NAME = VodSeasonSelectDialogFragment.class.getName();
    private static final String TAG = VodSeasonSelectDialogFragment.class.getSimpleName();

    private static final String KEY_SEASON_LIST_INFO = "SEASON_LISt";

    private final int VISIBLE_COUNT = 8;

    private String currentSeasonId;

    private VerticalGridView gridView;
    private SeasonItemBridgeAdapter itemBridgeAdapter;

    private EdgeDrawerLayout mDrawer = null;
    private LinearLayout mPanel = null;

    private ImageIndicator topIndicator;
    private ImageIndicator bottomIndicator;

    public VodSeasonSelectDialogFragment() {
    }

    public static VodSeasonSelectDialogFragment newInstance(SeasonListInfo seasonListInfo) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_SEASON_LIST_INFO, seasonListInfo);

        VodSeasonSelectDialogFragment fragment = new VodSeasonSelectDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {
                mDrawer.closeDrawer(mPanel, true);
            }
        };
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_vod_season_select, container, false);
        initDrawer(view);

        return view;
    }

    private void initDrawer(View view) {
        mDrawer = view.findViewById(R.id.drawer_layout);
        mPanel = view.findViewById(R.id.layout);
        mDrawer.addDrawerListener(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                mDrawer.openDrawer(mPanel, true);
            }
        }, 150);

    }

    private void initView(View view) {
        SeasonListInfo seasonListInfo = getArguments().getParcelable(KEY_SEASON_LIST_INFO);
        currentSeasonId = seasonListInfo.getSeasonId();

        ((TextView) view.findViewById(R.id.title)).setText(seasonListInfo.getMasterSeriesTitle());
        topIndicator = view.findViewById(R.id.top_indicator);
        bottomIndicator = view.findViewById(R.id.bottom_indicator);
        initGirdView(view, seasonListInfo);
    }

    private void initGirdView(View view, SeasonListInfo seasonListInfo) {
        List<SeasonInfo> seasonList = seasonListInfo.getSeasonList();

        gridView = view.findViewById(R.id.grid_view);
        topIndicator.initIndicator(seasonList.size(), VISIBLE_COUNT);
        bottomIndicator.initIndicator(seasonList.size(), VISIBLE_COUNT);

        VodSeasonPresenter vodSeasonPresenter = new VodSeasonPresenter();
        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter(vodSeasonPresenter);
        arrayObjectAdapter.addAll(0, seasonList);

        itemBridgeAdapter = new SeasonItemBridgeAdapter(arrayObjectAdapter, seasonListInfo.getSeasonId(), gridView, this);

        int initPosition = 0;
        for (int i = 0; i < seasonList.size(); i++) {
            SeasonInfo seasonInfo = seasonList.get(i);
            if (seasonInfo.getSeasonID().equals(currentSeasonId)) {
                initPosition = i;
                break;
            }
        }
        gridView.setSelectedPosition(initPosition);
        updateIndicator(initPosition);

        gridView.setAdapter(itemBridgeAdapter);
        gridView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int focus = gridView.getChildPosition(recyclerView.getFocusedChild());
                updateIndicator(focus);
            }
        });
    }

    private void updateIndicator(int focusedIndex) {
        topIndicator.updateArrow(focusedIndex);
        bottomIndicator.updateArrow(focusedIndex);
    }

    private boolean isRequest = false;

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        SeasonItemBridgeAdapter adapter = (SeasonItemBridgeAdapter) gridView.getAdapter();
        ArrayObjectAdapter arrayObjectAdapter = (ArrayObjectAdapter) adapter.getAdapter();
        int lastIndex = arrayObjectAdapter.size() - 1;
        int index = gridView.getSelectedPosition();

        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (mDrawer.isActivated()) {
                    return true;
                }

                if (isRequest) {
                    return true;
                }

                isRequest = true;
                SeasonInfo seasonInfo = (SeasonInfo) arrayObjectAdapter.get(index);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onKey, currentSeasonId : " + currentSeasonId + ", seasonInfo : " + seasonInfo);
                }

                if (!seasonInfo.getSeasonID().equalsIgnoreCase(currentSeasonId)) {
                    VodDetailsDialogFragment vodDetailsDialogFragment = (VodDetailsDialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag(VodDetailsDialogFragment.CLASS_NAME);
                    SeriesVodFragment parentFragment = (SeriesVodFragment) vodDetailsDialogFragment.getChildFragmentManager().findFragmentByTag(SeriesVodFragment.CLASS_NAME);
                    parentFragment.replaceSeasonDetail(seasonInfo);
                }

                dismiss();
                return true;
            case KeyEvent.KEYCODE_DPAD_UP:
                if (index == 0) {
                    gridView.setSelectedPosition(lastIndex);
                    updateIndicator(lastIndex);
                    return true;
                }

                return false;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                if (index == lastIndex) {
                    gridView.setSelectedPosition(0);
                    updateIndicator(0);
                    return true;
                }

                return false;
        }

        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mDrawer.removeDrawerListener(this);
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onDrawerOpened");
        }
    }

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onDrawerClosed");
        }
        dismiss();
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
