/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.widget.SearchOrbView;
import androidx.leanback.widget.TitleViewAdapter;

import kr.altimedia.launcher.jasmin.R;


public class MenuBrandedFragment extends Fragment {
    private static final String TITLE_SHOW = "titleShow";
    private boolean mShowingTitle = true;
    private CharSequence mTitle;
    private Drawable mBadgeDrawable;
    private View mTitleView;
    private TitleViewAdapter mTitleViewAdapter;
    private SearchOrbView.Colors mSearchAffordanceColors;
    private boolean mSearchAffordanceColorSet;
    private View.OnClickListener mExternalOnSearchClickedListener;
    private TitleHelper mTitleHelper;

    public MenuBrandedFragment() {
    }

    public View onInflateTitleView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        TypedValue typedValue = new TypedValue();
        boolean found = parent.getContext().getTheme().resolveAttribute(R.attr.browseTitleViewLayout, typedValue, true);
        return inflater.inflate(found ? typedValue.resourceId : R.layout.view_title, parent, false);
//        return inflater.inflate( R.layout.view_title, parent, false);
    }

    public void installTitleView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View titleLayoutRoot = this.onInflateTitleView(inflater, parent, savedInstanceState);
        if (titleLayoutRoot != null) {
            parent.addView(titleLayoutRoot);
            this.setTitleView(titleLayoutRoot.findViewById(R.id.browse_title_group));
        } else {
            this.setTitleView((View) null);
        }

    }

    public void setTitleView(View titleView) {
        this.mTitleView = titleView;
        if (this.mTitleView == null) {
            this.mTitleViewAdapter = null;
            this.mTitleHelper = null;
        } else {
            this.mTitleViewAdapter = ((TitleViewAdapter.Provider) this.mTitleView).getTitleViewAdapter();
            this.mTitleViewAdapter.setTitle(this.mTitle);
            this.mTitleViewAdapter.setBadgeDrawable(this.mBadgeDrawable);
            if (this.mSearchAffordanceColorSet) {
                this.mTitleViewAdapter.setSearchAffordanceColors(this.mSearchAffordanceColors);
            }

            if (this.mExternalOnSearchClickedListener != null) {
                this.setOnSearchClickedListener(this.mExternalOnSearchClickedListener);
            }

            if (this.getView() instanceof ViewGroup) {
                this.mTitleHelper = new TitleHelper((ViewGroup) this.getView(), this.mTitleView);
            }
        }

    }

    public View getTitleView() {
        return this.mTitleView;
    }

    public TitleViewAdapter getTitleViewAdapter() {
        return this.mTitleViewAdapter;
    }

    public TitleHelper getTitleHelper() {
        return this.mTitleHelper;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("titleShow", this.mShowingTitle);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            this.mShowingTitle = savedInstanceState.getBoolean("titleShow");
        }

        if (this.mTitleView != null && view instanceof ViewGroup) {
            this.mTitleHelper = new TitleHelper((ViewGroup) view, this.mTitleView);
            this.mTitleHelper.showTitle(this.mShowingTitle);
        }

    }

    public void onDestroyView() {
        super.onDestroyView();
        this.mTitleHelper = null;
    }

    public void showTitle(boolean show) {
        if (show != this.mShowingTitle) {
            this.mShowingTitle = show;
            if (this.mTitleHelper != null) {
                this.mTitleHelper.showTitle(show);
            }

        }
    }

    public void showTitle(int flags) {
        if (this.mTitleViewAdapter != null) {
            this.mTitleViewAdapter.updateComponentsVisibility(flags);
        }

        this.showTitle(true);
    }

    public void setBadgeDrawable(Drawable drawable) {
        if (this.mBadgeDrawable != drawable) {
            this.mBadgeDrawable = drawable;
            if (this.mTitleViewAdapter != null) {
                this.mTitleViewAdapter.setBadgeDrawable(drawable);
            }
        }

    }

    public Drawable getBadgeDrawable() {
        return this.mBadgeDrawable;
    }

    public void setTitle(CharSequence title) {
        this.mTitle = title;
        if (this.mTitleViewAdapter != null) {
            this.mTitleViewAdapter.setTitle(title);
        }

    }

    public CharSequence getTitle() {
        return this.mTitle;
    }

    public void setOnSearchClickedListener(View.OnClickListener listener) {
        this.mExternalOnSearchClickedListener = listener;
        if (this.mTitleViewAdapter != null) {
            this.mTitleViewAdapter.setOnSearchClickedListener(listener);
        }

    }

    public void setSearchAffordanceColors(SearchOrbView.Colors colors) {
        this.mSearchAffordanceColors = colors;
        this.mSearchAffordanceColorSet = true;
        if (this.mTitleViewAdapter != null) {
            this.mTitleViewAdapter.setSearchAffordanceColors(this.mSearchAffordanceColors);
        }

    }

    public SearchOrbView.Colors getSearchAffordanceColors() {
        if (this.mSearchAffordanceColorSet) {
            return this.mSearchAffordanceColors;
        } else if (this.mTitleViewAdapter == null) {
            throw new IllegalStateException("Fragment views not yet created");
        } else {
            return this.mTitleViewAdapter.getSearchAffordanceColors();
        }
    }

    public void setSearchAffordanceColor(int color) {
        this.setSearchAffordanceColors(new SearchOrbView.Colors(color));
    }

    public int getSearchAffordanceColor() {
        return this.getSearchAffordanceColors().color;
    }

    public void onStart() {
        super.onStart();
        if (this.mTitleViewAdapter != null) {
            this.showTitle(this.mShowingTitle);
            this.mTitleViewAdapter.setAnimationEnabled(true);
        }

    }

    public void onPause() {
        if (this.mTitleViewAdapter != null) {
            this.mTitleViewAdapter.setAnimationEnabled(false);
        }

        super.onPause();
    }

    public void onResume() {
        super.onResume();
        if (this.mTitleViewAdapter != null) {
            this.mTitleViewAdapter.setAnimationEnabled(true);
        }

    }

    public final boolean isShowingTitle() {
        return this.mShowingTitle;
    }
}
