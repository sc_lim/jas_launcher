/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.single;

import android.view.View;

import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.BaseShadowHelper;

public class SingleDetailShadowHelper extends BaseShadowHelper {
    private final int topShadowResId;
    private float mTargetLimitAlpha = 1f;
    private final View bottomShadow;
    private View mTopLayer = null;

    public SingleDetailShadowHelper(int topShadowResId, float targetLimitAlpha, View bottomShadow) {
        super();

        this.topShadowResId = topShadowResId;
        this.mTargetLimitAlpha = targetLimitAlpha;
        this.bottomShadow = bottomShadow;
    }

    @Override
    public void onScrolledOffsetCallback(int offset, int remainScroll, int totalScroll) {
        float f_offset = (float) offset;
        //            float f_remainScroll = (float) remainScroll;
        float total = (float) totalScroll;

        if (total == 0) {
            return;
        }
        float alphaOffset = f_offset / total;
        if (parentView != null && mTopLayer == null) {
            mTopLayer = parentView.findViewById(topShadowResId);
        }
        float resultOffset = 1f - alphaOffset;
        if (mTopLayer != null && resultOffset >= mTargetLimitAlpha) {
            mTopLayer.setAlpha(resultOffset);
        }
        bottomShadow.setAlpha(resultOffset);
    }
}
