/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;

/**
 * Created by mc.kim on 11,05,2020
 */
public class BlockedChannelResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private BlockedChannelResult blockedChannelResult;

    protected BlockedChannelResponse(Parcel in) {
        super(in);
        blockedChannelResult = in.readTypedObject(BlockedChannelResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(blockedChannelResult, flags);
    }

    public List<String> getBlockedChannelResult() {
        return blockedChannelResult.channelIdList;
    }

    @Override
    public String toString() {
        return "BlockedChannelResponse{" +
                "blockedChannelResult=" + blockedChannelResult +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BlockedChannelResponse> CREATOR = new Creator<BlockedChannelResponse>() {
        @Override
        public BlockedChannelResponse createFromParcel(Parcel in) {
            return new BlockedChannelResponse(in);
        }

        @Override
        public BlockedChannelResponse[] newArray(int size) {
            return new BlockedChannelResponse[size];
        }
    };

    private static class BlockedChannelResult implements Parcelable {
        @Expose
        @SerializedName("channelId")
        private List<String> channelIdList;

        protected BlockedChannelResult(Parcel in) {
            channelIdList = in.readArrayList(Long.class.getClassLoader());
        }

        public static final Creator<BlockedChannelResult> CREATOR = new Creator<BlockedChannelResult>() {
            @Override
            public BlockedChannelResult createFromParcel(Parcel in) {
                return new BlockedChannelResult(in);
            }

            @Override
            public BlockedChannelResult[] newArray(int size) {
                return new BlockedChannelResult[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(channelIdList);
        }

        @Override
        public String toString() {
            return "BlockedChannelResult{" +
                    "channelIdList=" + channelIdList +
                    '}';
        }
    }
}
