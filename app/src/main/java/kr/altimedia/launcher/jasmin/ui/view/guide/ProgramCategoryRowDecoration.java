package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;

public class ProgramCategoryRowDecoration extends RecyclerView.ItemDecoration {
    private Drawable divider;
    private Context mContext;

    public ProgramCategoryRowDecoration(Context mContext) {
        this.mContext = mContext;
    }

    public void setDrawable(Drawable divider) {
        this.divider = divider;
    }

    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth();

        Paint paint = new Paint();
        paint.setColor(mContext.getResources().getColor(R.color.color_0DFFFFFF, null));

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount - 1; i++) {
            View child = parent.getChildAt(i);

            int top = child.getBottom();
            int bottom = top + divider.getIntrinsicHeight();

            divider.setBounds(left, top, right, bottom);
            divider.draw(c);
        }
    }
}
