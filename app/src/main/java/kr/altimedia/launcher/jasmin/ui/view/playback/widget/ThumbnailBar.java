/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;

import com.altimedia.util.Log;

import java.util.Arrays;

import androidx.leanback.widget.SinglePresenterSelector;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.playback.ThumbnailObjectAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.playback.ThumbnailPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingHorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.GridLayoutManager;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;

/**
 * Created by mc.kim on 17,02,2020
 */
public class ThumbnailBar extends PagingHorizontalGridView implements View.OnFocusChangeListener {
    private final String TAG = ThumbnailBar.class.getSimpleName();
    private final ItemBridgeAdapter mItemBridgeAdapter;
    private final int THUMBNAIL_VISIBLE_COUNT = 6;
    public ThumbnailBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ThumbnailBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initHorizontal(THUMBNAIL_VISIBLE_COUNT);
        mItemBridgeAdapter = new ItemBridgeAdapter();
        setAdapter(mItemBridgeAdapter);
        setHorizontalSpacing(context.getResources().
                getDimensionPixelSize(R.dimen.playback_playback_transport_thumbs_margin));
//        setFocusScrollStrategy(FOCUS_SCROLL_PAGE);
        setOnFocusChangeListener(this);



    }

    private ThumbnailObjectAdapter thumbnailObjectAdapter = null;
    private OnThumbnailItemActionListener mOnThumbnailItemActionListener = null;
    private VideoPlaybackSeekDataProvider mSeekDataProvider = null;
    public void setSeekDataProvider(VideoPlaybackSeekDataProvider seekDataProvider
            , OnThumbnailItemActionListener onThumbnailItemActionListener) {
        mOnThumbnailItemActionListener = onThumbnailItemActionListener;
        mSeekDataProvider = seekDataProvider;
        RowPresenter thumbnailPresenter
                = new ThumbnailPresenter(seekDataProvider, onThumbnailItemActionListener);
        thumbnailObjectAdapter = new ThumbnailObjectAdapter(thumbnailPresenter, seekDataProvider);
        thumbnailObjectAdapter.setPresenterSelector(new SinglePresenterSelector(thumbnailPresenter));
        mItemBridgeAdapter.setAdapter(thumbnailObjectAdapter);
        mItemBridgeAdapter.setPresenter(new SinglePresenterSelector(thumbnailPresenter));
        mItemBridgeAdapter.notifyDataSetChanged();
        if (mSeekDataProvider != null) {
            mSeekDataProvider.setThumbnailBar(this, mItemBridgeAdapter);
            int length = mSeekDataProvider.getSeekPositions() != null ? mSeekDataProvider.getSeekPositions().length : 0;
            setItemViewCacheSize(length);
        }

    }

    public int getSeekableSize() {
        return thumbnailObjectAdapter.getSeekableSize();
    }

    public int getTotalSize() {
        return thumbnailObjectAdapter.size();
    }

    public int initCurrentPosition(long currentTimeMillis) {
        int position = findIndex(currentTimeMillis);
        this.post(new Runnable() {
            @Override
            public void run() {
                int selectionPosition = position;
                if (selectionPosition > getSeekableSize()) {
                    selectionPosition = 0;
                }
                smoothScrollToPosition(selectionPosition);
                requestFocus();
            }
        });
        return position;
    }

    private int findIndex(long timeMs) {
        if (timeMs == -1) {
            return 0;
        }
        if (mSeekDataProvider == null) {
            return 0;
        }

        int currentTimeSec = (int) (timeMs / 1000);
        long[] positions = mSeekDataProvider.getSeekPositions();
        int[] seekTimeSec = new int[positions.length];
        for (int i = 0; i < seekTimeSec.length; i++) {
            seekTimeSec[i] = (int) (positions[i] / 1000);
        }
        int index = Arrays.binarySearch(seekTimeSec, 0, seekTimeSec.length, currentTimeSec);
        if (index < 0) {
            index = 0;
            for (long time : positions) {
                if (timeMs > time) {
                    index++;
                    continue;
                }
                break;
            }
        }
        final int selectedIndex = index;
        return selectedIndex - 1;
    }

    public void scrollCurrentPosition(long currentTimeMillis, boolean force) {
        int position = findIndex(currentTimeMillis);
        int selectedPosition = getSelectedPosition();

        if (Log.INCLUDE) {
            Log.d(TAG, "scrollCurrentPosition |time : " + currentTimeMillis + ", index : " + position + ", selectedPosition : " + selectedPosition);
        }
        if (position == selectedPosition && !force) {
            if (Log.INCLUDE) {
                Log.d(TAG, "position == selectedPosition return");
            }
            return;
        }
        this.post(new Runnable() {
            @Override
            public void run() {
                int page = position == 0 ? 0 : position / THUMBNAIL_VISIBLE_COUNT;
                if (Log.INCLUDE) {
                    Log.d(TAG, "scrollCurrentPosition | page : " + page);
                    Log.d(TAG, "scrollCurrentPosition | page * THUMBNAIL_VISIBLE_COUNT : " + page * THUMBNAIL_VISIBLE_COUNT);
                }
                int currentPage = getSelectedPosition() == 0 ? 0 : getSelectedPosition() / THUMBNAIL_VISIBLE_COUNT;
                if (page == currentPage && !force) {
                    return;
                }
                smoothScrollToPosition(position);
                GridLayoutManager gridLayoutManager = (GridLayoutManager) getLayoutManager();
                View view = gridLayoutManager.findViewByPosition(position);
                updateAlign(view, position);
            }
        });
    }

    public void clearThumbBitmaps() {
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();

        if (mSeekDataProvider == null) {
            return false;
        }

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mOnThumbnailItemActionListener != null) {
                mOnThumbnailItemActionListener.onBackPressed();
                return true;
            }
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
            int position = getSelectedPosition();
            if (position == 0 && mSeekDataProvider != null && event.getRepeatCount() < 1) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    mSeekDataProvider.notifyLoadPrevProgram(this, mItemBridgeAdapter);
                }
                return true;
            }

        } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
            int position = getSelectedPosition();
            if (Log.INCLUDE) {
                Log.d(TAG, "position : " + position);
                Log.d(TAG, "getTotalSize() : " + getTotalSize());
                Log.d(TAG, "position : " + position);
            }
            if (position == mSeekDataProvider.getSeekPositions().length - 1 && mSeekDataProvider != null && event.getRepeatCount() < 1) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    mSeekDataProvider.notifyLoadNextProgram(this, mItemBridgeAdapter);
                }
                return true;
            }
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
            mSeekDataProvider.notifyLoadPlayingProgram(this, mItemBridgeAdapter);
        }
        return super.dispatchKeyEvent(event);
    }

    public interface OnThumbnailItemActionListener {
        void onItemSelected(long time);

        void onItemClicked(long time);

        void onBackPressed();
    }

    @Override
    public void requestChildFocus(View child, View focused) {
        super.requestChildFocus(child, focused);
        if (Log.INCLUDE) {
            Log.d(TAG, "requestChildFocus : " + focused);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onFocusChange : " + hasFocus);
        }

    }


    @Override
    protected void onDetachedFromWindow() {

        super.onDetachedFromWindow();
        if (mSeekDataProvider != null) {
            mSeekDataProvider.detach();
        }
    }
}
