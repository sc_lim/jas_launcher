/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.system.ota;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.PowerManager;
import android.view.KeyEvent;

import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.x_ttbb_diagmon.forceapkupdate.Lists;
import com.altimedia.update.apk.RequestApkDownloadCallback;
import com.altimedia.update.apk.RequestApkUpdateCallback;
import com.altimedia.update.apk.RequestApkUpdateInfo;
import com.altimedia.update.apk.RequestApkUpdateInfoCallback;
import com.altimedia.update.apk.UpdateApkData;
import com.altimedia.update.fw.RequestFwDownloadCallback;
import com.altimedia.update.fw.RequestFwUpdateCallback;
import com.altimedia.update.fw.RequestFwUpdateInfo;
import com.altimedia.update.fw.RequestFwUpdateInfoCallback;
import com.altimedia.update.fw.UpdateFwData;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPCPEManager;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPSystemUpdateManager;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

public class SystemUpdateManager {

    private static final SystemUpdateManager instance = new SystemUpdateManager();
    private final String TAG = SystemUpdateManager.class.getSimpleName();
    private SystemUpdateScheduler systemUpdateScheduler = null;
    private CWMPSystemUpdateManager updateManager = null;
    private PowerStatusReceiver powerStatusReceiver;

    public static final int TARGET_FW = 0;
    public static final int TARGET_APK = 1;

    public static final int TYPE_REQUEST = 0;
    public static final int TYPE_PROGRESS = 1;
    public static final int TYPE_COMPLETE = 2;
    public static final int TYPE_ERROR = 3;

    private final String FORCE_APK_STATUS_NO_DATA = "NoData";
    private final String FORCE_APK_STATUS_COMPLETE = "Complete";
    private final String FORCE_APK_STATUS_FAIL = "Fail";

    private long AUTO_HIDE_10 = 10 * 1000;
    private long AUTO_HIDE_30 = 30 * 1000;
    private long NO_HIDE = 0;

    private UpdateFwData fwData;
    private Map<String, UpdateApkData> apkDataMap = new HashMap<>();
    private ArrayList<String> apkKeyList = new ArrayList<>();
    private boolean isForceApkUpdate = false;

    private Context context;
    private String LAUNCHER_PKG_NAME = "kr.altimedia.launcher.jasmin";
    private boolean onDownloading = false;

    private SystemUpdateDialog systemDialog;

    private int[] HIDDEN_KEYS = new int[]{KeyEvent.KEYCODE_MEDIA_REWIND, KeyEvent.KEYCODE_MEDIA_REWIND, KeyEvent.KEYCODE_MEDIA_REWIND, KeyEvent.KEYCODE_MEDIA_REWIND, KeyEvent.KEYCODE_MEDIA_REWIND, KeyEvent.KEYCODE_1};
    private int checkHiddenIndex = 0;

    private boolean isInitialized = false;

    private Handler mCheckHandler = new Handler();
    private Runnable mCheckRunnable = new Runnable() {
        public void run() {
            if (Log.INCLUDE) {
                Log.d(TAG, "checkSoftwareUpdate");
            }

            if(!isSystemUpdateEnabled()){
                if (Log.INCLUDE) {
                    Log.d(TAG, "checkSoftwareUpdate: return since system update disabled");
                }
                return;
            }
            if(isOnDownloading()){
                if (Log.INCLUDE) {
                    Log.d(TAG, "checkSoftwareUpdate: return since downloading");
                }
                return;
            }

            requestFwUpdateInfo();
        }
    };

    public static SystemUpdateManager getInstance() {
        return instance;
    }

    public void init(Context context){
        if (Log.INCLUDE) {
            Log.d(TAG, "init");
        }
        if (isInitialized) {
            return;
        }
        this.isInitialized = true;

        setDownloadStatus(false);

        this.context = context;
        LAUNCHER_PKG_NAME = context.getPackageName();
        if (Log.INCLUDE) {
            Log.d(TAG, "init: LAUNCHER_PKG_NAME=" + LAUNCHER_PKG_NAME);
        }
        this.updateManager = CWMPSystemUpdateManager.getInstance();

        this.systemUpdateScheduler = new SystemUpdateScheduler();
        this.systemUpdateScheduler.start(true);

        this.powerStatusReceiver = new PowerStatusReceiver();
        startPowerStatusListener();

        checkHiddenIndex = 0;

        checkSoftwareUpdate();
    }

    /**
     * checkSoftwareUpdate (ALL)
     */
    public void checkSoftwareUpdate() {
        mCheckHandler.removeCallbacks(mCheckRunnable);
        mCheckHandler.postDelayed(mCheckRunnable, 1000);
    }

    public void restartUpdateScheduler() {
        boolean isScreenOn = isScreenOn(context);

        if (Log.INCLUDE) {
            Log.d(TAG, "restartUpdateScheduler, isScreenOn=" + isScreenOn);
        }

        if (isScreenOn) {
            systemUpdateScheduler.stop();
            systemUpdateScheduler.start(true);
        }
    }

    public boolean isScreenOn(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        return pm.isInteractive();
    }

    /**
     * checkSoftwareUpdate (Specific)
     *
     * @param target                TARGET_FW = 0, TARGET_APK = 1
     * @param onSWUpdateCheckResult
     */
    public Object checkSoftwareUpdate(int target, OnSWUpdateCheckResult onSWUpdateCheckResult) {
        if (Log.INCLUDE) {
            Log.d(TAG, "checkSoftwareUpdate: target=" + target);
        }
        if (target == TARGET_FW) {
            return CWMPSystemUpdateManager.getInstance().requestFwUpdateInfo(new RequestFwUpdateInfoCallback() {
                @Override
                public void onSuccess(UpdateFwData updateFwData) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "checkFwUpdate: onSuccess: url=" + updateFwData.getUrl() + ", version=" + updateFwData.getNewVersion());
                    }
                    fwData = updateFwData;
                    if (onSWUpdateCheckResult != null) {
                        onSWUpdateCheckResult.onResult(true);
                    }
                }

                @Override
                public void onFail(String message) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "checkFwUpdate: onFail: " + message);
                    }
                    fwData = null;
                    if (onSWUpdateCheckResult != null) {
                        onSWUpdateCheckResult.onResult(false);
                    }
                }

                @Override
                public void onError(String errorCode, String errorMessage) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "checkFwUpdate: onError: " + errorCode+", "+errorMessage);
                    }
                    fwData = null;
                    if (onSWUpdateCheckResult != null) {
                        onSWUpdateCheckResult.onError();

                        showErrorDialog(TARGET_FW, TYPE_ERROR, NO_HIDE, errorCode, errorMessage);
                    }
                }
            });

        } else if(target == TARGET_APK) {
            isForceApkUpdate = false;
            return CWMPSystemUpdateManager.getInstance().requestApkUpdateInfo(new RequestApkUpdateInfoCallback() {
                @Override
                public void onSuccess(List<UpdateApkData> updateApkDataList) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "checkApkUpdate: onSuccess");
                    }
                    if (updateApkDataList != null && updateApkDataList.size() > 0) {
                        makeApkDataMap(updateApkDataList);

                        if (apkKeyList.size() > 0) {
                            if (onSWUpdateCheckResult != null) {
                                onSWUpdateCheckResult.onResult(true);
                            }
                            return;
                        }
                    }
                    if (onSWUpdateCheckResult != null) {
                        onSWUpdateCheckResult.onResult(false);
                    }
                }

                @Override
                public void onFail(String message) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "checkApkUpdate: onFail: "+message);
                    }
                    if (onSWUpdateCheckResult != null) {
                        onSWUpdateCheckResult.onResult(false);
                    }
                }

                @Override
                public void onError(String errorCode, String errorMessage) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "checkApkUpdate: onError: " + errorCode+", "+errorMessage);
                    }
                    if (onSWUpdateCheckResult != null) {
                        onSWUpdateCheckResult.onError();

                        showErrorDialog(TARGET_APK, TYPE_ERROR, NO_HIDE, errorCode, errorMessage);
                    }
                }
            });
        }else{
            if (onSWUpdateCheckResult != null) {
                onSWUpdateCheckResult.onResult(false);
            }
        }

        return null;
    }

    public void cancelRequestFwUpdateInfo(RequestFwUpdateInfo mRequestFwUpdateInfo) {
        if (Log.INCLUDE) {
            Log.d(TAG, "cancelRequestFwUpdateInfo, mRequestFwUpdateInfo : " + mRequestFwUpdateInfo);
        }

        CWMPSystemUpdateManager.getInstance().cancelRequestFwUpdateInfo(mRequestFwUpdateInfo);
    }

    public void cancelRequestApkUpdateInfo(RequestApkUpdateInfo mRequestApkUpdateInfo) {
        if (Log.INCLUDE) {
            Log.d(TAG, "cancelRequestApkUpdateInfo, mRequestApkUpdateInfo : " + mRequestApkUpdateInfo);
        }

        CWMPSystemUpdateManager.getInstance().cancelRequestApkUpdateInfo(mRequestApkUpdateInfo);
    }

    /**
     * 주어진 fwData로 FW업그레이드를 시작합니다.
     */
    public void startFwUpgrade(){
        if (Log.INCLUDE) {
            Log.d(TAG, "startFwUpgrade");
        }
        if(isOnDownloading()){
            if (Log.INCLUDE) {
                Log.d(TAG, "startApkUpgrade: return since downloading");
            }
            return;
        }

        if(fwData != null) {
            requestFwDownload(fwData);
        }
    }

    public void startFwUpgrade(String url) {
        if (Log.INCLUDE) {
            Log.d(TAG, "startFwUpgrade: url="+url);
        }

        if(!isSystemUpdateEnabled()){
            if (Log.INCLUDE) {
                Log.d(TAG, "startFwUpgrade: return since system update disabled");
            }
            return;
        }
        if(isOnDownloading()){
            if (Log.INCLUDE) {
                Log.d(TAG, "startFwUpgrade: return since downloading");
            }
            return;
        }

        requestFwDownloadForce(url);
    }

    /**
     * 주어진 apkKeyList로 APK업그레이드를 시작합니다.
     */
    public void startApkUpgrade(){
        if (Log.INCLUDE) {
            Log.d(TAG, "startApkUpgrade");
        }

        if(isOnDownloading()){
            if (Log.INCLUDE) {
                Log.d(TAG, "startApkUpgrade: return since downloading");
            }
            return;
        }

        if (apkKeyList != null && apkKeyList.size() > 0) {
            String key = getCurrentApkKey();
            requestApkDownload(key);
        }
    }

    public void startApkUpgrade(String forceUpdate) {
        if (Log.INCLUDE) {
            Log.d(TAG, "startApkUpgrade: forceUpdate="+forceUpdate);
        }

        if(!isSystemUpdateEnabled()){
            if (Log.INCLUDE) {
                Log.d(TAG, "startApkUpgrade: return since system update disabled");
            }
            return;
        }
        if(isOnDownloading()){
            if (Log.INCLUDE) {
                Log.d(TAG, "startApkUpgrade: return since downloading");
            }
            return;
        }

        try{
            List<UpdateApkData> updateApkDataList = new ArrayList<>();
            UpdateApkData updateApkData = null;
            Lists lists = CWMPCPEManager.getInstance().getRoot().x_ttbb_diagMon.forceApkUpdate.lists;
            for (int listIndex : lists.getListIndexes()) {
                com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.x_ttbb_diagmon.forceapkupdate.List list =  lists.getList(listIndex);
                if(list != null) {
                    String packageName = list.PackageId.getValue();
                    String version = list.Version.getValue();
                    String url = list.Url.getValue();
                    updateApkData = new UpdateApkData(packageName, version, url);
                    updateApkDataList.add(updateApkData);
                }
            }

            if(updateApkDataList != null && updateApkDataList.size() > 0) {
                makeApkDataMap(updateApkDataList);

                if (apkKeyList != null && apkKeyList.size() > 0) {
                    isForceApkUpdate = true;
                    String key = getCurrentApkKey();
                    requestApkDownload(key);
                }
            }else{
                setForceApkUpdateStatus(FORCE_APK_STATUS_NO_DATA);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean isOnDownloading() {
        return onDownloading;
    }

    /***
     * 2020.09.16  Annotation processing call wakelock
     * call when Application created instead of below
     */
    private void setDownloadStatus(boolean inProgress) {
        onDownloading = inProgress;
        if (Log.INCLUDE) {
            Log.d(TAG, "setDownloadStatus: onDownloading="+onDownloading);
        }
//        if(onDownloading){
//            PowerStatusManager.getInstance().acquireWakeLock();
//        }else{
//            PowerStatusManager.getInstance().releaseWakeLock();
//        }
    }

    /**
     * FW 버전 확인
     */
    private void requestFwUpdateInfo() {
        if (Log.INCLUDE) {
            Log.d(TAG, "FwUpdateInfo: requestFwUpdateInfo");
        }
        CWMPSystemUpdateManager.getInstance().requestFwUpdateInfo(new RequestFwUpdateInfoCallback() {
            @Override
            public void onSuccess(UpdateFwData updateFwData) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwUpdateInfo: onSuccess: url=" + updateFwData.getUrl() + ", version=" + updateFwData.getNewVersion());
                }
                fwData = updateFwData;
                showUpdateDialog(TARGET_FW, TYPE_REQUEST, AUTO_HIDE_30);
            }

            @Override
            public void onFail(String message) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwUpdateInfo: onFail: " + message);
                }
                fwData = null;
                requestApkUpdateInfo();
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwUpdateInfo: onError: " + errorCode+", "+errorMessage);
                }
                fwData = null;
                requestApkUpdateInfo();
            }
        });
    }

    /**
     * FW 다운로드 실행
     * @param updateFwData
     */
    private void requestFwDownload(UpdateFwData updateFwData) {
        if (Log.INCLUDE) {
            Log.d(TAG, "FwDownload: requestFwDownload: FwData="+updateFwData);
        }
        if(updateFwData == null) return;

        setDownloadStatus(true);
        showUpdateDialog(TARGET_FW, TYPE_PROGRESS, NO_HIDE);
        CWMPSystemUpdateManager.getInstance().requestFwDownload(updateFwData, new RequestFwDownloadCallback() {
            @Override
            public void onDownloading(DownloadInfo downloadInfo) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwDownload: onDownloading: " + downloadInfo.getDownloadSize() + " / " + downloadInfo.getTotalSize());
                }
                if (systemDialog != null && systemDialog.isVisible()) {
                    systemDialog.updateProgressBar(downloadInfo.getDownloadSize(), downloadInfo.getTotalSize());
                }
            }

            @Override
            public void onCompleted(DownloadInfo downloadInfo) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwDownload: onCompleted");
                }

//                PowerStatusManager.getInstance().releaseWakeLock();
                showUpdateDialog(TARGET_FW, TYPE_COMPLETE, AUTO_HIDE_10);
            }

            @Override
            public void onCanceled() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwDownload: onCanceled");
                }
                setDownloadStatus(false);
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwDownload: onError: " + errorCode+", "+errorMessage);
                }
                setDownloadStatus(false);
                showErrorDialog(TARGET_FW, TYPE_ERROR, NO_HIDE, errorCode, errorMessage);
            }
        });
    }

    /**
     * FW 다운로드 강제 실행
     * @param url
     */
    private void requestFwDownloadForce(String url) {
        if (Log.INCLUDE) {
            Log.d(TAG, "FwDownloadForce: requestFwDownload: url=" + url);
        }
        setDownloadStatus(true);
        showUpdateDialog(TARGET_FW, TYPE_PROGRESS, NO_HIDE);
        fwData = CWMPSystemUpdateManager.getInstance().requestFwDownload(url, new RequestFwDownloadCallback() {
            @Override
            public void onDownloading(DownloadInfo downloadInfo) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwDownloadForce: onDownloading: " + downloadInfo.getDownloadSize() + " / " + downloadInfo.getTotalSize());
                }
                if (systemDialog != null && systemDialog.isVisible()) {
                    systemDialog.updateProgressBar(downloadInfo.getDownloadSize(), downloadInfo.getTotalSize());
                }
            }

            @Override
            public void onCompleted(DownloadInfo downloadInfo) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwDownloadForce: onCompleted");
                }

//                PowerStatusManager.getInstance().releaseWakeLock();
                showUpdateDialog(TARGET_FW, TYPE_COMPLETE, AUTO_HIDE_10);
            }

            @Override
            public void onCanceled() {
                setDownloadStatus(false);
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwDownloadForce: onError: " + errorCode+", "+errorMessage);
                }
                setDownloadStatus(false);
                showErrorDialog(TARGET_FW, TYPE_ERROR, NO_HIDE, errorCode, errorMessage);
            }
        });
    }

    /**
     * FW 업데이트 실행
     */
    private void requestFwUpdate() {
        if (Log.INCLUDE) {
            Log.d(TAG, "FwUpdate: requestFwUpdate");
        }
        CWMPSystemUpdateManager.getInstance().requestFwUpdate(fwData, new RequestFwUpdateCallback() {
            @Override
            public void onSuccess() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwUpdate: onSuccess");
                }
                setDownloadStatus(false);
            }

            @Override
            public void onFail(String errorCode, String errorMessage) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwDownloadForce: onError: " + errorCode+", "+errorMessage);
                }
                setDownloadStatus(false);
                showErrorDialog(TARGET_FW, TYPE_ERROR, NO_HIDE, errorCode, errorMessage);
            }
        });
    }

    /**
     * FW 다운로드 취소
     */
    public void cancelFwDownload() {
        try {
            if (fwData != null) CWMPSystemUpdateManager.getInstance().cancelFwDownload(fwData);
        }catch (Exception|Error e){
        }
    }

    /**
     * APK 버전 확인
     */
    private void requestApkUpdateInfo() {
        if (Log.INCLUDE) {
            Log.d(TAG, "ApkUpdateInfo: requestApkUpdateInfo");
        }
        isForceApkUpdate = false;
        CWMPSystemUpdateManager.getInstance().requestApkUpdateInfo(new RequestApkUpdateInfoCallback() {
            @Override
            public void onSuccess(List<UpdateApkData> updateApkDataList) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "ApkUpdateInfo: onSuccess");
                }
                setDownloadStatus(false);
                if (updateApkDataList != null && updateApkDataList.size() > 0) {
                    makeApkDataMap(updateApkDataList);

                    if (apkKeyList.size() > 0) {
                        showUpdateDialog(TARGET_APK, TYPE_REQUEST, AUTO_HIDE_30);
                        return;
                    }
                }
            }

            @Override
            public void onFail(String message) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "ApkUpdateInfo: onFail: "+message);
                }
                setDownloadStatus(false);
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "ApkUpdateInfo: onError: " + errorCode+", "+errorMessage);
                }
                setDownloadStatus(false);
            }
        });
    }

    /**
     * APK 다운로드 실행
     * @param key
     */
    private void requestApkDownload(String key) {
        if (Log.INCLUDE) {
            Log.d(TAG, "ApkDownload: requestApkDownload: key="+key);
        }
        setDownloadStatus(true);

        int totalApkSize = apkDataMap.size();
        int currApkNum = (totalApkSize - apkKeyList.size() + 1);
        if(systemDialog != null && systemDialog.isVisible()) {
            systemDialog.updateProgressCount(currApkNum, totalApkSize);
        }else{
            showProgressDialog(TARGET_APK, TYPE_PROGRESS, NO_HIDE, currApkNum, totalApkSize);
        }

        UpdateApkData apkData = apkDataMap.get(key);
        if(apkData != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "ApkDownload: target apk=" + apkData.getPackageName()+", version="+apkData.getVersion());
            }
            CWMPSystemUpdateManager.getInstance().requestApkDownload(apkData, new RequestApkDownloadCallback() {
                @Override
                public void onDownloading(DownloadInfo downloadInfo) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "ApkDownload: onDownloading: " + downloadInfo.getDownloadSize() + " / " + downloadInfo.getTotalSize());
                    }
                    if (systemDialog != null && systemDialog.isVisible()) {
                        systemDialog.updateProgressBar(downloadInfo.getDownloadSize(), downloadInfo.getTotalSize());
                    }
                }

                @Override
                public void onCompleted(DownloadInfo downloadInfo) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "ApkDownload: onCompleted");
                    }
                    String key = getCurrentApkKey();
                    requestApkUpdate(key);
                }

                @Override
                public void onCanceled() {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "ApkDownload: onCanceled");
                    }
                    setDownloadStatus(false);
                }

                @Override
                public void onError(String errorCode, String errorMessage) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "ApkDownload: onError: " + errorCode+", "+errorMessage);
                    }
                    if(isForceApkUpdate) {
                        setForceApkUpdateStatus(FORCE_APK_STATUS_FAIL);
                    }
                    setDownloadStatus(false);
                    showErrorDialog(TARGET_APK, TYPE_ERROR, NO_HIDE, errorCode, errorMessage);
                }
            });
        }
    }

    /**
     * APK 업데이트 실행
     * @param key
     */
    private void requestApkUpdate(String key) {
        if (Log.INCLUDE) {
            Log.d(TAG, "ApkUpdate: requestApkUpdate: key="+key);
        }
        UpdateApkData apkData = apkDataMap.get(key);
        if(apkData != null) {
            CWMPSystemUpdateManager.getInstance().requestApkUpdate(apkData, new RequestApkUpdateCallback() {
                @Override
                public void onSuccess() {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "ApkUpdate: onSuccess");
                    }

                    if (isForceApkUpdate) {
                        removeForceApkData(apkData);
                    }
                    if (apkKeyList.size() > 0) {
                        apkKeyList.remove(0);
                    }
                    if (apkKeyList.size() > 0) {
                        String key = getCurrentApkKey();
                        requestApkDownload(key);
                        return;
                    }
                    if(isForceApkUpdate) {
                        setForceApkUpdateStatus(FORCE_APK_STATUS_COMPLETE);
                    }
                    setDownloadStatus(false);
//                    PowerStatusManager.getInstance().releaseWakeLock();
                    showUpdateDialog(TARGET_APK, TYPE_COMPLETE, AUTO_HIDE_10);
                }

                @Override
                public void onFail(String errorCode, String errorMessage) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "ApkUpdate: onFail: "+errorCode+", "+errorMessage);
                    }
                    if(isForceApkUpdate) {
                        setForceApkUpdateStatus(FORCE_APK_STATUS_FAIL);
                    }
                    setDownloadStatus(false);
                    showErrorDialog(TARGET_APK, TYPE_ERROR, NO_HIDE, errorCode, errorMessage);
                }
            });
        }
    }

    /**
     * APK 다운로드 취소
     */
    public void cancelApkDownload() {
        try {
            if(apkDataMap != null) {
                String key = "";
                UpdateApkData apkData;
                for (int i = 0; i < apkKeyList.size(); i++) {
                    key = apkKeyList.get(i);
                    apkData = apkDataMap.get(key);
                    CWMPSystemUpdateManager.getInstance().cancelApkDownload(apkData);
                }
            }
        }catch (Exception|Error e){
        }
    }

    private void makeApkDataMap(List<UpdateApkData> updateApkDataList){
        try {
            apkDataMap.clear();
            apkKeyList.clear();

            String launcherKey = null;
            UpdateApkData launcherData = null;
            for (UpdateApkData data : updateApkDataList) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "buildApkDataList: package=" + data.getPackageName());
                    Log.d(TAG, "buildApkDataList: version=" + data.getVersion());
                    Log.d(TAG, "buildApkDataList: url=" + data.getUrl());
                }

                String key = data.getPackageName() + " : " + data.getVersion();
                if (LAUNCHER_PKG_NAME.equals(data.getPackageName())) {
                    launcherKey = key;
                    launcherData = data;
                } else {
                    apkKeyList.add(key);
                    apkDataMap.put(key, data);
                }
            }
            if (launcherKey != null && !launcherKey.isEmpty()) {
                launcherData.setReboot(true);
                apkKeyList.add(launcherKey);
                apkDataMap.put(launcherKey, launcherData);
            } else {
                String lastApkKey = getLastApkKey();
                UpdateApkData lastApkData = apkDataMap.get(lastApkKey);
                lastApkData.setReboot(true);
                apkDataMap.put(lastApkKey, lastApkData);
            }

            if (Log.INCLUDE) {
                String key = "";
                for(int i=0; i<apkKeyList.size(); i++) {
                    key = apkKeyList.get(i);

                    Log.d(TAG, "buildApkDataList: ["+i+"]apk=" + key +", rebooted="+apkDataMap.get(key).isReboot());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String getCurrentApkKey(){
        String key = "";
        try {
            if (apkKeyList.size() > 0) {
                key = apkKeyList.get(0);
            }
        }catch (Exception e){
        }
        return key;
    }

    private String getLastApkKey(){
        String key = "";
        try {
            int lastIndex = apkKeyList.size() - 1;
            if (lastIndex >= 0) {
                key = apkKeyList.get(lastIndex);
            }
        }catch (Exception e){
        }
        return key;
    }

    private void setForceApkUpdateStatus(String value){
        try{
            CWMPCPEManager.getInstance().getRoot().x_ttbb_diagMon.forceApkUpdate.UpdateState.setValue(value);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void removeForceApkData(UpdateApkData apkData){
        try{
            Lists lists = CWMPCPEManager.getInstance().getRoot().x_ttbb_diagMon.forceApkUpdate.lists;
            for (int listIndex : lists.getListIndexes()) {
                com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.x_ttbb_diagmon.forceapkupdate.List list =  lists.getList(listIndex);
                if(list != null) {
                    String packageName = list.PackageId.getValue();
                    String version = list.Version.getValue();
                    String url = list.Url.getValue();
                    if(packageName.equals(apkData.getPackageName()) &&
                       version.equals(apkData.getVersion()) &&
                       url.equals(apkData.getUrl())
                    ){
                        CWMPCPEManager.getInstance().getRoot().x_ttbb_diagMon.forceApkUpdate.lists.removeList(listIndex);
                        return;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showUpdateDialog(int target, int type, long delayTime){
        if(systemDialog != null && systemDialog.isVisible()){
            systemDialog.updateView(target, type, delayTime);

        }else {
            systemDialog = new SystemUpdateDialog(target, type, delayTime);

            systemDialog.setOnUpdateActionListener(new SystemUpdateDialog.OnUpdateActionListener() {
                @Override
                public void onConfirm(int target, int type) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onConfirm: target="+target+", type=" + type);
                    }
                    systemDialog.dismiss();
                    systemDialog = null;
                    if (target == TARGET_FW) {
                        if (type == TYPE_REQUEST) {
                            if (fwData != null) {
                                requestFwDownload(fwData);

                            }
                        } else if (type == TYPE_COMPLETE) {
                            requestFwUpdate(); // STB reboot
//                        DeviceUtil.reboot(context, "firmware upgrade");
                        }

                    } else if (target == TARGET_APK) {
                        if (type == TYPE_REQUEST) {
                            if (apkKeyList != null && apkKeyList.size() > 0) {
                                String key = getCurrentApkKey();
                                requestApkDownload(key);

                            }
                        } else if (type == TYPE_COMPLETE) {
//                            DeviceUtil.reboot(context, "apk upgrade");
                        }
                    }
                }

                @Override
                public void onCancel(int target, int type) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onCancel: target="+target+", type=" + type);
                    }
                    if (target == TARGET_FW) {
                        if (type == TYPE_REQUEST) {
                            cancelFwDownload();
                        }
                    } else if (target == TARGET_APK) {
                        if (type == TYPE_REQUEST) {
                            cancelApkDownload();
                        }
                    }
                    systemUpdateScheduler.stop();

                    setDownloadStatus(false);
                    systemDialog.dismiss();
                    systemDialog = null;
                }
            });
            systemDialog.show(context);
        }
    }

    private void showProgressDialog(int target, int type, long delayTime, int current, int total){
        showUpdateDialog(target, type, delayTime);
        if(systemDialog != null && systemDialog.isVisible()){
            systemDialog.updateProgressCount(current, total);
        }
    }

    private void showErrorDialog(int target, int type, long delayTime, String errorCode, String errorMessage){
        showUpdateDialog(target, type, delayTime);
        if (systemDialog != null && systemDialog.isVisible()) {
            systemDialog.updateErrorMessage(errorCode, ErrorMessageManager.getInstance().getErrorMessage(errorCode));
        }
//        PowerStatusManager.getInstance().releaseWakeLock();
    }

    private void startPowerStatusListener(){
        this.powerStatusReceiver.start(context, new SystemUpdateManager.OnPowerStatusListener() {
            @Override
            public void onScreenOn() {
                if(!isOnDownloading()){
                    systemUpdateScheduler.start(true);
                }
            }

            @Override
            public void onScreenOff() {
                if(!isOnDownloading()){
                    systemUpdateScheduler.start(false);
                }
            }
        });
    }

    public void resetHiddenKeyIndex(){
        checkHiddenIndex = 0;
    }

    public void setSystemUpdateEnable(int keyCode){
        if (checkHiddenIndex < HIDDEN_KEYS.length && keyCode == HIDDEN_KEYS[checkHiddenIndex]) {
            if (++checkHiddenIndex == HIDDEN_KEYS.length) {
                checkHiddenIndex = 0;
                UserPreferenceManagerImp up = new UserPreferenceManagerImp(context);
                boolean enabled = up.getSystemUpdateEnable();
                if (Log.INCLUDE) {
                    Log.d(TAG, "setSystemUpdateEnable: enabled="+enabled);
                }
                if(enabled){
                    up.setSystemUpdateEnable(false);
                    JasminToast.makeToast(context, R.string.sys_update_disabled);
                }else{
                    up.setSystemUpdateEnable(true);
                    JasminToast.makeToast(context, R.string.sys_update_enabled);
                }
            }
        }
    }

    private boolean isSystemUpdateEnabled(){
        UserPreferenceManagerImp up = new UserPreferenceManagerImp(context);
        return up.getSystemUpdateEnable();
    }

    public interface OnSWUpdateCheckResult {
        void onResult(boolean hasUpdate);
        void onError();
    }

    interface OnPowerStatusListener {
        void onScreenOn();
        void onScreenOff();
    }

    class PowerStatusReceiver extends BroadcastReceiver {
        private final String SCREEN_ON = Intent.ACTION_SCREEN_ON;
        private final String SCREEN_OFF = Intent.ACTION_SCREEN_OFF;

        private boolean isScreenOff;
        private Context context;
        private OnPowerStatusListener listener;

        void dispose() {
            try {
                if (context != null) {
                    context.unregisterReceiver(this);
                }
            } catch (Exception e) {
            }
            context = null;
        }

        void start(Context context, OnPowerStatusListener listener) {
            if (Log.INCLUDE) {
                Log.d(TAG, "start");
            }
            this.context = context;
            this.listener = listener;
            PowerManager powerManager = null;
            if (powerManager == null) {
                powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                isScreenOff = !powerManager.isInteractive();
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "start: isScreenOff="+isScreenOff);
            }
            try {
                IntentFilter filter = new IntentFilter();
                filter.addAction(Intent.ACTION_SCREEN_ON);
                filter.addAction(Intent.ACTION_SCREEN_OFF);
                context.registerReceiver(this, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * 대기모드 여부를 리턴합니다.
         *
         * @return
         */
        boolean isScreenOff() {
            return isScreenOff;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                boolean value = false;
                if (action.equals(SCREEN_ON)) {
                    value = false;
                } else if (action.equals(SCREEN_OFF)) {
                    value = true;
                }
                if(value == isScreenOff) return;
                isScreenOff = value;
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReceive: intent="+intent);
                }
                if(listener != null){
                    if(isScreenOff) {
                        listener.onScreenOff();
                    }else {
                        listener.onScreenOn();
                    }
                }
            }
        }
    }
}
