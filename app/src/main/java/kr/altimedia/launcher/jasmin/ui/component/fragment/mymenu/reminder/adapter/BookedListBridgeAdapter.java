/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.reminder.adapter;

import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;

import com.altimedia.util.Log;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.reminder.presenter.BookedItemPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.Reminder;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;

public class BookedListBridgeAdapter extends ItemBridgeAdapter {
    private static final String TAG = BookedListBridgeAdapter.class.getSimpleName();

    private BookedItemPresenter.OnKeyListener onKeyListener;

    public BookedListBridgeAdapter(ObjectAdapter adapter, int visibleCount) {
        super(adapter);

        initPagingAdapter(adapter, visibleCount);
    }

    private void initPagingAdapter(ObjectAdapter adapter, int visibleCount) {
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) adapter;
        int originalSize = objectAdapter.size();

        int mod = originalSize % visibleCount;
        if (mod != 0) {
            int binSize = visibleCount - mod;
            for (int i = 0; i < binSize; i++) {
                objectAdapter.add(null);
            }
        }
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        BookedItemPresenter presenter = (BookedItemPresenter) viewHolder.mPresenter;
        onKeyListener = presenter.getOnKeyListener();

        int index = (int) viewHolder.itemView.getTag(R.id.KEY_INDEX);
        if (onKeyListener != null) {
            viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    Reminder item = (Reminder) viewHolder.mItem;
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onKey: index=" + index);
                    }
                    return onKeyListener.onKey(keyCode, index, item);
                }
            });
        }

//        onBindListener.onBind(index);
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        viewHolder.mHolder.view.setOnKeyListener(null);
    }

    public ArrayList<Reminder> getList() {
        ArrayList<Reminder> list = new ArrayList<>();
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) getAdapter();
        for (int i = 0; i < objectAdapter.size(); i++) {
            Reminder product = (Reminder) objectAdapter.get(i);
            list.add(product);
        }

        return list;
    }

//    public interface OnBindListener {
//        void onBind(int index);
//    }

}
