
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StreamInfo implements Parcelable {
    @Expose
    @SerializedName("type")
    private StreamType streamType;
    @Expose
    @SerializedName("videoPath")
    private String[] videoPath;
    @Expose
    @SerializedName("thumbnailImage")
    private ThumbnailInfo thumbnailImage;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("previewInfo")
    private PreviewInfo previewInfo;
    @Expose
    @SerializedName("introInfo")
    private IntroInfo introInfo;
    @Expose
    @SerializedName("posterUrl")
    private String posterUrl;


    protected StreamInfo(Parcel in) {
        streamType = (StreamType) in.readSerializable();
        videoPath = in.createStringArray();
        thumbnailImage = in.readParcelable(ThumbnailInfo.class.getClassLoader());
        title = in.readString();
        previewInfo = in.readParcelable(PreviewInfo.class.getClassLoader());
        introInfo = in.readParcelable(IntroInfo.class.getClassLoader());
        posterUrl = in.readString();
    }

    public StreamInfo(StreamType streamType, String[] videoPath, ThumbnailInfo thumbnailImage, String title, String posterUrl) {
        this(streamType, videoPath, thumbnailImage, title, null, null, posterUrl);
    }

    public StreamInfo(StreamType streamType, String[] videoPath, ThumbnailInfo thumbnailImage, String title, PreviewInfo previewInfo, String posterUrl) {
        this(streamType, videoPath, thumbnailImage, title, previewInfo, null, posterUrl);
    }

    public StreamInfo(StreamType streamType, String[] videoPath, ThumbnailInfo thumbnailImage, String title, IntroInfo introInfo, String posterUrl) {
        this(streamType, videoPath, thumbnailImage, title, null, introInfo, posterUrl);
    }

    public StreamInfo(StreamType streamType, String[] videoPath, ThumbnailInfo thumbnailImage, String title, PreviewInfo previewInfo, IntroInfo introInfo, String posterUrl) {
        this.streamType = streamType;
        this.videoPath = videoPath;
        this.thumbnailImage = thumbnailImage;
        this.title = title;
        this.previewInfo = previewInfo;
        this.introInfo = introInfo;
        this.posterUrl = posterUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StreamInfo> CREATOR = new Creator<StreamInfo>() {
        @Override
        public StreamInfo createFromParcel(Parcel in) {
            return new StreamInfo(in);
        }

        @Override
        public StreamInfo[] newArray(int size) {
            return new StreamInfo[size];
        }
    };


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(streamType);
        dest.writeStringArray(videoPath);
        dest.writeParcelable(thumbnailImage, flags);
        dest.writeString(title);
        dest.writeParcelable(previewInfo, flags);
        dest.writeParcelable(introInfo, flags);
        dest.writeString(posterUrl);
    }

    public StreamType getStreamType() {
        return streamType;
    }

    public String[] getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String[] videoPath) {
        this.videoPath = videoPath;
    }

    public String getVideoPath(int index) {
        if (index < 0 || index >= videoPath.length) {
            return "";
        }
        return videoPath[index];
    }

    public PreviewInfo getPreviewInfo() {
        return previewInfo;
    }

    public ThumbnailInfo getThumbnailImage() {
        return thumbnailImage;
    }

    public String getTitle() {
        return title;
    }

    public IntroInfo getIntroInfo() {
        return introInfo;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    @Override
    public String toString() {
        return "StreamInfo{" +
                "streamType=" + streamType +
                ", videoPath='" + videoPath + '\'' +
                ", thumbnailImage=" + thumbnailImage +
                ", title='" + title + '\'' +
                ", previewInfo=" + previewInfo +
                ", introInfo=" + introInfo +
                ", posterUrl='" + posterUrl + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StreamInfo that = (StreamInfo) o;

        if (streamType != that.streamType) return false;
        if (videoPath != null ? !videoPath.equals(that.videoPath) : that.videoPath != null)
            return false;
        if (thumbnailImage != null ? !thumbnailImage.equals(that.thumbnailImage) : that.thumbnailImage != null)
            return false;
        return title != null ? title.equals(that.title) : that.title == null;
    }

    @Override
    public int hashCode() {
        int result = streamType != null ? streamType.hashCode() : 0;
        result = 31 * result + (videoPath != null ? videoPath.hashCode() : 0);
        result = 31 * result + (thumbnailImage != null ? thumbnailImage.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }
}
