/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.animation.TimeAnimator;
import android.os.Bundle;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.ViewHolderTask;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.ListRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;


public class RowsFragment extends BaseRowFragment implements MenuBrowseFragment.MainFragmentRowsAdapterProvider, MenuBrowseFragment.MainFragmentAdapterProvider {
    private RowsFragment.MainFragmentAdapter mMainFragmentAdapter;
    private RowsFragment.MainFragmentRowsAdapter mMainFragmentRowsAdapter;
    static final String TAG = "VerticalRowsFragment";
    static final boolean DEBUG = false;
    static final int ALIGN_TOP_NOT_SET = -2147483648;
    ItemBridgeAdapter.ViewHolder mSelectedViewHolder;
    private int mSubPosition;
    boolean mExpand = true;
    boolean mViewsCreated;
    private int mAlignedTop = -2147483648;
    boolean mAfterEntranceTransition = true;
    boolean mFreezeRows;
    BaseOnItemViewSelectedListener mOnItemViewSelectedListener;
    BaseOnItemViewClickedListener mOnItemViewClickedListener;
    int mSelectAnimatorDuration;
    Interpolator mSelectAnimatorInterpolator = new DecelerateInterpolator(2.0F);
    private RecyclerView.RecycledViewPool mRecycledViewPool;
    private ArrayList<Presenter> mPresenterMapper;
    ItemBridgeAdapter.AdapterListener mExternalAdapterListener;
    private final ItemBridgeAdapter.AdapterListener mBridgeAdapterListener = new ItemBridgeAdapter.AdapterListener() {
        public void onAddPresenter(Presenter presenter, int type) {
            if (RowsFragment.this.mExternalAdapterListener != null) {
                RowsFragment.this.mExternalAdapterListener.onAddPresenter(presenter, type);
            }

        }

        public void onCreate(ItemBridgeAdapter.ViewHolder vh) {
            VerticalGridView listView = RowsFragment.this.getVerticalGridView();
            if (listView != null) {
                listView.setClipChildren(false);
            }

            RowsFragment.this.setupSharedViewPool(vh);
            RowsFragment.this.mViewsCreated = true;
            vh.setExtraObject(RowsFragment.this.new RowViewHolderExtra(vh));
            RowsFragment.setRowViewSelected(vh, false, true);
            if (RowsFragment.this.mExternalAdapterListener != null) {
                RowsFragment.this.mExternalAdapterListener.onCreate(vh);
            }

            RowPresenter rowPresenter = (RowPresenter)vh.getPresenter();
            RowPresenter.ViewHolder rowVh = rowPresenter.getRowViewHolder(vh.getViewHolder());
            rowVh.setOnItemViewSelectedListener(RowsFragment.this.mOnItemViewSelectedListener);
            rowVh.setOnItemViewClickedListener(RowsFragment.this.mOnItemViewClickedListener);
        }

        public void onAttachedToWindow(ItemBridgeAdapter.ViewHolder vh) {
            RowsFragment.setRowViewExpanded(vh, RowsFragment.this.mExpand);
            RowPresenter rowPresenter = (RowPresenter)vh.getPresenter();
            RowPresenter.ViewHolder rowVh = rowPresenter.getRowViewHolder(vh.getViewHolder());
            rowPresenter.setEntranceTransitionState(rowVh, RowsFragment.this.mAfterEntranceTransition);
            rowPresenter.freeze(rowVh, RowsFragment.this.mFreezeRows);
            if (RowsFragment.this.mExternalAdapterListener != null) {
                RowsFragment.this.mExternalAdapterListener.onAttachedToWindow(vh);
            }

        }

        public void onDetachedFromWindow(ItemBridgeAdapter.ViewHolder vh) {
            if (RowsFragment.this.mSelectedViewHolder == vh) {
                RowsFragment.setRowViewSelected(RowsFragment.this.mSelectedViewHolder, false, true);
                RowsFragment.this.mSelectedViewHolder = null;
            }

            if (RowsFragment.this.mExternalAdapterListener != null) {
                RowsFragment.this.mExternalAdapterListener.onDetachedFromWindow(vh);
            }

        }

        public void onBind(ItemBridgeAdapter.ViewHolder vh) {
            if (RowsFragment.this.mExternalAdapterListener != null) {
                RowsFragment.this.mExternalAdapterListener.onBind(vh);
            }

        }

        public void onUnbind(ItemBridgeAdapter.ViewHolder vh) {
            RowsFragment.setRowViewSelected(vh, false, true);
            if (RowsFragment.this.mExternalAdapterListener != null) {
                RowsFragment.this.mExternalAdapterListener.onUnbind(vh);
            }

        }
    };

    public RowsFragment() {
    }

    public MenuBrowseFragment.MainFragmentAdapter getMainFragmentAdapter() {
        if (this.mMainFragmentAdapter == null) {
            this.mMainFragmentAdapter = new RowsFragment.MainFragmentAdapter(this);
        }

        return this.mMainFragmentAdapter;
    }

    public MenuBrowseFragment.MainFragmentRowsAdapter getMainFragmentRowsAdapter() {
        if (this.mMainFragmentRowsAdapter == null) {
            this.mMainFragmentRowsAdapter = new RowsFragment.MainFragmentRowsAdapter(this);
        }

        return this.mMainFragmentRowsAdapter;
    }

    protected VerticalGridView findGridViewFromRoot(View view) {
        return (VerticalGridView)view.findViewById(R.id.container_list);
    }

    public void setOnItemViewClickedListener(BaseOnItemViewClickedListener listener) {
        this.mOnItemViewClickedListener = listener;
        if (this.mViewsCreated) {
            throw new IllegalStateException("Item clicked listener must be set before views are created");
        }
    }

    public BaseOnItemViewClickedListener getOnItemViewClickedListener() {
        return this.mOnItemViewClickedListener;
    }

    /** @deprecated */
    @Deprecated
    public void enableRowScaling(boolean enable) {
    }

    public void setExpand(boolean expand) {
        this.mExpand = expand;
        VerticalGridView listView = this.getVerticalGridView();
        if (listView != null) {
            int count = listView.getChildCount();

            for(int i = 0; i < count; ++i) {
                View view = listView.getChildAt(i);
                ItemBridgeAdapter.ViewHolder vh = (ItemBridgeAdapter.ViewHolder)listView.getChildViewHolder(view);
                setRowViewExpanded(vh, this.mExpand);
            }
        }

    }

    public void setOnItemViewSelectedListener(BaseOnItemViewSelectedListener listener) {
        this.mOnItemViewSelectedListener = listener;
        VerticalGridView listView = this.getVerticalGridView();
        if (listView != null) {
            int count = listView.getChildCount();

            for(int i = 0; i < count; ++i) {
                View view = listView.getChildAt(i);
                ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder)listView.getChildViewHolder(view);
                getRowViewHolder(ibvh).setOnItemViewSelectedListener(this.mOnItemViewSelectedListener);
            }
        }

    }

    public BaseOnItemViewSelectedListener getOnItemViewSelectedListener() {
        return this.mOnItemViewSelectedListener;
    }

    void onRowSelected(RecyclerView parent, RecyclerView.ViewHolder viewHolder, int position, int subposition) {
        if (this.mSelectedViewHolder != viewHolder || this.mSubPosition != subposition) {
            this.mSubPosition = subposition;
            if (this.mSelectedViewHolder != null) {
                setRowViewSelected(this.mSelectedViewHolder, false, false);
            }

            this.mSelectedViewHolder = (ItemBridgeAdapter.ViewHolder)viewHolder;
            if (this.mSelectedViewHolder != null) {
                setRowViewSelected(this.mSelectedViewHolder, true, false);
            }
        }

        if (this.mMainFragmentAdapter != null) {
            this.mMainFragmentAdapter.getFragmentHost().showTitleView(position <= 0);
        }

    }

    public RowPresenter.ViewHolder getRowViewHolder(int position) {
        VerticalGridView verticalView = this.getVerticalGridView();
        return verticalView == null ? null : getRowViewHolder((ItemBridgeAdapter.ViewHolder)verticalView.findViewHolderForAdapterPosition(position));
    }

    int getLayoutResourceId() {
        return R.layout.fragment_row;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mSelectAnimatorDuration = this.getResources().getInteger(R.integer.lb_browse_rows_anim_duration);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.getVerticalGridView().setItemAlignmentViewId(R.id.row_content);
        this.getVerticalGridView().setSaveChildrenPolicy(2);
        this.setAlignment(this.mAlignedTop);
        this.mRecycledViewPool = null;
        this.mPresenterMapper = null;
        if (this.mMainFragmentAdapter != null) {
            this.mMainFragmentAdapter.getFragmentHost().notifyViewCreated(this.mMainFragmentAdapter);
        }

    }

    public void onDestroyView() {
        this.mViewsCreated = false;
        super.onDestroyView();
    }

    void setExternalAdapterListener(ItemBridgeAdapter.AdapterListener listener) {
        this.mExternalAdapterListener = listener;
    }

    static void setRowViewExpanded(ItemBridgeAdapter.ViewHolder vh, boolean expanded) {
        ((RowPresenter)vh.getPresenter()).setRowViewExpanded(vh.getViewHolder(), true);
    }

    static void setRowViewSelected(ItemBridgeAdapter.ViewHolder vh, boolean selected, boolean immediate) {
        RowsFragment.RowViewHolderExtra extra = (RowsFragment.RowViewHolderExtra)vh.getExtraObject();
        extra.animateSelect(selected, immediate);
        ((RowPresenter)vh.getPresenter()).setRowViewSelected(vh.getViewHolder(), selected);
    }

    void setupSharedViewPool(ItemBridgeAdapter.ViewHolder bridgeVh) {
        RowPresenter rowPresenter = (RowPresenter)bridgeVh.getPresenter();
        RowPresenter.ViewHolder rowVh = rowPresenter.getRowViewHolder(bridgeVh.getViewHolder());
        if (rowVh instanceof ListRowPresenter.ViewHolder) {
            HorizontalGridView view = ((ListRowPresenter.ViewHolder)rowVh).getGridView();
            if (this.mRecycledViewPool == null) {
                this.mRecycledViewPool = view.getRecycledViewPool();
            } else {
                view.setRecycledViewPool(this.mRecycledViewPool);
            }

            ItemBridgeAdapter bridgeAdapter = ((ListRowPresenter.ViewHolder)rowVh).getBridgeAdapter();
            if (this.mPresenterMapper == null) {
                this.mPresenterMapper = bridgeAdapter.getPresenterMapper();
            } else {
                bridgeAdapter.setPresenterMapper(this.mPresenterMapper);
            }
        }

    }

    void updateAdapter() {
        super.updateAdapter();
        this.mSelectedViewHolder = null;
        this.mViewsCreated = false;
        ItemBridgeAdapter adapter = this.getBridgeAdapter();
        if (adapter != null) {
            adapter.setAdapterListener(this.mBridgeAdapterListener);
        }

    }

    public boolean onTransitionPrepare() {
        boolean prepared = super.onTransitionPrepare();
        if (prepared) {
            this.freezeRows(true);
        }

        return prepared;
    }

    public void onTransitionEnd() {
        super.onTransitionEnd();
        this.freezeRows(false);
    }

    private void freezeRows(boolean freeze) {
        this.mFreezeRows = freeze;
        VerticalGridView verticalView = this.getVerticalGridView();
        if (verticalView != null) {
            int count = verticalView.getChildCount();

            for(int i = 0; i < count; ++i) {
                ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder)verticalView.getChildViewHolder(verticalView.getChildAt(i));
                RowPresenter rowPresenter = (RowPresenter)ibvh.getPresenter();
                RowPresenter.ViewHolder vh = rowPresenter.getRowViewHolder(ibvh.getViewHolder());
                rowPresenter.freeze(vh, freeze);
            }
        }

    }

    public void setEntranceTransitionState(boolean afterTransition) {
        this.mAfterEntranceTransition = afterTransition;
        VerticalGridView verticalView = this.getVerticalGridView();
        if (verticalView != null) {
            int count = verticalView.getChildCount();

            for(int i = 0; i < count; ++i) {
                ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder)verticalView.getChildViewHolder(verticalView.getChildAt(i));
                RowPresenter rowPresenter = (RowPresenter)ibvh.getPresenter();
                RowPresenter.ViewHolder vh = rowPresenter.getRowViewHolder(ibvh.getViewHolder());
                rowPresenter.setEntranceTransitionState(vh, this.mAfterEntranceTransition);
            }
        }

    }

    public void setSelectedPosition(int rowPosition, boolean smooth, final Presenter.ViewHolderTask rowHolderTask) {
        VerticalGridView verticalView = this.getVerticalGridView();
        if (verticalView != null) {
            ViewHolderTask task = null;
            if (rowHolderTask != null) {
                task = new ViewHolderTask() {
                    public void run(final RecyclerView.ViewHolder rvh) {
                        rvh.itemView.post(new Runnable() {
                            public void run() {
                                rowHolderTask.run(RowsFragment.getRowViewHolder((ItemBridgeAdapter.ViewHolder)rvh));
                            }
                        });
                    }
                };
            }

            if (smooth) {
                verticalView.setSelectedPositionSmooth(rowPosition, task);
            } else {
                verticalView.setSelectedPosition(rowPosition, task);
            }

        }
    }

    static RowPresenter.ViewHolder getRowViewHolder(ItemBridgeAdapter.ViewHolder ibvh) {
        if (ibvh == null) {
            return null;
        } else {
            RowPresenter rowPresenter = (RowPresenter)ibvh.getPresenter();
            return rowPresenter.getRowViewHolder(ibvh.getViewHolder());
        }
    }

    public boolean isScrolling() {
        if (this.getVerticalGridView() == null) {
            return false;
        } else {
            return this.getVerticalGridView().getScrollState() != 0;
        }
    }

    public void setAlignment(int windowAlignOffsetFromTop) {
        if (windowAlignOffsetFromTop != -2147483648) {
            this.mAlignedTop = windowAlignOffsetFromTop;
            VerticalGridView gridView = this.getVerticalGridView();
            if (gridView != null) {
                gridView.setItemAlignmentOffset(0);
                gridView.setItemAlignmentOffsetPercent(-1.0F);
                gridView.setItemAlignmentOffsetWithPadding(true);
                gridView.setWindowAlignmentOffset(this.mAlignedTop);
                gridView.setWindowAlignmentOffsetPercent(-1.0F);
                gridView.setWindowAlignment(0);
            }

        }
    }

    public RowPresenter.ViewHolder findRowViewHolderByPosition(int position) {
        return this.mVerticalGridView == null ? null : getRowViewHolder((ItemBridgeAdapter.ViewHolder)this.mVerticalGridView.findViewHolderForAdapterPosition(position));
    }

    /** @deprecated */
    @Deprecated
    public static class MainFragmentRowsAdapter extends MenuBrowseFragment.MainFragmentRowsAdapter<RowsFragment> {
        public MainFragmentRowsAdapter(RowsFragment fragment) {
            super(fragment);
        }

        public void setAdapter(ObjectAdapter adapter) {
            (this.getFragment()).setAdapter(adapter);
        }

        public void setOnItemViewClickedListener(OnItemViewClickedListener listener) {
            (this.getFragment()).setOnItemViewClickedListener(listener);
        }

        public void setOnItemViewSelectedListener(OnItemViewSelectedListener listener) {
            (this.getFragment()).setOnItemViewSelectedListener(listener);
        }

        public void setSelectedPosition(int rowPosition, boolean smooth, Presenter.ViewHolderTask rowHolderTask) {
            (this.getFragment()).setSelectedPosition(rowPosition, smooth, rowHolderTask);
        }

        public void setSelectedPosition(int rowPosition, boolean smooth) {
            (this.getFragment()).setSelectedPosition(rowPosition, smooth);
        }

        public int getSelectedPosition() {
            return (this.getFragment()).getSelectedPosition();
        }

        public RowPresenter.ViewHolder findRowViewHolderByPosition(int position) {
            return ((RowsFragment)this.getFragment()).findRowViewHolderByPosition(position);
        }
    }

    public static class MainFragmentAdapter extends MenuBrowseFragment.MainFragmentAdapter<RowsFragment> {
        public MainFragmentAdapter(RowsFragment fragment) {
            super(fragment);
            this.setScalingEnabled(true);
        }

        public boolean isScrolling() {
            return ((RowsFragment)this.getFragment()).isScrolling();
        }

        public void setExpand(boolean expand) {
            ((RowsFragment)this.getFragment()).setExpand(expand);
        }

        public void setEntranceTransitionState(boolean state) {
            ((RowsFragment)this.getFragment()).setEntranceTransitionState(state);
        }

        public void setAlignment(int windowAlignOffsetFromTop) {
            ((RowsFragment)this.getFragment()).setAlignment(windowAlignOffsetFromTop);
        }

        public boolean onTransitionPrepare() {
            return ((RowsFragment)this.getFragment()).onTransitionPrepare();
        }

        public void onTransitionStart() {
            ((RowsFragment)this.getFragment()).onTransitionStart();
        }

        public void onTransitionEnd() {
            ((RowsFragment)this.getFragment()).onTransitionEnd();
        }
    }

    final class RowViewHolderExtra implements TimeAnimator.TimeListener {
        final RowPresenter mRowPresenter;
        final Presenter.ViewHolder mRowViewHolder;
        final TimeAnimator mSelectAnimator = new TimeAnimator();
        int mSelectAnimatorDurationInUse;
        Interpolator mSelectAnimatorInterpolatorInUse;
        float mSelectLevelAnimStart;
        float mSelectLevelAnimDelta;

        RowViewHolderExtra(ItemBridgeAdapter.ViewHolder ibvh) {
            this.mRowPresenter = (RowPresenter)ibvh.getPresenter();
            this.mRowViewHolder = ibvh.getViewHolder();
            this.mSelectAnimator.setTimeListener(this);
        }

        public void onTimeUpdate(TimeAnimator animation, long totalTime, long deltaTime) {
            if (this.mSelectAnimator.isRunning()) {
                this.updateSelect(totalTime, deltaTime);
            }

        }

        void updateSelect(long totalTime, long deltaTime) {
            float fraction;
            if (totalTime >= (long)this.mSelectAnimatorDurationInUse) {
                fraction = 1.0F;
                this.mSelectAnimator.end();
            } else {
                fraction = (float)((double)totalTime / (double)this.mSelectAnimatorDurationInUse);
            }

            if (this.mSelectAnimatorInterpolatorInUse != null) {
                fraction = this.mSelectAnimatorInterpolatorInUse.getInterpolation(fraction);
            }

            float level = this.mSelectLevelAnimStart + fraction * this.mSelectLevelAnimDelta;
            this.mRowPresenter.setSelectLevel(this.mRowViewHolder, level);
        }

        void animateSelect(boolean select, boolean immediate) {
            this.mSelectAnimator.end();
            float end = select ? 1.0F : 0.0F;
            if (immediate) {
                this.mRowPresenter.setSelectLevel(this.mRowViewHolder, end);
            } else if (this.mRowPresenter.getSelectLevel(this.mRowViewHolder) != end) {
                this.mSelectAnimatorDurationInUse = RowsFragment.this.mSelectAnimatorDuration;
                this.mSelectAnimatorInterpolatorInUse = RowsFragment.this.mSelectAnimatorInterpolator;
                this.mSelectLevelAnimStart = this.mRowPresenter.getSelectLevel(this.mRowViewHolder);
                this.mSelectLevelAnimDelta = end - this.mSelectLevelAnimStart;
                this.mSelectAnimator.start();
            }

        }
    }
}
