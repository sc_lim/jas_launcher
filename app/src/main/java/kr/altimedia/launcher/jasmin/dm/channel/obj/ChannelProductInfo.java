/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChannelProductInfo implements Parcelable {
    public static final Creator<ChannelProductInfo> CREATOR = new Creator<ChannelProductInfo>() {
        @Override
        public ChannelProductInfo createFromParcel(Parcel in) {
            return new ChannelProductInfo(in);
        }

        @Override
        public ChannelProductInfo[] newArray(int size) {
            return new ChannelProductInfo[size];
        }
    };
    @Expose
    @SerializedName("offerList")
    private List<ChannelProductOffer> offerList;
    @Expose
    @SerializedName("productDescription")
    private String productDescription;
    @Expose
    @SerializedName("termsAndConditions")
    private String termsAndConditions;
    @Expose
    @SerializedName("productId")
    private String productId;
    @Expose
    @SerializedName("productTitle")
    private String productTitle;

    protected ChannelProductInfo(Parcel in) {
        offerList = in.createTypedArrayList(ChannelProductOffer.CREATOR);
        productDescription = in.readString();
        termsAndConditions = in.readString();
        productId = in.readString();
        productTitle = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(offerList);
        dest.writeValue(productDescription);
        dest.writeValue(termsAndConditions);
        dest.writeValue(productId);
        dest.writeValue(productTitle);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "ChannelProductInfo{" +
                "offerList=" + offerList +
                ", productDescription='" + productDescription + '\'' +
                ", termsAndConditions='" + termsAndConditions + '\'' +
                ", productId='" + productId + '\'' +
                ", productTitle='" + productTitle + '\'' +
                '}';
    }

    public List<ChannelProductOffer> getOfferList() {
        return offerList;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public String getProductCode() {
        return productId;
    }

    public String getProductTitle() {
        return productTitle;
    }
}