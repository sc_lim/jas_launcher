/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.logging;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.dm.user.object.LoginResult;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * LogBackServiceProvider
 */
public class LogBackServiceProvider {
        private final String TAG = LogBackServiceProvider.class.getSimpleName();

    private static LogBackServiceProvider instance;
    private Context context;

    private final String DEVICE_TYPE = "STB";
    private ServiceConnection serviceConnection;
    private LogBackService logBackService;
    private final long WATCHING_TIME = 3 * TimeUtil.SEC; // milliseconds
    private final long WATCHING_HISTORY_CYCLE_TIME = 5 * TimeUtil.MIN; // milliseconds
    private boolean logBackStarted = false;

    public static void removeInstance() {
        if (instance != null) {
            instance.dispose();
            instance = null;
        }
    }

    public static LogBackServiceProvider getInstance() {
        if (instance == null) {
            instance = new LogBackServiceProvider();
        }
        return instance;
    }

    private void dispose() {
        stop();

        context = null;
    }

    /**
     * LogBackServiceProvider를 초기화합니다.
     *
     * @param context
     */
    public void init(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "init");
        }

        this.context = context;
        this.logBackStarted = false;
    }

    /**
     * LogBackServiceProvider를 시작합니다.
     */
    public void start() {
        if (Log.INCLUDE) {
            Log.d(TAG, "start");
        }

        connect();
    }

    /**
     * LogBackServiceProvider를 중지합니다.
     */
    public void stop() {
        if (Log.INCLUDE) {
            Log.d(TAG, "stop");
        }
        if (serviceConnection != null) {
            context.unbindService(serviceConnection);
        }
        logBackStarted = false;
    }

    /**
     * LogBackService에 연결합니다.
     */
    private void connect() {
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                LogBackService.LogBackServiceBinder binder = (LogBackService.LogBackServiceBinder) service;
                logBackService = binder.getService();
                if (Log.INCLUDE) {
                    Log.d(TAG, "onServiceConnected");
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                logBackService = null;
                if (Log.INCLUDE) {
                    Log.d(TAG, "onServiceDisconnected");
                }
            }
        };

        try {
            Intent intent = new Intent(context, LogBackService.class);
            context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 채널 시청 로그를 큐에 적재합니다.
     *
     * @param channelId channel identifier
     * @param entryTime entry time for corresponding channel
     */
    public void addChannelLog(String channelId, long entryTime) {
        try {
            if (logBackService != null) {
                if (!logBackStarted) {
                    startLogBack();
                }
                logBackService.addChannelLog(channelId, entryTime);
            }
        } catch (RemoteException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * 채널 시청 인정 시간(milliseconds)을 리턴합니다.
     */
    public long getWatchingTime() {
        long watchingTime = WATCHING_TIME;
        try {
            LoginResult loginResult = (LoginResult) AccountManager.getInstance().getData(AccountManager.KEY_LOGIN_INFO);
            watchingTime = Integer.parseInt(loginResult.getWatchingTime()) * TimeUtil.SEC;
        } catch (Exception e) {
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "getWatchingTime: "+watchingTime);
        }
        if(watchingTime <= 0){
            return WATCHING_TIME;
        }else{
            return watchingTime;
        }
    }

    /**
     * 채널 시청 이력 전송 주기(milliseconds)을 리턴합니다.
     */
    public long getWatchingHistoryCycleTime() {
        long cycleTime = WATCHING_HISTORY_CYCLE_TIME;
        try {
            LoginResult loginResult = (LoginResult) AccountManager.getInstance().getData(AccountManager.KEY_LOGIN_INFO);
            cycleTime = Integer.parseInt(loginResult.getWatchingHistoryCycle()) * TimeUtil.MIN;
        } catch (Exception e) {
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "getWatchingHistoryCycleTime: "+cycleTime);
        }
        if(cycleTime <= 0){
            return WATCHING_HISTORY_CYCLE_TIME;
        }else{
            return cycleTime;
        }
    }

    private void startLogBack() {
        try {
            logBackService.startLogBack(DEVICE_TYPE);
            logBackStarted = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
