/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog.playbck.presenter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContent;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Created by mc.kim on 04,03,2020
 */
public class VodEndActionPresenter {
    private final VodEndDataProvider mVodEndDataProvider;
    private boolean mNextSeriesPopup = false;

    private final Action[] EPISODE_BUTTON = new Action[]{
            Action.NEXT_EPISODE, Action.NOT_NOW
    };
    private final Action[] EPISODE_WATCHABLE_BUTTON = new Action[]{
            Action.WATCH_NOW, Action.NOT_NOW
    };

    public enum FinishType {
        Exit, NextEpisodeDetail, TuneNextEpisode, Cancel
    }

    private View parentView = null;

    private TextView mWatchNowButton = null;

    public void onViewCreated(View view) {
        parentView = view;
        Content movie = mVodEndDataProvider.getContents();
        boolean isPurchased = mVodEndDataProvider.isPurchasedNextMovie();
        if (mVodEndDataProvider.hasSeries()) {
            ImageView nextEpisodePoster = view.findViewById(R.id.nextEpisodePoster);
            Glide.with(view.getContext())
                    .load(movie.getPosterSourceUrl(SeriesContent.mPosterDefaultRect.width(),
                            SeriesContent.mPosterDefaultRect.height())).diskCacheStrategy(DiskCacheStrategy.DATA).placeholder(R.drawable.detail_series_default).error(R.drawable.detail_series_default)
                    .centerCrop()
                    .into(nextEpisodePoster);
        }
        TextView contentTitle = view.findViewById(R.id.contentTitle);
        TextView nextEpisodeNotice = view.findViewById(R.id.nextEpisodeNotice);
        contentTitle.setText(movie.getTitle());
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        ViewGroup buttonDock = view.findViewById(R.id.button_dock);
        for (Action action : mNextSeriesPopup ?
                (isPurchased ? EPISODE_WATCHABLE_BUTTON : EPISODE_BUTTON) :
                VOD_BUTTON) {
            View button = inflater.inflate(R.layout.widget_end_watching_button, null);
            TextView textButton = button.findViewById(R.id.button);
            textButton.setText(action.getResourceID());
            button.setOnClickListener(new ActionKeyListener(action, mVodEndDataProvider));
            if (action.getResourceID() == Action.WATCH_NOW.getResourceID()) {
                mWatchNowButton = textButton;
            }
            buttonDock.addView(button);
        }
    }

    public void notifyCountDown(long timeMillis) {
        if (mWatchNowButton != null) {
            String timeTitle = parentView.getContext().getResources().getString(R.string.next_episode_in);
            long time = timeMillis / TimeUtil.SEC;
            mWatchNowButton.setWidth(mWatchNowButton.getContext().getResources().getDimensionPixelOffset(R.dimen.next_episode_max_width));
            mWatchNowButton.setText(timeTitle + " " + time);
        }
    }

    private final Action[] VOD_BUTTON = new Action[]{
            Action.EXIT, Action.CANCEL
    };

    public VodEndActionPresenter(VodEndDataProvider vodEndDataProvider) {
        this.mVodEndDataProvider = vodEndDataProvider;
    }

    public View generateView(LayoutInflater inflater, @Nullable ViewGroup container) {
        mNextSeriesPopup = this.mVodEndDataProvider.isEOF() && this.mVodEndDataProvider.hasSeries();

        if (mNextSeriesPopup) {
            return inflater.inflate(R.layout.dialog_end_watching_vod_series, container, false);
        } else {
            return inflater.inflate(R.layout.dialog_end_watching_vod, container, false);
        }
    }

    public enum Action {
        NEXT_EPISODE(R.string.button_purchase, FinishType.NextEpisodeDetail),
        NOT_NOW(R.string.button_not_now, FinishType.Exit),
        WATCH_NOW(R.string.watch_now, FinishType.TuneNextEpisode),
        EXIT(R.string.button_exit, FinishType.Exit),
        CANCEL(R.string.cancel, FinishType.Cancel);
        final int resourceID;
        final FinishType type;

        Action(int resourceID, FinishType type) {
            this.resourceID = resourceID;
            this.type = type;
        }

        public int getResourceID() {
            return resourceID;
        }

        public FinishType getType() {
            return type;
        }
    }

    private static class ActionKeyListener implements View.OnClickListener {
        final Action mCurrentAction;
        final VodEndDataProvider mVodEndDataProvider;

        public ActionKeyListener(Action action, VodEndDataProvider vodEndDataProvider) {
            this.mCurrentAction = action;
            this.mVodEndDataProvider = vodEndDataProvider;
        }

        @Override
        public void onClick(View v) {
            mVodEndDataProvider.onAction(mCurrentAction);
        }
    }
}
