/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.payment.obj.PaymentStatus;

public class CouponPurchaseResult implements Parcelable {
    @Expose
    @SerializedName("paymentStatus")
    private List<PaymentStatus> paymentStatusList;
    @Expose
    @SerializedName("successYn")
    private String isSuccess;


    protected CouponPurchaseResult(Parcel in) {
        paymentStatusList = in.createTypedArrayList(PaymentStatus.CREATOR);
        isSuccess = in.readString();
    }

    public static final Creator<CouponPurchaseResult> CREATOR = new Creator<CouponPurchaseResult>() {
        @Override
        public CouponPurchaseResult createFromParcel(Parcel in) {
            return new CouponPurchaseResult(in);
        }

        @Override
        public CouponPurchaseResult[] newArray(int size) {
            return new CouponPurchaseResult[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(paymentStatusList);
        dest.writeString(isSuccess);
    }

    @NonNull
    @Override
    public String toString() {
        return "CouponPurchaseResult{" +
                "paymentStatusList='" + paymentStatusList + '\'' +
                ", isSuccess='" + isSuccess + '\'' +
                '}';
    }

    public List<PaymentStatus> getPaymentStatusList() {
        return paymentStatusList;
    }

    public boolean isSuccess() {
        return isSuccess.equalsIgnoreCase("y");
    }
}
