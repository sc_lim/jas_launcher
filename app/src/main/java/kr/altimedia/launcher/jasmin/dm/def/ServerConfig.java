/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.def;

import kr.altimedia.launcher.jasmin.BuildConfig;

/**
 * Created by mc.kim on 11,05,2020
 */
public class ServerConfig {
    public static final long CONNECT_TIMEOUT_SECOND = 3;
    public static final long WRITE_TIMEOUT_SECOND = 3;
    public static final long READ_TIMEOUT_SECOND = 3;

    public static final String FLAVOR_TYPE_LIVE = "live";
    public static final String FLAVOR_TYPE_STAGING = "staging";
    public static final String FLAVOR_TYPE_KR = "kr";

    public static final String SYSTEM_PASSWORD;

    private static final String[] LIVE_LIST = {
            "https://mbs-ex.3bbtv.com/service/",
            "https://mbs-svc.3bbtv.com/ser/",
            "https://mbs-svc.3bbtv.com/ads/reportCollector/",
            "https://iptv.recommendrec.3bbtv.com/rec/",
            "https://iptv.license.3bbtv.com/LIC_REQ_CC",
            "360bbe60f0"
    };

    private static final String[] STAGING_LIST = {
            "https://stg.mbs-ex.3bbtv.com/service/",
            "https://stg.mbs-svc.3bbtv.com/ser/",
            "https://stg.mbs-svc.3bbtv.com/ads/reportCollector/",
            "https://iptvstaging.recommendrec.3bbtv.com/rec/",
            "https://iptvstaging.license.3bbtv.com/LIC_REQ_CC",
            "360bbe60f0"
    };

    private static final String[] KR_TESTBED_LIST = {
            "http://222.122.207.37/service/",
            "http://222.122.207.37/ser/",
            "http://222.122.207.37/ads/reportCollector/",
            "http://222.122.207.46:8080/rec/",
            "http://222.122.207.35:8110/LIC_REQ_CC",
            "stb_system"
    };

    public static final String SERVER_TYPE;

    public static final String MBS_URL;
    public static final String SEARCH_URL;
    public static final String AD_URL;
    public static final String RECOMMEND_URL;
    public static final String LA_URL;

    static {
        String[] serverList;
        if (FLAVOR_TYPE_LIVE.equals(BuildConfig.FLAVOR)) {
            SERVER_TYPE = "LIVE";
            serverList = LIVE_LIST; //live
        } else if (FLAVOR_TYPE_STAGING.equals(BuildConfig.FLAVOR)) {
            SERVER_TYPE = "STAGING";
            serverList = STAGING_LIST;  //staging
        } else {//FLAVOR_TYPE_KR
            SERVER_TYPE = "KR_TESTBED";
            serverList = KR_TESTBED_LIST;   //kr_testbed
        }
        MBS_URL = serverList[0];
        SEARCH_URL = serverList[1];
        AD_URL = serverList[2];
        RECOMMEND_URL = serverList[3];
        LA_URL = serverList[4];
        SYSTEM_PASSWORD = serverList[5];
    }
}
