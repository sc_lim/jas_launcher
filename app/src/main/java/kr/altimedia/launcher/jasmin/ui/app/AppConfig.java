/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.app;

import kr.altimedia.launcher.jasmin.BuildConfig;

public class AppConfig {
    public static final boolean USE_CACHED_EPG_INFO = true;

    //features
    public static final boolean FEATURE_TIME_SHIFT_ENABLED;
    public static final boolean FEATURE_THUMBNAIL_VIDEO_ENABLED;
    public static final boolean FEATURE_FINGER_PRINT_ENABLED;

    //for test
    public static final boolean TEST_START_LIVE_TV_WITHOUT_LOGIN_ENABLED;
    public static final boolean TEST_SHOW_PROGRAM_PR;
    public static final boolean TEST_CHANNEL_PREVIEW;

    static {
        if (BuildConfig.DEBUG == false) {//for release - DO NOT CHANGE THIS VALUES!
            //features
            FEATURE_TIME_SHIFT_ENABLED = true;
            FEATURE_THUMBNAIL_VIDEO_ENABLED = true;
            FEATURE_FINGER_PRINT_ENABLED = true;

            //always false for release
            TEST_START_LIVE_TV_WITHOUT_LOGIN_ENABLED = false;
            TEST_SHOW_PROGRAM_PR = false;
            TEST_CHANNEL_PREVIEW = false;
        } else {//for test - you can change this values for testing.
            //features
            FEATURE_TIME_SHIFT_ENABLED = true;
            FEATURE_THUMBNAIL_VIDEO_ENABLED = true;
            FEATURE_FINGER_PRINT_ENABLED = true;

            //set true when test
            TEST_START_LIVE_TV_WITHOUT_LOGIN_ENABLED = false;
            TEST_SHOW_PROGRAM_PR = false;
            TEST_CHANNEL_PREVIEW = false;
        }
    }
}
