/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityManager.AccessibilityStateChangeListener;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LifecycleObserver;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.tvmodule.common.WeakHandler;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.dao.ProgramImpl;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;
import com.google.auto.factory.AutoFactory;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.payment.VodPaymentDataManager;
import kr.altimedia.launcher.jasmin.dm.user.AuthenticationHandler;
import kr.altimedia.launcher.jasmin.system.settings.data.PipInfo;
import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;
import kr.altimedia.launcher.jasmin.tv.JasTvView;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.tv.manager.KeyEventManager;
import kr.altimedia.launcher.jasmin.tv.ui.MiniGuideDialogFragment;
import kr.altimedia.launcher.jasmin.tv.ui.PipContainerWindow;
import kr.altimedia.launcher.jasmin.tv.ui.TimeShiftFragment;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.AppConfig;
import kr.altimedia.launcher.jasmin.ui.app.module.DefaultBackendKnobsFlags;
import kr.altimedia.launcher.jasmin.ui.component.activity.BaseActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.BootDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ChannelErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.CouponNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FullscreenDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.HalfSizedDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.PinDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ProfileLoginNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SubscribeChannelDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.TimeShiftPinDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.TwoButtonDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.profile.ProfileSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.PlaybackFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.dca.DCADialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.MenuNavigationFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.message.MessageDetailDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.CouponHistoryDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.detail.CouponDetailDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.register.CouponRegistrationDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.list.CouponListDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.CouponPurchaseDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.CouponTermsDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment.CouponPaymentCancelDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment.CouponPaymentDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.MyMenuDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.ProfileIconSelectDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.ProfileManageDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.ProfilePinCheckDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.collection.CollectionListDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history.PurchasedListDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription.SubscriptionDetailDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription.SubscriptionListDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.reminder.BookedDeletePopup;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.reminder.BookedListDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingAccountPinCheckDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingMenuDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account.SettingAccountPinChangeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account.SettingAccountPinManageFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account.SettingAccountPinResetDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.edit.SettingDeviceEditDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.faq.SettingFAQDetailDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.lock.SettingProfileLockDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.lock.SettingProfileLockPinCheckDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.block.SettingProfileChannelBlockedResetDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.block.SettingProfileChannelBlockedSetupDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.rating.SettingProfileRatingLocksFragmentDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.subtitles.SettingSubtitlesSetupDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system.SettingSystemInfoUpdateDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodDetailsDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodRatingPinDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.SeriesProductSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.VodSeasonSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.PackageVodSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.VodResumeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.VodTrailerSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.VodWatchPackageDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.CouponSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPurchaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentCancelDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionInfoDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionPurchaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionTermsDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment.SubscriptionPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.guide.ProgramGuide;

/**
 * A class responsible for the life cycle and event handling of the pop-ups over TV view.
 */
@UiThread
@AutoFactory
public class TvOverlayManager implements AccessibilityStateChangeListener {
    private static final String TAG = TvOverlayManager.class.getSimpleName();
    private static final String INTRO_TRACKER_LABEL = "Intro dialog";

    public @interface MenuShowReason {
    }

    public static final int REASON_NONE = 0;
    public static final int REASON_GUIDE = 1;
    public static final int REASON_HOME = 2;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef(
            flag = true,
            value = {
                    FLAG_HIDE_OVERLAYS_DEFAULT,
                    FLAG_HIDE_OVERLAYS_WITHOUT_ANIMATION,
                    FLAG_HIDE_OVERLAYS_KEEP_SCENE,
                    FLAG_HIDE_OVERLAYS_KEEP_DIALOG,
                    FLAG_HIDE_OVERLAYS_KEEP_SIDE_PANELS,
                    FLAG_HIDE_OVERLAYS_KEEP_SIDE_PANEL_HISTORY,
                    FLAG_HIDE_OVERLAYS_KEEP_PROGRAM_GUIDE,
                    FLAG_HIDE_OVERLAYS_KEEP_MENU,
                    FLAG_HIDE_OVERLAYS_KEEP_FRAGMENT,
                    FLAG_HIDE_OVERLAYS_KEEP_PIP,
                    FLAG_HIDE_OVERLAYS_KEEP_MINIGUIDE,
                    FLAG_HIDE_OVERLAYS_KEEP_TUNE
            })
    private @interface HideOverlayFlag {
    }

    // FLAG_HIDE_OVERLAYs must be bitwise exclusive.
    public static final int FLAG_HIDE_OVERLAYS_DEFAULT = 0b00000000000;
    public static final int FLAG_HIDE_OVERLAYS_KEEP_TUNE = 0b00000000001;
    public static final int FLAG_HIDE_OVERLAYS_WITHOUT_ANIMATION = 0b00000000010;
    public static final int FLAG_HIDE_OVERLAYS_KEEP_SCENE = 0b00000000100;
    public static final int FLAG_HIDE_OVERLAYS_KEEP_DIALOG = 0b00000001000;
    public static final int FLAG_HIDE_OVERLAYS_KEEP_SIDE_PANELS = 0b00000010000;
    public static final int FLAG_HIDE_OVERLAYS_KEEP_SIDE_PANEL_HISTORY = 0b00000100000;
    public static final int FLAG_HIDE_OVERLAYS_KEEP_PROGRAM_GUIDE = 0b00001000000;
    public static final int FLAG_HIDE_OVERLAYS_KEEP_MENU = 0b00010000000;
    public static final int FLAG_HIDE_OVERLAYS_KEEP_FRAGMENT = 0b00100000000;
    public static final int FLAG_HIDE_OVERLAYS_KEEP_PIP = 0b01000000000;
    public static final int FLAG_HIDE_OVERLAYS_KEEP_MINIGUIDE = 0b10000000000;

    private static final int MSG_OVERLAY_CLOSED = 1000;


    /**
     * The overlay type which indicates that there are no overlays.
     */
    private static final int OVERLAY_TYPE_NONE = 0b000000000;
    /**
     * The overlay type for menu.
     */
    private static final int OVERLAY_TYPE_MENU = 0b000000001;
    /**
     * The overlay type for the side fragment.
     */
    private static final int OVERLAY_TYPE_SIDE_FRAGMENT = 0b000000010;
    /**
     * The overlay type for dialog fragment.
     */
    public static final int OVERLAY_TYPE_DIALOG = 0b000000100;
    /**
     * The overlay type for program guide.
     */
    private static final int OVERLAY_TYPE_GUIDE = 0b000001000;
    /**
     * The overlay type for channel banner.
     */
    private static final int OVERLAY_TYPE_SCENE_CHANNEL_BANNER = 0b000010000;

    /**
     * The overlay type for input banner.
     */
    private static final int OVERLAY_TYPE_SCENE_INPUT_BANNER = 0b000100000;
    /**
     * The overlay type for keypad channel switch view.
     */
    private static final int OVERLAY_TYPE_SCENE_KEYPAD_CHANNEL_SWITCH = 0b001000000;

    /**
     * The overlay type for select input view.
     */
    private static final int OVERLAY_TYPE_SCENE_SELECT_INPUT = 0b010000000;

    /**
     * The overlay type for fragment other than the side fragment and dialog fragment.
     */
    private static final int OVERLAY_TYPE_FRAGMENT = 0b100000000;
    /**
     * The overlay type for dca.
     */
    public static final int OVERLAY_TYPE_DCA = 0b100000001;


    /**
     * The overlay type for mini guide.
     */
    public static final int OVERLAY_TYPE_MINI_GUIDE = 0b100000010;
    /**
     * The overlay type for dca.
     */
    public static final int OVERLAY_PIP = 0b100000100;
    /**
     * The overlay type for dca.
     */
    public static final int OVERLAY_TYPE_NOTICE = 0b10000100;


    // Used for the padded print of the overlay type.
    private static final int NUM_OVERLAY_TYPES = 9;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            UPDATE_CHANNEL_BANNER_REASON_FORCE_SHOW,
            UPDATE_CHANNEL_BANNER_REASON_TUNE,
            UPDATE_CHANNEL_BANNER_REASON_TUNE_FAST,
            UPDATE_CHANNEL_BANNER_REASON_UPDATE_INFO,
            UPDATE_CHANNEL_BANNER_REASON_LOCK_OR_UNLOCK,
            UPDATE_CHANNEL_BANNER_REASON_UPDATE_STREAM_INFO
    })
    private @interface ChannelBannerUpdateReason {
    }

    /**
     * Updates channel banner because the channel banner is forced to show.
     */
    public static final int UPDATE_CHANNEL_BANNER_REASON_FORCE_SHOW = 1;
    /**
     * Updates channel banner because of tuning.
     */
    public static final int UPDATE_CHANNEL_BANNER_REASON_TUNE = 2;
    /**
     * Updates channel banner because of fast tuning.
     */
    public static final int UPDATE_CHANNEL_BANNER_REASON_TUNE_FAST = 3;
    /**
     * Updates channel banner because of info updating.
     */
    public static final int UPDATE_CHANNEL_BANNER_REASON_UPDATE_INFO = 4;
    /**
     * Updates channel banner because the current watched channel is locked or unlocked.
     */
    public static final int UPDATE_CHANNEL_BANNER_REASON_LOCK_OR_UNLOCK = 5;
    /**
     * Updates channel banner because of stream info updating.
     */
    public static final int UPDATE_CHANNEL_BANNER_REASON_UPDATE_STREAM_INFO = 6;
    /**
     * Updates channel banner because of channel signal updating.
     */
    public static final int UPDATE_CHANNEL_BANNER_REASON_UPDATE_SIGNAL_STRENGTH = 7;

    private static final String FRAGMENT_TAG_SETUP_SOURCES = "tag_setup_sources";
    private static final String FRAGMENT_TAG_NEW_SOURCES = "tag_new_sources";

    private static final Set<String> AVAILABLE_DIALOG_TAGS = new HashSet<>();
    private static final Set<Integer> NOT_SAVED_TAG = new HashSet<>();

    static {
        NOT_SAVED_TAG.add(OVERLAY_TYPE_DCA);
    }

    static {
        AVAILABLE_DIALOG_TAGS.add(DCADialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(MiniGuideDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(FullscreenDialogFragment.DIALOG_TAG);
        AVAILABLE_DIALOG_TAGS.add(HalfSizedDialogFragment.DIALOG_TAG);
        AVAILABLE_DIALOG_TAGS.add(BootDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(PinDialogFragment.DIALOG_TAG);
        AVAILABLE_DIALOG_TAGS.add(SubscribeChannelDialogFragment.DIALOG_TAG);
        AVAILABLE_DIALOG_TAGS.add(VodDetailsDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(VodRatingPinDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(VodPurchaseDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(VodPaymentDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SubscriptionPurchaseDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SubscriptionSelectDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(VodWatchPackageDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SeriesProductSelectDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(PackageVodSelectDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(VodResumeDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(VodTrailerSelectDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(CouponSelectDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingMenuDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingAccountPinManageFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingAccountPinChangeDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingAccountPinResetDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingSubtitlesSetupDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingFAQDetailDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingProfileDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingProfileRatingLocksFragmentDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingProfileChannelBlockedSetupDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingProfileChannelBlockedResetDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingProfileLockDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingDeviceEditDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(MessageDetailDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(VodSeasonSelectDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SubscriptionTermsDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SubscriptionInfoDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SubscriptionPaymentDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(MyMenuDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(ProfileManageDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(ProfilePinCheckDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(PurchasedListDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(CollectionListDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SubscriptionListDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SubscriptionDetailDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(BookedListDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(BookedDeletePopup.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(CouponHistoryDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(CouponRegistrationDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(CouponListDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(CouponDetailDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(CouponPurchaseDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(CouponTermsDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(CouponPaymentDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(TwoButtonDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(ProfileLoginNoticeDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingProfileLockPinCheckDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingSystemInfoUpdateDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(SettingAccountPinCheckDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(ProfileIconSelectDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(VodPaymentCancelDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(TimeShiftPinDialogFragment.DIALOG_TAG);
        AVAILABLE_DIALOG_TAGS.add(CouponPaymentCancelDialog.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(CouponNoticeDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(ProfileSelectDialogFragment.CLASS_NAME);
        AVAILABLE_DIALOG_TAGS.add(ChannelErrorDialogFragment.CLASS_NAME);
    }

    private final BaseActivity mMainActivity;
    private final TvTransitionManager mTransitionManager;
    private final ChannelDataManager mChannelDataManager;
    private final ProgramDataManager mProgramDataManager;
    private final TvInputManagerHelper mInputManager;
    //    private final ChannelBannerView mChannelBannerView;
//    private final ProgramGuide mProgramGuide;
    private SafeDismissDialogFragment mCurrentDialog;
    private boolean mChannelBannerHiddenBySideFragment;
    private final Handler mHandler = new TvOverlayHandler(this);
    private @TvOverlayType
    int mOpenedOverlays;

    private final List<Runnable> mPendingActions = new ArrayList<>();
    private final LinkedList<PendingDialogAction> mPendingDialogActionQueue = new LinkedList<>();

    private FragmentManager.OnBackStackChangedListener mOnBackStackChangedListener;

    private MiniGuideDialogFragment mMiniGuideDialogFragment;
    private ProgramGuide mProgramGuide = null;
    private final ViewGroup mSceneContainer;

    private PipContainerWindow mPipContainerWindow = null;

    private UserPreferenceManagerImp upManager = null;

    private final BackendKnobsFlags mBackendKnobsFlags;
    public TvOverlayManager(
            BaseActivity mainActivity,
            ViewGroup sceneContainer,
            ChannelDataManager channelDataManager,
            TvInputManagerHelper tvInputManager,
            ProgramDataManager programDataManager) {
        mMainActivity = mainActivity;
        mBackendKnobsFlags = TvSingletons.getSingletons(mMainActivity).getBackendKnobs();
        upManager = new UserPreferenceManagerImp(mainActivity);
        mSceneContainer = sceneContainer;
        mChannelDataManager = channelDataManager;
        mProgramDataManager = programDataManager;
        mInputManager = tvInputManager;
        mTransitionManager =
                new TvTransitionManager(
                        mainActivity,
                        sceneContainer);
        mTransitionManager.setListener(
                new TvTransitionManager.Listener() {
                    @Override
                    public void onSceneChanged(int fromScene, int toScene) {
                        // Call onOverlayOpened first so that the listener can know that a new scene
                        // will be opened when the onOverlayClosed is called.
                        if (toScene != TvTransitionManager.SCENE_TYPE_EMPTY) {
                            onOverlayOpened(convertSceneToOverlayType(toScene));
                        }
                        if (fromScene != TvTransitionManager.SCENE_TYPE_EMPTY) {
                            onOverlayClosed(convertSceneToOverlayType(fromScene));
                        }
                    }
                });


    }

    public void readyProgramGuide(ProgramGuide.OnProgramGuideListener onProgramGuideListener) {
        if (mProgramGuide != null) {
            return;
        }
        Runnable preShowRunnable = () -> onOverlayOpened(OVERLAY_TYPE_GUIDE);
        Runnable postHideRunnable = () -> onOverlayClosed(OVERLAY_TYPE_GUIDE);
        mProgramGuide =
                new ProgramGuide(
                        mMainActivity,
                        mInputManager,
                        mChannelDataManager,
                        mProgramDataManager,
                        preShowRunnable,
                        postHideRunnable, mSceneContainer, onProgramGuideListener);
    }

    public void release() {
//        mMenu.release();
        mHandler.removeCallbacksAndMessages(null);
//        if (mKeypadChannelSwitchView != null) {
//            mKeypadChannelSwitchView.setChannels(null);
//        }
    }

    /**
     * Returns the currently opened dialog.
     */
    public SafeDismissDialogFragment getCurrentDialog() {
        return mCurrentDialog;
    }


    /**
     * Shows the given dialog.
     */
    public void showDialogFragment(SafeDismissDialogFragment dialog) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showDialogFragment : " + dialog.getClassName());
        }
        showDialogFragment(dialog.getClassName(), dialog, false, false);
    }

    public void showDialogFragment(String tag, SafeDismissDialogFragment dialog) {
        showDialogFragment(tag, dialog, false, false);
    }

    public void showDialogFragment(String tag, SafeDismissDialogFragment dialog, boolean keepSidePanelHistory) {
        showDialogFragment(tag, dialog, keepSidePanelHistory, false);
    }

    public void initPlaybackController() {
        PlaybackFragment mPlaybackFragment = PlaybackFragment.newInstance();
        mPlaybackFragment.setTvOverlayManager(this);

        FragmentTransaction ft = mMainActivity
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.controllerFrame, mPlaybackFragment, PlaybackFragment.CLASS_NAME);
        if (mPlaybackFragment.isStateSaved()) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }

        initMiniGuideDialog();
    }

    private final List<OnOverlayDialogQueueListener> mDialogQueueListener = new ArrayList<>();


    private void dismissDialogFragment(
            String tag,
            SafeDismissDialogFragment dialog,
            boolean keepSidePanelHistory,
            boolean keepProgramGuide) {

        dialog.dismiss();
    }

    private void clearOverlayDialog() {

        if (Log.INCLUDE) {
            Log.d(TAG, "call clearOverlayDialog");
        }
        if (mCurrentDialog != null) {
            mCurrentDialog.dismiss();
            mCurrentDialog = null;
        }
        Iterator<PendingDialogAction> action = mPendingDialogActionQueue.iterator();
        while (action.hasNext()) {
            PendingDialogAction nextAction = action.next();
            nextAction.dismiss();
        }
        clearDialogQueue();
    }

    private void hideChannelOSD() {
        Fragment mFragment = mMainActivity.getSupportFragmentManager().findFragmentByTag(PlaybackFragment.CLASS_NAME);
        if (mFragment != null) {
            PlaybackFragment mPlaybackFragment = (PlaybackFragment) mFragment;
            mPlaybackFragment.hideChannelOSD();
        }
    }

    private void showDCADialogFragment(int keyCode) {
        hideChannelOSD();
        showDialogFragment(DCADialogFragment.newInstance(keyCode));
        onOverlayOpened(OVERLAY_TYPE_DCA);
    }

    private boolean checkWithNetwork() {
        if (!NetworkUtil.hasNetworkConnection(mMainActivity)) {
            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(MbsTaskCallback.REASON_NETWORK, mMainActivity);
            failNoticeDialogFragment.show(this, mMainActivity.getSupportFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
            return false;
        }

        if (!AccountManager.getInstance().isAuthenticatedUser()) {
            if (AppConfig.TEST_START_LIVE_TV_WITHOUT_LOGIN_ENABLED) {//just for testing
                return true;
            }
            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(MbsTaskCallback.REASON_LOGIN, mMainActivity);
            failNoticeDialogFragment.show(this, mMainActivity.getSupportFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
            return false;
        }
        return true;
    }

    private <T extends Fragment> T getFragment(String tag, Class<T> type) {
        return (T) (mMainActivity.getSupportFragmentManager().findFragmentByTag(tag));
    }

//    private boolean isPlaybackViewVisible() {
//        return getFragment(PlaybackFragment.CLASS_NAME, PlaybackFragment.class).isShowing();
//    }

    private void initMiniGuideDialog() {
        mMiniGuideDialogFragment = MiniGuideDialogFragment.newInstance();
        mMiniGuideDialogFragment.show(mMainActivity.getSupportFragmentManager(), MiniGuideDialogFragment.CLASS_NAME);
        onOverlayOpened(OVERLAY_TYPE_MINI_GUIDE);
    }

    public void checkAndShowMiniGuide() {
        if (mMainActivity instanceof LiveTvActivity) {
            ((LiveTvActivity) mMainActivity).checkAndShowMiniGuide();
        }
    }

    public boolean isMiniGuideVisible() {
        return mMiniGuideDialogFragment != null && mMiniGuideDialogFragment.isVisibleMiniGuide();
    }

    public void showMiniGuideFragment() {
        setVisibleMiniGuideDialog(true, true);
    }

    public void hideMiniGuideFragment() {
        hideMiniGuideFragment(true);
    }

    public void hideMiniGuideFragment(boolean showAnimation) {
        setVisibleMiniGuideDialog(false, showAnimation);
    }

    private void setVisibleMiniGuideDialog(boolean isVisible, boolean showAnimation) {
        if (isVisible && isProgramGuideVisible()) {
            return;
        }
        if (mMiniGuideDialogFragment != null) {
            mMiniGuideDialogFragment.setVisibleMiniGuideDialog(isVisible, showAnimation);
        }
    }

    private void runAfterFragmentsAreClosed(final Runnable runnable) {
//        if (isPlaybackViewVisible()) {
//            getFragment(PlaybackFragment.CLASS_NAME, PlaybackFragment.class).hideAll();
//        }


//        if (isPlaybackViewVisible()) {
//            final FragmentManager manager = mMainActivity.getSupportFragmentManager();
//            mOnBackStackChangedListener =
//                    new FragmentManager.OnBackStackChangedListener() {
//                        @Override
//                        public void onBackStackChanged() {
//                            if (manager.getBackStackEntryCount() == 0) {
//                                manager.removeOnBackStackChangedListener(this);
//                                mOnBackStackChangedListener = null;
//                                runnable.run();
//                            }
//                        }
//                    };
//            manager.addOnBackStackChangedListener(mOnBackStackChangedListener);
//        } else {
        runnable.run();
//        }
    }


    /**
     * Shows the main menu.
     */

    public void showMenu(@MenuShowReason int reason) {

        switch (reason) {
            case REASON_HOME:
                showFragment(MenuNavigationFragment.newInstance(), MenuNavigationFragment.CLASS_NAME);
                break;
        }
    }

    public boolean isMenuShowing() {
        Fragment targetView = mMainActivity
                .getSupportFragmentManager().findFragmentByTag(MenuNavigationFragment.CLASS_NAME);
        return targetView != null;
    }

    private boolean isShowingMenu() {
        return mMainActivity
                .getSupportFragmentManager().findFragmentByTag(MenuNavigationFragment.CLASS_NAME) != null;
    }


    private void showFragment(final Fragment fragment, final String tag) {
        onOverlayOpened(OVERLAY_TYPE_FRAGMENT);

        runAfterFragmentsAreClosed(
                () -> {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "showFragment(" + fragment + ")");
                    }
                    mMainActivity
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_container, fragment, tag)
                            .commitAllowingStateLoss();
                });
    }

    public void closeFragment(String fragmentTagToRemove) {
        if (Log.INCLUDE) {
            Log.d(TAG, "closeFragment(" + fragmentTagToRemove + ")");
        }
        onOverlayClosed(OVERLAY_TYPE_FRAGMENT);
        if (fragmentTagToRemove != null) {
            Fragment fragmentToRemove =
                    mMainActivity.getSupportFragmentManager().findFragmentByTag(fragmentTagToRemove);
            if (fragmentToRemove == null) {
                // If the fragment has not been added to the fragment manager yet, just remove the
                // listener not to add the fragment. This is needed because the side fragment is
                // closed asynchronously.
                mMainActivity
                        .getSupportFragmentManager()
                        .removeOnBackStackChangedListener(mOnBackStackChangedListener);
                mOnBackStackChangedListener = null;
            } else {
                FragmentTransaction ft = mMainActivity
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .remove(fragmentToRemove);

                if (fragmentToRemove.isStateSaved()) {
                    ft.commitAllowingStateLoss();
                } else {
                    ft.commit();
                }
            }
        }
    }

    private SafeDismissDialogFragment mCurrentNoticeDialog = null;


    public void showDialogFragment(
            String tag,
            SafeDismissDialogFragment dialog,
            boolean keepSidePanelHistory,
            boolean keepProgramGuide) {
        int flags = FLAG_HIDE_OVERLAYS_KEEP_DIALOG;
        if (keepSidePanelHistory) {
            flags |= FLAG_HIDE_OVERLAYS_KEEP_SIDE_PANEL_HISTORY;
        }
        if (keepProgramGuide) {
            flags |= FLAG_HIDE_OVERLAYS_KEEP_PROGRAM_GUIDE;
        }
        // A tag for dialog must be added to AVAILABLE_DIALOG_TAGS to make it launchable from TV.
        if (!AVAILABLE_DIALOG_TAGS.contains(tag)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "requested duplicated dialog showing make return");
            }
            return;
        }

        AVAILABLE_DIALOG_TAGS.remove(tag);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AVAILABLE_DIALOG_TAGS.add(tag);
            }
        }, 100);
        if (dialog.getOverlayType() == OVERLAY_TYPE_NOTICE) {
            if (mCurrentNoticeDialog != null) {
                mCurrentNoticeDialog.dismiss();
            }
            mCurrentNoticeDialog = dialog;
        }
        // Do not open two dialogs at the same time.
//        if (mCurrentDialog != null) {
        if (!NOT_SAVED_TAG.contains(dialog.getOverlayType())) {
            addDialogQueue(new PendingDialogAction(tag, dialog, keepSidePanelHistory, keepProgramGuide));
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "queue size : " + mPendingDialogActionQueue.size());
        }
//        }

        mCurrentDialog = dialog;
        dialog.show(mMainActivity.getSupportFragmentManager(), tag);
        // Calling this from SafeDismissDialogFragment.onCreated() might be late
        // because it takes time for onCreated to be called
        // and next key events can be handled by MainActivity, not Dialog.
        onOverlayOpened(OVERLAY_TYPE_DIALOG);
    }

    /**
     * It is called when a SafeDismissDialogFragment is destroyed.
     */

    public static final String KEY_CANCEL_PURCHASE_ID = "CANCEL_PURCHASE_ID";

    public void onSafeDismissDialogDestroyed(@TvOverlayType int type, SafeDismissDialogFragment dismissDialogFragment) {
//        mCurrentDialog = null;
        if (Log.INCLUDE) {
            Log.d(TAG, "call onSafeDismissDialogDestroyed");
        }

        Bundle bundle = dismissDialogFragment.getArguments();
        if (bundle != null && bundle.containsKey(KEY_CANCEL_PURCHASE_ID)) {
            String cancelPurchaseId = bundle.getString(KEY_CANCEL_PURCHASE_ID);
            cancelVodPayment(cancelPurchaseId);
        }

        PendingDialogAction pendingAction = new PendingDialogAction(dismissDialogFragment.getClassName(), dismissDialogFragment, false, false);
//        PendingDialogAction action = mPendingDialogActionQueue.poll();
        removeDialogQueue(pendingAction);
//        if (action == null) {
//        onOverlayClosed(type);
//        } else {
//            action.run();
//        }
    }

    private void cancelVodPayment(String cancelPurchaseId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "cancelVodPayment, cancelPurchaseId : " + cancelPurchaseId);
        }

        if (cancelPurchaseId != null && !cancelPurchaseId.isEmpty()) {
            VodPaymentDataManager vodPaymentDataManager = new VodPaymentDataManager();
            vodPaymentDataManager.cancelVodPayment(AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), cancelPurchaseId, new MbsDataProvider<String, Boolean>() {
                @Override
                public void needLoading(boolean loading) {

                }

                @Override
                public void onFailed(int reason) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "cancelVodPayment, onFailed");
                    }
                }

                @Override
                public void onSuccess(String id, Boolean result) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "cancelVodPayment, onSuccess");
                    }
                }

                @Override
                public void onError(String errorCode, String message) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "cancelVodPayment, onError, errorCode : " + errorCode);
                    }
                }
            });
        }
    }

    public void addOnQueueListener(OnOverlayDialogQueueListener onOverlayDialogQueueListener) {
        if (mDialogQueueListener.contains(onOverlayDialogQueueListener)) {
            return;
        }
        mDialogQueueListener.add(onOverlayDialogQueueListener);
    }

    public int getDialogQueueSize() {
        return mPendingDialogActionQueue.size();
    }

    public void removeOnQueueListener(OnOverlayDialogQueueListener onOverlayDialogQueueListener) {
        if (!mDialogQueueListener.contains(onOverlayDialogQueueListener)) {
            return;
        }
        mDialogQueueListener.remove(onOverlayDialogQueueListener);
    }

    private void fireDialogQueueChanged(int size) {
        for (OnOverlayDialogQueueListener onOverlayDialogQueueListener : mDialogQueueListener) {
            onOverlayDialogQueueListener.onSizeChanged(size);
        }
    }


    private void addDialogQueue(PendingDialogAction pendingAction) {
        boolean added = mPendingDialogActionQueue.offer(pendingAction);
        if (added) {
            fireDialogQueueChanged(mPendingDialogActionQueue.size());
        }
    }

    private void removeDialogQueue(PendingDialogAction pendingAction) {
        boolean removed = mPendingDialogActionQueue.remove(pendingAction);
        if (removed) {
            fireDialogQueueChanged(mPendingDialogActionQueue.size());
        }
    }

    private void clearDialogQueue() {
        if (mPendingDialogActionQueue.isEmpty()) {
            return;
        }
        mPendingDialogActionQueue.clear();
        fireDialogQueueChanged(mPendingDialogActionQueue.size());
    }

    public boolean isOverlayOpened() {
        return mOpenedOverlays != OVERLAY_TYPE_NONE;
    }

    @Override
    public void onAccessibilityStateChanged(boolean enabled) {
        // Propagate this to all elements that need it
    }

    /**
     * Returns true, if a main view needs to hide informational text. Specifically, when overlay UIs
     * except banner is shown, the informational text needs to be hidden for clean UI.
     */

    /**
     * Updates and shows channel banner if it's needed.
     */
    public void updateChannelBannerAndShowIfNeeded(@ChannelBannerUpdateReason int reason) {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateChannelBannerAndShowIfNeeded(reason=" + reason + ")");
        }
    }

    @TvOverlayType
    private int convertSceneToOverlayType(@TvTransitionManager.SceneType int sceneType) {
        switch (sceneType) {
            case TvTransitionManager.SCENE_TYPE_CHANNEL_BANNER:
                return OVERLAY_TYPE_SCENE_CHANNEL_BANNER;
            case TvTransitionManager.SCENE_TYPE_INPUT_BANNER:
                return OVERLAY_TYPE_SCENE_INPUT_BANNER;
            case TvTransitionManager.SCENE_TYPE_KEYPAD_CHANNEL_SWITCH:
                return OVERLAY_TYPE_SCENE_KEYPAD_CHANNEL_SWITCH;
            case TvTransitionManager.SCENE_TYPE_SELECT_INPUT:
                return OVERLAY_TYPE_SCENE_SELECT_INPUT;
            case TvTransitionManager.SCENE_TYPE_EMPTY:
            default:
                return OVERLAY_TYPE_NONE;
        }
    }

    private void onOverlayOpened(@TvOverlayType int overlayType) {
        if (Log.INCLUDE) {
            Log.d(TAG, "Overlay opened:  " + toBinaryString(overlayType));
        }
        mOpenedOverlays |= overlayType;
        if (Log.INCLUDE) {
            Log.d(TAG, "Opened overlays: " + toBinaryString(mOpenedOverlays));
        }
        mHandler.removeMessages(MSG_OVERLAY_CLOSED);
    }

    private void onOverlayClosed(@TvOverlayType int overlayType) {
        if (Log.INCLUDE) {
            Log.d(TAG, "Overlay closed:  " + toBinaryString(overlayType));
        }
        mOpenedOverlays &= ~overlayType;
        if (Log.INCLUDE) {
            Log.d(TAG, "Opened overlays: " + toBinaryString(mOpenedOverlays));
        }
        mHandler.removeMessages(MSG_OVERLAY_CLOSED);
        // Show the main menu again if there are no pop-ups or banners only.
        // The main menu should not be shown when the activity is in paused state.
        boolean menuAboutToShow = false;
        if (canExecuteCloseAction()) {
            mHandler.sendEmptyMessage(MSG_OVERLAY_CLOSED);
        }
        // Don't set screen name to main if the overlay closing is a banner
        // or if a non banner overlay is still open
        // or if we just opened the menu
        if (overlayType != OVERLAY_TYPE_SCENE_CHANNEL_BANNER
                && overlayType != OVERLAY_TYPE_SCENE_INPUT_BANNER
                && isOnlyBannerOrNoneOpened()
                && !menuAboutToShow) {
        }
    }

    public void hidePipOverlay() {
        if (!isPipShowing()) {
            return;
        }
        mPipContainerWindow.dismiss();
    }

    public boolean isPipShowing() {
        return mPipContainerWindow != null;
    }

    private PipInfo pipInfo;

    public void showPipOverlay(Channel channel, PipContainerWindow.OnPIPInteractor onPIPInteractor) {
        if (pipInfo == null) {
            pipInfo = upManager.getPipInfo();
        }
        String serviceId = pipInfo.getServiceId();
        //check validation of serviceId;
        if (mChannelDataManager.getChannelByServiceId(serviceId) == null) {
            serviceId = null;
        }
        if (serviceId == null) {
            serviceId = channel.getServiceId();
        }

        if (mPipContainerWindow != null) {
            return;
        }

        mPipContainerWindow = new PipContainerWindow(mMainActivity,
                pipInfo.getPosition(), pipInfo.getSize(),
                onPIPInteractor, new PipContainerWindow.OnPipLifecycleListener() {
            @Override
            public void show(LifecycleObserver observer) {
                mMainActivity.getLifecycle().addObserver(observer);
            }

            @Override
            public void setState(int position, int size) {
                pipInfo.setPosition(position);
                pipInfo.setSize(size);
                upManager.setPipInfo(pipInfo);
            }

            @Override
            public void setPipServiceId(String serviceId) {
                pipInfo.setServiceId(serviceId);
                upManager.setPipInfo(pipInfo);
            }

            @Override
            public void setVisiblePipOptions(boolean visible) {
                if (visible) {
                    hideMiniGuideFragment();
                } else {
                    checkAndShowMiniGuide();
                }
            }

            @Override
            public void dismiss(LifecycleObserver observer) {
                mMainActivity.getLifecycle().removeObserver(observer);
                mPipContainerWindow = null;
                checkAndShowMiniGuide();
            }
        });
        mPipContainerWindow.show(serviceId);
    }


    public void showBootDialog(int type, Consumer<Boolean> authenticationListener) {
        boolean isMenuShowing = isMenuShowing();

        if (Log.INCLUDE) {
            Log.d(TAG, "showBootDialog, type=" + type
                    + ", isMenuShowing=" + isMenuShowing);
        }

        if (isMenuShowing) {
            return;
        }

        showBootDialog(type, new BootDialogFragment.BootProcessListener() {
            @Override
            public void onFinished(DialogFragment dialogFragment) {
                dialogFragment.dismiss();
                showMenu(REASON_HOME);

                authenticationListener.accept(true);

                CWMPServiceProvider.getInstance().notifyBooting(true, null);

                if (mBackendKnobsFlags instanceof DefaultBackendKnobsFlags) {
                    ((DefaultBackendKnobsFlags) mBackendKnobsFlags).removeInitializeWork(DefaultBackendKnobsFlags.WORK_BOOT);
                }
            }

            @Override
            public void onFinishedWithError(DialogFragment dialogFragment, int type, String errorCode, String errorMessage) {
                dialogFragment.dismiss();
                if (type == AuthenticationHandler.WHAT_ERROR_AUTHENTICATION_NOT_CONNECTED) {
                    showErrorByNetworkFail();
                } else {
                    showError(type, errorCode, errorMessage);
                }
                showMenu(REASON_HOME);
                authenticationListener.accept(false);
                CWMPServiceProvider.getInstance().notifyBooting(false, errorMessage);
                if (mBackendKnobsFlags instanceof DefaultBackendKnobsFlags) {
                    ((DefaultBackendKnobsFlags) mBackendKnobsFlags).removeInitializeWork(DefaultBackendKnobsFlags.WORK_BOOT);
                }
            }
        });
    }


    public void showBootDialog(int type, BootDialogFragment.BootProcessListener bootProcessListener) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showBootDialog, type=" + type);
        }

        BootDialogFragment bootDialogFragment = BootDialogFragment.newInstance(type);
        bootDialogFragment.addBootProcessListener(bootProcessListener);
        showDialogFragment(
                BootDialogFragment.CLASS_NAME,
                bootDialogFragment,
                false);
    }

    private void showError(int type, String errorCode, String errorMessage) {
        if (!mMainActivity.isActivityResumed()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showError. but not resume so return");
            }
            return;
        }
        ErrorDialogFragment errorDialogFragment =
                ErrorDialogFragment.newInstance(TAG, errorCode, errorMessage);
        errorDialogFragment.show(mMainActivity.getSupportFragmentManager(), ErrorDialogFragment.CLASS_NAME);
    }

    private void showErrorByNetworkFail() {
        if (!mMainActivity.isActivityResumed()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showErrorByNetworkFail. but not resume so return");
            }
            return;
        }
        FailNoticeDialogFragment twoButtonDialogFragment = new FailNoticeDialogFragment(MbsTaskCallback.REASON_NETWORK, mMainActivity);
        twoButtonDialogFragment.show(this, mMainActivity.getSupportFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
    }

    private void showChannelBannerIfHiddenBySideFragment() {
        if (mChannelBannerHiddenBySideFragment) {
            updateChannelBannerAndShowIfNeeded(UPDATE_CHANNEL_BANNER_REASON_FORCE_SHOW);
        }
    }

    private String toBinaryString(int value) {
        return String.format("0b%" + NUM_OVERLAY_TYPES + "s", Integer.toBinaryString(value))
                .replace(' ', '0');
    }

    private boolean canExecuteCloseAction() {
        return mMainActivity.isActivityResumed() && isOnlyBannerOrNoneOpened();
    }

    private boolean isOnlyBannerOrNoneOpened() {
        return (mOpenedOverlays
                & ~OVERLAY_TYPE_SCENE_CHANNEL_BANNER
                & ~OVERLAY_TYPE_SCENE_INPUT_BANNER)
                == 0;
    }

    /**
     * Runs a given {@code action} after all the overlays are closed.
     */
    public void runAfterOverlaysAreClosed(Runnable action) {
        if (canExecuteCloseAction()) {
            action.run();
        } else {
            mPendingActions.add(action);
        }
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onKeyDown : " + keyCode);
        }
        if (KeyEventManager.isNavigationKey(keyCode)) {
            if (mProgramGuide != null && mProgramGuide.isActive()) {
                return true;
            }
        }
        if (KeyEventManager.isExitKey(keyCode)) {
            if (mProgramGuide != null && mProgramGuide.isActive()) {
                hideOverlays(FLAG_HIDE_OVERLAYS_DEFAULT | FLAG_HIDE_OVERLAYS_KEEP_MINIGUIDE);
                return true;
            }
        }

        if (keyCode == KeyEvent.KEYCODE_INFO) {
            return isPipShowing();
        }


        return isTimeShiftKeyDown(keyCode, event);
    }

    public boolean isTimeShiftKeyDown(int keyCode, KeyEvent event) {
        if (!isTimeShiftActive()) {
            return false;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
            case KeyEvent.KEYCODE_PROG_RED:
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_PROG_BLUE:
            case KeyEvent.KEYCODE_MEDIA_STOP:
            case KeyEvent.KEYCODE_PROG_YELLOW:
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
            case KeyEvent.KEYCODE_PROG_GREEN:

                return true;
        }
        return false;
    }

    public boolean onKeyNum(int keyCode, KeyEvent event) {

        if (KeyEventManager.isNumberKey(keyCode)) {
            showDCADialogFragment(keyCode);
            return true;
        }

        return false;
    }

    public boolean onKeyTvKey(int keyCode, KeyEvent event) {
        boolean consume = false;
        if (KeyEventManager.isTvKey(keyCode)) {
            if (!checkWithNetwork()) {
                consume = true;
            }
        }

        if (KeyEventManager.isNumberKey(keyCode)) {
            if (checkWithNetwork()) {
                showDCADialogFragment(keyCode);
            }
            consume = true;
        }
        return consume;
    }


    public void showProgramGuide() {
        if (mProgramGuide == null) {
            return;
        }
        mProgramGuide.show(
                () -> hideOverlays(TvOverlayManager.FLAG_HIDE_OVERLAYS_KEEP_PROGRAM_GUIDE));
    }

    public void showProgramGuide(int categoryType) {
        if (mProgramGuide == null) {
            return;
        }
        mProgramGuide.show(() -> {
            hideOverlays(TvOverlayManager.FLAG_HIDE_OVERLAYS_KEEP_PROGRAM_GUIDE);
        }, categoryType);
    }

    public boolean isProgramGuideVisible() {
        return mProgramGuide != null && mProgramGuide.isActive();
    }

    public void hideProgramGuide() {
        hideProgramGuide(true);
    }

    public void hideProgramGuide(boolean withTune) {
        if (!isProgramGuideVisible()) {
            return;
        }
        mProgramGuide.hide(withTune);
    }

    public void dismissOverlayDialog(SafeDismissDialogFragment dismissDialogFragment) {
        PendingDialogAction dialogAction = new PendingDialogAction(
                dismissDialogFragment.getClass().getName(), dismissDialogFragment, false, false);
        dialogAction.dismiss();
    }

    public void startTimeShift(Channel channel, Program program, long startTime, JasTvView tvView) {
        if (Log.INCLUDE) {
            Date sDate = (startTime > 0) ? new Date(startTime) : null;
            Log.d(TAG, "startTimeShift() | channel=" + channel.getDisplayName() + ", startTime=" + sDate);
        }

        hideOverlays(TvOverlayManager.FLAG_HIDE_OVERLAYS_KEEP_TUNE);
        Bundle bundle = new Bundle();
        bundle.putString(TimeShiftFragment.KEY_CHANNEL_ID, channel.getServiceId());
        bundle.putParcelable(TimeShiftFragment.KEY_PROGRAM, (ProgramImpl) program);
        bundle.putLong(TimeShiftFragment.KEY_START_TIME, startTime);
        TimeShiftFragment timeShiftFragment = TimeShiftFragment.newInstance(bundle, tvView);
        FragmentManager fragmentManager = mMainActivity.getSupportFragmentManager();
        Fragment prevFragment = fragmentManager.findFragmentByTag(TimeShiftFragment.CLASS_NAME);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (prevFragment != null) {
            ft.remove(prevFragment);
        }
        ft.add(mSceneContainer.getId(), timeShiftFragment, TimeShiftFragment.CLASS_NAME);
        if (timeShiftFragment.isStateSaved()) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }
    }

    public void startTimeShift(Channel channel, Program program, JasTvView tvView) {
        startTimeShift(channel, program, -1, tvView);
    }

    public boolean isTimeShiftActive() {
        if (Log.INCLUDE) {
            Log.d(TAG, "isTimeShiftActive()");
        }
        FragmentManager fragmentManager = mMainActivity.getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(TimeShiftFragment.CLASS_NAME);
        boolean result = (fragment != null);
        if (Log.INCLUDE) {
            Log.d(TAG, "isTimeShiftActive() return:" + result);
        }
        return result;
    }

    public TimeShiftFragment getTimeShiftFragment() {
        if (Log.INCLUDE) {
            Log.d(TAG, "getTimeShiftFragment()");
        }
        FragmentManager fragmentManager = mMainActivity.getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(TimeShiftFragment.CLASS_NAME);
        if (fragment == null || !(fragment instanceof TimeShiftFragment)) {
            return null;
        } else {
            return ((TimeShiftFragment) fragment);
        }
    }


    public void stopTimeShift() {
        if (Log.INCLUDE) {
            Log.d(TAG, "stopTimeShift()");
        }
        FragmentManager fragmentManager = mMainActivity.getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(TimeShiftFragment.CLASS_NAME);
        if (fragment == null) {
            return;
        }
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.remove(fragment);
        if (fragment.isStateSaved()) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }
    }

    public void hideOverlays(@HideOverlayFlag int flags) {
        if (Log.INCLUDE) {
            Log.d(TAG, "hideOverlay() flags:0b" + Integer.toBinaryString(flags));
        }
        if ((flags & FLAG_HIDE_OVERLAYS_KEEP_DIALOG) != 0) {
            // Keeps the dialog.
        } else {
            clearOverlayDialog();
        }
        boolean withAnimation = (flags & FLAG_HIDE_OVERLAYS_WITHOUT_ANIMATION) == 0;


        if ((flags & FLAG_HIDE_OVERLAYS_KEEP_MENU) != 0) {
            // Keeps the menu.
        } else {
            closeFragment(MenuNavigationFragment.CLASS_NAME);
        }
        if ((flags & FLAG_HIDE_OVERLAYS_KEEP_SCENE) != 0) {
            // Keeps the current scene.
        } else {
            mTransitionManager.goToEmptyScene(withAnimation);
        }
        if ((flags & FLAG_HIDE_OVERLAYS_KEEP_SIDE_PANELS) != 0) {
            // Keeps side panels.
        }
        if ((flags & FLAG_HIDE_OVERLAYS_KEEP_PROGRAM_GUIDE) != 0) {
            // Keep the program guide.
        } else {
            hideProgramGuide(flags != FLAG_HIDE_OVERLAYS_KEEP_TUNE);
        }
        if ((flags & FLAG_HIDE_OVERLAYS_KEEP_PIP) != 0) {
            // Keep the program guide.
        } else {
            hidePipOverlay();
        }
        if ((flags & FLAG_HIDE_OVERLAYS_KEEP_MINIGUIDE) != 0) {
            // Keep the mini guide.
        } else {
            hideMiniGuideFragment(false);
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onKeyDown : " + keyCode);
        }
        if (KeyEventManager.isNavigationKey(keyCode)) {
            if (mProgramGuide != null && mProgramGuide.isActive()) {
                return true;
            }
        }
        if (KeyEventManager.isExitKey(keyCode)) {
            if (mProgramGuide != null && mProgramGuide.isActive()) {
                hideOverlays(TvOverlayManager.FLAG_HIDE_OVERLAYS_DEFAULT);
                return true;
            }
        }
        if (keyCode == KeyEvent.KEYCODE_INFO) {
            return isPipShowing();
        }

        return isTimeShiftKeyDown(keyCode, event);
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef(
            flag = true,
            value = {
                    OVERLAY_TYPE_NONE,
                    OVERLAY_TYPE_MENU,
                    OVERLAY_TYPE_SIDE_FRAGMENT,
                    OVERLAY_TYPE_DIALOG,
                    OVERLAY_TYPE_GUIDE,
                    OVERLAY_TYPE_SCENE_CHANNEL_BANNER,
                    OVERLAY_TYPE_SCENE_INPUT_BANNER,
                    OVERLAY_TYPE_SCENE_KEYPAD_CHANNEL_SWITCH,
                    OVERLAY_TYPE_SCENE_SELECT_INPUT,
                    OVERLAY_TYPE_FRAGMENT,
                    OVERLAY_TYPE_DCA,
                    OVERLAY_TYPE_MINI_GUIDE
            })
    public @interface TvOverlayType {
    }

    private static class TvOverlayHandler extends WeakHandler<TvOverlayManager> {
        TvOverlayHandler(TvOverlayManager ref) {
            super(ref);
        }

        @Override
        public void handleMessage(Message msg, @NonNull TvOverlayManager tvOverlayManager) {
            switch (msg.what) {
                case MSG_OVERLAY_CLOSED:
                    if (!tvOverlayManager.canExecuteCloseAction()) {
                        return;
                    }
                    if (!tvOverlayManager.mPendingActions.isEmpty()) {
                        Runnable action = tvOverlayManager.mPendingActions.get(0);
                        tvOverlayManager.mPendingActions.remove(action);
                        action.run();
                    }
                    break;
            }
        }
    }

    private class PendingDialogAction {
        private final String mTag;
        private final SafeDismissDialogFragment mDialog;
        private final boolean mKeepSidePanelHistory;
        private final boolean mKeepProgramGuide;

        PendingDialogAction(
                String tag,
                SafeDismissDialogFragment dialog,
                boolean keepSidePanelHistory,
                boolean keepProgramGuide) {
            mTag = tag;
            mDialog = dialog;
            mKeepSidePanelHistory = keepSidePanelHistory;
            mKeepProgramGuide = keepProgramGuide;
        }

        void run() {
            showDialogFragment(mTag, mDialog, mKeepSidePanelHistory, mKeepProgramGuide);
        }

        void dismiss() {
            dismissDialogFragment(mTag, mDialog, mKeepSidePanelHistory, mKeepProgramGuide);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PendingDialogAction that = (PendingDialogAction) o;

            if (mTag != null ? !mTag.equals(that.mTag) : that.mTag != null) return false;
            return mDialog != null ? mDialog.equals(that.mDialog) : that.mDialog == null;
        }

        @Override
        public int hashCode() {
            int result = mTag != null ? mTag.hashCode() : 0;
            result = 31 * result + (mDialog != null ? mDialog.hashCode() : 0);
            return result;
        }
    }

    public interface OnOverlayDialogQueueListener {
        void onSizeChanged(int size);
    }
}
