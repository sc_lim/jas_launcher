/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget.icons;

import android.graphics.Bitmap;

/**
 * Created by mc.kim on 23,12,2019
 */
public class BitmapInfo {
    public static final Bitmap LOW_RES_ICON = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
    public Bitmap icon;
    public int color;

    public void applyTo(BitmapInfo info) {
        info.icon = icon;
        info.color = color;
    }

    public final boolean isLowRes() {
        return LOW_RES_ICON == icon;
    }

    public static BitmapInfo fromBitmap(Bitmap bitmap) {
        return fromBitmap(bitmap, null);
    }

    public static BitmapInfo fromBitmap(Bitmap bitmap, ColorExtractor dominantColorExtractor) {
        BitmapInfo info = new BitmapInfo();
        info.icon = bitmap;
        info.color = dominantColorExtractor != null
                ? dominantColorExtractor.findDominantColorByHue(bitmap)
                : 0;
        return info;
    }
}
