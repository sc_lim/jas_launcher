/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;

import com.altimedia.util.Log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.dm.message.obj.Message;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.CouponNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.message.MessageDetailDialogFragment;

public class CouponNoticeManager {
    private final String TAG = CouponNoticeManager.class.getSimpleName();

    private ArrayList<CouponNotification> notificationList = new ArrayList<>();
    private Context context;

    private CouponNoticeDialogFragment couponNoticeDialog;

    private static CouponNoticeManager instance = null;

    public static CouponNoticeManager getInstance() {
        if (instance == null) {
            instance = new CouponNoticeManager();
        }
        return instance;
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive: intent=" + intent);
            }
            String action = intent.getAction();
            if (PushMessageReceiver.ACTION_UPDATED_COUPON.equalsIgnoreCase(action)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReceive: action="+action);
                }
                Serializable typeData = intent.getSerializableExtra(PushMessageReceiver.KEY_TYPE);
                if (typeData == null) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onReceive: not contain type so return");
                    }
                    return;
                }
                try {
                    PushMessageReceiver.CouponUpdateType type = (PushMessageReceiver.CouponUpdateType) typeData;
                    String id = intent.getStringExtra(PushMessageReceiver.KEY_MESSAGE_ID);
                    String title = intent.getStringExtra(PushMessageReceiver.KEY_MESSAGE_TITLE);
                    String description = intent.getStringExtra(PushMessageReceiver.KEY_MESSAGE_DESC);
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onReceive: id="+id);
                        Log.d(TAG, "onReceive: title="+title);
                        Log.d(TAG, "onReceive: description="+description);
                    }
                    if(id == null || id.isEmpty()){
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onReceive: this notification ignored since data is invalid");
                        }
                        return;
                    }
                    long dateTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
                    try {
                        String formattedDate = intent.getStringExtra(PushMessageReceiver.KEY_DATE);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        dateTime = dateFormat.parse(formattedDate).getTime();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    Message message = new Message(id, title, description, dateTime);
                    CouponNotification couponNotification = new CouponNotification(type, message);
                    notificationList.add(couponNotification);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    };

    public void init(Context context){
        this.context = context;

        registerCouponNotiReceiver();
    }

    public void despose(){
        unregisterCouponNotiReceiver();
    }

    private void registerCouponNotiReceiver() {
        if(context == null) return;

        if (Log.INCLUDE) {
            Log.d(TAG, "registerCouponNotiReceiver: register broadcast receiver");
        }
        if(mBroadcastReceiver != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(PushMessageReceiver.ACTION_UPDATED_COUPON);
            LocalBroadcastManager.getInstance(context).registerReceiver(mBroadcastReceiver, intentFilter);
        }
    }

    private void unregisterCouponNotiReceiver() {
        if(context == null) return;

        if (Log.INCLUDE) {
            Log.d(TAG, "unregisterCouponNotiReceiver: unregister broadcast receiver");
        }
        if(mBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(mBroadcastReceiver);
        }
    }

    public void showCouponNotification(final Activity activity){
        if(notificationList == null || notificationList.isEmpty()) return;

        if (Log.INCLUDE) {
            Log.d(TAG, "showCouponNotification: notification size="+notificationList.size());
        }
        for(int i=0; i<notificationList.size(); i++) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showCouponNotification: notification#"+i);
            }
            CouponNotification couponNotification = notificationList.get(i);
            if(couponNoticeDialog != null && couponNoticeDialog.isVisible()){
                ;
            }else {
                try {
                    couponNoticeDialog = CouponNoticeDialogFragment.newInstance(couponNotification.getType(), couponNotification.getMessage());
                    couponNoticeDialog.setOnButtonActionListener(new CouponNoticeDialogFragment.OnButtonActionListener() {
                        @Override
                        public void onLinkToMessage(PushMessageReceiver.CouponUpdateType type, Message message) {
                            try {
                                if(activity != null && activity instanceof LauncherActivity) {
                                    MessageDetailDialogFragment fragment = MessageDetailDialogFragment.newInstance(message);
                                    fragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            if (!notificationList.isEmpty()) {
                                                showCouponNotification(activity);
                                            }
                                        }
                                    });
                                    TvOverlayManager tvOverlayManager = ((LauncherActivity) activity).getTvOverlayManager();
                                    tvOverlayManager.showDialogFragment(fragment);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            removeCouponNotification(message.getId());
                            couponNoticeDialog.dismiss();
                        }

                        @Override
                        public void onClosed(PushMessageReceiver.CouponUpdateType type, Message message) {
                            removeCouponNotification(message.getId());
                            couponNoticeDialog.dismiss();

                            if (!notificationList.isEmpty()) {
                                showCouponNotification(activity);
                            }
                        }
                    });

                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if(activity != null && activity instanceof LauncherActivity) {
                                    ((LauncherActivity) activity).getSupportFragmentManager().executePendingTransactions();
                                    TvOverlayManager tvOverlayManager = ((LauncherActivity) activity).getTvOverlayManager();
                                    tvOverlayManager.showDialogFragment(couponNoticeDialog);
                                }
                            } catch (Exception e){
                                e.printStackTrace();
                                if(couponNoticeDialog != null){
                                    couponNoticeDialog.dismiss();
                                    couponNoticeDialog = null;
                                }
                            }
                        }
                    }, 100);
                } catch (Exception e){
                    e.printStackTrace();
                    if(couponNoticeDialog != null){
                        couponNoticeDialog.dismiss();
                        couponNoticeDialog = null;
                    }
                }
            }
            return;
        }
    }

    public void removeCouponNotification(String id){
        if(notificationList == null || notificationList.isEmpty()) return;

        for(CouponNotification couponNotification : notificationList){
            Message message = couponNotification.getMessage();
            try {
                if (Log.INCLUDE) {
                    Log.d(TAG, "removeCouponNotification() id="+id);
                    Log.d(TAG, "removeCouponNotification() message_id="+message.getId());
                }
                if (message.getId().equals(id)) {
                    notificationList.remove(couponNotification);
                    break;
                }
            }catch (Exception e){
                e.printStackTrace();
                notificationList.remove(couponNotification);
            }
        }
    }

    private class CouponNotification {

        private PushMessageReceiver.CouponUpdateType type;
        private Message message;

        CouponNotification(PushMessageReceiver.CouponUpdateType type, Message message){
            this.type = type;
            this.message = message;
        }

        public PushMessageReceiver.CouponUpdateType getType(){
            return type;
        }

        public Message getMessage(){
            return message;
        }
    }
}
