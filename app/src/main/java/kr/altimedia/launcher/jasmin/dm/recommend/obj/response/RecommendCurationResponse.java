/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.recommend.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.def.NullToEmptyDeserializer;
import kr.altimedia.launcher.jasmin.dm.recommend.obj.RcmdCategory;
import kr.altimedia.launcher.jasmin.dm.recommend.obj.RcmdContent;

/**
 * Created by mc.kim on 02,06,2020
 */
public class RecommendCurationResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    @JsonAdapter(NullToEmptyDeserializer.class)
    private RecommendCurationResult data;

    protected RecommendCurationResponse(Parcel in) {
        super(in);
        data = in.readParcelable(RecommendCurationResult.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(data, flags);
    }

    public List<RcmdContent> getData() {
        if (data == null || data.rcdList == null) {
            return new ArrayList<>();
        }
        return data.rcdList;
    }


    private static class RecommendCurationResult implements Parcelable {
        @Expose
        @SerializedName("MENU_CNT")
        private int menuCnt;

        @Expose
        @SerializedName("MENU_LIST")
        private List<RcmdCategory> menuList;

        @Expose
        @SerializedName("ITEM_LIST")
        private List<RcmdContent> rcdList;

        public int getMenuCnt() {
            return menuCnt;
        }

        public List<RcmdContent> getRcdList() {
            return rcdList;
        }

        public List<RcmdCategory> getMenuList() {
            return menuList;
        }

        protected RecommendCurationResult(Parcel in) {
            menuCnt = in.readInt();
            menuList = in.createTypedArrayList(RcmdCategory.CREATOR);
            rcdList = in.createTypedArrayList(RcmdContent.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(menuCnt);
            dest.writeTypedList(menuList);
            dest.writeTypedList(rcdList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<RecommendCurationResult> CREATOR = new Creator<RecommendCurationResult>() {
            @Override
            public RecommendCurationResult createFromParcel(Parcel in) {
                return new RecommendCurationResult(in);
            }

            @Override
            public RecommendCurationResult[] newArray(int size) {
                return new RecommendCurationResult[size];
            }
        };

        @Override
        public String toString() {
            return "RecommendCurationResult{" +
                    "menuCnt=" + menuCnt +
                    ", menuList=" + menuList +
                    ", rcdList=" + rcdList +
                    '}';
        }
    }

}
