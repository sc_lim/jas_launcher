/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.recycler;

import android.content.Context;
import android.util.AttributeSet;

import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;


/**
 * Created by mc.kim on 16,12,2019
 */
public class WrappedVericalGridView extends VerticalGridView {
    public WrappedVericalGridView(Context context) {
        super(context);
    }

    public WrappedVericalGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WrappedVericalGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int heightSpec;


        if (getLayoutParams().height == LayoutParams.WRAP_CONTENT) {
            // The great Android "hackatlon", the love, the magic.
            // The two leftmost bits in the height measure spec have
            // a special meaning, hence we can't use them to describe height.

            heightSpec =
                    MeasureSpec.makeMeasureSpec(
                            Integer.MAX_VALUE >> 2, MeasureSpec.EXACTLY);
        } else {
            // Any other height should be respected as is.
            heightSpec = heightMeasureSpec;
        }

        super.onMeasure(widthMeasureSpec, heightSpec);
    }
}
