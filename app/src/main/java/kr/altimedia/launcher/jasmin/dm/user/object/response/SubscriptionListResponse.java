/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscribedContent;

public class SubscriptionListResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private SubscriptionList data;

    protected SubscriptionListResponse(Parcel in) {
        super(in);
        data = in.readParcelable(SubscriptionListResponse.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    public List<SubscribedContent> getSubscribedContentList() {
        return data.list;
    }

    private static class SubscriptionList implements Parcelable {
        public static final Creator<SubscriptionList> CREATOR = new Creator<SubscriptionListResponse.SubscriptionList>() {
            @Override
            public SubscriptionListResponse.SubscriptionList createFromParcel(Parcel in) {
                return new SubscriptionListResponse.SubscriptionList(in);
            }

            @Override
            public SubscriptionListResponse.SubscriptionList[] newArray(int size) {
                return new SubscriptionListResponse.SubscriptionList[size];
            }
        };
        @Expose
        @SerializedName("list")
        public List<SubscribedContent> list;

        protected SubscriptionList(Parcel in) {
            in.readList(list, SubscribedContent.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(list);
        }

        @Override
        public int describeContents() {
            return 0;
        }
    }
}
