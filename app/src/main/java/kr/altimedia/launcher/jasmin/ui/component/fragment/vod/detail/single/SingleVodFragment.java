/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.single;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.CheckBox;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.dm.payment.type.ContentType;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.DetailFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodDetailsDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.managerProvider;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.ProductButtonPresenterSelector;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.RelatedVodHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.VodListRowPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.VodPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonManager2;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.VodProductType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.single.presenter.SingleDetailsRowPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.PaymentItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPurchaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPurchaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentButton;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentWrapper;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment.SubscriptionPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.util.task.UserTaskManager;
import kr.altimedia.launcher.jasmin.ui.util.task.VodTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.row.VodOverviewRow;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;


public class SingleVodFragment extends DetailFragment {
    public static final String CLASS_NAME = SingleVodFragment.class.getName();
    private static final String TAG = SingleVodFragment.class.getSimpleName();

    private static final String KEY_CONTENT = "CONTENT";

    private final int DETAIL_ROW = 0;
    private final int RELATED_ROW = 1;

    private final float TARGET_ALPHA = 0.3f;

    private Content content;

    private View focusedView;

    private ArrayObjectAdapter mAdapter;
    private ClassPresenterSelector mPresenterSelector;
    private ProductButtonManager2 productButtonManager;
    private String categoryId;

    public SingleVodFragment() {
        super();
    }

    public static SingleVodFragment newInstance(Content content) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_CONTENT, content);
        SingleVodFragment fragment = new SingleVodFragment();
        fragment.setResourceId(R.layout.fragment_vod_details);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = getArguments().getParcelable(KEY_CONTENT);
        categoryId = content.getCategoryId();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (focusedView != null) {
            focusedView.requestFocus();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPresenterSelector = new ClassPresenterSelector();
        mAdapter = new ArrayObjectAdapter(mPresenterSelector);
        setAdapter(mAdapter);

        loadContent(true);
        initShadowCallback(R.id.detailInfoLayout, view.findViewById(R.id.bottomShadow));

        initChildFocusListener(view);
        setOnItemViewClickedListener(new ItemViewClickedListener());
    }

    private void loadContent(boolean isInit) {
        vodTaskManager.setOnCompleteLoadContentDetailListener(new VodTaskManager.OnCompleteLoadContentDetailListener<String, ContentDetailInfo>() {
            @Override
            public void needLoading(boolean isLoading) {
                if (isLoading) {
                    showProgress();
                } else {
                    hideProgress();
                }
            }

            @Override
            public void onSuccessLoadContentDetail(String id, ContentDetailInfo result) {
                ContentDetailInfo contentDetailInfo = result;
                if (Log.INCLUDE) {
                    Log.d(TAG, "onCompleteLoadContentDetail, contentDetailInfo : " + contentDetailInfo);
                }

                content = contentDetailInfo.getContent();
                if (isInit) {
                    initList();
                } else {
                    updatePurchaseButtons();
                }

                VodPaymentDialogFragment vodPaymentDialogFragment = (VodPaymentDialogFragment) findFragmentByTag(VodPaymentDialogFragment.CLASS_NAME);
                SubscriptionPaymentDialogFragment subscriptionPaymentDialogFragment = (SubscriptionPaymentDialogFragment) findFragmentByTag(SubscriptionPaymentDialogFragment.CLASS_NAME);
                if (vodPaymentDialogFragment != null || subscriptionPaymentDialogFragment != null) {
                    if (mVodRowFragment != null && mVodRowFragment.getVerticalGridView() != null) {
                        mVodRowFragment.getVerticalGridView().requestFocus();
                    }

                    if (vodPaymentDialogFragment != null) {
                        vodPaymentDialogFragment.showPurchaseSuccessFragment(mSenderId, vodPaymentDialogFragment.WORK_DETAIL_CALL);
                    } else if (subscriptionPaymentDialogFragment != null) {
                        subscriptionPaymentDialogFragment.showPurchaseSuccessFragment(true, mSenderId, subscriptionPaymentDialogFragment.WORK_DETAIL_CALL);
                    }
                }
            }

            @Override
            public void onFailLoadContentDetail(int key) {
                VodPaymentDialogFragment vodPaymentDialogFragment = (VodPaymentDialogFragment) findFragmentByTag(VodPaymentDialogFragment.CLASS_NAME);
                if (vodPaymentDialogFragment != null) {
                    vodPaymentDialogFragment.dismiss();
                }

                showLoadFailDialog(true, key);
            }

            @Override
            public void onError(String id, String errorCode, String message) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "loadContent, onError");
                }

                VodPaymentDialogFragment vodPaymentDialogFragment = (VodPaymentDialogFragment) findFragmentByTag(VodPaymentDialogFragment.CLASS_NAME);
                if (vodPaymentDialogFragment != null) {
                    vodPaymentDialogFragment.dismiss();
                }

                showLoadErrorDialog(true, errorCode, message);
            }
        });
        vodTaskManager.loadContentDetail(content.getContentGroupId(), content.getCategoryId());
    }

    private void initList() {
        SingleDetailsRowPresenter singleDetailsRowPresenter = new SingleDetailsRowPresenter();
        singleDetailsRowPresenter.setManagerProvider(
                new managerProvider() {
                    @Override
                    public TvOverlayManager getTvOverlayManager() {
                        return mChangeVodDetailListener.getTvOverlayManager();
                    }

                    @Override
                    public FragmentManager getFragmentManager() {
                        return SingleVodFragment.this.getFragmentManager();
                    }
                });
        mPresenterSelector.addClassPresenter(VodOverviewRow.class, singleDetailsRowPresenter);
        mPresenterSelector.addClassPresenter(ListRow.class, new VodListRowPresenter(R.layout.presenter_vod_horizontal_row));

        setupDetailsOverviewRow();
        loadRelatedMovieListRow();
    }

    private SingleDetailShadowHelper mSingleDetailShadowHelper = null;

    private void initShadowCallback(final int layer, final View bottomShadow) {
        mSingleDetailShadowHelper = new SingleDetailShadowHelper(layer, TARGET_ALPHA, bottomShadow);
        setOnScrollOffsetCallback(mSingleDetailShadowHelper);
    }

    private void initChildFocusListener(View view) {
        mRootView.setOnChildFocusListener(new BrowseFrameLayout.OnChildFocusListener() {
            @Override
            public boolean onRequestFocusInDescendants(int var1, Rect var2) {
                return true;
            }

            @Override
            public void onRequestChildFocus(View child, View focused) {
                focusedView = focused;

                int selectedIndex = mVodRowFragment.getSelectedPosition();
                if (selectedIndex == DETAIL_ROW) {
                    VodListRowPresenter.ViewHolder viewHolder = (VodListRowPresenter.ViewHolder) mVodRowFragment.getRowViewHolder(RELATED_ROW);

                    if (viewHolder == null) {
                        VodListRowPresenter vodListRowPresenter = (VodListRowPresenter) mPresenterSelector.getPresenters()[RELATED_ROW];
                        vodListRowPresenter.setHistoryView(focused);
                    } else {
                        viewHolder.setHistoryView(focused);
                    }
                }
            }
        });
    }

    @Override
    protected Rect getPadding() {
        Resources resource = getContext().getResources();
//        int left = (int) (resource.getDimension(R.dimen.vod_detail_padding_left) - resource.getDimension(R.dimen.related_vod_scale_padding));
        int top = (int) resource.getDimension(R.dimen.vod_detail_padding_top);
        Rect padding = new Rect(0, top, 0, top);
        return padding;
    }

    private void loadRelatedMovieListRow() {
        vodTaskManager.setOnCompleteLoadRelateListener(new VodTaskManager.OnCompleteLoadRelateListener<String, List<Content>>() {
            @Override
            public void needLoading(boolean isLoading) {
                if (isLoading) {
                    showProgress();
                } else {
                    hideProgress();
                }
            }

            @Override
            public void onSuccessLoadRelate(String id, List<Content> result) {
                setupRelatedMovieListRow(result);
            }

            @Override
            public void onFailLoadRelate(int key) {
//                showLoadFailDialog(false, key);
            }

            @Override
            public void onError(String id, String errorCode, String message) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "loadRelatedMovieListRow, onError");
                }

                showLoadErrorDialog(false, errorCode, message);
            }
        });

        vodTaskManager.loadRelatedList(content.getContentGroupId(), content.getCategoryId());
    }

    private ArrayObjectAdapter getRelatedRowAdapter(List<Content> movieDataList) {
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new VodPresenter());

        if (movieDataList != null) {
            listRowAdapter.addAll(0, movieDataList);
        }

        return listRowAdapter;
    }

    private void setupDetailsOverviewRow() {
        SingleDetailsRowPresenter detailsPresenter = (SingleDetailsRowPresenter) mPresenterSelector.getPresenters()[DETAIL_ROW];
        detailsPresenter.setOnPosterClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isWatchable = content.isWatchable();

                if (Log.INCLUDE) {
                    Log.d(TAG, "onClick, movie : " + content.getTitle() + ", isWatchable : " + isWatchable);
                }

                if (isWatchable) {
                    mChangeVodDetailListener.watchMainVideo(content);
                } else {
                    mChangeVodDetailListener.watchTrailerVideo(content);
                }
            }
        });

        setPurchaseButtons();
    }

    private void setPurchaseButtons() {
        productButtonManager = new ProductButtonManager2(getContext());
        ArrayObjectAdapter objectAdapter = getButtonAction();

        VodOverviewRow row = new VodOverviewRow(ProductType.SINGLE, content);
        row.setActionsAdapter(objectAdapter);

        mAdapter.add(DETAIL_ROW, row);
    }

    private void setupRelatedMovieListRow(List<Content> relatedList) {
        HeaderItem header = new HeaderItem(0, R.string.related_contents);
        ArrayObjectAdapter relatedObjectAdapter = getRelatedRowAdapter(relatedList);

        VodListRowPresenter mListRowPresenter = (VodListRowPresenter) mPresenterSelector.getPresenters()[RELATED_ROW];
        mListRowPresenter.setHeaderPresenter(new RelatedVodHeaderPresenter());

        mAdapter.add(new ListRow(header, relatedObjectAdapter));
    }

    private void purchaseVod(Product product) {
        Bundle bundle = new Bundle();
        bundle.putString(VodPurchaseFragment.KEY_CONTENT_TITLE, content.getTitle());
        bundle.putString(VodPurchaseFragment.KEY_CONTENT_POSTER_URL, content.getPosterSourceUrl(Content.mPosterPurchaseRect.width(),
                Content.mPosterPurchaseRect.height()));
        bundle.putParcelable(VodPurchaseFragment.KEY_PRODUCT, product);

        TvOverlayManager tvOverlayManager = mChangeVodDetailListener.getTvOverlayManager();
        VodPurchaseDialogFragment vodPurchaseDialogFragment = VodPurchaseDialogFragment.newInstance(bundle);
        vodPurchaseDialogFragment.setOnPaymentClickListener(new PaymentItemBridgeAdapter.OnPaymentClickListener() {
            @Override
            public void onPaymentClick(Object item) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onPaymentClick, item : " + item);
                }

                if (item == null) {
                    vodPurchaseDialogFragment.dismiss();
                    return;
                }

                PaymentButton paymentButton = (PaymentButton) item;
                PaymentType paymentType = paymentButton.getType();
                PaymentWrapper paymentWrapper = vodPurchaseDialogFragment.getPaymentWrapper();
                PurchaseInfo purchaseInfo = new PurchaseInfo(ContentType.VOD, content.getTitle(),
                        product, paymentType, paymentWrapper);
                Parcelable option = (Parcelable) vodPurchaseDialogFragment.getPaymentOption(paymentType);

                VodPaymentDialogFragment vodPaymentDialogFragment = VodPaymentDialogFragment.newInstance(mSenderId, purchaseInfo, option);
                vodPaymentDialogFragment.setOnPaymentListener(new VodPaymentDialogFragment.OnPaymentCompleteListener() {
                    @Override
                    public void onComplete(boolean isSuccess) {
                        vodPurchaseDialogFragment.dismiss();
                        if (isSuccess) {
//                            updatePaymentResult();
                        }
                    }

                    @Override
                    public void onRequestWatch(boolean isRequest) {
                        vodPaymentDialogFragment.dismiss();
                        if (isRequest) {
                            mChangeVodDetailListener.watchMainVideo(content);
                        }
                    }

                    @Override
                    public void onCancelPayment() {
                        vodPaymentDialogFragment.dismiss();
                    }
                });

                tvOverlayManager.showDialogFragment(vodPaymentDialogFragment);
            }
        });
        tvOverlayManager.showDialogFragment(vodPurchaseDialogFragment);
    }

    private void purchaseSingle(Product product) {
        if (Log.INCLUDE) {
            Log.d(TAG, "purchaseSingle, product : " + product);
        }

        purchaseVod(product);
    }

    @Override
    protected void onPurchasePushMessage(String offerId) {
        super.onPurchasePushMessage(offerId);

        ((VodDetailsDialogFragment) getParentFragment()).loadUserDiscountInfo();

        ArrayList<Product> products = (ArrayList<Product>) content.getProductList();
        boolean isContains = isContainsOfferId(offerId, products);

        if (Log.INCLUDE) {
            Log.d(TAG, "onPurchasePushMessage, offerId : " + offerId + ", isContains : " + isContains);
        }

        if (isContains) {
            updatePaymentResult();
        }
    }

    private void updatePaymentResult() {
        loadContent(false);
    }

    private void updatePurchaseButtons() {
        if (Log.INCLUDE) {
            Log.d(TAG, "updatePurchaseButtons");
        }

        VodOverviewRow vodOverviewRow = new VodOverviewRow(ProductType.SINGLE, content);
        ArrayObjectAdapter objectAdapter = getButtonAction();
        vodOverviewRow.setActionsAdapter(objectAdapter);
        mAdapter.replace(DETAIL_ROW, vodOverviewRow);
    }

    @Override
    protected void updateDetail(Content content) {
        super.updateDetail(content);

        if (Log.INCLUDE) {
            Log.d(TAG, "updateDetail");
        }

        loadContent(false);
    }

    @Override
    protected void setContentLike(Content mContent) {
        super.setContentLike(mContent);

        boolean isLike = mContent.isLike();
        if (Log.INCLUDE) {
            Log.d(TAG, "setContentLike, isLike : " + isLike);
        }

        if (content.isLike() == mContent.isLike()) {
            return;
        }

        content.setLike(isLike);
        CheckBox vodLikeView = getView().findViewById(R.id.my_list_button);
        vodLikeView.setChecked(isLike);

        mChangeVodDetailListener.onVodLikeChange(isLike);
    }

    private void onClickContentLike(Presenter.ViewHolder viewHolder) {
        ProductButtonPresenterSelector.MyListButtonPresenter.ViewHolder vh = (ProductButtonPresenterSelector.MyListButtonPresenter.ViewHolder) viewHolder;
        vodTaskManager.setOnCompleteContentFavoriteListener(
                new VodTaskManager.OnCompleteContentFavoriteListener() {
                    @Override
                    public void needLoading(boolean isLoading) {
                        if (isLoading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onSuccessAddFavorite(Object id, Object result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onCompleteAddFavorite");
                        }

                        content.setLike(true);
                        vh.toggleMyList(content);
                        int textId = R.string.my_list_add;
                        JasminToast.makeToast(getContext(), textId);

                        mChangeVodDetailListener.onVodLikeChange(true);
                    }

                    @Override
                    public void onSuccessRemoveFavorite(Object id, Object result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onCompleteRemoveFavorite");
                        }

                        content.setLike(false);
                        int textId = R.string.my_list_remove;
                        JasminToast.makeToast(getContext(), textId);
                        vh.toggleMyList(content);

                        mChangeVodDetailListener.onVodLikeChange(false);
                    }

                    @Override
                    public void onFailFavorite(int key) {
                        showLoadFailDialog(false, key);
                    }

                    @Override
                    public void onError(Object id, String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onClickContentLike, onError");
                        }

                        showLoadErrorDialog(false, errorCode, message);
                    }
                });

        boolean isLike = content.isLike();
        vodTaskManager.onClickLikeContent(isLike, content.getContentGroupId(), VodTaskManager.VodLikeType.VOD);
    }

    private void showProgress() {
        VodDetailsDialogFragment vodDetailsDialogFragment = (VodDetailsDialogFragment) getParentFragment();
        vodDetailsDialogFragment.showProgress();
    }

    private void hideProgress() {
        if (vodTaskManager.isEmpty() && UserTaskManager.getInstance().isEmpty()) {
            VodDetailsDialogFragment vodDetailsDialogFragment = (VodDetailsDialogFragment) getParentFragment();
            vodDetailsDialogFragment.hideProgress();
        }
    }

    private ArrayObjectAdapter getButtonAction() {
        return productButtonManager.getProductButtons(content, (ArrayList<Product>) content.getProductList());
    }

    @Override
    public void onDestroyView() {
        mRootView.setOnChildFocusListener(null);
        mRootView.setOnFocusSearchListener(null);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        mAdapter = null;
        super.onDestroy();
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder var3, Row row) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onItemClicked, item: " + item + ", row : " + row);
            }

            if (item == null) {
                return;
            }

            if (row instanceof VodOverviewRow) {
                if (item instanceof ProductButtonItem) {
                    ProductButtonItem productButtonItem = (ProductButtonItem) item;
                    VodProductType type = productButtonItem.getProductType();
                    Product product = productButtonItem.getProduct();

                    if (Log.INCLUDE) {
                        Log.d(TAG, "onItemClicked, productButtonItem : " + productButtonItem + ", product : " + product);
                    }

                    if (type == VodProductType.WATCH) {
                        mChangeVodDetailListener.watchMainVideo(content);
                    } else if (type == VodProductType.RENT || type == VodProductType.BUY) {
                        purchaseSingle(product);
                    } else if (type == VodProductType.PACKAGE) {
                        mChangeVodDetailListener.showPackage(categoryId, productButtonManager.getPackageMainList());
                    } else if (type == VodProductType.SUBSCRIBE) {
                        purchaseSubscription(productButtonManager, content.getTermsAndConditions());
                    }
                } else if (item instanceof Content) {
                    onClickContentLike(itemViewHolder);
                }
            } else if (item instanceof Content) {
                mChangeVodDetailListener.replaceDetail((Content) item);
            }
        }
    }
}


