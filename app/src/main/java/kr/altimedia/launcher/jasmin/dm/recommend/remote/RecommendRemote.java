/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.recommend.remote;


import com.altimedia.util.Log;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.MbsUtil;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.recommend.api.RecommendApi;
import kr.altimedia.launcher.jasmin.dm.recommend.obj.RcmdContent;
import kr.altimedia.launcher.jasmin.dm.recommend.obj.response.RecommendCurationResponse;
import kr.altimedia.launcher.jasmin.dm.recommend.obj.response.RecommendRelatedResponse;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mc.kim on 02,06,2020
 */
public class RecommendRemote {
    private static final String TAG = RecommendRemote.class.getSimpleName();
    private final String CONTENTS_VOD = "VOD";
    private final String REC_TYPE_VOD = "ITEM_VOD";
    private final String REC_TYPE_CURATION = "CURATION";
    private final String REC_GB_CODE_DETAIL = "M";
    private final String SVC_CODE = "OTV";
    public static final int MAX_CNT = 100;
    private final int MIN_CNT = 1;
    private final String STB_VERSION = "1.0";
    private RecommendApi mRecommendApi;

    public RecommendRemote(RecommendApi recommendApi) {
        mRecommendApi = recommendApi;
    }

    private String getCurrentReqDate() {
        Date myDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYYMMDDHHmmss");
        String date = dateFormat.format(myDate);
        return date;
    }

    public List<Content> getRelatedContents(String language, String saId, String profileId, String subscriptionId,
                                            String contentId, String categoryId, int itemCnt, boolean containAdult, int rating) throws IOException, ResponseException, AuthenticationException {
        Call<RecommendRelatedResponse> call = mRecommendApi
                .getRelatedContent(CONTENTS_VOD,
                        REC_TYPE_VOD, REC_GB_CODE_DETAIL,
                        saId, subscriptionId, getCurrentReqDate(), "H",
                        SVC_CODE, contentId,
                        null, itemCnt, containAdult ? "Y" : "N", STB_VERSION, profileId, language.toUpperCase(), categoryId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<RecommendRelatedResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        RecommendRelatedResponse mRecommendData = response.body();
        List<RcmdContent> rcmdContents = mRecommendData.getData();
        List<Content> contents = new ArrayList<>();
        for (RcmdContent rcmdContent : rcmdContents) {
            contents.add(Content.buildContent(rcmdContent));
        }
        return contents;
    }


    public List<Content> getCurationRecommendContents(String language, String saId, String profileId, String subscriptionId, String categoryId,
                                                      int itemCnt, boolean containAdult, int rating) throws IOException, ResponseException, AuthenticationException {
        Call<RecommendCurationResponse> call = mRecommendApi
                .getCurationContent(CONTENTS_VOD,
                        REC_TYPE_CURATION,
                        saId, subscriptionId, getCurrentReqDate(),
                        SVC_CODE, MIN_CNT, itemCnt, containAdult ? "Y" : "N", STB_VERSION, profileId, categoryId, language.toUpperCase());
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<RecommendCurationResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        RecommendCurationResponse mRecommendData = response.body();
        List<RcmdContent> rcmdContents = mRecommendData.getData();
        List<Content> contents = new ArrayList<>();
        for (RcmdContent rcmdContent : rcmdContents) {
            contents.add(Content.buildContent(rcmdContent));
        }
        return contents;
    }
}
