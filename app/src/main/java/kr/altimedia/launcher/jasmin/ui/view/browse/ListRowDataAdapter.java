/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;


import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.ui.view.row.Row;

public class ListRowDataAdapter extends ObjectAdapter {
    public static final int ON_ITEM_RANGE_CHANGED = 2;
    public static final int ON_ITEM_RANGE_INSERTED = 4;
    public static final int ON_ITEM_RANGE_REMOVED = 8;
    public static final int ON_CHANGED = 16;
    private final ObjectAdapter mAdapter;
    int mLastVisibleRowIndex;
    final DataObserver mDataObserver;

    public ListRowDataAdapter(ObjectAdapter adapter) {
        super(adapter.getPresenterSelector());
        this.mAdapter = adapter;
        this.initialize();
        if (adapter.isImmediateNotifySupported()) {
            this.mDataObserver = new ListRowDataAdapter.SimpleDataObserver();
        } else {
            this.mDataObserver = new ListRowDataAdapter.QueueBasedDataObserver();
        }

        this.attach();
    }

    void detach() {
        this.mAdapter.unregisterObserver(this.mDataObserver);
    }

    void attach() {
        this.initialize();
        this.mAdapter.registerObserver(this.mDataObserver);
    }

    void initialize() {
        this.mLastVisibleRowIndex = -1;

        for(int i = this.mAdapter.size() - 1; i >= 0; --i) {
            Row item = (Row)this.mAdapter.get(i);
            if (item.isRenderedAsRowView()) {
                this.mLastVisibleRowIndex = i;
                break;
            }
        }

    }

    public int size() {
        return this.mLastVisibleRowIndex + 1;
    }

    public Object get(int index) {
        return this.mAdapter.get(index);
    }

    void doNotify(int eventType, int positionStart, int itemCount) {
        switch(eventType) {
            case 2:
                this.notifyItemRangeChanged(positionStart, itemCount);
                break;
            case 4:
                this.notifyItemRangeInserted(positionStart, itemCount);
                break;
            case 8:
                this.notifyItemRangeRemoved(positionStart, itemCount);
                break;
            case 16:
                this.notifyChanged();
                break;
            default:
                throw new IllegalArgumentException("Invalid event type " + eventType);
        }

    }

    private class QueueBasedDataObserver extends DataObserver {
        QueueBasedDataObserver() {
        }

        public void onChanged() {
            ListRowDataAdapter.this.initialize();
            ListRowDataAdapter.this.notifyChanged();
        }
    }

    private class SimpleDataObserver extends DataObserver {
        SimpleDataObserver() {
        }

        public void onItemRangeChanged(int positionStart, int itemCount) {
            if (positionStart <= ListRowDataAdapter.this.mLastVisibleRowIndex) {
                this.onEventFired(2, positionStart, Math.min(itemCount, ListRowDataAdapter.this.mLastVisibleRowIndex - positionStart + 1));
            }

        }

        public void onItemRangeInserted(int positionStart, int itemCount) {
            if (positionStart <= ListRowDataAdapter.this.mLastVisibleRowIndex) {
                ListRowDataAdapter.this.mLastVisibleRowIndex += itemCount;
                this.onEventFired(4, positionStart, itemCount);
            } else {
                int lastVisibleRowIndex = ListRowDataAdapter.this.mLastVisibleRowIndex;
                ListRowDataAdapter.this.initialize();
                if (ListRowDataAdapter.this.mLastVisibleRowIndex > lastVisibleRowIndex) {
                    int totalItems = ListRowDataAdapter.this.mLastVisibleRowIndex - lastVisibleRowIndex;
                    this.onEventFired(4, lastVisibleRowIndex + 1, totalItems);
                }

            }
        }

        public void onItemRangeRemoved(int positionStart, int itemCount) {
            if (positionStart + itemCount - 1 < ListRowDataAdapter.this.mLastVisibleRowIndex) {
                ListRowDataAdapter.this.mLastVisibleRowIndex -= itemCount;
                this.onEventFired(8, positionStart, itemCount);
            } else {
                int lastVisibleRowIndex = ListRowDataAdapter.this.mLastVisibleRowIndex;
                ListRowDataAdapter.this.initialize();
                int totalItems = lastVisibleRowIndex - ListRowDataAdapter.this.mLastVisibleRowIndex;
                if (totalItems > 0) {
                    this.onEventFired(8, Math.min(ListRowDataAdapter.this.mLastVisibleRowIndex + 1, positionStart), totalItems);
                }

            }
        }

        public void onChanged() {
            ListRowDataAdapter.this.initialize();
            this.onEventFired(16, -1, -1);
        }

        protected void onEventFired(int eventType, int positionStart, int itemCount) {
            ListRowDataAdapter.this.doNotify(eventType, positionStart, itemCount);
        }
    }
}
