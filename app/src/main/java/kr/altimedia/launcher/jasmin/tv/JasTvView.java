/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv;

import android.content.Context;
import android.content.ContextWrapper;
import android.media.tv.TvContentRating;
import android.media.tv.TvInputInfo;
import android.media.tv.TvInputManager;
import android.media.tv.TvTrackInfo;
import android.media.tv.TvView;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.util.Log;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import androidx.annotation.IntDef;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.cwmp.service.def.CWMPEtcCode;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.logging.LogBackServiceProvider;
import kr.altimedia.launcher.jasmin.tv.ui.BlockScreen;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;

public abstract class JasTvView extends FrameLayout implements NetworkConnectManager.NetworkConnectListener {
    private static final String TAG = JasTvView.class.getSimpleName();
    private static final boolean INCLUDE = Log.INCLUDE;

    public static final int TYPE_MAIN = 0;
    public static final int TYPE_PIP = 1;
    public static final int TYPE_THUMBNAIL = 2;

    @IntDef({TYPE_MAIN, TYPE_PIP, TYPE_THUMBNAIL})
    public @interface TvViewType {
    }

    private static final String getTypeString(@TvViewType int type) {
        switch (type) {
            case TYPE_MAIN:
                return "TYPE_MAIN";
            case TYPE_PIP:
                return "TYPE_PIP";
            case TYPE_THUMBNAIL:
                return "TYPE_THUMBNAIL";
            default:
                return "UNDEFINED_TYPE:" + type;
        }
    }

    private final String type() {
        return "["+mType+"]";
    }

    protected static final String KEY_CHANNEL_INFO = "ChannelInfo";

    //for parental rating
    private static final String ACTION_UNBLOCK_CONTENT = "UNBLOCK_CONTENT";
    private static final String KEY_PR_RATING = "RATING_FLATTEN";
    private static final String KEY_IGNORE_PR_CHECK = "IGNORE_RATING";

    protected int mType;
    protected boolean mFeaturesLwcUpdateEnabled;
    protected boolean mFeaturesPreviewEnabled;

    protected ChannelDataManager mChannelDataManager;
    protected ProgramDataManager mProgramDataManager;
    protected TvInputManagerHelper mTvInputManagerHelper;
    private LastWatchedChannelUpdater mLastWatchedChannelUpdater = null;

    private final ChannelSwitcher mChannelSwitcher = new ChannelSwitcher();

    protected TvView tvView;
    protected BlockScreen blockScreen;

    //currunt info
    protected Channel currentChannel;
    protected Program currentProgram;
    protected TuningResult tuningResult;
    protected ParentalController.BlockType blockType;
    private long unblockedChannelId = -1;

    //Check Network
    private static NetworkConnectManager mNetworkConnectManager;

    private List<TvTrackInfo> tracks;
    private String trackId = null;
    private final String START_TIME = "START_TIME";

    private final ChannelDataManager.Listener channelDatatlistener = new ChannelDataManager.Listener() {
        @Override
        public void onLoadFinished() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "onLoadFinished()");
            }
        }

        @Override
        public void onChannelListUpdated() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "onChannelListUpdated()");
            }
            if (mType == TYPE_MAIN) {
                //If the EPG channel ring does not contain the watching channel, add it.
                if (!mChannelDataManager.containsEpgChannelRing(currentChannel)) {
                    mChannelDataManager.addChannelToEpgChannelRing(currentChannel);
                }
            }
            List<Channel> channels = mChannelDataManager.getChannelList();
            if (!channels.contains(currentChannel)) {
                Channel defaultChannel = mChannelDataManager.getLastWatchedChannel();
                if (defaultChannel == null || !channels.contains(defaultChannel)) {
                    defaultChannel = mChannelDataManager.getLowestChannel();
                    if (defaultChannel == null) {
                        if (Log.INCLUDE) {
                            Log.d(TAG + type(), "onChannelListUpdated() selected channel removed, but not have channel for move so return");
                        }
                        return;
                    }
                }
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "onChannelListUpdated() selected channel removed, tune default channel : " + defaultChannel);
                }
                tuneChannel(defaultChannel, false);
            }
        }


        @Override
        public void onChannelBrowsableChanged() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "onChannelBrowsableChanged()");
            }
        }

        @Override
        public void onUserBasedChannelListUpdated() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "onUserBasedChannelListUpdated()");
            }
        }
    };

    public JasTvView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mChannelDataManager = TvSingletons.getSingletons(context).getChannelDataManager();
        mProgramDataManager = TvSingletons.getSingletons(context).getProgramDataManager();
        mTvInputManagerHelper = TvSingletons.getSingletons(context).getTvInputManagerHelper();
        if (INCLUDE) {
            Log.d(TAG + type(), "JasTvView()");
        }

        if (mNetworkConnectManager == null) {
            mNetworkConnectManager = new NetworkConnectManager(context);
        }
    }

    public void attachChannelListListener() {
        if (mChannelDataManager == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "attachChannelListListener but mChannelDataManager == null so return");
            }
            return;
        }
        if (mChannelDataManager.listenerContains(channelDatatlistener)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "attachChannelListListener already Contains so return");
            }
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "attachChannelListListener add listener");
        }
        mChannelDataManager.addListener(channelDatatlistener);
    }

    public void detachChannelListListener() {
        if (mChannelDataManager == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "detachChannelListListener but mChannelDataManager == null so return");
            }
            return;
        }
        if (!mChannelDataManager.listenerContains(channelDatatlistener)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "detachChannelListListener not Contains so return");
            }
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "detachChannelListListener remove listener");
        }
        mChannelDataManager.removeListener(channelDatatlistener);
    }

    public JasTvView(Context context) {
        this(context, null);
    }

    public JasTvView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public JasTvView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public LiveTvActivity getLiveTvActivity() {
        Context context = getContext();
        if ((context instanceof LiveTvActivity == false) && context instanceof ContextWrapper) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (context instanceof LiveTvActivity) {
            return (LiveTvActivity) context;
        }
        return null;
    }

    public abstract void updateInfo(TuningResult tuningResult);

    protected abstract void setPreviewIndicator(Channel channel);

    protected abstract void updatePopularChannelData(String json);

    public Channel getCurrentChannel() {
        return currentChannel;
    }

    public Program getCurrentProgram() {
        return currentProgram;
    }

    public boolean isBlocked() {
//        if (Log.INCLUDE) {
//            Log.d(TAG + type(), "isBlocked() {");
//            Log.d(TAG + type(), "isBlocked()   blockType:" + blockType);
//            Log.d(TAG + type(), "isBlocked()   isBlocked:" + blockType.isBlocked());
//            Log.d(TAG + type(), "isBlocked()   isUnblocked:" + isUnblocked());
//            Log.d(TAG + type(), "isBlocked()   return:" + (blockType.isBlocked() && !isUnblocked()));
//            Log.d(TAG + type(), "isBlocked() }");
//        }
        return blockType.isBlocked() && !isUnblocked();
    }

    public boolean isPreview() {
//        if (Log.INCLUDE) {
//            Log.d(TAG + type(), "isPreview() :" + blockType.isPreview());
//        }
        return blockType.isPreview();
    }

    public ParentalController.BlockType getBlockType() {
        return blockType;
    }

    protected void init(@TvViewType int type, TvView tvView, BlockScreen blockScreen) {
        mType = type;
        mFeaturesLwcUpdateEnabled = (mType == TYPE_MAIN);
        mFeaturesPreviewEnabled = true;

        this.tvView = tvView;
        if (INCLUDE) {
            Log.d(TAG + type(), "init() tvView:" + tvView);
        }
        this.tvView.setCallback(new TvInputCallbackImpl());
        this.blockScreen = blockScreen;
        currentChannel = null;
        blockType = ParentalController.BlockType.CLEAN_CHANNEL;
        setTuningResult(TuningResult.TUNING_NONE);

        if (mFeaturesLwcUpdateEnabled) {
            mLastWatchedChannelUpdater = new LastWatchedChannelUpdater();
        }
    }

    protected abstract void updateMuting();

    public static boolean swapChannel(JasTvView mainTvView, JasTvView pipTvView) {
        if (INCLUDE) {
            Log.d(TAG, "swapChannel()");
        }
        Channel mainChannel = mainTvView.currentChannel;
        Channel pipChannel = pipTvView.currentChannel;

        if (mainChannel.isHiddenChannel()) {
            if (INCLUDE) {
                Log.d(TAG, "swapChannel() - main channel is hidden channel, don't change channel - return");
            }
            return false;
        }

        if (mainChannel.getId() == pipChannel.getId()) {
            if (mainTvView.tuningResult == pipTvView.tuningResult) {
                //same channel, don't change channel
                if (INCLUDE) {
                    Log.d(TAG, "swapChannel() - same channel, don't change channel - return");
                }
                return false;
            }
        }

        Program mainProgram = mainTvView.currentProgram;
        TuningResult mainTuningResult = mainTvView.tuningResult;
        ParentalController.BlockType mainBlockType = mainTvView.blockType;

        Program pipProgram = pipTvView.currentProgram;
        TuningResult pipTuningResult = pipTvView.tuningResult;
        ParentalController.BlockType pipBlockType = pipTvView.blockType;

        boolean resetPip = false;

        if (INCLUDE) {
            Log.d(TAG, "swapChannel() - mainTvView ch:" + mainChannel.getDisplayNumber() + ", mainTuningResult:" + mainTuningResult + ", mainBlockType:" + mainBlockType);
            Log.d(TAG, "swapChannel() - pipTvView ch:" + pipChannel.getDisplayNumber() + ", pipTuningResult:" + pipTuningResult + ", pipBlockType:" + pipBlockType);
        }

        if (INCLUDE) {
            Log.d(TAG, "swapChannel() - mainTvView.tuneChannel() ch:" + pipChannel.getDisplayNumber() + ", checkBlockNeeded:" + pipBlockType.isBlocked());
        }
        mainTvView.reset();
        mainTvView.tuneChannel(pipChannel, pipProgram, pipBlockType.isBlocked(), true);

        if (pipChannel.isUHDChannel()) {
            mainTvView.getLiveTvActivity().showPipUHDErrorToast();
            resetPip = true;
        }

        if (!resetPip) {
            if (INCLUDE) {
                Log.d(TAG, "swapChannel() - pipTvView.tuneChannel() ch:" + mainChannel.getDisplayNumber() + ", checkBlockNeeded:" + mainBlockType.isBlocked());
            }

            pipTvView.reset();
            pipTvView.tuneChannel(mainChannel, mainProgram, mainBlockType.isBlocked(), true);
            return false;
        } else {
            return true;
        }
    }

    public void unblockContent(TvContentRating rating) {
        if (Log.INCLUDE) {
            Log.d(TAG + type(), "unblockContent() rating:" + (rating != null ? rating.flattenToString() : null));
        }
        if (rating != null) {
            Bundle bundle = new Bundle();
            bundle.putString(KEY_PR_RATING, rating.flattenToString());
            tvView.sendAppPrivateCommand(ACTION_UNBLOCK_CONTENT, bundle);
            unblockedChannelId = currentChannel != null ? currentChannel.getId() : -1;
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "unblockContent() unblockedChannelId:"+unblockedChannelId);
            }
        }
    }

    public boolean isUnblocked() {
        return isUnblockedChannel(currentChannel);
    }

    public boolean isUnblockedChannel(Channel channel) {
        if (channel != null) {
            return channel.getId() == unblockedChannelId;
        } else {
            return false;
        }
    }

    protected Program getCurrentProgram(Channel channel) {
        if (channel != null) {
            return mProgramDataManager.getCurrentProgram(channel.getId());
        } else {
            return null;
        }
    }

    public TuningResult tuneChannel(Channel channel) {
        return tuneChannel(channel, true, true);
    }

    public TuningResult tuneChannel(Channel channel, boolean selfChanged) {
        return tuneChannel(channel, true, selfChanged);
    }

    public TuningResult tuneChannel(Channel channel, boolean checkBlockNeeded, boolean selfChanged) {
        return tuneChannel(channel, getCurrentProgram(channel), checkBlockNeeded, selfChanged);
    }

    public final TuningResult tuneChannel(Channel channel, Program program, boolean checkBlockNeeded, boolean selfChanged) {
        if (Log.INCLUDE) {
            Log.d(TAG + type(), "tuneChannel() --------------------------------");
            Log.d(TAG + type(), "tuneChannel() type:" + getTypeString(mType) + ", ch:" + (channel != null ? channel.getDisplayNumber() : null) + ", checkBlockNeeded:" + checkBlockNeeded + ", prog:" + program + ", selfChanged : " + selfChanged);
        }
        if (mLastWatchedChannelUpdater != null) {
            mLastWatchedChannelUpdater.resetChannel();
        }

        Channel prevChannel = currentChannel;

        currentChannel = channel;
        currentProgram = program;

        if (checkBlockNeeded) {
            blockType = ParentalController.checkBlock(channel, program, mFeaturesPreviewEnabled, mType);
        } else {
            if (tuningResult == TuningResult.TUNING_BLOCKED_PROGRAM) {
                unblockContent((TvContentRating) tuningResult.getData());
            }

            if (ParentalController.isPaidChannel(channel)) {
                blockType = ParentalController.checkPaidChannel(channel, mFeaturesPreviewEnabled);
            } else {
                blockType = ParentalController.BlockType.CLEAN_CHANNEL;
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG + type(), "tuneChannel() blockType:" + blockType);
        }
        if ((blockType.isBlocked() && !isUnblockedChannel(channel)) || blockType.isUnsubscribed()) {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "tuneChannel() isBlocked()");
            }
            reset(false);
            TuningResult result;
            if (blockType == ParentalController.BlockType.BLOCK_UNSUBSCRIBED) {
                result = TuningResult.TUNING_BLOCKED_UNSUBSCRIBED;
            } else if (blockType == ParentalController.BlockType.BLOCK_PROGRAM) {
                result = TuningResult.TUNING_BLOCKED_PROGRAM;
            } else if (blockType == ParentalController.BlockType.BLOCK_UHD) {
                result = TuningResult.TUNING_BLOCKED_UHD_IN_PIP;
            } else if (blockType == ParentalController.BlockType.BLOCK_ADULT_CHANNEL) {
                result = TuningResult.TUNING_BLOCKED_ADULT_CHANNEL;
            } else {
                result = TuningResult.TUNING_BLOCKED_CHANNEL;
            }
            setTuningResult(result);

            updateEpgChannelRingIfNeeded(prevChannel);
        } else {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "tuneChannel() try tune channel");
            }

            //clear block type, except preview type
            if (blockType != ParentalController.BlockType.PREVIEW_CHANNEL) {
                blockType = ParentalController.BlockType.CLEAN_CHANNEL;
            }

            setPreviewIndicator(null);
            if (!channel.getUri().equals(mChannelSwitcher.tunedUri)) {
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "tuneChannel() tune channel");
                }
                setTuningResult(TuningResult.TUNING_REQUESTED);
                mChannelSwitcher.tuneTvView(channel, checkBlockNeeded);

                updateEpgChannelRingIfNeeded(prevChannel);
            } else {
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "tuneChannel() same tunedUri - skip tune");
                }
            }
        }

        if (mLastWatchedChannelUpdater != null) {
            mLastWatchedChannelUpdater.channelTuned(channel);
        }

        if (Log.INCLUDE) {
            Log.d(TAG + type(), "tuneChannel() unblockedChannelId:" + unblockedChannelId);
        }
        if (mOnChangedListener != null) {
            mOnChangedListener.onChannelChanged(channel, selfChanged);
        }
        return tuningResult;
    }

    private void updateEpgChannelRingIfNeeded(Channel prevChannel) {
        if (mType == TYPE_MAIN && prevChannel != null) {
            mChannelDataManager.removeChannelFromEpgChannelRing(prevChannel);
        }
    }

    public void onNetworkConnected() {
        if (Log.INCLUDE) {
            Log.d(TAG + type(), "onNetworkConnected()");
        }
        mNetworkConnectManager.removeNetworkConnectListener(JasTvView.this);
        mChannelSwitcher.onNetworkConnected();
    }

    public void reset() {
        reset(true);
    }

    public void resetTimeShift() {
        long keepChannelId = unblockedChannelId;
        reset(false);
        unblockedChannelId = keepChannelId;
        if (Log.INCLUDE) {
            Log.d(TAG + type(), "resetTimeshift() rollback unblockedChannelId:"+unblockedChannelId);
        }
    }

    public void reset(boolean clearFlag) {
        if (Log.INCLUDE) {
            Log.d(TAG + type(), "reset() type:" + getTypeString(mType) + ", clearFlag:" + clearFlag);
        }
        mNetworkConnectManager.removeNetworkConnectListener(this);

        if (mLastWatchedChannelUpdater != null) {
            mLastWatchedChannelUpdater.resetChannel();
        }
        setPreviewIndicator(null);
        try {
            tvView.reset();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }

        mChannelSwitcher.reset();
        unblockedChannelId = -1;

        if (clearFlag) {
            mTimeShiftCallback = null;
            currentChannel = null;
            currentProgram = null;
            tuningResult = TuningResult.TUNING_NONE;
            blockType = ParentalController.BlockType.CLEAN_CHANNEL;
        }

        if (Log.INCLUDE) {
            Log.d(TAG + type(), "reset(clearFlag:"+clearFlag+") unblockedChannelId:"+unblockedChannelId);
        }
    }

    protected void setTuningResult(TuningResult tuningResult) {
        if (Log.INCLUDE) {
            Log.d(TAG + type(), "setTuningResult(type:" + getTypeString(mType) + ") ### tuningResult:" + tuningResult);
        }

        this.tuningResult = tuningResult;
        updateInfo(tuningResult);
        blockScreen.updateBlockScreen(tuningResult);
    }

    public TuningResult getTuningResult() {
        return tuningResult;
    }

    public List<TvTrackInfo> getAudioTracks() {
        return tvView.getTracks(TvTrackInfo.TYPE_AUDIO);
    }

    public String getSelectedAudioTrackId() {
        return tvView.getSelectedTrack(TvTrackInfo.TYPE_AUDIO);
    }

    public void selectAudioTrack(String trackId) {
        tvView.selectTrack(TvTrackInfo.TYPE_AUDIO, trackId);
    }

    public List<TvTrackInfo> getSubtitleTracks() {
        return tvView.getTracks(TvTrackInfo.TYPE_SUBTITLE);
    }

    public String getSelectedSubtitleTrackId() {
        return tvView.getSelectedTrack(TvTrackInfo.TYPE_SUBTITLE);
    }

    public void selectSubtitleTrack(String trackId) {
        tvView.selectTrack(TvTrackInfo.TYPE_SUBTITLE, trackId);
    }

    public enum TuningResult {
        TUNING_NONE(false),
        TUNING_REQUESTED(false),
        TUNING_SUCCESS(false),
        TUNING_SUCCESS_AUDIO(false),
        TUNING_BLOCKED_CHANNEL(true),
        TUNING_BLOCKED_PROGRAM(true),
        TUNING_BLOCKED_UNSUBSCRIBED(true),
        TUNING_BLOCKED_ADULT_CHANNEL(true),
        TUNING_BLOCKED_UHD_IN_PIP(true),
        TUNING_BLOCKED_NOTSUPPORTED(true),
        TUNING_BLOCKED_THUMB_WATCHING_CH(true),
        TUNING_BLOCKED_THUMB_UHD(true),
        TUNING_BLOCKED_THUMB_AUDIO(true),
        TUNING_BLOCKED_THUMB_NOT_AVAILABLE(true),
        TUNING_FAIL(false);

        private final boolean blocked;
        private Object data;
        private String errorTitle;
        private String errorMessage;

        TuningResult(boolean blocked) {
            this.blocked = blocked;
        }

        public final boolean isBlocked() {
            return blocked;
        }

        public TuningResult setData(Object data) {
            this.data = data;
            return this;
        }

        public Object getData() {
            return data;
        }

        public TuningResult setError(String title, String message) {
            this.errorTitle = title;
            this.errorMessage = message;
            return this;
        }

        public String getErrorTitle() {
            return errorTitle;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public ParentalController.BlockType getBlockType() {
            switch (this) {
                case TUNING_BLOCKED_ADULT_CHANNEL:
                    return ParentalController.BlockType.BLOCK_ADULT_CHANNEL;
                case TUNING_BLOCKED_UNSUBSCRIBED:
                    return ParentalController.BlockType.BLOCK_UNSUBSCRIBED;
                case TUNING_BLOCKED_CHANNEL:
                    return ParentalController.BlockType.BLOCK_CHANNEL;
                case TUNING_BLOCKED_PROGRAM:
                    return ParentalController.BlockType.BLOCK_PROGRAM;
                case TUNING_SUCCESS_AUDIO:
                    return ParentalController.BlockType.CLEAN_CHANNEL;
                default:
                    return ParentalController.BlockType.CLEAN_CHANNEL;
            }
        }
    }

    private TimeShiftCallback mTimeShiftCallback;

    public void setTimeShiftCallback(TimeShiftCallback timeShiftCallback) {
        this.mTimeShiftCallback = timeShiftCallback;
    }

    public void setTimeShiftPositionCallback(TvView.TimeShiftPositionCallback callback) {
        tvView.setTimeShiftPositionCallback(callback);
    }

    public void timeShiftPlay(Channel channel, Program program, String inputId, Uri recordedProgramUri) {
        timeShiftPlay(channel, program, inputId, recordedProgramUri, -1);
    }

    public void timeShiftPlay(Channel channel, Program program, String inputId, Uri recordedProgramUri, long startTimeOffset) {
        mChannelSwitcher.timeShiftPlay(channel, program, inputId, recordedProgramUri, startTimeOffset);
    }

    public void putCurrentPosition(long currentTime) {
        mChannelSwitcher.putCurrentPosition(currentTime);
    }

    public void clearPosition() {
        mChannelSwitcher.clearPosition();
    }

    public void timeShiftPause() {
        tvView.timeShiftPause();
    }

    public void timeShiftSeekTo(long timeMs) {
        if (Log.INCLUDE) {
            Log.d(TAG + type(), "timeShiftSeekTo : currentTime " + JasmineEpgApplication.SystemClock().currentTimeMillis());
            Log.d(TAG + type(), "timeShiftSeekTo : " + timeMs);
        }
        tvView.timeShiftSeekTo(timeMs);
    }

    public void timeShiftResume() {
        tvView.timeShiftResume();
    }

    public interface TimeShiftCallback {
        void onChannelRetuned(String inputId, Uri channelUri);

        void onTimeShiftStatusChanged(String inputId, int status);
    }

    private class TvInputCallbackImpl extends TvView.TvInputCallback {
        private static final String TAG = "TvInputCallback";

        static final String COMMON_KEY_EVENT = "event";
        static final String TUNING_EVENT_VALUE_SUCCESS = "TUNING_SUCCESS";
        static final String EVENT_TYPE_TUNING_EVENT = "TuningEvent";
        static final String EVENT_TYPE_CHANNEL_EVENT = "ChannelEvent";
        static final String EVENT_TYPE_PLAYER = "PlayerError";
        static final String TUNING_EVENT_KEY = COMMON_KEY_EVENT;
        static final String CHANNEL_EVENT_KEY_URI = "uri";
        static final String CHANNEL_EVENT_KEY_EVENT = COMMON_KEY_EVENT;

        static final String KEY_CENTRAL_URL = "CENTRAL_CDN";
        private static final String KEY_IS_CENTRAL_URL = "IS_CENTRAL_CDN";

        //player error
        private static final String KEY_PLAYER_ERROR_TYPE = "PLAYER_ERROR_TYPE";
        private static final String KEY_PLAYER_ERROR_REASON = "PLAYER_ERROR_REASON";
        private static final String KEY_PLAYER_ERROR_MESSAGE = "PLAYER_ERROR_MESSAGE";

        private static final String PLAYER_EVENT_SOURCE = "SOURCE";
        private static final String PLAYER_EVENT_RENDERER = "RENDERER";
        private static final String PLAYER_EVENT_UNEXPECTED = "UNEXPECTED";
        private static final String PLAYER_EVENT_REMOTE = "REMOTE";
        private static final String PLAYER_EVENT_OUT_OUT_MEMORY = "OUT_OUT_MEMORY";

        private void channelTuned() {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.channelTuned()");
            }

            updateMuting();
            setTuningResult(currentChannel.isAudioChannel() ? TuningResult.TUNING_SUCCESS_AUDIO : TuningResult.TUNING_SUCCESS);
            if (JasChannelManager.getInstance().isPreviewAvailable(currentChannel)) {
                setPreviewIndicator(currentChannel);
            }
        }

        public void onChannelRetuned(String inputId, Uri channelUri) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onChannelRetuned() inputId = [" + inputId + "], channelUri = [" + channelUri + "]");
            }

            channelTuned();

            if (mTimeShiftCallback != null) {
                mTimeShiftCallback.onChannelRetuned(inputId, channelUri);
            }
        }

        public void onConnectionFailed(String inputId) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onConnectionFailed() inputId = [" + inputId + "]");
            }
        }

        public void onContentAllowed(String inputId) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onContentAllowed() inputId = [" + inputId + "]");
            }
            updateMuting();

            if (blockType.isBlocked()) {
                setTuningResult(currentChannel.isAudioChannel() ? TuningResult.TUNING_SUCCESS_AUDIO : TuningResult.TUNING_SUCCESS);
                blockType = ParentalController.BlockType.CLEAN_CHANNEL;

                if (mTimeShiftCallback != null) {
                    mTimeShiftCallback.onTimeShiftStatusChanged(inputId, TvInputManager.TIME_SHIFT_STATUS_AVAILABLE);
                }
            }
        }

        public void onContentBlocked(String inputId, TvContentRating rating) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onContentBlocked() inputId = [" + inputId + "], rating = [" + (rating != null ? rating.flattenToString() : null) + "]");
            }
            updateMuting();
            setTuningResult(TuningResult.TUNING_BLOCKED_PROGRAM.setData(rating));
            blockType = ParentalController.BlockType.BLOCK_PROGRAM;
        }

        public void onDisconnected(String inputId) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onDisconnected() inputId = [" + inputId + "]");
            }
        }

        public void onTimeShiftStatusChanged(String inputId, int status) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onTimeShiftStatusChanged() inputId = [" + inputId + "], status = [" + status + "]");
            }
            if (mTimeShiftCallback != null) {
                mTimeShiftCallback.onTimeShiftStatusChanged(inputId, status);
            }
        }

        public void onTrackSelected(String inputId, int type, String trackId) {
            JasTvView.this.trackId = trackId;
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onTrackSelected() inputId = [" + inputId + "], type = [" + type + "], trackId = [" + trackId + "]");
            }
        }

        public void onTracksChanged(String inputId, List<TvTrackInfo> tracks) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onTracksChanged() inputId = [" + inputId + "], tracks = [" + tracks + "]");
            }
            JasTvView.this.tracks = tracks;
            TvTrackInfo track;
            for (int i = 0; tracks != null && i < tracks.size(); i++) {
                track = tracks.get(i);
                if (track.getType() == TvTrackInfo.TYPE_AUDIO) {
                    if (INCLUDE) {
                        Log.d(TAG + type(), "onTracksChanged() track=" + track.getType() + "/" + track.getId());
                        Log.d(TAG + type(), "onTracksChanged() audio lang=" + track.getLanguage());
                    }
                    Bundle extra = track.getExtra();
                    if (extra == null) {
                        if (INCLUDE) {
                            Log.w(TAG, "onTracksChanged() bundle is null");
                        }
                    } else {
                        int audioType = extra.getInt("audio_type");
                        String encoding = extra.getString("encoding");
                        if (INCLUDE) {
                            Log.d(TAG + type(), "onTracksChanged() type=" + audioType + ", encoding=" + encoding);
                        }
                    }

                } else if (track.getType() == TvTrackInfo.TYPE_VIDEO) {
                    Bundle extra = track.getExtra();
                    if (extra != null) {
                        String json = extra.getString(KEY_CHANNEL_INFO);
                        if (INCLUDE) {
                            Log.d(TAG + type(), "TvInputCallback.onTracksChanged() channelInfo:" + json);
                        }
                        if (json != null) {
                            updatePopularChannelData(json);
                        }
                    } else {
                        if (INCLUDE) {
                            Log.d(TAG + type(), "TvInputCallback.onTracksChanged() bundle is null");
                        }
                    }
                } else {
                    if (INCLUDE) {
                        Log.d(TAG + type(), "TvInputCallback.onTracksChanged() track.getType:" + track.getType());
                    }
                }
            }
        }

        public void onVideoAvailable(String inputId) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onVideoAvailable() inputId = [" + inputId + "]");
            }

            channelTuned();
        }

        public void onVideoSizeChanged(String inputId, int width, int height) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onVideoSizeChanged() inputId = [" + inputId + "], width = [" + width + "], height = [" + height + "]");
            }
        }

        public void onVideoUnavailable(String inputId, int reason) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onVideoUnavailable() inputId = [" + inputId + "], reason = [" + reason + "]");
            }
            TvInputInfo tvInputInfo = mTvInputManagerHelper.getTvInputInfo(inputId);
            CharSequence label = tvInputInfo.loadLabel(JasTvView.this.getContext().getApplicationContext());
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onVideoUnavailable() label:" + label);
            }
            //JasminToast.makeToast(JasTvView.this.getContext().getApplicationContext(), "onVideoUnavailable : " + label);
            updateMuting();
            if (reason == TvInputManager.VIDEO_UNAVAILABLE_REASON_AUDIO_ONLY
                    && currentChannel.isAudioChannel()) {
                setTuningResult(TuningResult.TUNING_SUCCESS_AUDIO);
            }
        }

        //@SystemApi
        public void onEvent(String inputId, String eventType, Bundle eventArgs) {
            if (INCLUDE) {
                Log.d(TAG + type(), "TvInputCallback.onEvent() inputId:" + inputId);
                Log.d(TAG + type(), "TvInputCallback.onEvent() eventType:" + eventType);
                Log.d(TAG + type(), "TvInputCallback.onEvent() bundle:" + bundleToString(eventArgs));
            }
            updateMuting();
            switch (eventType) {
                case EVENT_TYPE_TUNING_EVENT:
                    if (INCLUDE) {
                        Log.d(TAG + type(), "onEvent() eventType:EVENT_TYPE_TUNING_EVENT");
                    }

//                    if (value != null) {
//                        log("onEvent: " + eventType + ", event=" + value);
//                        if (TUNING_EVENT_VALUE_SUCCESS.equals(value)) {
//                            taurusHandleModule.attachDefaultOC();
//                        }
//                    }

                    if (eventArgs != null) {
                        if (INCLUDE) {
                            Log.d(TAG + type(), "onEvent() type:" + eventType + ", CURRENT_URL:" + eventArgs.getString("CURRENT_URL"));
                        }
                        Object isCentralUrl = eventArgs.get(KEY_IS_CENTRAL_URL);
                        if (isCentralUrl != null) {
                            mChannelSwitcher.updateIsCentralUrl(((Boolean) isCentralUrl).booleanValue());
                            if (INCLUDE) {
                                Log.d(TAG + type(), "onEvent() update isCentralUrl:" + mChannelSwitcher.isCentralUrl);
                            }
                        }
                    }
                    break;
                case EVENT_TYPE_CHANNEL_EVENT:
                    if (INCLUDE) {
                        Log.d(TAG + type(), "onEvent() eventType:EVENT_TYPE_CHANNEL_EVENT");
                    }
                    if (eventArgs.getString(CHANNEL_EVENT_KEY_URI) != null) {
                        if (INCLUDE) {
                            Log.d(TAG + type(), "onEvent() type:" + eventType + ", uri:" + eventArgs.getString(CHANNEL_EVENT_KEY_URI) + ", event:" + eventArgs.getInt(CHANNEL_EVENT_KEY_EVENT));
                        }
                    }
                    break;
                case EVENT_TYPE_PLAYER:
                    if (INCLUDE) {
                        Log.d(TAG + type(), "onEvent() eventType:EVENT_TYPE_PLAYER");
                    }
                    if (eventArgs != null) {
                        String errorType = eventArgs.getString(KEY_PLAYER_ERROR_TYPE);
                        String errorReason = eventArgs.getString(KEY_PLAYER_ERROR_REASON);
                        String errorMessage = eventArgs.getString(KEY_PLAYER_ERROR_MESSAGE);
                        if (errorType != null) {
                            onPlayerError(errorType, errorReason, errorMessage);
                        }
                    }
                    break;
                default:
                    if (INCLUDE) {
                        Log.d(TAG + type(), "onEvent() eventType:<"+eventType+">");
                    }
                    break;
            }
        }

        private void onPlayerError(String errorType, String errorReason, String errorMessage) {
            if (INCLUDE) {
                Log.d(TAG + type(), "onPlayerError() errorType:[" + errorType + "], errorReason:[" + errorReason + "], errorMessage:[" + errorMessage + "]");
            }

            String title = getErrorTitle(errorType);
            String msg = getErrorMessage(errorType);
            if (INCLUDE) {
                Log.d(TAG + type(), "onPlayerError() errorType:[" + errorType + "], title:[" + title + "], msg:[" + msg + "]");
            }

            sendErrorLog(errorType);
            switch (errorType) {
                case PLAYER_EVENT_SOURCE:
//                    if ("UnexpectedLoaderException".equalsIgnoreCase(errorReason)) {
//                        mChannelSwitcher.log("PlayerError : UnexpectedLoaderException");
//                        mChannelSwitcher.retryTuneUrlByUnexpectedLoaderException();
//                        break;
//                    }
                    //error processing in below case statement.
                case PLAYER_EVENT_UNEXPECTED:
                    //try next url
                    if (INCLUDE) {
                        Log.d(TAG + type(), "onPlayerError() errorType:" + errorType + ", try next url");
                    }

                    mChannelSwitcher.log("PlayerError : "+errorType);
                    if (mNetworkConnectManager.isDisconnected()) {
                        mChannelSwitcher.log("PlayerError : isDisconnected...");
                        mNetworkConnectManager.addNetworkConnectListener(JasTvView.this);
                        //change error message when network is disconnected
                        String networkErrorTitle = "LNC_0018";
                        String networkErrorMessage = JasTvView.this.getContext().getString(R.string.error_message_lnc_0018);
                        setTuningResult(TuningResult.TUNING_FAIL.setError(networkErrorTitle, networkErrorMessage));
                    } else {
                        mChannelSwitcher.log("PlayerError : try tune next url");
                        if (mChannelSwitcher.tryTuneNextUrl()) {
                            setTuningResult(TuningResult.TUNING_FAIL.setError(title, msg));
                        }
                    }

                    break;
                case PLAYER_EVENT_RENDERER:
                case PLAYER_EVENT_REMOTE:
                case PLAYER_EVENT_OUT_OUT_MEMORY:
                    mChannelSwitcher.log("PlayerError : "+errorType);
                    //retry same url
                    if (INCLUDE) {
                        Log.d(TAG + type(), "onPlayerError() errorType:" + errorType + ", retry same url");
                    }
                    mChannelSwitcher.log("PlayerError : retry tune url");
                    if (mChannelSwitcher.retryTuneUrl()) {
                        setTuningResult(TuningResult.TUNING_FAIL.setError(title, msg));
                    }
                    break;
                default:
                    break;
            }
        }

        private String getErrorTitle(String errorType) {
            Context context = JasTvView.this.getContext();
            switch (errorType) {
                case PLAYER_EVENT_SOURCE:
                    return "LNC_0028";
                case PLAYER_EVENT_UNEXPECTED:
                    return "LNC_0019";
                case PLAYER_EVENT_RENDERER:
                    return "LNC_0017";
                case PLAYER_EVENT_REMOTE:
                    return "LNC_0016";
                case PLAYER_EVENT_OUT_OUT_MEMORY:
                    return "LNC_0015";
                default:
                    return "";
            }
        }

        private String getErrorMessage(String errorType) {
            Context context = JasTvView.this.getContext();
            switch (errorType) {
                case PLAYER_EVENT_SOURCE:
                    return context.getString(R.string.error_message_lnc_0028);
                case PLAYER_EVENT_UNEXPECTED:
                    return context.getString(R.string.error_message_lnc_0019);
                case PLAYER_EVENT_RENDERER:
                    return context.getString(R.string.error_message_lnc_0017);
                case PLAYER_EVENT_REMOTE:
                    return context.getString(R.string.error_message_lnc_0016);
                case PLAYER_EVENT_OUT_OUT_MEMORY:
                    return context.getString(R.string.error_message_lnc_0015);
                default:
                    return "";
            }
        }


        private void sendErrorLog(String errorType) {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "sendErrorLog() errorType:" + errorType);
            }
            switch (errorType) {
                case PLAYER_EVENT_SOURCE:
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_604);
                    break;
                case PLAYER_EVENT_UNEXPECTED:
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_001);
                    break;
                case PLAYER_EVENT_RENDERER:
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_603);
                    break;
                case PLAYER_EVENT_REMOTE:
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_602);
                    break;
                case PLAYER_EVENT_OUT_OUT_MEMORY:
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_601);
                    break;
            }
        }
    }

    private class ChannelSwitcher {
        private static final String TAG = "ChannelSwitcher";

        //tuned info
        private String tunedInputId;
        private Uri tunedUri;
        private Bundle tunedBundle;
        boolean isCentralUrl;
        boolean isRetried;
        int retryTuneNextUrlCount;

        private void log(String msg) {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "log() " + msg);
            }
        }

        private void reset() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "reset()");
            }

            tunedInputId = null;
            tunedUri = null;
            tunedBundle = null;
            isCentralUrl = false;
            isRetried = false;
            retryTuneNextUrlCount = 0;
        }

        private void tuneTvView(Channel channel, boolean checkBlockNeeded) {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "tuneTvView()");
            }
            tunedInputId = channel.getInputId();
            tunedUri = channel.getUri();
            tunedBundle = null;

            if (!checkBlockNeeded || isUnblockedChannel(channel)) {
                //tune without check parental rating in TIS
                tunedBundle = new Bundle();
                tunedBundle.putBoolean(KEY_IGNORE_PR_CHECK, true);
                unblockedChannelId = channel.getId();
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "tuneTvView(ch:" + channel.getDisplayNumber() + ") ch.id:" + channel.getId() + ", unblockedChannelId:" + unblockedChannelId);
                }
            } else {
                unblockedChannelId = -1;
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "tuneTvView(ch:" + channel.getDisplayNumber() + ") ch.id:" + channel.getId() + ", unblockedChannelId:" + unblockedChannelId);
                }
            }

            if (Log.INCLUDE) {
                Log.d(TAG + type(), "tuneTvView(), tunedUri:" + tunedUri + ", tunedBundle:" + bundleToString(tunedBundle));
            }
            tvView.tune(tunedInputId, tunedUri, tunedBundle);

            isCentralUrl = false;
            isRetried = false;
            retryTuneNextUrlCount = 0;
        }

        private void timeShiftPlay(Channel channel, Program program, String inputId, Uri recordedProgramUri, long startTimeOffset) {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "timeShiftPlay()");
            }
            currentChannel = channel;
            currentProgram = program;
            tunedInputId = inputId;
            tunedUri = channel.getUri();
            if (!isUnblockedChannel(channel)) {
                unblockedChannelId = -1;
            }
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "timeShiftPlay() ch.id:" + channel.getId() + ", unblockedChannelId:" + unblockedChannelId);
            }

            setPreviewIndicator(null);
            setTuningResult(TuningResult.TUNING_REQUESTED);
            if (tunedBundle == null) {
                tunedBundle = new Bundle();
            }
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "timeShiftPlay | inputId : " + inputId + ", recordedProgramUri : " + recordedProgramUri.toString() + ", startTimeOffset : " + startTimeOffset);
            }
            if (startTimeOffset != -1) {
                tunedBundle.putLong(START_TIME, startTimeOffset);
                timeShiftSeekTo(startTimeOffset);
            } else {
                tunedBundle.remove(START_TIME);
                tvView.timeShiftPlay(inputId, recordedProgramUri);
            }

            isCentralUrl = false;
            isRetried = false;
            retryTuneNextUrlCount = 0;
        }

        private void putCurrentPosition(long currentTime) {
            if (tunedBundle == null) {
                tunedBundle = new Bundle();
            }
            tunedBundle.putLong(START_TIME, currentTime);
        }

        private void clearPosition() {
            if (tunedBundle == null) {
                return;
            }
            tunedBundle.remove(START_TIME);
        }

        private void updateIsCentralUrl(boolean b) {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "updateIsCentralUrl() isCentralUrl:"+b);
            }
            isCentralUrl = b;

            if (Log.INCLUDE) {
                Log.d(TAG + type(), "updateIsCentralUrl() tuning is completed successfully - reset retryTuneNextUrlCount=0");
            }
            retryTuneNextUrlCount = 0;
        }

        private boolean tryTuneNextUrl() {
            retryTuneNextUrlCount++;
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "tryTuneNextUrl(), isCentralUrl:" + isCentralUrl+", retryTuneNextUrlCount:"+retryTuneNextUrlCount);
            }

            //retryTuneNextUrlCount 0,1,2 : local url
            //retryTuneNextUrlCount 3,4,5 : central url
            //retryTuneNextUrlCount 6~ : don't try tune (if timeshift content is playing, exit timeshift mode.)
            if (isCentralUrl && retryTuneNextUrlCount < 3) {
                //skip 0~2 retry count
                retryTuneNextUrlCount = 4;
            }

            if (retryTuneNextUrlCount >= 6) {
                if (isTimeShiftMode()) {
                    if (Log.INCLUDE) {
                        Log.d(TAG + type(), "tryTuneNextUrl() - stop timeshift & tune Live url, retryCount:"+retryTuneNextUrlCount);
                    }
                    exitTimeShiftMode();
                    return false;
                }

                //do not tune
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "tryTuneNextUrl() - DO NOT RETRY any more, retryCount:"+retryTuneNextUrlCount);
                }
                return true;
            }

            if (retryTuneNextUrlCount < 3) {
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "tryTuneNextUrl() - try tune LOCAL url, retryCount:"+retryTuneNextUrlCount);
                }
            } else {
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "tryTuneNextUrl() - try tune CENTRAL url, retryCount:"+retryTuneNextUrlCount);
                }
                if (tunedBundle == null) {
                    tunedBundle = new Bundle();
                }
                tunedBundle.putBoolean(TvInputCallbackImpl.KEY_CENTRAL_URL, true);
            }

            if (Log.INCLUDE) {
                Log.d(TAG + type(), "tryTuneNextUrl(), tunedUri:" + tunedUri + ", tunedBundle:" + bundleToString(tunedBundle));
            }
            tvView.tune(tunedInputId, tunedUri, tunedBundle);
            isRetried = false;

            if (Log.INCLUDE) {
                Log.d(TAG + type(), "tryTuneNextUrl() - update retry count : "+retryTuneNextUrlCount);
            }
            return false;
        }

        private boolean retryTuneUrl() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "retryTuneUrl(), isRetried:" + isRetried);
            }
            if (isRetried) {
                if (isTimeShiftMode()) {
                    if (Log.INCLUDE) {
                        Log.d(TAG + type(), "retryTuneUrl() - stop timeshift & tune Live url, isRetried:"+isRetried);
                    }
                    exitTimeShiftMode();
                    return false;
                }

                //do not retry
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "retryTuneUrl() - already tune retried - do not retry.");
                }

                return true;
            } else {
                //retry
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "retryTuneUrl(), tunedUri:" + tunedUri + ", tunedBundle:" + bundleToString(tunedBundle));
                }
                tvView.tune(tunedInputId, tunedUri, tunedBundle);

                isRetried = true;

                return false;
            }
        }

        private void retryTuneUrlByUnexpectedLoaderException() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "retryTuneUrlByUnexpectedLoaderException(), tunedUri:" + tunedUri + ", tunedBundle:" + bundleToString(tunedBundle));
            }
            tvView.tune(tunedInputId, tunedUri, tunedBundle);
        }

        private void onNetworkConnected() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "onNetworkConnected()");
            }
            isRetried = false;
            retryTuneNextUrlCount = 0;
            retryTuneUrl();
        }

        private boolean isTimeShiftMode() {
            boolean result = mTimeShiftCallback != null;
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "isTimeShiftMode() : "+result);
            }
            return result;
        }

        private void exitTimeShiftMode() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "exitTimeShiftMode()");
            }
            if (mTimeShiftCallback != null) {
                mTimeShiftCallback.onTimeShiftStatusChanged(tunedInputId, TvInputManager.TIME_SHIFT_STATUS_UNAVAILABLE);
            }
        }
    }

    private class LastWatchedChannelUpdater {
        private final String TAG = LastWatchedChannelUpdater.class.getSimpleName();

        private static final boolean CHANNEL_LOGGING_ENABLE = true;
        private static final long TIME_NOT_SET = -1;
        private final long DELAY_TIME = TimeUnit.SECONDS.toMillis(3);
        private final Handler mHandler = new Handler();

        //for common
        private Channel mChannel;
        private long mStartTimeMs = TIME_NOT_SET;

        //for update LastWatchedChannel
        private final Runnable mRunnable = new Runnable() {
            public void run() {
                updateLastWatchedChannel();
            }
        };

        //for channel watching logging
        private final long channel_log_delay_time;
        private final long channel_log_cycle;
        private final Runnable mRunnable2 = new Runnable() {
            public void run() {
                updateChannelWatchingLog();
            }
        };

        private LastWatchedChannelUpdater() {
            if (CHANNEL_LOGGING_ENABLE) {
                channel_log_delay_time = LogBackServiceProvider.getInstance().getWatchingTime();
                channel_log_cycle = LogBackServiceProvider.getInstance().getWatchingHistoryCycleTime();
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "LastWatchedChannelUpdater.created() get channelLog watching time:" + channel_log_delay_time);
                    Log.d(TAG + type(), "LastWatchedChannelUpdater.created() get channelLog watching cycle:" + channel_log_cycle);
                }
            } else {
                channel_log_delay_time = 0;
                channel_log_cycle = 0;
                if (Log.INCLUDE) {
                    Log.d(TAG + type(), "LastWatchedChannelUpdater.created() set channelLog watching time to zero (disabled)");
                }
            }
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "LastWatchedChannelUpdater.created() lastWatchedChannel:" + (mChannelDataManager.getLastWatchedChannel() == null ? null : mChannelDataManager.getLastWatchedChannel().getDisplayNumber()));
                Log.d(TAG + type(), "LastWatchedChannelUpdater.created() CHANNEL_LOGGING_ENABLE:" + CHANNEL_LOGGING_ENABLE);
                Log.d(TAG + type(), "LastWatchedChannelUpdater.created() channelLog watching time:" + channel_log_delay_time);
                Log.d(TAG + type(), "LastWatchedChannelUpdater.created() channelLog watching cycle:" + channel_log_cycle);
                Log.d(TAG + type(), "LastWatchedChannelUpdater.created() now:" + JasmineEpgApplication.SystemClock().currentTimeMillis());
            }
        }

        private void channelTuned(Channel channel) {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "LastWatchedChannelUpdater.channelTuned() channel:" + (channel == null ? null : channel.getDisplayNumber()) + ", " + ((mStartTimeMs == TIME_NOT_SET) ? "update time" : "don't update time (same channel)"));
            }
            if (mStartTimeMs == TIME_NOT_SET)
                mStartTimeMs = JasmineEpgApplication.SystemClock().currentTimeMillis();
            mChannel = channel;

            //for update watching channel
            updateWatchingChannel(channel);

            //for update last watched channel
            mHandler.removeCallbacks(mRunnable);
            mHandler.postDelayed(mRunnable, DELAY_TIME);

            //for channel watching logging
            mHandler.removeCallbacks(mRunnable2);
            if (channel_log_delay_time > 0) {
                mHandler.postDelayed(mRunnable2, channel_log_delay_time);
            }
        }

        private void resetChannel() {
            long duration = mStartTimeMs != TIME_NOT_SET ? JasmineEpgApplication.SystemClock().currentTimeMillis() - mStartTimeMs : TIME_NOT_SET;
            mStartTimeMs = TIME_NOT_SET;
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "LastWatchedChannelUpdater.resetChannel() - duration : " + (duration == TIME_NOT_SET ? "TIME_NOT_SET" : (int) (duration / 1000L) + " seconds") + " channel:" + (mChannel == null ? null : mChannel.getDisplayNumber()));
            }

            //for update watching channel
            updateWatchingChannel(null);

            mHandler.removeCallbacks(mRunnable);
            mHandler.removeCallbacks(mRunnable2);

            updateLastWatchedChannel();
        }

        private void updateLastWatchedChannel() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "LastWatchedChannelUpdater.updateLastWatchedChannel()");
                Log.d(TAG + type(), "LastWatchedChannelUpdater.updateLastWatchedChannel() last channel:" + (mChannel == null ? null : mChannel.getDisplayNumber()));
            }
            if (mChannelDataManager.getBrowsableChannelList().contains(mChannel)) {
                JasChannelManager.getInstance().setLastWatchedChannel(mChannel);
            }
        }

        private void updateChannelWatchingLog() {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "LastWatchedChannelUpdater.updateChannelWatchingLog()");
                Log.d(TAG + type(), "LastWatchedChannelUpdater.updateChannelWatchingLog() channel.displayNo:" + (mChannel == null ? null : mChannel.getDisplayNumber()));
                Log.d(TAG + type(), "LastWatchedChannelUpdater.updateChannelWatchingLog() channel.serviceId:" + (mChannel == null ? null : mChannel.getServiceId()));
                Log.d(TAG + type(), "LastWatchedChannelUpdater.updateChannelWatchingLog() entryTime:" + mStartTimeMs);
            }
            if (CHANNEL_LOGGING_ENABLE) {
                //2020.10.06 - changed channel entry time to current time by KT request.
                //LogBackServiceProvider.getInstance().addChannelLog(mChannel.getServiceId(), mStartTimeMs);
                LogBackServiceProvider.getInstance().addChannelLog(mChannel.getServiceId(), JasmineEpgApplication.SystemClock().currentTimeMillis());

                //for channel watching logging
                mHandler.removeCallbacks(mRunnable2);
                if (channel_log_cycle > 0) {
                    mHandler.postDelayed(mRunnable2, channel_log_cycle);
                }
            }
        }

        private void updateWatchingChannel(Channel channel) {
            if (Log.INCLUDE) {
                Log.d(TAG + type(), "LastWatchedChannelUpdater.updateWatchingChannel()");
                Log.d(TAG + type(), "LastWatchedChannelUpdater.updateWatchingChannel() channel.displayNo:" + (channel == null ? null : channel.getDisplayNumber()));
                Log.d(TAG + type(), "LastWatchedChannelUpdater.updateWatchingChannel() channel.serviceId:" + (channel == null ? null : channel.getServiceId()));
            }
            CWMPServiceProvider.getInstance().setWatchingChannel(channel);
        }
    }

    private static String bundleToString(Bundle bundle) {
        if (bundle != null) {
            String str = "{";
            Set<String> keys = bundle.keySet();
            for (String key : keys) {
                str += "<" + key + ":" + bundle.get(key) + ">,";
            }
            return str + "}";
        } else {
            return null;
        }
    }

    private OnChangedListener mOnChangedListener;

    public void setOnChangedListener(OnChangedListener onChangedListener) {
        this.mOnChangedListener = onChangedListener;
    }

    public void clearListener() {
        this.mOnChangedListener = null;
    }

    public interface OnChangedListener {
        void onChannelChanged(Channel channel, boolean selfChanged);
    }
}