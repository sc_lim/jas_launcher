/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel;

/**
 * Created by mc.kim on 18,02,2020
 */
public enum SidePanelOption {
    SubTitle("SubtitleOption", 0), Audio("AudioOption", 0), AppLink("link", 1),
    BroadCast("BroadCastEvent", 4), Setting("Setting", 5), Fav("Favorite", 6),
    LanguageOption("languageOption", 2), Section("section", 3),
    LanguageStyle("languageStyle", 2);

    final String mType;
    final int buttonType;


    public boolean isMenu() {
        return buttonType == 0;
    }

    public boolean isButton() {
        return buttonType == 1 || buttonType == 4 || buttonType == 5;
    }

    public boolean isValue() {
        return buttonType == 2 || buttonType == 6;
    }

    public boolean isSection() {
        return buttonType == 3;
    }

    SidePanelOption(String type, int buttonType) {
        this.mType = type;
        this.buttonType = buttonType;
    }

    public String getType() {
        return mType;
    }

    public static SidePanelOption findByType(String type) {
        SidePanelOption[] sidePanelOptions = values();
        for (SidePanelOption option : sidePanelOptions) {
            if (option.mType.equalsIgnoreCase(type)) {
                return option;
            }
        }
        return null;
    }

}
