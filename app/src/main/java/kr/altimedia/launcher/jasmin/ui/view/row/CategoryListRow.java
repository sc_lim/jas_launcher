/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;

import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

/**
 * Created by mc.kim on 23,07,2020
 */
public class CategoryListRow extends ListRow {
    private final Category mCategory;

    public CategoryListRow(Category category, HeaderItem header, ObjectAdapter adapter) {
        super(header, adapter);
        mCategory = category;
    }

    public CategoryListRow(Category category, long id, HeaderItem header, ObjectAdapter adapter) {
        super(id, header, adapter);
        mCategory = category;
    }

    public CategoryListRow(Category category, ObjectAdapter adapter) {
        super(adapter);
        mCategory = category;
    }

    public Category getCategory() {
        return mCategory;
    }
}
