
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.list;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.coupon.CouponDataManager;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProduct;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProductList;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.MyContentListFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.OnDataLoadedListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.list.presenter.CouponProductListPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.list.presenter.CouponProductPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.CouponPurchaseDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.CouponPurchaseResultListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class CouponListDialog extends SafeDismissDialogFragment implements OnDataLoadedListener, View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = CouponListDialog.class.getName();
    public static final String TAG = CouponListDialog.class.getSimpleName();

    private MbsDataTask dataTask;
    private ProgressBarManager progressBarManager;

    private ArrayList<CouponProduct> couponList = new ArrayList<>();
    private MyContentListFragment couponProductListView;
    private ClassPresenterSelector couponProductListPresenterSelector;
    private ArrayObjectAdapter couponProductListRowsAdapter;
    private BaseOnItemViewClickedListener couponProductClickedListener;
    private BaseOnItemViewSelectedListener couponProductSelectedListener;

    private LinearLayout emptyDataLayer;
    private LinearLayout listDataLayer;
    private LinearLayout couponProductLayer;
    private TextView couponDesc;
    private ImageView arrowLeft;
    private ImageView arrowRight;
    private TextView btnCancel;
    private View rootView;

    private final int[] COUPON_BGs = new int[]{R.drawable.coupon_bg1, R.drawable.coupon_bg2, R.drawable.coupon_bg3, R.drawable.coupon_bg4};

    private CouponPurchaseResultListener mCouponPurchaseResultListener;
    private TvOverlayManager mTvOverlayManager;
    private String termsAndConditions = "";

    private CouponListDialog() {
    }

    public static CouponListDialog newInstance() {

        Bundle args = new Bundle();

        CouponListDialog fragment = new CouponListDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private void showSafeDialog(SafeDismissDialogFragment dialog) {
        if (mTvOverlayManager == null) {
            return;
        }
        mTvOverlayManager.showDialogFragment(dialog);
    }

    @Override
    public void onDestroyView() {
        if (dataTask != null) {
            dataTask.cancel(true);
        }
        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
        if (progressBarManager != null) {
            progressBarManager.hide();
            progressBarManager.setRootView(null);
        }
        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach: "+mTvOverlayManager);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogBlackTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        couponProductListView = (MyContentListFragment) getChildFragmentManager().findFragmentById(
                R.id.couponProductListView);
        if (couponProductListView == null) {
            Rect padding = new Rect(0, 0, 0, 0);
            couponProductListView = MyContentListFragment.newInstance(padding);
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.couponProductListView, couponProductListView).commit();
        }

        return inflater.inflate(R.layout.dialog_coupon_shop_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        initLoadingBar(view);
        initCoupon(view);
        initButton(view);

        loadData();
    }

    private void initLoadingBar(View view) {
        progressBarManager = new ProgressBarManager();
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);
        progressBarManager.setInitialDelay(0);

        showProgress();
    }

    private void initCoupon(View view) {
        emptyDataLayer = view.findViewById(R.id.emptyDataLayer);
        arrowLeft = view.findViewById(R.id.arrowL);
        arrowRight = view.findViewById(R.id.arrowR);
        listDataLayer = view.findViewById(R.id.listDataLayer);
        couponProductLayer = view.findViewById(R.id.couponProductLayer);
        couponDesc = view.findViewById(R.id.couponDesc);
    }

    private void initButton(View view) {

        btnCancel = view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private void loadData() {

        CouponDataManager couponDataManager = new CouponDataManager();
        dataTask = couponDataManager.getCouponProductList(
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                new MbsDataProvider<String, CouponProductList>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int reason) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadData: onFailed");
                        }
                        showView(0);

                        if(reason == MbsTaskCallback.REASON_NETWORK) {
                            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(reason, getTaskContext());
                            failNoticeDialogFragment.show(mTvOverlayManager,getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                        }
                    }

                    @Override
                    public void onSuccess(String key, CouponProductList result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadData: onSuccess: result=" + result);
                        }
                        if(couponList != null && !couponList.isEmpty()) {
                            couponList.clear();
                        }
                        if(result != null){
                            try {
                                couponList.addAll(result.getCouponList());
                                termsAndConditions = result.getTermsAndConditions();

                                int currBgIdx = 0;
                                for (CouponProduct coupon : couponList) {
                                    if (currBgIdx >= COUPON_BGs.length) {
                                        currBgIdx = 0;
                                    }
                                    coupon.setBgImage(COUPON_BGs[currBgIdx]);
                                    currBgIdx++;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        showView(0);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);

                        showView(0);
                    }

                    @Override
                    public Context getTaskContext() {
                        return CouponListDialog.this.getContext();
                    }
                });
    }

    @Override
    public void hideProgress() {
        if (progressBarManager != null) {
            progressBarManager.hide();
        }
    }

    @Override
    public void showProgress() {
        if (progressBarManager != null) {
            progressBarManager.show();
        }
    }

    @Override
    public void showView(int type) {
        setCouponView();
    }

    private void setCouponView() {
        hideProgress();

        if (couponList != null && couponList.size() > 0) {
            int size = couponList.size();

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) couponProductLayer.getLayoutParams();
            if(size >= 4){
                size = 4;
            }
            layoutParams.width = (int) getResources().getDimension(R.dimen.coupon_width) * size + (int) getResources().getDimension(R.dimen.coupon_list_horizontal_space) * (size - 1);
            couponProductLayer.setLayoutParams(layoutParams);

            RowPresenter contentPresenter = new CouponProductPresenter();
            ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(contentPresenter);
            listRowAdapter.addAll(0, couponList);
            HeaderItem header = new HeaderItem(0, "");
            ListRow listRow = new ListRow(header, listRowAdapter);

            couponProductListPresenterSelector = new ClassPresenterSelector();
            if (couponProductListRowsAdapter == null) {
                couponProductListRowsAdapter = new ArrayObjectAdapter(couponProductListPresenterSelector);
            } else {
                couponProductListRowsAdapter.clear();
            }
            couponProductListRowsAdapter.add(listRow);

            couponProductClickedListener = new CouponProductClickedListener();
            couponProductSelectedListener = new CouponProductSelectedListener();
            if (couponProductListView != null) {
                couponProductListView.setOnItemViewClickedListener(couponProductClickedListener);
                couponProductListView.setOnItemViewSelectedListener(couponProductSelectedListener);
            }
            CouponProductListPresenter rowPresenter = new CouponProductListPresenter();
            rowPresenter.setOnPresenterDispatchKeyListener(new CouponProductListPresenter.OnPresenterDispatchKeyListener() {
                @Override
                public boolean onKeyDown(KeyEvent event, Presenter.ViewHolder viewHolder, int position) {
                    int keyCode = event.getKeyCode();
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_RIGHT: {
                            HorizontalGridView hGridView = ((CouponProductListPresenter.ViewHolder) viewHolder).getGridView();
                            if (hGridView != null) {
                                int size = hGridView.getAdapter().getItemCount();
                                if (position == (size - 1)) {
                                    hGridView.scrollToPosition(0);
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                }
            });
            couponProductListPresenterSelector.addClassPresenter(ListRow.class, rowPresenter);

            if (couponProductListView != null) {
                couponProductListView.setAdapter(couponProductListRowsAdapter);
            }

            if (couponList.size() > 4) {
                showArrow(View.GONE, View.VISIBLE);
            }

            setAvailablePeriodInfo(couponList.get(0));

            emptyDataLayer.setVisibility(View.GONE);
            listDataLayer.setVisibility(View.VISIBLE);

            couponProductListView.resetFocus();
        }else{
            listDataLayer.setVisibility(View.GONE);
            emptyDataLayer.setVisibility(View.VISIBLE);
        }
    }

    private void showArrow(int left, int right){
        arrowLeft.setVisibility(left);
        arrowRight.setVisibility(right);
    }

    private void setAvailablePeriodInfo(CouponProduct couponProduct){
        String availablePeriod = this.getResources().getString(R.string.no_expiration_limit);
        try{
            String availablePeriodValue = couponProduct.getAvailablePeriodValue();
            if(availablePeriodValue != null && !availablePeriodValue.isEmpty()) {
                availablePeriod = availablePeriodValue + " " +
                        this.getResources().getString(couponProduct.getAvailablePeriodUnit().getName()) + " " +
                        this.getResources().getString(R.string.product_coupon_period_desc);
            }
        }catch (Exception e){
        }
        couponDesc.setText(availablePeriod);
    }

    public void showCouponPurchaseDialog(CouponProduct coupon) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showCouponPurchaseDialog: coupon=" + coupon);
        }

        CouponPurchaseDialog purchaseDialog = CouponPurchaseDialog.newInstance(coupon, termsAndConditions);
        purchaseDialog.setCouponPurchaseResultListener(mCouponPurchaseResultListener);
        purchaseDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                CouponListDialog.this.dismiss();
            }
        });
        showSafeDialog(purchaseDialog);
    }

    private final class CouponProductClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder rowViewHolder, Row row) {
            if (Log.INCLUDE) {
                Log.d(TAG, "CouponProductClickedListener: onItemClicked: item=" + item + ", row=" + row);
            }
            if (item == null) {
                return;
            }
            if (item instanceof CouponProduct) {
                showCouponPurchaseDialog((CouponProduct) item);
            }
        }
    }

    private final class CouponProductSelectedListener implements OnItemViewSelectedListener {
        @Override
        public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder rowViewHolder, Row row) {
            if (Log.INCLUDE) {
                Log.d(TAG, "CouponProductSelectedListener: onItemSelected: item=" + item + ", row=" + row);
            }
            if (item == null) {
                return;
            }
            if (item instanceof CouponProduct) {
                setAvailablePeriodInfo((CouponProduct)item);
            }

            int totalSize = couponList.size();
            if (couponList.size() > 4) {
                HorizontalGridView hGridView = ((CouponProductListPresenter.ViewHolder) rowViewHolder).getGridView();
                int currPos = hGridView.getSelectedPosition();
                if(currPos <= 3){
                    showArrow(View.GONE, View.VISIBLE);
                }else if(currPos > 3 && currPos <(totalSize - 1)){
                    showArrow(View.VISIBLE, View.VISIBLE);
                }else if(currPos >=(totalSize - 1)){
                    showArrow(View.VISIBLE, View.GONE);
                }
            }
        }
    }

    public void setCouponPurchaseResultListener(CouponPurchaseResultListener listener) {
        this.mCouponPurchaseResultListener = listener;
    }
}
