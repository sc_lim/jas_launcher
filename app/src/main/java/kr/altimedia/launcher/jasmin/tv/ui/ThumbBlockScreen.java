/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.JasTvView;

public class ThumbBlockScreen extends BlockScreen {
    private static final String TAG = ThumbBlockScreen.class.getSimpleName();
    private static final boolean INCLUDE = true;

    private ImageView thumb_iframe;
    private TextView thumb_iframe_text;

    public ThumbBlockScreen(Context context) {
        this(context, null);
    }

    public ThumbBlockScreen(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ThumbBlockScreen(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ThumbBlockScreen(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();

        thumb_iframe = findViewById(R.id.thumb_iframe);
        thumb_iframe_text = findViewById(R.id.thumb_iframe_text);
    }

    public boolean isVisible() {
        return getVisibility() == View.VISIBLE;
    }

    public void setVisible(boolean visible) {
        if (visible) {
            setVisibility(View.VISIBLE);
        } else {
            setVisibility(View.GONE);
        }
    }

    public void updateBlockScreen(JasTvView.TuningResult tuningResult) {
        if(tuningResult == null){
            thumb_iframe_text.setText("");
            setVisible(false);
            return;
        }
        int textId = 0;
        int resId = R.drawable.channel_img_iframe;
//        switch (tuningResult) {
//            case TUNING_BLOCKED_THUMB_WATCHING_CH:
//                textId = R.string.channel_thumb_watching;
//                break;
//            case TUNING_BLOCKED_THUMB_AUDIO:
//                textId = R.string.channel_thumb_audio;
//                break;
//            case TUNING_BLOCKED_THUMB_UHD:
//                textId = R.string.iframe_block_uhd_title;
//                break;
//            case TUNING_BLOCKED_CHANNEL:
//                textId = R.string.channel_thumb_blocked;
//                break;
//            case TUNING_BLOCKED_THUMB_NOT_AVAILABLE:
//                textId = R.string.channel_thumb_not_available;
//                break;
//            case TUNING_BLOCKED_PROGRAM:
//                textId = R.string.channel_thumb_blocked_program;
//                break;
//            case TUNING_BLOCKED_UNSUBSCRIBED:
//                textId = R.string.channel_thumb_unsubscribed;
//                break;
//            default:
//                break;
//        }
        switch (tuningResult) {
            case TUNING_FAIL:
                textId = R.string.empty;
                resId = R.drawable.channel_img_iframe_error;
                break;
            case TUNING_BLOCKED_PROGRAM:
                textId = R.string.channel_thumb_blocked_program;
                break;
            case TUNING_BLOCKED_ADULT_CHANNEL:
                textId = R.string.channel_thumb_adult_blocked;
                break;
            case TUNING_BLOCKED_UNSUBSCRIBED:
                textId = R.string.channel_thumb_unsubscribed;
                break;
            default:
                break;
        }
        if (textId != 0) {
            thumb_iframe_text.setText(textId);
            thumb_iframe.setImageResource(resId);
            setVisible(true);
        } else {
            thumb_iframe_text.setText(R.string.empty);
            thumb_iframe.setImageResource(resId);
            setVisible(false);
        }
    }
}
