/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.def.NormalDateTimeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.NormalDateTimeDeserializer2;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class SubscribedContent implements Parcelable {
    public static final Creator<SubscribedContent> CREATOR = new Creator<SubscribedContent>() {
        @Override
        public SubscribedContent createFromParcel(Parcel in) {
            return new SubscribedContent(in);
        }

        @Override
        public SubscribedContent[] newArray(int size) {
            return new SubscribedContent[size];
        }
    };

    @Expose
    @SerializedName("productId")
    private String productId;
    @Expose
    @SerializedName("productName")
    private String productName;
    @Expose
    @SerializedName("offerId")
    private String offerId;
    @Expose
    @SerializedName("purchaseId")
    private String purchaseId;
    @Expose
    @SerializedName("purchaseDateTime")
    @JsonAdapter(NormalDateTimeDeserializer.class)
    private Date purchaseDateTime;
    @Expose
    @SerializedName("renewalDate")
    @JsonAdapter(NormalDateTimeDeserializer2.class)
    private Date renewalDate;
    @Expose
    @SerializedName("period")
    private int period;
    @Expose
    @SerializedName("price")
    private String price;
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("refundYn")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isRefunded;

    protected SubscribedContent(Parcel in) {
        productId = in.readString();
        productName = in.readString();
        offerId = in.readString();
        purchaseId = in.readString();
        purchaseDateTime = ((Date) in.readValue((Date.class.getClassLoader())));
        renewalDate = ((Date) in.readValue((Date.class.getClassLoader())));
        period = ((Integer) in.readValue((Integer.class.getClassLoader())));
        price = in.readString();
        currency = in.readString();
        isRefunded = (boolean) in.readValue(Boolean.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productId);
        dest.writeString(productName);
        dest.writeString(offerId);
        dest.writeString(purchaseId);
        dest.writeValue(purchaseDateTime);
        dest.writeValue(renewalDate);
        dest.writeValue(period);
        dest.writeString(price);
        dest.writeString(currency);
        dest.writeValue(isRefunded);
    }

    public String getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public String getOfferId() {
        return offerId;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public Date getPurchaseDateTime() {
        return purchaseDateTime;
    }

    public Date getRenewalDate() {
        return renewalDate;
    }

    public int getPeriod() {
        return period;
    }

    public String getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public boolean isRefunded() {
        return isRefunded;
    }

    @NonNull
    @Override
    public String toString() {
        return "SubscribedContent{" +
                "productId=" + productId +
                ", productName=" + productName +
                ", offerId=" + offerId +
                ", purchaseId=" + purchaseId +
                ", purchaseDateTime=" + purchaseDateTime +
                ", renewalDate=" + renewalDate +
                ", period=" + period +
                ", price=" + price +
                ", currency=" + currency +
                ", isRefunded=" + isRefunded +
                "}";
    }


}
