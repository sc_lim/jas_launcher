/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import kr.altimedia.launcher.jasmin.R;

public class PointInputBoxView extends LinearLayout {
    private RelativeLayout layout;
    private LimitTextView limitTextView;

    private final String initValue = "0";
    private final String unit = "P";
    private final String fixed = initValue + unit;

    private OnChangeListener onChangeListener;

    public PointInputBoxView(Context context) {
        super(context);
        initView();
    }

    public PointInputBoxView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);
    }

    public PointInputBoxView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        getAttrs(attrs, defStyleAttr);

    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.common_point_input_box_view, this, false);
        addView(v);

        layout = v.findViewById(R.id.layout);
        limitTextView = v.findViewById(R.id.input_box);

        limitTextView.setTextGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        limitTextView.setText("0P");

        OnKeyListener onKeyListener = new OnKeyListener();
        onKeyListener.setPointInputBoxView(this);
        limitTextView.setOnKeyListener(onKeyListener);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.PointInputBoxViewTheme);
        setTypeArray(typedArray);
    }


    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.PointInputBoxViewTheme, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        int text_size = typedArray.getResourceId(R.styleable.PointInputBoxViewTheme_point_input_text_size, R.dimen.point_input_box_text_size);
        int text_color = typedArray.getResourceId(R.styleable.PointInputBoxViewTheme_point_input_text_color, R.drawable.selector_common_bg_text);
        int bgColor = typedArray.getResourceId(R.styleable.PointInputBoxViewTheme_point_input_bg_color, R.drawable.selector_button_focus_w);

        layout.setBackgroundResource(bgColor);
        limitTextView.setTextSize(text_size);
        limitTextView.setTextColor(text_color);

        typedArray.recycle();
    }

    public void setOnChangeListener(OnChangeListener onChangeListener) {
        this.onChangeListener = onChangeListener;
    }

    public void setPointLimit(int limit) {
        // 10 단위
        limit = (limit / 10) * 10;
        limitTextView.setLimit(limit);
    }

    public String getString() {
        return limitTextView.getString();
    }

    public int getPoint() {
        String point = getString();
        point = point.substring(0, point.length() - 1);

        return Integer.parseInt(point);
    }

    private void input(int inputDigit) {
        String current = limitTextView.getString();
        int last = current.length() - fixed.length();
        String newDigit = current.substring(0, last) + inputDigit;

        if (newDigit.startsWith("0")) {
            return;
        }

        requestInput(newDigit);
    }

    private void requestInput(String newDigit) {
        int digit = Integer.parseInt(newDigit);
        limitTextView.forceInput(digit + fixed);

        boolean isOver = limitTextView.isOverLimit(digit * 10);
        if (isOver) {
            String limit = limitTextView.getLimit() + "P";
            limitTextView.forceInput(limit);
        }

        notifyChange();
    }

    private void delete() {
        String current = limitTextView.getString();

        //fixed value "OP"
        if (current.length() == fixed.length()) {
            return;
        }

        requestDelete();
    }

    private void requestDelete() {
        String current = limitTextView.getString();

        int last = current.length() - fixed.length() + 1;
        current = current.substring(0, last);
        int currentDigit = Integer.parseInt(current);

        String delete = "";
        if (currentDigit >= 10) {
            delete = current.substring(0, current.length() - 2) + fixed;
        }

        limitTextView.setText(delete);
        notifyChange();
    }

    private void notifyChange() {
        if (onChangeListener != null) {
            onChangeListener.onChangeInput(getPoint());
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        onChangeListener = null;
        limitTextView.setOnKeyListener(null);
    }

    private static class OnKeyListener implements View.OnKeyListener {
        private PointInputBoxView pointInputBoxView;

        public void setPointInputBoxView(PointInputBoxView pointInputBoxView) {
            this.pointInputBoxView = pointInputBoxView;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != KeyEvent.ACTION_DOWN) {
                return false;
            }

            if (pointInputBoxView == null) {
                return false;
            }

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    pointInputBoxView.input(1);
                    return true;
                case KeyEvent.KEYCODE_BACK:
                    pointInputBoxView.input(2);
                    return true;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    pointInputBoxView.delete();
                    return true;
            }

            return false;
        }
    }

    public interface OnChangeListener {
        void onChangeInput(int point);
    }
}
