/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;

public class ProfileIcon implements Parcelable {

    public static final Creator<ProfileIcon> CREATOR = new Creator<ProfileIcon>() {
        @Override
        public ProfileIcon createFromParcel(Parcel in) {
            return new ProfileIcon(in);
        }

        @Override
        public ProfileIcon[] newArray(int size) {
            return new ProfileIcon[size];
        }
    };

    @Expose
    @SerializedName("profileIconId")
    private String id;
    @Expose
    @SerializedName("profileIconPath")
    private String path;

    public ProfileIcon(String id, String path){
        this.id = id;
        this.path = path;
    }

    protected ProfileIcon(Parcel in) {
        id = in.readString();
        path = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(path);
    }

    public String getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

    @NonNull
    @Override
    public String toString() {
        return "ProfileIcon{" +
                "id=" + id +
                ", path=" + path +
                "}";
    }
}
