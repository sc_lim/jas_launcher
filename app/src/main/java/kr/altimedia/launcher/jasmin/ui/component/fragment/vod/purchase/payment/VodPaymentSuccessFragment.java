/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;

public class VodPaymentSuccessFragment extends VodPaymentBaseFragment {
    public static final String CLASS_NAME = VodPaymentSuccessFragment.class.getName();
    private static final String TAG = VodPaymentSuccessFragment.class.getSimpleName();

    private static final String KEY_PURCHASED_INFO = "PURCHASED_INFO";

    public VodPaymentSuccessFragment(){}

    public static VodPaymentSuccessFragment newInstance(PurchaseInfo purchaseInfo) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_PURCHASED_INFO, purchaseInfo);

        VodPaymentSuccessFragment fragment = new VodPaymentSuccessFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_vod_payment_success;
    }

    @Override
    protected void initView(View view) {
        initLayout(view);
    }

    private void initLayout(View view) {
        PurchaseInfo purchaseInfo = getArguments().getParcelable(KEY_PURCHASED_INFO);

        String title = purchaseInfo.getTitle();
        ((TextView) view.findViewById(R.id.title)).setText(title);

        view.findViewById(R.id.watch_now).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOnPaymentListener().onRequestWatch(true);
            }
        });

        view.findViewById(R.id.watch_later).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOnPaymentListener().onRequestWatch(false);
            }
        });
    }
}
