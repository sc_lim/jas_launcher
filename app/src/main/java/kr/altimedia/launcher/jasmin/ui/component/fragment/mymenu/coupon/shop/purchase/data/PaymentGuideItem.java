/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data;

import android.os.Parcel;
import android.os.Parcelable;

public class PaymentGuideItem implements Parcelable {
    private int guide;
    private int guideImage;
    private int guideStepIcon;

    public PaymentGuideItem(int guide, int guideImage, int guideStepIcon) {
        this.guide = guide;
        this.guideImage = guideImage;
        this.guideStepIcon = guideStepIcon;
    }

    protected PaymentGuideItem(Parcel in) {
        guide = in.readInt();
        guideImage = in.readInt();
        guideStepIcon = in.readInt();
    }

    public static final Creator<PaymentGuideItem> CREATOR = new Creator<PaymentGuideItem>() {
        @Override
        public PaymentGuideItem createFromParcel(Parcel in) {
            return new PaymentGuideItem(in);
        }

        @Override
        public PaymentGuideItem[] newArray(int size) {
            return new PaymentGuideItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(guide);
        dest.writeInt(guideImage);
        dest.writeInt(guideStepIcon);
    }

    public int getGuide() {
        return guide;
    }

    public int getGuideImage() {
        return guideImage;
    }

    public int getGuideStepIcon() {
        return guideStepIcon;
    }
}
