/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.message.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.message.obj.Message;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class MessagePresenter extends Presenter {
    private static final String TAG = MessagePresenter.class.getSimpleName();
    private onKeyListener onKeyListener;

    public MessagePresenter(MessagePresenter.onKeyListener onKeyListener) {
        this.onKeyListener = onKeyListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_message, parent, false);
        MessagePresenterViewHolder viewHolder = new MessagePresenterViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (item == null) {
            viewHolder.view.setVisibility(View.INVISIBLE);
            return;
        }

        MessagePresenterViewHolder vh = (MessagePresenterViewHolder) viewHolder;
        Message message = (Message) item;

        vh.setDate(message);
        vh.view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }

    public MessagePresenter.onKeyListener getOnKeyListener() {
        return onKeyListener;
    }

    public class MessagePresenterViewHolder extends ViewHolder {
        TextView messageTitle;
        TextView messageDate;

        public MessagePresenterViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            messageTitle = view.findViewById(R.id.messageTitle);
            messageDate = view.findViewById(R.id.messageDate);

        }

        public void setDate(Message message) {
            messageTitle.setText(message.getTitle());
            String date = TimeUtil.getModifiedDate(message.getDate(), "dd. MM. yyyy");
            messageDate.setText(date);
        }
    }

    public interface onKeyListener {
        boolean onKey(int keyCode, Message message);
    }
}
