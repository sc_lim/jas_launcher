/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class FragmentUtil {
    public  static Context getContext(Fragment fragment) {
        return (Context)(Build.VERSION.SDK_INT >= 23 ? fragment.getContext() : fragment.getActivity());
    }

    private FragmentUtil() {
    }

    public static class spaceDecoration extends RecyclerView.ItemDecoration {
        private int rightSpace = -1;
        private int bottomSpace = -1;

        public spaceDecoration(float rightSpace, float bottomSpace, Context context) {
            this.rightSpace = convertDpToPixel(rightSpace, context);
            this.bottomSpace = convertDpToPixel(bottomSpace, context);
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);

            if (rightSpace != -1) {
                outRect.right = outRect.right + rightSpace;
            }

            if (bottomSpace != -1) {
                outRect.bottom = outRect.bottom + bottomSpace;
            }
        }
    }

    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);

        return (int) px;
    }
}
