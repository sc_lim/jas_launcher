/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class TimeSetView extends RelativeLayout {
    private LimitTextView hourView;
    private LimitTextView minView;

    private final View.OnKeyListener onKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            boolean isConsumed = false;
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    input(v, 6);
                    isConsumed = true;
                    break;
                case KeyEvent.KEYCODE_BACK:
                    input(v, 2);
                    isConsumed = true;
                    break;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    isConsumed = delete(v);
                    break;
                case KeyEvent.KEYCODE_DPAD_CENTER:
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    isConsumed = clickView(v, keyCode);
                    break;
            }

            return isConsumed;
        }
    };

    public TimeSetView(Context context) {
        super(context);
        initView();
    }

    public TimeSetView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public TimeSetView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.common_time_set, this, false);
        addView(v);

        hourView = v.findViewById(R.id.hour_view_1);
        minView = v.findViewById(R.id.min_view_1);

        hourView.setOnKeyListener(onKeyListener);
        minView.setOnKeyListener(onKeyListener);
    }

    private void input(View view, int inputDigit) {
        LimitTextView limitTextView = (LimitTextView) view;

        if (limitTextView.isFull()) {
            requestInput(limitTextView, inputDigit);
            return;
        }

        String mixText = limitTextView.getString() + inputDigit;
        int inputValue = Integer.parseInt(mixText);

        if (checkOverLimit(limitTextView, inputValue)) {
            return;
        }

        if (isLimitValue(limitTextView, inputValue)) {
            return;
        }

        requestInput(limitTextView, inputValue);

        if (limitTextView == hourView && limitTextView.isFull()) {
            minView.requestFocus();
        }
    }

    private void requestInput(LimitTextView limitTextView, int digit) {
        String current = limitTextView.getString();
        if (!limitTextView.isFull() && limitTextView.getString().startsWith("0")) {
            limitTextView.forceInput(current + digit);
            return;
        }

        limitTextView.input(digit);
    }

    private boolean checkOverLimit(LimitTextView limitTextView, int inputValue) {
        if (limitTextView.isOverLimit(inputValue)) {
            limitTextView.clear();
            toastWrongTimeSet();

            return true;
        }

        return false;
    }

    private boolean isLimitValue(LimitTextView limitTextView, int inputValue) {
        if (limitTextView == hourView) {
            if (hourView.isLimit(inputValue)) {
                hourView.forceInput("00");
                minView.requestFocus();

                return true;
            }
        } else if (limitTextView == minView) {
            if (minView.isLimit(inputValue)) {
                minView.clear();

                return true;
            }
        }

        return false;
    }

    private boolean delete(View view) {
        LimitTextView limitTextView = (LimitTextView) view;

        if (view == minView) {
            if (minView.isEmpty()) {
                hourView.requestFocus();
                return true;
            }
        } else if (view == hourView) {
            if (hourView.isEmpty()) {
                return false;
            }
        }

        requestDelete(limitTextView);
        return true;
    }

    private void requestDelete(LimitTextView limitTextView) {
        String mText = limitTextView.getString();
        int end = mText.length() - 1;
        if (end < 0) end = 0;

        String deletedText = mText.substring(0, end);

        mText = deletedText;

        limitTextView.setText(mText);
    }

    private boolean clickView(View view, int keyCode) {
        LimitTextView limitTextView = (LimitTextView) view;

        String text = limitTextView.getString();
        if (limitTextView.isEmpty()) {
            text = "00";
        } else if (!limitTextView.isFull()) {
            text = "0" + text;
        }

        limitTextView.forceInput(text);

        if (limitTextView == hourView) {
            minView.requestFocus();
        } else if (limitTextView == minView) {
            return keyCode != KeyEvent.KEYCODE_DPAD_RIGHT;
        }

        return true;
    }

    public void setTime(long time) {
        long hour = TimeUtil.getHour(time);
        long min = TimeUtil.getMin(time);

        if (hour < 10) {
            hourView.forceInput("0" + hour);
        } else {
            hourView.input((int) hour);
        }

        if (min < 10) {
            minView.forceInput("0" + min);
        } else {
            minView.input((int) min);
        }
    }

    public String getHourString() {
        return hourView.getString();
    }

    public String getMinString() {
        return minView.getString();
    }

    public String getTimeString() {
        return getHourString() + ":" + getMinString();
    }

    public long getHour() {
        long hour = 0;
        String hourString = getHourString();

        if (hourString != null && hourString.length() > 0 && !hourString.equals("")) {
            int h = Integer.parseInt(hourString);
            hour = TimeUtil.getHourTime(h);
        }

        return hour;
    }

    public long getMin() {
        long min = 0;
        String minString = getMinString();

        if (minString != null && minString.length() > 0 && !minString.equals("")) {
            int m = Integer.parseInt(minString);
            min = TimeUtil.getMinTime(m);
        }

        return min;
    }

    public long getTime() {
        return getHour() + getMin();
    }

    private void toastWrongTimeSet() {
        JasminToast.makeToast(getContext(), "wrong time");
    }

    public void addTextChangedListener(TextWatcher watcher) {
        hourView.addTextChangedListener(watcher);
        minView.addTextChangedListener(watcher);
    }
}
