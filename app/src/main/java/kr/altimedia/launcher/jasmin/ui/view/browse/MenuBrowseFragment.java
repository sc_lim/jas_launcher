/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import androidx.annotation.ColorInt;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.leanback.widget.ViewHolderTask;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.Map;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.TransitionListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.InvisibleRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.FragmentRow;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.PageRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.util.FragmentUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.StateMachine;
import kr.altimedia.launcher.jasmin.ui.view.util.TransitionHelper;

public class MenuBrowseFragment extends MenuBaseFragment {
    static final String HEADER_STACK_INDEX = "headerStackIndex";
    static final String HEADER_SHOW = "headerShow";
    private static final String IS_PAGE_ROW = "isPageRow";
    private static final String CURRENT_SELECTED_POSITION = "currentSelectedPosition";
    final StateMachine.State STATE_SET_ENTRANCE_START_STATE = new StateMachine.State("SET_ENTRANCE_START_STATE") {
        public void run() {
            MenuBrowseFragment.this.setEntranceTransitionStartState();
        }
    };
    final StateMachine.Event EVT_HEADER_VIEW_CREATED = new StateMachine.Event("headerFragmentViewCreated");
    final StateMachine.Event EVT_MAIN_FRAGMENT_VIEW_CREATED = new StateMachine.Event("mainFragmentViewCreated");
    final StateMachine.Event EVT_SCREEN_DATA_READY = new StateMachine.Event("screenDataReady");
    static final String TAG = "BrowseFragment";
    private static final String LB_HEADERS_BACKSTACK = "lbHeadersBackStack_";
    static final boolean DEBUG = true;
    public static final int HEADERS_ENABLED = 1;
    public static final int HEADERS_HIDDEN = 2;
    public static final int HEADERS_DISABLED = 3;
    private MenuBrowseFragment.MainFragmentAdapterRegistry mMainFragmentAdapterRegistry = new MenuBrowseFragment.MainFragmentAdapterRegistry();
    MenuBrowseFragment.MainFragmentAdapter mMainFragmentAdapter;
    Fragment mMainFragment;
    HeadersFragment mHeadersFragment;
    MenuBrowseFragment.MainFragmentRowsAdapter mMainFragmentRowsAdapter;
    ListRowDataAdapter mMainFragmentListRowDataAdapter;
    private ObjectAdapter mAdapter;
    private PresenterSelector mAdapterPresenter;
    private int mHeadersState = 1;
    private int mBrandColor = 0;
    private boolean mBrandColorSet;
    BrowseFrameLayout mBrowseFrame;
    private ScaleFrameLayout mScaleFrameLayout;
    boolean mHeadersBackStackEnabled = true;
    String mWithHeadersBackStackName;
    boolean mShowingHeaders = true;
    boolean mCanShowHeaders = true;
    private int mContainerListMarginStart;
    private int mContainerListAlignTop;
    private boolean mMainFragmentScaleEnabled = true;
    OnItemViewSelectedListener mExternalOnItemViewSelectedListener;
    private OnItemViewClickedListener mOnItemViewClickedListener;
    private int mSelectedPosition = -1;
    private float mScaleFactor;
    boolean mIsPageRow;
    Object mPageRow;
    boolean mStopped = true;
    private PresenterSelector mHeaderPresenterSelector;
    private final MenuBrowseFragment.SetSelectionRunnable mSetSelectionRunnable = new MenuBrowseFragment.SetSelectionRunnable();
    Object mSceneWithHeaders;
    Object mSceneWithoutHeaders;
    private Object mSceneAfterEntranceTransition;
    Object mHeadersTransition;
    MenuBrowseFragment.BackStackListener mBackStackChangedListener;
    MenuBrowseFragment.BrowseTransitionListener mBrowseTransitionListener;
    private static final String ARG_TITLE = MenuBrowseFragment.class.getCanonicalName() + ".title";
    private static final String ARG_HEADERS_STATE = MenuBrowseFragment.class.getCanonicalName() + ".headersState";
    private final BrowseFrameLayout.OnFocusSearchListener mOnFocusSearchListener = new BrowseFrameLayout.OnFocusSearchListener() {
        public View onFocusSearch(View focused, int direction) {
            if (MenuBrowseFragment.this.mCanShowHeaders && MenuBrowseFragment.this.isInHeadersTransition()) {
                return focused;
            } else if (MenuBrowseFragment.this.getTitleView() != null && focused != MenuBrowseFragment.this.getTitleView() && direction == 33) {
                return MenuBrowseFragment.this.getTitleView();
            } else if (MenuBrowseFragment.this.getTitleView() != null && MenuBrowseFragment.this.getTitleView().hasFocus() && direction == 130) {
                return (View) (MenuBrowseFragment.this.mCanShowHeaders && MenuBrowseFragment.this.mShowingHeaders ? MenuBrowseFragment.this.mHeadersFragment.getVerticalGridView() : MenuBrowseFragment.this.mMainFragment.getView());
            } else {
                boolean isRtl = ViewCompat.getLayoutDirection(focused) == ViewCompat.LAYOUT_DIRECTION_RTL;
                int towardStart = isRtl ? 66 : 17;
                int towardEnd = isRtl ? 17 : 66;
                if (MenuBrowseFragment.this.mCanShowHeaders && direction == towardStart) {
                    return (View) (!MenuBrowseFragment.this.isVerticalScrolling() && !MenuBrowseFragment.this.mShowingHeaders && MenuBrowseFragment.this.isHeadersDataReady() ? MenuBrowseFragment.this.mHeadersFragment.getVerticalGridView() : focused);
                } else if (direction == towardEnd) {
                    if (MenuBrowseFragment.this.isVerticalScrolling()) {
                        return focused;
                    } else {
                        return MenuBrowseFragment.this.mMainFragment != null && MenuBrowseFragment.this.mMainFragment.getView() != null ? MenuBrowseFragment.this.mMainFragment.getView() : focused;
                    }
                } else {
                    return direction == 130 && MenuBrowseFragment.this.mShowingHeaders ? focused : null;
                }
            }
        }
    };
    private final BrowseFrameLayout.OnChildFocusListener mOnChildFocusListener = new BrowseFrameLayout.OnChildFocusListener() {
        public boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
            if (MenuBrowseFragment.this.getChildFragmentManager().isDestroyed()) {
                return true;
            } else if (MenuBrowseFragment.this.mCanShowHeaders && MenuBrowseFragment.this.mShowingHeaders && MenuBrowseFragment.this.mHeadersFragment != null && MenuBrowseFragment.this.mHeadersFragment.getView() != null && MenuBrowseFragment.this.mHeadersFragment.getView().requestFocus(direction, previouslyFocusedRect)) {
                return true;
            } else if (MenuBrowseFragment.this.mMainFragment != null && MenuBrowseFragment.this.mMainFragment.getView() != null && MenuBrowseFragment.this.mMainFragment.getView().requestFocus(direction, previouslyFocusedRect)) {
                return true;
            } else {
                return MenuBrowseFragment.this.getTitleView() != null && MenuBrowseFragment.this.getTitleView().requestFocus(direction, previouslyFocusedRect);
            }
        }

        public void onRequestChildFocus(View child, View focused) {
            if (!MenuBrowseFragment.this.getChildFragmentManager().isDestroyed()) {
                if (MenuBrowseFragment.this.mCanShowHeaders && !MenuBrowseFragment.this.isInHeadersTransition()) {
                    int childId = child.getId();
                    if (childId == R.id.browse_container_dock && MenuBrowseFragment.this.mShowingHeaders) {
                        MenuBrowseFragment.this.startHeadersTransitionInternal(false);
                    } else if (childId == R.id.browse_headers_dock && !MenuBrowseFragment.this.mShowingHeaders) {
                        MenuBrowseFragment.this.startHeadersTransitionInternal(true);
                    }

                }
            }
        }
    };
    private HeadersFragment.OnHeaderClickedListener mHeaderClickedListener = new HeadersFragment.OnHeaderClickedListener() {
        public void onHeaderClicked(RowHeaderPresenter.ViewHolder viewHolder, Row row) {
            if (MenuBrowseFragment.this.mCanShowHeaders && MenuBrowseFragment.this.mShowingHeaders && !MenuBrowseFragment.this.isInHeadersTransition()) {
                if (MenuBrowseFragment.this.mMainFragment != null && MenuBrowseFragment.this.mMainFragment.getView() != null) {
                    MenuBrowseFragment.this.startHeadersTransitionInternal(false);
                    MenuBrowseFragment.this.mMainFragment.getView().requestFocus();
                }
            }
        }
    };
    private HeadersFragment.OnHeaderViewSelectedListener mHeaderViewSelectedListener = new HeadersFragment.OnHeaderViewSelectedListener() {
        public void onHeaderSelected(RowHeaderPresenter.ViewHolder viewHolder, Row row) {
            int position = MenuBrowseFragment.this.mHeadersFragment.getSelectedPosition();
            if (MenuBrowseFragment.this.mShowingHeaders) {
                MenuBrowseFragment.this.onRowSelected(position);
            }

        }
    };
    private final RecyclerView.OnScrollListener mWaitScrollFinishAndCommitMainFragment = new RecyclerView.OnScrollListener() {
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == 0) {
                recyclerView.removeOnScrollListener(this);
                if (!MenuBrowseFragment.this.mStopped) {
                    MenuBrowseFragment.this.commitMainFragment();
                }
            }

        }
    };

    public MenuBrowseFragment() {
    }

    void createStateMachineStates() {
        super.createStateMachineStates();
        this.mStateMachine.addState(this.STATE_SET_ENTRANCE_START_STATE);
    }

    void createStateMachineTransitions() {
        super.createStateMachineTransitions();
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_ON_PREPARED, this.STATE_SET_ENTRANCE_START_STATE, this.EVT_HEADER_VIEW_CREATED);
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_ON_PREPARED, this.STATE_ENTRANCE_ON_PREPARED_ON_CREATEVIEW, this.EVT_MAIN_FRAGMENT_VIEW_CREATED);
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_ON_PREPARED, this.STATE_ENTRANCE_PERFORM, this.EVT_SCREEN_DATA_READY);
    }

    private boolean createMainFragment(ObjectAdapter adapter, int position) {
        Object item = null;
        if (this.mCanShowHeaders) {
            if (adapter == null || adapter.size() == 0) {
                return false;
            }

            if (position < 0) {
                position = 0;
            } else if (position >= adapter.size()) {
                throw new IllegalArgumentException(String.format("Invalid position %d requested", position));
            }

            item = adapter.get(position);
        }

        if (item instanceof FragmentRow) {
            boolean oldIsPageRow = this.mIsPageRow;
            Object oldPageRow = this.mPageRow;
            this.mIsPageRow = this.mCanShowHeaders && item instanceof PageRow;
            this.mPageRow = this.mIsPageRow ? item : null;
            boolean swap;
            if (this.mMainFragment == null) {
                swap = true;
            } else if (oldIsPageRow) {
                if (this.mIsPageRow) {
                    if (oldPageRow == null) {
                        swap = false;
                    } else {
                        swap = oldPageRow != this.mPageRow;
                    }
                } else {
                    swap = true;
                }
            } else {
                swap = this.mIsPageRow;
            }
            this.mMainFragment = this.mMainFragmentAdapterRegistry.createFragment(item);
            if (!(this.mMainFragment instanceof MenuBrowseFragment.MainFragmentAdapterProvider)) {
//                throw new IllegalArgumentException("Fragment must implement MainFragmentAdapterProvider");
            }else{
                this.setMainFragmentAdapter();
            }


            return true;
        } else {
            boolean oldIsPageRow = this.mIsPageRow;
            Object oldPageRow = this.mPageRow;
            this.mIsPageRow = this.mCanShowHeaders && item instanceof PageRow;
            this.mPageRow = this.mIsPageRow ? item : null;
            boolean swap;
            if (this.mMainFragment == null) {
                swap = true;
            } else if (oldIsPageRow) {
                if (this.mIsPageRow) {
                    if (oldPageRow == null) {
                        swap = false;
                    } else {
                        swap = oldPageRow != this.mPageRow;
                    }
                } else {
                    swap = true;
                }
            } else {
                swap = this.mIsPageRow;
            }
            if (swap) {
                this.mMainFragment = this.mMainFragmentAdapterRegistry.createFragment(item);
                if (!(this.mMainFragment instanceof MenuBrowseFragment.MainFragmentAdapterProvider)) {
                    throw new IllegalArgumentException("Fragment must implement MainFragmentAdapterProvider");
                }

                this.setMainFragmentAdapter();
            }

            return swap;
        }


    }

    void setMainFragmentAdapter() {
        this.mMainFragmentAdapter = ((MenuBrowseFragment.MainFragmentAdapterProvider) this.mMainFragment).getMainFragmentAdapter();
        this.mMainFragmentAdapter.setFragmentHost(new MenuBrowseFragment.FragmentHostImpl());
        if (!this.mIsPageRow) {
            if (this.mMainFragment instanceof MenuBrowseFragment.MainFragmentRowsAdapterProvider) {
                this.setMainFragmentRowsAdapter(((MenuBrowseFragment.MainFragmentRowsAdapterProvider) this.mMainFragment).getMainFragmentRowsAdapter());
            } else {
                this.setMainFragmentRowsAdapter(null);
            }

            this.mIsPageRow = this.mMainFragmentRowsAdapter == null;
        } else {
            this.setMainFragmentRowsAdapter(null);
        }

    }

    public static Bundle createArgs(Bundle args, String title, int headersState) {
        if (args == null) {
            args = new Bundle();
        }

        args.putString(ARG_TITLE, title);
        args.putInt(ARG_HEADERS_STATE, headersState);
        return args;
    }

    public void setBrandColor(@ColorInt int color) {
        this.mBrandColor = color;
        this.mBrandColorSet = true;
        if (this.mHeadersFragment != null) {
            this.mHeadersFragment.setBackgroundColor(this.mBrandColor);
        }

    }

    @ColorInt
    public int getBrandColor() {
        return this.mBrandColor;
    }

    private void updateWrapperPresenter() {
        if (this.mAdapter == null) {
            this.mAdapterPresenter = null;
        } else {
            final PresenterSelector adapterPresenter = this.mAdapter.getPresenterSelector();
            if (adapterPresenter == null) {
                throw new IllegalArgumentException("Adapter.getPresenterSelector() is null");
            } else if (adapterPresenter != this.mAdapterPresenter) {
                this.mAdapterPresenter = adapterPresenter;
                Presenter[] presenters = adapterPresenter.getPresenters();
                final Presenter invisibleRowPresenter = new InvisibleRowPresenter();
                final Presenter[] allPresenters = new Presenter[presenters.length + 1];
                System.arraycopy(allPresenters, 0, presenters, 0, presenters.length);
                allPresenters[allPresenters.length - 1] = invisibleRowPresenter;
                this.mAdapter.setPresenterSelector(new PresenterSelector() {
                    public Presenter getPresenter(Object item) {
                        Row row = (Row) item;
                        return (Presenter) (row.isRenderedAsRowView() ? adapterPresenter.getPresenter(item) : invisibleRowPresenter);
                    }

                    public Presenter[] getPresenters() {
                        return allPresenters;
                    }
                });
            }
        }
    }

    public void setAdapter(ObjectAdapter adapter) {
        this.mAdapter = adapter;
        this.updateWrapperPresenter();
        if (this.getView() != null) {
            this.updateMainFragmentRowsAdapter();
            this.mHeadersFragment.setAdapter(this.mAdapter);
        }
    }

    void setMainFragmentRowsAdapter(MenuBrowseFragment.MainFragmentRowsAdapter mainFragmentRowsAdapter) {
        if (mainFragmentRowsAdapter != this.mMainFragmentRowsAdapter) {
            if (this.mMainFragmentRowsAdapter != null) {
                this.mMainFragmentRowsAdapter.setAdapter((ObjectAdapter) null);
            }

            this.mMainFragmentRowsAdapter = mainFragmentRowsAdapter;
            if (this.mMainFragmentRowsAdapter != null) {
                this.mMainFragmentRowsAdapter.setOnItemViewSelectedListener(new MenuBrowseFragment.MainFragmentItemViewSelectedListener(this.mMainFragmentRowsAdapter));
                this.mMainFragmentRowsAdapter.setOnItemViewClickedListener(this.mOnItemViewClickedListener);
            }

            this.updateMainFragmentRowsAdapter();
        }
    }

    void updateMainFragmentRowsAdapter() {
        if (this.mMainFragmentListRowDataAdapter != null) {
            this.mMainFragmentListRowDataAdapter.detach();
            this.mMainFragmentListRowDataAdapter = null;
        }

        if (this.mMainFragmentRowsAdapter != null) {
            this.mMainFragmentListRowDataAdapter = this.mAdapter == null ? null : new ListRowDataAdapter(this.mAdapter);
            this.mMainFragmentRowsAdapter.setAdapter(this.mMainFragmentListRowDataAdapter);
        }

    }

    public final MainFragmentAdapterRegistry getMainFragmentRegistry() {
        return this.mMainFragmentAdapterRegistry;
    }

    public ObjectAdapter getAdapter() {
        return this.mAdapter;
    }

    public void setOnItemViewSelectedListener(OnItemViewSelectedListener listener) {
        this.mExternalOnItemViewSelectedListener = listener;
    }

    public OnItemViewSelectedListener getOnItemViewSelectedListener() {
        return this.mExternalOnItemViewSelectedListener;
    }

    public RowsFragment getRowsFragment() {
        return this.mMainFragment instanceof RowsFragment ? (RowsFragment) this.mMainFragment : null;
    }

    public Fragment getMainFragment() {
        return this.mMainFragment;
    }

    public HeadersFragment getHeadersFragment() {
        return this.mHeadersFragment;
    }

    public void setOnItemViewClickedListener(OnItemViewClickedListener listener) {
        this.mOnItemViewClickedListener = listener;
        if (this.mMainFragmentRowsAdapter != null) {
            this.mMainFragmentRowsAdapter.setOnItemViewClickedListener(listener);
        }

    }

    public OnItemViewClickedListener getOnItemViewClickedListener() {
        return this.mOnItemViewClickedListener;
    }

    public void startHeadersTransition(boolean withHeaders) {
        if (!this.mCanShowHeaders) {
            throw new IllegalStateException("Cannot start headers transition");
        } else if (!this.isInHeadersTransition() && this.mShowingHeaders != withHeaders) {
            this.startHeadersTransitionInternal(withHeaders);
        }
    }

    public boolean isInHeadersTransition() {
        return this.mHeadersTransition != null;
    }

    public boolean isShowingHeaders() {
        return this.mShowingHeaders;
    }

    public void setBrowseTransitionListener(BrowseTransitionListener listener) {
        this.mBrowseTransitionListener = listener;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void enableRowScaling(boolean enable) {
        this.enableMainFragmentScaling(enable);
    }

    public void enableMainFragmentScaling(boolean enable) {
        this.mMainFragmentScaleEnabled = enable;
    }

    void startHeadersTransitionInternal(final boolean withHeaders) {
        if (!this.getFragmentManager().isDestroyed()) {
            if (this.isHeadersDataReady()) {
                this.mShowingHeaders = withHeaders;
                this.mMainFragmentAdapter.onTransitionPrepare();
                this.mMainFragmentAdapter.onTransitionStart();
                this.onExpandTransitionStart(!withHeaders, new Runnable() {
                    public void run() {
                        MenuBrowseFragment.this.mHeadersFragment.onTransitionPrepare();
                        MenuBrowseFragment.this.mHeadersFragment.onTransitionStart();
                        MenuBrowseFragment.this.createHeadersTransition();
                        if (MenuBrowseFragment.this.mBrowseTransitionListener != null) {
                            MenuBrowseFragment.this.mBrowseTransitionListener.onHeadersTransitionStart(withHeaders);
                        }

                        TransitionHelper.runTransition(withHeaders ? MenuBrowseFragment.this.mSceneWithHeaders : MenuBrowseFragment.this.mSceneWithoutHeaders, MenuBrowseFragment.this.mHeadersTransition);
                        if (MenuBrowseFragment.this.mHeadersBackStackEnabled) {
                            if (!withHeaders) {
                                MenuBrowseFragment.this.getFragmentManager().beginTransaction().addToBackStack(MenuBrowseFragment.this.mWithHeadersBackStackName).commit();
                            } else {
                                int index = MenuBrowseFragment.this.mBackStackChangedListener.mIndexOfHeadersBackStack;
                                if (index >= 0) {
                                    FragmentManager.BackStackEntry entry = MenuBrowseFragment.this.getFragmentManager().getBackStackEntryAt(index);
                                    MenuBrowseFragment.this.getFragmentManager().popBackStackImmediate(entry.getId(), 1);
                                }
                            }
                        }

                    }
                });
            }
        }
    }

    boolean isVerticalScrolling() {
        return this.mHeadersFragment.isScrolling() || this.mMainFragmentAdapter.isScrolling();
    }

    final boolean isHeadersDataReady() {
        return this.mAdapter != null && this.mAdapter.size() != 0;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentSelectedPosition", this.mSelectedPosition);
        outState.putBoolean("isPageRow", this.mIsPageRow);
        if (this.mBackStackChangedListener != null) {
            this.mBackStackChangedListener.save(outState);
        } else {
            outState.putBoolean("headerShow", this.mShowingHeaders);
        }

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context context = FragmentUtil.getContext(this);
        TypedArray ta = context.obtainStyledAttributes(R.styleable.LeanbackTheme);
        this.mContainerListMarginStart = (int) ta.getDimension(R.styleable.LeanbackTheme_browseRowsMarginStart, (float) context.getResources().getDimensionPixelSize(R.dimen.lb_browse_rows_margin_start));
        this.mContainerListAlignTop = (int) ta.getDimension(R.styleable.LeanbackTheme_browseRowsMarginTop, (float) context.getResources().getDimensionPixelSize(R.dimen.lb_browse_rows_margin_top));
        ta.recycle();
        this.readArguments(this.getArguments());
        if (this.mCanShowHeaders) {
            if (this.mHeadersBackStackEnabled) {
                this.mWithHeadersBackStackName = "lbHeadersBackStack_" + this;
                this.mBackStackChangedListener = new MenuBrowseFragment.BackStackListener();
                this.getFragmentManager().addOnBackStackChangedListener(this.mBackStackChangedListener);
                this.mBackStackChangedListener.load(savedInstanceState);
            } else if (savedInstanceState != null) {
                this.mShowingHeaders = savedInstanceState.getBoolean("headerShow");
            }
        }

        this.mScaleFactor = this.getResources().getFraction(R.fraction.lb_browse_rows_scale, 1, 1);
    }

    public void onDestroyView() {
        this.setMainFragmentRowsAdapter(null);
        this.mPageRow = null;
        this.mMainFragmentAdapter = null;
        this.mMainFragment = null;
        this.mHeadersFragment = null;
        super.onDestroyView();
    }

    public void onDestroy() {
        if (this.mBackStackChangedListener != null) {
            this.getFragmentManager().removeOnBackStackChangedListener(this.mBackStackChangedListener);
        }

        super.onDestroy();
    }

    public void setOnHeaderClickedListener(HeadersFragment.OnHeaderClickedListener listener) {
        this.mHeaderClickedListener = listener;
        this.mHeadersFragment.setOnHeaderClickedListener(this.mHeaderClickedListener);
    }

    public void setOnHeaderViewSelectedListener(HeadersFragment.OnHeaderViewSelectedListener listener) {
        this.mHeaderViewSelectedListener = listener;
        this.mHeadersFragment.setOnHeaderViewSelectedListener(this.mHeaderViewSelectedListener);
    }

    public HeadersFragment onCreateHeadersFragment() {
        return new HeadersFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.getChildFragmentManager().findFragmentById(R.id.scaleFrame) == null) {
            this.mHeadersFragment = this.onCreateHeadersFragment();
            this.createMainFragment(this.mAdapter, this.mSelectedPosition);
            FragmentTransaction ft = this.getChildFragmentManager().beginTransaction().replace(R.id.browse_headers_dock, this.mHeadersFragment);
            if (this.mMainFragment != null) {
                ft.replace(R.id.scaleFrame, this.mMainFragment);
            } else {
                this.mMainFragmentAdapter = new MainFragmentAdapter((Fragment) null);
                this.mMainFragmentAdapter.setFragmentHost(new FragmentHostImpl());
            }
            ft.commit();
        } else {
            this.mHeadersFragment = (HeadersFragment) this.getChildFragmentManager().findFragmentById(R.id.browse_headers_dock);
            this.mMainFragment = this.getChildFragmentManager().findFragmentById(R.id.scaleFrame);
            this.mIsPageRow = savedInstanceState != null && savedInstanceState.getBoolean("isPageRow", false);
            this.mSelectedPosition = savedInstanceState != null ? savedInstanceState.getInt("currentSelectedPosition", 0) : 0;
            this.setMainFragmentAdapter();
        }

        this.mHeadersFragment.setHeadersGone(!this.mCanShowHeaders);
        if (this.mHeaderPresenterSelector != null) {
            this.mHeadersFragment.setPresenterSelector(this.mHeaderPresenterSelector);
        }

        this.mHeadersFragment.setAdapter(this.mAdapter);
        this.mHeadersFragment.setOnHeaderViewSelectedListener(this.mHeaderViewSelectedListener);
        this.mHeadersFragment.setOnHeaderClickedListener(this.mHeaderClickedListener);
        View root = inflater.inflate(R.layout.fragment_browse_menu, container, false);
        this.getProgressBarManager().setRootView((ViewGroup) root);
        this.mBrowseFrame = (BrowseFrameLayout) root.findViewById(R.id.browse_frame);
        this.mBrowseFrame.setOnChildFocusListener(this.mOnChildFocusListener);
        this.mBrowseFrame.setOnFocusSearchListener(this.mOnFocusSearchListener);
        this.installTitleView(inflater, this.mBrowseFrame, savedInstanceState);
        this.mScaleFrameLayout = (ScaleFrameLayout) root.findViewById(R.id.scaleFrame);
        if (this.mScaleFrameLayout != null) {

            this.mScaleFrameLayout.setPivotX(0.0F);
            this.mScaleFrameLayout.setPivotY((float) this.mContainerListAlignTop);
        }
        if (this.mBrandColorSet) {
            this.mHeadersFragment.setBackgroundColor(this.mBrandColor);
        }

        this.mSceneWithHeaders = TransitionHelper.createScene(this.mBrowseFrame, new Runnable() {
            public void run() {
                MenuBrowseFragment.this.showHeaders(true);
            }
        });
        this.mSceneWithoutHeaders = TransitionHelper.createScene(this.mBrowseFrame, new Runnable() {
            public void run() {
                MenuBrowseFragment.this.showHeaders(false);
            }
        });
        this.mSceneAfterEntranceTransition = TransitionHelper.createScene(this.mBrowseFrame, new Runnable() {
            public void run() {
                MenuBrowseFragment.this.setEntranceTransitionEndState();
            }
        });
        return root;
    }

    void createHeadersTransition() {
        this.mHeadersTransition = TransitionHelper.loadTransition(FragmentUtil.getContext(this), this.mShowingHeaders ? R.transition.lb_browse_headers_in : R.transition.lb_browse_headers_out);
        TransitionHelper.addTransitionListener(this.mHeadersTransition, new TransitionListener() {
            public void onTransitionStart(Object transition) {
            }

            public void onTransitionEnd(Object transition) {
                MenuBrowseFragment.this.mHeadersTransition = null;
                if (MenuBrowseFragment.this.mMainFragmentAdapter != null) {
                    MenuBrowseFragment.this.mMainFragmentAdapter.onTransitionEnd();
                    if (!MenuBrowseFragment.this.mShowingHeaders && MenuBrowseFragment.this.mMainFragment != null) {
                        View mainFragmentView = MenuBrowseFragment.this.mMainFragment.getView();
                        if (mainFragmentView != null && !mainFragmentView.hasFocus()) {
                            mainFragmentView.requestFocus();
                        }
                    }
                }

                if (MenuBrowseFragment.this.mHeadersFragment != null) {
                    MenuBrowseFragment.this.mHeadersFragment.onTransitionEnd();
                    if (MenuBrowseFragment.this.mShowingHeaders) {
                        VerticalGridView headerGridView = MenuBrowseFragment.this.mHeadersFragment.getVerticalGridView();
                        if (headerGridView != null && !headerGridView.hasFocus()) {
                            headerGridView.requestFocus();
                        }
                    }
                }

                MenuBrowseFragment.this.updateTitleViewVisibility();
                if (MenuBrowseFragment.this.mBrowseTransitionListener != null) {
                    MenuBrowseFragment.this.mBrowseTransitionListener.onHeadersTransitionStop(MenuBrowseFragment.this.mShowingHeaders);
                }

            }
        });
    }

    void updateTitleViewVisibility() {
        boolean showTitleView;
        if (!this.mShowingHeaders) {
            if (this.mIsPageRow && this.mMainFragmentAdapter != null) {
                showTitleView = this.mMainFragmentAdapter.mFragmentHost.mShowTitleView;
            } else {
                showTitleView = this.isFirstRowWithContent(this.mSelectedPosition);
            }

            if (showTitleView) {
                this.showTitle(6);
            } else {
                this.showTitle(false);
            }
        } else {
            if (this.mIsPageRow && this.mMainFragmentAdapter != null) {
                showTitleView = this.mMainFragmentAdapter.mFragmentHost.mShowTitleView;
            } else {
                showTitleView = this.isFirstRowWithContent(this.mSelectedPosition);
            }

            boolean showSearch = this.isFirstRowWithContentOrPageRow(this.mSelectedPosition);
            int flags = 0;
            if (showTitleView) {
                flags |= 2;
            }

            if (showSearch) {
                flags |= 4;
            }

            if (flags != 0) {
                this.showTitle(flags);
            } else {
                this.showTitle(false);
            }
        }

    }

    boolean isFirstRowWithContentOrPageRow(int rowPosition) {
        if (this.mAdapter != null && this.mAdapter.size() != 0) {
            for (int i = 0; i < this.mAdapter.size(); ++i) {
                Row row = (Row) this.mAdapter.get(i);
                if (row.isRenderedAsRowView() || row instanceof PageRow) {
                    return rowPosition == i;
                }
            }

            return true;
        } else {
            return true;
        }
    }

    boolean isFirstRowWithContent(int rowPosition) {
        if (this.mAdapter != null && this.mAdapter.size() != 0) {
            for (int i = 0; i < this.mAdapter.size(); ++i) {
                Row row = (Row) this.mAdapter.get(i);
                if (row.isRenderedAsRowView()) {
                    return rowPosition == i;
                }
            }

            return true;
        } else {
            return true;
        }
    }

    public void setHeaderPresenterSelector(PresenterSelector headerPresenterSelector) {
        this.mHeaderPresenterSelector = headerPresenterSelector;
        if (this.mHeadersFragment != null) {
            this.mHeadersFragment.setPresenterSelector(this.mHeaderPresenterSelector);
        }

    }

    private void setHeadersOnScreen(boolean onScreen) {
        View containerList = this.mHeadersFragment.getView();
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) containerList.getLayoutParams();
        lp.setMarginStart(onScreen ? 0 : -this.mContainerListMarginStart);
        containerList.setLayoutParams(lp);
    }

    void showHeaders(boolean show) {
        this.mHeadersFragment.setHeadersEnabled(show);
        this.setHeadersOnScreen(show);
        this.expandMainFragment(!show);
    }

    private void expandMainFragment(boolean expand) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) this.mScaleFrameLayout.getLayoutParams();
        params.setMarginStart(!expand ? this.mContainerListMarginStart : 0);
        this.mScaleFrameLayout.setLayoutParams(params);
        this.mMainFragmentAdapter.setExpand(expand);
        this.setMainFragmentAlignment();
        float scaleFactor = !expand && this.mMainFragmentScaleEnabled && this.mMainFragmentAdapter.isScalingEnabled() ? this.mScaleFactor : 1.0F;
        this.mScaleFrameLayout.setLayoutScaleY(scaleFactor);
        this.mScaleFrameLayout.setChildScale(scaleFactor);
    }

    void onRowSelected(int position) {
        this.mSetSelectionRunnable.post(position, 0, true);
    }

    void setSelection(int position, boolean smooth) {
        if (position != -1) {
            this.mSelectedPosition = position;
            if (this.mHeadersFragment != null && this.mMainFragmentAdapter != null) {
                this.mHeadersFragment.setSelectedPosition(position, smooth);
                this.replaceMainFragment(position);
                if (this.mMainFragmentRowsAdapter != null) {
                    this.mMainFragmentRowsAdapter.setSelectedPosition(position, smooth);
                }

                this.updateTitleViewVisibility();
            }
        }
    }

    private void replaceMainFragment(int position) {
        if (this.createMainFragment(this.mAdapter, position)) {
            this.swapToMainFragment();
            expandMainFragment(!mCanShowHeaders || !mShowingHeaders);
        }
    }

    final void commitMainFragment() {
        FragmentManager fm = this.getChildFragmentManager();
        Fragment currentFragment = fm.findFragmentById(R.id.scaleFrame);
        if (currentFragment != this.mMainFragment) {
            fm.beginTransaction().replace(R.id.scaleFrame, this.mMainFragment).commit();
        }

    }

    private void swapToMainFragment() {
        if (!this.mStopped) {
            VerticalGridView gridView = this.mHeadersFragment.getVerticalGridView();
            if (this.isShowingHeaders() && gridView != null && gridView.getScrollState() != 0) {
                this.getChildFragmentManager().beginTransaction().replace(R.id.scaleFrame, new Fragment()).commit();
                gridView.removeOnScrollListener(this.mWaitScrollFinishAndCommitMainFragment);
                gridView.addOnScrollListener(this.mWaitScrollFinishAndCommitMainFragment);
            } else {
                this.commitMainFragment();
            }
        }
    }

    public void setSelectedPosition(int position) {
        this.setSelectedPosition(position, true);
    }

    public int getSelectedPosition() {
        return this.mSelectedPosition;
    }

    public RowPresenter.ViewHolder getSelectedRowViewHolder() {
        if (this.mMainFragmentRowsAdapter != null) {
            int rowPos = this.mMainFragmentRowsAdapter.getSelectedPosition();
            return this.mMainFragmentRowsAdapter.findRowViewHolderByPosition(rowPos);
        } else {
            return null;
        }
    }

    public void setSelectedPosition(int position, boolean smooth) {
        this.mSetSelectionRunnable.post(position, 1, smooth);
    }

    public void setSelectedPosition(int rowPosition, boolean smooth, ViewHolderTask rowHolderTask) {
        if (this.mMainFragmentAdapterRegistry != null) {
            if (rowHolderTask != null) {
                this.startHeadersTransition(false);
            }

            if (this.mMainFragmentRowsAdapter != null) {
                this.mMainFragmentRowsAdapter.setSelectedPosition(rowPosition, smooth, rowHolderTask);
            }

        }
    }

    public void onStart() {
        super.onStart();
        this.mHeadersFragment.setAlignment(this.mContainerListAlignTop);
        this.setMainFragmentAlignment();
        if (this.mCanShowHeaders && this.mShowingHeaders && this.mHeadersFragment != null && this.mHeadersFragment.getView() != null) {
            this.mHeadersFragment.getView().requestFocus();
        } else if ((!this.mCanShowHeaders || !this.mShowingHeaders) && this.mMainFragment != null && this.mMainFragment.getView() != null) {
            this.mMainFragment.getView().requestFocus();
        }

        if (this.mCanShowHeaders) {
            this.showHeaders(this.mShowingHeaders);
        }

        this.mStateMachine.fireEvent(this.EVT_HEADER_VIEW_CREATED);
        this.mStopped = false;
        this.commitMainFragment();
        this.mSetSelectionRunnable.start();
    }

    public void onStop() {
        this.mStopped = true;
        this.mSetSelectionRunnable.stop();
        super.onStop();
    }

    private void onExpandTransitionStart(boolean expand, Runnable callback) {
        if (expand) {
            callback.run();
        } else {
            (new MenuBrowseFragment.ExpandPreLayout(callback, this.mMainFragmentAdapter, this.getView())).execute();
        }
    }

    private void setMainFragmentAlignment() {
        int alignOffset = this.mContainerListAlignTop;
        if (this.mMainFragmentScaleEnabled && this.mMainFragmentAdapter.isScalingEnabled() && this.mShowingHeaders) {
            alignOffset = (int) ((float) alignOffset / this.mScaleFactor + 0.5F);
        }

        this.mMainFragmentAdapter.setAlignment(alignOffset);
    }

    public final void setHeadersTransitionOnBackEnabled(boolean headersBackStackEnabled) {
        this.mHeadersBackStackEnabled = headersBackStackEnabled;
    }

    public final boolean isHeadersTransitionOnBackEnabled() {
        return this.mHeadersBackStackEnabled;
    }

    private void readArguments(Bundle args) {
        if (args != null) {
            if (args.containsKey(ARG_TITLE)) {
                this.setTitle(args.getString(ARG_TITLE));
            }

            if (args.containsKey(ARG_HEADERS_STATE)) {
                this.setHeadersState(args.getInt(ARG_HEADERS_STATE));
            }

        }
    }

    public void setHeadersState(int headersState) {
        if (headersState >= 1 && headersState <= 3) {
            if (headersState != this.mHeadersState) {
                this.mHeadersState = headersState;
                switch (headersState) {
                    case 1:
                        this.mCanShowHeaders = true;
                        this.mShowingHeaders = true;
                        break;
                    case 2:
                        this.mCanShowHeaders = true;
                        this.mShowingHeaders = false;
                        break;
                    case 3:
                        this.mCanShowHeaders = false;
                        this.mShowingHeaders = false;
                        break;
                    default:
                }

                if (this.mHeadersFragment != null) {
                    this.mHeadersFragment.setHeadersGone(!this.mCanShowHeaders);
                }
            }

        } else {
            throw new IllegalArgumentException("Invalid headers state: " + headersState);
        }
    }

    public int getHeadersState() {
        return this.mHeadersState;
    }

    protected Object createEntranceTransition() {
        return TransitionHelper.loadTransition(FragmentUtil.getContext(this), R.transition.lb_browse_entrance_transition);
    }

    protected void runEntranceTransition(Object entranceTransition) {
        TransitionHelper.runTransition(this.mSceneAfterEntranceTransition, entranceTransition);
    }

    protected void onEntranceTransitionPrepare() {
        this.mHeadersFragment.onTransitionPrepare();
        this.mMainFragmentAdapter.setEntranceTransitionState(false);
        this.mMainFragmentAdapter.onTransitionPrepare();
    }

    protected void onEntranceTransitionStart() {
        this.mHeadersFragment.onTransitionStart();
        this.mMainFragmentAdapter.onTransitionStart();
    }

    protected void onEntranceTransitionEnd() {
        if (this.mMainFragmentAdapter != null) {
            this.mMainFragmentAdapter.onTransitionEnd();
        }

        if (this.mHeadersFragment != null) {
            this.mHeadersFragment.onTransitionEnd();
        }

    }

    void setSearchOrbViewOnScreen(boolean onScreen) {
        View searchOrbView = this.getTitleViewAdapter().getSearchAffordanceView();
        if (searchOrbView != null) {
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) searchOrbView.getLayoutParams();
            lp.setMarginStart(onScreen ? 0 : -this.mContainerListMarginStart);
            searchOrbView.setLayoutParams(lp);
        }

    }

    void setEntranceTransitionStartState() {
        this.setHeadersOnScreen(false);
        this.setSearchOrbViewOnScreen(false);
    }

    void setEntranceTransitionEndState() {
        this.setHeadersOnScreen(this.mShowingHeaders);
        this.setSearchOrbViewOnScreen(true);
        this.mMainFragmentAdapter.setEntranceTransitionState(true);
    }

    private class ExpandPreLayout implements ViewTreeObserver.OnPreDrawListener {
        private final View mView;
        private final Runnable mCallback;
        private int mState;
        private MenuBrowseFragment.MainFragmentAdapter mainFragmentAdapter;
        static final int STATE_INIT = 0;
        static final int STATE_FIRST_DRAW = 1;
        static final int STATE_SECOND_DRAW = 2;

        ExpandPreLayout(Runnable callback, MenuBrowseFragment.MainFragmentAdapter adapter, View view) {
            this.mView = view;
            this.mCallback = callback;
            this.mainFragmentAdapter = adapter;
        }

        void execute() {
            this.mView.getViewTreeObserver().addOnPreDrawListener(this);
            this.mainFragmentAdapter.setExpand(false);
            this.mView.invalidate();
            this.mState = 0;
        }

        public boolean onPreDraw() {
            if (MenuBrowseFragment.this.getView() != null && FragmentUtil.getContext(MenuBrowseFragment.this) != null) {
                if (this.mState == 0) {
                    this.mainFragmentAdapter.setExpand(true);
                    this.mView.invalidate();
                    this.mState = 1;
                } else if (this.mState == 1) {
                    this.mCallback.run();
                    this.mView.getViewTreeObserver().removeOnPreDrawListener(this);
                    this.mState = 2;
                }

                return false;
            } else {
                this.mView.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        }
    }

    class MainFragmentItemViewSelectedListener implements OnItemViewSelectedListener {
        MenuBrowseFragment.MainFragmentRowsAdapter mMainFragmentRowsAdapter;

        public MainFragmentItemViewSelectedListener(MenuBrowseFragment.MainFragmentRowsAdapter fragmentRowsAdapter) {
            this.mMainFragmentRowsAdapter = fragmentRowsAdapter;
        }

        public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder rowViewHolder, Row row) {
            int position = this.mMainFragmentRowsAdapter.getSelectedPosition();
            MenuBrowseFragment.this.onRowSelected(position);
            if (MenuBrowseFragment.this.mExternalOnItemViewSelectedListener != null) {
                MenuBrowseFragment.this.mExternalOnItemViewSelectedListener.onItemSelected(itemViewHolder, item, rowViewHolder, row);
            }

        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static final class MainFragmentAdapterRegistry {
        private final Map<Class, FragmentFactory> mItemToFragmentFactoryMapping = new HashMap();
        private static final MenuBrowseFragment.FragmentFactory sDefaultFragmentFactory = new MenuBrowseFragment.ListRowFragmentFactory();

        public MainFragmentAdapterRegistry() {
            this.registerFragment(ListRow.class, sDefaultFragmentFactory);
        }

        public void registerFragment(Class rowClass, MenuBrowseFragment.FragmentFactory factory) {
            this.mItemToFragmentFactoryMapping.put(rowClass, factory);
        }

        public Fragment createFragment(Object item) {
            if (item instanceof FragmentRow) {
                return ((FragmentRow) item).getFragment();
            }

            MenuBrowseFragment.FragmentFactory fragmentFactory = item == null ? sDefaultFragmentFactory : this.mItemToFragmentFactoryMapping.get(item.getClass());
            if (fragmentFactory == null && !(item instanceof PageRow)) {
                fragmentFactory = sDefaultFragmentFactory;
            }

            return fragmentFactory.createFragment(item);
        }
    }

    public static class ListRowFragmentFactory extends MenuBrowseFragment.FragmentFactory<RowsFragment> {
        public ListRowFragmentFactory() {
        }

        public RowsFragment createFragment(Object row) {
            return new RowsFragment();
        }
    }

    public abstract static class FragmentFactory<T extends Fragment> {
        public FragmentFactory() {
        }

        public abstract T createFragment(Object var1);
    }

    public static class MainFragmentRowsAdapter<T extends Fragment> {
        private final T mFragment;

        public MainFragmentRowsAdapter(T fragment) {
            if (fragment == null) {
                throw new IllegalArgumentException("Fragment can't be null");
            } else {
                this.mFragment = fragment;
            }
        }

        public final T getFragment() {
            return this.mFragment;
        }

        public void setAdapter(ObjectAdapter adapter) {
        }

        public void setOnItemViewClickedListener(OnItemViewClickedListener listener) {
        }

        public void setOnItemViewSelectedListener(OnItemViewSelectedListener listener) {
        }

        public void setSelectedPosition(int rowPosition, boolean smooth, ViewHolderTask rowHolderTask) {
        }

        public void setSelectedPosition(int rowPosition, boolean smooth) {
        }

        public int getSelectedPosition() {
            return 0;
        }

        public RowPresenter.ViewHolder findRowViewHolderByPosition(int position) {
            return null;
        }
    }

    public interface MainFragmentRowsAdapterProvider {
        MenuBrowseFragment.MainFragmentRowsAdapter getMainFragmentRowsAdapter();
    }

    public interface MainFragmentAdapterProvider {
        MenuBrowseFragment.MainFragmentAdapter getMainFragmentAdapter();
    }

    public static class MainFragmentAdapter<T extends Fragment> {
        private boolean mScalingEnabled;
        private final T mFragment;
        MenuBrowseFragment.FragmentHostImpl mFragmentHost;

        public MainFragmentAdapter(T fragment) {
            this.mFragment = fragment;
        }

        public final T getFragment() {
            return this.mFragment;
        }

        public boolean isScrolling() {
            return false;
        }

        public void setExpand(boolean expand) {
        }

        public void setEntranceTransitionState(boolean state) {
        }

        public void setAlignment(int windowAlignOffsetFromTop) {
        }

        public boolean onTransitionPrepare() {
            return false;
        }

        public void onTransitionStart() {
        }

        public void onTransitionEnd() {
        }

        public boolean isScalingEnabled() {
            return this.mScalingEnabled;
        }

        public void setScalingEnabled(boolean scalingEnabled) {
            this.mScalingEnabled = scalingEnabled;
        }

        public final MenuBrowseFragment.FragmentHost getFragmentHost() {
            return this.mFragmentHost;
        }

        void setFragmentHost(MenuBrowseFragment.FragmentHostImpl fragmentHost) {
            this.mFragmentHost = fragmentHost;
        }
    }

    private final class FragmentHostImpl implements MenuBrowseFragment.FragmentHost {
        boolean mShowTitleView = true;

        FragmentHostImpl() {
        }

        public void notifyViewCreated(MenuBrowseFragment.MainFragmentAdapter fragmentAdapter) {
            MenuBrowseFragment.this.mStateMachine.fireEvent(MenuBrowseFragment.this.EVT_MAIN_FRAGMENT_VIEW_CREATED);
            if (!MenuBrowseFragment.this.mIsPageRow) {
                MenuBrowseFragment.this.mStateMachine.fireEvent(MenuBrowseFragment.this.EVT_SCREEN_DATA_READY);
            }

        }

        public void notifyDataReady(MenuBrowseFragment.MainFragmentAdapter fragmentAdapter) {
            if (MenuBrowseFragment.this.mMainFragmentAdapter != null && MenuBrowseFragment.this.mMainFragmentAdapter.getFragmentHost() == this) {
                if (MenuBrowseFragment.this.mIsPageRow) {
                    MenuBrowseFragment.this.mStateMachine.fireEvent(MenuBrowseFragment.this.EVT_SCREEN_DATA_READY);
                }
            }
        }

        public void showTitleView(boolean show) {
            this.mShowTitleView = show;
            if (MenuBrowseFragment.this.mMainFragmentAdapter != null && MenuBrowseFragment.this.mMainFragmentAdapter.getFragmentHost() == this) {
                if (MenuBrowseFragment.this.mIsPageRow) {
                    MenuBrowseFragment.this.updateTitleViewVisibility();
                }
            }
        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    public interface FragmentHost {
        void notifyViewCreated(MenuBrowseFragment.MainFragmentAdapter var1);

        void notifyDataReady(MenuBrowseFragment.MainFragmentAdapter var1);

        void showTitleView(boolean var1);
    }

    private final class SetSelectionRunnable implements Runnable {
        static final int TYPE_INVALID = -1;
        static final int TYPE_INTERNAL_SYNC = 0;
        static final int TYPE_USER_REQUEST = 1;
        private int mPosition;
        private int mType;
        private boolean mSmooth;

        SetSelectionRunnable() {
            this.reset();
        }

        void post(int position, int type, boolean smooth) {
            if (type >= this.mType) {
                this.mPosition = position;
                this.mType = type;
                this.mSmooth = smooth;
                MenuBrowseFragment.this.mBrowseFrame.removeCallbacks(this);
                if (!MenuBrowseFragment.this.mStopped) {
                    MenuBrowseFragment.this.mBrowseFrame.post(this);
                }
            }

        }

        public void run() {
            MenuBrowseFragment.this.setSelection(this.mPosition, this.mSmooth);
            this.reset();
        }

        public void stop() {
            MenuBrowseFragment.this.mBrowseFrame.removeCallbacks(this);
        }

        public void start() {
            if (this.mType != -1) {
                MenuBrowseFragment.this.mBrowseFrame.post(this);
            }

        }

        private void reset() {
            this.mPosition = -1;
            this.mType = -1;
            this.mSmooth = false;
        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static class BrowseTransitionListener {
        public BrowseTransitionListener() {
        }

        public void onHeadersTransitionStart(boolean withHeaders) {
        }

        public void onHeadersTransitionStop(boolean withHeaders) {
        }
    }

    final class BackStackListener implements FragmentManager.OnBackStackChangedListener {
        int mLastEntryCount = MenuBrowseFragment.this.getFragmentManager().getBackStackEntryCount();
        int mIndexOfHeadersBackStack = -1;

        BackStackListener() {
        }

        void load(Bundle savedInstanceState) {
            if (savedInstanceState != null) {
                this.mIndexOfHeadersBackStack = savedInstanceState.getInt("headerStackIndex", -1);
                MenuBrowseFragment.this.mShowingHeaders = this.mIndexOfHeadersBackStack == -1;
            } else if (!MenuBrowseFragment.this.mShowingHeaders) {
                MenuBrowseFragment.this.getFragmentManager().beginTransaction().addToBackStack(MenuBrowseFragment.this.mWithHeadersBackStackName).commit();
            }

        }

        void save(Bundle outState) {
            outState.putInt("headerStackIndex", this.mIndexOfHeadersBackStack);
        }

        public void onBackStackChanged() {
            if (MenuBrowseFragment.this.getFragmentManager() == null) {
            } else {
                int count = MenuBrowseFragment.this.getFragmentManager().getBackStackEntryCount();
                if (count > this.mLastEntryCount) {
                    FragmentManager.BackStackEntry entry = MenuBrowseFragment.this.getFragmentManager().getBackStackEntryAt(count - 1);
                    if (MenuBrowseFragment.this.mWithHeadersBackStackName.equals(entry.getName())) {
                        this.mIndexOfHeadersBackStack = count - 1;
                    }
                } else if (count < this.mLastEntryCount && this.mIndexOfHeadersBackStack >= count) {
                    if (!MenuBrowseFragment.this.isHeadersDataReady()) {
                        MenuBrowseFragment.this.getFragmentManager().beginTransaction().addToBackStack(MenuBrowseFragment.this.mWithHeadersBackStackName).commit();
                        return;
                    }

                    this.mIndexOfHeadersBackStack = -1;
                    if (!MenuBrowseFragment.this.mShowingHeaders) {
                        MenuBrowseFragment.this.startHeadersTransitionInternal(true);
                    }
                }

                this.mLastEntryCount = count;
            }
        }
    }
}
