/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.presenter;

import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.RowHeaderView;

import kr.altimedia.launcher.jasmin.R;
import com.altimedia.util.Log;

public abstract class BaseHeaderPresenter extends Presenter {
    private final String TAG = BaseHeaderPresenter.class.getSimpleName();
    protected final int mLayoutResourceId;
    protected final Paint mFontMeasurePaint;
    protected boolean mNullItemVisibilityGone;
    protected final boolean mAnimateSelect;

    public BaseHeaderPresenter() {
        this(R.layout.item_row_header);
    }

    public BaseHeaderPresenter(int layoutResourceId) {
        this(layoutResourceId, true);
    }

    public BaseHeaderPresenter(int layoutResourceId, boolean animateSelect) {
        this.mFontMeasurePaint = new Paint(1);
        this.mLayoutResourceId = layoutResourceId;
        this.mAnimateSelect = animateSelect;
    }

    public void setNullItemVisibilityGone(boolean nullItemVisibilityGone) {
        this.mNullItemVisibilityGone = nullItemVisibilityGone;
    }

    public boolean isNullItemVisibilityGone() {
        return this.mNullItemVisibilityGone;
    }

    protected int getLayoutResourceId() {
        return mLayoutResourceId;
    }

    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        BaseHeaderPresenter.ViewHolder vh = this.createHeaderViewHolder(parent);
        return vh;
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        this.onBindHeaderViewHolder((BaseHeaderPresenter.ViewHolder) viewHolder, item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        onUnbindHeaderViewHolder(viewHolder);
    }

    protected abstract BaseHeaderPresenter.ViewHolder createHeaderViewHolder(ViewGroup var1);

    protected abstract void onBindHeaderViewHolder(BaseHeaderPresenter.ViewHolder vh, Object item);

    protected void onUnbindHeaderViewHolder(Presenter.ViewHolder viewHolder) {
        if (this.mAnimateSelect) {
            this.setSelectLevel((BaseHeaderPresenter.ViewHolder) viewHolder, 0.0F);
        }
    }

    public final void setSelectLevel(BaseHeaderPresenter.ViewHolder holder, float selectLevel) {
        holder.mSelectLevel = selectLevel;
        this.onSelectLevelChanged(holder);
    }

    protected void onSelectLevelChanged(BaseHeaderPresenter.ViewHolder holder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSelectLevelChanged : " + (holder.mUnselectAlpha + holder.mSelectLevel * (1.0F - holder.mUnselectAlpha)));
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onSelectLevelChanged mSelectLevel : " + holder.mSelectLevel);
        }
        if (this.mAnimateSelect) {
            float selectLevel = holder.mSelectLevel;
            float offset = holder.mUnselectAlpha + holder.mSelectLevel * (1.0F - holder.mUnselectAlpha);
            holder.view.setAlpha(offset);
            holder.view.setTranslationX(-selectLevel * 30);
            holder.view.setTranslationY(-selectLevel * 35);
        }

    }

    public int getSpaceUnderBaseline(BaseHeaderPresenter.ViewHolder holder) {
        int space = 0;
        TextView header = holder.getHeaderTextView();
        if (holder != null) {
            space = header.getPaddingBottom();
            space += (int) getFontDescent(header, this.mFontMeasurePaint);
        }

        return space;
    }

    protected static float getFontDescent(TextView textView, Paint fontMeasurePaint) {
        if (fontMeasurePaint.getTextSize() != textView.getTextSize()) {
            fontMeasurePaint.setTextSize(textView.getTextSize());
        }

        if (fontMeasurePaint.getTypeface() != textView.getTypeface()) {
            fontMeasurePaint.setTypeface(textView.getTypeface());
        }

        return fontMeasurePaint.descent();
    }

    public static abstract class ViewHolder extends Presenter.ViewHolder {
        float mSelectLevel;
        float mUnselectAlpha;
        protected RowHeaderView mTitleView;

        public ViewHolder(View view) {
            super(view);
            this.mTitleView = view.findViewById(R.id.row_header1);
            initColors();
        }

        public ViewHolder(RowHeaderView view) {
            super(view);
            this.mTitleView = view;
            initColors();
        }

        public abstract TextView getHeaderTextView();

        public int getHeaderViewHeight() {
            Rect bounds = getHeaderViewBounds(mTitleView);
            return bounds.height();
        }

        protected Rect getHeaderViewBounds(TextView textView) {
            String message = (String) textView.getText();
            Rect bounds = new Rect();
            Paint textPaint = textView.getPaint();
            textPaint.getTextBounds(message, 0, message.length(), bounds);

            return bounds;
        }

        protected void initColors() {
            mUnselectAlpha = view.getResources().getFraction(
                    R.fraction.lb_browse_header_unselect_alpha, 1, 1);
        }

        protected final float getSelectLevel() {
            return this.mSelectLevel;
        }
    }
}
