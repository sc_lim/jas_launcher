/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.RowHeaderView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class RowHeaderPresenter extends BaseHeaderPresenter {
    private final String TAG = RowHeaderPresenter.class.getSimpleName();

    public RowHeaderPresenter() {
        this(R.layout.item_row_header);
    }

    public RowHeaderPresenter(int layoutResourceId) {
        this(layoutResourceId, true);
    }

    public RowHeaderPresenter(int layoutResourceId, boolean animateSelect) {
        super(layoutResourceId, animateSelect);
    }

    @Override
    protected BaseHeaderPresenter.ViewHolder createHeaderViewHolder(ViewGroup var1) {
        View view = LayoutInflater.from(var1.getContext()).inflate(getLayoutResourceId(), var1, false);
        return new RowHeaderPresenter.ViewHolder(view);
    }

    @Override
    protected void onBindHeaderViewHolder(BaseHeaderPresenter.ViewHolder viewHolder, Object item) {
        HeaderItem headerItem = item == null ? null : ((Row) item).getHeaderItem();
        RowHeaderPresenter.ViewHolder vh = (ViewHolder) viewHolder;
        if (headerItem == null) {
            if (vh.mTitleView != null) {
                vh.mTitleView.setText(null);
            }

            viewHolder.view.setContentDescription(null);
            if (mNullItemVisibilityGone) {
                viewHolder.view.setVisibility(View.GONE);
            }
        } else {
            if (vh.mTitleView != null) {
                String title;

                Object name = headerItem.getName();
                if (name instanceof String) {
                    title = (String) name;
                } else {
                    title = vh.view.getContext().getString((Integer) headerItem.getName());
                }

                vh.mTitleView.setText(title);
            }
//            if (vh.mDescriptionView != null) {
//                if (TextUtils.isEmpty(headerItem.getDescription())) {
//                    vh.mDescriptionView.setVisibility(View.GONE);
//                } else {
//                    vh.mDescriptionView.setVisibility(View.VISIBLE);
//                }
//                vh.mDescriptionView.setText(headerItem.getDescription());
//            }
            viewHolder.view.setContentDescription(headerItem.getContentDescription());
            viewHolder.view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onUnbindHeaderViewHolder(Presenter.ViewHolder viewHolder) {
        super.onUnbindHeaderViewHolder(viewHolder);
        RowHeaderPresenter.ViewHolder vh = (RowHeaderPresenter.ViewHolder) viewHolder;
        if (vh.mTitleView != null) {
            vh.mTitleView.setText((CharSequence) null);
        }
    }

    public static class ViewHolder extends BaseHeaderPresenter.ViewHolder {
        RowHeaderView mTitleView;

        public ViewHolder(View view) {
            super(view);
            this.mTitleView = (RowHeaderView) view.findViewById(R.id.row_header);
            initColors();
        }

        public ViewHolder(RowHeaderView view) {
            super(view);
            this.mTitleView = view;
            initColors();
        }

        @Override
        public TextView getHeaderTextView() {
            return mTitleView;
        }
    }
}
