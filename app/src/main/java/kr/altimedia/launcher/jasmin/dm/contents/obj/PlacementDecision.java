package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mc.kim on 03,08,2020
 */
public class PlacementDecision implements Parcelable {

    @SerializedName("adContentType")
    @Expose
    private String adContentType;
    @SerializedName("enablePlayControl")
    @Expose
    private String enablePlayControl;
    @SerializedName("opportunityType")
    @Expose
    @JsonAdapter(OpportunityTypeDeserializer.class)
    private OpportunityType opportunityType;
    @SerializedName("placement")
    @Expose
    private List<Placement> placement = null;
    @SerializedName("skipPolicy")
    @Expose
    private String skipPolicy;
    public final static Parcelable.Creator<PlacementDecision> CREATOR = new Creator<PlacementDecision>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PlacementDecision createFromParcel(Parcel in) {
            return new PlacementDecision(in);
        }

        public PlacementDecision[] newArray(int size) {
            return (new PlacementDecision[size]);
        }

    };

    protected PlacementDecision(Parcel in) {
        this.adContentType = ((String) in.readValue((String.class.getClassLoader())));
        this.enablePlayControl = ((String) in.readValue((String.class.getClassLoader())));
        this.opportunityType = ((OpportunityType) in.readValue((OpportunityType.class.getClassLoader())));
        in.readList(this.placement, (Placement.class.getClassLoader()));
        this.skipPolicy = ((String) in.readValue((String.class.getClassLoader())));
    }

    public PlacementDecision() {
    }

    public String getAdContentType() {
        return adContentType;
    }

    public void setAdContentType(String adContentType) {
        this.adContentType = adContentType;
    }

    public String getEnablePlayControl() {
        return enablePlayControl;
    }

    public void setEnablePlayControl(String enablePlayControl) {
        this.enablePlayControl = enablePlayControl;
    }

    public OpportunityType getOpportunityType() {
        return opportunityType;
    }


    public List<Placement> getPlacement() {
        return placement;
    }


    public String getSkipPolicy() {
        return skipPolicy;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(adContentType);
        dest.writeValue(enablePlayControl);
        dest.writeValue(opportunityType);
        dest.writeList(placement);
        dest.writeValue(skipPolicy);
    }

    public int describeContents() {
        return 0;
    }

    public static class Placement implements Parcelable {

        @SerializedName("adContentUrl")
        @Expose
        private String adContentUrl;
        @SerializedName("duration")
        @Expose
        private String duration;
        @SerializedName("skipOffset")
        @Expose
        private String skipOffset;
        @SerializedName("slotNumber")
        @Expose
        private String slotNumber;
        @SerializedName("tracking")
        @Expose
        private List<Tracking> tracking = null;
        public final static Parcelable.Creator<Placement> CREATOR = new Creator<Placement>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Placement createFromParcel(Parcel in) {
                return new Placement(in);
            }

            public Placement[] newArray(int size) {
                return (new Placement[size]);
            }

        };

        protected Placement(Parcel in) {
            this.adContentUrl = ((String) in.readValue((String.class.getClassLoader())));
            this.duration = ((String) in.readValue((String.class.getClassLoader())));
            this.skipOffset = ((String) in.readValue((String.class.getClassLoader())));
            this.slotNumber = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(this.tracking, (Tracking.class.getClassLoader()));
        }

        public Placement() {
        }

        public String getAdContentUrl() {
            return adContentUrl;
        }


        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getSkipOffset() {
            return skipOffset;
        }


        public String getSlotNumber() {
            return slotNumber;
        }


        public List<Tracking> getTracking() {
            return tracking;
        }

        public Tracking getTracking(TrackingEvent trackingEvent) {
            for (Tracking tracking : getTracking()) {
                if (tracking.event == trackingEvent) {
                    return tracking;
                }
            }
            return null;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(adContentUrl);
            dest.writeValue(duration);
            dest.writeValue(skipOffset);
            dest.writeValue(slotNumber);
            dest.writeList(tracking);
        }

        public int describeContents() {
            return 0;
        }

        @Override
        public String toString() {
            return "Placement{" +
                    "adContentUrl='" + adContentUrl + '\'' +
                    ", duration='" + duration + '\'' +
                    ", skipOffset='" + skipOffset + '\'' +
                    ", slotNumber='" + slotNumber + '\'' +
                    ", tracking=" + tracking +
                    '}';
        }
    }


    public static class Tracking implements Parcelable {

        @SerializedName("event")
        @Expose
        @JsonAdapter(TrackingEventDeserializer.class)
        private TrackingEvent event;
        @SerializedName("url")
        @Expose
        private String url;
        public final static Parcelable.Creator<Tracking> CREATOR = new Creator<Tracking>() {


            @SuppressWarnings({
                    "unchecked"
            })
            public Tracking createFromParcel(Parcel in) {
                return new Tracking(in);
            }

            public Tracking[] newArray(int size) {
                return (new Tracking[size]);
            }

        };

        protected Tracking(Parcel in) {
            this.event = ((TrackingEvent) in.readValue((TrackingEvent.class.getClassLoader())));
            this.url = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Tracking() {
        }

        public TrackingEvent getEvent() {
            return event;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(event);
            dest.writeValue(url);
        }

        public int describeContents() {
            return 0;
        }

        @Override
        public String toString() {
            return "Tracking{" +
                    "event='" + event + '\'' +
                    ", url='" + url + '\'' +
                    '}';
        }
    }

}
