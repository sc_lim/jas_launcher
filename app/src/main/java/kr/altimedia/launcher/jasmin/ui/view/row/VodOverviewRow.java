/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.PresenterSelector;
import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.ProductButtonPresenterSelector;

public class VodOverviewRow extends Row {
    public static class Listener {

        /**
         * Called when DetailsOverviewRow has changed image drawable.
         */
        public void onImageDrawableChanged(VodOverviewRow row) {
        }

        /**
         * Called when DetailsOverviewRow has changed main item.
         */
        public void onItemChanged(VodOverviewRow row) {
        }

        /**
         * Called when DetailsOverviewRow has changed actions adapter.
         */
        public void onActionsAdapterChanged(VodOverviewRow row) {
        }
    }

    private ArrayList<WeakReference<Listener>> mListeners;

    private PresenterSelector mDefaultActionPresenter;
    private ObjectAdapter mActionsAdapter;
    private Object mItem;

    public VodOverviewRow(ProductType productType, Object item) {
        mItem = item;
        verify();

        mDefaultActionPresenter = new ProductButtonPresenterSelector(productType);
        mActionsAdapter = new ArrayObjectAdapter(mDefaultActionPresenter);
    }

    private void verify() {
        if (mItem == null) {
            throw new IllegalArgumentException("Object cannot be null");
        }
    }

    public final Object getItem() {
        return mItem;
    }

    public void addListener(Listener listener) {
        if (mListeners == null) {
            mListeners = new ArrayList<WeakReference<Listener>>();
        } else {
            for (int i = 0; i < mListeners.size(); ) {
                Listener l = mListeners.get(i).get();
                if (l == null) {
                    mListeners.remove(i);
                } else {
                    if (l == listener) {
                        return;
                    }
                    i++;
                }
            }
        }
        mListeners.add(new WeakReference<Listener>(listener));
    }

    /**
     * Removes listener of the details page.
     */
    public void removeListener(Listener listener) {
        if (mListeners != null) {
            for (int i = 0; i < mListeners.size(); ) {
                Listener l = mListeners.get(i).get();
                if (l == null) {
                    mListeners.remove(i);
                } else {
                    if (l == listener) {
                        mListeners.remove(i);
                        return;
                    }
                    i++;
                }
            }
        }
    }

    public void setActionsAdapter(ObjectAdapter adapter) {
        if (adapter != mActionsAdapter) {
            mActionsAdapter = adapter;
            if (mActionsAdapter.getPresenterSelector() == null) {
                mActionsAdapter.setPresenterSelector(mDefaultActionPresenter);
            }
            notifyActionsAdapterChanged();
        }
    }

    public final ObjectAdapter getActionsAdapter() {
        return mActionsAdapter;
    }


    public final void setItem(Object item) {
        if (item != mItem) {
            mItem = item;
            notifyItemChanged();
        }
    }

    final void notifyItemChanged() {
        if (mListeners != null) {
            for (int i = 0; i < mListeners.size(); ) {
                Listener l = mListeners.get(i).get();
                if (l == null) {
                    mListeners.remove(i);
                } else {
                    l.onItemChanged(this);
                    i++;
                }
            }
        }
    }


    final void notifyActionsAdapterChanged() {
        if (mListeners != null) {
            for (int i = 0; i < mListeners.size(); ) {
                Listener l = mListeners.get(i).get();
                if (l == null) {
                    mListeners.remove(i);
                } else {
                    l.onActionsAdapterChanged(this);
                    i++;
                }
            }
        }
    }
}
