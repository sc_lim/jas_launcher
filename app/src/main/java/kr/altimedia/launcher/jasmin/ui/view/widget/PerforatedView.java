/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 05,06,2020
 */
public class PerforatedView extends View {
    private final String TAG = PerforatedView.class.getSimpleName();

    public enum PerforateType {
        PIP(0), PUNCH(1), IMAGE(2), NOT_DEFINED(99);
        final int type;

        PerforateType(int type) {
            this.type = type;
        }

        static PerforateType findByType(int type) {
            PerforateType[] types = values();
            for (PerforateType checkType : types) {
                if (checkType.type == type) {
                    return checkType;
                }
            }
            return NOT_DEFINED;
        }
    }

    private PerforateType definedType = PerforateType.NOT_DEFINED;
    private Drawable perforatedImage = null;
    private TextView blockedText = null;
    private float radius;


    public PerforatedView(Context context) {
        super(context);
    }

    public PerforatedView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public PerforatedView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttribute(context, attrs, defStyleAttr);
    }

    private void initAttribute(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PerforatedView, defStyleAttr, 0);
        if (typedArray.hasValue(R.styleable.PerforatedView_perforatedType)) {
            int value = typedArray.getInt(R.styleable.PerforatedView_perforatedType, PerforateType.NOT_DEFINED.type);
            definedType = PerforateType.findByType(value);
        }
        if (typedArray.hasValue(R.styleable.PerforatedView_perforatedImage)) {
            int imageId = typedArray.getResourceId(R.styleable.PerforatedView_perforatedImage, -1);
            perforatedImage = getResources().getDrawable(imageId);
        }

        if (typedArray.hasValue(R.styleable.PerforatedView_perforatedRadius)) {
            radius = typedArray.getFloat(R.styleable.PerforatedView_perforatedRadius, 0f);
        }
        typedArray.recycle();
    }

    public void initBlockedText(TextView textView) {
        blockedText = textView;
    }

    public PerforateType getType() {
        if (Log.INCLUDE) {
            Log.d(TAG, "getType : " + definedType);
        }
        return definedType;
    }

    public void setType(PerforateType type) {
        this.definedType = type;
        if (Log.INCLUDE) {
            Log.d(TAG, "setType : " + type);
        }
//        invalidate();
    }

    public Drawable getPerforatedImage() {
        return perforatedImage;
    }

    public void setPerforatedImage(Drawable perforatedImage) {
        this.perforatedImage = perforatedImage;
    }

    public void setPerforatedImage(int resourceId) {
        this.perforatedImage = getContext().getResources().getDrawable(resourceId);
    }

    public void clearPerforatedImage() {
        this.perforatedImage = null;
    }

    public float getRadius() {
        return radius;
    }

    public void setText(int stringId) {
        if (blockedText != null) {
            blockedText.setText(stringId);
        }
    }
}
