/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class DiscountCheckBoxPresenter extends Presenter {
    private final float DEFAULT_ALPHA = 1.0f;
    private final float DIM_ALPHA = 0.4f;

    private OnClickDiscountCheckBox onClickDiscountCheckBox;

    public void setOnClickDiscountCheckBox(OnClickDiscountCheckBox onClickDiscountCheckBox) {
        this.onClickDiscountCheckBox = onClickDiscountCheckBox;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_discount_check_box, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        DiscountButton discountButton = (DiscountButton) item;

        View view = viewHolder.view;
        Context context = view.getContext();

        boolean isEnabled = discountButton.isEnabled();
        view.setFocusable(isEnabled);
        view.setFocusableInTouchMode(isEnabled);
        view.setEnabled(isEnabled);

        float alpha = isEnabled ? DEFAULT_ALPHA : DIM_ALPHA;
        view.setAlpha(alpha);

        TextView title = view.findViewById(R.id.title);
        ImageView icon = view.findViewById(R.id.icon);
        TextView value = view.findViewById(R.id.value);
        TextView unit = view.findViewById(R.id.unit);
        TextView description = view.findViewById(R.id.desc);
        CheckBox checkBox = view.findViewById(R.id.check_box);
        checkBox.setChecked(discountButton.isChecked());

        String discountTitle = context.getString(discountButton.getTitle());
        int iconResource = discountButton.getIcon();
        String discountValue = StringUtil.getFormattedNumber((long) discountButton.getValue());
        String discountUnit = context.getString(discountButton.getUnit());
        title.setText(discountTitle);
        value.setText(discountValue);
        unit.setText(discountUnit);
        if (iconResource != 0) {
            Drawable iconDrawable = view.getResources().getDrawable(discountButton.getIcon());
            icon.setBackground(iconDrawable);
        }

//        int visibility = discountButton.isChecked() ? View.INVISIBLE : View.VISIBLE;
//        description.setVisibility(visibility);
//        if (!discountButton.isChecked()) {
            int desc = discountButton.getDescription();
            if (desc != -1) {
                int pointValue = (int) (discountButton.getValue() / discountButton.getRatio());
                String str = "(" + pointValue + context.getString(desc) + ")";
                description.setText(str);
            }
//        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickDiscountCheckBox != null) {
                    onClickDiscountCheckBox.onClick(discountButton);
                }
            }
        });
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        setOnClickDiscountCheckBox(null);
    }

    public interface OnClickDiscountCheckBox {
        void onClick(DiscountButton discountButton);
    }

    public interface DiscountButton {
        int getTitle();

        int getUnit();

        int getDescription();

        float getValue();

        int getIcon();

        int getRatio();

        boolean isChecked();

        boolean isEnabled();
    }
}
