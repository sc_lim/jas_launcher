/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.presenter;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class CouponPaymentButtonPresenter extends Presenter {

    private int btnWidth = R.dimen.coupon_payment_button_width;
    private int btnHeight = R.dimen.coupon_payment_button_height;

    private OnKeyListener onKeyListener;
    private OnFocusListener onFocusListener;
    private boolean animated;

    public CouponPaymentButtonPresenter(OnKeyListener onKeyListener) {
        this(false, onKeyListener, null);
    }

    public CouponPaymentButtonPresenter(boolean animated, OnKeyListener onKeyListener) {
        this(animated, onKeyListener, null);
    }

    public CouponPaymentButtonPresenter(boolean animated, OnKeyListener onKeyListener, OnFocusListener onFocusListener) {
        this.animated = animated;
        this.onKeyListener = onKeyListener;
        this.onFocusListener = onFocusListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ItemViewHolder viewHolder = new ItemViewHolder(inflater.inflate(R.layout.item_coupon_shop_payment_button, null, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object object) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
        PaymentType item = (PaymentType) object;

        itemViewHolder.setItem(item);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
        itemViewHolder.dispose();
    }

    public OnKeyListener getOnKeyListener() {
        return onKeyListener;
    }

    public void setSize(int btnWidth, int btnHeight){
        this.btnWidth = btnWidth;
        this.btnHeight = btnHeight;
    }

    public interface OnFocusListener {
        void onFocus(View view, boolean hasFocus, Object item);
    }

    public interface OnKeyListener {
        boolean onKey(int keyCode, int index, Object item, ViewHolder viewHolder);
    }

    public class ItemViewHolder extends ViewHolder implements View.OnFocusChangeListener  {
        private final String TAG = ItemViewHolder.class.getSimpleName();

        private PaymentType item;
        private LinearLayout buttonLayer;
        private TextView textView;

        public ItemViewHolder(View view) {
            super(view);

            buttonLayer = view.findViewById(R.id.buttonLayer);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)buttonLayer.getLayoutParams();
            lp.width = (int) view.getResources().getDimension(btnWidth);
            lp.height = (int) view.getResources().getDimension(btnHeight);
            buttonLayer.setLayoutParams(lp);

            textView = view.findViewById(R.id.buttonTextView);
            view.setOnFocusChangeListener(this);
        }

        public void dispose() {

        }

        public void setItem(PaymentType item) {
            this.item = item;
            this.textView.setText(view.getResources().getString(item.getName()));
        }

        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (animated) {
                Animator animator = loadAnimator(view.getContext(), hasFocus ? R.animator.scale_up_50 : R.animator.scale_down_50);
                animator.setTarget(view);
                animator.start();
            }

            if (onFocusListener != null) {
                onFocusListener.onFocus(view, hasFocus, this.item);
            }
        }

        private Animator loadAnimator(Context context, int resId) {
            Animator animator = AnimatorInflater.loadAnimator(context, resId);
            return animator;
        }
    }
}