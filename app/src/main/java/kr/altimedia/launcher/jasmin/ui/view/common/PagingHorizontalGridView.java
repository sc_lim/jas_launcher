/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class PagingHorizontalGridView extends HorizontalGridView {
    private static String TAG = PagingHorizontalGridView.class.getSimpleName();

    private int visibleCount = 0;

    public PagingHorizontalGridView(Context context) {
        super(context);
    }

    public PagingHorizontalGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PagingHorizontalGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void initHorizontal(int visibleCount) {
        this.visibleCount = visibleCount;
        setItemAlignmentOffsetPercent(0);
    }


    @Override
    public void requestChildFocus(View child, View focused) {
        int index = getChildAdapterPosition(focused);
        float value = index % visibleCount;
        int width = child.getMeasuredWidth();
        int gap = getHorizontalSpacing();
        int offset = (int) ((width + gap) * value);

        setWindowAlignmentOffsetPercent(0);
        setWindowAlignmentOffset(offset);

        super.requestChildFocus(child, focused);
    }

    public void updateAlign(View child, int position) {
        if (child == null) {
            return;
        }
        int index = position;
        float value = index % visibleCount;
        int width = child.getMeasuredWidth();
        int gap = getHorizontalSpacing();
        int offset = (int) ((width + gap) * value);
        setWindowAlignmentOffsetPercent(0);
        setWindowAlignmentOffset(offset);
    }
}
