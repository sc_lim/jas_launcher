/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.def;


import com.altimedia.tvmodule.util.StringUtils;

public enum CategoryType {
    Invalid("Invalid", "invalid", 0x99),

    Search("Search", "search", 0x01),
    SubCategory("SubCategory", "menu", 0x02),
    LiveTv("LiveTv", "channel", 0x03),
    Apps("Apps", "apps", 0x04),
    AppLink("AppLink", "3rdPartyApp", 0x05),
    Message("Message", "message", 0x06),


    Banner("Event", "bannerPosition", 0x13),
    Promotion("MainBanner", "promoPosition", 0x11),

    Recommend("Recommend", "recommendation", 0x12),
    ContentsList("ContentsList", "asset", 0x14),
    RecentWatch("RecentWatch", "recentlyWatching", 0x15);


    final String categoryName;
    final String categoryType;
    final long categoryId;

    CategoryType(String name, String type, long id) {
        categoryName = name;
        categoryType = type;
        categoryId = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public long getCategoryTypeLong() {
        return categoryId;
    }

    public static CategoryType findByName(String categoryName) {
        CategoryType[] types = values();
        for (CategoryType categoryType : types) {

            if (categoryType.categoryName.equalsIgnoreCase(categoryName)) {
                return categoryType;
            }
        }
        return Invalid;
    }

    public static CategoryType findByType(String categoryType) {
        String categoryTypeString = StringUtils.nullToEmpty(categoryType);
        CategoryType[] types = values();
        for (CategoryType type : types) {
            if (type.categoryType.equalsIgnoreCase(categoryTypeString)) {
                return type;
            }
        }
        return Invalid;
    }
}
