/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.tutorial.presenter;

import android.content.Context;
import android.view.KeyEvent;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.widget.ArrayObjectAdapter;

import kr.altimedia.launcher.jasmin.ui.component.fragment.tutorial.TutorialItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.tutorial.TutorialPageFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.tutorial.view.TutorialPageView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.ViewPagerPresenter;

public class TutorialViewPagerPresenter extends ViewPagerPresenter {
    public TutorialViewPagerPresenter(boolean autoPaging, int indicatorLayoutId, FragmentManager fragmentManager) {
        super(autoPaging, indicatorLayoutId, fragmentManager);
    }

    @Override
    protected ViewPagerAdapter createPagerAdapter(FragmentManager fragmentManager, int behavior, Context context) {
        return new TutorialPagerAdapter(fragmentManager, behavior);
    }

    @Override
    protected PageRootView createPageView(Context context) {
        return new TutorialPageView(context);
    }

    @Override
    protected void initializeRowViewHolder(RowPresenter.ViewHolder holder) {
        super.initializeRowViewHolder(holder);
//        setOnPromotionChangedListener(new OnPromotionChangedListener() {
//
//            @Override
//            public void onPromotionComplete(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//
//            @Override
//            public void onPromotionChanged(int position, Object object) {
//
//            }
//
//            @Override
//            public boolean onPromotionNotifyKey(int position, KeyEvent keyCode) {
//                return false;
//            }
//
//            @Override
//            public void onPromotionSelected(Object object) {
//                int currentPosition = mViewPagerChangeListener.getCurrentPosition();
//                int size = mViewPagerChangeListener.getSize();
//                int nextPosition = currentPosition + 1;
//                if (nextPosition < size) {
//                    mViewPagerChangeListener.setCurrentItem(nextPosition, true);
//                }
//            }
//        });
    }

    private class TutorialPagerAdapter extends ViewPagerAdapter {
        private ArrayObjectAdapter mItemAdapter = null;

        public TutorialPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            TutorialItem item = (TutorialItem) getDataItem(position);
            return TutorialPageFragment.newInstance(item);
        }

        @Override
        public int getCount() {
            if (this.mItemAdapter == null) {
                return 0;
            } else {
                return this.mItemAdapter.size();
            }
        }

        @Override
        public void setItemAdapter(ArrayObjectAdapter objectAdapter) {
            this.mItemAdapter = objectAdapter;
            notifyDataSetChanged();
        }

        @Override
        public Object getDataItem(int position) {
            return mItemAdapter.get(position);
        }
    }
}
