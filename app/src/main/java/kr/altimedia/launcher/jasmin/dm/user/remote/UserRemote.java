/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.remote;


import android.os.Bundle;

import com.altimedia.util.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.MbsUtil;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.user.api.UserApi;
import kr.altimedia.launcher.jasmin.dm.user.object.CollectedContent;
import kr.altimedia.launcher.jasmin.dm.user.object.FAQInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.FavoriteContent;
import kr.altimedia.launcher.jasmin.dm.user.object.LoginResult;
import kr.altimedia.launcher.jasmin.dm.user.object.Membership;
import kr.altimedia.launcher.jasmin.dm.user.object.OtpId;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileIconCategory;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileList;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileLoginResult;
import kr.altimedia.launcher.jasmin.dm.user.object.PurchasedContent;
import kr.altimedia.launcher.jasmin.dm.user.object.ResumeTimeResult;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscribedContent;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscriberInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ActivationResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.BlockedChannelResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ChangePinCodeResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.CheckPinCodeResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.CollectionListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ExternalIntentDataResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.FAQInfoResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.FavoriteChannelResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.FavoriteContentListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.LoginResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.MembershipPointResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.OtpIdResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.OtpResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ProfileIconCategoryListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ProfileListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ProfileLoginResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.PurchaseListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ResumeTimeResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.SubscriberInfoResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.SubscriptionListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.UserDeviceListResponse;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mc.kim on 11,05,2020
 */
public class UserRemote {
    private static final String TAG = UserRemote.class.getSimpleName();
    private UserApi mUserApi;

    public UserRemote(UserApi userApi) {
        mUserApi = userApi;
    }

    public String getActivationInfo(String model, String macAddress, String ipAddress) throws IOException, ResponseException, AuthenticationException {
        Call<ActivationResponse> call = mUserApi.getActivationInfo(model, macAddress, ipAddress/*, Settings.Secure.ANDROID_ID*/);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ActivationResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ActivationResponse mBaseResponse = response.body();
        return mBaseResponse.getSaId();
    }

    public LoginResult requestLogin(String saId) throws IOException, ResponseException, AuthenticationException {
        Call<LoginResponse> call = mUserApi.requestLogin(saId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<LoginResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
        }
        MbsUtil.checkValid(response);
        LoginResponse mBaseResponse = response.body();
        return mBaseResponse.getLoginResult();
    }

    public boolean checkPinCode(String said, String profileId, String checkType, String currentPinCode) throws IOException, ResponseException, AuthenticationException {
        Call<CheckPinCodeResponse> call = mUserApi.checkPinCode(said, profileId, checkType, currentPinCode);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<CheckPinCodeResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);

        CheckPinCodeResponse mCheckPinCodeResponse = response.body();
        return mCheckPinCodeResponse.isCorrectPin();
    }

    public ChangePinCodeResponse changePinCode(String said, String profileId, String changeType, String currentPinCode, String newPinCode) throws IOException,
            ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("type", changeType);
        jsonObject.addProperty("currentPinCode", currentPinCode);
        jsonObject.addProperty("newPinCode", newPinCode);

        Call<ChangePinCodeResponse> call = mUserApi.changePinCode(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ChangePinCodeResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);

        ChangePinCodeResponse mChangePinCodeResponse = response.body();
        return mChangePinCodeResponse;
    }

    public boolean resetPinCode(String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);

        Call<BaseResponse> call = mUserApi.resetPinCode(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);

        BaseResponse mBaseResponse = response.body();
        boolean isSuccess = mBaseResponse.getReturnCode().equalsIgnoreCase("s");

        return isSuccess;
    }

    public OtpId getOtpId(String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<OtpIdResponse> call = mUserApi.getOtpId(said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<OtpIdResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);

        OtpIdResponse otpIdResponse = response.body();
        return otpIdResponse.getOtpId();
    }

    public OtpResponse.Otp getOtp(String loginId) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("loginId", loginId);

        Call<OtpResponse> call = mUserApi.getOtp(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<OtpResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);

        OtpResponse otpResponse = response.body();
        return otpResponse.getOtp();
    }

    public boolean checkOtp(String loginId, String otp) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("loginId", loginId);
        jsonObject.addProperty("otp", otp);
        Call<BaseResponse> call = mUserApi.checkOtp(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();
        return mBaseResponse.getReturnCode().equalsIgnoreCase("s");
    }

    public Membership getMembership(String said) throws IOException, ResponseException, AuthenticationException {
        Call<MembershipPointResponse> call = mUserApi.getMembership(said);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<MembershipPointResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        MembershipPointResponse mMembershipPointResponse = response.body();
        return mMembershipPointResponse.getMembership();
    }

    @Deprecated
    public SubscriberInfo getSubscriberInfo(String saId, String loginId) throws IOException, ResponseException, AuthenticationException {
        Call<SubscriberInfoResponse> call = mUserApi.getSubscriberInfo(saId, loginId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<SubscriberInfoResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        SubscriberInfoResponse mBaseResponse = response.body();
        return mBaseResponse.getData();
    }

    public boolean postWatchingInfo(String said, String profileId, String deviceType, JsonArray jsonArray) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("list", jsonArray);
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("deviceType", deviceType);
        Call<BaseResponse> call = mUserApi.postWatchingInfo(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();
        return true;
    }

    public List<String> getFavoriteChannelList(String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<FavoriteChannelResponse> call = mUserApi.getFavoriteChannel(said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<FavoriteChannelResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        FavoriteChannelResponse mFavoriteChannelResponse = response.body();
        return mFavoriteChannelResponse.getFavoriteChannelResult();
    }

    public boolean putFavoriteChannelList(String said, String profileId, String channelId) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("channelId", channelId);
        Call<BaseResponse> call = mUserApi.putFavoriteChannel(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();
        boolean isSuccess = mBaseResponse.getReturnCode().equalsIgnoreCase("s");
        return isSuccess;
    }

    public List<String> postFavoriteChannelList(String said, String profileId, Long... channelId) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        JsonArray array = new JsonArray();
        for (Long id : channelId) {
            array.add(id);
        }
        jsonObject.add("channelId", array);
        Call<FavoriteChannelResponse> call = mUserApi.postFavoriteChannel(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<FavoriteChannelResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        FavoriteChannelResponse mFavoriteChannelResponse = response.body();
        return mFavoriteChannelResponse.getFavoriteChannelResult();
    }

    public List<String> getBlockedChannelList(String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<BlockedChannelResponse> call = mUserApi.getBlockedChannel(said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BlockedChannelResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BlockedChannelResponse mBlockedChannelResponse = response.body();
        return mBlockedChannelResponse.getBlockedChannelResult();
    }

    public boolean putBlockedChannelList(String said, String profileId, Long channelId) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("channelId", channelId);
        Call<BaseResponse> call = mUserApi.putBlockedChannel(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();
        boolean isSuccess = mBaseResponse.getReturnCode().equalsIgnoreCase("s");
        return isSuccess;
    }

    public List<String> postBlockedChannelList(String said, String profileId, String... channelId) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        JsonArray array = new JsonArray();
        for (String id : channelId) {
            array.add(id);
        }
        jsonObject.add("channelId", array);
        Call<BlockedChannelResponse> call = mUserApi.postBlockedChannel(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BlockedChannelResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BlockedChannelResponse mBlockedChannelResponse = response.body();
        return mBlockedChannelResponse.getBlockedChannelResult();
    }

    public ResumeTimeResult getResumeTime(String said, String profileId, String contentId) throws IOException, ResponseException, AuthenticationException {
        Call<ResumeTimeResponse> call = mUserApi.getResumeTime(said, profileId, contentId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ResumeTimeResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ResumeTimeResponse mResumeTimeResponse = response.body();
        return mResumeTimeResponse.getResumeTimeResult();
    }

    public BaseResponse postResumeTime(String said, String profileId, String contentId, long resumeTime, long startTime, long endTime)
            throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("contentId", contentId);
        jsonObject.addProperty("resumeTime", resumeTime);
        jsonObject.addProperty("startTime", startTime);
        jsonObject.addProperty("endTime", endTime);
        Call<BaseResponse> call = mUserApi.postResumeTime(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.body().toString());
        }
        MbsUtil.checkValid(response);
        BaseResponse mResumeTimeResponse = response.body();
        return mResumeTimeResponse;
    }


    public List<FAQInfo> getFAQList(String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<FAQInfoResponse> call = mUserApi.getFAQList(said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<FAQInfoResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        FAQInfoResponse mFAQInfoResponse = response.body();
        return mFAQInfoResponse.getFaqInfoList();
    }

    public List<CollectedContent> getCollectionList(String language, String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<CollectionListResponse> call = mUserApi.getCollectionList(language, said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<CollectionListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        CollectionListResponse mCollectionListResponse = response.body();
        return mCollectionListResponse.getCollectionList();
    }

    public List<PurchasedContent> getPurchaseList(String language, String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<PurchaseListResponse> call = mUserApi.getPurchaseList(language, said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<PurchaseListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        PurchaseListResponse mPurchaseListResponse = response.body();
        return mPurchaseListResponse.getPurchasedContentList();
    }

    public boolean putPurchaseListHidden(String said, String profileId, String... contentId) throws IOException, ResponseException, AuthenticationException {
        JsonArray array = new JsonArray();
        for (String id : contentId) {
            array.add(id);
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("purchaseId", array);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("said", said);
        Call<BaseResponse> call = mUserApi.putPurchaseListHidden(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();
        return mBaseResponse.getReturnCode().equalsIgnoreCase("S");
    }

    public List<SubscribedContent> getSubscriptionList(String language, String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<SubscriptionListResponse> call = mUserApi.getSubscriptionList(language, said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<SubscriptionListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        SubscriptionListResponse mSubscriptionListResponse = response.body();
        return mSubscriptionListResponse.getSubscribedContentList();
    }

    public boolean putStopSubscription(String said, String profileId, String productId, String purchaseId) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("purchaseId", purchaseId);
        jsonObject.addProperty("productId", productId);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("said", said);
        Call<BaseResponse> call = mUserApi.putStopSubscription(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();
        return mBaseResponse.getReturnCode().equalsIgnoreCase("S");
    }

    public ProfileList getProfileList(String said) throws IOException, ResponseException, AuthenticationException {
        Call<ProfileListResponse> call = mUserApi.getProfileList(said, "Y");
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }

        Response<ProfileListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ProfileListResponse mProfileListResponse = response.body();
        return mProfileListResponse.getProfileList();
    }

    public boolean putProfile(String said, String profileId, String profileName, String profileIconId) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("profileIconId", profileIconId);
        jsonObject.addProperty("profileName", profileName);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("said", said);
        Call<BaseResponse> call = mUserApi.putProfile(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();
        return mBaseResponse.getReturnCode().equalsIgnoreCase("S");
    }

    public boolean postProfile(String said, String profileName, String profileIconId) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("profileIconId", profileIconId);
        jsonObject.addProperty("profileName", profileName);
        jsonObject.addProperty("said", said);
        Call<BaseResponse> call = mUserApi.postProfile(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();
        return mBaseResponse.getReturnCode().equalsIgnoreCase("S");
    }

    public boolean deleteProfile(String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<BaseResponse> call = mUserApi.deleteProfile(said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();
        return true;
    }

    public ProfileLoginResult requestProfileLogin(String loginId, String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<ProfileLoginResponse> call = mUserApi.requestProfileLogin(loginId, said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ProfileLoginResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ProfileLoginResponse mChangeProfileResponse = response.body();
        return mChangeProfileResponse.getProfileLoginResult();
    }

    public boolean putProfileRating(String said, String profileId, String rating) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("viewRestriction", rating);
        Call<BaseResponse> call = mUserApi.putProfileRating(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }

        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();

        return mBaseResponse.getReturnCode().equalsIgnoreCase("S");
    }

    public boolean putProfileLock(String said, String profileId, String lockYn, String pinCode) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("lockYn", lockYn);
        jsonObject.addProperty("pinCode", pinCode);
        Call<BaseResponse> call = mUserApi.putProfileLock(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }

        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();

        return mBaseResponse.getReturnCode().equalsIgnoreCase("S");
    }

    public List<ProfileIconCategory> getProfileIconCategoryList(String language, String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<ProfileIconCategoryListResponse> call = mUserApi.getProfileIconCategoryList(language, said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }

        Response<ProfileIconCategoryListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ProfileIconCategoryListResponse mProfileIconCategoryListResponse = response.body();
        return mProfileIconCategoryListResponse.getProfileIconCategoryList();
    }

    public List<UserDevice> getDeviceList(String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<UserDeviceListResponse> call = mUserApi.getDeviceList(said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<UserDeviceListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }

        MbsUtil.checkValid(response);
        UserDeviceListResponse mUserDeviceListResponse = response.body();

        return mUserDeviceListResponse.getUserDeviceList();
    }

    public boolean putDevice(String said, String profileId, String deviceId, String deviceName) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("deviceId", deviceId);
        jsonObject.addProperty("deviceName", deviceName);
        Call<BaseResponse> call = mUserApi.putDevice(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }

        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();

        return mBaseResponse.getReturnCode().equalsIgnoreCase("s");
    }

    public boolean deleteDevice(String said, String profileId, String deviceId) throws IOException, ResponseException, AuthenticationException {
        Call<BaseResponse> call = mUserApi.deleteDevice(said, profileId, deviceId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }

        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();

        return mBaseResponse.getReturnCode().equalsIgnoreCase("s");
    }

    public List<FavoriteContent> getFavoriteVod(String language, String said, String profileId) throws IOException, ResponseException, AuthenticationException {
        Call<FavoriteContentListResponse> call = mUserApi.getVodLike(language, said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<FavoriteContentListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        FavoriteContentListResponse mContentsListResponse = response.body();
        return mContentsListResponse.getContentList();
    }


    public Bundle getIntentData(String accountId, String appId, String macAddress) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("accountId", accountId);
        jsonObject.addProperty("appId", appId);
        jsonObject.addProperty("macAddress", macAddress);
        Call<ExternalIntentDataResponse> call = mUserApi.getIntentData(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ExternalIntentDataResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ExternalIntentDataResponse mExternalIntentDataResponse = response.body();
        return mExternalIntentDataResponse.getExternalIntentData();
    }
}
