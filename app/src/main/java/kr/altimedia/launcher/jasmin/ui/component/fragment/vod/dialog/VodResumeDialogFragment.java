/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class VodResumeDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = VodResumeDialogFragment.class.getName();
    private static final String TAG = VodResumeDialogFragment.class.getSimpleName();

    public static final int RESUME = 0;
    public static final int BEGINNING = 1;
    public static final int CANCEL = 2;

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @IntDef({RESUME, BEGINNING, CANCEL})
    public @interface ResumeType {
    }

    private static final String MOVIE = "MOVIE";
    private Content mContent;

    private TextView title;
    private TextView playTimeTextView;
    private TextView totalTimeTextView;
    private AppCompatSeekBar progressBar;
    private HorizontalGridView buttonsGrid;

    public VodResumeDialogFragment(){}

    public static VodResumeDialogFragment newInstance(Content content, OnButtonsAction onButtonsAction) {
        Bundle args = new Bundle();
        args.putParcelable(MOVIE, content);
        VodResumeDialogFragment fragment = new VodResumeDialogFragment();
        fragment.setOnButtonsAction(onButtonsAction);
        fragment.setArguments(args);
        return fragment;
    }

    private OnButtonsAction mOnButtonsAction = null;

    private void setOnButtonsAction(OnButtonsAction mOnButtonsAction) {
        this.mOnButtonsAction = mOnButtonsAction;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
        Bundle mBundle = getArguments();
        mContent = mBundle.getParcelable(MOVIE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_vod_resume, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        title = view.findViewById(R.id.title);
        playTimeTextView = view.findViewById(R.id.play_time);
        totalTimeTextView = view.findViewById(R.id.total_time);
        progressBar = view.findViewById(R.id.vod_progressbar);
        buttonsGrid = view.findViewById(R.id.buttons_grid);

        title.setText(mContent.getTitle());

        setProgressBar();
        initButtons();
    }

    private void setTimeString(long time, TextView textView, StringBuilder builder) {
        builder.setLength(0);
        formatTime(time, builder);
        textView.setText(builder.toString());
        builder.setLength(0);
    }

    private void setProgressBar() {
        long playTime = mContent.getResumeTime();
        long duration = mContent.getRunTime();
        StringBuilder builder = new StringBuilder();
        setTimeString(playTime, playTimeTextView, builder);
        setTimeString(duration, totalTimeTextView, builder);

        progressBar.setMin(0);
        progressBar.setMax((int) (duration / 1000));
        progressBar.setProgress((int) (playTime / 1000));
        progressBar.setSecondaryProgress((int) (playTime / 1000));
    }

    private void initButtons() {
        VodResumeButtonsAdapter mVodResumeButtonsAdapter = new VodResumeButtonsAdapter(mOnButtonsAction);

        buttonsGrid.setAdapter(mVodResumeButtonsAdapter);
        buttonsGrid.setHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.resume_button_space));
    }

    private class VodResumeButtonsAdapter extends RecyclerView.Adapter<VodResumeButtonsAdapter.VodResumeButtonsViewHolder> {


        private ArrayList<Integer> buttons = new ArrayList<>(Arrays.asList(R.string.resume, R.string.Play_from_beginning, R.string.cancel));
        //                Arrays.asList("Resume", "Play from beginning", "Cancel"));
        private OnButtonsAction mOnButtonsAction;

        public VodResumeButtonsAdapter(OnButtonsAction mOnButtonsAction) {
            this.mOnButtonsAction = mOnButtonsAction;
        }

        @NonNull
        @Override
        public VodResumeButtonsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(getContext()).inflate(R.layout.vod_resume_button, parent, false);
            return new VodResumeButtonsViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull VodResumeButtonsViewHolder holder, int position) {
            String title = holder.itemView.getContext().getString(buttons.get(position));
            holder.setData(position, title);
        }

        @Override
        public int getItemCount() {
            return buttons.size();
        }

        private class VodResumeButtonsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private TextView button;
            private int position;

            public VodResumeButtonsViewHolder(@NonNull View itemView) {
                super(itemView);

                initView(itemView);
            }

            private void initView(View view) {
                button = view.findViewById(R.id.button);
                view.setOnClickListener(this);
            }

            public void setData(int position, String title) {
                this.position = position;
                button.setText(title);
            }

            @Override
            public void onClick(View v) {
                mOnButtonsAction.onClickButtons(VodResumeDialogFragment.this, position);
            }
        }

    }

    public interface OnButtonsAction {
        void onClickButtons(DialogFragment dialogFragment, @ResumeType int position);
    }

    private void formatTime(long ms, StringBuilder sb) {
        sb.setLength(0);
        if (ms < 0) {
            sb.append("--");
            return;
        }
        long seconds = ms / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        seconds -= minutes * 60;
        minutes -= hours * 60;

        if (hours < 10) {
            sb.append('0');
        }
        sb.append(hours).append(':');

        if (minutes < 10) {
            sb.append('0');
        }

        sb.append(minutes).append(':');
        if (seconds < 10) {
            sb.append('0');
        }
        sb.append(seconds);
    }
}
