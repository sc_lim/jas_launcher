/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;

/**
 * Created by mc.kim on 07,05,2020
 */
public class PreviewPlaybackTransportRowPresenter extends VideoPlaybackRowPresenter {
    private final String TAG = PreviewPlaybackTransportRowPresenter.class.getSimpleName();

    /**
     * A ViewHolder for the PlaybackControlsRow supporting seek UI.
     */
    public class ViewHolder extends VideoPlaybackRowPresenter.ViewHolder {
        final TextView mRemainTime;
        long mTotalTimeInMs = Long.MIN_VALUE;
        long mCurrentTimeInMs = Long.MIN_VALUE;

        final VideoPlaybackControlsRow.OnPlaybackProgressCallback mListener =
                new VideoPlaybackControlsRow.OnPlaybackProgressCallback() {
                    @Override
                    public void onCurrentPositionChanged(VideoPlaybackControlsRow row, long ms, boolean seeking) {
                        setCurrentPosition(ms);
                    }

                    @Override
                    public void onDurationChanged(VideoPlaybackControlsRow row, long ms) {
                        setTotalTime(ms);
                    }

                    @Override
                    public void onBufferedPositionChanged(VideoPlaybackControlsRow row, long ms) {
                    }
                };


        public void setCurrentTimeInMs(long currentTimeInMs) {
            this.mCurrentTimeInMs = currentTimeInMs;
        }

        public ViewHolder(View rootView) {
            super(rootView);
            mRemainTime = rootView.findViewById(R.id.remainTime);
        }

        public void onItemSelected(long time) {
        }

        void setTotalTime(long totalTimeMs) {
            if (mTotalTimeInMs != totalTimeMs) {
                mTotalTimeInMs = totalTimeMs;
            }
        }

        void setCurrentPosition(long currentTimeMs) {
            if (currentTimeMs != mCurrentTimeInMs) {
                setCurrentTimeInMs(currentTimeMs);
            }
        }
    }


    static void formatTime(long ms, StringBuilder sb) {
        sb.setLength(0);
        if (ms < 0) {
            sb.append("--");
            return;
        }
        long seconds = ms / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        seconds -= minutes * 60;
        minutes -= hours * 60;

        if (hours < 10) {
            sb.append('0');
        }
        sb.append(hours).append(':');

        if (minutes < 10) {
            sb.append('0');
        }

        sb.append(minutes).append(':');
        if (seconds < 10) {
            sb.append('0');
        }
        sb.append(seconds);
    }


    public PreviewPlaybackTransportRowPresenter() {
        setHeaderPresenter(null);
        setSelectEffectEnabled(false);
    }

    @Override
    public void setEnable(RowPresenter.ViewHolder rowViewHolder, boolean enable) {

    }

    @Override
    public void onReappear(RowPresenter.ViewHolder rowViewHolder) {
    }


    @Override
    protected RowPresenter.ViewHolder createRowViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_preview_playback_transport_controls_row, parent, false);
        PreviewPlaybackTransportRowPresenter.ViewHolder vh = new PreviewPlaybackTransportRowPresenter.ViewHolder(v);
        initRow(vh);
        return vh;
    }

    private void initRow(final PreviewPlaybackTransportRowPresenter.ViewHolder vh) {
    }


    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);

        PreviewPlaybackTransportRowPresenter.ViewHolder vh = (PreviewPlaybackTransportRowPresenter.ViewHolder) holder;
        VideoPlaybackControlsRow row = (VideoPlaybackControlsRow) vh.getRow();


        vh.setTotalTime(row.getDuration());
        vh.setCurrentPosition(row.getCurrentPosition());
        row.setOnPlaybackProgressChangedListener(vh.mListener);
    }

    @Override
    protected void onUnbindRowViewHolder(RowPresenter.ViewHolder holder) {
        PreviewPlaybackTransportRowPresenter.ViewHolder vh = (PreviewPlaybackTransportRowPresenter.ViewHolder) holder;
        VideoPlaybackControlsRow row = (VideoPlaybackControlsRow) vh.getRow();

        row.setOnPlaybackProgressChangedListener(null);

        super.onUnbindRowViewHolder(holder);
    }


    @Override
    protected void onRowViewSelected(RowPresenter.ViewHolder vh, boolean selected) {
        super.onRowViewSelected(vh, selected);
    }

    @Override
    protected void onRowViewAttachedToWindow(RowPresenter.ViewHolder vh) {
        super.onRowViewAttachedToWindow(vh);
    }

    @Override
    protected void onRowViewDetachedFromWindow(RowPresenter.ViewHolder vh) {
        super.onRowViewDetachedFromWindow(vh);

    }


    private static class OnProgressKeyListener implements View.OnKeyListener {
        final TrailerPlaybackTransportRowPresenter.ViewHolder mViewHolder;

        public OnProgressKeyListener(TrailerPlaybackTransportRowPresenter.ViewHolder mViewHolder) {
            this.mViewHolder = mViewHolder;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent keyEvent) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    return true;
                case KeyEvent.KEYCODE_MINUS:
                case KeyEvent.KEYCODE_MEDIA_REWIND:
                case KeyEvent.KEYCODE_PLUS:
                case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                case KeyEvent.KEYCODE_DPAD_CENTER:
                case KeyEvent.KEYCODE_ENTER:
                case KeyEvent.KEYCODE_BACK:
                case KeyEvent.KEYCODE_ESCAPE:
                    return false;
            }
            return false;
        }
    }

}
