/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;

public class PopularChannel implements Parcelable {

    public static final Creator<PopularChannel> CREATOR = new Creator<PopularChannel>() {
        @Override
        public PopularChannel createFromParcel(Parcel in) {
            return new PopularChannel(in);
        }

        @Override
        public PopularChannel[] newArray(int size) {
            return new PopularChannel[size];
        }
    };

    @SerializedName("sid")
    @Expose
    private String serviceId;
    @SerializedName("programId")
    @Expose
    private String programId;
    @SerializedName("ranking")
    @Expose
    private int viewingRank;
    @SerializedName("ratingRate")
    @Expose
    private float viewingRate;
    @SerializedName("viewerCount")
    @Expose
    private long viewerCount;
    @SerializedName("layout")
    @Expose
    private PopularChannelLayout layout;

    private Channel channel;
    private Program program;

    private PopularChannel(){
    }

    protected PopularChannel(Parcel in) {
        serviceId = in.readString();
        programId = in.readString();
        viewingRank = in.readInt();
        viewingRate = in.readFloat();
        viewerCount = in.readLong();
        layout = ((PopularChannelLayout) in.readValue((PopularChannelLayout.class.getClassLoader())));
    }

    public Channel getChannel() {
        return channel;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getProgramId() {
        return programId;
    }

    public String getDisplayNumber() {
        try {
            return channel.getDisplayNumber();
        }catch (Exception|Error e){
        }
        return "0";
    }

    public String getDisplayName() {
        try {
            return channel.getDisplayName();
        }catch (Exception|Error e){
        }
        return "-";
    }

    public int getViewingRank() {
        return viewingRank;
    }

    public float getViewingRate() {
        return viewingRate;
    }

    public long getViewerCount() {
        return viewerCount;
    }

    public Program getProgram() {
        return program;
    }

    public long getTotalProgress() {
        if (program != null) {
            return program.getDurationMillis();
        }
        return 0;
    }

    public long getCurrentProgress() {
        if (program != null) {
            long currentProgress = JasmineEpgApplication.SystemClock().currentTimeMillis() - program.getStartTimeUtcMillis();
            if(currentProgress > 0){
                return currentProgress;
            }
        }
        return 0;
    }

    public PopularChannelLayout getLayout() {
        return layout;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public void setProgram(Program program){
        this.program = program;
    }

    public static PopularChannel buildPopularChannel(int rank) {
        PopularChannel pchannel = new PopularChannel();
        pchannel.viewingRank = rank;
        pchannel.layout = new PopularChannelLayout();
        pchannel.layout.width = 423;
        pchannel.layout.height = 237;
        return pchannel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(serviceId);
        parcel.writeString(programId);
        parcel.writeInt(viewingRank);
        parcel.writeFloat(viewingRate);
        parcel.writeLong(viewerCount);
        parcel.writeValue(layout);
    }

    public static class PopularChannelLayout implements Parcelable {
        public static final Creator<PopularChannelLayout> CREATOR = new Creator<PopularChannelLayout>() {
            @Override
            public PopularChannelLayout createFromParcel(Parcel in) {
                return new PopularChannelLayout(in);
            }

            @Override
            public PopularChannelLayout[] newArray(int size) {
                return new PopularChannelLayout[size];
            }
        };
        @SerializedName("x")
        private int x;
        @SerializedName("y")
        private int y;
        @SerializedName("width")
        private int width;
        @SerializedName("height")
        private int height;

        private PopularChannelLayout(){
        }

        protected PopularChannelLayout(Parcel in) {
            x = in.readInt();
            y = in.readInt();
            width = in.readInt();
            height = in.readInt();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(x);
            parcel.writeInt(y);
            parcel.writeInt(width);
            parcel.writeInt(height);
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }
    }

}
