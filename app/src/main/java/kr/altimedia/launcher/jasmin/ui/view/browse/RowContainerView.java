/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.ColorInt;

import kr.altimedia.launcher.jasmin.R;

public class RowContainerView extends RelativeLayout {
    private final String TAG = RowContainerView.class.getSimpleName();
    private ViewGroup mHeaderDock;
    private Drawable mForeground;
    private boolean mForegroundBoundsChanged;

    public RowContainerView(Context context) {
        this(context, (AttributeSet)null, 0);
    }

    public RowContainerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RowContainerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mForegroundBoundsChanged = true;
//        this.setOrientation(LinearLayout.VERTICAL);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.container_row_overlap, this);
        this.mHeaderDock = (ViewGroup)this.findViewById(R.id.lb_row_container_header_dock);
        this.mHeaderDock.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        this.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    }

    public void addHeaderView(View headerView) {
        if (this.mHeaderDock.indexOfChild(headerView) < 0) {
            this.mHeaderDock.addView(headerView, 0);
        }

    }

    public void removeHeaderView(View headerView) {
        if (this.mHeaderDock.indexOfChild(headerView) >= 0) {
            this.mHeaderDock.removeView(headerView);
        }

    }

    public void addRowView(View view) {
        this.addView(view);
    }

    public void showHeader(boolean show) {
        this.mHeaderDock.setVisibility(show ? View.VISIBLE : View.GONE);
//        this.mHeaderDock.setVisibility(View.GONE);
    }

    public void setForeground(Drawable d) {
        this.mForeground = d;
        this.setWillNotDraw(this.mForeground == null);
        this.invalidate();
    }

    public void setForegroundColor(@ColorInt int color) {
        if (this.mForeground instanceof ColorDrawable) {
            ((ColorDrawable)this.mForeground.mutate()).setColor(color);
            this.invalidate();
        } else {
            this.setForeground(new ColorDrawable(color));
        }

    }

    public Drawable getForeground() {
        return this.mForeground;
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.mForegroundBoundsChanged = true;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.mForeground != null) {
            if (this.mForegroundBoundsChanged) {
                this.mForegroundBoundsChanged = false;
                this.mForeground.setBounds(0, 0, this.getWidth(), this.getHeight());
            }

            this.mForeground.draw(canvas);
        }

    }

    public boolean hasOverlappingRendering() {
        return false;
    }
}
