/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.altimedia.launcher.jasmin.R;

public class SettingAccountPinResetSuccessFragment extends Fragment {
    public static final String CLASS_NAME = SettingAccountPinResetSuccessFragment.class.getName();

    private TextView changeNowButton;
    private TextView changeLaterButton;

    private OnClickChangeButtonListener onClickChangeButtonListener;

    private SettingAccountPinResetSuccessFragment() {
    }

    public static SettingAccountPinResetSuccessFragment newInstance() {

        Bundle args = new Bundle();

        SettingAccountPinResetSuccessFragment fragment = new SettingAccountPinResetSuccessFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnClickChangeButtonListener(OnClickChangeButtonListener onClickChangeButtonListener) {
        this.onClickChangeButtonListener = onClickChangeButtonListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_setting_pin_reset_success, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        changeNowButton = view.findViewById(R.id.change_now_button);
        changeLaterButton = view.findViewById(R.id.change_later_button);

        changeNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickChangeButtonListener.onClickChangeButton(true);
            }
        });

        changeLaterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickChangeButtonListener.onClickChangeButton(false);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onClickChangeButtonListener = null;
    }

    public interface OnClickChangeButtonListener {
        void onClickChangeButton(boolean isNowChange);
    }
}
