package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Bundle;
import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.def.ObjectBundleDeserializer;

public class ExternalIntentDataResponse extends BaseResponse {
    public static final Creator<ExternalIntentDataResponse> CREATOR = new Creator<ExternalIntentDataResponse>() {
        @Override
        public ExternalIntentDataResponse createFromParcel(Parcel in) {
            return new ExternalIntentDataResponse(in);
        }

        @Override
        public ExternalIntentDataResponse[] newArray(int size) {
            return new ExternalIntentDataResponse[size];
        }
    };
    @Expose
    @SerializedName("data")
    @JsonAdapter(ObjectBundleDeserializer.class)
    private Bundle intentData;

    protected ExternalIntentDataResponse(Parcel in) {
        super(in);
        intentData = in.readBundle();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeBundle(intentData);
    }

    public Bundle getExternalIntentData() {
        return intentData;
    }

}
