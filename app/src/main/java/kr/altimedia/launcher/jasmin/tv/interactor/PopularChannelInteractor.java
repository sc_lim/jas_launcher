package kr.altimedia.launcher.jasmin.tv.interactor;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;

public interface PopularChannelInteractor {
    static interface PopularChannelDataListener {
        void onPopularChannelDataUpdated(String jsonData);
    }

    void setPopularChannelDataUpdateListener(PopularChannelDataListener listener);

    void setAudioTrack(int ranking);

    Channel getChannel(String serviceId);

    Program getCurrentProgram(String serviceId);
}
