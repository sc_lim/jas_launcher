/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Date;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponStatus;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.PointCoupon;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class CouponExpiringListRowPresenter extends Presenter {

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_coupon_history_expiring_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        if (item == null) {
            viewHolder.view.setVisibility(View.INVISIBLE);
            return;
        }

        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((Coupon)item);
        viewHolder.view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private TextView purchaseDateView;

        private LinearLayout discountCouponNameLayer;
        private TextView discountPercentView;

        private LinearLayout cashCouponNameLayer;
        private TextView couponPriceView;
        private LinearLayout bonusValueLayer;
        private TextView bonusValueView;
        private TextView bonusUnitView;

        private TextView balanceView;
        private TextView balanceAmoutUnitView;
        private TextView balanceRateUnitView;
        private TextView expireDateView;
        private TextView expireUntilView;

        private final String UNLIMITED = "31.12.9999";

        public ViewHolder(View view) {
            super(view);

            initView(view);
        }

        private void initView(View view) {
            purchaseDateView = view.findViewById(R.id.purchaseDate);

            discountCouponNameLayer = view.findViewById(R.id.discountCouponNameLayer);
            discountPercentView = view.findViewById(R.id.discountPercent);

            cashCouponNameLayer = view.findViewById(R.id.cashCouponNameLayer);
            couponPriceView = view.findViewById(R.id.couponPrice);
            bonusValueLayer = view.findViewById(R.id.bonusValueLayer);
            bonusValueView = view.findViewById(R.id.bonusValue);
            bonusUnitView = view.findViewById(R.id.bonusUnit);

            balanceView = view.findViewById(R.id.balance);
            balanceAmoutUnitView = view.findViewById(R.id.balanceAmountUnit);
            balanceRateUnitView = view.findViewById(R.id.balanceRateUnit);
            expireDateView = view.findViewById(R.id.expireDate);
            expireUntilView = view.findViewById(R.id.expireUntil);
        }

        public void setData(Coupon coupon) {
            if(coupon instanceof PointCoupon) {
                PointCoupon pc = (PointCoupon)coupon;
                if(pc.getRegisterDate() != null){
                    purchaseDateView.setText(TimeUtil.getModifiedDate(pc.getRegisterDate().getTime()));
                }
                couponPriceView.setText(StringUtil.getFormattedNumber(pc.getPointPrice()));

//                if(pc.getBonusValue() > 0) {
//                    if(pc.getBonusType() != null) {
//                        bonusValueView.setText(StringUtil.getFormattedNumber(pc.getBonusValue()));
//                        if (pc.getBonusType() == BonusType.DC_RATE) {
//                            bonusUnitView.setText("%");
//                        } else if (pc.getBonusType() == BonusType.DC_AMOUNT) {
//                            bonusUnitView.setText(R.string.thb);
//                        }
//                        bonusValueLayer.setVisibility(View.VISIBLE);
//                    }
//                }else{
//                    bonusValueLayer.setVisibility(View.GONE);
//                }

                discountCouponNameLayer.setVisibility(View.GONE);
                cashCouponNameLayer.setVisibility(View.VISIBLE);

            }else if(coupon instanceof DiscountCoupon) {
                DiscountCoupon dc = (DiscountCoupon)coupon;
                if(dc.getRegisterDate() != null){
                    purchaseDateView.setText(TimeUtil.getModifiedDate(dc.getRegisterDate().getTime()));
                }
                discountPercentView.setText(String.valueOf(dc.getRate()));

                cashCouponNameLayer.setVisibility(View.GONE);
                discountCouponNameLayer.setVisibility(View.VISIBLE);
            }

            balanceAmoutUnitView.setVisibility(View.GONE);
            balanceRateUnitView.setVisibility(View.GONE);
            expireUntilView.setVisibility(View.GONE);
            CouponStatus status = coupon.getStatus();
            if(status != null) {
                switch (status) {
                    case USED: {
                        balanceView.setText(R.string.used);
                        setExpireDateView(coupon.getExpireDate());
                        break;
                    }
                    case EXPIRED:
                    case DESTROYED: {
                        balanceView.setText("-");
                        expireDateView.setText(R.string.expired);
                        break;
                    }
                    default: {
                        int balance = 0;
                        if(coupon instanceof PointCoupon) {
                            balance = ((PointCoupon)coupon).getBalance();
                            balanceAmoutUnitView.setVisibility(View.VISIBLE);
                        }else if(coupon instanceof DiscountCoupon) {
                            balance = ((DiscountCoupon) coupon).getRate();
                            balanceRateUnitView.setVisibility(View.VISIBLE);
                        }
                        balanceView.setText(StringUtil.getFormattedNumber(balance));
                        setExpireDateView(coupon.getExpireDate());
                    }
                }
            }
        }

        private void setExpireDateView(Date date){
            if(date != null) {
                if(date != null) {
                    String dateStr = TimeUtil.getModifiedDate(date.getTime());
                    if(UNLIMITED.equals(dateStr)){
                        expireDateView.setText(R.string.unlimited);
                    }else {
                        expireUntilView.setVisibility(View.VISIBLE);
                        expireDateView.setText(dateStr);
                    }
                }
            }
        }
    }
}
