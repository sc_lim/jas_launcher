/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.payment.VodPaymentDataManager;
import kr.altimedia.launcher.jasmin.dm.payment.obj.PaymentPromotion;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.BankResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.CreditCardResponse;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentWrapper;
import kr.altimedia.launcher.jasmin.ui.util.task.TaskManager;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPurchaseFragment.KEY_PRODUCT;

public class VodPurchaseDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = VodPurchaseDialogFragment.class.getName();
    public static final String TAG = VodPurchaseDialogFragment.class.getSimpleName();

    public static final String KEY_DISCOUNT_TYPE_LIST = "AVAILABLE_DISCOUNT_TYPE_LIST";
    public static final String KEY_PAYMENT_TYPE_LIST = "AVAILABLE_PAYMENT_TYPE_LIST";

    private VodPurchaseFragment vodPurchaseFragment;

    private final PurchaseTaskManager mPurchaseTaskManager = new PurchaseTaskManager();
    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private PaymentItemBridgeAdapter.OnPaymentClickListener onPaymentClickListener;

    private ArrayList<PaymentType> availablePaymentTypeList;
    private CreditCardResponse.CreditCardResult creditCardResult;
    private BankResponse.BankResult bankResult;

    public VodPurchaseDialogFragment() {

    }

    public static VodPurchaseDialogFragment newInstance(Bundle bundle) {
        VodPurchaseDialogFragment fragment = new VodPurchaseDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setOnPaymentClickListener(PaymentItemBridgeAdapter.OnPaymentClickListener onPaymentClickListener) {
        this.onPaymentClickListener = onPaymentClickListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_vod_purchase, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initProgressbar(view);
//        loadPaymentPromotion();
        loadAvailablePaymentMethod();
    }

    private void initView() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                ArrayList<PaymentType> discountTypes = new ArrayList<>();
                ArrayList<PaymentType> paymentTypes = new ArrayList<>();

                for (PaymentType paymentType : availablePaymentTypeList) {
                    switch (paymentType) {
                        case DISCOUNT_COUPON:
                        case CASH_COUPON:
                        case MEMBERSHIP:
                            discountTypes.add(paymentType);
                            break;
                        default:
                            paymentTypes.add(paymentType);
                            break;
                    }
                }

                if (paymentTypes.size() == 0) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "paymentTypes size == 0, there is no payment type");
                    }

                    String errorCode = "LNC_0021";
                    String message = ErrorMessageManager.getInstance().getErrorMessage(errorCode);
                    ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                    dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            dismiss();
                        }
                    });
                    dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);

                    return;
                }

                Bundle bundle = getArguments();
                bundle.putSerializable(KEY_DISCOUNT_TYPE_LIST, discountTypes);
                bundle.putSerializable(KEY_PAYMENT_TYPE_LIST, paymentTypes);

                vodPurchaseFragment = VodPurchaseFragment.newInstance(bundle);
                vodPurchaseFragment.setOnPaymentClickListener(onPaymentClickListener);
                getChildFragmentManager().beginTransaction()
                        .replace(R.id.layout, vodPurchaseFragment, VodPurchaseFragment.CLASS_NAME).commit();
            }
        });
    }

    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void showProgress() {
        mProgressBarManager.show();
    }

    private void hideProgress() {
        if (mPurchaseTaskManager.isEmpty()) {
            mProgressBarManager.hide();
            initView();
        }
    }

    public PaymentWrapper getPaymentWrapper() {
        if (vodPurchaseFragment != null) {
            return vodPurchaseFragment.getPaymentWrapper();
        }

        Product product = getArguments().getParcelable(KEY_PRODUCT);
        return new PaymentWrapper(product.getPrice());
    }

    /*private void loadPaymentPromotion() {
        mPurchaseTaskManager.getPaymentPromotion(
                new MbsDataProvider<String, List<PaymentPromotion>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            mPurchaseTaskManager.completeLoadPromotionTask();
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, List<PaymentPromotion> result) {
                        for (PaymentPromotion promotion : result) {
                            PaymentType paymentType = promotion.getPaymentType();
                            paymentType.setPromotion(promotion.getPromotionDescription());
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPurchaseDialogFragment.this.getContext();
                    }
                });
    }*/

    private void loadAvailablePaymentMethod() {
        Product product = getArguments().getParcelable(KEY_PRODUCT);

        if (Log.INCLUDE) {
            Log.d(TAG, "loadAvailablePaymentMethod, product : " + product);
        }

        mPurchaseTaskManager.getAvailablePaymentMethod(
                product.getOfferID(),
                new MbsDataProvider<String, List<PaymentType>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            mPurchaseTaskManager.completeLoadPaymentMethodTask();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, List<PaymentType> result) {
                        availablePaymentTypeList = (ArrayList<PaymentType>) result;

                        boolean needMoreLoad = false;
                        for (PaymentType paymentType : availablePaymentTypeList) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "loadAvailablePaymentMethod, paymentType : " + paymentType);
                            }

                            if (paymentType == PaymentType.CREDIT_CARD) {
                                needMoreLoad = true;
                                loadCreditCardList();
                            } else if (paymentType == PaymentType.INTERNET_MOBILE_BANK) {
                                needMoreLoad = true;
                                loadBankList();
                            }
                        }

                        if (!needMoreLoad) {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPurchaseDialogFragment.this.getContext();
                    }
                });
    }

    private void loadCreditCardList() {
        if (Log.INCLUDE) {
            Log.d(TAG, "loadCreditCardList");
        }

        mPurchaseTaskManager.getCreditCardListResult(
                new MbsDataProvider<String, CreditCardResponse.CreditCardResult>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            mPurchaseTaskManager.completeLoadCreditListTask();
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, CreditCardResponse.CreditCardResult result) {
                        creditCardResult = result;

                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadAvailablePaymentMethod, onSuccess, creditCardResult : " + creditCardResult);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPurchaseDialogFragment.this.getContext();
                    }
                });
    }

    private void loadBankList() {
        if (Log.INCLUDE) {
            Log.d(TAG, "loadBankList");
        }

        mPurchaseTaskManager.getBankListResult(
                new MbsDataProvider<String, BankResponse.BankResult>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            mPurchaseTaskManager.completeLoadBankListTask();
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, BankResponse.BankResult result) {
                        bankResult = result;

                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadAvailablePaymentMethod, onSuccess, bankResult : " + bankResult);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPurchaseDialogFragment.this.getContext();
                    }
                });
    }

    public Object getPaymentOption(PaymentType paymentType) {
        if (paymentType == null) {
            return null;
        }

        switch (paymentType) {
            case CREDIT_CARD:
                return creditCardResult;
            case INTERNET_MOBILE_BANK:
                return bankResult;
        }

        return null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPurchaseTaskManager.cancelAllTask();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onPaymentClickListener = null;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private static class PurchaseTaskManager extends TaskManager {
        private final VodPaymentDataManager mVodPaymentDataManager = new VodPaymentDataManager();

        private MbsDataTask promotionTask;
        private MbsDataTask paymentMethodTask;
        private MbsDataTask creditCardListTask;
        private MbsDataTask bankListTask;

        public PurchaseTaskManager() {
        }

        public void getPaymentPromotion(MbsDataProvider<String, List<PaymentPromotion>> mbsDataProvider) {
            promotionTask = mVodPaymentDataManager.getPaymentPromotion(mbsDataProvider);
            addTask(promotionTask);
        }

        public void getAvailablePaymentMethod(String offerId, MbsDataProvider<String, List<PaymentType>> mbsDataProvider) {
            paymentMethodTask = mVodPaymentDataManager.getAvailablePaymentType(offerId, "vod", mbsDataProvider);
            addTask(paymentMethodTask);
        }


        public void getCreditCardListResult(MbsDataProvider<String, CreditCardResponse.CreditCardResult> mbsDataProvider) {
            creditCardListTask = mVodPaymentDataManager.getCreditCardListResult(mbsDataProvider);
            addTask(creditCardListTask);
        }

        public void getBankListResult(MbsDataProvider<String, BankResponse.BankResult> mbsDataProvider) {
            bankListTask = mVodPaymentDataManager.getBankListResult(mbsDataProvider);
            addTask(bankListTask);
        }

        public void completeLoadPromotionTask() {
            completeTask(promotionTask);
        }

        public void completeLoadPaymentMethodTask() {
            completeTask(paymentMethodTask);
        }

        public void completeLoadCreditListTask() {
            completeTask(creditCardListTask);
        }

        public void completeLoadBankListTask() {
            completeTask(bankListTask);
        }
    }
}
