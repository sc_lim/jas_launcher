
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityManager.AccessibilityStateChangeListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.tvmodule.common.TimerEvent;
import com.altimedia.tvmodule.common.WeakHandler;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.tvmodule.util.Utils;
import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Observable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.widget.OnChildSelectedListener;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.system.manager.ExternalApplicationManager;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.tv.interactor.TvViewInteractor;
import kr.altimedia.launcher.jasmin.tv.ui.ChannelPreviewIndicator;
import kr.altimedia.launcher.jasmin.tv.ui.TimeShiftFragment;
import kr.altimedia.launcher.jasmin.tv.viewModel.TVGuideViewModel;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.activity.BaseActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.PopularChannelFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.HardwareLayerAnimatorListenerAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.Linker;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.Reminder;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.drawer.EdgeDrawerLayout;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.BaseGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.GenreUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;
import kr.altimedia.launcher.jasmin.ui.view.widget.PerforatedRelativeLayout;
import kr.altimedia.launcher.jasmin.ui.view.widget.PerforatedView;

import static kr.altimedia.launcher.jasmin.ui.view.guide.ProgramManager.VIEW_TYPE_AUDIO;
import static kr.altimedia.launcher.jasmin.ui.view.guide.ProgramManager.VIEW_TYPE_CHANNEL_LIST;
import static kr.altimedia.launcher.jasmin.ui.view.guide.ProgramManager.VIEW_TYPE_FAV_LIST;
import static kr.altimedia.launcher.jasmin.ui.view.guide.ProgramManager.VIEW_TYPE_POP_LIST;

/**
 * The program guide.
 */
public class ProgramGuide
        implements ProgramGrid.ChildFocusListener, AccessibilityStateChangeListener,
        PerforatedRelativeLayout.OnPerforatedCallback, DateSelectSliderLayout.OnDateSelectSliderInterface, View.OnUnhandledKeyEventListener
        , ReminderAlertManager.OnReminderListener {
    private static final String TAG = "ProgramGuide";

    private static final String KEY_SHOW_GUIDE_PARTIAL = "show_guide_partial";
    private static final long TIME_INDICATOR_UPDATE_FREQUENCY = TimeUnit.SECONDS.toMillis(5);
    private static final long HOUR_IN_MILLIS = TimeUnit.HOURS.toMillis(1);
    private static final long HALF_HOUR_IN_MILLIS = HOUR_IN_MILLIS / 2;
    public static final long MIN_DURATION_FROM_START_TIME_TO_CURRENT_TIME =
            ProgramManager.FIRST_ENTRY_MIN_DURATION;

    private static final int MSG_PROGRAM_TABLE_FADE_IN_ANIM = 1000;

    private final BaseActivity mActivity;
    private final ProgramManager mProgramManager;
    private final ChannelDataManager mChannelDataManager;
    private final AccessibilityManager mAccessibilityManager;
    private final Runnable mPreShowRunnable;
    private final Runnable mPostHideRunnable;

    private final int mWidthPerHour;
    private final long mViewPortMillis;
    private final int mRowHeight;
    private final int mSelectionRow; // Row that is focused
    private final int mTableFadeAnimDuration;
    private final int mAnimationDuration;
    private final int mDetailPadding;
    private int mCurrentTimeIndicatorWidth;
    private final View mParents;
    private final PerforatedRelativeLayout mContainer;
    private final View mTable;
    private final View timeShiftGuideText;
    private final View guideText;
    private final View pauseKeyGuideText;
    private final View backKeyGuideText;
    private final View mNoChannelList;
    private final TextView mNoChannelListTitle;

    /*CategoryContainer*/
    private final View mCategoryContainer;
    private final PerforatedView mPig;
    private final TextView mChannelNumber;
    private final TextView mChannelName;
    private final ImageView mChannelLogo;
    private final TextView mProgramTitle;
    private final VerticalGridView categoryList;
    private final View arrowUp;
    private final View arrowDown;
    private final TextView mTimeLineDate;
    private final TimelineRow mTimelineRow;
    private final ProgramGrid mGrid;
    private final View mSubGuideTable;
    private final ProgramInformationLayout mBubbleLayer;
    private final TimeListAdapter mTimeListAdapter;
    private final View mCurrentTimeIndicator;

    private final Animator mShowAnimatorFull;
    private final Animator mHideAnimatorFull;
    private final Animator mPartialToFullAnimator;
    private final Animator mFullToPartialAnimator;
    private final Animator mProgramTableFadeOutAnimator;
    private final Animator mProgramTableFadeInAnimator;

    private View mSelectedRow;

    private long mStartUtcTime;
    private boolean mTimelineAnimation;
    //    private long mLastRequestedDate = JasmineEpgApplication.SystemClock().currentTimeMillis();
    private int mLastRequestedLineUp = GenreUtil.ID_ALL_CHANNELS;
    private boolean mIsDuringResetRowSelection;
    private final Handler mHandler = new ProgramGuideHandler(this);
    private boolean mActive;

    private final long mShowDurationMillis;
    private ViewTreeObserver.OnGlobalLayoutListener mOnLayoutListenerForShow;

    private final ProgramManagerListener mProgramManagerListener = new ProgramManagerListener();
    private Button changeDateBtn = null;
    private EdgeDrawerLayout mDrawer = null;
    private DateSelectSliderLayout mDatePanel = null;
    private TimerEvent mTimerEvent;
    private TvViewInteractor mTvViewInteractor;
    private final Runnable mUpdateTimeIndicator =
            new Runnable() {
                @Override
                public void run() {
                    positionCurrentTimeIndicator();
                    mHandler.postAtTime(
                            this,
                            Utils.ceilTime(
                                    SystemClock.uptimeMillis(), TIME_INDICATOR_UPDATE_FREQUENCY));
                }
            };
    private final ProgramTableAdapter programTableAdapter;
    private final int ALIGN_INDEX = 3;
    private final OnProgramGuideListener mOnProgramGuideListener;
    private final ArrayList<ChannelPropertyChangeListener> mChannelPropertyChangeListenerList = new ArrayList<>();
    @SuppressWarnings("RestrictTo")
    public ProgramGuide(
            BaseActivity activity,
            TvInputManagerHelper tvInputManagerHelper,
            ChannelDataManager channelDataManager,
            ProgramDataManager programDataManager,
            Runnable preShowRunnable,
            Runnable postHideRunnable, ViewGroup container, OnProgramGuideListener onProgramGuideListener) {
        mActivity = activity;
        mProgramManager =
                new ProgramManager(activity,
                        tvInputManagerHelper,
                        channelDataManager,
                        programDataManager, this);
        mChannelDataManager = channelDataManager;
        mPreShowRunnable = preShowRunnable;
        mPostHideRunnable = postHideRunnable;
        mOnProgramGuideListener = onProgramGuideListener;
        Resources res = activity.getResources();

        mWidthPerHour = res.getDimensionPixelSize(R.dimen.program_guide_table_width_per_hour);
        GuideUtils.setWidthPerHour(mWidthPerHour);
        mProgramManager.setViewTypeMap(getViewTypeList(activity));
        Point displaySize = new Point();
        mActivity.getWindowManager().getDefaultDisplay().getSize(displaySize);
//        int gridWidth =
//                displaySize.x
//                        - res.getDimensionPixelOffset(R.dimen.program_guide_table_margin_start)
//                        - res.getDimensionPixelSize(R.dimen.program_guide_table_header_column_width);
//        mViewPortMillis = (gridWidth * HOUR_IN_MILLIS) / mWidthPerHour;
        mViewPortMillis = HOUR_IN_MILLIS + HALF_HOUR_IN_MILLIS;
        mRowHeight = res.getDimensionPixelSize(R.dimen.program_guide_table_item_row_height);
        mSelectionRow = res.getInteger(R.integer.program_guide_selection_row);
        mTableFadeAnimDuration =
                res.getInteger(R.integer.program_guide_table_detail_fade_anim_duration);
        mShowDurationMillis = res.getInteger(R.integer.program_guide_show_duration);
        mAnimationDuration =
                res.getInteger(R.integer.program_guide_table_detail_toggle_anim_duration);
        mDetailPadding = res.getDimensionPixelOffset(R.dimen.program_guide_table_detail_padding);
        mParents = mActivity.findViewById(R.id.guide_parents);
        mContainer = mParents.findViewById(R.id.program_guide);

        timeShiftGuideText = mParents.findViewById(R.id.timeShiftGuideText);
        guideText = mParents.findViewById(R.id.guideText);
        pauseKeyGuideText = mParents.findViewById(R.id.pauseKeyGuideText);
        backKeyGuideText = mParents.findViewById(R.id.backKeyGuideText);
        ViewTreeObserver.OnGlobalFocusChangeListener globalFocusChangeListener =
                new GlobalFocusChangeListener();
        mParents.getViewTreeObserver().addOnGlobalFocusChangeListener(globalFocusChangeListener);
        GenreListAdapter genreListAdapter = new GenreListAdapter(mActivity, mProgramManager, this);


        mTable = mContainer.findViewById(R.id.program_guide_table);
        mNoChannelList = mContainer.findViewById(R.id.no_channel_list);
        mNoChannelListTitle = mContainer.findViewById(R.id.no_channel_name);
        mPig = mContainer.findViewById(R.id.pig);
        mPig.initBlockedText(mContainer.findViewById(R.id.blockedText));
        mCategoryContainer = mContainer.findViewById(R.id.program_guide_category);
        mChannelNumber = mContainer.findViewById(R.id.channelNumber);
        mChannelName = mContainer.findViewById(R.id.channelName);
        mChannelLogo = mContainer.findViewById(R.id.channelLogo);
        mProgramTitle = mContainer.findViewById(R.id.programTitle);
        categoryList = mCategoryContainer.findViewById(R.id.categoryList);
        arrowUp = mContainer.findViewById(R.id.arrowUp);
        arrowDown = mContainer.findViewById(R.id.arrowDown);
        categoryList.setOnScrollOffsetCallback(new BaseGridView.OnScrollOffsetCallback() {
            @Override
            public void onScrolledOffsetCallback(int offset, int remainScroll, int totalScroll) {
                arrowUp.setVisibility(offset != 0 ? View.VISIBLE : View.INVISIBLE);
                arrowDown.setVisibility(remainScroll != 0 ? View.VISIBLE : View.INVISIBLE);
            }
        });
        ProgramCategoryRowDecoration mDividerItemDecoration = new ProgramCategoryRowDecoration(categoryList.getContext());
        mDividerItemDecoration.setDrawable(categoryList.getResources().getDrawable(R.drawable.line_filter_divder));
        categoryList.addItemDecoration(mDividerItemDecoration);
        categoryList.setAdapter(genreListAdapter);
        categoryList.setItemAlignmentOffsetPercent(0);
        categoryList.setWindowAlignmentOffsetPercent(0);
        categoryList.setWindowAlignmentOffset(res.getDimensionPixelOffset(R.dimen.program_guide_side_panel_row_height));
        mTimeLineDate = mTable.findViewById(R.id.clock);

        mTimelineRow = mTable.findViewById(R.id.time_row);
        mTimeListAdapter = new TimeListAdapter(res);
        mTimelineRow
                .getRecycledViewPool()
                .setMaxRecycledViews(
                        R.layout.program_guide_table_header_row_item,
                        res.getInteger(R.integer.max_recycled_view_pool_epg_header_row_item));
        mTimelineRow.setAdapter(mTimeListAdapter);
        programTableAdapter =
                new ProgramTableAdapter(mActivity, this);
        programTableAdapter.registerAdapterDataObserver(
                new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onChanged() {
                        // It is usually called when Genre is changed.
                        // Reset selection of ProgramGrid
                        resetRowSelection();
//                        updateGuidePosition();
                    }
                });
        mBubbleLayer = mTable.findViewById(R.id.bubbleLayer);
        mProgramManager.setOnInformationInteractor(mBubbleLayer.getInformationInteractor());
        mSubGuideTable = mParents.findViewById(R.id.sub_guide_table);
        mGrid = mTable.findViewById(R.id.grid);
        mGrid.initialize(mProgramManager);
        mGrid.getRecycledViewPool()
                .setMaxRecycledViews(
                        R.layout.program_guide_table_row,
                        res.getInteger(R.integer.max_recycled_view_pool_epg_table_row));
        mGrid.setAdapter(programTableAdapter);
        mGrid.setChildFocusListener(this);
        mGrid.setOnChildSelectedListener(
                new OnChildSelectedListener() {
                    @Override
                    public void onChildSelected(
                            ViewGroup parent, View view, int position, long id) {
                        if (mIsDuringResetRowSelection) {
                            mIsDuringResetRowSelection = false;
                            return;
                        }
                        if (Log.INCLUDE) {
                            Log.d(TAG, "call onChildSelected");
                        }
                        selectRow(view);
                    }
                });
        mGrid.setWindowAlignmentOffset(ALIGN_INDEX * mRowHeight + mGrid.getPaddingTop());
        mGrid.setWindowAlignmentOffsetPercent(ProgramGrid.WINDOW_ALIGN_HIGH_EDGE);
        mGrid.setItemAlignmentOffset(0);
        mGrid.setItemAlignmentOffsetPercent(ProgramGrid.ITEM_ALIGN_OFFSET_PERCENT_DISABLED);

        RecyclerView.OnScrollListener onScrollListener =
                new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        onHorizontalScrolled(dx);
                    }

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, final int newState) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "TimelineRow onScrollStateChanged. newState=" + newState + ", needToFocus : " + needToFocus);
                        }
                        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                            if (needToFocus) {
                                mGrid.scrollToPosition(findChannelPosition());
                                int position = mGrid.getSelectedPosition();
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "TimelineRow SCROLL_STATE_IDLE position : " + position);
                                }
                                RecyclerView.ViewHolder row = mGrid.findViewHolderForAdapterPosition(position);
                                requestCurrentProgramFocus(row.itemView);
                            }
                        } else if (newState == RecyclerView.SCROLL_STATE_SETTLING) {
                            if (needToFocus) {
                                mGrid.scrollToPosition(findChannelPosition());
                                int position = mGrid.getSelectedPosition();
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "TimelineRow SCROLL_STATE_SETTLING  position : " + position);
                                }
                                RecyclerView.ViewHolder row = mGrid.findViewHolderForAdapterPosition(position);
                                if (row != null) {
                                    makeKeepFocusToCurrentProgram(row.itemView);
                                }
                            }
                        }
                    }
                };
        mTimelineRow.addOnScrollListener(onScrollListener);

        mCurrentTimeIndicator = mTable.findViewById(R.id.current_time_indicator);

        mShowAnimatorFull =
                createAnimator(
                        R.animator.program_guide_table_enter_full);
        mShowAnimatorFull.addListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (mGrid != null) {
                                    mGrid.requestFocus();
                                }
                            }
                        });
                    }
                });


        mHideAnimatorFull =
                createAnimator(
                        R.animator.program_guide_table_exit);
        mHideAnimatorFull.addListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mParents.setVisibility(View.GONE);
                    }
                });

        mPartialToFullAnimator =
                createAnimator(
                        R.animator.program_guide_table_partial_to_full);
        mFullToPartialAnimator =
                createAnimator(
                        R.animator.program_guide_table_full_to_partial);

        mProgramTableFadeOutAnimator =
                AnimatorInflater.loadAnimator(mActivity, R.animator.program_guide_table_fade_out);
        mProgramTableFadeOutAnimator.setTarget(mTable);
        mProgramTableFadeOutAnimator.addListener(
                new HardwareLayerAnimatorListenerAdapter(mTable) {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                        if (!isActive()) {
                            return;
                        }
                        mProgramManager.resetChannelListWithGenre(mLastRequestedLineUp);
                        resetTimelineScroll();
                        if (!mHandler.hasMessages(MSG_PROGRAM_TABLE_FADE_IN_ANIM)) {
                            mHandler.sendEmptyMessage(MSG_PROGRAM_TABLE_FADE_IN_ANIM);
                        }
                    }
                });
        mProgramTableFadeInAnimator =
                AnimatorInflater.loadAnimator(mActivity, R.animator.program_guide_table_fade_in);
        mProgramTableFadeInAnimator.setTarget(mTable);
        mProgramTableFadeInAnimator.addListener(new HardwareLayerAnimatorListenerAdapter(mTable));
        mAccessibilityManager =
                (AccessibilityManager) mActivity.getSystemService(Context.ACCESSIBILITY_SERVICE);
//        mAutoHideScheduler = new AutoHideScheduler(activity, this::hide);

        changeDateBtn = mContainer.findViewById(R.id.changeDateBtn);

        mDrawer = mParents.findViewById(R.id.drawer_layout);
        mDrawer.setDrawerShadow(R.drawable.shadow_category_panel, Gravity.LEFT);
        mDatePanel = mParents.findViewById(R.id.datePanel);
        mDatePanel.initialDate(mActivity, mProgramManager, this);
        mDatePanel.setOnDateSelectSliderInterface(this);
        mDrawer.addDrawerListener(mSimpleDrawerListener);


        changeDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMenu();
            }
        });
    }

    private HashMap<Integer, Integer> getViewTypeList(Context context) {
        HashMap<Integer, Integer> viewTypeMap = new HashMap<>();
        String[] ids = GenreUtil.getLabels(context);
        for (int i = 0; i < ids.length; i++) {
            if (i == 1) {
                viewTypeMap.put(i, ProgramManager.VIEW_TYPE_POP_LIST);
            } else if (i == 2) {
                viewTypeMap.put(i, ProgramManager.VIEW_TYPE_FAV_LIST);
            } else if (i == 4) {
                viewTypeMap.put(i, VIEW_TYPE_AUDIO);
            } else {
                viewTypeMap.put(i, VIEW_TYPE_CHANNEL_LIST);
            }
        }
        return viewTypeMap;
    }

    public boolean isOpenMenu() {
        return mDrawer.isDrawerOpen(mDatePanel);
    }

    public void openMenu() {
        if (Log.INCLUDE) {
            Log.d(TAG, "EdgeDrawerLayout openMenu");
        }
        if (isOpenMenu()) {
            return;
        }
        if (!changeDateBtn.isFocusable()) {
            return;
        }
        mDatePanel.initializeSelectedTime(mProgramManager.getFromUtcMillis());
        mDrawer.openDrawer(mDatePanel);
    }

    public void closeMenu() {
        if (Log.INCLUDE) {
            Log.d(TAG, "EdgeDrawerLayout closeMenu");
        }
        if (!isOpenMenu()) {
            return;
        }

        mDrawer.closeDrawer(mDatePanel);
    }

    private final EdgeDrawerLayout.SimpleDrawerListener mSimpleDrawerListener = new EdgeDrawerLayout.SimpleDrawerListener() {
        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            if (Log.INCLUDE) {
                Log.d(TAG, "onDrawerOpened : " + drawerView.getClass().getSimpleName());
            }
            setFocusAllow(mContainer, false);
            setFocusAllow(((ViewGroup) drawerView), true);
            drawerView.requestFocus();
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
            if (Log.INCLUDE) {
                Log.d(TAG, "onDrawerClosed : " + needToGuideFocus + ", needToFocus : " + needToFocus);
            }
            setFocusAllow(((ViewGroup) drawerView), false);
            setFocusAllow(mContainer, true);

            if (needToGuideFocus) {
                needToGuideFocus = false;
                mGrid.scrollToPosition(findChannelPosition());
                int position = mGrid.getSelectedPosition();
                if (Log.INCLUDE) {
                    Log.d(TAG, "TimelineRow SCROLL_STATE_IDLE position : " + position);
                }
                RecyclerView.ViewHolder row = mGrid.findViewHolderForAdapterPosition(position);
                requestCurrentProgramFocus(row.itemView);
            } else if (!needToFocus) {
                if (needToScroll) {
                    needToScroll = false;

                    if (findChannelPosition() == -1) {
                        return;
                    }
                    int count = 0;
                    while (true) {

                        mGrid.scrollToPosition(findChannelPosition());
                        if (findChannelPosition() == mGrid.getSelectedPosition() || count > 5) {
                            mContainer.requestFocus();
                            break;
                        }
                        count++;
                    }
                } else {
                    mContainer.requestFocus();
                }
            } else {
                mContainer.requestFocus();
            }
        }

        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            super.onDrawerSlide(drawerView, slideOffset);
            EdgeDrawerLayout.DrawerState state = mDrawer.getDrawerState(drawerView);
        }
    };

    private void setFocusAllow(ViewGroup viewGroup, boolean focus) {
        viewGroup.setDescendantFocusability(focus ? ViewGroup.FOCUS_AFTER_DESCENDANTS : ViewGroup.FOCUS_BLOCK_DESCENDANTS);
    }

    @Override
    public void onRequestChildFocus(View oldFocus, View newFocus) {
        if (oldFocus != null && newFocus != null) {
            int selectionRowOffset = mSelectionRow * mRowHeight;
            if (oldFocus.getTop() < newFocus.getTop()) {
                // Selection moves downwards
                // Adjust scroll offset to be at the bottom of the target row and to expand up. This
                // will set the scroll target to be one row height up from its current position.
//                mGrid.setWindowAlignmentOffset(selectionRowOffset + mRowHeight);
//                mGrid.setItemAlignmentOffsetPercent(100);
            } else if (oldFocus.getTop() > newFocus.getTop()) {
                // Selection moves upwards
                // Adjust scroll offset to be at the top of the target row and to expand down. This
                // will set the scroll target to be one row height down from its current position.
//                mGrid.setWindowAlignmentOffset(selectionRowOffset);
//                mGrid.setItemAlignmentOffsetPercent(0);
            }
        }
    }

    public void show(final Runnable runnableAfterAnimatorReady) {
        this.show(runnableAfterAnimatorReady, 0);
    }

    private boolean isInitialize = false;

    private long calculatedInitialTime() {
        final long baseTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
        long time = baseTime - (MIN_DURATION_FROM_START_TIME_TO_CURRENT_TIME);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long checkTime = calendar.getTimeInMillis();
        while (true) {
            checkTime -= mViewPortMillis;
            if (checkTime < time) {
                break;
            }
        }
        return checkTime;
    }

    private long calculatedCurrentScrollTime() {
        long baseTime = JasmineEpgApplication.SystemClock().currentTimeMillis();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(baseTime);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long checkTime = calendar.getTimeInMillis();
        while (true) {
            checkTime -= mViewPortMillis;
            if (checkTime < baseTime) {
                break;
            }
        }
        if (Math.abs(checkTime - baseTime) == mViewPortMillis) {
            return baseTime;
        }
        return checkTime;
    }

    private long calculatedScrollTime() {
        TvOverlayManager tvOverlayManager = ((LiveTvActivity) mActivity).getTvOverlayManager();
        long baseTime;
        if (tvOverlayManager.isTimeShiftActive()) {
            TimeShiftFragment timeShiftFragment = tvOverlayManager.getTimeShiftFragment();
            if (timeShiftFragment == null) {
                baseTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
            } else {
                baseTime = timeShiftFragment.getCurrentProgram().getStartTimeUtcMillis();
            }
        } else {
            baseTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
        }

        long time = baseTime - (MIN_DURATION_FROM_START_TIME_TO_CURRENT_TIME);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        long checkTime = calendar.getTimeInMillis();
        while (true) {
            checkTime -= mViewPortMillis;
            if (checkTime < time) {
                break;
            }
        }
        if (Math.abs(checkTime - time) == mViewPortMillis) {
            return time;
        }
        return checkTime;
    }

    public boolean isTimeLineScrollState() {
        return mTimelineRow.getScrollState() != RecyclerView.SCROLL_STATE_IDLE;
    }

    public void show(final Runnable runnableAfterAnimatorReady, int channelLineUp) {
        if (mParents.getVisibility() == View.VISIBLE) {
            return;
        }
        addViewModelEvent(mActivity.getTVGuideViewModel());
        if (mPreShowRunnable != null) {
            mPreShowRunnable.run();
        }
        withHideTune = true;
        isInitialize = true;
        mLastRequestedLineUp = channelLineUp;
        mParents.addOnUnhandledKeyEventListener(this);
        mProgramManager.programGuideVisibilityChanged(true);
        mProgramManager.addListener(mProgramManagerListener);
        BackendKnobsFlags mBackendKnobsFlags = TvSingletons.getSingletons(mActivity).getBackendKnobs();

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mStartUtcTime = calculatedInitialTime();
                long focusStartTime = calculatedScrollTime();
                mProgramManager.updateInitialTimeRange(mStartUtcTime,
                        mStartUtcTime + MIN_DURATION_FROM_START_TIME_TO_CURRENT_TIME +
                                TimeUnit.HOURS.toMillis(mBackendKnobsFlags.programGuideMaxHours()),
                        focusStartTime + MIN_DURATION_FROM_START_TIME_TO_CURRENT_TIME,
                        focusStartTime + mViewPortMillis + MIN_DURATION_FROM_START_TIME_TO_CURRENT_TIME);
                mProgramManager.resetChannelListWithGenre(channelLineUp);
                mTimeListAdapter.update(mStartUtcTime);
            }
        });

//        mTimelineRow.resetScroll();
//        mProgramManager.shiftTime(currentTime - mProgramManager.getFromUtcMillis());
        mParents.setVisibility(View.VISIBLE);
        mActive = true;
        mTable.requestFocus();
        positionCurrentTimeIndicator();
        if (Log.INCLUDE) {
            Log.d(TAG, "show()");
        }
        mOnLayoutListenerForShow =
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mParents.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        mTable.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                        mTable.buildLayer();
                        mOnLayoutListenerForShow = null;
                        mTimelineAnimation = true;
                        // Make sure that time indicator update starts after animation is finished.
                        startCurrentTimeIndicator(TIME_INDICATOR_UPDATE_FREQUENCY);
//                        if (DEBUG) {
//                            mParents
//                                    .getViewTreeObserver()
//                                    .addOnDrawListener(
//                                            new ViewTreeObserver.OnDrawListener() {
//                                                long time = JasmineEpgApplication.SystemClock().currentTimeMillis();
//                                                int count = 0;
//
//                                                @Override
//                                                public void onDraw() {
//                                                    long curtime = JasmineEpgApplication.SystemClock().currentTimeMillis();
//                                                    Log.d(
//                                                            TAG,
//                                                            "onDraw "
//                                                                    + count++
//                                                                    + " "
//                                                                    + (curtime - time)
//                                                                    + "ms");
//                                                    time = curtime;
////                                                    if (count > 10) {
////                                                        mParents
////                                                                .getViewTreeObserver()
////                                                                .removeOnDrawListener(this);
////                                                    }
//                                                }
//                                            });
//                        }
                        updateGuidePosition();
                        runnableAfterAnimatorReady.run();
                        mShowAnimatorFull.start();
                    }
                };
        mParents.getViewTreeObserver().addOnGlobalLayoutListener(mOnLayoutListenerForShow);
        scheduleHide();
        initChannelInfo(mActivity.getTVGuideViewModel());
        mContainer.setOnPerforatedCallback(this);

        if (mActivity instanceof TvViewInteractor) {
            mTvViewInteractor = (TvViewInteractor) mActivity;
            //mTvViewInteractor.setPreviewListener(mContainer.findViewById(R.id.guide_preview_indicator));
            mTvViewInteractor.setPreviewListener(new ChannelPreviewIndicator.OnPreviewListener() {
                public void setVisibility(int visibility) {
                    mContainer.findViewById(R.id.guide_preview_indicator).setVisibility(visibility);
                }
                public void updateTime(String time, String unit) {
                    if (mContainer.findViewById(R.id.guide_preview_indicator).getVisibility() != View.VISIBLE) {
                        mContainer.findViewById(R.id.guide_preview_indicator).setVisibility(View.VISIBLE);
                    }
                    ((TextView)mContainer.findViewById(R.id.guide_preview_time)).setText(time);
                    ((TextView)mContainer.findViewById(R.id.guide_preview_time_unit)).setText(unit);
                }
            });
        }
        ReminderAlertManager.getInstance().addOnReminderListener(this);


        changeButtonFocusable(mProgramManager.getLastViewType(), false);
        changeButtonNextFocus(mProgramManager.getLastViewType(), false);
        mProgramManager.setBackChannelForMosaic(null);
        if (mOnProgramGuideListener != null) {
            mOnProgramGuideListener.onProgramGuideShow();
        }
        updateGuidePosition();
    }

    private int findChannelPosition() {

        TvOverlayManager tvOverlayManager = ((LiveTvActivity) mActivity).getTvOverlayManager();

        Channel currentChannel = null;
        if (tvOverlayManager.isTimeShiftActive()) {
            TimeShiftFragment timeShiftFragment = tvOverlayManager.getTimeShiftFragment();
            if (timeShiftFragment == null) {
                currentChannel = mActivity.getTVGuideViewModel().currentChannel.getCurrentChannel();
            } else {
                currentChannel = timeShiftFragment.getCurrentChannel();
            }
        } else {
            currentChannel = mActivity.getTVGuideViewModel().currentChannel.getCurrentChannel();
        }

        if (currentChannel == null) {
            currentChannel = mChannelDataManager.getGuideChannel();
        }
        if (currentChannel == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "findChannelPosition : return 0");
            }
            return 0;
        }
        int index = mProgramManager.getChannelIndex(currentChannel.getId());
        if (Log.INCLUDE) {
            Log.d(TAG, "findChannelPosition : return : " + index);
        }
        if (index == -1) {
            Channel channel = mChannelDataManager.getLastWatchedChannel();
            if (channel == null) {
                Channel lowestChannel = mChannelDataManager.getLowestChannel();
                int lowestIndex = mProgramManager.getChannelIndex(lowestChannel.getId());
                index = lowestIndex != -1 ? lowestIndex : 0;
            } else {
                int lcwIndex = mProgramManager.getChannelIndex(channel.getId());
                if (lcwIndex == -1) {
                    Channel lowestChannel = mChannelDataManager.getLowestChannel();
                    int lowestIndex = mProgramManager.getChannelIndex(lowestChannel.getId());
                    index = lowestIndex != -1 ? lowestIndex : 0;
                } else {
                    index = lcwIndex;
                }
            }

        }
        return index;
    }

    private Rect currentRect = new Rect();

    private void requestResize(Rect rect, @NonNull TvViewInteractor tvViewInteractor) {
        if (currentRect.equals(rect)) {
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "requestResize : " + rect);
        }
        currentRect = rect;
        tvViewInteractor.resizeTvView(rect);
    }

    public void requestBackPressed(View view) {
        categoryList.requestFocus();
    }

    public Channel findCurrentChannel() {
        TvOverlayManager tvOverlayManager = ((LiveTvActivity) mActivity).getTvOverlayManager();
        if (tvOverlayManager.isTimeShiftActive()) {
            TimeShiftFragment timeShiftFragment = tvOverlayManager.getTimeShiftFragment();
            if (timeShiftFragment == null) {
                return mActivity.getTVGuideViewModel().currentChannel.getCurrentChannel();
            } else {
                return timeShiftFragment.getCurrentChannel();
            }
        } else {
            return mActivity.getTVGuideViewModel().currentChannel.getCurrentChannel();
        }
    }

    public Program findCurrentProgram() {
        TvOverlayManager tvOverlayManager = ((LiveTvActivity) mActivity).getTvOverlayManager();
        if (tvOverlayManager.isTimeShiftActive()) {
            TimeShiftFragment timeShiftFragment = tvOverlayManager.getTimeShiftFragment();
            if (timeShiftFragment == null) {
                return mActivity.getTVGuideViewModel().currentProgram.getProgram();
            } else {
                return timeShiftFragment.getCurrentProgram();
            }
        } else {
            return mActivity.getTVGuideViewModel().currentProgram.getProgram();
        }
    }

    public void requestTuneChannel(Channel channel, Handler handler) {
        requestTuneChannel(channel, null, handler);
    }

    public void requestTuneChannel(Channel channel, Program program, Handler handler) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call requestTuneChannel");
        }
        if (mTvViewInteractor == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "call requestTuneChannel mTvViewInteractor == null return");
            }
            return;
        }
        TVGuideViewModel model = mActivity.getTVGuideViewModel();
        Channel currentChannel = findCurrentChannel();
        if (Log.INCLUDE) {
            Log.d(TAG, "call requestTuneChannel currentChannel.getServiceId()  : " + currentChannel.getServiceId());
            Log.d(TAG, "call requestTuneChannel channel.getServiceId()  : " + channel.getServiceId());
        }

        TvOverlayManager tvOverlayManager = ((LiveTvActivity) mActivity).getTvOverlayManager();
        if (program != null && tvOverlayManager.isTimeShiftActive()) {
            TvViewInteractor.TuneResult tuneResult = mTvViewInteractor.tuneChannel(channel);
        } else {
            if (currentChannel.getServiceId() == channel.getServiceId()) {
                hide();
                return;
            }
            TvViewInteractor.TuneResult tuneResult = mTvViewInteractor.tuneChannel(channel);
            if (Log.INCLUDE) {
                Log.d(TAG, "call requestTuneChannel tuneResult  : " + tuneResult);
            }
        }
    }

    public void addChannelPropertyChangeListener(ChannelPropertyChangeListener channelPropertyChangeListener) {
        if (Log.INCLUDE) {
            Log.d(TAG, "addChannelPropertyChangeListener");
        }
        if (mChannelPropertyChangeListenerList.contains(channelPropertyChangeListener)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "addChannelPropertyChangeListener contains return");
            }
            return;
        }
        mChannelPropertyChangeListenerList.add(channelPropertyChangeListener);
    }

    public void removeChannelPropertyChangeListener(ChannelPropertyChangeListener channelPropertyChangeListener) {
        if (Log.INCLUDE) {
            Log.d(TAG, "removeChannelPropertyChangeListener");
        }
        if (!mChannelPropertyChangeListenerList.contains(channelPropertyChangeListener)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "removeChannelPropertyChangeListener not contains return");
            }
            return;
        }
        mChannelPropertyChangeListenerList.remove(channelPropertyChangeListener);
    }

    public Channel getTuneChannel() {
        return findCurrentChannel();
    }


    private void setChannelInfo(Channel channel) {
        if (channel == null) {
            return;
        }
        if (channel.isHiddenChannel()) {
            mChannelNumber.setText("");
            mChannelName.setText("");
            mChannelLogo.setImageDrawable(null);
            return;
        }

        for (ChannelPropertyChangeListener channelPropertyChangeListener : mChannelPropertyChangeListenerList) {
            channelPropertyChangeListener.onChannelChanged(channel);
        }

        mChannelNumber.setText(String.format("%03d", Integer.parseInt(channel.getDisplayNumber())));
        String logo = channel.getLogoUri();
        Glide.with(mActivity)
                .load(logo).diskCacheStrategy(DiskCacheStrategy.DATA)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onLoadStarted(@Nullable Drawable placeholder) {
                        super.onLoadStarted(placeholder);
                        mChannelName.setText(channel.getDisplayName());
                        mChannelName.setVisibility(View.VISIBLE);
                        mChannelLogo.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        mChannelLogo.setImageDrawable(resource);
                        mChannelLogo.setVisibility(View.VISIBLE);
                        mChannelName.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        mChannelName.setText(channel.getDisplayName());
                        mChannelName.setVisibility(View.VISIBLE);
                        mChannelLogo.setVisibility(View.GONE);
                    }
                });
    }


    private static class GuideUnHandlerKeyListener implements View.OnUnhandledKeyEventListener {
        private final ProgramGuide programGuide;

        public GuideUnHandlerKeyListener(ProgramGuide programGuide) {
            this.programGuide = programGuide;
        }

        @Override
        public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
            return false;
        }
    }

    private void setProgramInfo(Channel channel, Program program) {
        Resources resources = mProgramTitle.getResources();
        if (program == null || StringUtils.nullToEmpty(program.getTitle()).isEmpty()) {
            if (channel != null && (channel.isHiddenChannel() || channel.isAudioChannel())) {
                mProgramTitle.setText("");
            } else {
                mProgramTitle.setText(resources.getString(R.string.program_title_for_no_information));
            }
        } else {
            if (channel.isHiddenChannel() || channel.isAudioChannel()) {
                mProgramTitle.setText("");
            } else {
                mProgramTitle.setText(program.getTitle());
            }
        }
    }

    private void setCurrentTuneResult(TvViewInteractor.TuneResult tuneResult, Channel channel) {
        Context context = mCategoryContainer.getContext();
        if (Log.INCLUDE) {
            Log.d(TAG, "setCurrentTuneResult : " + tuneResult);
        }
        if (tuneResult == TvViewInteractor.TuneResult.TUNE_SUCCESS) {
            if (channel == null) {
                mPig.setType(PerforatedView.PerforateType.PIP);
                mPig.clearPerforatedImage();
                mPig.setText(R.string.empty);
                return;
            }
            if (channel.isAudioChannel()) {
                mPig.setType(PerforatedView.PerforateType.IMAGE);
                mPig.setPerforatedImage(R.drawable.grid_pip_audio);
                mPig.setText(R.string.empty);
            } else if (channel.isMosaicChannel()) {
                mPig.setType(PerforatedView.PerforateType.IMAGE);
                mPig.setPerforatedImage(R.drawable.popular_channel_img);
                mPig.setText(R.string.empty);
            }
//            else if (channel.isLocked()) {
//                mPig.setType(PerforatedView.PerforateType.IMAGE);
//                mPig.setPerforatedImage(R.drawable.grid_pip_blocked);
//                mPig.setText(R.string.blocked_channel);
//            }
            else if (channel.isPaidChannel()) {
                if (JasChannelManager.getInstance().isPreviewAvailable(channel)) {
                    mPig.setType(PerforatedView.PerforateType.PIP);
                    mPig.clearPerforatedImage();
                    mPig.setText(R.string.empty);
                } else {
                    mPig.setType(PerforatedView.PerforateType.IMAGE);
                    mPig.setPerforatedImage(R.drawable.grid_pip_unsubscribed);
                    mPig.setText(R.string.unsubscribed_channel);
                }
            } else {
                mPig.setType(PerforatedView.PerforateType.PIP);
                mPig.clearPerforatedImage();
                mPig.setText(R.string.empty);
            }
        } else if (tuneResult == TvViewInteractor.TuneResult.TUNE_BLOCKED_CHANNEL) {
            mPig.setPerforatedImage(context.getDrawable(R.drawable.grid_pip_blocked));
            mPig.setType(PerforatedView.PerforateType.IMAGE);
            mPig.setText(R.string.blocked_channel);
        } else if (tuneResult == TvViewInteractor.TuneResult.TUNE_BLOCKED_ADULT_CHANNEL) {
            mPig.setPerforatedImage(context.getDrawable(R.drawable.grid_pip_blocked));
            mPig.setType(PerforatedView.PerforateType.IMAGE);
            mPig.setText(R.string.restricted_channel);
        } else if (tuneResult == TvViewInteractor.TuneResult.TUNE_BLOCKED_PROGRAM) {
            mPig.setPerforatedImage(context.getDrawable(R.drawable.grid_pip_parental));
            mPig.setType(PerforatedView.PerforateType.IMAGE);
            mPig.setText(R.string.parental_rating_program);
        } else if (tuneResult == TvViewInteractor.TuneResult.TUNE_BLOCKED_UNSUBSCRIBED) {
            mPig.setPerforatedImage(context.getDrawable(R.drawable.grid_pip_unsubscribed));
            mPig.setType(PerforatedView.PerforateType.IMAGE);
            mPig.setText(R.string.unsubscribed_channel);
        }
    }

    private final Observable.OnPropertyChangedCallback mProgramObserver = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            if (!(mParents.getVisibility() == View.VISIBLE)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "viewModel currentProgram changed but not showing programGuide so return");
                }
                return;
            }
            setProgramInfo(mActivity.getTVGuideViewModel().currentChannel.getCurrentChannel(),
                    mActivity.getTVGuideViewModel().currentProgram.getProgram());
        }
    };
    private final Observable.OnPropertyChangedCallback mCurrentChannelObserver = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            if (!(mParents.getVisibility() == View.VISIBLE)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "viewModel currentChannel changed but not showing programGuide so return");
                }
                return;
            }
            Channel channel = mActivity.getTVGuideViewModel().currentChannel.getCurrentChannel();


            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    setChannelInfo(channel);
                }
            });


        }
    };
    private final Observable.OnPropertyChangedCallback mTuneResultObserver = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            if (Log.INCLUDE) {
                Log.d(TAG, "viewModel.currentTuneResult : onPropertyChanged");
            }
            if (!(mParents.getVisibility() == View.VISIBLE)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "viewModel currentTuneResult changed but not showing programGuide so return");
                }
                return;
            }
            TvViewInteractor.TuneResult tuneResult = mActivity.getTVGuideViewModel().currentTuneResult.getTuneResult();
            setCurrentTuneResult(tuneResult, mActivity.getTVGuideViewModel().currentChannel.getCurrentChannel());
        }
    };

    private void clearViewModelEvent(final TVGuideViewModel viewModel) {
        viewModel.currentChannel.removeOnPropertyChangedCallback(mCurrentChannelObserver);
        viewModel.currentProgram.removeOnPropertyChangedCallback(mProgramObserver);
        viewModel.currentTuneResult.removeOnPropertyChangedCallback(mTuneResultObserver);
    }

    private void addViewModelEvent(final TVGuideViewModel viewModel) {
        viewModel.currentChannel.addOnPropertyChangedCallback(mCurrentChannelObserver);
        viewModel.currentProgram.addOnPropertyChangedCallback(mProgramObserver);
        viewModel.currentTuneResult.addOnPropertyChangedCallback(mTuneResultObserver);
    }

    private void initChannelInfo(final TVGuideViewModel viewModel) {
        if (Log.INCLUDE) {
            Log.d(TAG, "initChannelInfo");
        }
        setChannelInfo(findCurrentChannel());
        setProgramInfo(findCurrentChannel(), findCurrentProgram());
        setCurrentTuneResult(viewModel.currentTuneResult.getTuneResult(), findCurrentChannel());
    }

    private boolean isKeyBlock = false;

    public boolean isKeyBlock() {
        return isKeyBlock;
    }

    public void setKeyBlock(boolean keyBlock) {
        isKeyBlock = keyBlock;
    }

    public void requestReserveProgram(Program program, Handler handler) {
        if (mTvViewInteractor == null) {
            setKeyBlock(false);
            return;
        }
        if (program == null) {
            setKeyBlock(false);
            return;
        }

        if (ReminderAlertManager.getInstance().isReserved(program)) {
            mTvViewInteractor.cancelReserveProgram(program, handler);
        } else {
            mTvViewInteractor.reserveProgram(program, handler);
        }
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_PROG_GREEN ||
                event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                openMenu();
            }
            return true;
        } else if (event.getKeyCode() == KeyEvent.KEYCODE_BUTTON_1) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
//                mProgramManager.resetChannelListWithGenre(GenreUtil.getId(GenreUtil.FAVORITE_CHANNELS));
                if (mActivity instanceof LiveTvActivity) {
                    Linker.gotoMyMenu(((LiveTvActivity) mActivity).getTvOverlayManager(), mActivity.getSupportFragmentManager(), mActivity);
                } else if (mActivity instanceof LauncherActivity) {
                    Linker.gotoMyMenu(((LauncherActivity) mActivity).getTvOverlayManager(), mActivity.getSupportFragmentManager(), mActivity);
                }
            }
            return true;
        } else if (event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_REWIND ||
                event.getKeyCode() == KeyEvent.KEYCODE_PROG_RED) {
            return true;
        } else return (event.getKeyCode() == KeyEvent.KEYCODE_INFO);
    }

    public void requestTuneTimeShift(Channel channel, Program program, Handler handler) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestTuneTimeShift");
        }
        if (mTvViewInteractor == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "requestTuneTimeShift mTvViewInteractor == null return");
            }
            return;
        }

        if (!isTimeShiftEnabledChannel(channel)) {
            showToast(mActivity.getString(R.string.not_timeshift_channel));
        } else {
            if (!isTimeShiftEnabled(channel, program)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "requestTuneTimeShift isTimeShiftEnabled not available so return");
                }
                showToast(mActivity.getString(R.string.cannot_play_timeshift));
            } else {
                if (channel.isPaidChannel()) {
                    showToast(mActivity.getString(R.string.cannot_play_unsubscribed_timeshift));
                } else {
                    mTvViewInteractor.tuneTimeShift(channel, program, -1);
                }
            }
        }
    }

    private void showToast(String message) {
        JasminToast.makeToast(mActivity, message);
    }

    private boolean isTimeShiftEnabled(Channel channel, Program program) {
        if (program == null) {
            return false;
        }
        long timeShiftTimeMillis = channel != null ? channel.getTimeshiftService() : 0;
        if (Log.INCLUDE) {
            Log.d(TAG, "isTimeShiftEnabled | timeShiftTimeMillis : " + timeShiftTimeMillis);
        }
        long currentTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
        long availableTime = currentTime - timeShiftTimeMillis;
        return program.getEndTimeUtcMillis() > availableTime;
    }

    private boolean isTimeShiftEnabledChannel(Channel channel) {
        long timeShiftTimeMillis = channel != null ? channel.getTimeshiftService() : 0;
        if (Log.INCLUDE) {
            Log.d(TAG, "isTimeShiftEnabledChannel | timeShiftTimeMillis : " + timeShiftTimeMillis);
        }

        return timeShiftTimeMillis != 0;
    }

    public boolean isLockedChannel(Channel channel) {
        return isLockedChannel(channel, null);
    }

    public boolean isUnLockedChannel(Channel channel) {
        return mTvViewInteractor.isUnLocked(channel);
    }

    public boolean isRestrictedChannel(Channel channel) {
        return mTvViewInteractor.isLockedAdultChannel(channel);
    }

    public boolean isLockedChannel(Channel channel, Program program) {
        return mTvViewInteractor.isLockedChannel(channel, program);
    }

    public int getSelectedGenreId() {
        return mProgramManager.getSelectedGenreId();
    }

    /**
     * Hide the program guide.
     */
    private boolean withHideTune = false;

    public void hide(boolean withTune) {
        if (!isActive()) {
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "hide()");
        }
        closeMenu();
        withHideTune = withTune;
        mActive = false;
        clearViewModelEvent(mActivity.getTVGuideViewModel());
        mParents.removeOnUnhandledKeyEventListener(this);
        if (mLastRequestedLineUp != 0) {
            mLastRequestedLineUp = 0;
            mProgramManager.resetChannelListWithGenre(mLastRequestedLineUp, false);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mProgramManagerListener.onListViewTypeChanged(VIEW_TYPE_CHANNEL_LIST, mProgramManager.getFilteredChannels());
                }
            });
        }
        if (mOnLayoutListenerForShow != null) {
            mParents.getViewTreeObserver().removeOnGlobalLayoutListener(mOnLayoutListenerForShow);
            mOnLayoutListenerForShow = null;
        }
        cancelHide();
        mProgramManager.programGuideVisibilityChanged(false);
        mProgramManager.removeListener(mProgramManagerListener);
        mHideAnimatorFull.start();

        // Clears fade-out/in animation for genre change
        if (mProgramTableFadeOutAnimator.isRunning()) {
            mProgramTableFadeOutAnimator.cancel();
        }
        if (mProgramTableFadeInAnimator.isRunning()) {
            mProgramTableFadeInAnimator.cancel();
        }
        mHandler.removeMessages(MSG_PROGRAM_TABLE_FADE_IN_ANIM);
        mTable.setAlpha(1.0f);

        mTimelineAnimation = false;
        stopCurrentTimeIndicator();
        if (mPostHideRunnable != null) {
            mPostHideRunnable.run();
        }

        if (mTvViewInteractor != null) {
            Rect rect = new Rect();
            mActivity.getWindowManager().getDefaultDisplay().getRectSize(rect);

            requestResize(rect, mTvViewInteractor);

            mTvViewInteractor.setPreviewListener(null);
        }
//        mTvViewInteractor = null;

        ReminderAlertManager.getInstance().removeOnReminderListener(this);
        mChannelPropertyChangeListenerList.clear();
        if (withHideTune && mOnProgramGuideListener != null) {
            mOnProgramGuideListener.onProgramGuideDismiss();
        }
    }

    public void hide() {
        hide(true);
    }

    private void resetGuideConfig() {

    }


    /**
     * Schedules hiding the program guide.
     */
    public void scheduleHide() {
//        mAutoHideScheduler.schedule(mShowDurationMillis);
    }

    /**
     * Cancels hiding the program guide.
     */
    public void cancelHide() {
//        mAutoHideScheduler.cancel();
    }

    /**
     * Process the {@code KEYCODE_BACK} key event.
     */
    public void onBackPressed() {
        hide();
    }

    /**
     * Returns {@code true} if the program guide should process the input events.
     */
    public boolean isActive() {
        return mActive;
    }

    /**
     * Returns {@code true} if the program guide is shown, i.e. showing animation is done and hiding
     * animation is not started yet.
     */
    public boolean isRunningAnimation() {
        return mShowAnimatorFull.isStarted()
                || mHideAnimatorFull.isStarted();
    }

    void requestDateChange(long date) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestDateChange : " + date);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        long requestedTime = calendar.getTimeInMillis();
        long startTime = mProgramManager.getStartTime();
        if (Log.INCLUDE) {
            Log.d(TAG, "requestDateChange | checkTime :  " + new Date(requestedTime) + ", startTime : " + new Date(startTime));
        }
        if (requestedTime < startTime) {
            requestedTime = startTime;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "requestDateChange | requestedTime   : " + new Date(requestedTime) + ", mLastRequestedDate" + new Date(mProgramManager.getFromUtcMillis()));
        }
        long shiftTime = requestedTime - mProgramManager.getFromUtcMillis();
        mTimelineAnimation = /*shiftTime < (2 * TimeUtil.DAY)*/true;
        String dateString = DateListAdapter.getFilterDate(mActivity, requestedTime);
        needToScroll = shiftTime != 0;
        needToFocus = dateString.equalsIgnoreCase(mActivity.getResources().getString(R.string.today)) && shiftTime != 0;
        needToGuideFocus = dateString.equalsIgnoreCase(mActivity.getResources().getString(R.string.today)) && shiftTime == 0;
        mGrid.scrollToPosition(findChannelPosition());
        if (needToFocus) {
            shiftTime = calculatedCurrentScrollTime() - mProgramManager.getFromUtcMillis();
        }
        mProgramManager.shiftTime(shiftTime, true);
        closeMenu();

    }

    private boolean needToFocus;
    private boolean needToGuideFocus;
    private boolean needToScroll;

    void notifyOutKey(final String tag) {
        if (tag.equalsIgnoreCase(GenreListAdapter.CANONICAL_NAME)) {
            changeDateBtn.requestFocus();
        }
    }

    void setCategorySelection(int index) {
        categoryList.setSelectedPosition(index);
    }

    private boolean isFocusFromGrid = true;

    boolean requestGridFocus() {
        Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.sub_guide_table);
        if (fragment != null) {
            return false;
        }
        if (mNoChannelList.getVisibility() == View.VISIBLE) {
            return false;
        }
        isFocusFromGrid = true;
//

        mGrid.requestFocus();
        return true;
    }

    void requestGenreChange(int filterLabel) {
        if (GenreUtil.getCanonical(filterLabel).equalsIgnoreCase(GenreUtil.MULTI_VIEW_CHANNELS)) {
            tune4ChannelApp();
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "requestGenreChange : " + filterLabel);
        }
        if (mLastRequestedLineUp == filterLabel) {
            return;
        }
        mLastRequestedLineUp = filterLabel;
        if (mProgramTableFadeOutAnimator.isStarted()) {
            // When requestGenreChange is called repeatedly in short time, we keep the fade-out
            // state for mTableFadeAnimDuration from now. Without it, we'll see blinks.
            mHandler.removeMessages(MSG_PROGRAM_TABLE_FADE_IN_ANIM);
            mHandler.sendEmptyMessageDelayed(
                    MSG_PROGRAM_TABLE_FADE_IN_ANIM, mTableFadeAnimDuration);
            return;
        }
        if (mHandler.hasMessages(MSG_PROGRAM_TABLE_FADE_IN_ANIM)) {
            mProgramManager.resetChannelListWithGenre(mLastRequestedLineUp);
            mHandler.removeMessages(MSG_PROGRAM_TABLE_FADE_IN_ANIM);
            mHandler.sendEmptyMessageDelayed(
                    MSG_PROGRAM_TABLE_FADE_IN_ANIM, mTableFadeAnimDuration);
            return;
        }
        if (mProgramTableFadeInAnimator.isStarted()) {
            mProgramTableFadeInAnimator.cancel();
        }
        mProgramTableFadeOutAnimator.start();
    }

    /**
     * Returns the scroll offset of the time line row in pixels.
     */
    int getTimelineRowScrollOffset() {
        return mTimelineRow.getScrollOffset();
    }

    /**
     * Returns the program grid view that hold all component views.
     */
    ProgramGrid getProgramGrid() {
        return mGrid;
    }

    /**
     * Returns if Accessibility is enabled.
     */
    boolean isAccessibilityEnabled() {
        return mAccessibilityManager.isEnabled();
    }

    /**
     * Gets {@link VerticalGridView} for "genre select" side panel.
     */

    /**
     * Returns the program manager the program guide is using to provide program information.
     */
    ProgramManager getProgramManager() {
        return mProgramManager;
    }

    private void updateGuidePosition() {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
//                if (mGrid.getSelectedPosition() == findChannelPosition()) {
//                    return;
//                }
                mGrid.scrollToPosition(findChannelPosition());
                RecyclerView.ViewHolder row = mGrid.findViewHolderForAdapterPosition(findChannelPosition());
                if (row != null) {
                    requestCurrentProgramFocus(row.itemView);
                }

            }
        });
        // Align EPG at vertical center, if EPG table height is less than the screen size.
//        Resources res = mActivity.getResources();
//        int screenHeight = mContainer.getHeight();
//        if (screenHeight <= 0) {
//            // mContainer is not initialized yet.
//            return;
//        }
//        int startPadding = /*res.getDimensionPixelOffset(R.dimen.program_guide_table_margin_start)*/0;
//        int topPadding = /*res.getDimensionPixelOffset(R.dimen.program_guide_table_margin_top)*/0;
//        int bottomPadding = res.getDimensionPixelOffset(R.dimen.program_guide_table_margin_bottom);
//        int tableHeight =
//                res.getDimensionPixelOffset(R.dimen.program_guide_table_header_row_height)
//                        + mRowHeight * mGrid.getAdapter().getItemCount()
//                        + topPadding
//                        + bottomPadding;
//        if (tableHeight > screenHeight) {
//            // EPG height is longer that the screen height.
//            mTable.setPaddingRelative(startPadding, topPadding, 0, 0);
//            LayoutParams layoutParams = mTable.getLayoutParams();
//            layoutParams.height = LayoutParams.WRAP_CONTENT;
//            mTable.setLayoutParams(layoutParams);
//        } else {
//            mTable.setPaddingRelative(startPadding, topPadding, 0, bottomPadding);
//            LayoutParams layoutParams = mTable.getLayoutParams();
//            layoutParams.height = tableHeight;
//            mTable.setLayoutParams(layoutParams);
//        }
    }

    private Animator createAnimator(int tableAnimResId) {
        List<Animator> animatorList = new ArrayList<>();

        Animator tableAnimator = AnimatorInflater.loadAnimator(mActivity, tableAnimResId);
        tableAnimator.setTarget(mTable);
        tableAnimator.addListener(new HardwareLayerAnimatorListenerAdapter(mTable));
        animatorList.add(tableAnimator);

        AnimatorSet set = new AnimatorSet();
        set.playTogether(animatorList);
        return set;
    }

    private void startFull() {
        mPartialToFullAnimator.start();
    }

    private void startPartial() {
        mFullToPartialAnimator.start();
    }

    private void startCurrentTimeIndicator(long initialDelay) {
        stopCurrentTimeIndicator();
        mHandler.postDelayed(mUpdateTimeIndicator, initialDelay);
    }

    private void stopCurrentTimeIndicator() {
        mHandler.removeCallbacks(mUpdateTimeIndicator);
    }

    private void positionCurrentTimeIndicator() {
        int offset =
                GuideUtils.convertMillisToPixel(mStartUtcTime, JasmineEpgApplication.SystemClock().currentTimeMillis())
                        - mTimelineRow.getScrollOffset();
        if (offset < 0) {
            mCurrentTimeIndicator.setVisibility(View.GONE);
        } else {
            if (mCurrentTimeIndicatorWidth == 0) {
                mCurrentTimeIndicator.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                mCurrentTimeIndicatorWidth = mCurrentTimeIndicator.getMeasuredWidth();
            }
            mCurrentTimeIndicator.setPaddingRelative(
                    offset - mCurrentTimeIndicatorWidth / 2, 0, 0, 0);
            mCurrentTimeIndicator.setVisibility(View.VISIBLE);
        }
    }

    private void resetTimelineScroll() {
        if (mProgramManager.getFromUtcMillis() != mStartUtcTime) {
            boolean timelineAnimation = mTimelineAnimation;
            mTimelineAnimation = false;
            // mProgramManagerListener.onTimeRangeUpdated() will be called by shiftTime().
            long initialTime = calculatedInitialTime() + MIN_DURATION_FROM_START_TIME_TO_CURRENT_TIME;
            mProgramManager.shiftTime(initialTime - mProgramManager.getFromUtcMillis(), false);
            mTimelineAnimation = timelineAnimation;
        }
    }

    private void onHorizontalScrolled(int dx) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onHorizontalScrolled(dx=" + dx + ")");
        }
        positionCurrentTimeIndicator();
        for (int i = 0, n = mGrid.getChildCount(); i < n; ++i) {
            mGrid.getChildAt(i).findViewById(R.id.row).scrollBy(dx, 0);
        }
    }

    private void resetRowSelection() {
        mSelectedRow = null;
        mIsDuringResetRowSelection = true;
//        mGrid.setSelectedPosition(
//                Math.max(mProgramManager.getChannelIndex(mChannelTuner.getCurrentChannel()), 0));
//        mGrid.resetFocusState();
//        mGrid.onItemSelectionReset();
        mIsDuringResetRowSelection = false;
    }

    private void selectRow(View row) {
        if (row == null || row == mSelectedRow) {
            return;
        }
        if (mSelectedRow == null
                || mGrid.getChildAdapterPosition(mSelectedRow) == RecyclerView.NO_POSITION) {
            final ProgramRow programRow = row.findViewById(R.id.row);
            programRow.post(programRow::focusCurrentProgram);
        } else {
            animateRowChange(mSelectedRow, row);
        }
        mSelectedRow = row;
    }

    public View focusLeftView(View view) {
        if (!isFocusFromGrid && changeDateBtn.isFocusable()) {
            return changeDateBtn;
        } else {
            return categoryList;
        }
    }

    private void makeKeepFocusToCurrentProgram(View row) {
        final ProgramRow programRow = row.findViewById(R.id.row);
        programRow.post(programRow::makeKeepFocusToCurrentProgram);
    }

    private void requestCurrentProgramFocus(View row) {
        final ProgramRow programRow = row.findViewById(R.id.row);
        programRow.post(programRow::forceFocusCurrentProgram);
    }

    private void animateRowChange(View outRow, View inRow) {
    }

    @Override
    public void onAccessibilityStateChanged(boolean enabled) {
//        mAutoHideScheduler.onAccessibilityStateChanged(enabled);
    }

    private class GlobalFocusChangeListener
            implements ViewTreeObserver.OnGlobalFocusChangeListener {
        private static final int UNKNOWN = 0;
        private static final int SIDE_PANEL = 1;
        private static final int PROGRAM_TABLE = 2;
        private static final int CHANNEL_COLUMN = 3;
        private static final int GENRE_TABLE = 4;

        @Override
        public void onGlobalFocusChanged(View oldFocus, View newFocus) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onGlobalFocusChanged " + oldFocus + " -> " + newFocus);
            }
            if (!isActive()) {
                return;
            }
            int fromLocation = getLocation(oldFocus);
            int toLocation = getLocation(newFocus);
//            if (fromLocation == SIDE_PANEL && toLocation == PROGRAM_TABLE) {
//                startFull();
//            } else if (fromLocation == PROGRAM_TABLE && toLocation == SIDE_PANEL) {
//                startPartial();
//            } else if (fromLocation == CHANNEL_COLUMN && toLocation == PROGRAM_TABLE) {
//                startFull();
//            } else if (fromLocation == PROGRAM_TABLE && toLocation == CHANNEL_COLUMN) {
//                startPartial();
//            }

            if (toLocation == PROGRAM_TABLE || toLocation == CHANNEL_COLUMN) {
                backKeyGuideText.setVisibility(View.VISIBLE);
                categoryList.setAlpha(0.5f);

                arrowUp.setVisibility(View.INVISIBLE);
                arrowDown.setVisibility(View.INVISIBLE);
                setVisibleGuideText();
            } else if (toLocation == GENRE_TABLE) {
                categoryList.setAlpha(1f);
                backKeyGuideText.setVisibility(View.GONE);

                int position = categoryList.getSelectedPosition();
                int upVisibility = position < 2 ? View.INVISIBLE : View.VISIBLE;
                int downVisibility = position > categoryList.getAdapter().getItemCount() - 4 ? View.INVISIBLE : View.VISIBLE;
                arrowUp.setVisibility(upVisibility);
                arrowDown.setVisibility(downVisibility);
                setVisibleGuideText();
            } else {
                categoryList.setAlpha(0.5f);
                backKeyGuideText.setVisibility(View.GONE);

                arrowUp.setVisibility(View.INVISIBLE);
                arrowDown.setVisibility(View.INVISIBLE);
                setVisibleGuideText();
            }

        }

        private int getLocation(View view) {
            if (view == null) {
                return UNKNOWN;
            }
            for (Object obj = view; obj instanceof View; obj = ((View) obj).getParent()) {
                if (obj == mGrid) {
                    if (view instanceof ProgramItemView || view instanceof ProgramAudioItemView) {
                        return PROGRAM_TABLE;
                    } else {
                        return CHANNEL_COLUMN;
                    }
                } else if (obj == categoryList) {
                    return GENRE_TABLE;
                } else if (obj == mSubGuideTable) {
                    return CHANNEL_COLUMN;
                }
            }
            return UNKNOWN;
        }
    }

    public interface ProgramGuideInteractor {
        void hideForTune(Channel channel);

        void tuneChannel(Channel channel, Rect rect);
    }

    private static class ProgramGuideHandler extends WeakHandler<ProgramGuide> {
        ProgramGuideHandler(ProgramGuide ref) {
            super(ref);
        }

        @Override
        public void handleMessage(Message msg, @NonNull ProgramGuide programGuide) {
            if (msg.what == MSG_PROGRAM_TABLE_FADE_IN_ANIM) {
                programGuide.mProgramTableFadeInAnimator.start();
            }
        }
    }

    @Override
    public void onPerforated(RectF rectF, PerforatedView.PerforateType type) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPerforated : " + type);
        }
        if (mTvViewInteractor == null) {
            return;
        }
        if (!isActive()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onPerforated : !isActive() true so return");
            }
            return;
        }
        if (type == PerforatedView.PerforateType.PIP) {
            Rect performRect = new Rect((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
            requestResize(performRect, mTvViewInteractor);
        } else {
            Rect rect = new Rect();
            mActivity.getWindowManager().getDefaultDisplay().getRectSize(rect);
            requestResize(rect, mTvViewInteractor);
        }
    }

    @Override
    public void onReminderWatch(Reminder reminder) {

    }

    @Override
    public void onReminderConflict(Program program, Reminder conflictedReminder) {
    }

    @Override
    public void onReminderListChange(List<Reminder> list, Reminder targetProgram) {
        Intent intent = new Intent(ReminderAlertManager.BOOK_EVENT_CHANGE);
        intent.putExtra(ReminderAlertManager.KEY_PROGRAM_ID, targetProgram.getProgramId());
        LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);
    }

    private void tuneByViewChanged(List<Channel> filteredList) {

        if (!withHideTune) {
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "call tuneByViewChanged");
            for (Channel channel : filteredList) {
                Log.d(TAG, "tuneByViewChanged | filteredList " +
                        "" + channel.getDisplayName());
            }
        }
        Channel selectedChannel = null;
        if (filteredList != null && !filteredList.isEmpty()) {
            Channel currentChannel = findCurrentChannel();
            if (!filteredList.contains(currentChannel)) {
                Channel guideChannel = mChannelDataManager.getGuideChannel();
                if (!filteredList.contains(guideChannel)) {
                    Channel channel = filteredList.get(0);
                    selectedChannel = channel;
                } else {
                    selectedChannel = guideChannel;
                }
            }
        }
        if (selectedChannel == null) {
            return;
        }

        requestTuneChannel(selectedChannel, new Handler());
    }

    private void updateGuideText(long fromUtcTime) {
        String dateString = DateListAdapter.getFilterDate(mActivity, fromUtcTime);
        mTimeLineDate.setText(dateString);
        changeDateBtn.setText(dateString);
    }

    private class ProgramManagerListener extends ProgramManager.ListenerAdapter {

        @Override
        public void onGenresUpdated(int selectedGenreId) {
            super.onGenresUpdated(selectedGenreId);

            int viewType = mProgramManager.getLastViewType();
            if (Log.INCLUDE) {
                Log.d(TAG, "onGenresUpdated : " + selectedGenreId + ", viewType : " + viewType);
            }
            switch (viewType) {
                case VIEW_TYPE_FAV_LIST:
                case VIEW_TYPE_AUDIO:
                case VIEW_TYPE_CHANNEL_LIST: {
                    List<Channel> channels = mProgramManager.getFilteredChannels();
                    if (channels != null && !channels.isEmpty()) {
                        mTable.setVisibility(View.VISIBLE);
                        mNoChannelList.setVisibility(View.GONE);
                    } else {
                        mTable.setVisibility(View.GONE);
                        mNoChannelList.setVisibility(View.VISIBLE);
                        mNoChannelListTitle.setText(getChannelListName(mProgramManager.getLastViewType()));
                    }
                }
            }

        }

        @Override
        public void onChannelsUpdated() {
            super.onChannelsUpdated();
        }


        @Override
        public void onTimeRangeUpdated(boolean keepFocus) {
            int scrollOffset =
                    (int) (mWidthPerHour * mProgramManager.getShiftedTime() / HOUR_IN_MILLIS);
            if (Log.INCLUDE) {
                Log.d(
                        TAG,
                        "Horizontal scroll to "
                                + scrollOffset
                                + " pixels ("
                                + mProgramManager.getShiftedTime()
                                + " millis)");
            }
            int durationDiffer = (int) (mProgramManager.getShiftedTime() / TimeUtil.DAY) + 1;
            if (Log.INCLUDE) {
                Log.d(TAG, "onTimeRangeUpdated | durationDiffer : " + durationDiffer);
            }
            if (!keepFocus) {
                needToFocus = false;
            }
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    if (isInitialize) {
                        mTimelineRow.scrollTo(scrollOffset, false);
                        isInitialize = false;
                        mGrid.requestFocus();
                    } else {
                        mTimelineRow.scrollTo(scrollOffset, mTimelineAnimation);
                    }
                }
            });
            mTimelineAnimation = true;
            updateGuideText(mProgramManager.getFromUtcMillis());
        }


        private String getChannelListName(int viewType) {
            switch (viewType) {
                case VIEW_TYPE_FAV_LIST:
                    return mActivity.getString(R.string.no_favorite_channel);
                default:
                    return mActivity.getString(R.string.no_channel_list);
            }
        }


        @Override
        public void onListViewTypeChanged(int viewType, List<Channel> channels) {
            super.onListViewTypeChanged(viewType, channels);
            if (Log.INCLUDE) {
                Log.d(TAG, "onListViewTypeChanged : " + viewType);
                Log.d(TAG, "onListViewTypeChanged channels : " + (channels != null ? channels.size() : "null"));
            }

            switch (viewType) {
                case VIEW_TYPE_FAV_LIST:
                case VIEW_TYPE_AUDIO:
                case VIEW_TYPE_CHANNEL_LIST: {
                    Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.sub_guide_table);
                    if (fragment != null) {
                        boolean isSaveInstance = fragment.isStateSaved();
                        FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                        ft.remove(fragment);
                        if (isSaveInstance) {
                            ft.commitAllowingStateLoss();
                        } else {
                            ft.commit();
                        }
                    }
                    if (channels != null && !channels.isEmpty()) {
                        mTable.setVisibility(View.VISIBLE);
                        mNoChannelList.setVisibility(View.GONE);
                    } else {
                        mTable.setVisibility(View.GONE);
                        mNoChannelList.setVisibility(View.VISIBLE);
                        mNoChannelListTitle.setText(getChannelListName(viewType));
                    }
                    if (viewType == VIEW_TYPE_AUDIO) {
                        stopCurrentTimeIndicator();
                        mTimelineRow.setVisibility(View.INVISIBLE);
                        mCurrentTimeIndicator.setVisibility(View.INVISIBLE);
                    } else {
                        startCurrentTimeIndicator(TIME_INDICATOR_UPDATE_FREQUENCY);
                        mTimelineRow.setVisibility(View.VISIBLE);
                        mCurrentTimeIndicator.setVisibility(View.VISIBLE);
                    }

                }
                break;
                case VIEW_TYPE_POP_LIST: {
                    mProgramManager.setBackChannelForMosaic(findCurrentChannel());

                    mTable.setVisibility(View.INVISIBLE);
                    Fragment fragment = PopularChannelFragment.newInstance(ProgramGuide.this, new ProgramGuideInteractor() {
                        @Override
                        public void hideForTune(Channel channel) {
                            mTvViewInteractor.tuneChannel(channel);
                            hide();
                        }

                        @Override
                        public void tuneChannel(Channel channel, Rect rect) {
                            List<Channel> filteredList = mProgramManager.getFilteredChannels();

                            TVGuideViewModel model = mActivity.getTVGuideViewModel();
                            Channel currentChannel = findCurrentChannel();
                            if (currentChannel.getServiceId() == channel.getServiceId()) {
                                return;
                            }
                            requestTuneChannel(channel, new Handler());
                        }
                    });
                    mNoChannelList.setVisibility(View.GONE);
                    if (fragment != null) {
                        mActivity.getSupportFragmentManager().beginTransaction().replace(R.id.sub_guide_table,
                                fragment).commit();
                    }

                }
                break;
            }
            changeButtonFocusable(viewType, channels.isEmpty());
            changeButtonNextFocus(viewType, channels.isEmpty());
            changeGuideText(viewType, channels.isEmpty());
            tuneByViewChanged(channels);
        }
    }

    private void changeButtonNextFocus(int viewType, boolean empty) {
        changeDateBtn.setOnKeyListener(null);
        switch (viewType) {
            case VIEW_TYPE_FAV_LIST:
            case VIEW_TYPE_CHANNEL_LIST:
                if (empty) {
                    changeDateBtn.setNextFocusRightId(categoryList.getId());
                    changeDateBtn.setNextFocusDownId(categoryList.getId());
                } else {
                    changeDateBtn.setNextFocusRightId(mGrid.getId());
                    changeDateBtn.setNextFocusDownId(categoryList.getId());
                    changeDateBtn.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            switch (keyCode) {
                                case KeyEvent.KEYCODE_DPAD_RIGHT:
                                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                                        isFocusFromGrid = false;
                                        mGrid.scrollToPosition(findChannelPosition());
                                        mGrid.requestFocus();

                                    }
                                    return true;
                            }
                            return false;
                        }
                    });
                }
                break;
            case VIEW_TYPE_POP_LIST:
            case VIEW_TYPE_AUDIO:
                changeDateBtn.setNextFocusRightId(R.id.sub_guide_table);
                changeDateBtn.setNextFocusDownId(R.id.categoryList);
                changeDateBtn.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_DPAD_RIGHT:
                                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                                    Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.sub_guide_table);
                                    fragment.getView().requestFocus();
                                }
                                return true;
                        }
                        return false;
                    }
                });
                break;
        }
    }

    private void changeButtonFocusable(int viewType, boolean empty) {
        changeDateBtn.setOnKeyListener(null);
        switch (viewType) {
            case VIEW_TYPE_FAV_LIST:
            case VIEW_TYPE_CHANNEL_LIST:
                if (empty) {
                    changeDateBtn.setAlpha(0.5f);
                    changeDateBtn.setFocusable(false);
                    changeDateBtn.setFocusableInTouchMode(false);
                } else {
                    changeDateBtn.setAlpha(1f);
                    changeDateBtn.setFocusable(true);
                    changeDateBtn.setFocusableInTouchMode(true);
                }
                break;
            case VIEW_TYPE_POP_LIST:
            case VIEW_TYPE_AUDIO:
                changeDateBtn.setAlpha(0.5f);
                changeDateBtn.setFocusable(false);
                changeDateBtn.setFocusableInTouchMode(false);
                break;
        }
    }

    private void changeGuideText(int viewType, boolean empty) {
        switch (viewType) {
            case VIEW_TYPE_FAV_LIST:
            case VIEW_TYPE_CHANNEL_LIST:
                if (empty) {
                    timeShiftGuideText.setVisibility(View.GONE);
                    pauseKeyGuideText.setVisibility(View.GONE);
                } else {
                    timeShiftGuideText.setVisibility(View.VISIBLE);
                    pauseKeyGuideText.setVisibility(View.VISIBLE);
                }
                break;
            case VIEW_TYPE_POP_LIST:
            case VIEW_TYPE_AUDIO:

                timeShiftGuideText.setVisibility(View.GONE);
                pauseKeyGuideText.setVisibility(View.GONE);
                break;
        }
        setVisibleGuideText();
    }

    private void setVisibleGuideText() {
        boolean backKeyGuideTextVisible = backKeyGuideText.getVisibility() == View.VISIBLE;
        boolean pauseKeyGuideTextVisible = pauseKeyGuideText.getVisibility() == View.VISIBLE;

        if (!backKeyGuideTextVisible && !pauseKeyGuideTextVisible) {
            guideText.setVisibility(View.GONE);
        } else {
            guideText.setVisibility(View.VISIBLE);
        }
    }

    private void tune4ChannelApp() {
        ExternalApplicationManager.getInstance().launchApp(mActivity, ExternalApplicationManager.APP_4CHANNEL);
    }

    public interface OnProgramGuideListener {
        void onProgramGuideShow();

        void onProgramGuideDismiss();
    }

    public interface ChannelPropertyChangeListener {
        void onChannelChanged(Channel channel);
    }
}
