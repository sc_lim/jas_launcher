/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.def.RentalType;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonManager2;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.presenter.VodSelectPackagePresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.FocusHighlightHelper;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class PackageVodSelectDialogFragment extends SafeDismissDialogFragment implements View.OnKeyListener {
    public static final String CLASS_NAME = PackageVodSelectDialogFragment.class.getName();
    private static final String TAG = PackageVodSelectDialogFragment.class.getSimpleName();

    private static final String KEY_PACKAGE_LIST = "KEY_PACKAGE_LIST";

    private final int VISIBLE_COUNT = 5;

    private ImageIndicator leftIndicator;
    private ImageIndicator rightIndicator;
    private HorizontalGridView packageGrid;

    private OnClickPackageListener onClickPackageListener;

    public PackageVodSelectDialogFragment() {

    }

    public static PackageVodSelectDialogFragment newInstance(ArrayList<Product> packageList) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_PACKAGE_LIST, packageList);

        PackageVodSelectDialogFragment fragment = new PackageVodSelectDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnClickPackageListener(OnClickPackageListener onClickPackageListener) {
        this.onClickPackageListener = onClickPackageListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_package_vod_select, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        TextView cancel = view.findViewById(R.id.cancel);
        leftIndicator = view.findViewById(R.id.leftIndicator);
        rightIndicator = view.findViewById(R.id.rightIndicator);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        initVodList(view);
    }

    private void initVodList(View view) {
        ArrayList<Product> packageList = getPackageList();
        if (Log.INCLUDE) {
            Log.d(TAG, "initVodList, packageList : " + packageList);
        }
        if (packageList == null) {
            return;
        }

        TextView title = view.findViewById(R.id.title);
        TextView value = view.findViewById(R.id.value);
        TextView unit = view.findViewById(R.id.unit);
        packageGrid = getGridView(view, packageList.size());

        leftIndicator.initIndicator(packageList.size(), VISIBLE_COUNT);
        rightIndicator.initIndicator(packageList.size(), VISIBLE_COUNT);

        VodSelectPackagePresenter vodSelectPackagePresenter = new VodSelectPackagePresenter();
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(vodSelectPackagePresenter);
        objectAdapter.addAll(0, packageList);
        VodPackageItemBridgeAdapter indicatedItemAdapter = new VodPackageItemBridgeAdapter(objectAdapter, title, value, unit, packageGrid, this);
        FocusHighlightHelper.setupBrowseItemFocusHighlight(indicatedItemAdapter, 1, false);
        packageGrid.setAdapter(indicatedItemAdapter);

        packageGrid.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int focus = packageGrid.getChildPosition(recyclerView.getFocusedChild());
                updateIndicator(focus);
            }
        });
    }

    private ArrayList<Product> getPackageList() {
        ArrayList<Product> packageList = getArguments().getParcelableArrayList(KEY_PACKAGE_LIST);
        Collections.sort(packageList, new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return (o1.getPrice() - o2.getPrice()) > 0 ? 1 : -1;
            }
        });

        return packageList;
    }

    private HorizontalGridView getGridView(View view, int size) {
        HorizontalGridView packageGrid = view.findViewById(R.id.package_grid);

        if (size == 0) {
            packageGrid.setEnabled(false);
        }

        int gap = (int) view.getResources().getDimension(R.dimen.package_watch_presenter_spacing);
        int dimen = (int) view.getResources().getDimension(R.dimen.package_presenter_width) + gap;
        int paddingSide = packageGrid.getPaddingLeft() + packageGrid.getPaddingRight();
        int width = size < VISIBLE_COUNT ? (size * dimen - gap) : (VISIBLE_COUNT * dimen - gap);
        width += paddingSide;

        ViewGroup.LayoutParams params = packageGrid.getLayoutParams();
        params.width = width;
        packageGrid.setLayoutParams(params);
        packageGrid.setHorizontalSpacing(gap);

        return packageGrid;
    }

    private void updateIndicator(int focusedIndex) {
        leftIndicator.updateArrow(focusedIndex);
        rightIndicator.updateArrow(focusedIndex);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        onClickPackageListener = null;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        VodPackageItemBridgeAdapter adapter = (VodPackageItemBridgeAdapter) packageGrid.getAdapter();
        int lastIndex = adapter.getItemCount() - 1;
        int index = packageGrid.getChildPosition(packageGrid.getFocusedChild());

        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                Product product = (Product) adapter.getAdapter().get(index);
                onClickPackageListener.onClickPackage(product);
                return true;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (index == 0) {
                    packageGrid.scrollToPosition(lastIndex);
                    updateIndicator(lastIndex);
                    return true;
                }

                return false;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (index == lastIndex) {
                    packageGrid.scrollToPosition(0);
                    updateIndicator(0);
                    return true;
                }

                return false;
        }

        return false;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private static class ProductInformUpdater implements Runnable {
        private TextView title;
        private TextView value;
        private TextView unit;

        private Product mProduct;

        public ProductInformUpdater(TextView title, TextView value, TextView unit) {
            this.title = title;
            this.value = value;
            this.unit = unit;
        }

        public void setProduct(Product mProduct) {
            this.mProduct = mProduct;
        }

        @Override
        public void run() {
            Context mContext = value.getContext();
            title.setText(mProduct.getProductName());
            if (mProduct.getPrice() > 0) {
                if (mProduct.isPurchased()) {
                    RentalType rentalType = mProduct.getRentalType();
                    if (rentalType == RentalType.BUY) {
                        String buy = mContext.getString(R.string.unlimited);
                        value.setText(buy);
                    } else if (rentalType == RentalType.SUBSCRIBE) {
                        String subscribe = mContext.getString(R.string.vod_subscribing);
                        value.setText(subscribe);
                    } else if (rentalType == RentalType.RENT) {
                        ProductButtonManager2.WatchButtonInfo mWatchButtonInfo = ProductButtonManager2.getRentWatchInfo(mProduct.getPeriod());
                        String left = mWatchButtonInfo.getValue() + " " + mContext.getString(mWatchButtonInfo.getUnit()) + " " + mContext.getString(R.string.vod_watch_left);
                        value.setText(left);
                    }
                    unit.setText("");
                } else {
                    String thb = mContext.getString(R.string.thb);
                    value.setText(StringUtil.getFormattedPrice(mProduct.getPrice()));
                    unit.setText(thb);
                }
            } else {
                String free = mContext.getString(R.string.vod_free);
                value.setText(free);
                unit.setText("");
            }
        }
    }

    public class VodPackageItemBridgeAdapter extends ItemBridgeAdapter {
        private final float ALIGNMENT = 90.0f;

        private HorizontalGridView mHorizontalGridView;
        private View.OnKeyListener onKeyListener;

        private Handler mHandler = new Handler(Looper.getMainLooper());
        private ProductInformUpdater mProductInformUpdater;

        public VodPackageItemBridgeAdapter(ObjectAdapter adapter,
                                           TextView title, TextView value, TextView unit, HorizontalGridView mHorizontalGridView,
                                           View.OnKeyListener onKeyListener) {
            super(adapter);

            this.mHorizontalGridView = mHorizontalGridView;
            this.onKeyListener = onKeyListener;
            this.mProductInformUpdater = new ProductInformUpdater(title, value, unit);

            initAlignment();
        }

        private void initAlignment() {
            if (mHorizontalGridView == null) {
                return;
            }

            mHorizontalGridView.setWindowAlignmentOffsetPercent(ALIGNMENT);
        }

        @Override
        protected void onBind(ViewHolder viewHolder) {
            super.onBind(viewHolder);

            View view = viewHolder.itemView;
            RelativeLayout layout = view.findViewById(R.id.layout);
            Product product = (Product) viewHolder.mItem;

            viewHolder.getFocusChangeListener().setChainedListener(
                    new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            mProductInformUpdater.setProduct(product);
                            mHandler.post(mProductInformUpdater);
                        }
                    });

            viewHolder.getViewHolder().view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    if (onKeyListener != null) {
                        return onKeyListener.onKey(v, keyCode, event);
                    }

                    return false;
                }
            });
        }

        @Override
        protected void onUnbind(ViewHolder viewHolder) {
            super.onUnbind(viewHolder);

            View view = viewHolder.itemView;
            viewHolder.getViewHolder().view.setOnKeyListener(null);
            viewHolder.getFocusChangeListener().setChainedListener(null);
        }
    }

    public interface OnClickPackageListener {
        void onClickPackage(Product product);
    }
}
