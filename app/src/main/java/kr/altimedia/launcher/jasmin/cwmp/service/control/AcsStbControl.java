/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service.control;

import android.content.Context;
import android.os.AsyncTask;

import com.altimedia.cwmplibrary.common.CWMPMultiObject;
import com.altimedia.cwmplibrary.common.ModelObject;
import com.altimedia.cwmplibrary.common.method.GetParameterListener;
import com.altimedia.cwmplibrary.common.method.SetParameterListener;
import com.altimedia.cwmplibrary.ttbb.object.InternetGatewayDevice;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.X_TTBB_STBService;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.X_TTBB_STBServices;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.x_ttbb_stbservice.STBProfile;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.x_ttbb_stbservice.STBProfiles;
import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import java.util.List;

import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPController;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.system.settings.data.SubtitleBroadcastLanguage;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;

import static kr.altimedia.launcher.jasmin.system.settings.SettingControl.SUBTITLE_FONT_LARGE;
import static kr.altimedia.launcher.jasmin.system.settings.SettingControl.SUBTITLE_FONT_SMALL;

/**
 * AcsStbControl
 */
public class AcsStbControl {
    private final String CLASS_NAME = AcsStbControl.class.getName();
    private final String TAG = AcsStbControl.class.getSimpleName();

    private InternetGatewayDevice root;
    private Context context;

    public AcsStbControl(){
    }

    public void dispose() {
        root = null;
        context = null;
    }

    public void init(Context context) {
        Log.d(TAG ,"init");

        this.context = context;
    }

    public void start(InternetGatewayDevice root){
        if (Log.INCLUDE) {
            Log.d(TAG, "start");
        }
        this.root = root;

        if(this.root == null){
            if (Log.INCLUDE) {
                Log.d(TAG, "start: root is NOT available");
            }
            return;
        }

        for (final ModelObject object1 : root.getChilds()) {
//            Log.d(TAG, "start: ModelObject >> "+ object1.getFullName());
            if (object1 instanceof CWMPMultiObject && object1 instanceof X_TTBB_STBServices) {
                X_TTBB_STBService x_ttbb_stbService = ((X_TTBB_STBServices) object1).getX_TTBB_STBService(1);
                if (x_ttbb_stbService == null) {
                    x_ttbb_stbService = ((X_TTBB_STBServices) object1).addX_TTBB_STBService(new X_TTBB_STBService(1));
                }

                for (final ModelObject object2 : x_ttbb_stbService.getChilds()) {
//                    Log.d(TAG, "start: ModelObject of stbProfiles >> "+ object2.getFullName());
                    if (object2 instanceof CWMPMultiObject && object2 instanceof STBProfiles) {
                        STBProfile stbProfile = ((STBProfiles) object2).getSTBProfile(1);
                        if (stbProfile == null) {
                            stbProfile = ((STBProfiles) object2).addSTBProfile(new STBProfile(1));
                        }

                        stbProfile.stbControl.ChangeChannel.addSetListener(new SetChangeChannelListener(stbProfile));
                        // not supported
//                        stbProfile.stbControl.AspectRatioConversion.addSetListener(new SetAspectRatioConversionListener());
//                        stbProfile.stbControl.SPDIFOnOff.addSetListener(new SetSPDIFOnOffListener());
//                        stbProfile.stbControl.AudioSet.addSetListener(new SetAudioSetListener());

                        stbProfile.stbControl.adult.AgeLimit.addGetListener(new GetAgeLimitListener(stbProfile));
                        stbProfile.stbControl.adult.AgeLimit.addSetListener(new SetAgeLimitListener());
                        stbProfile.stbControl.adult.ChannelLimit.addGetListener(new GetChannelLimitListener(stbProfile));
                        stbProfile.stbControl.adult.ChannelLimit.addSetListener(new SetChannelLimitListener());
                        // not supported
//                        stbProfile.stbControl.adult.AgeLimit.addSetListener(new SetAdultMenuLimitListener());
//                        stbProfile.stbControl.adult.AgeLimit.addSetListener(new SetTimeLimitListener());

                        stbProfile.stbControl.channel.FavoriteChannel.addGetListener(new GetFavoriteChannelListener(stbProfile));
                        stbProfile.stbControl.channel.FavoriteChannel.addSetListener(new SetFavoriteChannelListener());
                        // not supported
//                        stbProfile.stbControl.channel.HideChannel.addSetListener(new SetHideChannelListener());

                        // not supported
//                        stbProfile.stbControl.system.AutoModeChange.addSetListener(new SetAutoModeChangeListener());
//                        stbProfile.stbControl.system.AlarmMsg.addSetListener(new SetAlarmMsgListener());

                        // not supported
//                        stbProfile.stbControl.audio.Commentary.addSetListener(new SetCommentaryListener());
//                        stbProfile.stbControl.audio.AudioLanguage.addSetListener(new SetAudioLanguageListener());

                        stbProfile.stbControl.visual.Subtitle.addGetListener(new GetSubtitleListener(stbProfile));
                        stbProfile.stbControl.visual.Subtitle.addSetListener(new SetSubtitleListener());
                        stbProfile.stbControl.visual.FontSize.addGetListener(new GetFontSizeListener(stbProfile));
                        stbProfile.stbControl.visual.FontSize.addSetListener(new SetFontSizeListener());
                        stbProfile.stbControl.visual.VisualLanguage.addGetListener(new GetVisualLanguageListener(stbProfile));
                        stbProfile.stbControl.visual.VisualLanguage.addSetListener(new SetVisualLanguageListener());

                        stbProfile.serviceInfo.CurrentChannel.addGetListener(new GetCurrentChannelListener(stbProfile));
                        stbProfile.serviceInfo.MultiCastAddress.addGetListener(new GetMultiCastAddressListener(stbProfile));

                        if (Log.INCLUDE) {
                            Log.d(TAG, "start: stb control listeners added");
                        }
                        return;
                    }
                }
            }
        }
    }

    private String getChannelList(List<String> serviceIdList) {
        String chNumList = "";
        try {
            if (serviceIdList != null && serviceIdList.size() > 0) {
                TvSingletons mTvSingletons = TvSingletons.getSingletons(context);
                ChannelDataManager channelDataManager = mTvSingletons.getChannelDataManager();
                for (int i=0; i<serviceIdList.size(); i++) {
                    try {
                        Channel channel = channelDataManager.getChannelByServiceId(serviceIdList.get(i));
                        if (channel != null) {
                            chNumList += channel.getDisplayNumber();
                            if (i<serviceIdList.size()-1) {
                                chNumList += ",";
                            }
                        }
                    }catch (Exception e){
                    }
                }
            }
        }catch (Exception e){
        }
        Log.d(TAG, "getChannelList: chNumList="+chNumList);
        return chNumList;
    }

    public void setRegistration(String serverAddress, boolean authorized, String description){
        if (Log.INCLUDE) {
            Log.d(TAG, "setRegistration: serverAddress="+serverAddress+", authorized="+authorized+", description="+description);
        }
        try {
            for (final ModelObject object1 : root.getChilds()) {
//            Log.d(TAG, "start: ModelObject >> "+ object1.getFullName());
                if (object1 instanceof CWMPMultiObject && object1 instanceof X_TTBB_STBServices) {
                    X_TTBB_STBService x_ttbb_stbService = ((X_TTBB_STBServices) object1).getX_TTBB_STBService(1);
                    if (x_ttbb_stbService == null) {
                        x_ttbb_stbService = ((X_TTBB_STBServices) object1).addX_TTBB_STBService(new X_TTBB_STBService(1));
                    }

                    for (final ModelObject object2 : x_ttbb_stbService.getChilds()) {
//                    Log.d(TAG, "start: ModelObject of stbProfiles >> "+ object2.getFullName());
                        if (object2 instanceof CWMPMultiObject && object2 instanceof STBProfiles) {
                            STBProfile stbProfile = ((STBProfiles) object2).getSTBProfile(1);
                            if (stbProfile == null) {
                                stbProfile = ((STBProfiles) object2).addSTBProfile(new STBProfile(1));
                            }
                            if(serverAddress != null && !serverAddress.isEmpty()) {
                                stbProfile.registration.MBSServer.setValue(serverAddress);
                            }
                            if(authorized) {
                                stbProfile.registration.Status.setValue(1);
                                stbProfile.registration.StatusDesc.setValue("");
                            }else{
                                stbProfile.registration.Status.setValue(0);
                                if(description != null && !description.isEmpty()) {
                                    stbProfile.registration.StatusDesc.setValue(description);
                                }
                            }
                            return;
                        }
                    }
                }
            }
        }catch (Exception e){
        }
    }

    public void setCurrentChannel(String channelInfo){
        if (Log.INCLUDE) {
            Log.d(TAG, "setCurrentChannel: channelInfo=" + channelInfo);
        }
        try {
            if(channelInfo == null || channelInfo.isEmpty()){
                return;
            }

            for (final ModelObject object1 : root.getChilds()) {
//            Log.d(TAG, "start: ModelObject >> "+ object1.getFullName());
                if (object1 instanceof CWMPMultiObject && object1 instanceof X_TTBB_STBServices) {
                    X_TTBB_STBService x_ttbb_stbService = ((X_TTBB_STBServices) object1).getX_TTBB_STBService(1);
                    if (x_ttbb_stbService == null) {
                        x_ttbb_stbService = ((X_TTBB_STBServices) object1).addX_TTBB_STBService(new X_TTBB_STBService(1));
                    }

                    for (final ModelObject object2 : x_ttbb_stbService.getChilds()) {
//                    Log.d(TAG, "start: ModelObject of stbProfiles >> "+ object2.getFullName());
                        if (object2 instanceof CWMPMultiObject && object2 instanceof STBProfiles) {
                            STBProfile stbProfile = ((STBProfiles) object2).getSTBProfile(1);
                            if (stbProfile == null) {
                                stbProfile = ((STBProfiles) object2).addSTBProfile(new STBProfile(1));
                            }
                            stbProfile.serviceInfo.CurrentChannel.setValue(channelInfo);
                            stbProfile.serviceInfo.MultiCastAddress.setValue("Not Supported");
                            return;
                        }
                    }
                }
            }
        }catch (Exception e){
        }
    }

    private class SetChangeChannelListener implements SetParameterListener{
        private STBProfile stbProfile;
        SetChangeChannelListener(STBProfile stbProfile){
            this.stbProfile = stbProfile;
        }

        @Override
        public void onSet(Object value) {
            if (Log.INCLUDE) {
                Log.d(TAG, "ChangeChannelListener: onSet: value=" + value);
            }
            if(value instanceof String) {
                try{
                    if(stbProfile != null){
                        stbProfile.stbControl.ChangeChannel.setValue(new String("0"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    String chno = (String) value;
                    CWMPController.getInstance().getCWMPEventListener().onSetChangeChannel(chno);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    private class SetAspectRatioConversionListener implements SetParameterListener{
//        @Override
//        public void onSet(Object value) {
//            if (Log.INCLUDE) {
//                Log.d(TAG, "AspectRatioConversion: onSet: value=" + value);
//            }
//            if(value instanceof Integer) {
//                Integer command = (Integer) value;
////                if(command == 1) { // 원본비율유지
////                } else if(command == 2) { // Full Screen
////                } else if(command == 3) { // Crop
////                }
//                try {
//                    CWMPController.getInstance().getCWMPEventListener().onSetAspectRatioConversion(command);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

//    private class SetSPDIFOnOffListener implements SetParameterListener{
//        @Override
//        public void onSet(Object value) {
//            Log.d(TAG, "SPDIFOnOff: onSet: value=" + value);
//            if(value instanceof Integer) {
//                Integer command = (Integer) value;
//                if(command == 1) { // SPDIF(Sony/Philips Digital Interface) 활성 여부 Enabled
//                } else if(command == 2) { // Disabled
//                }
//            }
//        }
//    }

//    private class SetAudioSetListener implements SetParameterListener{
//        @Override
//        public void onSet(Object value) {
//            Log.d(TAG, "AudioSet: onSet: value=" + value);
//            if(value instanceof Integer) {
//                Integer command = (Integer) value;
//                if(command == 0) { // STB의 오디오설정 OFF
//                } else if(command == 1) { // PCM
//                } else if(command == 2) { // AC3
//                }
//            }
//        }
//    }

    private class GetAgeLimitListener implements GetParameterListener{
        private STBProfile stbProfile;
        GetAgeLimitListener(STBProfile stbProfile){
            this.stbProfile = stbProfile;
        }
        @Override
        public void onGet() {
            try {
                int rating = AccountManager.getInstance().getCurrentRating();
                if (Log.INCLUDE) {
                    Log.d(TAG, "AgeLimit: onGet: rating=" + rating);
                }
                stbProfile.stbControl.adult.AgeLimit.setValue(rating);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class SetAgeLimitListener implements SetParameterListener{
        @Override
        public void onSet(Object value) {
            if (Log.INCLUDE) {
                Log.d(TAG, "AgeLimit: onSet: value=" + value);
            }
            if(value instanceof Integer) {
                Integer command = (Integer) value;
//                if(command == 0) {
//                } else if(command == 19) {
//                } else if(command == 15) {
//                } else if(command == 12) {
//                } else if(command == 7) {
//                }

                try {
                    CWMPController.getInstance().getCWMPEventListener().onSetAgeLimit(command);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    private class SetAdultMenuLimitListener implements SetParameterListener{
//        @Override
//        public void onSet(Object value) {
//            Log.d(TAG, "AdultMenuLimit: onSet: value=" + value);
//            if(value instanceof Integer) {
//                Integer command = (Integer) value;
//                if(command == 0) { // 성인메뉴숨김
//                } else if(command == 1) { // 성인메뉴표시
//                }
//            }
//        }
//    }

//    private class SetTimeLimitListener implements SetParameterListener{
//        @Override
//        public void onSet(Object value) {
//            Log.d(TAG, "TimeLimit: onSet: value=" + value);
//            if(value instanceof Integer) {
//                Integer command = (Integer) value;
//                if(command == 0) { // 시청시간제한 해제
//                } else if(command == 1) { // 설정
//                }
//            }
//        }
//    }

    private class GetChannelLimitListener implements GetParameterListener{
        private STBProfile stbProfile;
        private boolean isCompleted = false;
        private String channelList = "";
;        GetChannelLimitListener(STBProfile stbProfile){
            this.stbProfile = stbProfile;
        }
        @Override
        public void onGet() {
            try {
                if (Log.INCLUDE) {
                    Log.d(TAG, "ChannelLimit: onGet");
                }
                isCompleted = false;
                channelList = "";
                UserDataManager userDataManager = new UserDataManager();
                AsyncTask dataTask = userDataManager.getBlockedChannelList(
                        AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(),
                        new MbsDataProvider<String, List<String>>() {
                            @Override
                            public void needLoading(boolean loading) {
                            }

                            @Override
                            public void onFailed(int key) {
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "getBlockedChannelList: onFailed, key:" + key);
                                }
                                isCompleted = true;
                            }

                            @Override
                            public void onSuccess(String id, List<String> result) {
                                if (Log.INCLUDE) {
                                    if(result != null) {
                                        Log.d(TAG, "getBlockedChannelList: list.size:" + result.size());
                                    }
                                }
                                if(result != null) {
                                    channelList = getChannelList(result);
                                }
                                isCompleted = true;
                            }

                            @Override
                            public void onError(String errorCode, String message) {
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "getBlockedChannelList: onError: errorCode=" + errorCode + ", message=" + message);
                                }
                                isCompleted = true;
                            }
                        });

                try{
                    long waitTime = 0;
                    while(waitTime < 6) {
                        if(isCompleted){
                            break;
                        }
                        Thread.sleep(500);
                        waitTime++;
                    }
                }catch (Exception e){
                }
                if(dataTask != null){
                    dataTask.cancel(true);
                    dataTask = null;
                }
                stbProfile.stbControl.adult.ChannelLimit.setValue(channelList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class SetChannelLimitListener implements SetParameterListener{
        @Override
        public void onSet(Object value) {
            if (Log.INCLUDE) {
                Log.d(TAG, "ChannelLimit: onSet: value=" + value);
            }
            if(value instanceof String) {
                // 최대 120개까지 등록이 가능하며 콤마(,)로 구분
                String chlist = (String) value;
                try {
                    CWMPController.getInstance().getCWMPEventListener().onSetChannelLimit(chlist);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class GetFavoriteChannelListener implements GetParameterListener{
        private STBProfile stbProfile;
        private boolean isCompleted = false;
        private String channelList = "";
        GetFavoriteChannelListener(STBProfile stbProfile){
            this.stbProfile = stbProfile;
        }
        @Override
        public void onGet() {
            try {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FavoriteChannel: onGet");
                }
                isCompleted = false;
                channelList = "";
                UserDataManager userDataManager = new UserDataManager();
                AsyncTask dataTask = userDataManager.getFavoriteChannelList(
                        AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(),
                        new MbsDataProvider<String, List<String>>() {
                            @Override
                            public void needLoading(boolean loading) {
                            }

                            @Override
                            public void onFailed(int key) {
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "getFavoriteChannelList: onFailed, key:" + key);
                                }
                                isCompleted = true;
                            }

                            @Override
                            public void onSuccess(String id, List<String> result) {
                                if (Log.INCLUDE) {
                                    if(result != null) {
                                        Log.d(TAG, "getFavoriteChannelList: list.size:" + result.size());
                                    }
                                }
                                if(result != null) {
                                    channelList = getChannelList(result);
                                }
                                isCompleted = true;
                            }

                            @Override
                            public void onError(String errorCode, String message) {
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "getFavoriteChannelList: onError: errorCode=" + errorCode + ", message=" + message);
                                }
                                isCompleted = true;
                            }
                        });

                try{
                    long waitTime = 0;
                    while(waitTime < 6) {
                        if(isCompleted){
                            break;
                        }
                        Thread.sleep(500);
                        waitTime++;
                    }
                }catch (Exception e){
                }
                if(dataTask != null){
                    dataTask.cancel(true);
                    dataTask = null;
                }
                stbProfile.stbControl.channel.FavoriteChannel.setValue(channelList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class SetFavoriteChannelListener implements SetParameterListener{
        @Override
        public void onSet(Object value) {
            if (Log.INCLUDE) {
                Log.d(TAG, "FavoriteChannel: onSet: value=" + value);
            }
            if(value instanceof String) {
                // 최대 70개까지 등록이 가능하며 콤마(,)로 구분
                String chlist = (String) value;
                try {
                    CWMPController.getInstance().getCWMPEventListener().onSetFavoriteChannel(chlist);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    private class SetHideChannelListener implements SetParameterListener{
//        @Override
//        public void onSet(Object value) {
//            Log.d(TAG, "HideChannel: onSet: value=" + value);
//            if(value instanceof String) {
//                // 최대 250개까지 등록이 가능하며 콤마(,)로 구분
//            }
//        }
//    }

//    private class SetAutoModeChangeListener implements SetParameterListener{
//        @Override
//        public void onSet(Object value) {
//            Log.d(TAG, "AutoModeChange: onSet: value=" + value);
//            if(value instanceof Integer) {
//                Integer command = (Integer) value;
//                if(command == 0) { // 자동대기모드 OFF
//                } else if(command == 1) { // ON
//                }
//            }
//        }
//    }

//    private class SetAlarmMsgListener implements SetParameterListener{
//        @Override
//        public void onSet(Object value) {
//            Log.d(TAG, "AlarmMsg: onSet: value=" + value);
//            if(value instanceof Integer) {
//                Integer command = (Integer) value;
//                if(command == 0) { // 알람메세지 OFF
//                } else if(command == 1) { // ON
//                }
//            }
//        }
//    }

//    private class SetCommentaryListener implements SetParameterListener{
//        @Override
//        public void onSet(Object value) {
//            Log.d(TAG, "Commentary: onSet: value=" + value);
//            if(value instanceof Integer) {
//                Integer command = (Integer) value;
//                if(command == 0) { // 화면해설 OFF
//                } else if(command == 1) { // ON
//                }
//            }
//        }
//    }

//    private class SetAudioLanguageListener implements SetParameterListener{
//        @Override
//        public void onSet(Object value) {
//            Log.d(TAG, "AudioLanguage: onSet: value=" + value);
//            if(value instanceof String) {
//                // th:태국어, us:영어
//            }
//        }
//    }

    private class GetSubtitleListener implements GetParameterListener{
        private STBProfile stbProfile;
        GetSubtitleListener(STBProfile stbProfile){
            this.stbProfile = stbProfile;
        }
        @Override
        public void onGet() {
            if (Log.INCLUDE) {
                Log.d(TAG, "Subtitle: onGet");
            }
            try {
                int value = 0;
                SettingControl settingControl = SettingControl.getInstance();
                boolean enabled = settingControl.isSubtitleEnable();
                if(enabled){
                    value = 1;
                }
                stbProfile.stbControl.visual.Subtitle.setValue(value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class SetSubtitleListener implements SetParameterListener{
        @Override
        public void onSet(Object value) {
            if (Log.INCLUDE) {
                Log.d(TAG, "Subtitle: onSet: value=" + value);
            }
            if(value instanceof Integer) {
                Integer command = (Integer) value;
//                if(command == 0) { // 자막 OFF
//                } else if(command == 1) { // ON
//                }

                try {
                    CWMPController.getInstance().getCWMPEventListener().onSetSubtitle(command);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class GetFontSizeListener implements GetParameterListener{
        private STBProfile stbProfile;
        GetFontSizeListener(STBProfile stbProfile){
            this.stbProfile = stbProfile;
        }
        @Override
        public void onGet() {
            if (Log.INCLUDE) {
                Log.d(TAG, "FontSize: onGet");
            }
            try {
                int value = 0;
                SettingControl settingControl = SettingControl.getInstance();
                String size = StringUtils.nullToEmpty(settingControl.getSubtitleLanguageFontSize());
                if (SUBTITLE_FONT_SMALL.equalsIgnoreCase(size)) {
                    value = 0;
                } else if (SUBTITLE_FONT_LARGE.equalsIgnoreCase(size)) {
                    value = 2;
                } else {
                    value = 1;
                }
                stbProfile.stbControl.visual.FontSize.setValue(value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class SetFontSizeListener implements SetParameterListener{
        @Override
        public void onSet(Object value) {
            if (Log.INCLUDE) {
                Log.d(TAG, "FontSize: onSet: value=" + value);
            }
            if(value instanceof Integer) {
                Integer command = (Integer) value;
//                if(command == 0) { // 글자크기 작게
//                } else if(command == 1) { // 중간
//                } else if(command == 2) { // 크게
//                }

                try {
                    CWMPController.getInstance().getCWMPEventListener().onSetFontSize(command);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class GetVisualLanguageListener implements GetParameterListener{
        private STBProfile stbProfile;
        GetVisualLanguageListener(STBProfile stbProfile){
            this.stbProfile = stbProfile;
        }
        @Override
        public void onGet() {
            if (Log.INCLUDE) {
                Log.d(TAG, "VisualLanguage: onGet");
            }
            try {
                String value = SubtitleBroadcastLanguage.THAI.getKey();
                SettingControl settingControl = SettingControl.getInstance();
                String lang = StringUtils.nullToEmpty(settingControl.getSubtitleLanguageCode());
                if (SubtitleBroadcastLanguage.ENG.getKey().equalsIgnoreCase(lang)) {
                    value = "us";
                }
                stbProfile.stbControl.visual.VisualLanguage.setValue(value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private class SetVisualLanguageListener implements SetParameterListener{
        @Override
        public void onSet(Object value) {
            if (Log.INCLUDE) {
                Log.d(TAG, "VisualLanguage: onSet: value=" + value);
            }
            if(value instanceof String) {
                // df:기본, us:영어, th:태국
                String language = (String) value;
                try {
                    CWMPController.getInstance().getCWMPEventListener().onSetVisualLanguage(language);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class GetCurrentChannelListener implements GetParameterListener {
        private STBProfile stbProfile;
        GetCurrentChannelListener(STBProfile stbProfile){
            this.stbProfile = stbProfile;
        }

        @Override
        public void onGet() {
            if (Log.INCLUDE) {
                Log.d(TAG, "CurrentChannel: onGet");
            }
            String watchingInfo = "";
            try {
                Channel channel = CWMPServiceProvider.getInstance().getWatchingChannel();
                if(channel != null) {
                    watchingInfo = channel.getDisplayNumber() + " " + channel.getDisplayName();

//                    long currentTimeMillis = JasmineEpgApplication.SystemClock().currentTimeMillis();
//                    ProgramDataManager programDataManager = TvSingletons.getSingletons(context).getProgramDataManager();
//                    List<Program> programs = programDataManager.getPrograms(channel.getId(), currentTimeMillis);
//                    for (Program program : programs) {
//                        program.getStartTimeUtcMillis();
//                        long startTime = program.getStartTimeUtcMillis();
//                        long endTime = program.getEndTimeUtcMillis();
//                        if (startTime <= currentTimeMillis && currentTimeMillis <= endTime) {
//                            watchingInfo = channel.getDisplayNumber() + " " + program.getTitle();
//                            break;
//                        }
//                    }
                }else{
                    String vod = CWMPServiceProvider.getInstance().getWatchingVod();
                    if(vod != null){
                        watchingInfo = "VOD "+ vod;
                    }
                }
                stbProfile.serviceInfo.CurrentChannel.setValue(watchingInfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetMultiCastAddressListener implements GetParameterListener{
        private STBProfile stbProfile;
        GetMultiCastAddressListener(STBProfile stbProfile){
            this.stbProfile = stbProfile;
        }
        @Override
        public void onGet() {
            try {
                stbProfile.serviceInfo.MultiCastAddress.setValue("Not Supported");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}

