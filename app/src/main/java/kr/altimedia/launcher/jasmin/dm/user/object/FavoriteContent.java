/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.tvmodule.util.StringUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.def.EventFlagDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.StringArrayDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class FavoriteContent implements Parcelable {

    @SerializedName("containerType")
    @Expose
    private String containerType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("genre")
    @Expose
    @JsonAdapter(StringArrayDeserializer.class)
    private List<String> genre;
    @SerializedName("isSeries")
    @Expose
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isSeries;
    @SerializedName("seriesAssetId")
    @Expose
    private String seriesAssetId;
    @SerializedName("isPackage")
    @Expose
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isPackage;
    @SerializedName("packageId")
    @Expose
    private String packageId;
    @SerializedName("rating")
    @Expose
    private int rating;
    @SerializedName("eventFlag")
    @Expose
    @JsonAdapter(EventFlagDeserializer.class)
    private List<String> eventFlag;
    @SerializedName("contentGroupId")
    @Expose
    private String contentGroupId;
    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;
    @SerializedName("videoUrl")
    @Expose
    private String videoUrl;
    @SerializedName("posterFileName")
    @Expose
    private String posterFileName;

    public final static Creator<FavoriteContent> CREATOR = new Creator<FavoriteContent>() {

        @SuppressWarnings({
                "unchecked"
        })
        public FavoriteContent createFromParcel(Parcel in) {
            return new FavoriteContent(in);
        }

        public FavoriteContent[] newArray(int size) {
            return (new FavoriteContent[size]);
        }

    };

    protected FavoriteContent(Parcel in) {
        this.contentGroupId = ((String) in.readValue((String.class.getClassLoader())));
        this.eventFlag = new ArrayList<>();
        in.readList(this.eventFlag, String.class.getClassLoader());
        this.genre = new ArrayList<>();
        in.readList(this.genre, String.class.getClassLoader());
        this.isSeries = in.readByte() != 0;
        this.seriesAssetId = ((String) in.readValue((String.class.getClassLoader())));
        this.isPackage = in.readByte() != 0;
        this.packageId = ((String) in.readValue((String.class.getClassLoader())));
        this.posterFileName = ((String) in.readValue((String.class.getClassLoader())));
        this.rating = ((int) in.readValue((Integer.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.containerType = ((String) in.readValue((String.class.getClassLoader())));
        this.videoUrl = ((String) in.readValue((String.class.getClassLoader())));
    }

    public String getContentGroupId() {
        return contentGroupId;
    }

    public List<String> getEventFlag() {
        return eventFlag;
    }

    public List<String> getGenre() {
        if (genre == null) {
            return new ArrayList<>();
        }
        return genre;
    }

    public boolean isSeries() {
        return isSeries;
    }

    public String getSeriesAssetId() {
        return seriesAssetId;
    }

    public boolean isPackage() {
        return isPackage;
    }

    public String getPackageId() {
        return packageId;
    }

    public String getPosterSourceUrl(int width, int height) {
        String posterType = StringUtils.nullToEmpty(containerType).isEmpty() ? "" : containerType;
        if (posterFileName != null) {
            return posterFileName + "_" + width + "x" + height + "." + posterType;
        }
        return "";
    }

    public int getRating() {
        return rating;
    }

    public String getTitle() {
        return title;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(contentGroupId);
        dest.writeList(eventFlag);
        dest.writeList(genre);
        dest.writeByte((byte) (isSeries ? 1 : 0));
        dest.writeValue(seriesAssetId);
        dest.writeByte((byte) (isPackage ? 1 : 0));
        dest.writeValue(packageId);
        dest.writeValue(posterFileName);
        dest.writeValue(rating);
        dest.writeValue(title);
        dest.writeValue(containerType);
        dest.writeValue(videoUrl);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "FavoriteContent{" +
                ", contentGroupId='" + contentGroupId + '\'' +
                ", eventFlag=" + eventFlag +
                ", genre=" + genre +
                ", isSeries=" + isSeries +
                ", seriesAssetId='" + seriesAssetId + '\'' +
                ", isPackage=" + isPackage +
                ", packageId='" + packageId + '\'' +
                ", posterFileName='" + posterFileName + '\'' +
                ", rating=" + rating +
                ", title='" + title + '\'' +
                ", containerType='" + containerType + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", orderNumber='" + orderNumber + '\'' +
                '}';
    }
}