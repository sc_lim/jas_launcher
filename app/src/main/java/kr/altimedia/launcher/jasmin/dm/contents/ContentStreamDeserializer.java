/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents;

import android.graphics.Rect;

import com.altimedia.tvmodule.util.StringUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.contents.obj.IntroInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PreviewInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamType;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ThumbnailInfo;

/**
 * Created by mc.kim on 01,06,2020
 */
public class ContentStreamDeserializer implements JsonDeserializer<List<StreamInfo>> {
    private final Rect mDefaultThumbnailRect = new Rect(0, 0, 218, 122);
    private final Rect mTrailerDefaultThumbnailRect = new Rect(0, 0, 320, 180);
    private final String TAG = ContentStreamDeserializer.class.getSimpleName();
    private final String KEY_MAIN = "main";
    private final String KEY_TRAILER = "trailer";
    public static final String TITLE_MAIN_PREVIEW = "Main Preview";
    private final String TIME_FORMAT = "HH:mm:ss";

    private final long SECOND = 1 * 1000;
    private final long MINUTE = 60 * SECOND;
    private final long HOUR = 60 * MINUTE;


    private String getPosterUrl(JsonObject object, boolean trailer) {
        final String posterFileName = "thumbnailFileName";
        final String posterFormat = "containerType";

        String fileName = getString(posterFileName, object);
        String fileFormat = getString(posterFormat, object);
        if (trailer) {
            return fileName + "_" + mTrailerDefaultThumbnailRect.width() + "x" + mTrailerDefaultThumbnailRect.height() + "." + fileFormat;
        } else {
            return fileName + "_" + mDefaultThumbnailRect.width() + "x" + mDefaultThumbnailRect.height() + "." + fileFormat;
        }
    }

    @Override
    public List<StreamInfo> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        List<StreamInfo> streamInfoList = new ArrayList<>();
        JsonObject streamObject = json.getAsJsonObject();
        if (streamObject.has(KEY_MAIN) && streamObject.get(KEY_MAIN) != null && !streamObject.get(KEY_MAIN).isJsonNull()) {
            JsonObject mainObject = streamObject.getAsJsonObject(KEY_MAIN);
            if (mainObject != null) {
                StreamInfo maiInfo = new StreamInfo(StreamType.Main,
                        getStringArray("videoUrl", mainObject),
                        getThumbnailUrl(mainObject), StreamType.Main.getName(), generateIntroInfo(mainObject), getPosterUrl(mainObject, true));
                streamInfoList.add(maiInfo);
                if (mainObject.has("freePreviewStartPosition") &&
                        !mainObject.get("freePreviewStartPosition").isJsonNull() &&
                        getAsLong("freePreviewDuration", mainObject) != -1) {
                    String startPosition = getString("freePreviewStartPosition", mainObject);
                    long previewDuration = getAsLong("freePreviewDuration", mainObject) * 1000;
                    long previewStartTime = convertTime(startPosition);
                    PreviewInfo previewInfo = new PreviewInfo(previewStartTime, previewDuration);
                    StreamInfo previewStreamInfo = new StreamInfo(StreamType.Preview,
                            getStringArray("videoUrl", mainObject),
                            getThumbnailUrl(mainObject), TITLE_MAIN_PREVIEW, previewInfo, getPosterUrl(mainObject, true));
                    streamInfoList.add(previewStreamInfo);
                }
            }
        }
        if (streamObject.has(KEY_TRAILER) && streamObject.get(KEY_TRAILER) != null && !streamObject.get(KEY_TRAILER).isJsonNull()) {
            JsonArray trailerArray = streamObject.getAsJsonArray(KEY_TRAILER);
            if (trailerArray == null) {
                return streamInfoList;
            }
            for (int i = 0; i < trailerArray.size(); i++) {
                if (trailerArray.get(i).isJsonNull()) {
                    continue;
                }
                JsonObject trailerObject = trailerArray.get(i).getAsJsonObject();
                if (trailerObject == null) {
                    continue;
                }
                StreamInfo trailerInfo = new StreamInfo(StreamType.Trailer,
                        getStringArray("previewUrl", trailerObject),
                        getThumbnailUrl(trailerObject),
                        getString("title", trailerObject), getPosterUrl(trailerObject, true));
                streamInfoList.add(0, trailerInfo);
            }
        }
        return streamInfoList;
    }

    private IntroInfo generateIntroInfo(JsonObject mainObject) {
        if (mainObject.has("skipIntroStartPosition")
                && mainObject.has("skipIntroEndPosition")) {
            String introStartTime = getString("skipIntroStartPosition", mainObject);
            String introEndTime = getString("skipIntroEndPosition", mainObject);
            IntroInfo introInfo = new IntroInfo(convertTime(introStartTime), convertTime(introEndTime));
            return introInfo;
        } else {
            return null;
        }
    }

    private long convertTime(String timeString) {
        if(StringUtils.nullToEmpty(timeString).isEmpty()){
            return 0;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(TIME_FORMAT);
        try {
            Date date = dateFormat.parse(timeString);
            long time = (date.getSeconds() * SECOND) + (date.getMinutes() * MINUTE) + (date.getHours() * HOUR);
            return time;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private ThumbnailInfo getThumbnailUrl(JsonObject object) {
        if (StringUtils.nullToEmpty(getString("thumbnailFolder", object)).isEmpty() ||
                StringUtils.nullToEmpty(getString("webvttFileName", object)).isEmpty()) {
            return null;
        }
        String thumbNailFullUrl = getString("thumbnailFolder", object) +
                getString("webvttFileName", object) +
                "_" + mDefaultThumbnailRect.width() + "x" + mDefaultThumbnailRect.height() + ".vtt";
        String thumbNailFolder = getString("thumbnailFolder", object);
        ThumbnailInfo info = new ThumbnailInfo(thumbNailFolder, thumbNailFullUrl);
        return info;
    }

    private long getAsLong(String key, JsonObject object) {
        if (object.has(key) && object.get(key) != null && !object.get(key).isJsonNull()) {
            try{
                return object.get(key).getAsLong();
            }catch (Exception e){
                e.printStackTrace();
                return -1;
            }

        }
        return -1;
    }

    private String getString(String key, JsonObject object) {
        if (object.has(key) && object.get(key) != null && !object.get(key).isJsonNull()) {
            return object.get(key).getAsString();
        }
        return "";
    }

    private String[] getStringArray(String key, JsonObject object) {
        if (object.has(key) && object.get(key) != null && !object.get(key).isJsonNull()) {
            String[] resultArray;
            if (object.get(key).isJsonArray()) {
                JsonArray jsonArray = object.get(key).getAsJsonArray();
                resultArray = new String[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    resultArray[i] = jsonArray.get(i).getAsString();
                }
            } else {
                resultArray = new String[1];
                resultArray[0] = object.get(key).getAsString();
            }
            return resultArray;
        }
        return null;
    }
}
