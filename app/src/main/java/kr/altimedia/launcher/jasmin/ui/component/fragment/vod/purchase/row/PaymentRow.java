/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.row;

import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.PaymentItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;

public class PaymentRow extends Row {
    private final PaymentItemBridgeAdapter paymentItemBridgeAdapter;

    public PaymentRow(PaymentItemBridgeAdapter paymentItemBridgeAdapter) {
        this.paymentItemBridgeAdapter = paymentItemBridgeAdapter;
    }

    public PaymentItemBridgeAdapter getPaymentItemBridgeAdapter() {
        return paymentItemBridgeAdapter;
    }
}
