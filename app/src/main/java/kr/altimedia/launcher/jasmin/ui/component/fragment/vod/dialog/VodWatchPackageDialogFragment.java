/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.presenter.VodWatchPackagePresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.FocusHighlightHelper;

public class VodWatchPackageDialogFragment extends SafeDismissDialogFragment implements View.OnKeyListener {
    public static final String CLASS_NAME = VodWatchPackageDialogFragment.class.getName();
    private static final String TAG = VodWatchPackageDialogFragment.class.getSimpleName();

    private static final String KEY_PACKAGE_TITLE = "PACKAGE_TITLE";
    private static final String KEY_CONTENT_LIST = "CONTENT_LIST";

    private final int VISIBLE_COUNT = 5;

    private OnSelectWatchPackageListener onSelectWatchPackageListener;

    private HorizontalGridView packageGrid;
    private ImageIndicator leftIndicator;
    private ImageIndicator rightIndicator;
    public VodWatchPackageDialogFragment(){}
    public static VodWatchPackageDialogFragment newInstance(String packageTitle, ArrayList<Content> packageList) {
        Bundle args = new Bundle();
        args.putString(KEY_PACKAGE_TITLE, packageTitle);
        args.putParcelableArrayList(KEY_CONTENT_LIST, packageList);

        VodWatchPackageDialogFragment fragment = new VodWatchPackageDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnSelectWatchPackageListener(OnSelectWatchPackageListener onSelectWatchPackageListener) {
        this.onSelectWatchPackageListener = onSelectWatchPackageListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_watch_package, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        String packageTitle = getArguments().getString(KEY_PACKAGE_TITLE);
        ((TextView) view.findViewById(R.id.title)).setText(packageTitle);
        leftIndicator = view.findViewById(R.id.leftIndicator);
        rightIndicator = view.findViewById(R.id.rightIndicator);

        view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        initVodList(view);
    }

    private void initVodList(View view) {
        ArrayList<Content> packageList = getArguments().getParcelableArrayList(KEY_CONTENT_LIST);

        packageGrid = getGridView(view, packageList.size());
        leftIndicator.initIndicator(packageList.size(), VISIBLE_COUNT);
        rightIndicator.initIndicator(packageList.size(), VISIBLE_COUNT);

        VodWatchPackagePresenter vodWatchPackagePresenter = new VodWatchPackagePresenter();
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(vodWatchPackagePresenter);
        objectAdapter.addAll(0, packageList);
        VodPackageItemBridgeAdapter indicatedItemAdapter = new VodPackageItemBridgeAdapter(objectAdapter, packageGrid, this);
        FocusHighlightHelper.setupBrowseItemFocusHighlight(indicatedItemAdapter, 1, false);
        packageGrid.setAdapter(indicatedItemAdapter);

        packageGrid.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int focus = packageGrid.getChildPosition(recyclerView.getFocusedChild());
                updateIndicator(focus);
            }
        });
    }

    private HorizontalGridView getGridView(View view, int size) {
        HorizontalGridView packageGrid = view.findViewById(R.id.package_grid);

        if (size == 0) {
            packageGrid.setEnabled(false);
        }

        int gap = (int) view.getResources().getDimension(R.dimen.package_watch_presenter_spacing);
        int dimen = (int) view.getResources().getDimension(R.dimen.package_presenter_width) + gap;
        int paddingSide = packageGrid.getPaddingLeft() + packageGrid.getPaddingRight();
        int width = size < VISIBLE_COUNT ? (size * dimen - gap) : (VISIBLE_COUNT * dimen - gap);
        width += paddingSide;

        ViewGroup.LayoutParams params = packageGrid.getLayoutParams();
        params.width = width;
        packageGrid.setLayoutParams(params);
        packageGrid.setHorizontalSpacing(gap);

        return packageGrid;
    }

    private void updateIndicator(int focusedIndex) {
        leftIndicator.updateArrow(focusedIndex);
        rightIndicator.updateArrow(focusedIndex);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        onSelectWatchPackageListener = null;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        VodPackageItemBridgeAdapter adapter = (VodPackageItemBridgeAdapter) packageGrid.getAdapter();
        int lastIndex = adapter.getItemCount() - 1;
        int index = packageGrid.getChildPosition(packageGrid.getFocusedChild());

        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                Content content = (Content) adapter.getAdapter().get(index);
                onSelectWatchPackageListener.onWatchPackage(content);
                return true;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (index == 0) {
                    packageGrid.scrollToPosition(lastIndex);
                    updateIndicator(lastIndex);
                    return true;
                }

                return false;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (index == lastIndex) {
                    packageGrid.scrollToPosition(0);
                    updateIndicator(0);
                    return true;
                }

                return false;
        }

        return false;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private class VodPackageItemBridgeAdapter extends ItemBridgeAdapter {
        private final float ALIGNMENT = 90.0f;
        private HorizontalGridView mHorizontalGridView;

        private View.OnKeyListener onKeyListener;

        public VodPackageItemBridgeAdapter(ObjectAdapter adapter, HorizontalGridView mHorizontalGridView, View.OnKeyListener onKeyListener) {
            super(adapter);

            this.mHorizontalGridView = mHorizontalGridView;
            this.onKeyListener = onKeyListener;

            initAlignment();
        }

        private void initAlignment() {
            if (mHorizontalGridView == null) {
                return;
            }

            mHorizontalGridView.setWindowAlignmentOffsetPercent(ALIGNMENT);
        }

        @Override
        protected void onBind(ViewHolder viewHolder) {
            super.onBind(viewHolder);

            viewHolder.getViewHolder().view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    if (onKeyListener != null) {
                        return onKeyListener.onKey(v, keyCode, event);
                    }

                    return false;
                }
            });
        }

        @Override
        protected void onUnbind(ViewHolder viewHolder) {
            super.onUnbind(viewHolder);

            viewHolder.getViewHolder().view.setOnKeyListener(null);
        }
    }

    public interface OnSelectWatchPackageListener {
        void onWatchPackage(Content content);
    }
}
