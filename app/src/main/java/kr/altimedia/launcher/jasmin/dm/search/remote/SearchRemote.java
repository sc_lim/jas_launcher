/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search.remote;


import com.altimedia.util.Log;

import java.io.IOException;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.MbsUtil;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.search.api.SearchApi;
import kr.altimedia.launcher.jasmin.dm.search.obj.SearchData;
import kr.altimedia.launcher.jasmin.dm.search.obj.response.SearchResponse;
import retrofit2.Call;
import retrofit2.Response;

public class SearchRemote {
    private static final String TAG = SearchRemote.class.getSimpleName();

    private SearchApi searchApi;

    public SearchRemote(SearchApi messageApi) {
        searchApi = messageApi;
    }

    public SearchData getTotalSearch(String contentsType,
                                     String sortType,
                                     String searchPosition,
                                     String searchCount,
                                     String searchWord,
                                     String sortFielDs,
                                     String category,
                                     String productID,
                                     String adultYn,
                                     String saID,
                                     String deviceType,
                                     String exposureTime,
                                     String STB_VER,
                                     String wonYn,
                                     String language,
                                     String transactionID, String rating
    ) throws IOException, ResponseException, AuthenticationException {

        Call<SearchResponse> call = searchApi.getTotalSearch(
                contentsType,
                sortType,
                searchPosition,
                searchCount,
                searchWord,
                sortFielDs,
                category,
                productID,
                adultYn,
                saID,
                deviceType,
                exposureTime,
                STB_VER,
                wonYn,
                language,
                transactionID, rating);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call(url) : " + call.request().url().toString());
            Log.d(TAG, "call(body) : " + call.request().body());
        }
        Response<SearchResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response(success) : " + response.isSuccessful());
            Log.d(TAG, "response(message) : " + response.message());
            Log.d(TAG, "response(code) : " + response.code());
            Log.d(TAG, "response(error) : " + response.errorBody());
        }

        // check validation
        MbsUtil.checkValid(response);
        SearchResponse searchResponse = response.body();

        return searchResponse.getSearchData();
    }
}
