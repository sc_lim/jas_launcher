/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.ParentalController;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class MiniGuideChannelListPresenter extends Presenter {
    private MiniGuideDialogFragment mMiniGuideDialogFragment = null;
    private boolean mItemSelected = false;
    private static final String TAG = MiniGuideChannelListPresenter.class.getSimpleName();

    public MiniGuideChannelListPresenter(MiniGuideDialogFragment miniGuideDialogFragment) {
        mMiniGuideDialogFragment = miniGuideDialogFragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_miniepg_channellist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        Channel channel = (Channel) item;
        setSelected(viewHolder.view.isSelected());
        vh.setData(channel, isTunedChannel(channel));
    }

    private boolean isTunedChannel(Channel channel) {
//        Channel tunedChannel = mMiniGuideDialogFragment.getEpgViewModel().getTunedChannel();
//        return (tunedChannel == channel);
        return false;
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.clearData();
        viewHolder.view.setSelected(false);
    }

    public void setSelected(boolean selected) {
        mItemSelected = selected;
    }

    public boolean isItemSelected() {
        return mItemSelected;
    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private ImageView channelThumbnail;
        private ImageView channelIframe;
        private TextView channelNum;
        private TextView channelName;
        private ImageView channelIcon;
        private TextView blockText;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            channelThumbnail = view.findViewById(R.id.channel_thumbnail);
            channelIframe = view.findViewById(R.id.channel_iframe);
            channelNum = view.findViewById(R.id.channel_number);
            channelName = view.findViewById(R.id.channel_name);
            channelIcon = view.findViewById(R.id.channel_icon);
            blockText = view.findViewById(R.id.block_text);
        }

        public void setData(Channel channel, boolean isCurrentChannel) {
            String chNum = StringUtil.getFormattedNumber(channel.getDisplayNumber(), 3);
            channelNum.setText(chNum);
            channelNum.setVisibility(channel.isHiddenChannel() ? View.GONE : View.VISIBLE);
            setChannelIcon(channel);
            channelName.setText(channel.getDisplayName());
            if (Log.INCLUDE) {
                Log.d(TAG, "setData : channel : " + isCurrentChannel + ", channel : " + channel.getDisplayName());
            }
            setChannelThumbnail(channel, isCurrentChannel);
        }

        public void clearData() {
            channelThumbnail.setImageResource(R.drawable.channel_img_iframe);
            channelIframe.setImageResource(R.drawable.channel_img_iframe);
            channelIframe.setVisibility(View.VISIBLE);
            channelNum.setText("");
            channelName.setText("");
            blockText.setText("");
        }

        private void setChannelThumbnail(Channel channel, boolean isCurrentChannel) {
            int stringid = 0;
            if (isCurrentChannel) {
                stringid = R.string.channel_thumb_watching;
            } else if (channel.isLocked()) {
                stringid = R.string.channel_thumb_blocked;
            } else if (channel.isAdultChannel()) {
                stringid = R.string.channel_thumb_adult_blocked;
            } else if (channel.isPaidChannel()) {
                stringid = R.string.channel_thumb_unsubscribed;
            } else if (channel.isAudioChannel()) {
                stringid = R.string.channel_thumb_audio;
            } else if (isProgramRatingBlocked(channel)) {
                stringid = R.string.channel_thumb_blocked_program;
            }

            if (stringid == R.string.channel_thumb_audio) {
                blockText.setText(R.string.empty);
                blockText.setVisibility(View.INVISIBLE);
                channelIframe.setImageResource(R.drawable.audio_ch_img);
                channelIframe.setVisibility(View.VISIBLE);
            } else if (stringid != 0) {
                blockText.setText(stringid);
                blockText.setVisibility(View.VISIBLE);
                channelIframe.setImageResource(R.drawable.channel_img_iframe);
                channelIframe.setVisibility(View.VISIBLE);
            } else {
                blockText.setText(R.string.empty);
                blockText.setVisibility(View.INVISIBLE);
                channelIframe.setVisibility(View.GONE);
                Glide.with(view.getContext())
                        .load(ChannelImageManager.getChannelThumbnailUrl(channel))
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .signature(new ObjectKey(String.valueOf(System.currentTimeMillis() / 60000L)))
                        .placeholder(R.drawable.mini_epg_ch_thumb_default)
                        .error(R.drawable.mini_epg_ch_thumb_default)
                        .into(channelThumbnail);
            }
        }

        private boolean isProgramRatingBlocked(Channel channel) {
            return ParentalController.isProgramRatingBlocked(view.getContext(), channel);
        }

        private static final int[] DUMMY_CHANNEL_THUMB = {
                R.drawable.dummy_channel_img_0,
                R.drawable.dummy_channel_img_1,
                R.drawable.dummy_channel_img_2,
                R.drawable.dummy_channel_img_3,
                R.drawable.dummy_channel_img_4,
                R.drawable.dummy_channel_img_5,
                R.drawable.dummy_channel_img_6,
                R.drawable.dummy_channel_img_7,
        };

        private void setChannelIcon(Channel channel) {
            int icon = 0;
            if (channel.isLocked() || channel.isAdultChannel()) {
                icon = R.drawable.ch_icon_lock;
            } else if (channel.isFavoriteChannel()) {
                icon = R.drawable.ch_icon_fav_f;
            } else if (channel.isPaidChannel()) {
                icon = R.drawable.ch_icon_pay;
            } else if (channel.isAudioChannel()) {
                icon = R.drawable.ch_icon_audio;
            }

            if (icon != 0) {
                channelIcon.setImageResource(icon);
                channelIcon.setVisibility(View.VISIBLE);
//                Drawable drawable = view.getResources().getDrawable(icon);
//                Glide.with(view.getContext())
//                        .load(drawable)
//                        .centerCrop()
//                        .into(channelIcon);
            } else {
                channelIcon.setVisibility(View.GONE);
            }
        }
    }
}
