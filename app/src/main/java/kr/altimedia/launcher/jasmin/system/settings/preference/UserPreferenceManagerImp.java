/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.system.settings.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.altimedia.util.Log;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import kr.altimedia.launcher.jasmin.dm.def.ImmutableListDeserializer;
import kr.altimedia.launcher.jasmin.system.settings.data.ChannelPreviewInfo;
import kr.altimedia.launcher.jasmin.system.settings.data.PipInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.Reminder;

/**
 * Created by mc.kim on 12,02,2020
 */
public class UserPreferenceManagerImp extends SystemUserPreferenceManager {
    private final String TAG = UserPreferenceManagerImp.class.getSimpleName();

    public enum Result {
        Success, fail, invalidPassword, invalidDefaultValue
    }

    public UserPreferenceManagerImp(Context context) {
        super(context);
    }

    public boolean setNotifications(ArrayList<String> notifications) {
        Gson gson = new Gson();
        String json = gson.toJson(notifications, new TypeToken<List<String>>() {
        }.getType());

        if (Log.INCLUDE) {
            Log.d(TAG, "setNotifications, json : " + json);
        }

        return this.writeString(KEY_NOTIFICATIONS, json);
    }

    public List<String> getNotifications() {
        String data = readString(KEY_NOTIFICATIONS);

        if (Log.INCLUDE) {
            Log.d(TAG, "getNotifications, json : " + data);
        }

        Gson gson = new Gson();
        return gson.fromJson(data, new TypeToken<List<String>>() {
        }.getType());
    }

    public boolean getAutoPlay() {
        return this.readBoolean(UserPreferenceManagerImp.KEY_SERIES_AUTO_PLAY);
    }

    public boolean setAutoPlay(boolean flag) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setAutoPlay, flag : " + flag);
        }
        return this.writeBoolean(UserPreferenceManagerImp.KEY_SERIES_AUTO_PLAY, flag);
    }

    public boolean setLastWatchedChannel(String serviceId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setLastWatchedChannel(), serviceId:" + serviceId);
        }
        String data = readString(KEY_LAST_WATCHED_CHANNEL);
        if (data != null && data.equalsIgnoreCase(serviceId)) {
            return false;
        } else {
            return writeString(KEY_LAST_WATCHED_CHANNEL, serviceId);
        }
    }

    public String getLastWatchedChannel() {
        String data = readString(KEY_LAST_WATCHED_CHANNEL);
        if (Log.INCLUDE) {
            Log.d(TAG, "getLastWatchedChannel(), serviceId:" + data);
        }
        return data;
    }

    public boolean setReminderList(List<Reminder> list) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ImmutableList.class, new ImmutableListDeserializer())
                .create();
        String json = gson.toJson(list, new TypeToken<List<Reminder>>() {
        }.getType());

        if (Log.INCLUDE) {
            Log.d(TAG, "setReminderList, json : " + json);
        }

        return this.writeString(KEY_REMINDER_PROGRAM_LIST, json);
    }

    public ArrayList<Reminder> getReminderList() {
        String data = this.readString(KEY_REMINDER_PROGRAM_LIST);

        if (Log.INCLUDE) {
            Log.d(TAG, "getReminderList, json : " + data);
        }

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ImmutableList.class, new ImmutableListDeserializer())
                .create();
        ArrayList<Reminder> list = gson.fromJson(data, new TypeToken<ArrayList<Reminder>>() {
        }.getType());

        if (Log.INCLUDE) {
            Log.d(TAG, "getReminderList() list.size:" + (list != null ? list.size() : null));
            for (int i = 0; list != null && i < list.size(); i++) {
                Log.d(TAG, "getReminderList[" + i + "]:" + list.get(i));
            }
            Log.d(TAG, "getReminderList() --------------------------------------------");
        }

        return list;
    }

    public boolean getSystemUpdateEnable() {
        return this.readBoolean(UserPreferenceManagerImp.KEY_SYSTEM_UPDATE_ENABLE);
    }

    public boolean setSystemUpdateEnable(boolean flag) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setSystemUpdateEnable, flag : " + flag);
        }
        return this.writeBoolean(UserPreferenceManagerImp.KEY_SYSTEM_UPDATE_ENABLE, flag);
    }

    public boolean isAutoRebootEnable() {
        return this.readBoolean(UserPreferenceManagerImp.KEY_AUTO_REBOOT_ENABLE);
    }

    public boolean setAutoRebootEnable(boolean flag) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setAutoRebootEnable, flag : " + flag);
        }
        return this.writeBoolean(UserPreferenceManagerImp.KEY_AUTO_REBOOT_ENABLE, flag);
    }

    public ChannelPreviewInfo getChannelPreviewInfo() {
        String json = readString(KEY_CHANNEL_PREVIEW_MAP);
        return new ChannelPreviewInfo(json);
    }

    public boolean setChannelPreviewInfo(ChannelPreviewInfo channelPreviewInfo) {
        return writeString(KEY_CHANNEL_PREVIEW_MAP, channelPreviewInfo.getDataAsJson());
    }

    public PipInfo getPipInfo() {
        String json = readString(KEY_PIP_INFO);
        return PipInfo.getDataFromJson(json);
    }

    public boolean setPipInfo(PipInfo pipInfo) {
        return writeString(KEY_PIP_INFO, pipInfo.getDataAsJson());
    }

    public boolean reset() {
        try {
            writeBoolean(UserPreferenceManagerImp.KEY_SERIES_AUTO_PLAY, getDefaultBoolean(KEY_SERIES_AUTO_PLAY));

            writeString(KEY_CHANNEL_PREVIEW_MAP, getDefaultString(KEY_CHANNEL_PREVIEW_MAP));

            writeString(KEY_REMINDER_PROGRAM_LIST, getDefaultString(KEY_REMINDER_PROGRAM_LIST));

            writeString(KEY_PIP_INFO, getDefaultString(KEY_PIP_INFO));
            return true;
        } catch (Exception | Error e) {
        }
        return false;
    }

    @Override
    public String readString(String key) {
        String defaultValue = getDefaultString(key);
        return getString(PREF_USER, key, defaultValue);
    }

    @Override
    public boolean readBoolean(String key) {
        boolean defaultValue = getDefaultBoolean(key);
        return getBoolean(PREF_USER, key, defaultValue);
    }

    @Override
    public int readInt(String key) {
        int defaultValue = getDefaultInt(key);
        return getInt(PREF_USER, key, defaultValue);
    }

    @Override
    public long readLong(String key) {
        long defaultValue = getDefaultLong(key);
        return getLong(PREF_USER, key, defaultValue);
    }

    @Override
    public boolean writeString(String key, String value) {
        return putString(PREF_USER, key, value);
    }

    @Override
    public boolean writeBoolean(String key, boolean value) {
        return putBoolean(PREF_USER, key, value);
    }

    @Override
    public Set<String> getStringSet(String key) {
        return getStringSet(PREF_USER, key);
    }

    @Override
    public boolean putStringSet(String key, Set<String> stringSet) {
        return putStringSet(PREF_USER, key, stringSet);
    }

    public boolean addStringSet(String key, String value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "addStringSet  key : " + key + ", value : " + value);
        }
        Set<String> savedSet = getStringSet(key);
        Set<String> cloneSet = new HashSet<>();
        cloneSet.addAll(savedSet);
        if (Log.INCLUDE) {
            Iterator<String> checkSet = cloneSet.iterator();
            while (checkSet.hasNext()) {
                String setValue = checkSet.next();
                Log.d(TAG, "addStringSet  before : " + setValue);
            }
        }

        cloneSet.add(value);

        if (Log.INCLUDE) {
            Iterator<String> checkSet = cloneSet.iterator();
            while (checkSet.hasNext()) {
                String setValue = checkSet.next();
                Log.d(TAG, "addStringSet after : " + setValue);
            }
        }
        return putStringSet(key, cloneSet);
    }

    public boolean removeStringSet(String key, String value) {
        Set<String> savedSet = getStringSet(key);
        savedSet.remove(value);
        return putStringSet(key, savedSet);
    }

    @Override
    public boolean removeFile(String key) {
        return removeFile(PREF_USER, key);
    }

    @Override
    public boolean writeInt(String key, int value) {
        return putInt(PREF_USER, key, value);
    }

    @Override
    public boolean writeLong(String key, long value) {
        return putLong(PREF_USER, key, value);
    }

    private ArrayList<UserPreferenceChangedListener> mCallbackList = new ArrayList<>();

    public void registUserPreferenceChangedListener(UserPreferenceChangedListener userPreferenceChangedListener) {
        if (mCallbackList.contains(userPreferenceChangedListener)) {
            return;
        }
        mCallbackList.add(userPreferenceChangedListener);
    }

    public void unRegistUserPreferenceChangedListener(UserPreferenceChangedListener userPreferenceChangedListener) {
        if (!mCallbackList.contains(userPreferenceChangedListener)) {
            return;
        }
        mCallbackList.remove(userPreferenceChangedListener);
    }

    private void fireUpChangedEvent(String key, Object value) {
        if (mCallbackList.isEmpty()) {
            return;
        }
        for (UserPreferenceChangedListener userPreferenceChangedListener : mCallbackList) {
            userPreferenceChangedListener.notifyUpChanged(key, value);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Map<String, ?> map = sharedPreferences.getAll();
        Object o = map.get(key);
        fireUpChangedEvent(key, o);
    }


    public interface UserPreferenceChangedListener {
        void notifyUpChanged(String key, Object value);
    }
}
