/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search.api;

import kr.altimedia.launcher.jasmin.dm.search.obj.response.SearchResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface SearchApi {
    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("totalSearch")
    Call<SearchResponse> getTotalSearch(
            @Query("contents_type") String contentsType,
            @Query("sortType") String sortType,
            @Query("searchPosition") String searchPosition,
            @Query("searchCount") String searchCount,
            @Query("searchWord") String searchWord,
            @Query("sortFields") String sortFielDs,
            @Query("cate") String category,
            @Query("product_id") String productID,
            @Query("adult_yn") String adultYn,
            @Query("sa_id") String saID,
            @Query("device_Type") String deviceType,
            @Query("exposure_time") String exposureTime,
            @Query("STB_VER") String STB_VER,
            @Query("won_yn") String wonYn,
            @Query("langType") String language,
            @Query("TRANSACTION_ID") String transactionID,
            @Query("rating") String rating
    );
}
