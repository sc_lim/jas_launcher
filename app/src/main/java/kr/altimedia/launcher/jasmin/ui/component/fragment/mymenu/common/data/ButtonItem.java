/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data;

public class ButtonItem {
    public static final int TYPE_BUTTON0 = 0;
    public static final int TYPE_CANCEL = 0;
    public static final int TYPE_CLOSE = 0;

    public static final int TYPE_BUTTON1 = 1;
    public static final int TYPE_OK = 1;
    public static final int TYPE_DELETE = 1;
    public static final int TYPE_BUY = 1;

    public static final int TYPE_BUTTON2 = 2;
    public static final int TYPE_SELECT_ALL = 2;
    public static final int TYPE_WATCH_CHANNEL = 2;

    private int type;
    private String name;

    public ButtonItem(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
