/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOption;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOptionCallback;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOptionProvider;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object.SideOptionCategory;
import kr.altimedia.launcher.jasmin.ui.view.common.CheckTextView;
import kr.altimedia.launcher.jasmin.ui.view.row.SideOptionValueRow;

/**
 * Created by mc.kim on 20,02,2020
 */
public class SideOptionValuePresenter extends Presenter {
    private final String TAG = SideOptionValuePresenter.class.getSimpleName();

    public SideOptionValuePresenter() {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_side_option_value, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBindViewHolder");
        }
        View parentsView = viewHolder.view;
        CheckTextView mCheckTextView = (CheckTextView) parentsView;
        mCheckTextView.setWidthTextView(R.dimen.side_panel_option_presenter_text_width);
        mCheckTextView.setTextGravity(Gravity.LEFT);
        mCheckTextView.setTextLayoutAlignment(RelativeLayout.ALIGN_PARENT_LEFT, R.dimen.side_panel_option_presenter_margin_left, -1, -1, -1);
        mCheckTextView.setCheckIconLayoutAlignment(RelativeLayout.ALIGN_PARENT_RIGHT, -1, -1, R.dimen.side_panel_option_presenter_margin_right, -1);
        mCheckTextView.setMaxLineTextView(1);

        if (item instanceof SideOptionValueRow) {
            SideOptionValueRow optionRow = (SideOptionValueRow) item;
            SideOptionCategory type = optionRow.getOptionType();
            mCheckTextView.setOnClickListener(optionRow);
            mCheckTextView.setOnKeyListener(optionRow);

            if (type.getOptionType() == SidePanelOption.Fav) {
                mCheckTextView.setCheckBoxIcon(R.drawable.selector_fav_icon);
                SidePanelOptionProvider mSidePanelOptionProvider = optionRow.getPanelOptionProvider();
                Object itemObject = mSidePanelOptionProvider.getItem();

                boolean isFavorite = false;
                if (itemObject instanceof Channel) {
                    Channel mChannel = (Channel) itemObject;
                    isFavorite = mChannel.isFavoriteChannel();
                } else if (itemObject instanceof Content) {
                    Content mContent = (Content) itemObject;
                    isFavorite = mContent.isLike();
                }

                mCheckTextView.setSelected(isFavorite);
                if (isFavorite) {
                    mCheckTextView.setText(parentsView.getResources().getString(R.string.removeFav));
                    //stateIcon.setImageResource(selector) is not working
                    mCheckTextView.setSelected(true);
                } else {
                    mCheckTextView.setText(parentsView.getResources().getString(R.string.addFav));
                    //stateIcon.setImageResource(selector) is not working
                    mCheckTextView.setSelected(false);
                }
            } else if (type.getOptionType() == SidePanelOption.LanguageOption) {
                mCheckTextView.setCheckBoxIcon(R.drawable.selector_setting_check_button);
                SideOptionCategory mSideOptionCategory = optionRow.getOptionType().getParentsCategory();
                mCheckTextView.setText(type.getOptionName());
                SidePanelOptionCallback mSidePanelOptionProvider = (SidePanelOptionCallback) optionRow.getPanelOptionProvider();
                if (mSideOptionCategory.getOptionType() == SidePanelOption.SubTitle) {
                    String language = mSidePanelOptionProvider.getCurrentSubtitleLanguage();
                    boolean selected = language.equalsIgnoreCase(type.getOptionName());
                    mCheckTextView.setChecked(selected);
                } else if (mSideOptionCategory.getOptionType() == SidePanelOption.Audio) {
                    String language = mSidePanelOptionProvider.getCurrentAudioLanguage();
                    boolean selected = language.equalsIgnoreCase(type.getOptionName());
                    mCheckTextView.setChecked(selected);
                }
            } else if (type.getOptionType() == SidePanelOption.LanguageStyle) {
                mCheckTextView.setCheckBoxIcon(R.drawable.selector_setting_check_button);
                mCheckTextView.setText(type.getOptionName());
                SidePanelOptionCallback mSidePanelOptionProvider = (SidePanelOptionCallback) optionRow.getPanelOptionProvider();
                String style = mSidePanelOptionProvider.getSubtitleStyle();
                boolean selected = style.equalsIgnoreCase(type.getOptionName());
                mCheckTextView.setChecked(selected);
            }
        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onUnbindViewHolder");
        }
        View parentsView = viewHolder.view;
        parentsView.setOnClickListener(null);
        parentsView.setOnKeyListener(null);
    }
}
