/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.presenter;

import android.content.res.Resources;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.altimedia.util.Log;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewKeyListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.BaseGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.BaseHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.rowView.ContentsRowView;
import kr.altimedia.launcher.jasmin.ui.view.util.FocusHighlightHelper;
import kr.altimedia.launcher.jasmin.ui.view.util.TransitionHelper;

public class ProfileIconListRowPresenter extends RowPresenter {
    private static final String TAG = ProfileIconListRowPresenter.class.getSimpleName();

    protected final int mFocusZoomFactor = 0;
    protected int mContentsResourceId = -1;
    private View historyView;

    public ProfileIconListRowPresenter() {
        this.mContentsResourceId = R.layout.presenter_mymenu_profile_icon_list_row;
    }

    public void setHistoryView(View historyView) {
        this.historyView = historyView;
    }

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {
        ContentsRowView rowView = mContentsResourceId != -1 ?
                new ContentsRowView(parent.getContext(), mContentsResourceId) : new ContentsRowView(parent.getContext());
        return new ViewHolder(rowView, rowView.getGridView(), this);
    }

    @Override
    protected void initializeRowViewHolder(RowPresenter.ViewHolder vh) {
        super.initializeRowViewHolder(vh);
        ViewHolder viewHolder = (ViewHolder) vh;
        viewHolder.mItemBridgeAdapter = new ProfileIconListItemBridgeAdapter(viewHolder);
        viewHolder.setHistoryView(historyView);
        FocusHighlightHelper.setupBrowseItemFocusHighlight(viewHolder.mItemBridgeAdapter, this.mFocusZoomFactor, false);
    }

    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);

        ViewHolder viewHolder = (ViewHolder) holder;
        ListRow rowItem = (ListRow) item;
        viewHolder.setData(rowItem);
    }

    @Override
    protected void onUnbindRowViewHolder(RowPresenter.ViewHolder vh) {
        ViewHolder viewHolder = (ViewHolder) vh;
        viewHolder.layout.setOnFocusSearchListener(null);
        viewHolder.mGridView.setAdapter(null);
        viewHolder.mItemBridgeAdapter.clear();
        viewHolder.setBaseOnItemKeyListener(null);

        super.onUnbindRowViewHolder(viewHolder);
    }

    public static class ViewHolder extends RowPresenter.ViewHolder {
        protected final BrowseFrameLayout layout;
        private LinearLayout contentLayer;
        protected final HorizontalGridView mGridView;
        protected final ImageView mArrowLeft;
        protected final ImageView mArrowRight;
        protected final ProfileIconListRowPresenter mListRowPresenter;
        public ProfileIconListItemBridgeAdapter mItemBridgeAdapter;

        private View historyView;

        public ViewHolder(View rootView, HorizontalGridView gridView, ProfileIconListRowPresenter p) {
            super(rootView);
            this.layout = rootView.findViewById(R.id.layout);
            this.contentLayer = rootView.findViewById(R.id.row_content_layer);
            this.mGridView = gridView;
            this.mArrowLeft = rootView.findViewById(R.id.arrowL);
            this.mArrowRight = rootView.findViewById(R.id.arrowR);
            this.mListRowPresenter = p;

            if (layout != null) {
                layout.setOnFocusSearchListener(new BrowseFrameLayout.OnFocusSearchListener() {
                    @Override
                    public View onFocusSearch(View child, int direction) {
                        if (direction == View.FOCUS_UP) {
                            return historyView;
                        }

                        return null;
                    }
                });
            }
        }

        public void setData(ListRow rowItem) {
            mItemBridgeAdapter.setAdapter(rowItem.getAdapter());
            mGridView.setAdapter(mItemBridgeAdapter);
            mGridView.setContentDescription(rowItem.getContentDescription());

            initGridView();
            initKeyListener();
        }

        protected void initGridView() {
            BaseHeaderPresenter.ViewHolder headerViewHolder = getHeaderViewHolder();
            View headerView = headerViewHolder.view;
            int headerBottom = headerViewHolder.getHeaderViewHeight();

            Resources resources = headerView.getResources();
            int top = headerBottom + resources.getDimensionPixelSize(R.dimen.mymenu_profile_icon_list_header_padding_bottom);
            int left = resources.getDimensionPixelSize(R.dimen.mymenu_profile_icon_list_padding_left);
            int right = resources.getDimensionPixelSize(R.dimen.mymenu_profile_icon_list_padding_right);

            headerView.setPadding(left, headerView.getPaddingTop(), headerView.getPaddingRight(), headerView.getPaddingBottom());
            mGridView.setPadding(left, top, right, 0);
            mGridView.setClipChildren(false);

            int gap = (int) resources.getDimension(R.dimen.mymenu_profile_icon_list_horizontal_space);
            mGridView.setHorizontalSpacing(gap);

            try {
                int totalSize = mItemBridgeAdapter.getItemCount();
                if(totalSize >= 5){
                    if(totalSize > 5){
                        mArrowRight.setVisibility(View.VISIBLE);
                    }
                    totalSize = 5;
                }
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) contentLayer.getLayoutParams();
                layoutParams.width = (int) view.getResources().getDimension(R.dimen.mymenu_profile_width) * totalSize + (int) view.getResources().getDimension(R.dimen.mymenu_profile_icon_list_horizontal_space) * (totalSize - 1);
                contentLayer.setLayoutParams(layoutParams);

            }catch (Exception e){
            }
        }

        protected void initKeyListener() {
            setBaseOnItemKeyListener(new ProfileIconListKeyListener(mGridView));
        }

        public final HorizontalGridView getGridView() {
            return this.mGridView;
        }

        public void setHeaderViewSelected(boolean selected){
            try {
                View headerView = getHeaderViewHolder().view;
                if(headerView.isSelected() != selected) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "setHeaderViewSelected");
                    }
                    headerView.post(new Runnable() {
                        @Override
                        public void run() {
                            headerView.setSelected(selected);
                        }
                    });
                }
            }catch (Exception e){
            }
        }

        public void setHistoryView(View historyView) {
            this.historyView = historyView;
        }

        public void setArrowVisibility(boolean selected){
            if (Log.INCLUDE) {
                Log.d(TAG, "setArrowVisibility: selected=" + selected);
            }
            if(selected){
                try {
                    int totalSize = mGridView.getAdapter().getItemCount();
                    int currPos = mGridView.getSelectedPosition();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "setArrowVisibility: currPos=" + currPos + ", totalSize=" + totalSize);
                    }
                    if (totalSize > 5) {
                        if(currPos <= 4){
                            mArrowLeft.setVisibility(View.GONE);
                            mArrowRight.setVisibility(View.VISIBLE);
                        }else if(currPos > 4 && currPos <(totalSize - 1)){
                            mArrowLeft.setVisibility(View.VISIBLE);
                            mArrowRight.setVisibility(View.VISIBLE);
                        }else if(currPos >=(totalSize - 1)){
                            mArrowLeft.setVisibility(View.VISIBLE);
                            mArrowRight.setVisibility(View.GONE);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    protected static class ProfileIconListKeyListener implements BaseOnItemViewKeyListener<Row> {
        protected HorizontalGridView mGridView;

        public ProfileIconListKeyListener(HorizontalGridView mGridView) {
            this.mGridView = mGridView;
        }

        @Override
        public boolean onItemKey(int keyCode, Presenter.ViewHolder var1, Object var2, Presenter.ViewHolder var3, Row var4) {
            int lastIndex = mGridView.getAdapter().getItemCount() - 1;
            int index = mGridView.getChildPosition(mGridView.getFocusedChild());

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    if (index == lastIndex) {
                        mGridView.scrollToPosition(0);
                        return true;
                    }

                    return false;
            }

            return false;
        }
    }

    protected static class ProfileIconListItemBridgeAdapter extends ItemBridgeAdapter {
        private float ALIGNMENT = 100f;
        private ProfileIconListRowPresenter.ViewHolder mRowViewHolder;
        private HorizontalGridView mHorizontalGridView;

        public ProfileIconListItemBridgeAdapter(ProfileIconListRowPresenter.ViewHolder mRowViewHolder) {
            this.mRowViewHolder = mRowViewHolder;
            this.mHorizontalGridView = mRowViewHolder.getGridView();

            initAlignment();
        }

        public void setAlignment(float alignment) {
            this.ALIGNMENT = alignment;

            if (mHorizontalGridView == null) {
                return;
            }

            mHorizontalGridView.setFocusScrollStrategy(BaseGridView.FOCUS_SCROLL_ALIGNED);
            mHorizontalGridView.setWindowAlignmentOffsetPercent(ALIGNMENT);
            mHorizontalGridView.setItemAlignmentOffsetPercent(ALIGNMENT);
        }

        protected void onCreate(ViewHolder viewHolder) {
            if (viewHolder.itemView instanceof ViewGroup) {
                TransitionHelper.setTransitionGroup((ViewGroup) viewHolder.itemView, true);
            }
        }

        protected void initAlignment() {
            if (mHorizontalGridView == null) {
                return;
            }

            setAlignment(ALIGNMENT);
        }

        public void onBind(final ViewHolder viewHolder) {
            if (this.mRowViewHolder.getOnItemViewClickedListener() != null) {
                viewHolder.mHolder.view.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ViewHolder ibh = (ViewHolder) mRowViewHolder.mGridView.getChildViewHolder(viewHolder.itemView);
                        if (mRowViewHolder.getOnItemViewClickedListener() != null) {
                            mRowViewHolder.getOnItemViewClickedListener().onItemClicked(viewHolder.mHolder, ibh.mItem, mRowViewHolder, mRowViewHolder.getRow());
                        }

                    }
                });
            }

            if (this.mRowViewHolder.getBaseOnItemKeyListener() != null) {
                viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() != KeyEvent.ACTION_DOWN) {
                            return false;
                        }

                        ViewHolder ibh = (ViewHolder) mRowViewHolder.mGridView.getChildViewHolder(viewHolder.itemView);
                        return mRowViewHolder.getBaseOnItemKeyListener().onItemKey(keyCode, viewHolder.mHolder, ibh.mItem, mRowViewHolder, mRowViewHolder.getRow());
                    }
                });
            }
        }

        public void onUnbind(ViewHolder viewHolder) {
            viewHolder.mHolder.view.setOnClickListener(null);
            viewHolder.mHolder.view.setOnKeyListener(null);
        }

        public void onAttachedToWindow(ViewHolder viewHolder) {
            this.mRowViewHolder.syncActivatedStatus(viewHolder.itemView);
        }
    }
}
