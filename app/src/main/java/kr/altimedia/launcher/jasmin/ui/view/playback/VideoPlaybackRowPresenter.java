/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback;

import android.view.View;

import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;


/**
 * Created by mc.kim on 05,02,2020
 */
public abstract class VideoPlaybackRowPresenter extends RowPresenter {

    public static class ViewHolder extends RowPresenter.ViewHolder {
        public ViewHolder(View view) {
            super(view);
        }
    }

    public void onReappear(RowPresenter.ViewHolder rowViewHolder) {
    }

    public void onHide(RowPresenter.ViewHolder rowViewHolder) {
    }

    public void onOptionEnabled(RowPresenter.ViewHolder rowViewHolder, boolean enable) {

    }

    public abstract void setEnable(RowPresenter.ViewHolder rowViewHolder, boolean enable);
}
