/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series;

import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter.VodSeasonPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class SeasonItemBridgeAdapter extends ItemBridgeAdapter {
    private final float ALIGNMENT = 93.3f;
    private String seasonId;

    private VerticalGridView mVerticalGridView;
    private View.OnKeyListener onKeyListener;

    public SeasonItemBridgeAdapter(ArrayObjectAdapter arrayObjectAdapter, String seasonId, VerticalGridView mVerticalGridView
            , View.OnKeyListener onKeyListener) {
        super(arrayObjectAdapter);

        this.seasonId = seasonId;
        this.mVerticalGridView = mVerticalGridView;
        this.onKeyListener = onKeyListener;

        initAlignment();
    }

    private void initAlignment() {
        if (mVerticalGridView == null) {
            return;
        }

        mVerticalGridView.setWindowAlignmentOffsetPercent(ALIGNMENT);
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        VodSeasonPresenter.ViewHolder vh = (VodSeasonPresenter.ViewHolder) viewHolder.mHolder;
        SeasonInfo seasonInfo = (SeasonInfo) viewHolder.mItem;

        if (seasonId != null) {
            boolean isChecked = seasonId.equalsIgnoreCase(seasonInfo.getSeasonID());
            vh.setChecked(isChecked);
        }

        vh.view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                if (onKeyListener != null) {
                    return onKeyListener.onKey(v, keyCode, event);
                }

                return false;
            }
        });
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);

        VodSeasonPresenter.ViewHolder vh = (VodSeasonPresenter.ViewHolder) viewHolder.mHolder;
        vh.view.setOnKeyListener(null);
    }
}
