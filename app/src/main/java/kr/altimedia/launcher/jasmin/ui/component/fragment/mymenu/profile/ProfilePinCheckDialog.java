/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingAccountPinCheckDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.lock.SettingProfileLockDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;

public class ProfilePinCheckDialog extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = ProfilePinCheckDialog.class.getName();
    private final String TAG = ProfilePinCheckDialog.class.getSimpleName();

    public static final String KEY_PROFILE = "PROFILE";

    //    private final ProgressBarManager progressBarManager = new ProgressBarManager();
    private final UserDataManager mUserDataManager = new UserDataManager();
    private MbsDataTask checkPinTask;

    private ProfileInfo profileInfo;

    private PasswordView password;
    private TextView description;
    private TextView btnCancel;
    private TextView btnForgotPin;

    private OnPinCheckResultListener onPinCheckResultListener;

    public ProfilePinCheckDialog(OnPinCheckResultListener listener){
        onPinCheckResultListener = listener;
    }

    public static ProfilePinCheckDialog newInstance(ProfileInfo profileInfo, OnPinCheckResultListener listener) {
        Bundle args = new Bundle();
        args.putParcelable(ProfilePinCheckDialog.KEY_PROFILE, profileInfo);
        ProfilePinCheckDialog dialog = new ProfilePinCheckDialog(listener);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_mymenu_profile_pin_check, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated");
        }

        Bundle bundle = getArguments();
        profileInfo = bundle.getParcelable(KEY_PROFILE);

//        initProgressbar(view);
        initContent(view);
        initButton(view);

        showView();
    }

//    private void initProgressbar(View view) {
//        LayoutInflater inflater = LayoutInflater.from(view.getContext());
//        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
//        progressBarManager.setProgressBarView(loadingView);
//    }
//
//    public void hideProgress() {
//        if (progressBarManager != null) {
//            progressBarManager.hide();
//        }
//    }
//
//    public void showProgress() {
//        if (progressBarManager != null) {
//            progressBarManager.show();
//        }
//    }

    private void initContent(View view) {
        description = view.findViewById(R.id.description);
        password = view.findViewById(R.id.pincode);
        password.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyInputChange, isFull : " + isFull + ", input : " + input);
                }
                if (isFull) {
                    checkPinCode(input);
                }
            }
        });
        password.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return false;
            }
        });
    }

    private void initButton(View view) {
        btnCancel = view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btnForgotPin = view.findViewById(R.id.btnForgotPin);
        btnForgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putParcelable(SettingProfileDialogFragment.KEY_PROFILE, profileInfo);
                SettingAccountPinCheckDialogFragment accountPinCheckDialog = SettingAccountPinCheckDialogFragment.newInstance(args);
                accountPinCheckDialog.setOnPinCheckCompleteListener(
                        new SettingAccountPinCheckDialogFragment.OnPinCheckCompleteListener() {
                        @Override
                        public void onPinCheckComplete() {
                            accountPinCheckDialog.dismiss();

                            SettingProfileLockDialogFragment profileLockDialog = SettingProfileLockDialogFragment.newInstance(args);
                            profileLockDialog.setOnLockChangeListener(
                                    new SettingProfileLockDialogFragment.OnLockChangeListener() {
                                    @Override
                                    public void onLockChange(boolean isLock) {
                                        profileLockDialog.dismiss();
                                        JasminToast.makeToast(getContext(), R.string.saved);

                                        resetPonCode();
                                    }
                            });
                            profileLockDialog.show(getFragmentManager(), SettingProfileLockDialogFragment.CLASS_NAME);
                        }
                });
                accountPinCheckDialog.show(getFragmentManager(), SettingAccountPinCheckDialogFragment.CLASS_NAME);
            }
        });
    }

    private void showView() {
        if (profileInfo != null) {
            password.requestFocus();
        } else {
            btnCancel.requestFocus();
        }
    }

    private void resetPonCode(){
        description.setVisibility(View.GONE);
        password.clear();
        password.requestFocus();
    }

    private void checkPinCode(String password) {
        checkPinTask = mUserDataManager.checkProfilePinCode(
                AccountManager.getInstance().getSaId(), profileInfo.getProfileId(), password,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
//                        if (loading) {
//                            showProgress();
//                        } else {
//                            hideProgress();
//                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onFailed");
                        }
                        showPinError();
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onSuccess, result : " + result);
                        }

                        if (result) {
                            dismiss();

                            if(onPinCheckResultListener != null){
                                onPinCheckResultListener.onPinCheckResult(true, profileInfo);
                            }
                        } else {
                            showPinError();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }
                });
    }

    private void showPinError() {
        password.clear();
        description.setVisibility(View.VISIBLE);
    }

    public interface OnPinCheckResultListener {
        void onPinCheckResult(boolean correct, Object object);
    }
}
