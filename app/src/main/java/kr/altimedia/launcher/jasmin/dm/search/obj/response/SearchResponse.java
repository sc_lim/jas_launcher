/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search.obj.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.search.obj.SearchData;

public class SearchResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private SearchData searchData;

    protected SearchResponse(Parcel in) {
        super(in);
        searchData = in.readTypedObject(SearchData.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(searchData, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchResponse> CREATOR = new Creator<SearchResponse>() {
        @Override
        public SearchResponse createFromParcel(Parcel in) {
            return new SearchResponse(in);
        }

        @Override
        public SearchResponse[] newArray(int size) {
            return new SearchResponse[size];
        }
    };

    public SearchData getSearchData() {
        return searchData;
    }

    @Override
    public String toString() {
        return "SearchResponse{" +
                "searchData=" + searchData +
                '}';
    }
}
