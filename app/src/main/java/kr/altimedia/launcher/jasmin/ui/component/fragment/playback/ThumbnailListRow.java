/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.playback;

import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

/**
 * Created by mc.kim on 03,03,2020
 */
public class ThumbnailListRow extends ListRow {

    public ThumbnailListRow(HeaderItem header, ObjectAdapter adapter) {
        super(header, adapter);
    }

    public ThumbnailListRow(long id, HeaderItem header, ObjectAdapter adapter) {
        super(id, header, adapter);
    }

    public ThumbnailListRow(ObjectAdapter adapter) {
        super(adapter);
    }


}
