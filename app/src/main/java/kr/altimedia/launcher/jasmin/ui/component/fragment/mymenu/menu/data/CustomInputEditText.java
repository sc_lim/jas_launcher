/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.data;

import android.content.Context;
import android.text.method.HideReturnsTransformationMethod;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.altimedia.util.Log;
import com.google.android.material.textfield.TextInputEditText;

public class CustomInputEditText extends TextInputEditText {
    private static final String TAG = CustomInputEditText.class.getSimpleName();

    private TextView.OnEditorActionListener mOnEditorActionListener;

    public CustomInputEditText(Context context) {
        super(context);
        setOnEditorAction();
    }

    public CustomInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnEditorAction();
    }

    public CustomInputEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOnEditorAction();
    }

    public void setOnEditorActionListener(OnEditorActionListener mOnEditorActionListener) {
        this.mOnEditorActionListener = mOnEditorActionListener;
    }

    private void setOnEditorAction() {
        setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        super.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "setOnEditorAction: actionId=" + actionId);
                }

                mOnEditorActionListener.onEditorAction(v, actionId, event);

                // Can get the next focus or the previous one according to the needs
                View nextView = v.focusSearch(View.FOCUS_DOWN);
                if (nextView != null) {
                    if (hideKeyboard()) {
                        return true;
                    }
                    nextView.requestFocus(View.FOCUS_DOWN);
                }
                //This must be returned here.true
                return true;
            }
        });
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT);
    }

    public boolean hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.hideSoftInputFromWindow(CustomInputEditText.this.getWindowToken(), 0);
            return true;
        }

        return false;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        hideKeyboard();
        mOnEditorActionListener = null;
    }

    //    @Override
//    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
//        if(Log.INCLUDE){
//            Log.d(TAG, "onKeyPreIme: keyCode="+keyCode);
//        }
//
//        if(keyCode == KeyEvent.KEYCODE_DPAD_UP) {
//            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//            if (imm.isActive()) {
//                imm.hideSoftInputFromWindow(this.getWindowToken(), 0);
//                return true;
//            }
//        }
//        return super.onKeyPreIme(keyCode, event);
//    }


}
