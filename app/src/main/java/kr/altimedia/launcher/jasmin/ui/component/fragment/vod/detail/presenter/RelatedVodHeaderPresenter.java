/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.presenter.BaseHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;

public class RelatedVodHeaderPresenter extends BaseHeaderPresenter {
    @Override
    protected ViewHolder createHeaderViewHolder(ViewGroup var1) {
        View view = LayoutInflater.from(var1.getContext()).inflate(R.layout.presenter_related_vod_header, var1, false);
        return new RelatedVodHeaderPresenterViewHolder(view);
    }

    @Override
    public int getSpaceUnderBaseline(ViewHolder holder) {
        Context context = holder.view.getContext();
        int relatedHeaderUnderLine = context.getResources().getDimensionPixelSize(R.dimen.related_header_underline);
        return relatedHeaderUnderLine;
    }

    @Override
    protected void onBindHeaderViewHolder(ViewHolder vh, Object item) {
        ListRow listRow = (ListRow) item;
        RelatedVodHeaderPresenterViewHolder viewHolder = (RelatedVodHeaderPresenterViewHolder) vh;

        View view = vh.view;
        Context context = view.getContext();
        int name = (int) listRow.getHeaderItem().getName();
        String header = context.getString(name);
        viewHolder.setData(header);
    }

    public class RelatedVodHeaderPresenterViewHolder extends ViewHolder {
        TextView row_header1;

        public RelatedVodHeaderPresenterViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view){
            row_header1 = view.findViewById(R.id.row_header1);
        }

        public void setData(String header){
            row_header1.setText(header);
        }

        @Override
        public TextView getHeaderTextView() {
            return row_header1;
        }
    }
}
