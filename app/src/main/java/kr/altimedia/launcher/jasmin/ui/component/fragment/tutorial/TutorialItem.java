/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.tutorial;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

public class TutorialItem implements Parcelable {
    private int title;
    private int desc;
    private Drawable image;

    public TutorialItem(int title, int desc, Drawable image) {
        this.title = title;
        this.desc = desc;
        this.image = image;
    }

    protected TutorialItem(Parcel in) {
        title = (int) in.readValue(Integer.class.getClassLoader());
        desc = (int) in.readValue(Integer.class.getClassLoader());
        Bitmap bitmap = (Bitmap) in.readValue(Bitmap.class.getClassLoader());
        if (bitmap != null) {
            image = new BitmapDrawable(bitmap);
        } else {
            image = null;
        }
    }

    public static final Creator<TutorialItem> CREATOR = new Creator<TutorialItem>() {
        @Override
        public TutorialItem createFromParcel(Parcel in) {
            return new TutorialItem(in);
        }

        @Override
        public TutorialItem[] newArray(int size) {
            return new TutorialItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeValue(desc);
        if (image != null) {
            Bitmap bitmap = ((BitmapDrawable) image).getBitmap();
            dest.writeValue(bitmap);
        } else {
            dest.writeValue(null);
        }
    }

    public int getTitle() {
        return title;
    }

    public int getDesc() {
        return desc;
    }

    public Drawable getImage() {
        return image;
    }
}
