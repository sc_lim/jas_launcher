/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;

import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;

import java.util.ArrayList;
import java.util.Arrays;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingMenuItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingChannelVodMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingOtherMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingUserMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SettingMenuHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SettingMenuPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class SettingMenuListFragment extends Fragment implements SettingMenuItemBridgeAdapter.OnClickMenuListener {
    public static final String CLASS_NAME = SettingMenuListFragment.class.getName();
    private static final String TAG = SettingMenuListFragment.class.getSimpleName();

    private ClassPresenterSelector presenterSelector;
    private VerticalGridView gridView;
    private int currentMenuPosition = -1;

    private OnFragmentChange onFragmentChange;

    private final ViewTreeObserver.OnGlobalFocusChangeListener
            focusChangeListener = new ViewTreeObserver.OnGlobalFocusChangeListener() {
        @Override
        public void onGlobalFocusChanged(View oldFocus, View newFocus) {
            if (newFocus == null || (oldFocus != null && oldFocus.getId() == R.id.settingList)) {
                return;
            }

            if (oldFocus == null) {
                if (newFocus.getId() == R.id.setting_menu) {
                    onChangeParentFocusStatus(true);
                }
            } else if (oldFocus.getId() != R.id.setting_menu && newFocus.getId() == R.id.setting_menu) {
                onChangeParentFocusStatus(true);
            } else if (oldFocus.getId() == R.id.setting_menu && newFocus.getId() != R.id.setting_menu) {
                onChangeParentFocusStatus(false);
            } else if (oldFocus.getId() == R.id.setting_menu && newFocus.getId() == R.id.setting_menu) {
                onChangeChildFocusStatus(oldFocus);
            }
        }
    };

    public static SettingMenuListFragment newInstance(OnFragmentChange onFragmentChange) {

        Bundle args = new Bundle();

        SettingMenuListFragment fragment = new SettingMenuListFragment();
        fragment.setOnMenuItemClick(onFragmentChange);
        fragment.setArguments(args);
        return fragment;
    }

    private void setOnMenuItemClick(OnFragmentChange onFragmentChange) {
        this.onFragmentChange = onFragmentChange;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings_menu, container, false);
    }

    private ViewGroup parentView = null;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (view instanceof ViewGroup) {
            parentView = (ViewGroup) view;
        }

        initView(view);
    }

    private void initView(View view) {
        gridView = view.findViewById(R.id.settingList);

        ArrayObjectAdapter rowAdapter = new ArrayObjectAdapter();
        if (isValidStatus()) {
            setUserMenu(rowAdapter);
            setChannelVodMenu(rowAdapter);
        }
        setOthersMenu(rowAdapter);

        presenterSelector = new ClassPresenterSelector();
        presenterSelector.addClassPresenter(HeaderItem.class, new SettingMenuHeaderPresenter());
        presenterSelector.addClassPresenter(SettingUserMenu.class, new SettingMenuPresenter());
        presenterSelector.addClassPresenter(SettingChannelVodMenu.class, new SettingMenuPresenter());
        presenterSelector.addClassPresenter(SettingOtherMenu.class, new SettingMenuPresenter());

        SettingMenuItemBridgeAdapter itemBridgeAdapter = new SettingMenuItemBridgeAdapter(rowAdapter, presenterSelector, gridView);
        itemBridgeAdapter.setOnClickMenuListener(this);
        gridView.setAdapter(itemBridgeAdapter);

        getView().getViewTreeObserver().addOnGlobalFocusChangeListener(focusChangeListener);
        gridView.setOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {
            @Override
            public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                super.onChildViewHolderSelected(parent, child, position, subposition);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onChildViewHolderSelected | position :  " + position + ", subposition : " + subposition);
                }
                if (currentMenuPosition != position) {
                    currentMenuPosition = position;

                    ItemBridgeAdapter.ViewHolder itemViewHolder = (ItemBridgeAdapter.ViewHolder) child;
                    showMenu(itemViewHolder.getItem());
                }
            }

            @Override
            public void onChildViewHolderSelectedAndPositioned(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                super.onChildViewHolderSelectedAndPositioned(parent, child, position, subposition);
            }
        });
    }

    private boolean isValidStatus() {
        return NetworkUtil.hasNetworkConnection(getContext())
                && AccountManager.getInstance().isAuthenticatedUser();
    }

    private void onChangeParentFocusStatus(boolean hasParentFocus) {
        if (gridView == null) {
            return;
        }

        for (int i = 0; i < gridView.getChildCount(); i++) {
            View view = gridView.getChildAt(i);
            ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) gridView.getChildViewHolder(view);
            SettingMenuPresenter.ViewHolder vh = (SettingMenuPresenter.ViewHolder) viewHolder.getViewHolder();
            vh.setParentFocus(hasParentFocus);
        }
    }

    private void onChangeChildFocusStatus(View oldFocus) {
        if (oldFocus != null) {
            SettingMenuPresenter.ViewHolder oldVh = (SettingMenuPresenter.ViewHolder) ((ItemBridgeAdapter.ViewHolder) gridView.getChildViewHolder(oldFocus)).getViewHolder();
            oldVh.setParentFocus(true);
        }
    }

    private void setUserMenu(ArrayObjectAdapter rowAdapter) {
//        HeaderItem channelVodHeaderItem = new HeaderItem(1, SettingChannelVodMenu.getMenuTitle());
        SettingUserMenu[] userMenus = SettingUserMenu.class.getEnumConstants();
        ArrayList<SettingUserMenu> channelVodList = new ArrayList<>(Arrays.asList(userMenus));

        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter();
        objectAdapter.addAll(0, channelVodList);

        ListRow row = new ListRow(/*channelVodHeaderItem,*/objectAdapter);
        rowAdapter.add(row);
    }

    private void setChannelVodMenu(ArrayObjectAdapter rowAdapter) {
//        HeaderItem channelVodHeaderItem = new HeaderItem(1, SettingChannelVodMenu.getMenuTitle());
        SettingChannelVodMenu[] channelVodMenus = SettingChannelVodMenu.class.getEnumConstants();
        ArrayList<SettingChannelVodMenu> channelVodList = new ArrayList<>(Arrays.asList(channelVodMenus));

        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter();
        objectAdapter.addAll(0, channelVodList);

        ListRow row = new ListRow(/*channelVodHeaderItem,*/objectAdapter);
        rowAdapter.add(row);
    }

    private void setOthersMenu(ArrayObjectAdapter rowAdapter) {
        ArrayList<SettingOtherMenu> othersList = isValidStatus() ?
                new ArrayList<>(Arrays.asList(SettingOtherMenu.values())) : new ArrayList<>(Arrays.asList(SettingOtherMenu.SYSTEM_SETTING, SettingOtherMenu.SYSTEM_INFO));

        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter();
        objectAdapter.addAll(0, othersList);

        ListRow row = new ListRow(objectAdapter);
        rowAdapter.add(row);
    }

    public void onUpdateMenuItem(int index, Object item) {
        SettingMenuItemBridgeAdapter itemBridgeAdapter = (SettingMenuItemBridgeAdapter) gridView.getAdapter();
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) itemBridgeAdapter.getAdapter();
        objectAdapter.replace(index, item);
    }

    private void showMenu(Object item) {
        if (!(item instanceof SettingMenu)) {
            return;
        }
        boolean isEnable = ((SettingMenu) item).isEnable();
        if (Log.INCLUDE) {
            Log.d(TAG, "showMenu | isEnable : " + isEnable);
        }
//        if (!isEnable) {
//            return;
//        }
        onFragmentChange.onChangeMenu((SettingMenu) item);
    }

    public void setFocusable(boolean isFocusable) {
        int currentDescendants = parentView.getDescendantFocusability();
        int requestDescendants = isFocusable ? ViewGroup.FOCUS_AFTER_DESCENDANTS : ViewGroup.FOCUS_BLOCK_DESCENDANTS;

        if (Log.INCLUDE) {
            Log.d(TAG, "setFocusable, currentDescendants : " + currentDescendants + ", requestDescendant : " + requestDescendants);
        }

        if (currentDescendants != requestDescendants) {
            parentView.setDescendantFocusability(requestDescendants);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getView().getViewTreeObserver().removeOnGlobalFocusChangeListener(focusChangeListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onFragmentChange = null;
    }

    @Override
    public void onClickMenu() {
        onFragmentChange.onClickMenu();
    }
}