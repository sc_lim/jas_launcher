/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.media;

import android.content.Context;
import android.view.SurfaceView;
import android.widget.RelativeLayout;

import com.coretrust.coretracker.kt3bb.FPInserter;

import com.altimedia.player.PlayerConstants;
import com.altimedia.util.Log;

public class FingerPrinting {
    private static final String TAG = FingerPrinting.class.getSimpleName();
    private static final int ERROR_FP_IS_NULL = -1;
    private static final int DEFAULT_WIDTH = 1920;
    private static final int DEFAULT_HEIGHT = 1080;

    private FPInserter fpInserter = null;
    private SurfaceView surfaceView = null;

    public FingerPrinting() {
        if (Log.INCLUDE) {
            Log.d(TAG, "FingerPrinting() hash:" + hashCode());
        }
    }

    private static final byte[] convertToBytes(String userCode) {
        if (userCode == null || userCode.length() == 0) {
            return null;
        }
        String str = getHexString(userCode);
        byte[] bytes = hexToByteArray(str);
        if (Log.INCLUDE) {
            Log.d(TAG, "convertToBytes() input : " + userCode);
            Log.d(TAG, "convertToBytes() output: " + printBytes(bytes));
        }
        return bytes;
    }

    private static final String getHexString(String str) {
        String ret = "";
        char[] chars = (str == null ? new char[0] : str.toCharArray());
        char ch;
        for (int i = 0 ; i < chars.length ; i++) {
            ch = chars[i];
            if (('0' <= ch && ch <= '9')
                    || ('a' <= ch && ch <= 'f')
                    || ('A' <= ch && ch <= 'F')) {
                ret += ch;
            }
        }
        if (ret.length() % 2 == 1) {
            ret = "0" + ret;
        }
        return ret;
    }

    private static byte[] hexToByteArray(String hex) {
        if (hex == null || hex.length() == 0) {
            return null;
        }
        byte[] ba = new byte[hex.length() / 2];
        for (int i = 0 ; i < ba.length ; i++) {
            ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return ba;
    }

    private static String printBytes(byte[] bytes) {
        String str = "";
        String hex = "";
        for (int i = 0 ; i < bytes.length ; i++) {
            hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                hex = "0"+hex;
            }
            str += hex;
        }
        return str;
    }

    public int initializePF(RelativeLayout relativeLayout, Context context, SurfaceView surfaceView, String userCode) {
        int result = ERROR_FP_IS_NULL;
        this.surfaceView = surfaceView;
        fpInserter = new FPInserter();
        if (fpInserter != null) {
            result = fpInserter.initializeFP(relativeLayout, context, surfaceView, PlayerConstants.getPackageName(), null, DEFAULT_WIDTH, DEFAULT_HEIGHT, convertToBytes(userCode));
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "initializePF() return:" + errorToString(result) + ", DEFAULT_WIDTH:" + DEFAULT_WIDTH + ", DEFAULT_HEIGHT:" + DEFAULT_HEIGHT);
        }
        return result;
    }

    public int startPF() {
        int result = ERROR_FP_IS_NULL;
        int width = DEFAULT_WIDTH;
        int height = DEFAULT_HEIGHT;
        if (surfaceView != null) {
            width = surfaceView.getWidth();
            height = surfaceView.getHeight();
        }

        if (fpInserter != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "startPF() width:" + width + ", height:" + height);
            }
            result = fpInserter.startFP(width, height);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "startPF() return:" + errorToString(result) + ", width:" + width + ", height:" + height);
        }
        return result;
    }

    public int resizePF(int width, int height) {
        int result = -1;
        if (fpInserter != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "resizePF() param:" + width + "x" + height);
            }
            result = fpInserter.resizeFP(width, height);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "resizePF() return:" + errorToString(result) + ", width:" + width + ", height:" + height);
        }
        return result;
    }

    public void stopPF() {
        if (Log.INCLUDE) {
            Log.d(TAG, "stopPF()");
        }
        if (fpInserter != null) {
            fpInserter.stopFP();
        }
    }

    public void finalizePF() {
        if (Log.INCLUDE) {
            Log.d(TAG, "finalizePF()");
        }
        surfaceView = null;
        if (fpInserter != null) {
            fpInserter.finalizeFP();
            fpInserter = null;
        }
    }

    private static final String errorToString(int error) {
        switch (error) {
            case ERROR_FP_IS_NULL:
                return "Error!!! : fpInserter == null";
            case 0:
                return "CTFP_SUCCESS";
            case 4097:
                return "CTFP_ERROR_INVALID_PARENTLAYOUT";
            case 4098:
                return "CTFP_ERROR_INVALID_PARENTCONTEXT";
            case 4099:
                return "CTFP_ERROR_INVALID_PLAYERVIEW";
            case 4100:
                return "CTFP_ERROR_INVALID_PACKAGENAME";
            case 4101:
                return "CTFP_ERROR_INVALID_LIB_PATH";
            case 4102:
                return "CTFP_ERROR_INVALID_RESOLUTION";
            case 4103:
                return "CTFP_ERROR_CANNOT_INSERT_IMAGE";
            case 4104:
                return "CTFP_ERROR_INVALIDE_CODE_DATA";
            case 262145:
                return "CTFP_ERROR_INVALID_USERID";
            case 262146:
                return "CTFP_ERROR_INVALID_APPVERSION";
            default:
                return "UNDEFINED_ERROR:"+error;
        }
    }
}
