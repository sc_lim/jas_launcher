/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscribedContent;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class SubscriptionStopFragment extends Fragment implements View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = SubscriptionDetailDialog.class.getName();
    private final String TAG = SubscriptionDetailDialog.class.getSimpleName();

    private final ProgressBarManager progressBarManager = new ProgressBarManager();
    private final UserDataManager mUserDataManager = new UserDataManager();
    private MbsDataTask checkPinTask;

    private SubscribedContent subscriptionItem;

    private TextView title;
    private TextView remainedDate;
    private TextView pincodeMessage;
    private PasswordView password;
    private TextView btnCancel;
    private TextView btnStopSubscription;
    private View rootView;

    public static SubscriptionStopFragment newInstance(SubscribedContent item) {
        Bundle args = new Bundle();
        args.putParcelable(SubscriptionDetailDialog.KEY_SUBSCRIPTION, item);
        SubscriptionStopFragment fragment = new SubscriptionStopFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (checkPinTask != null) {
            checkPinTask.cancel(true);
        }
        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if(keyCode >= KeyEvent.KEYCODE_0 && keyCode <=KeyEvent.KEYCODE_9){
            if(btnStopSubscription.isFocused() || btnCancel.isFocused()) {
                return true;
            }
        }

        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreateView");
        }

        return inflater.inflate(R.layout.fragment_mymenu_subscription_stop, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        Bundle args = getArguments();
        if (args != null) {
            subscriptionItem = args.getParcelable(SubscriptionDetailDialog.KEY_SUBSCRIPTION);
        }

        initProgressbar(view);
        initContent(view);
        initButton(view);

        showView();
    }

    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);
    }

    public void hideProgress() {
        if (progressBarManager != null) {
            progressBarManager.hide();
        }
    }

    public void showProgress() {
        if (progressBarManager != null) {
            progressBarManager.show();
        }
    }

    private void initContent(View view) {
        title = view.findViewById(R.id.title);
        remainedDate = view.findViewById(R.id.date);
        pincodeMessage = view.findViewById(R.id.pincodeMessage);

        password = view.findViewById(R.id.pincode);
        password.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyInputChange, isFull : " + isFull + ", input : " + input);
                }

                if (isFull) {
                    checkPinCode(input);
                }else{
                    btnStopSubscription.setEnabled(false);
                }
            }
        });
        password.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return false;
            }
        });
    }

    private void initButton(View view) {
        btnCancel = view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideProgress();
                ((SubscriptionDetailDialog) getParentFragment()).backToFragment();
            }
        });

        btnStopSubscription = view.findViewById(R.id.btnStopSubscription);
        btnStopSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestStop();
            }
        });
    }

    private void showView() {
        if (subscriptionItem != null) {
            title.setText(subscriptionItem.getProductName());
            String dateStr = "";
            try {
                dateStr = TimeUtil.getModifiedDate(subscriptionItem.getRenewalDate().getTime());
            } catch (Exception e) {
            }
            remainedDate.setText(dateStr);

            password.requestFocus();
        } else {
            btnCancel.requestFocus();
        }
    }

    private void checkPinCode(String password) {
        checkPinTask = mUserDataManager.checkAccountPinCode(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), password,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onFailed");
                        }

                        setPinError();
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onSuccess, result : " + result);
                        }

                        if (result) {
                            pincodeMessage.setVisibility(View.GONE);
                            btnStopSubscription.setEnabled(true);
                            btnStopSubscription.requestFocus();
                        } else {
                            setPinError();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }
                });
    }

    private void setPinError() {
        password.clear();
        String text = getString(R.string.incorrect_pin_desc);
        pincodeMessage.setText(text);
//                    Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
        pincodeMessage.setVisibility(View.VISIBLE);
    }

    private void requestStop() {
        UserDataManager userDataManager = new UserDataManager();
        userDataManager.putStopSubscription(
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                subscriptionItem.getProductId(),
                subscriptionItem.getPurchaseId(),
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        SubscriptionDetailDialog dialog = ((SubscriptionDetailDialog) getParentFragment());
                        SubscriptionDetailDialog.SubscriptionStopResultListener listener = dialog.getSubscriptionStopResultListener();
                        if (listener != null) {
                            listener.onFailed();
                        }
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        SubscriptionDetailDialog dialog = ((SubscriptionDetailDialog) getParentFragment());
                        SubscriptionDetailDialog.SubscriptionStopResultListener listener = dialog.getSubscriptionStopResultListener();
                        if (listener != null) {
                            listener.onSuccess();
                        }

                        dialog.showDetailView(SubscriptionDetailDialog.TYPE_RESULT, subscriptionItem);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        SubscriptionDetailDialog dialog = ((SubscriptionDetailDialog) getParentFragment());
                        SubscriptionDetailDialog.SubscriptionStopResultListener listener = dialog.getSubscriptionStopResultListener();
                        if (listener != null) {
                            listener.onFailed();
                        }

                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }
                });
    }
}
