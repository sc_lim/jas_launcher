/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.view.View;

import androidx.leanback.widget.Action;
import androidx.leanback.widget.OnActionClickedListener;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.tv.ui.TimeShiftSupportFragment;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekUi;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;

/**
 * Created by mc.kim on 05,02,2020
 */
public class TimeShiftPlaybackSupportFragmentGlueHost extends VideoPlaybackGlueHost implements VideoPlaybackSeekUi {
    final TimeShiftSupportFragment mFragment;
    final String TAG = TimeShiftPlaybackSupportFragmentGlueHost.class.getCanonicalName();
    final PlayerCallback mPlayerCallback =
            new PlayerCallback() {
                @Override
                public void onBufferingStateChanged(boolean start) {
                    mFragment.onBufferingStateChanged(start);
                }

                @Override
                public void onError(int errorCode, CharSequence errorMessage) {
                    mFragment.onError(errorCode, errorMessage);
                }

                @Override
                public void onVideoSizeChanged(int videoWidth, int videoHeight) {
                    mFragment.onVideoSizeChanged(videoWidth, videoHeight);
                }
            };

    public TimeShiftPlaybackSupportFragmentGlueHost(TimeShiftSupportFragment fragment) {
        this.mFragment = fragment;
    }

    @Override
    public boolean isControlsOverlayAutoHideEnabled() {
        return mFragment.isControlsOverlayAutoHideEnabled();
    }

    @Override
    public void setControlsOverlayAutoHideEnabled(boolean enabled) {
        mFragment.setControlsOverlayAutoHideEnabled(enabled);
    }

    @Override
    public void setOnKeyInterceptListener(View.OnKeyListener onKeyListener) {
        mFragment.setOnKeyInterceptListener(onKeyListener);
    }

    @Override
    public void setOnActionClickedListener(final OnActionClickedListener listener) {
        if (listener == null) {
            mFragment.setOnPlaybackItemViewClickedListener(null);
        } else {
            mFragment.setOnPlaybackItemViewClickedListener(new OnItemViewClickedListener() {


                @Override
                public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder rowViewHolder, Row var4) {
                    if (item instanceof Action) {
                        listener.onActionClicked((Action) item);
                    }
                }

            });
        }
    }

    @Override
    public void setHostCallback(HostCallback callback) {
        mFragment.setHostCallback(callback);
    }

    @Override
    public void notifyPlaybackRowChanged() {
        mFragment.notifyPlaybackRowChanged();
    }

    @Override
    public void setPlaybackRowPresenter(VideoPlaybackRowPresenter presenter) {
        mFragment.setVideoPlaybackRowPresenter(presenter);
    }

    @Override
    public void setPlaybackRow(Row row) {
        mFragment.setPlaybackRow(row);
    }

    @Override
    public void fadeOut() {
        mFragment.fadeOut();
    }

    @Override
    public boolean isControlsOverlayVisible() {
        return mFragment.isControlsOverlayVisible();
    }

    @Override
    public void hideControlsOverlay(boolean runAnimation) {
        mFragment.hideControlsOverlay(runAnimation);
    }

    @Override
    public void showControlsOverlay(boolean runAnimation) {
        mFragment.showControlsOverlay(runAnimation);
    }

    @Override
    public void changeTrailerMode(boolean selectionMode, boolean runAnimation) {
        mFragment.changeTrailerMode(selectionMode, runAnimation);
    }

    @Override
    public void showStateOverlay(boolean runAnimation) {
        mFragment.showStateOverlay(runAnimation);
    }

    @Override
    public boolean isStateOverlayShowing() {
        return mFragment.isStateOverlayShowing();
    }

    @Override
    public void hideStateOverlay(boolean runAnimation) {
        mFragment.hideStateOverlay(runAnimation);
    }

    @Override
    public void showIconView(boolean runAnimation) {
        mFragment.showIconView(runAnimation);
    }

    @Override
    public void hideIconView(boolean runAnimation) {
        mFragment.hideIconView(runAnimation);
    }

    @Override
    public void setPlaybackSeekUiClient(Client client) {
        mFragment.setPlaybackSeekUiClient(client);
    }

    @Override
    public PlayerCallback getPlayerCallback() {
        return mPlayerCallback;
    }

    @Override
    public void onFinish() {
//        super.onFinish();
        mFragment.onFinishedCalled();
    }
}
