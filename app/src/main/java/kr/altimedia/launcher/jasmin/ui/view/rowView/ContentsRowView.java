/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.rowView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class ContentsRowView extends LinearLayout {
    private final String TAG = ContentsRowView.class.getSimpleName();

    private HorizontalGridView mGridView;

    public ContentsRowView(Context context) {
        this(context, null);
    }

    public ContentsRowView(Context context, int resourceId) {
        this(context, null, 0, resourceId);
    }

    public ContentsRowView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public ContentsRowView(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs, defStyle, R.layout.list_contents_row_view);

    }

    public ContentsRowView(Context context, AttributeSet attrs, int defStyle, int resourceId) {
        super(context, attrs, defStyle);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(resourceId, this);

        mGridView = (HorizontalGridView) findViewById(R.id.row_content);
        // since we use WRAP_CONTENT for height in lb_list_row, we need set fixed size to false
        mGridView.setHasFixedSize(false);
        mGridView.setHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.contents_horizontal_space));
        // Uncomment this to experiment with page-based scrolling.
        // mGridView.setFocusScrollStrategy(HorizontalGridView.FOCUS_SCROLL_PAGE);

//        int paddingBottom = mGridView.getPaddingBottom();
//        int paddingTop = mGridView.getPaddingTop();
//        int paddingLeft = mGridView.getPaddingLeft();
//        int paddingRight = mGridView.getPaddingRight();
//
//        Log.d(TAG,"paddingBottom : "+paddingBottom+", paddingTop : "+paddingTop+", paddingLeft : "+paddingLeft+", paddingRight : "+paddingRight);
//        mGridView.setPadding(218, 16, 218, 16);

        setOrientation(LinearLayout.VERTICAL);
        setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
//        Log.d(TAG, "dispatchKeyEvent : " + event.getKeyCode());
        if (mOnDispatchKeyListener != null) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                mOnDispatchKeyListener.onKeyDown(event);
            }
        }
        return super.dispatchKeyEvent(event);
    }

    private OnDispatchKeyListener mOnDispatchKeyListener = null;

    public void setOnDispatchKeyListener(OnDispatchKeyListener onDispatchKeyListener) {
        this.mOnDispatchKeyListener = onDispatchKeyListener;
    }

    public void disposeListener() {
        this.mOnDispatchKeyListener = null;
    }

    /**
     * Returns the HorizontalGridView.
     */
    public HorizontalGridView getGridView() {
        return mGridView;
    }


    public interface OnDispatchKeyListener {
        void onKeyDown(KeyEvent keyCode);
    }

}
