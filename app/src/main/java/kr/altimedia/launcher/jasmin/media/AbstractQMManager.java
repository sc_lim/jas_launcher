package kr.altimedia.launcher.jasmin.media;

import com.altimedia.util.Log;

import af.qm.JasQmAvailableListener;
import af.qm.JasQmManager;

public abstract class AbstractQMManager implements JasQmAvailableListener {
    protected static final String TAG = "QMManager";

    /*
    private static QMManagerTable mTable = new QMManagerTable();
    private static class QMManagerTable extends Hashtable<Integer, AbstractQMManager> {
        private static final int MAX = 10;
        public int getKey(AbstractQMManager value) {
            int key = -1;
            for (Map.Entry entry : entrySet()) {
                if (value.equals(entry.getValue())) {
                    key = (Integer) entry.getKey();
                    break;
                }
            }
            return key;
        }
        public int put(AbstractQMManager value) {
            if (contains(value)) {
                return getKey(value);
            } else {
                for (int i = 0; i < MAX ; i++) {
                    if (containsKey(i) == false) {
                        put(i, value);
                        return i;
                    }
                }
                return -1;
            }
        }
        public int remove(AbstractQMManager value) {
            int key = getKey(value);
            remove(key);
            return key;
        }
    };
    */

    protected static final int CHANNEL_OPEN = 1;
    protected static final int CHANNEL_1ST_FRAME = 2;
    protected static final int CHANNEL_CLOSE = 3;

    protected static final int BUFFERING_START = JasQmManager.BUFFERING_START;
    protected static final int BUFFERING_END = JasQmManager.BUFFERING_END;

    private JasQmManager jasQmManager;
    protected int id = -1;

    protected int channelState = CHANNEL_CLOSE;
    protected int bufferingState = BUFFERING_END;
    protected boolean onSeekState = false;
    protected boolean isAvailable;

    protected AbstractQMManager() {
        if (Log.INCLUDE) {
            Log.d(TAG, "QMManager() created... this:" + this);
        }
        try {
            jasQmManager = JasQmManager.getInstance();
            isAvailable = false;
            jasQmManager.setAvailableListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void destroy() {
        if (Log.INCLUDE) {
            Log.d(TAG, "QMManager() destroy... this:" + this);
        }

        if (id != -1) {
            closeChannelMsg();
        }
    }

    public void onAvailable() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onAvailable()");
        }
        isAvailable = true;
    }

    protected void changeChannelState(int state) {
        if (channelState == state) {
            if (Log.INCLUDE) {
                Log.d(TAG, "changeChannelState() same state:" + channelStateToString(state));
            }
            return;
        } else {
            if (Log.INCLUDE) {
                Log.d(TAG, "changeChannelState() state:" + channelStateToString(state) + ", prev:" + channelStateToString(channelState));
            }
        }
        channelState = state;
        insertChannelMsg(state);
    }

    private void insertChannelMsg(int status) {
        if (Log.INCLUDE) {
            Log.d(TAG, "insertChannelMsg() this:" + this + ", state:" + channelStateToString(status));
        }
        if (!isAvailable) {
            if (Log.INCLUDE) {
                Log.d(TAG, "insertChannelMsg() QM is not yet available. - return");
            }
            return;
        }

        switch (status) {
            case CHANNEL_OPEN:
                openChannelMsg();
                break;
            case CHANNEL_1ST_FRAME:
                firstFrameChannelMsg();
                break;
            case CHANNEL_CLOSE:
                closeChannelMsg();
                break;
        }
    }

    protected void changeBufferingState(int state) {
        if (!isAvailable) {
            if (Log.INCLUDE) {
                Log.d(TAG, "changeBufferingState() QM is not yet available. - return");
            }
            return;
        }
        if (bufferingState == state) {
            if (Log.INCLUDE) {
                Log.d(TAG, "changeBufferingState() state:" + bufferingStateToString(state) + ", return - same state:" + bufferingStateToString(state));
            }
            return;
        } else if (channelState != CHANNEL_1ST_FRAME) {
            if (Log.INCLUDE) {
                Log.d(TAG, "changeBufferingState() state:" + bufferingStateToString(state) + ", return - not CHANNEL_1ST_FRAME state");
            }
            return;
        } else if (onSeekState) {
            if (Log.INCLUDE) {
                Log.d(TAG, "changeBufferingState() state:" + bufferingStateToString(state) + ", return - onSeekState");
            }
            return;
        } else {
            if (Log.INCLUDE) {
                Log.d(TAG, "changeBufferingState() state:" + bufferingStateToString(state) + ", prev:" + bufferingStateToString(bufferingState));
            }
        }
        bufferingState = state;
        insertBufferingMsg(bufferingState);
    }

    private void openChannelMsg() {
        try {
            id = jasQmManager.openChannelMsg();
            if (Log.INCLUDE) {
                Log.d(TAG, "openChannelMsg() this:" + this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void firstFrameChannelMsg() {
        try {
            boolean result = jasQmManager.firstFrameChannelMsg(id);
            if (Log.INCLUDE) {
                Log.d(TAG, "firstFrameChannelMsg() this:" + this + ", result:" + result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeChannelMsg() {
        try {
            boolean result = jasQmManager.closeChannelMsg(id);
            if (Log.INCLUDE) {
                Log.d(TAG, "closeChannelMsg() this:" + this + ", result:" + result);
            }
            id = -1;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void insertBufferingMsg(int status) {
        try {
            if (Log.INCLUDE) {
                Log.d(TAG, "insertBufferingMsg: this:" + this + ", state:" + bufferingStateToString(status));
            }
            jasQmManager.insertBufferingMsg(id, status);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void insertMediaSwitchMsg(int width, int height, int videoBps, int audioBps) {
        try {
            if (Log.INCLUDE) {
                Log.d(TAG, "insertMediaSwitchMsg: this:" + this + ", width:" + width + ", height:" + height + ", videoBps:" + videoBps + ", audioBps:" + audioBps);
            }
            jasQmManager.insertMediaSwitchMsg(id, width, height, videoBps, audioBps);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void insertMediaReceiveMsg(long videoDuration, long videoByte, long audioDuration, long audioByte) {
        try {
            if (Log.INCLUDE) {
                Log.d(TAG, "insertMediaReceiveMsg: this:" + this + ", videoDuration:" + videoDuration + ", videoByte:" + videoByte + ", audioDuration:" + audioDuration + ", audioByte:" + audioByte);
            }
            jasQmManager.insertMediaReceiveMsg(id, videoDuration, videoByte, audioDuration, audioByte);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected static String channelStateToString(int state) {
        switch (state) {
            case CHANNEL_OPEN:
                return "CHANNEL_OPEN";
            case CHANNEL_1ST_FRAME:
                return "CHANNEL_1ST_FRAME";
            case CHANNEL_CLOSE:
                return "CHANNEL_CLOSE";
            default:
                return "UNDEFINED_CHANNEL_STATE:"+state;
        }
    }

    protected static String bufferingStateToString(int state) {
        switch (state) {
            case BUFFERING_START:
                return "BUFFERING_START";
            case BUFFERING_END:
                return "BUFFERING_END";
            default:
                return "UNDEFINED_BUFFERING_STATE:"+state;
        }
    }

    @Override
    public String toString() {
        return "QMManager{0x" + Integer.toHexString(hashCode()) + ", id:" + id + '}';
    }
}
