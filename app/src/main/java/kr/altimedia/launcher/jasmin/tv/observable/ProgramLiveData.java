/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.observable;

import androidx.databinding.BaseObservable;

import com.altimedia.tvmodule.dao.Program;

public class ProgramLiveData extends BaseObservable {

    private Program currentProgram = null;

    public void setValue(Program program) {
        currentProgram = program;
    }

    public Program getProgram() {
        return currentProgram;
    }
}