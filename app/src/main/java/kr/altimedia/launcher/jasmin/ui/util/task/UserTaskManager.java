/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.util.task;


import android.content.Context;

import com.altimedia.util.Log;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponHomeInfo;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.user.object.Membership;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseDiscountType;

public class UserTaskManager extends TaskManager {
    public static final String CLASS_NAME = UserTaskManager.class.getName();
    private static final String TAG = UserTaskManager.class.getSimpleName();

    private static final UserTaskManager INSTANCE = new UserTaskManager();
    private MbsDataTask couponCountTask;

    private ArrayList<OnUpdateUserDiscountListener> listeners = new ArrayList<>();

    private Membership membership;
    private int pointBalance;
    private int pointCouponSize;
    private int discountCouponSize;
    private ArrayList<DiscountCoupon> discountCoupons = new ArrayList<>();
    private int expiringDiscountCouponSize;
    private int expiringPointCouponPrice;

    public int getPointCoupon() {
        return pointBalance;
    }

    private UserTaskManager() {

    }

    public static UserTaskManager getInstance() {
        return INSTANCE;
    }

    public Membership getMembership() {
        return membership;
    }

    public int getDiscountCouponSize() {
        return discountCouponSize;
    }

    public ArrayList<DiscountCoupon> getDiscountCoupons() {
        return discountCoupons;
    }

    public int getExpiringDiscountCouponSize() {
        return expiringDiscountCouponSize;
    }

    public long getExpiringPointCouponPrice() {
        return expiringPointCouponPrice;
    }

    public void addUserDiscountListener(OnUpdateUserDiscountListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public void removeUserDiscountListener(OnUpdateUserDiscountListener listener) {
        listeners.remove(listener);
    }

    private void notifyUserDiscountChange(int pointCouponSize, int discountCouponSize, ArrayList<DiscountCoupon> discountCoupons, Membership membership) {
        for (OnUpdateUserDiscountListener listener : listeners) {
            listener.notifyUserDiscountChange(pointCouponSize, discountCouponSize, discountCoupons, membership);
        }
    }

    private void notifyFail(int key) {
        for (OnUpdateUserDiscountListener listener : listeners) {
            listener.notifyFail(key);
        }
    }

    private void notifyError(String errorCode, String message) {
        for (OnUpdateUserDiscountListener listener : listeners) {
            listener.notifyError(errorCode, message);
        }
    }

    public void loadUserDiscountInfo(Context context) {
        if (couponCountTask != null && couponCountTask.isRunning()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "loadUserDiscountInfo, user discount info is already Running!");
            }

            return;
        }

        couponCountTask = mCouponDataManager.getCouponHomeInfo(AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(), true, new MbsDataProvider<String, CouponHomeInfo>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (!loading) {
                            completeTask(couponCountTask);
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadUserDiscountInfo, onFailed...");
                        }

//                        notifyFail(key);
                    }

                    @Override
                    public void onSuccess(String id, CouponHomeInfo result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadUserDiscountInfo, onSuccess result : " + result);
                        }
                        if (result == null) {
                            return;
                        }
                        pointCouponSize = result.getPointCouponCount();
                        pointBalance = result.getPointBalance();
                        discountCouponSize = result.getDiscountCouponCount();
                        discountCoupons.clear();
                        discountCoupons.addAll(0, result.getDiscountCouponList());
                        membership = new Membership(result.getMembershipPoint());
                        expiringDiscountCouponSize = result.getCpnExpCount();
                        expiringPointCouponPrice = result.getCpnExpPoint();

                        PurchaseDiscountType.MEMBERSHIP.setLimitRate(result.getPointLimitRate());
                        PurchaseDiscountType.MEMBERSHIP.setRatio(result.getPointRatio());

                        notifyUserDiscountChange(pointCouponSize, discountCouponSize, discountCoupons, membership);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadUserDiscountInfo, onError message : " + message);
                        }

                        notifyError(errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });

        addTask(couponCountTask);
    }

    public interface OnUpdateUserDiscountListener {
        void notifyUserDiscountChange(int pointCouponSize, int discountCouponSize, ArrayList<DiscountCoupon> discountCoupons, Membership membership);

        void notifyFail(int key);

        void notifyError(String errorCode, String message);
    }
}
