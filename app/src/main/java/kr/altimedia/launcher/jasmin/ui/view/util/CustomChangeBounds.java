/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.animation.Animator;
import android.transition.ChangeBounds;
import android.transition.TransitionValues;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;

public class CustomChangeBounds extends ChangeBounds {
    int mDefaultStartDelay;
    final HashMap<View, Integer> mViewStartDelays = new HashMap();
    final SparseIntArray mIdStartDelays = new SparseIntArray();
    final HashMap<String, Integer> mClassStartDelays = new HashMap();

    CustomChangeBounds() {
    }

    private int getDelay(View view) {
        Integer delay = (Integer)this.mViewStartDelays.get(view);
        if (delay != null) {
            return delay;
        } else {
            int idStartDelay = this.mIdStartDelays.get(view.getId(), -1);
            if (idStartDelay != -1) {
                return idStartDelay;
            } else {
                delay = (Integer)this.mClassStartDelays.get(view.getClass().getName());
                return delay != null ? delay : this.mDefaultStartDelay;
            }
        }
    }

    public Animator createAnimator(ViewGroup sceneRoot, TransitionValues startValues, TransitionValues endValues) {
        Animator animator = super.createAnimator(sceneRoot, startValues, endValues);
        if (animator != null && endValues != null && endValues.view != null) {
            animator.setStartDelay((long)this.getDelay(endValues.view));
        }

        return animator;
    }

    public void setStartDelay(View view, int startDelay) {
        this.mViewStartDelays.put(view, startDelay);
    }

    public void setStartDelay(int viewId, int startDelay) {
        this.mIdStartDelays.put(viewId, startDelay);
    }

    public void setStartDelay(String className, int startDelay) {
        this.mClassStartDelays.put(className, startDelay);
    }

    public void setDefaultStartDelay(int startDelay) {
        this.mDefaultStartDelay = startDelay;
    }
}
