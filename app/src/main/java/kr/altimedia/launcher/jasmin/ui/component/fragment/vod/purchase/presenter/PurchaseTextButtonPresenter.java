package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseTextButton;

public class PurchaseTextButtonPresenter extends Presenter {
    private View.OnClickListener mOnClickListener;
    private View.OnFocusChangeListener mOnFocusChangeListener;

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_purchase_text_button, parent, false);
        return new ViewHolder(view);
    }

    public void setOnClickListener(View.OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public void setOnFocusChangeListener(View.OnFocusChangeListener mOnFocusChangeListener) {
        this.mOnFocusChangeListener = mOnFocusChangeListener;
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((PurchaseTextButton) item, mOnClickListener, mOnFocusChangeListener);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.view.setOnClickListener(null);
        vh.view.setOnFocusChangeListener(null);
    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private final float DEFAULT_ALPHA = 1.0f;
        private final float DIM_ALPHA = 0.4f;

        public ViewHolder(View view) {
            super(view);
        }

        private void setData(PurchaseTextButton button, View.OnClickListener mOnClickListener, View.OnFocusChangeListener mOnFocusChangeListener) {
            TextView textView = (TextView) view;
            String value = view.getResources().getString(button.getValue());
            textView.setText(value);
            setEnable(button.isEnabled());

            textView.setOnClickListener(mOnClickListener);
            textView.setOnFocusChangeListener(mOnFocusChangeListener);
        }

        public void setEnable(boolean isEnable) {
            float alpha = isEnable ? DEFAULT_ALPHA : DIM_ALPHA;
            view.setAlpha(alpha);
            view.setEnabled(isEnable);
        }
    }
}
