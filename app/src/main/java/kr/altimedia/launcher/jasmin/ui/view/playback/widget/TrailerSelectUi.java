/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.widget;

/**
 * Created by mc.kim on 06,05,2020
 */
public interface TrailerSelectUi {
    class Client {

        public boolean isSelectionEnabled() {
            return false;
        }

        public void onSelectStarted() {
        }

        public TrailerDataProvider getTrailerDataProvider() {
            return null;
        }

        public void onSelectFinished(boolean cancelled) {
        }
    }

    void setTrailerSelectUiClient(Client client);
}
