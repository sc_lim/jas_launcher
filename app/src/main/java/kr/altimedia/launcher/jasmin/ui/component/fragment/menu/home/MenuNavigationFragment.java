/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.app.ProgressBarManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.category.MenuManager;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.dm.def.CategoryType;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.search.JasSearchManager;
import kr.altimedia.launcher.jasmin.system.manager.ExternalApplicationManager;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.AppConfig;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.BootDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ChannelErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.installedAppList.InstalledAppListFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.menu.MenuSliderLayout;
import kr.altimedia.launcher.jasmin.ui.component.fragment.message.MessageListFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.Linker;
import kr.altimedia.launcher.jasmin.ui.util.PermissionUtils;
import kr.altimedia.launcher.jasmin.ui.view.drawer.EdgeDrawerLayout;

public class MenuNavigationFragment extends Fragment implements OnMenuInteractor {
    public static final String CLASS_NAME = MenuNavigationFragment.class.getName();
    private static final String TAG = "MenuNavigationFragment";
    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private final List<Category> mCurrentCategory = new ArrayList<>();
    private final HashMap<String, CategoryType> mActionMap = new HashMap<>();
    private static final String KEY_NEED_ATTACHED = "attached";

    public MenuNavigationFragment() {
        // Required empty public constructor
        mActionMap.put(Intent.ACTION_ALL_APPS, CategoryType.Apps);
        mActionMap.put(Intent.ACTION_VIEW, CategoryType.SubCategory);
        mActionMap.put(Intent.ACTION_MAIN, CategoryType.SubCategory);
        mActionMap.put(Linker.ACTION_START_MYMENU, CategoryType.SubCategory);
    }

    public static MenuNavigationFragment newInstance() {
        MenuNavigationFragment fragment = new MenuNavigationFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    private boolean isLoginProcessing = false;
    private final BroadcastReceiver mMenuReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!intent.getAction().equalsIgnoreCase(PushMessageReceiver.ACTION_JUMP_MENU)) {
                return;
            }
            PushMessageReceiver.MenuType menuType =
                    (PushMessageReceiver.MenuType) intent.getSerializableExtra(PushMessageReceiver.KEY_MENU_TYPE);

            if (!NetworkUtil.hasNetworkConnection(getContext())) {
                FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(MbsTaskCallback.REASON_NETWORK, getContext());
                failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                return;
            }

            if (!AccountManager.getInstance().isAuthenticatedUser()) {
                if (isLoginProcessing) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "isLogin already doing so return");
                    }
                    return;
                }

                mTvOverlayManager.showBootDialog(BootDialogFragment.TYPE_RE_BOOT, new BootDialogFragment.BootProcessListener() {
                    @Override
                    public void onFinished(DialogFragment dialogFragment) {
                        dialogFragment.dismiss();
                        authenticationListener(true);
                        isLoginProcessing = false;
                        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(PushMessageReceiver.ACTION_LOGIN));

                        jumpLiveTv(menuType);
                    }

                    @Override
                    public void onFinishedWithError(DialogFragment dialogFragment, int type, String errorCode, String errorMessage) {
                        dialogFragment.dismiss();
                        isLoginProcessing = false;
                        authenticationListener(false);
                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(MbsTaskCallback.REASON_NETWORK, getContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }
                });
                isLoginProcessing = true;
            }
        }
    };

    private void jumpLiveTv(PushMessageReceiver.MenuType menuType) {
        int keyCode = menuType == PushMessageReceiver.MenuType.Guide ? KeyEvent.KEYCODE_GUIDE : KeyEvent.KEYCODE_TV;
        if (!TvSingletons.getSingletons(getContext()).getBackendKnobs().isEpgReady(true)) {
            String errorTitle = getString(R.string.error_title_dbs);
            TvSingletons tvSingletons = TvSingletons.getSingletons(getContext());
            ChannelErrorDialogFragment dialogFragment = ChannelErrorDialogFragment.newInstance(
                    tvSingletons.getChannelDataManager(), keyCode, TAG, errorTitle, tvSingletons.getBackendKnobs());
            dialogFragment.show(getFragmentManager(), ChannelErrorDialogFragment.CLASS_NAME);
            if (!PermissionUtils.hasReadTvListings(getContext())) {
                PermissionUtils.requestPermissions(getActivity(), PermissionUtils.PERMISSION_READ_TV_LISTINGS, 0);
            }
            return;
        }
        Intent tvIntent = new Intent(menuType == PushMessageReceiver.MenuType.Guide ?
                PlaybackUtil.ACTION_START_GUIDE : PlaybackUtil.ACTION_START_LIVE);
        tvIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        tvIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(tvIntent);
    }

    private void authenticationListener(boolean success) {
        if (Log.INCLUDE) {
            Log.d(TAG, "authenticationListener() success:" + success);
            Log.d(TAG, "authenticationListener() Said:" + AccountManager.getInstance().getSaId());
            Log.d(TAG, "authenticationListener() ProfileId:" + AccountManager.getInstance().getProfileId());
        }
        JasChannelManager.getInstance().initMBS(success);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_menu, container, false);
    }

    private EdgeDrawerLayout mDrawer = null;
    public MenuSliderLayout mSidePanel = null;
    private ViewGroup contentsView = null;
    private Category currentCategory = new Category(CategoryType.Invalid);
    private final EdgeDrawerLayout.SimpleDrawerListener mSimpleDrawerListener = new EdgeDrawerLayout.SimpleDrawerListener() {
        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            if (Log.INCLUDE) {
                Log.d(TAG, "onDrawerOpened : " + drawerView.getClass().getSimpleName());
            }
            mSidePanel.clearSelection();
            setFocusAllow(contentsView, false);
            setFocusAllow(((ViewGroup) drawerView), true);
            mSidePanel.onDrawerStateChanged(EdgeDrawerLayout.DrawerState.OPENED, mDrawer.getDrawerOffset(drawerView));
            drawerView.requestFocus();
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
            if (Log.INCLUDE) {
                Log.d(TAG, "onDrawerClosed : " + drawerView.getClass().getSimpleName());
            }
            setFocusAllow(((ViewGroup) drawerView), false);
            setFocusAllow(contentsView, true);
            contentsView.requestFocus();

            mSidePanel.onDrawerStateChanged(EdgeDrawerLayout.DrawerState.CLOSED, mDrawer.getDrawerOffset(drawerView));
        }

        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            super.onDrawerSlide(drawerView, slideOffset);
            EdgeDrawerLayout.DrawerState state = mDrawer.getDrawerState(drawerView);
            mSidePanel.onDrawerSlide(state, slideOffset);
        }
    };

    private void setFocusAllow(ViewGroup viewGroup, boolean focus) {
        viewGroup.setDescendantFocusability(focus ? ViewGroup.FOCUS_AFTER_DESCENDANTS : ViewGroup.FOCUS_BLOCK_DESCENDANTS);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (Log.INCLUDE) {
            Log.d(TAG, "onDestroyView");
        }
        mResumeHandler.removeCallbacks(mResumeRunnable);
        mDrawer.removeDrawerListener(mSimpleDrawerListener);
    }

    public boolean isInitialized() {
        return currentCategory == null || currentCategory.getLinkType() == CategoryType.Invalid;
    }

//    private void initHomeMenuCategory(Category category) {
//        initialize = true;
//        setCurrentCategory(category);
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction ft = fragmentManager.beginTransaction();
//        ft.addToBackStack(HomeVerticalFragment.CLASS_NAME);
//        ft.replace(R.id.contentsView, HomeVerticalFragment.newInstance(this, category),
//                HomeVerticalFragment.CLASS_NAME).commit();
//    }

    private boolean jumpMenu(Category category, Fragment fragment, String className) {
        return jumpMenu(category, fragment, className, false);
    }

    private boolean jumpMenu(Category category, Fragment fragment, String className, boolean force) {
        FragmentManager fragmentManager = getChildFragmentManager();
        if (!force && currentCategory == category) {
            return false;
        }

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.contentsView, fragment,
                className).commitAllowingStateLoss();
        return true;
    }


    private TvOverlayManager mTvOverlayManager;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (Log.INCLUDE) {
            Log.d(TAG, "onSaveInstanceState");
        }
        outState.putBoolean(KEY_NEED_ATTACHED, true);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
        LocalBroadcastManager.getInstance(context).registerReceiver(mMenuReceiver, new IntentFilter(PushMessageReceiver.ACTION_JUMP_MENU));
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewStateRestored");
        }
    }

    private void recoveryFromSaveInstance(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return;
        }
        boolean calledAttached = savedInstanceState.getBoolean(KEY_NEED_ATTACHED, false);
        if (calledAttached) {
            savedInstanceState.putBoolean(KEY_NEED_ATTACHED, false);
            if (getActivity() instanceof LauncherActivity) {
                mTvOverlayManager = ((LauncherActivity) getActivity()).getTvOverlayManager();
            } else if (getActivity() instanceof LiveTvActivity) {
                mTvOverlayManager = ((LiveTvActivity) getActivity()).getTvOverlayManager();
            } else if (getActivity() instanceof VideoPlaybackActivity) {
                mTvOverlayManager = ((VideoPlaybackActivity) getActivity()).getTvOverlayManager();
            }

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recoveryFromSaveInstance(savedInstanceState);
        contentsView = view.findViewById(R.id.contentsView);
        mDrawer = view.findViewById(R.id.drawer_layout);
        mSidePanel = view.findViewById(R.id.sidePanel);
        mSidePanel.setFragmentManager(getChildFragmentManager());
        mSidePanel.loadMainCategory(this, true);
        mDrawer.setClosedPaddingRate(0.392f);
        mDrawer.addDrawerListener(mSimpleDrawerListener);

        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private final Runnable mResumeRunnable = new Runnable() {
        @Override
        public void run() {
            onResumeProcess();

            if (isOpenMenu()) {
                setFocusAllow(contentsView, false);
                setFocusAllow(mSidePanel, true);
                mSidePanel.requestFocus();
            } else {
                setFocusAllow(mSidePanel, false);
                setFocusAllow(contentsView, true);
                contentsView.requestFocus();
            }
        }
    };
    private final Handler mResumeHandler = new Handler(Looper.getMainLooper());

    @Override
    public void onResume() {
        super.onResume();
        mResumeHandler.removeCallbacks(mResumeRunnable);
        mResumeHandler.post(mResumeRunnable);
    }

    @Override
    public Category getCurrentCategory() {
        return currentCategory;
    }

    private void onResumeProcess() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onResumeProcess");
        }

        Intent intent = getActivity().getIntent();
        if (intent == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onResumeProcess : intent == null");
            }
        } else {
            String action = intent.getAction();
            if (Log.INCLUDE) {
                Log.d(TAG, "onResumeProcess : action : " + action);
            }
            if (StringUtils.nullToEmpty(action).isEmpty()) {
                openMenu(false);
            } else {
                if (action.equalsIgnoreCase(Intent.ACTION_VIEW)) {
                    if (isInitialized()) {
                        openMenu(true);
                    }
                } else if (action.equalsIgnoreCase(Intent.ACTION_MAIN)) {
                    if (isInitialized()) {
                        openMenu(true);
                    }
                }
            }
        }
        getActivity().setIntent(null);
    }

    public void menuContainerRenewal() {
        menuContainerRenewal(false);
    }

    public void menuContainerRenewal(boolean isHomeOnly) {
        BackendKnobsFlags mBackendKnobsFlags = TvSingletons.getSingletons(getContext()).getBackendKnobs();
        if (mSidePanel != null) {
            List<Category> categories = new ArrayList<>();
            categories.addAll(mSidePanel.getCurrentCategoryList());
            for (Category category : categories) {
                if (mBackendKnobsFlags.checkCategoryUpdated(category.getCategoryID(), false)) {
                    Category updatedCategory = MenuManager.getInstance().getCategoryInfo(category.getCategoryID());
                    mSidePanel.updateList(updatedCategory);
                }
            }
        }

        Fragment fragment = getChildFragmentManager().findFragmentByTag(HomeVerticalFragment.CLASS_NAME);
        if (fragment != null && fragment.isVisible()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "menuContainerRenewal");
            }
            HomeVerticalFragment mHomeVerticalFragment = (HomeVerticalFragment) fragment;
            if (isHomeOnly) {
                if (!mHomeVerticalFragment.getCurrentCategory().isHome()) {
                    return;
                }
            }
            Category category = mHomeVerticalFragment.getCurrentCategory();
            if (mBackendKnobsFlags.checkCategoryUpdated(category.getCategoryID(), true)) {
                Category updatedCategory = MenuManager.getInstance().getCategoryInfo(category.getCategoryID());
                mHomeVerticalFragment.reloadList(updatedCategory);
            }
//            else {
//                mHomeVerticalFragment.renewalList();
//            }
        }
    }

    public void jumpAppList() {
        Category category = findCategoryByType(mCurrentCategory, CategoryType.Apps);
        menuSelected(category, false, false);
        mSidePanel.notifyPositionChanged();
    }

    public void jumpHome(boolean stopped) {
        if (Log.INCLUDE) {
            Log.d(TAG, "jumpHome stopped : " + stopped);
        }
        if (stopped) {
            menuContainerRenewal(true);
        }
        Fragment fragment = getChildFragmentManager().findFragmentByTag(HomeVerticalFragment.CLASS_NAME);
        if (fragment != null && fragment.isVisible()) {
            HomeVerticalFragment mHomeVerticalFragment = (HomeVerticalFragment) fragment;
            Category category = mHomeVerticalFragment.getCurrentCategory();
            if (Log.INCLUDE) {
                Log.d(TAG, "jumpHome category : " + category.isHome());
            }
            if (category.isHome()) {
                Category selectedCategory = mSidePanel.getSelectedCategory();
                if (selectedCategory != null && category.getCategoryID().equalsIgnoreCase(selectedCategory.getCategoryID())) {
                } else {
                    mSidePanel.notifyPositionChanged();
                }
                openMenu(true);
                return;
            }
        }
        openMenu(false);
        Category category = findCategoryHome(mCurrentCategory);
        menuSelected(category, false, false);
        mSidePanel.notifyPositionChanged();
    }

    private boolean mSideMenuEnable = false;

    @Override
    public void setSideMenuEnable(boolean enable) {
        mSideMenuEnable = enable;
    }

    @Override
    public boolean isMenuShowing() {
        return isOpenMenu();
    }


    public void openMenu(boolean withAnimation) {
        if (Log.INCLUDE) {
            Log.d(TAG, "EdgeDrawerLayout openMenu");
        }
        if (isOpenMenu()) {
            return;
        }
        mSidePanel.clearSelection();
        mDrawer.openDrawer(mSidePanel, withAnimation);
    }


    public void closeMenu(boolean withAnimation, boolean force) {
        if (Log.INCLUDE) {
            Log.d(TAG, "EdgeDrawerLayout closeMenu");
        }
        if (!mSideMenuEnable && !force) {
            return;
        }
        if (!isOpenMenu()) {
            return;
        }

        mDrawer.closeDrawer(mSidePanel, withAnimation);
    }

    public boolean isOpenMenu() {
        return mDrawer.isDrawerOpen(mSidePanel);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    private void setCurrentCategory(Category category) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setCurrentCategory : " + category);
        }
        currentCategory = category;
    }


    private boolean checkNetworkAndError(final Category category, @NonNull OnMenuSelectedCheckResult mOnMenuSelectedCheckResult) {
        boolean enabledCategory = !needToNetworkForEnterCategory(category);
        if (enabledCategory) {
            return true;
        }

        if (!NetworkUtil.hasNetworkConnection(getContext())) {
            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(MbsTaskCallback.REASON_NETWORK, getContext());
            failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
            return false;
        }

        if (!AccountManager.getInstance().isAuthenticatedUser()) {
            if (AppConfig.TEST_START_LIVE_TV_WITHOUT_LOGIN_ENABLED) {//just for testing
                return true;
            }
            mTvOverlayManager.showBootDialog(BootDialogFragment.TYPE_RE_BOOT, new BootDialogFragment.BootProcessListener() {
                @Override
                public void onFinished(DialogFragment dialogFragment) {
                    dialogFragment.dismiss();
                    authenticationListener(true);
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(PushMessageReceiver.ACTION_LOGIN));
                    mOnMenuSelectedCheckResult.onSuccess(category);
                }

                @Override
                public void onFinishedWithError(DialogFragment dialogFragment, int type, String errorCode, String errorMessage) {
                    dialogFragment.dismiss();
                    authenticationListener(false);
                    mOnMenuSelectedCheckResult.onFail(MbsTaskCallback.REASON_LOGIN);
                }
            });
            return false;
        }
        return true;
    }


    private boolean needToNetworkForEnterCategory(Category category) {
        switch (category.getLinkType()) {
            case Apps:
            case AppLink:
                return false;

            default:
                return true;
        }
    }


    private boolean jumpMenu(Category category, boolean force, boolean closeMenu) {
        boolean needToUpdateFocus = false;
        switch (category.getLinkType()) {
            case Search: {
                JasSearchManager.getInstance().startKeyboardSearch(getActivity());
                needToUpdateFocus = false;
            }
            break;
            case AppLink: {
                String categoryID = category.getLinkInfo();
                ExternalApplicationManager.getInstance().launchApp(getContext(), categoryID, true);
                needToUpdateFocus = false;
            }
            break;

            case SubCategory: {
                if (closeMenu) {
                    closeMenu(false, false);
                }
                boolean isJump = jumpMenu(category, HomeVerticalFragment.newInstance(category), HomeVerticalFragment.CLASS_NAME, force);
                if (isJump) {
                    setCurrentCategory(category);
                }
                needToUpdateFocus = true;
            }
            break;

            case Apps: {
                if (closeMenu) {
                    closeMenu(false, true);
                }
                boolean isJump = jumpMenu(category, InstalledAppListFragment.newInstance(), InstalledAppListFragment.CLASS_NAME, force);
                if (isJump) {
                    setCurrentCategory(category);
                }
                needToUpdateFocus = true;
            }
            break;
            case Message: {
                if (closeMenu) {
                    closeMenu(false, false);
                }
                boolean isJump = jumpMenu(category, MessageListFragment.newInstance(), MessageListFragment.CLASS_NAME, force);
                if (isJump) {
                    setCurrentCategory(category);
                }
                needToUpdateFocus = true;
            }
            break;
            case LiveTv: {
                if (!TvSingletons.getSingletons(getContext()).getBackendKnobs().isEpgReady(true)) {
                    String errorTitle = getString(R.string.error_title_dbs);
                    TvSingletons tvSingletons = TvSingletons.getSingletons(getContext());
                    ChannelErrorDialogFragment dialogFragment = ChannelErrorDialogFragment.newInstance(tvSingletons.getChannelDataManager(), 0, TAG, errorTitle,
                            tvSingletons.getBackendKnobs());
                    dialogFragment.show(getFragmentManager(), ChannelErrorDialogFragment.CLASS_NAME);

                    //check READ_TV_LISTINGS permission
                    if (!PermissionUtils.hasReadTvListings(getActivity())) {
                        PermissionUtils.requestPermissions(getActivity(), PermissionUtils.PERMISSION_READ_TV_LISTINGS, 0);
                    }
                    break;
                }
                Intent intent = new Intent(PlaybackUtil.ACTION_START_LIVE);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                needToUpdateFocus = false;
            }
            break;
        }

        return needToUpdateFocus;
    }

    @Override
    public void renewalMenu(Category menu) {
        if (Log.INCLUDE) {
            Log.d(TAG, "renewalMenu : " + menu);
        }

        Fragment fragment = getChildFragmentManager().findFragmentByTag(HomeVerticalFragment.CLASS_NAME);
        if (fragment != null && fragment.isVisible()) {

            HomeVerticalFragment mHomeVerticalFragment = (HomeVerticalFragment) fragment;
            Category category = mHomeVerticalFragment.getCurrentCategory();
            if (menu.equals(category)) {
                mHomeVerticalFragment.reloadList();
                return;
            }
        }

        Fragment installFragment = getChildFragmentManager().findFragmentByTag(InstalledAppListFragment.CLASS_NAME);
        if (installFragment != null && installFragment.isVisible()) {
            InstalledAppListFragment mInstalledAppListFragment = (InstalledAppListFragment) installFragment;
            if (menu.equals(currentCategory)) {
                mInstalledAppListFragment.renewalList();
                return;
            }
        }

        menuSelected(menu, false, false);
    }

    @Override
    public void menuSelected(Category category, boolean force, boolean closeMenu) {
        if (Log.INCLUDE) {
            Log.d(TAG, "menuSelected : " + category);
        }
        boolean enableEnterCategory = checkNetworkAndError(category, new OnMenuSelectedCheckResult() {
            @Override
            public void onSuccess(Category category) {
                boolean needUpdateFocus = jumpMenu(category, force, closeMenu);
                if (needUpdateFocus) {
                    mSidePanel.notifyPositionChanged();
                }
            }

            @Override
            public void onFail(int reason) {
                FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(reason, getContext());
                failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);

            }
        });

        if (!enableEnterCategory) {
            return;
        }

        boolean needUpdateFocus = jumpMenu(category, force, closeMenu);
        if (needUpdateFocus) {
            mSidePanel.notifyPositionChanged();
        }
    }

    public void notifyCheckMenuVersion(String action, boolean force) {

        Fragment fragment = getChildFragmentManager().findFragmentByTag(HomeVerticalFragment.CLASS_NAME);
        if (fragment != null && fragment.isAdded()) {
            HomeVerticalFragment homeVerticalFragment = (HomeVerticalFragment) fragment;
            homeVerticalFragment.clearData();
        }
        Fragment appListFragment = getChildFragmentManager().findFragmentByTag(InstalledAppListFragment.CLASS_NAME);
        if (appListFragment != null && appListFragment.isAdded()) {
            InstalledAppListFragment installedAppListFragment = (InstalledAppListFragment) appListFragment;
            installedAppListFragment.clearData();
        }
        final CategoryType targetType;
        if (mActionMap.containsKey(action)) {
            targetType = mActionMap.get(action);
        } else {
            if (action == null) {
                targetType = currentCategory.getLinkType();
            } else {
                targetType = CategoryType.SubCategory;
            }
        }
        mSidePanel.loadMainCategory(targetType, this, false);
    }

    public void notifyLoginStateChanged() {
        mSidePanel.loadMainCategory(this, false);
        mSidePanel.renewalProfileInfo();
    }

    public void notifyFocusLiveTv() {
        currentCategory = findCategoryByType(mCurrentCategory, CategoryType.LiveTv);
        mSidePanel.notifyPositionChanged();
    }

    public void notifyHomeUserContentsChanged() {
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyHomeUserContentsChanged");
        }
        Fragment fragment = getChildFragmentManager().findFragmentByTag(HomeVerticalFragment.CLASS_NAME);
        if (fragment != null && fragment.isAdded()) {
            HomeVerticalFragment homeVerticalFragment = (HomeVerticalFragment) fragment;
            homeVerticalFragment.notifyUpdateContentsAfterWatching(false);
        }
    }

    public void notifyProfileStateChanged(PushMessageReceiver.ProfileUpdateType updateType, String id) {
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyProfileStateChanged : " + updateType + ", id : " + id);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyProfileStateChanged : " + AccountManager.getInstance().getProfileId());
        }


        Fragment fragment = getChildFragmentManager().findFragmentByTag(HomeVerticalFragment.CLASS_NAME);
        switch (updateType) {
            case login:
                mSidePanel.renewalProfileInfo();
                if (fragment != null && fragment.isAdded()) {
                    HomeVerticalFragment homeVerticalFragment = (HomeVerticalFragment) fragment;
                    if (homeVerticalFragment.getCurrentCategory().isHome()) {
                        mSidePanel.notifyPositionChanged();
                        homeVerticalFragment.notifyUpdateUserContents(true);
                    } else {
                        jumpHome(false);
                    }
                } else {
                    jumpHome(false);
                }
                break;
            case rating:
                if (id == null || !id.equalsIgnoreCase(AccountManager.getInstance().getProfileId())) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "notifyProfileStateChanged : !id.equalsIgnoreCase(AccountManager.getInstance().getProfileId()) so return");
                    }
                    return;
                }
                if (fragment != null && fragment.isAdded()) {
                    HomeVerticalFragment homeVerticalFragment = (HomeVerticalFragment) fragment;
                    homeVerticalFragment.notifyUpdateContents();
                }
                break;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMenuReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void openSideMenu(boolean withAnimation) {
        openMenu(withAnimation);
    }


    @Override
    public void closeSideMenu(boolean withAnimation, boolean force) {
        closeMenu(withAnimation, force);
    }

    @Override
    public ProgressBarManager getProgressBarManager() {
        return mProgressBarManager;
    }

    @Override
    public TvOverlayManager getTvOverlayManager() {
        return mTvOverlayManager;
    }

    @Override
    public void needMenuUpdate(MbsDataProvider<String, Boolean> mbsDataProvider) {

        MenuManager.getInstance().checkMainMenu(getContext(), new MbsDataProvider<String, Boolean>() {
            @Override
            public void needLoading(boolean loading) {
                mbsDataProvider.needLoading(loading);
            }

            @Override
            public void onFailed(int reason) {
                mbsDataProvider.onFailed(reason);
            }

            @Override
            public void onSuccess(String id, Boolean result) {
                mbsDataProvider.onSuccess(id, result);
            }

            @Override
            public void onError(String errorCode, String message) {
                mbsDataProvider.onError(errorCode, message);
            }
        });
    }

    @Override
    public void onReadyToLoad(CategoryType type, MbsDataProvider<String, List<Category>> mbsDataProvider) {
        MenuManager.getInstance().getMainMenu(getContext(), new MbsDataProvider<String, List<Category>>() {
            @Override
            public void needLoading(boolean loading) {
                mbsDataProvider.needLoading(loading);
            }

            @Override
            public void onFailed(int key) {

            }

            @Override
            public void onSuccess(String id, List<Category> result) {
                mCurrentCategory.clear();
                mCurrentCategory.addAll(result);
                mbsDataProvider.onSuccess(id, result);
                if (type != null) {
                    currentCategory = findCategoryByType(mCurrentCategory, type);
                }
                showInitialPage(result);
            }

            @Override
            public void onError(String errorCode, String message) {
                ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                errorDialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
            }

            @Override
            public Context getTaskContext() {
                return MenuNavigationFragment.this.getContext();
            }
        });
    }

    private Category findCategoryHome(List<Category> categories) {
        for (Category category : categories) {
            if (category.getLinkType() == CategoryType.SubCategory && category.isHome()) {
                return category;
            }
        }
        return null;
    }

    private Category findCategoryByType(List<Category> categories, CategoryType type) {
        for (Category category : categories) {
            if (category.getLinkType() == type) {
                return category;
            }
        }
        return null;
    }

    private void showInitialPage(List<Category> categories) {
        boolean isNotLogin = !AccountManager.getInstance().isAuthenticatedUser();
        if (Log.INCLUDE) {
            Log.d(TAG, "showInitialPage | isNotLogin :  " + isNotLogin +
                    ", isInitialize : " + isInitialized() + ", currentCategory : " + currentCategory);
        }
        if (isNotLogin) {
            Category category = findCategoryByType(categories, CategoryType.Apps);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    menuSelected(category, true, false);
                }
            });
        } else if (isInitialized()) {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    Category category = findCategoryByType(categories, CategoryType.SubCategory);
                    menuSelected(category, true, false);
                }
            }, 500);
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    menuSelected(currentCategory, true, false);
                }
            });
        }
    }


    private interface OnMenuSelectedCheckResult {
        void onSuccess(Category category);

        void onFail(int reason);
    }

}
