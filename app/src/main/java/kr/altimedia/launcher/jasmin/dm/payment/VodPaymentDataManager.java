/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment;

import com.altimedia.util.Log;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.payment.module.PaymentModule;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Payment;
import kr.altimedia.launcher.jasmin.dm.payment.obj.PaymentPromotion;
import kr.altimedia.launcher.jasmin.dm.payment.obj.RequestVodPaymentResultInfo;
import kr.altimedia.launcher.jasmin.dm.payment.obj.VodPaymentResult;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.BankResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.CreditCardResponse;
import kr.altimedia.launcher.jasmin.dm.payment.remote.PaymentRemote;
import kr.altimedia.launcher.jasmin.dm.payment.type.ContentType;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class VodPaymentDataManager extends BaseDataManager {
    private final String TAG = VodPaymentDataManager.class.getSimpleName();
    private PaymentRemote mPaymentRemote;

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        PaymentModule mPaymentModule = new PaymentModule();
        Retrofit paymentRetrofit = mPaymentModule.providePaymentModule(mOkHttpClient);
        mPaymentRemote = new PaymentRemote(mPaymentModule.providePaymentApi(paymentRetrofit));
    }

    @Override
    public String getLoginToken() {
        return AccountManager.getInstance().getLoginToken();
    }

    public MbsDataTask getAvailablePaymentType(String offerId, String contentType,
                                               MbsDataProvider<String, List<PaymentType>> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, List<PaymentType>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mPaymentRemote.getAvailablePaymentType(AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), offerId, contentType);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<PaymentType> result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "getAvailablePaymentType, onTaskPostExecute, result : " + result);
                }

                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(offerId);
        return mMbsDataTask;
    }

    public MbsDataTask getPaymentPromotion(MbsDataProvider<String, List<PaymentPromotion>> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, List<PaymentPromotion>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mPaymentRemote.getPaymentPromotion();
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<PaymentPromotion> result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "getPaymentPromotion, onTaskPostExecute, result : " + result);
                }

                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(String.valueOf(0));
        return mMbsDataTask;
    }

    public MbsDataTask getCreditCardListResult(MbsDataProvider<String, CreditCardResponse.CreditCardResult> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, CreditCardResponse.CreditCardResult>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mPaymentRemote.getCreditCardListResult(AccountManager.getInstance().getSaId());
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, CreditCardResponse.CreditCardResult result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "getPaymentPromotion, onTaskPostExecute, result : " + result);
                }

                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(String.valueOf(0));
        return mMbsDataTask;
    }

    public MbsDataTask getBankListResult(MbsDataProvider<String, BankResponse.BankResult> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, BankResponse.BankResult>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mPaymentRemote.getBankListResult();
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, BankResponse.BankResult result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "getPaymentPromotion, onTaskPostExecute, result : " + result);
                }

                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(String.valueOf(0));
        return mMbsDataTask;
    }

    public MbsDataTask requestVodPayment(String said, String profileId, ContentType contentType,
                                         String offerId, List<Payment> payments, float totalPrice,
                                         MbsDataProvider<String, RequestVodPaymentResultInfo> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, RequestVodPaymentResultInfo>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mPaymentRemote.requestVodPayment(said, profileId, contentType, offerId, payments, totalPrice);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, RequestVodPaymentResultInfo result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "requestVodPayment, onTaskPostExecute, result : " + result);
                }

                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(offerId);
        return mMbsDataTask;
    }

    public MbsDataTask cancelVodPayment(String said, String profileId, String purchaseId,
                                        MbsDataProvider<String, Boolean> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Boolean>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mPaymentRemote.cancelVodPayment(said, profileId, purchaseId);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "requestVodPayment, onTaskPostExecute, result : " + result);
                }

                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(purchaseId);
        return mMbsDataTask;
    }

    public MbsDataTask getVodPaymentResult(String said, String profileId, String purchaseId,
                                           MbsDataProvider<String, VodPaymentResult> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, VodPaymentResult>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mPaymentRemote.getVodPaymentResult(said, profileId, purchaseId);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, VodPaymentResult result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "getVodPaymentResult, onTaskPostExecute, result : " + result);
                }

                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(purchaseId);
        return mMbsDataTask;
    }
}
