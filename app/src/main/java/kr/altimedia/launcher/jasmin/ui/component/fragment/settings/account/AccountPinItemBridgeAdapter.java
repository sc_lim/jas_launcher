/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account;

import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.PinPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class AccountPinItemBridgeAdapter extends ItemBridgeAdapter {
    private VerticalGridView gridView;
    private OnButtonFocusRequestListener onButtonFocusRequestListener;

    public AccountPinItemBridgeAdapter(ObjectAdapter adapter, VerticalGridView gridView) {
        super(adapter);
        this.gridView = gridView;
    }

    public void setOnButtonFocusRequestListener(OnButtonFocusRequestListener onButtonFocusRequestListener) {
        this.onButtonFocusRequestListener = onButtonFocusRequestListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        PinPresenter.ViewHolder vh = (PinPresenter.ViewHolder) viewHolder.getViewHolder();
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) getAdapter();

        PinPresenter.PinButtonItem item = (PinPresenter.PinButtonItem) viewHolder.mItem;
        int index = item.getType();

        vh.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                item.setPassword(input);

                if (isFull) {
                    moveDownNextPin(item.getType(), vh);
                } else if (isLastPinView(index) && !isFull) {
                    if (onButtonFocusRequestListener != null) {
                        onButtonFocusRequestListener.onDisableSave();
                    }
                }
            }
        });

        vh.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        if (vh.isFullPassword()) {
                            moveDownNextPin(index, vh);
                        }
                        return true;
                    case KeyEvent.KEYCODE_DPAD_DOWN:
                        moveButtons(vh.isFullPassword(), index);
                        return true;
                    case KeyEvent.KEYCODE_DPAD_UP:
                        if (index > 0) {
                            vh.clearPassword();

                            item.setEnable(false);
                            objectAdapter.replace(index, item);
                            gridView.setSelectedPosition(index - 1);

                            ItemBridgeAdapter.ViewHolder nextVh = (ViewHolder) gridView.getChildViewHolder(gridView.getFocusedChild());
                            PinPresenter.ViewHolder next = (PinPresenter.ViewHolder) nextVh.getViewHolder();
                            next.clearPassword();

                            if (isLastPinView(index)) {
                                if (onButtonFocusRequestListener != null) {
                                    onButtonFocusRequestListener.onDisableSave();
                                }
                            }
                            return true;
                        }

                        return false;
                }

                return false;
            }
        });

    }

    private void moveDownNextPin(int index, PinPresenter.ViewHolder vh) {
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) getAdapter();
        boolean isFull = vh.isFullPassword();
        int size = objectAdapter.size();
        int next = index < (size - 1) ? index + 1 : size - 1;

        PinPresenter.PinButtonItem[] items = new PinPresenter.PinButtonItem[size];
        for (int i = 0; i < size; i++) {
            items[i] = (PinPresenter.PinButtonItem) objectAdapter.get(i);
        }

        PinPresenter.PinButtonItem nextItem = items[next];

        if (isFull && !isLastPinView(index)) {
            nextItem.setEnable(true);
            objectAdapter.replace(nextItem.getType(), nextItem);
            gridView.setSelectedPosition(nextItem.getType());
        } else {
            moveButtons(isFull, next);
        }
    }

    private void moveButtons(boolean isFull, int index) {
        if (onButtonFocusRequestListener != null) {
            if (isFull && isLastPinView(index)) {
                onButtonFocusRequestListener.onSaveButtonRequestFocus();
            } else {
                onButtonFocusRequestListener.onCancelButtonRequestFocus();
            }
        }
    }

    private boolean isLastPinView(int index) {
        int size = getAdapter().size();
        return index == (size - 1);
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        PinPresenter.ViewHolder vh = (PinPresenter.ViewHolder) viewHolder.getViewHolder();
        vh.setOnPasswordComplete(null);
        vh.setOnKeyListener(null);
    }

    public interface OnButtonFocusRequestListener {
        void onSaveButtonRequestFocus();

        void onCancelButtonRequestFocus();

        void onDisableSave();
    }
}
