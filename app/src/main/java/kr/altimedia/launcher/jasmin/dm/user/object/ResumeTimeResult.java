/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 20,05,2020
 */
public class ResumeTimeResult implements Parcelable {
    @Expose
    @SerializedName("contentId")
    private String contentId;
    @Expose
    @SerializedName("resumeTime")
    private long resumeTime;
    @Expose
    @SerializedName("profileId")
    private String profileId;

    protected ResumeTimeResult(Parcel in) {
        contentId = in.readString();
        resumeTime = in.readLong();
        profileId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contentId);
        dest.writeLong(resumeTime);
        dest.writeString(profileId);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResumeTimeResult> CREATOR = new Creator<ResumeTimeResult>() {
        @Override
        public ResumeTimeResult createFromParcel(Parcel in) {
            return new ResumeTimeResult(in);
        }

        @Override
        public ResumeTimeResult[] newArray(int size) {
            return new ResumeTimeResult[size];
        }
    };

    public String getContentId() {
        return contentId;
    }

    public long getResumeTime() {
        return resumeTime;
    }

    public String getProfileId() {
        return profileId;
    }

    @Override
    public String toString() {
        return "ResumeTimeResult{" +
                "contentId='" + contentId + '\'' +
                ", resumeTime='" + resumeTime + '\'' +
                ", profileId='" + profileId + '\'' +
                '}';
    }
}
