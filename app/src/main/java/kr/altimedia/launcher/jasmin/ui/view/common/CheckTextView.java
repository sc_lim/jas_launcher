/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;

public class CheckTextView extends RelativeLayout {
    private RelativeLayout bg;
    private CheckBox checkBox;
    private TextView textView;

    public CheckTextView(Context context) {
        super(context);
        initView();
    }

    public CheckTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);
    }

    public CheckTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        getAttrs(attrs, defStyleAttr);
    }

    private void initView() {
        setGravity(Gravity.CENTER);

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.common_check_textview, this, false);
        addView(v);

        bg = v.findViewById(R.id.bg);
        checkBox = v.findViewById(R.id.check_icon);
        textView = v.findViewById(R.id.button);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CheckTextViewTheme);
        setTypeArray(typedArray);
    }


    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CheckTextViewTheme, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        int icon = typedArray.getResourceId(R.styleable.CheckTextViewTheme_check_text_icon, R.drawable.selector_setting_check_button);
        int textSize = typedArray.getResourceId(R.styleable.CheckTextViewTheme_check_text_size, R.dimen.setting_unfocus_check_text_size);
        int textColor = typedArray.getResourceId(R.styleable.CheckTextViewTheme_check_text_color, R.drawable.selector_common_bg_text);
        int fontFamily = typedArray.getResourceId(R.styleable.CheckTextViewTheme_check_text_font_family, R.font.font_prompt_medium);
        int bgColor = typedArray.getResourceId(R.styleable.CheckTextViewTheme_check_text_bg_drawable, R.drawable.selector_button_focus_w);
        int gravity = typedArray.getResourceId(R.styleable.CheckTextViewTheme_check_text_gravity, Gravity.CENTER);

        initCheckBox(icon);
        initTextView(gravity, textSize, textColor, fontFamily);

        bg.setBackgroundResource(bgColor);

        typedArray.recycle();
    }

    private void initCheckBox(int icon) {
        Drawable drawable = getContext().getDrawable(icon);
        drawable.getIntrinsicHeight();
        ViewGroup.LayoutParams params = checkBox.getLayoutParams();
        params.width = drawable.getIntrinsicWidth();
        params.height = drawable.getIntrinsicHeight();
        checkBox.setLayoutParams(params);
        checkBox.setBackgroundResource(icon);
    }

    private void initTextView(int text_gravity, int text_size, int text_color, int font_family) {
        Resources resources = getResources();
        int textSize = (int) resources.getDimension(text_size);

        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        textView.setTextColor(getResources().getColorStateList(text_color, null));
        textView.setTypeface(resources.getFont(font_family), Typeface.NORMAL);
        textView.setGravity(text_gravity);
    }

    public void setCheckBoxIcon(int iconId) {
        initCheckBox(iconId);
    }

    public void setChecked(boolean isChecked) {
        checkBox.setChecked(isChecked);
    }

    public void setEnabled(boolean enabled, float alpha) {
        bg.setAlpha(alpha);
        bg.setEnabled(enabled);
    }

    public void setVisibilityCheckBox(int visibility) {
        checkBox.setVisibility(visibility);
    }

    public void setText(String text) {
        textView.setText(text);
    }

    public void setWidthTextView(int dimenId) {
        RelativeLayout.LayoutParams params = (LayoutParams) textView.getLayoutParams();
        params.width = (int) getResources().getDimension(dimenId);

        textView.setLayoutParams(params);
    }

    public void setMaxLineTextView(int lineCount) {
        textView.setMaxLines(lineCount);
    }

    public void setCheckIconLayoutAlignment(int verb, int leftMarge, int topMargin, int rightMargin, int bottomMargin) {
        RelativeLayout.LayoutParams params = (LayoutParams) checkBox.getLayoutParams();

        Resources resources = getResources();

        params.addRule(verb);
        params.leftMargin = leftMarge != -1 ? (int) resources.getDimension(leftMarge) : 0;
        params.topMargin = topMargin != -1 ? (int) resources.getDimension(topMargin) : 0;
        params.rightMargin = rightMargin != -1 ? (int) resources.getDimension(rightMargin) : 0;
        params.bottomMargin = bottomMargin != -1 ? (int) resources.getDimension(bottomMargin) : 0;

        checkBox.setLayoutParams(params);
    }

    public void setTextGravity(int text_gravity) {
        textView.setGravity(text_gravity);
    }

    public void setTextLayoutGravity(int verb) {
        RelativeLayout.LayoutParams params = (LayoutParams) textView.getLayoutParams();
        params.addRule(verb);
    }

    public void setTextLayoutAlignment(int verb, int leftMarge, int topMargin, int rightMargin, int bottomMargin) {
        RelativeLayout.LayoutParams params = (LayoutParams) textView.getLayoutParams();

        Resources resources = getResources();

        params.addRule(verb);
        params.leftMargin = leftMarge != -1 ? (int) resources.getDimension(leftMarge) : 0;
        params.topMargin = topMargin != -1 ? (int) resources.getDimension(topMargin) : 0;
        params.rightMargin = rightMargin != -1 ? (int) resources.getDimension(rightMargin) : 0;
        params.bottomMargin = bottomMargin != -1 ? (int) resources.getDimension(bottomMargin) : 0;

        textView.setLayoutParams(params);
    }

    @Override
    public void setOnKeyListener(OnKeyListener l) {
        bg.setOnKeyListener(l);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        bg.setOnClickListener(l);
    }
}
