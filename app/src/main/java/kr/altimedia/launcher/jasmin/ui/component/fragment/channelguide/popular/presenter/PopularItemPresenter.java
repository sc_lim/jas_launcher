/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.presenter;

import android.content.Context;
import android.media.tv.TvContentRating;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.util.Log;
import com.google.common.collect.ImmutableList;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.ParentalController;
import kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.data.PopularChannel;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class PopularItemPresenter extends Presenter {

    private final int[] RANK_ICON_IDs = new int[]{
            R.drawable.rank_01, R.drawable.rank_02, R.drawable.rank_03,
            R.drawable.rank_04, R.drawable.rank_05, R.drawable.rank_06,
            R.drawable.rank_07, R.drawable.rank_08, R.drawable.rank_09,
    };
    private final int[] BLOCKED_IMG_IDs = new int[]{
            R.drawable.grid_pip_parental, R.drawable.grid_pip_blocked, R.drawable.grid_pip_unsubscribed
    };
    private final int[] BLOCKED_DESC_IDs = new int[]{
            R.string.parental_rating_program, R.string.blocked_channel, R.string.unsubscribed_channel
    };
    private final int TYPE_BLOCKED_PR = 0;
    private final int TYPE_BLOCKED_USER = 1;
    private final int TYPE_UNAUTHORIZED = 2;
    private final int TYPE_NORMAL = 3;

    private final int ALPHA_NORMAL = 178;
    private final int ALPHA_FOCUS = 255;

    private OnKeyListener onKeyListener;
    private OnFocusListener onFocusListener;

    public PopularItemPresenter(OnKeyListener onKeyListener, OnFocusListener onFocusListener) {
        this.onKeyListener = onKeyListener;
        this.onFocusListener = onFocusListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ChannelItemViewHolder channelItemViewHolder = new ChannelItemViewHolder(inflater.inflate(R.layout.item_channel_guide_popular_channel, null, false));
        return channelItemViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        ChannelItemViewHolder itemViewHolder = (ChannelItemViewHolder) viewHolder;
        PopularChannel channel = (PopularChannel) item;
        itemViewHolder.setItem(channel);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        ChannelItemViewHolder itemViewHolder = (ChannelItemViewHolder) viewHolder;
        itemViewHolder.removeMainImage();
    }

    public OnKeyListener getOnKeyListener() {
        return onKeyListener;
    }

    public class ChannelItemViewHolder extends ViewHolder implements View.OnFocusChangeListener {
        private final String TAG = ChannelItemViewHolder.class.getSimpleName();

        private PopularChannel popularChannel;

        private RelativeLayout cellLayer;
        private RelativeLayout cellDimLayer;
        private RelativeLayout cellFocusLayer;

        private ImageView rankIcon;
        private TextView channelNumber;
        private TextView channelName;
        private ImageView channelLogo;
        private ProgressBar programProgressbar;
        private TextView viewerCount;
        private LinearLayout channelBlockImg;
        private TextView channelBlockDesc;

        private boolean isBlocked = false;

        public ChannelItemViewHolder(View view) {
            super(view);

            cellLayer = view.findViewById(R.id.cellLayer);
            cellDimLayer = view.findViewById(R.id.cellDimLayer);
            cellFocusLayer = view.findViewById(R.id.cellFocusLayer);

            rankIcon = view.findViewById(R.id.rankIcon);
            channelNumber = view.findViewById(R.id.channelNumber);
            channelName = view.findViewById(R.id.channelName);
            channelLogo = view.findViewById(R.id.channelLogo);
            programProgressbar = view.findViewById(R.id.programProgressbar);
            viewerCount = view.findViewById(R.id.viewerCount);
            channelBlockImg = view.findViewById(R.id.channelBlockImg);
            channelBlockDesc = view.findViewById(R.id.channelBlockDesc);

            view.setOnFocusChangeListener(this);
        }

        public void setItem(PopularChannel popularChannel) {
            PopularChannel.PopularChannelLayout layout = popularChannel.getLayout();
            setCellSize(layout);

            this.popularChannel = popularChannel;
            String formattedNum = StringUtil.getFormattedNumber(popularChannel.getDisplayNumber(), 3);
            this.channelNumber.setText(formattedNum);

            this.channelLogo.setVisibility(View.GONE);
            this.channelName.setVisibility(View.VISIBLE);
            this.channelName.setText(popularChannel.getDisplayName());

//            this.channelName.setVisibility(View.GONE);
//            this.channelLogo.setVisibility(View.VISIBLE);
//            this.channelLogo.setImageResource(R.drawable.dummy_channel_logo_000); // TODO set corresponding logo
//            this.channelLogo.setImageAlpha(ALPHA_NORMAL);

            int rank = popularChannel.getViewingRank();
            if (rank >= 1 && rank <= 9) {
                rankIcon.setImageResource(RANK_ICON_IDs[rank - 1]);
            }
            try {
                this.viewerCount.setText(Float.toString(popularChannel.getViewingRate()));
            }catch (Exception e){
            }
            programProgressbar.setMax((int) popularChannel.getTotalProgress());
            try {
                programProgressbar.getProgressDrawable().setAlpha(ALPHA_NORMAL);
            }catch (Exception e){
            }
            setProgramProgressbar();

            Channel channel = popularChannel.getChannel();
            isBlocked = false;
            if(channel != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "setItem: rank=" + rank +", locked="+channel.isLocked() + ", paid=" + channel.isPaidChannel());
                }
                int type = TYPE_NORMAL;
                Program program = popularChannel.getProgram();
                if (program != null && isRatingBlocked(program.getContentRatings())){
                    type = TYPE_BLOCKED_PR;
                } else if (channel.isLocked()){
                    type = TYPE_BLOCKED_USER;
                } else if (channel.isPaidChannel()){
                    type = TYPE_UNAUTHORIZED;
                }

                if(type == TYPE_NORMAL){
                    if(channel.isAudioChannel()){
                        channelBlockDesc.setText("");
                        channelBlockImg.setBackground(view.getResources().getDrawable(R.drawable.grid_pip_audio, null));
                        channelBlockImg.setVisibility(View.VISIBLE);
                    }else {
                        channelBlockImg.setVisibility(View.GONE);
                    }
                }else{
                    channelBlockDesc.setText(BLOCKED_DESC_IDs[type]);
                    channelBlockImg.setBackground(view.getResources().getDrawable(BLOCKED_IMG_IDs[type], null));
                    channelBlockImg.setVisibility(View.VISIBLE);
                    isBlocked = true;
                }
            }
        }

        private void setProgramProgressbar(){
            if(popularChannel != null) {
                if(popularChannel.getCurrentProgress() <= popularChannel.getTotalProgress()) {
                    programProgressbar.setProgress((int) popularChannel.getCurrentProgress());
                }else{
                    programProgressbar.setProgress((int) popularChannel.getTotalProgress());
                }
            }
        }

        private void setCellSize(PopularChannel.PopularChannelLayout layout){
            if(layout == null) return;
            if(layout.getWidth() <= 0 || layout.getHeight() <= 0) return;

            int videoWidth = layout.getWidth();
            int videoHeight = layout.getHeight();
            int outerWidth = (int)(videoWidth + (3.5 * 2));
            int outerHeight = (int)(videoHeight + (3.5 * 2));

            if (Log.INCLUDE) {
                Log.d(TAG, "setCellSize: outer=(" + outerWidth +", " + outerHeight +"), video=(" + videoWidth +", " + videoHeight +")");
            }

            RelativeLayout.LayoutParams outerParams = new RelativeLayout.LayoutParams(outerWidth, outerHeight);
            cellLayer.setLayoutParams(outerParams);
            cellFocusLayer.setLayoutParams(outerParams);
        }

        public boolean isBlocked() {
            return isBlocked;
        }

        private boolean isRatingBlocked(ImmutableList<TvContentRating> ratings) {
            try {
                return ParentalController.isProgramRatingBlocked(ratings);
            }catch (Exception e){
            }
            return false;
        }

        public void removeMainImage() {
            rankIcon.setImageDrawable(null);
            channelLogo.setImageDrawable(null);
        }

        private void updateView(int alpha, boolean selected){
            if(channelLogo.getVisibility() == View.VISIBLE) {
                channelLogo.setImageAlpha(alpha);
            }
            if(channelName.getVisibility() == View.VISIBLE){
                channelName.setSelected(selected);
            }
            if(programProgressbar.getVisibility() == View.VISIBLE) {
                try {
                    programProgressbar.getProgressDrawable().setAlpha(alpha);
                }catch (Exception e){
                }
            }
            setProgramProgressbar();
        }

        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
                updateView(ALPHA_FOCUS, true);
                cellDimLayer.setVisibility(View.GONE);

            }else{
                updateView(ALPHA_NORMAL, false);
                cellDimLayer.setVisibility(View.VISIBLE);
            }
            if (onFocusListener != null) {
                int index = (int) view.getTag(R.id.KEY_INDEX);
                onFocusListener.onFocus(hasFocus, index, popularChannel, isBlocked);
            }

        }
    }

    public interface OnKeyListener {
        boolean onKey(int keyAction, int keyCode, int index, Object item, ViewHolder viewHolder);
    }

    public interface OnFocusListener {
        void onFocus(boolean hasFocus, int index, Object item, boolean isBlocked);
    }
}