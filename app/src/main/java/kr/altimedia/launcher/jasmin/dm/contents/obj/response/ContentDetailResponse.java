/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import com.altimedia.util.Log;


/**
 * Created by mc.kim on 08,05,2020
 */
public class ContentDetailResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private ContentsDetailResult result;

    protected ContentDetailResponse(Parcel in) {
        super(in);
        result = in.readTypedObject(ContentsDetailResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(result, flags);
    }

    public ContentDetailInfo getDetailInfo() {
        result.content.setTermsAndConditions(result.termsAndConditions);
        ContentDetailInfo detailInfo = new ContentDetailInfo(result.content);
        if (Log.INCLUDE) {
            Log.d("CHECK :  ", "result.content : " + result.content.toString());
        }
        return detailInfo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContentsDetailResult> CREATOR = new Creator<ContentsDetailResult>() {
        @Override
        public ContentsDetailResult createFromParcel(Parcel in) {
            return new ContentsDetailResult(in);
        }

        @Override
        public ContentsDetailResult[] newArray(int size) {
            return new ContentsDetailResult[size];
        }
    };

    @Override
    public String toString() {
        return "ContentsDetailResult{" +
                "result=" + result +
                '}';
    }

    private static class ContentsDetailResult implements Parcelable {
        @SerializedName("termsAndConditions")
        @Expose
        private String termsAndConditions;
        @SerializedName("content")
        @Expose
        private Content content;

        protected ContentsDetailResult(Parcel in) {
            termsAndConditions = in.readString();
            content = (Content) in.readValue(Content.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(termsAndConditions);
            dest.writeValue(content);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<ContentsDetailResult> CREATOR = new Creator<ContentsDetailResult>() {
            @Override
            public ContentsDetailResult createFromParcel(Parcel in) {
                return new ContentsDetailResult(in);
            }

            @Override
            public ContentsDetailResult[] newArray(int size) {
                return new ContentsDetailResult[size];
            }
        };

    }
}
