/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.user.object.Membership;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.managerProvider;
import kr.altimedia.launcher.jasmin.ui.util.task.UserTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewKeyListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.VodOverviewRow;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class SeriesDetailRowPresenter extends RowPresenter {
    private static final String TAG = SeriesDetailRowPresenter.class.getSimpleName();

    private String masterSeriesTitle;
    private int count = 0;

    private BaseOnItemViewKeyListener baseOnItemViewKeyListener;
    private managerProvider managerProvider;

    private int getLayoutResourceId() {
        return R.layout.presenter_series_detail;
    }

    public void setEpisodeInfo(String masterSeriesTitle, int count) {
        this.masterSeriesTitle = masterSeriesTitle;
        this.count = count;
    }

    public void setManagerProvider(managerProvider managerProvider) {
        this.managerProvider = managerProvider;
    }

    @Override
    protected RowPresenter.ViewHolder createRowViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(getLayoutResourceId(), parent, false);
        return new VodDetailsRowPresenterViewHolder(v);
    }

    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);

        VodOverviewRow row = (VodOverviewRow) item;
        VodDetailsRowPresenterViewHolder viewHolder = (VodDetailsRowPresenterViewHolder) holder;
        viewHolder.mActionBridgeAdapter = new ActionsItemBridgeAdapter(viewHolder);
        viewHolder.setBaseOnItemKeyListener(baseOnItemViewKeyListener);
        viewHolder.onBind();
        viewHolder.setData((Content) row.getItem(), masterSeriesTitle, count);
        viewHolder.setManagerProvider(managerProvider);
    }

    @Override
    protected void onUnbindRowViewHolder(ViewHolder vh) {
        VodDetailsRowPresenterViewHolder viewHolder = (VodDetailsRowPresenterViewHolder) vh;
        viewHolder.onUnbind();
//        viewHolder.setOnItemViewClickedListener(null);
//        viewHolder.setBaseOnItemKeyListener(null);

        super.onUnbindRowViewHolder(vh);
    }

    public void setBaseOnItemViewKeyListener(BaseOnItemViewKeyListener baseOnItemViewKeyListener) {
        this.baseOnItemViewKeyListener = baseOnItemViewKeyListener;
    }

    public static class VodDetailsRowPresenterViewHolder extends RowPresenter.ViewHolder
            implements UserTaskManager.OnUpdateUserDiscountListener {
        private LinearLayout layout = null;

        private TextView title = null;
        private TextView subTitle = null;
        private TextView releaseDate = null;
        private TextView genre = null;
        private TextView duration = null;
        private TextView seriesCount = null;
        private TextView director = null;
        private TextView cast = null;
        private TextView description = null;

        private TextView membershipView = null;
        private TextView couponPoint = null;
        private TextView couponDiscount = null;

        private ItemBridgeAdapter mActionBridgeAdapter;
        private HorizontalGridView mActionsRow;

        private VodOverviewRow.Listener mRowListener = createRowListener();
        private managerProvider managerProvider;

        private final UserTaskManager userTaskManager = UserTaskManager.getInstance();

        public VodDetailsRowPresenterViewHolder(View rootView) {
            super(rootView);

            initView(rootView);

            userTaskManager.addUserDiscountListener(this);
        }

        private void initView(View view) {
            layout = view.findViewById(R.id.layout);

            title = view.findViewById(R.id.title);
            subTitle = view.findViewById(R.id.sub_title);
            releaseDate = view.findViewById(R.id.releaseDate);
            genre = view.findViewById(R.id.genre);
            duration = view.findViewById(R.id.duration);
            seriesCount = view.findViewById(R.id.series_count);
            director = view.findViewById(R.id.director);
            cast = view.findViewById(R.id.cast);
            description = view.findViewById(R.id.description);
            mActionsRow = view.findViewById(R.id.details_overview_actions);

            membershipView = view.findViewById(R.id.membership);
            couponPoint = view.findViewById(R.id.coupon_point);
            couponDiscount = view.findViewById(R.id.coupon_discount);

            layout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        mActionsRow.requestFocus();
                    }
                }
            });
        }

        public void setData(Content content, String masterSeriesTitle, int count) {
            title.setText(masterSeriesTitle);
            subTitle.setText(content.getTitle());
            releaseDate.setText(content.getReleaseDate());
            genre.setText(content.getGenre().toString()
                    .replace("[", "").replace("]", ""));
            duration.setText(String.valueOf(content.getRunTime() / TimeUtil.MIN));
            seriesCount.setText(String.valueOf(count));
            director.setText(content.getDirector());
            cast.setText(content.getActor());
            description.setText(content.getSynopsis());

            setMembershipView();
            setCouponView();
        }

        public void setManagerProvider(managerProvider managerProvider) {
            this.managerProvider = managerProvider;
        }

        public HorizontalGridView getActionsRow() {
            return mActionsRow;
        }

        private void setMembershipView() {
            Membership membership = userTaskManager.getMembership();
            if (membership != null) {
                int membershipPoint = membership.getMembershipPoint();
                membershipView.setText(StringUtil.getFormattedNumber(membershipPoint));
            }
        }

        private void setCouponView() {
            int totalPointCoupon = userTaskManager.getPointCoupon();
            int size = userTaskManager.getDiscountCouponSize();

            couponPoint.setText(StringUtil.getFormattedNumber(totalPointCoupon));
            couponDiscount.setText(StringUtil.getFormattedNumber(size));
        }

        protected VodOverviewRow.Listener createRowListener() {
            return new VodOverviewRowListener();
        }

        void onBind() {
            VodOverviewRow row = (VodOverviewRow) getRow();
            bindActions(row.getActionsAdapter());
            row.addListener(mRowListener);
        }

        void onUnbind() {
            VodOverviewRow row = (VodOverviewRow) getRow();
            row.removeListener(mRowListener);
            mActionsRow.setAdapter(null);
            mRowListener = null;
            mActionBridgeAdapter.clear();
            mActionBridgeAdapter = null;

            managerProvider = null;
            userTaskManager.removeUserDiscountListener(this);
        }

        void bindActions(ObjectAdapter adapter) {
            mActionBridgeAdapter.setAdapter(adapter);
            mActionsRow.setAdapter(mActionBridgeAdapter);
        }

        @Override
        public void notifyUserDiscountChange(int pointCouponSize, int discountCouponSize, ArrayList<DiscountCoupon> discountCoupons, Membership membership) {
            setCouponView();
            setMembershipView();
        }

        @Override
        public void notifyFail(int key) {
        }

        @Override
        public void notifyError(String errorCode, String message) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + message);
            }


            ErrorDialogFragment dialogFragment =
                    ErrorDialogFragment.newInstance(TAG, errorCode, message);
            dialogFragment.show(managerProvider.getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
        }


        public class VodOverviewRowListener extends VodOverviewRow.Listener {
            @Override
            public void onImageDrawableChanged(VodOverviewRow row) {
            }

            @Override
            public void onItemChanged(VodOverviewRow row) {
            }

            @Override
            public void onActionsAdapterChanged(VodOverviewRow row) {
                bindActions(row.getActionsAdapter());
            }
        }
    }

    private class ActionsItemBridgeAdapter extends ItemBridgeAdapter {
        VodDetailsRowPresenterViewHolder mViewHolder;

        ActionsItemBridgeAdapter(VodDetailsRowPresenterViewHolder viewHolder) {
            mViewHolder = viewHolder;
        }

        @Override
        public void onBind(final ItemBridgeAdapter.ViewHolder ibvh) {
            if (mViewHolder.getOnItemViewClickedListener() != null) {
                Presenter presenter = ibvh.getPresenter();
                presenter.setOnClickListener(
                        ibvh.getViewHolder(), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mViewHolder.getOnItemViewClickedListener() != null) {
                                    mViewHolder.getOnItemViewClickedListener().onItemClicked(
                                            ibvh.getViewHolder(), ibvh.getItem(),
                                            mViewHolder, mViewHolder.getRow());
                                }
                            }
                        });
            }

            if (mViewHolder.getBaseOnItemKeyListener() != null) {
                ibvh.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() != KeyEvent.ACTION_DOWN) {
                            return false;
                        }

                        ItemBridgeAdapter.ViewHolder ibh = (ItemBridgeAdapter.ViewHolder) mViewHolder.mActionsRow.getChildViewHolder(ibvh.itemView);
                        return mViewHolder.getBaseOnItemKeyListener().onItemKey(keyCode, ibvh.mHolder, ibh.mItem, mViewHolder, mViewHolder.getRow());
                    }
                });
            }
        }

        @Override
        public void onUnbind(final ItemBridgeAdapter.ViewHolder ibvh) {
            if (mViewHolder.getOnItemViewClickedListener() != null) {
                ibvh.getPresenter().setOnClickListener(ibvh.getViewHolder(), null);
            }

            ibvh.mHolder.view.setOnKeyListener(null);
        }
    }
}