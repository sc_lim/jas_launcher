/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.system.settings;

import android.content.Context;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Objects;

import kr.altimedia.launcher.jasmin.cwmp.service.CWMPSettingControl;
import kr.altimedia.launcher.jasmin.system.settings.data.SubtitleBroadcastLanguage;
import kr.altimedia.launcher.jasmin.system.settings.data.SubtitleFontSize;

public class SettingControl implements CWMPSettingControl.CWMPSettingsEventListener {
    private final String TAG = SettingControl.class.getSimpleName();
    public static final String KEY_ACCOUNT_ID = "af.jas_account_id";
    public static final String KEY_SAID = CWMPSettingControl.KEY_SAID;
    public static final String KEY_PROFILE_ID = CWMPSettingControl.KEY_PROFILE_ID;
    public static final String KEY_LA_URL = CWMPSettingControl.KEY_LA_URL;
    public static final String KEY_LOGIN_TOKEN = CWMPSettingControl.KEY_LOGIN_TOKEN;
    public static final String KEY_REGION_CODE = CWMPSettingControl.KEY_REGION_CODE;

    public static final String KEY_SUBTITLE_ENABLE = CWMPSettingControl.KEY_SUBTITLE_ENABLE;
    public static final String KEY_SUBTITLE_LANGUAGE_CODE = CWMPSettingControl.KEY_SUBTITLE_LANGUAGE_CODE;
    public static final String KEY_SUBTITLE_FONT_SIZE = CWMPSettingControl.KEY_SUBTITLE_FONT_SIZE;
    public static final String KEY_CLOSEDCAPTION_ENABLE = CWMPSettingControl.KEY_CLOSEDCAPTION_ENABLE;
    public static final String KEY_DEBUG_INFORMATION = CWMPSettingControl.KEY_DEBUG_INFORMATION;

    private final String DISABLE = "0";
    private final String ENABLE = "1";
    public static final String LANG_ENG = "en";
    public static final String LANG_THAI = "th";
    public static final String SUBTITLE_FONT_SMALL = "0";
    public static final String SUBTITLE_FONT_STANDARD = "1";
    public static final String SUBTITLE_FONT_LARGE = "2";


    public static final int VIDEO_RESOLUTION_PAL = CWMPSettingControl.VIDEO_RESOLUTION_PAL;
    public static final int VIDEO_RESOLUTION_NTSC = CWMPSettingControl.VIDEO_RESOLUTION_NTSC;
    public static final int VIDEO_RESOLUTION_480p = CWMPSettingControl.VIDEO_RESOLUTION_480p;
    public static final int VIDEO_RESOLUTION_576p = CWMPSettingControl.VIDEO_RESOLUTION_576p;
    public static final int VIDEO_RESOLUTION_720p = CWMPSettingControl.VIDEO_RESOLUTION_720p;
    public static final int VIDEO_RESOLUTION_720p50hz = CWMPSettingControl.VIDEO_RESOLUTION_720p50hz;
    public static final int VIDEO_RESOLUTION_1080i = CWMPSettingControl.VIDEO_RESOLUTION_1080i;
    public static final int VIDEO_RESOLUTION_1080i50hz = CWMPSettingControl.VIDEO_RESOLUTION_1080i50hz;
    public static final int VIDEO_RESOLUTION_1080p = CWMPSettingControl.VIDEO_RESOLUTION_1080p;
    public static final int VIDEO_RESOLUTION_1080p24hz = CWMPSettingControl.VIDEO_RESOLUTION_1080p24hz;
    public static final int VIDEO_RESOLUTION_1080p50hz = CWMPSettingControl.VIDEO_RESOLUTION_1080p50hz;
    public static final int VIDEO_RESOLUTION_2160p30hz = CWMPSettingControl.VIDEO_RESOLUTION_2160p30hz;
    public static final int VIDEO_RESOLUTION_2160p60hz = CWMPSettingControl.VIDEO_RESOLUTION_2160p60hz;


    private static final SettingControl INSTANCE = new SettingControl();
    private final CWMPSettingControl mCWMPSettingControl;

    private SettingControl() {
        mCWMPSettingControl = new CWMPSettingControl();
    }

    public static SettingControl getInstance() {
        return INSTANCE;
    }

    private String get(String key) {
        return StringUtils.nullToEmpty(mCWMPSettingControl.get(key));
    }

    private void set(String key, String value) {
        mCWMPSettingControl.set(key, value);
    }

    public void setParentalRating(int rating) {
        mCWMPSettingControl.setParentalRating(rating);
    }

    public void setSaid(String said) {
        set(KEY_SAID, said);
    }


    public void setAccountId(String accountId) {
        set(KEY_ACCOUNT_ID, accountId);
    }

    public String getSaId() {
        return get(KEY_SAID);
    }

    public String getProfileId() {
        return get(KEY_PROFILE_ID);
    }

    public void setProfileId(String profileId) {
        set(KEY_PROFILE_ID, profileId);
    }

    public String getLaUrl() {
        return get(KEY_LA_URL);
    }

    public void setLaUrl(String laUrl) {
        set(KEY_LA_URL, laUrl);
    }

    public String getLoginToken() {
        return get(KEY_LOGIN_TOKEN);
    }

    public void setLoginToken(String loginToken) {
        set(KEY_LOGIN_TOKEN, loginToken);
    }

    public void setRegionCode(String regionCode) {
        set(KEY_REGION_CODE, regionCode);
    }

    public boolean isSubtitleEnable() {
        String value = get(KEY_SUBTITLE_ENABLE);
        return value.equalsIgnoreCase(ENABLE);
    }

    public void setSubtitleEnable(Boolean isEnable) {
        String value = isEnable ? ENABLE : DISABLE;
        set(KEY_SUBTITLE_ENABLE, value);
    }

    public String getSubtitleLanguageCode() {
        return get(KEY_SUBTITLE_LANGUAGE_CODE);
    }

    public String getSubtitleLanguageFontSize() {
        return get(KEY_SUBTITLE_FONT_SIZE);
    }

    public void setSubtitleOption(boolean isEnable, String langType, String fontSize) {
        setSubtitleEnable(isEnable);
        set(KEY_SUBTITLE_LANGUAGE_CODE, langType);
        set(KEY_SUBTITLE_FONT_SIZE, fontSize);
    }

    public boolean isClosedCaptionEnable() {
        String value = get(KEY_CLOSEDCAPTION_ENABLE);
        return value.equalsIgnoreCase(ENABLE);
    }

    public void setClosedCaptionEnable(boolean isEnable) {
        String value = isEnable ? ENABLE : DISABLE;
        set(KEY_CLOSEDCAPTION_ENABLE, value);
    }

    public boolean isDebugInfo() {
        String value = get(KEY_DEBUG_INFORMATION);
        return value.equalsIgnoreCase(ENABLE);
    }

    public void setDebugInfo(boolean isEnable) {
        String value = isEnable ? ENABLE : DISABLE;
        set(KEY_DEBUG_INFORMATION, value);
    }

    public void toggleDebugInfo() {
        setDebugInfo(!isDebugInfo());
    }


    private ArrayList<SettingsEventListenerProxy> listeners = new ArrayList<>();

    public void addSettingsEventListener(SettingsEventListener listener) {
        SettingsEventListenerProxy settingsEventListenerProxy = new SettingsEventListenerProxy(listener);
        if (listeners.contains(settingsEventListenerProxy)) {
            return;
        }
        listeners.add(settingsEventListenerProxy);
        if (!listeners.isEmpty()) {
            mCWMPSettingControl.setSettingsEventListener(this);
        }
    }

    public void addSettingsEventListener(SettingsEventListener listener, String[] keys) {
        SettingsEventListenerProxy settingsEventListenerProxy = new SettingsEventListenerProxy(listener);
        settingsEventListenerProxy.keys = keys;
        if (listeners.contains(settingsEventListenerProxy)) {
            return;
        }
        listeners.add(settingsEventListenerProxy);
        if (!listeners.isEmpty()) {
            mCWMPSettingControl.setSettingsEventListener(this);
        }
    }

    public void removeSettingsEventListener(SettingsEventListener listener) {
        SettingsEventListenerProxy settingsEventListenerProxy = new SettingsEventListenerProxy(listener);
        if (!listeners.contains(settingsEventListenerProxy)) {
            return;
        }
        listeners.remove(settingsEventListenerProxy);
        if (listeners.isEmpty()) {
            mCWMPSettingControl.clearSettingsEventListener();
        }
    }

    public boolean reset() {
        try {
            String lang = SubtitleBroadcastLanguage.THAI.getKey();
            String size = SubtitleFontSize.MEDIUM.getKey();
            setSubtitleOption(false, lang, size);
            setClosedCaptionEnable(false);
            return true;
        } catch (Exception | Error e) {
        }
        return false;
    }

    public void setPowerMode(Context context, CWMPSettingControl.POWER_MODE power_mode) {
        mCWMPSettingControl.setPowerMode(context, power_mode);
    }

    public void factoryReset(Context context) {
        mCWMPSettingControl.factoryReset(context);
    }

    public void setVideoResolution(int resolution) {
        mCWMPSettingControl.setVideoResolution(resolution);
    }

    private class SettingsEventListenerProxy {
        public SettingsEventListener listener;
        public String[] keys;

        public SettingsEventListenerProxy(SettingsEventListener listener) {
            this.listener = listener;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SettingsEventListenerProxy that = (SettingsEventListenerProxy) o;
            return Objects.equals(listener, that.listener);
        }

        @Override
        public int hashCode() {
            return Objects.hash(listener);
        }
    }

    public interface SettingsEventListener {
        void onSettingChanged(String var1);
    }

    @Override
    public void onSettingChanged(String var1) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSettingChanged : " + var1);
        }
        for (SettingsEventListenerProxy settingsEventListenerProxy : listeners) {
            if (settingsEventListenerProxy.keys == null) {
                settingsEventListenerProxy.listener.onSettingChanged(var1);
            } else {
                if (isContains(settingsEventListenerProxy.keys, var1)) {
                    settingsEventListenerProxy.listener.onSettingChanged(var1);
                }
            }
        }
    }

    private boolean isContains(String[] list, String data) {
        for (String checkData : list) {
            if (checkData.equalsIgnoreCase(data)) {
                return true;
            }
        }
        return false;
    }
}
