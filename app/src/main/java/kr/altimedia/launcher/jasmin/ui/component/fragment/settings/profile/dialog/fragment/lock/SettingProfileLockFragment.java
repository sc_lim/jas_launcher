/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.lock;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import com.altimedia.util.Log;

import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.system.settings.data.OnOffValue;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.CheckButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.CheckButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.SettingProfileBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment.KEY_PROFILE;

public class SettingProfileLockFragment extends SettingProfileBaseFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton<SettingProfileLockFragment.LockCheckButtonButton> {
    public static final String CLASS_NAME = SettingProfileLockFragment.class.getName();
    private final String TAG = SettingProfileLockFragment.class.getSimpleName();

    private VerticalGridView gridView;
    private ArrayObjectAdapter objectAdapter;
    private CheckButtonBridgeAdapter checkButtonBridgeAdapter;

    private LockCheckButtonButton btnOn;
    private LockCheckButtonButton btnOff;

    private SettingProfileLockFragment() {
    }

    public static SettingProfileLockFragment newInstance(Bundle bundle) {
        SettingProfileLockFragment fragment = new SettingProfileLockFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_setting_profile_lock;
    }

    @Override
    protected void initView(View view) {
        initList(view);
    }

    private void initList(View view) {
        ProfileInfo profileInfo = getArguments().getParcelable(KEY_PROFILE);
        boolean isLock = profileInfo.isLock();

        btnOn = new LockCheckButtonButton(OnOffValue.SET.getType(), isLock);
        btnOff = new LockCheckButtonButton(OnOffValue.OFF.getType(), !isLock);

        CheckButtonPresenter checkButtonPresenter = new CheckButtonPresenter();
        objectAdapter = new ArrayObjectAdapter(checkButtonPresenter);
        objectAdapter.add(btnOn);
        objectAdapter.add(btnOff);

        gridView = view.findViewById(R.id.gridView);
        checkButtonBridgeAdapter = new CheckButtonBridgeAdapter(objectAdapter);
        checkButtonBridgeAdapter.setListener(this);
        gridView.setAdapter(checkButtonBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        gridView.setVerticalSpacing(spacing);
    }

    private void updateLock(boolean isLock) {
        if (isLock == btnOn.isChecked) {
            return;
        }

        SettingProfileDialogFragment mSettingProfileDialogFragment = (SettingProfileDialogFragment) getParentFragment();
        mSettingProfileDialogFragment.updateProfileLockIcon(isLock);

        btnOn.setChecked(isLock);
        btnOff.setChecked(!isLock);

        objectAdapter.replace(btnOn.getType(), btnOn);
        objectAdapter.replace(btnOff.getType(), btnOff);
    }

    @Override
    public boolean onClickButton(LockCheckButtonButton item) {
        int type = item.getType();

        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + type);
        }

        if (type == OnOffValue.SET.getType()) {
            if (!btnOn.isChecked()) {
                SettingProfileLockDialogFragment settingProfileLockDialogFragment = SettingProfileLockDialogFragment.newInstance(getArguments());
                settingProfileLockDialogFragment.setOnLockChangeListener(
                        new SettingProfileLockDialogFragment.OnLockChangeListener() {
                            @Override
                            public void onLockChange(boolean isLock) {
                                if (isLock) {
                                    updateLock(isLock);
                                    JasminToast.makeToast(getContext(), R.string.saved);
                                    ProfileInfo profileInfo = getArguments().getParcelable(KEY_PROFILE);
                                    profileInfo.setLock(isLock);
                                    mOnPushProfileChangeMessage.onPushProfile(PushMessageReceiver.ProfileUpdateType.lock, profileInfo);
                                } else {
                                    gridView.scrollToPosition(btnOff.getType());
                                }

                                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        settingProfileLockDialogFragment.dismiss();
                                    }
                                }, 5);
                            }
                        });
                onFragmentChange.getTvOverlayManager().showDialogFragment(settingProfileLockDialogFragment);
            }
        } else if (type == OnOffValue.OFF.getType()) {
            if (!btnOff.isChecked()) {
                SettingProfileLockPinCheckDialogFragment settingProfileLockPinCheckDialogFragment = SettingProfileLockPinCheckDialogFragment.newInstance(getArguments());
                settingProfileLockPinCheckDialogFragment.setOnPinCheckCompleteListener(new SettingProfileLockPinCheckDialogFragment.OnPinCheckCompleteListener() {
                    @Override
                    public void onPinCheckComplete() {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onPinCheckComplete");
                        }

                        updateLock(false);
                        ProfileInfo profileInfo = getArguments().getParcelable(KEY_PROFILE);
                        profileInfo.setLock(false);
                        mOnPushProfileChangeMessage.onPushProfile(PushMessageReceiver.ProfileUpdateType.lock, profileInfo);
                        JasminToast.makeToast(getContext(), R.string.profile_unlocked);

                        settingProfileLockPinCheckDialogFragment.dismiss();
                    }
                });
                onFragmentChange.getTvOverlayManager().showDialogFragment(settingProfileLockPinCheckDialogFragment);
            }
        }

        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        onFragmentChange = null;
    }

    protected class LockCheckButtonButton implements CheckButtonPresenter.CheckButtonItem {

        private OnOffValue onOff;
        private boolean isChecked;

        LockCheckButtonButton(int type, boolean isChecked) {
            onOff = (type == OnOffValue.SET.getType()) ? OnOffValue.SET : OnOffValue.OFF;
            this.isChecked = isChecked;
        }

        @Override
        public boolean isEnabledCheck() {
            return true;
        }

        @Override
        public int getType() {
            return onOff.getType();
        }

        @Override
        public int getTitle() {
            return onOff.getTitle();
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

    }
}
