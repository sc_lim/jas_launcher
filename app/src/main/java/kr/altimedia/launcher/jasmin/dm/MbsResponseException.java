package kr.altimedia.launcher.jasmin.dm;

import androidx.annotation.Nullable;

/**
 * Created by mc.kim on 11,08,2020
 */
class MbsResponseException extends Exception {
    private final String code;
    private final String message;
    private final MbsUtil.ResponseType responseType;
    private final MbsUtil.ServerType serverType = MbsUtil.ServerType.MBS;

    public MbsResponseException(MbsUtil.ResponseType responseType, String code, String message) {
        super(message);
        this.responseType = responseType;
        this.code = code;
        this.message = message;
    }

    public MbsUtil.ResponseType getResponseType() {
        return responseType;
    }
    public MbsUtil.ServerType getServerType() {
        return serverType;
    }
    public boolean isNetworkError() {
        return responseType != MbsUtil.ResponseType.Connected;
    }

    public String getCode() {
        return code;
    }

    @Nullable
    @Override
    public String getMessage() {
        return message;
    }
}