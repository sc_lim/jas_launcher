package kr.altimedia.launcher.jasmin.ui.component.fragment.sleep;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.List;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import kr.altimedia.launcher.jasmin.R;

public class SleepModeDialog implements View.OnKeyListener, LifecycleObserver {
    private final String TAG = SleepModeDialog.class.getSimpleName();

    public static final String SETTINGS = "android.settings.SETTINGS";
    public static final String SCREENSAVER_SETTINGS = "android.settings.DREAM_SETTINGS";

    private boolean isShow = false;

    private WindowManager wm;
    private View parentsView;

    public SleepModeDialog(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        parentsView = inflater.inflate(R.layout.dialog_sleep_mode, null, false);
        initView(parentsView);
    }

    private void initView(View view) {
        TextView okButton = view.findViewById(R.id.ok_button);
        TextView settingsButton = view.findViewById(R.id.settings_button);

        okButton.setOnKeyListener(this);
        settingsButton.setOnKeyListener(this);
    }

    public void show(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "show");
        }

        isShow = true;

        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                PixelFormat.TRANSLUCENT);
        mParams.gravity = Gravity.CENTER;

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.addView(parentsView, mParams);
            }
        });
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void dismiss() {
        if (Log.INCLUDE) {
            Log.d(TAG, "dismiss");
        }

        isShow = false;

        ProcessLifecycleOwner.get().getLifecycle().removeObserver(this);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.removeView(parentsView);
            }
        });
    }

    public boolean isShow() {
        return isShow;
    }

    private void openScreenSaverSetting(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "openScreenSaverSetting");
        }

        Intent intent = new Intent(SCREENSAVER_SETTINGS);
        if (!intentAvailable(context, intent)) {
            // Try opening the daydream settings activity directly: https://gist.github.com/reines/bc798a2cb539f51877bb279125092104
            intent = new Intent(Intent.ACTION_MAIN).setClassName("com.android.tv.settings", "com.android.tv.settings.device.display.daydream.DaydreamActivity");
            if (!intentAvailable(context, intent)) {
                intent = new Intent(SETTINGS);
            }
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private boolean intentAvailable(Context context, Intent intent) {
        PackageManager manager = context.getPackageManager();
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        return !infos.isEmpty();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (v.getId() == R.id.settings_button) {
                    openScreenSaverSetting(v.getContext());
                }

                dismiss();
                return true;
            case KeyEvent.KEYCODE_BACK:
                dismiss();
                return true;
        }

        return false;
    }
}