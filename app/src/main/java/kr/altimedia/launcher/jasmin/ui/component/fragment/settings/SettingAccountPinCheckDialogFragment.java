package kr.altimedia.launcher.jasmin.ui.component.fragment.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment.KEY_PROFILE;

public class SettingAccountPinCheckDialogFragment extends SettingBaseProfileDialogFragment {
    public static final String CLASS_NAME = SettingAccountPinCheckDialogFragment.class.getName();

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private SettingTaskManager settingTaskManager;
    private MbsDataTask checkPinTask;
    private MbsDataTask profileLockTask;

    private PasswordView password;

    private OnPinCheckCompleteListener mOnPinCheckCompleteListener;

    public static SettingAccountPinCheckDialogFragment newInstance(Bundle bundle) {
        SettingAccountPinCheckDialogFragment fragment = new SettingAccountPinCheckDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setOnPinCheckCompleteListener(OnPinCheckCompleteListener mOnPinCheckCompleteListener) {
        this.mOnPinCheckCompleteListener = mOnPinCheckCompleteListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_setting_account_pin_check, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        settingTaskManager = new SettingTaskManager(getFragmentManager(), getTvOverlayManager());

        initProgressbar(view);
        initPasswordView(view);
        initButton(view);
    }

    protected void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    public void showProgressbar(boolean isLoading) {
        if (isLoading) {
            mProgressBarManager.show();
        } else {
            mProgressBarManager.hide();
        }
    }

    private void initPasswordView(View view) {
        password = view.findViewById(R.id.pin);
        password.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyInputChange, isFull : " + isFull + ", input : " + input);
                }

                if (isFull) {
                    checkPinCode(input);
                }
            }
        });
    }

    private void initButton(View view) {
        TextView cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void checkPinCode(String password) {
        ProfileInfo profileInfo = getArguments().getParcelable(KEY_PROFILE);
        checkPinTask = settingTaskManager.checkAccountPinCode(
                profileInfo.getProfileId(), password,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        showProgressbar(loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onFailed");
                        }

                        settingTaskManager.showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onSuccess, result : " + result);
                        }

                        if (result) {
                            offProfileLockCode();
                        } else {
                            setPinError();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        settingTaskManager.showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingAccountPinCheckDialogFragment.this.getContext();
                    }
                });
    }

    private void offProfileLockCode() {
        ProfileInfo profileInfo = getArguments().getParcelable(KEY_PROFILE);
        profileLockTask = settingTaskManager.putProfileLock(
                profileInfo.getProfileId(), "N", "",
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "needLoading");
                        }

                        showProgressbar(loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed");
                        }

                        settingTaskManager.showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSuccess, result : " + result);
                        }

                        mOnPinCheckCompleteListener.onPinCheckComplete();
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        settingTaskManager.showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingAccountPinCheckDialogFragment.this.getContext();
                    }
                });
    }


    private void setPinError() {
        getView().findViewById(R.id.description).setVisibility(View.VISIBLE);
        password.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (checkPinTask != null) {
            checkPinTask.cancel(true);
        }

        mOnPinCheckCompleteListener = null;
    }

    @Override
    protected ProfileInfo getProfileInfo() {
        return getArguments().getParcelable(KEY_PROFILE);
    }

    @Override
    protected void updateProfile(PushMessageReceiver.ProfileUpdateType type) {
        super.updateProfile(type);

        if (Log.INCLUDE) {
            Log.d(TAG, "updateProfile, type : " + type);
        }

        dismiss();
    }

    public interface OnPinCheckCompleteListener {
        void onPinCheckComplete();
    }
}
