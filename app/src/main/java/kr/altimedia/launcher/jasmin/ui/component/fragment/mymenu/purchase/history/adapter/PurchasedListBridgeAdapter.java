/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history.adapter;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ArrayObjectAdapter;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.PurchasedContent;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history.presenter.PurchasedItemPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;

public class PurchasedListBridgeAdapter extends ItemBridgeAdapter {
    private static final String TAG = PurchasedListBridgeAdapter.class.getSimpleName();
    private OnBindListener onBindListener;

    public PurchasedListBridgeAdapter(OnBindListener onBindListener) {
        this.onBindListener = onBindListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        PurchasedItemPresenter presenter = (PurchasedItemPresenter) viewHolder.mPresenter;
        PurchasedItemPresenter.OnKeyListener onKeyListener = presenter.getOnKeyListener();

        PurchasedItemPresenter.VodItemViewHolder itemViewHolder = (PurchasedItemPresenter.VodItemViewHolder) viewHolder.getViewHolder();

        int index = (int) viewHolder.itemView.getTag(R.id.KEY_INDEX);
        if (onKeyListener != null) {
            viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    PurchasedContent purchasedItem = (PurchasedContent) viewHolder.mItem;
                    PurchasedContent content = purchasedItem;
                    String title = content.getTitle();
                    Log.d(TAG, "onKey() item=" + title + ", index=" + index);

                    return onKeyListener.onKey(keyCode, index, purchasedItem, itemViewHolder);
                }
            });
        }

        onBindListener.onBind(index);
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        viewHolder.mHolder.view.setOnKeyListener(null);
    }

    public ArrayList<PurchasedContent> getList() {
        ArrayList<PurchasedContent> list = new ArrayList<>();
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) getAdapter();
        for (int i = 0; i < objectAdapter.size(); i++) {
            PurchasedContent purchasedItem = (PurchasedContent) objectAdapter.get(i);
            list.add(purchasedItem);
        }

        return list;
    }

    public interface OnBindListener {
        void onBind(int index);
    }
}
