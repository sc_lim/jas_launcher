/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import kr.altimedia.launcher.jasmin.ui.view.browse.RowsSupportFragment;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class VodRowFragment extends RowsSupportFragment {
    private final String TAG = VodRowFragment.class.getSimpleName();
    private static final String KEY_PADDING_RECT = "padding";

    private VerticalGridView.OnScrollOffsetCallback mOnScrollOffsetCallback;
    private BaseOnItemViewSelectedListener baseOnItemViewSelectedListener;

    public static VodRowFragment newInstance(Rect rect) {
        VodRowFragment fragment = new VodRowFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_PADDING_RECT, rect);
        fragment.setArguments(args);
        return fragment;
    }

    public VodRowFragment() {
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Rect rect = getArguments().getParcelable(KEY_PADDING_RECT);
        mVerticalGridView.setPadding(rect.left, rect.top, rect.right, rect.bottom);

        setOnScrollOffsetCallback(mOnScrollOffsetCallback);
        setOnItemViewSelectedListener(baseOnItemViewSelectedListener);
    }


    public void setOnScrollOffsetCallback(VerticalGridView.OnScrollOffsetCallback onScrollOffsetCallback) {
        if (mVerticalGridView == null) {
            mOnScrollOffsetCallback = onScrollOffsetCallback;
            return;
        }
        mVerticalGridView.setOnScrollOffsetCallback(onScrollOffsetCallback);

        if (onScrollOffsetCallback instanceof BaseShadowHelper) {
            ((BaseShadowHelper) onScrollOffsetCallback).setParentView(mVerticalGridView);
        }
    }

    @Override
    public void setOnItemViewSelectedListener(BaseOnItemViewSelectedListener listener) {
        if (mVerticalGridView == null) {
            baseOnItemViewSelectedListener = listener;
        }

        super.setOnItemViewSelectedListener(listener);
    }

    @Override
    protected void setDivider() {
        if (dividerResourceId != -1) {
            VodRowDividerItemDecoration vodRowDividerItemDecoration = new VodRowDividerItemDecoration(getContext());
            vodRowDividerItemDecoration.setDrawable(dividerResourceId);
            mVerticalGridView.addItemDecoration(vodRowDividerItemDecoration);
        }
    }

    @Override
    public void onDestroyView() {
        mVerticalGridView.setOnScrollOffsetCallback(null);
        setOnItemViewSelectedListener(null);

        super.onDestroyView();
    }

    private class VodRowDividerItemDecoration extends RecyclerView.ItemDecoration {
        private Drawable divider;
        private Context mContext;

        public VodRowDividerItemDecoration(Context context) {
            mContext = context;
        }

        public void setDrawable(int resourceId) {
            divider = mContext.getResources().getDrawable(resourceId);
        }

        @Override
        public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount - 1; i++) {
                View child = parent.getChildAt(i);

                int top = child.getBottom();
                int bottom = top + divider.getIntrinsicHeight();

                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        }
    }
}
