/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.def;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class CategoryTypeDeserializer implements JsonDeserializer<CategoryType>, JsonSerializer<CategoryType> {
    private final String TAG = CategoryTypeDeserializer.class.getSimpleName();


    @Override
    public JsonElement serialize(CategoryType src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src.categoryType);
    }

    @Override
    public CategoryType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        boolean isNull = json.isJsonNull();
        if (isNull) {
            return CategoryType.SubCategory;
        }
        return CategoryType.findByType(json.getAsString());
    }
}
