/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.view.View;
import android.view.ViewGroup;

import androidx.core.view.ViewCompat;

import kr.altimedia.launcher.jasmin.ui.view.util.LeanbackTransitionHelper;
import kr.altimedia.launcher.jasmin.ui.view.util.TransitionHelper;


public class TitleHelper {
    ViewGroup mSceneRoot;
    View mTitleView;
    private Object mTitleUpTransition;
    private Object mTitleDownTransition;
    private Object mSceneWithTitle;
    private Object mSceneWithoutTitle;
    private final BrowseFrameLayout.OnFocusSearchListener mOnFocusSearchListener = new BrowseFrameLayout.OnFocusSearchListener() {
        public View onFocusSearch(View focused, int direction) {
            if (focused != TitleHelper.this.mTitleView && direction == 33) {
                return TitleHelper.this.mTitleView;
            } else {
                boolean isRtl = ViewCompat.getLayoutDirection(focused) == ViewCompat.LAYOUT_DIRECTION_RTL;
                int forward = isRtl ? 17 : 66;
                return !TitleHelper.this.mTitleView.hasFocus() || direction != 130 && direction != forward ? null : TitleHelper.this.mSceneRoot;
            }
        }
    };

    public TitleHelper(ViewGroup sceneRoot, View titleView) {
        if (sceneRoot != null && titleView != null) {
            this.mSceneRoot = sceneRoot;
            this.mTitleView = titleView;
            this.createTransitions();
        } else {
            throw new IllegalArgumentException("Views may not be null");
        }
    }

    private void createTransitions() {
        this.mTitleUpTransition = LeanbackTransitionHelper.loadTitleOutTransition(this.mSceneRoot.getContext());
        this.mTitleDownTransition = LeanbackTransitionHelper.loadTitleInTransition(this.mSceneRoot.getContext());
        this.mSceneWithTitle = TransitionHelper.createScene(this.mSceneRoot, new Runnable() {
            public void run() {
                TitleHelper.this.mTitleView.setVisibility(View.VISIBLE);
            }
        });
        this.mSceneWithoutTitle = TransitionHelper.createScene(this.mSceneRoot, new Runnable() {
            public void run() {
                TitleHelper.this.mTitleView.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void showTitle(boolean show) {
        if (show) {
            TransitionHelper.runTransition(this.mSceneWithTitle, this.mTitleDownTransition);
        } else {
            TransitionHelper.runTransition(this.mSceneWithoutTitle, this.mTitleUpTransition);
        }

    }

    public ViewGroup getSceneRoot() {
        return this.mSceneRoot;
    }

    public View getTitleView() {
        return this.mTitleView;
    }

    public BrowseFrameLayout.OnFocusSearchListener getOnFocusSearchListener() {
        return this.mOnFocusSearchListener;
    }
}
