package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;

public class VodPaymentCancelDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = VodPaymentCancelDialogFragment.class.getName();

    private static final String key_PURCHASE_INFO = "PURCHASE_INFO";

    private OnClickButtonListener onClickButtonListener;

    public static VodPaymentCancelDialogFragment newInstance(PurchaseInfo purchaseInfo) {

        Bundle args = new Bundle();
        args.putParcelable(key_PURCHASE_INFO, purchaseInfo);

        VodPaymentCancelDialogFragment fragment = new VodPaymentCancelDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnClickButtonListener(OnClickButtonListener onClickButtonListener) {
        this.onClickButtonListener = onClickButtonListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {
                if (onClickButtonListener != null) {
                    onClickButtonListener.onclickDoNotCancelPayment();
                }
            }
        };
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_vod_payment_cancel, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        PurchaseInfo purchaseInfo = bundle.getParcelable(key_PURCHASE_INFO);

        initView(purchaseInfo, view);
        initButton(view);
    }

    private void initView(PurchaseInfo purchaseInfo, View view) {
        TextView titleView = view.findViewById(R.id.title);
        TextView paymentType = view.findViewById(R.id.payment_type);

        titleView.setText(purchaseInfo.getTitle());
        paymentType.setText(purchaseInfo.getPaymentType().getDescriptionName());
    }

    private void initButton(View view) {
        TextView cancelPaymentButton = view.findViewById(R.id.cancel_payment_button);
        TextView doNotCancelPaymentButton = view.findViewById(R.id.do_not_cancel_payment_button);

        cancelPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickButtonListener != null) {
                    onClickButtonListener.onClickCancelPayment();
                }
            }
        });

        doNotCancelPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickButtonListener != null) {
                    onClickButtonListener.onclickDoNotCancelPayment();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onClickButtonListener = null;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public interface OnClickButtonListener {
        void onClickCancelPayment();

        void onclickDoNotCancelPayment();
    }
}
