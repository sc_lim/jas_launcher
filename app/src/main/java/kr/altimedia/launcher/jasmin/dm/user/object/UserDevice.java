/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import kr.altimedia.launcher.jasmin.dm.def.NormalDateTimeDeserializer2;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class UserDevice implements Parcelable {
    public static final Creator<UserDevice> CREATOR = new Creator<UserDevice>() {
        @Override
        public UserDevice createFromParcel(Parcel in) {
            return new UserDevice(in);
        }

        @Override
        public UserDevice[] newArray(int size) {
            return new UserDevice[size];
        }
    };
    @Expose
    @SerializedName("deviceId")
    private String deviceId;
    @Expose
    @SerializedName("deviceName")
    private String deviceName;
    @Expose
    @SerializedName("firstLoginTime")
    @JsonAdapter(NormalDateTimeDeserializer2.class)
    private Date firstLoginDate;
    @Expose
    @SerializedName("isDeleteDisabled")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isDeleteDisabled;

    protected UserDevice(Parcel in) {
        deviceId = in.readString();
        deviceName = in.readString();
        firstLoginDate = (Date) in.readValue(Date.class.getClassLoader());
        isDeleteDisabled = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(deviceId);
        dest.writeString(deviceName);
        dest.writeValue(firstLoginDate);
        dest.writeByte((byte) (isDeleteDisabled ? 1 : 0));
    }

    @Override
    public String toString() {
        return "UserDevice{" +
                "deviceId='" + deviceId + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", firstLoginDate=" + firstLoginDate +
                ", isDeleteDisabled=" + isDeleteDisabled +
                '}';
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Date getFirstLoginDate() {
        return firstLoginDate;
    }

    public boolean isDeleteDisabled() {
        return isDeleteDisabled;
    }
}
