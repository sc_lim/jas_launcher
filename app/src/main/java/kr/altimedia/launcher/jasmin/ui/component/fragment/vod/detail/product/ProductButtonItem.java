/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product;

import android.os.Parcel;
import android.os.Parcelable;

import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;


public class ProductButtonItem implements Parcelable {
    private Product product;
    private VodProductType vodProductType;
    private float value;
    private String unit;

    public ProductButtonItem(VodProductType vodProductType, int value, String unit) {
        this(null, vodProductType, value, unit);
    }

    public ProductButtonItem(Product product, VodProductType vodProductType, float value, String unit) {
        this.product = product;
        this.vodProductType = vodProductType;
        this.value = value;
        this.unit = unit;
    }

    protected ProductButtonItem(Parcel in) {
        product = (Product) in.readValue(Product.class.getClassLoader());
        vodProductType = (VodProductType) in.readValue(VodProductType.class.getClassLoader());
        value = (float) in.readValue(Float.class.getClassLoader());
        unit = (String) in.readValue(String.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(product);
        dest.writeValue(vodProductType);
        dest.writeValue(value);
        dest.writeValue(unit);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProductButtonItem> CREATOR = new Creator<ProductButtonItem>() {
        @Override
        public ProductButtonItem createFromParcel(Parcel in) {
            return new ProductButtonItem(in);
        }

        @Override
        public ProductButtonItem[] newArray(int size) {
            return new ProductButtonItem[size];
        }
    };

    @Override
    public String toString() {
        return "ProductButtonItem{" +
                "product=" + product +
                ", vodProductType=" + vodProductType +
                ", value=" + value +
                ", unit='" + unit + '\'' +
                '}';
    }

    public Product getProduct() {
        return product;
    }

    public VodProductType getProductType() {
        return vodProductType;
    }

    public float getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }
}