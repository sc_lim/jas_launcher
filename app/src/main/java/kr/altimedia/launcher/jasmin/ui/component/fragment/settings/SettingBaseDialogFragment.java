package kr.altimedia.launcher.jasmin.ui.component.fragment.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PROFILE;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_PROFILE_ID;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_TYPE;
import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment.KEY_FROM_SETTING;

public class SettingBaseDialogFragment extends SafeDismissDialogFragment implements View.OnUnhandledKeyEventListener {
    private final String TAG = SettingBaseDialogFragment.class.getSimpleName();

    private View parentView;

    protected LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver mPushMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive, Action : " + intent.getAction());
            }

            receivePushMessage(intent);
        }
    };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parentView = view;
        parentView.addOnUnhandledKeyEventListener(this);
        registerPushReceiver();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        parentView.removeOnUnhandledKeyEventListener(this);
        unRegisterPushReceiver();
    }

    protected void receivePushMessage(Intent mIntent) {
        boolean isFromSetting = mIntent.getBooleanExtra(KEY_FROM_SETTING, false);
        if (Log.INCLUDE) {
            Log.d(TAG, "receivePushMessage, isFromSetting : " + isFromSetting);
        }

        if (isFromSetting) {
            return;
        }

        Bundle mBundle = mIntent.getExtras();
        Serializable mSerializable = mBundle.getSerializable(KEY_TYPE);

        if (Log.INCLUDE) {
            Log.d(TAG, "receivePushMessage, mSerializable : " + mSerializable);
        }

        if (mSerializable != null) {
            if (mSerializable instanceof PushMessageReceiver.ProfileUpdateType) {
                String profileId = StringUtils.nullToEmpty(mBundle.getString(KEY_PROFILE_ID));
                PushMessageReceiver.ProfileUpdateType type = (PushMessageReceiver.ProfileUpdateType) mSerializable;
                receivePushProfile(profileId, type);
            }
        }
    }

    protected void receivePushProfile(String profileId, PushMessageReceiver.ProfileUpdateType type) {

    }

    private void registerPushReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_UPDATED_PROFILE);

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        mLocalBroadcastManager.registerReceiver(mPushMessageReceiver, filter);
    }

    private void unRegisterPushReceiver() {
        mLocalBroadcastManager.unregisterReceiver(mPushMessageReceiver);
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (keyCode == KeyEvent.KEYCODE_GUIDE || keyCode == KeyEvent.KEYCODE_TV) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                return getActivity().onKeyDown(keyCode, event);
            } else {
                return getActivity().onKeyUp(keyCode, event);
            }
        }
        return false;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }
}
