/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.obj;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VodPaymentResult implements Parcelable {
    @Expose
    @SerializedName("paymentStatus")
    private List<PaymentStatus> paymentStatusList;
    @Expose
    @SerializedName("successYn")
    private String isSuccess;


    protected VodPaymentResult(Parcel in) {
        paymentStatusList = in.createTypedArrayList(PaymentStatus.CREATOR);
        isSuccess = in.readString();
    }

    public static final Creator<VodPaymentResult> CREATOR = new Creator<VodPaymentResult>() {
        @Override
        public VodPaymentResult createFromParcel(Parcel in) {
            return new VodPaymentResult(in);
        }

        @Override
        public VodPaymentResult[] newArray(int size) {
            return new VodPaymentResult[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(paymentStatusList);
        dest.writeString(isSuccess);
    }

    @NonNull
    @Override
    public String toString() {
        return "VodPaymentResult{" +
                "paymentStatusList='" + paymentStatusList + '\'' +
                ", isSuccess='" + isSuccess + '\'' +
                '}';
    }

    public List<PaymentStatus> getPaymentStatusList() {
        return paymentStatusList;
    }

    public boolean isSuccess() {
        return isSuccess.equalsIgnoreCase("y");
    }
}
