package kr.altimedia.launcher.jasmin.dm;

import androidx.annotation.Nullable;

import retrofit2.Response;

/**
 * Created by mc.kim on 12,08,2020
 */
public class AuthenticationException extends Exception {
    private final String code;
    private final String message;
    private final Response response;

    public AuthenticationException(Response response, String code, String message) {
        super(message);
        this.response = response;
        this.code = code;
        this.message = message;
    }

    public Response getResponse() {
        return response;
    }

    public String getCode() {
        return code;
    }


    @Nullable
    @Override
    public String getMessage() {
        return message;
    }
}
