/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.dca;

import android.animation.AnimatorInflater;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildSelectedListener;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.dca.presenter.DCAPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class DCADialogFragment extends SafeDismissDialogFragment
        implements DialogInterface.OnKeyListener, DCAItemBridgeAdapter.OnDCAClickListener, ValueAnimator.AnimatorUpdateListener {
    public static final String CLASS_NAME = DCADialogFragment.class.getName();
    private final String TAG = DCADialogFragment.class.getSimpleName();

    private static final String INPUT_KEY_CODE = "KEY_CODE";

    private final int VISIBLE_COUNT = 3;
    private final int HANDLE_ID = 0;

    private String userInput = "";
    private TextView dcaView;
    private VerticalGridView dcaList;
    private ImageIndicator upIndicator;
    private ImageIndicator dnIndicator;

    private ArrayList<Channel> allChannelList = new ArrayList<>();

    private ValueAnimator visibleAnimator;
    private ValueAnimator invisibleDownAnimator;
    private final long KEY_DELAY_TIME = TimeUtil.SEC * 3;
    private final Handler keyHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (Log.INCLUDE) {
                Log.d(TAG, "call handleMessage");
            }

            tuneToChannel();
        }
    };
    private LinearLayout layout;

    public DCADialogFragment() {
    }

    public static DCADialogFragment newInstance(int keyCode) {

        Bundle args = new Bundle();
        args.putInt(INPUT_KEY_CODE, keyCode);

        DCADialogFragment fragment = new DCADialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DCA;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_dca, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setAllChannelList(view.getContext());
        initView(view);
        initAnimator(view);
        initDCA();
    }

    private void initView(View view) {
        dcaView = view.findViewById(R.id.dca);
        dcaList = view.findViewById(R.id.dca_list);
        upIndicator = view.findViewById(R.id.upIndicator);
        dnIndicator = view.findViewById(R.id.dnIndicator);

        getDialog().setOnKeyListener(this);
    }

    private void initAnimator(View view) {
        layout = view.findViewById(R.id.layout);

        Context context = view.getContext();
        visibleAnimator = loadAnimator(context, R.animator.dca_visible);
        visibleAnimator.setInterpolator(new AccelerateInterpolator());
        visibleAnimator.addUpdateListener(this);

        invisibleDownAnimator = loadAnimator(context, R.animator.dca_invisible);
        invisibleDownAnimator.setInterpolator(new DecelerateInterpolator());
        invisibleDownAnimator.addUpdateListener(this);

        visibleAnimator.start();
    }

    private void initDCA() {
        String input = getKeyValue(getArguments().getInt(INPUT_KEY_CODE));
        userInput = input;

        if (Log.INCLUDE) {
            Log.d(TAG, "initDCA, userInput : " + userInput);
        }

        setNumberFormat(input);
        setRelatedChannelList(input);
        keyHandler.sendEmptyMessageDelayed(HANDLE_ID, KEY_DELAY_TIME);
    }

    private ValueAnimator loadAnimator(Context context, int resId) {
        ValueAnimator animator = (ValueAnimator) AnimatorInflater.loadAnimator(context, resId);
        return animator;
    }

    private void setAllChannelList(Context context) {
        TvSingletons mTvSingletons = TvSingletons.getSingletons(context);
        allChannelList = (ArrayList<Channel>) mTvSingletons.getChannelDataManager().getBrowsableChannelList();
    }

    private void setRelatedChannelList(String input) {
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(new DCAPresenter());
        ArrayList<Channel> relatedList = getRelatedChannelList(input);
        objectAdapter.addAll(0, relatedList);

        DCAItemBridgeAdapter dcaItemBridgeAdapter = new DCAItemBridgeAdapter(objectAdapter);
        dcaItemBridgeAdapter.setOnDCAClickListener(this);
        dcaList.setAdapter(dcaItemBridgeAdapter);

        upIndicator.initIndicator(relatedList.size(), VISIBLE_COUNT);
        dnIndicator.initIndicator(relatedList.size(), VISIBLE_COUNT);

        dcaList.setOnChildSelectedListener(new OnChildSelectedListener() {
            @Override
            public void onChildSelected(ViewGroup parent, View view, int position, long id) {
                upIndicator.updateArrow(position);
                dnIndicator.updateArrow(position);
            }
        });

        int gap = (int) getContext().getResources().getDimension(R.dimen.dca_channel_vertical_gap);
        dcaList.setVerticalSpacing(gap);
    }

    private ArrayList<Channel> getRelatedChannelList(String input) {
        ArrayList<Channel> list = new ArrayList<>();

        if (input.isEmpty()) {
            return allChannelList;
        }

        for (Channel channel : allChannelList) {
            String channelNum = String.format("%03d", Integer.parseInt(channel.getDisplayNumber()));
            if (channelNum.contains(input)) {
                list.add(channel);
            }
        }

        return list;
    }

    private void tuneToChannel() {
        String channelNumber = getNumString(userInput);
        if (Log.INCLUDE) {
            Log.d(TAG, "tuneToChannel, channelNumber : " + channelNumber);
        }

        tuneToChannel(channelNumber);
    }

    private void tuneToChannel(String channelNumber) {
        if (channelNumber.isEmpty()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "tuneToChannel, channel is Empty, so dismiss DCA");
            }

            dismiss();
            return;
        }

        int num = Integer.parseInt(channelNumber);
        if (num > 0) {
            for (Channel channel : allChannelList) {
                int channelNum = Integer.parseInt(channel.getDisplayNumber());
                if (num == channelNum) {
                    tuneToChannel(channel);
                    return;
                }
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "tuneToChannel, there is no channelNumber : " + num + ", so dismiss DCA");
        }

        dismiss();
    }

    private void tuneToChannel(Channel channel) {
        keyHandler.removeMessages(HANDLE_ID);
        if (Log.INCLUDE) {
            Log.d(TAG, "tuneToChannel, channel : " + channel);
        }
        FragmentActivity activity = getActivity();

        if (activity instanceof LiveTvActivity) {
            ((LiveTvActivity) activity).hideProgramGuide();
            ((LiveTvActivity) activity).tuneChannel(channel);
        } else if (activity instanceof LauncherActivity) {
            PlaybackUtil.startLiveTvActivityWithChannel(activity, channel.getId());
        } else if (activity instanceof VideoPlaybackActivity) {
            ((VideoPlaybackActivity) activity).notifyTuneChannel(channel);
        }
        dismiss();
    }

    private void setNumberFormat(String input) {
        dcaView.setText(input);
    }

    private String getNumString(String input) {
        if (input.isEmpty()) {
            return "";
        }

        int num = Integer.parseInt(input);
        return String.valueOf(num);
    }

    private void deleteInput() {
        int size = userInput.length();

        if (size <= 0) {
            return;
        }

        int deletedIndex = size > 0 ? size - 1 : 0;
        userInput = userInput.substring(0, deletedIndex);
        setNumberFormat(userInput);

        String currentNum = getNumString(userInput);
        setRelatedChannelList(currentNum);
    }

    private String getKeyValue(int keyCode) {
        return String.valueOf(keyCode - 7);
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        boolean isConsumed = false;
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                invisibleDownAnimator.start();
                return true;
            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
                isConsumed = true;

                userInput += getKeyValue(keyCode);

                if (Log.INCLUDE) {
                    Log.d(TAG, "KEYCODE_NUM, userInput : " + userInput);
                }

                if (userInput.length() == 3) {
                    tuneToChannel(userInput);
                    return true;
                }

                setNumberFormat(userInput);
                setRelatedChannelList(userInput);

                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                isConsumed = true;
                deleteInput();

                if (Log.INCLUDE) {
                    Log.d(TAG, "KEYCODE_DPAD_LEFT, userInput : " + userInput);
                }
                break;
        }

        if (isConsumed || keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
            keyHandler.removeMessages(HANDLE_ID);
            keyHandler.sendEmptyMessageDelayed(HANDLE_ID, KEY_DELAY_TIME);
        }


        return isConsumed;
    }

    @Override
    public void onClickDCAItem(Channel channel) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickDCAItem, channel : " + channel);
        }

        tuneToChannel(channel);
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        layout.setAlpha(value);

        if (value == 0 && animation == invisibleDownAnimator) {
            dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        getDialog().setOnKeyListener(null);
        keyHandler.removeMessages(HANDLE_ID);

        super.onDestroyView();
    }
}
