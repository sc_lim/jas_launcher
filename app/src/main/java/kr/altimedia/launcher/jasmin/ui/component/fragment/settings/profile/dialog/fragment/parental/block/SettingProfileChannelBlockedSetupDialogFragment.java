/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.block;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.BrowseFrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseProfileDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.ChannelCheckButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.TextButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.ChannelCheckboxPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.TextButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.favorite.ChannelItem;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.ChannelCheckButtonBridgeAdapter.NUMBER_ROWS;

public class SettingProfileChannelBlockedSetupDialogFragment extends SettingBaseProfileDialogFragment implements SettingBaseButtonItemBridgeAdapter.OnClickButton {
    public static final String CLASS_NAME = SettingProfileChannelBlockedSetupDialogFragment.class.getName();
    private static final String TAG = SettingProfileChannelBlockedSetupDialogFragment.class.getSimpleName();

    private static final String KEY_PROFILE_INFO = "PROFILE_INFO";
    private static final String KEY_BLOCKED_LIST = "BLOCKED_LIST";

    private ProfileInfo profileInfo;

    private ArrayList<ChannelItem> channelList = new ArrayList<>();
    private ArrayList<ChannelItem> blockedList = new ArrayList<>();

    private TextView selectedCountView;
    private PagingVerticalGridView blockGridView;
    private VerticalGridView buttonsGridView;

    private ArrayObjectAdapter listObjectAdapter;
    private ArrayObjectAdapter buttonObjectAdapter;
    private TextButtonBridgeAdapter textButtonBridgeAdapter;
    private ChannelCheckButtonBridgeAdapter channelCheckButtonBridgeAdapter;

    private OnBlockedChannelChangeResultCallback onBlockedChannelChangeResultCallback;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private SettingTaskManager settingTaskManager;
    private MbsDataTask blockedTask;

    private SettingProfileChannelBlockedSetupDialogFragment() {

    }

    public static SettingProfileChannelBlockedSetupDialogFragment newInstance(ProfileInfo profileInfo, ArrayList<String> blockedList) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_PROFILE_INFO, profileInfo);
        //args.putStringArray(KEY_BLOCKED_LIST, Strings.toArray(blockedList));
        args.putStringArray(KEY_BLOCKED_LIST, blockedList.toArray(new String[]{}));

        SettingProfileChannelBlockedSetupDialogFragment fragment = new SettingProfileChannelBlockedSetupDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnBlockedChannelChangeResultCallback(OnBlockedChannelChangeResultCallback onBlockedChannelChangeResultCallback) {
        this.onBlockedChannelChangeResultCallback = onBlockedChannelChangeResultCallback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            LauncherActivity launcherActivity = (LauncherActivity) context;
            ArrayList<Channel> originList = (ArrayList<Channel>) launcherActivity.getBrowsableChannelList();

            for (Channel channel : originList) {
                ChannelItem channelItem = new ChannelItem(channel, false);
                channelList.add(channelItem);
            }

            initData();
        }
    }

    private void initData() {
        Bundle bundle = getArguments();
        profileInfo = bundle.getParcelable(KEY_PROFILE_INFO);

        setBlockedList(getOriginalList());
    }

    private ArrayList<String> getOriginalList() {
        //long[] blockedList = getArguments().getLongArray(KEY_BLOCKED_LIST);
        String[] blockedList = getArguments().getStringArray(KEY_BLOCKED_LIST);
        ArrayList<String> list = new ArrayList<>();
        if (blockedList.length > 0) {
            Collections.addAll(list, blockedList);
        }

        return list;
    }

    private void setBlockedList(ArrayList<String> blockedChannels) {
        if (blockedChannels != null && blockedChannels.size() > 0) {
            for (String channelNumber : blockedChannels) {
                for (ChannelItem channelItem : channelList) {
                    String serviceId = channelItem.getChannel().getServiceId();
                    if (channelNumber.equals(serviceId)) {
                        channelItem.setChecked(true);
                        blockedList.add(channelItem);
                        continue;
                    }
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_channel_blocked_setup, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        settingTaskManager = new SettingTaskManager(getFragmentManager(), mTvOverlayManager);
        initProgressbar(view);
        initCount(view);
        initChannelGridView(view);
        initButtons(view);
        initLayout(view);
    }

    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void showProgress() {
        mProgressBarManager.show();
    }

    private void hideProgress() {
        if (mProgressBarManager != null) {
            mProgressBarManager.hide();
        }
    }

    private void initCount(View view) {
        selectedCountView = view.findViewById(R.id.selected_count);
        selectedCountView.setText(getFormattedNumber(blockedList.size()));

        TextView allCountView = view.findViewById(R.id.channels_count);
        String count = String.format("%03d", channelList.size());
        allCountView.setText(count);
    }

    private String getFormattedNumber(int number) {
        if (number < 10) {
            return "00" + number;
        } else if (number < 100) {
            return "0" + number;
        } else {
            return String.valueOf(number);
        }
    }

    private void initChannelGridView(View view) {
        blockGridView = view.findViewById(R.id.channel_gridView);
        if (channelList.size() == 0) {
            blockGridView.setFocusable(false);
            blockGridView.setFocusableInTouchMode(false);
            blockGridView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            return;
        }

        blockGridView.initVertical(NUMBER_ROWS);
        blockGridView.setNumColumns(ChannelCheckButtonBridgeAdapter.NUMBER_COLUMNS);

        ChannelCheckboxPresenter channelCheckboxPresenter = new ChannelCheckboxPresenter();
        listObjectAdapter = new ArrayObjectAdapter(channelCheckboxPresenter);
        listObjectAdapter.addAll(0, channelList);

        channelCheckButtonBridgeAdapter = new ChannelCheckButtonBridgeAdapter(listObjectAdapter, blockGridView);
        channelCheckButtonBridgeAdapter.setListener(this);
        blockGridView.setAdapter(channelCheckButtonBridgeAdapter);

        ThumbScrollbar scrollbar = view.findViewById(R.id.scrollbar);
        scrollbar.setList(getRowIndex(channelList.size()), NUMBER_ROWS);
        scrollbar.setFocusable(false);
        scrollbar.setFocusableInTouchMode(false);
        blockGridView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int focus = blockGridView.getChildPosition(recyclerView.getFocusedChild());
                scrollbar.moveFocus(getRowIndex(focus + 1) - 1);
            }
        });

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        blockGridView.setVerticalSpacing(spacing);
    }

    private int getRowIndex(int index) {
        int row = index / ChannelCheckButtonBridgeAdapter.NUMBER_COLUMNS;
        if (index % ChannelCheckButtonBridgeAdapter.NUMBER_COLUMNS > 0) {
            row += 1;
        }

        return row;
    }

    private void initLayout(View view) {
        BrowseFrameLayout browseFrameLayout = view.findViewById(R.id.browse_frame);
        browseFrameLayout.setOnFocusSearchListener(new BrowseFrameLayout.OnFocusSearchListener() {
            @Override
            public View onFocusSearch(View focused, int direction) {
                if (direction == View.FOCUS_RIGHT && focused.getParent() == blockGridView) {
                    return buttonsGridView.getChildAt(BlockButton.SAVE.getType());
                }

                return null;
            }
        });
    }

    private void toastSaveBlockedList() {
        JasminToast.makeToast(getContext(), R.string.saved);
    }

    private void toastExceededBlockedList() {
        JasminToast.makeToast(getContext(), R.string.channel_blocked_exceeded);
    }

    private void toastResetComplete() {
        JasminToast.makeToast(getContext(), R.string.channel_blocked_reset_complete);
    }

    private void initButtons(View view) {
        boolean isEnabled = blockedList.size() > 0;
        BlockButton buttonReset = BlockButton.RESET;
        buttonReset.setEnabled(isEnabled);

        TextButtonPresenter buttonPresenter = new TextButtonPresenter();
        buttonObjectAdapter = new ArrayObjectAdapter(buttonPresenter);
        ArrayList<BlockButton> list = new ArrayList<>(Arrays.asList(buttonReset, BlockButton.SAVE, BlockButton.CANCEL));
        buttonObjectAdapter.addAll(0, list);

        buttonsGridView = view.findViewById(R.id.buttons_gridView);
        textButtonBridgeAdapter = new TextButtonBridgeAdapter(buttonObjectAdapter);
        textButtonBridgeAdapter.setListener(this);
        buttonsGridView.setAdapter(textButtonBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_basic_button_vertical_spacing);
        buttonsGridView.setVerticalSpacing(spacing);
    }

    @Override
    public boolean onClickButton(Object item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        if (item instanceof BlockButton) {
            return OnClickButtonItem((BlockButton) item);
        } else if (item instanceof ChannelItem) {
            return onClickChannelItem((ChannelItem) item);
        }

        return false;
    }

    private boolean isSaved = false;
    private boolean OnClickButtonItem(BlockButton button) {
        int type = button.getType();

        if (type == BlockButton.RESET.getType()) {
            resetBlock();
            return true;
        } else if (type == BlockButton.SAVE.getType()) {
            if (!isSaved) {
                isSaved = true;
                saveBlock();
            }
        } else if (type == BlockButton.CANCEL.getType()) {
            cancelBlock();
            onBlockedChannelChangeResultCallback.onChangeBlockedChannel(false, getOriginalList());
        }

        return true;
    }

    private boolean onClickChannelItem(ChannelItem channel) {
        if (blockedList.contains(channel)) {
            channel.setChecked(false);
            blockedList.remove(channel);
        } else {
            channel.setChecked(true);
            blockedList.add(channel);
        }

        updateView();

        return true;
    }

    private void updateView() {
        int count = blockedList.size();
        selectedCountView.setText(getFormattedNumber(count));

        boolean isEnabled = blockedList.size() > 0;
        if (isEnabled != BlockButton.RESET.isEnabled()) {
            BlockButton.RESET.setEnabled(isEnabled);
            buttonObjectAdapter.replace(BlockButton.RESET.getType(), BlockButton.RESET);
        }
    }

    private void resetBlock() {
        SettingProfileChannelBlockedResetDialogFragment settingProfileChannelBlockedResetDialogFragment = SettingProfileChannelBlockedResetDialogFragment.newInstance(profileInfo);
        settingProfileChannelBlockedResetDialogFragment.setOnResetBlockedChannelResultCallback(new SettingProfileChannelBlockedResetDialogFragment.OnResetBlockedChannelResultCallback() {
            @Override
            public void onChangeResetBlockedChannel(boolean isReset) {
                if (isReset) {
                    for (ChannelItem channel : blockedList) {
                        channel.setChecked(false);
                        int idx = channelList.indexOf(channel);
                        listObjectAdapter.replace(idx, channel);
                    }

                    blockGridView.setSelectedPosition(0);
                    blockedList.clear();
                    updateView();

                    toastResetComplete();
                }

                settingProfileChannelBlockedResetDialogFragment.dismiss();
            }
        });

        getTvOverlayManager().showDialogFragment(settingProfileChannelBlockedResetDialogFragment);
    }

    private void saveBlock() {
        ArrayList<String> list = new ArrayList<>();
        for (ChannelItem channelItem : blockedList) {
            String serviceId = channelItem.getChannel().getServiceId();
            list.add(serviceId);
        }

        blockedTask = settingTaskManager.postProfileBlockedChannel(
                profileInfo.getProfileId(),
                list.toArray(new String[]{}),
                new MbsDataProvider<String, List<String>>() {

                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "saveBlock, onFailed");
                        }

                        isSaved = false;
                        settingTaskManager.showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, List<String> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "saveBlock, onSuccess");
                        }

                        toastSaveBlockedList();
                        if (onBlockedChannelChangeResultCallback != null) {
                            onBlockedChannelChangeResultCallback.onChangeBlockedChannel(true, result);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        isSaved = false;
                        settingTaskManager.showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingProfileChannelBlockedSetupDialogFragment.this.getContext();
                    }
                });

    }

    private void cancelBlock() {
        blockedList.clear();
    }

    @Override
    public ProfileInfo getProfileInfo() {
        return profileInfo;
    }

    @Override
    protected void updateProfile(PushMessageReceiver.ProfileUpdateType type) {
        super.updateProfile(type);

        if (Log.INCLUDE) {
            Log.d(TAG, "updateProfile, type : " + type);
        }

        dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (blockedTask != null) {
            blockedTask.cancel(true);
        }
        textButtonBridgeAdapter.setListener(null);
        if (channelCheckButtonBridgeAdapter != null) {
            channelCheckButtonBridgeAdapter.setListener(null);
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onBlockedChannelChangeResultCallback = null;
    }

    protected enum BlockButton implements TextButtonPresenter.TextButtonItem {
        RESET(0, R.string.reset_settings, false), SAVE(1, R.string.save, true), CANCEL(2, R.string.cancel, true);

        private int type;
        private int title;
        private boolean isEnabled;

        BlockButton(int type, int title, boolean isEnabled) {
            this.type = type;
            this.title = title;
            this.isEnabled = isEnabled;
        }

        public void setEnabled(boolean enabled) {
            isEnabled = enabled;
        }

        @Override
        public int getTitle() {
            return title;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public boolean isEnabled() {
            return isEnabled;
        }
    }

    public interface OnBlockedChannelChangeResultCallback {
        void onChangeBlockedChannel(boolean isSuccess, List<String> list);
    }
}
