/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.altimedia.util.Log;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.menu.MenuSliderLayout;
import kr.altimedia.launcher.jasmin.ui.view.row.UserMenuRow;

/**
 * Created by mc.kim on 04,02,2020
 */
public class UserMenuPresenter extends Presenter {
    private final String TAG = UserMenuPresenter.class.getSimpleName();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreateViewHolder ");
        }
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewGroup parentsView = (ViewGroup) inflater.inflate(R.layout.layout_user_menu, null);
        return new UserMenuViewHolder(parentsView);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBindViewHolder ");
        }
        UserMenuRow userMenuRow = (UserMenuRow) item;
        MenuSliderLayout menuSliderLayout = userMenuRow.getMenuSlider();
        UserMenuRow.OnUserMenuClickListener menuClickListener = userMenuRow.getMenuClickListener();
        ((UserMenuViewHolder) viewHolder).setListener(userMenuRow, menuSliderLayout, menuClickListener);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onUnbindViewHolder ");
        }
        ((UserMenuViewHolder) viewHolder).removeListener();
    }

    private static class UserMenuViewHolder extends ViewHolder {
        private final String TAG = UserMenuViewHolder.class.getSimpleName();
        private ImageButton mIconUser;
        private ImageButton mIconSetting;
        private View mIconNoti;
        private View parentView;

        public UserMenuViewHolder(View view) {
            super(view);
            parentView = view;
            mIconUser = view.findViewById(R.id.iconUser);
            mIconSetting = view.findViewById(R.id.iconSetting);
            mIconNoti = view.findViewById(R.id.iconNotification);
            parentView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        mIconUser.requestFocus();
                    }
                }
            });
        }

        public void setListener(UserMenuRow userMenuRow, MenuSliderLayout menuSliderLayout,
                                UserMenuRow.OnUserMenuClickListener menuClickListener) {
            OnLastFocusListener lastFocusListener = new OnLastFocusListener(parentView, userMenuRow, menuSliderLayout);
            OnButtonClickListener mOnButtonClickListener = new OnButtonClickListener(menuClickListener);
            mIconUser.setOnKeyListener(lastFocusListener);
            mIconSetting.setOnKeyListener(lastFocusListener);
            mIconNoti.setOnKeyListener(lastFocusListener);

            mIconUser.setOnClickListener(mOnButtonClickListener);
            mIconSetting.setOnClickListener(mOnButtonClickListener);
            mIconNoti.setOnClickListener(mOnButtonClickListener);
        }

        public void removeListener() {
            mIconUser.setOnClickListener(null);
            mIconSetting.setOnClickListener(null);
            mIconNoti.setOnClickListener(null);

            mIconUser.setOnKeyListener(null);
            mIconSetting.setOnKeyListener(null);
            mIconNoti.setOnKeyListener(null);

            parentView.setOnFocusChangeListener(null);
        }
    }

    private static class OnButtonClickListener implements View.OnClickListener {
        private UserMenuRow.OnUserMenuClickListener mOnUserMenuClickListener;

        public OnButtonClickListener(UserMenuRow.OnUserMenuClickListener menuClickListener) {
            this.mOnUserMenuClickListener = menuClickListener;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iconUser:
                    this.mOnUserMenuClickListener.onMenuClicked(UserMenuRow.UserMenu.USER);
                    break;
                case R.id.iconSetting:
                    this.mOnUserMenuClickListener.onMenuClicked(UserMenuRow.UserMenu.SETTINGS);
                    break;
            }
        }
    }

    private static class OnLastFocusListener implements View.OnKeyListener {
        final MenuSliderLayout mMenuSliderLayout;
        final View mRootView;
        final UserMenuRow userMenuRow;

        public OnLastFocusListener(View rootView, UserMenuRow userMenuRow, MenuSliderLayout menuSliderLayout) {
            this.mMenuSliderLayout = menuSliderLayout;
            this.mRootView = rootView;
            this.userMenuRow = userMenuRow;
        }

        @Override
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (userMenuRow.getMenuClickListener().dispatchKey(keyEvent)) {
                return true;
            }
            int id = view.getNextFocusRightId();
            View nextView = mRootView.findViewById(id);
            boolean isLastView = nextView == null || nextView.getVisibility() != View.VISIBLE;
            if (!isLastView) {
                return false;
            }
            int action = keyEvent.getAction();
            int keyCode = keyEvent.getKeyCode();
            if (action == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                    this.mMenuSliderLayout.onCanceled(view, null);
                    return true;
                }
            }
            return false;
        }
    }


}
