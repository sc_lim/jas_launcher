/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class ProfileLoginResult implements Parcelable {
    public static final Creator<ProfileLoginResult> CREATOR = new Creator<ProfileLoginResult>() {
        @Override
        public ProfileLoginResult createFromParcel(Parcel in) {
            return new ProfileLoginResult(in);
        }

        @Override
        public ProfileLoginResult[] newArray(int size) {
            return new ProfileLoginResult[size];
        }
    };
    @Expose
    @SerializedName("loginToken")
    private String loginToken;
    @Expose
    @SerializedName("viewActivityYn")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean viewActivityYn;
    @Expose
    @SerializedName("viewRestriction")
    private String viewRestriction;

    protected ProfileLoginResult(Parcel in) {
        loginToken = in.readString();
        viewActivityYn = in.readByte() != 0;
        viewRestriction = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(loginToken);
        dest.writeByte((byte) (viewActivityYn ? 1 : 0));
        dest.writeString(viewRestriction);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public boolean isViewActivityYn() {
        return viewActivityYn;
    }

    public String getViewRestriction() {
        return viewRestriction;
    }

    @Override
    public String toString() {
        return "ProfileLoginResult{" +
                "loginToken='" + loginToken + '\'' +
                ", viewActivityYn='" + viewActivityYn + '\'' +
                ", viewRestriction='" + viewRestriction + '\'' +
                '}';
    }
}
