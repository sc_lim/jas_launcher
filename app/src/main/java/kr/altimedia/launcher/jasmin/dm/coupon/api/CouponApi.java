/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.api;

import com.google.gson.JsonObject;

import kr.altimedia.launcher.jasmin.dm.coupon.object.response.CouponHomeInfoResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.CouponProductListResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.CouponPurchaseRequestResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.CouponPurchaseResultResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.CouponRegisterResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.MyCouponListResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CouponApi {

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("retrieveowncouponhomeinfo")
    Call<CouponHomeInfoResponse> getCouponHomeInfo(@Query("said") String said,
                                                   @Query("profileId") String profileId,
                                                   @Query("includeCoupons") boolean includeCoupons);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("retrieveowncouponlist")
    Call<MyCouponListResponse> getMyCouponList(@Query("said") String said,
                                               @Query("profileId") String profileId,
                                               @Query("searchTarget") String searchTarget,
                                               @Query("includehistory") boolean includeHistory);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("requestpromotioncouponnumberregister")
    Call<CouponRegisterResponse> postRegisterCoupon(@Body JsonObject bodyData);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("retrieveproductcouponlist")
    Call<CouponProductListResponse> getCouponProductList(@Query("said") String said,
                                                         @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("purchasecoupon")
    Call<CouponPurchaseRequestResponse> requestPurchaseCoupon(@Body JsonObject bodyData);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("getpaymentresult")
    Call<CouponPurchaseResultResponse> getPurchaseCouponResult(@Query("purchaseId") String purchaseId);

}
