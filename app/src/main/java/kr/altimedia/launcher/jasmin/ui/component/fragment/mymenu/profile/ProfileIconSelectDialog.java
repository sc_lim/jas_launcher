/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile;

import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileIcon;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileIconCategory;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.MyContentListFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.MyContentListShadowHelper;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.OnDataLoadedListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.presenter.ProfileIconHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.presenter.ProfileIconListRowPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.presenter.ProfileIconPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class ProfileIconSelectDialog extends SafeDismissDialogFragment implements OnDataLoadedListener, View.OnUnhandledKeyEventListener{
    public static final String CLASS_NAME = ProfileIconSelectDialog.class.getName();
    private final String TAG = ProfileIconSelectDialog.class.getSimpleName();

    private final int INDEX_NORMAL_LIST = 0;

    private ProgressBarManager progressBarManager;

    private MbsDataTask dataTask;
    private List<ProfileIconCategory> profileIconCategoryList = new ArrayList<>();
    private ArrayObjectAdapter mAdapter;
    private ClassPresenterSelector mPresenterSelector;

    protected BrowseFrameLayout mRootView;
    protected MyContentListFragment mProfileIconListFragment;
    private View focusedView;
    private FrameLayout profileIconListContainer;
    private LinearLayout emptyListContainer;

    private MyContentListShadowHelper mProfileIconShadowHelper = null;
    private OnProfileIconSelectedListener mOnProfileIconSelectedListener = null;
    private DialogInterface.OnDismissListener mOnDismissListener = null;

    private String profileId = "";

    ProfileIconSelectDialog() {
    }

    public static ProfileIconSelectDialog newInstance(String profileId) {
        ProfileIconSelectDialog fragment = new ProfileIconSelectDialog();
        Bundle args = new Bundle();
        args.putString(ProfileManageDialog.KEY_PROFILE_ID, profileId);
        fragment.setArguments(args);
        return fragment;
    }

    public void showDialog(TvOverlayManager mTvOverlayManager) {
        if (mTvOverlayManager == null) {
            return;
        }
        this.mTvOverlayManager = mTvOverlayManager;
        this.mTvOverlayManager.showDialogFragment(this);
    }

    public void setOnDismissListener(@NonNull DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
            mOnDismissListener = null;
        }
    }

    public void setOnProfileIconSelectedListener(OnProfileIconSelectedListener onProfileIconSelectedListener) {
        mOnProfileIconSelectedListener = onProfileIconSelectedListener;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onDestroyView() {

        if (dataTask != null) {
            dataTask.cancel(true);
            dataTask = null;
        }
        if(profileIconCategoryList != null){
            profileIconCategoryList.clear();
            profileIconCategoryList = null;
        }
        if (progressBarManager != null) {
            progressBarManager.hide();
            progressBarManager.setRootView(null);
        }
        if (mRootView != null) {
            mRootView.removeOnUnhandledKeyEventListener(this);
        }
        mProfileIconShadowHelper = null;
        mOnProfileIconSelectedListener = null;
        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = (BrowseFrameLayout)inflater.inflate(R.layout.dialog_mymenu_profile_icon_select, container, false);

        mProfileIconListFragment = (MyContentListFragment) getChildFragmentManager().findFragmentById(
                R.id.profileIconListContainer);
        if (mProfileIconListFragment == null) {
            Rect padding = new Rect(0, 0, 0, 0);
            mProfileIconListFragment = MyContentListFragment.newInstance(padding);
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.profileIconListContainer, mProfileIconListFragment).commit();
        }
        mProfileIconListFragment.setAdapter(mAdapter);

        mRootView.setOnFocusSearchListener(new BrowseFrameLayout.OnFocusSearchListener() {
            @Override
            public View onFocusSearch(View focused, int direction) {
                return focused;
            }
        });

        return mRootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if(bundle != null) {
            profileId = bundle.getString(ProfileManageDialog.KEY_PROFILE_ID, "");
        }
        mRootView.addOnUnhandledKeyEventListener(this);

        initLoadingBar(view);

        initView(view);

        loadProfileIconList();
    }

    private void initView(View view){
        profileIconListContainer = view.findViewById(R.id.profileIconListContainer);
        emptyListContainer = view.findViewById(R.id.emptyListContainer);

        mPresenterSelector = new ClassPresenterSelector();
        mAdapter = new ArrayObjectAdapter(mPresenterSelector);
        if (mProfileIconListFragment != null) {
            mProfileIconListFragment.setAdapter(mAdapter);
        }

        initShadowCallback(view.findViewById(R.id.bottomShadow));
        initChildFocusListener();
        setOnItemViewClickedListener();
    }

    private void initLoadingBar(View view) {
        progressBarManager = new ProgressBarManager();
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, (ViewGroup) view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);

        showProgress();
    }

    @Override
    public void hideProgress() {
        if (progressBarManager != null) {
            progressBarManager.hide();
        }
    }

    @Override
    public void showProgress() {
        if (progressBarManager != null) {
            progressBarManager.show();
        }
    }

    @Override
    public void showView(int type) {
        if(profileIconCategoryList != null && !profileIconCategoryList.isEmpty()) {
            mPresenterSelector.addClassPresenter(ListRow.class, new ProfileIconListRowPresenter());

            for (int index = 0; index < profileIconCategoryList.size(); index++) {
                ProfileIconCategory category = profileIconCategoryList.get(index);
                List<ProfileIcon> list = category.getProfileIconList();
                if (Log.INCLUDE) {
                    Log.d(TAG, "showView: profileIconList[" + index + "]=" + list.size());
                }
                addProfileIconListRow(index, category, list);
            }

            emptyListContainer.setVisibility(View.GONE);
            profileIconListContainer.setVisibility(View.VISIBLE);
        }else{
            profileIconListContainer.setVisibility(View.GONE);
            emptyListContainer.setVisibility(View.VISIBLE);
        }

        hideProgress();
    }

    private void setVerticalSpacing(int spacing) {
        if (mProfileIconListFragment != null) {
            mProfileIconListFragment.getVerticalGridView().setVerticalSpacing(spacing);
        }
    }

    private void setOnItemViewClickedListener() {
        ItemViewClickedListener listener = new ItemViewClickedListener();
        if (mProfileIconListFragment != null) {
            mProfileIconListFragment.setOnItemViewClickedListener(listener);
        }
    }

    private void setOnScrollOffsetCallback(VerticalGridView.OnScrollOffsetCallback mOnScrollOffsetCallback) {
        mProfileIconListFragment.setOnScrollOffsetCallback(mOnScrollOffsetCallback);
    }

    private void loadProfileIconList() {
        UserDataManager userDataManager = new UserDataManager();
        dataTask = userDataManager.getProfileIconCategoryList(
                AccountManager.getInstance().getLocalLanguage(),
                AccountManager.getInstance().getSaId(),
                profileId,
                new MbsDataProvider<String, List<ProfileIconCategory>>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfileIconList: onFailed");
                        }

                        showView(0);
                    }

                    @Override
                    public void onSuccess(String key, List<ProfileIconCategory> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfileIconList: onSuccess: result=" + result);
                        }

                        if(profileIconCategoryList != null && profileIconCategoryList.isEmpty()) {
                            profileIconCategoryList.clear();
                        }

//                        profileIconCategoryList.addAll(result);
                        if(result != null) {
                            for (ProfileIconCategory category : result) {
                                List<ProfileIcon> list = category.getProfileIconList();
                                if(list != null && list.size() > 0) {
                                    profileIconCategoryList.add(category);
                                }
                            }
                        }

                        showView(0);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);

                        showView(0);
                    }
                });
    }

    private void initShadowCallback(final View bottomShadow) {
        mProfileIconShadowHelper = new MyContentListShadowHelper(bottomShadow);
        setOnScrollOffsetCallback(mProfileIconShadowHelper);
    }

    private void initChildFocusListener() {
        mRootView.setOnChildFocusListener(new BrowseFrameLayout.OnChildFocusListener() {
            @Override
            public boolean onRequestFocusInDescendants(int var1, Rect var2) {
                return true;
            }

            @Override
            public void onRequestChildFocus(View child, View focused) {
                focusedView = focused;
                try{
                    int size = mProfileIconListFragment.getBridgeAdapter().getItemCount();
                    int selectedIndex = mProfileIconListFragment.getSelectedPosition();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onRequestChildFocus: size="+size+", selectedIndex=" + selectedIndex);
                    }
                    for(int i=0; i<size; i++){
                        RowPresenter.ViewHolder viewHolder = mProfileIconListFragment.getRowViewHolder(i);
                        if (viewHolder != null) {
                            if(i == selectedIndex) {
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "onRequestChildFocus: viewHolder="+viewHolder);
                                }
                                if(viewHolder instanceof ProfileIconListRowPresenter.ViewHolder){
                                    ((ProfileIconListRowPresenter.ViewHolder)viewHolder).setHeaderViewSelected(true);
                                    ((ProfileIconListRowPresenter.ViewHolder)viewHolder).setArrowVisibility(true);
                                }

                            }else{
                                if(viewHolder instanceof ProfileIconListRowPresenter.ViewHolder){
                                    ((ProfileIconListRowPresenter.ViewHolder)viewHolder).setHeaderViewSelected(false);
                                    ((ProfileIconListRowPresenter.ViewHolder)viewHolder).setArrowVisibility(false);
                                }
                            }
                        }
                    }
                }catch (Exception e){
                }
            }
        });
    }

    private ArrayObjectAdapter getProfileIconRowAdapter(List<ProfileIcon> iconList) {
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new ProfileIconPresenter());

        if (iconList != null) {
            listRowAdapter.addAll(0, iconList);
        }

        return listRowAdapter;
    }

    private void addProfileIconListRow(int index, ProfileIconCategory category, List<ProfileIcon> iconList) {

        try{
            HeaderItem header = new HeaderItem(index, category.getName());
            ArrayObjectAdapter objectAdapter = getProfileIconRowAdapter(iconList);

            ProfileIconListRowPresenter mListRowPresenter = (ProfileIconListRowPresenter) mPresenterSelector.getPresenters()[INDEX_NORMAL_LIST];
            mListRowPresenter.setHeaderPresenter(new ProfileIconHeaderPresenter());

            mAdapter.add(index, new ListRow(header, objectAdapter));
        }catch (Exception e){
        }
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder var3, Row row) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onItemClicked: item=" + item + ", row=" + row);
            }

            if (item == null) {
                return;
            }

            if(item instanceof ProfileIcon){
                if(mOnProfileIconSelectedListener != null){
                    mOnProfileIconSelectedListener.onProfileIconSelected((ProfileIcon)item);
                }
            }
            dismiss();
        }
    }

    public interface OnProfileIconSelectedListener {
        void onProfileIconSelected(ProfileIcon profileIcon);
    }
}
