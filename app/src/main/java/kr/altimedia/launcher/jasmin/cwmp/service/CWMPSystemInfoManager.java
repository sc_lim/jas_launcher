/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service;

import android.os.SystemClock;

import af.system.JasSysInfoEdidEventListener;
import af.system.JasSysInfoErrorMessageListener;
import af.system.JasSystemManager;

public class CWMPSystemInfoManager {
    private final String TAG = CWMPSystemInfoManager.class.getSimpleName();
    private final String INVALID_DATA;
    private final JasSystemManager mJasSystemManager;

    public CWMPSystemInfoManager() {
        this.INVALID_DATA = "invalid data";
        this.mJasSystemManager = JasSystemManager.getInstance();
    }

    public CWMPSystemInfoManager(String INVALID_DATA_STRING) {
        this.INVALID_DATA = INVALID_DATA_STRING;
        this.mJasSystemManager = JasSystemManager.getInstance();
    }

    public String getFirmwareVersion() {
        try {
            return this.mJasSystemManager.getFirmwareVersion();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public String getMainSoftwareVersion() {
        try {
            return this.mJasSystemManager.getMainSoftwareVersion();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public String getSerialNumber() {
        try {
            return this.mJasSystemManager.getSerialNumber();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public String getModelName() {
        try {
            return this.mJasSystemManager.getModelName();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public String getManufacturerName() {
        try {
            return this.mJasSystemManager.getManufacturerName();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public String getManufacturerOUI() {
        try {
            return this.mJasSystemManager.getManufacturerOUI();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public String getHwVersion() {
        try {
            return this.mJasSystemManager.getHwVersion();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public long getUpTime() {
        long var1 = SystemClock.uptimeMillis() / 1000L;
        return var1;
    }

    public String getManufactureYearMonth() {
        try {
            return this.mJasSystemManager.getManufactureYearMonth();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public String getSocName() {
        try {
            return this.mJasSystemManager.getSocName();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public String getDecoderName() {
        try {
            return this.mJasSystemManager.getDecoderName();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public String getCodecSupport() {
        try {
            return this.mJasSystemManager.getCodecSupport();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public String getBootLoaderVersion() {
        try {
            return this.mJasSystemManager.getBootLoaderVersion();
        } catch (Exception e) {
            e.printStackTrace();
            return INVALID_DATA;
        }
    }

    public void addSysInfoEdidEventListener(JasSysInfoEdidEventListener var1) {
        try {
            this.mJasSystemManager.addSysInfoEdidEventListener(var1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeSysInfoEdidEventListener(JasSysInfoEdidEventListener var1) {
        try {
            this.mJasSystemManager.removeSysInfoEdidEventListener(var1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addSysInfoErrorMessageListener(JasSysInfoErrorMessageListener var1) {
        try {
            this.mJasSystemManager.addSysInfoErrorMessageListener(var1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeSysInfoErrorMessageListener(JasSysInfoErrorMessageListener var1) {
        try {

            this.mJasSystemManager.removeSysInfoErrorMessageListener(var1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
