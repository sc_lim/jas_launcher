/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.observable;

import android.view.View;

import androidx.databinding.BaseObservable;

import com.altimedia.tvmodule.dao.Program;

import java.text.SimpleDateFormat;
import java.util.Date;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.ParentalController;
import kr.altimedia.launcher.jasmin.tv.manager.MiniEpgDataManager;
import kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel;
import kr.altimedia.launcher.jasmin.ui.app.AppConfig;

import static kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel.FLAG_IS_UNBLOCKED_CHANNEL;

public class ProgramEntryData extends BaseObservable {
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("HH:mm");

    private EpgViewModel.ResourceProvider mResourceProvider = null;
    private final boolean isCurrentProgram;
    private MiniEpgDataManager.TableEntry currentEntry = null;
    private ParentalController.BlockType blockType;
    private int flags;

    private boolean hasFlag(int bit) { return EpgViewModel.hasFlag(flags, bit); }

    public ProgramEntryData(EpgViewModel.ResourceProvider resourceProvider, boolean isCurrent) {
        mResourceProvider = resourceProvider;
        isCurrentProgram = isCurrent;
    }

    public void setValue(MiniEpgDataManager.TableEntry entry, ParentalController.BlockType blockType, int flags) {
        currentEntry = entry;
        this.blockType = blockType;
        this.flags = flags;
    }

    public MiniEpgDataManager.TableEntry getValue() {
        return currentEntry;
    }

    public String getTitle() {
        String rating = "";
        if (AppConfig.TEST_SHOW_PROGRAM_PR) {
            Program prog = currentEntry != null ? currentEntry.getProgram() : null;
            rating = ParentalController.getRatingStringForDebug(prog);
        }

        String title = "";
        if (isCurrentProgram) {
            if (blockType == ParentalController.BlockType.BLOCK_ADULT_CHANNEL) {
                title = mResourceProvider.getString(R.string.mini_epg_restricted_channel);
            } else if (blockType == ParentalController.BlockType.BLOCK_CHANNEL) {
                title = mResourceProvider.getString(R.string.mini_epg_blocked_channel);
            } else if (currentEntry == null) {
                title = mResourceProvider.getString(R.string.mini_epg_no_program);
            } else {
                title = currentEntry.getProgramTitle();
            }
        } else {
            if (blockType != null && blockType.isBlocked()) {
                title = mResourceProvider.getString(R.string.empty);
            } else if (currentEntry == null) {
                title = mResourceProvider.getString(R.string.empty);
            } else {
                title = currentEntry.getProgramTitle();
            }
        }

        return rating + title;
    }

    public String getTime() {
        if (currentEntry != null) {
            return getFilterDate(currentEntry.getStartTime()) + " " + currentEntry.getTime();
        } else {
            return "";
        }
    }

    public String getFilterDate(long time) {
        String format = mResourceProvider.getString(R.string.EEE_dd_MMM);
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        String filterValue = dateFormat.format(new Date(time));
        String today = dateFormat.format(new Date());
        if (filterValue.equalsIgnoreCase(today)) {
            filterValue = mResourceProvider.getString(R.string.today2);
        }
        return filterValue;
    }

    public int getProgress() {
        return (getProgramVisibility() == View.VISIBLE) ? currentEntry.getProgress() : 0;
    }

    public int getProgramVisibility() {
        if (currentEntry != null && (blockType != null && !blockType.isBlocked())) {
            return View.VISIBLE;
        } else {
            return View.INVISIBLE;
        }
    }

    public int getReservedVisibility() {
        if (getProgramVisibility() == View.VISIBLE && currentEntry != null && currentEntry.isReserved()) {
            return View.VISIBLE;
        } else {
            return View.GONE;
        }
    }

    public int getAgeRestrictedVisibility() {
        if (getProgramVisibility() == View.VISIBLE && currentEntry != null) {
            if (hasFlag(FLAG_IS_UNBLOCKED_CHANNEL)) {
                return View.GONE;
            } else {
                return ParentalController.isProgramRatingBlocked(currentEntry.getProgram()) ? View.VISIBLE : View.GONE;
            }
        } else {
            return View.GONE;
        }
    }

    public int getVideoTypeIconId() {
        Program program = (currentEntry != null) ? currentEntry.getProgram() : null;
        int type = (program != null) ? program.getVideoType() : Program.VIDEO_TYPE_UNKNOWN;
        switch (type) {
            case Program.VIDEO_TYPE_SD:
                return R.drawable.icon_sd;
            case Program.VIDEO_TYPE_HD:
                return R.drawable.icon_hd;
            case Program.VIDEO_TYPE_UHD:
                return R.drawable.icon_uhd;
            default:
                return 0;
        }
    }

    public int getVideoTypeIconVisibility() {
        if (getProgramVisibility() == View.VISIBLE && getVideoTypeIconId() != 0) {
            return View.VISIBLE;
        } else {
            return View.GONE;
        }
    }
}