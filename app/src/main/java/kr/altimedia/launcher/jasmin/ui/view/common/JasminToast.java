/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import kr.altimedia.launcher.jasmin.R;

public class JasminToast extends Toast {

    private static Toast toast = null;

    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you
     * can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application}
     *                or {@link Activity} object.
     */
    public JasminToast(Context context) {
        super(context);
    }

    public static void makeToast(Context context, int stringId) {
        String message = context.getString(stringId);
        makeToast(context, message);
    }

    public static void makeToast(Context context, String message) {
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = li.inflate(R.layout.toast_jasmin, null);

        TextView textView = view.findViewById(R.id.toast_message);
        setMessage(context, textView, message);

        if (toast != null) {
            //hide previous toast if it is showing
            toast.cancel();
        }

        toast = new Toast(context.getApplicationContext());
        toast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(view);
        toast.show();
    }

    private static void setMessage(Context context, TextView textView, String message) {
        textView.setText(message);

        Rect bounds = new Rect();
        Paint textPaint = textView.getPaint();
        textPaint.getTextBounds(message, 0, message.length(), bounds);

        Resources resources = context.getResources();
        float dp = bounds.width();
        float small = resources.getDimension(R.dimen.toast_small_width);
        float large = resources.getDimension(R.dimen.toast_large_width);

        int sidePadding = textView.getPaddingLeft() * 2;
        ViewGroup.LayoutParams params = textView.getLayoutParams();
        params.width = (int) (dp < (small - sidePadding) ? small : large);
        textView.setLayoutParams(params);
    }
}
