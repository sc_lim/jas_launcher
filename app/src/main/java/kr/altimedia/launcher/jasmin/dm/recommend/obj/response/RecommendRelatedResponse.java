/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.recommend.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.def.NullToEmptyDeserializer;
import kr.altimedia.launcher.jasmin.dm.recommend.obj.RcmdContent;

/**
 * Created by mc.kim on 01,06,2020
 */
public class RecommendRelatedResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    @JsonAdapter(NullToEmptyDeserializer.class)
    private RecommendResult data;

    protected RecommendRelatedResponse(Parcel in) {
        super(in);
        data = in.readParcelable(RecommendResult.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(data, flags);
    }

    public List<RcmdContent> getData() {
        if (data == null || data.rcdList == null) {
            return new ArrayList<>();
        }
        return data.rcdList;
    }

    private static class RecommendResult implements Parcelable {
        @Expose
        @SerializedName("ITEM_CNT")
        private int itemCnt;
        @Expose
        @SerializedName("RC_LIST")
        private List<RcmdContent> rcdList;

        public int getItemCnt() {
            return itemCnt;
        }

        public List<RcmdContent> getRcdList() {
            return rcdList;
        }

        @Override
        public String toString() {
            return "RecommendResult{" +
                    "itemCnt=" + itemCnt +
                    ", rcdList=" + rcdList +
                    '}';
        }

        protected RecommendResult(Parcel in) {
            itemCnt = in.readInt();
            rcdList = in.createTypedArrayList(RcmdContent.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(itemCnt);
            dest.writeTypedList(rcdList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<RecommendResult> CREATOR = new Creator<RecommendResult>() {
            @Override
            public RecommendResult createFromParcel(Parcel in) {
                return new RecommendResult(in);
            }

            @Override
            public RecommendResult[] newArray(int size) {
                return new RecommendResult[size];
            }
        };
    }
}
