/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.content.Context;

import com.altimedia.tvmodule.common.JasGenre;
import com.altimedia.tvmodule.manager.ChannelRingStorage;

import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 25,06,2020
 */
public class GenreUtil {
    public static final String ALL_CHANNELS = "99:1";
    public static final String POP_CHANNELS = "99:2";
    public static final String FAVORITE_CHANNELS = "99:3";
    public static final String MULTI_VIEW_CHANNELS = "99:4";
    public static final String AUDIO_CHANNELS = "99:5";
    public static int ID_ALL_CHANNELS = 0;

    private static final String[] CANONICAL_FILTERS = {
            ALL_CHANNELS,
            POP_CHANNELS,
            FAVORITE_CHANNELS,
            MULTI_VIEW_CHANNELS,
            AUDIO_CHANNELS,
            JasGenre.DIGITAL_TV,
            JasGenre.MOVIE,
            JasGenre.KIDS,
            JasGenre.SPORT,
            JasGenre.NEWS,
            JasGenre.ENTERTAINMENT,
            JasGenre.EDUCATION,
            JasGenre.DOCUMENTARY,
            JasGenre.ADULT
    };

    public static String[] getLabels(Context context) {
        String[] filterLabels = context.getResources().getStringArray(R.array.lineup_labels);
        String[] genreLabels = JasGenre.getLabels(context);
        String[] resultLabel = new String[filterLabels.length + genreLabels.length];
        System.arraycopy(filterLabels, 0, resultLabel, 0, filterLabels.length);
        System.arraycopy(genreLabels, 0, resultLabel, filterLabels.length, genreLabels.length);
        return resultLabel;
    }

    public static ChannelRingStorage.RingFunction getRingFunction(String canonicalGenre) {
        switch (canonicalGenre) {
            case JasGenre.DIGITAL_TV:
                return JasGenre.RING_GENRE_DIGITAL_TV;
            case JasGenre.SPORT:
                return JasGenre.RING_GENRE_SPORT;
            case JasGenre.NEWS:
                return JasGenre.RING_GENRE_NEWS;
            case JasGenre.KIDS:
                return JasGenre.RING_GENRE_KIDS;
            case JasGenre.DOCUMENTARY:
                return JasGenre.RING_GENRE_DOCUMENTARY;
            case JasGenre.ENTERTAINMENT:
                return JasGenre.RING_GENRE_ENTERTAINMENT;
            case JasGenre.MOVIE:
                return JasGenre.RING_GENRE_MOVIE;
            case JasGenre.EDUCATION:
                return JasGenre.RING_GENRE_EDUCATION;
            case JasGenre.ADULT:
                return JasGenre.RING_GENRE_ADULT;
        }
        return null;
    }

    public static int getCount() {
        int filterCount = CANONICAL_FILTERS.length;
        return filterCount;
    }

    public static String getCanonical(int id) {
        if (id < 0 || id >= CANONICAL_FILTERS.length) {
            return null;
        }
        return CANONICAL_FILTERS[id];
    }

    public static int getId(String canonicalGenre) {
        if (canonicalGenre == null) {
            return ID_ALL_CHANNELS;
        }
        for (int i = 1; i < CANONICAL_FILTERS.length; ++i) {
            if (CANONICAL_FILTERS[i].equals(canonicalGenre)) {
                return i;
            }
        }
        return ID_ALL_CHANNELS;
    }


    public static String getGenreLabel(Context context, int id) {
        return getFilterLabel(context, id);
    }

    public static String[] getCanonicalGenres() {
        int count = getCount();
        String[] canonicalKey = new String[count];
        for (int i = 0; i < count; i++) {
            canonicalKey[i] = getCanonical(i);
        }
        return canonicalKey;
    }

    private static String getFilterLabel(Context context, int id) {
        if (id < 0 || id >= getCount()) {
            return null;
        }
        return getLabels(context)[id];
    }


}
