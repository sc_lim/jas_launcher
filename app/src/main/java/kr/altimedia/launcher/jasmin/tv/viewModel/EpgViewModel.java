/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.viewModel;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.altimedia.tvmodule.dao.Channel;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.databinding.Observable;
import androidx.lifecycle.ViewModel;
import kr.altimedia.launcher.jasmin.tv.ParentalController;
import kr.altimedia.launcher.jasmin.tv.manager.MiniEpgDataManager;
import kr.altimedia.launcher.jasmin.tv.observable.ChannelLiveData;
import kr.altimedia.launcher.jasmin.tv.observable.EpgGuiData;
import kr.altimedia.launcher.jasmin.tv.observable.ProgramEntryData;

public class EpgViewModel extends ViewModel {
    @Retention(RetentionPolicy.SOURCE)
    @IntDef(flag = true,
            value = {
                    FLAG_DEFAULT,
                    FLAG_IS_WATCHING_CHANNEL,
                    FLAG_IS_UNBLOCKED_CHANNEL,
                    FLAG_HAS_PROGRAM,
                    FLAG_IS_CURRENT_PROGRAM,
                    FLAG_PIP_VISIBLE,
            })
    private @interface ButtonFlag {}

    public static final int FLAG_DEFAULT                = 0b00000000;
    public static final int FLAG_IS_WATCHING_CHANNEL    = 0b00000001;
    public static final int FLAG_IS_UNBLOCKED_CHANNEL   = 0b00000010;
    public static final int FLAG_HAS_PROGRAM            = 0b00000100;
    public static final int FLAG_IS_CURRENT_PROGRAM     = 0b00001000;
    public static final int FLAG_PIP_VISIBLE            = 0b00010000;

    public static final int setFlag(int flag, int bit) { return flag |= bit; }
    public static final boolean hasFlag(int flag, int bit) { return ((flag & bit) != 0); }

    public static final String flagToString(int flag) {
        String str = "";
        if ((flag & FLAG_IS_WATCHING_CHANNEL) != 0) {
            str += "isWatchingChannel,";
        }
        if ((flag & FLAG_IS_UNBLOCKED_CHANNEL) != 0) {
            str += "isUnblockedChannel,";
        }
        if ((flag & FLAG_HAS_PROGRAM) != 0) {
            str += "hasProgram,";
        }
        if ((flag & FLAG_IS_CURRENT_PROGRAM) != 0) {
            str += "isCurrentProgram,";
        }
        if ((flag & FLAG_PIP_VISIBLE) != 0) {
            str += "pipVisible,";
        }
        return str;
    }

    public class ResourceProvider {
        private final Context mContext;

        public ResourceProvider(Context context) {
            mContext = context;
        }

        public String getString(int resourceId) {
            return mContext.getString(resourceId);
        }
    }

    private Observable.OnPropertyChangedCallback mOnPropertyChangedCallback = null;
    private Activity mActivity = null;
    private Application mApplication = null;

    public final ChannelLiveData currentChannel;
    public final ChannelLiveData watchingChannel;
    public final ProgramEntryData currentProgram;
    public final ProgramEntryData nextProgram;
    public final EpgGuiData epgGuiData;
    public ParentalController.BlockType selectedChBlockType;
    public int lastFlags;

    public EpgViewModel(Activity mActivity, Application mApplication) {
        this(null, mActivity, mApplication);
    }

    public EpgViewModel(Observable.OnPropertyChangedCallback mOnPropertyChangedCallback, Activity mActivity, Application mApplication) {
        this.mOnPropertyChangedCallback = mOnPropertyChangedCallback;
        this.mActivity = mActivity;
        this.mApplication = mApplication;
        ResourceProvider reosurceProvider = new ResourceProvider(mActivity);

        currentChannel = new ChannelLiveData();
        watchingChannel = new ChannelLiveData();
        currentProgram = new ProgramEntryData(reosurceProvider, true);
        nextProgram = new ProgramEntryData(reosurceProvider, false);
        epgGuiData = new EpgGuiData();
    }

    public void setValue(Channel selectedChannel, MiniEpgDataManager.TableEntry current, MiniEpgDataManager.TableEntry next, ParentalController.BlockType selectedChBlockType, ParentalController.BlockType tunedChBlockType, @ButtonFlag int flags) {
        this.selectedChBlockType = selectedChBlockType;
        this.lastFlags = flags;

        currentChannel.setValue(selectedChannel);
//        currentTvData.notifyChange();
        currentChannel.notifyPropertyChanged(0);

        currentProgram.setValue(current, selectedChBlockType, lastFlags);
        currentProgram.notifyChange();

        nextProgram.setValue(next, selectedChBlockType, lastFlags);
        nextProgram.notifyChange();

        epgGuiData.setValue(selectedChannel, selectedChBlockType, tunedChBlockType, lastFlags);
        epgGuiData.notifyChange();
    }

    public void setBlockType(ParentalController.BlockType tunedChBlockType) {
        epgGuiData.setValue(currentChannel.getCurrentChannel(), selectedChBlockType, tunedChBlockType, lastFlags);
        epgGuiData.notifyChange();
    }

    public void setSelectedChannel(Channel selectedChannel) {
        currentChannel.setValue(selectedChannel);
        currentChannel.notifyPropertyChanged(0);
    }

    public void setWatchingChannel(Channel channel) {
        watchingChannel.setValue(channel);
        watchingChannel.notifyPropertyChanged(0);
    }

    public Channel getWatchingChannel() {
        return watchingChannel.getCurrentChannel();
    }

    public Channel getCurrentChannel() {
        return currentChannel != null ? currentChannel.getCurrentChannel() : null;
    }
}
