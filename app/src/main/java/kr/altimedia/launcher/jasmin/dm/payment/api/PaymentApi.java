/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.api;

import com.google.gson.JsonObject;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.AvailablePaymentTypeResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.BankResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.CreditCardResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.PaymentPromotionResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.PaymentResultResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.RequestVodPaymentResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface PaymentApi {
    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("paymentmethod")
    Call<AvailablePaymentTypeResponse> getAvailablePaymentType(@Query("said") String said,
                                                               @Query("profileId") String profileId,
                                                               @Query("offerId") String offerId,
                                                               @Query("contentType") String contentType);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("paymentpromotion")
    Call<PaymentPromotionResponse> getPaymentPromotion();

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("creditcardlist")
    Call<CreditCardResponse> getCreditCardList(@Query("said") String said);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("banklist")
    Call<BankResponse> getBankList();

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("requestpayment")
    Call<RequestVodPaymentResponse> requestVodPayment(@Body JsonObject watchingInfo);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("cancelpayment")
    Call<BaseResponse> cancelVodPayment(@Body JsonObject watchingInfo);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("getpaymentresult")
    Call<PaymentResultResponse> getVodPaymentResult(@Query("purchaseId") String purchaseId);
}
