/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.TypedArray;
import android.transition.TransitionValues;
import android.transition.Visibility;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import kr.altimedia.launcher.jasmin.R;

public class SlideKitkat extends Visibility {
    private static final String TAG = "SlideKitkat";
    private static final TimeInterpolator sDecelerate = new DecelerateInterpolator();
    private static final TimeInterpolator sAccelerate = new AccelerateInterpolator();
    private int mSlideEdge;
    private SlideKitkat.CalculateSlide mSlideCalculator;
    private static final SlideKitkat.CalculateSlide sCalculateLeft = new SlideKitkat.CalculateSlideHorizontal() {
        public float getGone(View view) {
            return view.getTranslationX() - (float)view.getWidth();
        }
    };
    private static final SlideKitkat.CalculateSlide sCalculateTop = new SlideKitkat.CalculateSlideVertical() {
        public float getGone(View view) {
            return view.getTranslationY() - (float)view.getHeight();
        }
    };
    private static final SlideKitkat.CalculateSlide sCalculateRight = new SlideKitkat.CalculateSlideHorizontal() {
        public float getGone(View view) {
            return view.getTranslationX() + (float)view.getWidth();
        }
    };
    private static final SlideKitkat.CalculateSlide sCalculateBottom = new SlideKitkat.CalculateSlideVertical() {
        public float getGone(View view) {
            return view.getTranslationY() + (float)view.getHeight();
        }
    };
    private static final SlideKitkat.CalculateSlide sCalculateStart = new SlideKitkat.CalculateSlideHorizontal() {
        public float getGone(View view) {
            return view.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL ? view.getTranslationX() + (float)view.getWidth() : view.getTranslationX() - (float)view.getWidth();
        }
    };
    private static final SlideKitkat.CalculateSlide sCalculateEnd = new SlideKitkat.CalculateSlideHorizontal() {
        public float getGone(View view) {
            return view.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL ? view.getTranslationX() - (float)view.getWidth() : view.getTranslationX() + (float)view.getWidth();
        }
    };

    public SlideKitkat() {
        this.setSlideEdge(80);
    }

    public SlideKitkat(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.lbSlide);
        int edge = a.getInt(R.styleable.lbSlide_lb_slideEdge, 80);
        this.setSlideEdge(edge);
        long duration = (long)a.getInt(R.styleable.lbSlide_android_duration, -1);
        if (duration >= 0L) {
            this.setDuration(duration);
        }

        long startDelay = (long)a.getInt(R.styleable.lbSlide_android_startDelay, -1);
        if (startDelay > 0L) {
            this.setStartDelay(startDelay);
        }

        int resID = a.getResourceId(R.styleable.lbSlide_android_interpolator, 0);
        if (resID > 0) {
            this.setInterpolator(AnimationUtils.loadInterpolator(context, resID));
        }

        a.recycle();
    }

    public void setSlideEdge(int slideEdge) {
        switch(slideEdge) {
            case 3:
                this.mSlideCalculator = sCalculateLeft;
                break;
            case 5:
                this.mSlideCalculator = sCalculateRight;
                break;
            case 48:
                this.mSlideCalculator = sCalculateTop;
                break;
            case 80:
                this.mSlideCalculator = sCalculateBottom;
                break;
            case 8388611:
                this.mSlideCalculator = sCalculateStart;
                break;
            case 8388613:
                this.mSlideCalculator = sCalculateEnd;
                break;
            default:
                throw new IllegalArgumentException("Invalid slide direction");
        }

        this.mSlideEdge = slideEdge;
    }

    public int getSlideEdge() {
        return this.mSlideEdge;
    }

    private Animator createAnimation(View view, Property<View, Float> property, float start, float end, float terminalValue, TimeInterpolator interpolator, int finalVisibility) {
        float[] startPosition = (float[])((float[])view.getTag(R.id.lb_slide_transition_value));
        if (startPosition != null) {
            start = View.TRANSLATION_Y == property ? startPosition[1] : startPosition[0];
            view.setTag(R.id.lb_slide_transition_value, (Object)null);
        }

        ObjectAnimator anim = ObjectAnimator.ofFloat(view, property, start, end);
        SlideKitkat.SlideAnimatorListener listener = new SlideKitkat.SlideAnimatorListener(view, property, terminalValue, end, finalVisibility);
        anim.addListener(listener);
        anim.addPauseListener(listener);
        anim.setInterpolator(interpolator);
        return anim;
    }

    public Animator onAppear(ViewGroup sceneRoot, TransitionValues startValues, int startVisibility, TransitionValues endValues, int endVisibility) {
        View view = endValues != null ? endValues.view : null;
        if (view == null) {
            return null;
        } else {
            float end = this.mSlideCalculator.getHere(view);
            float start = this.mSlideCalculator.getGone(view);
            return this.createAnimation(view, this.mSlideCalculator.getProperty(), start, end, end, sDecelerate, 0);
        }
    }

    public Animator onDisappear(ViewGroup sceneRoot, TransitionValues startValues, int startVisibility, TransitionValues endValues, int endVisibility) {
        View view = startValues != null ? startValues.view : null;
        if (view == null) {
            return null;
        } else {
            float start = this.mSlideCalculator.getHere(view);
            float end = this.mSlideCalculator.getGone(view);
            return this.createAnimation(view, this.mSlideCalculator.getProperty(), start, end, start, sAccelerate, 4);
        }
    }

    private static class SlideAnimatorListener extends AnimatorListenerAdapter {
        private boolean mCanceled = false;
        private float mPausedValue;
        private final View mView;
        private final float mEndValue;
        private final float mTerminalValue;
        private final int mFinalVisibility;
        private final Property<View, Float> mProp;

        public SlideAnimatorListener(View view, Property<View, Float> prop, float terminalValue, float endValue, int finalVisibility) {
            this.mProp = prop;
            this.mView = view;
            this.mTerminalValue = terminalValue;
            this.mEndValue = endValue;
            this.mFinalVisibility = finalVisibility;
            view.setVisibility(View.VISIBLE);
        }

        public void onAnimationCancel(Animator animator) {
            float[] transitionPosition = new float[]{this.mView.getTranslationX(), this.mView.getTranslationY()};
            this.mView.setTag(R.id.lb_slide_transition_value, transitionPosition);
            this.mProp.set(this.mView, this.mTerminalValue);
            this.mCanceled = true;
        }

        public void onAnimationEnd(Animator animator) {
            if (!this.mCanceled) {
                this.mProp.set(this.mView, this.mTerminalValue);
            }

            this.mView.setVisibility(this.mFinalVisibility);
        }

        public void onAnimationPause(Animator animator) {
            this.mPausedValue = (Float)this.mProp.get(this.mView);
            this.mProp.set(this.mView, this.mEndValue);
            this.mView.setVisibility(this.mFinalVisibility);
        }

        public void onAnimationResume(Animator animator) {
            this.mProp.set(this.mView, this.mPausedValue);
            this.mView.setVisibility(View.VISIBLE);
        }
    }

    private abstract static class CalculateSlideVertical implements SlideKitkat.CalculateSlide {
        CalculateSlideVertical() {
        }

        public float getHere(View view) {
            return view.getTranslationY();
        }

        public Property<View, Float> getProperty() {
            return View.TRANSLATION_Y;
        }
    }

    private abstract static class CalculateSlideHorizontal implements SlideKitkat.CalculateSlide {
        CalculateSlideHorizontal() {
        }

        public float getHere(View view) {
            return view.getTranslationX();
        }

        public Property<View, Float> getProperty() {
            return View.TRANSLATION_X;
        }
    }

    private interface CalculateSlide {
        float getGone(View var1);

        float getHere(View var1);

        Property<View, Float> getProperty();
    }
}
