/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;

import androidx.annotation.NonNull;

import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;

import java.util.ArrayList;

class NetworkConnectManager {
    interface NetworkConnectListener {
        void onNetworkConnected();
    }

    private final String TAG = NetworkConnectManager.class.getSimpleName();
    private ArrayList<NetworkConnectListener> listeners = new ArrayList<>();
    private Context context;

    private ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback() {
        @Override
        public void onAvailable(@NonNull Network network) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onAvailable()");
            }
            fireOnNetworkConnected();
        }

        @Override
        public void onLost(@NonNull Network network) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onLost()");
            }
        }

        @Override
        public void onUnavailable() {
            if (Log.INCLUDE) {
                Log.d(TAG, "onUnavailable()");
            }
        }
    };

    NetworkConnectManager(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "NetworkConnectListener() this:0x" + Integer.toHexString(hashCode()));
        }
        this.context = context;
        ConnectivityManager connectivityManager = context.getSystemService(ConnectivityManager.class);
        connectivityManager.registerDefaultNetworkCallback(networkCallback);
    }

    private void dispose() {
        if (Log.INCLUDE) {
            Log.d(TAG, "dispose() this:0x" + Integer.toHexString(hashCode()));
        }
        ConnectivityManager connectivityManager = context.getSystemService(ConnectivityManager.class);
        connectivityManager.unregisterNetworkCallback(networkCallback);
        networkCallback = null;
    }

    boolean isDisconnected() {
        if (Log.INCLUDE) {
            Log.d(TAG, "isDisconnected():"+ !NetworkUtil.hasNetworkConnection(context));
        }
        return !NetworkUtil.hasNetworkConnection(context);
    }

    void addNetworkConnectListener(NetworkConnectListener listener) {
        if (!listeners.contains(listener)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "addNetworkConnectListener() listener:0x" + Integer.toHexString(listener.hashCode()));
            }
            listeners.add(listener);

            if (Log.INCLUDE) {
                listeners.forEach((v) -> {
                    Log.d(TAG, "addNetworkConnectListener() list:0x" + Integer.toHexString(v.hashCode()));
                });
            }
        }
    }

    void removeNetworkConnectListener(NetworkConnectListener listener) {
        if (listeners.contains(listener)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "removeNetworkConnectListener() listener:0x" + Integer.toHexString(listener.hashCode()));
            }
            listeners.remove(listener);

            if (Log.INCLUDE) {
                listeners.forEach((v) -> {
                    Log.d(TAG, "removeNetworkConnectListener() list:0x" + Integer.toHexString(v.hashCode()));
                });
            }
        }
    }

    private void fireOnNetworkConnected() {
        if (Log.INCLUDE) {
            Log.d(TAG, "fireOnNetworkConnected() this:0x" + Integer.toHexString(hashCode()));
        }

        for (NetworkConnectListener listener : new ArrayList<NetworkConnectListener>(listeners)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "fire onNetworkConnected() listener:0x" + Integer.toHexString(listener.hashCode()));
            }
            listener.onNetworkConnected();
        }
    }

}
