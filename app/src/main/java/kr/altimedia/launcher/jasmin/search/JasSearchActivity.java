/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.search;

import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.util.Log;

import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.ui.component.activity.BaseActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ChannelErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.Linker;
import kr.altimedia.launcher.jasmin.ui.util.PermissionUtils;

/*
 * JasSearchActivity
 */
public class JasSearchActivity extends BaseActivity {
    private final String TAG = JasSearchActivity.class.getSimpleName();

    private TvOverlayManager mTvOverlayManager;
    private ChannelDataManager mChannelDataManager;
    private ProgramDataManager mProgramDataManager;
    private TvInputManagerHelper mTvInputManagerHelper;

    private final TvOverlayManager.OnOverlayDialogQueueListener queueListener = new TvOverlayManager.OnOverlayDialogQueueListener() {
        @Override
        public void onSizeChanged(int size) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onSizeChanged : " + size);
            }
            if (size == 0) {
                finish();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mTvOverlayManager != null) {
            mTvOverlayManager.removeOnQueueListener(queueListener);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mChannelDataManager = TvSingletons.getSingletons(this).getChannelDataManager();
        mProgramDataManager = TvSingletons.getSingletons(this).getProgramDataManager();
        mTvInputManagerHelper = TvSingletons.getSingletons(this).getTvInputManagerHelper();

        ViewGroup parentScene = findViewById(R.id.controllerFrame);
        mTvOverlayManager = new TvOverlayManager(this,
                parentScene,
                mChannelDataManager,
                mTvInputManagerHelper,
                mProgramDataManager);

        mTvOverlayManager.addOnQueueListener(queueListener);

        Intent intent = getIntent();
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreate: intent=" + intent);
        }
        if (intent != null) {
            onSearchIntentReceived(intent);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (Log.INCLUDE) {
            Log.d(TAG, "onNewIntent: intent=" + intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Log.INCLUDE) {
            Log.d(TAG, "onStart");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Log.INCLUDE) {
            Log.d(TAG, "onResume");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Log.INCLUDE) {
            Log.d(TAG, "onPause");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (Log.INCLUDE) {
            Log.d(TAG, "onStop");
        }
    }

    public TvOverlayManager getTvOverlayManager() {
        return mTvOverlayManager;
    }

    private void onSearchIntentReceived(Intent intent) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSearchIntentReceived: intent=" + intent);
        }
        String action = intent.getAction();
        Uri uri = intent.getData();
        if (Log.INCLUDE) {
            Log.d(TAG, "onSearchIntentReceived: action=" + action + ", uri=" + uri);
        }
        if (action != null) {
            if (action.equals(JasSearchManager.SEARCH_ACTION_VOD)) {
                String contentId = uri.toString();
                String seriesFlag = "";
                try {
                    seriesFlag = intent.getExtras().getString(SearchManager.EXTRA_DATA_KEY);
                }catch (Exception e){
                }
                JasSearchManager.getInstance().goToVodDetail(this, contentId, seriesFlag);
            } else if (action.equals(JasSearchManager.SEARCH_ACTION_CHANNEL)) {
                String serviceId = uri.toString();
                JasSearchManager.getInstance().tuneToChannel(this, serviceId);
            } else if (action.equals(JasSearchManager.SEARCH_ACTION_MONOMAX)) {
                JasSearchManager.getInstance().startApp(this, JasSearchManager.PKG_MONOMAX);
            }
//            else if (action.equals(JasSearchManager.SEARCH_ACTION_YOUTUBE)) {
//                startApp(activity, "com.google.android.youtube.tv");
//            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onKeyDown: keyCode=" + keyCode);
        }

        if(mTvOverlayManager == null) {
            return super.onKeyDown(keyCode, event);
        }
        if (mTvOverlayManager.onKeyTvKey(keyCode, event)) {
            return true;
        }
        switch (keyCode) {

            case KeyEvent.KEYCODE_GUIDE:
            case KeyEvent.KEYCODE_TV:
            case KeyEvent.KEYCODE_BUTTON_1:
                if (!TvSingletons.getSingletons(this).getBackendKnobs().isEpgReady(true)) {
                    String errorTitle = getString(R.string.error_title_dbs);
                    TvSingletons baseSingletons = TvSingletons.getSingletons(this);
                    ChannelErrorDialogFragment dialogFragment = ChannelErrorDialogFragment.newInstance(
                            baseSingletons.getChannelDataManager(), keyCode, TAG, errorTitle, baseSingletons.getBackendKnobs());
                    dialogFragment.show(getSupportFragmentManager(), ChannelErrorDialogFragment.CLASS_NAME);

                    //check READ_TV_LISTINGS permission
                    if (!PermissionUtils.hasReadTvListings(this)) {
                        PermissionUtils.requestPermissions(this, PermissionUtils.PERMISSION_READ_TV_LISTINGS, 0);
                    }

                    break;
                }
                if (keyCode == KeyEvent.KEYCODE_GUIDE) {
                    CWMPServiceProvider.getInstance().notifyHotKeyClicked(keyCode);
                    Intent intent = new Intent(PlaybackUtil.ACTION_START_GUIDE);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else if (keyCode == KeyEvent.KEYCODE_TV) {
                    Intent intent = new Intent(PlaybackUtil.ACTION_START_LIVE);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else if (keyCode == KeyEvent.KEYCODE_BUTTON_1) {
//                    Intent intent = new Intent(PlaybackUtil.ACTION_START_FAV_GUIDE);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
                    Linker.gotoMyMenu(mTvOverlayManager, getSupportFragmentManager(), this);
                }

                return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
