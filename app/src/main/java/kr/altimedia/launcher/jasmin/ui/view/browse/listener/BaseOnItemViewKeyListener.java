/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse.listener;

import androidx.leanback.widget.Presenter;

public interface BaseOnItemViewKeyListener<T> {
    boolean onItemKey(int keyCode, Presenter.ViewHolder var1, Object var2, Presenter.ViewHolder var3, T var4);
}
