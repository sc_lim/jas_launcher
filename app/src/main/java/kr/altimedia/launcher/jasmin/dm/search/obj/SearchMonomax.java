
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search.obj;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchMonomax implements Parcelable {
    public static final Creator<SearchMonomax> CREATOR = new Creator<SearchMonomax>() {
        @Override
        public SearchMonomax createFromParcel(Parcel in) {
            return new SearchMonomax(in);
        }

        @Override
        public SearchMonomax[] newArray(int size) {
            return new SearchMonomax[size];
        }
    };
    @Expose
    @SerializedName("poster")
    private String poster;
    @Expose
    @SerializedName("updated")
    private String updated;
    @Expose
    @SerializedName("category")
    private String category;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("detail")
    private String detail;
    @Expose
    @SerializedName("id")
    private String id;
    @Expose
    @SerializedName("directors")
    private String directors;
    @Expose
    @SerializedName("year")
    private String year;
    @Expose
    @SerializedName("actors")
    private String actors;

    protected SearchMonomax(Parcel in) {
        poster = in.readString();
        updated = in.readString();
        category = in.readString();
        title = in.readString();
        detail = in.readString();
        id = in.readString();
        directors = in.readString();
        year = in.readString();
        actors = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(poster);
        parcel.writeString(updated);
        parcel.writeString(category);
        parcel.writeString(title);
        parcel.writeString(detail);
        parcel.writeString(id);
        parcel.writeString(directors);
        parcel.writeString(year);
        parcel.writeString(actors);
    }

    public String getPoster() {
        return poster;
    }

    public String getUpdated() {
        return updated;
    }

    public String getCategory() {
        return category;
    }

    public String getTitle() {
        return title;
    }

    public String getDetail() {
        return detail;
    }

    public String getId() {
        return id;
    }

    public String getDirectors() {
        return directors;
    }

    public String getYear() {
        return year;
    }

    public String getActors() {
        return actors;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @NonNull
    @Override
    public String toString() {
        return "SearchChannel{" +
                "poster=" + poster +
                ", updated=" + updated +
                ", category=" + category +
                ", title=" + title +
                ", detail=" + detail +
                ", id=" + id +
                ", directors=" + directors +
                ", year=" + year +
                ", actors=" + actors +
                "}";
    }
}