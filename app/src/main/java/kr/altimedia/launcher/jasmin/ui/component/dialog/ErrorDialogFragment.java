/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public class ErrorDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = ErrorDialogFragment.class.getName();
    private final String TAG = ErrorDialogFragment.class.getSimpleName();

    private static final String KEY_FROM_CLASS_NAME = "FROM_CLASS_NAME";
    private static final String KEY_ERROR_CODE = "ERROR_CODE";
    private static final String KEY_ERROR_DESCRIPTION = "ERROR_DESCRIPTION";

    private DialogInterface.OnDismissListener mOnDismissListener = null;


    public ErrorDialogFragment() {
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public static ErrorDialogFragment newInstance(String fromClassName, String errorCode, String description) {

        Bundle args = new Bundle();
        args.putString(KEY_FROM_CLASS_NAME, fromClassName);
        args.putString(KEY_ERROR_CODE, errorCode);
        args.putString(KEY_ERROR_DESCRIPTION, description);

        ErrorDialogFragment fragment = new ErrorDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnDismissListener(this);
        return dialog;
    }


    public void setOnDismissListener(@NonNull DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_error, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initVieW(view);
        initButton(view);
    }

    private void initVieW(View view) {
        Bundle mBundle = getArguments();
        String fromClass = mBundle.getString(KEY_FROM_CLASS_NAME);
        String errorCode = mBundle.getString(KEY_ERROR_CODE);
        String description = mBundle.getString(KEY_ERROR_DESCRIPTION);

        if (Log.INCLUDE) {
            Log.d(TAG, "error code : " + errorCode + ", fromClass : " + fromClass);
        }

        TextView popupTitleView = view.findViewById(R.id.popupTitle);
        TextView errorCodeView = view.findViewById(R.id.error_code);
        TextView errorMessageView = view.findViewById(R.id.error_message);

        String popupTitle = ErrorMessageManager.getInstance().getPopUpTitle(view.getContext(), errorCode);
        if (!StringUtils.nullToEmpty(popupTitle).isEmpty()) {
            popupTitleView.setText(popupTitle);
        }
        errorCodeView.setText(errorCode);
        errorMessageView.setText(description);
    }

    private void initButton(View view) {
        TextView closeButton = view.findViewById(R.id.close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
            mOnDismissListener = null;
        }
    }

}
