/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.databinding.FragmentOsdBinding;
import kr.altimedia.launcher.jasmin.tv.viewModel.OSDViewModel;
import kr.altimedia.launcher.jasmin.ui.component.activity.BaseActivity;
import kr.altimedia.launcher.jasmin.ui.component.fragment.interactor.OnFragmentVisibilityInteractor;

public class OSDFragment extends Fragment {
    public static final String CLASS_NAME = OSDFragment.class.getName();
    private OnFragmentVisibilityInteractor mOnFragmentVisibilityInteractor = null;


    public OSDFragment() {
        // Required empty public constructor
    }

    public static OSDFragment newInstance() {
        OSDFragment fragment = new OSDFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentOsdBinding miniGuideBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_osd, container, false);
        miniGuideBinding.setOSDViewModel(getOSDViewModel());
        return miniGuideBinding.getRoot();
    }

    private OSDViewModel getOSDViewModel() {
        return ((BaseActivity) getActivity()).getOSDViewModel();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentVisibilityInteractor) {
            mOnFragmentVisibilityInteractor = (OnFragmentVisibilityInteractor) context;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mOnFragmentVisibilityInteractor != null) {
            mOnFragmentVisibilityInteractor.show(CLASS_NAME);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mOnFragmentVisibilityInteractor != null) {
            mOnFragmentVisibilityInteractor.hide(CLASS_NAME);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnFragmentVisibilityInteractor = null;
    }

}
