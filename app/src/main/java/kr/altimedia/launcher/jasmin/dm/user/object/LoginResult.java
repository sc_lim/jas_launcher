/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

/**
 * Created by mc.kim on 16,06,2020
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.util.Log;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResult implements Parcelable {
    public static final Creator<LoginResult> CREATOR = new Creator<LoginResult>() {
        @Override
        public LoginResult createFromParcel(Parcel in) {
            return new LoginResult(in);
        }

        @Override
        public LoginResult[] newArray(int size) {
            return new LoginResult[size];
        }
    };
    @Expose
    @SerializedName("baseUrl")
    private String baseUrl;
    @Expose
    @SerializedName("closedCaptionYn")
    private String closedCaptionYn;
    @Expose
    @SerializedName("lastChannel")
    private String lastChannel;
    @Expose
    @SerializedName("loginId")
    private String loginId;
    @Expose
    @SerializedName("loginToken")
    private String loginToken;
    @Expose
    @SerializedName("regionCode")
    private String regionCode;
    @Expose
    @SerializedName("watchingTime")
    private String watchingTime;
    @Expose
    @SerializedName("watchingHistoryCycle")
    private String watchingHistoryCycle;
    @Expose
    @SerializedName("profile")
    private List<ProfileInfo> profileInfoList;
    @Expose
    @SerializedName("accountId")
    private String accountId;

    @Expose
    @SerializedName("accountName")
    private String accountName;

    @Expose
    @SerializedName("productCode")
    private String productCode;

    @Expose
    @SerializedName("productName")
    private String productName;
    @Expose
    @SerializedName("channelPreviewTime")
    private long channelPreviewTime;
    @Expose
    @SerializedName("channelPreviewLimitTime")
    private long channelPreviewLimitTime;

    protected LoginResult(Parcel in) {
        baseUrl = in.readString();
        closedCaptionYn = in.readString();
        lastChannel = in.readString();
        loginId = in.readString();
        loginToken = in.readString();
        regionCode = in.readString();
        watchingTime = in.readString();
        watchingHistoryCycle = in.readString();
        profileInfoList = in.createTypedArrayList(ProfileInfo.CREATOR);
        accountId = in.readString();
        accountName = in.readString();
        productCode = in.readString();
        productName = in.readString();
        channelPreviewTime = in.readLong();
        channelPreviewLimitTime = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(baseUrl);
        dest.writeString(closedCaptionYn);
        dest.writeString(lastChannel);
        dest.writeString(loginId);
        dest.writeString(loginToken);
        dest.writeString(regionCode);
        dest.writeString(watchingTime);
        dest.writeString(watchingHistoryCycle);
        dest.writeTypedList(profileInfoList);
        dest.writeString(accountId);
        dest.writeString(accountName);
        dest.writeString(productCode);
        dest.writeString(productName);
        dest.writeLong(channelPreviewTime);
        dest.writeLong(channelPreviewLimitTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getClosedCaptionYn() {
        return closedCaptionYn;
    }

    public String getLastChannel() {
        return lastChannel;
    }

    public String getLoginId() {
        return loginId;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public String getRegionCode() {
        return regionCode;
    }


    public String getWatchingTime() {
        return watchingTime;
    }

    public String getWatchingHistoryCycle() {
        return watchingHistoryCycle;
    }

    public List<ProfileInfo> getProfileInfoList() {
        return profileInfoList;
    }

    public void setProfileInfoList(List<ProfileInfo> profileInfoList) {
        this.profileInfoList = profileInfoList;
    }

    public void updateProfileInfo(ProfileInfo profileInfo) {
        for (int i = 0; i < profileInfoList.size(); i++) {
            if (profileInfoList.get(i).getProfileId().equalsIgnoreCase(profileInfo.getProfileId())) {
                profileInfoList.set(i, profileInfo);
            }
        }
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getProductName() {
        return productName;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public long getChannelPreviewTime() {
        return channelPreviewTime;
    }

    public long getChannelPreviewLimitTime() {
        return channelPreviewLimitTime;
    }

    public void setLastProfile(String newProfileId) {
        for (ProfileInfo profileInfo : profileInfoList) {
            boolean login = profileInfo.getProfileId().equalsIgnoreCase(newProfileId);
            if (Log.INCLUDE) {
                Log.d("setLastProfile", "setLastProfile : " + login +
                        ", profileInfo.getProfileId() : " + profileInfo.getProfileId() + ", newProfileId : " + newProfileId);
            }
            profileInfo.setLastLoginYN(login);
        }
    }

    @Override
    public String toString() {
        return "LoginResult{" +
                "baseUrl='" + baseUrl + '\'' +
                ", closedCaptionYn='" + closedCaptionYn + '\'' +
                ", lastChannel='" + lastChannel + '\'' +
                ", loginId='" + loginId + '\'' +
                ", loginToken='" + loginToken + '\'' +
                ", regionCode='" + regionCode + '\'' +
                ", watchingTime='" + watchingTime + '\'' +
                ", watchingHistoryCycle='" + watchingHistoryCycle + '\'' +
                ", profileInfoList=" + profileInfoList +
                ", accountId='" + accountId + '\'' +
                ", accountName='" + accountName + '\'' +
                ", productCode='" + productCode + '\'' +
                ", channelPreviewTime=" + channelPreviewTime +
                ", channelPreviewLimitTime=" + channelPreviewLimitTime +
                '}';
    }
}