/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionPriceInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.row.DurationRow;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class SubscriptionDurationPresenter extends Presenter {
    private static final String TAG = SubscriptionDurationPresenter.class.getName();

    private OnSelectSubscriptionDurationListener onSelectSubscriptionDurationListener;

    public SubscriptionDurationPresenter() {
    }

    public void setOnSelectSubscriptionDurationListener(OnSelectSubscriptionDurationListener onSelectSubscriptionDurationListener) {
        this.onSelectSubscriptionDurationListener = onSelectSubscriptionDurationListener;
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_subscription_duration, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        DurationRow row = (DurationRow) item;
        SubscriptionInfo subscriptionInfo = (SubscriptionInfo) row.getAdapter().get(0);

        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData(subscriptionInfo);
        vh.setOnSelectSubscriptionDurationListener(onSelectSubscriptionDurationListener);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public interface OnSelectSubscriptionDurationListener {
        void onFocusSubscriptionDuration(SubscriptionPriceInfo subscriptionPriceInfo);

        void onSelectSubscriptionDuration(SubscriptionPriceInfo subscriptionPriceInfo);
    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private HorizontalGridView gridView;
        private DurationItemBridgeAdapter durationItemBridgeAdapter;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            gridView = view.findViewById(R.id.duration_grid_view);
        }

        public void setData(SubscriptionInfo subscriptionInfo) {
            ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter(new SubscriptionCheckDurationPresenter());
            arrayObjectAdapter.addAll(0, subscriptionInfo.getSubscriptionPriceInfoList());
            durationItemBridgeAdapter = new DurationItemBridgeAdapter(arrayObjectAdapter, gridView);
            gridView.setAdapter(durationItemBridgeAdapter);
            int gap = (int) view.getResources().getDimension(R.dimen.coupon_subscription_button_spacing);
            gridView.setHorizontalSpacing(gap);
        }

        public void setOnSelectSubscriptionDurationListener(OnSelectSubscriptionDurationListener onSelectSubscriptionDurationListener) {
            durationItemBridgeAdapter.setOnSelectSubscriptionDurationListener(onSelectSubscriptionDurationListener);
        }
    }

    private static class DurationItemBridgeAdapter extends ItemBridgeAdapter {
        private final float ALIGNMENT = 87.8f;

        private SubscriptionPriceInfo selectedPrice;

        private HorizontalGridView gridView;
        private OnSelectSubscriptionDurationListener onSelectSubscriptionDurationListener;

        public DurationItemBridgeAdapter(ObjectAdapter adapter, HorizontalGridView gridView) {
            super(adapter);

            this.gridView = gridView;
            gridView.setWindowAlignmentOffsetPercent(ALIGNMENT);
        }

        public void setOnSelectSubscriptionDurationListener(OnSelectSubscriptionDurationListener onSelectSubscriptionDurationListener) {
            this.onSelectSubscriptionDurationListener = onSelectSubscriptionDurationListener;
        }

        @Override
        protected void onBind(ViewHolder viewHolder) {
            super.onBind(viewHolder);

            SubscriptionCheckDurationPresenter.ViewHolder vh = (SubscriptionCheckDurationPresenter.ViewHolder) viewHolder.getViewHolder();
            SubscriptionPriceInfo subscriptionPriceInfo = (SubscriptionPriceInfo) viewHolder.getItem();
            View view = vh.view;

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SubscriptionPriceInfo price = vh.isChecked() ? null : subscriptionPriceInfo;
                    updateCheckBox(vh, price);

                    if (onSelectSubscriptionDurationListener != null) {
                        onSelectSubscriptionDurationListener.onSelectSubscriptionDuration(selectedPrice);
                    }
                }
            });

            view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus && onSelectSubscriptionDurationListener != null) {
                        onSelectSubscriptionDurationListener.onFocusSubscriptionDuration(subscriptionPriceInfo);
                    }
                }
            });
        }

        @Override
        protected void onUnbind(ViewHolder viewHolder) {
            super.onUnbind(viewHolder);

            SubscriptionCheckDurationPresenter.ViewHolder vh = (SubscriptionCheckDurationPresenter.ViewHolder) viewHolder.mHolder;

            View view = vh.view;
            view.setOnClickListener(null);
            view.setOnFocusChangeListener(null);
        }

        private void updateCheckBox(SubscriptionCheckDurationPresenter.ViewHolder vh, SubscriptionPriceInfo price) {
            if (price != null && selectedPrice != null) {
                ArrayObjectAdapter arrayObjectAdapter = (ArrayObjectAdapter) getAdapter();
                int index = arrayObjectAdapter.indexOf(selectedPrice);
                arrayObjectAdapter.replace(index, selectedPrice);
            }

            vh.toggleChecked();
            this.selectedPrice = price;
        }
    }
}
