/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.media;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.player.AltiMediaSource;
import com.altimedia.player.AltiPlayer;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.cwmp.service.def.CWMPEtcCode;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.AppConfig;

/**
 * Created by mc.kim on 27,04,2020
 */
public class MediaPlayerAdapter extends VideoPlayerAdapter {
    private final static String TAG = MediaPlayerAdapter.class.getSimpleName();
    Context mContext;
    AltiPlayer mPlayer;
    RelativeLayout mRelativeLayout;
    SurfaceView mSurfaceView;
    FingerPrinting mFingerPrinting;
    final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (getCallback() == null) {
                return;
            }
            getCallback().onCurrentPositionChanged(MediaPlayerAdapter.this);
            mHandler.postDelayed(this, getProgressUpdatingInterval());
        }
    };
    final Handler mHandler = new Handler();
    boolean mInitialized = false; // true when the MediaPlayer is prepared/initialized
    AltiMediaSource[] mMediaSourceUri = null;
    boolean mHasDisplay;
    long mBufferedProgress;
    private SettingControl.SettingsEventListener mSettingsEventListener;

    private long mTuneDelayTime = 0L;

    public void setTuneDelayTime(long tuneDelayTime) {
        this.mTuneDelayTime = tuneDelayTime;
    }

    private VideoPlayerSurfaceHolderCallback mVideoPlayerSurfaceHolderCallback = new VideoPlayerSurfaceHolderCallback();

    public VideoPlayerSurfaceHolderCallback getVideoPlayerSurfaceHolderCallback() {
        return mVideoPlayerSurfaceHolderCallback;
    }


    private boolean mIsLimitMode = false;
    MediaEventListener mEventListener = new MediaEventListener(this);
    private boolean isAutoPlay = true;

    private QMManager mQMManager = null;

    private void prepareMediaForPlaying() {
        reset();
        if (Log.INCLUDE) {
            Log.d("MediaPlayerAdapter", "mMediaSourceUri : " + mMediaSourceUri.toString());
        }
        if (mMediaSourceUri == null || mPlayer == null) {
            return;
        }
        if (mRelativeLayout != null && mSurfaceView != null && AppConfig.FEATURE_FINGER_PRINT_ENABLED) {
            mFingerPrinting = new FingerPrinting();
            mFingerPrinting.initializePF(mRelativeLayout, mContext, mSurfaceView, AccountManager.getInstance().getSaId());
        }
        mEventListener.setJasMediaSource(mMediaSourceUri, 0);
        mPlayer.setEventListener(mEventListener);
        mPlayer.setVideoListener(mOnVideoListener);
//        notifyBufferingStartEnd();
        mPlayer.prepare(mMediaSourceUri[0]);
        getCallback().onPlayStateChanged(MediaPlayerAdapter.this, VideoState.PREPARE);
    }

    AltiPlayer.VideoListener mOnVideoListener = new AltiPlayer.VideoListener() {
        @Override
        public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

        }

        @Override
        public void onSurfaceSizeChanged(int width, int height) {
            if (mFingerPrinting != null) {
                mFingerPrinting.resizePF(width, height);
            }
        }

        @Override
        public void onRenderedFirstFrame() {

        }
    };


    private Runnable mPlayerRunnable = new Runnable() {
        @Override
        public void run() {
            if (Log.INCLUDE) {
                Log.d(TAG, "isAutoPlay : " + isAutoPlay);
            }
            if (mMediaSourceUri == null) {
                return;
            }
            if (mPlayer == null) {
                return;
            }
            if (isAutoPlay) {
                mPlayer.play();
            }
            getCallback().onPlayStateChanged(MediaPlayerAdapter.this, VideoState.READY);
            notifyBufferingStartEnd();
            if (mHasDisplay) {
                getCallback().onPreparedStateChanged(MediaPlayerAdapter.this);
            }
        }
    };
    private Handler mPlayerHandler = new Handler();

    private void onPrepare(AltiPlayer altiPlayer) {
        if (mPlayerRunnable != null) {
            mPlayerHandler.removeCallbacks(mPlayerRunnable);
        }
        mInitialized = true;
        mPlayerHandler.postDelayed(mPlayerRunnable, mTuneDelayTime);
    }

    public void requestPlayWhenPause() {
        mInitialized = true;
        play();
        getCallback().onPlayStateChanged(MediaPlayerAdapter.this, VideoState.READY);
        notifyBufferingStartEnd();
        if (mHasDisplay) {
            getCallback().onPreparedStateChanged(MediaPlayerAdapter.this);
        }
    }


    private void onComplete() {
        getCallback().onPlayStateChanged(MediaPlayerAdapter.this, VideoState.COMPLETE);
        getCallback().onPlayCompleted(MediaPlayerAdapter.this);
    }

    final MediaPlayer.OnCompletionListener mOnCompletionListener =
            new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    getCallback().onPlayStateChanged(MediaPlayerAdapter.this, VideoState.COMPLETE);
                    getCallback().onPlayCompleted(MediaPlayerAdapter.this);
                }
            };

    final MediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener =
            new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                    mBufferedProgress = getDuration() * percent / 100;
                    getCallback().onBufferedPositionChanged(MediaPlayerAdapter.this);
                }
            };

    final MediaPlayer.OnVideoSizeChangedListener mOnVideoSizeChangedListener =
            new MediaPlayer.OnVideoSizeChangedListener() {
                @Override
                public void onVideoSizeChanged(MediaPlayer mediaPlayer, int width, int height) {
                    getCallback().onVideoSizeChanged(MediaPlayerAdapter.this, width, height);
                }
            };


    final MediaPlayer.OnSeekCompleteListener mOnSeekCompleteListener =
            new MediaPlayer.OnSeekCompleteListener() {
                @Override
                public void onSeekComplete(MediaPlayer mp) {
                    MediaPlayerAdapter.this.onSeekComplete();
                }
            };

    final MediaPlayer.OnInfoListener mOnInfoListener = new MediaPlayer.OnInfoListener() {
        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            boolean handled = false;
            switch (what) {
                case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                    mBufferingStart = true;
                    notifyBufferingStartEnd();
                    handled = true;
                    break;
                case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                    mBufferingStart = false;
                    notifyBufferingStartEnd();
                    handled = true;
                    break;
            }
            boolean thisHandled = MediaPlayerAdapter.this.onInfo(what, extra);
            return handled || thisHandled;
        }
    };

    boolean mBufferingStart;

    void notifyBufferingStartEnd() {
        getCallback().onBufferingStateChanged(MediaPlayerAdapter.this,
                mBufferingStart || !mInitialized);
    }

    public MediaPlayerAdapter(Context context) {
        this(context, null, null);
    }

    private long mLimitedStartTimeMillis = 0;

    public void setScaleMode(boolean cropping) {
        if (cropping) {
            mPlayer.setVideoScalingMode(AltiPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        }
    }

    public void onAttachedToHost() {
    }

    /**
     * Will reset the {@link MediaPlayer} and the glue such that a new file can be played. You are
     * not required to call this method before playing the first file. However you have to call it
     * before playing a second one.
     */
    public void reset() {
        changeToUnitialized();
//        mPlayer.reset();
    }

    void changeToUnitialized() {
        if (mInitialized) {
            mInitialized = false;
            notifyBufferingStartEnd();
            if (mHasDisplay) {
                getCallback().onPreparedStateChanged(MediaPlayerAdapter.this);
            }
        }
    }

    /**
     * Release internal MediaPlayer. Should not use the object after call release().
     */
    public void release() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call release");
        }
//        changeToUnitialized();
        mHasDisplay = false;
        if (mFingerPrinting != null) {
            mFingerPrinting.finalizePF();
            mFingerPrinting = null;
        }
        if (mPlayer != null) {
            if (mQMManager != null) {
                mQMManager.destroy();
                mQMManager = null;
            }
            mPlayer.setQMEventListener(null);
            mPlayer.releasePlayer();
        }
        mPlayer = null;

        if (mSettingsEventListener != null) {
            SettingControl.getInstance().removeSettingsEventListener(mSettingsEventListener);
            mSettingsEventListener = null;
        }
    }

    public void onDetachedFromHost() {
//        reset();
        if (Log.INCLUDE) {
            Log.d(TAG, "detachedFromHost");
        }
        release();


    }

    protected boolean onError(int type, String msg, Exception exception) {
        getCallback().onError(this, type, msg);
        return true;
    }

    /**
     * Called to indicate the completion of a seek operation.
     */
    protected void onSeekComplete() {
    }

    protected boolean onInfo(int what, int extra) {
        return false;
    }

    public void setSurfaceView(SurfaceView surfaceView) {
        boolean hadDisplay = mHasDisplay;
        mHasDisplay = surfaceView != null;
        if (hadDisplay == mHasDisplay) {
            return;
        }
        mPlayer.setVideoSurfaceView(surfaceView);
        if (mHasDisplay) {
            if (mInitialized) {
                getCallback().onPreparedStateChanged(MediaPlayerAdapter.this);
            }
        } else {
            if (mInitialized) {
                getCallback().onPreparedStateChanged(MediaPlayerAdapter.this);
            }
        }
    }

    public void setDisplay(SurfaceHolder surfaceHolder) {
        boolean hadDisplay = mHasDisplay;
        mHasDisplay = surfaceHolder != null;
        if (hadDisplay == mHasDisplay) {
            return;
        }
        mPlayer.setVideoSurfaceHolder(surfaceHolder);
        if (mHasDisplay) {
            if (mInitialized) {
                getCallback().onPreparedStateChanged(MediaPlayerAdapter.this);
            }
        } else {
            if (mInitialized) {
                getCallback().onPreparedStateChanged(MediaPlayerAdapter.this);
            }
        }

    }

    @Override
    public void setProgressUpdatingEnabled(final boolean enabled) {
        mHandler.removeCallbacks(mRunnable);
        if (!enabled) {
            return;
        }
        mHandler.postDelayed(mRunnable, getProgressUpdatingInterval());
    }

    /**
     * Return updating interval of progress UI in milliseconds. Subclass may override.
     *
     * @return Update interval of progress UI in milliseconds.
     */
    public int getProgressUpdatingInterval() {
        return 16;
    }

    @Override
    public boolean isPlaying() {
        return mInitialized && mPlayer != null && mEventListener.isPlaying();
    }

    private long mLimitedEndTimeMillis = 0;

    public MediaPlayerAdapter(Context context, RelativeLayout relativeLayout, SurfaceView surfaceView) {
        mContext = context;
        mPlayer = new AltiPlayer.Builder(context).build();
        if (Log.INCLUDE) {
            Log.d(TAG, "called player generated");
        }
        mRelativeLayout = relativeLayout;
        mSurfaceView = surfaceView;
        mIsLimitMode = false;

        mQMManager = new QMManager();
        mPlayer.setQMEventListener(mQMManager);

        mPlayer.setVideoInfoEnabled(SettingControl.getInstance().isDebugInfo());
        mSettingsEventListener = new SettingControl.SettingsEventListener() {
            public void onSettingChanged(String var1) {
                if (mPlayer != null) {
                    mPlayer.setVideoInfoEnabled(SettingControl.getInstance().isDebugInfo());
                }
            }
        };
        SettingControl.getInstance().addSettingsEventListener(mSettingsEventListener, new String[]{SettingControl.KEY_DEBUG_INFORMATION});
    }

    @Override
    public void play() {

        if (Log.INCLUDE) {
            Log.d(TAG, "call play | mInitialized : " + mInitialized + ", mPlayer.isPlaying() : " + mEventListener.isPlaying());
        }
        if (!mInitialized || mEventListener.isPlaying()) {
            return;
        }
        mPlayer.play();
        getCallback().onCurrentPositionChanged(MediaPlayerAdapter.this);
        getCallback().onPlayStateChanged(MediaPlayerAdapter.this, VideoState.PLAY);
    }

    @Override
    public void pause() {
        if (isPlaying()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "call Pause");
            }
            mPlayer.pause();
            getCallback().onPlayStateChanged(MediaPlayerAdapter.this, VideoState.PAUSE);
        }
    }


    @Override
    public void stop() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call stop ");
        }
        if (mPlayerRunnable != null) {
            mPlayerHandler.removeCallbacks(mPlayerRunnable);
        }
        if (mPlayer != null) {
            mPlayer.stop();
            mMediaSourceUri = null;
            mEventListener.reset();
            getCallback().onPlayStateChanged(MediaPlayerAdapter.this, VideoState.STOP);
        }
    }

    public void muteToggle(boolean isMute) {
        // mute
        if (mPlayer == null) {
            return;
        }
        if (isMute) {
            setSelectedAudioTrackIndex(-1);
        } else {
            setSelectedAudioTrackIndex(0);
        }
    }

    @Override
    public long getDuration() {
        if (Log.INCLUDE) {
            Log.d(TAG, "getDuration | mIsLimitMode : " + mIsLimitMode + ", mInitialized : " + mInitialized + ", mPlayer.getDuration() : " + mPlayer.getDuration());
        }
        if (mIsLimitMode) {
            return mInitialized ? mLimitedEndTimeMillis : -1;
        }
        return mInitialized ? mPlayer.getDuration() : -1;
    }

    @Override
    public long getBufferedPosition() {
        return mBufferedProgress;
    }

    /**
     * Sets the media source of the player witha given URI.
     *
     * @return Returns <code>true</code> if uri represents a new media; <code>false</code>
     * otherwise.
     * @see MediaPlayer#setDataSource(String)
     */
    public boolean setDataSource(AltiMediaSource[] altiMediaSource) {
        return setDataSource(altiMediaSource, 0, true);
    }

    public void resetInfo() {
        mMediaSourceUri = null;
    }

    @Override
    public long getCurrentPosition() {
        if (mIsLimitMode) {
            long fakeTime = mInitialized ? mPlayer != null ? mPlayer.getCurrentPosition() - mLimitedStartTimeMillis : -1 : -1;
            if (mInitialized && fakeTime >= getDuration() && isPlaying()) {
                pause();
                onComplete();
            }
            return fakeTime;
        }
        return mInitialized ? mPlayer != null ? mPlayer.getCurrentPosition() : -1 : -1;
    }

    @Override
    public void seekTo(long newPosition){
        if (!mInitialized) {
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "seekTo : " + newPosition);
        }
        long checkTime = 0;
        if (mIsLimitMode) {
            mPlayer.seekTo((int) newPosition + mLimitedStartTimeMillis);
            checkTime = (int) newPosition + mLimitedStartTimeMillis;
        } else {
            mPlayer.seekTo((int) newPosition);
            checkTime = (int) newPosition;
        }
        if (checkTime >= getDuration()) {
            mPlayer.stop();
            onComplete();
        }
    }

    public boolean setDataSourceLimitMode(AltiMediaSource[] altiMediaSource, long startTime, long endTime) {
        mIsLimitMode = true;
        mLimitedStartTimeMillis = startTime;
        mLimitedEndTimeMillis = endTime;
        isAutoPlay = true;
        if (Arrays.equals(mMediaSourceUri, altiMediaSource)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "isSame request so return : " + altiMediaSource.toString());
            }
            return false;
        }
        mMediaSourceUri = altiMediaSource;
        prepareMediaForPlaying();
        if (Log.INCLUDE) {
            Log.d(TAG, "startTime : " + startTime);
        }
        if (startTime != 0) {
            mPlayer.seekTo(startTime);
        }
        return true;
    }

    public boolean setDataSource(AltiMediaSource[] altiMediaSource, long startTime, boolean autoPlay) {
        if (Arrays.equals(mMediaSourceUri, altiMediaSource)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "isSame request so return : " + altiMediaSource.toString());
            }
            return false;
        }
        isAutoPlay = autoPlay;
        mIsLimitMode = false;
        mMediaSourceUri = altiMediaSource;
        prepareMediaForPlaying();
        if (Log.INCLUDE) {
            Log.d(TAG, "startTime : " + startTime);
        }
        if (startTime != 0) {
            mPlayer.seekTo(startTime);
        }
        return true;
    }

    private boolean isReady = false;

    private static class MediaEventListener implements AltiPlayer.EventListener {
        final MediaPlayerAdapter mAdapter;
        AltiMediaSource[] mAltiMediaSource;
        int mCurrentPlayIndex = 0;
        HashMap<AltiMediaSource, Integer> mMediaSourceRetryMap = new HashMap<>();
        private boolean isPlaying = false;
        public MediaEventListener(MediaPlayerAdapter adapter) {
            mAdapter = adapter;
        }

        public void setJasMediaSource(AltiMediaSource[] mAltiMediaSource, int currentIndex) {
            this.mAltiMediaSource = mAltiMediaSource;
            this.mCurrentPlayIndex = currentIndex;
            for (AltiMediaSource altiMediaSource : mAltiMediaSource) {
                mMediaSourceRetryMap.put(altiMediaSource, 0);
            }
        }

        public void reset() {
            isPlaying = false;
        }


        @Override
        public void onLoadingChanged(boolean isLoading) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onLoadingChanged : " + isLoading);
            }
        }

        public boolean isPlaying() {
            return isPlaying;
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            isPlaying = playWhenReady;
            if (Log.INCLUDE) {
                Log.d(TAG, "onPlayerStateChanged : " + playWhenReady + ", playbackState : "
                        + AltiPlayer.EventListener.stateToString(playbackState) + "(" + playbackState + ")");
            }
            if (mAdapter.getCallback() != null) {
                mAdapter.getCallback().setIsPlaying(playWhenReady);
            }
            switch (playbackState) {
                case AltiPlayer.STATE_IDLE:
                    break;
                case AltiPlayer.STATE_BUFFERING:
                    if (!mAdapter.isReady()) {
                        mAdapter.getCallback().onReady(mAdapter.isReady);
                    }
                    break;
                case AltiPlayer.STATE_READY:
                    if (!mAdapter.mInitialized) {
                        mAdapter.onPrepare(mAdapter.mPlayer);
                    }
                    if (!mAdapter.isReady()) {
                        mAdapter.isReady = true;
                        mAdapter.getCallback().onReady(mAdapter.isReady);
                    }
                    break;
                case AltiPlayer.STATE_ENDED:
                    mAdapter.onComplete();
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onPlaybackSuppressionReasonChanged(int playbackSuppressionReason) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onPlaybackSuppressionReasonChanged : " + playbackSuppressionReason);
            }
        }

        @Override
        public void onIsPlayingChanged(boolean isPlaying) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onIsPlayingChanged : " + isPlaying);
            }
            if (mAdapter.mFingerPrinting != null) {
                if (isPlaying) {
                    mAdapter.mFingerPrinting.startPF();
                } else {
                    mAdapter.mFingerPrinting.stopPF();
                }
            }
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onRepeatModeChanged : " + repeatMode);
            }
        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onShuffleModeEnabledChanged : " + shuffleModeEnabled);
            }
        }

        @Override
        public void onPositionDiscontinuity(int reason) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onPositionDiscontinuity : " + reason);
            }
        }


        @Override
        public void onSeekProcessed() {

            mAdapter.onSeekComplete();
            if (Log.INCLUDE) {
                Log.d(TAG, "onSeekProcessed");
            }
        }

        private void sendErrorLog(int type, String msg, Exception exception) {
            if (Log.INCLUDE) {
                Log.d(TAG, "sendErrorLog : type:" + type + ", msg:" + msg);
            }
            switch (type) {
                case AltiPlayer.PLAYER_ERROR_TYPE_SOURCE:
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_604);
                    break;
                case AltiPlayer.PLAYER_ERROR_TYPE_UNEXPECTED:
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_001);
                    break;
                case AltiPlayer.PLAYER_ERROR_TYPE_RENDERER:
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_603);
                    break;
                case AltiPlayer.PLAYER_ERROR_TYPE_REMOTE:
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_602);
                    break;
                case AltiPlayer.PLAYER_ERROR_TYPE_OUT_OF_MEMORY:
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_601);
                    break;
            }
        }

        @Override
        public void onPlayerError(int type, String msg, Exception exception) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onPlayerError : type:" + type + ", msg:" + msg);
            }
            sendErrorLog(type, msg, exception);
            switch (type) {
                case AltiPlayer.PLAYER_ERROR_TYPE_SOURCE:
                case AltiPlayer.PLAYER_ERROR_TYPE_UNEXPECTED: {
                    AltiMediaSource nextStream = nextSource();
                    if (nextStream == null) {
                        AltiMediaSource retryStream = retrySource();
                        if (retryStream == null) {
                            mAdapter.onError(type, msg, exception);
                        } else {
                            long currentPosition = mAdapter.getCurrentPosition();
                            mAdapter.mPlayer.prepare(retryStream);
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onPlayerError : tune try current Stream again : " + currentPosition);
                            }
                            if (currentPosition > 0) {
                                mAdapter.mPlayer.seekTo(currentPosition);
                            }
                        }
                    } else {
                        CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_827);
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onPlayerError : tune next Stream" + nextStream.toString());
                        }
                        long currentPosition = mAdapter.getCurrentPosition();
                        mAdapter.mPlayer.prepare(nextStream);
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onPlayerError : tune next Stream currentPosition : " + currentPosition);
                        }
                        if (currentPosition > 0) {
                            mAdapter.mPlayer.seekTo(currentPosition);
                        }
                    }
                }
                break;
                case AltiPlayer.PLAYER_ERROR_TYPE_RENDERER:
                case AltiPlayer.PLAYER_ERROR_TYPE_REMOTE:
                case AltiPlayer.PLAYER_ERROR_TYPE_OUT_OF_MEMORY: {
                    AltiMediaSource retryStream = retrySource();
                    if (retryStream == null) {
                        mAdapter.onError(type, msg, exception);
                    } else {
                        long currentPosition = mAdapter.getCurrentPosition();
                        mAdapter.mPlayer.prepare(retryStream);
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onPlayerError : tune try current Stream again : " + currentPosition);
                        }
                        if (currentPosition > 0) {
                            mAdapter.mPlayer.seekTo(currentPosition);
                        }
                    }
                }
                break;
            }

        }

        private AltiMediaSource nextSource() {
            int length = mAltiMediaSource.length;
            int currentIndex = mCurrentPlayIndex;
            int nextIndex = currentIndex + 1;
            if (length > nextIndex) {
                mCurrentPlayIndex = nextIndex;
                return mAltiMediaSource[mCurrentPlayIndex];
            }
            return null;
        }

        private AltiMediaSource retrySource() {
            AltiMediaSource currentMediaSource = mAltiMediaSource[mCurrentPlayIndex];
            int retryCount = mMediaSourceRetryMap.get(currentMediaSource);
            if (Log.INCLUDE) {
                Log.d(TAG, "retrySource mCurrentPlayIndex : " + mCurrentPlayIndex + ", retryCnt : " + retryCount);
            }
            if (retryCount <= 0) {
                mMediaSourceRetryMap.replace(currentMediaSource, retryCount + 1);
                return currentMediaSource;
            }
            return null;
        }
    }


    class VideoPlayerSurfaceHolderCallback implements SurfaceHolder.Callback {
        @Override
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            setDisplay(surfaceHolder);
        }

        @Override
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            setDisplay(null);
        }
    }

    public ArrayList<String> getSubtitleList() {

        ArrayList<String> subtitleList = mPlayer.getTrackList(AltiPlayer.TRACK_TYPE_TEXT);
        ArrayList<String> resultList = new ArrayList<>();
        for (String subtitle : subtitleList) {
            if (!StringUtils.nullToEmpty(subtitle).isEmpty() && !subtitle.equalsIgnoreCase("null")) {
                Locale locale = Locale.forLanguageTag(subtitle);
                if(locale == null){
                    resultList.add(subtitle);
                }else{
                    resultList.add(locale.getDisplayName());
                }
            }
        }
        return resultList;
    }

    private @AltiPlayer.SubtitleFontStyle
    int currentFontStyle = AltiPlayer.SUBTITLE_FONT_MEDIUM;

    /**
     * Returns the selected subtitle track index
     *
     * @return The selected subtitle track index or -1 if there is no selection
     */
    public int getSelectedSubtitleTrackIndex() {
        return mPlayer.getSelectedTrackIndex(AltiPlayer.TRACK_TYPE_TEXT);
    }

    public ArrayList<String> getAudioList() {
        ArrayList<String> audioList = mPlayer.getTrackList(AltiPlayer.TRACK_TYPE_AUDIO);
        ArrayList<String> resultList = new ArrayList<>();
        for (String audio : audioList) {
            if (!StringUtils.nullToEmpty(audio).isEmpty() && !audio.equalsIgnoreCase("null")) {
                Locale locale = Locale.forLanguageTag(audio);
                if(locale == null){
                    resultList.add(audio);
                }else{
                    resultList.add(locale.getDisplayName());
                }
            }
        }
        return resultList;
    }


    public void setSelectedSubtitleTrackByLanguageCode(String languageCode) {
        List<String> subtitleList = getSubtitleList();
        int index = subtitleList.indexOf(languageCode);
        if (index == -1) {
            Locale locale = Locale.forLanguageTag(languageCode);
            index = subtitleList.indexOf(locale.getDisplayName());
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "setSelectedSubtitleTrackByLanguageCode | lang :  " + languageCode);
            Log.d(TAG, "setSelectedSubtitleTrackByLanguageCode | index : " + index);
        }
        setSelectedSubtitleTrackIndex(index);
    }

    /**
     * set selected subtitle track index
     *
     * @param index - the track index to select or -1 to clear the track selection
     */
    public void setSelectedSubtitleTrackIndex(int index) {
        mPlayer.setSelectedTrackIndex(AltiPlayer.TRACK_TYPE_TEXT, index);
    }

    /**
     * Returns the selected audio track index
     *
     * @return The selected audio track index or -1 if there is no selection
     */
    public int getSelectedAudioTrackIndex() {
        return mPlayer.getSelectedTrackIndex(AltiPlayer.TRACK_TYPE_AUDIO);
    }

    public void insertSubtitleView(ViewGroup viewGroup) {
        mPlayer.insertSubtitleView(viewGroup);
    }

    public void setVideoInfoView(TextView videoInfoTextView) {
        mPlayer.setVideoInfoView(videoInfoTextView);
    }

    /**
     * set selected audio track index
     *
     * @param index - the track index to select or -1 to clear the track selection
     */
    public void setSelectedAudioTrackIndex(int index) {
        mPlayer.setSelectedTrackIndex(AltiPlayer.TRACK_TYPE_AUDIO, index);
    }

    public void setSubtitleStyle(String jsonString) {
        mPlayer.setSubtitleStyle(jsonString);
    }

    public void setSubtitleStyle(@AltiPlayer.SubtitleFontStyle int fontStyle) {
        mPlayer.setSubtitleStyle(fontStyle);
    }

    public String getCurrentFontStyle() {
        switch (currentFontStyle) {
            case AltiPlayer.SUBTITLE_FONT_SMALL:
                return mContext.getString(R.string.small);
            case AltiPlayer.SUBTITLE_FONT_MEDIUM:
                return mContext.getString(R.string.medium);
            case AltiPlayer.SUBTITLE_FONT_LARGE:
                return mContext.getString(R.string.large);
        }
        return "";
    }

    public void setSubtitleStyleByName(String name) {
        String small = mContext.getString(R.string.small);
        String medium = mContext.getString(R.string.medium);
        String large = mContext.getString(R.string.large);
        if (name.equalsIgnoreCase(small)) {
            currentFontStyle = AltiPlayer.SUBTITLE_FONT_SMALL;
        } else if (name.equalsIgnoreCase(medium)) {
            currentFontStyle = AltiPlayer.SUBTITLE_FONT_MEDIUM;
        } else if (name.equalsIgnoreCase(large)) {
            currentFontStyle = AltiPlayer.SUBTITLE_FONT_LARGE;
        }
        setSubtitleStyle(currentFontStyle);
    }

    public void setSubtitleStyleFontCode(String fontCode) {
        switch (fontCode) {
            case SettingControl.SUBTITLE_FONT_SMALL:
                currentFontStyle = AltiPlayer.SUBTITLE_FONT_SMALL;
                break;
            case SettingControl.SUBTITLE_FONT_LARGE:
                currentFontStyle = AltiPlayer.SUBTITLE_FONT_LARGE;
                break;
            default:
                currentFontStyle = AltiPlayer.SUBTITLE_FONT_MEDIUM;
                break;
        }
        setSubtitleStyle(currentFontStyle);
    }

    @Override
    public void resume() {
        mPlayer.play();
    }

    @Override
    public boolean isPrepared() {
        return mInitialized;
    }

    @Override
    public boolean isReady() {
        return isReady;
    }

    private boolean whenPlayWithControlBar = false;

    public void setWhenPlayWithControlBar(boolean whenPlayWithControlBar) {
        this.whenPlayWithControlBar = whenPlayWithControlBar;
    }

    @Override
    public boolean whenPlayWithControlBar() {
        return whenPlayWithControlBar;
    }
}