package kr.altimedia.launcher.jasmin.dm.contents.obj;

/**
 * Created by mc.kim on 04,08,2020
 */
public enum OpportunityType {
    PreRoll("Pre-Roll"),
    MidRoll("Mid-Roll"),
    PostRoll("Post-Roll"),
    PlayerRoll("Player-Event"),
    TimerRoll("Timer-Event"),
    Invalid("Invalid");
    private final String name;

    OpportunityType(String name) {
        this.name = name;
    }

    public static OpportunityType findByName(String name) {
        OpportunityType[] types = values();
        for (OpportunityType type : types) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return Invalid;
    }
}
