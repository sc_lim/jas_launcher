/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.content.Context;
import android.graphics.Typeface;

import androidx.core.content.res.ResourcesCompat;

import java.util.HashMap;

import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 30,03,2020
 */
public class FontUtil {

    private final static FontUtil instance = new FontUtil();
    private HashMap<Font, Typeface> fontMap = new HashMap<>();

    public static FontUtil getInstance() {
        return instance;
    }

    public void load(Context context) {
        fontMap.put(Font.light, ResourcesCompat.getFont(context, R.font.font_prompt_light));
        fontMap.put(Font.medium, ResourcesCompat.getFont(context, R.font.font_prompt_medium));
        fontMap.put(Font.semiBold, ResourcesCompat.getFont(context, R.font.font_prompt_semibold));
        fontMap.put(Font.regular, ResourcesCompat.getFont(context,R.font.font_prompt_regular));
    }

    public Typeface getTypeface(Font font){
        return fontMap.get(font);
    }

    public enum Font {
        light, medium,  semiBold,regular
    }
}
