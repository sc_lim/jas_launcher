/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;

import kr.altimedia.launcher.jasmin.ui.view.browse.listener.TransitionListener;
import kr.altimedia.launcher.jasmin.ui.view.util.FragmentUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.StateMachine;
import kr.altimedia.launcher.jasmin.ui.view.util.TransitionHelper;


public class MenuBaseFragment extends MenuBrandedFragment {
    final StateMachine.State STATE_START = new StateMachine.State("START", true, false);
    final StateMachine.State STATE_ENTRANCE_INIT = new StateMachine.State("ENTRANCE_INIT");
    final StateMachine.State STATE_ENTRANCE_ON_PREPARED = new StateMachine.State("ENTRANCE_ON_PREPARED", true, false) {
        public void run() {
            MenuBaseFragment.this.mProgressBarManager.show();
        }
    };
    final StateMachine.State STATE_ENTRANCE_ON_PREPARED_ON_CREATEVIEW = new StateMachine.State("ENTRANCE_ON_PREPARED_ON_CREATEVIEW") {
        public void run() {
            MenuBaseFragment.this.onEntranceTransitionPrepare();
        }
    };
    final StateMachine.State STATE_ENTRANCE_PERFORM = new StateMachine.State("STATE_ENTRANCE_PERFORM") {
        public void run() {
            MenuBaseFragment.this.mProgressBarManager.hide();
            MenuBaseFragment.this.onExecuteEntranceTransition();
        }
    };
    final StateMachine.State STATE_ENTRANCE_ON_ENDED = new StateMachine.State("ENTRANCE_ON_ENDED") {
        public void run() {
            MenuBaseFragment.this.onEntranceTransitionEnd();
        }
    };
    final StateMachine.State STATE_ENTRANCE_COMPLETE = new StateMachine.State("ENTRANCE_COMPLETE", true, false);
    final StateMachine.Event EVT_ON_CREATE = new StateMachine.Event("onCreate");
    final StateMachine.Event EVT_ON_CREATEVIEW = new StateMachine.Event("onCreateView");
    final StateMachine.Event EVT_PREPARE_ENTRANCE = new StateMachine.Event("prepareEntranceTransition");
    final StateMachine.Event EVT_START_ENTRANCE = new StateMachine.Event("startEntranceTransition");
    final StateMachine.Event EVT_ENTRANCE_END = new StateMachine.Event("onEntranceTransitionEnd");
    final StateMachine mStateMachine = new StateMachine();
    Object mEntranceTransition;
    final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    @SuppressLint({"ValidFragment"})
    MenuBaseFragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        this.createStateMachineStates();
        this.createStateMachineTransitions();
        this.mStateMachine.start();
        super.onCreate(savedInstanceState);
        this.mStateMachine.fireEvent(this.EVT_ON_CREATE);
    }

    void createStateMachineStates() {
        this.mStateMachine.addState(this.STATE_START);
        this.mStateMachine.addState(this.STATE_ENTRANCE_INIT);
        this.mStateMachine.addState(this.STATE_ENTRANCE_ON_PREPARED);
        this.mStateMachine.addState(this.STATE_ENTRANCE_ON_PREPARED_ON_CREATEVIEW);
        this.mStateMachine.addState(this.STATE_ENTRANCE_PERFORM);
        this.mStateMachine.addState(this.STATE_ENTRANCE_ON_ENDED);
        this.mStateMachine.addState(this.STATE_ENTRANCE_COMPLETE);
    }

    void createStateMachineTransitions() {
        this.mStateMachine.addTransition(this.STATE_START, this.STATE_ENTRANCE_INIT, this.EVT_ON_CREATE);
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_INIT, this.STATE_ENTRANCE_COMPLETE, this.EVT_ON_CREATEVIEW);
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_INIT, this.STATE_ENTRANCE_ON_PREPARED, this.EVT_PREPARE_ENTRANCE);
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_ON_PREPARED, this.STATE_ENTRANCE_ON_PREPARED_ON_CREATEVIEW, this.EVT_ON_CREATEVIEW);
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_ON_PREPARED, this.STATE_ENTRANCE_PERFORM, this.EVT_START_ENTRANCE);
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_ON_PREPARED_ON_CREATEVIEW, this.STATE_ENTRANCE_PERFORM);
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_PERFORM, this.STATE_ENTRANCE_ON_ENDED, this.EVT_ENTRANCE_END);
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_ON_ENDED, this.STATE_ENTRANCE_COMPLETE);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mStateMachine.fireEvent(this.EVT_ON_CREATEVIEW);
    }

    public void prepareEntranceTransition() {
        this.mStateMachine.fireEvent(this.EVT_PREPARE_ENTRANCE);
    }

    protected Object createEntranceTransition() {
        return null;
    }

    protected void runEntranceTransition(Object entranceTransition) {
    }

    protected void onEntranceTransitionPrepare() {
    }

    protected void onEntranceTransitionStart() {
    }

    protected void onEntranceTransitionEnd() {
    }

    public void startEntranceTransition() {
        this.mStateMachine.fireEvent(this.EVT_START_ENTRANCE);
    }

    void onExecuteEntranceTransition() {
        final View view = this.getView();
        if (view != null) {
            view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    view.getViewTreeObserver().removeOnPreDrawListener(this);
                    if (FragmentUtil.getContext(MenuBaseFragment.this) != null && MenuBaseFragment.this.getView() != null) {
                        MenuBaseFragment.this.internalCreateEntranceTransition();
                        MenuBaseFragment.this.onEntranceTransitionStart();
                        if (MenuBaseFragment.this.mEntranceTransition != null) {
                            MenuBaseFragment.this.runEntranceTransition(MenuBaseFragment.this.mEntranceTransition);
                        } else {
                            MenuBaseFragment.this.mStateMachine.fireEvent(MenuBaseFragment.this.EVT_ENTRANCE_END);
                        }

                        return false;
                    } else {
                        return true;
                    }
                }
            });
            view.invalidate();
        }
    }

    void internalCreateEntranceTransition() {
        this.mEntranceTransition = this.createEntranceTransition();
        if (this.mEntranceTransition != null) {
            TransitionHelper.addTransitionListener(this.mEntranceTransition, new TransitionListener() {
                public void onTransitionEnd(Object transition) {
                    MenuBaseFragment.this.mEntranceTransition = null;
                    MenuBaseFragment.this.mStateMachine.fireEvent(MenuBaseFragment.this.EVT_ENTRANCE_END);
                }
            });
        }
    }

    public final ProgressBarManager getProgressBarManager() {
        return this.mProgressBarManager;
    }
}
