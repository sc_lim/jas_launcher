/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;

import android.view.KeyEvent;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import kr.altimedia.launcher.jasmin.dm.category.obj.Category;

/**
 * Created by mc.kim on 04,02,2020
 */
public class MenuRow extends Row {
    private final Category mItemMenu;
    private final OnMenuSelectedListener mOnMenuSelectedListener;
    private final RecyclerView recyclerView;
    private boolean isSelected = false;
    public MenuRow(RecyclerView recyclerView, Category menu, OnMenuSelectedListener onMenuSelectedListener) {
        this.mItemMenu = menu;
        this.recyclerView = recyclerView;
        this.mOnMenuSelectedListener = onMenuSelectedListener;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Category getItem() {
        return mItemMenu;
    }

    public OnMenuSelectedListener getOnMenuSelectedListener() {
        return mOnMenuSelectedListener;
    }

    public interface OnMenuSelectedListener {
        void onItemSelected(View view, Category menu);

        void onCanceled(View view, Category menu);

        boolean dispatchKey(KeyEvent keyEvent);
    }
}
