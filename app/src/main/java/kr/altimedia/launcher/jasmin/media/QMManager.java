package kr.altimedia.launcher.jasmin.media;

import com.altimedia.player.AltiPlayer;
import com.altimedia.player.QMEventListener;
import com.altimedia.util.Log;

public class QMManager extends AbstractQMManager implements QMEventListener {

    private int videoBitrate = 0;
    private int videoWidth = 0;
    private int videoHeight = 0;
    private int audioBitrate = 0;
    private String qualityToString() {
        return "Video bitrate:" + videoBitrate + ", size:" + videoWidth + "x" + videoHeight + ", Audio bitrate:" + audioBitrate;
    }

    public QMManager() {
        super();
    }

    public void destroy() {
        super.destroy();
    }

    public void onInitialDelay(long initTime) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onInitialDelay() id:" + id + ", initTime:" + initTime);
        }
    }

    public void onBufferUnderruns(long playerBufferingAmount, long playerBufferThreshold) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBufferUnderruns() id:" + id + ", playerBufferingAmount:" + playerBufferingAmount + ", playerBufferThreshold:" + playerBufferThreshold);
        }
    }

    private void checkAndReportQuality(int bitrate, int width, int height) {
        if (Log.INCLUDE) {
            Log.d(TAG, "checkAndReportQuality() id:" + id + ", video bitrate:" + bitrate + ", size:" + width + "x" + height+", currentQuality:"+qualityToString());
        }
        int prevVideoBitrate = videoBitrate;
        videoBitrate = bitrate;
        videoWidth = width;
        videoHeight = height;

        if (prevVideoBitrate > bitrate) {
            //quality가 낮아진 경우에 insertMediaSwitchMsg를 호출
            if (Log.INCLUDE) {
                Log.d(TAG, "checkAndReportQuality() report quality:"+qualityToString());
            }
            insertMediaSwitchMsg(videoWidth, videoHeight, videoBitrate, audioBitrate);
        }
    }

    private void checkAndReportQuality(int bitrate) {
        if (Log.INCLUDE) {
            Log.d(TAG, "checkAndReportQuality() id:" + id + ", audio bitrate:" + bitrate+", currentQuality:"+qualityToString());
        }
        int prevAudioBitrate = audioBitrate;
        audioBitrate = bitrate;

        if (prevAudioBitrate > bitrate) {
            //quality가 낮아진 경우에 insertMediaSwitchMsg를 호출
            if (Log.INCLUDE) {
                Log.d(TAG, "checkAndReportQuality() report quality:"+qualityToString());
            }
            insertMediaSwitchMsg(videoWidth, videoHeight, videoBitrate, audioBitrate);
        }
    }

    public void onQualitySwitchVideo(int bitrate, int width, int height) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onQualitySwitchVideo() id:" + id + ", video bitrate:" + bitrate + ", size:" + width + "x" + height+", currentQuality:"+qualityToString());
        }
        checkAndReportQuality(bitrate, width, height);
    }

    public void onQualitySwitchAudio(int bitrate) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onQualitySwitchAudio() id:" + id + ", audio bitrate:" + bitrate+", currentQuality:"+qualityToString());
        }
        checkAndReportQuality(bitrate);
    }

    public void onMediaThroughputVideo(long size, long duration) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onMediaThroughputVideo() id:" + id + ", size:" + size + ", duration:" + duration + " (video rate:" + (size / duration) + ")");
        }
        insertMediaReceiveMsg(duration, size, 0, 0);
    }

    public void onMediaThroughputAudio(long size, long duration) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onMediaThroughputAudio() id:" + id + ", size:" + size + ", duration:" + duration + " (audio rate:" + (size / duration) + ")");
        }
        insertMediaReceiveMsg(0, 0, duration, size);
    }

    public void onVideoTracksChanged(int bitrate, int width, int height) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onVideoTracksChanged() id:" + id + ", video bitrate:" + bitrate + ", size:" + width + "x" + height);
        }
        checkAndReportQuality(bitrate, width, height);
    }

    public void onAudioTracksChanged(int bitrate) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onAudioTracksChanged() id:" + id + ", audio bitrate:" + bitrate);
        }
        checkAndReportQuality(bitrate);
    }

    public void onSeek() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSeek() id:" + id);
        }
        onSeekState = true;
    }

    public void onSeekComplete() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSeekComplete() id:" + id);
        }
        onSeekState = false;
    }

    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPlayerStateChanged() id:" + id + ", playWhenReady:" + playWhenReady + ", playbackState:" + playbackState + ", str:" + AltiPlayer.EventListener.stateToString(playbackState));
        }
        switch (playbackState) {
            case AltiPlayer.STATE_BUFFERING:
                if (channelState == CHANNEL_CLOSE) {
                    changeChannelState(CHANNEL_OPEN);
                }
                changeBufferingState(BUFFERING_START);
                break;
            case AltiPlayer.STATE_READY:
                if (playWhenReady) {
                    if (channelState == CHANNEL_OPEN) {
                        changeChannelState(CHANNEL_1ST_FRAME);
                    }
                }
                changeBufferingState(BUFFERING_END);
                break;
            case AltiPlayer.STATE_IDLE:
            case AltiPlayer.STATE_ENDED:
                changeBufferingState(BUFFERING_END);
                changeChannelState(CHANNEL_CLOSE);
                break;
        }
    }
}
