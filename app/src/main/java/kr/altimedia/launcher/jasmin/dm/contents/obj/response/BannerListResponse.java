/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;

/**
 * Created by mc.kim on 08,05,2020
 */
public class BannerListResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private BannerListResult result;

    protected BannerListResponse(Parcel in) {
        super(in);
        result = in.readTypedObject(BannerListResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(result, flags);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BannerListResponse> CREATOR = new Creator<BannerListResponse>() {
        @Override
        public BannerListResponse createFromParcel(Parcel in) {
            return new BannerListResponse(in);
        }

        @Override
        public BannerListResponse[] newArray(int size) {
            return new BannerListResponse[size];
        }
    };

    public BannerListResult getResult() {
        return result;
    }

    public List<Banner> getBannerList() {
        if (result.totalPageNo == 0) {
            return new ArrayList<>();
        }
        return result.bannerList;
    }


    private static class BannerListResult implements Parcelable {

        @Expose
        @SerializedName("banner")
        private List<Banner> bannerList;
        @Expose
        @SerializedName("totalPageNo")
        private int totalPageNo;

        protected BannerListResult(Parcel in) {
            bannerList = in.createTypedArrayList(Banner.CREATOR);
            totalPageNo = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(bannerList);
            dest.writeInt(totalPageNo);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<BannerListResult> CREATOR = new Creator<BannerListResult>() {
            @Override
            public BannerListResult createFromParcel(Parcel in) {
                return new BannerListResult(in);
            }

            @Override
            public BannerListResult[] newArray(int size) {
                return new BannerListResult[size];
            }
        };

        @Override
        public String toString() {
            return "BannerListResult{" +
                    "bannerID=" + bannerList +
                    '}';
        }

        public int getTotalPageNo() {
            return totalPageNo;
        }
    }

}
