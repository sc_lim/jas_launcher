/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public abstract class CouponPaymentBaseFragment extends Fragment {
    public static final String CLASS_NAME = CouponPaymentBaseFragment.class.getName();
    private final String TAG = CouponPaymentBaseFragment.class.getSimpleName();

    protected final long TOTAL_COUNT_DOWN = 5 * TimeUtil.MIN;
    protected final long INTERVAL = TimeUtil.SEC;
    protected final long CHECK_PAYMENT_INTERVAL = 5;

    protected MbsDataTask requestTask;
    protected MbsDataTask resultTask;
    protected String purchaseId = null;

    protected int layoutResourceId = -1;

    private OnPaymentListener onPaymentListener;

    public void setOnPaymentListener(OnPaymentListener onPaymentListener) {
        this.onPaymentListener = onPaymentListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutResourceId = initLayoutResourceId();
        return inflater.inflate(layoutResourceId, container, false);
    }

    protected abstract int initLayoutResourceId();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    protected void initView(View view) {
    }

    public OnPaymentListener getOnPaymentListener() {
        return onPaymentListener;
    }

    protected void showPaymentCancelDialog(PurchaseInfo purchaseInfo, String purchaseId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showPaymentCancelDialog, purchaseId : " + purchaseId);
        }

        SafeDismissDialogFragment dialogFragment = (SafeDismissDialogFragment) getParentFragment();
        TvOverlayManager tvOverlayManager = dialogFragment.getTvOverlayManager();

        CouponPaymentCancelDialog couponPaymentCancelDialog = CouponPaymentCancelDialog.newInstance(purchaseInfo);
        couponPaymentCancelDialog.setOnClickButtonListener(new CouponPaymentCancelDialog.OnClickButtonListener() {
            @Override
            public void onClickCancelPayment() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onClickCancelPayment");
                }

                cancelPayment(purchaseId);
                couponPaymentCancelDialog.dismiss();
            }

            @Override
            public void onclickDoNotCancelPayment() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onclickDoNotCancelPayment");
                }

                couponPaymentCancelDialog.dismiss();
            }
        });

        tvOverlayManager.showDialogFragment(couponPaymentCancelDialog);
    }

    private void cancelPayment(String cancelPurchaseId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "cancelPayment");
        }

        if (onPaymentListener != null) {
            onPaymentListener.onPaymentCancel(cancelPurchaseId);
        }
    }

    @Override
    public void onDestroyView() {
        if (requestTask != null) {
            requestTask.cancel(true);
        }

        if (resultTask != null) {
            resultTask.cancel(true);
        }
        onPaymentListener = null;

        super.onDestroyView();
    }

    public interface OnPaymentListener {
        void onPaymentComplete(boolean isSuccess);
        void onPaymentError(String errorCode);
        void onPaymentCancel(String cancelPurchaseId);
        void onFragmentDismiss();
        void gotoHistoryListBySuccess();
    }
}
