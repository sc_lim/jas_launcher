/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.system.settings.data;

import android.content.Context;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.common.SpinnerTextView;

import static kr.altimedia.launcher.jasmin.system.settings.SettingControl.SUBTITLE_FONT_LARGE;
import static kr.altimedia.launcher.jasmin.system.settings.SettingControl.SUBTITLE_FONT_SMALL;
import static kr.altimedia.launcher.jasmin.system.settings.SettingControl.SUBTITLE_FONT_STANDARD;

public enum SubtitleFontSize implements SpinnerTextView.SpinnerOptionType {
    SMALL(0, R.string.subtitle_font_small, SUBTITLE_FONT_SMALL),
    MEDIUM(1, R.string.subtitle_font_medium, SUBTITLE_FONT_STANDARD),
    LARGE(2, R.string.subtitle_font_large, SUBTITLE_FONT_LARGE);

    private int type;
    private int title;
    private String key;

    SubtitleFontSize(int type, int title, String key) {
        this.type = type;
        this.title = title;
        this.key = key;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public int getTitle() {
        return title;
    }

    @Override
    public String getKey() {
        return key;
    }

    public static SubtitleFontSize findByName(Context context, String title) {
        SubtitleFontSize[] types = values();

        title = title.toLowerCase();
        for (SubtitleFontSize fontSize : types) {
            String option = context.getResources().getString(fontSize.title).toLowerCase();
            if (option.equalsIgnoreCase(title)) {
                return fontSize;
            }
        }

        return MEDIUM;
    }
}
