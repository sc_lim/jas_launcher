/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 20,06,2020
 */
public class PreviewInfo implements Parcelable {
    public static final Creator<PreviewInfo> CREATOR = new Creator<PreviewInfo>() {
        @Override
        public PreviewInfo createFromParcel(Parcel in) {
            return new PreviewInfo(in);
        }

        @Override
        public PreviewInfo[] newArray(int size) {
            return new PreviewInfo[size];
        }
    };
    @Expose
    @SerializedName("freePreviewStartPosition")
    private long freePreviewStartPosition;
    @Expose
    @SerializedName("freePreviewDuration")
    private long freePreviewDuration;

    protected PreviewInfo(Parcel in) {
        freePreviewStartPosition = in.readLong();
        freePreviewDuration = in.readLong();
    }

    public PreviewInfo(long freePreviewStartPosition, long freePreviewDuration) {
        this.freePreviewStartPosition = freePreviewStartPosition;
        this.freePreviewDuration = freePreviewDuration;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(freePreviewStartPosition);
        dest.writeLong(freePreviewDuration);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public long getFreePreviewStartPosition() {
        return freePreviewStartPosition;
    }

    public long getFreePreviewDuration() {
        return freePreviewDuration;
    }
}
