/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.annotation.NonNull;

public class ProfileList implements Parcelable {
    @Expose
    @SerializedName("profileList")
    private List<ProfileInfo> profileList;
    @Expose
    @SerializedName("maxProfileCnt")
    private int maxProfileCount;


    public static final Creator<ProfileList> CREATOR = new Creator<ProfileList>() {
        @Override
        public ProfileList createFromParcel(Parcel in) {
            return new ProfileList(in);
        }
        @Override
        public ProfileList[] newArray(int size) {
            return new ProfileList[size];
        }
    };

    protected ProfileList(Parcel in) {
        in.readList(profileList, ProfileInfo.class.getClassLoader());
        maxProfileCount = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(profileList);
        dest.writeInt(maxProfileCount);
    }

    public List<ProfileInfo> getProfileInfoList() {
        return profileList;
    }

    public int getMaxProfileCount(){
        return maxProfileCount;
    }

    @NonNull
    @Override
    public String toString() {
        return "ProfileList{" +
                "profileList='" + profileList + '\'' +
                ", maxProfileCount='" + maxProfileCount + '\'' +
                '}';
    }
}
