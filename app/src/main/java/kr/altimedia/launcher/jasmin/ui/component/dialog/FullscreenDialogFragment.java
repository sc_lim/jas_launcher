/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;


/**
 * Dialog fragment with full screen.
 */
public class FullscreenDialogFragment extends SafeDismissDialogFragment {
    public static final String DIALOG_TAG = FullscreenDialogFragment.class.getName();
    public static final String VIEW_LAYOUT_ID = "viewLayoutId";

    /**
     * Creates a FullscreenDialogFragment. View class of viewLayoutResId should implement {@link
     * DialogView}.
     */
    public static FullscreenDialogFragment newInstance(int viewLayoutResId, String trackerLabel) {
        FullscreenDialogFragment f = new FullscreenDialogFragment();
        Bundle args = new Bundle();
        args.putInt(VIEW_LAYOUT_ID, viewLayoutResId);
        f.setArguments(args);
        return f;
    }

    private DialogView mDialogView;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        FullscreenDialog dialog =
                new FullscreenDialog(getActivity(), R.style.Theme_TV_dialog_Fullscreen);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        Bundle args = getArguments();
        int viewLayoutResId = args.getInt(VIEW_LAYOUT_ID);
        View v = inflater.inflate(viewLayoutResId, null);
        dialog.setContentView(v);
        mDialogView = (DialogView) v;
        mDialogView.initialize((LauncherActivity) getActivity(), dialog);
        return dialog;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DCA;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDialogView.onDestroy();
    }


    private class FullscreenDialog extends Dialog {
        public FullscreenDialog(Context context, int theme) {
            super(context, theme);
        }

        @Override
        public void setContentView(View dialogView) {
            super.setContentView(dialogView);
            mDialogView = (DialogView) dialogView;
        }

        @Override
        public boolean dispatchKeyEvent(KeyEvent event) {
            boolean handled = super.dispatchKeyEvent(event);
            return handled || ((View) mDialogView).dispatchKeyEvent(event);
        }

        @Override
        public void onBackPressed() {
            mDialogView.onBackPressed();
        }
    }

    /**
     * Interface for the view of {@link FullscreenDialogFragment}.
     */
    public interface DialogView {
        /**
         * Called after the view is inflated and attached to the dialog.
         */
        void initialize(LauncherActivity activity, Dialog dialog);

        /**
         * Called when a back key is pressed.
         */
        void onBackPressed();

        /**
         * Called when {@link DialogFragment#onDestroy} is called.
         */
        void onDestroy();
    }
}
