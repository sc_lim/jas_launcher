/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.def.CouponType;
import kr.altimedia.launcher.jasmin.dm.def.CouponTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.NormalDateTimeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.NormalDateTimeDeserializer2;

public class Coupon implements Parcelable {
    public static final Creator<Coupon> CREATOR = new Creator<Coupon>() {
        @Override
        public Coupon createFromParcel(Parcel in) {
            return new Coupon(in);
        }

        @Override
        public Coupon[] newArray(int size) {
            return new Coupon[size];
        }
    };

    @Expose
    @SerializedName("cpnId")
    protected String id; // 쿠폰 ID
    @Expose
    @SerializedName("cpnNm")
    protected String name; // 쿠폰명
    @Expose
    @SerializedName("cpnRegDt")
    @JsonAdapter(NormalDateTimeDeserializer.class)
    protected Date registerDate; // 쿠폰등록일
    @Expose
    @SerializedName("cpnExpDate")
    @JsonAdapter(NormalDateTimeDeserializer2.class)
    protected Date expireDate; // 유효종료일
    @Expose
    @SerializedName("cpnSttus")
    @JsonAdapter(CouponStatusDeserializer.class)
    protected CouponStatus couponStatus; // 쿠폰상태
    @Expose
    @SerializedName("cpnType")
    @JsonAdapter(CouponTypeDeserializer.class)
    protected CouponType couponType; // 쿠폰종류
    @Expose
    @SerializedName("expiringYn")
    protected boolean expiringYn; // 만료예정여부

    protected int bgImage;

    protected Coupon() {
    }

    protected Coupon(Parcel in) {
        id = in.readString();
        name = in.readString();
        registerDate = (Date) in.readValue(Date.class.getClassLoader());
        expireDate = (Date) in.readValue(Date.class.getClassLoader());
        couponStatus = (CouponStatus) in.readValue(CouponStatus.class.getClassLoader());
        couponType = (CouponType) in.readValue(CouponType.class.getClassLoader());
//        expiringYn = (boolean) in.readValue(Boolean.class.getClassLoader());
        expiringYn = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeValue(registerDate);
        dest.writeValue(expireDate);
        dest.writeValue(couponStatus);
        dest.writeValue(couponType);
//        dest.writeValue(expiringYn);
        dest.writeByte((byte) (expiringYn ? 1 : 0));
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public CouponStatus getStatus() {
        return couponStatus;
    }

    public CouponType getType() {
        return couponType;
    }

    public boolean isExpiringYn() {
        return expiringYn;
    }

    public int getBgImage() {
        return bgImage;
    }

    public void setBgImage(int rscId) {
        bgImage = rscId;
    }

    @NonNull
    @Override
    public String toString() {
        return "Coupon{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                "registerDate='" + registerDate + '\'' +
                "expireDate='" + expireDate + '\'' +
                "couponStatus='" + couponStatus + '\'' +
                "couponType='" + couponType + '\'' +
                "expiringYn='" + expiringYn + '\'' +
                "}";
    }
}
