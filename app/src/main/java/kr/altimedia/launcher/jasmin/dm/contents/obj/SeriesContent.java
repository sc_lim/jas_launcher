/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.contents.ContentStreamDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

/**
 * Created by mc.kim on 08,05,2020
 */
public class SeriesContent implements Parcelable {
    public static final Rect mPosterDefaultRect = new Rect(0, 0, 320, 180);
    @Expose
    @SerializedName("containerType")
    private String containerType;
    @Expose
    @SerializedName("episodeId")
    private String episodeId;
    @Expose
    @SerializedName("posterSourceUrl")
    private String posterSourceUrl;
    @Expose
    @SerializedName("resumeTime")
    private long resumeTime;
    @Expose
    @SerializedName("seriesAssetId")
    private String seriesAssetId;
    @Expose
    @SerializedName("movieAssetId")
    private String movieAssetId;
    @Expose
    @SerializedName("contentGroupId")
    private String contentGroupId;
    @Expose
    @SerializedName("seriesName")
    private String seriesName;
    @Expose
    @SerializedName("streamInfo")
    @JsonAdapter(ContentStreamDeserializer.class)
    private List<StreamInfo> trailer;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("nextSeriesAssetId")
    @Expose
    private String nextSeriesAssetId;
    @SerializedName("isPurchased")
    @Expose
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isWatchable;

    protected SeriesContent(Parcel in) {
        containerType = in.readString();
        episodeId = in.readString();
        posterSourceUrl = in.readString();
        resumeTime = in.readLong();
        seriesAssetId = in.readString();
        movieAssetId = in.readString();
        contentGroupId = in.readString();
        seriesName = in.readString();
        trailer = in.createTypedArrayList(StreamInfo.CREATOR);
        categoryId = in.readString();
        nextSeriesAssetId = in.readString();
        this.isWatchable = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(containerType);
        dest.writeString(episodeId);
        dest.writeString(posterSourceUrl);
        dest.writeLong(resumeTime);
        dest.writeString(seriesAssetId);
        dest.writeString(movieAssetId);
        dest.writeString(contentGroupId);
        dest.writeString(seriesName);
        dest.writeTypedList(trailer);
        dest.writeString(categoryId);
        dest.writeString(nextSeriesAssetId);
        dest.writeByte((byte) (isWatchable ? 1 : 0));
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SeriesContent> CREATOR = new Creator<SeriesContent>() {
        @Override
        public SeriesContent createFromParcel(Parcel in) {
            return new SeriesContent(in);
        }

        @Override
        public SeriesContent[] newArray(int size) {
            return new SeriesContent[size];
        }
    };

    @Override
    public String toString() {
        return "SeriesContent{" +
                "containerType='" + containerType + '\'' +
                ", episodeId='" + episodeId + '\'' +
                ", posterSourceUrl='" + posterSourceUrl + '\'' +
                ", resumeTime=" + resumeTime +
                ", seriesAssetId='" + seriesAssetId + '\'' +
                ", movieAssetId='" + movieAssetId + '\'' +
                ", contentGroupId='" + contentGroupId + '\'' +
                ", seriesName='" + seriesName + '\'' +
                ", trailer=" + trailer +
                ", categoryId='" + categoryId + '\'' +
                ", nextSeriesAssetId='" + nextSeriesAssetId + '\'' +
                ", isWatchable=" + isWatchable +
                '}';
    }

    public String getEpisodeId() {
        return episodeId;
    }


    public String getPosterSourceUrl(int width, int height) {
        return posterSourceUrl + "_" + width + "x" + height + "." + containerType;
    }

    public String getContentGroupId() {
        return contentGroupId;
    }

    public long getResumeTime() {
        return resumeTime;
    }

    public String getSeriesAssetId() {
        return seriesAssetId;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public List<StreamInfo> getTrailer() {
        List<StreamInfo> infoList = new ArrayList<>();
        for (StreamInfo streamInfo : trailer) {
            if (streamInfo.getStreamType() != StreamType.Main) {
                infoList.add(streamInfo);
            }
        }

        return infoList;
    }

    public String getMovieAssetId() {
        return movieAssetId;
    }

    public String getContainerType() {
        return containerType;
    }

    public String getPosterSourceUrl() {
        return posterSourceUrl;
    }

    public String getNextSeriesAssetId() {
        return nextSeriesAssetId;
    }

    public boolean isWatchable() {
        return isWatchable;
    }

    public void setWatchable(boolean isWatchable) {
        this.isWatchable = isWatchable;
    }
}
