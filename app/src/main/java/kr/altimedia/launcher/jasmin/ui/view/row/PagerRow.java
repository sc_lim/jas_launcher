/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;

import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class PagerRow extends Row {
    private final ObjectAdapter mAdapter;
    private CharSequence mContentDescription;

    /**
     * Returns the {@link ObjectAdapter} that represents a list of objects.
     */
    public final ObjectAdapter getAdapter() {
        return mAdapter;
    }

    public PagerRow(HeaderItem header, ObjectAdapter adapter) {
        super(header);
        mAdapter = adapter;
        verify();
    }

    public PagerRow(long id, HeaderItem header, ObjectAdapter adapter) {
        super(id, header);
        mAdapter = adapter;
        verify();
    }

    public PagerRow(ObjectAdapter adapter) {
        super();
        mAdapter = adapter;
        verify();
    }

    public PagerRow(int id, ObjectAdapter adapter) {
        super();
        setId(id);
        mAdapter = adapter;
        verify();
    }

    private void verify() {
        if (mAdapter == null) {
            throw new IllegalArgumentException("ObjectAdapter cannot be null");
        }
    }

    public CharSequence getContentDescription() {
        if (mContentDescription != null) {
            return mContentDescription;
        }
        final HeaderItem headerItem = getHeaderItem();
        if (headerItem != null) {
            CharSequence contentDescription = headerItem.getContentDescription();
            return contentDescription;
//            return headerItem.getName();
        }
        return null;
    }

    public void setContentDescription(CharSequence contentDescription) {
        mContentDescription = contentDescription;
    }

}
