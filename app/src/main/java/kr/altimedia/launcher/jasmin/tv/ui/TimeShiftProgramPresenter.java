/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.tvmodule.dao.Program;
import com.altimedia.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;

/**
 * Created by mc.kim on 15,07,2020
 */
class TimeShiftProgramPresenter extends Presenter {
    private final String TAG = TimeShiftProgramPresenter.class.getSimpleName();
    public TimeShiftProgramPresenter() {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_side_timeshift_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (!(item instanceof TimeShiftProgramRow)) {
            return;
        }
        TimeShiftProgramRow mTimeShiftProgramRow = (TimeShiftProgramRow) item;


        viewHolder.view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                v.setSelected(hasFocus);
            }
        });


        viewHolder.view.setOnKeyListener(mTimeShiftProgramRow);
        viewHolder.view.setOnClickListener(mTimeShiftProgramRow);
        Program program = mTimeShiftProgramRow.getProgram();
        long startTime = program.getStartTimeUtcMillis();
        long endTime = program.getEndTimeUtcMillis();

        TextView programTitle = viewHolder.view.findViewById(R.id.programTitle);
        TextView programStartDate = viewHolder.view.findViewById(R.id.programStartDate);
        TextView programTime = viewHolder.view.findViewById(R.id.programTime);
        ImageView programIcon = viewHolder.view.findViewById(R.id.programIcon);
        ImageView lockIcon = viewHolder.view.findViewById(R.id.lockIcon);

        lockIcon.setVisibility(mTimeShiftProgramRow.getInterface().isLocked(program) ? View.VISIBLE : View.GONE);
        long currentTimeMs = JasmineEpgApplication.SystemClock().currentTimeMillis();
        if (mTimeShiftProgramRow.getInterface().isPlayingProgram(program)) {
            programIcon.setImageResource(R.drawable.icon_playing);
        } else if (startTime <= currentTimeMs && currentTimeMs <= endTime) {
            programIcon.setImageResource(R.drawable.icon_live);
        } else {
            programIcon.setImageDrawable(null);
        }


        programTitle.setText(program.getTitle());

        programStartDate.setText(getDateTime(startTime, viewHolder.view.getResources().getString(R.string.EEE_dd_MMM)));

        String programTimeString = getDateTime(startTime, "HH:mm") + " - " + getDateTime(endTime, "HH:mm");
        programTime.setText(programTimeString);
    }

    private String getDateTime(long time, String format) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        String formattedString = simpleDateFormat.format(new Date(time));
        if (Log.INCLUDE) {
            Log.d(TAG, "getDateTime : " + time + ", format : " + format + ", formattedString : " + formattedString);
        }
        return formattedString;
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        TextView programTitle = viewHolder.view.findViewById(R.id.programTitle);
        TextView programStartDate = viewHolder.view.findViewById(R.id.programStartDate);
        TextView programTime = viewHolder.view.findViewById(R.id.programTime);
        ImageView programIcon = viewHolder.view.findViewById(R.id.programIcon);

        programTitle.setText(null);
        programStartDate.setText(null);
        programTime.setText(null);
        programIcon.setImageDrawable(null);

        viewHolder.view.setOnFocusChangeListener(null);
    }
}