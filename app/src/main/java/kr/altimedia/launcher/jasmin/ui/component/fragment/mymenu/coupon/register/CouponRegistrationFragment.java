/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.register;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.coupon.CouponDataManager;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountType;
import kr.altimedia.launcher.jasmin.dm.coupon.object.MyCoupon;
import kr.altimedia.launcher.jasmin.dm.def.CouponType;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.LimitTextView;

public class CouponRegistrationFragment extends Fragment implements View.OnKeyListener, View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = CouponRegistrationFragment.class.getName();
    public final String TAG = CouponRegistrationFragment.class.getSimpleName();

    private final String CPN_ERROR_EXPIRED_COUPON = "CPN_7008";
    private final String CPN_ERROR_WRONG_COUPON_NUMBER = "CPN_7030";
    private final String CPN_ERROR_REGISTERED_COUPON = "CPN_7031";
    private final String MBS_ERROR_EXPIRED_COUPON = "MBS_7008";
    private final String MBS_ERROR_WRONG_COUPON_NUMBER = "MBS_7030";
    private final String MBS_ERROR_REGISTERED_COUPON = "MBS_7031";

    private TextView description;
    private TextView btnRedeem;
    private TextView btnCancel;
    private View rootView;

    private ArrayList<LimitTextView> viewList = new ArrayList<>();

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            LimitTextView limitTextView = viewList.get(viewList.size() - 1);
            if (limitTextView.isFull()) {
                btnRedeem.setEnabled(true);
                btnRedeem.requestFocus();
            }
        }
    };

    private CouponRegistrationFragment() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        btnRedeem.setOnClickListener(null);
        btnCancel.setOnClickListener(null);
        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if(keyCode >= KeyEvent.KEYCODE_0 && keyCode <=KeyEvent.KEYCODE_9){
            if(btnRedeem.isFocused() || btnCancel.isFocused()) {
                return true;
            }
        }

        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    public static CouponRegistrationFragment newInstance() {
        CouponRegistrationFragment fragment = new CouponRegistrationFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_coupon_registration, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        initView(view);
    }

    private void initView(View view) {
        initTextView(view);
        initInputView(view);
    }

    private void initTextView(View view) {
        description = view.findViewById(R.id.description);

        btnRedeem = view.findViewById(R.id.btnRedeem);
        btnRedeem.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if(event.getAction() != KeyEvent.ACTION_DOWN) {
                    return true;
                }
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:{
                        ((CouponRegistrationDialog)getParentFragment()).dismiss();
                        return true;
                    }
                    case KeyEvent.KEYCODE_ENTER:
                    case KeyEvent.KEYCODE_DPAD_CENTER:{
                        checkValidNumber();
                        return true;
                    }
                    case KeyEvent.KEYCODE_DPAD_LEFT:
                        return true;
                    case KeyEvent.KEYCODE_DPAD_UP:{
                        requestLastFilledFocus();
                        return true;
                    }
                }
                return false;
            }
        });

        btnCancel = view.findViewById(R.id.btnCancel);
        btnCancel.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if(event.getAction() != KeyEvent.ACTION_DOWN) {
                    return true;
                }
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                    case KeyEvent.KEYCODE_ENTER:
                    case KeyEvent.KEYCODE_DPAD_CENTER:{
                        ((CouponRegistrationDialog)getParentFragment()).dismiss();
                        return true;
                    }
                    case KeyEvent.KEYCODE_DPAD_LEFT:
                        if(btnRedeem.isEnabled()){
                            btnRedeem.requestFocus();
                        }
                        return true;
                    case KeyEvent.KEYCODE_DPAD_RIGHT:
                        return true;
                    case KeyEvent.KEYCODE_DPAD_UP:{
                        requestLastFilledFocus();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void initInputView(View view) {
        LimitTextView input1 = view.findViewById(R.id.input_1);
        LimitTextView input2 = view.findViewById(R.id.input_2);
        LimitTextView input3 = view.findViewById(R.id.input_3);
        LimitTextView input4 = view.findViewById(R.id.input_4);

        input1.setLength(5);
        input2.setLength(3);
        input3.setLength(4);
        input4.setLength(3);

        viewList.add(input1);
        viewList.add(input2);
        viewList.add(input3);
        viewList.add(input4);

        input1.setOnKeyListener(this);
        input2.setOnKeyListener(this);
        input3.setOnKeyListener(this);
        input4.setOnKeyListener(this);
        input4.addTextChangedListener(textWatcher);

        resetInputView();
    }

    private void resetInputView(){
        for(LimitTextView view : viewList){
            view.clear();
            setHint(view);
        }
        btnRedeem.setEnabled(false);
        LimitTextView view = getLimitedTextView(0);
        view.requestFocus();
    }

    private void setHint(LimitTextView view) {
        String init = "";

        int length = view.getInputMaxLength();
        for (int i = 0; i < length; i++) {
            init += "0";
        }

        view.setHint(init);
    }

    private void checkValidNumber() {
        boolean isValid = true;
        String number = "";
        for (LimitTextView view : viewList) {
            number += view.getString();
            if (!view.isFull()) {
                isValid = false;
            }
        }

        if(Log.INCLUDE) {
            Log.d(TAG, "checkValidNumber, isValid : " + isValid + ", number : " + number);
        }
        if (isValid) {
            registerCoupon(number);

        } else {
            setErrorMessage(R.string.coupon_registration_wrong_desc);
            resetInputView();
        }
    }

    private void setErrorMessage(int id){
        description.setTextColor(getResources().getColor(R.color.color_FFA67C52, null));
        String text = getResources().getString(id);
        description.setText(text);
    }

    private LimitTextView getLimitedTextView(int index) {
        return viewList.get(index);
    }

    private void requestNextFocus(LimitTextView view) {
        int index = viewList.indexOf(view);
        if (index < (viewList.size() - 1)) {
            View nextView = getLimitedTextView(index + 1);
            nextView.requestFocus();
        }
    }

    private void requestPrevFocus(LimitTextView view) {
        try {
            int index = viewList.indexOf(view);
            if (index > 0) {
                LimitTextView prevView = getLimitedTextView(index - 1);
                prevView.requestFocus();
                prevView.delete(false);
            }
        }catch (Exception e){
        }
    }

    private void requestLastFilledFocus() {
        if(viewList != null && !viewList.isEmpty()) {
            LimitTextView view = viewList.get(0);
            for (int i = 0; i<viewList.size(); i++) {
                view = viewList.get(i);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onKey: ["+i+"] isEmpty=" + view.isEmpty() +", isFull="+view.isEmpty());
                }
                if (!view.isFull()) {
                    btnRedeem.setEnabled(false);
                    view.requestFocus();
                    return;
                }
            }
            view.requestFocus();
        }
    }

    private boolean nextViewIsFilled(LimitTextView view) {
        int index = viewList.indexOf(view);
        LimitTextView nextView = getLimitedTextView(index);

        return !nextView.isEmpty();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        LimitTextView limitTextView = (LimitTextView) v;
//        if (Log.INCLUDE) {
//            Log.d(TAG, "onKey: limitTextView=" + limitTextView +", keyCode="+keyCode);
//        }
        int newDigit = 0;
        if(keyCode >= KeyEvent.KEYCODE_0 && keyCode <=KeyEvent.KEYCODE_9){
            newDigit = getNumber(keyCode);

        }else {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    if(!limitTextView.isEmpty() && limitTextView.isFull()) {
                        int index = viewList.indexOf(limitTextView);
                        if (index == viewList.size() - 1) {
                            btnRedeem.requestFocus();
                            return true;
                        }
                    }
                    return false;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    try {
                        if (limitTextView.isEmpty()) {
                            requestPrevFocus(limitTextView);
                        }else{
                            limitTextView.delete(false);
                            btnRedeem.setEnabled(false);
                        }
                    }catch (Exception e){
                    }
                    return true;

                case KeyEvent.KEYCODE_DPAD_RIGHT:
//                    boolean isMoveRight = !nextViewIsFilled(limitTextView);
                    return true;
                default:
                    return false;
            }
        }

        //When you press the number keys after moving the focus to a field where all the digits are filled
        try {
            String currStr = limitTextView.getString();
            String newStr = currStr += newDigit;
//            if (Log.INCLUDE) {
//                Log.d(TAG, "onKey: newDigit="+newDigit+", isFull=" + limitTextView.isFull() +", currStr="+currStr+", newStr="+newStr);
//            }
            if (!limitTextView.isFull()) {
                limitTextView.forceInput(newStr);
                if (limitTextView.isFull()) {
                    requestNextFocus(limitTextView);
                }
            }else{
                limitTextView.forceInput(Integer.toString(newDigit));
            }
        }catch (Exception e){
        }
        return true;
    }

    private int getNumber(int keyCode){
        int number = keyCode - KeyEvent.KEYCODE_0;
        return number;
    }

    private void registerCoupon(String couponNumber) {
        if (Log.INCLUDE) {
            Log.d(TAG, "registerCoupon: couponNumber=" + couponNumber);
        }
        CouponDataManager couponDataManager = new CouponDataManager();
        couponDataManager.postRegisterCoupon(
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                couponNumber,
                new MbsDataProvider<String, MyCoupon>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                    }

                    @Override
                    public void onSuccess(String id, MyCoupon result) {
                        CouponRegistrationDialog dialog = ((CouponRegistrationDialog)getParentFragment());
                        if (Log.INCLUDE) {
                            Log.d(TAG, "registerCoupon: onSuccess: result=" + result);
                        }
                        Coupon coupon = null;
                        if(result != null) {
                            CouponType couponType = result.getType();
                            if(couponType != null) {
                                if (couponType == CouponType.DISCOUNT && result.getDiscountType() != DiscountType.AMOUNT) {
                                    coupon = result.getDiscountCoupon();
                                } else {
                                    coupon = result.getPointCoupon();
                                }
                            }
                        }
                        dialog.showRegisterSuccessFragment(coupon);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if(CPN_ERROR_EXPIRED_COUPON.equals(errorCode) || MBS_ERROR_EXPIRED_COUPON.equals(errorCode)){
                            setErrorMessage(R.string.coupon_registration_expired_desc);
                        }else if(CPN_ERROR_WRONG_COUPON_NUMBER.equals(errorCode) || MBS_ERROR_WRONG_COUPON_NUMBER.equals(errorCode)){
                            setErrorMessage(R.string.coupon_registration_wrong_desc);
                        }else if(CPN_ERROR_REGISTERED_COUPON.equals(errorCode) || MBS_ERROR_REGISTERED_COUPON.equals(errorCode)){
                            setErrorMessage(R.string.coupon_registration_registered_desc);
                        }else {
                            description.setTextColor(getResources().getColor(R.color.color_80E6E6E6, null));
                            description.setText(R.string.coupon_registration_default_desc);

                            ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                            errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                        }
                        resetInputView();
                    }
                });
    }
}
