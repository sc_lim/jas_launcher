/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.animation.AnimatorInflater;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.JasTvView;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Created by mc.kim on 30,07,2020
 */
public class PipContainerWindow implements View.OnKeyListener, LifecycleObserver {
    private View parentsView;
    private final WindowManager wm;
    private final String TAG = PipContainerWindow.class.getSimpleName();
    private final Context mContext;
    private OnPIPInteractor mOnPIPInteractor;
    private OnPipLifecycleListener mOnPipLifecycleListener;
    private static final int ANIMATION_MULTIPLIER = 1;

    private final Handler mPipControlHandler = new Handler(Looper.getMainLooper());

    private final TimeInterpolator mLogDecelerateInterpolator = new TimeInterpolator() {
        @Override
        public float getInterpolation(float t) {
            float mLogScale = 1f / computeLog(1, 100, 0);
            return computeLog(t, 100, 0) * mLogScale;
        }
    };

    private final TimeInterpolator mLogAccelerateInterpolator = new TimeInterpolator() {
        @Override
        public float getInterpolation(float t) {
            float mLogScale = 1f / computeLog(1, 100, 0);
            return 1 - computeLog(1 - t, 100, 0) * mLogScale;
        }
    };

    private float computeLog(float t, int base, int drift) {
        return (float) -Math.pow(base, -t) + 1 + (drift * t);
    }

    private final long DELAY_TIME = 5 * TimeUtil.SEC;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            setPipControllerShowing(false);
            reverseFirstOrStartSecond(mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator,
                    true);
        }
    };

    ValueAnimator mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator;

    private static ValueAnimator loadAnimator(Context context, int resId) {
        ValueAnimator animator = (ValueAnimator) AnimatorInflater.loadAnimator(context, resId);
        animator.setDuration(animator.getDuration() * ANIMATION_MULTIPLIER);
        return animator;
    }

    private void loadTransportAnimator(Context context) {
        final float mTransportTranslateY = context.getResources().getDimension(R.dimen.side_panel_padding_end);
        final ValueAnimator.AnimatorUpdateListener updateListener = new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator arg0) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call onAnimationUpdate");
                }
                final float fraction = (Float) arg0.getAnimatedValue();
                if (Log.INCLUDE) {
                    Log.d(TAG, "call onAnimationUpdate | fraction " + fraction);
                }
                float translateValue = mTransportTranslateY * (1f - fraction);
                if (Log.INCLUDE) {
                    Log.d(TAG, "call onAnimationUpdate | translateValue " + translateValue);
                }
                mPipOptionView.setTranslationY((int)translateValue);
//                startPlayerAnimation(mLastLocateIndex, ((int) translateValue / 2));
            }
        };
        mTransportRowFadeInAnimator = loadAnimator(context, R.animator.pip_control_fade_in);
        mTransportRowFadeInAnimator.addUpdateListener(updateListener);
        mTransportRowFadeInAnimator.setInterpolator(mLogDecelerateInterpolator);
        mTransportRowFadeOutAnimator = loadAnimator(context, R.animator.pip_control_fade_out);
        mTransportRowFadeOutAnimator.addUpdateListener(updateListener);
        mTransportRowFadeOutAnimator.setInterpolator(mLogAccelerateInterpolator);
    }

    static void endAll(ValueAnimator first, ValueAnimator second) {
        if (first.isStarted()) {
            first.end();
        } else if (second.isStarted()) {
            second.end();
        }
    }

    private void reverseFirstOrStartSecond(ValueAnimator first, ValueAnimator second,
                                           boolean runAnimation) {
        if (Log.INCLUDE) {
            Log.d(TAG, "first.isStarted() : " + first.isStarted());
        }
        if (first.isStarted()) {
            first.reverse();
            if (!runAnimation) {
                first.end();
            }
        } else {
            second.start();
            if (!runAnimation) {
                second.end();
            }
        }
    }

    private boolean isShowingController = true;

    private void setPipControllerShowing(boolean value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setPipControllerShowing : " + value);
        }
        isShowingController = value;
        if (mOnPipLifecycleListener != null) {
            mOnPipLifecycleListener.setVisiblePipOptions(value);
        }
    }

    private boolean isPipControllerShowing() {
        return isShowingController;
    }

    private boolean checkOverlayController(int keyCode) {
        mPipControlHandler.removeCallbacks(mHideRunnable);
        endAll(mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator);
        if (Log.INCLUDE) {
            Log.d(TAG, "checkOverlayController : " + isPipControllerShowing());
        }
        if (isPipControllerShowing()) {
            mPipControlHandler.postDelayed(mHideRunnable, DELAY_TIME);
            return false;
        } else {
            setPipControllerShowing(true);
            reverseFirstOrStartSecond(mTransportRowFadeOutAnimator, mTransportRowFadeInAnimator,
                    false);
            mPipControlHandler.postDelayed(mHideRunnable, DELAY_TIME);
            return true;
        }
    }

    private void hidePipController() {
        if (isPipControllerShowing()) {
            setPipControllerShowing(false);
            mPipControlHandler.removeCallbacks(mHideRunnable);
            reverseFirstOrStartSecond(mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator, true);
        }
    }

    public PipContainerWindow(Context context, int viewLocate, int viewSize, OnPIPInteractor onPIPInteractor, OnPipLifecycleListener onPipLifecycleListener) {
        mContext = context;

        mLastLocateIndex = viewLocate;
        mLastSizeIndex = viewSize;
        loadTransportAnimator(context);
        mOnPIPInteractor = onPIPInteractor;
        mOnPipLifecycleListener = onPipLifecycleListener;
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    }

    private PipTvView mPipPlayer = null;
    private View mPipOptionView = null;

    public enum ViewLocate {
        LEFT_TOP(94, 64, -1, -1, 2),
        RIGHT_TOP(-1, 64, 94, -1, 3),
        LEFT_BOTTOM(94, -1, -1, 135, 1),
        RIGHT_BOTTOM(-1, -1, 94, 135, 0);
        final int left;
        final int top;
        final int right;
        final int bottom;
        final int index;

        ViewLocate(int left, int top, int right, int bottom, int index) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
            this.index = index;
        }

        public static ViewLocate findByIndex(int index) {
            ViewLocate[] locateArray = values();
            for (ViewLocate locate : locateArray) {
                if (locate.index == index) {
                    return locate;
                }
            }
            return RIGHT_BOTTOM;
        }
    }

    public enum ViewSize {
        Small(592, 333, 0),
        Medium(704, 396, 1),
        Large(816, 459, 2);
        final int width;
        final int height;
        final int index;

        ViewSize(int width, int height, int index) {
            this.width = width;
            this.height = height;
            this.index = index;
        }

        public static ViewSize findByIndex(int index) {
            ViewSize[] sizeArray = values();
            for (ViewSize size : sizeArray) {
                if (size.index == index) {
                    return size;
                }
            }
            return Small;
        }
    }

    private ChannelDataManager mChannelDataManager;

    private int mLastLocateIndex = ViewLocate.RIGHT_BOTTOM.index;
    private int mLastSizeIndex = ViewSize.Medium.index;
    private Channel mSelectedChannel;

    public void show(String serviceId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "show");
        }
        mChannelDataManager = TvSingletons.getSingletons(mContext).getChannelDataManager();
        String srcId = serviceId;
        if (Log.INCLUDE) {
            Log.d(TAG, "show srcId : " + srcId);
        }
        if (StringUtils.nullToEmpty(srcId).isEmpty()) {
            return;
        }
        mSelectedChannel = mChannelDataManager.getChannelByServiceId(srcId);
        if (mOnPipLifecycleListener != null) {
            mOnPipLifecycleListener.show(this);
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void onCreate() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onCreate");
        }
        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_BASE_APPLICATION,
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                PixelFormat.TRANSLUCENT);
        if (parentsView != null) {
            wm.removeView(parentsView);
            parentsView = null;
        }
        parentsView = LayoutInflater.from(mContext).inflate(R.layout.dialog_pip_container, null, true);
        parentsView.setOnKeyListener(this);
        wm.addView(parentsView, mParams);
        mPipPlayer = parentsView.findViewById(R.id.pip_tv_view);
        setVideoSize(mLastSizeIndex);
        mPipOptionView = parentsView.findViewById(R.id.pip_option_view);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onStart() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onStart");
        }
        tuneChannel(mSelectedChannel);
        if (mPipPlayer != null) {
            mPipPlayer.attachChannelListListener();
        }
        mPipControlHandler.removeCallbacks(mHideRunnable);
        mPipControlHandler.postDelayed(mHideRunnable, DELAY_TIME);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onStop() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onStop");
        }

        if (mPipPlayer != null) {
            mPipPlayer.detachChannelListListener();
        }
        mPipPlayer.reset();

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroyed() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onDestroyed");
        }
        dismiss();
    }

    public void dismiss() {
        parentsView.setVisibility(View.GONE);
        mPipControlHandler.removeCallbacks(mHideRunnable);
        onStop();
        if (mOnPipLifecycleListener != null) {
            mOnPipLifecycleListener.dismiss(this);
            mOnPipLifecycleListener = null;
        }
        mOnPIPInteractor = null;
        wm.removeView(parentsView);
        parentsView = null;
    }

    public void tuneChannel(Channel channel) {
        mPipPlayer.tuneChannel(channel);
        mOnPipLifecycleListener.setPipServiceId(channel.getServiceId());
        mSelectedChannel = channel;
    }

    private void swapChannel() {
        boolean resetPip = mOnPIPInteractor.swapChannel(mPipPlayer);
        if (resetPip) {
            dismiss();
        } else {
            Channel ch = mPipPlayer.getCurrentChannel();
            mOnPipLifecycleListener.setPipServiceId(ch.getServiceId());
        }
    }

    private void setVideoSize(int index) {
        mLastSizeIndex = index;
        mOnPipLifecycleListener.setState(mLastLocateIndex, mLastSizeIndex);
        ViewLocate requestedLocate = ViewLocate.findByIndex(mLastLocateIndex);
        ViewSize requestedSize = ViewSize.findByIndex(index);
        int width = requestedSize.width;
        int height = requestedSize.height;

        int left = requestedLocate.left;
        int top = requestedLocate.top;
        int right = requestedLocate.right;
        int bottom = requestedLocate.bottom;

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height); // or wrap_content
        switch (requestedLocate) {
            case LEFT_BOTTOM:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.leftMargin = left;
                layoutParams.bottomMargin = bottom;

                break;

            case RIGHT_BOTTOM:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.bottomMargin = bottom;
                layoutParams.rightMargin = right;
                break;

            case LEFT_TOP:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.topMargin = top;
                layoutParams.leftMargin = left;
                break;


            case RIGHT_TOP:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.topMargin = top;
                layoutParams.rightMargin = right;

                break;
        }
        mPipPlayer.setLayoutParams(layoutParams);
    }

    private void changeVideoSize(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_PROG_YELLOW:
            case KeyEvent.KEYCODE_MEDIA_STOP:
                int size = ViewSize.values().length;
                int currentIndex = mLastSizeIndex + 1;
                if (currentIndex >= size) {
                    currentIndex = 0;
                }
                setVideoSize(currentIndex);
                break;
        }
    }

    private void slideTo(int keyCode, boolean smoothSlide) {
        int currentIndex = mLastLocateIndex;
        int size = ViewLocate.values().length;
        if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
            currentIndex = mLastLocateIndex + 1;
            if (currentIndex >= size) {
                currentIndex = 0;
            }
            startSlide(currentIndex, smoothSlide);
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
            currentIndex = mLastLocateIndex - 1;
            if (currentIndex < 0) {
                currentIndex = size - 1;
            }
            startSlide(currentIndex, smoothSlide);
        }
        mLastLocateIndex = currentIndex;
        mOnPipLifecycleListener.setState(mLastLocateIndex, mLastSizeIndex);
    }

    private void startPlayerAnimation(int index, int topMargin) {
        ViewLocate requestedLocate = ViewLocate.findByIndex(index);
        ViewSize requestedSize = ViewSize.findByIndex(mLastSizeIndex);

        if (requestedLocate != ViewLocate.LEFT_BOTTOM &&
                requestedLocate != ViewLocate.RIGHT_BOTTOM) {
            return;
        }

        int width = requestedSize.width;
        int height = requestedSize.height;

        int left = requestedLocate.left;
        int right = requestedLocate.right;
        int bottom = requestedLocate.bottom - topMargin;

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height); // or wrap_content
        switch (requestedLocate) {
            case LEFT_BOTTOM:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.leftMargin = left;
                layoutParams.bottomMargin = bottom;

                break;

            case RIGHT_BOTTOM:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.bottomMargin = bottom;
                layoutParams.rightMargin = right;
                break;

        }
        mPipPlayer.setLayoutParams(layoutParams);
    }

    private void startSlide(int index, boolean smoothSlide) {
        ViewLocate requestedLocate = ViewLocate.findByIndex(index);
        ViewSize requestedSize = ViewSize.findByIndex(mLastSizeIndex);

        int width = requestedSize.width;
        int height = requestedSize.height;

        int left = requestedLocate.left;
        int top = requestedLocate.top;
        int right = requestedLocate.right;
        int bottom = requestedLocate.bottom;

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height); // or wrap_content
        switch (requestedLocate) {
            case LEFT_BOTTOM:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.leftMargin = left;
                layoutParams.bottomMargin = bottom;

                break;

            case RIGHT_BOTTOM:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.bottomMargin = bottom;
                layoutParams.rightMargin = right;
                break;

            case LEFT_TOP:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.topMargin = top;
                layoutParams.leftMargin = left;
                break;


            case RIGHT_TOP:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.topMargin = top;
                layoutParams.rightMargin = right;

                break;
        }
        mPipPlayer.setLayoutParams(layoutParams);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent keyEvent) {
        if (keyEvent.getAction() == KeyEvent.ACTION_UP) {
            return true;
        } else {
            if (mOnPIPInteractor != null && mOnPIPInteractor.needPassKeyEvent(keyCode, keyEvent)) {
                return mOnPIPInteractor.onPipKeyEvent(keyCode, keyEvent);
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "onKey, keyCode=" + keyCode + "(" + KeyEvent.keyCodeToString(keyCode) + ")");
        }

        boolean consumed = false;

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                slideTo(keyCode, false);
                consumed = true;
                break;

            case KeyEvent.KEYCODE_DPAD_UP:
                tuneChannel(mPipPlayer.getNextChannel(true));
                consumed = true;
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                tuneChannel(mPipPlayer.getNextChannel(false));
                consumed = true;
                break;

            case KeyEvent.KEYCODE_MEDIA_STOP:
            case KeyEvent.KEYCODE_PROG_YELLOW:
                changeVideoSize(keyCode);
                consumed = true;
                break;

            case KeyEvent.KEYCODE_PROG_GREEN:
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                swapChannel();
                consumed = true;
                break;

            case KeyEvent.KEYCODE_BACK:
                dismiss();
                consumed = true;
                break;
        }

        if (consumed) {
            checkOverlayController(keyCode);
            return true;
        } else {
            hidePipController();
        }

        if (mOnPIPInteractor == null) {
            return true;
        }
        return mOnPIPInteractor.onPipKeyEvent(keyCode, keyEvent);
    }

    public interface OnPIPInteractor {
        boolean onPipKeyEvent(int keyCode, KeyEvent keyEvent);

        boolean needPassKeyEvent(int keyCode, KeyEvent keyEvent);

        boolean swapChannel(JasTvView pipTvView);

    }

    public interface OnPipLifecycleListener {
        void show(LifecycleObserver observer);

        void setState(int position, int size);

        void setPipServiceId(String serviceId);

        void setVisiblePipOptions(boolean visible);

        void dismiss(LifecycleObserver observer);
    }
}
