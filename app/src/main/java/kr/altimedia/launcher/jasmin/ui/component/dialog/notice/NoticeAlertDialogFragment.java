package kr.altimedia.launcher.jasmin.ui.component.dialog.notice;


import android.animation.AnimatorInflater;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.Map;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.message.MessageDataManager;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.AlertSystemDialog;

public class NoticeAlertDialogFragment implements View.OnKeyListener, ValueAnimator.AnimatorUpdateListener {
    public static final String CLASS_NAME = NoticeAlertDialogFragment.class.getName();
    private final String TAG = NoticeAlertDialogFragment.class.getSimpleName();

    private WindowManager wm;

    private final Context context;
    private final View parentsView;
    private RelativeLayout layout;

    private float layoutHeight = 0;
    private ValueAnimator moveUpAnimator;
    private ValueAnimator moveDownAnimator;

    private final Map<String, String> noticeMap;

    private MbsDataTask messageViewTask;

//    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    public NoticeAlertDialogFragment(Context context, Map<String, String> noticeMap) {
        this.context = context;
        this.noticeMap = noticeMap;

        LayoutInflater inflater = LayoutInflater.from(context);
        parentsView = inflater.inflate(R.layout.dialog_notice_alert, null, false);

        initView(parentsView);
    }

    private void initView(View view) {
        layout = view.findViewById(R.id.layout);

        initProgressbar(view);
        initNotice(view);
        initButton(view);
        initAnimator();
    }

    private void initProgressbar(View view) {
//        LayoutInflater inflater = LayoutInflater.from(view.getContext());
//        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
//        mProgressBarManager.setProgressBarView(loadingView);
    }

    private ValueAnimator loadAnimator(Context context, int resId) {
        ValueAnimator animator = (ValueAnimator) AnimatorInflater.loadAnimator(context, resId);
        return animator;
    }

    private void initNotice(View view) {
        TextView title = view.findViewById(R.id.notice_tile);
        TextView desc = view.findViewById(R.id.notice_desc);

        title.setText(noticeMap.get(PushMessageReceiver.KEY_MESSAGE_TITLE));
        desc.setText(noticeMap.get(PushMessageReceiver.KEY_MESSAGE_DESC));
    }

    private void initButton(View view) {
        TextView detailButton = view.findViewById(R.id.detail_button);
        TextView closeButton = view.findViewById(R.id.close_button);

        detailButton.setOnKeyListener(this);
        closeButton.setOnKeyListener(this);
    }

    private void initAnimator() {
        layoutHeight = context.getResources().getDimension(R.dimen.notice_alert_bottom_height);

        moveUpAnimator = loadAnimator(context, R.animator.reminder_alert_move_up);
        moveUpAnimator.setInterpolator(new DecelerateInterpolator());
        moveUpAnimator.addUpdateListener(this);

        moveDownAnimator = loadAnimator(context, R.animator.reminder_alert_move_down);
        moveDownAnimator.setInterpolator(new AccelerateInterpolator());
        moveDownAnimator.addUpdateListener(this);
    }

    public void show() {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                PixelFormat.TRANSLUCENT);
        mParams.gravity = Gravity.CENTER;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.addView(parentsView, mParams);
                moveUpAnimator.start();
            }
        });
    }

    private void dismiss() {
        if (messageViewTask != null && messageViewTask.isRunning()) {
            messageViewTask.cancel(true);
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.removeView(parentsView);
            }
        });
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        float y = value * layoutHeight;
        layout.setTranslationY(y);

        if (y >= layoutHeight && animation == moveDownAnimator) {
            dismiss();
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onKey : " + keyCode);
        }

        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (v.getId() == R.id.detail_button) {
                    showMessage();
                } else {
                    moveDownAnimator.start();
                }

                return true;
            case KeyEvent.KEYCODE_BACK:
                moveDownAnimator.start();
                return true;
        }

        return false;
    }

    private void showMessage() {
        if (messageViewTask != null && messageViewTask.isRunning()) {
            return;
        }

        MessageDataManager mMessageDataManager = new MessageDataManager();
        AccountManager accountManager = AccountManager.getInstance();

        String id = noticeMap.get(PushMessageReceiver.BUNDLE_KEY_ID);
        messageViewTask = mMessageDataManager.postMessageView(accountManager.getAccountId(), accountManager.getProfileId(), id,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
//                        if (loading) {
//                            mProgressBarManager.show();
//                        } else {
//                            mProgressBarManager.hide();
//                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed");
                        }
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSuccess");
                        }

                        NoticeDetailDialogFragment mNoticeDetailDialogFragment = new NoticeDetailDialogFragment(context, noticeMap);
                        mNoticeDetailDialogFragment.show();
                        dismiss();
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        AlertSystemDialog alertSystemDialog = new AlertSystemDialog(context, errorCode, message);
                        alertSystemDialog.show(context);
                    }
                });
    }
}
