/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.widget;

import android.graphics.drawable.Drawable;

import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;

/**
 * Created by mc.kim on 18,02,2020
 */
public class VideoPlaybackSeekDataProvider {
    private final Type type;

    public VideoPlaybackSeekDataProvider(Type type, int visibleSize) {
        this.type = type;
        this.mVisibleSize = visibleSize;
    }

    private final int mVisibleSize;

    public Type getType() {
        return type;
    }

    public int getVisibleSize() {
        return mVisibleSize;
    }

    public static class ResultCallback {
        public void onThumbnailLoaded(Drawable drawable, long seekTime) {
        }
    }

    public long[] getSeekPositions() {
        return null;
    }

    public void getThumbnail(long seekTime, ResultCallback callback) {
    }

    public void reset() {
    }

    public long getSkipTimeMillis() {
        return 0;
    }

    public enum Type {
        VOD, TIMESHIFT
    }

    public void setThumbnailBar(ThumbnailBar thumbnailBar, ItemBridgeAdapter adapter) {

    }

    public void notifyLoadPrevProgram(ThumbnailBar thumbnailBar, ItemBridgeAdapter adapter) {

    }

    public void notifyLoadNextProgram(ThumbnailBar thumbnailBar, ItemBridgeAdapter adapter) {

    }

    public void notifyLoadPlayingProgram(ThumbnailBar thumbnailBar, ItemBridgeAdapter adapter) {

    }

    public void notifySeekProgram(long timeMs) {

    }

    public void detach() {

    }
}
