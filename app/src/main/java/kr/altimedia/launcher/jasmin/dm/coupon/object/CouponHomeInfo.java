/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CouponHomeInfo implements Parcelable {
    public static final Creator<CouponHomeInfo> CREATOR = new Creator<CouponHomeInfo>() {
        @Override
        public CouponHomeInfo createFromParcel(Parcel in) {
            return new CouponHomeInfo(in);
        }

        @Override
        public CouponHomeInfo[] newArray(int size) {
            return new CouponHomeInfo[size];
        }
    };
    @Expose
    @SerializedName("totalCpnCnt")
    private int totalCouponCount;
    @Expose
    @SerializedName("thbCpnCnt")
    private int pointCouponCount;
    @Expose
    @SerializedName("rateCpnCnt")
    private int discountCouponCount;
    @Expose
    @SerializedName("list")
    private List<DiscountCoupon> discountCouponList;
    @Expose
    @SerializedName("pointBlncVal")
    private int pointBlncVal;   // Remaining amount after deducting the amount used
    @Expose
    @SerializedName("cpnExpPoint")
    private int cpnExpPoint;
    @Expose
    @SerializedName("cpnExpCnt")
    private int cpnExpCount;

    @Expose
    @SerializedName("membershipPoint")
    private int membershipPoint;
    @Expose
    @SerializedName("pointRatio")
    private int pointRatio;
    @Expose
    @SerializedName("pointLimit")
    private int pointLimitRate;

    protected CouponHomeInfo(Parcel in) {
        totalCouponCount = in.readInt();
        pointCouponCount = in.readInt();
        discountCouponCount = in.readInt();
        discountCouponList = in.createTypedArrayList(DiscountCoupon.CREATOR);
        pointBlncVal = in.readInt();
        cpnExpPoint = in.readInt();
        cpnExpCount = in.readInt();
        membershipPoint = in.readInt();
        pointRatio = in.readInt();
        pointLimitRate = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(totalCouponCount);
        dest.writeInt(pointCouponCount);
        dest.writeInt(discountCouponCount);
        dest.writeTypedList(discountCouponList);
        dest.writeInt(pointBlncVal);
        dest.writeInt(cpnExpPoint);
        dest.writeInt(cpnExpCount);
        dest.writeInt(membershipPoint);
        dest.writeInt(pointRatio);
        dest.writeInt(pointLimitRate);
    }

    public int getTotalCouponCount() {
        return totalCouponCount;
    }

    public int getPointCouponCount() {
        return pointCouponCount;
    }

    public int getDiscountCouponCount() {
        return discountCouponCount;
    }

    public List<DiscountCoupon> getDiscountCouponList() {
        return discountCouponList;
    }

    public int getPointBalance() {
        return pointBlncVal;
    }

    public int getCpnExpPoint() {
        return cpnExpPoint;
    }

    public int getCpnExpCount() {
        return cpnExpCount;
    }

    public int getMembershipPoint() {
        return membershipPoint;
    }

    public int getPointRatio() {
        return pointRatio;
    }

    public int getPointLimitRate() {
        return pointLimitRate;
    }

    @Override
    public String toString() {
        return "CouponHomeInfo{" +
                "totalCouponCount=" + totalCouponCount +
                ", pointCouponCount=" + pointCouponCount +
                ", discountCouponCount=" + discountCouponCount +
                ", discountCouponList=" + discountCouponList +
                ", pointBlncVal=" + pointBlncVal +
                ", cpnExpPoint=" + cpnExpPoint +
                ", cpnExpCount=" + cpnExpCount +
                ", membershipPoint=" + membershipPoint +
                ", pointRatio=" + pointRatio +
                ", pointLimit=" + pointLimitRate +
                '}';
    }
}
