/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.presenter;

import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;
import java.util.Iterator;

import androidx.annotation.NonNull;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;

public class VodSelectPackagePresenter extends Presenter {
    public final Rect posterRect = new Rect(0, 0, 196, 280);
    private final String TAG = VodSelectPackagePresenter.class.getSimpleName();

    private HashMap<String, Integer> flagMap = new HashMap<>();

    public VodSelectPackagePresenter() {
        flagMap.put(Content.FLAG_EVENT, R.id.flagEvent);
        flagMap.put(Content.FLAG_HOT, R.id.flagHot);
        flagMap.put(Content.FLAG_NEW, R.id.flagNew);
        flagMap.put(Content.FLAG_PREMIUM, R.id.flagPremium);
        flagMap.put(Content.FLAG_UHD, R.id.flagUhd);
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_vod_select_package, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((Product) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    private class ViewHolder extends Presenter.ViewHolder {
        private ImageView poster;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View view) {
            poster = view.findViewById(R.id.poster);
        }

        public void setData(Product product) {
            String thumbnailFileName = product.getPosterSourceUrl(posterRect.width(), posterRect.height());
            if (thumbnailFileName != null && !thumbnailFileName.isEmpty()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "package, thumbnailFileName : " + thumbnailFileName);
                }
                setPoster(thumbnailFileName);
            }

            initFlag(view, product);
        }

        private void setPoster(String url) {
            Glide.with(poster.getContext())
                    .load(url).diskCacheStrategy(DiskCacheStrategy.DATA)
                    .centerCrop()
                    .placeholder(R.drawable.poster_default)
                    .error(R.drawable.poster_default)
                    .into(poster);
        }

        private void initFlag(View view, Product product) {
            Iterator<String> keyIterator = flagMap.keySet().iterator();
            while (keyIterator.hasNext()) {
                String key = keyIterator.next();
                View flagView = view.findViewById(flagMap.get(key));
                flagView.setVisibility(View.GONE);
            }

            View lockIcon = view.findViewById(R.id.iconVodLock);
            if (lockIcon != null) {
                int rating = product.getRating();
                boolean isOverRating = PlaybackUtil.isVodOverRating(rating);

                if (!isOverRating) {
                    lockIcon.setVisibility(View.INVISIBLE);
                }
            }
        }
    }
}
