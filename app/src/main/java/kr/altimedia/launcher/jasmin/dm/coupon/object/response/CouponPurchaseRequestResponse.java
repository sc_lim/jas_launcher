/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseRequest;

public class CouponPurchaseRequestResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private CouponPurchaseRequest couponPurchaseRequest;

    protected CouponPurchaseRequestResponse(Parcel in) {
        super(in);
        couponPurchaseRequest = in.readParcelable(CouponPurchaseRequestResponse.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(couponPurchaseRequest, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CouponPurchaseRequestResponse> CREATOR = new Creator<CouponPurchaseRequestResponse>() {
        @Override
        public CouponPurchaseRequestResponse createFromParcel(Parcel in) {
            return new CouponPurchaseRequestResponse(in);
        }

        @Override
        public CouponPurchaseRequestResponse[] newArray(int size) {
            return new CouponPurchaseRequestResponse[size];
        }
    };

    public CouponPurchaseRequest getCouponPurchaseRequest() {
        return couponPurchaseRequest;
    }
}
