/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.contents.ContentStreamDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.EventFlagDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.dm.def.RentalType;
import kr.altimedia.launcher.jasmin.dm.def.StringArrayDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.TimeDateLongDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;
import kr.altimedia.launcher.jasmin.dm.recommend.obj.RcmdContent;
import kr.altimedia.launcher.jasmin.dm.user.object.CollectedContent;
import kr.altimedia.launcher.jasmin.dm.user.object.FavoriteContent;
import kr.altimedia.launcher.jasmin.dm.user.object.PurchasedContent;

/**
 * Created by mc.kim on 08,05,2020
 */
public class Content implements Parcelable {
    public static final Rect mPosterDefaultRect = new Rect(0, 0, 196, 280);
    public static final Rect mPosterPurchaseRect = new Rect(0, 0, 364, 520);


    public static final String FLAG_EVENT = "event";
    public static final String FLAG_HOT = "hot";
    public static final String FLAG_NEW = "new";
    public static final String FLAG_PREMIUM = "premium";
    public static final String FLAG_UHD = "uhd";

    @SerializedName("actor")
    @Expose
    private String actor;
    @SerializedName("contentGroupId")
    @Expose
    private String contentGroupId;
    @SerializedName("contentGroupAssetId")
    @Expose
    private String contentGroupAssetId;
    @SerializedName("director")
    @Expose
    private String director;
    @SerializedName("eventFlag")
    @Expose
    @JsonAdapter(EventFlagDeserializer.class)
    private List<String> eventFlag;
    @SerializedName("genre")
    @Expose
    @JsonAdapter(StringArrayDeserializer.class)
    private List<String> genre;
    @SerializedName("isLike")
    @Expose
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isLike;
    @SerializedName("isSeries")
    @Expose
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isSeries;
    @SerializedName("nextSeriesAssetId")
    @Expose
    private String nextSeriesAssetId;
    @SerializedName("posterAssetId")
    @Expose
    private String posterAssetId;
    @SerializedName("posterFileName")
    @Expose
    private String posterFileName;
    @SerializedName("posterSourceUrl")
    @Expose
    private String posterSourceUrl;
    @SerializedName("posterType")
    @Expose
    private String posterType;
    @SerializedName("productList")
    @Expose
    private List<Product> productList = new ArrayList<>();
    @SerializedName("rating")
    @Expose
    private int rating;
    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;
    @SerializedName("resolutionType")
    @Expose
    private String resolutionType;
    @SerializedName("resumeTime")
    @Expose
    @JsonAdapter(TimeDateLongDeserializer.class)
    private long resumeTime;
    @SerializedName("runTimeDisplay")
    @Expose
    private long runTimeDisplay;
    @SerializedName("seriesAssetId")
    @Expose
    private String seriesAssetId;
    @SerializedName("movieAssetId")
    @Expose
    private String movieAssetId;
    @SerializedName("streaminfo")
    @Expose
    @JsonAdapter(ContentStreamDeserializer.class)
    private List<StreamInfo> streamInfoList = new ArrayList<>();
    @SerializedName("synopsis")
    @Expose
    private String synopsis;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("containerType")
    @Expose
    private String containerType;
    @SerializedName("videoUrl")
    @Expose
    private String videoUrl;
    @SerializedName("episodeCnt")
    @Expose
    private int episodeCnt;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    private String termsAndConditions;

    @Expose
    @SerializedName("bannerClipAsset")
    private BannerClipAsset bannerClipAsset;
    @Expose
    @SerializedName("bannerImageAsset")
    private BannerImageAsset bannerImageAsset;
    @Expose
    @SerializedName("bannerType")
    private int bannerType;
    public final static Parcelable.Creator<Content> CREATOR = new Creator<Content>() {

        @SuppressWarnings({
                "unchecked"
        })
        public Content createFromParcel(Parcel in) {
            return new Content(in);
        }

        public Content[] newArray(int size) {
            return (new Content[size]);
        }

    };

    protected Content(Parcel in) {
        this.actor = ((String) in.readValue((String.class.getClassLoader())));
        this.contentGroupId = ((String) in.readValue((String.class.getClassLoader())));
        this.contentGroupAssetId = ((String) in.readValue((String.class.getClassLoader())));
        this.director = ((String) in.readValue((String.class.getClassLoader())));
        this.eventFlag = new ArrayList<>();
        in.readList(this.eventFlag, String.class.getClassLoader());
        this.genre = new ArrayList<>();
        in.readList(this.genre, String.class.getClassLoader());
        this.isLike = in.readByte() != 0;
        this.isSeries = in.readByte() != 0;
        this.nextSeriesAssetId = ((String) in.readValue((String.class.getClassLoader())));
        this.posterAssetId = ((String) in.readValue((String.class.getClassLoader())));
        this.posterFileName = ((String) in.readValue((String.class.getClassLoader())));
        this.posterSourceUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.posterType = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.productList, (Product.class.getClassLoader()));
        this.rating = ((int) in.readValue((Integer.class.getClassLoader())));
        this.releaseDate = ((String) in.readValue((String.class.getClassLoader())));
        this.resolutionType = ((String) in.readValue((String.class.getClassLoader())));
        this.resumeTime = ((Long) in.readValue((Long.class.getClassLoader())));
        this.runTimeDisplay = ((Long) in.readValue((Long.class.getClassLoader())));
        this.seriesAssetId = ((String) in.readValue((String.class.getClassLoader())));
        this.movieAssetId = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.streamInfoList, StreamInfo.class.getClassLoader());
        this.synopsis = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.containerType = ((String) in.readValue((String.class.getClassLoader())));
        this.videoUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.episodeCnt = in.readInt();
        this.categoryId = in.readString();
        this.termsAndConditions = in.readString();
        this.bannerClipAsset = (BannerClipAsset) in.readValue(BannerClipAsset.class.getClassLoader());
        this.bannerImageAsset = (BannerImageAsset) in.readValue(BannerImageAsset.class.getClassLoader());
        this.bannerType = in.readInt();
    }

    private Content() {
    }

    public String getActor() {
        return actor;
    }


    public String getContentGroupId() {
        if (!StringUtils.nullToEmpty(contentGroupId).isEmpty()) {
            return contentGroupId;
        } else {
            return contentGroupAssetId;
        }
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        if (Log.INCLUDE) {
            Log.d("contents", "setCategoryId : " + categoryId);
        }
        this.categoryId = categoryId;
    }

    private static String decodeString(String text) throws UnsupportedEncodingException {
        return new String(text.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
    }

    public static Content buildContent(RcmdContent rcmdContent) {
        Content content = new Content();
        try {
            String encodedString = decodeString(rcmdContent.getItemName());
            content.title = encodedString;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            content.title = "";
        }
        content.posterFileName = rcmdContent.getImgUrl();
        content.contentGroupAssetId = rcmdContent.getItemId();
        content.rating = rcmdContent.getRating();
        content.eventFlag = rcmdContent.getNewHot();
        content.isSeries = rcmdContent.getSeriesYn().equalsIgnoreCase("Y");
        content.seriesAssetId = rcmdContent.getSeriesId();
        content.containerType = rcmdContent.getImgExt();
        content.categoryId = rcmdContent.getCatId();
        return content;
    }

    public static Content buildFavoriteContent(FavoriteContent favoriteContent) {
        Content content = new Content();
        content.contentGroupId = favoriteContent.getContentGroupId();
        content.rating = favoriteContent.getRating();
        content.isSeries = favoriteContent.isSeries();
        content.seriesAssetId = favoriteContent.getSeriesAssetId();
        return content;
    }

    public static Content buildCollectedContent(CollectedContent collectedContent) {
        Content content = new Content();
        content.contentGroupId = collectedContent.getContentGroupId();
        content.rating = collectedContent.getRating();
        content.isSeries = collectedContent.isSeries();
        content.seriesAssetId = collectedContent.getSeriesAssetId();
        return content;
    }

    public static Content buildPurchasedContent(PurchasedContent purchasedContent) {
        Content content = new Content();
        content.contentGroupId = purchasedContent.getContentGroupId();
        content.rating = purchasedContent.getRating();
        content.isSeries = purchasedContent.isSeries();
        content.seriesAssetId = purchasedContent.getSeriesAssetId();
        return content;
    }

    public String getDirector() {
        return director;
    }


    public List<String> getEventFlag() {
        return eventFlag;
    }


    public List<String> getGenre() {
        if (genre == null) {
            return new ArrayList<>();
        }
        return genre;
    }


    public void setLike(boolean like) {
        isLike = like;
    }

    public boolean isLike() {
        return isLike;
    }

    public boolean isSeries() {
        return isSeries;
    }

    public String getNextSeriesAssetId() {
        return nextSeriesAssetId;
    }


    public String getPosterAssetId() {
        return posterAssetId;
    }


    public String getPosterSourceUrl(int width, int height) {
        String posterType = StringUtils.nullToEmpty(containerType).isEmpty() ? "" : containerType;
        if (posterFileName == null) {
            return posterSourceUrl + "_" + width + "x" + height + "." + posterType;
        } else {
            return posterFileName + "_" + width + "x" + height + "." + posterType;
        }
    }


    public String getPosterType() {
        return posterType;
    }


    public List<Product> getProductList() {
        List<Product> list = new ArrayList<>();
        for (Product product : productList) {
            if (product.getProductType() == ProductType.ERROR || product.getRentalType() == RentalType.ERROR) {
                if (Log.INCLUDE) {
                    Log.d("Content", "ERROR Type : " + product);
                }
                continue;
            }

            list.add(product);
        }

        return list;
    }

    public HashMap<String, List<Product>> getSubscriptionMap() {
        HashMap<String, List<Product>> resultMap = new HashMap<>();
        for (Product product : productList) {
            if (product.getRentalType() != RentalType.SUBSCRIBE) {
                continue;
            }
            String subscribeId = StringUtils.nullToEmpty(product.getProductId());
            if (resultMap.containsKey(subscribeId)) {
                List<Product> savedProductList = resultMap.get(subscribeId);
                savedProductList.add(product);
                resultMap.put(subscribeId, savedProductList);
            } else {
                List<Product> savedProductList = new ArrayList<>();
                savedProductList.add(product);
                resultMap.put(subscribeId, savedProductList);
            }
        }
        return resultMap;
    }

    public List<Product> getSubscriptionList(String targetSubscriptionId) {
        List<Product> resultList = new ArrayList<>();
        for (Product product : productList) {
            if (product.getRentalType() != RentalType.SUBSCRIBE) {
                continue;
            }
            String subscribeId = StringUtils.nullToEmpty(product.getProductId());
            if (subscribeId.equalsIgnoreCase(targetSubscriptionId)) {
                resultList.add(product);
            }
        }
        return resultList;
    }

    public int getRating() {
        return rating;
    }

    public String getReleaseDate() {
        return StringUtils.nullToEmpty(releaseDate);
    }

    public String getResolutionType() {
        return resolutionType;
    }


    public void setResumeTime(long resumeTime) {
        this.resumeTime = resumeTime;
    }

    public long getResumeTime() {
        return resumeTime;
    }

    public long getRunTime() {
        return runTimeDisplay;
    }

    public String getSeriesAssetId() {
        return seriesAssetId;
    }

    public StreamInfo getMainStreamInfo() {
        for (StreamInfo streamInfo : streamInfoList) {
            if (streamInfo.getStreamType() == StreamType.Main) {
                return streamInfo;
            }
        }
        return null;
    }

    public boolean isWatchable() {
        List<Product> productList = getProductList();
        if (productList == null) {
            return false;
        }
        boolean watchable = false;
        for (Product product : productList) {
            if (product.isPurchased() || product.getPrice() == 0) {
                watchable = true;
                break;
            }
        }
        return watchable;
    }

    public List<StreamInfo> getTrailerStreamInfo() {
        List<StreamInfo> infoList = new ArrayList<>();
        boolean isWatchable = isWatchable();

        for (StreamInfo streamInfo : streamInfoList) {
            if (streamInfo.getStreamType() != StreamType.Main) {
                if (isWatchable && (streamInfo.getStreamType() == StreamType.Preview)) {
                    continue;
                }
                infoList.add(streamInfo);
            }
        }
        return infoList;
    }


    public String getSynopsis() {
        return synopsis;
    }


    public String getTitle() {
        return title;
    }

    public String getMovieAssetId() {
        return movieAssetId;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public int getEpisodeCnt() {
        return episodeCnt;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(actor);
        dest.writeValue(contentGroupId);
        dest.writeValue(contentGroupAssetId);
        dest.writeValue(director);
        dest.writeList(eventFlag);
        dest.writeList(genre);
        dest.writeByte((byte) (isLike ? 1 : 0));
        dest.writeByte((byte) (isSeries ? 1 : 0));
        dest.writeValue(nextSeriesAssetId);
        dest.writeValue(posterAssetId);
        dest.writeValue(posterFileName);
        dest.writeValue(posterSourceUrl);
        dest.writeValue(posterType);
        dest.writeList(productList);
        dest.writeValue(rating);
        dest.writeValue(releaseDate);
        dest.writeValue(resolutionType);
        dest.writeValue(resumeTime);
        dest.writeValue(runTimeDisplay);
        dest.writeValue(seriesAssetId);
        dest.writeValue(movieAssetId);
        dest.writeList(streamInfoList);
        dest.writeValue(synopsis);
        dest.writeValue(title);
        dest.writeValue(containerType);
        dest.writeValue(videoUrl);
        dest.writeInt(episodeCnt);
        dest.writeString(categoryId);
        dest.writeString(termsAndConditions);
        dest.writeValue(bannerClipAsset);
        dest.writeValue(bannerImageAsset);
        dest.writeInt(bannerType);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "Content{" +
                "actor='" + actor + '\'' +
                ", contentGroupId='" + contentGroupId + '\'' +
                ", contentGroupAssetId='" + contentGroupAssetId + '\'' +
                ", director='" + director + '\'' +
                ", eventFlag=" + eventFlag +
                ", genre=" + genre +
                ", isLike=" + isLike +
                ", isSeries=" + isSeries +
                ", nextSeriesAssetId='" + nextSeriesAssetId + '\'' +
                ", posterAssetId='" + posterAssetId + '\'' +
                ", posterFileName='" + posterFileName + '\'' +
                ", posterSourceUrl='" + posterSourceUrl + '\'' +
                ", posterType='" + posterType + '\'' +
                ", productList=" + productList +
                ", rating=" + rating +
                ", releaseDate='" + releaseDate + '\'' +
                ", resolutionType='" + resolutionType + '\'' +
                ", resumeTime=" + resumeTime +
                ", runTimeDisplay=" + runTimeDisplay +
                ", seriesAssetId='" + seriesAssetId + '\'' +
                ", movieAssetId='" + movieAssetId + '\'' +
                ", streamInfoList=" + streamInfoList +
                ", synopsis='" + synopsis + '\'' +
                ", title='" + title + '\'' +
                ", containerType='" + containerType + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", episodeCnt=" + episodeCnt +
                ", categoryId='" + categoryId + '\'' +
                ", termsAndConditions='" + termsAndConditions + '\'' +
                ", bannerClipAsset=" + bannerClipAsset +
                ", bannerImageAsset=" + bannerImageAsset +
                ", bannerType=" + bannerType +
                '}';
    }

    public BannerClipAsset getBannerClipAsset() {
        if (bannerClipAsset.getFileName() == null) {
            return null;
        }
        return bannerClipAsset;
    }

    public BannerImageAsset getBannerImageAsset() {

        return bannerImageAsset;
    }

    public int getBannerType() {
        return bannerType;
    }
}