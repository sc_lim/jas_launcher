package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase;

import android.graphics.Typeface;
import android.view.View;
import android.widget.CheckBox;

import androidx.leanback.widget.ObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.single.presenter.SubscriptionButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.BaseGridView;

public class SubscriptionButtonBridgeAdapter extends ItemBridgeAdapter {
    private OnButtonClickListener mOnButtonClickListener;
    private BaseGridView mBaseGridView;

    private Typeface focusFont;
    private Typeface unFocusFont;

    public SubscriptionButtonBridgeAdapter(ObjectAdapter adapter, BaseGridView gridView) {
        super(adapter);
        this.mBaseGridView = gridView;
    }

    public void setOnButtonClickListener(OnButtonClickListener mOnButtonClickListener) {
        this.mOnButtonClickListener = mOnButtonClickListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        SubscriptionButtonPresenter.ViewHolder vh = (SubscriptionButtonPresenter.ViewHolder) viewHolder.getViewHolder();

        focusFont = vh.view.getResources().getFont(R.font.font_prompt_medium);
        unFocusFont = vh.view.getResources().getFont(R.font.font_prompt_regular);

        CheckBox checkBox = (CheckBox) vh.view;
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnButtonClickListener != null) {
                    mOnButtonClickListener.onClick(mBaseGridView.getSelectedPosition(), checkBox.isChecked());
                }
            }
        });
        checkBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Typeface font = hasFocus ? focusFont : unFocusFont;
                checkBox.setTypeface(font);
            }
        });
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        SubscriptionButtonPresenter.ViewHolder vh = (SubscriptionButtonPresenter.ViewHolder) viewHolder.getViewHolder();
        vh.view.setOnClickListener(null);
        vh.view.setOnFocusChangeListener(null);
    }

    public interface OnButtonClickListener {
        void onClick(int index, boolean isChecked);
    }
}
