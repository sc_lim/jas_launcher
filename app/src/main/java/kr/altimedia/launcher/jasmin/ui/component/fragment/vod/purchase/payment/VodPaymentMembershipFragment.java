/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Payment;
import kr.altimedia.launcher.jasmin.dm.payment.obj.RequestVodPaymentResultInfo;
import kr.altimedia.launcher.jasmin.dm.payment.obj.VodPaymentResult;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentWrapper;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class VodPaymentMembershipFragment extends VodPaymentBaseFragment {
    public static final String CLASS_NAME = VodPaymentMembershipFragment.class.getName();
    private final String TAG = VodPaymentMembershipFragment.class.getSimpleName();

    private static final String KEY_PURCHASED_INFO = "PURCHASED_INFO";

    private TextView minView;
    private TextView secView;
    private CountDownTimer countDownTimer;

    private PurchaseInfo purchaseInfo;

    public VodPaymentMembershipFragment() {
    }

    public static VodPaymentMembershipFragment newInstance(PurchaseInfo purchaseInfo) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_PURCHASED_INFO, purchaseInfo);

        VodPaymentMembershipFragment fragment = new VodPaymentMembershipFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_vod_payment_membership;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        initAccountInfo(view);
        initButton(view);
        initTimer(view);
        requestPayment();
    }

    private void initAccountInfo(View view) {
        AccountManager accountManager = AccountManager.getInstance();

        TextView accountName = view.findViewById(R.id.account_name);
        TextView accountId = view.findViewById(R.id.account_id);

        accountName.setText(accountManager.getAccountName());
        accountId.setText(accountManager.getSaId());
    }

    private void initButton(View view) {
        TextView cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOnPaymentListener().onDismissFragment();
            }
        });
    }

    private void initTimer(View view) {
        minView = view.findViewById(R.id.min_counting);
        secView = view.findViewById(R.id.sec_counting);
        setTime(TOTAL_COUNT_DOWN);

        String timeOutErrorMessage = getString(R.string.payment_error);
        countDownTimer = new CountDownTimer(TOTAL_COUNT_DOWN, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                setTime(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                if (getOnPaymentListener() != null) {
                    getOnPaymentListener().onPaymentError(timeOutErrorMessage);
                }
            }
        };
    }

    private int setTime(long millisUntilFinished) {
        long min = TimeUtil.getMin(millisUntilFinished);
        long sec = TimeUtil.getSec(millisUntilFinished);

        String minText = min < 10 ? "0" + min : String.valueOf(min);
        String secText = sec < 10 ? "0" + sec : String.valueOf(sec);
        minView.setText(minText);
        secView.setText(secText);

        return (int) sec;
    }

    private void requestPayment() {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestPayment");
        }

        purchaseInfo = getArguments().getParcelable(KEY_PURCHASED_INFO);
        Product product = purchaseInfo.getProduct();
        PaymentWrapper paymentWrapper = purchaseInfo.getPaymentWrapper();

        ArrayList<Payment> payments = getPaymentList(purchaseInfo);

        paymentRequestTask = vodPaymentDataManager.requestVodPayment(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), purchaseInfo.getContentType(),
                product.getOfferID(), payments, paymentWrapper.getPrice(),
                new MbsDataProvider<String, RequestVodPaymentResultInfo>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, RequestVodPaymentResultInfo result) {
                        purchaseId = result.getPurchaseId();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onSuccess, purchasedId : " + purchaseId);
                        }

                        countDownTimer.start();
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPaymentMembershipFragment.this.getContext();
                    }
                });
    }

    private TvOverlayManager mTvOverlayManager = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttached : context");
        }
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
    }

    @Override
    protected void getPaymentResult(String resultId) {
        String offerID = purchaseInfo.getProduct().getOfferID();
        if (Log.INCLUDE) {
            Log.d(TAG, "getPaymentResult, offerID : " + offerID);
        }

        if (purchaseId == null || !resultId.equalsIgnoreCase(offerID)) {
            return;
        }

        if (paymentResultTask != null && paymentResultTask.isRunning()) {
            paymentResultTask.cancel(true);
        }

        paymentResultTask = vodPaymentDataManager.getVodPaymentResult(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), purchaseId,
                new MbsDataProvider<String, VodPaymentResult>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPaymentResult, onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, VodPaymentResult result) {
                        boolean isSuccess = result.isSuccess();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPaymentResult, onSuccess, isSuccess : " + isSuccess);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentComplete(false, isSuccess);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPaymentResult, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPaymentMembershipFragment.this.getContext();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        countDownTimer.cancel();
    }
}
