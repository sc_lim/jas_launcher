/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/** A class that includes convenience methods for view classes. */
public class ViewUtils {
    private static final String TAG = "ViewUtils";

    private ViewUtils() {
        // Prevent instantiation.
    }

    public static void setTransitionAlpha(View v, float alpha) {
        /* Begin_AOSP_Before_Q_Comment_Out */
        /* End_AOSP_Before_Q_Comment_Out */
        Method method;
        try {
            method = View.class.getDeclaredMethod("setTransitionAlpha", Float.TYPE);
            method.invoke(v, alpha);
        } catch (NoSuchMethodException
                | IllegalAccessException
                | IllegalArgumentException
                | InvocationTargetException e) {
            Log.e(TAG, "Fail to call View.setTransitionAlpha", e);
        }
    }
    public static Animator createHeightAnimator(
            final View target, int initialHeight, int targetHeight) {
        ValueAnimator animator = ValueAnimator.ofInt(initialHeight, targetHeight);
        animator.addUpdateListener(
                new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        int value = (Integer) animation.getAnimatedValue();
                        if (value == 0) {
                            if (target.getVisibility() != View.GONE) {
                                target.setVisibility(View.GONE);
                            }
                        } else {
                            if (target.getVisibility() != View.VISIBLE) {
                                target.setVisibility(View.VISIBLE);
                            }
                            setLayoutHeight(target, value);
                        }
                    }
                });
        return animator;
    }

    /** Gets view's layout height. */
    public static int getLayoutHeight(View view) {
        LayoutParams layoutParams = view.getLayoutParams();
        return layoutParams.height;
    }

    /** Sets view's layout height. */
    public static void setLayoutHeight(View view, int height) {
        LayoutParams layoutParams = view.getLayoutParams();
        if (height != layoutParams.height) {
            layoutParams.height = height;
            view.setLayoutParams(layoutParams);
        }
    }
}
