/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.observable;

import androidx.databinding.BaseObservable;

import com.altimedia.tvmodule.dao.Channel;

import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class ChannelLiveData extends BaseObservable {

    private Channel currentChannel = null;

    public void setValue(Channel value) {
        currentChannel = value;
    }

    public Channel getCurrentChannel() {
        return currentChannel;
    }

    public String getChannelNum() {
        if (currentChannel == null) {
            return "000";
        } else if (currentChannel.isHiddenChannel()) {
            return "";
        } else {
            String chNum = StringUtil.getFormattedNumber(currentChannel.getDisplayNumber(), 3);
            return chNum;
        }
    }

    public String getChannelName() {
        if (currentChannel == null) {
            return "";
        } else {
            return currentChannel.getDisplayName();
        }
    }

    public boolean isTimeShiftEnabled() {
        return (currentChannel != null ? currentChannel.getTimeshiftService() > 0 : false);
    }
}
