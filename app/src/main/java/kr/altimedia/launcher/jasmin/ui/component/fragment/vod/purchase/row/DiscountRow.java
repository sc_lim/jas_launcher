/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.row;

import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Row;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;

public class DiscountRow extends Row {
    private Product product;
    private final ObjectAdapter mAdapter;

    public DiscountRow(Product product, ObjectAdapter mAdapter) {
        this.product = product;
        this.mAdapter = mAdapter;
    }

    public Product getProduct() {
        return product;
    }

    public ObjectAdapter getAdapter() {
        return mAdapter;
    }
}
