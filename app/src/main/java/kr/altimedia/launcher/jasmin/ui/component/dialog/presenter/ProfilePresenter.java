/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;

public class ProfilePresenter extends Presenter {
    public static final int TYPE_SELECT = 1;
    public static final int TYPE_EDIT = 2;
    private final int TYPE;

    public ProfilePresenter() {
        TYPE = TYPE_SELECT;
    }

    public ProfilePresenter(int type) {
        TYPE = type;
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_watching_profile, parent, false);
        return new ViewHolder(view, TYPE);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.view.setVisibility(View.VISIBLE);
        vh.setData((ProfileInfo) item, TYPE);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private ImageView pic;
        private ImageView lock;
        private TextView name;

        public ViewHolder(View view, int type) {
            super(view);
            initView(view, type);
        }

        private void initView(View view, int type) {
            pic = view.findViewById(R.id.profile_pic);
            lock = view.findViewById(R.id.lock);
            if (type == TYPE_SELECT) {
                lock.setImageResource(R.drawable.my_profile_lock);
            } else {
                lock.setImageResource(R.drawable.profile_edit);
            }
            name = view.findViewById(R.id.profile_name);
        }

        public void setData(ProfileInfo profileInfo, int type) {
            name.setText(profileInfo.getProfileName());
            try {
                profileInfo.setProfileIcon(pic);
            }catch (Exception e){
            }
            if (type == TYPE_SELECT) {
                int visibility = profileInfo.isLock() ? View.VISIBLE : View.INVISIBLE;
                lock.setVisibility(visibility);
            } else {
                lock.setVisibility(View.VISIBLE);
            }

        }
    }
}
