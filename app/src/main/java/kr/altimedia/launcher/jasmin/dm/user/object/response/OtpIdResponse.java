/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.OtpId;

public class OtpIdResponse extends BaseResponse {
    public static final Creator<OtpIdResponse> CREATOR = new Creator<OtpIdResponse>() {
        @Override
        public OtpIdResponse createFromParcel(Parcel in) {
            return new OtpIdResponse(in);
        }

        @Override
        public OtpIdResponse[] newArray(int size) {
            return new OtpIdResponse[size];
        }
    };
    @Expose
    @SerializedName("data")
    private OtpId otpId;

    protected OtpIdResponse(Parcel in) {
        super(in);
        otpId = in.readParcelable(OtpId.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(otpId);
    }

    public OtpId getOtpId() {
        return otpId;
    }
}
