package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class CreditCardPageBaseFragment extends Fragment {
    protected static int INIT_POSITION = 0;

    public static void setSelectedPosition(int selectedPosition) {
        INIT_POSITION = selectedPosition;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getResourceLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    protected void initView(View view) {
    }

    protected abstract int getResourceLayoutId();

    protected abstract int getSize();

    protected abstract Object getItem(int position);

    protected abstract int getFocusedPosition();

    protected abstract void requestPosition(int position);

    protected abstract boolean isLastColumn(int position);
}
