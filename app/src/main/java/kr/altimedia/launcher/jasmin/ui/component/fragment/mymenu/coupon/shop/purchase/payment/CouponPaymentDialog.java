/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProduct;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Bank;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.BankResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.CreditCardResponse;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.PurchaseInfo;


public class CouponPaymentDialog extends SafeDismissDialogFragment implements CouponPaymentBaseFragment.OnPaymentListener {
    public static final String CLASS_NAME = CouponPaymentDialog.class.getName();
    private static final String TAG = CouponPaymentDialog.class.getSimpleName();

    private static final String KEY_PURCHASE_INFO = "PURCHASE_INFO";
    public static final String KEY_PURCHASE_OPTION_ITEM = "OPTION_LIST";

    private CouponProduct couponProduct;
    private PaymentType paymentType;
    private PurchaseInfo purchaseInfo;

    private OnPaymentCompleteListener onPaymentListener;

    private CouponPaymentDialog() {

    }

    public static CouponPaymentDialog newInstance(PurchaseInfo purchaseInfo, Parcelable object) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_PURCHASE_INFO, purchaseInfo);
        args.putParcelable(KEY_PURCHASE_OPTION_ITEM, object);

        CouponPaymentDialog fragment = new CouponPaymentDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnPaymentListener(OnPaymentCompleteListener onPaymentListener) {
        this.onPaymentListener = onPaymentListener;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogBlackTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_coupon_shop_payment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initPurchaseInfo();

        switch (paymentType) {
            case BILLING:
                showBillingFragment();
                break;
            case MEMBERSHIP_APP:
                showMembershipFragment();
                break;
            case AIR_PAY:
            case QR:
                showGuideFragment();
                break;
            case CREDIT_CARD:
                showCreditCardFragment();
                break;
            case INTERNET_MOBILE_BANK:
                showBankingFragment();
                break;
        }
    }

    private void initPurchaseInfo() {
        purchaseInfo = getArguments().getParcelable(KEY_PURCHASE_INFO);
        this.couponProduct = purchaseInfo.getCouponProduct();
        this.paymentType = purchaseInfo.getPaymentType();

        if (Log.INCLUDE) {
            Log.d(TAG, "initPurchaseInfo: couponProduct=" + couponProduct + ", paymentType=" + paymentType);
        }
    }

    private void showBillingFragment() {
        CouponPaymentBillingFragment couponPaymentBillingFragment = CouponPaymentBillingFragment.newInstance(purchaseInfo);
        couponPaymentBillingFragment.setOnPaymentListener(this);
        attacheFragment(couponPaymentBillingFragment, CouponPaymentBillingFragment.CLASS_NAME);
    }

    private void showMembershipFragment() {
        CouponPaymentMembershipFragment couponPaymentMembershipFragment = CouponPaymentMembershipFragment.newInstance(purchaseInfo);
        couponPaymentMembershipFragment.setOnPaymentListener(this);
        attacheFragment(couponPaymentMembershipFragment, CouponPaymentMembershipFragment.CLASS_NAME);
    }

    private void showGuideFragment() {
        CouponPaymentGuideFragment couponPaymentGuideFragment = CouponPaymentGuideFragment.newInstance(purchaseInfo);
        attacheFragment(couponPaymentGuideFragment, CouponPaymentGuideFragment.CLASS_NAME);
    }

    private void showCreditCardFragment() {
        CreditCardResponse.CreditCardResult creditCardResult = getArguments().getParcelable(KEY_PURCHASE_OPTION_ITEM);
        if (creditCardResult == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showCreditCardFragment: creditCardResult is null, so do not show payment dialog");
            }

            dismiss();
            return;
        }

        ArrayList<CreditCard> otherCardList = (ArrayList<CreditCard>) creditCardResult.getOtherCardList();
        ArrayList<CreditCard> creditCardList = (ArrayList<CreditCard>) creditCardResult.getCreditCardList();

        CouponPaymentCreditCardFragment paymentCreditCardFragment = CouponPaymentCreditCardFragment.newInstance(otherCardList, creditCardList);
        paymentCreditCardFragment.setOnClickCreditCardListener(new CouponPaymentCreditCardFragment.OnClickCreditCardListener() {
            @Override
            public void onClickCreditCard(CreditCard creditCard) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "showCreditCardFragment: onClickBank: creditCard=" + creditCard);
                }

                purchaseInfo.setCreditCard(creditCard);
                showGuideFragment();
            }
        });
        attacheFragment(paymentCreditCardFragment, CouponPaymentCreditCardFragment.CLASS_NAME);
    }

    private void showBankingFragment() {
        BankResponse.BankResult bankResult = getArguments().getParcelable(KEY_PURCHASE_OPTION_ITEM);
        if (bankResult == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showBankingFragment: bankResult is null, so do not show payment dialog");
            }

            dismiss();
            return;
        }

        ArrayList optionList = (ArrayList) bankResult.getBankList();
        CouponPaymentBankingFragment paymentBankingFragment = CouponPaymentBankingFragment.newInstance(optionList);
        paymentBankingFragment.setOnClickBankListener(new CouponPaymentBankingFragment.OnClickBankListener() {
            @Override
            public void onClickBank(Bank bank) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "showBankingFragment: onClickBank: bank=" + bank);
                }

                purchaseInfo.setBank(bank);
                showGuideFragment();
            }
        });
        attacheFragment(paymentBankingFragment, CouponPaymentBankingFragment.CLASS_NAME);
    }

    private void showPurchaseSuccessFragment() {
        CouponPaymentSuccessFragment paymentSuccessFragment = CouponPaymentSuccessFragment.newInstance(couponProduct);
        attacheFragment(paymentSuccessFragment, CouponPaymentSuccessFragment.CLASS_NAME);
    }

    private void showPurchaseFailFragment(String errorMessage) {
        CouponPaymentFailFragment paymentFailFragment = CouponPaymentFailFragment.newInstance(couponProduct, errorMessage);
        attacheFragment(paymentFailFragment, CouponPaymentFailFragment.CLASS_NAME);
    }

    private void attacheFragment(Fragment fragment, String tag) {
        CouponPaymentBaseFragment couponPaymentBaseFragment = (CouponPaymentBaseFragment) fragment;
        couponPaymentBaseFragment.setOnPaymentListener(this);
        getChildFragmentManager().beginTransaction().replace(R.id.paymentFrame, couponPaymentBaseFragment, tag).commit();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onPaymentListener = null;
    }

    @Override
    public void onPaymentComplete(boolean isSuccess) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPaymentComplete: isSuccess=" + isSuccess);
        }
        if (isSuccess) {
            showPurchaseSuccessFragment();
        }
    }

    @Override
    public void onPaymentError(String errorMessage) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPaymentError: errorMessage " + errorMessage);
        }

        if (onPaymentListener != null) {
            onPaymentListener.onFailure();
        }
        showPurchaseFailFragment(errorMessage);
    }

    @Override
    public void onPaymentCancel(String cancelPurchaseId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPaymentCancel");
        }

        if (onPaymentListener != null) {
            onPaymentListener.onCancel();
        }
    }

    @Override
    public void onFragmentDismiss() {
        dismiss();
    }

    @Override
    public void gotoHistoryListBySuccess() {
        if (onPaymentListener != null) {
            onPaymentListener.onSuccess();
        }
        dismiss();
    }

    public interface OnPaymentCompleteListener {
        void onSuccess();
        void onFailure();
        void onCancel();
    }
}
