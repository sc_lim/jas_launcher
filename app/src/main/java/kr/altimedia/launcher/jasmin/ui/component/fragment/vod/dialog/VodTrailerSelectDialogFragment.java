/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamType;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.presenter.VodTrailerSelectPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.listenter.OnVodViewChangeListener;
import kr.altimedia.launcher.jasmin.ui.view.adapter.IndicatedItemAdapter;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class VodTrailerSelectDialogFragment extends SafeDismissDialogFragment implements View.OnKeyListener {
    public static final String CLASS_NAME = VodTrailerSelectDialogFragment.class.getName();
    private static final String TAG = VodTrailerSelectDialogFragment.class.getSimpleName();

    private static final String TRAILER_LIST = "TRAILER_LIST";
    private static final String CONTENT = "CONTENT";

    private final int VISIBLE_COUNT = 3;

    private HorizontalGridView trailerGrid;
    private ImageIndicator leftIndicator;
    private ImageIndicator rightIndicator;

    private OnVodViewChangeListener onVodViewChangeListener;

    public VodTrailerSelectDialogFragment() {

    }

    public static VodTrailerSelectDialogFragment newInstance(Parcelable parcelable, ArrayList<StreamInfo> previewStreamInfos) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(TRAILER_LIST, previewStreamInfos);
        args.putParcelable(CONTENT, parcelable);
        VodTrailerSelectDialogFragment fragment = new VodTrailerSelectDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnVodViewChangeListener(OnVodViewChangeListener onVodViewChangeListener) {
        this.onVodViewChangeListener = onVodViewChangeListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_trailer_select, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        leftIndicator = view.findViewById(R.id.leftIndicator);
        rightIndicator = view.findViewById(R.id.rightIndicator);

        view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        initVodList(view);
    }

    private Parcelable currentObject = null;

    private void initVodList(View view) {
        ArrayList<StreamInfo> previewStreamInfoList = getArguments().getParcelableArrayList(TRAILER_LIST);
        currentObject = getArguments().getParcelable(CONTENT);
        trailerGrid = getGridView(view, previewStreamInfoList.size());
        leftIndicator.initIndicator(previewStreamInfoList.size(), VISIBLE_COUNT);
        rightIndicator.initIndicator(previewStreamInfoList.size(), VISIBLE_COUNT);

        VodTrailerSelectPresenter vodTrailerSelectPresenter = new VodTrailerSelectPresenter();
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(vodTrailerSelectPresenter);
        objectAdapter.addAll(0, previewStreamInfoList);
        VodTrailerItemBridgeAdapter vodTrailerItemBridgeAdapter = new VodTrailerItemBridgeAdapter(objectAdapter, trailerGrid, this);
        trailerGrid.setAdapter(vodTrailerItemBridgeAdapter);

        trailerGrid.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int focus = trailerGrid.getChildPosition(recyclerView.getFocusedChild());
                updateIndicator(focus);
            }
        });

        String title = currentObject instanceof Content ? ((Content) currentObject).getTitle() : ((SeriesContent) currentObject).getSeriesName();
        ((TextView) view.findViewById(R.id.title)).setText(title);
    }

    private HorizontalGridView getGridView(View view, int size) {
        HorizontalGridView trailerGrid = view.findViewById(R.id.trailer_grid);

        if (size == 0) {
            trailerGrid.setEnabled(false);
        }

        int gap = (int) view.getResources().getDimension(R.dimen.vod_trailer_presenter_spacing);
        int dimen = (int) view.getResources().getDimension(R.dimen.vod_trailer_presenter_scale_width) + gap;
        int width = size < VISIBLE_COUNT ? (size * dimen - gap) : (VISIBLE_COUNT * dimen - gap);
        ViewGroup.LayoutParams params = trailerGrid.getLayoutParams();
        params.width = width;
        trailerGrid.setLayoutParams(params);

        int spacing = getResources().getDimensionPixelOffset(R.dimen.vod_trailer_presenter_spacing);
        trailerGrid.setHorizontalSpacing(spacing);
        return trailerGrid;
    }

    private void updateIndicator(int focusedIndex) {
        leftIndicator.updateArrow(focusedIndex);
        rightIndicator.updateArrow(focusedIndex);
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        onVodViewChangeListener = null;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        VodTrailerItemBridgeAdapter adapter = (VodTrailerItemBridgeAdapter) trailerGrid.getAdapter();
        int lastIndex = adapter.getItemCount() - 1;
        int index = trailerGrid.getChildPosition(trailerGrid.getFocusedChild());

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
                StreamInfo streamInfo = (StreamInfo) adapter.getAdapter().get(index);
                Bundle bundle = new Bundle();
                bundle.putParcelable(VideoPlaybackActivity.KEY_MOVIE, currentObject);
                bundle.putInt(VideoPlaybackActivity.KEY_MOVIE_INDEX, index);
                bundle.putSerializable(VideoPlaybackActivity.KEY_TYPE, streamInfo.getStreamType() == StreamType.Preview ? VideoPlaybackActivity.PlayerType.Preview : VideoPlaybackActivity.PlayerType.Trailer);
                onVodViewChangeListener.requestTuneVod(bundle);

                dismiss();
                return true;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (index == 0) {
                    trailerGrid.scrollToPosition(lastIndex);
                    updateIndicator(lastIndex);
                    return true;
                }

                return false;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (index == lastIndex) {
                    trailerGrid.scrollToPosition(0);
                    updateIndicator(0);
                    return true;
                }

                return false;
        }

        return false;
    }

    private static class VodTrailerItemBridgeAdapter extends IndicatedItemAdapter {
        private final float ALIGNMENT = 83.2f;

        private HorizontalGridView mHorizontalGridView;
        private View.OnKeyListener onKeyListener;

        public VodTrailerItemBridgeAdapter(ObjectAdapter adapter, HorizontalGridView mHorizontalGridView, View.OnKeyListener onKeyListener) {
            super(adapter);
            this.mHorizontalGridView = mHorizontalGridView;
            this.onKeyListener = onKeyListener;

            initAlignment();
        }

        private void initAlignment() {
            if (mHorizontalGridView == null) {
                return;
            }

            mHorizontalGridView.setWindowAlignmentOffsetPercent(ALIGNMENT);
        }

        @Override
        protected void onBind(ViewHolder viewHolder) {
            super.onBind(viewHolder);

            View view = viewHolder.getViewHolder().view;
            View targetView = view.findViewById(R.id.layout);
            targetView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    int animationId = hasFocus ? R.animator.scale_up_select_trailer : R.animator.scale_down_50;
                    Animator animator = AnimatorInflater.loadAnimator(v.getContext(), animationId);

                    animator.setTarget(v);
                    animator.start();
                }
            });

            targetView.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    if (onKeyListener != null) {
                        return onKeyListener.onKey(v, keyCode, event);
                    }

                    return false;
                }
            });
        }

        @Override
        protected void onUnbind(ViewHolder viewHolder) {
            super.onUnbind(viewHolder);

            View view = viewHolder.getViewHolder().view;
            View targetView = view.findViewById(R.id.layout);
            targetView.setOnFocusChangeListener(null);
            targetView.setOnKeyListener(null);
        }
    }
}
