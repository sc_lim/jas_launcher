/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.system.manager;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;

import java.util.HashMap;

import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.external.proxy.multiview.LauncherProxy;
import kr.altimedia.launcher.jasmin.external.proxy.multiview.data.ChannelInfo;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.module.DefaultBackendKnobsFlags;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ProgressBarWindow;

/**
 * Created by mc.kim on 17,07,2020
 */
public class ExternalApplicationManager {
    public static final String APP_TIS = "kr.altimedia.jasmine.tis";
    public static final String APP_4CHANNEL = "com.kt.amuz4ch";
    public static final String APP_HBO = "com.hbo.asia.androidtv";
    public static final String APP_YOUTUBE = "com.google.android.youtube.tv";
    public static final String APP_MONOMAX = "com.doonung.dtv";
    public static final String KEY_TARGET_ACTIVITY = "activitypath";
    private static final ExternalApplicationManager ourInstance = new ExternalApplicationManager();
    private final String TAG = ExternalApplicationManager.class.getSimpleName();
    private final HashMap<String, Bundle> appExtraBundle = new HashMap<>();

    private ExternalApplicationManager() {
    }

    public static ExternalApplicationManager getInstance() {
        return ourInstance;
    }

    public Bundle getBundle(String packageId) {
        if (packageId != null && packageId.equals(APP_4CHANNEL)) {
            return generated4ChannelBundle();
        }

        return null;
    }

    private Bundle generated4ChannelBundle() {
        Bundle bundle = new Bundle();
        ChannelInfo[] channelInfos = JasChannelManager.getInstance().getChannelInfo();

        if (Log.INCLUDE) {
            int index = 0;
            for (ChannelInfo channel : channelInfos) {
                Log.d(TAG, "Channel[" + (index++) + "]=" + channel);
            }
        }

        bundle.putString(LauncherProxy.INTENT_NAME_SAID, AccountManager.getInstance().getSaId());
        bundle.putString(LauncherProxy.INTENT_NAME_LA_URL, AccountManager.getInstance().getLicenseUrl());
        bundle.putString(LauncherProxy.INTENT_NAME_PROFILE_ID, AccountManager.getInstance().getProfileId());
        bundle.putString(LauncherProxy.INTENT_NAME_LOGIN_TOKEN, AccountManager.getInstance().getLoginToken());
        bundle.putInt(LauncherProxy.INTENT_NAME_RATING, AccountManager.getInstance().getCurrentRating());
        bundle.putParcelableArray(LauncherProxy.INTENT_NAME_CHANNEL, channelInfos);

        return bundle;
    }

    public void launchApp(Context context, String action) {
        this.launchApp(context, action, false, getBundle(action));
    }

    public void launchApp(Context context, String action, boolean checkAppStore) {
        this.launchApp(context, action, checkAppStore, getBundle(action));
    }

    public void launchApp(Context context, String action, boolean checkAppStore, Bundle bundleData) {
        DefaultBackendKnobsFlags mBackendKnobsFlags = (DefaultBackendKnobsFlags) TvSingletons.getSingletons(context).getBackendKnobs();
        boolean isSupportedAutoLogin = AccountManager.getInstance().isSupportedAutoLogin(action);
        if (Log.INCLUDE) {
            Log.d(TAG, "launchApp=" + action
                    + ", checkAppStore=" + checkAppStore
                    + ", bundleData=" + bundleData + ", isSupportedAutoLogin : " + isSupportedAutoLogin);
        }

        if (mBackendKnobsFlags.isLaunchAppKeBlock()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "isLaunchAppKeBlock so return");
            }
            return;
        }

        AccountManager accountManager = AccountManager.getInstance();
        if (isSupportedAutoLogin && accountManager.isAuthenticatedUser()) {
            mBackendKnobsFlags.setLaunchAppKeBlock(true);
            UserDataManager userDataManager = new UserDataManager();
            ProgressBarWindow mProgressBarWindow = new ProgressBarWindow(context);
            userDataManager.getIntentData(accountManager.getAccountId(), action, NetworkUtil.getMacAddress(), new MbsDataProvider<String, Bundle>() {
                @Override
                public void needLoading(boolean loading) {
                    mBackendKnobsFlags.setLaunchAppKeBlock(loading);
                    if (loading) {
                        mProgressBarWindow.show();
                    } else {
                        mProgressBarWindow.hide(true);
                    }
                }

                @Override
                public void onFailed(int reason) {
                    requestLaunchApp(context, action, checkAppStore, bundleData);
                }

                @Override
                public void onSuccess(String id, Bundle result) {
                    requestLaunchApp(context, action, checkAppStore, result);
                }

                @Override
                public void onError(String errorCode, String message) {
                    requestLaunchApp(context, action, checkAppStore, bundleData);
                }

                @Override
                public Context getTaskContext() {
                    return context;
                }
            });
        } else {
            requestLaunchApp(context, action, checkAppStore, bundleData);
        }

    }


    private void requestLaunchApp(Context context, String action, boolean checkAppStore, Bundle bundleData) {
        if (Log.INCLUDE) {
            Log.d(TAG, "launchApp=" + action
                    + ", checkAppStore=" + checkAppStore
                    + ", bundleData=" + bundleData);
        }


        Intent intent = context.getPackageManager().getLeanbackLaunchIntentForPackage(action);
        if (intent == null) {
            intent = context.getPackageManager().getLaunchIntentForPackage(action);
        }

        if (intent == null) {
            if (checkAppStore) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "launchApp called but not installed so go to appStore");
                }
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=" + action));
            } else {
                if (Log.INCLUDE) {
                    Log.d(TAG, "launchApp called but not installed so return");
                }
                return;
            }
        } else {
            if (bundleData != null && bundleData.containsKey(KEY_TARGET_ACTIVITY)) {
                intent.setClassName(action, bundleData.getString(KEY_TARGET_ACTIVITY));
                bundleData.remove(KEY_TARGET_ACTIVITY);
            }
        }
        if (bundleData != null) {
            intent.putExtras(bundleData);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
