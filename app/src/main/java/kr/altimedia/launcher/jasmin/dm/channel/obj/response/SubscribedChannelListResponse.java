/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;

public class SubscribedChannelListResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private SubscribedChannelListResult subscribedChannelListResult;

    protected SubscribedChannelListResponse(Parcel in) {
        super(in);
        subscribedChannelListResult = in.readTypedObject(SubscribedChannelListResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(subscribedChannelListResult, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SubscribedChannelListResponse> CREATOR = new Creator<SubscribedChannelListResponse>() {
        @Override
        public SubscribedChannelListResponse createFromParcel(Parcel in) {
            return new SubscribedChannelListResponse(in);
        }

        @Override
        public SubscribedChannelListResponse[] newArray(int size) {
            return new SubscribedChannelListResponse[size];
        }
    };

    public List<String> getSubscribedChannelListResult() {
        return subscribedChannelListResult.channelIdList;
    }

    @Override
    public String toString() {
        return "SubscribedChannelListResponse{" +
                "SubscribedChannelListResult=" + subscribedChannelListResult +
                '}';
    }

    private static class SubscribedChannelListResult implements Parcelable {
        @Expose
        @SerializedName("channelId")
        private List<String> channelIdList;

        protected SubscribedChannelListResult(Parcel in) {
            channelIdList = in.readArrayList(String.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(channelIdList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<SubscribedChannelListResult> CREATOR = new Creator<SubscribedChannelListResult>() {
            @Override
            public SubscribedChannelListResult createFromParcel(Parcel in) {
                return new SubscribedChannelListResult(in);
            }

            @Override
            public SubscribedChannelListResult[] newArray(int size) {
                return new SubscribedChannelListResult[size];
            }
        };

        @Override
        public String toString() {
            return "SubscribedChannelListResult{" +
                    "channelIdList=" + channelIdList +
                    '}';
        }
    }
}
