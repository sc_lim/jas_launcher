/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import com.altimedia.util.Log;

public enum DiscountType {
    RATE("20101", "DiscountRate"), AMOUNT("20102", "DiscountAmount"), NOT_APPLICABLE("20100", "NotApplicable");

    private final String type;
    private final String mName;

    DiscountType(String type, String mName) {
        this.type = type;
        this.mName = mName;
    }

    public static DiscountType findByName(String name) {
//        Log.d("DiscountType", "findByName: name="+name);
        DiscountType[] types = values();
        for (DiscountType type : types) {
            if (type.type.equalsIgnoreCase(name)) {
                return type;
            }
        }

        return NOT_APPLICABLE;
    }
}
