/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class SpinnerTextView extends RelativeLayout {
    private RelativeLayout layout;
    private TextView textView;
    private ArrayList<SpinnerPresenterItem> options;

    private PopupWindow popup;
    private SpinnerPresenter presenter;

    private boolean isOkPressed = false;

    public SpinnerTextView(Context context) {
        super(context);
        initView();
    }

    public SpinnerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);
    }

    public SpinnerTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        getAttrs(attrs, defStyleAttr);
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.common_spinner_textview, this, false);
        addView(v);

        layout = v.findViewById(R.id.spinnerLayout);
        textView = v.findViewById(R.id.spinnerTextView);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SpinnerTextViewTheme);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SpinnerTextViewTheme, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        int icon = typedArray.getResourceId(R.styleable.SpinnerTextViewTheme_spinner_icon, R.drawable.drop_down);
        int fontFamily = typedArray.getResourceId(R.styleable.SpinnerTextViewTheme_spinner_font_family, R.font.font_prompt_medium);
        int textSize = typedArray.getResourceId(R.styleable.SpinnerTextViewTheme_spinner_text_size, R.dimen.spinner_text_view_text_size);
        int textColor = typedArray.getResourceId(R.styleable.SpinnerTextViewTheme_spinner_text_color, R.drawable.selector_check_textview_text);
        int bgColor = typedArray.getResourceId(R.styleable.SpinnerTextViewTheme_spinner_bg_drawable, R.drawable.selector_button_focus_1);

        textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, icon, 0);
        textView.setTypeface(getResources().getFont(fontFamily), Typeface.NORMAL);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(textSize));
        textView.setTextColor(getResources().getColorStateList(textColor, null));
        layout.setBackgroundResource(bgColor);

        initPopup();

        typedArray.recycle();
    }

    //it should be set.
    public void setSpinner(SpinnerPresenter present, SpinnerOptionButton optionButton) {
        this.presenter = present;

        Context context = getContext();
        SpinnerOptionType[] options = optionButton.getOptions();
        ArrayList<SpinnerPresenterItem> optionLst = new ArrayList<>();
        for (int i = 0; i < options.length; i++) {
            String title = context.getString(options[i].getTitle());
            optionLst.add(new SpinnerPresenterItem(optionButton.getType(), options[i].getType(), title));
        }

        this.options = optionLst;

        SpinnerOptionType currentOption = optionButton.getCurrentOption();
        String currentTitle = context.getString(currentOption.getTitle());
        textView.setText(currentTitle);
    }

    private void initPopup() {
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.setFocusable(false);
                layout.setFocusableInTouchMode(false);

                LayoutInflater inflater = (LayoutInflater) v.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout spinnerList = (LinearLayout) inflater.inflate(R.layout.view_spinner_list, null);

                TextView spinnerCurrentText = spinnerList.findViewById(R.id.spinner_current_title);
                spinnerCurrentText.setText(textView.getText());

                VerticalGridView spinnerGridView = spinnerList.findViewById(R.id.spinner_grid);

                popup = new PopupWindow(v, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                popup.setContentView(spinnerList);

                popup.setTouchable(true);
                popup.setFocusable(true);
                popup.setAnimationStyle(0);
                int rowHeight = (int) getResources().getDimension(R.dimen.spinner_text_view_height);
                popup.showAsDropDown(v, 0, (-1 * rowHeight));

                isOkPressed = false;
                popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        layout.setFocusable(true);
                        layout.setFocusableInTouchMode(true);
                        if (!isOkPressed) {
                            layout.requestFocus();
                        }
                    }
                });

                initPopupPresenter(spinnerGridView);
            }
        });
    }

    private void initPopupPresenter(VerticalGridView gridView) {
        gridView.setNumColumns(1);
        gridView.setClipToPadding(false);
        int rowHeight = (int) getResources().getDimension(R.dimen.spinner_text_view_height);
        ViewGroup.LayoutParams lp = gridView.getLayoutParams();
        lp.height = rowHeight * options.size();
        gridView.setLayoutParams(lp);

        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(presenter);
        objectAdapter.addAll(0, options);

        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(objectAdapter);
        gridView.setAdapter(itemBridgeAdapter);

        SpinnerLineDividerDecoration itemDecoration = new SpinnerLineDividerDecoration(getContext());
        itemDecoration.setDrawable(R.drawable.line_divider);
        gridView.addItemDecoration(itemDecoration);
    }

    public void onClickSpinnerItem(String value) {
        isOkPressed = true;
        textView.setText(value);

        if (popup != null && popup.isShowing()) {
            popup.dismiss();
        }
    }

    public interface SpinnerOptionType {
        int getType();

        int getTitle();

        String getKey();
    }

    public interface SpinnerOptionButton {
        int getType();

        int getTitle();

        SpinnerOptionType[] getOptions();

        SpinnerOptionType getCurrentOption();
    }

    protected class SpinnerLineDividerDecoration extends RecyclerView.ItemDecoration {
        private Drawable divider;
        private Context mContext;

        public SpinnerLineDividerDecoration(Context context) {
            mContext = context;
        }

        public void setDrawable(int resourceId) {
            divider = mContext.getResources().getDrawable(resourceId);
        }

        @Override
        public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth();

            Paint paint = new Paint();
            paint.setColor(getResources().getColor(R.color.color_33222535, null));

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount - 1; i++) {
                View child = parent.getChildAt(i);

                int top = child.getBottom();
                int bottom = top + (int) getResources().getDimension(R.dimen.spinner_text_view_divider_height);

                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
        }
    }

    public interface OnSpinnerClickListener {
        void onClick(SpinnerPresenterItem item);
    }

    public abstract static class SpinnerPresenter extends Presenter {
        private SpinnerTextView.OnSpinnerClickListener onSpinnerClickListener;

        public void setOnSpinnerClickListener(OnSpinnerClickListener onSpinnerClickListener) {
            this.onSpinnerClickListener = onSpinnerClickListener;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            return onCreateSpinnerViewHolder(parent);
        }

        @Override
        public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
            ViewHolder vh = (ViewHolder) viewHolder;
            vh.setOnSpinnerClickListener(onSpinnerClickListener);

            onBindSpinnerViewHolder(viewHolder, item);
        }

        @Override
        public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
            ViewHolder vh = (ViewHolder) viewHolder;
            vh.setOnSpinnerClickListener(null);

            onUnbindSpinnerViewHolder(viewHolder);
        }

        protected abstract ViewHolder onCreateSpinnerViewHolder(ViewGroup parent);

        protected abstract void onBindSpinnerViewHolder(Presenter.ViewHolder viewHolder, Object item);

        protected abstract void onUnbindSpinnerViewHolder(Presenter.ViewHolder viewHolder);

        public class ViewHolder extends Presenter.ViewHolder {
            private OnSpinnerClickListener onSpinnerClickListener;

            public ViewHolder(View view) {
                super(view);
            }

            public void setOnSpinnerClickListener(OnSpinnerClickListener onSpinnerClickListener) {
                this.onSpinnerClickListener = onSpinnerClickListener;
            }

            public OnSpinnerClickListener getOnSpinnerClickListener() {
                return onSpinnerClickListener;
            }
        }
    }

    public static class SpinnerPresenterItem {
        private int parentType;
        private int type;
        private String title;

        public SpinnerPresenterItem(int parentType, int type, String title) {
            this.parentType = parentType;
            this.type = type;
            this.title = title;
        }

        public int getParentType() {
            return parentType;
        }

        public int getType() {
            return type;
        }

        public String getTitle() {
            return title;
        }
    }
}
