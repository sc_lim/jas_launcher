/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.SinglePresenterSelector;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.CouponSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentWrapper;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseDiscountType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.row.DiscountRow;
import kr.altimedia.launcher.jasmin.ui.util.task.UserTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class DiscountPresenter extends Presenter {
    private static final String CLASS_NAME = DiscountPresenter.class.getName();
    private static final String TAG = DiscountPresenter.class.getSimpleName();

    private boolean isFocusableDiscountRow;
    private OnDiscountListener onDiscountListener;

    public void setFocusableDiscountRow(boolean isFocusableDiscountRow) {
        this.isFocusableDiscountRow = isFocusableDiscountRow;
    }

    public void setOnDiscountListener(OnDiscountListener onDiscountListener) {
        this.onDiscountListener = onDiscountListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_discount, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        DiscountRow row = (DiscountRow) item;
        ArrayObjectAdapter arrayObjectAdapter = (ArrayObjectAdapter) row.getAdapter();
        Product product = row.getProduct();

        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setOnDiscountListener(onDiscountListener);
        vh.setDiscountTypes(isFocusableDiscountRow, arrayObjectAdapter);
        vh.setData(product);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setOnDiscountListener(null);
        onDiscountListener = null;
    }

    public interface OnDiscountListener {
        TvOverlayManager getTvOverlayManager();

        void onChangeDiscountValue(PurchaseDiscountType purchaseDiscount, PaymentWrapper paymentWrapper);
    }

    public static class ViewHolder extends Presenter.ViewHolder implements DiscountCheckBoxPresenter.OnClickDiscountCheckBox, CouponSelectDialogFragment.OnSelectCouponDiscountListener {
        private TextView totalPayment;
        private HorizontalGridView discountView;

        private PaymentWrapper paymentWrapper;

        private UserTaskManager userTaskManager = UserTaskManager.getInstance();

        private OnDiscountListener onDiscountListener;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            totalPayment = view.findViewById(R.id.total_payment);
            initDiscountGridView(view);
        }

        public void setData(Product product) {
            paymentWrapper = new PaymentWrapper(product.getPrice());
            updateTotalPayment();
        }

        private void updateTotalPayment() {
            float total = paymentWrapper.getAccountPayment();
            totalPayment.setText(StringUtil.getFormattedPrice(total));
        }

        public void updateTotalPayment(float total, boolean isIncludeVat) {
            if (Log.INCLUDE) {
                Log.d(TAG, "updateTotalPayment, total : " + total + ", isIncludeVat : " + isIncludeVat);
            }

            totalPayment.setText(StringUtil.getFormattedPrice(total));
        }

        public void setOnDiscountListener(OnDiscountListener onDiscountListener) {
            this.onDiscountListener = onDiscountListener;
        }

        public void setDiscountTypes(boolean isFocusableDiscountRow, ArrayObjectAdapter arrayObjectAdapter) {
            if (isFocusableDiscountRow) {
                if (!PurchaseDiscountType.COUPON_DISCOUNT.isEnabled() && !PurchaseDiscountType.COUPON_POINT.isEnabled() && !PurchaseDiscountType.MEMBERSHIP.isEnabled()) {
                    isFocusableDiscountRow = false;
                }
            }

            discountView.setFocusable(isFocusableDiscountRow);
            discountView.setFocusableInTouchMode(isFocusableDiscountRow);

            DiscountCheckBoxPresenter discountCheckBoxPresenter = new DiscountCheckBoxPresenter();
            discountCheckBoxPresenter.setOnClickDiscountCheckBox(this);
            arrayObjectAdapter.setPresenterSelector(new SinglePresenterSelector(discountCheckBoxPresenter));

            ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(arrayObjectAdapter);
            discountView.setAdapter(itemBridgeAdapter);
        }

        private void initDiscountGridView(View view) {
            int discountCouponSize = userTaskManager.getDiscountCouponSize();
            int pointCoupon = userTaskManager.getPointCoupon();
            int membershipPoint = userTaskManager.getMembership() != null ? userTaskManager.getMembership().getMembershipPoint() : 0;

            PurchaseDiscountType.COUPON_DISCOUNT.setChecked(false);
            PurchaseDiscountType.COUPON_DISCOUNT.setValue(discountCouponSize);
            PurchaseDiscountType.COUPON_DISCOUNT.setUnit(R.string.ea);

            PurchaseDiscountType.COUPON_POINT.setChecked(false);
            PurchaseDiscountType.COUPON_POINT.setValue(pointCoupon);
            PurchaseDiscountType.COUPON_POINT.setUnit(R.string.thb);

            PurchaseDiscountType.MEMBERSHIP.setChecked(false);
            PurchaseDiscountType.MEMBERSHIP.setValue(membershipPoint);
            PurchaseDiscountType.MEMBERSHIP.setUnit(R.string.p);

            DiscountCheckBoxPresenter discountCheckBoxPresenter = new DiscountCheckBoxPresenter();
            discountCheckBoxPresenter.setOnClickDiscountCheckBox(this);

            int gap = (int) view.getContext().getResources().getDimension(R.dimen.discount_button_spacing);
            discountView = view.findViewById(R.id.discount_view);
            discountView.setHorizontalSpacing(gap);
        }

        private void updateDiscountView(PurchaseDiscountType button) {
            ArrayObjectAdapter arrayObjectAdapter = (ArrayObjectAdapter) ((ItemBridgeAdapter) discountView.getAdapter()).getAdapter();
            arrayObjectAdapter.replace(button.getType(), button);
        }

        public int getSelectedPosition() {
            return discountView.getSelectedPosition();
        }

        @Override
        public void onClick(DiscountCheckBoxPresenter.DiscountButton discountButton) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onClick, discountButton : " + discountButton);
            }

            if (discountButton == PurchaseDiscountType.COUPON_DISCOUNT) {
                if (PurchaseDiscountType.COUPON_DISCOUNT.isChecked()) {
                    onSelectCouponDiscount(null);
                    return;
                }

                ArrayList<DiscountCoupon> couponList = userTaskManager.getDiscountCoupons();
                CouponSelectDialogFragment couponSelectDialogFragment = CouponSelectDialogFragment.newInstance(paymentWrapper.getPrice(), couponList);
                couponSelectDialogFragment.setOnSelectCouponDiscountListener(this);

                TvOverlayManager tvOverlayManager = onDiscountListener.getTvOverlayManager();
                tvOverlayManager.showDialogFragment(couponSelectDialogFragment);
            } else if (discountButton == PurchaseDiscountType.COUPON_POINT) {
                onSelectCouponPoint();
                checkTotalPaymentZero();
            } else if (discountButton == PurchaseDiscountType.MEMBERSHIP) {
                onSelectMembership();
                checkTotalPaymentZero();
            }
        }

        @Override
        public void onSelectCouponDiscount(DiscountCoupon coupon) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onSelectCouponDiscount, coupon : " + coupon);
            }

            if (coupon != null) {
                DiscountCoupon couponDiscount = paymentWrapper.getDiscountCoupon();

                //when same coupon reselect, just hide dialog
                if (couponDiscount != null && coupon.getId() == couponDiscount.getId()) {
                    if (onDiscountListener != null) {
                        onDiscountListener.onChangeDiscountValue(PurchaseDiscountType.COUPON_DISCOUNT, paymentWrapper);
                    }

                    return;
                }

                if (isResetDiscount()) {
                    toastSelectDiscountAgain();
                }
            }

            paymentWrapper.setDiscountCoupon(coupon);
            if (coupon == null) {
                PurchaseDiscountType.COUPON_DISCOUNT.setChecked(false);
                PurchaseDiscountType.COUPON_DISCOUNT.setValue(userTaskManager.getDiscountCoupons().size());
                PurchaseDiscountType.COUPON_DISCOUNT.setUnit(R.string.ea);
            } else {
                PurchaseDiscountType.COUPON_DISCOUNT.setChecked(true);
                PurchaseDiscountType.COUPON_DISCOUNT.setValue(paymentWrapper.getCouponDiscountValue());
                PurchaseDiscountType.COUPON_DISCOUNT.setUnit(R.string.thb);
            }

            updateTotalDiscount(PurchaseDiscountType.COUPON_DISCOUNT);
            updateDiscountView(PurchaseDiscountType.COUPON_DISCOUNT);

            checkTotalPaymentZero();
        }

        private void onSelectCouponPoint() {
            float usedPoint = paymentWrapper.getCouponPoint();
            if (usedPoint > 0) {
                resetCouponPoint();
                return;
            }

            int totalPayment = paymentWrapper.getAccountPayment();
            int couponPoint = userTaskManager.getPointCoupon();
            int availablePoint = totalPayment < couponPoint ? totalPayment : couponPoint;

            if (Log.INCLUDE) {
                Log.d(TAG, "onSelectCouponPoint, " +
                        "totalPayment : " + totalPayment + ", couponPoint : " + couponPoint + ", availablePoint : " + availablePoint);
            }

            paymentWrapper.setCouponPoint(availablePoint);
            PurchaseDiscountType.COUPON_POINT.setChecked(true);
            PurchaseDiscountType.COUPON_POINT.setValue(availablePoint);

            updateDiscountView(PurchaseDiscountType.COUPON_POINT);
            updateTotalDiscount(PurchaseDiscountType.COUPON_POINT);
        }

        private void onSelectMembership() {
            if (PurchaseDiscountType.MEMBERSHIP.isChecked()) {
                resetMembership();
                return;
            }

            int price = (int) paymentWrapper.getPrice();
            int totalPayment = paymentWrapper.getAccountPayment();
            float membershipLimitPoint = (float) (price * PurchaseDiscountType.MEMBERSHIP.getLimitRate()) / 100;
            int membershipLimitPointCeil = (int) Math.ceil(membershipLimitPoint);
            int membershipPoint = userTaskManager.getMembership().getMembershipPoint() / PurchaseDiscountType.MEMBERSHIP.getRatio();
            int availableTHB = totalPayment > membershipLimitPointCeil ? membershipLimitPointCeil : totalPayment;
            if (availableTHB > membershipPoint) {
                availableTHB = membershipPoint;
            }
            int usagePoint = availableTHB * PurchaseDiscountType.MEMBERSHIP.getRatio();

            if (Log.INCLUDE) {
                Log.d(TAG, "onSelectMembership"
                        + ", price : " + price
                        + ", totalPayment : " + totalPayment
                        + ", membershipLimitRate : " + PurchaseDiscountType.MEMBERSHIP.getLimitRate()
                        + ", membershipLimitPoint : " + membershipLimitPoint
                        + ", membershipLimitPointCeil : " + membershipLimitPointCeil
                        + ", membershipPoint : " + membershipPoint
                        + ", availableTHB : " + availableTHB
                        + ", usagePoint : " + usagePoint);
            }

            paymentWrapper.setMembershipPoint(availableTHB);
            PurchaseDiscountType.MEMBERSHIP.setChecked(true);
            PurchaseDiscountType.MEMBERSHIP.setValue(usagePoint);

            updateDiscountView(PurchaseDiscountType.MEMBERSHIP);
            updateTotalDiscount(PurchaseDiscountType.MEMBERSHIP);
        }

        private void updateTotalDiscount(PurchaseDiscountType purchaseDiscount) {
            updateTotalPayment();

            if (onDiscountListener != null) {
                onDiscountListener.onChangeDiscountValue(purchaseDiscount, paymentWrapper);
            }
        }

        private void checkTotalPaymentZero() {
            float totalPayment = paymentWrapper.getAccountPayment();
            PurchaseDiscountType[] discounts = PurchaseDiscountType.values();
            for (PurchaseDiscountType discount : discounts) {
                //total zero, unChecked discount disable
                if (totalPayment <= 0) {
                    if (!discount.isChecked()) {
                        discount.setEnabled(false);
                        updateDiscountView(discount);
                    }
                } else {
                    //total zero -> zero X , disable rollback
                    if (!discount.isEnabled()) {
                        if (discount.getValue() > 0) {
                            discount.setEnabled(true);
                            updateDiscountView(discount);
                        }
                    }
                }
            }
        }

        private boolean isResetDiscount() {
            boolean resetDiscount = false;

            if (PurchaseDiscountType.COUPON_POINT.isChecked()) {
                resetDiscount = true;
                resetCouponPoint();
            }

            if (PurchaseDiscountType.MEMBERSHIP.isChecked()) {
                resetDiscount = true;
                resetMembership();
            }

            if (resetDiscount) {
                toastSelectDiscountAgain();
            }

            return resetDiscount;
        }

        private void resetCouponPoint() {
            paymentWrapper.setCouponPoint(0);
            PurchaseDiscountType.COUPON_POINT.setChecked(false);
            PurchaseDiscountType.COUPON_POINT.setValue(userTaskManager.getPointCoupon());

            updateDiscountView(PurchaseDiscountType.COUPON_POINT);
            updateTotalDiscount(PurchaseDiscountType.COUPON_POINT);
        }

        private void resetMembership() {
            if (Log.INCLUDE) {
                Log.d(TAG, "resetMembership");
            }

            paymentWrapper.setMembershipPoint(0);
            PurchaseDiscountType.MEMBERSHIP.setChecked(false);
            PurchaseDiscountType.MEMBERSHIP.setValue(userTaskManager.getMembership().getMembershipPoint());

            updateDiscountView(PurchaseDiscountType.MEMBERSHIP);
            updateTotalDiscount(PurchaseDiscountType.MEMBERSHIP);
        }

        private void toastSelectDiscountAgain() {
            JasminToast.makeToast(view.getContext(), R.string.purchase_select_discount_again);
        }
    }
}