/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.favorite;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.BrowseFrameLayout;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.ChannelCheckButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.TextButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.ChannelCheckboxPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.TextButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.ChannelCheckButtonBridgeAdapter.NUMBER_ROWS;

public class SettingProfileChannelFavoriteSetupDialogFragment extends DialogFragment implements SettingBaseButtonItemBridgeAdapter.OnClickButton {
    public static final String CLASS_NAME = SettingProfileChannelFavoriteSetupDialogFragment.class.getName();
    private static final String TAG = SettingProfileChannelFavoriteSetupDialogFragment.class.getSimpleName();

    private static final String KEY_FAVORITE_LIST = "FAVORITE_LIST";

    private ArrayList<ChannelItem> channelList = new ArrayList<>();
    private ArrayList<ChannelItem> favoriteList = new ArrayList<>();

    private TextView selectedCountView;
    private PagingVerticalGridView favoriteGridView;
    private VerticalGridView buttonsGridView;

    private ArrayObjectAdapter listObjectAdapter;
    private ArrayObjectAdapter buttonObjectAdapter;
    private TextButtonBridgeAdapter textButtonBridgeAdapter;
    private ChannelCheckButtonBridgeAdapter channelCheckButtonBridgeAdapter;

    private OnFavoriteChannelChangeResultCallback onFavoriteChannelChangeResultCallback;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private final UserDataManager mUserDataManager = new UserDataManager();
    private MbsDataTask favoriteTask;

    private SettingProfileChannelFavoriteSetupDialogFragment() {

    }

    public static SettingProfileChannelFavoriteSetupDialogFragment newInstance(ArrayList<String> favoriteList) {
        Bundle args = new Bundle();
        args.putStringArray(KEY_FAVORITE_LIST, favoriteList.toArray(new String[]{}));

        SettingProfileChannelFavoriteSetupDialogFragment fragment = new SettingProfileChannelFavoriteSetupDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnFavoriteChannelChangeResultCallback(OnFavoriteChannelChangeResultCallback onFavoriteChannelChangeResultCallback) {
        this.onFavoriteChannelChangeResultCallback = onFavoriteChannelChangeResultCallback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            LauncherActivity launcherActivity = (LauncherActivity) context;
            ArrayList<Channel> originList = (ArrayList<Channel>) launcherActivity.getBrowsableChannelList();

            for (Channel channel : originList) {
                ChannelItem channelItem = new ChannelItem(channel, false);
                channelList.add(channelItem);
            }

            initData();
        }
    }

    private void initData() {
        setFavoriteList(getOriginalList());
    }

    private ArrayList<String> getOriginalList() {
        String[] blockedList = getArguments().getStringArray(KEY_FAVORITE_LIST);
        ArrayList<String> list = new ArrayList<>();
        if (blockedList.length > 0) {
            Collections.addAll(list, blockedList);
        }

        return list;
    }

    public void setFavoriteList(ArrayList<String> favoriteChannels) {
        if (favoriteChannels != null && favoriteChannels.size() > 0) {
            for (String channelNumber : favoriteChannels) {
                for (ChannelItem channelItem : channelList) {
                    String serviceId = channelItem.getChannel().getServiceId();
                    if (channelNumber.equals(serviceId)) {
                        channelItem.setChecked(true);
                        favoriteList.add(channelItem);
                        continue;
                    }
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_channel_favorite_setup, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        initProgressbar(view);
        initCount(view);
        initChannelGridView(view);
        initButtons(view);
        initLayout(view);
    }

    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void showProgress() {
        mProgressBarManager.show();
    }

    private void hideProgress() {
        if (mProgressBarManager != null) {
            mProgressBarManager.hide();
        }
    }

    private void initCount(View view) {
        selectedCountView = view.findViewById(R.id.selected_count);
        selectedCountView.setText(getFormattedNumber(favoriteList.size()));

        TextView allCountView = view.findViewById(R.id.channels_count);
        allCountView.setText(getFormattedNumber(channelList.size()));
    }

    private String getFormattedNumber(int number) {
        if (number < 10) {
            return "00" + number;
        } else if (number < 100) {
            return "0" + number;
        } else {
            return String.valueOf(number);
        }
    }

    private void initChannelGridView(View view) {
        favoriteGridView = view.findViewById(R.id.channel_gridView);
        favoriteGridView.setNumColumns(ChannelCheckButtonBridgeAdapter.NUMBER_COLUMNS);

        ChannelCheckboxPresenter channelCheckboxPresenter = new ChannelCheckboxPresenter();
        listObjectAdapter = new ArrayObjectAdapter(channelCheckboxPresenter);
        listObjectAdapter.addAll(0, channelList);

        channelCheckButtonBridgeAdapter = new ChannelCheckButtonBridgeAdapter(listObjectAdapter, favoriteGridView);
        channelCheckButtonBridgeAdapter.setListener(this);
        favoriteGridView.setAdapter(channelCheckButtonBridgeAdapter);

        ThumbScrollbar scrollbar = view.findViewById(R.id.scrollbar);
        scrollbar.setList(getRowIndex(channelList.size()), NUMBER_ROWS);
        scrollbar.setFocusable(false);
        scrollbar.setFocusableInTouchMode(false);

        favoriteGridView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int focus = favoriteGridView.getChildPosition(recyclerView.getFocusedChild());
                scrollbar.moveFocus(getRowIndex(focus + 1) - 1);
            }
        });

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        favoriteGridView.setVerticalSpacing(spacing);
    }

    private int getRowIndex(int index) {
        int row = index / ChannelCheckButtonBridgeAdapter.NUMBER_COLUMNS;
        if (index % ChannelCheckButtonBridgeAdapter.NUMBER_COLUMNS > 0) {
            row += 1;
        }

        return row;
    }

    private void initLayout(View view) {
        BrowseFrameLayout browseFrameLayout = view.findViewById(R.id.browse_frame);
        browseFrameLayout.setOnFocusSearchListener(new BrowseFrameLayout.OnFocusSearchListener() {
            @Override
            public View onFocusSearch(View focused, int direction) {
                if (direction == View.FOCUS_RIGHT && focused.getParent() == favoriteGridView) {
                    return buttonsGridView.getChildAt(FavoriteButton.SAVE.getType());
                }

                return null;
            }
        });
    }


    private void initButtons(View view) {
        buttonsGridView = view.findViewById(R.id.buttons_gridView);

        boolean isEnabled = favoriteList.size() > 0;
        FavoriteButton buttonReset = FavoriteButton.RESET;
        buttonReset.setEnabled(isEnabled);

        ArrayList<FavoriteButton> list = new ArrayList<>(Arrays.asList(buttonReset, FavoriteButton.SAVE, FavoriteButton.CANCEL));
        TextButtonPresenter buttonPresenter = new TextButtonPresenter();
        buttonObjectAdapter = new ArrayObjectAdapter(buttonPresenter);
        buttonObjectAdapter.addAll(0, list);

        textButtonBridgeAdapter = new TextButtonBridgeAdapter(buttonObjectAdapter);
        textButtonBridgeAdapter.setListener(this);
        buttonsGridView.setAdapter(textButtonBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_basic_button_vertical_spacing);
        buttonsGridView.setVerticalSpacing(spacing);
    }

    @Override
    public boolean onClickButton(Object item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        if (item instanceof FavoriteButton) {
            return OnClickButtonItem((FavoriteButton) item);
        } else if (item instanceof ChannelItem) {
            return onClickChannelItem((ChannelItem) item);
        }

        return false;
    }

    private boolean OnClickButtonItem(FavoriteButton button) {
        int type = button.getType();

        if (type == FavoriteButton.RESET.getType()) {
            resetFavorite();
            return true;
        } else if (type == FavoriteButton.SAVE.getType()) {
            saveFavorite();
        } else if (type == FavoriteButton.CANCEL.getType()) {
            cancelFavorite();
            onFavoriteChannelChangeResultCallback.onChangeFavoriteChannel(false, 0);
        }

        return true;
    }

    private boolean onClickChannelItem(ChannelItem channel) {
        if (favoriteList.contains(channel)) {
            favoriteList.remove(channel);
        } else {
            favoriteList.add(channel);
        }

        updateView();

        return true;
    }

    private void updateView() {
        int count = favoriteList.size();
        selectedCountView.setText(getFormattedNumber(count));

        boolean isEnabled = favoriteList.size() > 0;
        if (isEnabled != FavoriteButton.RESET.isEnabled()) {
            FavoriteButton.RESET.setEnabled(isEnabled);
            buttonObjectAdapter.replace(FavoriteButton.RESET.getType(), FavoriteButton.RESET);
        }
    }

    private void resetFavorite() {
        for (ChannelItem channel : favoriteList) {
            channel.setChecked(false);
            int idx = channelList.indexOf(channel);
            listObjectAdapter.replace(idx, channel);
        }

        favoriteGridView.setSelectedPosition(0);
        favoriteList.clear();
        updateView();
    }

    private void saveFavorite() {
        ArrayList<Long> list = new ArrayList<>();
        for (ChannelItem channelItem : favoriteList) {
            Long channel = channelItem.getChannel().getId();
            list.add(channel);
        }

        favoriteTask = mUserDataManager.postFavoriteChannelList(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), list.toArray(new Long[0]),
                new MbsDataProvider<String, List<Long>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "saveFavorite, onFailed");
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<Long> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "saveFavorite, onSuccess");
                        }

                        onFavoriteChannelChangeResultCallback.onChangeFavoriteChannel(true, result.size());
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }
                });
    }

    private void cancelFavorite() {
        favoriteList.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (favoriteTask != null) {
            favoriteTask.cancel(true);
        }
        textButtonBridgeAdapter.setListener(null);
        channelCheckButtonBridgeAdapter.setListener(null);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onFavoriteChannelChangeResultCallback = null;
    }

    protected enum FavoriteButton implements TextButtonPresenter.TextButtonItem {
        RESET(0, R.string.reset_settings, false), SAVE(1, R.string.save, true), CANCEL(2, R.string.cancel, true);

        private int type;
        private int title;
        private boolean isEnabled;

        FavoriteButton(int type, int title, boolean isEnabled) {
            this.type = type;
            this.title = title;
            this.isEnabled = isEnabled;
        }

        public void setEnabled(boolean enabled) {
            isEnabled = enabled;
        }

        @Override
        public int getTitle() {
            return title;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public boolean isEnabled() {
            return isEnabled;
        }
    }

    public interface OnFavoriteChannelChangeResultCallback {
        void onChangeFavoriteChannel(boolean isSuccess, int favoriteSize);
    }
}
