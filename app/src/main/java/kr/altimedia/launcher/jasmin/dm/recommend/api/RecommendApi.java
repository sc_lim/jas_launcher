/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.recommend.api;

import kr.altimedia.launcher.jasmin.dm.recommend.obj.response.RecommendCurationResponse;
import kr.altimedia.launcher.jasmin.dm.recommend.obj.response.RecommendRelatedResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by mc.kim on 01,06,2020
 */
public interface RecommendApi {

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("recomm")
    Call<RecommendRelatedResponse> getRelatedContent(@Query("CONTENT") String content,
                                                     @Query("REC_TYPE") String recType,
                                                     @Query("REC_GB_CODE") String recGbCode,
                                                     @Query("SA_ID") String saId,
                                                     @Query("PRODUCT_LIST") String subscriptionId,
                                                     @Query("REQ_DATE") String reqDate,
                                                     @Query("REQ_CODE") String reqCode,
                                                     @Query("SVC_CODE") String svcCode,
                                                     @Query("CONS_ID") String contentId,
                                                     @Query("CAT_TYPE") String categoryType,
                                                     @Query("ITEM_CNT") int itemCnt,
                                                     @Query("CONS_RATE") String isAdult,
                                                     @Query("STB_VER") String stbVersion,
                                                     @Query("PROFILE_ID") String profileId,
                                                     @Query("LANGTYPE") String language,
                                                     @Query("CAT_ID") String categoryId);


    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("curation")
    Call<RecommendCurationResponse> getCurationContent(@Query("CONTENT") String content,
                                                       @Query("REC_TYPE") String recType,
                                                       @Query("SA_ID") String saId,
                                                       @Query("PRODUCT_LIST") String subscriptionId,
                                                       @Query("REQ_DATE") String reqDate,
                                                       @Query("SVC_CODE") String svcCode,
                                                       @Query("ITEM_MIN_CNT") int maxCnt,
                                                       @Query("ITEM_MAX_CNT") int itemCnt,
                                                       @Query("CONS_RATE") String isAdult,
                                                       @Query("STB_VER") String stbVersion,
                                                       @Query("PROFILE_ID") String profileId,
                                                       @Query("CAT_ID") String categoryId,
                                                       @Query("LANGTYPE") String language);
}
