/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm;

import androidx.annotation.Nullable;

/**
 * Created by mc.kim on 18,05,2020
 */
public class ResponseException extends Exception {
    private final String code;
    private final String message;

    public ResponseException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }


    public String getCode() {
        return code;
    }


    @Nullable
    @Override
    public String getMessage() {
        return message;
    }
}
