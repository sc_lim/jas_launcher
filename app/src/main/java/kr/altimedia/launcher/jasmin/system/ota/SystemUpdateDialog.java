/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.system.ota;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class SystemUpdateDialog implements View.OnKeyListener {
    public static final String CLASS_NAME = SystemUpdateDialog.class.getName();
    private final String TAG = SystemUpdateDialog.class.getSimpleName();

    private int target;
    private int type;
    private long delayTime;

    private WindowManager windowManager;
    private Context context;

    private View rootView;
    private TextView titleView;
    private LinearLayout normalMsgLayer;
    private TextView messageView1;
    private TextView messageView2;
    private LinearLayout countingLayer;
    private TextView minCounting;
    private TextView secCounting;
    private LinearLayout progressMsgLayer;
    private TextView progressCountView;
    private ProgressBar progressBar = null;
    private LinearLayout errorMsgLayer;
    private TextView errorCodeView;
    private TextView errorMessageView;
    private TextView confirmButton;
    private TextView cancelButton;

    private boolean visible = false;

    private final long TOTAL_COUNT_DOWN = 30 * TimeUtil.SEC;
    private final long INTERVAL_COUNT_DOWN = TimeUtil.SEC;
    private CountDownTimer countDownTimer;

    private OnUpdateActionListener onUpdateActionListener;

    private final int MSG_AUTO_HIDE = 0;
    private final int MSG_UPDATE_VIEW = 1;

    private final Handler hideHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            Log.d(TAG, "handleMessage: what="+msg.what);
            if(msg.what == MSG_AUTO_HIDE) {
                if (onUpdateActionListener != null) {
                    onUpdateActionListener.onConfirm(target, type);
                }

            }else if(msg.what == MSG_UPDATE_VIEW) {
                initView(rootView);
            }
        }
    };

    public SystemUpdateDialog(int target, int type, long delayTime) {
        this.target = target;
        this.type = type;
        this.delayTime = delayTime;
    }

    public void dismiss() {
        try {
            visible = false;
            onUpdateActionListener = null;
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
            if(hideHandler != null) {
                hideHandler.removeMessages(MSG_AUTO_HIDE);
                hideHandler.removeMessages(MSG_UPDATE_VIEW);
            }
            if(rootView != null && context != null){
                ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).removeView(rootView);
            }
        }catch (Exception | Error e){
        }
    }

    public void show(Context context) {

        try {
            this.context = context;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    PixelFormat.TRANSLUCENT);
            mParams.gravity = Gravity.CENTER;

            rootView = inflater.inflate(R.layout.dialog_system_update, null, false);

            initView(rootView);

            windowManager.addView(rootView, mParams);

            visible = true;
        }catch (Exception|Error e){
            if(Log.INCLUDE) {
                Log.d(TAG, "show: error occurred while showing dialog");
            }
        }
    }

    public boolean isVisible(){
        return visible;
    }

    private void initView(View view) {
        if(Log.INCLUDE) {
            Log.d(TAG, "initView: type=" + type);
        }

        initInfo(view);
        startDisplayTimer();
    }

    private void initInfo(View view) {
        try {
            titleView = view.findViewById(R.id.title);

            normalMsgLayer = view.findViewById(R.id.normalMsgLayer);
            messageView1 = view.findViewById(R.id.message1);
            messageView2 = view.findViewById(R.id.message2);
            countingLayer = view.findViewById(R.id.countingLayer);

            progressMsgLayer = view.findViewById(R.id.progressMsgLayer);
            progressCountView = view.findViewById(R.id.progressCount);
            progressBar = view.findViewById(R.id.progressBar);

            errorMsgLayer = view.findViewById(R.id.errorMsgLayer);
            errorCodeView = view.findViewById(R.id.errorCode);
            errorMessageView = view.findViewById(R.id.errorMessage);

            confirmButton = view.findViewById(R.id.btnConfirm);
            cancelButton = view.findViewById(R.id.btnCancel);

            Handler mHandler = new Handler(Looper.getMainLooper());
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        String title = "";
                        if(type == SystemUpdateManager.TYPE_ERROR){
                            title = view.getResources().getString(R.string.error);
                        }else {
                            if (target == SystemUpdateManager.TARGET_FW) {
                                title = view.getResources().getString(R.string.firmware_update);
                            } else if (target == SystemUpdateManager.TARGET_APK) {
                                title = view.getResources().getString(R.string.software_update);
                            }
                        }
                        titleView.setText(title);

                        if (type == SystemUpdateManager.TYPE_REQUEST) {
                            messageView1.setText(view.getResources().getString(R.string.sys_update_request1));
                            messageView2.setText(view.getResources().getString(R.string.sys_update_request2));
                            errorMsgLayer.setVisibility(View.GONE);
                            progressMsgLayer.setVisibility(View.GONE);
                            normalMsgLayer.setVisibility(View.VISIBLE);

                            initTimer(view);
                            showDoubleButton(view);

                        } else if (type == SystemUpdateManager.TYPE_PROGRESS) {
                            errorMsgLayer.setVisibility(View.GONE);
                            countingLayer.setVisibility(View.GONE);
                            normalMsgLayer.setVisibility(View.GONE);
                            progressMsgLayer.setVisibility(View.VISIBLE);

                            showSingleButton(view);

                        } else if (type == SystemUpdateManager.TYPE_COMPLETE) {
                            messageView1.setText(view.getResources().getString(R.string.sys_update_complete1));
                            messageView2.setText(view.getResources().getString(R.string.sys_update_complete2));
                            errorMsgLayer.setVisibility(View.GONE);
                            progressMsgLayer.setVisibility(View.GONE);
                            countingLayer.setVisibility(View.GONE);
                            normalMsgLayer.setVisibility(View.VISIBLE);

                            showNoButton(view);

                        } else if (type == SystemUpdateManager.TYPE_ERROR) {
                            errorCodeView.setText("");
                            errorMessageView.setText("");
                            progressMsgLayer.setVisibility(View.GONE);
                            countingLayer.setVisibility(View.GONE);
                            normalMsgLayer.setVisibility(View.GONE);
                            errorMsgLayer.setVisibility(View.VISIBLE);

                            showSingleButton(view);
                        }
                    }catch (Exception e){
                        if(Log.INCLUDE) {
                            Log.d(TAG, "initInfo: error occurred while setting view");
                        }
                    }
                }
            });
        }catch (Exception e){
            if(Log.INCLUDE) {
                Log.d(TAG, "initInfo: error occurred while initializing view");
            }
        }

    }

    private void initTimer(View view) {
        minCounting = view.findViewById(R.id.minCounting);
        secCounting = view.findViewById(R.id.secCounting);
        countingLayer.setVisibility(View.VISIBLE);
        countDownTimer = new CountDownTimer(TOTAL_COUNT_DOWN, INTERVAL_COUNT_DOWN) {
            @Override
            public void onTick(long millisUntilFinished) {
                long sec = TimeUtil.getSec(millisUntilFinished);
                String secText = StringUtil.getFormattedNumber(String.valueOf(sec), 2);
                secCounting.setText(secText);
            }

            @Override
            public void onFinish() {
                if (onUpdateActionListener != null) {
                    onUpdateActionListener.onConfirm(target, type);
                }
            }
        };
        countDownTimer.start();
    }

    private void showDoubleButton(View view){
        confirmButton.setText(view.getResources().getString(R.string.confirm));
        confirmButton.setVisibility(View.VISIBLE);
        confirmButton.setOnKeyListener(this);
        cancelButton.setVisibility(View.VISIBLE);
        cancelButton.setOnKeyListener(this);
    }

    private void showSingleButton(View view){
        confirmButton.setText(view.getResources().getString(R.string.close));
        confirmButton.setOnKeyListener(this);
        cancelButton.setVisibility(View.GONE);
        confirmButton.setVisibility(View.VISIBLE);
    }

    private void showNoButton(View view){
        confirmButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);
    }

    private void startDisplayTimer() {
        if(delayTime > 0) {
            hideHandler.sendEmptyMessageDelayed(MSG_AUTO_HIDE, delayTime);
        }
    }

    public void updateView(int target, int type, long delayTime) {
        this.target = target;
        this.type = type;
        this.delayTime = delayTime;
        if(delayTime > 0) {
            hideHandler.sendEmptyMessageDelayed(MSG_AUTO_HIDE, delayTime);
        }else {
            hideHandler.removeMessages(MSG_AUTO_HIDE);
        }

        hideHandler.sendEmptyMessageDelayed(MSG_UPDATE_VIEW, 10);
    }

    public void updateErrorMessage(String errorCode, String errorMessage){
        if(errorCodeView != null && errorCode != null && !errorCode.isEmpty()) {
            errorCodeView.post(new Runnable() {
                @Override
                public void run() {
                    errorCodeView.setText(errorCode);
                    errorMessageView.setText(errorMessage);
                }
            });
        }
    }

    public void updateProgressCount(long current, long total){
        if(progressCountView != null) {
            progressCountView.post(new Runnable() {
                @Override
                public void run() {
                    String count = "("+current +" / "+total+")";
                    progressCountView.setText(count);
                }
            });
        }
    }

    public void updateProgressBar(long current, long total){
        if(progressBar != null) {
            progressBar.post(new Runnable() {
                @Override
                public void run() {
                    progressBar.setMax((int)total);
                    progressBar.setProgress((int)current);
                }
            });
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if(Log.INCLUDE) {
            Log.d(TAG, "onKey : " + keyCode);
        }
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        hideHandler.removeMessages(MSG_AUTO_HIDE);

        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (v.getId() == R.id.btnConfirm) {
                    if (onUpdateActionListener != null) {
                        onUpdateActionListener.onConfirm(target, type);
                    }
                }else if (v.getId() == R.id.btnCancel) {
                    if (onUpdateActionListener != null) {
                        onUpdateActionListener.onCancel(target, type);
                    }
                }
                return true;
            case KeyEvent.KEYCODE_BACK:
                if (onUpdateActionListener != null) {
                    onUpdateActionListener.onCancel(target, type);
                }
                return true;
        }
        return false;
    }

    public void setOnUpdateActionListener(OnUpdateActionListener onUpdateActionListener) {
        this.onUpdateActionListener = onUpdateActionListener;
    }

    public interface OnUpdateActionListener {
        void onConfirm(int target, int type);

        void onCancel(int target, int type);
    }
}
