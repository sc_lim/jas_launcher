/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOption;

/**
 * Created by mc.kim on 20,02,2020
 */
public class SideOptionCategory implements Parcelable {
    @Expose
    @SerializedName("type")
    private SidePanelOption optionType;
    @Expose
    @SerializedName("name")
    private String optionName;
    @Expose
    @SerializedName("isLeaf")
    private boolean isLeaf;
    @Expose
    @SerializedName("isRoot")
    private boolean isRoot;
    @Expose
    @SerializedName("action")
    private String action;

    private SideOptionCategory mParentsCategory;

    public String getAction() {
        return action;
    }

    public SideOptionCategory(SidePanelOption optionType, String optionName) {
        this.optionType = optionType;
        this.optionName = optionName;
        this.isRoot = false;
        this.isLeaf = true;
    }

    protected SideOptionCategory(Parcel in) {
        optionType = (SidePanelOption) in.readValue(SidePanelOption.class.getClassLoader());
        optionName = in.readString();
        isLeaf = in.readByte() != 0;
        isRoot = in.readByte() != 0;
        action = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(optionType);
        dest.writeString(optionName);
        dest.writeByte((byte) (isLeaf ? 1 : 0));
        dest.writeByte((byte) (isRoot ? 1 : 0));
        dest.writeString(action);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SideOptionCategory> CREATOR = new Creator<SideOptionCategory>() {
        @Override
        public SideOptionCategory createFromParcel(Parcel in) {
            return new SideOptionCategory(in);
        }

        @Override
        public SideOptionCategory[] newArray(int size) {
            return new SideOptionCategory[size];
        }
    };

    public SidePanelOption getOptionType() {
        return optionType;
    }

    public String getOptionName() {
        return optionName;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public boolean isRootNode() {
        return isRoot;
    }

    public SideOptionCategory getParentsCategory() {
        return mParentsCategory;
    }

    public void setParentsCategory(SideOptionCategory mParentsCategory) {
        this.mParentsCategory = mParentsCategory;
    }


    @Override
    public String toString() {
        return "SideOptionCategory{" +
                ", optionType=" + optionType +
                ", optionName='" + optionName + '\'' +
                ", isLeaf=" + isLeaf +
                ", isRoot=" + isRoot +
                ", action=" + action +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SideOptionCategory that = (SideOptionCategory) o;

        if (isLeaf != that.isLeaf) return false;
        if (isRoot != that.isRoot) return false;
        if (optionType != that.optionType) return false;
        return optionName.equals(that.optionName);
    }

    @Override
    public int hashCode() {
        int result = optionType.hashCode();
        result = 31 * result + optionName.hashCode();
        result = 31 * result + (isLeaf ? 1 : 0);
        result = 31 * result + (isRoot ? 1 : 0);
        return result;
    }


}
