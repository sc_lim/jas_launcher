/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.common.SpinnerTextView;

public class OptionButtonPresenter extends Presenter {
    private OptionButtonPresenter.OnKeyListener onKeyListener;

    public OptionButtonPresenter(OptionButtonPresenter.OnKeyListener listener) {
        onKeyListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_option_button, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (item instanceof SpinnerTextView.SpinnerOptionButton) {
            SpinnerTextView.SpinnerOptionButton optionButton = (SpinnerTextView.SpinnerOptionButton) item;

            View view = viewHolder.view;
            TextView title = view.findViewById(R.id.optionTitle);
            SpinnerTextView button = view.findViewById(R.id.optionButton);

            Context context = view.getContext();
            String titleName = context.getString(optionButton.getTitle());
            title.setText(titleName);

            SettingSpinnerPresenter settingSpinnerPresenter = new SettingSpinnerPresenter();
            settingSpinnerPresenter.setOnSpinnerClickListener(
                    new SpinnerTextView.OnSpinnerClickListener() {
                        @Override
                        public void onClick(SpinnerTextView.SpinnerPresenterItem item) {
                            button.onClickSpinnerItem(item.getTitle());
                            if (onKeyListener != null) {
                                onKeyListener.onClick(item);
                            }
                        }
                    });

            button.setSpinner(settingSpinnerPresenter, optionButton);

            RelativeLayout spinnerLayout = button.findViewById(R.id.spinnerLayout);
            spinnerLayout.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    return onKeyListener.onKey(keyCode, optionButton.getType());
                }
            });
        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        View view = viewHolder.view;
        SpinnerTextView button = view.findViewById(R.id.optionButton);
        RelativeLayout spinnerLayout = button.findViewById(R.id.spinnerLayout);
        spinnerLayout.setOnKeyListener(null);
    }

    public interface OnKeyListener {
        boolean onKey(int keyCode, int buttonType);

        void onClick(SpinnerTextView.SpinnerPresenterItem item);
    }
}
