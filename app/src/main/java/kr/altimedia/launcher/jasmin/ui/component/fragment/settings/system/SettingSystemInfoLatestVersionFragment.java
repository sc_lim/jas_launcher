/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system.SettingSystemInfoUpdateDialogFragment.KEY_UPDATE_VERSION_TYPE;

public class SettingSystemInfoLatestVersionFragment extends SettingBaseFragment {
    public static final String CLASS_NAME = SettingSystemInfoLatestVersionFragment.class.getName();

    public SettingSystemInfoLatestVersionFragment() {
    }

    public static SettingSystemInfoLatestVersionFragment newInstance(Bundle bundle) {
        SettingSystemInfoLatestVersionFragment fragment = new SettingSystemInfoLatestVersionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_system_info_latest_version;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        initText(view);
        initButton(view);
    }

    private void initText(View view) {
        SettingSystemInfoUpdateDialogFragment.UpdateVersionType type = (SettingSystemInfoUpdateDialogFragment.UpdateVersionType) getArguments().getSerializable(KEY_UPDATE_VERSION_TYPE);
        int descResourceId = type == SettingSystemInfoUpdateDialogFragment.UpdateVersionType.FIRMWARE ? R.string.sysinfo_no_update_firmware : R.string.sysinfo_no_update_apk;
        String title = getString(R.string.sysinfo_no_update);
        String desc = getString(descResourceId);

        ((TextView) view.findViewById(R.id.title)).setText(title);
        ((TextView) view.findViewById(R.id.desc)).setText(desc);
    }

    private void initButton(View view) {
        TextView closeButton = view.findViewById(R.id.close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SafeDismissDialogFragment) getParentFragment()).dismiss();
            }
        });
    }
}
