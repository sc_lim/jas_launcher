package kr.altimedia.launcher.jasmin.dm.adds.remote;

import com.altimedia.util.Log;

import java.io.IOException;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.MbsUtil;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.adds.api.ADDSApi;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mc.kim on 04,08,2020
 */
public class AddsRemote {
    private static final String TAG = AddsRemote.class.getSimpleName();
    private ADDSApi mADDSApi;
    private final String DOMAIN = "3BB";
    private final String PLATFORM_TYPE = "STB";
    private final String INVENTORY_TYPE = "VOD";

    public AddsRemote(ADDSApi addsApi) {
        mADDSApi = addsApi;
    }

    public Boolean reportADPlay(String trackingId,
                                String subscriptionId, String campaignId,
                                String userId,
                                String regionId,
                                String opportunityType,
                                String categoryId,
                                String assetId,
                                String adPlayStartTime,
                                String adPlayEndTime) throws ResponseException, IOException, AuthenticationException {
        Call<BaseResponse> call = mADDSApi.reportADPlay(DOMAIN,
                trackingId, subscriptionId, campaignId, userId, regionId, PLATFORM_TYPE, INVENTORY_TYPE, opportunityType, categoryId, assetId, adPlayStartTime, adPlayEndTime);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mCategoryResponse = response.body();
        return true;
    }

}
