/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.recommend.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.recommend.obj.RcmdContent;

/**
 * Created by mc.kim on 02,06,2020
 */
public class RecommendTopContentResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private RecommendTopContentResultData data;

    protected RecommendTopContentResponse(Parcel in) {
        super(in);
        data = in.readParcelable(RecommendTopContentResultData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(data, flags);
    }

    public RecommendTopContentResultData getData() {
        return data;
    }


    private static class RecommendTopContentResultData implements Parcelable {
        @Expose
        @SerializedName("result")
        private RecommendTopContentResult result;

        protected RecommendTopContentResultData(Parcel in) {
            result = in.readParcelable(RecommendTopContentResult.class.getClassLoader());
        }

        public RecommendTopContentResult getResult() {
            return result;
        }

        @Override
        public String toString() {
            return "RecommendTopContentResultData{" +
                    "result=" + result +
                    '}';
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(result, flags);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<RecommendTopContentResultData> CREATOR = new Creator<RecommendTopContentResultData>() {
            @Override
            public RecommendTopContentResultData createFromParcel(Parcel in) {
                return new RecommendTopContentResultData(in);
            }

            @Override
            public RecommendTopContentResultData[] newArray(int size) {
                return new RecommendTopContentResultData[size];
            }
        };
    }

    private static class RecommendTopContentResult implements Parcelable {
        @Expose
        @SerializedName("ITEM_CNT")
        private int itemCnt;
        @Expose
        @SerializedName("TOP_TITLE")
        private String topTitle;
        @Expose
        @SerializedName("RC_LIST")
        private List<RcmdContent> rcdList;

        public int getItemCnt() {
            return itemCnt;
        }

        public List<RcmdContent> getRcdList() {
            return rcdList;
        }

        public String getTopTitle() {
            return topTitle;
        }

        @Override
        public String toString() {
            return "RecommendTopContentResult{" +
                    "itemCnt=" + itemCnt +
                    ", topTitle='" + topTitle + '\'' +
                    ", rcdList=" + rcdList +
                    '}';
        }

        protected RecommendTopContentResult(Parcel in) {
            itemCnt = in.readInt();
            topTitle = in.readString();
            rcdList = in.createTypedArrayList(RcmdContent.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(itemCnt);
            dest.writeString(topTitle);
            dest.writeTypedList(rcdList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<RecommendTopContentResult> CREATOR = new Creator<RecommendTopContentResult>() {
            @Override
            public RecommendTopContentResult createFromParcel(Parcel in) {
                return new RecommendTopContentResult(in);
            }

            @Override
            public RecommendTopContentResult[] newArray(int size) {
                return new RecommendTopContentResult[size];
            }
        };
    }
}
