/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingMenu;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;

public abstract class SettingBaseFragment extends Fragment {
    protected int layoutResourceId = -1;

    private WeakReference<OnFragmentChange> mOnFragmentChange;
    protected OnFragmentChange onFragmentChange = new OnFragmentChange() {
        @Override
        public void onChangeMenu(SettingMenu menu) {
            if (mOnFragmentChange != null) {
                OnFragmentChange fragmentChange = mOnFragmentChange.get();
                if (fragmentChange != null) {
                    fragmentChange.onChangeMenu(menu);
                }
            }
        }

        @Override
        public void onNextFragment(Fragment fragment, String tag) {
            if (mOnFragmentChange != null) {
                OnFragmentChange fragmentChange = mOnFragmentChange.get();
                if (fragmentChange != null) {
                    fragmentChange.onNextFragment(fragment, tag);
                }
            }
        }

        @Override
        public void onClickMenu() {
            if (mOnFragmentChange != null) {
                OnFragmentChange fragmentChange = mOnFragmentChange.get();
                if (fragmentChange != null) {
                    fragmentChange.onClickMenu();
                }
            }
        }

        @Override
        public void onUpdateMenuItem(int index, Object item) {
            if (mOnFragmentChange != null) {
                OnFragmentChange fragmentChange = mOnFragmentChange.get();
                if (fragmentChange != null) {
                    fragmentChange.onUpdateMenuItem(index, item);
                }
            }
        }

        @Override
        public void onMenuFocus() {
            if (mOnFragmentChange != null) {
                OnFragmentChange fragmentChange = mOnFragmentChange.get();
                if (fragmentChange != null) {
                    fragmentChange.onMenuFocus();
                }
            }
        }

        @Override
        public void notifyFrameAttached() {
            if (mOnFragmentChange != null) {
                OnFragmentChange fragmentChange = mOnFragmentChange.get();
                if (fragmentChange != null) {
                    fragmentChange.notifyFrameAttached();
                }
            }
        }

        @Override
        public SettingTaskManager getSettingTaskManager() {
            if (mOnFragmentChange != null) {
                OnFragmentChange fragmentChange = mOnFragmentChange.get();
                if (fragmentChange != null) {
                    return fragmentChange.getSettingTaskManager();
                }
            }

            return null;
        }

        @Override
        public void showProgressbar(boolean isLoading) {
            if (mOnFragmentChange != null) {
                OnFragmentChange fragmentChange = mOnFragmentChange.get();
                if (fragmentChange != null) {
                    fragmentChange.showProgressbar(isLoading);
                }
            }
        }

        @Override
        public TvOverlayManager getTvOverlayManager() {
            if (mOnFragmentChange != null) {
                OnFragmentChange fragmentChange = mOnFragmentChange.get();
                if (fragmentChange != null) {
                    return fragmentChange.getTvOverlayManager();
                }
            }

            return null;
        }
    };

    public void setOnFragmentChange(OnFragmentChange onFragmentChange) {
        this.mOnFragmentChange = new WeakReference<>(onFragmentChange);
    }

    public OnFragmentChange getOnFragmentChange() {
        return mOnFragmentChange != null ? mOnFragmentChange.get() : null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutResourceId = initLayoutResourceId();
        return inflater.inflate(layoutResourceId, container, false);
    }

    protected abstract int initLayoutResourceId();

    private View parentView;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parentView = view;
        initView(view);

        if (onFragmentChange != null) {
            onFragmentChange.notifyFrameAttached();
        }
    }

    protected void initView(View view) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        onFragmentChange.showProgressbar(false);

        if (mOnFragmentChange != null) {
            mOnFragmentChange.clear();
        }
    }
}
