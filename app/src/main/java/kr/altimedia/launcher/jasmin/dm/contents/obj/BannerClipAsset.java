/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.def.StringArrayDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;
import com.altimedia.tvmodule.util.StringUtils;

/**
 * Created by mc.kim on 08,05,2020
 */
public class BannerClipAsset implements Parcelable {
    @Expose
    @SerializedName("provider")
    private String provider;
    @Expose
    @SerializedName("containerType")
    private String containerType;
    @Expose
    @SerializedName("id")
    private String id;
    @Expose
    @SerializedName("fileName")
    @JsonAdapter(StringArrayDeserializer.class)
    private List<String> fileName;
    @Expose
    @SerializedName("coverImageId")
    private String coverImageId;
    @Expose
    @SerializedName("audioType")
    private String audioType;
    @Expose
    @SerializedName("videoType")
    private String videoType;

    @Expose
    @SerializedName("uhdFlag")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean uhdFlag;
    @Expose
    @SerializedName("webvttFileName")
    private String webvttFileName;
    @Expose
    @SerializedName("thumbnailFolder")
    private String thumbnailFolder;
    @Expose
    @SerializedName("coverImageName")
    private String coverImageName;
    @Expose
    @SerializedName("encryptionType")
    private String encryptionType;

    protected BannerClipAsset(Parcel in) {
        provider = in.readString();
        containerType = in.readString();
        id = in.readString();
        fileName = in.createStringArrayList();
        coverImageId = in.readString();
        audioType = in.readString();
        videoType = in.readString();
        uhdFlag = in.readByte() != 0;
        webvttFileName = in.readString();
        thumbnailFolder = in.readString();
        coverImageName = in.readString();
        encryptionType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(provider);
        dest.writeString(containerType);
        dest.writeString(id);
        dest.writeStringList(fileName);
        dest.writeString(coverImageId);
        dest.writeString(audioType);
        dest.writeString(videoType);
        dest.writeByte((byte) (uhdFlag ? 1 : 0));
        dest.writeString(webvttFileName);
        dest.writeString(thumbnailFolder);
        dest.writeString(coverImageName);
        dest.writeString(encryptionType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BannerClipAsset> CREATOR = new Creator<BannerClipAsset>() {
        @Override
        public BannerClipAsset createFromParcel(Parcel in) {
            return new BannerClipAsset(in);
        }

        @Override
        public BannerClipAsset[] newArray(int size) {
            return new BannerClipAsset[size];
        }
    };

    public String getProvider() {
        return provider;
    }

    public String getContainerType() {
        return containerType;
    }

    public String getId() {
        return id;
    }

    public List<String> getFileName() {
        return fileName;
    }

    public String getFileName(int index) {
        if (index < 0 || index >= fileName.size()) {
            return "";
        }
        return fileName.get(index);
    }

    public String getCoverImageId() {
        return coverImageId;
    }

    public String getAudioType() {
        return audioType;
    }

    public String getVideoType() {
        return videoType;
    }

    public boolean isUhdFlag() {
        return uhdFlag;
    }

    public String getWebvttFileName() {
        return webvttFileName;
    }

    public String getThumbnailFolder() {
        return thumbnailFolder;
    }

    public String getCoverImageName() {
        return coverImageName;
    }

    public String getEncryptionType() {
        return encryptionType;
    }

    public String getPosterSourceUrl(int width, int height) {
        String posterType = StringUtils.nullToEmpty(containerType).isEmpty() ? "" : containerType;
        return coverImageName + "_" + width + "x" + height + "." + posterType;
    }


    @Override
    public String toString() {
        return "BannerClipAsset{" +
                "provider='" + provider + '\'' +
                ", containerType='" + containerType + '\'' +
                ", id='" + id + '\'' +
                ", fileName='" + fileName + '\'' +
                ", coverImageId='" + coverImageId + '\'' +
                ", audioType='" + audioType + '\'' +
                ", videoType='" + videoType + '\'' +
                ", uhdFlag=" + uhdFlag +
                ", webvttFileName='" + webvttFileName + '\'' +
                ", thumbnailFolder='" + thumbnailFolder + '\'' +
                ", coverImageName='" + coverImageName + '\'' +
                ", encryptionType='" + encryptionType + '\'' +
                '}';
    }
}
