/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter;

import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.PaymentItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentButton;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.row.PaymentRow;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class PaymentPresenter extends Presenter {
    private static final String CLASS_NAME = PaymentPresenter.class.getName();
    private static final String TAG = PaymentPresenter.class.getSimpleName();

    private PaymentItemBridgeAdapter.OnPaymentClickListener onPaymentClickListener;
    private OnFocusPaymentListener onFocusPaymentListener;

    public PaymentPresenter() {
    }

    public void setOnPaymentClickListener(PaymentItemBridgeAdapter.OnPaymentClickListener onPaymentClickListener) {
        this.onPaymentClickListener = onPaymentClickListener;
    }

    public void setOnFocusPaymentListener(OnFocusPaymentListener onFocusPaymentListener) {
        this.onFocusPaymentListener = onFocusPaymentListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_payment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.initChildFocusListener(onFocusPaymentListener);
        vh.setPaymentList((PaymentRow) item, onPaymentClickListener);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        BrowseFrameLayout layout = viewHolder.view.findViewById(R.id.layout);
        layout.setOnChildFocusListener(null);
        layout.setOnFocusSearchListener(null);
        onPaymentClickListener = null;
        onFocusPaymentListener = null;
    }

    public interface OnFocusPaymentListener {
        void onFocusPayment(PaymentType paymentType);
    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private TextView paymentSelect;
        private TextView paymentDesc;
        private LinearLayout promotionLayout;
        private HorizontalGridView paymentGridView;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            paymentSelect = view.findViewById(R.id.payment_select);
            promotionLayout = view.findViewById(R.id.promotion_layout);
            paymentDesc = view.findViewById(R.id.promotion_desc);

            paymentGridView = view.findViewById(R.id.payment_grid_view);
        }

        private void initChildFocusListener(OnFocusPaymentListener onFocusPaymentListener) {
            BrowseFrameLayout layout = view.findViewById(R.id.layout);
            layout.setOnChildFocusListener(new BrowseFrameLayout.OnChildFocusListener() {
                @Override
                public boolean onRequestFocusInDescendants(int var1, Rect var2) {
                    return false;
                }

                @Override
                public void onRequestChildFocus(View child, View focused) {
                    int index = paymentGridView.getSelectedPosition();
                    ArrayObjectAdapter arrayObjectAdapter = (ArrayObjectAdapter) ((PaymentItemBridgeAdapter) paymentGridView.getAdapter()).getAdapter();
                    PaymentButton paymentButton = index > -1 && index < arrayObjectAdapter.size() ? (PaymentButton) arrayObjectAdapter.get(index) : null;

                    boolean hasPromotion = false;
                    if (paymentButton != null) {
                        PaymentType paymentType = paymentButton.getType();
                        hasPromotion = paymentType.hasPromotion();
                        if (hasPromotion) {
                            paymentDesc.setText(paymentType.getPromotion());
                        }

                        onFocusPaymentListener.onFocusPayment(paymentButton.getType());
                    }

//                    setPromotion(hasPromotion);
                }
            });
            layout.setOnFocusSearchListener(new BrowseFrameLayout.OnFocusSearchListener() {
                @Override
                public View onFocusSearch(View focused, int direction) {
                    if (direction == View.FOCUS_UP) {
                        onFocusPaymentListener.onFocusPayment(null);
                    }

                    return null;
                }
            });
        }

        public void setPromotion(boolean isVisible) {
            int promotionVisibility = isVisible ? View.VISIBLE : View.INVISIBLE;
            int paymentSelectTextVisibility = isVisible ? View.INVISIBLE : View.VISIBLE;

            if (promotionLayout.getVisibility() != promotionVisibility) {
                promotionLayout.setVisibility(promotionVisibility);
            }
            if (paymentSelect.getVisibility() != paymentSelectTextVisibility) {
                paymentSelect.setVisibility(paymentSelectTextVisibility);
            }
        }

        public void setPaymentList(PaymentRow paymentRow, PaymentItemBridgeAdapter.OnPaymentClickListener onPaymentClickListener) {
            PaymentItemBridgeAdapter paymentItemBridgeAdapter = paymentRow.getPaymentItemBridgeAdapter();
            paymentItemBridgeAdapter.setOnPaymentClickListener(onPaymentClickListener);

            paymentGridView = view.findViewById(R.id.payment_grid_view);
            paymentGridView.setAdapter(paymentItemBridgeAdapter);

            int gap = (int) view.getResources().getDimension(R.dimen.discount_button_spacing);
            paymentGridView.setHorizontalSpacing(gap);
        }

        public boolean updatePaymentEnable(boolean isEnable) {
            ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) ((ItemBridgeAdapter) paymentGridView.getAdapter()).getAdapter();
            if (objectAdapter.size() <= 0) {
                return false;
            }

            PaymentButton button = (PaymentButton) objectAdapter.get(1);

            //when user payment is 0, all payment button disable except billing
            if (isEnable == button.isEnable()) {
                return false;
            }

            boolean hasBillingPayment = false;
            int size = objectAdapter.size();
            for (int i = 0; i < size; i++) {
                PaymentButton mPaymentButton = (PaymentButton) objectAdapter.get(i);
                PaymentType type = mPaymentButton.getType();

                if (type == PaymentType.BILLING) {
                    if (!hasBillingPayment) {
                        hasBillingPayment = true;
                    }
                } else {
                    objectAdapter.replace(i, new PaymentButton(type, isEnable));
                }
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "isEnable : " + isEnable + ", hasBillingPayment : " + hasBillingPayment);
            }

            boolean gridViewEnable = isEnable || hasBillingPayment;
            int textVisibility = gridViewEnable ? View.VISIBLE : View.INVISIBLE;
            paymentGridView.setEnabled(gridViewEnable);
            paymentSelect.setVisibility(textVisibility);

            return gridViewEnable;
        }

        public boolean isEnabled() {
            return paymentGridView.isEnabled();
        }
    }
}
