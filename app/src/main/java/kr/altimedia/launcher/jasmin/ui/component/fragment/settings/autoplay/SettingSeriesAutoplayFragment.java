/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.autoplay;

import android.view.View;

import androidx.leanback.widget.ArrayObjectAdapter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.CheckButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.CheckButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import com.altimedia.util.Log;

public class SettingSeriesAutoplayFragment extends SettingBaseFragment implements SettingBaseButtonItemBridgeAdapter.OnClickButton<SettingSeriesAutoplayFragment.SeriesButton> {
    public static final String CLASS_NAME = SettingSeriesAutoplayFragment.class.getName();
    private static final String TAG = SettingSeriesAutoplayFragment.class.getSimpleName();

    private ArrayObjectAdapter objectAdapter;
    private CheckButtonBridgeAdapter checkButtonBridgeAdapter;

    private UserPreferenceManagerImp userPreferenceManagerImp;

    private SettingSeriesAutoplayFragment() {
    }

    public static SettingSeriesAutoplayFragment newInstance() {
        SettingSeriesAutoplayFragment fragment = new SettingSeriesAutoplayFragment();
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_series_autoplay;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        initData(view);
        initList(view);
    }

    private void initData(View view) {
        userPreferenceManagerImp = new UserPreferenceManagerImp(view.getContext());
        boolean isOn = userPreferenceManagerImp.getAutoPlay();

        SeriesButton.ON.setChecked(isOn);
        SeriesButton.OFF.setChecked(!isOn);
    }

    private void initList(View view) {
        objectAdapter = new ArrayObjectAdapter(new CheckButtonPresenter());
        objectAdapter.add(SeriesButton.ON);
        objectAdapter.add(SeriesButton.OFF);

        VerticalGridView gridView = view.findViewById(R.id.gridView);
        checkButtonBridgeAdapter = new CheckButtonBridgeAdapter(objectAdapter);
        checkButtonBridgeAdapter.setListener(this);
        gridView.setAdapter(checkButtonBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        gridView.setVerticalSpacing(spacing);
    }

    @Override
    public boolean onClickButton(SeriesButton item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        boolean isOn = item.getType() == SeriesButton.ON.getType();

        if ((isOn && SeriesButton.ON.isChecked()) || (!isOn && SeriesButton.OFF.isChecked())) {
            return true;
        }

        SeriesButton.ON.setChecked(isOn);
        SeriesButton.OFF.setChecked(!isOn);

        objectAdapter.replace(SeriesButton.ON.getType(), SeriesButton.ON);
        objectAdapter.replace(SeriesButton.OFF.getType(), SeriesButton.OFF);

        userPreferenceManagerImp.setAutoPlay(isOn);

        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        checkButtonBridgeAdapter.setListener(null);
    }

    protected enum SeriesButton implements CheckButtonPresenter.CheckButtonItem {
        ON(0, R.string.on), OFF(1, R.string.off);

        private int type;
        private int title;
        private boolean isChecked;

        SeriesButton(int type, int title) {
            this.type = type;
            this.title = title;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public int getTitle() {
            return title;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public boolean isChecked() {
            return isChecked;
        }

        @Override
        public boolean isEnabledCheck() {
            return true;
        }
    }
}
