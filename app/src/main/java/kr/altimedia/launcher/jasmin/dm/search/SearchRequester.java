/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search;

import java.io.IOException;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.search.module.SearchModule;
import kr.altimedia.launcher.jasmin.dm.search.obj.SearchData;
import kr.altimedia.launcher.jasmin.dm.search.remote.SearchRemote;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class SearchRequester extends BaseDataManager {
    public static final String SEARCH_ALL = "0xFF";
    public static final String SEARCH_VOD = "0x01";
    public static final String SEARCH_CHANNEL = "0x02";
    public static final String FILTER_PAY = "Y";
    public static final String FILTER_FREE = "N";
    public static final String FILTER_FULL = "None";
    private final String TAG = SearchRequester.class.getSimpleName();
    private SearchRemote searchRemote;

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        SearchModule searchModule = new SearchModule();
        Retrofit contentRetrofit = searchModule.provideSearchModule(mOkHttpClient);
        searchRemote = new SearchRemote(searchModule.provideSearchApi(contentRetrofit));
    }

    @Override
    public String getLoginToken() {
        return AccountManager.getInstance().getLoginToken();
    }

    public SearchData getTotalSearch(String contentsType,
                                     String sortType,
                                     String searchPosition,
                                     String searchCount,
                                     String searchWord,
                                     String sortFielDs,
                                     String category,
                                     String productID,
                                     String adultYn,
                                     String saID,
                                     String deviceType,
                                     String exposureTime,
                                     String STB_VER,
                                     String wonYn,
                                     String language,
                                     String transactionID, String rating) {

        try {
            return searchRemote.getTotalSearch(
                    contentsType,
                    sortType,
                    searchPosition,
                    searchCount,
                    searchWord,
                    sortFielDs,
                    category,
                    productID,
                    adultYn,
                    saID,
                    deviceType,
                    exposureTime,
                    STB_VER,
                    wonYn,
                    language,
                    transactionID, rating);
        } catch (Exception e) {
        }
        return null;
    }

    public MbsDataTask getTotalSearch(String contentsType,
                                      String sortType,
                                      String searchPosition,
                                      String searchCount,
                                      String searchWord,
                                      String sortFielDs,
                                      String category,
                                      String productID,
                                      String adultYn,
                                      String saID,
                                      String deviceType,
                                      String exposureTime,
                                      String STB_VER,
                                      String wonYn,
                                      String language,
                                      String transactionID, String rating,
                                      MbsDataProvider<String, SearchData> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, SearchData>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return searchRemote.getTotalSearch(
                        contentsType,
                        sortType,
                        searchPosition,
                        searchCount,
                        searchWord,
                        sortFielDs,
                        category,
                        productID,
                        adultYn,
                        saID,
                        deviceType,
                        exposureTime,
                        STB_VER,
                        wonYn,
                        language,
                        transactionID, rating);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, SearchData result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(saID);
        return mMbsDataTask;
    }
}