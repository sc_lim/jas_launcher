/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContentInfo;

/**
 * Created by mc.kim on 08,05,2020
 */
public class SeriesListResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private SeriesListResult seriesListResult;

    protected SeriesListResponse(Parcel in) {
        super(in);
        seriesListResult = in.readTypedObject(SeriesListResult.CREATOR);
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(seriesListResult, flags);
    }

    public SeriesContentInfo getSeriesContInfo() {
        if (seriesListResult == null) {
            return null;
        }
        SeriesContentInfo seriesContentInfo = new SeriesContentInfo(seriesListResult.focusedEpisode, seriesListResult.rating, seriesListResult.seriesList);
        return seriesContentInfo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SeriesListResponse> CREATOR = new Creator<SeriesListResponse>() {
        @Override
        public SeriesListResponse createFromParcel(Parcel in) {
            return new SeriesListResponse(in);
        }

        @Override
        public SeriesListResponse[] newArray(int size) {
            return new SeriesListResponse[size];
        }
    };

    @Override
    public String toString() {
        return "SeriesListResponse{" +
                "seriesListResult=" + seriesListResult +
                '}';
    }

    private static class SeriesListResult implements Parcelable {
        @Expose
        @SerializedName("focusedEpisode")
        private String focusedEpisode;

        @Expose
        @SerializedName("rating")
        private int rating;

        @Expose
        @SerializedName("list")
        private List<SeriesContent> seriesList;


        protected SeriesListResult(Parcel in) {
            focusedEpisode = in.readString();
            rating = in.readInt();
            seriesList = in.createTypedArrayList(SeriesContent.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(focusedEpisode);
            dest.writeInt(rating);
            dest.writeTypedList(seriesList);
        }

        public String getFocusedEpisode() {
            return focusedEpisode;
        }

        public int getRating() {
            return rating;
        }

        public List<SeriesContent> getSeriesList() {
            return seriesList;
        }

        @Override
        public String toString() {
            return "SeriesListResult{" +
                    "focusedEpisode='" + focusedEpisode + '\'' +
                    ", rating=" + rating +
                    ", seriesList=" + seriesList +
                    '}';
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<SeriesListResult> CREATOR = new Creator<SeriesListResult>() {
            @Override
            public SeriesListResult createFromParcel(Parcel in) {
                return new SeriesListResult(in);
            }

            @Override
            public SeriesListResult[] newArray(int size) {
                return new SeriesListResult[size];
            }
        };
    }
}
