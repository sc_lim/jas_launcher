/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.api;

import com.google.gson.JsonObject;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ActivationResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.BlockedChannelResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ChangePinCodeResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.CheckPinCodeResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.CollectionListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ExternalIntentDataResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.FAQInfoResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.FavoriteChannelResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.FavoriteContentListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.LoginResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.MembershipPointResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.OtpIdResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.OtpResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ProfileIconCategoryListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ProfileListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ProfileLoginResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.PurchaseListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ResumeTimeResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.SubscriberInfoResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.SubscriptionListResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.UserDeviceListResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by mc.kim on 11,05,2020
 */
public interface UserApi {

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("activation")
    Call<ActivationResponse> getActivationInfo(@Query("model") String model,
                                               @Query("macAddress") String macAddress, @Query("ipAddress") String ipAddress/*, @Query("uuid") String uuid*/);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("login")
    Call<LoginResponse> requestLogin(@Query("said") String said);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("checkpincode")
    Call<CheckPinCodeResponse> checkPinCode(@Query("said") String said,
                                            @Query("profileId") String profileId,
                                            @Query("type") String type,
                                            @Query("pinCode") String currentPinCode);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @PUT("changepincode")
    Call<ChangePinCodeResponse> changePinCode(@Body JsonObject pinCode);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("resetpincode")
    Call<BaseResponse> resetPinCode(@Body JsonObject pinCode);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @GET("inquiryotpid")
    Call<OtpIdResponse> getOtpId(@Query("said") String said,
                                 @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("otp")
    Call<OtpResponse> getOtp(@Body JsonObject object);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("checkotp")
    Call<BaseResponse> checkOtp(@Body JsonObject checkOtp);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("membershippoint")
    Call<MembershipPointResponse> getMembership(@Query("said") String said);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("subscriberinfo")
    @Deprecated
    Call<SubscriberInfoResponse> getSubscriberInfo(@Query("said") String said, @Query("loginId") String loginId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("watchinginfo")
    Call<BaseResponse> postWatchingInfo(@Body JsonObject watchingInfo);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("favoritechannel")
    Call<FavoriteChannelResponse> getFavoriteChannel(@Query("said") String said,
                                                     @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @PUT("favoritechannel")
    Call<BaseResponse> putFavoriteChannel(@Body JsonObject favoriteChannelInfo);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("favoritechannel")
    Call<FavoriteChannelResponse> postFavoriteChannel(@Body JsonObject favoriteChannelInfo);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("blockedchannel")
    Call<BlockedChannelResponse> getBlockedChannel(@Query("said") String said,
                                                   @Query("profileId") String profileId);


    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @PUT("blockedchannel")
    Call<BaseResponse> putBlockedChannel(@Body JsonObject blockedChannelInfo);


    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("blockedchannel")
    Call<BlockedChannelResponse> postBlockedChannel(@Body JsonObject blockedChannelInfo);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("resumetime")
    Call<ResumeTimeResponse> getResumeTime(@Query("said") String said,
                                           @Query("profileId") String profileId,
                                           @Query("contentId") String contentID);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @PUT("resumetime")
    Call<BaseResponse> postResumeTime(@Body JsonObject resumeBody);


    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("faqlist")
    Call<FAQInfoResponse> getFAQList(@Query("said") String said,
                                     @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("collectionlist")
    Call<CollectionListResponse> getCollectionList(@Query("language") String language,
                                                   @Query("said") String said,
                                                   @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("purchaselist")
    Call<PurchaseListResponse> getPurchaseList(@Query("language") String language,
                                               @Query("said") String said,
                                               @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @PUT("purchaselisthidden")
    Call<BaseResponse> putPurchaseListHidden(@Body JsonObject bodyData);

    @Headers("Content-Type: application/json")
    @GET("subscriptionlist")
    Call<SubscriptionListResponse> getSubscriptionList(@Query("language") String language,
                                                       @Query("said") String said,
                                                       @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @PUT("stopsubscription")
    Call<BaseResponse> putStopSubscription(@Body JsonObject bodyData);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("getprofile")
    Call<ProfileListResponse> getProfileList(@Query("said") String said,
                                             @Query("latestYn") String latestYn);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("profile")
    Call<BaseResponse> postProfile(@Body JsonObject bodyData);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @PUT("profile/changeinfo")
    Call<BaseResponse> putProfile(@Body JsonObject bodyData);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @HTTP(method = "DELETE", path = "profile", hasBody = false)
    Call<BaseResponse> deleteProfile(@Query("said") String said,
                                     @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("profilelogin")
    Call<ProfileLoginResponse> requestProfileLogin(@Query("loginId") String loginId,
                                                   @Query("said") String said,
                                                   @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @PUT("profilerating")
    Call<BaseResponse> putProfileRating(@Body JsonObject bodyData);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @PUT("profilelock")
    Call<BaseResponse> putProfileLock(@Body JsonObject bodyData);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("defaultprofileicon")
    Call<ProfileIconCategoryListResponse> getProfileIconCategoryList(@Query("language") String language,
                                                                     @Query("said") String said,
                                                                     @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("devicemanager")
    Call<UserDeviceListResponse> getDeviceList(@Query("said") String said,
                                               @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @PUT("devicemanager")
    Call<BaseResponse> putDevice(@Body JsonObject bodyData);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @HTTP(method = "DELETE", path = "devicemanager", hasBody = false)
    Call<BaseResponse> deleteDevice(@Query("said") String said,
                                    @Query("profileId") String profileId,
                                    @Query("deviceId") String deviceId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("vodlike")
    Call<FavoriteContentListResponse> getVodLike(@Query("language") String language,
                                                 @Query("said") String said,
                                                 @Query("profileId") String profileID);


    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("intentstoken")
    Call<ExternalIntentDataResponse> getIntentData(@Body JsonObject bodyData);
}
