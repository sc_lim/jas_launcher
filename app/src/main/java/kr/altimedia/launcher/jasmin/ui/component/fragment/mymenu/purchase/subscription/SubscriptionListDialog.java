/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscribedContent;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.OnDataLoadedListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription.adapter.SubscriptionListBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription.presenter.SubscriptionItemPresenter;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_SUBSCRIBED_CHANNEL;

public class SubscriptionListDialog extends SafeDismissDialogFragment implements OnDataLoadedListener, View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = SubscriptionListDialog.class.getName();
    private final String TAG = SubscriptionListDialog.class.getSimpleName();

    private ProgressBarManager progressBarManager;

    private List<SubscribedContent> subscriptionList = new ArrayList<>();
    private ArrayList<Integer> taskList = new ArrayList<>();
    private MbsDataTask dataTask;

    private TextView countView;
    private PagingVerticalGridView gridView;
    private ThumbScrollbar gridScrollbar;
    private final int VISIBLE_ROW_SIZE = 7;
    private SubscriptionListBridgeAdapter gridBridgeAdapter;

    private LinearLayout listDataLayer;
    private LinearLayout emptyDataLayer;
    private View rootView;
    private TvOverlayManager mTvOverlayManager;

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent == null) return;

            String action = intent.getAction();
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive: intent=" + intent.getAction());
            }
            if(action.equals(ACTION_UPDATED_SUBSCRIBED_CHANNEL)) {
                loadData();
            }
        }
    };

    public static SubscriptionListDialog newInstance() {
        Bundle args = new Bundle();
        SubscriptionListDialog fragment = new SubscriptionListDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private void showSafeDialog(SafeDismissDialogFragment dialog) {
        if (mTvOverlayManager == null) {
            return;
        }
        mTvOverlayManager.showDialogFragment(dialog);
    }

    @Override
    public void onDestroyView() {

        if(dataTask != null){
            dataTask.cancel(true);
        }
        if (subscriptionList != null && !subscriptionList.isEmpty()) {
            subscriptionList.clear();
        }
        if (progressBarManager != null) {
            progressBarManager.hide();
            progressBarManager.setRootView(null);
        }
        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);

        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach: "+mTvOverlayManager);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_mymenu_subscription_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        initProgressbar(view);
        initView(view);

        loadData();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_UPDATED_SUBSCRIBED_CHANNEL);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, filter);
    }

    private void initProgressbar(View view) {
        progressBarManager = new ProgressBarManager();
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);
        showProgress();
    }

    private void initView(View view) {

        countView = view.findViewById(R.id.count);
        gridView = view.findViewById(R.id.gridView);
        gridScrollbar = view.findViewById(R.id.gridScrollbar);

        int verticalSpacing = (int) view.getResources().getDimension(R.dimen.mymenu_menu_list_vertical_space);
        gridView.setVerticalSpacing(verticalSpacing);
        gridView.initVertical(VISIBLE_ROW_SIZE);

        listDataLayer = view.findViewById(R.id.listDataLayer);
        emptyDataLayer = view.findViewById(R.id.emptyDataLayer);

    }

    private void checkLoadList(Integer task, boolean added) {
        if (added) {
            if (taskList.contains(task)) {
                return;
            }
            taskList.add(task);
        } else {
            taskList.remove(task);
        }
        if (taskList.isEmpty()) {
            hideProgress();
        }
    }

    private void loadData() {
        UserDataManager userDataManager = new UserDataManager();
        dataTask = userDataManager.getSubscriptionList(
                AccountManager.getInstance().getLocalLanguage(),
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                new MbsDataProvider<String, List<SubscribedContent>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(0, loading);
                    }

                    @Override
                    public void onFailed(int reason) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadData");
                        }
                        showView(0);

                        if(reason == MbsTaskCallback.REASON_NETWORK) {
                            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(reason, getTaskContext());
                            failNoticeDialogFragment.show(mTvOverlayManager,getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                        }
                    }

                    @Override
                    public void onSuccess(String key, List<SubscribedContent> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadData: result=" + result);
                        }

                        if (subscriptionList != null && !subscriptionList.isEmpty()) {
                            subscriptionList.clear();
                        }
                        if(result != null) {
                            List<SubscribedContent> tmpList = result;
                            if(tmpList != null) {
                                Collections.sort(tmpList, new Comparator<SubscribedContent>() {
                                    @Override
                                    public int compare(SubscribedContent content1, SubscribedContent content2) {
                                        long purchasedTime1 = 0;
                                        long purchasedTime2 = 0;
                                        try {
                                            if (content1 != null && content1.getPurchaseDateTime() != null) {
                                                purchasedTime1 = content1.getPurchaseDateTime().getTime();
                                            }
                                            if (content2 != null && content2.getPurchaseDateTime() != null) {
                                                purchasedTime2 = content2.getPurchaseDateTime().getTime();
                                            }
                                            if (purchasedTime1 < purchasedTime2) {
                                                return 1;
                                            } else if (purchasedTime1 > purchasedTime2) {
                                                return -1;
                                            }
                                        }catch (Exception e){
                                        }
                                        return 0;
                                    }
                                });

                                subscriptionList.addAll(tmpList);
                            }
                        }

                        showView(0);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                        showView(0);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SubscriptionListDialog.this.getContext();
                    }
                });
    }

    @Override
    public void hideProgress() {
        if (progressBarManager != null) {
            progressBarManager.hide();
        }
    }

    @Override
    public void showProgress() {
        if (taskList.isEmpty()) {
            progressBarManager.show();
        }
    }

    @Override
    public void showView(int type) {
        completeTask(type);

        if (taskList.isEmpty()) {
            setGridView();
            hideProgress();
        }
    }

    private void completeTask(int type) {
        switch (type) {
            case 0:
                taskList.remove(dataTask);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "completeTask() removed type=" + type + ", current task size=" + taskList.size());
        }
    }

    private void setGridView() {
        if(!isAdded() || getActivity() == null){
            if (Log.INCLUDE) {
                Log.d(TAG, "setGridView: not added to context");
            }
            return;
        }

        if (subscriptionList != null && !subscriptionList.isEmpty()) {

            SubscriptionItemPresenter presenter = new SubscriptionItemPresenter(new SubscriptionItemPresenter.OnKeyListener() {
                @Override
                public boolean onKey(int keyCode, int index, Object item) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onKey() keyCode=" + keyCode + ", index=" + index);
                    }

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                            if (item instanceof SubscribedContent) {
                                onItemSelected(index, (SubscribedContent) item);
                            }
                            return true;
                        case KeyEvent.KEYCODE_DPAD_RIGHT:
                        case KeyEvent.KEYCODE_DPAD_LEFT:
                            return true;
                        case KeyEvent.KEYCODE_DPAD_UP:
                            if(gridView != null) {
                                if (index == 0) {
                                    int size = subscriptionList.size(); // gridView.getAdapter().getItemCount();
                                    gridView.scrollToPosition(size - 1);
                                    return true;
                                }
                            }
                            break;
                        case KeyEvent.KEYCODE_DPAD_DOWN:
                            if(gridView != null) {
                                int size = subscriptionList.size(); // gridView.getAdapter().getItemCount();
                                if (index == (size - 1)) {
                                    gridView.scrollToPosition(0);
                                    return true;
                                }
                            }
                            break;
                    }
                    return false;
                }
            });
            ArrayObjectAdapter listObjectAdapter = new ArrayObjectAdapter(presenter);
            for (int i = 0; i < subscriptionList.size(); i++) {
                listObjectAdapter.add(subscriptionList.get(i));
            }
            gridBridgeAdapter = new SubscriptionListBridgeAdapter(listObjectAdapter, VISIBLE_ROW_SIZE);
            gridBridgeAdapter.setAdapter(listObjectAdapter);
            gridView.setAdapter(gridBridgeAdapter);

            countView.setText("(" + subscriptionList.size() + ")");

            gridScrollbar.setList(subscriptionList.size(), VISIBLE_ROW_SIZE);
            gridView.setNumColumns(1);
            gridView.addOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {
                @Override
                public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                    super.onChildViewHolderSelected(parent, child, position, subposition);
                    gridScrollbar.moveFocus(position);
                }
            });

            gridScrollbar.setFocusable(false);

            emptyDataLayer.setVisibility(View.GONE);
            countView.setVisibility(View.VISIBLE);
            listDataLayer.setVisibility(View.VISIBLE);

        } else {
            countView.setVisibility(View.GONE);
            listDataLayer.setVisibility(View.GONE);
            emptyDataLayer.setVisibility(View.VISIBLE);
        }
    }

    private void onItemSelected(int index, SubscribedContent item) {
        SubscriptionDetailDialog dialog = SubscriptionDetailDialog.newInstance(SubscriptionDetailDialog.TYPE_INFO, item);
        dialog.setSubscriptionStopResultListener(new SubscriptionDetailDialog.SubscriptionStopResultListener() {
            @Override
            public void onSuccess() {
                loadData();
            }

            @Override
            public void onFailed() {

            }
        });
        showSafeDialog(dialog);
    }
}
