/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;

/**
 * Created by mc.kim on 08,06,2020
 */
public class ActivationResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private ActivationData data;

    protected ActivationResponse(Parcel in) {
        super(in);
        data = in.readParcelable(ActivationData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    public String getSaId() {
        return data.saId;
    }


    private static class ActivationData implements Parcelable {
        @Expose
        @SerializedName("said")
        private String saId;

        protected ActivationData(Parcel in) {
            saId = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(saId);
        }


        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<ActivationData> CREATOR = new Creator<ActivationData>() {
            @Override
            public ActivationData createFromParcel(Parcel in) {
                return new ActivationData(in);
            }

            @Override
            public ActivationData[] newArray(int size) {
                return new ActivationData[size];
            }
        };
    }
}
