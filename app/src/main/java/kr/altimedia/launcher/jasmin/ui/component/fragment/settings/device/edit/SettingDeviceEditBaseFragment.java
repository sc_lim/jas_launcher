/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.edit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.DeviceEditType;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;

public abstract class SettingDeviceEditBaseFragment extends Fragment {
    private OnChangeFragmentListener onChangeFragmentListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getResourceId(), container, false);
    }

    protected abstract int getResourceId();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    protected abstract void initView(View view);

    public OnChangeFragmentListener getOnChangeFragmentListener() {
        return onChangeFragmentListener;
    }

    public void setOnChangeFragmentListener(OnChangeFragmentListener onChangeFragmentListener) {
        this.onChangeFragmentListener = onChangeFragmentListener;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onChangeFragmentListener = null;
    }

    public interface OnChangeFragmentListener {
        void onNextFragment(DeviceEditType editButton);

        void onBackFragment();

        void onEditName(UserDevice deviceItem, String editNameTask);

        void onEditDelete(UserDevice deviceItem);

        void onDismiss();

        void showProgressbar(boolean isLoading);

        SettingTaskManager getSettingTaskManager();
    }
}
