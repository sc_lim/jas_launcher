/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;

/**
 * Created by mc.kim on 08,05,2020
 */
public class ContentsListResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private ContentsListResult result;

    protected ContentsListResponse(Parcel in) {
        super(in);
        result = in.readTypedObject(ContentsListResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(result, flags);
    }

    public List<Content> getContentList() {
        if (result.totalContentNo == 0) {
            return new ArrayList<>();
        }
        return result.contentList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContentsListResponse> CREATOR = new Creator<ContentsListResponse>() {
        @Override
        public ContentsListResponse createFromParcel(Parcel in) {
            return new ContentsListResponse(in);
        }

        @Override
        public ContentsListResponse[] newArray(int size) {
            return new ContentsListResponse[size];
        }
    };

    @Override
    public String toString() {
        return "ContentsListResponse{" +
                "result=" + result +
                '}';
    }

    private static class ContentsListResult implements Parcelable {
        @SerializedName("contentList")
        @Expose
        private List<Content> contentList;
        @Expose
        @SerializedName("totalContentNo")
        private int totalContentNo;

        protected ContentsListResult(Parcel in) {
            contentList = in.createTypedArrayList(Content.CREATOR);
            totalContentNo = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(contentList);
            dest.writeInt(totalContentNo);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<ContentsListResult> CREATOR = new Creator<ContentsListResult>() {
            @Override
            public ContentsListResult createFromParcel(Parcel in) {
                return new ContentsListResult(in);
            }

            @Override
            public ContentsListResult[] newArray(int size) {
                return new ContentsListResult[size];
            }
        };
    }


}
