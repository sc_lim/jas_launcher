/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.subtitles;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.system.settings.data.SubtitleBroadcastLanguage;
import kr.altimedia.launcher.jasmin.system.settings.data.SubtitleFontSize;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.CheckButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.OptionButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.TextButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.common.SpinnerTextView;
import kr.altimedia.launcher.jasmin.ui.view.common.SpinnerTextView.SpinnerOptionType;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.system.settings.SettingControl.LANG_ENG;
import static kr.altimedia.launcher.jasmin.system.settings.SettingControl.SUBTITLE_FONT_LARGE;
import static kr.altimedia.launcher.jasmin.system.settings.SettingControl.SUBTITLE_FONT_SMALL;

public class SettingSubtitlesSetupDialogFragment extends SettingBaseDialogFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton<SettingSubtitlesSetupDialogFragment.OkCancelButton> {
    public static final String CLASS_NAME = SettingSubtitlesSetupDialogFragment.class.getName();
    private static final String TAG = SettingSubtitlesSetupDialogFragment.class.getSimpleName();

    private final SettingControl settingControl = SettingControl.getInstance();

    private VerticalGridView subtitleGridView;
    private HorizontalGridView buttonsGridView;
    private CheckButtonBridgeAdapter checkButtonBridgeAdapter;
    private OnSubtitlesChangeResultCallback onSubtitlesChangeResultCallback;

    private SettingSubtitlesSetupDialogFragment() {

    }

    public static SettingSubtitlesSetupDialogFragment newInstance() {
        SettingSubtitlesSetupDialogFragment fragment = new SettingSubtitlesSetupDialogFragment();
        return fragment;
    }

    public void setOnSubtitlesChangeResultCallback(OnSubtitlesChangeResultCallback onSubtitlesChangeResultCallback) {
        this.onSubtitlesChangeResultCallback = onSubtitlesChangeResultCallback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_subtitles_setup, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        initData(view);
        initGridView(view);
        initButtons(view);
    }

    private void initData(View view) {
        String lang = StringUtils.nullToEmpty(settingControl.getSubtitleLanguageCode());
        String size = StringUtils.nullToEmpty(settingControl.getSubtitleLanguageFontSize());

        SubtitleBroadcastLanguage broadcastLanguage = lang.equalsIgnoreCase(LANG_ENG) ? SubtitleBroadcastLanguage.ENG : SubtitleBroadcastLanguage.THAI;
        SubtitleFontSize fontSize;
        if (size.equalsIgnoreCase(SUBTITLE_FONT_SMALL)) {
            fontSize = SubtitleFontSize.SMALL;
        } else if (size.equalsIgnoreCase(SUBTITLE_FONT_LARGE)) {
            fontSize = SubtitleFontSize.LARGE;
        } else {
            fontSize = SubtitleFontSize.MEDIUM;
        }

        SubtitleOptionButton.BROADCASTING.setCurrentOption(broadcastLanguage);
        SubtitleOptionButton.FONT_SIZE.setCurrentOption(fontSize);
    }

    private void initGridView(View view) {
        OptionButtonPresenter optionButtonPresenter = new OptionButtonPresenter(new OptionButtonPresenter.OnKeyListener() {
            @Override
            public boolean onKey(int keyCode, int buttonType) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_LEFT:
                        return true;
                    case KeyEvent.KEYCODE_DPAD_DOWN:
                        if (buttonType == SubtitleOptionButton.FONT_SIZE.getType()) {
                            buttonsGridView.requestFocus();
                            return true;
                        }
                }
                return false;
            }

            @Override
            public void onClick(SpinnerTextView.SpinnerPresenterItem item) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onClick, item=" + item);
                }

                SubtitleOptionButton parent = null;
                for (SubtitleOptionButton optionButton : SubtitleOptionButton.values()) {
                    if (optionButton.getType() == item.getParentType()) {
                        parent = optionButton;
                        break;
                    }
                }

                if (Log.INCLUDE) {
                    Log.d(TAG, "onClick, parent : " + parent);
                }

                if (parent != null) {
                    SpinnerOptionType[] options = parent.getOptions();
                    for (SpinnerOptionType option : options) {
                        if (option.getType() == item.getType()) {
                            parent.setCurrentOption(option);
                            break;
                        }
                    }

                    if (parent == SubtitleOptionButton.FONT_SIZE) {
                        buttonsGridView.getChildAt(OkCancelButton.SAVE.getType()).requestFocus();
                    }
                }
            }
        });

        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(optionButtonPresenter);
        objectAdapter.add(SubtitleOptionButton.BROADCASTING);
        objectAdapter.add(SubtitleOptionButton.FONT_SIZE);

        subtitleGridView = view.findViewById(R.id.subtitleGridView);
        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(objectAdapter);
        subtitleGridView.setAdapter(itemBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        subtitleGridView.setVerticalSpacing(spacing);
    }

    private void initButtons(View view) {
        TextButtonPresenter textButtonPresenter = new TextButtonPresenter();
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(textButtonPresenter);
        objectAdapter.add(OkCancelButton.SAVE);
        objectAdapter.add(OkCancelButton.CANCEL);

        checkButtonBridgeAdapter = new CheckButtonBridgeAdapter(objectAdapter);
        checkButtonBridgeAdapter.setListener(this);
        buttonsGridView = view.findViewById(R.id.buttons_gridView);
        buttonsGridView.setAdapter(checkButtonBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_horizontal_spacing);
        buttonsGridView.setHorizontalSpacing(spacing);
    }

    @Override
    public boolean onClickButton(OkCancelButton item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        int type = item.getType();
        boolean isSuccess = false;

        if (type == OkCancelButton.SAVE.getType()) {
            isSuccess = true;

            saveData();
            onSubtitlesChangeResultCallback.onChangeSubtitles(isSuccess);
        } else {
            onSubtitlesChangeResultCallback.onChangeSubtitles(false);
        }

        return true;
    }

    private void saveData() {
        SubtitleBroadcastLanguage broadcastLanguage = (SubtitleBroadcastLanguage) SubtitleOptionButton.BROADCASTING.getCurrentOption();
        SubtitleFontSize fontSizeType = (SubtitleFontSize) SubtitleOptionButton.FONT_SIZE.getCurrentOption();

        settingControl.setSubtitleOption(true, broadcastLanguage.getKey(), fontSizeType.getKey());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        checkButtonBridgeAdapter.setListener(null);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onSubtitlesChangeResultCallback = null;
    }

    public enum SubtitleOptionButton implements SpinnerTextView.SpinnerOptionButton {
        BROADCASTING(0, R.string.subtitle_broadcasting_lang, SubtitleBroadcastLanguage.values()),
        FONT_SIZE(1, R.string.font_size, SubtitleFontSize.values());

        private int type;
        private int title;
        private SpinnerOptionType[] options;
        private SpinnerOptionType currentOption;

        SubtitleOptionButton(int type, int title, SpinnerOptionType[] options) {
            this.type = type;
            this.title = title;
            this.options = options;
        }

        public void setCurrentOption(SpinnerOptionType option) {
            currentOption = option;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public int getTitle() {
            return title;
        }

        @Override
        public SpinnerOptionType[] getOptions() {
            return options;
        }

        @Override
        public SpinnerOptionType getCurrentOption() {
            return currentOption;
        }
    }

    protected enum OkCancelButton implements TextButtonPresenter.TextButtonItem {
        SAVE(0, R.string.save), CANCEL(1, R.string.cancel);

        private int type;
        private int title;

        OkCancelButton(int type, int title) {
            this.type = type;
            this.title = title;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public int getTitle() {
            return title;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }

    public interface OnSubtitlesChangeResultCallback {
        void onChangeSubtitles(boolean isSuccess);
    }
}
