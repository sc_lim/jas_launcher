/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel;


import com.altimedia.util.Log;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.channel.module.ChannelModule;
import kr.altimedia.launcher.jasmin.dm.channel.obj.ChannelProductInfo;
import kr.altimedia.launcher.jasmin.dm.channel.remote.ChannelRemote;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class MbsChannelDataManager extends BaseDataManager {
    private static final String TAG = MbsChannelDataManager.class.getSimpleName();
    private static final boolean DEBUG = Log.INCLUDE;

    private ChannelRemote mChannelRemote;

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        ChannelModule channelModule = new ChannelModule();
        Retrofit contentRetrofit = channelModule.provideChannelModule(mOkHttpClient);
        mChannelRemote = new ChannelRemote(channelModule.provideChannelApi(contentRetrofit));
    }

    @Override
    public String getLoginToken() {
        return AccountManager.getInstance().getLoginToken();
    }

    public MbsDataTask getSubscribedChannelList(String said, String profiledId,
                                                MbsDataProvider<String, List<String>> provider) {
        if (DEBUG)
            Log.d(TAG, "getSubscribedChannelList() said:[" + said + "], profiledId:[" + profiledId + "]");
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<String>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mChannelRemote.getSubscribedChannelList(said, profiledId);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onTaskPostExecute(String key, List<String> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(profiledId, said);
        return mMbsDataTask;
    }

    public MbsDataTask getChannelProductInfo(String said, String profiledId, String channelId, String language,
                                             MbsDataProvider<String, ChannelProductInfo> provider) {
        if (DEBUG) Log.d(TAG, "getChannelProductInfo() channelId:" + channelId);
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, ChannelProductInfo>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mChannelRemote.getChannelProductInfo(said, profiledId, channelId, language);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, ChannelProductInfo result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(profiledId, channelId, language, said);
        return mMbsDataTask;
    }
}
