/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.CouponUtil;

public class PaymentWrapper implements Parcelable {
    private static final String TAG = PaymentWrapper.class.getSimpleName();

    private float price;
    private DiscountCoupon discountCoupon;
    private float couponPoint;
    private float membershipPoint;

    public PaymentWrapper(float price) {
        this.price = price;
    }

    protected PaymentWrapper(Parcel in) {
        price = (float) in.readValue(Float.class.getClassLoader());
        discountCoupon = (DiscountCoupon) in.readValue(DiscountCoupon.class.getClassLoader());
        couponPoint = (float) in.readValue(Float.class.getClassLoader());
        membershipPoint = (float) in.readValue(Float.class.getClassLoader());
    }

    public static final Creator<PaymentWrapper> CREATOR = new Creator<PaymentWrapper>() {
        @Override
        public PaymentWrapper createFromParcel(Parcel in) {
            return new PaymentWrapper(in);
        }

        @Override
        public PaymentWrapper[] newArray(int size) {
            return new PaymentWrapper[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(price);
        dest.writeValue(discountCoupon);
        dest.writeValue(couponPoint);
        dest.writeValue(membershipPoint);
    }

    public DiscountCoupon getDiscountCoupon() {
        return discountCoupon;
    }

    public void setCouponPoint(float couponPoint) {
        this.couponPoint = couponPoint;
    }

    public void setMembershipPoint(float membershipPoint) {
        this.membershipPoint = membershipPoint;
    }

    public float getPrice() {
        return price;
    }

    public void setDiscountCoupon(DiscountCoupon discountCoupon) {
        this.discountCoupon = discountCoupon;
    }

    public int getCouponDiscountValue() {
        return CouponUtil.getCouponDiscountValue(getPrice(), discountCoupon);
    }

    public float getCouponPoint() {
        return couponPoint;
    }

    public float getMembershipPoint() {
        return membershipPoint;
    }

    public float getTotalDiscount() {
        float totalDiscount = 0;

        if (discountCoupon != null) {
            totalDiscount += getCouponDiscountValue();
        }

        totalDiscount += couponPoint;
        totalDiscount += membershipPoint;

        return totalDiscount;
    }

    public int getAccountPayment() {
        int price = (int) getPrice();
        int couponDiscount = getCouponDiscountValue();
        int couponPoint = (int) getCouponPoint();
        int membershipPoint = (int) getMembershipPoint();
        int totalPayment = price - (couponDiscount + couponPoint + membershipPoint);

        if (Log.INCLUDE) {
            Log.d(TAG, "getAccountPayment"
                    + ", price : " + price
                    + ", couponDiscount : " + couponDiscount
                    + ", couponPoint : " + couponPoint
                    + ", membershipPoint : " + membershipPoint
                    + ", totalPayment : " + totalPayment);
        }

        return totalPayment > 0 ? totalPayment : 0;
    }

    public boolean isIncludeVat(PaymentType paymentType) {
        return true;
    }

    @Override
    public String toString() {
        return "PaymentWrapper{" +
                "price=" + price +
                ", discountCoupon=" + discountCoupon +
                ", couponPoint=" + couponPoint +
                ", membershipPoint=" + membershipPoint +
                '}';
    }
}
