/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.Action;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;

import com.altimedia.util.Log;

import java.lang.ref.WeakReference;

import kr.altimedia.launcher.jasmin.media.VideoPlayerAdapter;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.util.CountDownTimerWithPause;

/**
 * Created by mc.kim on 07,05,2020
 */
public class PreviewPlaybackTransportControlGlue<T extends VideoPlayerAdapter>
        extends VideoPlaybackBaseControlGlue<T> {

    static final String TAG = "PreviewPlaybackTransportControlGlue";
    static final boolean DEBUG = false;

    static final int MSG_UPDATE_PLAYBACK_STATE = 100;

    boolean mSeekEnabled;
    private final long INTERVAL = 1000;
    private CountDownTimerWithPause countDownTimer = null;

    private class UpdatePlaybackStateHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_PLAYBACK_STATE:
                    PreviewPlaybackTransportControlGlue glue =
                            ((WeakReference<PreviewPlaybackTransportControlGlue>) msg.obj).get();
                    if (glue != null) {
                        glue.onUpdatePlaybackState();
                    }
                    break;
            }
        }
    }

    final Handler sHandler = new UpdatePlaybackStateHandler();

    final WeakReference<VideoPlaybackBaseControlGlue> mGlueWeakReference = new WeakReference(this);

    /**
     * Constructor for the glue.
     *
     * @param context
     * @param impl    Implementation to underlying media player.
     */
    public PreviewPlaybackTransportControlGlue(Context context, T impl) {
        super(context, impl);
    }

    public void readyPreviewTimer(long time) {
        countDownTimer = new CountDownTimerWithPause(time, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onTick " + millisUntilFinished);
                }
                getHost().updateRemainTime(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onFinish ");
                }
                getHost().onFinish();
            }
        };
    }

    @Override
    public void setControlsRow(VideoPlaybackControlsRow controlsRow) {
        super.setControlsRow(controlsRow);
        clearSHandler();
        onUpdatePlaybackState();
    }

    private void clearSHandler() {
        sHandler.removeMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference);
    }

    @Override
    protected void onCreatePrimaryActions(ArrayObjectAdapter primaryActionsAdapter) {
        primaryActionsAdapter.add(mPlayPauseAction =
                new VideoPlaybackControlsRow.PlayPauseAction(getContext()));
    }

    @Override
    protected VideoPlaybackRowPresenter onCreateRowPresenter() {

        PreviewPlaybackTransportRowPresenter rowPresenter = new PreviewPlaybackTransportRowPresenter() {
            @Override
            protected void onBindRowViewHolder(RowPresenter.ViewHolder vh, Object item) {
                super.onBindRowViewHolder(vh, item);
                vh.setOnKeyListener(PreviewPlaybackTransportControlGlue.this);
            }

            @Override
            protected void onUnbindRowViewHolder(RowPresenter.ViewHolder vh) {
                super.onUnbindRowViewHolder(vh);
                vh.setOnKeyListener(null);
            }
        };
        return rowPresenter;
    }

    @Override
    protected void onAttachedToHost(VideoPlaybackGlueHost host) {
        super.onAttachedToHost(host);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttachedToHost");
        }
    }

    @Override
    protected void onDetachedFromHost() {
        super.onDetachedFromHost();
        clearSHandler();
        countDownTimer.release();
    }


    @Override
    public void onActionClicked(Action action) {
        dispatchAction(action, null);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_ESCAPE:
                return false;
        }

        final ObjectAdapter primaryActionsAdapter = mControlsRow.getPrimaryActionsAdapter();
        Action action = mControlsRow.getActionForKeyCode(primaryActionsAdapter, keyCode);
        if (action == null) {
            action = mControlsRow.getActionForKeyCode(mControlsRow.getSecondaryActionsAdapter(),
                    keyCode);
        }

        if (action != null) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                dispatchAction(action, event);
            }
            return true;
        }
        return false;
    }

    @Override
    protected void onUpdatePlaybackStatusAfterUserAction(final int msgWhat) {
        updatePlaybackState(mIsPlaying);
        sendHandlerMsg(msgWhat);
    }


    void sendHandlerMsg(int msgWhat) {
        clearSHandler();
        switch (msgWhat) {
            case MSG_UPDATE_PLAYBACK_STATE:
                sHandler.sendMessage(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference));
                break;
        }
    }


    /**
     * Called when the given action is invoked, either by click or keyevent.
     */
    boolean dispatchAction(Action action, KeyEvent keyEvent) {
        boolean handled = false;
        if (keyEvent == null) {
            return false;
        }
        if (action instanceof VideoPlaybackControlsRow.PlayPauseAction) {

            boolean canPlay = keyEvent == null
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY;
            boolean canPause = keyEvent == null
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PAUSE;
            if (canPause && mIsPlaying) {
                pause();
                if (getHost().isControlsOverlayVisible()) {
                    onUpdatePlaybackStatusAfterUserAction(MSG_UPDATE_PLAYBACK_STATE);
                }
            } else if (canPlay && !mIsPlaying) {
                play();
                onUpdatePlaybackStatusAfterUserAction(MSG_UPDATE_PLAYBACK_STATE);
            }
            handled = true;
        } else if (action instanceof VideoPlaybackControlsRow.SkipNextAction) {
            next();
            handled = true;
        } else if (action instanceof VideoPlaybackControlsRow.SkipPreviousAction) {
            previous();
            handled = true;
        }
        return handled;
    }


    @Override
    protected void onPlayStateChanged() {
        if (DEBUG) Log.v(TAG, "onStateChanged");

        if (sHandler.hasMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference)) {
            sHandler.removeMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference);
            if (mPlayerAdapter.isPlaying() != mIsPlaying) {
                if (DEBUG) Log.v(TAG, "Status expectation mismatch, delaying update");
                sendHandlerMsg(MSG_UPDATE_PLAYBACK_STATE);
            } else {
                if (DEBUG) Log.v(TAG, "Update state matches expectation");
                onUpdatePlaybackState();
            }
        } else {
            onUpdatePlaybackState();
        }

        if (mPlayerAdapter.isPlaying()) {
            countDownTimer.start();
        } else if (mPlayerAdapter.isPaused()) {
            countDownTimer.pause();
        }

        super.onPlayStateChanged();
    }

    void onUpdatePlaybackState() {
        mIsPlaying = mPlayerAdapter.isPlaying();
        updatePlaybackState(mIsPlaying);
    }

    private void updatePlaybackState(boolean isPlaying) {
        int index = !isPlaying
                ? VideoPlaybackControlsRow.PlayPauseAction.INDEX_PLAY
                : VideoPlaybackControlsRow.PlayPauseAction.INDEX_PAUSE;
        updatePlaybackState(isPlaying, index);
    }

    private void updatePlaybackState(boolean isPlaying, int iconIndex) {
        if (mControlsRow == null) {
            return;
        }

        if (!isPlaying) {
            onUpdateProgress();
            mPlayerAdapter.setProgressUpdatingEnabled(true);
        } else {
            mPlayerAdapter.setProgressUpdatingEnabled(true);
        }

        if (mFadeWhenPlaying && getHost() != null) {
            getHost().setControlsOverlayAutoHideEnabled(isPlaying);
        }

        if (mPlayPauseAction != null) {
            int index = iconIndex;
            int mPlayPauseActionIndex = mPlayPauseAction.getIndex();
            if (mPlayPauseActionIndex != index
                    || mPlayPauseActionIndex == VideoPlaybackControlsRow.PlayPauseAction.INDEX_REWIND
                    || mPlayPauseActionIndex == VideoPlaybackControlsRow.PlayPauseAction.INDEX_FAST_FORWARD) {
                mPlayPauseAction.setIndex(index);
                notifyItemChanged((ArrayObjectAdapter) getControlsRow().getPrimaryActionsAdapter(),
                        mPlayPauseAction);
            }
        }
    }

    public final void setSeekEnabled(boolean seekEnabled) {
        mSeekEnabled = seekEnabled;
    }


}
