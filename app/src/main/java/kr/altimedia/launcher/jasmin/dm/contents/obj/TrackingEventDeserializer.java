package kr.altimedia.launcher.jasmin.dm.contents.obj;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by mc.kim on 13,08,2020
 */
public class TrackingEventDeserializer implements JsonDeserializer<TrackingEvent> {

    @Override
    public TrackingEvent deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return TrackingEvent.findByActionName(json.getAsString());
    }
}
