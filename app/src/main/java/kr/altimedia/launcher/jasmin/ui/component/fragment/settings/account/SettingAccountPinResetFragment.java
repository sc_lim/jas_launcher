/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.OtpId;
import kr.altimedia.launcher.jasmin.dm.user.object.response.OtpResponse;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class SettingAccountPinResetFragment extends Fragment
        implements View.OnKeyListener, PasswordView.OnPasswordComplete {
    public static final String CLASS_NAME = SettingAccountPinResetFragment.class.getName();
    private final String TAG = SettingAccountPinResetFragment.class.getSimpleName();

    private final long INTERVAL = TimeUtil.SEC;

    private final ProgressBarManager progressBarManager = new ProgressBarManager();

    private TextView desc;
    private TextView otpIdView;
    private TextView resetButton;
    private TextView cancelButton;
    private PasswordView inputOtpView;
    private TextView countDownView;
    private TextView sendOtpButton;
    private View loadingView;

    private CountDownTimer countDownTimer = null;

    private CountDownTimer generateCountDownTimer(long totalTime) {
        CountDownTimer countDownTimer = new CountDownTimer(totalTime, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                long min = TimeUtil.getMin(millisUntilFinished);
                long sec = TimeUtil.getSec(millisUntilFinished);

                String minText = min < 10 ? "0" + min : String.valueOf(min);
                String secText = sec < 10 ? "0" + sec : String.valueOf(sec);
                String left = minText + ":" + secText;
                countDownView.setText(left);
            }

            @Override
            public void onFinish() {
                String timeOutText = getResources().getString(R.string.otp_time_out);
                desc.setTextColor(getResources().getColor(R.color.color_FFA67C52));
                desc.setText(timeOutText);

                inputOtpView.forceInputPassword("");
                countDownView.setText("");
                sendOtpButton.requestFocus();
                inputOtpView.setEnabled(false);
                resetButton.setEnabled(false);
            }
        };
        return countDownTimer;
    }

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.reset_pin_send_otp) {
                if (otpId == null) {
                    showErrorDialog("", "loadOtpId error");
                    return;
                }

                desc.setText(getString(R.string.reset_pin_desc));
                desc.setTextColor(getResources().getColor(R.color.color_80E6E6E6));

                otp = null;
                loadOtp();
            } else if (id == R.id.reset_button) {
                String inputOtp = String.valueOf(inputOtpView.getPassword());
                if (Log.INCLUDE) {
                    Log.d(TAG, "resetButton onClick, inputOtp : " + inputOtp + ", otp : " + otp);
                }

                checkOtp();
            }
        }
    };

    private MbsDataTask otpIdTask;
    private MbsDataTask otpTask;
    private MbsDataTask checkTask;
    private MbsDataTask resetTask;
    private OtpId otpId;
    private OtpResponse.Otp otp;
    private OnResetPinListener onResetPinListener;

    private SettingAccountPinResetFragment() {
    }

    public static SettingAccountPinResetFragment newInstance() {
        return new SettingAccountPinResetFragment();
    }

    public void setOnResetPinListener(OnResetPinListener onResetPinListener) {
        this.onResetPinListener = onResetPinListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_setting_pin_reset, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        initLoadingBar(view);
        initInputOtp(view);
        initButtons(view);
        loadOtpId();
    }

    private void initLoadingBar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);
        showProgress();
    }

    private void initButtons(View view) {
        countDownView = view.findViewById(R.id.count_down);
        resetButton = view.findViewById(R.id.reset_button);
        cancelButton = view.findViewById(R.id.cancel_button);

        desc = view.findViewById(R.id.description);
        sendOtpButton = view.findViewById(R.id.reset_pin_send_otp);
        sendOtpButton.setOnKeyListener(this);
        sendOtpButton.setOnClickListener(mOnClickListener);
        resetButton.setOnClickListener(mOnClickListener);

        cancelButton.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        onResetPinListener.onResetComplete(false);
                        return true;
                    case KeyEvent.KEYCODE_DPAD_RIGHT:
                        return true;
                }

                return false;
            }
        });
    }

    private void initInputOtp(View view) {
        otpIdView = view.findViewById(R.id.otp_id);
        inputOtpView = view.findViewById(R.id.otp);
        inputOtpView.setEnabled(false);
        inputOtpView.setOnKeyListener(this);
        inputOtpView.setOnPasswordComplete(this);
    }

    private void loadOtpId() {
        otpIdTask = onResetPinListener.getSettingTaskManager().getOtpId(
                new MbsDataProvider<String, OtpId>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadOtpId, onFailed");
                        }
                        onResetPinListener.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, OtpId result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadOtpId, onSuccess, result : " + result);
                        }

                        otpId = result;
                        if (otpId.getOtpId() != null) {
                            otpIdView.setText(otpId.getOtpId());
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadOtpId, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        onResetPinListener.getSettingTaskManager().showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingAccountPinResetFragment.this.getContext();
                    }
                });
    }

    private void loadOtp() {
        if (Log.INCLUDE) {
            Log.d(TAG, "loadOtp, otpId : " + otpId);
        }

        otpTask = onResetPinListener.getSettingTaskManager().getOtp(
                otpId.getOtpId(),
                new MbsDataProvider<String, OtpResponse.Otp>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            sendOtpButton.setOnClickListener(null);
                            showProgress();
                        } else {
                            sendOtpButton.setOnClickListener(mOnClickListener);
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadOtp, onFailed");
                        }

                        onResetPinListener.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, OtpResponse.Otp result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadOtp, onSuccess, result : " + result);
                        }

                        otp = result;
                        long totalTime = otp.getExpireMinute() * TimeUtil.MIN;

                        inputOtpView.clear();
                        inputOtpView.setEnabled(true);
                        inputOtpView.requestFocus();
                        startTimer(totalTime);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadOtp, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        onResetPinListener.getSettingTaskManager().showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingAccountPinResetFragment.this.getContext();
                    }
                });
    }

    private void checkOtp() {
        if (Log.INCLUDE) {
            Log.d(TAG, "checkOtp");
        }

        String inputOtp = String.valueOf(inputOtpView.getPassword());
        checkTask = onResetPinListener.getSettingTaskManager().checkOtp(
                otpId.getOtpId(), inputOtp,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            resetButton.setOnClickListener(null);
                            showProgress();
                        } else {
                            resetButton.setOnClickListener(mOnClickListener);
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkOtp, onFailed");
                        }

                        onResetPinListener.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean isSuccess) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkOtp, onSuccess, isSuccess : " + isSuccess);
                        }

                        if (isSuccess) {
                            resetPinCode();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkOtp, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        String matchErrorText = getResources().getString(R.string.otp_match_error);
                        desc.setTextColor(getResources().getColor(R.color.color_FFA67C52));
                        desc.setText(matchErrorText);
                        inputOtpView.forceInputPassword("");
                        inputOtpView.requestFocus();
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingAccountPinResetFragment.this.getContext();
                    }
                });
    }

    private void resetPinCode() {
        resetTask = onResetPinListener.getSettingTaskManager().resetAccountPinCode(
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "resetPinCode, onFailed");
                        }
                        onResetPinListener.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "resetPinCode, onSuccess, result : " + result);
                        }

                        onResetPinListener.onResetComplete(true);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "resetPinCode, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        onResetPinListener.getSettingTaskManager().showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingAccountPinResetFragment.this.getContext();
                    }
                });
    }

    private void showErrorDialog(String errorCode, String message) {
        ErrorDialogFragment dialogFragment =
                ErrorDialogFragment.newInstance(TAG, errorCode, message);
        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
    }

    private void startTimer(long totalCount) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }

        countDownTimer = generateCountDownTimer(totalCount);
        countDownTimer.start();
    }

    public void showProgress() {
        progressBarManager.show();
    }

    public void hideProgress() {
        progressBarManager.hide();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        int id = v.getId();
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_DOWN:
                if (resetButton.isEnabled()) resetButton.requestFocus();
                else cancelButton.requestFocus();
                return true;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (id == R.id.reset_pin_send_otp && !inputOtpView.isEnabled()) {
                    return true;
                }
                break;
        }

        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        onResetPinListener = null;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
//        inputOtpView.removeTextChangedListener(otpTextWatcher);

        if (progressBarManager != null) {
            progressBarManager.hide();
            progressBarManager.setRootView(null);
        }

        if (otpIdTask != null) {
            otpIdTask.cancel(true);
        }

        if (otpTask != null) {
            otpTask.cancel(true);
        }

        if (checkTask != null) {
            checkTask.cancel(true);
        }

        if (resetTask != null) {
            resetTask.cancel(true);
        }
    }

    @Override
    public void notifyInputChange(boolean isFull, String input) {
        if (Log.INCLUDE) {
            Log.d(TAG, "input : " + input);
        }

        boolean isEnable = !input.isEmpty();
        resetButton.setEnabled(isEnable);
    }

    public interface OnResetPinListener {
        void onResetComplete(boolean isSuccess);

        SettingTaskManager getSettingTaskManager();
    }
}
