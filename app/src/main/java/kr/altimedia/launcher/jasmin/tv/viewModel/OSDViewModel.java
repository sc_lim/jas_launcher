/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.viewModel;

import android.app.Activity;
import android.app.Application;

import androidx.databinding.Observable;
import androidx.lifecycle.ViewModel;

import kr.altimedia.launcher.jasmin.tv.observable.ChannelLiveData;
import com.altimedia.tvmodule.dao.Channel;

public class OSDViewModel extends ViewModel {
    private Observable.OnPropertyChangedCallback mOnPropertyChangedCallback = null;
    private Activity mActivity = null;
    private Application mApplication = null;

    public OSDViewModel(Activity mActivity, Application mApplication) {
        this(null, mActivity, mApplication);
    }

    public OSDViewModel(Observable.OnPropertyChangedCallback mOnPropertyChangedCallback, Activity mActivity, Application mApplication) {
        this.mOnPropertyChangedCallback = mOnPropertyChangedCallback;
        this.mActivity = mActivity;
        this.mApplication = mApplication;
    }

    public final ChannelLiveData currentTvData = new ChannelLiveData();

    public void setValue(Channel channel) {
        currentTvData.setValue(channel);
        currentTvData.notifyPropertyChanged(0);
    }
}
