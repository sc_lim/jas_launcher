/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;

public class PasswordView extends RelativeLayout {
    public static final String CLASS_NAME = PasswordView.class.getName();
    private static final String TAG = PasswordView.class.getSimpleName();

    private OnPasswordComplete onPasswordComplete;

    //TODO
    public static final int SHOW_PASSWORD = 0;
    public static final int HIDE_PASSWORD = 1;
    public static final int OTP_PASSWORD = 2;

    private int mode = -1;

    public int PASSWORD_LENGTH = 4;
    private TextView passwordView;
    private ArrayList<CheckBox> viewList = new ArrayList<>();
    private RelativeLayout layout;

    private String password = "";

    public PasswordView(Context context) {
        super(context);
        initView();
    }

    public PasswordView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);
    }

    public PasswordView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        getAttrs(attrs, defStyleAttr);
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.common_password_view, this, false);
        addView(v);

        layout = v.findViewById(R.id.password_bg);

        passwordView = v.findViewById(R.id.visible_password);
        CheckBox input1 = v.findViewById(R.id.password1);
        CheckBox input2 = v.findViewById(R.id.password2);
        CheckBox input3 = v.findViewById(R.id.password3);
        CheckBox input4 = v.findViewById(R.id.password4);

        viewList.add(input1);
        viewList.add(input2);
        viewList.add(input3);
        viewList.add(input4);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.PasswordViewTheme);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.PasswordViewTheme, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        int mode = typedArray.getInt(R.styleable.PasswordViewTheme_password_mode, HIDE_PASSWORD);
        int hideDrawable = typedArray.getResourceId(R.styleable.PasswordViewTheme_password_hide_drawable, R.drawable.selector_password);
        int bgDrawable = typedArray.getResourceId(R.styleable.PasswordViewTheme_password_bg_drawable, R.drawable.selector_password_bg);

        int textSize = typedArray.getResourceId(R.styleable.PasswordViewTheme_password_text_size, R.dimen.digit_text_view_text_size);
        int textColor = typedArray.getResourceId(R.styleable.PasswordViewTheme_password_text_color, R.color.color_FFFFFFFF);
        int textFont = typedArray.getResourceId(R.styleable.PasswordViewTheme_password_text_font, R.font.font_prompt_medium);
        int size = (int) getResources().getDimension(textSize);

        if (mode != OTP_PASSWORD) {
            for (CheckBox view : viewList) {
                view.setChecked(false);
                view.setForeground(getResources().getDrawable(hideDrawable));
                view.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
                view.setTextColor(getResources().getColorStateList(textColor, null));
                view.setTypeface(getResources().getFont(textFont), Typeface.NORMAL);
            }
        } else {
            passwordView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
            passwordView.setTextColor(getResources().getColorStateList(textColor, null));
            passwordView.setTypeface(getResources().getFont(textFont), Typeface.NORMAL);
        }

        setMode(mode);
        layout.setBackgroundResource(bgDrawable);

        typedArray.recycle();
    }

    public void setMode(int mode) {
        if (this.mode == mode) {
            return;
        }

        this.mode = mode;

        initPassword();
    }

    private void initPassword() {
        switch (mode) {
            case SHOW_PASSWORD:
            case HIDE_PASSWORD:
                for (CheckBox view : viewList) {
                    view.setText("");
                    view.setChecked(false);
                }
                passwordView.setVisibility(View.INVISIBLE);
                break;
            case OTP_PASSWORD:
                inputPassword("");
                break;
        }
    }

    public int getMode() {
        return mode;
    }

    public String getPassword() {
        return password;
    }

    public void forceInputPassword(String input) {
        password = input;

        if (mode == OTP_PASSWORD) {
            passwordView.setText(password);
        } else {
            int size = password.length();
            switch (mode) {
                case SHOW_PASSWORD:
                    for (int i = 0; i < viewList.size(); i++) {
                        CheckBox checkBox = viewList.get(i);
                        if (i < size) {
                            checkBox.setText(password.indexOf(i));
                            checkBox.setActivated(true);
                        } else {
                            checkBox.setText("");
                            checkBox.setActivated(false);
                        }
                    }
                    break;
                case HIDE_PASSWORD:
                    for (int i = 0; i < viewList.size(); i++) {
                        boolean isChecked = i < size;
                        CheckBox checkBox = viewList.get(i);
                        checkBox.setChecked(isChecked);
                    }
                    break;
            }
        }


        if (onPasswordComplete != null) {
            notifyChange();
        }
    }

    private void inputPassword(CharSequence charSequence) {
        if (password.length() >= PASSWORD_LENGTH) {
            return;
        }

        password += charSequence;

        if (mode == OTP_PASSWORD) {
            passwordView.setText(password);
        } else {
            int last = password.length() - 1;
            CheckBox view = viewList.get(last);

            switch (mode) {
                case SHOW_PASSWORD:
                    view.setText(charSequence);
                    view.setActivated(true);
                    break;
                case HIDE_PASSWORD:
                    view.setChecked(true);
                    break;
            }

        }

        if (onPasswordComplete != null) {
            notifyChange();
        }
    }

    private boolean deletePassword() {
        int size = password.length();
        if (size <= 0) {
            return false;
        }

        int deletedIndex = size - 1;
        password = password.substring(0, deletedIndex);

        if (mode == OTP_PASSWORD) {
            passwordView.setText(password);
        } else {
            CheckBox view = viewList.get(deletedIndex);
            switch (mode) {
                case SHOW_PASSWORD:
                    view.setText("");
                    view.setActivated(false);
                    break;
                case HIDE_PASSWORD:
                    view.setChecked(false);
                    break;
            }
        }

        if (onPasswordComplete != null) {
            notifyChange();
        }

        return true;
    }

    public void clear() {
        password = "";

        initPassword();
        notifyChange();
    }

    public boolean isFull() {
        return password.length() == PASSWORD_LENGTH;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();

        if (action != KeyEvent.ACTION_DOWN) {
            return super.dispatchKeyEvent(event);
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_0:
                inputPassword("0");
                return true;
            case KeyEvent.KEYCODE_1:
                inputPassword("1");
                return true;
            case KeyEvent.KEYCODE_2:
                inputPassword("2");
                return true;
            case KeyEvent.KEYCODE_3:
                inputPassword("3");
                return true;
            case KeyEvent.KEYCODE_4:
                inputPassword("4");
                return true;
            case KeyEvent.KEYCODE_5:
                inputPassword("5");
                return true;
            case KeyEvent.KEYCODE_6:
                inputPassword("6");
                return true;
            case KeyEvent.KEYCODE_7:
                inputPassword("7");
                return true;
            case KeyEvent.KEYCODE_8:
                inputPassword("8");
                return true;
            case KeyEvent.KEYCODE_9:
                inputPassword("9");
                return true;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                return deletePassword();
        }

        return super.dispatchKeyEvent(event);
    }

    @Override
    public void setOnKeyListener(OnKeyListener l) {
        layout.setOnKeyListener(l);
    }

    @Override
    public boolean isEnabled() {
        return layout.isEnabled();
    }

    public void setEnabled(boolean enabled) {
        layout.setEnabled(enabled);

        if (!enabled) {
            clear();
        }
    }

    public void setEnabled(boolean enabled, float alpha) {
        for (CheckBox view : viewList) {
            view.setChecked(false);
        }

        layout.setAlpha(alpha);
        layout.setEnabled(enabled);

        if (!enabled) {
            clear();
        }
    }

    private void notifyChange() {
        if (onPasswordComplete != null) {
            onPasswordComplete.notifyInputChange(isFull(), password);
        }
    }

    public void setOnPasswordComplete(OnPasswordComplete onPasswordComplete) {
        this.onPasswordComplete = onPasswordComplete;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        onPasswordComplete = null;
        layout.setOnKeyListener(null);
    }

    public interface OnPasswordComplete {
        void notifyInputChange(boolean isFull, String input);
    }
}
