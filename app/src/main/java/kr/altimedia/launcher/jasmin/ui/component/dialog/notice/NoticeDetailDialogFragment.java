package kr.altimedia.launcher.jasmin.ui.component.dialog.notice;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.Map;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.view.common.ScrollDescriptionTextButton;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;

public class NoticeDetailDialogFragment {
    public static final String CLASS_NAME = NoticeDetailDialogFragment.class.getName();
    private final String TAG = NoticeDetailDialogFragment.class.getSimpleName();

    private final int MAX_LINE_COUNT = 7;

    private WindowManager wm;

    private Context context;
    private View parentsView;

    private Map<String, String> noticeMap;

    public NoticeDetailDialogFragment(Context context, Map<String, String> noticeMap) {
        this.context = context;
        this.noticeMap = noticeMap;

        LayoutInflater inflater = LayoutInflater.from(context);
        parentsView = inflater.inflate(R.layout.dialog_notice_detail, null, false);

        initView(parentsView);
    }

    private void initView(View view) {
        TextView titleView = view.findViewById(R.id.title);
        TextView dateView = view.findViewById(R.id.date);
        TextView descView = view.findViewById(R.id.description);

        titleView.setText(noticeMap.get(PushMessageReceiver.KEY_MESSAGE_TITLE));
        dateView.setText(noticeMap.get(PushMessageReceiver.KEY_DATE));
        descView.setText(noticeMap.get(PushMessageReceiver.KEY_MESSAGE_DESC));

        ScrollDescriptionTextButton close = view.findViewById(R.id.close);
        close.setDescriptionTextView(descView);
        close.setOnButtonKeyListener(new ScrollDescriptionTextButton.OnButtonKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        dismiss();
                        return true;
                }
                return false;
            }
        });

        ThumbScrollbar mThumbScrollbar = view.findViewById(R.id.scrollbar);
        descView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                mThumbScrollbar.setList(descView.getLineCount(), MAX_LINE_COUNT);
            }
        });

        descView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                mThumbScrollbar.moveScroll((scrollY > oldScrollY));
            }
        });

        view.findViewById(R.id.layout).setVisibility(View.VISIBLE);
    }

    public void show() {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                PixelFormat.TRANSLUCENT);
        mParams.gravity = Gravity.CENTER;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.addView(parentsView, mParams);
            }
        });
    }

    private void dismiss() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.removeView(parentsView);
            }
        });
    }
}