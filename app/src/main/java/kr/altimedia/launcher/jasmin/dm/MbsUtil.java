/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.dm;

import android.util.Pair;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.cwmp.service.def.CWMPEtcCode;
import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mc.kim on 18,05,2020
 */
public class MbsUtil {
    private static final String WRONG_SAID = "MBS_0002";

    private final static String TAG = MbsUtil.class.getSimpleName();
    private final static String KEY_RETURN_CODE = "returnCode";
    private final static String VALUE_RETURN_CODE_SUCCESS = "S";
    private final static String KEY_ERROR_CODE = "errorCode";
    private final static String KEY_ERROR_MESSAGE = "errorMessage";

    enum ResponseType {
        Connected, NotConnected, NotResponse, JsonException
    }

    private static HashMap<Pair<ServerType, ResponseType>, String> errorCodeMap = new HashMap<>();

    static {
        errorCodeMap.put(new Pair<>(ServerType.MBS, ResponseType.NotConnected), "LNC_0001");
        errorCodeMap.put(new Pair<>(ServerType.MBS, ResponseType.NotResponse), "LNC_0002");
        errorCodeMap.put(new Pair<>(ServerType.MBS, ResponseType.JsonException), "LNC_0020");
        errorCodeMap.put(new Pair<>(ServerType.SEARCH, ResponseType.NotConnected), "LNC_0003");
        errorCodeMap.put(new Pair<>(ServerType.SEARCH, ResponseType.NotResponse), "LNC_0004");
        errorCodeMap.put(new Pair<>(ServerType.SEARCH, ResponseType.JsonException), "LNC_0020");
        errorCodeMap.put(new Pair<>(ServerType.ADDS, ResponseType.NotConnected), "LNC_0014");
        errorCodeMap.put(new Pair<>(ServerType.ADDS, ResponseType.NotResponse), "LNC_0010");
        errorCodeMap.put(new Pair<>(ServerType.ADDS, ResponseType.JsonException), "LNC_0020");
        errorCodeMap.put(new Pair<>(ServerType.REC, ResponseType.NotConnected), "LNC_0011");
        errorCodeMap.put(new Pair<>(ServerType.REC, ResponseType.NotResponse), "LNC_0012");
        errorCodeMap.put(new Pair<>(ServerType.REC, ResponseType.JsonException), "LNC_0020");
    }

    enum ServerType {
        MBS(ServerConfig.MBS_URL, MbsResponseException.class),
        SEARCH(ServerConfig.SEARCH_URL, SearchResponseException.class),
        ADDS(ServerConfig.AD_URL, AddsResponseException.class),
        REC(ServerConfig.RECOMMEND_URL, RecResponseException.class),
        INVALID("", ResponseException.class);
        private final String value;
        private final Class classType;

        ServerType(String value, Class classType) {
            this.value = value;
            this.classType = classType;
        }

        private static ServerType findConfigByUrl(String url) {
            ServerType[] types = values();
            for (ServerType type : types) {
                if (url.startsWith(type.value)) {
                    return type;
                }
            }
            return INVALID;
        }
    }

    private static String getErrorCode(ServerType serverType, ResponseType responseType) {
        String errorCode = errorCodeMap.get(new Pair<>(serverType, responseType));
        if (StringUtils.nullToEmpty(errorCode).isEmpty()) {
            return "";
        }
        return errorCode;
    }

    private static String getErrorMessage(ServerType serverType, ResponseType responseType) {
        String errorCode = errorCodeMap.get(new Pair<>(serverType, responseType));
        return ErrorMessageManager.getInstance().getErrorMessage(errorCode);
    }

    private static boolean isInvalidSaId(String errorCode) {
        return errorCode.equalsIgnoreCase(WRONG_SAID);
    }

    public static Response checkConnectionValid(Call call) throws ResponseException, IOException {
        Response result = null;
        try {
            result = checkValidRequest(call);
        } catch (MbsResponseException e) {
            e.printStackTrace();
            if (e.isNetworkError()) {
                CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_341);
                throw new ResponseException(getErrorCode(e.getServerType(), e.getResponseType()), getErrorMessage(e.getServerType(), e.getResponseType()));
            } else {
                throw new ResponseException(e.getCode(), e.getMessage());
            }
        } catch (SearchResponseException e) {
            e.printStackTrace();
            if (e.isNetworkError()) {
                CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_301);
                throw new ResponseException(getErrorCode(e.getServerType(), e.getResponseType()), getErrorMessage(e.getServerType(), e.getResponseType()));
            } else {
                throw new ResponseException(e.getCode(), e.getMessage());
            }
        } catch (AddsResponseException e) {
            e.printStackTrace();
            if (e.isNetworkError()) {
                CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_303);
                throw new ResponseException(getErrorCode(e.getServerType(), e.getResponseType()), getErrorMessage(e.getServerType(), e.getResponseType()));
            } else {
                throw new ResponseException(e.getCode(), e.getMessage());
            }
        } catch (RecResponseException e) {
            e.printStackTrace();
            if (e.isNetworkError()) {
                CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_302);
                throw new ResponseException(getErrorCode(e.getServerType(), e.getResponseType()), getErrorMessage(e.getServerType(), e.getResponseType()));
            } else {
                throw new ResponseException(e.getCode(), e.getMessage());
            }
        }
        return result;
    }

    public static void checkValid(Response response) throws ResponseException, IOException, AuthenticationException {
        try {
            checkValidResponse(response);
        } catch (MbsResponseException e) {
            e.printStackTrace();
            if (e.isNetworkError()) {
                CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_341);
                throw new ResponseException(getErrorCode(e.getServerType(), e.getResponseType()), getErrorMessage(e.getServerType(), e.getResponseType()));
            } else {
                throw new ResponseException(e.getCode(), e.getMessage());
            }
        } catch (SearchResponseException e) {
            e.printStackTrace();
            if (e.isNetworkError()) {
                CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_301);
                throw new ResponseException(getErrorCode(e.getServerType(), e.getResponseType()), getErrorMessage(e.getServerType(), e.getResponseType()));
            } else {
                throw new ResponseException(e.getCode(), e.getMessage());
            }
        } catch (AddsResponseException e) {
            e.printStackTrace();
            if (e.isNetworkError()) {
                CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_303);
                throw new ResponseException(getErrorCode(e.getServerType(), e.getResponseType()), getErrorMessage(e.getServerType(), e.getResponseType()));
            } else {
                throw new ResponseException(e.getCode(), e.getMessage());
            }
        } catch (RecResponseException e) {
            e.printStackTrace();
            if (e.isNetworkError()) {
                CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_302);
                throw new ResponseException(getErrorCode(e.getServerType(), e.getResponseType()), getErrorMessage(e.getServerType(), e.getResponseType()));
            } else {
                throw new ResponseException(e.getCode(), e.getMessage());
            }
        }
    }

    private static Response checkValidRequest(Call call) throws MbsResponseException,
            SearchResponseException, AddsResponseException,
            RecResponseException, ResponseException {
        String url = call.request().url().toString();
        ServerType type = ServerType.findConfigByUrl(url);
        Response response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
            makeServerException(type, ResponseType.NotConnected, "", e.getMessage());
        } catch (JsonSyntaxException je) {
            je.printStackTrace();
            makeServerException(type, ResponseType.JsonException, "", je.getMessage());
        }
        return response;
    }

    private static void checkValidResponse(Response response) throws MbsResponseException,
            SearchResponseException, AddsResponseException,
            RecResponseException, IOException, ResponseException {
        String url = response.raw().request().url().toString();
        ServerType type = ServerType.findConfigByUrl(url);
        if (Log.INCLUDE) {
            Log.d(TAG, " type : " + type);
            Log.d(TAG, " url : " + url);
        }
        try {
            if (response.isSuccessful()) {
                String bodyString = new Gson().toJson(response.body());
                if (Log.INCLUDE) {
                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                    String strBody = gson.toJson(response.body());

                    Log.d(TAG, " body : " + strBody);
                }
                BaseResponse responseData = new Gson().fromJson(bodyString, BaseResponse.class);
                String resultCode = responseData.getReturnCode();
                if (resultCode.equalsIgnoreCase(VALUE_RETURN_CODE_SUCCESS)) {
                    return;
                }
                makeServerException(type, ResponseType.Connected, responseData.getErrorCode(), responseData.getErrorMessage());
            } else {
                JSONObject errorJson = new JSONObject(response.errorBody().string());
                String resultCode = errorJson.getString(KEY_RETURN_CODE);
                if (resultCode.equalsIgnoreCase(VALUE_RETURN_CODE_SUCCESS)) {
                    return;
                }
                makeServerException(type, ResponseType.Connected, errorJson.getString(KEY_ERROR_CODE), errorJson.getString(KEY_ERROR_MESSAGE));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            makeServerException(type, ResponseType.NotResponse, String.valueOf(response.code()), response.errorBody().toString());
        } catch (NullPointerException e) {
            e.printStackTrace();
            makeServerException(type, ResponseType.NotConnected, String.valueOf(response.code()), response.errorBody().toString());
        }
    }

    private static void makeServerException(ServerType type, ResponseType responseType, String errorCode, String errorMsg) throws MbsResponseException, SearchResponseException,
            AddsResponseException, ResponseException, RecResponseException {
        switch (type) {
            case MBS:
                throw new MbsResponseException(responseType, errorCode, errorMsg);
            case SEARCH:
                throw new SearchResponseException(responseType, errorCode, errorMsg);
            case ADDS:
                throw new AddsResponseException(responseType, errorCode, errorMsg);
            case REC:
                throw new RecResponseException(responseType, errorCode, errorMsg);
            case INVALID:
                throw new ResponseException(errorCode, errorMsg);
        }
    }
}
