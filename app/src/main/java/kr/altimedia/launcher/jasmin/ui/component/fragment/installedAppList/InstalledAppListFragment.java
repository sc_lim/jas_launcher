/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.installedAppList;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.SinglePresenterSelector;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.ui.component.fragment.interactor.LaunchPath;
import kr.altimedia.launcher.jasmin.ui.component.fragment.interactor.OnAppLaunchListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.OnMenuInteractor;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.AppPresenter;
import kr.altimedia.launcher.jasmin.ui.view.util.InstalledAppUtil;

public class InstalledAppListFragment extends Fragment implements AppPresenter.OnKeyListener {
    public static final String CLASS_NAME = InstalledAppListFragment.class.getName();
    private static final String TAG = InstalledAppListFragment.class.getSimpleName();

    private static final int NUMBER_COLUMNS = 5;
    private static final int NUMBER_ROWS = 4;

    private OnAppLaunchListener mOnAppLaunchListener = null;
    private OnMenuInteractor mOnMenuInteractor = null;

    private PagingVerticalGridView gridView;
    private InstalledAppListBridgeAdapter gridBridgeAdapter;

    private InstalledAppUtil installedAppUtil = new InstalledAppUtil();
    private ArrayList<InstalledAppUtil.AppInfo> sortedList;
    final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private AppSortMode sortMode = AppSortMode.SORT_ALPHABET;
    private int selectedSortIndex = -1;
    private ArrayList<InstalledAppUtil.AppInfo> appListByRecency;
    private ArrayObjectAdapter mRowsAdapter = new ArrayObjectAdapter();

    public static InstalledAppListFragment newInstance() {
        Bundle args = new Bundle();
        InstalledAppListFragment fragment = new InstalledAppListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAppLaunchListener) {
            mOnAppLaunchListener = (OnAppLaunchListener) context;
        }
        if (getParentFragment() instanceof OnMenuInteractor) {
            this.mOnMenuInteractor = (OnMenuInteractor) getParentFragment();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mOnAppLaunchListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_app_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initProgressbar(view);
        loadAppList();
//        initSortButton(view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (appAsyncTask != null) {
            appAsyncTask.cancel(true);
            appAsyncTask = null;
        }
    }

    public void clearData() {
        mRowsAdapter.clear();
    }

    public void renewalList() {
        clearData();
        loadAppList();
    }

    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
        mProgressBarManager.show();
    }


    private void loadList() {
        initAppList();
        if (sortedList.size() > 0) {
            mRowsAdapter.addAll(0, sortedList);
        }
        initAdapter();
    }


    private void initAdapter() {
        AppPresenter appPresenter = new AppPresenter(this);
        mRowsAdapter.setPresenterSelector(new SinglePresenterSelector(appPresenter));
        initView(getView(), mRowsAdapter);
    }

    private void initView(View view, ArrayObjectAdapter rowAdapter) {
        gridView = view.findViewById(R.id.gridView);
        gridView.initVertical(NUMBER_ROWS);
        gridView.setNumColumns(NUMBER_COLUMNS);
        gridView.setClipToPadding(false);

        int horizontalSpacing = (int) view.getResources().getDimension(R.dimen.app_horizontal_space);
        int verticalSpacing = (int) view.getResources().getDimension(R.dimen.app_vertical_space);
        gridView.setHorizontalSpacing(horizontalSpacing);
        gridView.setVerticalSpacing(verticalSpacing);

        gridBridgeAdapter = new InstalledAppListBridgeAdapter(rowAdapter, NUMBER_ROWS, NUMBER_COLUMNS,
                new InstalledAppListBridgeAdapter.OnBindListener() {
                    @Override
                    public void onBind(int index) {
                        if (sortMode != AppSortMode.SORT_EDIT && index == sortedList.size() - 1) {
//                            requestFocus(0);
                        }
                    }
                });

        gridView.setAdapter(gridBridgeAdapter);
    }

    /*private void initSortButton(View view) {
        TextView alphabetSortButton = view.findViewById(R.id.alphabet_sort_button);
        TextView userSortButton = view.findViewById(R.id.user_sort_button);

        Context context = view.getContext();
        String alphabet = context.getString(AppSortMode.SORT_ALPHABET.getTitle());
        String user = context.getString(AppSortMode.SORT_EDIT.getTitle());
        alphabetSortButton.setText(alphabet);
        userSortButton.setText(user);

        alphabetSortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sortMode != AppSortMode.SORT_ALPHABET) {
                    setMode(AppSortMode.SORT_ALPHABET);
                    gridBridgeAdapter.updateList(appListByAlphabet);
                }
            }
        });

        userSortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sortMode != AppSortMode.SORT_EDIT) {
                    setMode(AppSortMode.SORT_EDIT);
                    requestFocus(0);
                }
            }
        });
    }*/

    private void setMode(AppSortMode mode) {
        if (sortMode == AppSortMode.SORT_EDIT) {
            setUserEdit();
        }

        sortMode = mode;
    }

    private AppAsyncTask appAsyncTask = null;

    private void loadAppList() {
        appAsyncTask = new AppAsyncTask(new AppAsyncTask.AppTaskCallback() {
            @Override
            public Object onTaskBackGround() throws IOException, ResponseException, JSONException {
                return getAppListByRecency();
            }

            @Override
            public void onTaskPostExecute(Object result) {
                appListByRecency = (ArrayList<InstalledAppUtil.AppInfo>) result;
                if (!appListByRecency.isEmpty()) {
                    mOnMenuInteractor.closeSideMenu(false, true);
                }
                mProgressBarManager.hide();
                loadList();
            }

            @Override
            public Object onCanceled() {
                mProgressBarManager.hide();
                return null;
            }
        });
        appAsyncTask.execute();
    }

    private void initAppList() {
        ArrayList<InstalledAppUtil.AppInfo> gameList = new ArrayList<>();
        ArrayList<InstalledAppUtil.AppInfo> others = new ArrayList<>();

        for (InstalledAppUtil.AppInfo info : appListByRecency) {
//            if (info.getCategory() == InstalledAppUtil.CATEGORY_GAME) {
//                gameList.add(info);
//            } else {
            others.add(info);
//            }
        }

        sortedList = new ArrayList<>();
        sortedList.addAll(others);
        sortedList.addAll(gameList);
    }

    private void requestFocus(int index) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestFocus : " + index);
        }

        gridView.setSelectedPosition(index);
    }

    private ArrayList<InstalledAppUtil.AppInfo> getAppListByRecency() {
        return installedAppUtil.getInstalledAppList(getContext(), true, AppSortMode.SORT_RECENCY);
    }

    private void setUserEdit() {
        ArrayList<InstalledAppUtil.AppInfo> apps = gridBridgeAdapter.getList();
        installedAppUtil.setInstalledAppListByUserEdit(apps);
    }

    @Override
    public boolean onKey(KeyEvent event, int index, InstalledAppUtil.AppInfo appInfo) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onKey, keyCode : " + event.getKeyCode() + ", index : " + index);
        }

        if (sortMode == AppSortMode.SORT_EDIT) {
            boolean isConsumed = onKeyToEdit(event.getKeyCode(), index);
            if (Log.INCLUDE) {
                Log.d(TAG, "onKey, onKeyToEdit isConsumed : " + isConsumed);
            }

            if (isConsumed) {
                return true;
            }
        }

        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                mOnAppLaunchListener.launchApp(LaunchPath.AppLIst, appInfo.getPkgName());
                return true;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (index % NUMBER_COLUMNS == 0) {
                    if (event.getRepeatCount() > 0) {
                        return true;
                    }
                    if (mOnMenuInteractor != null) {
                        mOnMenuInteractor.openSideMenu(true);
                        return true;
                    }
                }

                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if ((index + 1) % NUMBER_COLUMNS == 0) {
                    requestFocus(index + 1);
                    return true;
                } else if (index == sortedList.size() - 1) {
                    requestFocus(0);
                    return true;
                }

                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                int size = sortedList.size();
                int moveIndex = index + NUMBER_COLUMNS;
                int lastRow = gridView.getRowIndex(size - 1);
                int row = gridView.getRowIndex(moveIndex);

                if (row > lastRow) {
                    requestFocus(0);
                    return true;
                } else if (moveIndex >= size) {
                    requestFocus(size - 1);
                    return true;
                }

                break;
            case KeyEvent.KEYCODE_BACK:
                if (mOnMenuInteractor != null) {
                    mOnMenuInteractor.openSideMenu(true);
                    return true;
                }

                break;
        }

        return false;
    }

    private boolean onKeyToEdit(int keyCode, int index) {
        if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
            if (selectedSortIndex >= 0) {
                selectedSortIndex = -1;
            } else {
                selectedSortIndex = index;
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "onKeyToEdit, selectedSortIndex : " + selectedSortIndex);
            }

            return true;
        }

        //there is no selected Item
        if (selectedSortIndex < 0) {
            return false;
        }

        int moveIndex = -1;
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                moveIndex = selectedSortIndex - 1;
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                moveIndex = selectedSortIndex + 1;
                break;
            case KeyEvent.KEYCODE_DPAD_UP:
                moveIndex = selectedSortIndex - NUMBER_COLUMNS;
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                moveIndex = selectedSortIndex + NUMBER_COLUMNS;
                break;
            case KeyEvent.KEYCODE_BACK:
                selectedSortIndex = -1;
                return true;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "onKeyToEdit, from : " + selectedSortIndex + ", to : " + moveIndex);
        }

        if (moveIndex < 0 || moveIndex >= sortedList.size()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onKeyToEdit, return wrong index : " + moveIndex);
            }
            return true;
        }

        gridBridgeAdapter.onItemMove(selectedSortIndex, moveIndex);
        selectedSortIndex = moveIndex;

        if (Log.INCLUDE) {
            Log.d(TAG, "onKeyToEdit, selectedSortIndex : " + selectedSortIndex);
        }

        return true;
    }
}
