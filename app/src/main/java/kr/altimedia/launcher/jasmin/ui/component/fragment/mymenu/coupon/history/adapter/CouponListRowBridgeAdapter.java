/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.adapter;

import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;

public class CouponListRowBridgeAdapter extends ItemBridgeAdapter {

    private OnKeyListener onKeyListener;

    public CouponListRowBridgeAdapter(ObjectAdapter adapter, int visibleCount) {
        super(adapter);

        initPagingAdapter(adapter, visibleCount);
    }

    private void initPagingAdapter(ObjectAdapter adapter, int visibleCount) {
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) adapter;
        int originalSize = objectAdapter.size();

        int mod = originalSize % visibleCount;
        if (mod != 0) {
            int binSize = visibleCount - mod;
            for (int i = 0; i < binSize; i++) {
                objectAdapter.add(null);
            }
        }
    }

    public void setOnKeyListener(OnKeyListener onKeyListener) {
        this.onKeyListener = onKeyListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        if (onKeyListener != null) {
            Coupon coupon = (Coupon) viewHolder.getItem();
            Presenter.ViewHolder vh = viewHolder.getViewHolder();
            vh.view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    return onKeyListener.onKey(keyCode, coupon);
                }
            });
        }
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);

        Presenter.ViewHolder vh = viewHolder.getViewHolder();
        vh.view.setOnKeyListener(null);
    }

    public interface OnKeyListener {
        boolean onKey(int keyCode, Coupon coupon);
    }
}
