/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj;

import android.os.Parcel;
import android.os.Parcelable;

import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Bank;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;
import kr.altimedia.launcher.jasmin.dm.payment.type.ContentType;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class PurchaseInfo implements Parcelable {
    private ContentType contentType;
    private String title;
    private Product product;
    private PaymentType paymentType;
    private PaymentWrapper paymentWrapper;
    private CreditCard creditCard;
    private Bank bank;

    public PurchaseInfo(ContentType contentType, String title, Product product, PaymentType paymentType, PaymentWrapper paymentWrapper) {
        this.contentType = contentType;
        this.title = title;
        this.product = product;
        this.paymentType = paymentType;
        this.paymentWrapper = paymentWrapper;
    }

    protected PurchaseInfo(Parcel in) {
        contentType = ContentType.findByName(in.readString());
        title = (String) in.readValue(String.class.getClassLoader());
        product = (Product) in.readValue(Product.class.getClassLoader());
        String paymentCode = (String) in.readValue(String.class.getClassLoader());
        if (paymentCode != null && !paymentCode.isEmpty()) {
            paymentType = PaymentType.findByName(paymentCode);
        }
        paymentWrapper = (PaymentWrapper) in.readValue(PaymentWrapper.class.getClassLoader());
        creditCard = (CreditCard) in.readValue(CreditCard.class.getClassLoader());
        bank = (Bank) in.readValue(Bank.class.getClassLoader());
    }

    public static final Creator<PurchaseInfo> CREATOR = new Creator<PurchaseInfo>() {
        @Override
        public PurchaseInfo createFromParcel(Parcel in) {
            return new PurchaseInfo(in);
        }

        @Override
        public PurchaseInfo[] newArray(int size) {
            return new PurchaseInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contentType.getType());
        dest.writeValue(title);
        dest.writeValue(product);
        String paymentCode = paymentType != null ? paymentType.getCode() : null;
        dest.writeValue(paymentCode);
        dest.writeValue(paymentWrapper);
        dest.writeValue(creditCard);
        dest.writeValue(bank);
    }

    @Override
    public String toString() {
        return "PurchaseInfo{" +
                "contentType=" + contentType +
                ", title='" + title + '\'' +
                ", product=" + product +
                ", paymentType=" + paymentType +
                ", paymentWrapper=" + paymentWrapper +
                ", creditCard=" + creditCard +
                ", bank=" + bank +
                '}';
    }

    public ContentType getContentType() {
        return contentType;
    }

    public String getTitle() {
        return title;
    }

    public Product getProduct() {
        return product;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public PaymentWrapper getPaymentWrapper() {
        return paymentWrapper;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public Bank getBank() {
        return bank;
    }
}
