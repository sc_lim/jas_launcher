package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Bank;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;

public class BankingPresenter extends Presenter {
    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_banking, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((Bank) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private TextView bankName;
        private ImageView logo;

        public ViewHolder(View view) {
            super(view);

            initView(view);
        }

        private void initView(View view) {
            bankName = view.findViewById(R.id.name);
            logo = view.findViewById(R.id.logo);
        }

        public void setData(Bank bank) {
            String logoUri = bank.getBankLogo();
            if (logoUri != null && !logoUri.isEmpty()) {
                Glide.with(view.getContext())
                        .load(logoUri).diskCacheStrategy(DiskCacheStrategy.DATA)
                        .centerCrop()
                        .into(logo);
                logo.setVisibility(View.VISIBLE);
            }

            String name = AccountManager.getInstance().getLocalLanguage().equals("EN") ? bank.getBankEngName() : bank.getBankThiName();
            bankName.setText(name);
        }
    }
}
