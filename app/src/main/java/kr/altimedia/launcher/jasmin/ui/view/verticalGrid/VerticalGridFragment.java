/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.verticalGrid;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.OnChildLaidOutListener;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.BaseGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.VerticalGridPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.util.StateMachine;

public class VerticalGridFragment extends VerticalBaseGridFragment {
    static final String TAG = "VuxVerticalGridSupportFragment";

    private ObjectAdapter mAdapter;
    private VerticalGridPresenter mGridPresenter;
    VerticalGridPresenter.ViewHolder mGridViewHolder;
    OnItemViewSelectedListener mOnItemViewSelectedListener;
    private OnItemViewClickedListener mOnItemViewClickedListener;
    private int mSelectedPosition = -1;
    private View mHeaderView = null;

    /**
     * State to setEntranceTransitionState(false)
     */
    final StateMachine.State STATE_SET_ENTRANCE_START_STATE = new StateMachine.State("SET_ENTRANCE_START_STATE") {
        @Override
        public void run() {
            setEntranceTransitionState(false);
        }
    };

    public void setHeaderView(View mHeaderView) {
        this.mHeaderView = mHeaderView;
    }


    /**
     * Sets the grid presenter.
     */
    public void setGridPresenter(VerticalGridPresenter gridPresenter) {
        if (gridPresenter == null) {
            throw new IllegalArgumentException("Grid presenter may not be null");
        }
        mGridPresenter = gridPresenter;
        mGridPresenter.setOnItemViewSelectedListener(mViewSelectedListener);
        if (mOnItemViewClickedListener != null) {
            mGridPresenter.setOnItemViewClickedListener(mOnItemViewClickedListener);
        }
    }

    /**
     * Returns the grid presenter.
     */
    public VerticalGridPresenter getGridPresenter() {
        return mGridPresenter;
    }

    /**
     * Sets the object adapter for the fragment.
     */
    public void setAdapter(ObjectAdapter adapter) {
        mAdapter = adapter;
        updateAdapter();
    }

    /**
     * Returns the object adapter.
     */
    public ObjectAdapter getAdapter() {
        return mAdapter;
    }

    final private OnItemViewSelectedListener mViewSelectedListener =
            new OnItemViewSelectedListener() {

                @Override
                public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder rowViewHolder, Row row) {
                    int position = mGridViewHolder.getGridView().getSelectedPosition();
                    if (Log.INCLUDE) {
                        Log.v(TAG, "grid selected position " + position);
                    }
                    gridOnItemSelected(position);
                    if (mOnItemViewSelectedListener != null) {
                        mOnItemViewSelectedListener.onItemSelected(itemViewHolder, item,
                                rowViewHolder, row);
                    }
                }
            };

    final private OnChildLaidOutListener mChildLaidOutListener =
            new OnChildLaidOutListener() {
                @Override
                public void onChildLaidOut(ViewGroup parent, View view, int position, long id) {
                    if (position == 0) {
                        showOrHideTitle();
                    }
                }
            };

    /**
     * Sets an item selection listener.
     */
    public void setOnItemViewSelectedListener(OnItemViewSelectedListener listener) {
        mOnItemViewSelectedListener = listener;
    }

    void gridOnItemSelected(int position) {
        if (position != mSelectedPosition) {
            mSelectedPosition = position;
            showOrHideTitle();
        }
    }


    void showOrHideTitle() {
        if (mGridViewHolder.getGridView().findViewHolderForAdapterPosition(mSelectedPosition)
                == null) {
            return;
        }
        if (!mGridViewHolder.getGridView().hasPreviousViewInSameRow(mSelectedPosition)) {
            showTitle(true);
            if (mHeaderView != null) {
                mHeaderView.setVisibility(View.VISIBLE);
            }
        } else {
            showTitle(false);
            if (mHeaderView != null) {
                mHeaderView.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Sets an item clicked listener.
     */
    public void setOnItemViewClickedListener(OnItemViewClickedListener listener) {
        mOnItemViewClickedListener = listener;
        if (mGridPresenter != null) {
            mGridPresenter.setOnItemViewClickedListener(mOnItemViewClickedListener);
        }
    }

    protected void replaceFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(android.R.id.content, fragment);
        ft.commit();
    }

    protected void addFragment(Fragment fragment, String tag) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(android.R.id.content, fragment);
        ft.addToBackStack(tag);
        ft.commit();
    }

    protected void popBackStack(String name, int flag) {
        FragmentManager fm = getFragmentManager();
        fm.popBackStack(name, flag);
    }

    /**
     * Returns the item clicked listener.
     */
    public OnItemViewClickedListener getOnItemViewClickedListener() {
        return mOnItemViewClickedListener;
    }

    private boolean mExpand = false;

    public void setExpand(boolean expand) {
        mExpand = expand;
        if (mGridViewHolder == null) {
            return;
        }
        VerticalGridView listView = mGridViewHolder.getGridView();
        if (listView != null) {
            final int count = listView.getChildCount();
            if (Log.INCLUDE) {
                Log.v(TAG, "setExpand " + expand + " count " + count);
            }
            for (int i = 0; i < count; i++) {
                View view = listView.getChildAt(i);
                ItemBridgeAdapter.ViewHolder vh =
                        (ItemBridgeAdapter.ViewHolder) listView.getChildViewHolder(view);
                setRowViewExpanded(vh, mExpand);
            }
        }
    }

    static void setRowViewExpanded(ItemBridgeAdapter.ViewHolder vh, boolean expanded) {
        ((RowPresenter) vh.getPresenter()).setRowViewExpanded(vh.getViewHolder(), true);
    }

    private int mResourceId = R.layout.fragment_contents;

    private int getResourceId() {
        return mResourceId;
    }

    public void setResourceId(int mResourceId) {
        this.mResourceId = mResourceId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(getResourceId(),
                container, false);
        ViewGroup gridFrame = root.findViewById(R.id.grid_frame);
        installTitleView(inflater, gridFrame, savedInstanceState);
        getProgressBarManager().setRootView(root);

        ViewGroup gridDock = root.findViewById(R.id.browse_grid_dock);
        mGridViewHolder = mGridPresenter.onCreateViewHolder(gridDock);
        gridDock.addView(mGridViewHolder.view);
        mGridViewHolder.getGridView().setOnChildLaidOutListener(mChildLaidOutListener);
        mGridViewHolder.getGridView().setOnKeyInterceptListener(new BaseGridView.OnKeyInterceptListener() {
            @Override
            public boolean onInterceptKeyEvent(KeyEvent event) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onInterceptKeyEvent");
                }
                return false;
            }
        });
        mGridViewHolder.getGridView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onKey");
                }
                return false;
            }
        });
        updateAdapter();
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mGridViewHolder = null;
    }

    /**
     * Sets the selected item position.
     */
    public void setSelectedPosition(int position) {
        mSelectedPosition = position;
        if (mGridViewHolder != null && mGridViewHolder.getGridView().getAdapter() != null) {
            mGridViewHolder.getGridView().setSelectedPositionSmooth(position);
        }
    }

    private void updateAdapter() {
        if (mGridViewHolder != null) {
            mGridPresenter.onBindViewHolder(mGridViewHolder, mAdapter);
            if (mSelectedPosition != -1) {
                mGridViewHolder.getGridView().setSelectedPosition(mSelectedPosition);
            }
        }
        ItemBridgeAdapter adapter = getBridgeAdapter();
        if (adapter != null) {
            adapter.setAdapterListener(mBridgeAdapterListener);
        }
    }

    public ItemBridgeAdapter getBridgeAdapter() {
        if (mGridViewHolder == null) {
            return null;
        }
        return mGridViewHolder.getItemBridgeAdapter();
    }

    public VerticalGridView getVerticalGridView() {
        if (mGridViewHolder == null) {
            return null;
        }
        return mGridViewHolder.getGridView();
    }

    public void setEntranceTransitionState(boolean afterTransition) {
        mGridPresenter.setEntranceTransitionState(mGridViewHolder, afterTransition);
    }

    private final ItemBridgeAdapter.AdapterListener mBridgeAdapterListener =
            new ItemBridgeAdapter.AdapterListener() {
                @Override
                public void onAddPresenter(Presenter presenter, int type) {
                }

                @Override
                public void onCreate(ItemBridgeAdapter.ViewHolder vh) {
                    VerticalGridView listView = getVerticalGridView();
                    if (listView != null) {
                        // set clip children false for slide animation
                        listView.setClipChildren(false);
                    }
                    // selected state is initialized to false, then driven by grid view onChildSelected
                    // events.  When there is rebind, grid view fires onChildSelected event properly.
                    // So we don't need do anything special later in onBind or onAttachedToWindow.
                }

                @Override
                public void onAttachedToWindow(ItemBridgeAdapter.ViewHolder vh) {
                    if (DEBUG) Log.v(TAG, "onAttachToWindow");
                    // All views share the same mExpand value.  When we attach a view to grid view,
                    // we should make sure it pick up the latest mExpand value we set early on other
                    // attached views.  For no-structure-change update,  the view is rebound to new data,
                    // but again it should use the unchanged mExpand value,  so we don't need do any
                    // thing in onBind.
                    setRowViewExpanded(vh, mExpand);
                }

                @Override
                public void onDetachedFromWindow(ItemBridgeAdapter.ViewHolder vh) {
                }

                @Override
                public void onBind(ItemBridgeAdapter.ViewHolder vh) {

                }

                @Override
                public void onUnbind(ItemBridgeAdapter.ViewHolder vh) {
                }
            };

}
