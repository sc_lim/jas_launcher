/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;


abstract class BaseRowFragment extends Fragment {
    private static final String CURRENT_SELECTED_POSITION = "currentSelectedPosition";
    private ObjectAdapter mAdapter;
    VerticalGridView mVerticalGridView;
    private PresenterSelector mPresenterSelector;
    final ItemBridgeAdapter mBridgeAdapter = new ItemBridgeAdapter();
    int mSelectedPosition = -1;
    private boolean mPendingTransitionPrepare;
    BaseRowFragment.LateSelectionObserver mLateSelectionObserver = new BaseRowFragment.LateSelectionObserver();
    private final OnChildViewHolderSelectedListener mRowSelectedListener = new OnChildViewHolderSelectedListener() {
        public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder view, int position, int subposition) {
            if (!BaseRowFragment.this.mLateSelectionObserver.mIsLateSelection) {
                BaseRowFragment.this.mSelectedPosition = position;
                BaseRowFragment.this.onRowSelected(parent, view, position, subposition);
            }

        }
    };

    BaseRowFragment() {
    }

    abstract int getLayoutResourceId();

    void onRowSelected(RecyclerView parent, RecyclerView.ViewHolder view, int position, int subposition) {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.getLayoutResourceId(), container, false);
        this.mVerticalGridView = this.findGridViewFromRoot(view);
        if (this.mPendingTransitionPrepare) {
            this.mPendingTransitionPrepare = false;
            this.onTransitionPrepare();
        }

        return view;
    }

    VerticalGridView findGridViewFromRoot(View view) {
        return (VerticalGridView) view;
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            this.mSelectedPosition = savedInstanceState.getInt("currentSelectedPosition", -1);
        }

        this.setAdapterAndSelection();
        this.mVerticalGridView.setOnChildViewHolderSelectedListener(this.mRowSelectedListener);
    }

    void setAdapterAndSelection() {
        if (this.mAdapter != null) {
            if (this.mVerticalGridView.getAdapter() != this.mBridgeAdapter) {
                this.mVerticalGridView.setAdapter(this.mBridgeAdapter);
            }

            boolean lateSelection = this.mBridgeAdapter.getItemCount() == 0 && this.mSelectedPosition >= 0;
            if (lateSelection) {
                this.mLateSelectionObserver.startLateSelection();
            } else if (this.mSelectedPosition >= 0) {
                this.mVerticalGridView.setSelectedPosition(this.mSelectedPosition);
            }

        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.mLateSelectionObserver.clear();
        this.mVerticalGridView = null;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentSelectedPosition", this.mSelectedPosition);
    }

    public final void setPresenterSelector(PresenterSelector presenterSelector) {
        if (this.mPresenterSelector != presenterSelector) {
            this.mPresenterSelector = presenterSelector;
            this.updateAdapter();
        }

    }

    public final PresenterSelector getPresenterSelector() {
        return this.mPresenterSelector;
    }

    public final void setAdapter(ObjectAdapter rowsAdapter) {
        if (this.mAdapter != rowsAdapter) {
            this.mAdapter = rowsAdapter;
            this.updateAdapter();
        }

    }

    public final ObjectAdapter getAdapter() {
        return this.mAdapter;
    }

    public final ItemBridgeAdapter getBridgeAdapter() {
        return this.mBridgeAdapter;
    }

    public void setSelectedPosition(int position) {
        this.setSelectedPosition(position, true);
    }

    public int getSelectedPosition() {
        return this.mSelectedPosition;
    }

    public void setSelectedPosition(int position, boolean smooth) {
        if (this.mSelectedPosition != position) {
            this.mSelectedPosition = position;
            if (this.mVerticalGridView != null) {
                if (this.mLateSelectionObserver.mIsLateSelection) {
                    return;
                }

                if (smooth) {
                    this.mVerticalGridView.setSelectedPositionSmooth(position);
                } else {
                    this.mVerticalGridView.setSelectedPosition(position);
                }
            }

        }
    }

    public final VerticalGridView getVerticalGridView() {
        return this.mVerticalGridView;
    }

    void updateAdapter() {
        this.mBridgeAdapter.setAdapter(this.mAdapter);
        this.mBridgeAdapter.setPresenter(this.mPresenterSelector);
        if (this.mVerticalGridView != null) {
            this.setAdapterAndSelection();
        }

    }

    Object getItem(Row row, int position) {
        return row instanceof ListRow ? ((ListRow) row).getAdapter().get(position) : null;
    }

    public boolean onTransitionPrepare() {
        if (this.mVerticalGridView != null) {
            this.mVerticalGridView.setAnimateChildLayout(false);
            this.mVerticalGridView.setScrollEnabled(false);
            return true;
        } else {
            this.mPendingTransitionPrepare = true;
            return false;
        }
    }

    public void onTransitionStart() {
        if (this.mVerticalGridView != null) {
            this.mVerticalGridView.setPruneChild(false);
            this.mVerticalGridView.setLayoutFrozen(true);
            this.mVerticalGridView.setFocusSearchDisabled(true);
        }

    }

    public void onTransitionEnd() {
        if (this.mVerticalGridView != null) {
            this.mVerticalGridView.setLayoutFrozen(false);
            this.mVerticalGridView.setAnimateChildLayout(true);
            this.mVerticalGridView.setPruneChild(true);
            this.mVerticalGridView.setFocusSearchDisabled(false);
            this.mVerticalGridView.setScrollEnabled(true);
        }

    }

    public void setAlignment(int windowAlignOffsetTop) {
        if (this.mVerticalGridView != null) {
            this.mVerticalGridView.setItemAlignmentOffset(0);
            this.mVerticalGridView.setItemAlignmentOffsetPercent(-1.0F);
            this.mVerticalGridView.setWindowAlignmentOffset(windowAlignOffsetTop);
            this.mVerticalGridView.setWindowAlignmentOffsetPercent(-1.0F);
            this.mVerticalGridView.setWindowAlignment(0);
        }

    }

    private class LateSelectionObserver extends RecyclerView.AdapterDataObserver {
        boolean mIsLateSelection = false;

        LateSelectionObserver() {
        }

        public void onChanged() {
            this.performLateSelection();
        }

        public void onItemRangeInserted(int positionStart, int itemCount) {
            this.performLateSelection();
        }

        void startLateSelection() {
            this.mIsLateSelection = true;
            BaseRowFragment.this.mBridgeAdapter.registerAdapterDataObserver(this);
        }

        void performLateSelection() {
            this.clear();
            if (BaseRowFragment.this.mVerticalGridView != null) {
                BaseRowFragment.this.mVerticalGridView.setSelectedPosition(BaseRowFragment.this.mSelectedPosition);
            }

        }

        void clear() {
            if (this.mIsLateSelection) {
                this.mIsLateSelection = false;
                BaseRowFragment.this.mBridgeAdapter.unregisterAdapterDataObserver(this);
            }

        }
    }
}
