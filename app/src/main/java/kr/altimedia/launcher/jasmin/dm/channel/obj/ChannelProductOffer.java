/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChannelProductOffer implements Parcelable {
    public static final Parcelable.Creator<ChannelProductOffer> CREATOR = new Parcelable.Creator<ChannelProductOffer>() {
        @Override
        public ChannelProductOffer createFromParcel(Parcel in) {
            return new ChannelProductOffer(in);
        }

        @Override
        public ChannelProductOffer[] newArray(int size) {
            return new ChannelProductOffer[size];
        }
    };
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("offerDuration")
    private int offerDuration;
    @Expose
    @SerializedName("offerId")
    private String offerId;
    @Expose
    @SerializedName("price")
    private float price;

    protected ChannelProductOffer(Parcel in) {
        currency = in.readString();
        offerDuration = in.readInt();
        offerId = in.readString();
        price = in.readFloat();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(currency);
        dest.writeValue(offerDuration);
        dest.writeValue(offerId);
        dest.writeValue(price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "ChannelProductOffer{" +
                "currency=" + currency +
                ", offerDuration=" + offerDuration +
                ", offerId=" + offerId +
                ", price=" + price +
                '}';
    }

    public String getCurrency() {
        return currency;
    }

    public int getOfferDuration() {
        return offerDuration;
    }

    public String getOfferId() {
        return offerId;
    }

    public float getPrice() {
        return price;
    }
}