/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfile;

public class ProfilePresenter extends Presenter {

    private final int ALPHA_NORMAL = 153;
    private final int ALPHA_FOCUS = 255;

    private OnKeyListener onKeyListener;
    private OnFocusListener onFocusListener;

    public ProfilePresenter(OnKeyListener onKeyListener, OnFocusListener onFocusListener) {
        this.onKeyListener = onKeyListener;
        this.onFocusListener = onFocusListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ItemViewHolder viewHolder = new ItemViewHolder(inflater.inflate(R.layout.item_mymenu_profile, null, false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object object) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
        UserProfile item = (UserProfile) object;

        itemViewHolder.setItem(item);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
        itemViewHolder.dispose();
    }

    public OnKeyListener getOnKeyListener() {
        return onKeyListener;
    }

    public class ItemViewHolder extends ViewHolder implements View.OnFocusChangeListener  {
        private final String TAG = ItemViewHolder.class.getSimpleName();

        private UserProfile item;
        private LinearLayout profileLayer;
        private ImageView profileIcon;
        private ImageView selectedIcon;
        private ImageView lockIcon;
        private TextView profileName;
        private ImageView profileSetIcon;

        public ItemViewHolder(View view) {
            super(view);

            profileLayer = view.findViewById(R.id.profileLayer);
            profileIcon = view.findViewById(R.id.profileIcon);
            selectedIcon = view.findViewById(R.id.selectedIcon);
            lockIcon = view.findViewById(R.id.lockIcon);
            profileName = view.findViewById(R.id.profileName);
            profileSetIcon = view.findViewById(R.id.profileSetIcon);

            view.setOnFocusChangeListener(this);
        }

        public void dispose() {

        }

        public void setItem(UserProfile item) {
            this.item = item;

            if (item.isAddOption()) {
                lockIcon.setVisibility(View.GONE);
                profileIcon.setVisibility(View.GONE);
                profileSetIcon.setImageResource(R.drawable.profile_icon_add);
                profileSetIcon.setImageAlpha(ALPHA_NORMAL);
                profileSetIcon.setVisibility(View.VISIBLE);
                profileName.setText(view.getResources().getText(R.string.add));
                view.setSelected(false);
            } else if (item.isManageOption()) {
                lockIcon.setVisibility(View.GONE);
                profileIcon.setVisibility(View.GONE);
                profileSetIcon.setImageResource(R.drawable.profile_icon_manage);
                profileSetIcon.setImageAlpha(ALPHA_NORMAL);
                profileSetIcon.setVisibility(View.VISIBLE);
                profileName.setText(view.getResources().getText(R.string.manage));
                view.setSelected(false);
            } else {
                profileSetIcon.setVisibility(View.GONE);
                item.getProfileInfo().setProfileIcon(profileIcon);
                profileIcon.setVisibility(View.VISIBLE);
                profileName.setText(item.getName());
                if (item.isSelected()) {
                    view.setSelected(true);
                } else {
                    view.setSelected(false);
                }
                if(item.getProfileInfo().isLock()){
                    lockIcon.setVisibility(View.VISIBLE);
                }else{
                    lockIcon.setVisibility(View.GONE);
                }
            }

            if(view.isActivated() && !profileName.isActivated()){
                profileName.setActivated(true);
            }else if(!view.isActivated() && profileName.isActivated()){
                profileName.setActivated(false);
            }
        }

        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
                if (item.isAddOption() || item.isManageOption()) {
                    profileSetIcon.setImageAlpha(ALPHA_FOCUS);
                }
            }else{
                if (item.isAddOption() || item.isManageOption()) {
                    profileSetIcon.setImageAlpha(ALPHA_NORMAL);
                }
            }
            if(onFocusListener != null) {
                int index = (int) view.getTag(R.id.KEY_INDEX);
                onFocusListener.onFocus(hasFocus, view, index, item);
            }
        }
    }

    public interface OnKeyListener {
        boolean onKey(int keyCode, int index, Object item, ViewHolder viewHolder);
    }

    public interface OnFocusListener {
        void onFocus(boolean hasFocus, View view, int index, Object item);
    }
}