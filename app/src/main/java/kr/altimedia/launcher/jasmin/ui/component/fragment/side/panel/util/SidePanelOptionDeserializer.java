/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOption;

/**
 * Created by mc.kim on 20,02,2020
 */
public class SidePanelOptionDeserializer implements JsonDeserializer<SidePanelOption> {
    @Override
    public SidePanelOption deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return SidePanelOption.findByType(json.getAsString());
    }
}
