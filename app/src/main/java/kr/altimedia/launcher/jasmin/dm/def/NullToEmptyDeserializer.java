/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.def;

import android.os.Parcelable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import com.altimedia.tvmodule.util.StringUtils;

/**
 * Created by mc.kim on 01,07,2020
 */
public class NullToEmptyDeserializer implements
        JsonDeserializer<Parcelable> {

    @Override
    public Parcelable deserialize(JsonElement json, Type typeOfT,
                                  JsonDeserializationContext jsc)
            throws JsonParseException {
        if (json.isJsonObject()) {
            return jsc.deserialize(json, typeOfT);
        }
        if (StringUtils.nullToEmpty(json.getAsString()).isEmpty()) {
            return null;
        }
        return jsc.deserialize(json, typeOfT);
    }
}