/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponHomeInfo;

public class CouponHomeInfoResponse extends BaseResponse {
    public static final Creator<CouponHomeInfoResponse> CREATOR = new Creator<CouponHomeInfoResponse>() {
        @Override
        public CouponHomeInfoResponse createFromParcel(Parcel in) {
            return new CouponHomeInfoResponse(in);
        }

        @Override
        public CouponHomeInfoResponse[] newArray(int size) {
            return new CouponHomeInfoResponse[size];
        }
    };
    @Expose
    @SerializedName("data")
    private CouponHomeInfo couponHomeInfo;

    protected CouponHomeInfoResponse(Parcel in) {
        super(in);
        couponHomeInfo = in.readParcelable(CouponHomeInfo.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(couponHomeInfo, flags);
    }

    public CouponHomeInfo getCouponHomeInfo() {
        return couponHomeInfo;
    }
}
