package kr.altimedia.launcher.jasmin.ui.component.fragment.settings;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;

public abstract class SettingBaseProfileDialogFragment extends SettingBaseDialogFragment {
    public final String TAG = SettingBaseProfileDialogFragment.class.getSimpleName();

    protected abstract ProfileInfo getProfileInfo();

    @Override
    protected void receivePushProfile(String profileId, PushMessageReceiver.ProfileUpdateType type) {
        super.receivePushProfile(profileId, type);

        if (Log.INCLUDE) {
            Log.d(TAG, "receivePushProfile, profileId : " + profileId + ", type : " + type);
        }

        switch (type) {
            case rating:
            case blockedChannel:
            case lock:
            case delete:
                ProfileInfo profileInfo = getProfileInfo();
                String selectedProfileId = profileInfo != null ? profileInfo.getProfileId() : "";

                if (Log.INCLUDE) {
                    Log.d(TAG, "receivePushProfile, selectedProfileId : " + selectedProfileId);
                }

                if (!profileId.isEmpty() && selectedProfileId.equalsIgnoreCase(profileId)) {
                    updateProfile(type);
                }

                break;
        }
    }

    protected void updateProfile(PushMessageReceiver.ProfileUpdateType type) {

    }
}
