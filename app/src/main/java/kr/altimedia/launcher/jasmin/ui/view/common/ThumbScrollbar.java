/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import kr.altimedia.launcher.jasmin.R;

public class ThumbScrollbar extends RelativeLayout {
    private static final String TAG = ThumbScrollbar.class.getSimpleName();
    public static final String CLASS_NAME = ThumbScrollbar.class.getName();

    public static final int SCROLL_TYPE_EDGE = 0;
    public static final int SCROLL_TYPE_CENTER = 1;
    public static final int SCROLL_TYPE_PAGE = 2;

    private float height = 0;
    private float thumbHeight = 0;

    private int size = 0;
    private int visibleCount = 0;
    private int centerIndex = 0;
    private int scrollCount = 0;

    private View bg;
    private ImageView thumb;
    private int scrollType;

    private final OnLayoutChangeListener onLayoutChangeListener = new OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
            height = v.getMeasuredHeight();
            if (height > 0) {
                initThumb();
                v.removeOnLayoutChangeListener(onLayoutChangeListener);
            }
        }
    };

    public ThumbScrollbar(Context context) {
        super(context);
        initView();
    }

    public ThumbScrollbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);
    }

    public ThumbScrollbar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
        getAttrs(attrs, defStyle);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ThumbScrollbarTheme);
        setTypeArray(typedArray);
    }


    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ThumbScrollbarTheme, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        scrollType = typedArray.getInt(R.styleable.ThumbScrollbarTheme_thumb_scroll_type, SCROLL_TYPE_EDGE);

        int thumb_resID = typedArray.getResourceId(R.styleable.ThumbScrollbarTheme_thumb_scroll_thumb, R.drawable.scroll_w);
        thumb.setBackgroundResource(thumb_resID);

        int scrollbar_resID = typedArray.getResourceId(R.styleable.ThumbScrollbarTheme_thumb_scroll_bg, R.drawable.bg_transparent);
        bg.setBackgroundResource(scrollbar_resID);

        typedArray.recycle();
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.common_scrollbar, this, false);
        addView(v);

        bg = findViewById(R.id.bg);
        thumb = findViewById(R.id.thumb);

        getRootView().addOnLayoutChangeListener(onLayoutChangeListener);

        setVisibility(INVISIBLE);
    }

    public void setList(int totalLine, int maxLine) {
        this.size = totalLine;
        this.visibleCount = maxLine;
        this.centerIndex = visibleCount / 2;

        if (((float) totalLine / maxLine) > 1.0) {
            initThumb();
            setVisibility(VISIBLE);
        }
    }

    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        thumb.setOnKeyListener(onKeyListener);
        setFocusable(true);
    }

    @Override
    public void setFocusable(boolean focusable) {
        thumb.setFocusable(focusable);
    }

    @Override
    public void setFocusableInTouchMode(boolean focusableInTouchMode) {
        thumb.setFocusableInTouchMode(focusableInTouchMode);
    }

    // (scrollY > oldScrollY)
    public void moveScroll(boolean moveDown) {
        float currentY = thumb.getY();
        float y = moveDown ? currentY + thumbHeight : currentY - thumbHeight;
        thumb.setY(y);
    }

    // focusIndex
    public void moveFocus(int focusIndex) {
        float y = 0;

        switch (scrollType) {
            case SCROLL_TYPE_EDGE:
                y = thumbHeight * (focusIndex - visibleCount + 1);
                break;
            case SCROLL_TYPE_CENTER:
                y = thumbHeight * (focusIndex - centerIndex);
                break;
            case SCROLL_TYPE_PAGE:
                y = thumbHeight * (focusIndex / visibleCount);
                break;
        }

        if (y < 0) y = 0;
        if (y >= height) y = (thumbHeight * (scrollCount - 1));

        thumb.setY(y);
    }

    private void initThumb() {
        if (height <= 0 || size <= 0) {
            return;
        }

        switch (scrollType) {
            case SCROLL_TYPE_EDGE:
                scrollCount = size - visibleCount + 1;
                thumbHeight = height / scrollCount;
                break;
            case SCROLL_TYPE_CENTER:
                scrollCount = size / centerIndex + 1;
                thumbHeight = height / scrollCount;
                break;
            case SCROLL_TYPE_PAGE:
                int value = size / visibleCount;
                scrollCount = size % visibleCount > 0 ? value + 1 : value;
                thumbHeight = height / scrollCount;
                break;
        }

        ViewGroup.LayoutParams layoutParams = thumb.getLayoutParams();
        layoutParams.height = (int) thumbHeight;

        thumb.setLayoutParams(layoutParams);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        thumb.setOnKeyListener(null);
    }
}