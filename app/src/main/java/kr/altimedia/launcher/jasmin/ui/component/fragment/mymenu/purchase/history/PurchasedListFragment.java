/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.altimedia.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.PurchasedContent;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.Linker;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.OnDataLoadedListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history.adapter.PurchasedListBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history.presenter.PurchasedItemPresenter;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PURCHASE_INFORM;

public class PurchasedListFragment extends PurchasedBaseFragment implements OnDataLoadedListener {
    public static final String CLASS_NAME = PurchasedListFragment.class.getName();
    private static final String TAG = PurchasedListFragment.class.getSimpleName();

    private ArrayList<PurchasedContent> visibleList = new ArrayList<>();
    private List<WeakReference<AsyncTask>> taskList = new ArrayList<>();

    private boolean created = false;

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent == null) return;

            String action = intent.getAction();
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive: intent=" + intent.getAction());
            }
            if(action.equals(ACTION_UPDATED_PURCHASE_INFORM)) {
                loadData();
            }
        }
    };

    private PurchasedListFragment() {
        created = true;
    }

    public static PurchasedListFragment newInstance(ArrayList<PurchasedContent> pList, ArrayList<PurchasedContent> hList) {
        PurchasedListFragment fragment = new PurchasedListFragment();
        Bundle args = new Bundle();
        if (pList != null && pList.size() > 0) {
            args.putParcelableArrayList(PurchasedListDialog.KEY_PURCHASED_LIST, pList);
        }
        if (hList != null && hList.size() > 0) {
            args.putParcelableArrayList(PurchasedListDialog.KEY_HIDDEN_LIST, hList);
        }
        fragment.setArguments(args);
        return fragment;
    }

    private void clearThread() {
        try {
            for (WeakReference<AsyncTask> task : taskList) {
                if (task.get() != null) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "clearThread");
                    }
                    task.get().cancel(true);
                }
            }
            taskList.clear();
        }catch (Exception e){
        }
    }

    @Override
    public void onDestroyView() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onDestroyView");
        }
        clearThread();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(Log.INCLUDE){
            Log.d(TAG,"onViewCreated");
        }

        setButtonListener(view);

        loadData();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_UPDATED_PURCHASE_INFORM);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, filter);
    }

    private void setButtonListener(View view) {
        try {
            if (btnEdit != null) {
                btnEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onClick");
                        }
                        ((PurchasedListDialog) getParentFragment()).showView(PurchasedListDialog.TYPE_EDIT, purchasedList, hiddenList);
                    }
                });
                btnEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if (hasFocus) {
                            infoLayer.setVisibility(View.GONE);
                        }
                    }
                });
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void loadData() {

        if (Log.INCLUDE) {
            Log.d(TAG, "loadData");
        }

        showProgress();

        UserDataManager userDataManager = new UserDataManager();
        MbsDataTask dataTask = userDataManager.getPurchaseList(
                AccountManager.getInstance().getLocalLanguage(),
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                new MbsDataProvider<String, List<PurchasedContent>>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int reason) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadData: onFailed");
                        }
                        showView(0);

                        if (reason == MbsTaskCallback.REASON_NETWORK) {
                            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(reason, getTaskContext());
                            failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<PurchasedContent> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadData: onSuccess: result=" + result);
                        }

                        purchasedList.clear();
                        hiddenList.clear();
                        visibleList.clear();

                        List<PurchasedContent> tmpList = result;
                        if(tmpList != null) {
                            Collections.sort(tmpList, new Comparator<PurchasedContent>() {
                                @Override
                                public int compare(PurchasedContent content1, PurchasedContent content2) {
                                    long purchasedTime1 = 0;
                                    long purchasedTime2 = 0;
                                    try {
                                        if (content1 != null && content1.getPurchaseDateTime() != null) {
                                            purchasedTime1 = content1.getPurchaseDateTime().getTime();
                                        }
                                        if (content2 != null && content2.getPurchaseDateTime() != null) {
                                            purchasedTime2 = content2.getPurchaseDateTime().getTime();
                                        }
                                        if (purchasedTime1 < purchasedTime2) {
                                            return 1;
                                        } else if (purchasedTime1 > purchasedTime2) {
                                            return -1;
                                        }
                                    }catch (Exception e){
                                    }
                                    return 0;
                                }
                            });

                            for (PurchasedContent content : tmpList) {
                                purchasedList.add(content);
                                if (content.isHidden()) {
                                    hiddenList.add(content);
                                } else {
                                    visibleList.add(content);
                                }
                            }
                        }

                        showView(0);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);

                        showView(0);
                    }

                    @Override
                    public Context getTaskContext() {
                        return PurchasedListFragment.this.getContext();
                    }
                });

        taskList.add(new WeakReference<>(dataTask));
    }

    @Override
    public void hideProgress() {
        if (progressBarManager != null) {
            progressBarManager.hide();
        }
    }

    @Override
    public void showProgress() {
        if (taskList.isEmpty()) {
            progressBarManager.show();
        }
    }

    @Override
    public void showView(int type) {
        completeTask(type);
        if (taskList.isEmpty()) {
            setGridView();
            hideProgress();
        }
        created = false;
    }

    private void completeTask(int type) {
        clearThread();
        if (Log.INCLUDE) {
            Log.d(TAG, "completeTask: removed type=" + type + ", current task size=" + taskList.size());
        }
    }

    private void setGridView() {
        if(!isAdded() || getActivity() == null){
            if (Log.INCLUDE) {
                Log.d(TAG, "setGridView: not added to context");
            }
            return;
        }

        if (visibleList != null && visibleList.size() > 0) {
            ArrayObjectAdapter listObjectAdapter = null;
            PurchasedItemPresenter presenter = new PurchasedItemPresenter(false,
                new PurchasedItemPresenter.OnKeyListener() {
                    @Override
                    public boolean onKey(int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onKey: keyCode=" + keyCode + ", index=" + index);
                        }

                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                            case KeyEvent.KEYCODE_DPAD_CENTER:
                                if (item instanceof PurchasedContent) {
                                    PurchasedContent content = (PurchasedContent)item;
                                    if (content.isPackage()) {
                                        Linker.goToPackageVodDetail(mTvOverlayManager, content.getPackageId(), content.getRating());
                                    } else {
                                        Linker.goToVodDetail(mTvOverlayManager, Content.buildPurchasedContent(content));
                                    }
                                }
                                return true;

                            case KeyEvent.KEYCODE_DPAD_DOWN:
                                int size = (visibleList != null ? visibleList.size() : 0);
                                int moveIndex = index + PurchasedListDialog.NUMBER_COLUMNS;
                                int lastRow = gridView.getRowIndex(size - 1);
                                int row = gridView.getRowIndex(moveIndex);

                                if (row > lastRow) {
                                    setFocus(0);
                                    return true;
                                } else if (moveIndex >= size) {
                                    setFocus(size - 1);
                                    return true;
                                }


                                break;
                        }

                        return false;
                    }
                }, new PurchasedItemPresenter.OnFocusListener() {
                    @Override
                    public void onFocus(Object item) {
                        if (item instanceof PurchasedContent) {
                            PurchasedContent content = (PurchasedContent) item;
                            setContentInfo(content);
                        }
                    }
            });
            listObjectAdapter = new ArrayObjectAdapter(presenter);
            listObjectAdapter.addAll(0, visibleList);

            gridBridgeAdapter = new PurchasedListBridgeAdapter(new PurchasedListBridgeAdapter.OnBindListener() {
                @Override
                public void onBind(int index) {
                }
            });

            gridBridgeAdapter.setAdapter(listObjectAdapter);
            gridView.setAdapter(gridBridgeAdapter);


            countView.setText("(" + visibleList.size() + ")");

            emptyVodLayer.setVisibility(View.GONE);
            btnGroupLayer.setVisibility(View.GONE);

            countView.setVisibility(View.VISIBLE);
            btnEdit.setVisibility(View.VISIBLE);
            infoLayer.setVisibility(View.VISIBLE);
            contentLayer.setVisibility(View.VISIBLE);

            if(!created){
                btnEdit.requestFocus();
            }
        } else {
            countView.setVisibility(View.GONE);
            contentLayer.setVisibility(View.GONE);
            emptyVodLayer.setVisibility(View.VISIBLE);
        }
    }
}
