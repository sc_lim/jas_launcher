/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.MyCoupon;

public class MyCouponListResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private MyCouponList data;

    protected MyCouponListResponse(Parcel in) {
        super(in);
        data = in.readParcelable(MyCouponListResponse.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    public List<MyCoupon> getCouponList() {
        return data.list;
    }

    private static class MyCouponList implements Parcelable {
        public static final Creator<MyCouponList> CREATOR = new Creator<MyCouponListResponse.MyCouponList>() {
            @Override
            public MyCouponListResponse.MyCouponList createFromParcel(Parcel in) {
                return new MyCouponListResponse.MyCouponList(in);
            }

            @Override
            public MyCouponListResponse.MyCouponList[] newArray(int size) {
                return new MyCouponListResponse.MyCouponList[size];
            }
        };
        @Expose
        @SerializedName("result")
        public List<MyCoupon> list;

        protected MyCouponList(Parcel in) {
            in.readList(list, MyCoupon.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(list);
        }

        @Override
        public int describeContents() {
            return 0;
        }
    }
}
