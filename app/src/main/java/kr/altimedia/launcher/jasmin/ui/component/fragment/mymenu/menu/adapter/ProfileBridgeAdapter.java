/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.adapter;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfile;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.presenter.ProfilePresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;

public class ProfileBridgeAdapter extends ItemBridgeAdapter {
    private static final String TAG = ProfileBridgeAdapter.class.getSimpleName();

    public ProfileBridgeAdapter() {
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        ProfilePresenter presenter = (ProfilePresenter) viewHolder.mPresenter;
        ProfilePresenter.OnKeyListener onKeyListener = presenter.getOnKeyListener();

        ProfilePresenter.ItemViewHolder itemViewHolder = (ProfilePresenter.ItemViewHolder) viewHolder.getViewHolder();

        int index = (int) viewHolder.itemView.getTag(R.id.KEY_INDEX);
        if (onKeyListener != null) {
            viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_UP){
                        if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
                            UserProfile item = (UserProfile) viewHolder.mItem;
                            String name = item.getName();
                            Log.d(TAG, "onKey(RELEASE) name=" + name + ", index=" + index);

                            return onKeyListener.onKey(keyCode, index, item, itemViewHolder);
                        }else{
                            return true;
                        }
                    }else{
                        if (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DPAD_CENTER){
                            return true;
                        }else{
                            UserProfile item = (UserProfile) viewHolder.mItem;
                            String name = item.getName();
                            Log.d(TAG, "onKey(PRESS) name=" + name + ", index=" + index);

                            return onKeyListener.onKey(keyCode, index, item, itemViewHolder);
                        }
                    }
                }
            });
        }
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        viewHolder.mHolder.view.setOnKeyListener(null);
    }
}