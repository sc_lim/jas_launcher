/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.util.Log;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

/**
 * Created by mc.kim on 08,05,2020
 */
public class Banner implements Parcelable {
    public static final String LINK_TYPE_CHANNEL = "channel";
    public static final String LINK_TYPE_CONTENTS = "contentGroupAsset";
    public static final String LINK_TYPE_SERIES = "seriesAsset";
    public static Rect PROMOTION_BANNER = new Rect(0, 0, 1920, 614);
    public static Rect BANNER = new Rect(0, 0, 668, 280);
    public static final int VIDEO_TYPE = 2;
    public static final int IMAGE_TYPE = 1;

    @Expose
    @SerializedName("bannerClipAsset")
    private BannerClipAsset bannerClipAsset;
    @Expose
    @SerializedName("bannerImageAsset")
    private BannerImageAsset bannerImageAsset;

    @Expose
    @SerializedName("bannerType")
    private int bannerType;
    @Expose
    @SerializedName("linkType")
    private String linkType;
    @Expose
    @SerializedName("linkInfo")
    private String linkInfo;
    @Expose
    @SerializedName("autoSlidingFlag")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean autoSlidingFlag;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;

    protected Banner(Parcel in) {
        bannerClipAsset = (BannerClipAsset) in.readValue(BannerClipAsset.class.getClassLoader());
        bannerImageAsset = (BannerImageAsset) in.readValue(BannerImageAsset.class.getClassLoader());
        bannerType = in.readInt();
        linkType = in.readString();
        linkInfo = in.readString();
        autoSlidingFlag = in.readByte() != 0;
        this.categoryId = in.readString();
    }

    private Banner() {

    }

    public static Banner buildBanner(Content content) {
        if (content == null) {
            return null;
        }
        if (Log.INCLUDE) {
            Log.d("buildBanner", content.toString());
        }
        if (content.getBannerType() == Banner.IMAGE_TYPE) {
            if (content.getBannerImageAsset() == null) {
                return null;
            }
        } else if (content.getBannerType() == Banner.VIDEO_TYPE) {
            if (content.getBannerClipAsset() == null) {
                return null;
            }
        } else {
            return null;
        }


        Banner banner = new Banner();
        banner.setCategoryId(content.getCategoryId());
        banner.bannerClipAsset = content.getBannerClipAsset();
        banner.bannerImageAsset = content.getBannerImageAsset();
        banner.bannerType = content.getBannerType();
        banner.linkType = null;
        banner.linkInfo = null;
        banner.autoSlidingFlag = true;
        return banner;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(bannerClipAsset);
        dest.writeValue(bannerImageAsset);
        dest.writeInt(bannerType);
        dest.writeString(linkType);
        dest.writeString(linkInfo);
        dest.writeByte((byte) (autoSlidingFlag ? 1 : 0));
        dest.writeString(categoryId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Banner> CREATOR = new Creator<Banner>() {
        @Override
        public Banner createFromParcel(Parcel in) {
            return new Banner(in);
        }

        @Override
        public Banner[] newArray(int size) {
            return new Banner[size];
        }
    };

    public BannerClipAsset getBannerClipAsset() {
        return bannerClipAsset;
    }

    public BannerImageAsset getBannerImageAsset() {
        return bannerImageAsset;
    }

    public int getBannerType() {
        return bannerType;
    }

    public String getLinkType() {
        return linkType;
    }

    public String getLinkInfo() {
        return linkInfo;
    }

    public boolean isAutoSlidingFlag() {
        return autoSlidingFlag;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Banner{" +
                "bannerClipAsset=" + bannerClipAsset +
                ", bannerImageAsset=" + bannerImageAsset +
                ", bannerType=" + bannerType +
                ", linkType='" + linkType + '\'' +
                ", linkInfo='" + linkInfo + '\'' +
                '}';
    }
}
