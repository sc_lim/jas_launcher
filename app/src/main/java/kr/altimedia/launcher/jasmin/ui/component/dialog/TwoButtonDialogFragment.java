/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

/**
 * Created by mc.kim on 17,07,2020
 */
public class TwoButtonDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = TwoButtonDialogFragment.class.getName();
    private static final String KEY_TITLE = "title";
    private static final String KEY_SUB_TITLE = "subTitle";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_BUTTON_NAME = "buttonArray";
    private final String TAG = TwoButtonDialogFragment.class.getSimpleName();
    private DialogInterface.OnDismissListener mOnDismissListener = null;
    private View.OnClickListener mOkButtonClickedListener;

    protected TwoButtonDialogFragment(Context context, View.OnClickListener onClickListener) {
        this.mOkButtonClickedListener = onClickListener;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_NOTICE;
    }

    public static TwoButtonDialogFragment newInstance(Context context, String title,
                                                      String subTitle,
                                                      String description,
                                                      String confirmName, View.OnClickListener okButtonClickedListener) {

        Bundle args = new Bundle();
        args.putString(KEY_TITLE, title);
        args.putString(KEY_SUB_TITLE, subTitle);
        args.putString(KEY_DESCRIPTION, description);
        args.putString(KEY_BUTTON_NAME, confirmName);

        TwoButtonDialogFragment fragment = new TwoButtonDialogFragment(context, okButtonClickedListener);
        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnDismissListener(this);
        return dialog;
    }


    public void setOnDismissListener(@NonNull DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }


    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_error_network, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initVieW(view);
        initButton(view);
    }

    private void initVieW(View view) {
        Bundle mBundle = getArguments();
        String title = mBundle.getString(KEY_TITLE);
        String subtitle = mBundle.getString(KEY_SUB_TITLE);
        String description = mBundle.getString(KEY_DESCRIPTION);

        if (Log.INCLUDE) {
            Log.d(TAG, "title : " + title + ", description : " + description);
        }

        TextView titleView = view.findViewById(R.id.title);
        TextView subTitleView = view.findViewById(R.id.subTitle);
        TextView descriptionView = view.findViewById(R.id.description);

        titleView.setText(title);
        subTitleView.setText(subtitle);
        descriptionView.setText(description);
    }

    private void initButton(View view) {

        Bundle mBundle = getArguments();
        String buttonName = mBundle.getString(KEY_BUTTON_NAME);

        TextView okButton = view.findViewById(R.id.okButton);
        if(buttonName == null){
            okButton.setVisibility(View.GONE);
        }else{
            okButton.setVisibility(View.VISIBLE);
            okButton.setText(buttonName);
        }
        TextView closeButton = view.findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        okButton.setOnClickListener(mOkButtonClickedListener);
    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (Log.INCLUDE) {
            Log.d(TAG, "onDismiss");
        }
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
            mOnDismissListener = null;
        }
    }
}
