/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;

import com.altimedia.util.Log;

import java.lang.ref.WeakReference;

import androidx.leanback.widget.Action;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import kr.altimedia.launcher.jasmin.media.VideoPlayerAdapter;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.TrailerDataProvider;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.TrailerDetailsDescriptionPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.TrailerSelectUi;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekDataProvider;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekUi;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;

/**
 * Created by mc.kim on 29,04,2020
 */
public class TrailerPlaybackTransportControlGlue<T extends VideoPlayerAdapter>
        extends VideoPlaybackBaseControlGlue<T> implements TrailerPlaybackTransportRowPresenter.OnTrailerModeChanged {

    static final String TAG = "TrailerPlaybackTransportControlGlue";
    static final boolean DEBUG = false;

    static final int MSG_UPDATE_PLAYBACK_STATE = 100;
    static final int MSG_HIDE_CONTROL_STATE = 101;
    static final int MSG_HIDE_PLAYBACK_STATE = 102;
    static final int MSG_HIDE_PLAY_CONTROL_STATE = 103;

    static final int HIDE_CONTROL_STATE_DELAY_MS = 1500;
    static final int HIDE_PLAYBACK_STATE_DELAY_MS = 3500;
    static final int HIDE_PLAY_CONTROLLER_STATE_DELAY_MS = HIDE_CONTROL_STATE_DELAY_MS + HIDE_PLAYBACK_STATE_DELAY_MS;

    final SeekUiClient mPlaybackSeekUiClient = new SeekUiClient();
    boolean mSeekEnabled;
    TrailerDataProvider mVideoProvider;

    private class UpdatePlaybackStateHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_PLAYBACK_STATE:
                    TrailerPlaybackTransportControlGlue glue =
                            ((WeakReference<TrailerPlaybackTransportControlGlue>) msg.obj).get();
                    if (glue != null) {
                        glue.onUpdatePlaybackState();
                    }
                    sendHandlerMsg(MSG_HIDE_CONTROL_STATE);
                    break;
                case MSG_HIDE_CONTROL_STATE:
                    getHost().hideStateOverlay(true);
                    if (getHost().isControlsOverlayVisible()) {
                        sendHandlerMsg(MSG_HIDE_PLAYBACK_STATE);
                    } else {
                        if (isPlaying()) {
                            getHost().hideIconView(true);
                        } else {
                            getHost().showIconView(true);
                        }
                    }
                    break;
                case MSG_HIDE_PLAYBACK_STATE:
                    getHost().hideControlsOverlay(true);
                    break;

                case MSG_HIDE_PLAY_CONTROL_STATE:
                    getHost().hideStateOverlay(true);
                    getHost().hideControlsOverlay(true);
                    break;
            }

        }
    }

    final Handler sHandler = new UpdatePlaybackStateHandler();

    final WeakReference<VideoPlaybackBaseControlGlue> mGlueWeakReference = new WeakReference(this);

    /**
     * Constructor for the glue.
     *
     * @param context
     * @param impl    Implementation to underlying media player.
     */
    public TrailerPlaybackTransportControlGlue(Context context, T impl) {
        super(context, impl);
    }

    @Override
    public void setControlsRow(VideoPlaybackControlsRow controlsRow) {
        super.setControlsRow(controlsRow);
        clearSHandler();
//        onUpdatePlaybackState();
    }

    private void clearSHandler() {
        sHandler.removeMessages(MSG_HIDE_PLAY_CONTROL_STATE, mGlueWeakReference);
        sHandler.removeMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference);
        sHandler.removeMessages(MSG_HIDE_PLAYBACK_STATE, mGlueWeakReference);
        sHandler.removeMessages(MSG_HIDE_CONTROL_STATE, mGlueWeakReference);
    }

    @Override
    protected void onTimerStop() {
        super.onTimerStop();
        clearSHandler();
    }

    @Override
    protected void onCreatePrimaryActions(ArrayObjectAdapter primaryActionsAdapter) {
        primaryActionsAdapter.add(mPlayPauseAction =
                new VideoPlaybackControlsRow.PlayPauseAction(getContext()));
    }

    @Override
    protected VideoPlaybackRowPresenter onCreateRowPresenter() {

        final TrailerDetailsDescriptionPresenter detailsPresenter =
                new TrailerDetailsDescriptionPresenter(mVideoProvider) {
                    @Override
                    protected void onBindDescription(ViewHolder
                                                             viewHolder, Object obj) {
                        VideoPlaybackBaseControlGlue glue = (VideoPlaybackBaseControlGlue) obj;
                        viewHolder.getTitle().setText(glue.getTitle());
                    }
                };

        TrailerPlaybackTransportRowPresenter rowPresenter = new TrailerPlaybackTransportRowPresenter() {
            @Override
            protected void onBindRowViewHolder(RowPresenter.ViewHolder vh, Object item) {
                super.onBindRowViewHolder(vh, item);
                vh.setOnKeyListener(TrailerPlaybackTransportControlGlue.this);
            }

            @Override
            protected void onUnbindRowViewHolder(RowPresenter.ViewHolder vh) {
                super.onUnbindRowViewHolder(vh);
                vh.setOnKeyListener(null);
            }

            @Override
            protected void onProgressBarKey(ViewHolder vh, KeyEvent keyEvent) {
                super.onProgressBarKey(vh, keyEvent);
                VideoPlaybackControlsRow.PlayPauseAction playPauseAction = new VideoPlaybackControlsRow.PlayPauseAction(getContext());
                dispatchAction(playPauseAction, keyEvent);
            }

            @Override
            protected void onProgressBarClicked(ViewHolder vh) {
                super.onProgressBarClicked(vh);
                VideoPlaybackControlsRow.PlayPauseAction playPauseAction = new VideoPlaybackControlsRow.PlayPauseAction(getContext());
                dispatchAction(playPauseAction, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE));
            }
        };
        rowPresenter.setDescriptionPresenter(detailsPresenter);
        rowPresenter.setOnModeChangedListener(TrailerPlaybackTransportControlGlue.this);
        return rowPresenter;
    }

    @Override
    protected void onAttachedToHost(VideoPlaybackGlueHost host) {
        super.onAttachedToHost(host);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttachedToHost");
        }
        if (host instanceof TrailerSelectUi) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onAttachedToHost TrailerSelectUi");
            }
            ((TrailerSelectUi) host).setTrailerSelectUiClient(mTrailerSelectUiClient);
        }
    }

    @Override
    protected void onDetachedFromHost() {
        super.onDetachedFromHost();
        clearSHandler();
        if (getHost() instanceof TrailerSelectUi) {
            ((TrailerSelectUi) getHost()).setTrailerSelectUiClient(null);
        }
    }


    @Override
    public void onActionClicked(Action action) {
        dispatchAction(action, null);
    }

    boolean isDPADKeyEvent(KeyEvent keyEvent) {


        boolean isCenter = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER;
        boolean forward = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT;
        boolean left = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;

        boolean up = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP;
        boolean down = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN;
        if (isCenter) {
            if (getHost().isControlsOverlayVisible()) {
                return false;
            }
            if (keyEvent.getAction() == KeyEvent.ACTION_UP) {
                return true;
            }
            if (isPlaying()) {
                pause();
                mIsPlaying = false;
            } else {
                play();
                mIsPlaying = true;
            }
            updatePlaybackState(mIsPlaying);
            getHost().showStateOverlay(true);
            getHost().showControlsOverlay(true);
            sendHandlerMsg(MSG_HIDE_CONTROL_STATE);
            return true;
        } else if (forward || left) {
            if (getHost().isControlsOverlayVisible()) {
                return false;
            }
            if (forward) {
                onForward(keyEvent.getAction() == KeyEvent.ACTION_UP);
            } else {
                onBackward(keyEvent.getAction() == KeyEvent.ACTION_UP);
            }

            int actionIndex = forward ?
                    VideoPlaybackControlsRow.PlayPauseAction.INDEX_FAST_FORWARD : VideoPlaybackControlsRow.PlayPauseAction.INDEX_REWIND;
            onUpdatePlaybackStatusAfterUserAction(actionIndex, MSG_HIDE_CONTROL_STATE, keyEvent.getAction());

            getHost().showStateOverlay(true);
            getHost().showControlsOverlay(true);
            return true;
        } else if (up || down) {
            if (getHost().isControlsOverlayVisible()) {
                return false;
            }
            getHost().showControlsOverlay(true);
            sendHandlerMsg(MSG_UPDATE_PLAYBACK_STATE);
            return true;
        }
        return false;
    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_ESCAPE:
                return false;
        }
        if (mControlsRow == null) {
            return false;
        }
        if (isDPADKeyEvent(event)) {
            return true;
        }
        final ObjectAdapter primaryActionsAdapter = mControlsRow.getPrimaryActionsAdapter();
        Action action = mControlsRow.getActionForKeyCode(primaryActionsAdapter, keyCode);
        if (action == null) {
            action = mControlsRow.getActionForKeyCode(mControlsRow.getSecondaryActionsAdapter(),
                    keyCode);
        }

        if (action != null) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                dispatchAction(action, event);
            }
            return true;
        }
        return false;
    }

    @Override
    protected void onUpdatePlaybackStatusAfterUserAction(final int msgWhat) {
        updatePlaybackState(mIsPlaying);
        sendHandlerMsg(msgWhat);
    }

    void sendHandlerMsg(int msgWhat) {
        clearSHandler();
        switch (msgWhat) {
            case MSG_UPDATE_PLAYBACK_STATE:
                sHandler.sendMessage(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference));
                break;

            case MSG_HIDE_CONTROL_STATE:
                sHandler.sendMessageDelayed(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference), HIDE_CONTROL_STATE_DELAY_MS);
                break;

            case MSG_HIDE_PLAYBACK_STATE:
                sHandler.sendMessageDelayed(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference), HIDE_PLAYBACK_STATE_DELAY_MS);
                break;

            case MSG_HIDE_PLAY_CONTROL_STATE:
                sHandler.sendMessageDelayed(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference), HIDE_PLAY_CONTROLLER_STATE_DELAY_MS);
                break;
        }
    }


    void onUpdatePlaybackStatusAfterUserAction(int index, int msgWhat, int keyEvent) {
        updatePlaybackState(mIsPlaying, index, keyEvent);
        sendHandlerMsg(msgWhat);
    }


    /**
     * Called when the given action is invoked, either by click or keyevent.
     */
    boolean dispatchAction(Action action, KeyEvent keyEvent) {

        boolean handled = false;
        if (keyEvent == null) {
            return false;
        }
        if (mPlayerAdapter.isKeyBlock()) {
            return false;
        }

        if (!mPlayerAdapter.isReady()) {
            return false;
        }

        if (action instanceof VideoPlaybackControlsRow.PlayPauseAction) {


            boolean canPlay = keyEvent == null
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY;
            boolean canPause = keyEvent == null
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PAUSE;
            //            PLAY_PAUSE    PLAY      PAUSE
            // playing    paused                  paused
            // paused     playing       playing
            // ff/rw      playing       playing   paused
            if (canPause && mIsPlaying) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                    return true;
                }
                pause();
                mIsPlaying = false;
                onUpdatePlaybackStatusAfterUserAction(MSG_UPDATE_PLAYBACK_STATE);
                getHost().showStateOverlay(true);
                getHost().showControlsOverlay(true);
            } else if (canPlay && !mIsPlaying) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                    return true;
                }
                play();
                mIsPlaying = true;
                onUpdatePlaybackStatusAfterUserAction(MSG_UPDATE_PLAYBACK_STATE);
                getHost().showStateOverlay(true);
                getHost().showControlsOverlay(true);
            } else {
                boolean forward = (keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT);
//                if (!isPlaying()) {
//                    play();
//                    mIsPlaying = true;
//                    updatePlaybackState(mIsPlaying);
//                }
                if (forward) {
                    onForward(keyEvent.getAction() == KeyEvent.ACTION_UP);
                } else {
                    onBackward(keyEvent.getAction() == KeyEvent.ACTION_UP);
                }
                int actionIndex = forward ?
                        VideoPlaybackControlsRow.PlayPauseAction.INDEX_FAST_FORWARD : VideoPlaybackControlsRow.PlayPauseAction.INDEX_REWIND;
                onUpdatePlaybackStatusAfterUserAction(actionIndex, MSG_HIDE_CONTROL_STATE, keyEvent.getAction());
                getHost().showControlsOverlay(true);
            }

            getHost().showStateOverlay(true);

            handled = true;
        } else if (action instanceof VideoPlaybackControlsRow.SkipNextAction) {
            next();
            handled = true;
        } else if (action instanceof VideoPlaybackControlsRow.SkipPreviousAction) {
            previous();
            handled = true;
        }
        return handled;
    }

    @Override
    protected long getDuration() {
        return super.getDuration();
    }

    @Override
    protected long getCurrentPosition() {
        return super.getCurrentPosition();
    }

    boolean onForward(boolean keyUp) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onForward : " + keyUp);
        }
        if (keyUp) {
            mPlaybackSeekUiClient.onSeekFinished(false);
            return false;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, " onForward : " + mPlaybackSeekUiClient.mIsSeek);
        }
        if (!mPlaybackSeekUiClient.mIsSeek) {
            mPlaybackSeekUiClient.onSeekStarted();
        }
        long currentPosition = mPlaybackSeekUiClient.mLastUserPosition != -1 ? mPlaybackSeekUiClient.mLastUserPosition : mPlayerAdapter.getCurrentPosition();
        long seekTimeInMs = currentPosition + VideoPlaybackTransportRowPresenter.SEEK_DIFFER_TIME;
        if (seekTimeInMs > getDuration()) {
            seekTimeInMs = getDuration();
        }
        mPlaybackSeekUiClient.onSeekPositionChanged(seekTimeInMs);
        return true;
    }

    boolean onBackward(boolean keyUp) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBackward : " + keyUp);
        }
        if (keyUp) {
            mPlaybackSeekUiClient.onSeekFinished(false);
            return false;
        }

        if (!mPlaybackSeekUiClient.mIsSeek) {
            mPlaybackSeekUiClient.onSeekStarted();
        }
        long currentPosition = mPlaybackSeekUiClient.mLastUserPosition != -1 ? mPlaybackSeekUiClient.mLastUserPosition : mPlayerAdapter.getCurrentPosition();
        long seekTimeInMs = currentPosition - VideoPlaybackTransportRowPresenter.SEEK_DIFFER_TIME;
        if (seekTimeInMs < 0) {
            seekTimeInMs = 0;
        }
        mPlaybackSeekUiClient.onSeekPositionChanged(seekTimeInMs);
        return true;
    }

    @Override
    protected void onPlayStateChanged() {
        if (DEBUG) {
            Log.d(TAG, "onStateChanged");
        }

        if (!isReady()) {
            return;
        }

        if (sHandler.hasMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference)) {
            sHandler.removeMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference);
            if (mPlayerAdapter.isPlaying() != mIsPlaying) {
                if (DEBUG) {
                    Log.d(TAG, "Status expectation mismatch, delaying update");
                }
                sHandler.sendMessageDelayed(sHandler.obtainMessage(MSG_UPDATE_PLAYBACK_STATE,
                        mGlueWeakReference), HIDE_CONTROL_STATE_DELAY_MS);
            } else {
                if (DEBUG) {
                    Log.d(TAG, "Update state matches expectation");
                }
                onUpdatePlaybackState();
            }
        } else {
            onUpdatePlaybackState();
        }

        super.onPlayStateChanged();
    }

    void onUpdatePlaybackState() {
        mIsPlaying = mPlayerAdapter.isPlaying();
        updatePlaybackState(mIsPlaying);
    }

    private void updatePlaybackState(boolean isPlaying) {
        int index = !isPlaying
                ? VideoPlaybackControlsRow.PlayPauseAction.INDEX_PLAY
                : VideoPlaybackControlsRow.PlayPauseAction.INDEX_PAUSE;
        if (Log.INCLUDE) {
            Log.d(TAG, "updatePlaybackState : " + isPlaying);
        }
        updatePlaybackState(isPlaying, index, KeyEvent.ACTION_DOWN);

        if (isPlaying) {
            getHost().hideIconView(true);
        }
    }


    private void updatePlaybackState(boolean isPlaying, int iconIndex, int keyAcion) {
        if (mControlsRow == null) {
            return;
        }

        if (!isPlaying) {
            onUpdateProgress();
            mPlayerAdapter.setProgressUpdatingEnabled(true);
        } else {
            mPlayerAdapter.setProgressUpdatingEnabled(true);
        }

        if (mFadeWhenPlaying && getHost() != null) {
            getHost().setControlsOverlayAutoHideEnabled(isPlaying);
        }

        if (mPlayPauseAction != null) {
            int index = iconIndex;
            int mPlayPauseActionIndex = mPlayPauseAction.getIndex();
            if (mPlayPauseActionIndex != index
                    || mPlayPauseActionIndex == VideoPlaybackControlsRow.PlayPauseAction.INDEX_REWIND
                    || mPlayPauseActionIndex == VideoPlaybackControlsRow.PlayPauseAction.INDEX_FAST_FORWARD) {
                mPlayPauseAction.setIndex(index);
                mPlayPauseAction.setAction(keyAcion);
                notifyItemChanged((ArrayObjectAdapter) getControlsRow().getPrimaryActionsAdapter(),
                        mPlayPauseAction);
            }
        }
    }

    public final void setSeekEnabled(boolean seekEnabled) {
        mSeekEnabled = seekEnabled;
    }

    public final boolean isSeekEnabled() {
        return mSeekEnabled;
    }


    final TrailerSelectUiClient mTrailerSelectUiClient = new TrailerSelectUiClient();

    class TrailerSelectUiClient extends TrailerSelectUi.Client {

        @Override
        public boolean isSelectionEnabled() {
            return super.isSelectionEnabled();
        }

        @Override
        public void onSelectStarted() {
            super.onSelectStarted();
        }

        @Override
        public TrailerDataProvider getTrailerDataProvider() {
            return super.getTrailerDataProvider();
        }

        @Override
        public void onSelectFinished(boolean cancelled) {
            super.onSelectFinished(cancelled);
        }
    }


    public final void setVideoSelectProvider(TrailerDataProvider dataProvider) {
        mVideoProvider = dataProvider;
    }

    public void forceControllerHide() {
        getHost().hideControlsOverlay(true);
        sHandler.removeMessages(MSG_HIDE_CONTROL_STATE, mGlueWeakReference);
    }

    @Override
    public void modeChanged(TrailerPlaybackTransportRowPresenter.TrailerMode previousMode,
                            TrailerPlaybackTransportRowPresenter.TrailerMode currentMode) {
        if (Log.INCLUDE) {
            Log.d(TAG, "modeChanged : currentMode : " + currentMode);
            Log.d(TAG, "modeChanged : previousMode : " + previousMode);
        }
        if (!getHost().canUsingTrailerMode()) {
            return;
        }
        onUpdatePlaybackStatusAfterUserAction(MSG_HIDE_PLAY_CONTROL_STATE);
        getHost().changeTrailerMode(currentMode.isSelectMode, true);
    }

    class SeekUiClient extends VideoPlaybackSeekUi.Client {
        boolean mPausedBeforeSeek;
        long mPositionBeforeSeek;
        long mLastUserPosition;
        boolean mIsSeek;

        @Override
        public VideoPlaybackSeekDataProvider getPlaybackSeekDataProvider() {
            return null;
        }

        @Override
        public boolean isSeekEnabled() {
            return false;
        }

        @Override
        public void onSeekStarted() {
            mIsSeek = true;
            mPausedBeforeSeek = !isPlaying();
            mPlayerAdapter.setProgressUpdatingEnabled(true);
            // if we seek thumbnails, we don't need save original position because current
            // position is not changed during seeking.
            // otherwise we will call seekTo() and may need to restore the original position.
            mPositionBeforeSeek = mPlayerAdapter.getCurrentPosition();
            mLastUserPosition = -1;
            pause();
        }

        @Override
        public void onSeekPositionChanged(long pos) {
            mPlayerAdapter.seekTo(pos);
            if (mControlsRow != null) {
                mControlsRow.setCurrentPosition(pos, true);
            }
        }

        @Override
        public void onSeekFinished(boolean cancelled) {
            if (!cancelled) {
                if (mLastUserPosition >= 0) {
                    seekTo(mLastUserPosition);
                }
            } else {
                if (mPositionBeforeSeek >= 0) {
                    seekTo(mPositionBeforeSeek);
                }
            }
            mIsSeek = false;
            if (!mPausedBeforeSeek) {
                play();
            } else {
                mPlayerAdapter.setProgressUpdatingEnabled(false);
                // we neeed update UI since PlaybackControlRow still saves previous position.
                onUpdateProgress();
            }
        }
    }
}
