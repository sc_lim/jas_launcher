/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;

public class ScaleFrameLayout extends FrameLayout {
    private static final int DEFAULT_CHILD_GRAVITY = 8388659;
    private float mLayoutScaleX;
    private float mLayoutScaleY;
    private float mChildScale;

    public ScaleFrameLayout(Context context) {
        this(context, (AttributeSet) null);
    }

    public ScaleFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScaleFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mLayoutScaleX = 1.0F;
        this.mLayoutScaleY = 1.0F;
        this.mChildScale = 1.0F;

    }

    public void setLayoutScaleX(float scaleX) {
        if (scaleX != this.mLayoutScaleX) {
            this.mLayoutScaleX = scaleX;
            this.requestLayout();
        }

    }

    public void setLayoutScaleY(float scaleY) {
        if (scaleY != this.mLayoutScaleY) {
            this.mLayoutScaleY = scaleY;
            this.requestLayout();
        }

    }

    public void setChildScale(float scale) {
        if (this.mChildScale != scale) {
            this.mChildScale = scale;

            for (int i = 0; i < this.getChildCount(); ++i) {
                this.getChildAt(i).setScaleX(scale);
                this.getChildAt(i).setScaleY(scale);
            }
        }

    }

    public void addView(View child, int index, LayoutParams params) {
        super.addView(child, index, params);
        child.setScaleX(this.mChildScale);
        child.setScaleY(this.mChildScale);
    }

    protected boolean addViewInLayout(View child, int index, LayoutParams params, boolean preventRequestLayout) {
        boolean ret = super.addViewInLayout(child, index, params, preventRequestLayout);
        if (ret) {
            child.setScaleX(this.mChildScale);
            child.setScaleY(this.mChildScale);
        }

        return ret;
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int count = this.getChildCount();
        int layoutDirection = this.getLayoutDirection();
        float pivotX = layoutDirection == 1 ? (float) this.getWidth() - this.getPivotX() : this.getPivotX();
        int parentLeft;
        int parentRight;
        if (this.mLayoutScaleX != 1.0F) {
            parentLeft = this.getPaddingLeft() + (int) (pivotX - pivotX / this.mLayoutScaleX + 0.5F);
            parentRight = (int) (pivotX + ((float) (right - left) - pivotX) / this.mLayoutScaleX + 0.5F) - this.getPaddingRight();
        } else {
            parentLeft = this.getPaddingLeft();
            parentRight = right - left - this.getPaddingRight();
        }

        float pivotY = this.getPivotY();
        int parentTop;
        int parentBottom;
        if (this.mLayoutScaleY != 1.0F) {
            parentTop = this.getPaddingTop() + (int) (pivotY - pivotY / this.mLayoutScaleY + 0.5F);
            parentBottom = (int) (pivotY + ((float) (bottom - top) - pivotY) / this.mLayoutScaleY + 0.5F) - this.getPaddingBottom();
        } else {
            parentTop = this.getPaddingTop();
            parentBottom = bottom - top - this.getPaddingBottom();
        }

        for (int i = 0; i < count; ++i) {
            View child = this.getChildAt(i);
            if (child.getVisibility() != View.GONE) {
                LayoutParams lp = (LayoutParams) child.getLayoutParams();
                int width = child.getMeasuredWidth();
                int height = child.getMeasuredHeight();
                int gravity = lp.gravity;
                if (gravity == -1) {
                    gravity = 8388659;
                }

                int absoluteGravity = Gravity.getAbsoluteGravity(gravity, layoutDirection);
                int verticalGravity = gravity & 112;
                int childLeft;
                switch (absoluteGravity & 7) {
                    case 1:
                        childLeft = parentLeft + (parentRight - parentLeft - width) / 2 + lp.leftMargin - lp.rightMargin;
                        break;
                    case 2:
                    case 3:
                    case 4:
                    default:
                        childLeft = parentLeft + lp.leftMargin;
                        break;
                    case 5:
                        childLeft = parentRight - width - lp.rightMargin;
                }

                int childTop;
                switch (verticalGravity) {
                    case 16:
                        childTop = parentTop + (parentBottom - parentTop - height) / 2 + lp.topMargin - lp.bottomMargin;
                        break;
                    case 48:
                        childTop = parentTop + lp.topMargin;
                        break;
                    case 80:
                        childTop = parentBottom - height - lp.bottomMargin;
                        break;
                    default:
                        childTop = parentTop + lp.topMargin;
                }

                child.layout(childLeft, childTop, childLeft + width, childTop + height);
                child.setPivotX(pivotX - (float) childLeft);
                child.setPivotY(pivotY - (float) childTop);
            }
        }

    }

    private static int getScaledMeasureSpec(int measureSpec, float scale) {
        return scale == 1.0F ? measureSpec : MeasureSpec.makeMeasureSpec((int) ((float) MeasureSpec.getSize(measureSpec) / scale + 0.5F), MeasureSpec.getMode(measureSpec));
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.mLayoutScaleX == 1.0F && this.mLayoutScaleY == 1.0F) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } else {
            int scaledWidthMeasureSpec = getScaledMeasureSpec(widthMeasureSpec, this.mLayoutScaleX);
            int scaledHeightMeasureSpec = getScaledMeasureSpec(heightMeasureSpec, this.mLayoutScaleY);
            super.onMeasure(scaledWidthMeasureSpec, scaledHeightMeasureSpec);
            this.setMeasuredDimension((int) ((float) this.getMeasuredWidth() * this.mLayoutScaleX + 0.5F), (int) ((float) this.getMeasuredHeight() * this.mLayoutScaleY + 0.5F));
        }

    }

    public void setForeground(Drawable d) {
        throw new UnsupportedOperationException();
    }

}
