/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel.obj.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.channel.obj.ChannelProductInfo;

public class ChannelProductInfoResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private ChannelProductInfo channelProductInfo;

    protected ChannelProductInfoResponse(Parcel in) {
        super(in);
        channelProductInfo = in.readTypedObject(ChannelProductInfo.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(channelProductInfo, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "ChannelProductInfoResponse{" +
                "channelProductInfoResult=" + channelProductInfo +
                '}';
    }

    public static final Creator<ChannelProductInfoResponse> CREATOR = new Creator<ChannelProductInfoResponse>() {
        @Override
        public ChannelProductInfoResponse createFromParcel(Parcel in) {
            return new ChannelProductInfoResponse(in);
        }

        @Override
        public ChannelProductInfoResponse[] newArray(int size) {
            return new ChannelProductInfoResponse[size];
        }
    };

    public ChannelProductInfo getChannelProductInfo() {
        return channelProductInfo;
    }
}
