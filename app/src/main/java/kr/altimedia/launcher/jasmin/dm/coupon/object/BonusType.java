/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import com.altimedia.util.Log;

public enum BonusType {
    AMOUNT("21301", "BonusAmount"), RATE("21302", "BonusRate"),
    DC_AMOUNT("21303", "BonusDiscountAmount"), DC_RATE("21304", "BonusDiscountRate"),
    UNKNOWN("0", "Unknown");

    private final String type;
    private final String mName;

    BonusType(String type, String mName) {
        this.type = type;
        this.mName = mName;
    }

    public static BonusType findByName(String name) {
//        Log.d("BonusType", "findByName: name="+name);
        BonusType[] statuses = values();
        for (BonusType status : statuses) {
            if (status.type.equalsIgnoreCase(name)) {
                return status;
            }
        }

        return UNKNOWN;
    }
}
