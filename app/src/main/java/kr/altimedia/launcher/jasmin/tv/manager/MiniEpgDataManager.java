/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.manager;

import android.util.TimingLogger;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager;

public class MiniEpgDataManager {
    private static final boolean DEBUG = false;
    private static final String TAG = MiniEpgDataManager.class.getSimpleName();

    private static final void log(String msg) { if (DEBUG) Log.d(TAG, msg); }

    private static final long RETRIEVE_DURATION = TimeUnit.HOURS.toMillis(3);//3hours
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm");

    static final long FIRST_ENTRY_MIN_DURATION = TimeUnit.MINUTES.toMillis(1);

    private final TvInputManagerHelper mTvInputManagerHelper;
    private final ChannelDataManager mChannelDataManager;
    private final ProgramDataManager mProgramDataManager;
    private final MiniEpgDataListener mMiniEpgDataListener;

    private List<Channel> mChannels = new ArrayList<>();
    private final Map<Long, List<TableEntry>> mChannelIdEntriesMap = new HashMap<>();
    private long mStartUtcMillis;
    private long mEndUtcMillis;

    public interface MiniEpgDataListener {
        void channelUpdated();
        void programUpdated();
    }

    public static class TableEntry {
        final long channelId;
        final Program program;
        final long entryStartUtcMillis;
        final long entryEndUtcMillis;
        final boolean mIsBlocked;

        private TableEntry(long channelId, long startUtcMillis, long endUtcMillis) {
            this(channelId, null, startUtcMillis, endUtcMillis, false);
        }

        private TableEntry(
                long channelId, long startUtcMillis, long endUtcMillis, boolean blocked) {
            this(channelId, null, startUtcMillis, endUtcMillis, blocked);
        }

        private TableEntry(
                long channelId,
                Program program,
                long entryStartUtcMillis,
                long entryEndUtcMillis,
                boolean blocked) {
            this.channelId = channelId;
            this.program = program;
            this.entryStartUtcMillis = entryStartUtcMillis;
            this.entryEndUtcMillis = entryEndUtcMillis;
            this.mIsBlocked = blocked;
            //log("TableEntry created() #########");
        }

        @Override
        public String toString() {
            return "TableEntry{" +
                    "channelId=" + channelId +
                    ", program=" + (program == null ? "null" : program.getTitle()) +
                    ", time:[" + DATE_FORMAT.format(entryStartUtcMillis) +
                    "~" + DATE_FORMAT.format(entryEndUtcMillis) +"]" +
                    ", mIsBlocked=" + mIsBlocked +
                    '}';
        }

        public Program getProgram() {
            return program;
        }

        public String getProgramTitle() {
            if (program != null) {
                return program.getTitle();
            }
            return null;
        }

        public long getStartTime() {
            return entryStartUtcMillis;
        }

        public String getTime() {
            if (entryStartUtcMillis == 0 && entryEndUtcMillis == 0) {
                return "";
            } else {
                return DATE_FORMAT.format(entryStartUtcMillis) +
                        " - " + DATE_FORMAT.format(entryEndUtcMillis);
            }
        }

        public int getProgress() {
            long now = JasmineEpgApplication.SystemClock().currentTimeMillis();
            int progress = (int)(100d * (now - entryStartUtcMillis) / (entryEndUtcMillis - entryStartUtcMillis));
            progress = Math.max(progress, 0);
            progress = Math.min(progress, 100);
            return progress;
        }

        public boolean isFutureProgram() {
            long now = JasmineEpgApplication.SystemClock().currentTimeMillis();
            return (now < entryStartUtcMillis);
        }

        public boolean isReserved() {
            if (program != null) {
                return ReminderAlertManager.getInstance().isReserved(program);
            } else {
                return false;
            }
        }
    }

    private final ChannelDataManager.Listener mChannelDataManagerListener =
            new ChannelDataManager.Listener() {
                @Override
                public void onLoadFinished() {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onUserBasedChannelListUpdated");
                    }
                    updateChannels(false);
                    mMiniEpgDataListener.channelUpdated();
                }

                @Override
                public void onChannelListUpdated() {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onChannelListUpdated");
                    }
                    updateChannels(false);
                    mMiniEpgDataListener.channelUpdated();
                }

                @Override
                public void onChannelBrowsableChanged() {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onChannelBrowsableChanged");
                    }
                    updateChannels(false);
                    mMiniEpgDataListener.channelUpdated();
                }

                @Override
                public void onUserBasedChannelListUpdated() {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onUserBasedChannelListUpdated");
                    }

                }
            };

    private final ProgramDataManager.Callback mProgramDataManagerCallback =
            new ProgramDataManager.Callback() {
                @Override
                public void onProgramUpdated() {
                    log("ProgramDataManager.onProgramUpdated()");
                    //updateTableEntries(true);
                }

                @Override
                public void onChannelUpdated() {
                    log("ProgramDataManager.onChannelUpdated()");
                    updateAllData(false);
//                    updateTableEntriesWithoutNotification(false);
//                    notifyTableEntriesUpdated();-
                }

                @Override
                public void onChannelTuned(long tuneChannelId) {

                }
            };

    public MiniEpgDataManager(TvInputManagerHelper tvInputManagerHelper,
                              ChannelDataManager channelDataManager,
                              ProgramDataManager programDataManager,
                              MiniEpgDataManager.MiniEpgDataListener miniEpgDataListener) {
        log("MiniEpgDataManager()");
        TimingLogger timings = new TimingLogger(TAG, "MiniEpgDataManager()");
        timings.addSplit("start");

        mTvInputManagerHelper = tvInputManagerHelper;
        mChannelDataManager = channelDataManager;
        mProgramDataManager = programDataManager;
        mMiniEpgDataListener = miniEpgDataListener;

        mChannelDataManager.addListener(mChannelDataManagerListener);
        mProgramDataManager.addCallback(mProgramDataManagerCallback);

        timings.addSplit("done");
        timings.dumpToLog();
    }

    public void dispose() {
        log("MiniEpgDataManager.dispose()");
        TimingLogger timings = new TimingLogger(TAG, "dispose()");
        timings.addSplit("start");

        mChannelDataManager.removeListener(mChannelDataManagerListener);
        mProgramDataManager.removeCallback(mProgramDataManagerCallback);

        timings.addSplit("done");
        timings.dumpToLog();
    }

    public void updateChannels(boolean clearPrevChannelData) {
        log("updateChannels() clearPrevChannelData:"+clearPrevChannelData);
        TimingLogger timings = new TimingLogger(TAG, "updateChannels()");
        timings.addSplit("start");

        mStartUtcMillis = JasmineEpgApplication.SystemClock().currentTimeMillis();
        mEndUtcMillis = mStartUtcMillis + RETRIEVE_DURATION;

        mChannels = mChannelDataManager.getBrowsableChannelList();

        log("  updateChannels() mChannels.size:"+(mChannels != null ? mChannels.size() : "null"));
        for (Channel ch : mChannels) {
            log("  updateChannels()   mChannels=["+ch.getDisplayNumber()+"] "+ch.getDisplayName()+", id:"+ch.getId()+", inputId:"+ch.getInputId());
        }

        updateAllData(clearPrevChannelData);
        log("updateChannels() done.");
        timings.addSplit("done");
        timings.dumpToLog();
    }

    private void updateAllData(boolean clear) {
        long current = JasmineEpgApplication.SystemClock().currentTimeMillis();
        log("updateAllData() clear:"+clear);
        TimingLogger timings = new TimingLogger(TAG, "updateAllData() clear:"+clear);
        timings.addSplit("start");

        if (clear) {
            log("updateAllData() clear done.");
            mChannelIdEntriesMap.clear();
        }

        timings.addSplit("clear");

/*
        for (Channel channel : mChannels) {
            long channelId = channel.getId();
            List<TableEntry> entries = createProgramEntries(channelId);

            log("  updateAllData() channel:["+channelToString(channel)+"], entries.size:"+(entries != null ? entries.size() : "null"));
            for (int i = 0 ; entries != null && i < entries.size() ; i++) {
                log("  updateAllData()   channel:["+channelToString(channel)+"], entries["+i+"]:"+entries.get(i));
            }

            mChannelIdEntriesMap.put(channelId, entries);

            int size = entries.size();
            if (size == 0) {
                continue;
            }
            TableEntry lastEntry = entries.get(size - 1);
            if (mEndUtcMillis < lastEntry.entryEndUtcMillis
                    && lastEntry.entryEndUtcMillis != Long.MAX_VALUE) {
                mEndUtcMillis = lastEntry.entryEndUtcMillis;
            }
        }
*/

        timings.addSplit("step 1");

/*
        if (mEndUtcMillis > mStartUtcMillis) {
            for (Channel channel : mChannels) {
                long channelId = channel.getId();
                List<TableEntry> entries = mChannelIdEntriesMap.get(channelId);
                if (entries.isEmpty()) {
                    log("  updateAllData() entries is empty - create null entries");
                    entries.add(new TableEntry(channelId, mStartUtcMillis, mEndUtcMillis, false));
                } else {
                    TableEntry lastEntry = entries.get(entries.size() - 1);
                    if (mEndUtcMillis > lastEntry.entryEndUtcMillis) {
                        log("  updateAllData() create entry #1");
                        entries.add(
                                new TableEntry(channelId, lastEntry.entryEndUtcMillis, mEndUtcMillis, false));
                    } else if (lastEntry.entryEndUtcMillis == Long.MAX_VALUE) {
                        log("  updateAllData() create entry #2 - remove & add");
                        entries.remove(entries.size() - 1);
                        entries.add(
                                new TableEntry(
                                        lastEntry.channelId,
                                        lastEntry.program,
                                        lastEntry.entryStartUtcMillis,
                                        mEndUtcMillis,
                                        lastEntry.mIsBlocked));
                    }
                }
            }
        }
*/

        timings.addSplit("step 2");

        log("  updateAllData() result ------------------------");
        for (Channel channel : mChannels) {
            long channelId = channel.getId();
            List<TableEntry> entries = mChannelIdEntriesMap.get(channelId);
            log("  updateAllData() result channel:["+channelToString(channel)+"], entries.size:"+(entries != null ? entries.size() : "null"));
            for (int i = 0 ; entries != null && i < entries.size() ; i++) {
                log("  updateAllData()   result channel:["+channelToString(channel)+"], entries["+i+"]:"+entries.get(i));
            }
        }

        timings.addSplit("print channels");

        log("  updateAllData() result ------------------------");

        log("updateAllData() done.");

        log("updateAllData() call mMiniEpgDataListener.programUpdated()");

        timings.addSplit("before call programUpdated()");

        mMiniEpgDataListener.programUpdated();

        timings.addSplit("done");
        timings.dumpToLog();
    }

    private List<TableEntry> createProgramEntries(long channelId) {
        log("  createProgramEntries() channel:["+channelToString(channelId) +"], mStartUtcMillis:"+mStartUtcMillis);

        TimingLogger timings = new TimingLogger(TAG, "createProgramEntries() channelId:"+channelId+", ch:"+channelToString(channelId));
        timings.addSplit("start");

        List<TableEntry> entries = new ArrayList<>();
        long lastProgramEndTime = mStartUtcMillis;
        List<Program> programs = mProgramDataManager.getPrograms(channelId, mStartUtcMillis);
        log("    createProgramEntries() channel:["+channelToString(channelId)+"], programs.size:"+(programs != null ? programs.size() : "null"));
        for (int i = 0 ; programs != null && i < programs.size() ; i++) {
            log("    createProgramEntries() channel:["+channelToString(channelId)+"],   programs["+i+"]:"+programs.get(i));
        }

        for (int i = 0 ; programs != null && i < programs.size() ; i++) {
            Program program = programs.get(i);
            long programStartTime = program.getStartTimeUtcMillis();
            long programEndTime = program.getEndTimeUtcMillis();
            if (programStartTime > lastProgramEndTime) {
                // Gap since the last program.
                log("    createProgramEntries() from program - create entry #2");
                entries.add(new TableEntry(channelId, lastProgramEndTime, programStartTime));
                lastProgramEndTime = programStartTime;
            }
            if (programEndTime > lastProgramEndTime) {
                log("    createProgramEntries() from program - create entry #3");
                if (i == 0) {
                    lastProgramEndTime = programStartTime;
                }
                entries.add(
                        new TableEntry(
                                channelId,
                                program,
                                lastProgramEndTime,
                                programEndTime,
                                false));
                lastProgramEndTime = programEndTime;
            }
        }

        timings.addSplit("step 1");

        if (entries.size() > 1) {
            TableEntry secondEntry = entries.get(1);
            if (secondEntry.entryStartUtcMillis < mStartUtcMillis + FIRST_ENTRY_MIN_DURATION) {
                log("    createProgramEntries() remove & set - create entry #4");
                // If the first entry's width doesn't have enough width, it is not good to show
                // the first entry from UI perspective. So we clip it out.
                entries.remove(0);
                entries.set(
                        0,
                        new TableEntry(
                                secondEntry.channelId,
                                secondEntry.program,
                                mStartUtcMillis,
                                secondEntry.entryEndUtcMillis,
                                secondEntry.mIsBlocked));
            }
        }

        timings.addSplit("step 2");

        log("    createProgramEntries() result channel:["+channelToString(channelId)+"], entries.size:"+(entries != null ? entries.size() : "null"));
        for (int i = 0 ; entries != null && i < entries.size() ; i++) {
            log("    createProgramEntries()   result channel:["+channelToString(channelId)+"], entries["+i+"]:"+entries.get(i));
        }

        log("  createProgramEntries() channel:["+channelToString(channelId)+"], done.");

        timings.addSplit("done - entries.size:"+(entries != null ? entries.size() : "null"));
        timings.dumpToLog();

        return entries;
    }

    public void onChannelTuned(long channelId) {
        log("onChannelTuned() channel:["+channelToString(channelId)+"]");
        TimingLogger timings = new TimingLogger(TAG, "onChannelTuned() ch:"+channelToString(channelId));
        timings.addSplit("start");

//        mProgramDataManager.onChannelTuned(channelId);

        List<TableEntry> entries = createProgramEntries(channelId);
        mChannelIdEntriesMap.put(channelId, entries);

        timings.addSplit("done");
        timings.dumpToLog();
    }

    public TableEntry[] getProgram(long channelId, int index, TableEntry[] returnEntries) {
        log("getProgram() channel:["+channelToString(channelId)+"]");
        TimingLogger timings = new TimingLogger(TAG, "getProgram() ch:"+channelToString(channelId));
        timings.addSplit("start");

        returnEntries[0] = null;
        returnEntries[1] = null;

        List<TableEntry> entries = mChannelIdEntriesMap.get(channelId);
        if (entries != null) {
            if (entries.size() >= 2) {
                returnEntries[1] = entries.get(1);
            }
            if (entries.size() >= 1) {
                returnEntries[0] = entries.get(0);
            }
        }

        //Log.d("SHLEE", "getProgram() size:"+(entries != null ? ""+entries.size() : "null"));

        log("getProgram() channel:["+channelToString(channelId)+"], return[0]:"+returnEntries[0]+", return[1]:"+returnEntries[1]);
        timings.addSplit("done");
        timings.dumpToLog();

        return returnEntries;
    }

    public List<TableEntry> getTableEntry(long channelId) {
        TimingLogger timings = new TimingLogger(TAG, "getTableEntry() ch:"+channelToString(channelId));
        timings.addSplit("start");

        List<TableEntry> cachedPrograms = mChannelIdEntriesMap.get(channelId);
        if (cachedPrograms == null) {
            timings.addSplit("done - return empty list");
            timings.dumpToLog();

            return Collections.emptyList();
        }
        int startIndex = getProgramIndexAt(cachedPrograms, JasmineEpgApplication.SystemClock().currentTimeMillis());
        timings.addSplit("done");
        timings.dumpToLog();

        return Collections.unmodifiableList(
                cachedPrograms.subList(startIndex, cachedPrograms.size()));
    }

    private int getProgramIndexAt(List<TableEntry> programs, long time) {
        TableEntry entry;
        for (int i = 0 ; i < programs.size() ; i++) {
            entry = programs.get(i);
            if (time <= entry.entryStartUtcMillis) {
                //Log.d("SHLEE", "getProgramIndexAt()#err1 return:"+i);
                return i;
            } else if (entry.entryStartUtcMillis <= time && time <= entry.entryEndUtcMillis) {
                //Log.d("SHLEE", "getProgramIndexAt() return:"+i);
                return i;
            }
        }
        //Log.d("SHLEE", "getProgramIndexAt()#err2 return:"+0);
        return 0;
    }

    public void test(long channelId) {
        if (mChannelIdEntriesMap.isEmpty()) {
            return;
        }

        List<TableEntry> entries = mChannelIdEntriesMap.get(channelId);
        log("test() channelID:"+channelId+", entries.size:"+(entries != null ? entries.size() : "null"));
        for (int i = 0 ; entries != null && i < entries.size() ; i++) {
            log("test() channelID:"+channelId+", entries["+i+"]:"+entries.get(i));
        }
    }

    //for debugging
    private final HashMap<Long, Channel> channelMap = new HashMap<>();

    private String channelToString(long channelId) {
        Channel ch = channelMap.get(channelId);
//        Log.d("SHLEE", "channelToString() channelId:"+channelId);
//        Log.d("SHLEE", "channelToString() ch:"+ch);
//        Log.d("SHLEE", "channelToString() ch.getId:"+ (ch != null ? ch.getId() : "null"));
        if (ch == null) {
            channelMap.clear();
            for (Channel channel : mChannels) {
                channelMap.put(channel.getId(), channel);
            }
            ch = channelMap.get(channelId);
        }
        return (ch != null ? ch.getDisplayNumber() : "null (chId:"+channelId+")");
    }

    private static String channelToString(Channel ch) {
        return ch.getDisplayNumber();
    }
}
