/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.def;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class CouponTypeDeserializer implements JsonDeserializer<CouponType> {
    private final String KEY_CODE = "code";
    private final String KEY_VALUE = "value";

    @Override
    public CouponType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        String str = "";
        try {
            if (object.has(KEY_CODE) && object.get(KEY_CODE) != null && !object.get(KEY_CODE).isJsonNull()) {
                str = object.get(KEY_CODE).getAsString();
            }
        }catch (Exception|Error e){
        }
        return CouponType.findByName(str);
    }
}
