package kr.altimedia.launcher.jasmin.dm.contents.obj;

/**
 * Created by mc.kim on 13,08,2020
 */
public enum TrackingEvent {
    impression("impression"), //광고 노출 집계
    start("start"), //광고노출 시작
    firstQuartile("firstQuartile"), // 광고 25% 시청시
    midpoint("midpoint"), // 광고 50% 시청시
    thirdQuartile("thirdQuartile"), //광고 75% 시청시
    complete("complete"), //광고 100% 시청시
    skip("skip"), //광고가 skip되었을 때
    close("close"), //광고를 보다가 나가기를 한 경우
    pause("pause"), //광고를 pause 한경우
    resume("resume"), //광고를 이어보기 시작한 경우
    click("click"),//광고를 클릭(자세히보기 등)한 경우
    Invalid("invalid");
    private final String action;

    TrackingEvent(String action) {
        this.action = action;
    }

    public static TrackingEvent findByActionName(String name) {
        TrackingEvent[] types = values();
        for (TrackingEvent type : types) {
            if (type.action.equalsIgnoreCase(name.trim())) {
                return type;
            }
        }
        return Invalid;
    }
}
