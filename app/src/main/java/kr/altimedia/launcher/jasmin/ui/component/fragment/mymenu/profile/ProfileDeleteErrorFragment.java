/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;

public class ProfileDeleteErrorFragment extends Fragment {
    public static final String CLASS_NAME = ProfileDeleteErrorFragment.class.getName();
    private static final String TAG = ProfileDeleteErrorFragment.class.getSimpleName();

    private TextView btnDone;

    @NotNull
    public static ProfileDeleteErrorFragment newInstance() {
        ProfileDeleteErrorFragment fragment = new ProfileDeleteErrorFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(Log.INCLUDE) {
            Log.d(TAG, "onCreateView");
        }
        return inflater.inflate(R.layout.fragment_mymenu_profile_delete_error, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
    }

    private void initView(View view) {

        btnDone = view.findViewById(R.id.btnDone);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideView();
            }
        });

        btnDone.requestFocus();
    }

    private void hideView(){
        ProfileManageDialog manageDialog = ((ProfileManageDialog) getParentFragment());
//        if (manageDialog.isLinkFromMyMenu()) {
//            manageDialog.dismiss();
//        } else {
            manageDialog.backToFragment();
//        }
    }
}
