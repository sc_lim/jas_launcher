/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.SettingProfileBaseFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment.KEY_PROFILE;

public class SettingProfilePinFragment extends SettingProfileBaseFragment {
    public static final String CLASS_NAME = SettingProfilePinFragment.class.getName();
    private static final String TAG = SettingProfilePinFragment.class.getSimpleName();

    private PasswordView password;

    private MbsDataTask checkPinTask;

    private SettingProfilePinFragment() {

    }

    public static SettingProfilePinFragment newInstance(Bundle bundle) {
        SettingProfilePinFragment fragment = new SettingProfilePinFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_pin_setting;
    }

    @Override
    protected void initView(View view) {
        initPasswordView(view);
    }

    private void initPasswordView(View view) {
        password = view.findViewById(R.id.pin);
        password.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyInputChange, isFull : " + isFull + ", input : " + input);
                }

                if (isFull) {
                    checkPinCode(input);
                }
            }
        });

        OnKeyListener onKeyListener = new OnKeyListener();
        password.setOnKeyListener(onKeyListener);
    }

    private void checkPinCode(String password) {
        ProfileInfo profileInfo = getArguments().getParcelable(KEY_PROFILE);
        checkPinTask = onFragmentChange.getSettingTaskManager().checkAccountPinCode(
                profileInfo.getProfileId(), password,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        onFragmentChange.showProgressbar(loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onFailed");
                        }

                        onFragmentChange.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onSuccess, result : " + result);
                        }

                        if (result) {
                            showParentalFragment();
                        } else {
                            setPinError();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        onFragmentChange.getSettingTaskManager().showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingProfilePinFragment.this.getContext();
                    }
                });
    }

    private void showParentalFragment() {
        if (Log.INCLUDE) {
            Log.d(TAG, "showNextFragment, menu : ");
        }

        SettingProfileParentalFragment settingProfileParentalFragment = SettingProfileParentalFragment.newInstance(getArguments());
        settingProfileParentalFragment.setOnFragmentChange(getOnFragmentChange());
        settingProfileParentalFragment.setOnPushProfileChangeMessage(mOnPushProfileChangeMessage);
        onFragmentChange.onNextFragment(settingProfileParentalFragment, SettingProfileParentalFragment.CLASS_NAME);
    }

    private void setPinError() {
        getView().findViewById(R.id.description).setVisibility(View.VISIBLE);
        password.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (checkPinTask != null) {
            checkPinTask.cancel(true);
        }
    }

    private static class OnKeyListener implements View.OnKeyListener {
        public OnKeyListener() {
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != KeyEvent.ACTION_DOWN) {
                return false;
            }

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    return true;
            }

            return false;
        }
    }
}
