/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamType;
import kr.altimedia.launcher.jasmin.media.MediaPlayerAdapter;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.component.activity.interactor.VideoPlaybackInteractor;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.playback.PlaybackVideoFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.playback.TrailerVideoFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelLayout;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.VideoSidePanelOptionProvider;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.VodResumeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.drawer.EdgeDrawerLayout;

/**
 * Created by mc.kim on 06,01,2020
 */
public class VideoPlaybackActivity extends BaseActivity
        implements SidePanelLayout.SidePanelActionListener, VideoPlaybackInteractor {
    public enum PlayerType {
        Movie, Trailer, Preview
    }

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Log.INCLUDE) {
                Log.d(TAG, "action : " + action);
            }
            switch (action) {
                case PushMessageReceiver.ACTION_UPDATED_PROFILE:
                    PushMessageReceiver.ProfileUpdateType typeData = (PushMessageReceiver.ProfileUpdateType) intent.getSerializableExtra(PushMessageReceiver.KEY_TYPE);
                    if (typeData == null) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "ACTION_UPDATED_PROFILE not contain type so return");
                        }
                        return;
                    }
                    if (Log.INCLUDE) {
                        Log.d(TAG, "ACTION_UPDATED_PROFILE typeData : " + typeData);
                    }

                    switch (typeData) {
                        case userDeleted:
                            VideoPlaybackActivity.this.setResult(LauncherActivity.RESULT_CODE_USER_DELETED);
                            finish();
                            break;
                    }
                    break;
            }
        }
    };
    public static final String KEY_START_TIME = "startTime";
    public static final String KEY_MOVIE = "movie";
    public static final String KEY_MOVIE_ID = "movie_id";
    public static final String KEY_THUMBNAIL_DATA = "thumbnail";
    public static final String KEY_MOVIE_INDEX = "movie_index";
    public static final String KEY_TYPE = "type";
    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private final String TAG = VideoPlaybackActivity.class.getSimpleName();
    private EdgeDrawerLayout mDrawer = null;
    private SidePanelLayout mSidePanel = null;
    private ViewGroup containerView = null;
    private TvOverlayManager mTvOverlayManager;
    private ChannelDataManager mChannelDataManager;
    private ProgramDataManager mProgramDataManager;
    private TvInputManagerHelper mTvInputManagerHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_playback);

        mDrawer = findViewById(R.id.drawer_layout);
        mSidePanel = findViewById(R.id.sidePanel);
        containerView = findViewById(R.id.containerView);
        mSidePanel.setSidePanelActionListener(this);
        mDrawer.addDrawerListener(mSimpleDrawerListener);
        mDrawer.setClosedPaddingRate(0f);

        LayoutInflater inflater = LayoutInflater.from(this);
        View loadingView = inflater.inflate(R.layout.view_loading, findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);


        mChannelDataManager = TvSingletons.getSingletons(this).getChannelDataManager();
        mProgramDataManager = TvSingletons.getSingletons(this).getProgramDataManager();
        mTvInputManagerHelper = TvSingletons.getSingletons(this).getTvInputManagerHelper();

        mTvOverlayManager = new TvOverlayManager(this,
                containerView,
                mChannelDataManager,
                mTvInputManagerHelper,
                mProgramDataManager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CWMPServiceProvider.getInstance().setWatchingVod(null);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MEDIA_STOP) {
            onBackPressed();
            return true;
        }
//        else if (mTvOverlayManager.onKeyNum(keyCode, event)) {
//            return true;
//        }
        return super.onKeyDown(keyCode, event);
    }

    private final EdgeDrawerLayout.SimpleDrawerListener mSimpleDrawerListener = new EdgeDrawerLayout.SimpleDrawerListener() {
        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);

            if (Log.INCLUDE) {
                Log.d(TAG, "onDrawerOpened : " + drawerView.getClass().getSimpleName());
            }
            setFocusAllow(containerView, false);
            setFocusAllow(((ViewGroup) drawerView), true);
            drawerView.requestFocus();
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);

            if (Log.INCLUDE) {
                Log.d(TAG, "onDrawerClosed : " + drawerView.getClass().getSimpleName());
            }
            setFocusAllow(containerView, true);
            setFocusAllow(((ViewGroup) drawerView), false);
            containerView.requestFocus();
        }

        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            super.onDrawerSlide(drawerView, slideOffset);
        }
    };

    @Override
    public void openMenu() {
        if (Log.INCLUDE) {
            Log.d(TAG, "EdgeDrawerLayout openMenu");
        }
        if (!isEnabled()) {
            return;
        }
        if (isOpenMenu()) {
            return;
        }
        mSidePanel.renewalPanelData(this);
        mDrawer.openDrawer(mSidePanel);
    }

    private boolean isEnabled() {
        return mSidePanel.isEnabled();
    }

    public boolean isOpenMenu() {
        return mDrawer.isDrawerOpen(mSidePanel);
    }

    @Override
    public void closeMenu() {
        if (Log.INCLUDE) {
            Log.d(TAG, "EdgeDrawerLayout closeMenu");
        }
        if (!isEnabled()) {
            return;
        }
        if (!isOpenMenu()) {
            return;
        }

        mDrawer.closeDrawer(mSidePanel);
    }

    private void setFocusAllow(ViewGroup viewGroup, boolean focus) {
        viewGroup.setDescendantFocusability(focus ? ViewGroup.FOCUS_AFTER_DESCENDANTS : ViewGroup.FOCUS_BLOCK_DESCENDANTS);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();

        if (savedInstanceState == null) {
            tuneContent(bundle);
        }
    }

    private void setStreamInfo(Bundle bundle) {
        PlayerType type = (PlayerType) bundle.getSerializable(KEY_TYPE);
        Parcelable sourceData = bundle.getParcelable(KEY_MOVIE);
        final int trailerIndex = bundle.getInt(VideoPlaybackActivity.KEY_MOVIE_INDEX, 0);
        String title = null;
        switch (type) {
            case Preview:
            case Trailer:
                if (sourceData instanceof Content) {
                    Content content = (Content) sourceData;
                    List<StreamInfo> streamInfoList = content.getTrailerStreamInfo();
                    StreamInfo info = streamInfoList.get(trailerIndex);
                    title = type == PlayerType.Preview ? content.getTitle() + " Preview" : info.getTitle();
                } else {
                    SeriesContent content = (SeriesContent) sourceData;
                    List<StreamInfo> streamInfoList = content.getTrailer();
                    StreamInfo info = streamInfoList.get(trailerIndex);
                    title = type == PlayerType.Preview ? content.getSeriesName() + " Preview" : info.getTitle();
                }
                break;
            case Movie:
                if (sourceData != null) {
                    if (sourceData instanceof Content) {
                        Content content = (Content) sourceData;
                        title = content.getTitle();
                    } else {
                        SeriesContent content = (SeriesContent) sourceData;
                        title = content.getSeriesName();
                    }
                }
                break;
        }

        CWMPServiceProvider.getInstance().setWatchingVod(title);
    }

    private void tuneContent(Bundle bundle) {
        PlayerType type = (PlayerType) bundle.getSerializable(KEY_TYPE);
        setStreamInfo(bundle);
        switch (type) {
            case Preview:
            case Trailer:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerView, TrailerVideoFragment.newInstance(bundle))
                        .commit();
                break;
            case Movie:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.containerView, PlaybackVideoFragment.newInstance(bundle))
                        .commit();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.containerView);
        if (fragment instanceof PlaybackVideoFragment) {
            ((PlaybackVideoFragment) fragment).onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onErrorWhenReady(Content content, String errorCode) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onErrorWhenReady : " + errorCode);
        }
        Intent intent = new Intent();
        intent.putExtra(LauncherActivity.KEY_VOD_DETAIL, content);
        intent.putExtra(LauncherActivity.KEY_ERROR_CODE, errorCode);
        setResult(LauncherActivity.RESULT_CODE_ERROR_READY, intent);
        finish();
    }


    @Override
    public void requestTuneTrailer(Parcelable sourceData, StreamInfo selectedStream) {
        final List<StreamInfo> streamInfoList;
        if (sourceData instanceof Content) {
            Content content = (Content) sourceData;
            streamInfoList = content.getTrailerStreamInfo();
        } else {
            SeriesContent content = (SeriesContent) sourceData;
            streamInfoList = content.getTrailer();
        }
        if (streamInfoList == null) {
            return;
        }

        int index = streamInfoList.indexOf(selectedStream);
        if (index == -1) {
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_TYPE, selectedStream.getStreamType() == StreamType.Trailer ? PlayerType.Trailer : PlayerType.Preview);
        bundle.putParcelable(KEY_MOVIE, sourceData);
        bundle.putInt(KEY_MOVIE_INDEX, index);
        tuneContent(bundle);
    }

    @Override
    public void requestVodWatched(Content content) {
        Intent intent = new Intent();
        intent.putExtra(LauncherActivity.KEY_VOD_DETAIL, content);
        setResult(LauncherActivity.RESULT_CODE_WATCH_FINISHED, intent);
        finish();
    }

    @Override
    public void requestShowVodDetail(Content content) {
        Intent intent = new Intent();
        intent.putExtra(LauncherActivity.KEY_VOD_DETAIL, content);
        setResult(LauncherActivity.RESULT_CODE_SHOW_VOD_DETAIL, intent);
        finish();
    }

    @Override
    public void requestTuneVod(Content content) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_TYPE, PlayerType.Movie);
        bundle.putParcelable(KEY_MOVIE, content);
        if (content.getResumeTime() != 0) {
            VodResumeDialogFragment resumeDialogFragment = VodResumeDialogFragment.newInstance(content, new VodResumeDialogFragment.OnButtonsAction() {
                @Override
                public void onClickButtons(DialogFragment dialogFragment, int position) {
                    dialogFragment.dismiss();
                    bundle.putLong(KEY_START_TIME, position);
                    tuneContent(bundle);
                }
            });
            resumeDialogFragment.show(getSupportFragmentManager(), VodResumeDialogFragment.CLASS_NAME);
            return;
        }

        tuneContent(bundle);
    }


    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, new IntentFilter(PushMessageReceiver.ACTION_UPDATED_PROFILE));
        if (pendingFinish) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.containerView);
            if (fragment instanceof PlaybackVideoFragment) {
                ((PlaybackVideoFragment) fragment).onBackPressed();
            } else {
                finish();
            }
        }
    }

    private boolean pendingFinish = false;

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        pendingFinish = true;
    }

    private boolean isScreenOn(Context context) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        return pm.isInteractive();
    }

    @Override
    public void setSideOptionProvider(Content content, MediaPlayerAdapter adapter) {
        VideoSidePanelOptionProvider sidePanelOptionProvider = new VideoSidePanelOptionProvider(content) {
            private int currentSelectedSubtitleIndex = -1;
            private int currentSelectedAudioIndex = -1;

            @Override
            public void reset() {
                super.reset();
                currentSelectedSubtitleIndex = adapter.getSelectedSubtitleTrackIndex();
                currentSelectedAudioIndex = adapter.getSelectedAudioTrackIndex();
            }

            @Override
            public ArrayList<String> getSubtitleList() {
                ArrayList<String> subtitleList = adapter.getSubtitleList();
                subtitleList.add(0, getOffString());
                return subtitleList;
            }

            @Override
            public ArrayList<String> getAudioList() {
                ArrayList<String> audioList = adapter.getAudioList();
                return audioList;
            }

            @Override
            public void setSubtitle(String lang) {
                super.setSubtitle(lang);
                if (Log.INCLUDE) {
                    Log.d(TAG, "setSubtitle : " + lang);
                }
                ArrayList<String> subtitleList = adapter.getSubtitleList();
                int index = subtitleList.indexOf(lang);
                if (index == -1) {
                    Locale locale = Locale.forLanguageTag(lang);
                    index = subtitleList.indexOf(locale.getDisplayName());
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "setSubtitle | index : " + index);
                }
                currentSelectedSubtitleIndex = index;
                adapter.setSelectedSubtitleTrackIndex(index);
            }

            @Override
            public void setAudio(String lang) {
                super.setAudio(lang);
                if (Log.INCLUDE) {
                    Log.d(TAG, "setAudio : " + lang);
                }
                ArrayList<String> audioList = adapter.getAudioList();
                int index = audioList.indexOf(lang);
                if (index == -1) {
                    Locale locale = Locale.forLanguageTag(lang);
                    index = audioList.indexOf(locale.getDisplayName());
                }

                if (Log.INCLUDE) {
                    Log.d(TAG, "setAudio | index : " + index);
                }
                currentSelectedAudioIndex = index;
                adapter.setSelectedAudioTrackIndex(index);
            }

            @Override
            public String getCurrentSubtitleLanguage() {
                int index = adapter.getSelectedSubtitleTrackIndex();
                if (index != currentSelectedSubtitleIndex) {
                    index = currentSelectedSubtitleIndex;
                }
                ArrayList<String> subtitleList = adapter.getSubtitleList();
                if (index < 0) {
                    return super.getCurrentSubtitleLanguage();
                } else {
                    String currentSubtitle = subtitleList.get(index);
                    if (!StringUtils.nullToEmpty(currentSubtitle).isEmpty() && !currentSubtitle.equalsIgnoreCase("null")) {
                        return currentSubtitle;
                    } else {
                        return super.getCurrentSubtitleLanguage();
                    }
                }
            }

            @Override
            public String getCurrentAudioLanguage() {
                int index = adapter.getSelectedAudioTrackIndex();
                if (index != currentSelectedAudioIndex) {
                    index = currentSelectedAudioIndex;
                }
                ArrayList<String> audioList = adapter.getAudioList();
                if (index < 0) {
                    return super.getCurrentSubtitleLanguage();
                } else {
                    String currentAudio = audioList.get(index);
                    if (!StringUtils.nullToEmpty(currentAudio).isEmpty() && !currentAudio.equalsIgnoreCase("null")) {
                        return currentAudio;
                    } else {
                        return super.getCurrentSubtitleLanguage();
                    }
                }
            }

            @Override
            public String getOffString() {
                return getString(R.string.off);
            }

            @Override
            public String getSubtitleStyle() {
                return adapter.getCurrentFontStyle();
            }

            @Override
            public void setSubtitleStyle(String style) {
                super.setSubtitleStyle(style);
                adapter.setSubtitleStyleByName(style);
            }
        };
        mSidePanel.setSidePanelType(getApplicationContext(), SidePanelType.VOD, sidePanelOptionProvider);
    }

    public TvOverlayManager getTvOverlayManager() {
        return mTvOverlayManager;
    }


    public void notifyTuneChannel(Channel channel) {
        if (channel == null) {
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyTuneChannel : " + channel.toString());
        }

        PlaybackUtil.startLiveTvActivityWithChannel(VideoPlaybackActivity.this, channel.getId());
    }

}
