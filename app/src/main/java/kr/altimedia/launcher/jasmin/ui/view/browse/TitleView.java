/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.SearchOrbView;
import androidx.leanback.widget.TitleViewAdapter;

import kr.altimedia.launcher.jasmin.R;


public class TitleView extends FrameLayout implements TitleViewAdapter.Provider {
    private ImageView mBadgeView;
    private TextView mTextView;
    private SearchOrbView mSearchOrbView;
    private int flags;
    private boolean mHasSearchListener;
    private final TitleViewAdapter mTitleViewAdapter;

    public TitleView(Context context) {
        this(context, (AttributeSet) null);
    }

    public TitleView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.browseTitleViewStyle);
    }

    public TitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.flags = 6;
        this.mHasSearchListener = false;
        this.mTitleViewAdapter = new TitleViewAdapter() {
            public View getSearchAffordanceView() {
                return TitleView.this.getSearchAffordanceView();
            }

            public void setOnSearchClickedListener(OnClickListener listener) {
                TitleView.this.setOnSearchClickedListener(listener);
            }

            public void setAnimationEnabled(boolean enable) {
                TitleView.this.enableAnimation(enable);
            }

            public Drawable getBadgeDrawable() {
                return TitleView.this.getBadgeDrawable();
            }

            public SearchOrbView.Colors getSearchAffordanceColors() {
                return TitleView.this.getSearchAffordanceColors();
            }

            public CharSequence getTitle() {
                return TitleView.this.getTitle();
            }

            public void setBadgeDrawable(Drawable drawable) {
                TitleView.this.setBadgeDrawable(drawable);
            }

            public void setSearchAffordanceColors(SearchOrbView.Colors colors) {
                TitleView.this.setSearchAffordanceColors(colors);
            }

            public void setTitle(CharSequence titleText) {
                TitleView.this.setTitle(titleText);
            }

            public void updateComponentsVisibility(int flags) {
                TitleView.this.updateComponentsVisibility(flags);
            }
        };
        LayoutInflater inflater = LayoutInflater.from(context);
        View rootView = inflater.inflate(R.layout.view_title, this);
        this.mBadgeView = (ImageView) rootView.findViewById(R.id.title_badge);
        this.mTextView = (TextView) rootView.findViewById(R.id.title_text);
        this.mSearchOrbView = (SearchOrbView) rootView.findViewById(R.id.title_orb);
        this.setClipToPadding(false);
        this.setClipChildren(false);
    }

    public void setTitle(CharSequence titleText) {
        this.mTextView.setText(titleText);
        this.updateBadgeVisibility();
    }

    public CharSequence getTitle() {
        return this.mTextView.getText();
    }

    public void setBadgeDrawable(Drawable drawable) {
        this.mBadgeView.setImageDrawable(drawable);
        this.updateBadgeVisibility();
    }

    public Drawable getBadgeDrawable() {
        return this.mBadgeView.getDrawable();
    }

    public void setOnSearchClickedListener(OnClickListener listener) {
        this.mHasSearchListener = listener != null;
        this.mSearchOrbView.setOnOrbClickedListener(listener);
        this.updateSearchOrbViewVisiblity();
    }

    public View getSearchAffordanceView() {
        return this.mSearchOrbView;
    }

    public void setSearchAffordanceColors(SearchOrbView.Colors colors) {
        this.mSearchOrbView.setOrbColors(colors);
    }

    public SearchOrbView.Colors getSearchAffordanceColors() {
        return this.mSearchOrbView.getOrbColors();
    }

    public void enableAnimation(boolean enable) {
        this.mSearchOrbView.enableOrbColorAnimation(enable && this.mSearchOrbView.hasFocus());
    }

    public void updateComponentsVisibility(int flags) {
        this.flags = flags;
        if ((flags & 2) == 2) {
            this.updateBadgeVisibility();
        } else {
            this.mBadgeView.setVisibility(View.GONE);
            this.mTextView.setVisibility(View.GONE);
        }

        this.updateSearchOrbViewVisiblity();
    }

    private void updateSearchOrbViewVisiblity() {
//        int visibility = this.mHasSearchListener && (this.flags & 4) == 4 ? 0 : 4;

        boolean visibility = this.mHasSearchListener && (this.flags & 4) == 4;

        this.mSearchOrbView.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
    }

    private void updateBadgeVisibility() {
        Drawable drawable = this.mBadgeView.getDrawable();
        if (drawable != null) {
            this.mBadgeView.setVisibility(View.VISIBLE);
            this.mTextView.setVisibility(View.GONE);
        } else {
            this.mBadgeView.setVisibility(View.GONE);
            this.mTextView.setVisibility(View.VISIBLE);
        }

    }

    public TitleViewAdapter getTitleViewAdapter() {
        return this.mTitleViewAdapter;
    }
}
