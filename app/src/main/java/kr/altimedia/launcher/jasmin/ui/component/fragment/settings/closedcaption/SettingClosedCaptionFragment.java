/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.closedcaption;

import android.view.View;

import androidx.leanback.widget.ArrayObjectAdapter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.CheckButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.CheckButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import com.altimedia.util.Log;

public class SettingClosedCaptionFragment extends SettingBaseFragment implements SettingBaseButtonItemBridgeAdapter.OnClickButton<SettingClosedCaptionFragment.ClosedCaptionButton> {
    public static final String CLASS_NAME = SettingClosedCaptionFragment.class.getName();
    private static final String TAG = SettingClosedCaptionFragment.class.getSimpleName();

    private final SettingControl settingControl = SettingControl.getInstance();

    private ArrayObjectAdapter objectAdapter;
    private CheckButtonBridgeAdapter checkButtonBridgeAdapter;

    private SettingClosedCaptionFragment() {
    }

    public static SettingClosedCaptionFragment newInstance() {
        SettingClosedCaptionFragment fragment = new SettingClosedCaptionFragment();
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_closed_caption;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        initData(view);
        initList(view);
    }

    private void initData(View view) {
        boolean isOn = settingControl.isClosedCaptionEnable();

        ClosedCaptionButton.ON.setChecked(isOn);
        ClosedCaptionButton.OFF.setChecked(!isOn);
    }

    private void initList(View view) {
        objectAdapter = new ArrayObjectAdapter(new CheckButtonPresenter());
        objectAdapter.add(ClosedCaptionButton.ON);
        objectAdapter.add(ClosedCaptionButton.OFF);

        VerticalGridView gridView = view.findViewById(R.id.gridView);
        checkButtonBridgeAdapter = new CheckButtonBridgeAdapter(objectAdapter);
        checkButtonBridgeAdapter.setListener(this);
        gridView.setAdapter(checkButtonBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        gridView.setVerticalSpacing(spacing);
    }

    @Override
    public boolean onClickButton(ClosedCaptionButton item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        boolean isOn = item.getType() == ClosedCaptionButton.ON.getType();

        if ((isOn && ClosedCaptionButton.ON.isChecked()) || (!isOn && ClosedCaptionButton.OFF.isChecked())) {
            return true;
        }

        ClosedCaptionButton.ON.setChecked(isOn);
        ClosedCaptionButton.OFF.setChecked(!isOn);

        objectAdapter.replace(ClosedCaptionButton.ON.getType(), ClosedCaptionButton.ON);
        objectAdapter.replace(ClosedCaptionButton.OFF.getType(), ClosedCaptionButton.OFF);

        settingControl.setClosedCaptionEnable(isOn);

        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        checkButtonBridgeAdapter.setListener(null);
    }

    protected enum ClosedCaptionButton implements CheckButtonPresenter.CheckButtonItem {
        ON(0, R.string.on), OFF(1, R.string.off);

        private int type;
        private int title;
        private boolean isChecked;

        ClosedCaptionButton(int type, int title) {
            this.type = type;
            this.title = title;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public int getTitle() {
            return title;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public boolean isChecked() {
            return isChecked;
        }

        @Override
        public boolean isEnabledCheck() {
            return true;
        }
    }
}