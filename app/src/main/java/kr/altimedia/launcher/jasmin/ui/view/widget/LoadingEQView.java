/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import kr.altimedia.launcher.jasmin.R;

public class LoadingEQView extends AppCompatImageView {
    public LoadingEQView(Context context) {
        super(context);
        init(context);
    }

    public LoadingEQView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LoadingEQView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == View.VISIBLE) {
            final CallbackAnimationDrawable frameAnimation = new CallbackAnimationDrawable((AnimationDrawable) getContext().getDrawable(R.drawable.loading_eq1_start));
            setImageDrawable(frameAnimation);
            post(new Runnable() {
                public void run() {
                    frameAnimation.start();
                }
            });
            frameAnimation.setAnimDrawableListener(new CallbackAnimationDrawable.OnAnimDrawableCallback() {
                @Override
                public void onAnimationOneShotFinished() {
                    final AnimationDrawable frameAnimation = (AnimationDrawable) getContext().getDrawable(R.drawable.loading_eq2_loop);
                    setImageDrawable(frameAnimation);
                    post(new Runnable() {
                        public void run() {
                            frameAnimation.start();
                        }
                    });
                }
            });
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        final AnimationDrawable frameAnimation = (AnimationDrawable) getBackground();
        if (frameAnimation == null) {
            return;
        }
        post(new Runnable() {
            public void run() {
                frameAnimation.stop();
            }
        });
    }
}
