/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.os.Handler;
import android.os.Looper;

public class AutoHider {
    private final long autoHideTime;
    private final Runnable runnable;

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    public AutoHider(long autoHideTime, Runnable runnable) {
        this.autoHideTime = autoHideTime;
        this.runnable = runnable;
    }

    public void refreshTimer() {
        mHandler.removeCallbacks(runnable);
        mHandler.postDelayed(runnable, autoHideTime);
    }

    public void cancel() {
        mHandler.removeCallbacks(runnable);
    }
}
