/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account;

import android.content.Context;
import android.view.View;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.TextButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.TextButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class SettingAccountPinManageFragment extends SettingBaseFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton<SettingAccountPinManageFragment.PinButton> {
    public static final String CLASS_NAME = SettingAccountPinManageFragment.class.getName();
    public static final String TAG = SettingAccountPinManageFragment.class.getSimpleName();

    private VerticalGridView gridView;
    private ArrayObjectAdapter objectAdapter;
    private TextButtonBridgeAdapter textButtonBridgeAdapter;

    private SettingTaskManager settingTaskManager;

    private SettingAccountPinManageFragment() {
    }

    public static SettingAccountPinManageFragment newInstance() {
        return new SettingAccountPinManageFragment();
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_pin_manage;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        settingTaskManager = new SettingTaskManager(getFragmentManager(), mTvOverlayManager);
        initGridView(view);
    }

    private void initGridView(View view) {
        ArrayList<PinButton> buttons = new ArrayList<>(Arrays.asList(PinButton.values()));

        TextButtonPresenter textButtonPresenter = new TextButtonPresenter();
        textButtonPresenter.setResourceId(R.layout.presenter_text_button);
        objectAdapter = new ArrayObjectAdapter(textButtonPresenter);
        objectAdapter.addAll(0, buttons);

        gridView = view.findViewById(R.id.gridView);
        textButtonBridgeAdapter = new TextButtonBridgeAdapter(objectAdapter);
        textButtonBridgeAdapter.setListener(this);
        gridView.setAdapter(textButtonBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        gridView.setVerticalSpacing(spacing);
    }

    private TvOverlayManager mTvOverlayManager = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach: " + mTvOverlayManager);
        }
    }

    @Override
    public boolean onClickButton(SettingAccountPinManageFragment.PinButton item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        TvOverlayManager tvOverlayManager = onFragmentChange.getTvOverlayManager();
        int type = item.getType();

        if (type == PinButton.CHANGE_PIN.getType()) {
            final SettingAccountPinChangeDialogFragment changeDialogFragment = SettingAccountPinChangeDialogFragment.newInstance();
            changeDialogFragment.setOnPinChangeResultCallback(new SettingAccountPinChangeDialogFragment.OnPinChangeResultCallback() {
                @Override
                public void onSuccess() {
                    toastSave();
                    changeDialogFragment.dismiss();
                }

                @Override
                public void onFail(int key) {
                    FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getContext());
                    failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                }

                @Override
                public void onError(String errorCode, String message) {
                    settingTaskManager.showErrorDialog(getContext(), TAG, errorCode, message);
                }

                @Override
                public void onCancel() {
                    changeDialogFragment.dismiss();
                }

                @Override
                public SettingTaskManager getSettingTaskManager() {
                    return settingTaskManager;
                }
            });

            tvOverlayManager.showDialogFragment(changeDialogFragment);
            return true;
        }

        if (type == PinButton.RESET_PIN.getType()) {
            SettingAccountPinResetDialogFragment settingAccountPinResetDialogFragment = SettingAccountPinResetDialogFragment.newInstance();
            settingAccountPinResetDialogFragment.setOnResetChangeListener(new SettingAccountPinResetDialogFragment.OnResetChangeListener() {
                @Override
                public void onRestPin() {
                    gridView.setSelectedPosition(PinButton.RESET_PIN.getType());
                }

                @Override
                public void onResetAndChangePin() {
                    gridView.setSelectedPosition(PinButton.CHANGE_PIN.getType());
                }
            });
            tvOverlayManager.showDialogFragment(settingAccountPinResetDialogFragment);
            return true;
        }

        return true;
    }

    private void toastSave() {
        JasminToast.makeToast(getContext(), R.string.saved);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        textButtonBridgeAdapter.setListener(null);
    }

    protected enum PinButton implements TextButtonPresenter.TextButtonItem {
        CHANGE_PIN(0, R.string.change_account_pin), RESET_PIN(1, R.string.reset_account_pin);

        private int type;
        private int title;
        private boolean isChecked = false;

        PinButton(int type, int title) {
            this.type = type;
            this.title = title;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public int getTitle() {
            return title;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }
}
