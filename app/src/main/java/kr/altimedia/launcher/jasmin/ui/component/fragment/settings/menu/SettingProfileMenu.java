/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu;

import kr.altimedia.launcher.jasmin.R;

public enum SettingProfileMenu implements SettingMenu {
    PARENTAL(R.string.parental_controls, 0), LOCKS(R.string.profile_lock, 1);

    private int menuName;
    private int menuId;
    private boolean isEnable = true;

    SettingProfileMenu(int menuName, int menuId) {
        this.menuName = menuName;
        this.menuId = menuId;
    }

    @Override
    public int getMenuName() {
        return menuName;
    }

    @Override
    public int getMenuId() {
        return menuId;
    }

    @Override
    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean enable) {
        isEnable = enable;
    }
}
