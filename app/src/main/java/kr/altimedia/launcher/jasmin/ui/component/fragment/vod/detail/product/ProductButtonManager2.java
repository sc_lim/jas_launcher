/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product;

import android.content.Context;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Duration;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PackageContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.dm.def.RentalType;

public class ProductButtonManager2 {
    private static final String TAG = ProductButtonManager2.class.getSimpleName();

    private final ArrayList<VodProductType> POSITION_PRIORITY = new ArrayList<>(
            Arrays.asList(VodProductType.WATCH, VodProductType.RENT, VodProductType.BUY, VodProductType.ALL_EPISODE, VodProductType.PACKAGE, VodProductType.SUBSCRIBE));
    private final ArrayList<VodProductType> PACKAGE_POSITION_PRIORITY = new ArrayList<>(
            Arrays.asList(VodProductType.RENT_PACKAGE, VodProductType.BUY_PACKAGE));
    private final ArrayList<VodProductType> VIEW_LIMITED_PERIOD_PRIORITY = new ArrayList<>(
            Arrays.asList(VodProductType.BUY, VodProductType.SUBSCRIBE, VodProductType.RENT));

    private Context context;

    private ArrayList<Product> singleList = new ArrayList<>();
    private HashMap<String, ArrayList<Product>> seriesMap = new HashMap<>();
    private HashMap<String, ArrayList<Product>> packageMap = new HashMap<>();
    private HashMap<String, ArrayList<Product>> subscriptionMap = new HashMap<>();
    private ArrayList<Product> purchasedList = new ArrayList<>();

    public ProductButtonManager2(Context context) {
        this.context = context;
    }

    public ArrayList<ProductButtonItem> getSeriesButtonList() {
        ArrayList<Product> products = getMainList(ProductType.SERIES);

        ArrayList<ProductButtonItem> productButtonList = new ArrayList<>();
        for (Product product : products) {
            VodProductType vodProductType = VodProductType.getVodProductType(false, product);
            ProductButtonItem productButtonItem = new ProductButtonItem(product, vodProductType, product.getPrice(), product.getCurrency());
            productButtonList.add(productButtonItem);
        }

        return productButtonList;
    }

    public ArrayList<Product> getPackageMainList() {
        return getMainList(ProductType.PACKAGE);
    }

    private ArrayList<Product> getMainList(ProductType productType) {
        ArrayList<Product> list = new ArrayList<>();

        switch (productType) {
            //Rent, Buy
            case SINGLE:
                list = singleList;
                break;
            //All Ep. Rent / Buy in same productId Group
            case SERIES:
                String key = seriesMap.keySet().iterator().next();
                list = seriesMap.get(key);
                break;
            //each buy or lower package items list (present package each productId Group)
            case PACKAGE:
                HashMap<String, ArrayList<Product>> map = packageMap;
                Iterator<String> iterator = map.keySet().iterator();
                while (iterator.hasNext()) {
                    Product buyProduct = null;
                    Product subscriptionProduct = null;
                    Product rentProduct = null;
                    Product lowPriceProduct = null;

                    ArrayList<Product> productList = map.get(iterator.next());
                    for (Product product : productList) {
                        RentalType rentalType = product.getRentalType();

                        if (lowPriceProduct == null || lowPriceProduct.getPrice() > product.getPrice()) {
                            lowPriceProduct = product;
                        }

                        if (product.isPurchased()) {
                            if (rentalType == RentalType.BUY) {
                                buyProduct = product;
                                break;
                            } else if (rentalType == RentalType.SUBSCRIBE) {
                                subscriptionProduct = product;
                            } else if (rentalType == RentalType.RENT) {
                                if (rentProduct != null) {
                                    if (product.getPeriod() > rentProduct.getPeriod()) {
                                        rentProduct = product;
                                    }
                                } else {
                                    rentProduct = product;
                                }
                            }
                        }
                    }

                    if (buyProduct != null) {
                        list.add(buyProduct);
                    } else if (subscriptionProduct != null) {
                        list.add(subscriptionProduct);
                    } else if (rentProduct != null) {
                        list.add(rentProduct);
                    } else if (lowPriceProduct != null) {
                        list.add(lowPriceProduct);
                    }
                }
                break;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "getMainList, list : " + list.size());
        }

        return list;
    }

    public ArrayList<Product> getPackageList(String productId) {
        return getProductIdList(packageMap, productId);
    }

    public ArrayList<Product> getSubscribeMainProductList() {
        ArrayList<Product> list = new ArrayList<>();

        Iterator<String> iterator = subscriptionMap.keySet().iterator();
        while (iterator.hasNext()) {
            ArrayList<Product> subscriptionList = subscriptionMap.get(iterator.next());
            list.add(subscriptionList.get(0));
        }


        Collections.sort(list, new SubscriptionMainPriceComparator());
        return list;
    }

    public ArrayList<Product> getSubscribeProductList(Product product) {
        RentalType mRentalType = product.getRentalType();

        if (Log.INCLUDE) {
            Log.d(TAG, "getSubscribeProductList, mRentalType : " + mRentalType);
        }

        if (mRentalType != RentalType.SUBSCRIBE) {
            return null;
        }

        return getSubscriptionList(product.getProductId());
    }

    public static boolean isWatchable(List<Product> list) {
        for (Product product : list) {
            if (product.isPurchased() || product.getPrice() == 0) {
                return true;
            }
        }

        return false;
    }

    private ArrayList<Product> getProductIdList(HashMap map, String productId) {
        ArrayList<Product> list = (ArrayList<Product>) map.get(productId);

        if (Log.INCLUDE) {
            Log.d(TAG, "getProductIdList, productId : " + productId + ", list : " + list.size());
        }

        return list;
    }

    private void clearAllList() {
        singleList.clear();
        seriesMap.clear();
        packageMap.clear();
        subscriptionMap.clear();
        purchasedList.clear();
    }

    public ArrayObjectAdapter getProductButtons(Object content, ArrayList<Product> list) {
        clearAllList();

        boolean isPurchasedSubscribe = false;
        ArrayList<Product> subscribeProductList = new ArrayList<>();

        if (Log.INCLUDE) {
            Log.d(TAG, "getProductButtons, list : " + list.size());
        }

        for (Product product : list) {
            if (Log.INCLUDE) {
                Log.d(TAG, "getProductButtons, product : " + product);
            }

            ProductType productType = product.getProductType();
            RentalType rentalType = product.getRentalType();

            if (productType == ProductType.ERROR || rentalType == RentalType.ERROR) {
                continue;
            }

            //for check watch button
            if (product.isPurchased() || product.getPrice() == 0) {
                purchasedList.add(product);
                //Package product button should show for entering Package detail, exception subscription
                if (productType == ProductType.PACKAGE) {
                    categorizeByProductId(product);
                }

                if (rentalType == RentalType.SUBSCRIBE) {
                    isPurchasedSubscribe = true;
                }
                continue;
            }

            if (rentalType == RentalType.SUBSCRIBE) {
                if (productType == ProductType.PACKAGE) {
                    categorizeByProductId(product);
                } else {
                    subscribeProductList.add(product);
                }
                continue;
            }

            //Others
            categorizeByProductId(product);
        }

        ArrayObjectAdapter actionAdapter = new ArrayObjectAdapter();

        //Display watch button
        if (purchasedList.size() > 0) {
            actionAdapter.add(0, getWatchButton(purchasedList));
        }

        //Display all single product buttons, exception subscription
        if (singleList.size() > 0) {
            actionAdapter.addAll(actionAdapter.size(), getSingleButtons());
        }

        //Display only one Series product button, exception subscription
        //If it is more than one, button will show it has how many product
        if (seriesMap.size() > 0) {
            ProductButtonItem productButtonItem = getSeriesButton();
            if (productButtonItem != null) {
                actionAdapter.add(productButtonItem);
            }
        }

        //Display only one Package product button
        //If it is more than one, button will show it has how many product
        if (packageMap.size() > 0) {
            ProductButtonItem productButtonItem = getPackageButton();
            if (productButtonItem != null) {
                actionAdapter.add(productButtonItem);
            }
        }

        //Display only one Subscription product button
        //If it is more than one, button will show it has how many product
        if (!isPurchasedSubscribe) {
            if (subscribeProductList.size() > 0) {
                arrangeSubscriptionList(subscribeProductList);
                ProductButtonItem subscriptionButton = getSubscriptionButton();
                if (subscriptionButton != null) {
                    actionAdapter.add(subscriptionButton);
                }
            }
        }

        //Display content like for My List
        actionAdapter.add(content);

        if (Log.INCLUDE) {
            Log.d(TAG, "getProductButtons"
                    + ", purchasedList : " + purchasedList.size() + ", subscribeProductList : " + subscribeProductList.size()
                    + ", singleMap : " + singleList.size() + ", seriesMap : " + seriesMap.size() + ", packageMap : " + packageMap.size());
        }

        return actionAdapter;
    }

    public ArrayObjectAdapter getPackageProductButtons(PackageContent packageDetailData, ArrayList<Product> list) {
        clearAllList();

        boolean isPurchasedSubscribe = false;
        ArrayList<Product> subscribeProductList = new ArrayList<>();

        for (Product product : list) {
            if (Log.INCLUDE) {
                Log.d(TAG, "getPackageProductButtons, product : " + product);
            }

            ProductType productType = product.getProductType();
            RentalType rentalType = product.getRentalType();

            if (productType == ProductType.ERROR || rentalType == RentalType.ERROR) {
                continue;
            }

            //for check watch button
            if (product.isPurchased() || product.getPrice() == 0) {
                purchasedList.add(product);

                if (rentalType == RentalType.SUBSCRIBE) {
                    isPurchasedSubscribe = true;
                }
                continue;
            }

            if (rentalType == RentalType.SUBSCRIBE) {
                subscribeProductList.add(product);
                continue;
            }

            //Others
            singleList.add(product);
        }

        ArrayObjectAdapter actionAdapter = new ArrayObjectAdapter();

        //Display watch button
        if (purchasedList.size() > 0) {
            actionAdapter.add(0, getWatchButton(purchasedList));
        }

        if (singleList.size() > 0) {
            actionAdapter.addAll(actionAdapter.size(), getPackageSingleButtons());
        }

        //Display only one Subscription product button
        //If it is more than one, button will show it has how many product
        if (!isPurchasedSubscribe) {
            if (subscribeProductList.size() > 0) {
                arrangeSubscriptionList(subscribeProductList);
                ProductButtonItem subscriptionButton = getSubscriptionButton();
                if (subscriptionButton != null) {
                    actionAdapter.add(subscriptionButton);
                }
            }
        }

        actionAdapter.add(packageDetailData);

        if (Log.INCLUDE) {
            Log.d(TAG, "getProductButtons"
                    + ", isPurchasedSubscribe : " + isPurchasedSubscribe
                    + ", purchasedList : " + purchasedList.size() + ", subscribeProductList : " + subscribeProductList.size()
                    + ", singleMap : " + singleList.size() + ", seriesMap : " + seriesMap.size() + ", packageMap : " + packageMap.size());
        }

        return actionAdapter;
    }

    private void categorizeByProductId(Product product) {
        ProductType productType = product.getProductType();

        if (productType == ProductType.SINGLE) {
            //Single doesn't have productId
            singleList.add(product);
        } else if (productType == ProductType.PACKAGE) {
            //Package can have productId more than one
            categorizeList(packageMap, product);
        } else if (productType == ProductType.SERIES) {
            //Series will be set only one productId by Policy
            categorizeList(seriesMap, product);
        }
    }

    private void categorizeList(HashMap map, Product product) {
        ProductType productType = product.getProductType();

        String key = product.getProductId();

        if (Log.INCLUDE) {
            Log.d(TAG, "categorizeList, productType : " + productType + ", key : " + key);
        }

        if (key == null || key.equalsIgnoreCase("")) {
            if (Log.INCLUDE) {
                Log.d(TAG, "categorizeList, key == null, so return... product : " + product);
            }

            return;
        }

        ArrayList<Product> offerList = (ArrayList<Product>) map.get(key);
        if (offerList != null) {
            offerList.add(product);
        } else {
            offerList = new ArrayList<>();
            offerList.add(product);
            map.put(key, offerList);
        }
    }

    private void arrangeSubscriptionList(ArrayList<Product> subscribeProductList) {
        for (Product product : subscribeProductList) {
            String productId = product.getProductId();

            ArrayList<Product> list = getSubscriptionList(productId);
            list.add(product);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "arrangeSubscriptionList"
                    + ", subscriptionMap : " + subscriptionMap.size());
        }
    }

    private ProductButtonItem getSubscriptionButton() {
        int groupSubscriptionTotal = subscriptionMap.keySet().size();

        if (Log.INCLUDE) {
            Log.d(TAG, "groupSubscriptionTotal : " + groupSubscriptionTotal);
        }

        if (groupSubscriptionTotal == 0) {
            return null;
        }

        if (groupSubscriptionTotal == 1) {
            String currency = context.getString(R.string.thb_month);
            String key = subscriptionMap.keySet().iterator().next();
            ArrayList<Product> list = getSubscriptionList(key);
            Product product = list.get(0);
            return new ProductButtonItem(product, VodProductType.SUBSCRIBE, product.getPrice(), currency);
        }

        String value = context.getString(R.string.subscriptions);
        return new ProductButtonItem(VodProductType.SUBSCRIBE, subscriptionMap.size(), value);
    }

    private ArrayList<Product> getSubscriptionList(String productId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "getSubscriptionList, productId : " + productId);
        }

        ArrayList<Product> list = subscriptionMap.get(productId);
        if (list == null) {
            list = new ArrayList<>();
            subscriptionMap.put(productId, list);
        }

        Collections.sort(list, new SubscriptionDurationComparator());
        return list;
    }

    private void sortProductButtons(boolean isSingleButtonType, ArrayList<VodProductType> positionPriorityList) {
        Product[] products = new Product[positionPriorityList.size()];
        for (int i = 0; i < singleList.size(); i++) {
            Product product = singleList.get(i);
            VodProductType vodProductType = VodProductType.getVodProductType(isSingleButtonType, product);
            int index = positionPriorityList.indexOf(vodProductType);
            products[index] = product;
        }

        ArrayList<Product> productList = new ArrayList<>(Arrays.asList(products));
        productList.removeAll(Arrays.asList("", null));
        singleList = (ArrayList<Product>) productList.clone();
    }

    private ArrayList<ProductButtonItem> getSingleButtons() {
        if (Log.INCLUDE) {
            Log.d(TAG, "getSingleButtons, singleMap size : " + singleList.size());
        }

        sortProductButtons(true, POSITION_PRIORITY);

        ArrayList<ProductButtonItem> buttonItems = new ArrayList<>();
        for (Product product : singleList) {
            VodProductType type = VodProductType.getVodProductType(true, product);
            buttonItems.add(new ProductButtonItem(product, type, product.getPrice(), product.getCurrency()));
        }

        return buttonItems;
    }

    private ArrayList<ProductButtonItem> getPackageSingleButtons() {
        if (Log.INCLUDE) {
            Log.d(TAG, "getPackageSingleButtons, singleMap size : " + singleList.size());
        }

        sortProductButtons(false, PACKAGE_POSITION_PRIORITY);

        ArrayList<ProductButtonItem> buttonItems = new ArrayList<>();
        for (Product product : singleList) {
            VodProductType vodProductType = VodProductType.getVodProductType(false, product);
            buttonItems.add(new ProductButtonItem(product, vodProductType, product.getPrice(), product.getCurrency()));
        }

        return buttonItems;
    }

    private ProductButtonItem getSeriesButton() {
        if (Log.INCLUDE) {
            Log.d(TAG, "getSeriesButton, seriesMap size : " + seriesMap.size());
        }

        Iterator<String> keys = seriesMap.keySet().iterator();
        while (keys.hasNext()) {
            ArrayList<Product> products = seriesMap.get(keys.next());
            if (products.size() == 1) {
                Product product = products.get(0);
                RentalType rentalType = product.getRentalType();
                VodProductType vodProductType = (rentalType == RentalType.RENT) ? VodProductType.RENT_ALL_EPISODE : VodProductType.BUY_ALL_EPISODE;
                return new ProductButtonItem(product, vodProductType, product.getPrice(), product.getCurrency());
            } else {
                String value = context.getString(R.string.vod_rent_buy);
                return new ProductButtonItem(VodProductType.ALL_EPISODE, -1, value);
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "getSeriesButton, return null...");
        }

        return null;
    }

    private ProductButtonItem getPackageButton() {
        if (Log.INCLUDE) {
            Log.d(TAG, "getPackageButton, packageMap size : " + packageMap.size());
        }

        Iterator<String> keys = packageMap.keySet().iterator();
        ArrayList<Product> productList = packageMap.get(keys.next());

        //If there is only one package, show the price
        //If bought the package, show remaining period on package button
        Product buyProduct = null;
        Product rentProduct = null;
        Product freeProduct = null;
        Product lowProduct = null;
        ProductButtonItem packageButton = null;

        if (packageMap.size() == 1) {
            if (productList.size() == 1) {
                Product product = productList.get(0);
                if (Log.INCLUDE) {
                    Log.d(TAG, "getPackageButton, product : " + product);
                }

                if (product.getPrice() == 0) {
                    freeProduct = product;
                } else if (product.isPurchased()) {
                    RentalType rentalType = product.getRentalType();
                    if (rentalType == RentalType.RENT) {
                        rentProduct = product;
                    } else if (rentalType == RentalType.BUY) {
                        buyProduct = product;
                    }
                } else {
                    lowProduct = product;
                }
            } else {
                for (Product product : productList) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "getPackageButton, product : " + product);
                    }

                    RentalType rentalType = product.getRentalType();
                    if (rentalType == RentalType.BUY && product.isPurchased()) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPackageButton, package buy...");
                        }

                        buyProduct = product;
                        break;
                    } else {
                        if (rentalType == RentalType.RENT && product.isPurchased()) {
                            rentProduct = product;
                        }

                        if (lowProduct == null || lowProduct.getPrice() > product.getPrice()) {
                            lowProduct = product;
                        }
                    }
                }
            }

            if (buyProduct != null) {
                String unit = context.getString(R.string.vod_watch_days_unlimited);
                packageButton = new ProductButtonItem(buyProduct, VodProductType.PACKAGE, -1, unit);
            } else if (rentProduct != null) {
                WatchButtonInfo watchButtonInfo = getRentWatchInfo(rentProduct.getPeriod());
                String unit = context.getString(watchButtonInfo.getUnit()) + " " + context.getString(R.string.vod_watch_left);
                packageButton = new ProductButtonItem(rentProduct, VodProductType.PACKAGE, watchButtonInfo.getValue(), unit);
            } else if (freeProduct != null) {
                String free = context.getString(R.string.vod_free);
                packageButton = new ProductButtonItem(freeProduct, VodProductType.PACKAGE, -1, free);
            } else if (lowProduct != null) {
                packageButton = new ProductButtonItem(lowProduct, VodProductType.PACKAGE, lowProduct.getPrice(), lowProduct.getCurrency());
            } else {
                if (Log.INCLUDE) {
                    Log.d(TAG, "getPackageButton, wrong check...packageButton == null");
                }
            }
        } else {
            //If there are more than 2 packages, show the number of package
            //If you purchased one of the two packages, show the number of package
            String value = context.getString(R.string.packages);
            packageButton = new ProductButtonItem(VodProductType.PACKAGE, packageMap.size(), value);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "getPackageButton, buttonItem : " + packageButton);
        }

        return packageButton;
    }

    private ProductButtonItem getWatchButton(ArrayList<Product> purchasedProductList) {
        if (Log.INCLUDE) {
            Log.d(TAG, "getWatchButton, productList : " + purchasedProductList.size());
        }

        Product[] arrangedList = new Product[VIEW_LIMITED_PERIOD_PRIORITY.size()];
        for (int i = 0; i < purchasedProductList.size(); i++) {
            Product product = purchasedProductList.get(i);
            VodProductType vodProductType = VodProductType.getVodProductType(false, product);

            if (Log.INCLUDE) {
                Log.d(TAG, "getWatchButton, vodProductType : " + vodProductType + ", product : " + product);
            }

            if (vodProductType == VodProductType.BUY || vodProductType == VodProductType.BUY_ALL_EPISODE || vodProductType == VodProductType.BUY_PACKAGE) {
                int index = VIEW_LIMITED_PERIOD_PRIORITY.indexOf(VodProductType.BUY);
                arrangedList[index] = product;
                continue;
            } else if (vodProductType == VodProductType.RENT || vodProductType == VodProductType.RENT_ALL_EPISODE || vodProductType == VodProductType.RENT_PACKAGE) {
                int index = VIEW_LIMITED_PERIOD_PRIORITY.indexOf(VodProductType.RENT);
                Product rentProduct = arrangedList[index];
                if (rentProduct != null) {
                    if (product.getPeriod() > rentProduct.getPeriod()) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getWatchButton, replace long Watchable Rent Product");
                        }
                        arrangedList[index] = product;
                    }
                } else {
                    arrangedList[index] = product;
                }

                continue;
            }

            int index = VIEW_LIMITED_PERIOD_PRIORITY.indexOf(vodProductType);
            arrangedList[index] = product;
        }

        ProductButtonItem watchButtonItem = null;
        for (int j = 0; j < arrangedList.length; j++) {
            if (arrangedList[j] != null) {
                Product product = arrangedList[j];
                watchButtonItem = getWatchButtonItem(product);

                if (watchButtonItem != null) {
                    clearWatchConflictButtons(product);
                }
                break;
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "getWatch, watchButtonItem : " + watchButtonItem);
        }

        return watchButtonItem;
    }

    private ProductButtonItem getWatchButtonItem(Product product) {
        RentalType rentalType = product.getRentalType();

        if (Log.INCLUDE) {
            Log.d(TAG, "getWatchButtonItem, product : " + product + ", productType : " + rentalType);
        }

        int valueId = -1;

        if (product.getPrice() == 0) {
            String unit = context.getString(R.string.vod_free);
            return new ProductButtonItem(VodProductType.WATCH, -1, unit);
        }

        switch (rentalType) {
            case BUY:
                valueId = R.string.vod_watch_days_unlimited;
                break;
            case SUBSCRIBE:
                valueId = R.string.vod_subscribing;
                break;
            default:
                return getWatchProductButtonItem(product);
        }

        String unit = context.getString(valueId);
        return new ProductButtonItem(VodProductType.WATCH, -1, unit);
    }

    public ProductButtonItem getWatchProductButtonItem(Product product) {
        WatchButtonInfo watchButtonInfo = getRentWatchInfo(product.getPeriod());
        int value = watchButtonInfo.getValue();
        int id = watchButtonInfo.getUnit();

        String unit = context.getString(id) + " " + context.getString(R.string.vod_watch_left);
        return new ProductButtonItem(product, VodProductType.WATCH, value, unit);
    }

    public static WatchButtonInfo getRentWatchInfo(int period) {
        int day = period / 24;
        if (Log.INCLUDE) {
            Log.d(TAG, "getRentWatchInfo, period : " + period + ", day : " + day);
        }

        if (period <= 0) {
            day = -1;
        } else if (period < 49) {
            int id = period <= 1 ? R.string.hour : R.string.hours;
            return new WatchButtonInfo(period, id);
        }

        return new WatchButtonInfo(day, R.string.days);
    }

    private void clearWatchConflictButtons(Product product) {
        if (Log.INCLUDE) {
            Log.d(TAG, "clearWatchConflictButtons, product : " + product);
        }

        ProductType productType = product.getProductType();
        RentalType rentalType = product.getRentalType();

        if (productType == ProductType.SINGLE && rentalType == RentalType.BUY) {
            clearSingleRentalType(RentalType.RENT);
        } else if (productType == ProductType.SERIES) {
            clearSingleConflictButton(rentalType);
            if (rentalType != RentalType.RENT) {
                clearSeriesRent(product.getProductId());
            }
        } else if (productType == ProductType.PACKAGE) {
            clearSingleConflictButton(rentalType);
            if (rentalType != RentalType.RENT) {
                clearPackageRent(product.getProductId());
            }
        }

        if (rentalType == RentalType.SUBSCRIBE) {
            clearSingleRentalType(RentalType.RENT);
        }
    }

    private void clearSingleConflictButton(RentalType rentalType) {
        if (rentalType == RentalType.BUY) {
            clearSingleRentalType(RentalType.RENT);
            clearSingleRentalType(RentalType.BUY);
        } else if (rentalType == RentalType.RENT) {
            clearSingleRentalType(RentalType.RENT);
        }
    }

    private void clearSingleRentalType(RentalType rentalType) {
        ArrayList<Product> list = (ArrayList<Product>) singleList.clone();
        for (Product singleProduct : list) {
            if (singleProduct.getRentalType() == rentalType) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "clearSingleRentalType, invisible rental product : " + singleProduct);
                }

                singleList.remove(singleProduct);
            }
        }
    }

    private void clearSeriesRent(String productId) {
        ArrayList<Product> seriesList = seriesMap.get(productId);

        if (seriesList == null) {
            return;
        }

        ArrayList<Product> list = (ArrayList<Product>) seriesList.clone();
        for (Product seriesProduct : list) {
            if (seriesProduct.getRentalType() == RentalType.RENT) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "clearSeriesRent, invisible rental product : " + seriesProduct);
                }

                seriesList.remove(seriesProduct);

                if (seriesList.size() == 0) {
                    seriesMap.remove(productId);
                }
            }
        }
    }

    private void clearPackageRent(String productId) {
        ArrayList<Product> packageList = packageMap.get(productId);

        if (packageList == null) {
            return;
        }

        ArrayList<Product> list = (ArrayList<Product>) packageList.clone();
        for (Product packageProduct : list) {
            if (packageProduct.getRentalType() == RentalType.RENT) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "clearPackageRent, invisible rental product : " + packageProduct);
                }

                packageList.remove(packageProduct);

                if (packageList.size() == 0) {
                    packageMap.remove(productId);
                }
            }
        }
    }

    public static class WatchButtonInfo {
        int value;
        int unit;

        public WatchButtonInfo(int value, int unit) {
            this.value = value;
            this.unit = unit;
        }

        public int getValue() {
            return value;
        }

        public int getUnit() {
            return unit;
        }
    }

    public class SubscriptionMainPriceComparator implements Comparator<Product> {
        @Override
        public final int compare(Product a, Product b) {
            float aPrice = a.getPrice();
            float bPrice = b.getPrice();

            return (int) (aPrice - bPrice);
        }
    }

    public class SubscriptionDurationComparator implements Comparator<Product> {
        @Override
        public final int compare(Product a, Product b) {
            Duration aDur = a.getDuration();
            Duration bDur = b.getDuration();

            if (aDur == null || bDur == null) {
                return 0;
            }


            return aDur.getValue() - bDur.getValue();
        }
    }
}
