/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.collection.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;
import java.util.Iterator;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.user.object.CollectedContent;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;


public class VodCollectionPresenter extends RowPresenter {

    private VodCollectionPresenter.LoadWorkPosterListener mLoadWorkPosterListener;
    private HashMap<String, Integer> flagMap = new HashMap<>();

    public VodCollectionPresenter() {
        super();

        flagMap.put(Content.FLAG_EVENT, R.id.flagEvent);
        flagMap.put(Content.FLAG_HOT, R.id.flagHot);
        flagMap.put(Content.FLAG_NEW, R.id.flagNew);
        flagMap.put(Content.FLAG_PREMIUM, R.id.flagPremium);
        flagMap.put(Content.FLAG_UHD, R.id.flagUhd);
    }

    public VodCollectionPresenter(int headerLayoutId) {
        super(headerLayoutId);
    }

    public void setLoadWorkPosterListener(VodCollectionPresenter.LoadWorkPosterListener loadMoviePosterListener) {
        mLoadWorkPosterListener = loadMoviePosterListener;
    }

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        VodItemViewHolder mVodItemViewHolder = new VodItemViewHolder(inflater.inflate(R.layout.item_mymenu_vod_content, null, false));
        return mVodItemViewHolder;
    }

    @Override
    protected void onBindRowViewHolder(ViewHolder viewHolder, Object item) {
        super.onBindRowViewHolder(viewHolder, item);
        VodItemViewHolder itemViewHolder = (VodItemViewHolder) viewHolder;
        CollectedContent collectedItem = (CollectedContent) item;

        itemViewHolder.setText(collectedItem);

        initFlag(collectedItem, viewHolder.view);

//        boolean isLocked = isLockedByRating(collectedItem.getRating());
//        if (isLocked) {
//            itemViewHolder.getMainImageView().setImageResource(R.drawable.poster_block);
//        } else {

        Glide.with(viewHolder.view.getContext())
                .load(collectedItem.getPosterSourceUrl(Content.mPosterDefaultRect.width(),
                        Content.mPosterDefaultRect.height())).placeholder(R.drawable.poster_default)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .centerCrop()
                .into(itemViewHolder.getMainImageView());

        if (mLoadWorkPosterListener != null) {
            mLoadWorkPosterListener.onLoadWorkPoster(collectedItem, itemViewHolder.getMainImageView());
        }
//        }
    }

    private void initFlag(CollectedContent content, View view) {
//        if (content.getEventFlag() == null) {
//            removeFlag(view);
//            return;
//        }
        Iterator<String> keyIterator = flagMap.keySet().iterator();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
//            boolean hasFlag = content.getEventFlag() != null && content.getEventFlag().contains(key);
            View flagView = view.findViewById(flagMap.get(key));
//            flagView.setVisibility(hasFlag ? View.VISIBLE : View.GONE);
            flagView.setVisibility(View.GONE);
        }

        View lockIcon = view.findViewById(R.id.iconVodLock);
        if (lockIcon != null) {
            boolean isLocked = isLockedByRating(content.getRating());
            lockIcon.setVisibility(isLocked ? View.VISIBLE : View.GONE);
        }
    }

    private void removeFlag(View view) {
        Iterator<String> keyIterator = flagMap.keySet().iterator();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            View flagView = view.findViewById(flagMap.get(key));
            flagView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onUnbindRowViewHolder(ViewHolder viewHolder) {
        super.onUnbindRowViewHolder(viewHolder);

        VodItemViewHolder cardView = (VodItemViewHolder) viewHolder;
        removeFlag(cardView.view);
        cardView.removeBadgeImage();
        cardView.removeMainImage();
    }

    private boolean isLockedByRating(int rating) {
        ProfileInfo profileInfo = AccountManager.getInstance().getSelectedProfile();
        if (profileInfo == null) {
            return false;
        }
        int userRating = profileInfo.getRating();
        return userRating != 0 && rating >= userRating;
    }

    public interface LoadWorkPosterListener {
        void onLoadWorkPoster(CollectedContent work, ImageView imageView);
    }

    private static class VodItemViewHolder extends ViewHolder {
        private final String TAG = VodItemViewHolder.class.getSimpleName();
        private ImageView mainImageView = null;
        private TextView titleView = null;

        public VodItemViewHolder(View view) {
            super(view);
            mainImageView = view.findViewById(R.id.mainImage);
            titleView = view.findViewById(R.id.titleLayer);

        }

        public ImageView getMainImageView() {
            return mainImageView;
        }

        public void setText(CollectedContent title) {
            titleView.setText(title.getTitle());
        }

        public void removeBadgeImage() {}

        public void removeMainImage() {
            mainImageView.setImageDrawable(null);
        }
    }
}