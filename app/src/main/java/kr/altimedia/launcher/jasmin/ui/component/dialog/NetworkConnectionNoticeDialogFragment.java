/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.content.Context;
import android.view.View;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;

/**
 * Created by mc.kim on 17,07,2020
 */
public class NetworkConnectionNoticeDialogFragment extends TwoButtonDialogFragment {
    public static final String CLASS_NAME = NetworkConnectionNoticeDialogFragment.class.getName();

    public NetworkConnectionNoticeDialogFragment(Context context, View.OnClickListener onClickListener) {
        super(context, onClickListener);
    }

    public static TwoButtonDialogFragment newInstance(Context context, View.OnClickListener onClickListener) {
        String title = context.getString(R.string.error_ip_not_allocate);
        String subtitle = context.getString(R.string.error_network_title);
        String description = ErrorMessageManager.getInstance().getErrorMessage(title);
        String buttonTitle = context.getString(R.string.button_network);
        return newInstance(context, title, subtitle, description, buttonTitle, onClickListener);
    }
}
