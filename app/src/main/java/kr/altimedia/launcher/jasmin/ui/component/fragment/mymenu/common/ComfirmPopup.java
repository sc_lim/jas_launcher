/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.leanback.widget.ArrayObjectAdapter;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.adapter.ButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.ButtonItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.presenter.ButtonItemPresenter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class ComfirmPopup extends DialogFragment {
    public static final String CLASS_NAME = ComfirmPopup.class.getName();
    private final String TAG = ComfirmPopup.class.getSimpleName();

    private String title;
    private String description1;
    private String description2;

    private String btnOk;
    private String btnCancel;
    private int btnWidth;
    private int btnHeight;

    private HorizontalGridView buttonGridView;
    private ArrayObjectAdapter buttonObjectAdapter = null;

    private ButtonItemPresenter.OnKeyListener onKeyListener;

    private ComfirmPopup(String title, ButtonItemPresenter.OnKeyListener onKeyListener) {
        this.title = title;
        this.onKeyListener = onKeyListener;
    }

    public static ComfirmPopup newInstance(String title, ButtonItemPresenter.OnKeyListener onKeyListener) {
        ComfirmPopup fragment = new ComfirmPopup(title, onKeyListener);
        return fragment;
    }

    public void setContent(String description1, String description2) {
        this.description1 = description1;
        this.description2 = description2;
    }

    public void setButtonTitle(String ok, String cancel) {
        this.btnOk = ok;
        this.btnCancel = cancel;
    }

    public void setButtonSize(int width, int height) {
        this.btnWidth = width;
        this.btnHeight = height;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_mymenu_confirm, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        Log.d(TAG, "initView");
        TextView popupTitle = view.findViewById(R.id.popupTitle);
        TextView popupDesc1 = view.findViewById(R.id.popupDesc1);
        TextView popupDesc2 = view.findViewById(R.id.popupDesc2);

        buttonGridView = view.findViewById(R.id.buttonGridView);

        popupTitle.setText(title);
        popupDesc1.setText(description1);
        popupDesc2.setText(description2);

        ArrayList<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(new ButtonItem(ButtonItem.TYPE_OK, btnOk));
        buttonItems.add(new ButtonItem(ButtonItem.TYPE_CANCEL, btnCancel));
        ButtonItemPresenter presenter = new ButtonItemPresenter(false, onKeyListener);
        if (btnWidth > 0 && btnHeight > 0) {
            presenter.setSize(btnWidth, btnHeight);
        } else {
            presenter.setSize(R.dimen.mymenu_button_width2, R.dimen.mymenu_button_height2);
        }
        buttonObjectAdapter = new ArrayObjectAdapter(presenter);
        buttonObjectAdapter.addAll(0, buttonItems);
        ButtonBridgeAdapter bridgeAdapter = new ButtonBridgeAdapter();
        bridgeAdapter.setAdapter(buttonObjectAdapter);
        buttonGridView.setAdapter(bridgeAdapter);

        int horizontalSpacing = (int) view.getResources().getDimension(R.dimen.mymenu_button_horizontal_space);
        buttonGridView.setHorizontalSpacing(horizontalSpacing);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onKeyListener = null;
    }
}
