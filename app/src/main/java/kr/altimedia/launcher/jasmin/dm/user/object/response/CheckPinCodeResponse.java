/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class CheckPinCodeResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private CheckPin checkPin;

    protected CheckPinCodeResponse(Parcel in) {
        super(in);
        checkPin = (CheckPin) in.readValue(CheckPin.class.getClassLoader());
    }

    public static final Creator<CheckPinCodeResponse> CREATOR = new Creator<CheckPinCodeResponse>() {
        @Override
        public CheckPinCodeResponse createFromParcel(Parcel in) {
            return new CheckPinCodeResponse(in);
        }

        @Override
        public CheckPinCodeResponse[] newArray(int size) {
            return new CheckPinCodeResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(checkPin);
    }

    public boolean isCorrectPin() {
        return checkPin != null && checkPin.isCorrectPin();
    }

    private static class CheckPin implements Parcelable {

        public static final Creator<CheckPin> CREATOR = new Creator<CheckPin>() {
            @Override
            public CheckPin createFromParcel(Parcel in) {
                return new CheckPin(in);
            }

            @Override
            public CheckPin[] newArray(int size) {
                return new CheckPin[size];
            }
        };

        protected CheckPin(Parcel in) {
            isCorrectPin = in.readByte() != 0;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isCorrectPin ? 1 : 0));
        }

        @Expose
        @SerializedName("yn")
        @JsonAdapter(YNBooleanDeserializer.class)
        private boolean isCorrectPin;

        public boolean isCorrectPin() {
            return isCorrectPin;
        }
    }
}
