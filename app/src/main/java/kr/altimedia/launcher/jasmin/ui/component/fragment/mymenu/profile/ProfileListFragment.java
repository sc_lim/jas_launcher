/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildLaidOutListener;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileList;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.OnDataLoadedListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfile;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfileBuilder;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.adapater.ProfileBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.presenter.ProfilePresenter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class ProfileListFragment extends Fragment implements OnDataLoadedListener {
    public static final String CLASS_NAME = ProfileListFragment.class.getName();
    private final String TAG = ProfileListFragment.class.getSimpleName();

    private ArrayList<Integer> taskList = new ArrayList<>();
    private ProgressBarManager progressBarManager;

    private ArrayList<UserProfile> profileList = new ArrayList<>();
    private HorizontalGridView profileGridView;
    private ArrayObjectAdapter profileObjectAdapter = null;

    private LinearLayout profileLayer;
    private TextView btnDone;

    private int maxProfileSize = ProfileManageDialog.MAX_PROFILE_SIZE;
    private boolean profileUpdated = false;
    private UserProfile updatedProfile;

    public static ProfileListFragment newInstance(ArrayList<UserProfile> profileList, UserProfile profile, boolean profileUpdated) {
        ProfileListFragment fragment = new ProfileListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ProfileManageDialog.KEY_PROFILE_LIST, profileList);
        args.putParcelable(ProfileManageDialog.KEY_PROFILE, profile);
        args.putBoolean(ProfileManageDialog.KEY_PROFILE_UPDATED, profileUpdated);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreateView");
        }
        return inflater.inflate(R.layout.fragment_mymenu_profile_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated");
        }
        super.onViewCreated(view, savedInstanceState);

        Bundle arguments = getArguments();

        initLoadingBar(view);
        initProfile(view);
        initButton(view);

        loadProfile(arguments);
    }

    private void initLoadingBar(View view) {
        progressBarManager = new ProgressBarManager();
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, (ViewGroup) view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);
        showProgress();
    }

    private void initProfile(View view) {
        profileLayer = view.findViewById(R.id.profileLayer);
        profileGridView = view.findViewById(R.id.profileGridView);
        profileGridView.setNumRows(1);
        profileGridView.setClipToPadding(false);

        int horizontalSpacing = (int) view.getResources().getDimension(R.dimen.mymenu_profile_horizontal_space);
        profileGridView.setHorizontalSpacing(horizontalSpacing);
    }

    private void initButton(View view) {

        btnDone = view.findViewById(R.id.btnDone);
        btnDone.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getAction() != KeyEvent.ACTION_UP) {
                    return false;
                }

                int keyCode = keyEvent.getKeyCode();
                if (Log.INCLUDE) {
                    Log.d(TAG, "DoneButton: onKey: keyCode=" + keyCode);
                }
                switch (keyCode) {
                    case KeyEvent.KEYCODE_ENTER:
                    case KeyEvent.KEYCODE_BACK:
                    case KeyEvent.KEYCODE_DPAD_CENTER: {
                        hideView();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void checkLoadList(Integer task, boolean added) {
        if (added) {
            if (taskList.contains(task)) {
                return;
            }
            taskList.add(task);
        } else {
            taskList.remove(task);
        }
        if (taskList.isEmpty()) {
            hideProgress();
        }
    }

    private void loadProfile(Bundle bundle) {
        profileUpdated = bundle.getBoolean(ProfileManageDialog.KEY_PROFILE_UPDATED, false);
        updatedProfile = bundle.getParcelable(ProfileManageDialog.KEY_PROFILE);
        ArrayList pList = bundle.getParcelableArrayList(ProfileManageDialog.KEY_PROFILE_LIST);
        if (Log.INCLUDE) {
            Log.d(TAG, "loadProfile: profileList=" + pList + ", profile="+updatedProfile);
        }
        if (pList != null) {
            if (!profileList.isEmpty()) {
                profileList.clear();
            }
            profileList.addAll(pList);
            setProfileView();

        } else {
            UserDataManager userDataManager = new UserDataManager();
            AsyncTask task = userDataManager.getProfileList(
                    AccountManager.getInstance().getSaId(),
                    new MbsDataProvider<String, ProfileList>() {
                        @Override
                        public void needLoading(boolean loading) {
                            checkLoadList(0, loading);
                        }

                        @Override
                        public void onFailed(int key) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "loadProfile: onFailed");
                            }
                            addAddOption();
                            showView(0);
                        }

                        @Override
                        public void onSuccess(String key, ProfileList result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "loadProfile: onSuccess: result=" + result);
                            }
                            profileUpdated = true;

                            if (!profileList.isEmpty()) {
                                profileList.clear();
                            }

                            List<ProfileInfo> tmpList = result.getProfileInfoList();
                            for (ProfileInfo profileInfo : tmpList) {
                                profileList.add(UserProfileBuilder.createUserProfile(profileInfo));
                            }
                            maxProfileSize = result.getMaxProfileCount();

                            boolean optionIncluded = false;
                            for (UserProfile profile : profileList) {
                                if (profile.isAddOption()) {
                                    optionIncluded = true;
                                    break;
                                }
                            }
                            if (!optionIncluded) {
                                addAddOption();
                            }

                            showView(0);
                        }

                        @Override
                        public void onError(String errorCode, String message) {
                            ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                            errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                        }
                    });
        }
    }

    private void addAddOption() {
        try {
            if (profileList.size() < maxProfileSize) {
                if (!UserProfileBuilder.checkProfileOptionIncluded(profileList, UserProfileBuilder.TYPE_ADD_OPTION)) {
                    profileList.add(UserProfileBuilder.createAddOption(getResources()));
                }
            }
        }catch(Exception e){
        }
    }

    @Override
    public void hideProgress() {
        if (progressBarManager != null) {
            progressBarManager.hide();
        }
    }

    @Override
    public void showProgress() {
        if (taskList.isEmpty()) {
            progressBarManager.show();
        }
    }

    @Override
    public void showView(int type) {
        setProfileView();
    }

    private void setProfileView() {
        try {
            hideProgress();

            if (profileList != null && profileList.size() > 0) {
                int size = profileList.size();
                if(size <= 12) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) profileLayer.getLayoutParams();
                    layoutParams.width = (int) getResources().getDimension(R.dimen.mymenu_profile_width) * size + (int) getResources().getDimension(R.dimen.mymenu_profile_gap) * (size - 1);
                    profileLayer.setLayoutParams(layoutParams);
                }
                ProfilePresenter presenter = new ProfilePresenter(false, new ProfilePresenter.OnKeyListener() {
                    @Override
                    public boolean onKey(int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "ProfilePresenter: onKey: keyCode=" + keyCode + ", index=" + index);
                        }
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                            case KeyEvent.KEYCODE_DPAD_CENTER: {
                                if (item instanceof UserProfile) {
                                    UserProfile user = (UserProfile) item;
                                    onProfileSelected(user);
                                    return true;
                                }
                            }
                            case KeyEvent.KEYCODE_BACK: {
                                hideView();
                                return true;
                            }
                        }
                        return false;
                    }
                });
                profileObjectAdapter = new ArrayObjectAdapter(presenter);
                profileObjectAdapter.addAll(0, profileList);
                ProfileBridgeAdapter bridgeAdapter = new ProfileBridgeAdapter();
                bridgeAdapter.setAdapter(profileObjectAdapter);
                profileGridView.setAdapter(bridgeAdapter);

                setSmartProfileFocus(size);
            }
        }catch(Exception e){
        }
    }

    private void setSmartProfileFocus(int size){
        if(updatedProfile != null){
            int index = 0;
            String profileId = updatedProfile.getId();
            if(profileId != null) {
                for (int i = 0; i < size; i++) {
                    UserProfile profile = (UserProfile) profileObjectAdapter.get(i);
                    try {
                        if (profileId.equals(profile.getId())){
                            if (Log.INCLUDE) {
                                Log.d(TAG, "setSmartProfileFocus: selected index=" + i + ", profile name=" + profile.getName());
                            }
                            index = i;
                            break;
                        }
                    } catch (Exception e) {
                    }
                }
            }
            final int selectedIndex = index;
            profileGridView.setOnChildLaidOutListener(new OnChildLaidOutListener() {
                @Override
                public void onChildLaidOut(ViewGroup parent, View view, int position, long id) {
                    if(position == selectedIndex) {
                        try {
                            profileGridView.setSelectedPosition(selectedIndex);
                            View child = profileGridView.getLayoutManager().findViewByPosition(selectedIndex);
                            if (child != null) {
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "setSmartProfileFocus: index=" + selectedIndex);
                                }
                                child.requestFocus();
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            });
        }
    }

    private void onProfileSelected(UserProfile profile) {
        if (profile.isAddOption()) {
            ((ProfileManageDialog) getParentFragment()).showProfileView(ProfileManageDialog.TYPE_ADD, null, profileList);

        } else {
            ((ProfileManageDialog) getParentFragment()).showProfileView(ProfileManageDialog.TYPE_EDIT, profile, profileList);
        }
    }

    private void hideView() {
        if (Log.INCLUDE) {
            Log.d(TAG, "hideView: profileUpdated=" + profileUpdated);
        }
        if (profileUpdated) {
            profileUpdated = false;
            ((ProfileManageDialog) getParentFragment()).hideProfileView(null);
        } else {
            ((ProfileManageDialog) getParentFragment()).hideProfileView();
        }
    }
}
