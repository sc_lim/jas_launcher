package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.util.Log;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.dm.def.RentalType;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class PackageContent implements Parcelable {
    @Expose
    @SerializedName("packageId")
    private String packageId;
    @Expose
    @SerializedName("contentCnt")
    private int contentCnt;
    @Expose
    @SerializedName("contentList")
    private List<Content> contentList;
    @Expose
    @SerializedName("isLike")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isLike;
    @Expose
    @SerializedName("rating")
    private int rating;
    @Expose
    @SerializedName("packageDescription")
    private String packageDescription;
    @Expose
    @SerializedName("packageTitle")
    private String packageTitle;
    @SerializedName("productList")
    @Expose
    private List<Product> productList = new ArrayList<>();
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("termsAndConditions")
    @Expose
    private String termsAndConditions;

    protected PackageContent(Parcel in) {
        packageId = in.readString();
        contentCnt = in.readInt();
        contentList = in.createTypedArrayList(Content.CREATOR);
        isLike = in.readByte() != 0;
        rating = in.readInt();
        packageDescription = in.readString();
        packageTitle = in.readString();
        in.readList(this.productList, (Product.class.getClassLoader()));
        categoryId = in.readString();
        termsAndConditions = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(packageId);
        dest.writeInt(contentCnt);
        dest.writeTypedList(contentList);
        dest.writeByte((byte) (isLike ? 1 : 0));
        dest.writeInt(rating);
        dest.writeString(packageDescription);
        dest.writeString(packageTitle);
        dest.writeList(productList);
        dest.writeString(categoryId);
        dest.writeString(termsAndConditions);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<PackageContent> CREATOR = new Parcelable.Creator<PackageContent>() {
        @Override
        public PackageContent createFromParcel(Parcel in) {
            return new PackageContent(in);
        }

        @Override
        public PackageContent[] newArray(int size) {
            return new PackageContent[size];
        }
    };

    public int getContentCnt() {
        return contentCnt;
    }

    public List<Content> getContentList() {
        return contentList;
    }

    public void setLike(boolean like) {
        isLike = like;
    }

    public boolean isLike() {
        return isLike;
    }

    public int getRating() {
        return rating;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public String getPackageTitle() {
        return packageTitle;
    }

    public String getPackageId() {
        return packageId;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public List<Product> getProductList() {
        List<Product> list = new ArrayList<>();
        for (Product product : productList) {
            if (product.getProductType() == ProductType.ERROR || product.getRentalType() == RentalType.ERROR) {
                if (Log.INCLUDE) {
                    Log.d("PackageContent", "ERROR Type : " + product);
                }
                continue;
            }

            list.add(product);
        }

        return list;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "PackageContent{" +
                "packageId='" + packageId + '\'' +
                ", contentCnt=" + contentCnt +
                ", contentList=" + contentList +
                ", isLike=" + isLike +
                ", rating=" + rating +
                ", packageDescription='" + packageDescription + '\'' +
                ", packageTitle='" + packageTitle + '\'' +
                ", productList=" + productList +
                ", categoryId='" + categoryId + '\'' +
                ", termsAndConditions='" + termsAndConditions + '\'' +
                '}';
    }
}
