/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.message.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mc.kim on 10,12,2019
 */
public class MessageList implements Parcelable {

    @SerializedName("list")
    @Expose
    private java.util.List<Message> list = null;

    public MessageList(Parcel in) {
        in.readList(this.list, (Message.class.getClassLoader()));
    }

    public static final Creator<MessageList> CREATOR = new Creator<MessageList>() {
        @Override
        public MessageList createFromParcel(Parcel in) {
            return new MessageList(in);
        }

        @Override
        public MessageList[] newArray(int size) {
            return new MessageList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(list);
    }

    public List<Message> getList() {
        return list;
    }
}
