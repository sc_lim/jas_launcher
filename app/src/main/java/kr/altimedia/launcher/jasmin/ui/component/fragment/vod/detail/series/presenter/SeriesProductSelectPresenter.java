package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonItem;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class SeriesProductSelectPresenter extends Presenter {
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.presenter_series_product_select, parent, false);
        return new ProductViewHolder(v, parent.getLayoutDirection());
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        ProductViewHolder vh = (ProductViewHolder) viewHolder;

        ProductButtonItem productButtonItem = (ProductButtonItem) item;
        String type = vh.view.getContext().getString(productButtonItem.getProductType().getName());
        String unit = productButtonItem.getUnit();
        float value = productButtonItem.getValue();

        vh.title.setText(type);
        vh.unit.setText(unit);
        if (value != -1) {
            vh.value.setText(StringUtil.getFormattedPrice(value));
        }

        //error case for expireDate
        if (value == -1 && unit.contains("left")) {
            vh.value.setText("-1");
        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }

    public static class ProductViewHolder extends Presenter.ViewHolder {
        TextView title;
        TextView value;
        TextView unit;

        int mLayoutDirection;

        public ProductViewHolder(View view, int layoutDirection) {
            super(view);
            title = view.findViewById(R.id.title);
            value = view.findViewById(R.id.value);
            unit = view.findViewById(R.id.unit);
            mLayoutDirection = layoutDirection;
        }
    }
}
