/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.widget;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.leanback.widget.Presenter;
import androidx.recyclerview.widget.RecyclerView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.playback.TrailerObjectAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.BaseGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

/**
 * Created by mc.kim on 29,04,2020
 */
public abstract class TrailerDetailsDescriptionPresenter extends Presenter {
    private final TrailerDataProvider mDataProvider;
    private final String TAG = TrailerDetailsDescriptionPresenter.class.getSimpleName();
    private final ItemBridgeAdapter mItemBridgeAdapter;

    public TrailerDetailsDescriptionPresenter(@NonNull TrailerDataProvider dataProvider) {
        this.mDataProvider = dataProvider;
        this.mItemBridgeAdapter = new ItemBridgeAdapter();

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        final ImageView mTrailerDescriptionIcon;
        final TextView mTrailerDescription;
        final TextView mTitle;
        ImageIndicator mLeftIndicator;
        ImageIndicator mRightIndicator;
        HorizontalGridView mTrailerListView;

        private boolean mIsTrailerMode = false;

        public ViewHolder(final View view) {
            super(view);
            mTitle = view.findViewById(R.id.lb_details_description_title);
            mTrailerDescriptionIcon = view.findViewById(R.id.trailerDescriptionIcon);
            mTrailerDescription = view.findViewById(R.id.trailerDescription);
            mLeftIndicator = view.findViewById(R.id.leftIndicator);
            mRightIndicator = view.findViewById(R.id.rightIndicator);
            mTrailerListView = view.findViewById(R.id.trailerListView);
        }

        public TextView getTitle() {
            return mTitle;
        }

        public void setEnableTrailerSelectMode(boolean isTrailerMode) {
            if (mIsTrailerMode == isTrailerMode) {
                return;
            }
            mIsTrailerMode = isTrailerMode;

            mTrailerDescriptionIcon.setImageResource(mIsTrailerMode ? R.drawable.icon_up : R.drawable.icon_down);
            mTrailerDescription.setText(mIsTrailerMode ? R.string.close : R.string.more_trailer);
        }

        public boolean isTrailerMode() {
            return mIsTrailerMode;
        }
    }

    private final int mResourceId = R.layout.view_trailer_description;

    @Override
    public final ViewHolder onCreateViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(mResourceId, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public final void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        onBindDescription(vh, item);
        onBindTrailerList(vh, item);
    }

    protected abstract void onBindDescription(ViewHolder vh, Object item);

    private void onBindTrailerList(ViewHolder vh, Object item) {
        Log.d(TAG, "onBindTrailerList");

        int size = mDataProvider.getTrailerDataList().size();
        if (size <= 1) {
            vh.mTrailerDescriptionIcon.setVisibility(View.INVISIBLE);
            vh.mTrailerDescription.setVisibility(View.INVISIBLE);
            vh.mLeftIndicator.setVisibility(View.INVISIBLE);
            vh.mRightIndicator.setVisibility(View.INVISIBLE);
            vh.mTrailerListView.setVisibility(View.INVISIBLE);
            return;
        }

        HorizontalGridView trailerList = vh.mTrailerListView;
        trailerList.setFocusScrollStrategy(BaseGridView.FOCUS_SCROLL_ITEM);
        trailerList.setHorizontalSpacing(vh.view.getResources().getDimensionPixelSize(R.dimen.trailer_space));
        vh.mLeftIndicator.initIndicator(mDataProvider.getTrailerDataList().size(), mDataProvider.getVisibleSize());
        vh.mRightIndicator.initIndicator(mDataProvider.getTrailerDataList().size(), mDataProvider.getVisibleSize());
        TrailerVideoPresenter trailerVideoPresenter = new TrailerVideoPresenter(mDataProvider);
        TrailerObjectAdapter objectAdapter = new TrailerObjectAdapter(trailerVideoPresenter, mDataProvider);
        mItemBridgeAdapter.setAdapter(objectAdapter);
        trailerList.setAdapter(mItemBridgeAdapter);

        trailerList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int focus = recyclerView.getChildPosition(recyclerView.getFocusedChild());

                vh.mLeftIndicator.updateArrow(focus);
                vh.mRightIndicator.updateArrow(focus);
            }
        });
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
    }

    @Override
    public void onViewAttachedToWindow(Presenter.ViewHolder holder) {
        ViewHolder vh = (ViewHolder) holder;
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(Presenter.ViewHolder holder) {
        ViewHolder vh = (ViewHolder) holder;
        super.onViewDetachedFromWindow(holder);
    }

    private void setTopMargin(TextView textView, int topMargin) {
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) textView.getLayoutParams();
        lp.topMargin = topMargin;
        textView.setLayoutParams(lp);
    }

}
