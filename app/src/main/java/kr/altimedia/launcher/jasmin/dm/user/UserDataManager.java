/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user;

import android.os.Bundle;

import com.google.gson.JsonArray;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.user.module.UserModule;
import kr.altimedia.launcher.jasmin.dm.user.object.CollectedContent;
import kr.altimedia.launcher.jasmin.dm.user.object.FAQInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.FavoriteContent;
import kr.altimedia.launcher.jasmin.dm.user.object.Membership;
import kr.altimedia.launcher.jasmin.dm.user.object.OtpId;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileIconCategory;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileList;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileLoginResult;
import kr.altimedia.launcher.jasmin.dm.user.object.PurchasedContent;
import kr.altimedia.launcher.jasmin.dm.user.object.ResumeTimeResult;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscribedContent;
import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ChangePinCodeResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.OtpResponse;
import kr.altimedia.launcher.jasmin.dm.user.remote.UserRemote;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class UserDataManager extends BaseDataManager {
    private final String TAG = UserDataManager.class.getSimpleName();
    private UserRemote mUserRemote;

    private final String TYPE_ACCOUNT_PIN = "accountPin";
    private final String TYPE_PROFILE_PIN = "profilePin";

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        UserModule userModule = new UserModule();
        Retrofit userRetrofit = userModule.provideUserModule(mOkHttpClient);
        mUserRemote = new UserRemote(userModule.provideUserApi(userRetrofit));
    }

    @Override
    public String getLoginToken() {
        return AccountManager.getInstance().getLoginToken();
    }

    public MbsDataTask checkAccountPinCode(String said, String profileId, String currentPinCode, MbsDataProvider<String, Boolean> provider) {
        return checkPinCode(said, profileId, TYPE_ACCOUNT_PIN, currentPinCode, provider);
    }

    public MbsDataTask checkProfilePinCode(String said, String profileId, String currentPinCode, MbsDataProvider<String, Boolean> provider) {
        return checkPinCode(said, profileId, TYPE_PROFILE_PIN, currentPinCode, provider);
    }

    private MbsDataTask checkPinCode(String said, String profileId, String checkType, String currentPinCode, MbsDataProvider<String, Boolean> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, Boolean>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException , AuthenticationException {
                return mUserRemote.checkPinCode(said, profileId, checkType, currentPinCode);
            }


            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask changeAccountPinCode(String said, String profileId, String currentPinCode, String newPinCode, MbsDataProvider<String, ChangePinCodeResponse> provider) {
        return changePinCode(said, profileId, TYPE_ACCOUNT_PIN, currentPinCode, newPinCode, provider);
    }

    public MbsDataTask changeProfilePinCode(String said, String profileId, String currentPinCode, String newPinCode, MbsDataProvider<String, ChangePinCodeResponse> provider) {
        return changePinCode(said, profileId, TYPE_PROFILE_PIN, currentPinCode, newPinCode, provider);
    }

    private MbsDataTask changePinCode(String said, String profileId, String changeType, String currentPinCode, String newPinCode, MbsDataProvider<String, ChangePinCodeResponse> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, ChangePinCodeResponse>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.changePinCode(said, profileId, changeType, currentPinCode, newPinCode);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, ChangePinCodeResponse result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask resetProfilePinCode(String said, String profileId, MbsDataProvider<String, Boolean> provider) {
        return null;
    }

    public MbsDataTask resetAccountPinCode(String said, String profileId, MbsDataProvider<String, Boolean> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, Boolean>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException , AuthenticationException{
                return mUserRemote.resetPinCode(said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getOtpId(String said, String profileId, MbsDataProvider<String, OtpId> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, OtpId>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException , AuthenticationException{
                return mUserRemote.getOtpId(said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, OtpId result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getOtp(String loginId, MbsDataProvider<String, OtpResponse.Otp> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, OtpResponse.Otp>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getOtp(loginId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, OtpResponse.Otp result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(loginId);
        return mMbsDataTask;
    }

    public MbsDataTask checkOtp(String loginId, String otp, MbsDataProvider<String, Boolean> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, Boolean>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.checkOtp(loginId, otp);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(loginId);
        return mMbsDataTask;
    }

    public MbsDataTask getMembership(String said, MbsDataProvider<String, Membership> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, Membership>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getMembership(said);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Membership result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask postWatchingInfo(String said, String profileId, String deviceType, JsonArray jsonArray,
                                        MbsDataProvider<String, Boolean> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Boolean>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.postWatchingInfo(said, profileId, deviceType, jsonArray);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getFavoriteChannelList(String said, String profileID,
                                              MbsDataProvider<String, List<String>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<String>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getFavoriteChannelList(said, profileID);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<String> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask putFavoriteChannelList(String said, String profileId, String channelID,
                                              MbsDataProvider<String, Boolean> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, Boolean>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.putFavoriteChannelList(said, profileId, channelID);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask postFavoriteChannelList(String said, String profileId, Long[] channelID,
                                               MbsDataProvider<String, List<Long>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<Long>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.postFavoriteChannelList(said, profileId, channelID);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<Long> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getBlockedChannelList(String said, String profileID,
                                             MbsDataProvider<String, List<String>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<String>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getBlockedChannelList(said, profileID);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<String> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask putBlockedChannelList(String said, String profileId, Long channelID,
                                             MbsDataProvider<String, Boolean> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, Boolean>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.putBlockedChannelList(said, profileId, channelID);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask postBlockedChannelList(String said, String profileId, String[] channelID,
                                              MbsDataProvider<String, List<String>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<String>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.postBlockedChannelList(said, profileId, channelID);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<String> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getResumeTime(String said, String profileId, String contentId,
                                     MbsDataProvider<String, ResumeTimeResult> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, ResumeTimeResult>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getResumeTime(said, profileId, contentId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, ResumeTimeResult result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(contentId);
        return mMbsDataTask;
    }

    public MbsDataTask postResumeTime(String said, String profileId, String contentId, long resumeTime, long startTime, long endTime,
                                      MbsDataProvider<String, Boolean> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, BaseResponse>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.postResumeTime(said, profileId, contentId, resumeTime, startTime, endTime);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, BaseResponse result) {
                provider.needLoading(false);
                provider.onSuccess(key, true);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(contentId);
        return mMbsDataTask;
    }

    public MbsDataTask getFAQList(String said, String profileId,
                                  MbsDataProvider<String, List<FAQInfo>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<FAQInfo>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getFAQList(said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<FAQInfo> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getCollectionList(String language, String said, String profileId,
                                         MbsDataProvider<String, List<CollectedContent>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<CollectedContent>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getCollectionList(language, said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<CollectedContent> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(profileId);
        return mMbsDataTask;
    }

    public MbsDataTask getPurchaseList(String language, String said, String profileId,
                                       MbsDataProvider<String, List<PurchasedContent>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<PurchasedContent>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getPurchaseList(language, said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<PurchasedContent> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(profileId);
        return mMbsDataTask;
    }

    public MbsDataTask putPurchaseListHidden(String said, String profileId, String[] contentIds,
                                             MbsDataProvider<String, Boolean> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Boolean>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.putPurchaseListHidden(said, profileId, contentIds);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getSubscriptionList(String language, String said, String profileId,
                                           MbsDataProvider<String, List<SubscribedContent>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<SubscribedContent>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getSubscriptionList(language, said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<SubscribedContent> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(profileId);
        return mMbsDataTask;
    }

    public MbsDataTask putStopSubscription(String said, String profileId, String productId, String purchaseId,
                                           MbsDataProvider<String, Boolean> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Boolean>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.putStopSubscription(said, profileId, productId, purchaseId);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getProfileList(String said, MbsDataProvider<String, ProfileList> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, ProfileList>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getProfileList(said);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, ProfileList result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask putProfile(String said, String profileId, String profileName, String profileIconId,
                                  MbsDataProvider<String, Boolean> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Boolean>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.putProfile(said, profileId, profileName, profileIconId);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask postProfile(String said, String profileName, String profileIconId,
                                   MbsDataProvider<String, Boolean> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Boolean>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.postProfile(said, profileName, profileIconId);
            }


            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask deleteProfile(String said, String profileId,
                                     MbsDataProvider<String, Boolean> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Boolean>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.deleteProfile(said, profileId);
            }


            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask requestProfileLogin(String loginId, String said, String profileId, MbsDataProvider<String, ProfileLoginResult> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, ProfileLoginResult>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.requestProfileLogin(loginId, said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, ProfileLoginResult result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(profileId);
        return mMbsDataTask;
    }

    public MbsDataTask putProfileRating(String said, String profileId, String rating, MbsDataProvider<String, Boolean> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, Boolean>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.putProfileRating(said, profileId, rating);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask putProfileLock(String said, String profileId, String lockYn, String pinCode, MbsDataProvider<String, Boolean> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, Boolean>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.putProfileLock(said, profileId, lockYn, pinCode);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getProfileIconCategoryList(String language, String said, String profileId, MbsDataProvider<String, List<ProfileIconCategory>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<ProfileIconCategory>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getProfileIconCategoryList(language, said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<ProfileIconCategory> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(profileId);
        return mMbsDataTask;
    }

    public MbsDataTask getDeviceList(String said, String profileId, MbsDataProvider<String, List<UserDevice>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<UserDevice>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getDeviceList(said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<UserDevice> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask putDevice(String said, String profileId, String deviceId, String deviceName, MbsDataProvider<String, Boolean> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, Boolean>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.putDevice(said, profileId, deviceId, deviceName);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask deleteDevice(String said, String profileId, String deviceId, MbsDataProvider<String, Boolean> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, Boolean>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.deleteDevice(said, profileId, deviceId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getFavoriteVodList(String language, String said, String profileId,
                                          MbsDataProvider<String, List<FavoriteContent>> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, List<FavoriteContent>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getFavoriteVod(language, said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<FavoriteContent> result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }


    public MbsDataTask getIntentData(String accountId, String appId, String macAddress,
                                     MbsDataProvider<String, Bundle> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Bundle>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getIntentData(accountId, appId, macAddress);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Bundle result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(appId);
        return mMbsDataTask;
    }
}
