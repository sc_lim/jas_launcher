/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter;

import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;

public class SettingBaseButtonItemBridgeAdapter extends ItemBridgeAdapter {
    private final String TAG = SettingBaseButtonItemBridgeAdapter.class.getSimpleName();

    protected SettingBaseButtonItemBridgeAdapter.OnClickButton onClickButton;
    private OnKeyListener onKeyListener;

    public SettingBaseButtonItemBridgeAdapter(ObjectAdapter adapter) {
        super(adapter);
    }

    public void setListener(OnClickButton onClickButton) {
        this.onClickButton = onClickButton;
    }

    public void setOnKeyListener(OnKeyListener onKeyListener) {
        this.onKeyListener = onKeyListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);
        OnButtonKeyListener onButtonKeyListener = new OnButtonKeyListener(onClickButton, viewHolder, onKeyListener);
        viewHolder.getViewHolder().view.setOnKeyListener(onButtonKeyListener);
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        viewHolder.getViewHolder().view.setOnKeyListener(null);
    }

    private static class OnButtonKeyListener implements View.OnKeyListener {
        private final SettingBaseButtonItemBridgeAdapter.OnClickButton onClickButton;
        private final ViewHolder viewHolder;
        private final OnKeyListener onKeyListener;

        public OnButtonKeyListener(OnClickButton onClickButton,
                                   ViewHolder viewHolder, OnKeyListener onKeyListener) {
            this.onClickButton = onClickButton;
            this.viewHolder = viewHolder;
            this.onKeyListener = onKeyListener;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != KeyEvent.ACTION_DOWN) {
                return false;
            }

            boolean isConsumed = false;

            switch (keyCode) {
                case KeyEvent.KEYCODE_ENTER:
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    if (onClickButton != null) {
                        isConsumed = onClickButton.onClickButton(viewHolder.getItem());
                    }

                    break;
            }

            if (onKeyListener != null) {
                return onKeyListener.onKey(keyCode, isConsumed, viewHolder);
            }

            return isConsumed;
        }
    }

    public interface OnClickButton<T> {
        boolean onClickButton(T item);
    }

    public interface OnKeyListener {
        boolean onKey(int keyCode, boolean isConsumed, ViewHolder viewHolder);
    }
}