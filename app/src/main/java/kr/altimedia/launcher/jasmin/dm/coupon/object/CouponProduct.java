/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.def.NormalDateTimeDeserializer2;

public class CouponProduct implements Parcelable {
    public static final Creator<CouponProduct> CREATOR = new Creator<CouponProduct>() {
        @Override
        public CouponProduct createFromParcel(Parcel in) {
            return new CouponProduct(in);
        }

        @Override
        public CouponProduct[] newArray(int size) {
            return new CouponProduct[size];
        }
    };
    @Expose
    @SerializedName("cpnTypeId")
    private String id;
    @Expose
    @SerializedName("cpnNm")
    private String name;
    @Expose
    @SerializedName("saleAmt")
    private int price; // 쿠폰가격
    @Expose
    @SerializedName("bnsType")
    @JsonAdapter(BonusTypeDeserializer.class)
    private BonusType bonusType; //보너스 구분
    @Expose
    @SerializedName("bnsAmt")
    private int bonusAmount; // 보너스 금액
    @Expose
    @SerializedName("bnsVal")
    private int bonusValue; // 보너스 요율 혹은 금액
    @Expose
    @SerializedName("chargAmt")
    private int chargedAmount; // 충전포인트
    @Expose
    @SerializedName("efctPerdRstrtnVal")
    private String availablePeriodValue; // 유효기간제한값
    @Expose
    @SerializedName("efctPerdRstrtnUnit")
    @JsonAdapter(AvailablePeriodUnitDeserializer.class)
    private AvailablePeriodUnit availablePeriodUnit; //유효기간단위코드
    @Expose
    @SerializedName("efctPerdStDt")
    @JsonAdapter(NormalDateTimeDeserializer2.class)
    private Date availableStartDate; //유효시작일
    @Expose
    @SerializedName("efctPerdFnsDt")
    @JsonAdapter(NormalDateTimeDeserializer2.class)
    private Date availableEndDate; //유효종료일


    protected int bgImage;

    protected CouponProduct(Parcel in) {
        id = in.readString();
        name = in.readString();
        price = in.readInt();
        bonusType = ((BonusType) in.readValue((BonusType.class.getClassLoader())));
        bonusAmount = in.readInt();
        bonusValue = in.readInt();
        chargedAmount = in.readInt();
        availablePeriodValue = in.readString();
        availablePeriodUnit = ((AvailablePeriodUnit) in.readValue((AvailablePeriodUnit.class.getClassLoader())));
        availableStartDate = ((Date) in.readValue((Date.class.getClassLoader())));
        availableEndDate = ((Date) in.readValue((Date.class.getClassLoader())));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeInt(price);
        dest.writeValue(bonusType);
        dest.writeInt(bonusAmount);
        dest.writeInt(bonusValue);
        dest.writeInt(chargedAmount);
        dest.writeString(availablePeriodValue);
        dest.writeValue(availablePeriodUnit);
        dest.writeValue(availableStartDate);
        dest.writeValue(availableEndDate);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public BonusType getBonusType() {
        return bonusType;
    }

    public int getBonusAmount() {
        return bonusAmount;
    }

    public int getBonusValue() {
        return bonusValue;
    }

    public int getChargedAmount() {
        return chargedAmount;
    }

    public String getAvailablePeriodValue() {
        return availablePeriodValue;
    }

    public AvailablePeriodUnit getAvailablePeriodUnit() {
        return availablePeriodUnit;
    }

    public Date getAvailableStartDate() {
        return availableStartDate;
    }

    public Date getAvailableEndDate() {
        return availableEndDate;
    }

    public int getBgImage() {
        return bgImage;
    }

    public void setBgImage(int rscId) {
        bgImage = rscId;
    }

    @NonNull
    @Override
    public String toString() {
        return "CouponProduct{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                "price='" + price + '\'' +
                "bonusType='" + bonusType + '\'' +
                "bonusAmount='" + bonusAmount + '\'' +
                "bonusValue='" + bonusValue + '\'' +
                "chargedAmount='" + chargedAmount + '\'' +
                "availablePeriodValue='" + availablePeriodValue + '\'' +
                "availablePeriodUnit='" + availablePeriodUnit + '\'' +
                "availableStartDate='" + availableStartDate + '\'' +
                "availableEndDate='" + availableEndDate + '\'' +
                "}";
    }
}
