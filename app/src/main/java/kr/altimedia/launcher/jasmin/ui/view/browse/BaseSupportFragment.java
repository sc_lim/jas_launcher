/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.BrandedSupportFragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.transition.TransitionHelper;
import androidx.leanback.transition.TransitionListener;

import kr.altimedia.launcher.jasmin.ui.view.util.StateMachine;

public class BaseSupportFragment extends BrandedSupportFragment {
    /**
     * The start state for all
     */
    protected final StateMachine.State STATE_START = new StateMachine.State("START", true, false);

    /**
     * Initial State for ENTRNACE transition.
     */
    protected final StateMachine.State STATE_ENTRANCE_INIT = new StateMachine.State("ENTRANCE_INIT");

    /**
     * prepareEntranceTransition is just called, but view not ready yet. We can enable the
     * busy spinner.
     */
    protected final StateMachine.State STATE_ENTRANCE_ON_PREPARED = new StateMachine.State("ENTRANCE_ON_PREPARED", true, false) {
        @Override
        public void run() {
            mProgressBarManager.show();
        }
    };

    /**
     * prepareEntranceTransition is called and main content view to slide in was created, so we can
     * call {@link #onEntranceTransitionPrepare}. Note that we dont set initial content to invisible
     * in this State, the process is very different in subclass, e.g. BrowseSupportFragment hide header
     * views and hide main fragment view in two steps.
     */
    protected final StateMachine.State STATE_ENTRANCE_ON_PREPARED_ON_CREATEVIEW = new StateMachine.State(
            "ENTRANCE_ON_PREPARED_ON_CREATEVIEW") {
        @Override
        public void run() {
            onEntranceTransitionPrepare();
        }
    };

    /**
     * execute the entrance transition.
     */
    protected final StateMachine.State STATE_ENTRANCE_PERFORM = new StateMachine.State("STATE_ENTRANCE_PERFORM") {
        @Override
        public void run() {
            mProgressBarManager.hide();
            onExecuteEntranceTransition();
        }
    };

    /**
     * execute onEntranceTransitionEnd.
     */
    protected final StateMachine.State STATE_ENTRANCE_ON_ENDED = new StateMachine.State("ENTRANCE_ON_ENDED") {
        @Override
        public void run() {
            onEntranceTransitionEnd();
        }
    };

    /**
     * either entrance transition completed or skipped
     */
    protected final StateMachine.State STATE_ENTRANCE_COMPLETE = new StateMachine.State("ENTRANCE_COMPLETE", true, false);

    /**
     * Event fragment.onCreate()
     */
    protected final StateMachine.Event EVT_ON_CREATE = new StateMachine.Event("onCreate");

    /**
     * Event fragment.onViewCreated()
     */
    protected final StateMachine.Event EVT_ON_CREATEVIEW = new StateMachine.Event("onCreateView");

    /**
     * Event for {@link #prepareEntranceTransition()} is called.
     */
    protected final StateMachine.Event EVT_PREPARE_ENTRANCE = new StateMachine.Event("prepareEntranceTransition");

    /**
     * Event for {@link #startEntranceTransition()} is called.
     */
    protected final StateMachine.Event EVT_START_ENTRANCE = new StateMachine.Event("startEntranceTransition");

    /**
     * Event for entrance transition is ended through Transition listener.
     */
    protected final StateMachine.Event EVT_ENTRANCE_END = new StateMachine.Event("onEntranceTransitionEnd");

    /**
     * Event for skipping entrance transition if not supported.
     */
    protected final StateMachine.Condition COND_TRANSITION_NOT_SUPPORTED = new StateMachine.Condition("EntranceTransitionNotSupport") {
        @SuppressLint("RestrictedApi")
        @Override
        public boolean canProceed() {
            return !TransitionHelper.systemSupportsEntranceTransitions();
        }
    };

    @SuppressLint("RestrictedApi")
    protected final StateMachine mStateMachine = new StateMachine();

    Object mEntranceTransition;
    final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        createStateMachineStates();
        createStateMachineTransitions();
        mStateMachine.start();
        super.onCreate(savedInstanceState);
        mStateMachine.fireEvent(EVT_ON_CREATE);
    }

    @SuppressLint("RestrictedApi")
    protected void createStateMachineStates() {
        mStateMachine.addState(STATE_START);
        mStateMachine.addState(STATE_ENTRANCE_INIT);
        mStateMachine.addState(STATE_ENTRANCE_ON_PREPARED);
        mStateMachine.addState(STATE_ENTRANCE_ON_PREPARED_ON_CREATEVIEW);
        mStateMachine.addState(STATE_ENTRANCE_PERFORM);
        mStateMachine.addState(STATE_ENTRANCE_ON_ENDED);
        mStateMachine.addState(STATE_ENTRANCE_COMPLETE);
    }

    @SuppressLint("RestrictedApi")
    protected void createStateMachineTransitions() {
        mStateMachine.addTransition(STATE_START, STATE_ENTRANCE_INIT, EVT_ON_CREATE);
        mStateMachine.addTransition(STATE_ENTRANCE_INIT, STATE_ENTRANCE_COMPLETE,
                COND_TRANSITION_NOT_SUPPORTED);
        mStateMachine.addTransition(STATE_ENTRANCE_INIT, STATE_ENTRANCE_COMPLETE,
                EVT_ON_CREATEVIEW);
        mStateMachine.addTransition(STATE_ENTRANCE_INIT, STATE_ENTRANCE_ON_PREPARED,
                EVT_PREPARE_ENTRANCE);
        mStateMachine.addTransition(STATE_ENTRANCE_ON_PREPARED,
                STATE_ENTRANCE_ON_PREPARED_ON_CREATEVIEW,
                EVT_ON_CREATEVIEW);
        mStateMachine.addTransition(STATE_ENTRANCE_ON_PREPARED,
                STATE_ENTRANCE_PERFORM,
                EVT_START_ENTRANCE);
        mStateMachine.addTransition(STATE_ENTRANCE_ON_PREPARED_ON_CREATEVIEW,
                STATE_ENTRANCE_PERFORM);
        mStateMachine.addTransition(STATE_ENTRANCE_PERFORM,
                STATE_ENTRANCE_ON_ENDED,
                EVT_ENTRANCE_END);
        mStateMachine.addTransition(STATE_ENTRANCE_ON_ENDED, STATE_ENTRANCE_COMPLETE);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mStateMachine.fireEvent(EVT_ON_CREATEVIEW);
    }

    /**
     * Enables entrance transition.<p>
     * Entrance transition is the standard slide-in transition that shows rows of data in
     * browse screen and details screen.
     * <p>
     * The method is ignored before LOLLIPOP (API21).
     * <p>
     * This method must be called in or
     * before onCreate().  Typically entrance transition should be enabled when savedInstance is
     * null so that fragment restored from instanceState does not run an extra entrance transition.
     * When the entrance transition is enabled, the fragment will make headers and content
     * hidden initially.
     * When data of rows are ready, app must call {@link #startEntranceTransition()} to kick off
     * the transition, otherwise the rows will be invisible forever.
     * <p>
     * It is similar to android:windowsEnterTransition and can be considered a late-executed
     * android:windowsEnterTransition controlled by app.  There are two reasons that app needs it:
     * <li> Workaround the problem that activity transition is not available between launcher and
     * app.  Browse activity must programmatically start the slide-in transition.</li>
     * <li> Separates DetailsOverviewRow transition from other rows transition.  So that
     * the DetailsOverviewRow transition can be executed earlier without waiting for all rows
     * to be loaded.</li>
     * <p>
     * Transition object is returned by createEntranceTransition().  Typically the app does not need
     * override the default transition that browse and details provides.
     */
    @SuppressLint("RestrictedApi")
    public void prepareEntranceTransition() {
        mStateMachine.fireEvent(EVT_PREPARE_ENTRANCE);
    }

    /**
     * Create entrance transition.  Subclass can override to load transition from
     * resource or construct manually.  Typically app does not need to
     * override the default transition that browse and details provides.
     */
    protected Object createEntranceTransition() {
        return null;
    }

    /**
     * Run entrance transition.  Subclass may use TransitionManager to perform
     * go(Scene) or beginDelayedTransition().  App should not override the default
     * implementation of browse and details fragment.
     */
    protected void runEntranceTransition(Object entranceTransition) {
    }

    /**
     * Callback when entrance transition is prepared.  This is when fragment should
     * stop user input and animations.
     */
    protected void onEntranceTransitionPrepare() {
    }

    /**
     * Callback when entrance transition is started.  This is when fragment should
     * stop processing layout.
     */
    protected void onEntranceTransitionStart() {
    }

    /**
     * Callback when entrance transition is ended.
     */
    protected void onEntranceTransitionEnd() {
    }

    /**
     * When fragment finishes loading data, it should call startEntranceTransition()
     * to execute the entrance transition.
     * startEntranceTransition() will start transition only if both two conditions
     * are satisfied:
     * <li> prepareEntranceTransition() was called.</li>
     * <li> has not executed entrance transition yet.</li>
     * <p>
     * If startEntranceTransition() is called before onViewCreated(), it will be pending
     * and executed when view is created.
     */
    public void startEntranceTransition() {
        mStateMachine.fireEvent(EVT_START_ENTRANCE);
    }

    void onExecuteEntranceTransition() {
        // wait till views get their initial position before start transition
        final View view = getView();
        if (view == null) {
            // fragment view destroyed, transition not needed
            return;
        }
        view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                view.getViewTreeObserver().removeOnPreDrawListener(this);
                if (getContext() == null || getView() == null) {
                    // bail out if fragment is destroyed immediately after startEntranceTransition
                    return true;
                }
                internalCreateEntranceTransition();
                onEntranceTransitionStart();
                if (mEntranceTransition != null) {
                    runEntranceTransition(mEntranceTransition);
                } else {
                    mStateMachine.fireEvent(EVT_ENTRANCE_END);
                }
                return false;
            }
        });
        view.invalidate();
    }

    @SuppressLint("RestrictedApi")
    void internalCreateEntranceTransition() {
        mEntranceTransition = createEntranceTransition();
        if (mEntranceTransition == null) {
            return;
        }
        TransitionHelper.addTransitionListener(mEntranceTransition, new TransitionListener() {
            @Override
            public void onTransitionEnd(Object transition) {
                mEntranceTransition = null;
                mStateMachine.fireEvent(EVT_ENTRANCE_END);
            }
        });
    }

    /**
     * Returns the {@link ProgressBarManager}.
     *
     * @return The {@link ProgressBarManager}.
     */
    public final ProgressBarManager getProgressBarManager() {
        return mProgressBarManager;
    }
}