/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.adapter;

import android.view.KeyEvent;
import android.view.View;

import java.util.ArrayList;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.data.PopularChannel;
import kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.presenter.PopularItemPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;

public class PopularListBridgeAdapter extends ItemBridgeAdapter {
    private static final String TAG = PopularListBridgeAdapter.class.getSimpleName();
    private OnBindListener onBindListener;

    public PopularListBridgeAdapter(OnBindListener onBindListener) {
        this.onBindListener = onBindListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        PopularItemPresenter presenter = (PopularItemPresenter) viewHolder.mPresenter;
        PopularItemPresenter.OnKeyListener onKeyListener = presenter.getOnKeyListener();

        PopularItemPresenter.ChannelItemViewHolder itemViewHolder = (PopularItemPresenter.ChannelItemViewHolder) viewHolder.getViewHolder();

        int index = (int) viewHolder.itemView.getTag(R.id.KEY_INDEX);
        PopularChannel channel = (PopularChannel) viewHolder.mItem;
        if (onKeyListener != null) {
            viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    return onKeyListener.onKey(event.getAction(), keyCode, index, channel, itemViewHolder);
                }
            });
        }

        if (onBindListener != null) {
            onBindListener.onBind(index, channel, itemViewHolder);
        }
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        viewHolder.mHolder.view.setOnKeyListener(null);
    }

    public ArrayList<PopularChannel> getList() {
        ArrayList<PopularChannel> list = new ArrayList<>();
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) getAdapter();
        for (int i = 0; i < objectAdapter.size(); i++) {
            PopularChannel channel = (PopularChannel) objectAdapter.get(i);
            list.add(channel);
        }

        return list;
    }

    public interface OnBindListener {
        void onBind(int index, PopularChannel channel, Presenter.ViewHolder viewHolder);
    }
}
