/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.DeviceUtil;
import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.user.module.UserModule;
import kr.altimedia.launcher.jasmin.dm.user.object.LoginResult;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscriberInfo;
import kr.altimedia.launcher.jasmin.dm.user.remote.UserRemote;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static kr.altimedia.launcher.jasmin.dm.user.AuthenticationHandler.KEY_ERROR_CODE;
import static kr.altimedia.launcher.jasmin.dm.user.AuthenticationHandler.KEY_ERROR_MESSAGE;
import static kr.altimedia.launcher.jasmin.dm.user.AuthenticationHandler.KEY_ERROR_TYPE;

/**
 * Created by mc.kim on 08,06,2020
 */
public class AuthenticationManager extends BaseDataManager {
    private final String TAG = AuthenticationManager.class.getSimpleName();
    private UserRemote mUserRemote;
    private List<WeakReference<MbsDataTask>> taskList = new ArrayList<>();
    private final Context mContext;

    public AuthenticationManager(Context mContext) {
        super();
        this.mContext = mContext;
    }

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        UserModule userModule = new UserModule();
        Retrofit userRetrofit = userModule.provideUserModule(mOkHttpClient);
        mUserRemote = new UserRemote(userModule.provideUserApi(userRetrofit));
    }

    private AuthenticationHandler mAuthenticationHandler = new AuthenticationHandler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            getParentHandler().sendEmptyMessage(msg.what);
            super.handleMessage(msg);
            switch (msg.what) {
                case WHAT_START_ACTIVATION:
                    break;
                case WHAT_START_LOGIN_TOKEN:
                    break;
                case WHAT_START_SUBSCRIBER_INFO:
                    break;
                case WHAT_COMPLETE_ACTIVATION:
                    String saId = msg.getData().getString(KEY_ACTIVATION);
                    putString(KEY_ACTIVATION, saId);
                    startLoginTokenProcess(saId, mAuthenticationHandler);
                    break;
                case WHAT_COMPLETE_LOGIN_TOKEN: {
                    LoginResult loginResult = msg.getData().getParcelable(KEY_LOGIN_INFO);
                    putParcelable(KEY_LOGIN_INFO, loginResult);
                    Message message = Message.obtain(getParentHandler(), WHAT_COMPLETE_AUTHENTICATION);
                    message.what = WHAT_COMPLETE_AUTHENTICATION;
                    message.setData(getAuthenticationBundle());
                    getParentHandler().sendMessage(message);
                }
                break;
                case WHAT_COMPLETE_SUBSCRIBER_INFO: {
                    SubscriberInfo subscriberInfo = msg.getData().getParcelable(KEY_SUBSCRIBER_INFO);
                    putParcelable(KEY_SUBSCRIBER_INFO, subscriberInfo);
                    Message message = Message.obtain(getParentHandler(), WHAT_COMPLETE_AUTHENTICATION);
                    message.what = WHAT_COMPLETE_AUTHENTICATION;
                    message.setData(getAuthenticationBundle());
                    getParentHandler().sendMessage(message);
                }
                break;
                case WHAT_ERROR_ACTIVATION:
                    break;
                case WHAT_ERROR_LOGIN_TOKEN:
                    break;
                case WHAT_ERROR_SUBSCRIBER_INFO:
                    break;
            }
        }
    };

    public void requestAuthentication(@NonNull AuthenticationHandler authenticationHandler) {
        authenticationHandler.sendEmptyMessage(AuthenticationHandler.WHAT_START_AUTHENTICATION);
        mAuthenticationHandler.setParentHandler(authenticationHandler);
        startActivationProcess(NetworkUtil.getMacAddress(), NetworkUtil.getLocalIpAddress(), mAuthenticationHandler);
    }


    public MbsDataTask getActivation(String macAddress, String ipAddress, MbsDataProvider<String, String> provider) {

        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, String>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                String saIdFromSetting = StringUtils.nullToEmpty(SettingControl.getInstance().getSaId());
                if (Log.INCLUDE) {
                    Log.d(TAG, "saId : " + saIdFromSetting);
                }
                if (!saIdFromSetting.isEmpty()) {
                    return saIdFromSetting;
                }
                return mUserRemote.getActivationInfo(DeviceUtil.getModel(), macAddress, ipAddress);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, String result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }


        });

        mMbsDataTask.execute(macAddress);
        return mMbsDataTask;
    }

    public void reissueSaId() throws AuthenticationException, IOException, ResponseException {
        String api = mUserRemote.getActivationInfo(DeviceUtil.getModel(),NetworkUtil.getMacAddress(), NetworkUtil.getLocalIpAddress());
    }


    public MbsDataTask getLoginToken(String saId, MbsDataProvider<String, LoginResult> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, LoginResult>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.requestLogin(saId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, LoginResult result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }

        });

        mMbsDataTask.execute(saId);
        return mMbsDataTask;
    }

    @Deprecated
    public MbsDataTask getSubscriberInfo(String saId, String loginId, MbsDataProvider<String, SubscriberInfo> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, SubscriberInfo>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mUserRemote.getSubscriberInfo(saId, loginId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, SubscriberInfo result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(saId);
        return mMbsDataTask;
    }


    private void startActivationProcess(String macAddress, String ipAddress, AuthenticationHandler authenticationHandler) {
        authenticationHandler.sendEmptyMessage(AuthenticationHandler.WHAT_START_ACTIVATION);
        MbsDataTask mbsDataTask = getActivation(macAddress, ipAddress, new MbsDataProvider<String, String>() {
            @Override
            public void needLoading(boolean loading) {

            }

            @Override
            public void onFailed(int key) {

                String saIdFromSetting = StringUtils.nullToEmpty(SettingControl.getInstance().getSaId());
                if (!saIdFromSetting.isEmpty()) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "Activation fail network but saved Said exist so make success");
                    }
                    Message message = Message.obtain(authenticationHandler, AuthenticationHandler.WHAT_COMPLETE_ACTIVATION);
                    message.what = AuthenticationHandler.WHAT_COMPLETE_ACTIVATION;
                    Bundle bundle = new Bundle();
                    bundle.putString(AuthenticationHandler.KEY_ACTIVATION, saIdFromSetting);
                    message.setData(bundle);
                    authenticationHandler.sendMessage(message);
                } else {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "Activation fail network");
                    }
                    Message handlerMessage = makeFailMessage(AuthenticationHandler.WHAT_ERROR_ACTIVATION, authenticationHandler);
                    authenticationHandler.getParentHandler().sendMessage(handlerMessage);
                }
            }

            @Override
            public void onSuccess(String id, String result) {
                Message message = Message.obtain(authenticationHandler, AuthenticationHandler.WHAT_COMPLETE_ACTIVATION);
                message.what = AuthenticationHandler.WHAT_COMPLETE_ACTIVATION;
                Bundle bundle = new Bundle();
                bundle.putString(AuthenticationHandler.KEY_ACTIVATION, result);
                message.setData(bundle);
                authenticationHandler.sendMessage(message);
            }

            @Override
            public void onError(String errorCode, String message) {
                Message handlerMessage = makeErrorMessage(AuthenticationHandler.WHAT_ERROR_ACTIVATION,
                        errorCode, message, authenticationHandler);
                authenticationHandler.getParentHandler().sendMessage(handlerMessage);
            }

            @Override
            public Context getTaskContext() {
                return mContext;
            }

            @Override
            public boolean ignoreUserLogin() {
                return true;
            }
        });
        taskList.add(new WeakReference<>(mbsDataTask));
    }

    private void startLoginTokenProcess(String saId, AuthenticationHandler authenticationHandler) {
        authenticationHandler.sendEmptyMessage(AuthenticationHandler.WHAT_START_LOGIN_TOKEN);
        MbsDataTask mbsDataTask = getLoginToken(saId, new MbsDataProvider<String, LoginResult>() {
            @Override
            public void needLoading(boolean loading) {

            }

            @Override
            public void onFailed(int key) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "Login fail network");
                }
                AccountManager.getInstance().putData(AccountManager.KEY_SAID, saId);
                Message handlerMessage = makeFailMessage(AuthenticationHandler.WHAT_ERROR_LOGIN_TOKEN, authenticationHandler);
                authenticationHandler.getParentHandler().sendMessage(handlerMessage);
            }

            @Override
            public void onSuccess(String id, LoginResult result) {
                Message message = Message.obtain(authenticationHandler, AuthenticationHandler.WHAT_COMPLETE_LOGIN_TOKEN);
                message.what = AuthenticationHandler.WHAT_COMPLETE_LOGIN_TOKEN;
                Bundle bundle = new Bundle();
                bundle.putParcelable(AuthenticationHandler.KEY_LOGIN_INFO, result);
                message.setData(bundle);
                authenticationHandler.sendMessage(message);
            }

            @Override
            public void onError(String errorCode, String message) {
                Message handlerMessage = makeErrorMessage(AuthenticationHandler.WHAT_ERROR_LOGIN_TOKEN, errorCode, message, authenticationHandler);
                authenticationHandler.getParentHandler().sendMessage(handlerMessage);
            }

            @Override
            public Context getTaskContext() {
                return mContext;
            }

            @Override
            public boolean ignoreUserLogin() {
                return true;
            }
        });
        taskList.add(new WeakReference<>(mbsDataTask));
    }


    private Message makeErrorMessage(int type, String errorCode, String errorMessage, AuthenticationHandler handler) {
        Message handlerMessage = Message.obtain(handler, AuthenticationHandler.WHAT_ERROR_AUTHENTICATION);
        handlerMessage.what = AuthenticationHandler.WHAT_ERROR_AUTHENTICATION;
        Bundle bundle = handler.getAuthenticationBundle();
        if (Log.INCLUDE) {
            Log.d(TAG, "bundle : " + bundle.toString());
        }
        bundle.putInt(KEY_ERROR_TYPE, type);
        bundle.putString(KEY_ERROR_CODE, errorCode);
        bundle.putString(KEY_ERROR_MESSAGE, errorMessage);
        handlerMessage.setData(bundle);
        return handlerMessage;
    }

    private Message makeFailMessage(int type, AuthenticationHandler handler) {
        Message handlerMessage = Message.obtain(handler, AuthenticationHandler.WHAT_ERROR_AUTHENTICATION_NOT_CONNECTED);
        handlerMessage.what = AuthenticationHandler.WHAT_ERROR_AUTHENTICATION_NOT_CONNECTED;
        Bundle bundle = handler.getAuthenticationBundle();
        if (Log.INCLUDE) {
            Log.d(TAG, "bundle : " + bundle.toString());
        }
        bundle.putInt(KEY_ERROR_TYPE, type);
        handlerMessage.setData(bundle);
        return handlerMessage;
    }
}
