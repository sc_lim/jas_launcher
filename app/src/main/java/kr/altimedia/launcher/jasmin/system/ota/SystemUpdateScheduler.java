/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.system.ota;

import com.altimedia.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;

public class SystemUpdateScheduler {

    private final String TAG = SystemUpdateScheduler.class.getSimpleName();

    private long NORMAL_CHECK_DELAY = 10 * 60 * 1000;
    private long STANDBY_UPDATE_DELAY = 10 * 60 * 1000;

    private Timer checkTimer = null;
    private CheckTimerTask checkTimerTask = null;

    protected SystemUpdateScheduler() {
    }

    private long getDelayTime() {
        long interval = CWMPServiceProvider.getInstance().getNormalPeriod();

        if (interval > 0) {
            return interval;
        }

        return NORMAL_CHECK_DELAY;
    }

    protected void start(boolean isScreenOn) {
        long delay = getDelayTime();

        if (Log.INCLUDE) {
            Log.d(TAG, "start: delay=" + delay + ", isScreenOn=" + isScreenOn);
        }

        if (!isScreenOn) {
            delay = STANDBY_UPDATE_DELAY;
        }

        startTimer(delay);
    }

    protected void stop() {
        if (Log.INCLUDE) {
            Log.d(TAG, "stop scheduler");
        }
        stopTimer();
    }

    private void stopTimer() {
        if (Log.INCLUDE) {
            Log.d(TAG, "stopTimer");
        }
        if (checkTimer != null) {
            checkTimer.cancel();
        }
        if (checkTimerTask != null) {
            checkTimerTask.cancel();
        }
    }

    private void startTimer(long delayTime) {
        stopTimer();

        if (Log.INCLUDE) {
            Log.d(TAG, "startTimer: delay=" + delayTime);
        }
        checkTimer = new Timer();
        checkTimerTask = new CheckTimerTask();
        checkTimer.schedule(checkTimerTask, delayTime);
    }

    private class CheckTimerTask extends TimerTask {
        @Override
        public void run() {
            long delay = getDelayTime();

            if (Log.INCLUDE) {
                Log.d(TAG, "CheckTimerTask: went-off, delay=" + delay);
            }

            SystemUpdateManager systemUpdateManager = SystemUpdateManager.getInstance();

            if (!systemUpdateManager.isOnDownloading()) {
                systemUpdateManager.checkSoftwareUpdate();
            }

            startTimer(delay);
        }
    }
}
