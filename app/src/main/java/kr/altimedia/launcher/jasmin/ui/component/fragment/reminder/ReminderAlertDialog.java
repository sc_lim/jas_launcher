/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.reminder;

import android.animation.AnimatorInflater;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class ReminderAlertDialog implements View.OnKeyListener,
        ValueAnimator.AnimatorUpdateListener, LifecycleObserver {
    public static final String CLASS_NAME = ReminderAlertDialog.class.getName();
    private final String TAG = ReminderAlertDialog.class.getSimpleName();

    private Reminder reminder;

    private WindowManager wm;

    private View parentsView;
    private RelativeLayout layout;

    private float layoutHeight = 0;
    private ValueAnimator moveUpAnimator;
    private ValueAnimator moveDownAnimator;

    private OnReminderAlertActionListener onReminderAlertActionListener;

    private final int MESSAGE_ID = 0;
    private final long KEY_DELAY_TIME = TimeUtil.MIN;
    private final Handler hideHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (Log.INCLUDE) {
                Log.d(TAG, "call handleMessage");
            }

            if (onReminderAlertActionListener != null) {
                onReminderAlertActionListener.onReminderAlertWatch(reminder);
            }
        }
    };

    public ReminderAlertDialog(Context context, Reminder reminder) {
        this.reminder = reminder;

        LayoutInflater inflater = LayoutInflater.from(context);
        parentsView = inflater.inflate(R.layout.dialog_reminder_alert, null, false);

        initView(parentsView);
    }

    public void show(Context context) {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                PixelFormat.TRANSLUCENT);
        mParams.gravity = Gravity.CENTER;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.addView(parentsView, mParams);
                moveUpAnimator.start();
            }
        });
        if (onReminderAlertActionListener != null) {
            onReminderAlertActionListener.onShow();
        }
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }


    public void dismiss() {
        if (onReminderAlertActionListener != null) {
            onReminderAlertActionListener.onDismiss();
        }

        ProcessLifecycleOwner.get().getLifecycle().removeObserver(this);
        onReminderAlertActionListener = null;
        hideHandler.removeMessages(MESSAGE_ID);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.removeView(parentsView);
            }
        });

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        //App in background
        if (Log.INCLUDE) {
            Log.d(TAG, "call on Stopped");
        }
        dismiss();
    }

    private void initView(View view) {
        layout = view.findViewById(R.id.layout);

        initReminderInfo(view);
        initButton(view);
        initAnimator(view);
        startDisplayCountingStart();
    }

    private void initReminderInfo(View view) {
        TextView channelNumber = view.findViewById(R.id.channel_number);
        TextView channelName = view.findViewById(R.id.channel_name);
        TextView programTitle = view.findViewById(R.id.program_title);

        channelNumber.setText(reminder.getChannelNumber());
        channelName.setText(reminder.getChannelName());

        programTitle.setText(reminder.getTitle());
    }

    private void initButton(View view) {
        TextView watchNowButton = view.findViewById(R.id.watch_now_button);
        TextView cancelButton = view.findViewById(R.id.cancel_button);

        watchNowButton.setOnKeyListener(this);
        cancelButton.setOnKeyListener(this);
    }

    private void initAnimator(View view) {
        Context context = view.getContext();
        layoutHeight = context.getResources().getDimension(R.dimen.reminder_alert_bottom_height);

        moveUpAnimator = loadAnimator(context, R.animator.reminder_alert_move_up);
        moveUpAnimator.setInterpolator(new DecelerateInterpolator());
        moveUpAnimator.addUpdateListener(this);

        moveDownAnimator = loadAnimator(context, R.animator.reminder_alert_move_down);
        moveDownAnimator.setInterpolator(new AccelerateInterpolator());
        moveDownAnimator.addUpdateListener(this);
    }

    private void startDisplayCountingStart() {
        hideHandler.sendEmptyMessageDelayed(MESSAGE_ID, KEY_DELAY_TIME);
    }

    private ValueAnimator loadAnimator(Context context, int resId) {
        ValueAnimator animator = (ValueAnimator) AnimatorInflater.loadAnimator(context, resId);
        return animator;
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        float y = value * layoutHeight;
        layout.setTranslationY(y);

        if (y >= layoutHeight && animation == moveDownAnimator) {
            if (onReminderAlertActionListener != null) {
                onReminderAlertActionListener.onReminderAlertCancel();
            }
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onKey : " + keyCode);
        }

        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (v.getId() == R.id.watch_now_button) {
                    if (onReminderAlertActionListener != null) {
                        onReminderAlertActionListener.onReminderAlertWatch(reminder);
                    }
                } else {
                    moveDownAnimator.start();
                }

                return true;
            case KeyEvent.KEYCODE_BACK:
                if (onReminderAlertActionListener != null) {
                    onReminderAlertActionListener.onReminderAlertCancel();
                }

                return true;
        }

        return false;
    }

    public void setOnReminderAlertActionListener(OnReminderAlertActionListener onReminderAlertActionListener) {
        this.onReminderAlertActionListener = onReminderAlertActionListener;
    }

    public interface OnReminderAlertActionListener {
        void onReminderAlertWatch(Reminder reminder);

        void onReminderAlertCancel();

        void onDismiss();

        void onShow();
    }
}
