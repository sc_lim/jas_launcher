/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.view.View;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by mc.kim on 22,01,2020
 */
public class ScrollEnableCheckUtil {
    private final String TAG = ScrollEnableCheckUtil.class.getSimpleName();

    public enum Direction {
        LEFT, RIGHT, UP, DOWN
    }

    private final RecyclerView mRecyclerView;
    private OnIndicateChangedListener mOnIndicateChangedListener;
    private final boolean mIsVertical;

    public ScrollEnableCheckUtil(RecyclerView recyclerView, boolean isVertical) {
        this.mIsVertical = isVertical;
        this.mRecyclerView = recyclerView;
        addListener();
    }

    public void setIndicateChangedListetener(OnIndicateChangedListener onIndicateChangedListener){
        this.mOnIndicateChangedListener = onIndicateChangedListener;
    }


    private void addListener() {
        this.mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                notifyIndicateStateChange(recyclerView);
            }
        });
    }


    private void notifyIndicateStateChange(@NonNull RecyclerView recyclerView) {
        if (mOnIndicateChangedListener == null) {
            return;
        }

        boolean canScrollPreIndex = mIsVertical ? recyclerView.canScrollVertically(-1) : recyclerView.canScrollHorizontally(-1);
        boolean canScrollNextIndex = mIsVertical ? recyclerView.canScrollVertically(1) : recyclerView.canScrollHorizontally(1);

        if (Log.INCLUDE) {
            Log.d(TAG, "recyclerView.canScrollVertically(-1) : " + recyclerView.canScrollVertically(View.FOCUS_FORWARD));
            Log.d(TAG, "recyclerView.canScrollVertically(1) : " + recyclerView.canScrollVertically(View.FOCUS_BACKWARD));
            Log.d(TAG, "recyclerView.canScrollHorizontally(-1) : " + recyclerView.canScrollHorizontally(View.FOCUS_LEFT));
            Log.d(TAG, "recyclerView.canScrollHorizontally(-1) : " + recyclerView.canScrollHorizontally(View.FOCUS_RIGHT));
        }

        if (mIsVertical) {
            /*LEFT. RIGHT*/
            mOnIndicateChangedListener.notifyStateChanged(Direction.UP, canScrollPreIndex);
            mOnIndicateChangedListener.notifyStateChanged(Direction.DOWN, canScrollNextIndex);
        } else {
            /*UP. DOWN*/
            mOnIndicateChangedListener.notifyStateChanged(Direction.LEFT, canScrollPreIndex);
            mOnIndicateChangedListener.notifyStateChanged(Direction.RIGHT, canScrollNextIndex);
        }
    }


    public interface OnIndicateChangedListener {
        void notifyStateChanged(Direction direction, boolean canScroll);
    }
}
