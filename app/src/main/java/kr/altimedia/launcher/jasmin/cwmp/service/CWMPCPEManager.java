/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service;

import android.content.Context;

import com.altimedia.cwmplibrary.common.CWMPParameter;
import com.altimedia.cwmplibrary.common.ModelObject;
import com.altimedia.cwmplibrary.common.core.CPEManager;
import com.altimedia.cwmplibrary.common.method.AcsNetworkListener;
import com.altimedia.cwmplibrary.common.method.InformCallback;
import com.altimedia.cwmplibrary.common.method.RPCListener;
import com.altimedia.cwmplibrary.ttbb.inform.TTBBInform;
import com.altimedia.cwmplibrary.ttbb.object.InternetGatewayDevice;

public class CWMPCPEManager {
    private static final CWMPCPEManager instance = new CWMPCPEManager();
    private CPEManager mCPEManager;

    private CWMPCPEManager() {
        try {
            mCPEManager = CPEManager.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static CWMPCPEManager getInstance() {
        return instance;
    }

    public boolean init(Context context, RPCListener listener) {
        try {
            return mCPEManager.init(context, listener);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized boolean start(int tcpSrcPort, int udpSrcPort, int udpDestPort) {
        try {
            return mCPEManager.start(tcpSrcPort, udpSrcPort, udpDestPort);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized boolean stop() {
        try {
            return mCPEManager.stop();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public InternetGatewayDevice getRoot() {
        try {
            return mCPEManager.getRoot();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean sendInform(TTBBInform.Command command) {
        try {
            return mCPEManager.sendInform(command);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean sendInform(TTBBInform.Command command, InformCallback callback) {
        try {
            return mCPEManager.sendInform(command, callback);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void addEtcError(String code, String desc) {
        try {
            mCPEManager.addEtcError(code, desc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getVersion() {
        try {
            return mCPEManager.getVersion();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getSpecVersion() {
        try {
            return mCPEManager.getSpecVersion();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public Context getContext() {
        try {
            return mCPEManager.getContext();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isStarted() {
        try {
            return mCPEManager.isStarted();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public CWMPParameter getParameter(String name, ModelObject object) {
        try {
            return mCPEManager.getParameter(name, object);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setAcsNetworkListener(AcsNetworkListener listener) {
        try {
            mCPEManager.setAcsNetworkListener(listener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
