/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.packages.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.leanback.widget.Presenter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;
import java.util.Iterator;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;

public class PackagePresenter extends Presenter {

    private HashMap<String, Integer> flagMap = new HashMap<>();

    public PackagePresenter() {
        flagMap.put(Content.FLAG_EVENT, R.id.flagEvent);
        flagMap.put(Content.FLAG_HOT, R.id.flagHot);
        flagMap.put(Content.FLAG_NEW, R.id.flagNew);
        flagMap.put(Content.FLAG_PREMIUM, R.id.flagPremium);
        flagMap.put(Content.FLAG_UHD, R.id.flagUhd);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_package, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        Content content = (Content) item;

        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData(vh.view, content);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public class ViewHolder extends Presenter.ViewHolder {
        private ImageView poster;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            poster = view.findViewById(R.id.mainImage);
        }

        public void setData(View view, Content content) {
            initFlag(content, view);

            Glide.with(view.getContext())
                    .load(content.getPosterSourceUrl(Content.mPosterDefaultRect.width(),
                            Content.mPosterDefaultRect.height())).diskCacheStrategy(DiskCacheStrategy.DATA)
                    .centerCrop()
                    .placeholder(R.drawable.poster_default)
                    .error(R.drawable.poster_default)
                    .into(poster);
        }

        private void initFlag(Content work, View view) {
            if (work.getEventFlag() == null) {
                removeFlag(view);
                return;
            }
            Iterator<String> keyIterator = flagMap.keySet().iterator();
            while (keyIterator.hasNext()) {
                String key = keyIterator.next();
                boolean hasFlag = work.getEventFlag().contains(key);
                View flagView = view.findViewById(flagMap.get(key));
                flagView.setVisibility(hasFlag ? View.VISIBLE : View.GONE);
            }

            View lockIcon = view.findViewById(R.id.iconVodLock);
            if (lockIcon != null) {
                boolean isLocked = PlaybackUtil.isVodOverRating(work.getRating());
                lockIcon.setVisibility(isLocked ? View.VISIBLE : View.GONE);
            }
        }


        private void removeFlag(View view) {
            Iterator<String> keyIterator = flagMap.keySet().iterator();
            while (keyIterator.hasNext()) {
                String key = keyIterator.next();
                View flagView = view.findViewById(flagMap.get(key));
                flagView.setVisibility(View.GONE);
            }

            View lockIcon = view.findViewById(R.id.iconVodLock);
            if (lockIcon != null) {
                lockIcon.setVisibility(View.GONE);
            }
        }
    }
}
