/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import kr.altimedia.launcher.jasmin.R;

public class VodTrailerPosterView extends RelativeLayout {
    private RelativeLayout layout;
    private ImageView poster;
    private ImageView trailerIcon;
    private TextView trailerText;
    private ImageView bottomShadow;
    private LinearLayout trailerView;

    private int defaultImage = R.drawable.poster_default;
    private int blockImage = R.drawable.poster_block;

    public VodTrailerPosterView(Context context) {
        super(context);
        initView();
    }

    public VodTrailerPosterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);
    }

    public VodTrailerPosterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        getAttrs(attrs, defStyleAttr);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TrailerVodPosterViewThem);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TrailerVodPosterViewThem, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        int trailer_resID = typedArray.getResourceId(R.styleable.TrailerVodPosterViewThem_trailer_icon, R.drawable.icon_trailer);
        int bg_drawable_resID = typedArray.getResourceId(R.styleable.TrailerVodPosterViewThem_trailer_bg_drawable, R.drawable.selector_poster_focus);

        trailerIcon.setBackgroundResource(trailer_resID);
        layout.setBackgroundResource(bg_drawable_resID);

        typedArray.recycle();
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.common_trailer_poster_view, this, false);
        addView(v);

        setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);

        layout = v.findViewById(R.id.layout);
        poster = v.findViewById(R.id.poster);
        trailerIcon = v.findViewById(R.id.trailer_icon);
        trailerText = v.findViewById(R.id.trailer_text);
        bottomShadow = v.findViewById(R.id.bottom_shadow);
        trailerView = v.findViewById(R.id.trailer_view);
    }

    public void setPadding(int left, int top, int right, int bottom) {
        layout.setPadding(left, top, right, bottom);
    }

    public void setDefaultImage(int defaultImage) {
        this.defaultImage = defaultImage;
    }

    public void setBlockImage(int blockImage) {
        this.blockImage = blockImage;
    }

    public void setPoster(String url) {
        Glide.with(poster.getContext())
                .load(url).diskCacheStrategy(DiskCacheStrategy.DATA)
                .centerCrop()
                .placeholder(defaultImage)
                .error(defaultImage)
                .into(poster);
    }

    public void setPoster(Drawable drawable) {
        Glide.with(getContext())
                .load(drawable)
                .centerCrop()
                .placeholder(defaultImage)
                .error(defaultImage)
                .into(poster);
    }

    public void setTrailerText(String text) {
        trailerText.setText(text);
    }

    public void setBottomShadow(Drawable drawable) {
        bottomShadow.setBackground(drawable);
    }

    public void setBlock() {
        Drawable blockPoster = ContextCompat.getDrawable(getContext(), blockImage);
        setPoster(blockPoster);
    }

    public int getTrailerVisibility() {
        return trailerView.getVisibility();
    }

    public void setTrailerVisibility(int visibility) {
        trailerView.setVisibility(visibility);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        layout.setOnClickListener(l);
    }
}
