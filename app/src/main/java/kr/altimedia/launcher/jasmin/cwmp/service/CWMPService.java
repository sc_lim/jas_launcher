/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;

import com.altimedia.util.Log;

import androidx.annotation.Nullable;

/**
 * CWMPClient
 */
public class CWMPService extends Service {
    private final String TAG = CWMPService.class.getSimpleName();

    public static final String KEY_TARGET_SIGNAL = "KEY_TARGET_SIGNAL";
    public static final String KEY_SAID = "KEY_SAID";
    public static final String KEY_REGISTRATION_SERVER = "KEY_REGISTRATION_SERVER";
    public static final String KEY_LAUNCHER_VERSION = "KEY_LAUNCHER_VERSION";

    public static final int TYPE_LIVE = 0;
    public static final int TYPE_TESTBED = 1;
    public static final int TYPE_KT_MOKDONG = 2;

    @Override
    public void onDestroy() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onDestroy");
        }
        // stop cwmp
        CWMPController.removeInstance();

        super.onDestroy();
    }

    @Override
    public void onCreate() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreate");
        }
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBind: intent=" + intent);
        }

        initialize(intent);

        return new CWMPServiceBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onStartCommand: intent=" + intent + ", flags=" + flags + ", startId=" + startId);
        }

        initialize(intent);

        return super.onStartCommand(intent, flags, startId);
    }

    private void initialize(Intent intent) {
        int targetSignal = 0;
        String said = "";
        String registrationServer = "";
        String launcherVersion = "Not Supported";

        if (Log.INCLUDE) {
            Log.d(TAG, "initialize: intent=" + intent);
        }
        if (intent != null) {
            targetSignal = intent.getIntExtra(KEY_TARGET_SIGNAL, 0);
            said = intent.getStringExtra(KEY_SAID);
            registrationServer = intent.getStringExtra(KEY_REGISTRATION_SERVER);
            launcherVersion = intent.getStringExtra(KEY_LAUNCHER_VERSION);
        }
        CWMPController.getInstance().init(this, targetSignal, said, registrationServer, launcherVersion);
        CWMPController.getInstance().start();
    }

    public long getNormalPeriod() throws RemoteException {
        return CWMPController.getInstance().getNormalPeriod();
    }

    public void requestDefaultSetting(CWMPCallback cwmpCallback) throws RemoteException {
        CWMPController.getInstance().requestDefaultSetting(cwmpCallback);
    }

    public void onHotKeyClicked(int keyCode) throws RemoteException{
        CWMPController.getInstance().onHotKeyClicked(keyCode);
    }

    public void notifyBooting(boolean authorized, String said, String description) throws RemoteException {
        CWMPController.getInstance().notifyBooting(authorized, said, description);
    }

    public void notifyPowerState(boolean standby, String reason) throws RemoteException {
        CWMPController.getInstance().notifyPowerState(standby, reason);
    }

    public void notifyEtcError(String errorCode, String errorDesc, long errorTime) throws RemoteException {
        CWMPController.getInstance().notifyEtcError(errorCode, errorDesc, errorTime);
    }

    public void notifyWatchAuthority() throws RemoteException {
        CWMPController.getInstance().notifyWatchAuthority();
    }

    public void registerCWMPEventListener(CWMPEventListener listener) throws RemoteException {
        CWMPController.getInstance().registerCWMPEventListener(listener);
    }

    public void unregisterCWMPEventListener(CWMPEventListener listener) throws RemoteException {
        CWMPController.getInstance().unregisterCWMPEventListener(listener);
    }

    public class CWMPServiceBinder extends Binder {
        public CWMPService getService() {
            return CWMPService.this;
        }
    }
}
