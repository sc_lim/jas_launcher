/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.register;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public class CouponRegistrationDialog extends SafeDismissDialogFragment implements View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = CouponRegistrationDialog.class.getName();
    private final String TAG = CouponRegistrationDialog.class.getSimpleName();

    private View rootView;
    private CouponRegisterListener couponRegisterListener;

    private CouponRegistrationDialog() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    public static CouponRegistrationDialog newInstance() {
        CouponRegistrationDialog fragment = new CouponRegistrationDialog();
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_coupon_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        showRegisterFragment();
    }

    private void showRegisterFragment() {
        CouponRegistrationFragment fragment = CouponRegistrationFragment.newInstance();
        attachFragment(fragment, CouponRegistrationFragment.CLASS_NAME);
    }

    public void showRegisterSuccessFragment(Coupon coupon) {
        CouponRegistrationCompleteFragment fragment = CouponRegistrationCompleteFragment.newInstance(coupon);
        attachFragment(fragment, CouponRegistrationCompleteFragment.CLASS_NAME);
    }

    private void attachFragment(Fragment fragment, String tag) {
        getChildFragmentManager().beginTransaction()
                .replace(R.id.frame_layout, fragment, tag).commit();
    }

    public CouponRegisterListener getCouponRegisterListener(){
        return couponRegisterListener;
    }

    public void setCouponRegisterListener(CouponRegisterListener listener) {
        this.couponRegisterListener = listener;
    }

    public interface CouponRegisterListener {
        void onSuccess(Coupon coupon);
        void onFailed();
    }
}
