/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.message.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class Message implements Parcelable {
    @SerializedName("messageId")
    @Expose
    private String id;
    @SerializedName("messageTitle")
    @Expose
    private String title;
    @SerializedName("messageDescription")
    @Expose
    private String description;
    @SerializedName("messageDate")
    @Expose
    private Date date;
    @SerializedName("viewYN")
    @Expose
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isView;

    public Message() {
    }

    public Message(String id, String title, String description, long date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = new Date(date);
        this.isView = false;
    }

    protected Message(Parcel in) {
        readFromParcel(in);
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public long getDate() {
        return date != null ? date.getTime() : 0;
    }

    public boolean isView() {
        return isView;
    }

    public void setView(boolean isView) {
        this.isView = isView;
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeValue(description);
        dest.writeValue(date);
        dest.writeValue(isView);
    }

    private void readFromParcel(Parcel in) {
        id = (String) in.readValue(String.class.getClassLoader());
        title = ((String) in.readValue((String.class.getClassLoader())));
        description = ((String) in.readValue((String.class.getClassLoader())));
        date = ((Date) in.readValue((Date.class.getClassLoader())));
        isView = (boolean) in.readValue(Boolean.class.getClassLoader());
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", isView=" + isView +
                '}';
    }
}
