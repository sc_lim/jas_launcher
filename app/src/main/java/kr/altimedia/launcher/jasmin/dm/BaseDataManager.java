/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm;

import android.util.Base64;

import java.util.UUID;

import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import okhttp3.OkHttpClient;

/**
 * Created by mc.kim on 10,06,2020
 */
public abstract class BaseDataManager {
    protected final OkHttpClient mOkHttpClient;

    public BaseDataManager() {
        OkHttpClient mOkHttpClient = OkHttpGenerator.getUnsafeOkHttpClient(
                getSystemPassword(), getLoginToken(), getTransactionId(), getFcmToken(), getLanguageType(), getUUID());
        this.mOkHttpClient = mOkHttpClient;
        initializeManager(mOkHttpClient);
    }

    public abstract void initializeManager(OkHttpClient mOkHttpClient);

    public String getSystemPassword() {
        String systemPassword = "STB" + ":" + ServerConfig.SYSTEM_PASSWORD;
        return Base64.encodeToString(systemPassword.getBytes(), Base64.NO_WRAP);
    }

    public String getLoginToken() {
        return "";
    }

    public String getTransactionId() {
        return UUID.randomUUID().toString();
    }

    public String getFcmToken() {
        return AccountManager.getInstance().getFcmToken();
    }

    public String getLanguageType() {
        return AccountManager.getInstance().getLocalLanguage();
    }

    private String getUUID() {
        return AccountManager.getInstance().getAndroidId();
    }
}
