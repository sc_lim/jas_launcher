/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.indicator;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;

public class ImageIndicator extends RelativeLayout {
    private final String TAG = ImageIndicator.class.getSimpleName();
    public static final int TYPE_INDICATOR_TOP = 0;
    public static final int TYPE_INDICATOR_BOTTOM = 1;
    public static final int TYPE_INDICATOR_LEFT = 2;
    public static final int TYPE_INDICATOR_RIGHT = 3;

    public static final int TYPE_SCROLL_CENTER = 0;
    public static final int TYPE_SCROLL_EDGE = 1;
    public static final int TYPE_SCROLL_PAGING = 2;

    private int indicatorType = -1;
    private int scrollType = -1;

    private int listSize = 0;
    private int visibleCount = 0;
    private int centerIndex = 0;

    private ImageView arrow;

    public ImageIndicator(Context context) {
        super(context, null);
        initView();
    }

    public ImageIndicator(Context context, AttributeSet attrs) {
        super(context, attrs, -1);
        initView();
        getAttrs(attrs);
    }

    public ImageIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr, -1);
        initView();
        getAttrs(attrs, defStyleAttr);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ImageIndicatorTheme);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ImageIndicatorTheme, defStyleAttr, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        indicatorType = typedArray.getInt(R.styleable.ImageIndicatorTheme_indicator_type, TYPE_INDICATOR_LEFT);
        scrollType = typedArray.getInt(R.styleable.ImageIndicatorTheme_indicator_scroll_type, TYPE_SCROLL_CENTER);

        int visibility = isTopIndicator() ? View.INVISIBLE : View.VISIBLE;
        arrow.setVisibility(visibility);

        int thumb_resID = typedArray.getResourceId(R.styleable.ImageIndicatorTheme_indicator_drawable, R.drawable.arrow_epg_l);
        arrow.setBackgroundResource(thumb_resID);

        typedArray.recycle();
    }

    private void initView() {
        arrow = new ImageView(getContext());
        addView(arrow);
    }

    public void initIndicator(int size, int visibleCount) {
        this.listSize = size;
        this.visibleCount = visibleCount;
        this.centerIndex = visibleCount / 2;

        if (!isTopIndicator()) {
            int visibility = isSinglePage() ? View.GONE : View.VISIBLE;
            arrow.setVisibility(visibility);
        }
    }

    public void updateArrow(int position) {
        if (isSinglePage()) {
            return;
        }

        boolean visible = false;

        switch (indicatorType) {
            case TYPE_INDICATOR_TOP:
            case TYPE_INDICATOR_LEFT:
                visible = isMorePrev(position);
                break;
            case TYPE_INDICATOR_BOTTOM:
            case TYPE_INDICATOR_RIGHT:
                visible = isMoreNext(position);
                break;
        }

        int arrowVisibility = arrow.getVisibility();
        int visibility = visible ? View.VISIBLE : View.INVISIBLE;

        if(Log.INCLUDE){
            Log.d(TAG,"updateArrow : "+position+", arrowVisibility : "+arrowVisibility+", visibility : "+visibility);
        }
        if (arrowVisibility != visibility) {
            arrow.setVisibility(visibility);
        }

    }

    private boolean isMorePrev(int position) {
        if (scrollType == TYPE_SCROLL_CENTER) {
            return (centerIndex + 1) <= position;
        } else if (scrollType == TYPE_SCROLL_EDGE) {
            return position >= visibleCount;
        } else if (scrollType == TYPE_SCROLL_PAGING) {
            return position / visibleCount > 0;
        }

        return false;
    }

    public void setArrowVisible(int visible){
        arrow.setVisibility(visible);
    }

    private boolean isMoreNext(int position) {
        if (scrollType == TYPE_SCROLL_CENTER) {
            return ((listSize - 1) - position) > centerIndex;
        } else if (scrollType == TYPE_SCROLL_EDGE) {
            return position < (listSize - 1);
        }else if(scrollType == TYPE_SCROLL_PAGING){
            int totalPage = ((listSize - 1) / visibleCount) + 1;
            int currentPage = (position / visibleCount) + 1;
            return totalPage > currentPage;
//            int remainSize = listSize - position - 1;
//            return remainSize / visibleCount > 0;
        }

        return false;
    }

    private boolean isSinglePage() {
        return visibleCount >= listSize;
    }

    private boolean isTopIndicator() {
        return (indicatorType == TYPE_INDICATOR_TOP || indicatorType == TYPE_INDICATOR_LEFT);
    }
}
