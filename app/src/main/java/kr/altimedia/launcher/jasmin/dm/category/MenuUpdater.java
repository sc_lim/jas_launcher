package kr.altimedia.launcher.jasmin.dm.category;

import android.content.Context;
import android.os.AsyncTask;

import com.altimedia.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.dm.contents.ContentDataManager;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.def.CategoryType;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.HomeVerticalFragment;

/**
 * Created by mc.kim on 11,09,2020
 */
public class MenuUpdater implements Runnable {
    private final String TAG = MenuUpdater.class.getSimpleName();
    private final Context context;
    private final AccountManager mAccountManager;
    private List<WeakReference<AsyncTask>> taskList = new ArrayList<>();
    private MenuManager.OnMenuUpdateCallback mOnMenuUpdateCallback;

    public MenuUpdater(Context context, MenuManager.OnMenuUpdateCallback onMenuUpdateCallback) {
        this.context = context;
        this.mAccountManager = AccountManager.getInstance();
        this.mOnMenuUpdateCallback = onMenuUpdateCallback;
    }

    public void dispose() {
        this.mOnMenuUpdateCallback = null;
        clearPreviousTask();
    }

    private void clearPreviousTask() {
        if (taskList != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "taskList size : " + taskList.size());
            }
            for (WeakReference<AsyncTask> task : taskList) {
                if (task.get() != null) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "canceled");
                    }
                    task.get().cancel(true);
                }
            }
            taskList.clear();
        }
    }

    @Override
    public void run() {

        if (Log.INCLUDE) {
            Log.d(TAG, "run menu update check");
        }
        clearPreviousTask();
        MenuManager.getInstance().updateMenu(context, new MenuManager.OnMenuUpdateCallback() {

            @Override
            public void onMenuMenuUpdated(List<Category> result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onMenuMenuUpdated");
                }
                if (mOnMenuUpdateCallback != null) {
                    mOnMenuUpdateCallback.onMenuMenuUpdated(result);
                }
            }

            @Override
            public void onCategoryUpdated(List<Category> needUpdateCategoryList) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onCategoryUpdated : " + needUpdateCategoryList.size());
                }
//                if (version.equalsIgnoreCase("-1")) {
//                    if (Log.INCLUDE) {
//                        Log.d(TAG, "version return -1 it means not having version at ");
//                    }
//                    version = category.getCategoryVersion();
//                }

                if (Log.INCLUDE) {
                    for (Category category : needUpdateCategoryList) {
                        Log.d(TAG, "onCategoryUpdated : " + category);
                    }
                }

                if (mOnMenuUpdateCallback != null) {
                    mOnMenuUpdateCallback.onCategoryUpdated(needUpdateCategoryList);
                }
                for (Category category : needUpdateCategoryList) {
                    boolean categoryUpdated = MenuManager.getInstance().updateRootMenu(context, category, category.getCategoryVersion());
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onCategoryUpdated | categoryUpdated :" + categoryUpdated);
                    }
                    if (category.getLinkType() != CategoryType.SubCategory) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onCategoryUpdated | categoryUpdated :" + category.getLinkType() + ", so return");
                        }
                        return;
                    }
                    MenuManager.getInstance().loadSubCategory(context, mAccountManager.getLocalLanguage(), mAccountManager.getSaId(), category.getCategoryID(),
                            category.getCategoryVersion(), 0, HomeVerticalFragment.LOAD_SIZE, new MbsDataProvider<String, List<Category>>() {
                                @Override
                                public void needLoading(boolean loading) {
                                }

                                @Override
                                public void onFailed(int key) {
                                    if (Log.INCLUDE) {
                                        Log.d(TAG, "onFailed : " + key);
                                    }
                                }

                                @Override
                                public void onSuccess(String id, List<Category> result) {
                                    for (Category category : result) {
                                        if (Log.INCLUDE) {
                                            Log.d(TAG, "onSuccess : Category " + category.toString());
                                        }
                                        switch (category.getLinkType()) {
                                            case Promotion:
                                                loadPromotion(category);
                                                break;
                                            case Banner:
                                                loadBanner(category);
                                                break;
                                            case ContentsList:
                                                loadContentsData(category);
                                                break;
                                        }
                                    }
                                }

                                @Override
                                public void onError(String errorCode, String message) {
                                    if (Log.INCLUDE) {
                                        Log.d(TAG, "onError category : " + errorCode + ", message : " + message);
                                    }
                                }

                                @Override
                                public Context getTaskContext() {
                                    return context;
                                }
                            });
                }

            }

            @Override
            public void onContentsCategoryUpdated(String categoryId, String version) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onContentsCategoryUpdated : " + categoryId + ", version : " + version);
                }
                mOnMenuUpdateCallback.onContentsCategoryUpdated(categoryId, version);
            }

            @Override
            public void onFailed() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onFailed");
                }
                if (mOnMenuUpdateCallback != null) {
                    mOnMenuUpdateCallback.onFailed();
                }
            }
        });
    }


    private void loadPromotion(Category category) {
        ContentDataManager contentDataManager = new ContentDataManager();
        AsyncTask task = contentDataManager.getPromotionList(context, mAccountManager.getLocalLanguage(), mAccountManager.getSaId(),
                category.getLinkInfo(), category.getCategoryID(), category.getCategoryVersion(), new MbsDataProvider<String, List<Banner>>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed banner : " + key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<Banner> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "promotion response : " + result.size());
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError banner : " + errorCode + ", message : " + message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });
        taskList.add(new WeakReference<>(task));
    }

    private void loadBanner(Category category) {
        ContentDataManager contentDataManager = new ContentDataManager();
        AsyncTask task = contentDataManager.getImageBannerList(context, mAccountManager.getLocalLanguage(), mAccountManager.getSaId(),
                category.getLinkInfo(), category.getCategoryID(), category.getCategoryVersion(), new MbsDataProvider<String, List<Banner>>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed banner : " + key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<Banner> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "banner response : " + result.size());
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError banner : " + errorCode + ", message : " + message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });
        taskList.add(new WeakReference<>(task));
    }


    private void loadContentsData(final Category category) {
        ContentDataManager contentDataManager = new ContentDataManager();
        AsyncTask task = contentDataManager.getContentsList(context, mAccountManager.getLocalLanguage(), mAccountManager.getSaId(),
                mAccountManager.getProfileId(), category.getCategoryID(), category.getCategoryVersion(),
                0, 100, category.getContentBannerFlag(), new MbsDataProvider<String, List<Content>>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, " onFailed : " + key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<Content> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadContentsData response | id : " + result.size());
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });
        taskList.add(new WeakReference<>(task));
    }

}
