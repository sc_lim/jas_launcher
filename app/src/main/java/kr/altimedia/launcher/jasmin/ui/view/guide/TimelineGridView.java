/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.HorizontalGridView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TimelineGridView extends HorizontalGridView {
    public TimelineGridView(Context context) {
        this(context, null);
    }

    public TimelineGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TimelineGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setLayoutManager(
                new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false) {
                    @Override
                    public boolean onRequestChildFocus(
                            RecyclerView parent, State state, View child, View focused) {
                        // This disables the default scroll behavior for focus movement.
                        return true;
                    }

                    @Override
                    public void onLayoutChildren(Recycler recycler, State state) {
                        super.onLayoutChildren(recycler, state);
                    }

                    @Nullable
                    @Override
                    public View onInterceptFocusSearch(@NonNull View focused, int direction) {
                        return super.onInterceptFocusSearch(focused, direction);
                    }
                });

        // RecyclerView is always focusable, however this is not desirable for us, so disable.
        // See b/18863217 (ag/634046) for reasons to why RecyclerView is focusable.
        setFocusable(false);
        setFocusableInTouchMode(false);
        // Don't cache anything that is off screen. Normally it is good to prefetch and prepopulate
        // off screen views in order to reduce jank, however the program guide is capable to scroll
        // in all four directions so not only would we prefetch views in the scrolling direction
        // but also keep views in the perpendicular direction up to date.
        // E.g. when scrolling horizontally we would have to update rows above and below the current
        // view port even though they are not visible.
        setItemViewCacheSize(0);
    }
}
