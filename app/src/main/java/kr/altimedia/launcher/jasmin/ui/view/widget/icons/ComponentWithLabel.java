/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget.icons;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.UserHandle;

/**
 * Created by mc.kim on 23,12,2019
 */
public interface ComponentWithLabel {
    ComponentName getComponent();

    UserHandle getUser();

    CharSequence getLabel(PackageManager pm);

    class ComponentCachingLogic implements CachingLogic<ComponentWithLabel> {
        private final PackageManager mPackageManager;

        public ComponentCachingLogic(Context context) {
            mPackageManager = context.getPackageManager();
        }

        @Override
        public ComponentName getComponent(ComponentWithLabel object) {
            return object.getComponent();
        }

        @Override
        public UserHandle getUser(ComponentWithLabel object) {
            return object.getUser();
        }

        @Override
        public CharSequence getLabel(ComponentWithLabel object) {
            return object.getLabel(mPackageManager);
        }

        @Override
        public void loadIcon(Context context,
                             ComponentWithLabel object, BitmapInfo target) {
            // Do not load icon.
            target.icon = BitmapInfo.LOW_RES_ICON;
        }
    }
}
