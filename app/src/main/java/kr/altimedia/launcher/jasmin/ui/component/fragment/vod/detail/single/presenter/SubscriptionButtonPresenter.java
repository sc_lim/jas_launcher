package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.single.presenter;

import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;

public class SubscriptionButtonPresenter extends Presenter {

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_subscription_button, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((ButtonItem) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private final float DEFAULT_ALPHA = 1.0f;
        private final float DIM_ALPHA = 0.5f;

        private CheckBox checkBox;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            checkBox = view.findViewById(R.id.check_box);
        }

        public void setData(ButtonItem buttonItem) {
            if (buttonItem.isCheckable()) {
                Drawable drawableLeft = view.getContext().getDrawable(R.drawable.selector_channel_check_button);
                drawableLeft.setBounds(0, 0, drawableLeft.getIntrinsicWidth(), drawableLeft.getIntrinsicHeight());
                checkBox.setCompoundDrawables(drawableLeft, null, null, null);
                checkBox.setCompoundDrawablePadding(6);
                checkBox.setPadding(50, 0, 0, 0);
            } else {
                checkBox.setGravity(Gravity.CENTER);
            }

            checkBox.setText(buttonItem.getTitle());
            checkBox.setChecked(buttonItem.isChecked());
            checkBox.setEnabled(buttonItem.isEnabled());

            float alpha = buttonItem.isEnabled() ? DEFAULT_ALPHA : DIM_ALPHA;
            checkBox.setAlpha(alpha);
        }
    }


    public static class ButtonItem {
        private int title;
        private boolean isCheckable;
        private boolean isChecked;
        private boolean isEnabled;

        public ButtonItem(int title, boolean isCheckable, boolean isEnabled) {
            this.title = title;
            this.isCheckable = isCheckable;
            this.isChecked = false;
            this.isEnabled = isEnabled;
        }

        public int getTitle() {
            return title;
        }

        public boolean isCheckable() {
            return isCheckable;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean isChecked) {
            this.isChecked = isChecked;
        }

        public boolean isEnabled() {
            return isEnabled;
        }

        public void setEnabled(boolean isEnabled) {
            this.isEnabled = isEnabled;
        }
    }
}
