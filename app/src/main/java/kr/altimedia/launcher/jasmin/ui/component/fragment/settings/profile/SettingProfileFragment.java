/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileList;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SettingProfilePresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;

public class SettingProfileFragment extends SettingBaseFragment
        implements SettingProfileItemBridgeAdapter.OnClickProfileListener {
    public static final String CLASS_NAME = SettingProfileFragment.class.getName();
    private final String TAG = SettingProfileFragment.class.getSimpleName();

    private final int VISIBLE_COUNT = 5;
    private PagingVerticalGridView profileGridView;

    private MbsDataTask profileTask;

    private SettingProfileFragment() {
    }

    public static SettingProfileFragment newInstance() {
        return new SettingProfileFragment();
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_setting_profile;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        initGridView(view);
        loadProfileList(true);
    }

    private void initGridView(View view) {
        profileGridView = getView().findViewById(R.id.profile_grid_view);
        profileGridView.initVertical(VISIBLE_COUNT);
        int spacing = (int) getResources().getDimension(R.dimen.setting_profile_vertical_gap);
        profileGridView.setVerticalSpacing(spacing);
        profileGridView.setNumColumns(1);
    }

    private void loadProfileList(boolean isInit) {
        SettingTaskManager settingTaskManager = onFragmentChange.getSettingTaskManager();
        profileTask = settingTaskManager.getProfileList(
                new MbsDataProvider<String, ProfileList>() {
                    @Override
                    public void needLoading(boolean loading) {
                        onFragmentChange.showProgressbar(loading);
                        profileGridView.setFocusable(!loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfileList, onFailed");
                        }

                        setBlockFocus();
                        onFragmentChange.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, ProfileList result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfileList, onSuccess, result : " + result);
                        }

                        if (result == null) {
                            return;
                        }

                        ArrayList<ProfileInfo> list = (ArrayList<ProfileInfo>) result.getProfileInfoList();
                        if (isInit) {
                            setProfileGridView(list);
                        } else {
                            updateList(list);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfileList, onError");
                        }

                        setBlockFocus();
                        onFragmentChange.getSettingTaskManager().showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingProfileFragment.this.getContext();
                    }
                });
    }

    private void setBlockFocus() {
        profileGridView.setFocusable(false);
        profileGridView.setFocusableInTouchMode(false);
        profileGridView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
    }

    private void setProfileGridView(ArrayList<ProfileInfo> list) {
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(new SettingProfilePresenter());
        objectAdapter.addAll(0, list);
        SettingProfileItemBridgeAdapter settingProfileItemBridgeAdapter = new SettingProfileItemBridgeAdapter(objectAdapter, VISIBLE_COUNT);
        settingProfileItemBridgeAdapter.setOnClickProfileListener(this);
        profileGridView.setAdapter(settingProfileItemBridgeAdapter);

        initScrollbar(list.size());
    }

    private void initScrollbar(int size) {
        ThumbScrollbar scrollbar = getView().findViewById(R.id.scrollbar);
        scrollbar.setList(size, VISIBLE_COUNT);

        profileGridView.addOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {
            @Override
            public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                super.onChildViewHolderSelected(parent, child, position, subposition);
                scrollbar.moveFocus(position);
            }
        });
        scrollbar.setFocusable(false);
    }

    private void updateProfileInfo(ProfileInfo profileInfo) {
        SettingProfileItemBridgeAdapter bridgeAdapter = (SettingProfileItemBridgeAdapter) profileGridView.getAdapter();
        ArrayObjectAdapter adapter = (ArrayObjectAdapter) bridgeAdapter.getAdapter();
        int index = adapter.indexOf(profileInfo);
        if (Log.INCLUDE) {
            Log.d(TAG, "updateProfileInfo, index : " + index + ", profileInfo : " + profileInfo);
        }

        adapter.replace(index, profileInfo);
    }

    private void updateList(ArrayList<ProfileInfo> list) {
        int selectedPosition = profileGridView.getChildPosition(profileGridView.getFocusedChild());
        profileGridView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);

        SettingProfileItemBridgeAdapter bridgeAdapter = (SettingProfileItemBridgeAdapter) profileGridView.getAdapter();
        ArrayObjectAdapter adapter = (ArrayObjectAdapter) bridgeAdapter.getAdapter();
        adapter.clear();
        adapter.addAll(0, list);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                profileGridView.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
                profileGridView.scrollToPosition(selectedPosition);
            }
        }, 150);
    }

    @Override
    public void onDestroyView() {
        onFragmentChange.showProgressbar(false);

        super.onDestroyView();

        if (profileTask != null) {
            profileTask.cancel(true);
        }
    }

    @Override
    public void onClickProfile(ProfileInfo profileInfo) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickProfile, profileInfo : " + profileInfo);
        }

        SettingProfileDialogFragment settingProfileDialogFragment = SettingProfileDialogFragment.newInstance(profileInfo);
        settingProfileDialogFragment.setOnProfileChangeListener(new SettingProfileDialogFragment.OnProfileChangeListener() {
            @Override
            public void onChangeProfile(PushMessageReceiver.ProfileUpdateType profileUpdateType, ProfileInfo profileInfo) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onChangeProfile, type : " + profileUpdateType + ", profileInfo : " + profileInfo);
                }

                if (profileUpdateType == PushMessageReceiver.ProfileUpdateType.lock) {
                    updateProfileInfo(profileInfo);
                }
            }

            @Override
            public void updateProfileList() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "updateProfileList");
                }

                loadProfileList(false);
            }
        });
        onFragmentChange.getTvOverlayManager().showDialogFragment(settingProfileDialogFragment);
    }
}
