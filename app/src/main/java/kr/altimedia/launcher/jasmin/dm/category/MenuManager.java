/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.category;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.dm.category.obj.response.CategoryResponse;
import kr.altimedia.launcher.jasmin.dm.category.obj.response.CategoryVersionCheckResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.def.CategoryType;
import kr.altimedia.launcher.jasmin.dm.def.CategoryTypeDeserializer;
import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.util.FileUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.ResourceLoader;

public class MenuManager {
    private final String TAG = MenuManager.class.getSimpleName();
    private static final MenuManager ourInstance = new MenuManager();

    public static MenuManager getInstance() {
        return ourInstance;
    }

    private final String FILENAME_ROOT_MENU = "rootMenu_" + AccountManager.getInstance().getLocalLanguage();
    private final String CATEGORY_FILE_LIST = "rootMenu_list_" + AccountManager.getInstance().getLocalLanguage();
    private final String FILENAME_CONTENTS_LIST = "contents_list_" + AccountManager.getInstance().getLocalLanguage();
    private final String FILENAME_BANNER_LIST = "banner_list_" + AccountManager.getInstance().getLocalLanguage();
    private final String SUBCATEGORY_FILE_LIST = "subCategory_list_" + AccountManager.getInstance().getLocalLanguage();
    private List<Category> mCurrentCategories = null;
    private HashMap<String, List<Category>> mCategoryMap = new HashMap<>();
//    private CategoryVersionCheckResponse previousVersionCheck;

    private MenuManager() {
    }

    public Category getCategoryInfo(String categoryId) {
        for (Category category : mCurrentCategories) {
            if (category.getCategoryID().equalsIgnoreCase(categoryId)) {
                return category;
            }
        }
        return null;
    }

    private CategoryResponse.CategoryResult getSavedList(Context context) {
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        String rootMenu = userPreferenceManagerImp.readString(FILENAME_ROOT_MENU);
        if (Log.INCLUDE) {
            Log.d(TAG, "loadFile : " + rootMenu);
        }
        if (StringUtils.nullToEmpty(rootMenu).isEmpty()) {
            return null;
        }
        CategoryResponse.CategoryResult categoryResponse = getGson().fromJson(rootMenu, CategoryResponse.CategoryResult.class);
        return categoryResponse;
    }

    private boolean saveCategoryList(Context context, String version, List<Category> categories,
                                     CategoryVersionCheckResponse categoryVersionCheckResponse, boolean update) {
        saveMainMenuAtUp(context, version, categories);
        return clearSubCategoryList(context, update, categoryVersionCheckResponse);
    }

    private boolean clearSubCategoryList(Context context, boolean update, CategoryVersionCheckResponse categoryVersionCheckResponse) {
        return clearSubCategoryList(context, update, categoryVersionCheckResponse, null);
    }


    private boolean clearCategoryContents(Context context,
                                          CategoryVersionCheckResponse categoryVersionCheckResponse, @Nullable OnMenuUpdateCallback callback) {
        boolean result = false;
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        Set<String> subListSet = userPreferenceManagerImp.getStringSet(SUBCATEGORY_FILE_LIST);
        Set<String> subListSetCopy = new HashSet<>();
        subListSetCopy.addAll(subListSet);
        Iterator<String> categoryFileName = subListSetCopy.iterator();
        while (categoryFileName.hasNext()) {
            String fileName = categoryFileName.next();
            if (Log.INCLUDE) {
                Log.d(TAG, "clearCategoryContents : " + fileName);
            }
            String[] fileSplitData = fileName.split("_");
            if (Log.INCLUDE) {
                Log.d(TAG, "clearCategoryContents | fileSplitData : " + fileSplitData);
            }
            if (fileSplitData.length != 4) {
                continue;
            }
            String categoryId = fileSplitData[3];
            if (Log.INCLUDE) {
                Log.d(TAG, "clearCategoryContents | categoryId : " + categoryId);
            }

            if (!categoryVersionCheckResponse.getResult().isContainCategory(categoryId)) {
                userPreferenceManagerImp.removeFile(fileName);
                userPreferenceManagerImp.removeStringSet(SUBCATEGORY_FILE_LIST, fileName);
                result = true;
                if (Log.INCLUDE) {
                    Log.d(TAG, "clearCategoryContents | not contain Category so clear : " + categoryId);
                }
                if (callback != null) {
                    callback.onContentsCategoryUpdated(categoryId, "-1");
                }
                continue;
            }

            String categoryVersion = categoryVersionCheckResponse.getResult().getCategoryVersion(categoryId);

            if (fileName.startsWith(FILENAME_CONTENTS_LIST)) {
                String contents = userPreferenceManagerImp.readString(fileName);
                if (StringUtils.nullToEmpty(contents).isEmpty()) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "clearCategoryContents StringUtils.nullToEmpty(contents).isEmpty() so return null");
                    }
                    continue;
                }
                ContentsListWithVersion contentsListWithVersion = new Gson().fromJson(contents, ContentsListWithVersion.class);
                if (!contentsListWithVersion.getVersion().equalsIgnoreCase(categoryVersion)) {
                    userPreferenceManagerImp.removeFile(fileName);
                    userPreferenceManagerImp.removeStringSet(SUBCATEGORY_FILE_LIST, fileName);
                    if (Log.INCLUDE) {
                        Log.d(TAG, "clearCategoryContents not same version so clear : " + fileName);
                    }
                    result = true;
                    if (callback != null) {
                        callback.onContentsCategoryUpdated(categoryId, categoryVersion);
                    }
                    continue;
                } else {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "clearCategoryContents  same version not so clear : " + fileName);
                    }
                }
            } else if (fileName.startsWith(FILENAME_BANNER_LIST)) {
                String contents = userPreferenceManagerImp.readString(fileName);
                if (StringUtils.nullToEmpty(contents).isEmpty()) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "clearCategoryContents StringUtils.nullToEmpty(contents).isEmpty() so return null");
                    }
                    continue;
                }
                BannerListWithVersion bannerListWithVersion = new Gson().fromJson(contents, BannerListWithVersion.class);
                if (!bannerListWithVersion.getVersion().equalsIgnoreCase(categoryVersion)) {
                    userPreferenceManagerImp.removeFile(fileName);
                    userPreferenceManagerImp.removeStringSet(SUBCATEGORY_FILE_LIST, fileName);
                    result = true;
                    if (Log.INCLUDE) {
                        Log.d(TAG, "clearCategoryContents not same version so clear : " + fileName);
                    }

                    if (callback != null) {
                        callback.onContentsCategoryUpdated(categoryId, categoryVersion);
                    }
                    continue;
                } else {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "clearCategoryContents  same version not so clear : " + fileName);
                    }
                }
            }
        }
        return result;
    }

    private boolean isEmpty(List item) {
        return item == null || item.isEmpty();
    }

    public void checkMainMenu(Context context, MbsDataProvider<String, Boolean> mbsDataProvider) {
        CategoryResponse.CategoryResult categories = getSavedList(context);
        boolean isSavedListExist = categories != null;
        String version = isSavedListExist ? categories.getCategoryVersion() : "-1";
        if (version.equalsIgnoreCase("-1")) {
            mbsDataProvider.onSuccess(version, true);
            return;
        }
        CategoryDataManager categoryDataManager = new CategoryDataManager();
        categoryDataManager.checkCategoryVersion(version, new MbsDataProvider<String, CategoryVersionCheckResponse>() {
            @Override
            public void needLoading(boolean loading) {
                mbsDataProvider.needLoading(loading);
            }

            @Override
            public void onFailed(int key) {
            }

            @Override
            public void onSuccess(String id, CategoryVersionCheckResponse result) {
                TvSingletons.getSingletons(context).getBackendKnobs().notifyMenuVersionChecked();
                mbsDataProvider.onSuccess(id, checkCategoryUpdate(context, result));
            }

            @Override
            public void onError(String errorCode, String message) {
            }

            @Override
            public Context getTaskContext() {
                return context;
            }
        });
    }

    private boolean checkCategoryUpdate(Context context, CategoryVersionCheckResponse result) {
        return clearSubCategoryList(context, result.getResult().isUpdateYN(), result);
    }

    public void saveCategoryBannerList(Context context, String categoryId, String bannerPositionId, String version, List<Banner> bannerList) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call saveCategoryContents");
        }
        String fileName = FILENAME_BANNER_LIST + "_" + categoryId;
        BannerListWithVersion mBannerListWithVersion = new BannerListWithVersion(version, categoryId, bannerPositionId, bannerList);
        String bannerListData = new Gson().toJson(mBannerListWithVersion);
        if (Log.INCLUDE) {
            Log.d(TAG, "call mBannerListWithVersion : " + mBannerListWithVersion.toString());
        }
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        userPreferenceManagerImp.writeString(fileName, bannerListData);
        userPreferenceManagerImp.addStringSet(SUBCATEGORY_FILE_LIST, fileName);
    }

    public List<Banner> getCategoryBannerList(Context context, String categoryId, String bannerPosition, String version) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call getCategoryBannerList");
        }
        String fileName = FILENAME_BANNER_LIST + "_" + categoryId;
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        String contents = userPreferenceManagerImp.readString(fileName);
        if (StringUtils.nullToEmpty(contents).isEmpty()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "call getCategoryBannerList StringUtils.nullToEmpty(contents).isEmpty() so return null");
            }
            return null;
        }
        BannerListWithVersion bannerListWithVersion = new Gson().fromJson(contents, BannerListWithVersion.class);
        if (bannerListWithVersion.getVersion().equalsIgnoreCase(version) && bannerListWithVersion.getBannerPositionId().equalsIgnoreCase(bannerPosition)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "call getCategoryBannerList same version return");
            }
            return bannerListWithVersion.getBannerList();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "call getCategoryBannerList dirty so return null");
        }
        userPreferenceManagerImp.removeFile(fileName);
        userPreferenceManagerImp.removeStringSet(SUBCATEGORY_FILE_LIST, fileName);
        return null;
    }

    public void saveCategoryContents(Context context, String categoryId, String version, List<Content> contentList) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call saveCategoryContents");
        }

        String fileName = FILENAME_CONTENTS_LIST + "_" + categoryId;
        ContentsListWithVersion mContentsListWithVersion = new ContentsListWithVersion(version, categoryId, contentList);
        String contentsListData = new Gson().toJson(mContentsListWithVersion);
        if (Log.INCLUDE) {
            Log.d(TAG, "call mContentsListWithVersion : " + mContentsListWithVersion.toString());
        }
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        userPreferenceManagerImp.writeString(fileName, contentsListData);
        userPreferenceManagerImp.addStringSet(SUBCATEGORY_FILE_LIST, fileName);
    }

    public List<Content> getCategoryContent(Context context, String categoryId, String version) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call getCategoryContent");
        }


        String fileName = FILENAME_CONTENTS_LIST + "_" + categoryId;
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        String contents = userPreferenceManagerImp.readString(fileName);
        if (StringUtils.nullToEmpty(contents).isEmpty()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "call getCategoryContent StringUtils.nullToEmpty(contents).isEmpty() so return null");
            }
            return null;
        }
        ContentsListWithVersion contentsListWithVersion = new Gson().fromJson(contents, ContentsListWithVersion.class);
        if (contentsListWithVersion.getVersion().equalsIgnoreCase(version)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "call getCategoryContent same version return");
            }
            return contentsListWithVersion.getContentList();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "call getCategoryContent dirty so return null");
        }
        userPreferenceManagerImp.removeFile(fileName);
        userPreferenceManagerImp.removeStringSet(SUBCATEGORY_FILE_LIST, fileName);
        return null;
    }

    public void updateMenu(Context context, OnMenuUpdateCallback updateCallback) {
        CategoryResponse.CategoryResult categories = getSavedList(context);
        boolean isSavedListExist = categories != null;
        String version = isSavedListExist ? categories.getCategoryVersion() : "0";
        List<Category> saveList = isSavedListExist ? categories.getCategoryList() : new ArrayList<>();

        if (isEmpty(saveList)) {
            updateCallback.onFailed();
            return;
        }

        CategoryDataManager categoryDataManager = new CategoryDataManager();
        categoryDataManager.checkCategoryVersion(version, new MbsDataProvider<String, CategoryVersionCheckResponse>() {
            @Override
            public void needLoading(boolean loading) {
            }

            @Override
            public void onFailed(int key) {
                updateCallback.onFailed();
            }

            @Override
            public void onSuccess(String id, CategoryVersionCheckResponse result) {

                TvSingletons.getSingletons(context).getBackendKnobs().notifyMenuVersionChecked();
                if (result.getResult().isUpdateYN()) {
                    updateMainMenu(context, result.getResult().getVersion(),
                            result.getResult().getRootCategoryId(), result, updateCallback);
                } else {
                    makeCategoryHomeByType(saveList, CategoryType.SubCategory);
                    updateCategoryList(context, id, saveList, result, false, new OnMenuUpdateCallback() {
                        @Override
                        public void onMenuMenuUpdated(List<Category> result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "wrong root menu check");
                            }
                        }

                        @Override
                        public void onCategoryUpdated(List<Category> updatedList) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onCategoryUpdated on main category updated so load mainMenu :" + updatedList.size());
                            }

                            if (Log.INCLUDE) {
                                for (Category category : updatedList) {
                                    Log.d(TAG, "onCategoryUpdated : " + category);
                                }
                            }
                            updateMainMenuWithNotifyCategoryUpdate(context, result.getResult().getVersion(),
                                    result.getResult().getRootCategoryId(), updatedList, updateCallback);
                        }

                        @Override
                        public void onContentsCategoryUpdated(String categoryId, String version) {
                            updateCallback.onContentsCategoryUpdated(categoryId, version);
                        }

                        @Override
                        public void onFailed() {
                            updateCallback.onFailed();
                        }
                    });
                }
            }

            @Override
            public void onError(String errorCode, String message) {
                updateCallback.onFailed();
            }

            @Override
            public Context getTaskContext() {
                return context;
            }
        });

    }


    public void getMainMenu(Context context, MbsDataProvider<String, List<Category>> mbsDataProvider) {
        CategoryResponse.CategoryResult categories = getSavedList(context);
        boolean isSavedListExist = categories != null;

        String version = isSavedListExist ? categories.getCategoryVersion() : "0";
        List<Category> saveList = isSavedListExist ? categories.getCategoryList() : new ArrayList<>();


        CategoryDataManager categoryDataManager = new CategoryDataManager();
        categoryDataManager.checkCategoryVersion(version, new MbsDataProvider<String, CategoryVersionCheckResponse>() {
            @Override
            public void needLoading(boolean loading) {
                mbsDataProvider.needLoading(loading);
            }

            @Override
            public void onFailed(int key) {
                getMenuFromErrorCase(context, saveList, mbsDataProvider);
            }

            @Override
            public void onSuccess(String id, CategoryVersionCheckResponse result) {
                TvSingletons.getSingletons(context).getBackendKnobs().notifyMenuVersionChecked();
                if (result.getResult().isUpdateYN() || isEmpty(saveList)) {
                    loadMainMenu(context, result.getResult().getVersion(),
                            result.getResult().getRootCategoryId(), result, mbsDataProvider);
                } else {
                    makeCategoryHomeByType(saveList, CategoryType.SubCategory);
                    boolean isClear = saveCategoryList(context, id, saveList, result, false);
                    if (isClear) {
                        loadMainMenu(context, result.getResult().getVersion(),
                                result.getResult().getRootCategoryId(), result, mbsDataProvider);
                    } else {
                        mbsDataProvider.onSuccess(id, saveList);
                    }
                }
            }

            @Override
            public void onError(String errorCode, String message) {
                mbsDataProvider.onError(errorCode, message);
                getMenuFromErrorCase(context, saveList, mbsDataProvider);
            }

            @Override
            public Context getTaskContext() {
                return context;
            }
        });
    }

    private void getMenuFromErrorCase(Context context, List<Category> saveList, MbsDataProvider<String, List<Category>> mbsDataProvider) {
        if (saveList == null || saveList.isEmpty()) {
            loadFromRawFile(context, "0", mbsDataProvider);
        } else {
            mbsDataProvider.onSuccess("0", saveList);
        }
    }

    private void loadFromRawFile(Context context, String rootCategoryId, MbsDataProvider<String, List<Category>> mbsDataProvider) {
        String mainMenuString = "";
        try {
            mainMenuString = FileUtil.getRawFile(context, R.raw.dummy_home_category);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Category> response = getGson().fromJson(mainMenuString, CategoryResponse.class).getCategoryList();
        if (Log.INCLUDE) {
            for (Category category : response) {
                Log.d(TAG, "response :  category : " + category);
            }
        }
        makeCategoryHomeByType(response, CategoryType.SubCategory);
        mbsDataProvider.onSuccess(rootCategoryId, response);
    }

    private void removeCategoryInfo(UserPreferenceManagerImp userPreferenceManagerImp, String fileName, String categoryId) {
        userPreferenceManagerImp.removeFile(fileName);
        userPreferenceManagerImp.removeStringSet(CATEGORY_FILE_LIST, fileName);
        userPreferenceManagerImp.removeFile(FILENAME_ROOT_MENU + "_" + categoryId);
        mCategoryMap.remove(categoryId);
    }

    private boolean clearSubCategoryList(Context context, boolean update,
                                         CategoryVersionCheckResponse categoryVersionCheckResponse, @Nullable OnMenuUpdateCallback callback) {


        boolean result = false;
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        Set<String> subListSet = userPreferenceManagerImp.getStringSet(CATEGORY_FILE_LIST);
        Set<String> checkListSet = new HashSet<>();
        checkListSet.addAll(subListSet);
        Iterator<String> categoryFileName = checkListSet.iterator();
        ArrayList<String> updateCategoryList = new ArrayList<>();

        if (mCurrentCategories != null) {
            for (Category category : mCurrentCategories) {
                updateCategoryList.add(category.getCategoryID());
            }
        }

        ArrayList<String> updatedContentsList = new ArrayList<>();
        while (categoryFileName.hasNext()) {

            String fileName = categoryFileName.next();
            if (Log.INCLUDE) {
                Log.d(TAG, "clearSubCategoryFile : " + fileName);
            }
            String[] fileSplitData = fileName.split("_");
            if (Log.INCLUDE) {
                Log.d(TAG, "clearSubCategoryFile | fileSplitData : " + fileSplitData);
            }
            if (fileSplitData.length != 3) {
                continue;
            }
            String categoryId = fileSplitData[2];
            if (Log.INCLUDE) {
                Log.d(TAG, "clearSubCategoryFile | categoryId : " + categoryId);
            }
            boolean isContainCategory = categoryVersionCheckResponse.getResult().isContainCategory(categoryId);
            if (update) {
                removeCategoryInfo(userPreferenceManagerImp, fileName, categoryId);
                result = true;
            } else {
                if (isContainCategory) {
                    String categoryVersion = categoryVersionCheckResponse.getResult().getCategoryVersion(categoryId);
                    String savedData = userPreferenceManagerImp.readString(fileName);
                    CategoryResponse.CategoryResult categoryResult = new Gson().fromJson(savedData, CategoryResponse.CategoryResult.class);
                    if (categoryResult != null && !StringUtils.nullToEmpty(categoryResult.getCategoryVersion()).equalsIgnoreCase(categoryVersion)) {
                        removeCategoryInfo(userPreferenceManagerImp, fileName, categoryId);
                        result = true;
                    } else {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "clearSubCategoryFile | removeCategoryList : " + categoryId);
                        }
                        updateCategoryList.remove(categoryId);
                    }
                } else {
                    removeCategoryInfo(userPreferenceManagerImp, fileName, categoryId);
                    result = true;
                }
            }
            boolean contentsRemoved = clearCategoryContents(context, categoryVersionCheckResponse, callback);
            if (contentsRemoved) {
                removeCategoryInfo(userPreferenceManagerImp, fileName, categoryId);
                if (!updateCategoryList.contains(categoryId)) {
                    updateCategoryList.add(categoryId);
                }

                if (!updatedContentsList.contains(categoryId)) {
                    updatedContentsList.add(categoryId);
                }

                result = true;
            }

        }

        if (mCurrentCategories != null && categoryVersionCheckResponse != null) {

            List<Category> needUpdateCategoryList = new ArrayList<>();
            for (Category category : mCurrentCategories) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "check update Category List : " + category.getCategoryID());
                }
                if (updateCategoryList.contains(category.getCategoryID())) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "check update Category List | result " + result);
                    }
                    if (categoryVersionCheckResponse.getResult().isContainCategory(category.getCategoryID())) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "check update Category List | isContainCategory");
                        }
                        String version = categoryVersionCheckResponse.getResult().getCategoryVersion(category.getCategoryID());
                        if (!category.getCategoryVersion().equalsIgnoreCase(version)) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "check update Category List not same so add category");
                            }
                            needUpdateCategoryList.add(category);
                        } else {
                            if (updatedContentsList.contains(category.getCategoryID())) {
                                needUpdateCategoryList.add(category);
                            }
                        }
                    } else {
                        needUpdateCategoryList.add(category);
                    }
                }
            }
            if (callback != null && !needUpdateCategoryList.isEmpty()) {
                callback.onCategoryUpdated(needUpdateCategoryList);
            }
            if (!needUpdateCategoryList.isEmpty()) {
                result = true;
            }
        }

        return result;
    }


    private void updateCategoryList(Context context, String version, List<Category> categories,
                                    CategoryVersionCheckResponse categoryVersionCheckResponse, boolean update, OnMenuUpdateCallback callback) {

        saveMainMenuAtUp(context, version, categories);
        clearSubCategoryList(context, update, categoryVersionCheckResponse, callback);
    }

    private void saveMainMenuAtUp(Context context, String version, List<Category> categories) {
        if (Log.INCLUDE) {
            Log.d(TAG, "saveMainMenuAtUp : " + version);
        }
        mCurrentCategories = categories;
        putAutoLoginAppList(categories);
        CategoryResponse.CategoryResult result = new CategoryResponse.CategoryResult(version, categories);
        String categoryListString = getGson().toJson(result);
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        userPreferenceManagerImp.writeString(FILENAME_ROOT_MENU, categoryListString);
    }

    private void putAutoLoginAppList(List<Category> categories) {
        List<String> packageIdList = new ArrayList<>();
        for (Category category : categories) {
            if (category.getLinkType() == CategoryType.AppLink) {
                packageIdList.add(category.getLinkInfo());
            }
        }
        AccountManager.getInstance().putData(AccountManager.KEY_AUTO_LOGIN_APP_LIST, packageIdList);
    }

    private void updateMainMenuWithNotifyCategoryUpdate(Context context, String version, String rootCategoryId,
                                                        List<Category> updateCategoryList,
                                                        OnMenuUpdateCallback callback) {
        CategoryDataManager categoryDataManager = new CategoryDataManager();
        categoryDataManager.getCategoryList(AccountManager.getInstance().getLocalLanguage(),
                AccountManager.getInstance().getSaId(), rootCategoryId, version, new MbsDataProvider<String, List<Category>>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        callback.onFailed();
                    }

                    @Override
                    public void onSuccess(String id, List<Category> categories) {

                        saveMainMenuAtUp(context, version, categories);
                        List<Category> filteredCategoryList = new ArrayList<>();
                        for (int i = 0; i < categories.size(); i++) {
                            Category checkCategory = categories.get(i);
                            for (Category updateCategory : updateCategoryList) {
                                if (updateCategory.getCategoryID().equalsIgnoreCase(checkCategory.getCategoryID())) {
                                    filteredCategoryList.add(checkCategory);
                                }
                            }
                        }
                        callback.onCategoryUpdated(filteredCategoryList);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        callback.onFailed();
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });
    }

    private void updateMainMenu(Context context, String version, String rootCategoryId,
                                CategoryVersionCheckResponse categoryVersionCheckResponse,
                                OnMenuUpdateCallback callback) {
        CategoryDataManager categoryDataManager = new CategoryDataManager();
        categoryDataManager.getCategoryList(AccountManager.getInstance().getLocalLanguage(),
                AccountManager.getInstance().getSaId(), rootCategoryId, version, new MbsDataProvider<String, List<Category>>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        callback.onFailed();
                    }

                    @Override
                    public void onSuccess(String id, List<Category> result) {

                        makeCategoryHomeByType(result, CategoryType.SubCategory);
                        callback.onMenuMenuUpdated(result);
                        updateCategoryList(context, id, result, categoryVersionCheckResponse, true, callback);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        callback.onFailed();
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });
    }

    public boolean updateRootMenu(Context context, Category updatedSubCategory, String version) {

        ArrayList<Category> currentCategory = new ArrayList<>(mCurrentCategories.size());
        currentCategory.addAll(mCurrentCategories);
        boolean isUpdated = false;
        for (Category category : mCurrentCategories) {
            if (category.getCategoryID().equalsIgnoreCase(updatedSubCategory.getCategoryID())) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "updateRootMenu requestedVersion : " + version);
                    Log.d(TAG, "updateRootMenu category.getCategoryVersion() : " + category.getCategoryVersion());
                }
                boolean sameVersion = category.getCategoryVersion().equalsIgnoreCase(version);
                if (Log.INCLUDE) {
                    Log.d(TAG, "updateRootMenu checkCategory : " + category.getCategoryID() + ", is Same : " + sameVersion);
                }
                if (!sameVersion) {

                    int index = currentCategory.indexOf(category);
                    currentCategory.remove(index);
                    updatedSubCategory.setVersion(version);
                    currentCategory.add(index, updatedSubCategory);
                    isUpdated = true;
                }
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "updateRootMenu : isUpdated : " + isUpdated);
        }
        if (isUpdated) {
            mCurrentCategories = currentCategory;
            putAutoLoginAppList(currentCategory);
            CategoryResponse.CategoryResult result =
                    new CategoryResponse.CategoryResult(getRootMenuVersion(context), mCurrentCategories);
            String categoryListString = getGson().toJson(result);

            if (Log.INCLUDE) {
                Log.d(TAG, "updateRootMenu : categoryListString : " + categoryListString);
            }

            UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
            userPreferenceManagerImp.writeString(FILENAME_ROOT_MENU, categoryListString);
        }

        return isUpdated;
    }

    private String getRootMenuVersion(Context context) {
        CategoryResponse.CategoryResult categories = getSavedList(context);
        boolean isSavedListExist = categories != null;
        String version = isSavedListExist ? categories.getCategoryVersion() : "0";
        return version;
    }
    private void loadMainMenu(Context context, String version, String rootCategoryId,
                              CategoryVersionCheckResponse categoryVersionCheckResponse,
                              MbsDataProvider<String, List<Category>> mbsDataProvider) {


        CategoryDataManager categoryDataManager = new CategoryDataManager();
        categoryDataManager.getCategoryList(AccountManager.getInstance().getLocalLanguage(),
                AccountManager.getInstance().getSaId(), rootCategoryId, version, new MbsDataProvider<String, List<Category>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        mbsDataProvider.needLoading(loading);
                    }

                    @Override
                    public void onFailed(int key) {
//                        mbsDataProvider.onFailed(key);
                        loadFromRawFile(context, "0", mbsDataProvider);
                    }

                    @Override
                    public void onSuccess(String id, List<Category> result) {

                        makeCategoryHomeByType(result, CategoryType.SubCategory);
                        saveCategoryList(context, id, result, categoryVersionCheckResponse, true);
                        mbsDataProvider.onSuccess(id, result);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
//                        mbsDataProvider.onError(errorCode, message);
                        loadFromRawFile(context, "0", mbsDataProvider);
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });
    }

    private void putSubCategory(Context context, String id, String version, List<Category> result, boolean update) {
        mCategoryMap.put(id, result);
        if (update) {
            saveSubCategoryList(context, version, id, result);
        }
    }

    public void loadSubCategory(Context context, String language, String said, String categoryId, String version,
                                int startIndex, int count, MbsDataProvider<String, List<Category>> mbsDataProvider) {
        if (Log.INCLUDE) {
            Log.d(TAG, "called LoadSubCategory");
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "mCategoryMap.containsKey(" + categoryId + ")" + "=" + mCategoryMap.containsKey(categoryId));
        }
        if (mCategoryMap.containsKey(categoryId) && !mCategoryMap.get(categoryId).isEmpty()) {
            loadSubCategoryFromCache(categoryId, startIndex, count, mCategoryMap.get(categoryId), mbsDataProvider);
            return;
        }

        List<Category> subCategory = getSubCategoryListFromUp(context, version, categoryId);
        if (subCategory != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "mCategoryMap.containsKey(" + categoryId + ") getSubCategoryListFromUp not null so get from up");
            }
            putSubCategory(context, categoryId, version, subCategory, false);
            loadSubCategoryFromCache(categoryId, startIndex, count, mCategoryMap.get(categoryId), mbsDataProvider);
            return;
        }

        CategoryDataManager categoryDataManager = new CategoryDataManager();
        categoryDataManager.getCategoryList(language, said, categoryId, version, new MbsDataProvider<String, List<Category>>() {
            @Override
            public void needLoading(boolean loading) {
                mbsDataProvider.needLoading(loading);
            }

            @Override
            public void onFailed(int reason) {
                mbsDataProvider.onFailed(reason);
            }

            @Override
            public void onSuccess(String id, List<Category> result) {
                putSubCategory(context, categoryId, version, result, true);
                loadSubCategoryFromCache(categoryId, startIndex, count, mCategoryMap.get(categoryId), mbsDataProvider);
            }

            @Override
            public void onError(String errorCode, String message) {
                mbsDataProvider.onError(errorCode, message);
            }
        });
    }

    private void saveSubCategoryList(Context context, String version, String categoryId, List<Category> categories) {
        if (Log.INCLUDE) {
            Log.d(TAG, "saveSubCategoryList | categoryID : " + categoryId);
        }
        String subCategoryFileName = FILENAME_ROOT_MENU + "_" + categoryId;
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);

        CategoryResponse.CategoryResult result = new CategoryResponse.CategoryResult(version, categories);
        String categoryListString = getGson().toJson(result);

        userPreferenceManagerImp.writeString(subCategoryFileName, categoryListString);

        userPreferenceManagerImp.addStringSet(CATEGORY_FILE_LIST, subCategoryFileName);
    }


    private List<Category> getSubCategoryListFromUp(Context context, String version, String categoryId) {
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        String rootMenu = userPreferenceManagerImp.readString(FILENAME_ROOT_MENU + "_" + categoryId);
        if (Log.INCLUDE) {
            Log.d(TAG, "loadSubCategoryFile : " + rootMenu);
        }
        if (StringUtils.nullToEmpty(rootMenu).isEmpty()) {
            return null;
        }
        CategoryResponse.CategoryResult categoryResponse = getGson().fromJson(rootMenu, CategoryResponse.CategoryResult.class);

        boolean isSavedResultDataExist = categoryResponse != null;

        String previousVersion = isSavedResultDataExist ? categoryResponse.getCategoryVersion() : "-1";
        if (previousVersion.equalsIgnoreCase(version)) {

            if (isSavedResultDataExist && categoryResponse.getCategoryList() != null && !categoryResponse.getCategoryList().isEmpty()) {
                return categoryResponse.getCategoryList();
            } else {
                return null;
            }

        } else {
            userPreferenceManagerImp.writeString(FILENAME_ROOT_MENU + "_" + categoryId, "");
            return null;
        }
    }

    public boolean canLoadMore(String categoryId, int index) {
        if (mCategoryMap.containsKey(categoryId)) {
            int size = mCategoryMap.get(categoryId).size();
            return index < size;
        }
        return true;
    }

    private void loadSubCategoryFromCache(String categoryId, int startIndex, int count, List<Category> result, MbsDataProvider<String, List<Category>> mbsDataProvider) {
        int resultSize = result.size();
        if (startIndex >= resultSize) {
            mbsDataProvider.onFailed(MbsTaskCallback.REASON_NULL_OBJECT);
            return;
        }

        int endIndex = startIndex + count;
        if (endIndex >= resultSize) {
            endIndex = resultSize;
        }
        List<Category> subCategory = result.subList(startIndex, endIndex);
        makeCategoryHomeByType(subCategory, CategoryType.SubCategory);
        mbsDataProvider.onSuccess(categoryId, subCategory);
    }


    public void loadMenuIcon(Context context, ImageView view, Category category) {
        if (category == null) {
            return;
        }

        String selectedIcon = category.getSelectedIcon();
        String unSelectedIcon = category.getUnselectedIcon();
        if (StringUtils.nullToEmpty(selectedIcon).isEmpty() || StringUtils.nullToEmpty(unSelectedIcon).isEmpty()) {
            return;
        }
        String[] requestedUrl = new String[2];
        requestedUrl[0] = selectedIcon;
        requestedUrl[1] = unSelectedIcon;

        ResourceLoader<String> resourceLoader = new ResourceLoader<>();
        resourceLoader.loadResource(context, new ResourceLoader.ResourceListCallback() {
            @Override
            public void onLoaded(List<Pair<String, Drawable>> loadResource) {
                StateListDrawable states = new StateListDrawable();
                states.addState(new int[]{android.R.attr.state_focused},
                        findDrawableByStateUrl(category.getSelectedIcon(), loadResource));
                states.addState(new int[]{android.R.attr.state_selected},
                        findDrawableByStateUrl(category.getSelectedIcon(), loadResource));
                states.addState(new int[]{},
                        findDrawableByStateUrl(category.getUnselectedIcon(), loadResource));

                view.setImageDrawable(states);
            }
        }, requestedUrl);
    }

    private Drawable findDrawableByStateUrl(String url, List<Pair<String, Drawable>> loadResource) {
        for (Pair<String, Drawable> pair : loadResource) {
            if (url == null || pair.first == null) {
                continue;
            }
            if (pair.first.equalsIgnoreCase(url)) {
                return pair.second;
            }
        }
        return null;
    }

    private Category findCategoryById(String id) {
        for (Category category : mCurrentCategories) {
            if (category.getCategoryID().equalsIgnoreCase(id)) {
                return category;
            }
        }
        return null;
    }

    private Category findCategoryByType(CategoryType type) {
        for (Category category : mCurrentCategories) {
            if (category.getLinkType() == type) {
                return category;
            }
        }
        return null;
    }

    private Category makeCategoryHomeByType(List<Category> result, CategoryType type) {
        for (Category category : result) {
            if (category.getLinkType() == type) {
                category.setHome(true);
                return category;
            }
        }
        return null;
    }

    private Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(CategoryType.class, new CategoryTypeDeserializer());
        return builder.create();
    }


    private static class ContentsListWithVersion implements Parcelable {
        @Expose
        @SerializedName("version")
        private String version;

        @Expose
        @SerializedName("categoryId")
        private String categoryId;

        @Expose
        @SerializedName("contentList")
        private List<Content> contentList;

        public ContentsListWithVersion(String version, String categoryId, List<Content> contentList) {
            this.version = version;
            this.categoryId = categoryId;
            this.contentList = contentList;
        }

        protected ContentsListWithVersion(Parcel in) {
            version = in.readString();
            categoryId = in.readString();
            contentList = in.createTypedArrayList(Content.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(version);
            dest.writeString(categoryId);
            dest.writeTypedList(contentList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getVersion() {
            return version;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public List<Content> getContentList() {
            return contentList;
        }

        @Override
        public String toString() {
            return "CategoryListWithVersion{" +
                    "version='" + version + '\'' +
                    ", categoryId='" + categoryId + '\'' +
                    ", contentList=" + contentList +
                    '}';
        }

        public static final Creator<ContentsListWithVersion> CREATOR = new Creator<ContentsListWithVersion>() {
            @Override
            public ContentsListWithVersion createFromParcel(Parcel in) {
                return new ContentsListWithVersion(in);
            }

            @Override
            public ContentsListWithVersion[] newArray(int size) {
                return new ContentsListWithVersion[size];
            }
        };
    }


    private static class BannerListWithVersion implements Parcelable {
        @Expose
        @SerializedName("version")
        private String version;

        @Expose
        @SerializedName("categoryId")
        private String categoryId;
        @Expose
        @SerializedName("bannerPositionId")
        private String bannerPositionId;
        @Expose
        @SerializedName("bannerList")
        private List<Banner> bannerList;

        public BannerListWithVersion(String version, String categoryId, String bannerPositionId, List<Banner> bannerList) {
            this.version = version;
            this.categoryId = categoryId;
            this.bannerPositionId = bannerPositionId;
            this.bannerList = bannerList;
        }

        protected BannerListWithVersion(Parcel in) {
            version = in.readString();
            categoryId = in.readString();
            bannerPositionId = in.readString();
            bannerList = in.createTypedArrayList(Banner.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(version);
            dest.writeString(categoryId);
            dest.writeString(bannerPositionId);
            dest.writeTypedList(bannerList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getVersion() {
            return version;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public String getBannerPositionId() {
            return bannerPositionId;
        }

        public List<Banner> getBannerList() {
            return bannerList;
        }

        @Override
        public String toString() {
            return "BannerListWithVersion{" +
                    "version='" + version + '\'' +
                    ", categoryId='" + categoryId + '\'' +
                    ", bannerPositionId='" + bannerPositionId + '\'' +
                    ", bannerList=" + bannerList +
                    '}';
        }

        public static final Creator<BannerListWithVersion> CREATOR = new Creator<BannerListWithVersion>() {
            @Override
            public BannerListWithVersion createFromParcel(Parcel in) {
                return new BannerListWithVersion(in);
            }

            @Override
            public BannerListWithVersion[] newArray(int size) {
                return new BannerListWithVersion[size];
            }
        };
    }


    public interface OnMenuUpdateCallback {
        void onMenuMenuUpdated(List<Category> result);

        void onCategoryUpdated(List<Category> updateList);

        void onContentsCategoryUpdated(String categoryId, String version);

        void onFailed();
    }

    private final String DUMMY_CONTENTS = "{\n" +
            "\t\"data\": {\n" +
            "\t\t\"contentList\": [{\n" +
            "\t\t\t\"bannerClipAsset\": {\n" +
            "\t\t\t\t\"uhdFlag\": \"N\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"bannerImageAsset\": {\n" +
            "\t\t\t\t\"containerType\": \"jpg\",\n" +
            "\t\t\t\t\"fileName\": \"http://110.164.125.14:8080/bannerImage/153/153\",\n" +
            "\t\t\t\t\"id\": \"153\",\n" +
            "\t\t\t\t\"provider\": \"Castis\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"bannerType\": 1,\n" +
            "\t\t\t\"containerType\": \"jpg\",\n" +
            "\t\t\t\"contentGroupId\": \"114\",\n" +
            "\t\t\t\"episodeCnt\": 0,\n" +
            "\t\t\t\"eventFlag\": [],\n" +
            "\t\t\t\"genre\": \"\",\n" +
            "\t\t\t\"isLike\": \"N\",\n" +
            "\t\t\t\"isSeries\": \"N\",\n" +
            "\t\t\t\"movieAssetId\": \"114\",\n" +
            "\t\t\t\"posterFileName\": \"http://110.164.125.14:8080/poster/301/301\",\n" +
            "\t\t\t\"productList\": [],\n" +
            "\t\t\t\"rating\": 18,\n" +
            "\t\t\t\"resumeTime\": 0,\n" +
            "\t\t\t\"runTimeDisplay\": 0,\n" +
            "\t\t\t\"streaminfo\": {},\n" +
            "\t\t\t\"title\": \"Renegdes\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"bannerClipAsset\": {\n" +
            "\t\t\t\t\"uhdFlag\": \"N\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"bannerImageAsset\": {\n" +
            "\t\t\t\t\"containerType\": \"jpg\",\n" +
            "\t\t\t\t\"fileName\": \"http://110.164.125.14:8080/bannerImage/153/153\",\n" +
            "\t\t\t\t\"id\": \"153\",\n" +
            "\t\t\t\t\"provider\": \"Castis\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"bannerType\": 2,\n" +
            "\t\t\t\"containerType\": \"jpg\",\n" +
            "\t\t\t\"contentGroupId\": \"114\",\n" +
            "\t\t\t\"episodeCnt\": 0,\n" +
            "\t\t\t\"eventFlag\": [],\n" +
            "\t\t\t\"genre\": \"\",\n" +
            "\t\t\t\"isLike\": \"N\",\n" +
            "\t\t\t\"isSeries\": \"N\",\n" +
            "\t\t\t\"movieAssetId\": \"114\",\n" +
            "\t\t\t\"posterFileName\": \"http://110.164.125.14:8080/poster/301/301\",\n" +
            "\t\t\t\"productList\": [],\n" +
            "\t\t\t\"rating\": 18,\n" +
            "\t\t\t\"resumeTime\": 0,\n" +
            "\t\t\t\"runTimeDisplay\": 0,\n" +
            "\t\t\t\"streaminfo\": {},\n" +
            "\t\t\t\"title\": \"Renegdes\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"bannerClipAsset\": {\n" +
            "\t\t\t\t\"audioType\": \"aac\",\n" +
            "\t\t\t\t\"containerType\": \"jpg\",\n" +
            "\t\t\t\t\"coverImageId\": \"151\",\n" +
            "\t\t\t\t\"coverImageName\": \"http://110.164.125.14:8080/bannerImage/151/151\",\n" +
            "\t\t\t\t\"fileName\": [\"http://110.164.125.11:8080/3bb/bannerClip/80/80.mpd\", \"http://110.164.125.9:8080/3bb/bannerClip/80/80.mpd\"],\n" +
            "\t\t\t\t\"provider\": \"Castis\",\n" +
            "\t\t\t\t\"uhdFlag\": \"N\",\n" +
            "\t\t\t\t\"videoType\": \"h264\"\n" +
            "\t\t\t},\n" +
            "\t\t\t\"bannerImageAsset\": {},\n" +
            "\t\t\t\"bannerType\": 2,\n" +
            "\t\t\t\"containerType\": \"jpg\",\n" +
            "\t\t\t\"contentGroupId\": \"114\",\n" +
            "\t\t\t\"episodeCnt\": 0,\n" +
            "\t\t\t\"eventFlag\": [],\n" +
            "\t\t\t\"genre\": \"\",\n" +
            "\t\t\t\"isLike\": \"N\",\n" +
            "\t\t\t\"isSeries\": \"N\",\n" +
            "\t\t\t\"movieAssetId\": \"114\",\n" +
            "\t\t\t\"posterFileName\": \"http://110.164.125.14:8080/poster/301/301\",\n" +
            "\t\t\t\"productList\": [],\n" +
            "\t\t\t\"rating\": 18,\n" +
            "\t\t\t\"resumeTime\": 0,\n" +
            "\t\t\t\"runTimeDisplay\": 0,\n" +
            "\t\t\t\"streaminfo\": {},\n" +
            "\t\t\t\"title\": \"Renegdes\"\n" +
            "\t\t}],\n" +
            "\t\t\"totalContentNo\": 3\n" +
            "\t},\n" +
            "\t\"returnCode\": \"S\"\n" +
            "}";


    private final String DUMMY_MAIN_MENU = "{\n" +
            " \t\"list\": [{\n" +
            " \t\t\"adult\": \"N\",\n" +
            " \t\t\"categoryId\": \"45\",\n" +
            " \t\t\"categoryName\": \"SEARCH\",\n" +
            " \t\t\"categoryParentId\": \"44\",\n" +
            " \t\t\"contentBannerFlag\": \"N\",\n" +
            " \t\t\"isHome\": false,\n" +
            " \t\t\"linkType\": \"search\",\n" +
            " \t\t\"orderNumber\": 1,\n" +
            " \t\t\"selectedIcon\": \"http://110.164.125.14:8080/menuIcon/45/45_sel_20200827094258.png\",\n" +
            " \t\t\"unselectedIcon\": \"http://110.164.125.14:8080/menuIcon/45/45_unsel_20200827094258.png\",\n" +
            " \t\t\"version\": \"56\",\n" +
            " \t\t\"visibility\": \"Y\"\n" +
            " \t}, {\n" +
            " \t\t\"adult\": \"N\",\n" +
            " \t\t\"categoryId\": \"46\",\n" +
            " \t\t\"categoryName\": \"HOME\",\n" +
            " \t\t\"categoryParentId\": \"44\",\n" +
            " \t\t\"contentBannerFlag\": \"N\",\n" +
            " \t\t\"isHome\": true,\n" +
            " \t\t\"linkType\": \"menu\",\n" +
            " \t\t\"orderNumber\": 2,\n" +
            " \t\t\"selectedIcon\": \"http://110.164.125.14:8080/menuIcon/46/46_sel_20200827094238.png\",\n" +
            " \t\t\"unselectedIcon\": \"http://110.164.125.14:8080/menuIcon/46/46_unsel_20200827094238.png\",\n" +
            " \t\t\"version\": \"56\",\n" +
            " \t\t\"visibility\": \"Y\"\n" +
            " \t}, {\n" +
            " \t\t\"adult\": \"N\",\n" +
            " \t\t\"categoryId\": \"55\",\n" +
            " \t\t\"categoryName\": \"LIVE TV\",\n" +
            " \t\t\"categoryParentId\": \"44\",\n" +
            " \t\t\"contentBannerFlag\": \"N\",\n" +
            " \t\t\"isHome\": false,\n" +
            " \t\t\"linkType\": \"channel\",\n" +
            " \t\t\"orderNumber\": 3,\n" +
            " \t\t\"selectedIcon\": \"http://110.164.125.14:8080/menuIcon/55/55_sel_20200827095348.png\",\n" +
            " \t\t\"unselectedIcon\": \"http://110.164.125.14:8080/menuIcon/55/55_unsel_20200827095348.png\",\n" +
            " \t\t\"version\": \"56\",\n" +
            " \t\t\"visibility\": \"Y\"\n" +
            " \t}, {\n" +
            " \t\t\"adult\": \"N\",\n" +
            " \t\t\"categoryId\": \"56\",\n" +
            " \t\t\"categoryName\": \"HBO GO\",\n" +
            " \t\t\"categoryParentId\": \"44\",\n" +
            " \t\t\"contentBannerFlag\": \"N\",\n" +
            " \t\t\"isHome\": false,\n" +
            " \t\t\"linkInfo\": \"com.hbo.asia.androidtv\",\n" +
            " \t\t\"linkType\": \"3rdPartyApp\",\n" +
            " \t\t\"orderNumber\": 4,\n" +
            " \t\t\"selectedIcon\": \"http://110.164.125.14:8080/menuIcon/56/56_sel_20200827095743.png\",\n" +
            " \t\t\"unselectedIcon\": \"http://110.164.125.14:8080/menuIcon/56/56_unsel_20200827095743.png\",\n" +
            " \t\t\"version\": \"56\",\n" +
            " \t\t\"visibility\": \"Y\"\n" +
            " \t}, {\n" +
            " \t\t\"adult\": \"N\",\n" +
            " \t\t\"categoryId\": \"57\",\n" +
            " \t\t\"categoryName\": \"MONOMAX\",\n" +
            " \t\t\"categoryParentId\": \"44\",\n" +
            " \t\t\"contentBannerFlag\": \"N\",\n" +
            " \t\t\"isHome\": false,\n" +
            " \t\t\"linkInfo\": \"com.google.android.youtube.tv\",\n" +
            " \t\t\"linkType\": \"3rdPartyApp\",\n" +
            " \t\t\"orderNumber\": 5,\n" +
            " \t\t\"selectedIcon\": \"http://110.164.125.14:8080/menuIcon/57/57_sel_20200827095926.png\",\n" +
            " \t\t\"unselectedIcon\": \"http://110.164.125.14:8080/menuIcon/57/57_unsel_20200827095926.png\",\n" +
            " \t\t\"version\": \"57\",\n" +
            " \t\t\"visibility\": \"Y\"\n" +
            " \t}, {\n" +
            " \t\t\"adult\": \"N\",\n" +
            " \t\t\"categoryId\": \"58\",\n" +
            " \t\t\"categoryName\": \"KIDS\",\n" +
            " \t\t\"categoryParentId\": \"44\",\n" +
            " \t\t\"contentBannerFlag\": \"N\",\n" +
            " \t\t\"isHome\": false,\n" +
            " \t\t\"linkType\": \"menu\",\n" +
            " \t\t\"orderNumber\": 6,\n" +
            " \t\t\"selectedIcon\": \"http://110.164.125.14:8080/menuIcon/58/58_sel_20200827100003.png\",\n" +
            " \t\t\"unselectedIcon\": \"http://110.164.125.14:8080/menuIcon/58/58_unsel_20200827100003.png\",\n" +
            " \t\t\"version\": \"56\",\n" +
            " \t\t\"visibility\": \"Y\"\n" +
            " \t}, {\n" +
            " \t\t\"adult\": \"N\",\n" +
            " \t\t\"categoryId\": \"59\",\n" +
            " \t\t\"categoryName\": \"APPS\",\n" +
            " \t\t\"categoryParentId\": \"44\",\n" +
            " \t\t\"contentBannerFlag\": \"N\",\n" +
            " \t\t\"isHome\": false,\n" +
            " \t\t\"linkType\": \"apps\",\n" +
            " \t\t\"orderNumber\": 7,\n" +
            " \t\t\"selectedIcon\": \"http://110.164.125.14:8080/menuIcon/59/59_sel_20200827100347.png\",\n" +
            " \t\t\"unselectedIcon\": \"http://110.164.125.14:8080/menuIcon/59/59_unsel_20200827100347.png\",\n" +
            " \t\t\"version\": \"56\",\n" +
            " \t\t\"visibility\": \"Y\"\n" +
            " \t}, {\n" +
            " \t\t\"adult\": \"N\",\n" +
            " \t\t\"categoryId\": \"60\",\n" +
            " \t\t\"categoryName\": \"MESSAGE\",\n" +
            " \t\t\"categoryParentId\": \"44\",\n" +
            " \t\t\"contentBannerFlag\": \"N\",\n" +
            " \t\t\"isHome\": false,\n" +
            " \t\t\"linkType\": \"message\",\n" +
            " \t\t\"orderNumber\": 8,\n" +
            " \t\t\"selectedIcon\": \"http://110.164.125.14:8080/menuIcon/60/60_sel_20200827100417.png\",\n" +
            " \t\t\"unselectedIcon\": \"http://110.164.125.14:8080/menuIcon/60/60_unsel_20200827100417.png\",\n" +
            " \t\t\"version\": \"56\",\n" +
            " \t\t\"visibility\": \"Y\"\n" +
            " \t}],\n" +
            " \t\"categoryVersion\": \"56\"\n" +
            " }";

    private final String DUMMY_VERSION = "{\n" +
            "\t\"data\": {\n" +
            "\t\t\"categoryList\": [{\n" +
            "\t\t\t\t\"categoryId\": \"44\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"45\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"46\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"47\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"48\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"49\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"50\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"51\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"52\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"53\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"54\",\n" +
            "\t\t\t\t\"version\": \"57\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"55\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"56\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"57\",\n" +
            "\t\t\t\t\"version\": \"57\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"58\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"59\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"60\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"61\",\n" +
            "\t\t\t\t\"version\": \"51\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"62\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"63\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"64\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"81\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"82\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"119\",\n" +
            "\t\t\t\t\"version\": \"57\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"categoryId\": \"120\",\n" +
            "\t\t\t\t\"version\": \"56\"\n" +
            "\t\t\t}\n" +
            "\t\t],\n" +
            "\t\t\"rootCategoryId\": \"44\",\n" +
            "\t\t\"updateYn\": \"N\",\n" +
            "\t\t\"version\": \"56\"\n" +
            "\t},\n" +
            "\t\"returnCode\": \"S\"\n" +
            "}";
}
