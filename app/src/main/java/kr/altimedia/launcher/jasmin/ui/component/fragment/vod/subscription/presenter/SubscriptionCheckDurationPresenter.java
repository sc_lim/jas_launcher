/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionPriceInfo;

public class SubscriptionCheckDurationPresenter extends Presenter {

    public SubscriptionCheckDurationPresenter() {
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_subscription_check_duration, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((SubscriptionPriceInfo) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public class ViewHolder extends Presenter.ViewHolder {
        private TextView title;
        private TextView value;
        private CheckBox checkBox;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            title = view.findViewById(R.id.title);
            value = view.findViewById(R.id.value);
            checkBox = view.findViewById(R.id.check_box);
        }

        public void setData(SubscriptionPriceInfo price) {
            checkBox.setChecked(false);
            title.setText(getUnit(price.getDuration()));
            value.setText(price.getPrice());
        }

        public void toggleChecked() {
            boolean isChecked = isChecked();
            checkBox.setChecked(!isChecked);
        }

        public boolean isChecked() {
            return checkBox.isChecked();
        }

        private String getUnit(int duration) {
            int dur = duration;

            Context context = view.getContext();
            if (dur > 1) {
                return duration + " " + context.getString(R.string.months);
            }

            return context.getString(R.string.monthly);
        }
    }
}
