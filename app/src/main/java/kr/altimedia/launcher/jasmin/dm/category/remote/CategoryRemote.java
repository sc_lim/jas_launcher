/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.category.remote;


import com.altimedia.util.Log;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.MbsUtil;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.category.api.CategoryApi;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.dm.category.obj.response.CategoryResponse;
import kr.altimedia.launcher.jasmin.dm.category.obj.response.CategoryVersionCheckResponse;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mc.kim on 06,12,2019
 */
public class CategoryRemote {

    private static final String TAG = CategoryRemote.class.getSimpleName();
    private CategoryApi mCategoryApi;
    private final String DEVICE_TYPE = "STB";

    public CategoryRemote(CategoryApi categoryApi) {
        mCategoryApi = categoryApi;
    }

    public List<Category> getCategoryList(String lang, String said, String categoryId, String version) throws ResponseException, IOException, AuthenticationException {
        Call<CategoryResponse> call = mCategoryApi.getCategoryList(lang, said, categoryId, DEVICE_TYPE, version);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<CategoryResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        CategoryResponse mCategoryResponse = response.body();
        return mCategoryResponse.getCategoryList();
    }

    public CategoryVersionCheckResponse checkCategoryVersion(String currentVersion) throws IOException, ResponseException, AuthenticationException {
        Call<CategoryVersionCheckResponse> call = mCategoryApi.getCategoryVersionCheckResult(DEVICE_TYPE, currentVersion);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<CategoryVersionCheckResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        CategoryVersionCheckResponse mCategoryVersionResponse = response.body();


        return mCategoryVersionResponse;
    }


    private final String DUMMY_VERSION = "{\n" +
            "\t\"data\": {\n" +
            "\t\t\"rootCategoryId\": \"44\",\n" +
            "\t\t\"updateYn\": \"N\",\n" +
            "\t\t\"version\": \"48\",\n" +
            "\t\t\"categoryList\": [{\n" +
            "\t\t\t\t\"categoryId\": \"46\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"58\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"50\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"47\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"48\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"81\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"51\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"52\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"54\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}\n" +
            "\n" +
            "\t\t\t, {\n" +
            "\t\t\t\t\"categoryId\": \"82\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"61\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"62\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"63\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}, {\n" +
            "\t\t\t\t\"categoryId\": \"64\",\n" +
            "\t\t\t\t\"version\": \"48\"\n" +
            "\t\t\t}\n" +
            "\n" +
            "\t\t]\n" +
            "\t},\n" +
            "\t\"returnCode\": \"S\"\n" +
            "}";
}
