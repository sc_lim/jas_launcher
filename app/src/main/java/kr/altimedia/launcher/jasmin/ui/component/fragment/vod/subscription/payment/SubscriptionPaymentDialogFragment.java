/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.type.ContentType;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionPriceInfo;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PURCHASE_INFORM;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_MESSAGE_DESC;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_OFFER_ID;
import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionPurchaseDialogFragment.KEY_SUBSCRIPTION_INFO;

public class SubscriptionPaymentDialogFragment extends SafeDismissDialogFragment implements SubscriptionPaymentSuccessFragment.OnPaymentListener {
    public static final String CLASS_NAME = SubscriptionPaymentDialogFragment.class.getName();
    private static final String TAG = SubscriptionPaymentDialogFragment.class.getSimpleName();

    private static final String KEY_SUBSCRIPTION_PRICE = "PRICE";

    private static final String KEY_SENDER_ID = "SENDER_ID";
    public final String WORK_DETAIL_CALL = "DETAIL";
    public final String WORK_PURCHASE_CHECK = "PURCHASE_CHECK";
    private ArrayList<String> mWorkList = new ArrayList<>();

    private SubscriptionInfo subscriptionInfo;
    private SubscriptionPriceInfo price;

    private OnCompleteSubscribePaymentListener onCompleteSubscribePaymentListener;

    private int mSenderId;

    public SubscriptionPaymentDialogFragment() {
    }

    public static SubscriptionPaymentDialogFragment newInstance(int mSenderId, SubscriptionInfo subscriptionInfo, SubscriptionPriceInfo price) {

        Bundle args = new Bundle();
        args.putInt(KEY_SENDER_ID, mSenderId);
        args.putParcelable(KEY_SUBSCRIPTION_INFO, subscriptionInfo);
        args.putParcelable(KEY_SUBSCRIPTION_PRICE, price);

        SubscriptionPaymentDialogFragment fragment = new SubscriptionPaymentDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {
                Fragment child = getChildFragmentManager().findFragmentByTag(SubscriptionPaymentFragment.CLASS_NAME);
                if (child != null) {
                    super.onBackPressed();
                } else if (child == null && onCompleteSubscribePaymentListener != null) {
                    dismiss();
                    onCompleteSubscribePaymentListener.onDismissPurchaseDialog();
                }
            }
        };
    }

    public void setOnCompleteSubscribePaymentListener(OnCompleteSubscribePaymentListener onCompleteSubscribePaymentListener) {
        this.onCompleteSubscribePaymentListener = onCompleteSubscribePaymentListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_vod_payment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        mSenderId = bundle.getInt(KEY_SENDER_ID);
        subscriptionInfo = bundle.getParcelable(KEY_SUBSCRIPTION_INFO);
        price = bundle.getParcelable(KEY_SUBSCRIPTION_PRICE);

        showPaymentFragment();
    }

    private void showPaymentFragment() {
        SubscriptionPaymentFragment subscriptionPaymentFragment = SubscriptionPaymentFragment.newInstance(subscriptionInfo, price);
        subscriptionPaymentFragment.setOnPaymentListener(this);
        attacheFragment(subscriptionPaymentFragment, SubscriptionPaymentFragment.CLASS_NAME);
    }

    public void showPurchaseSuccessFragment(boolean isVodSubscription, int senderId, String work) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showPurchaseSuccessFragment, senderId : " + senderId + ", work : " + work);
        }

        if (this.mSenderId != senderId || mWorkList.contains(work)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showPurchaseSuccessFragment, return");
            }
            return;
        }

        mWorkList.add(work);

        if (Log.INCLUDE) {
            Log.d(TAG, "showPurchaseSuccessFragment, isVodSubscription : " + isVodSubscription
                    + ", work : " + work + ", work size : " + mWorkList.size());
        }

        if ((isVodSubscription && mWorkList.size() == 2) || (!isVodSubscription && mWorkList.size() == 1)) {
            SubscriptionPaymentSuccessFragment subscriptionPaymentSuccessFragment = SubscriptionPaymentSuccessFragment.newInstance(subscriptionInfo);
            subscriptionPaymentSuccessFragment.setOnPaymentListener(this);
            attacheFragment(subscriptionPaymentSuccessFragment, SubscriptionPaymentFailFragment.CLASS_NAME);
            mWorkList.clear();
        }
    }

    private void showPurchaseFailFragment(String paymentErrorMessage) {
        SubscriptionPaymentFailFragment subscriptionPaymentFailFragment = SubscriptionPaymentFailFragment.newInstance(subscriptionInfo, paymentErrorMessage);
        subscriptionPaymentFailFragment.setOnPaymentListener(this);
        attacheFragment(subscriptionPaymentFailFragment, SubscriptionPaymentFailFragment.CLASS_NAME);
    }

    private void attacheFragment(Fragment fragment, String tag) {
        SubscriptionPaymentBaseFragment subscriptionPaymentBaseFragment = (SubscriptionPaymentBaseFragment) fragment;
        getChildFragmentManager().beginTransaction().replace(R.id.frame, subscriptionPaymentBaseFragment, tag).commit();
    }

    private void onPurchasePushMessage() {
        Intent mIntent = new Intent(getContext(), PushMessageReceiver.class);
        mIntent.setAction(ACTION_UPDATED_PURCHASE_INFORM);
        mIntent.putExtra(KEY_MESSAGE_DESC, "Success");
        mIntent.putExtra(KEY_OFFER_ID, price.getOfferId());
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(mIntent);
    }

    @Override
    public void onPaymentSubscription(boolean isSuccess) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPaymentSubscription, isSuccess : " + isSuccess);
        }

        if (isSuccess) {
            onPurchasePushMessage();
            boolean isVodSubscription = subscriptionInfo.getContentType() == ContentType.VOD;
            showPurchaseSuccessFragment(isVodSubscription, mSenderId, WORK_PURCHASE_CHECK);
        }

        if (onCompleteSubscribePaymentListener != null) {
            onCompleteSubscribePaymentListener.onCompleteSubscribePayment(isSuccess);
        }
    }

    @Override
    public void onPaymentError(String paymentErrorMessage) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPaymentSubscription, paymentErrorMessage : " + paymentErrorMessage);
        }

        if (onCompleteSubscribePaymentListener != null) {
            onCompleteSubscribePaymentListener.onCompleteSubscribePayment(false);
        }

        showPurchaseFailFragment(paymentErrorMessage);
    }

    @Override
    public void onDismissPaymentDialog() {
        dismiss();
    }

    @Override
    public void onDismissPurchaseDialog() {
        dismiss();
        if (onCompleteSubscribePaymentListener != null) {
            onCompleteSubscribePaymentListener.onDismissPurchaseDialog();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        onCompleteSubscribePaymentListener = null;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public interface OnCompleteSubscribePaymentListener {
        void onCompleteSubscribePayment(boolean isSuccess);

        void onDismissPurchaseDialog();
    }
}
