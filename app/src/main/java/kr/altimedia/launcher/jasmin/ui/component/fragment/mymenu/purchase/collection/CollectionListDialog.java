/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.collection;

import android.content.Context;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.contents.ContentDataManager;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.dm.contents.obj.BannerImageAsset;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.CollectedContent;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.MyContentListFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.Linker;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.OnDataLoadedListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.presenter.MyContentListPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.collection.presenter.VodCollectionPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class CollectionListDialog extends SafeDismissDialogFragment implements OnDataLoadedListener, View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = CollectionListDialog.class.getName();
    private final String TAG = CollectionListDialog.class.getSimpleName();

    private final Integer TYPE_PROMOTION = new Integer(0);
    private final Integer TYPE_COLLECTION = new Integer(1);

    private final List<WeakReference<AsyncTask>> taskList = new ArrayList<>();

    private ProgressBarManager progressBarManager;

    private final String BANNER_POSITION_ID = "stb_col_banner";
    private final int BANNER_WIDTH = 1612;
    private final int BANNER_HEIGHT = 381;
    private Banner selectedBanner;
    private LinearLayout promotionLayer;
    private ImageView promotionBanner;

    private final int CONTENTS_VISIBLE_SIZE = 6;

    private RelativeLayout collectionLayer;
    private MyContentListFragment collectionView;
    private ClassPresenterSelector collectionPresenterSelector;
    private ArrayObjectAdapter collectionArrayObjAdapter;
    private BaseOnItemViewClickedListener mOnItemViewClickedListener;
    private LinearLayout emptyVodLayer;
    private TextView countView;
    private View rootView;

    private final ArrayList<CollectedContent> contentList = new ArrayList<>();

    public static CollectionListDialog newInstance() {
        Bundle args = new Bundle();
        CollectionListDialog fragment = new CollectionListDialog();
        fragment.setArguments(args);
        return fragment;
    }

    private void clearThread() {
        for (WeakReference<AsyncTask> task : taskList) {
            if (task.get() != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "canceled");
                }
                task.get().cancel(true);
            }
        }
        taskList.clear();
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onDestroyView() {

        clearThread();
        if (contentList != null && !contentList.isEmpty()) {
            contentList.clear();
        }
        if (progressBarManager != null) {
            progressBarManager.hide();
            progressBarManager.setRootView(null);
        }
        if (rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        collectionView = (MyContentListFragment) getChildFragmentManager().findFragmentById(R.id.collectionView);
        if (collectionView == null) {
            Rect padding = new Rect(30, 0, 0, 0);
            collectionView = MyContentListFragment.newInstance(padding);
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.collectionView, collectionView).commit();
        }

        return inflater.inflate(R.layout.dialog_mymenu_collection_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        countView = view.findViewById(R.id.count);
        initLoadingBar(view);
        initPromotion(view);
        initCollection(view);

        loadPromotion();
        loadCollection();
    }

    private void initLoadingBar(View view) {
        progressBarManager = new ProgressBarManager();
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);
        showProgress();
    }

    private void initPromotion(View view) {

        promotionLayer = view.findViewById(R.id.promotionLayer);
        promotionBanner = view.findViewById(R.id.promotionBanner);

        promotionLayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedBanner != null) {
                    String linkType = selectedBanner.getLinkType();
                    if (linkType.equalsIgnoreCase(Banner.LINK_TYPE_CONTENTS)) {
                        String id = selectedBanner.getLinkInfo();
                        Linker.goToVodDetail(mTvOverlayManager, getFragmentManager(), id);

                    } else if (linkType.equalsIgnoreCase(Banner.LINK_TYPE_SERIES)) {
                        String id = selectedBanner.getLinkInfo();
                        String categoryId = selectedBanner.getCategoryId();
                        Linker.goToSeriesVodDetail(mTvOverlayManager, id, categoryId, "");
                    } else if (linkType.equalsIgnoreCase(Banner.LINK_TYPE_CHANNEL)) {
                        String id = selectedBanner.getLinkInfo();
                        PlaybackUtil.startLiveTvActivityWithServiceId(getContext(), id);
                    }
                }
            }
        });
    }

    private void initCollection(View view) {
        collectionLayer = view.findViewById(R.id.collectionLayer);
        emptyVodLayer = view.findViewById(R.id.emptyVodLayer);
    }


    private void loadPromotion() {
        ContentDataManager contentDataManager = new ContentDataManager();
        AsyncTask task = contentDataManager.getBannerList(
                AccountManager.getInstance().getLocalLanguage(),
                AccountManager.getInstance().getSaId(),
                BANNER_POSITION_ID, "",
                new MbsDataProvider<String, List<Banner>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int reason) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadPromotion: onFailed");
                        }
                        showView(TYPE_PROMOTION);
                    }

                    @Override
                    public void onSuccess(String key, List<Banner> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadPromotion: result=" + result);
                        }
                        List<Banner> tmplist = result;
                        for (Banner banner : tmplist) {
                            if (banner.getBannerType() == Banner.IMAGE_TYPE) {
                                selectedBanner = banner;
                            }
                        }
                        showView(TYPE_PROMOTION);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }
                });

        taskList.add(new WeakReference<>(task));
    }

    private void loadCollection() {
        if (contentList != null && !contentList.isEmpty()) {
            contentList.clear();
        }

        UserDataManager userDataManager = new UserDataManager();
        AsyncTask task = userDataManager.getCollectionList(
                AccountManager.getInstance().getLocalLanguage(),
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                new MbsDataProvider<String, List<CollectedContent>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int reason) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadCollection: onFailed");
                        }
                        showView(TYPE_COLLECTION);

                        if (reason == MbsTaskCallback.REASON_NETWORK) {
                            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(reason, getTaskContext());
                            failNoticeDialogFragment.show(mTvOverlayManager,getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                        }
                    }

                    @Override
                    public void onSuccess(String key, List<CollectedContent> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadCollection: result=" + result);
                        }
                        contentList.clear();
                        List<CollectedContent> tmplist = result;
                        for (CollectedContent content : tmplist) {
                            if (!content.isHidden()) {
                                contentList.add(content);
                            }
                        }

                        showView(TYPE_COLLECTION);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        showView(TYPE_COLLECTION);

                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return CollectionListDialog.this.getContext();
                    }
                });
        taskList.add(new WeakReference<>(task));
    }

    @Override
    public void hideProgress() {
        if (progressBarManager != null) {
            progressBarManager.hide();
        }
    }

    @Override
    public void showProgress() {
        if (taskList.isEmpty()) {
            progressBarManager.show();
        }
    }

    @Override
    public void showView(int type) {
        if (type == TYPE_PROMOTION) {
            setPromotionView();
        } else if (type == TYPE_COLLECTION) {
            setVodView();
        }
    }

    private void setPromotionView() {
        if (selectedBanner != null) {
            try {
                BannerImageAsset imageAsset = selectedBanner.getBannerImageAsset();
                if (imageAsset != null) {
                    Glide.with(getContext())
                            .load(imageAsset.getPosterSourceUrl(BANNER_WIDTH, BANNER_HEIGHT)).
                            diskCacheStrategy(DiskCacheStrategy.DATA)
                            .centerCrop()
                            .placeholder(R.drawable.default_background)
                            .error(R.drawable.default_background)
                            .into(promotionBanner);
                }
            } catch (Exception e) {
            }
        }
    }

    private void setVodView() {
        if (contentList != null && contentList.size() > 0) {
            try {
                VodCollectionPresenter contentPresenter = new VodCollectionPresenter();
                ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter(contentPresenter);
                arrayObjectAdapter.addAll(0, contentList);
                HeaderItem header = new HeaderItem(0, "");
                ListRow listRow = new ListRow(header, arrayObjectAdapter);

                collectionPresenterSelector = new ClassPresenterSelector();
                if (collectionArrayObjAdapter == null) {
                    collectionArrayObjAdapter = new ArrayObjectAdapter(collectionPresenterSelector);
                } else {
                    collectionArrayObjAdapter.clear();
                }
                collectionArrayObjAdapter.add(listRow);

                mOnItemViewClickedListener = new VodItemClickedListener();
                if (collectionView != null) {
                    collectionView.setOnItemViewClickedListener(mOnItemViewClickedListener);
                }

                MyContentListPresenter rowPresenter = new MyContentListPresenter();
                rowPresenter.setFixedItemIndex(-1);
                rowPresenter.setOnPresenterDispatchKeyListener(new MyContentListPresenter.OnPresenterDispatchKeyListener() {
                    @Override
                    public boolean notifyKey(KeyEvent event, Presenter.ViewHolder viewHolder, int position) {
                        int keyCode = event.getKeyCode();
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_DPAD_RIGHT: {
                                HorizontalGridView hGridView = ((MyContentListPresenter.ViewHolder) viewHolder).getGridView();
                                if (hGridView != null) {
                                    int size = hGridView.getAdapter().getItemCount();
                                    if (position == (size - 1)) {
                                        hGridView.scrollToPosition(0);
                                        return true;
                                    }
                                }
                            }
                        }
                        return false;
                    }
                });
                collectionPresenterSelector.addClassPresenter(ListRow.class, rowPresenter);

                if (collectionView != null) {
                    collectionView.setAdapter(collectionArrayObjAdapter);
                }

                countView.setText("(" + contentList.size() + ")");

                emptyVodLayer.setVisibility(View.GONE);
                countView.setVisibility(View.VISIBLE);
                collectionLayer.setVisibility(View.VISIBLE);

                setFocus(TYPE_COLLECTION);
            }catch (Exception e){
                setFocus(TYPE_PROMOTION);
            }
        } else {
            countView.setVisibility(View.GONE);
            collectionLayer.setVisibility(View.GONE);
            emptyVodLayer.setVisibility(View.VISIBLE);

            setFocus(TYPE_PROMOTION);
        }
    }

    private void setFocus(int type) {
        promotionLayer.setFocusable(true);
        promotionLayer.setFocusableInTouchMode(true);

        if (type == TYPE_PROMOTION) {
            promotionLayer.requestFocus();

        } else if (type == TYPE_COLLECTION) {
            collectionLayer.requestFocus();
            collectionView.resetFocus();
        }
    }

    private final class VodItemClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder var, Row row) {
            if (Log.INCLUDE) {
                Log.d(TAG, "VodItemClickedListener: onItemClicked: item=" + item + ", row=" + row);
            }
            if (item == null) {
                return;
            }
            if (item instanceof CollectedContent) {
                CollectedContent content = (CollectedContent) item;
                if (content.isPackage()) {
                    Linker.goToPackageVodDetail(mTvOverlayManager, content.getPackageId(), content.getRating());
                } else {
                    Linker.goToVodDetail(mTvOverlayManager, Content.buildCollectedContent(content));
                }
            }
        }
    }

}
