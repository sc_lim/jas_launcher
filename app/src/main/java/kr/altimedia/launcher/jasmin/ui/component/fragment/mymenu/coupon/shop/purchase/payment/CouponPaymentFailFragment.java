/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProduct;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class CouponPaymentFailFragment extends CouponPaymentBaseFragment {
    public static final String CLASS_NAME = CouponPaymentFailFragment.class.getName();
    private static final String TAG = CouponPaymentFailFragment.class.getSimpleName();

    private static final String KEY_COUPON = "COUPON";
    private static final String KEY_COUPON_ERROR_MESSAGE = "KEY_COUPON_ERROR_MESSAGE";

    public static CouponPaymentFailFragment newInstance(CouponProduct couponItem, String errorMessage) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_COUPON, couponItem);
        args.putString(KEY_COUPON_ERROR_MESSAGE, errorMessage);

        CouponPaymentFailFragment fragment = new CouponPaymentFailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_coupon_shop_payment_fail;
    }

    @Override
    protected void initView(View view) {
        initLayout(view);
        initErrorCode(view);
    }

    private void initLayout(View view) {
        CouponProduct couponItem = getArguments().getParcelable(KEY_COUPON);

        String title = view.getResources().getString(R.string.coupon);
        try {
            title += " " + StringUtil.getFormattedNumber(couponItem.getChargedAmount()) + " " + view.getResources().getString(R.string.thb);
        }catch (Exception e){
        }
        ((TextView) view.findViewById(R.id.title)).setText(title);

        view.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getOnPaymentListener() != null) {
                    getOnPaymentListener().onFragmentDismiss();
                }
            }
        });
    }

    private void initErrorCode(View view) {
        try {
            String errorMessage = getArguments().getString(KEY_COUPON_ERROR_MESSAGE);
            TextView errorView = view.findViewById(R.id.error_view);
            if (errorMessage != null && !errorMessage.isEmpty()) {
                errorView.setText(errorMessage);
            } else {
                errorView.setText(getString(R.string.payment_error));
            }
        }catch (Exception e){
        }
    }
}