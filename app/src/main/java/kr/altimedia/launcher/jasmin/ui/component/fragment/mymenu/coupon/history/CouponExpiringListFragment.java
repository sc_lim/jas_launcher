
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.adapter.CouponListRowBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.presenter.CouponExpiringListRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class CouponExpiringListFragment extends CouponBaseListFragment {
    public static final String CLASS_NAME = CouponExpiringListFragment.class.getName();
    private final String TAG = CouponExpiringListFragment.class.getSimpleName();

    private final int VISIBLE_ROW_SIZE = 4;

    private TextView totalCashView;
    private TextView totalDiscountView;
//    private PagingVerticalGridView gridView;
    private ThumbScrollbar gridScrollbar;
    private CouponListRowBridgeAdapter gridBridgeAdapter;

    private LinearLayout listDataLayer;
    private LinearLayout emptyDataLayer;

    private ArrayList<Coupon> couponList = new ArrayList<>();
    private int totalDiscountSize = 0;
    private long totalBalance;

    private CouponExpiringListFragment() {
    }

    public static CouponExpiringListFragment newInstance(ArrayList<Coupon> list, int totalDiscountSize, long totalBalance) {
        CouponExpiringListFragment fragment = new CouponExpiringListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(CouponHistoryDialog.KEY_COUPON_LIST, list);
        args.putInt(CouponHistoryDialog.KEY_TOTAL_SIZE, totalDiscountSize);
        args.putLong(CouponHistoryDialog.KEY_TOTAL_BALANCE, totalBalance);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_coupon_history_list_expiring, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);

        loadData(getArguments());
    }

    private void initView(View view) {

        totalCashView = view.findViewById(R.id.totalCash);
        totalDiscountView = view.findViewById(R.id.totalDiscount);

        gridView = view.findViewById(R.id.gridView);
        gridScrollbar = view.findViewById(R.id.gridScrollbar);

        listDataLayer = view.findViewById(R.id.listDataLayer);
        emptyDataLayer = view.findViewById(R.id.emptyDataLayer);

        int verticalSpacing = (int) view.getResources().getDimension(R.dimen.mymenu_menu_list_vertical_space);
        gridView.setVerticalSpacing(verticalSpacing);
        gridView.initVertical(VISIBLE_ROW_SIZE);
    }

    @Override
    protected int getSelectedPosition() {
        return gridView.getSelectedPosition();
    }

    @Override
    protected int getTotalSize() {
        if(couponList != null) {
            return couponList.size();
        }
        return 0;
    }

    private void loadData(Bundle bundle) {
        couponList.clear();
        if(bundle != null) {
            ArrayList list = bundle.getParcelableArrayList(CouponHistoryDialog.KEY_COUPON_LIST);
            if (list != null) {
                couponList = list;
            }
            totalDiscountSize = bundle.getInt(CouponHistoryDialog.KEY_TOTAL_SIZE);
            totalBalance = bundle.getLong(CouponHistoryDialog.KEY_TOTAL_BALANCE);
        }

        setGridView();
    }

    private void setGridView() {
        if (couponList != null && !couponList.isEmpty()) {

            CouponExpiringListRowPresenter presenter = new CouponExpiringListRowPresenter();
            ArrayObjectAdapter listObjectAdapter = new ArrayObjectAdapter(presenter);
            listObjectAdapter.addAll(0, couponList);
            gridBridgeAdapter = new CouponListRowBridgeAdapter(listObjectAdapter, VISIBLE_ROW_SIZE);
            gridBridgeAdapter.setAdapter(listObjectAdapter);
            gridBridgeAdapter.setOnKeyListener(this);
            gridView.setAdapter(gridBridgeAdapter);

            gridScrollbar.setList(couponList.size(), VISIBLE_ROW_SIZE);
            gridView.setNumColumns(1);
            gridView.addOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {
                @Override
                public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                    super.onChildViewHolderSelected(parent, child, position, subposition);
                    gridScrollbar.moveFocus(position);
                }
            });
            gridScrollbar.setFocusable(false);

            totalDiscountView.setText(StringUtil.getFormattedNumber(totalDiscountSize));
            totalCashView.setText(StringUtil.getFormattedNumber(totalBalance));

            emptyDataLayer.setVisibility(View.GONE);
            listDataLayer.setVisibility(View.VISIBLE);

        } else {
            listDataLayer.setVisibility(View.GONE);
            emptyDataLayer.setVisibility(View.VISIBLE);
        }
    }
}