/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history.presenter;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.PurchasedContent;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;

public class PurchasedItemPresenter extends Presenter {

    private PurchasedItemPresenter.LoadWorkPosterListener mLoadWorkPosterListener;

    private OnKeyListener onKeyListener;
    private OnFocusListener onFocusListener;

    private boolean isEditMode = false;
    private String currentProfileId;

    public PurchasedItemPresenter(boolean isEditMode, OnKeyListener onKeyListener, OnFocusListener onFocusListener) {
        this.isEditMode = isEditMode;
        this.onKeyListener = onKeyListener;
        this.onFocusListener = onFocusListener;
        if(this.isEditMode) {
            this.currentProfileId = AccountManager.getInstance().getProfileId();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        VodItemViewHolder mVodItemViewHolder = new VodItemViewHolder(inflater.inflate(R.layout.item_mymenu_purchased_content, null, false));
        return mVodItemViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        VodItemViewHolder itemViewHolder = (VodItemViewHolder) viewHolder;
        PurchasedContent purchasedItem = (PurchasedContent) item;

        itemViewHolder.setItem(purchasedItem);

        View lockIcon = viewHolder.view.findViewById(R.id.iconVodLock);
        if (lockIcon != null) {
            boolean isLocked = isLockedByRating(purchasedItem.getRating());
            lockIcon.setVisibility(isLocked ? View.VISIBLE : View.GONE);
        }

//        boolean isLocked = isLockedByRating(purchasedItem.getRating());
//        if (isLocked) {
//            itemViewHolder.getMainImageView().setImageResource(R.drawable.poster_block);
//        } else {
        Glide.with(viewHolder.view.getContext())
                .load(purchasedItem.getPosterSourceUrl(Content.mPosterDefaultRect.width(),
                        Content.mPosterDefaultRect.height())).placeholder(R.drawable.vod_list_default)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .centerCrop()
                .into(itemViewHolder.getMainImageView());

        if (mLoadWorkPosterListener != null) {
            mLoadWorkPosterListener.onLoadWorkPoster(purchasedItem, itemViewHolder.getMainImageView());
        }
//        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        VodItemViewHolder cardView = (VodItemViewHolder) viewHolder;
        cardView.removeMainImage();
    }

    private boolean isLockedByRating(int rating) {
        ProfileInfo profileInfo = AccountManager.getInstance().getSelectedProfile();
        if (profileInfo == null) {
            return false;
        }
        int userRating = profileInfo.getRating();
        return userRating != 0 && rating >= userRating;
    }

    public void setLoadWorkPosterListener(PurchasedItemPresenter.LoadWorkPosterListener loadMoviePosterListener) {
        mLoadWorkPosterListener = loadMoviePosterListener;
    }

    public OnKeyListener getOnKeyListener() {
        return onKeyListener;
    }

    public interface LoadWorkPosterListener {
        void onLoadWorkPoster(PurchasedContent work, ImageView imageView);
    }

    public class VodItemViewHolder extends ViewHolder implements View.OnFocusChangeListener  {
        private final String TAG = VodItemViewHolder.class.getSimpleName();

        private PurchasedContent purchasedItem;

        private ImageView mainImageView = null;
        private RelativeLayout mainDimView = null;
        private TextView titleView = null;
        public CheckBox checkBox = null;

        public VodItemViewHolder(View view) {
            super(view);
            mainImageView = view.findViewById(R.id.mainImage);
            mainDimView = view.findViewById(R.id.mainDim);
            titleView = view.findViewById(R.id.titleLayer);
            checkBox = view.findViewById(R.id.checkBox);
            view.setOnFocusChangeListener(this);
        }

        public ImageView getMainImageView() {
            return mainImageView;
        }

        public void setItem(PurchasedContent item) {
            purchasedItem = item;
            PurchasedContent content = purchasedItem;
            if(isEditMode) {
                if(currentProfileId != null && currentProfileId.equals(purchasedItem.getProfileId())) {
                    checkBox.setChecked(item.isHidden());
                    checkBox.setVisibility(View.VISIBLE);
                }
                mainDimView.setVisibility(View.VISIBLE);
            }
            this.titleView.setText(content.getTitle());
        }

        public void removeMainImage() {
            mainImageView.setImageDrawable(null);
        }

        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            Animator animator = loadAnimator(view.getContext(), hasFocus ? R.animator.scale_up_50 : R.animator.scale_down_50);
            animator.setTarget(view);
            animator.start();

            if(hasFocus){
                if(onFocusListener != null) {
                    onFocusListener.onFocus(purchasedItem);
                }
                titleView.setSelected(true);
            }else{
                titleView.setSelected(false);
            }
        }

        private Animator loadAnimator(Context context, int resId) {
            Animator animator = AnimatorInflater.loadAnimator(context, resId);
            return animator;
        }

    }

    public interface OnKeyListener {
        boolean onKey(int keyCode, int index, Object item, ViewHolder viewHolder);
    }

    public interface OnFocusListener {
        void onFocus(Object item);
    }
}