/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.adapter;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.leanback.widget.FacetProvider;
import androidx.leanback.widget.FacetProviderAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.FocusHighlightHandler;

/**
 * Created by mc.kim on 10,01,2020
 */
@Deprecated
public class IndicatedItemAdapter extends RecyclerView.Adapter implements FacetProviderAdapter {
    static final String TAG = IndicatedItemAdapter.class.getSimpleName();
    static final boolean DEBUG = false;
    private ObjectAdapter mAdapter;
    IndicatedItemAdapter.Wrapper mWrapper;
    private PresenterSelector mPresenterSelector;
    FocusHighlightHandler mFocusHighlight;
    private IndicatedItemAdapter.AdapterListener mAdapterListener;
    private ArrayList<Presenter> mPresenters;
    private ObjectAdapter.DataObserver mDataObserver;
    private RecyclerView mRecyclerView = null;

    public enum Direction {
        LEFT, RIGHT, UP, DOWN
    }

    public void setAttachedRecyclerView(RecyclerView recyclerView) {
        this.mRecyclerView = recyclerView;
        this.mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                boolean checkRight = recyclerView.canScrollHorizontally(1);
                boolean checkLeft = recyclerView.canScrollHorizontally(-1 );
                Log.d(TAG,"recyclerView.canScrollVertically(-1) : "+checkRight);
                Log.d(TAG,"recyclerView.canScrollVertically(1) : "+checkLeft);
            }
        });
    }

    private IndicatedItemAdapter.OnIndicateChangedListener mIndicateChangedListener = null;

    public IndicatedItemAdapter(ObjectAdapter adapter, PresenterSelector presenterSelector) {
        this.mPresenters = new ArrayList();
        this.mDataObserver = new ObjectAdapter.DataObserver() {
            public void onChanged() {
                IndicatedItemAdapter.this.notifyDataSetChanged();
            }

            public void onItemRangeChanged(int positionStart, int itemCount) {
                IndicatedItemAdapter.this.notifyItemRangeChanged(positionStart, itemCount);
            }

            public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
                IndicatedItemAdapter.this.notifyItemRangeChanged(positionStart, itemCount, payload);
            }

            public void onItemRangeInserted(int positionStart, int itemCount) {
                IndicatedItemAdapter.this.notifyItemRangeInserted(positionStart, itemCount);
            }

            public void onItemRangeRemoved(int positionStart, int itemCount) {
                IndicatedItemAdapter.this.notifyItemRangeRemoved(positionStart, itemCount);
            }

            public void onItemMoved(int fromPosition, int toPosition) {
                IndicatedItemAdapter.this.notifyItemMoved(fromPosition, toPosition);
            }
        };
        this.setAdapter(adapter);
        this.mPresenterSelector = presenterSelector;
    }

    public IndicatedItemAdapter(ObjectAdapter adapter) {
        this(adapter, (PresenterSelector) null);
    }


    public IndicatedItemAdapter() {
        this.mPresenters = new ArrayList();
        this.mDataObserver = new ObjectAdapter.DataObserver() {
            public void onChanged() {
                IndicatedItemAdapter.this.notifyDataSetChanged();
            }

            public void onItemRangeChanged(int positionStart, int itemCount) {
                IndicatedItemAdapter.this.notifyItemRangeChanged(positionStart, itemCount);
            }

            public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
                IndicatedItemAdapter.this.notifyItemRangeChanged(positionStart, itemCount, payload);
            }

            public void onItemRangeInserted(int positionStart, int itemCount) {
                IndicatedItemAdapter.this.notifyItemRangeInserted(positionStart, itemCount);
            }

            public void onItemRangeRemoved(int positionStart, int itemCount) {
                IndicatedItemAdapter.this.notifyItemRangeRemoved(positionStart, itemCount);
            }

            public void onItemMoved(int fromPosition, int toPosition) {
                IndicatedItemAdapter.this.notifyItemMoved(fromPosition, toPosition);
            }
        };
    }

    public ObjectAdapter getAdapter() {
        return mAdapter;
    }

    public void setAdapter(ObjectAdapter adapter) {
        if (adapter != this.mAdapter) {
            if (this.mAdapter != null) {
                this.mAdapter.unregisterObserver(this.mDataObserver);
            }

            this.mAdapter = adapter;
            if (this.mAdapter == null) {
                this.notifyDataSetChanged();
            } else {
                this.mAdapter.registerObserver(this.mDataObserver);
                if (this.hasStableIds() != this.mAdapter.hasStableIds()) {
                    this.setHasStableIds(this.mAdapter.hasStableIds());
                }

                this.notifyDataSetChanged();
            }
        }
    }

    public void setPresenter(PresenterSelector presenterSelector) {

        this.mPresenterSelector = presenterSelector;
        this.notifyDataSetChanged();
    }

    public void setWrapper(IndicatedItemAdapter.Wrapper wrapper) {
        this.mWrapper = wrapper;
    }

    public IndicatedItemAdapter.Wrapper getWrapper() {
        return this.mWrapper;
    }

    public void setFocusHighlight(FocusHighlightHandler listener) {
        this.mFocusHighlight = listener;
    }

    public void clear() {
        this.setAdapter((ObjectAdapter) null);
    }

    public void setPresenterMapper(ArrayList<Presenter> presenters) {
        this.mPresenters = presenters;
    }

    public ArrayList<Presenter> getPresenterMapper() {
        return this.mPresenters;
    }

    public int getItemCount() {
        return this.mAdapter != null ? this.mAdapter.size() : 0;
    }

    public int getItemViewType(int position) {
        PresenterSelector presenterSelector = this.mPresenterSelector != null ? this.mPresenterSelector : this.mAdapter.getPresenterSelector();
        Object item = this.mAdapter.get(position);
        Presenter presenter = presenterSelector.getPresenter(item);
        int type = this.mPresenters.indexOf(presenter);
        if (type < 0) {
            this.mPresenters.add(presenter);
            type = this.mPresenters.indexOf(presenter);
            this.onAddPresenter(presenter, type);
            if (this.mAdapterListener != null) {
                this.mAdapterListener.onAddPresenter(presenter, type);
            }
        }

        return type;
    }

    protected void onAddPresenter(Presenter presenter, int type) {
    }

    protected void onCreate(IndicatedItemAdapter.ViewHolder viewHolder) {
    }

    protected void onBind(IndicatedItemAdapter.ViewHolder viewHolder) {
    }

    protected void onUnbind(IndicatedItemAdapter.ViewHolder viewHolder) {
    }

    protected void onAttachedToWindow(IndicatedItemAdapter.ViewHolder viewHolder) {
    }

    protected void onDetachedFromWindow(IndicatedItemAdapter.ViewHolder viewHolder) {
    }

    public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG,"onCreateViewHolder");
        Presenter presenter = (Presenter) this.mPresenters.get(viewType);
        Presenter.ViewHolder presenterVh;
        View view;
        if (this.mWrapper != null) {
            view = this.mWrapper.createWrapper(parent);
            presenterVh = presenter.onCreateViewHolder(parent);
            this.mWrapper.wrap(view, presenterVh.view);
        } else {
            presenterVh = presenter.onCreateViewHolder(parent);
            view = presenterVh.view;
        }

        IndicatedItemAdapter.ViewHolder viewHolder = new IndicatedItemAdapter.ViewHolder(presenter, view, presenterVh);
        this.onCreate(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onCreate(viewHolder);
        }

        View presenterView = viewHolder.mHolder.view;
        if (presenterView != null) {
            Log.d(TAG,"onCreateViewHolder presenterView != null : "+presenterView.getClass().getSimpleName());
            viewHolder.mFocusChangeListener.mChainedListener = presenterView.getOnFocusChangeListener();
            presenterView.setFocusable(true);
            presenterView.setFocusableInTouchMode(true);
            presenterView.setOnFocusChangeListener(viewHolder.mFocusChangeListener);
        }

        if (this.mFocusHighlight != null) {
            this.mFocusHighlight.onInitializeView(view);
        }

        return viewHolder;
    }

    public void setAdapterListener(IndicatedItemAdapter.AdapterListener listener) {
        this.mAdapterListener = listener;
    }

    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        IndicatedItemAdapter.ViewHolder viewHolder = (IndicatedItemAdapter.ViewHolder) holder;
        viewHolder.itemView.setTag(R.id.KEY_INDEX,position);
        viewHolder.mItem = this.mAdapter.get(position);
        viewHolder.mPresenter.onBindViewHolder(viewHolder.mHolder, viewHolder.mItem);
        this.onBind(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onBind(viewHolder);
        }
    }


    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List payloads) {
        Log.d(TAG, "onBindViewHolder : " + holder + ", position : " + position);
        IndicatedItemAdapter.ViewHolder viewHolder = (IndicatedItemAdapter.ViewHolder) holder;
        ((IndicatedItemAdapter.ViewHolder) holder).mHolder.view.setSelected(false);
        viewHolder.itemView.setTag(R.id.KEY_INDEX,position);
        viewHolder.mItem = this.mAdapter.get(position);
        viewHolder.mPresenter.onBindViewHolder(viewHolder.mHolder, viewHolder.mItem, payloads);
        this.onBind(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onBind(viewHolder, payloads);
        }

    }

    public final void onViewRecycled(RecyclerView.ViewHolder holder) {
        IndicatedItemAdapter.ViewHolder viewHolder = (IndicatedItemAdapter.ViewHolder) holder;
        viewHolder.mPresenter.onUnbindViewHolder(viewHolder.mHolder);
        viewHolder.mHolder.view.setOnKeyListener(null);
        this.onUnbind(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onUnbind(viewHolder);
        }

        viewHolder.mItem = null;
    }

    public final boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
        this.onViewRecycled(holder);
        return false;
    }

    public final void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        IndicatedItemAdapter.ViewHolder viewHolder = (IndicatedItemAdapter.ViewHolder) holder;
        this.onAttachedToWindow(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onAttachedToWindow(viewHolder);
        }

        viewHolder.mPresenter.onViewAttachedToWindow(viewHolder.mHolder);
    }

    public final void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        IndicatedItemAdapter.ViewHolder viewHolder = (IndicatedItemAdapter.ViewHolder) holder;
        viewHolder.mPresenter.onViewDetachedFromWindow(viewHolder.mHolder);
        this.onDetachedFromWindow(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onDetachedFromWindow(viewHolder);
        }

    }

    public long getItemId(int position) {
        return this.mAdapter.getId(position);
    }

    public FacetProvider getFacetProvider(int type) {
        return (FacetProvider) this.mPresenters.get(type);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements FacetProvider {
        public final Presenter mPresenter;
        public final Presenter.ViewHolder mHolder;
        public final IndicatedItemAdapter.OnFocusChangeListener mFocusChangeListener = new OnFocusChangeListener();
        public Object mItem;
        public Object mExtraObject;

        public final Presenter getPresenter() {
            return this.mPresenter;
        }

        public final Presenter.ViewHolder getViewHolder() {
            return this.mHolder;
        }

        public final Object getItem() {
            return this.mItem;
        }

        public final Object getExtraObject() {
            return this.mExtraObject;
        }

        public void setExtraObject(Object object) {
            this.mExtraObject = object;
        }

        public Object getFacet(Class<?> facetClass) {
            return this.mHolder.getFacet(facetClass);
        }

        ViewHolder(Presenter presenter, View view, Presenter.ViewHolder holder) {
            super(view);
            this.mPresenter = presenter;
            this.mHolder = holder;
        }
    }

    final class OnFocusChangeListener implements View.OnFocusChangeListener {
        View.OnFocusChangeListener mChainedListener;

        OnFocusChangeListener() {
        }

        public void onFocusChange(View view, boolean hasFocus) {
            if (IndicatedItemAdapter.this.mWrapper != null) {
                view = (View) view.getParent();
            }


            if (IndicatedItemAdapter.this.mFocusHighlight != null) {
                IndicatedItemAdapter.this.mFocusHighlight.onItemFocused(view, hasFocus);
            }

            if (this.mChainedListener != null) {
                this.mChainedListener.onFocusChange(view, hasFocus);
            }


        }
    }

    public abstract static class Wrapper {
        public Wrapper() {
        }

        public abstract View createWrapper(View var1);

        public abstract void wrap(View var1, View var2);
    }

    public static class AdapterListener {
        public AdapterListener() {
        }

        public void onAddPresenter(Presenter presenter, int type) {
        }

        public void onCreate(IndicatedItemAdapter.ViewHolder viewHolder) {
        }

        public void onBind(IndicatedItemAdapter.ViewHolder viewHolder) {
        }

        public void onBind(IndicatedItemAdapter.ViewHolder viewHolder, List payloads) {
            this.onBind(viewHolder);
        }

        public void onUnbind(IndicatedItemAdapter.ViewHolder viewHolder) {
        }

        public void onAttachedToWindow(IndicatedItemAdapter.ViewHolder viewHolder) {
        }

        public void onDetachedFromWindow(IndicatedItemAdapter.ViewHolder viewHolder) {
        }
    }

    public void setIndicateChangedListener(IndicatedItemAdapter.OnIndicateChangedListener indicateChangedListener) {
        this.mIndicateChangedListener = indicateChangedListener;
    }

    private void notifyIndicateStateChange(@NonNull RecyclerView.LayoutManager pLayoutManager, int currentIndex) {
        if (mIndicateChangedListener == null) {
            return;
        }
        if (pLayoutManager == null) {
            return;
        }


        if (pLayoutManager instanceof StaggeredGridLayoutManager) {
            throw new IllegalArgumentException("StaggeredGridLayoutManager not supported this function");
        }
        int orientation = 0;
        int firstVisibleItem = 0;
        int lastVisibleItem = 0;
        int itemCount = 0;
        if (pLayoutManager instanceof GridLayoutManager) {
            GridLayoutManager linearLayoutManager = (GridLayoutManager) pLayoutManager;
            orientation = linearLayoutManager.getOrientation();
            firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
            lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        } else if (pLayoutManager instanceof LinearLayoutManager) {
            LinearLayoutManager linearLayoutManager = (LinearLayoutManager) pLayoutManager;
            orientation = linearLayoutManager.getOrientation();
            firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
            lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
        }

        boolean canScrollPreIndex = firstVisibleItem != 0;
        boolean canScrollNextIndex = lastVisibleItem >= itemCount - 1;

        if (orientation == RecyclerView.HORIZONTAL) {
            /*LEFT. RIGHT*/
            mIndicateChangedListener.notifyStateChanged(IndicatedItemAdapter.Direction.LEFT, canScrollPreIndex);
            mIndicateChangedListener.notifyStateChanged(IndicatedItemAdapter.Direction.RIGHT, canScrollNextIndex);
        } else {
            /*UP. DOWN*/
            mIndicateChangedListener.notifyStateChanged(IndicatedItemAdapter.Direction.UP, canScrollPreIndex);
            mIndicateChangedListener.notifyStateChanged(IndicatedItemAdapter.Direction.DOWN, canScrollNextIndex);
        }
    }

    public interface OnIndicateChangedListener {
        void notifyStateChanged(Direction direction, boolean canScroll);
    }

}
