/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.view.common.ScrollDescriptionTextButton;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;

public class SubscriptionTermsDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = SubscriptionTermsDialogFragment.class.getName();
    private static final String TAG = SubscriptionTermsDialogFragment.class.getSimpleName();

    private static final String KEY_DESC = "DESC";
    private final int MAX_LINE_COUNT = 7;

    private OnClickListener onClickListener;

    private final ScrollDescriptionTextButton.OnButtonKeyListener mOnButtonKeyListener = new ScrollDescriptionTextButton.OnButtonKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    boolean isAgree = v.getId() == R.id.agree;
                    onClickListener.isAgree(isAgree);
                    return true;
            }
            return false;
        }
    };

    public SubscriptionTermsDialogFragment() {

    }

    public static SubscriptionTermsDialogFragment newInstance(String desc) {
        Bundle args = new Bundle();
        args.putString(KEY_DESC, desc);

        SubscriptionTermsDialogFragment fragment = new SubscriptionTermsDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_subscription_terms, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        initScroll(view);
    }

    private void initScroll(View view) {
        TextView desc = view.findViewById(R.id.desc);
        String text = getArguments().getString(KEY_DESC);
        desc.setText(text);

        ThumbScrollbar scrollbar = view.findViewById(R.id.scrollbar);
        desc.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                scrollbar.setList(desc.getLineCount(), MAX_LINE_COUNT);
            }
        });

        desc.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                boolean isMove = scrollY > oldScrollY;
                scrollbar.moveScroll(isMove);
            }
        });

        ScrollDescriptionTextButton agree = view.findViewById(R.id.agree);
        ScrollDescriptionTextButton cancel = view.findViewById(R.id.disagree);

        agree.setDescriptionTextView(desc);
        cancel.setDescriptionTextView(desc);

        agree.setOnButtonKeyListener(mOnButtonKeyListener);
        cancel.setOnButtonKeyListener(mOnButtonKeyListener);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onClickListener = null;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public interface OnClickListener {
        void isAgree(boolean isAgree);
    }
}
