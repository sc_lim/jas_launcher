/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.remote;


import com.altimedia.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.MbsUtil;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.payment.api.PaymentApi;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Payment;
import kr.altimedia.launcher.jasmin.dm.payment.obj.PaymentPromotion;
import kr.altimedia.launcher.jasmin.dm.payment.obj.RequestVodPaymentResultInfo;
import kr.altimedia.launcher.jasmin.dm.payment.obj.VodPaymentResult;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.AvailablePaymentTypeResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.BankResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.CreditCardResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.PaymentPromotionResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.PaymentResultResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.RequestVodPaymentResponse;
import kr.altimedia.launcher.jasmin.dm.payment.type.ContentType;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import retrofit2.Call;
import retrofit2.Response;

public class PaymentRemote {
    private static final String TAG = PaymentRemote.class.getSimpleName();
    private PaymentApi mPaymentApi;

    public PaymentRemote(PaymentApi mPaymentApi) {
        this.mPaymentApi = mPaymentApi;
    }

    public List<PaymentType> getAvailablePaymentType(String said, String profileId,
                                                     String offerId, String contentType) throws IOException, ResponseException, AuthenticationException {
        Call<AvailablePaymentTypeResponse> call = mPaymentApi.getAvailablePaymentType(said, profileId, offerId, contentType);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<AvailablePaymentTypeResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        AvailablePaymentTypeResponse mAvailablePaymentTypeResponse = response.body();
        return mAvailablePaymentTypeResponse.getAvailablePaymentTypeList();
    }

    public List<PaymentPromotion> getPaymentPromotion() throws IOException, ResponseException, AuthenticationException {
        Call<PaymentPromotionResponse> call = mPaymentApi.getPaymentPromotion();
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<PaymentPromotionResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        PaymentPromotionResponse mPaymentPromotionResponse = response.body();
        return mPaymentPromotionResponse.getPromotionList();
    }

    public CreditCardResponse.CreditCardResult getCreditCardListResult(String said) throws IOException, ResponseException, AuthenticationException {
        Call<CreditCardResponse> call = mPaymentApi.getCreditCardList(said);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<CreditCardResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        CreditCardResponse cardResponse = response.body();

        return cardResponse.getResult();
    }

    public BankResponse.BankResult getBankListResult() throws IOException, ResponseException, AuthenticationException {
        Call<BankResponse> call = mPaymentApi.getBankList();
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BankResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BankResponse bankResponse = response.body();
        return bankResponse.getResult();
    }

    public RequestVodPaymentResultInfo requestVodPayment(String said, String profileId, ContentType contentType,
                                                         String offerId, List<Payment> payments, float totalPrice)
            throws IOException, ResponseException, AuthenticationException {
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(payments, new TypeToken<List<Payment>>() {
        }.getType());
        JsonArray jsonArray = element.getAsJsonArray();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("contentType", contentType.getType());
        jsonObject.addProperty("offerId", offerId);
        jsonObject.add("payment", jsonArray);
        jsonObject.addProperty("totalPrice", (int) totalPrice);

        Call<RequestVodPaymentResponse> call = mPaymentApi.requestVodPayment(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
            Log.d(TAG, "call : " + new Gson().toJson(jsonObject));
        }
        Response<RequestVodPaymentResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        RequestVodPaymentResponse mRequestVodPaymentResponse = response.body();
        return mRequestVodPaymentResponse.getRequestVodPaymentResultInfo();
    }

    public Boolean cancelVodPayment(String said, String profileId, String purchaseId)
            throws IOException, ResponseException, AuthenticationException {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("purchaseId", purchaseId);

        Call<BaseResponse> call = mPaymentApi.cancelVodPayment(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
            Log.d(TAG, "call : " + new Gson().toJson(jsonObject));
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse baseResponse = response.body();
        return baseResponse.getReturnCode().equalsIgnoreCase("s");
    }

    public VodPaymentResult getVodPaymentResult(String said, String profileId, String purchaseId) throws IOException,
            ResponseException, AuthenticationException {
        Call<PaymentResultResponse> call = mPaymentApi.getVodPaymentResult(purchaseId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<PaymentResultResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        PaymentResultResponse mVodPaymentResult = response.body();
        return mVodPaymentResult.getResult();
    }
}
