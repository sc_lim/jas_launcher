/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.system.service;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.TestOnly;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.user.AuthenticationManager;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.LoginResult;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileList;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.notice.NoticeAlertDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

public class PushMessageReceiver extends FirebaseMessagingService {
    private static final String TAG = PushMessageReceiver.class.getSimpleName();

    public enum MenuType {
        LiveTv, Guide
    }

    public static final String ACTION_JUMP_MENU = "kr.altimedia.intent.action.JumpMenu";
    public static final String ACTION_LOGIN = "kr.altimedia.intent.action.Update.Login";
    public static final String ACTION_UPDATED_FAVORITE_CHANNEL = "kr.altimedia.intent.action.Update.FavoriteChannel";
    public static final String ACTION_UPDATED_SUBSCRIBED_CHANNEL = "kr.altimedia.intent.action.Update.SubscribedChannel";
    public static final String ACTION_UPDATED_PURCHASE_INFORM = "kr.altimedia.intent.action.Update.PurchaseInformation";
    public static final String ACTION_UPDATED_COUPON = "kr.altimedia.intent.action.Update.CouponList";
    public static final String ACTION_UPDATED_PROFILE = "kr.altimedia.intent.action.Update.Profile";
    public static final String ACTION_UPDATE_VOD_FAVORITE = "kr.altimedia.intent.action.Update.VodFavorite";
    public static final String ACTION_UPDATED_MENU = "kr.altimedia.intent.action.Update.Menu";
    public static final String BUNDLE_KEY_ID = "id";
    public static final String KEY_MENU_TYPE = "menuType";
    public static final String KEY_TYPE = "type";
    public static final String KEY_CHANNEL_ID = "channelId";
    public static final String KEY_OFFER_ID = "offerId";
    public static final String KEY_PROFILE_ID = "profileId";
    public static final String KEY_MESSAGE_ID = "messageId";
    public static final String KEY_MESSAGE_TITLE = "title";
    public static final String KEY_MESSAGE_DESC = "description";
    public static final String KEY_CHANGED_LOGIN_USER_INFO = "changedLoginUserInfo";
    public static final String KEY_DATE = "date";

    private Intent generateIntent(Map<String, String> pushMap) {
        if (!pushMap.containsKey(KEY_TYPE)) {
            return new Intent();
        }
        String type = pushMap.get(KEY_TYPE);
        Intent intent = new Intent();
        switch (type) {
            case VALUE_PROFILE:
                intent.setAction(ACTION_UPDATED_PROFILE);
                intent.putExtra(KEY_TYPE, ProfileUpdateType.needToCheck);
                break;

            case VALUE_BLOCKED_CHANNEL:
                intent.setAction(ACTION_UPDATED_PROFILE);
                intent.putExtra(KEY_TYPE, ProfileUpdateType.blockedChannel);
                intent.putExtra(KEY_PROFILE_ID, pushMap.get(KEY_PROFILE_ID));
                break;
            case VALUE_FAVORITE_CHANNEL:
                intent.setAction(ACTION_UPDATED_FAVORITE_CHANNEL);
                break;
            case VALUE_SUBSCRIBED_CHANNEL:
                intent.setAction(ACTION_UPDATED_SUBSCRIBED_CHANNEL);
                intent.putExtra(KEY_CHANNEL_ID, pushMap.get(BUNDLE_KEY_ID));
                break;
            case VALUE_PURCHASE_RESULT:
                intent.setAction(ACTION_UPDATED_PURCHASE_INFORM);
                intent.putExtra(KEY_OFFER_ID, pushMap.get(BUNDLE_KEY_ID));
                intent.putExtra(KEY_MESSAGE_DESC, pushMap.get(KEY_MESSAGE_DESC));
                break;
            case ACTION_UPDATE_VOD_FAVORITE:
                intent.setAction(ACTION_UPDATE_VOD_FAVORITE);
                break;
            case VALUE_COUPON_ISSUED:
            case VALUE_COUPON_EXPIRED:
                intent.setAction(ACTION_UPDATED_COUPON);
                if (VALUE_COUPON_ISSUED.equalsIgnoreCase(type)) {
                    intent.putExtra(KEY_TYPE, CouponUpdateType.issued);
                } else if (VALUE_COUPON_EXPIRED.equalsIgnoreCase(type)) {
                    intent.putExtra(KEY_TYPE, CouponUpdateType.expired);
                }
                intent.putExtra(KEY_MESSAGE_ID, pushMap.get(BUNDLE_KEY_ID));
                intent.putExtra(KEY_MESSAGE_TITLE, pushMap.get(KEY_MESSAGE_TITLE));
                intent.putExtra(KEY_MESSAGE_DESC, pushMap.get(KEY_MESSAGE_DESC));
                intent.putExtra(KEY_DATE, pushMap.get(KEY_DATE));
                break;
            case VALUE_NOTIFICATION:
                NoticeAlertDialogFragment mNoticeAlertDialogFragment = new NoticeAlertDialogFragment(getApplicationContext(), pushMap);
                mNoticeAlertDialogFragment.show();
                break;
        }
        return intent;
    }

    public enum ProfileUpdateType {
        login, rating, lock, add, delete, blockedChannel, needToCheck, userDeleted
    }

    public enum CouponUpdateType {
        issued, expired
    }

    private final String VALUE_BLOCKED_CHANNEL = "blockedChannel";
    private final String VALUE_FAVORITE_CHANNEL = "favoriteChannel";
    private final String VALUE_SUBSCRIBED_CHANNEL = "subscribedChannel";
    private final String VALUE_PURCHASE_RESULT = "purchaseResult";
    private final String VALUE_COUPON_ISSUED = "couponIssued";
    private final String VALUE_COUPON_EXPIRED = "couponExpired";
    private final String VALUE_PROFILE = "profile";
    private final String VALUE_NOTIFICATION = "notification";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onMessageReceived | remoteMessage == null so return");
            }
            return;
        }

        if (remoteMessage.getNotification() != null) {
            String notificationBody = remoteMessage.getNotification().getBody();
            if (Log.INCLUDE) {
                Log.d(TAG, "onMessageReceived | notificationBody : " + notificationBody);
            }
        }else{
            if(Log.INCLUDE) {
                Log.d(TAG, "onMessageReceived | null notification");
            }
        }

        if (remoteMessage.getData() != null) {
            Map<String, String> dataMap = remoteMessage.getData();
            if (Log.INCLUDE) {
                Iterator<String> keyIterator = dataMap.keySet().iterator();
                while (keyIterator.hasNext()) {
                    String key = keyIterator.next();
                    String value = dataMap.get(key);
                    Log.d(TAG, "onMessageReceived | data : key : " + key + ", value : " + value);
                }
            }

            Intent brIntent = generateIntent(dataMap);
            if (StringUtils.nullToEmpty(brIntent.getAction()).isEmpty()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onMessageReceived | action not defined so not send BroadcastEvent");
                }
                return;
            }

            if (needToTypeCheckAfterSend(brIntent)) {
                checkAndDefineActionSend(brIntent);
            } else {
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(brIntent);
            }

        }
    }

    private void checkAndDefineActionSend(Intent brIntent) {
        switch (brIntent.getAction()) {
            case ACTION_UPDATED_PROFILE:
//                AuthenticationManager authenticationManager = new AuthenticationManager(getApplicationContext());
//                authenticationManager.getLoginToken(AccountManager.getInstance().getSaId(), new MbsDataProvider<String, LoginResult>() {
//                    @Override
//                    public void needLoading(boolean loading) {
//
//                    }
//
//                    @Override
//                    public void onFailed(int reason) {
//
//                    }
//
//                    @Override
//                    public void onSuccess(String id, LoginResult result) {
//                        Intent defineIntent = defineProfileUpdateType(brIntent, result.getProfileInfoList());
//                        if (defineIntent == null) {
//                            return;
//                        }
//                        AccountManager.getInstance().updateProfileList(result.getProfileInfoList());
//                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(defineIntent);
//                    }
//
//                    @Override
//                    public void onError(String errorCode, String message) {
//
//                    }
//                });

                UserDataManager userDataManager = new UserDataManager();
                userDataManager.getProfileList(AccountManager.getInstance().getSaId(), new MbsDataProvider<String, ProfileList>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkAndDefineActionSend needLoading");
                        }
                    }

                    @Override
                    public void onFailed(int reason) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkAndDefineActionSend onFailed : " + reason);
                        }
                    }

                    @Override
                    public void onSuccess(String id, ProfileList result) {
                        Intent defineIntent = defineProfileUpdateType(brIntent, result.getProfileInfoList());
                        if (defineIntent == null) {
                            return;
                        }
                        AccountManager.getInstance().updateProfileList(result.getProfileInfoList());
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(defineIntent);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkAndDefineActionSend onError : " + errorCode + ", message : " + message);
                        }
                    }
                });
                break;

            default:
                if (Log.INCLUDE) {
                    Log.d(TAG, "checkAndDefineActionSend wrong call");
                }
                break;
        }
    }

    private static boolean isCurrentUserDeleted(List<ProfileInfo> result) {
        String currentProfileId = AccountManager.getInstance().getProfileId();
        for (ProfileInfo info : result) {
            if (info.getProfileId().equalsIgnoreCase(currentProfileId)) {
                return false;
            }
        }
        return true;
    }


    private static  Intent defineProfileUpdateType(Intent brIntent, List<ProfileInfo> result) {
        List<ProfileInfo> list = AccountManager.getInstance().getProfileList();
        if (Log.INCLUDE) {
            Log.d(TAG, "defineProfileUpdateType | size check : " + (list.size() == result.size()));
        }
        if (list.size() == result.size()) {
            Pair<ProfileUpdateType, ProfileInfo> invalidInfo = findInvalidInfo(result, list);
            if (Log.INCLUDE) {
                Log.d(TAG, "defineProfileUpdateType | invalidInfo : " + (invalidInfo == null));
            }
            if (invalidInfo != null) {
                String profileId = invalidInfo.second.getProfileId();
                ProfileUpdateType type = invalidInfo.first;
                if (Log.INCLUDE) {
                    Log.d(TAG, "defineProfileUpdateType | ProfileUpdateType : " + type + ", profileId : " + profileId);
                }
                brIntent.putExtra(KEY_TYPE, type);
                brIntent.putExtra(KEY_PROFILE_ID, profileId);
                brIntent.putExtra(KEY_CHANGED_LOGIN_USER_INFO, profileId.equalsIgnoreCase(AccountManager.getInstance().getProfileId()));
                return brIntent;
            }
            return null;
        } else {
            if (isCurrentUserDeleted(result)) {
                brIntent.putExtra(KEY_TYPE, ProfileUpdateType.userDeleted);
            } else {
                brIntent.putExtra(KEY_TYPE, list.size() > result.size() ? ProfileUpdateType.delete : ProfileUpdateType.add);
            }
        }
        return brIntent;
    }

    private static ProfileInfo findProfileById(String id, List<ProfileInfo> saveList) {
        for (ProfileInfo profileInfo : saveList) {

            if (profileInfo.getProfileId().equalsIgnoreCase(id)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "profileInfo.getProfileId().equalsIgnoreCase(id) :  " + id);
                }
                return profileInfo;
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "not findProfileById : " + id + ", profileInfo : " + profileInfo.getProfileId());
            }
        }
        return null;
    }


    private static Pair<ProfileUpdateType, ProfileInfo> findInvalidInfo(List<ProfileInfo> newList, List<ProfileInfo> saveList) {
        for (ProfileInfo profileInfo : newList) {
            ProfileInfo info = findProfileById(profileInfo.getProfileId(), saveList);
            if (info == null) {
                continue;
            }
            if (info.getRating() != profileInfo.getRating()) {
                return new Pair<>(ProfileUpdateType.rating, info);
            }

            if (info.isLock() != profileInfo.isLock()) {
                return new Pair<>(ProfileUpdateType.lock, info);
            }
        }
        return null;
    }

    private boolean needToTypeCheckAfterSend(Intent brIntent) {
        Serializable serializable = brIntent.getSerializableExtra(KEY_TYPE);
        if (serializable instanceof ProfileUpdateType) {
            ProfileUpdateType updateType = (ProfileUpdateType) serializable;
            return updateType == ProfileUpdateType.needToCheck;
        }
        return false;
    }

    @Override
    public void onSendError(@NonNull String s, @NonNull Exception e) {
        super.onSendError(s, e);
        if (Log.INCLUDE) {
            Log.d(TAG, "onSendError | s : " + s + "exception : " + e.getMessage());
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        if (Log.INCLUDE) {
            Log.d(TAG, "onNewToken | s : " + s);
        }
        AccountManager.getInstance().putFcmToken(s);
    }

    private void showToast(String text) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                JasminToast.makeToast(getApplicationContext(), text);
            }
        });
    }


    @TestOnly
    public static void checkAndDefineActionSendForTest(Context context, Intent brIntent) {
        switch (brIntent.getAction()) {
            case ACTION_UPDATED_PROFILE:
                AuthenticationManager authenticationManager = new AuthenticationManager(context);
                authenticationManager.getLoginToken(AccountManager.getInstance().getSaId(), new MbsDataProvider<String, LoginResult>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int reason) {

                    }

                    @Override
                    public void onSuccess(String id, LoginResult result) {
                        Intent defineIntent = defineProfileUpdateType(brIntent, result.getProfileInfoList());
                        if (defineIntent == null) {
                            return;
                        }
                        AccountManager.getInstance().updateProfileList(result.getProfileInfoList());
                        LocalBroadcastManager.getInstance(context).sendBroadcast(defineIntent);
                    }

                    @Override
                    public void onError(String errorCode, String message) {

                    }
                });

                break;

            default:
                break;
        }
    }
}
