/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.channel.obj.MbsChannel;

/**
 * Created by mc.kim on 11,05,2020
 */
public class ChannelListResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private MbsChannelResult result;

    protected ChannelListResponse(Parcel in) {
        super(in);
        result = in.readTypedObject(MbsChannelResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(result, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChannelListResponse> CREATOR = new Creator<ChannelListResponse>() {
        @Override
        public ChannelListResponse createFromParcel(Parcel in) {
            return new ChannelListResponse(in);
        }

        @Override
        public ChannelListResponse[] newArray(int size) {
            return new ChannelListResponse[size];
        }
    };

    public List<MbsChannel> getChannelList() {
        return result.channelList;
    }

    @Override
    public String toString() {
        return "ChannelListResponse{" +
                "result=" + result +
                '}';
    }

    private static class MbsChannelResult implements Parcelable {
        @Expose
        @SerializedName("channelList")
        private List<MbsChannel> channelList;

        protected MbsChannelResult(Parcel in) {
            channelList = in.createTypedArrayList(MbsChannel.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(channelList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<MbsChannelResult> CREATOR = new Creator<MbsChannelResult>() {
            @Override
            public MbsChannelResult createFromParcel(Parcel in) {
                return new MbsChannelResult(in);
            }

            @Override
            public MbsChannelResult[] newArray(int size) {
                return new MbsChannelResult[size];
            }
        };

        @Override
        public String toString() {
            return "MbsChannelResult{" +
                    "channelList=" + channelList +
                    '}';
        }


    }
}
