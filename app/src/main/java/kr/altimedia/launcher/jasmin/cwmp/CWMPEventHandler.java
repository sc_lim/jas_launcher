/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.cwmp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.DeviceUtil;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPEventListener;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPSettingControl;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.ota.SystemUpdateManager;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.system.settings.data.ParentalRating;
import kr.altimedia.launcher.jasmin.system.settings.data.SubtitleBroadcastLanguage;
import kr.altimedia.launcher.jasmin.system.settings.data.SubtitleFontSize;
import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * CWMPEventHandler
 */
public class CWMPEventHandler implements CWMPEventListener {
    private static final String CLASS_NAME = CWMPEventHandler.class.getName();
    private static final String TAG = CWMPEventHandler.class.getSimpleName();

    private final int NA = -1;
    private final int REBOOT = 0;
    private final int REBOOT_ON_STANDBY = 1;
    private final long STB_IDLE_TIME = 10 * TimeUtil.MIN;
    private int rebootType = NA;
    private TimerTask mTask;
    private Timer mTimer;

    private Context context;
    private UserPreferenceManagerImp upManager;
    private final SettingControl settingControl = SettingControl.getInstance();
    private PowerStatusReceiver powerStatusReceiver;

    public CWMPEventHandler(Context context) {
        this.context = context;
        rebootType = NA;
        upManager = new UserPreferenceManagerImp(this.context);
        powerStatusReceiver = new PowerStatusReceiver();
        startPowerStatusListener();
    }

    public void dispose() {
        stopTimer();
        rebootType = NA;
        if(powerStatusReceiver != null){
            powerStatusReceiver.dispose();
        }

        context = null;
        upManager = null;
    }

    private void startPowerStatusListener() {
        if (powerStatusReceiver != null) {
            powerStatusReceiver.start(context, new CWMPEventHandler.OnPowerStatusListener() {
                @Override
                public void onScreenOn() {
                    CWMPEventHandler.this.handleRebootOnStandby();
                }

                @Override
                public void onScreenOff() {
                    CWMPEventHandler.this.handleRebootOnStandby();
                }
            });
        }
    }

    @Override
    public void onSetChangeChannel(String channelNum) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSetChangeChannel");
        }

        try {
            TvSingletons mTvSingletons = TvSingletons.getSingletons(context);
            ArrayList<Channel> allChannelList = (ArrayList<Channel>) mTvSingletons.getChannelDataManager().getBrowsableChannelList();

            long channelNumber = -1;
            channelNumber = (channelNum == null || channelNum.isEmpty()) ? 0 : Long.parseLong(channelNum);
            if (channelNumber >= 0) {
                for (Channel channel : allChannelList) {
                    if (channelNumber == Long.parseLong(channel.getDisplayNumber())) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSetChangeChannel: channelNumber="+channelNumber);
                        }
//                        PlaybackUtil.startLiveTvActivityWithChannel(context, channel.getId());
                        Intent intent = new Intent(LiveTvActivity.ACTION_CHANNEL_CHANGE);
                        intent.putExtra(LiveTvActivity.KEY_CHANNEL_NUM, Long.toString(channelNumber));
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                        return;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSetAgeLimit(int value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSetAgeLimit");
        }

        try {
            ProfileInfo profileInfo = AccountManager.getInstance().getSelectedProfile();
            if (profileInfo == null) {
                return;
            }
            int rating = profileInfo.getRating();

            if (value == ParentalRating.LIMIT_18.getRating()) {
                rating = ParentalRating.LIMIT_18.getRating();
            } else if (value == ParentalRating.LIMIT_13.getRating()) {
                rating = ParentalRating.LIMIT_13.getRating();
            } else if (value == ParentalRating.LIMIT_6.getRating()) {
                rating = ParentalRating.LIMIT_6.getRating();
            } else if (value == ParentalRating.LIMIT_3.getRating()) {
                rating = ParentalRating.LIMIT_3.getRating();
            } else if (value == 19) {
                rating = ParentalRating.NO_LIMIT.getRating();
            }
            UserDataManager userDataManager = new UserDataManager();
            userDataManager.putProfileRating(
                    AccountManager.getInstance().getSaId(),
                    AccountManager.getInstance().getProfileId(),
                    String.valueOf(rating),
                    new MbsDataProvider<String, Boolean>() {
                        @Override
                        public void needLoading(boolean loading) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "setPr: needLoading: loading=" + loading);
                            }
                        }

                        @Override
                        public void onFailed(int key) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "setPr: onFailed");
                            }
                        }

                        @Override
                        public void onSuccess(String id, Boolean result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "setPr: onSuccess: result=" + result);
                            }
                        }

                        @Override
                        public void onError(String errorCode, String message) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "setPr: onError: errorCode=" + errorCode + ", message=" + message);
                            }
                        }
                    });
        }catch (Exception|Error e){
        }
    }

    @Override
    public void onSetChannelLimit(String value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSetChannelLimit");
        }
        // 콤마(,)로 구분
        ArrayList<Long> list = getChannelList(value, true);

        UserDataManager userDataManager = new UserDataManager();
        userDataManager.postBlockedChannelList(
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                list.toArray(new String[]{}),
                new MbsDataProvider<String, List<String>>() {

                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "SetBlocked: onFailed");
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<String> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "SetBlocked: onSuccess: result=" + result);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "SetBlocked: onError: errorCode="+errorCode+", message=" + message);
                        }
                    }
                });
    }

    @Override
    public void onSetFavoriteChannel(String value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSetFavoriteChannel");
        }
        // 콤마(,)로 구분
        ArrayList<Long> list = getChannelList(value, false);

        UserDataManager userDataManager = new UserDataManager();
        userDataManager.postFavoriteChannelList(
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                list.toArray(new Long[0]),
                new MbsDataProvider<String, List<Long>>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "SetFavorite: onFailed");
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<Long> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "SetFavorite: onSuccess: result=" + result);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "SetFavorite: onError: errorCode="+errorCode+", message=" + message);
                        }
                    }
                });
    }

    @Override
    public void onSetSubtitle(int value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSetSubtitle");
        }
        try {
            boolean enabled = settingControl.isSubtitleEnable();
            if (value == 0) { // 자막 OFF
                enabled = false;
            } else if (value == 1) { // ON
                enabled = true;
            }
            settingControl.setSubtitleEnable(enabled);
        }catch (Exception|Error e){
        }
    }

    @Override
    public void onSetFontSize(int value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSetFontSize");
        }

        try {
            boolean enabled = settingControl.isSubtitleEnable();
            String lang = StringUtils.nullToEmpty(settingControl.getSubtitleLanguageCode());
            String size = StringUtils.nullToEmpty(settingControl.getSubtitleLanguageFontSize());
            if (value == 0) { // 글자크기 작게
                size = SubtitleFontSize.SMALL.getKey();
            } else if (value == 1) { // 중간
                size = SubtitleFontSize.MEDIUM.getKey();
            } else if (value == 2) { // 크게
                size = SubtitleFontSize.LARGE.getKey();
            }
            settingControl.setSubtitleOption(enabled, lang, size);
        }catch (Exception|Error e){
        }
    }

    @Override
    public void onSetVisualLanguage(String value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSetVisualLanguage");
        }

        try {
            boolean enabled = settingControl.isSubtitleEnable();
            String lang = StringUtils.nullToEmpty(settingControl.getSubtitleLanguageCode());
            String size = StringUtils.nullToEmpty(settingControl.getSubtitleLanguageFontSize());
            if (value.equalsIgnoreCase("us")) { // us:영어
                lang = SubtitleBroadcastLanguage.ENG.getKey();
            }else{
                lang = SubtitleBroadcastLanguage.THAI.getKey();
            }
            settingControl.setSubtitleOption(enabled, lang, size);
        }catch (Exception|Error e){
        }
    }

    @Override
    public void onSetConfigInit(int value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSetConfigInit");
        }
        // 0 : default
        // 1 : App/설정 초기화
        // 2 : 사용자 정보 제외 설정 초기화
        // 3 : 개통정보를 제외한 초기화
        // 4 : Unbound Application 초기화
        if (value == 0) { // default
        } else if (value == 1 || value == 2) { // App/설정 초기화
            SettingControl.getInstance().setSaid("");
            rebootAfterReset();
        } else if (value == 3) { // App/설정 초기화
            rebootAfterReset();
        }
    }

    private void rebootAfterReset(){
        try{
            SettingControl.getInstance().reset();

            if(upManager == null){
                upManager = new UserPreferenceManagerImp(this.context);
            }
            upManager.reset();

            new Thread(){
                public void run(){
                    try{
                        Thread.sleep(2000);
                    }catch (Exception e){
                    }
                    DeviceUtil.reboot(context, "ucems_config_init_stb_reboot");
                }
            }.start();
        }catch (Exception e){
        }
    }

    @Override
    public void onSetReboot(int value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSetReboot");
        }
        rebootType = value;
        if (rebootType == REBOOT) { // 단말(앱) Reboot 명령 수행
            DeviceUtil.reboot(context, "ucems_stb_reboot");
        } else if (rebootType == REBOOT_ON_STANDBY) { // 단말(앱)이 대기상태일 경우 Reboot 명령 수행
            // 단말(앱)이 대기모드(Stand By)이고 다른 서비스앱들의 미사용상태가 10분 이상 유지될 경우(IDLE상태) Reboot 명령을 수행.
            handleRebootOnStandby();
        }
    }

    @Override
    public void onSetStandBy(int value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSetStandBy");
        }
        try {
            if (value == 1) { // 대기모드에서 시청모드로 ON
                if(powerStatusReceiver != null && powerStatusReceiver.isScreenOff()) {
                    SettingControl.getInstance().setPowerMode(context, CWMPSettingControl.POWER_MODE.RUNNING);
                }
            } else if (value == 2) { // 시청모드에서 대기모드로 OFF
                if(powerStatusReceiver != null && !powerStatusReceiver.isScreenOff()) {
                    SettingControl.getInstance().setPowerMode(context, CWMPSettingControl.POWER_MODE.STANDBY);
                }
            }
        }catch (Exception|Error e){
        }
    }

    @Override
    public void onFactoryReset() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onFactoryReset");
        }
        try {
            SettingControl.getInstance().factoryReset(context);
        }catch (Exception|Error e){
        }
    }

    @Override
    public void onReboot() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onReboot");
        }
        DeviceUtil.reboot(context, "ucems_rpc_stb_reboot");
    }

    @Override
    public void onAppRestart() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onReboot");
            DeviceUtil.reboot(context, "ucems_app_restart");
        }
    }

    @Override
    public void onUpgradeTrigger() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onUpgradeTrigger");
        }

        SystemUpdateManager.getInstance().checkSoftwareUpdate();
    }

    @Override
    public void onNormalPeriodUpdated(int value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onNormalPeriodUpdated, value=" + value);
        }

        SystemUpdateManager.getInstance().restartUpdateScheduler();
    }

    @Override
    public void onFwUrlUpdated(String value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onFwUrlUpdated");
        }

        SystemUpdateManager.getInstance().startFwUpgrade(value);
    }

    @Override
    public void onApkUrlUpdated(String value) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onApkUrlUpdated");
        }

        SystemUpdateManager.getInstance().startApkUpgrade(value);
    }

    private ArrayList getChannelList(String chNumList, boolean stringFormat) {
        ArrayList list = new ArrayList();
        try {
            if (chNumList != null && chNumList.length() > 0) {
                StringTokenizer tokenizer = new StringTokenizer(chNumList, ",");
                if (tokenizer != null){
                    TvSingletons mTvSingletons = TvSingletons.getSingletons(context);
                    ArrayList<Channel> allChannelList = (ArrayList<Channel>) mTvSingletons.getChannelDataManager().getBrowsableChannelList();
                    long channelNumber = -1;
                    while (tokenizer.hasMoreTokens()) {
                        channelNumber = Long.parseLong(tokenizer.nextToken());
                        for (Channel channel : allChannelList) {
                            if (channelNumber == Long.parseLong(channel.getDisplayNumber())) {
                                if(stringFormat) {
                                    list.add(channel.getServiceId());
                                }else{
                                    list.add(Long.parseLong(channel.getServiceId()));
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
        }

        return list;
    }

    private void handleRebootOnStandby() {
        if (powerStatusReceiver != null) {
            if (rebootType == REBOOT_ON_STANDBY && powerStatusReceiver.isScreenOff()) {
                startTimer(STB_IDLE_TIME);
            } else {
                stopTimer();
            }
        }
    }

    private void stopTimer() {
        if (Log.INCLUDE) {
            Log.d(TAG, "stopTimer");
        }
        if (mTimer != null) {
            mTimer.cancel();
        }
        if (mTask != null) {
            mTask.cancel();
        }
    }

    private void startTimer(long delay) {
        stopTimer();

        if (Log.INCLUDE) {
            Log.d(TAG, "startTimer: delay=" + delay);
        }
        mTask = new TimerTask() {
            @Override
            public void run() {
                if (powerStatusReceiver != null) {
                    if (rebootType == REBOOT_ON_STANDBY && powerStatusReceiver.isScreenOff()) {
                        DeviceUtil.reboot(context);
                        rebootType = NA;
                    }
                }
            }
        };

        mTimer = new Timer();
        mTimer.schedule(mTask, delay);
    }

    interface OnPowerStatusListener {
        void onScreenOn();
        void onScreenOff();
    }

    class PowerStatusReceiver extends BroadcastReceiver {
        private final String SCREEN_ON = Intent.ACTION_SCREEN_ON;
        private final String SCREEN_OFF = Intent.ACTION_SCREEN_OFF;

        private boolean isScreenOff;
        private Context context;
        private OnPowerStatusListener listener;

        void dispose() {
            try {
                if (context != null) {
                    context.unregisterReceiver(this);
                }
            } catch (Exception e) {
            }
            context = null;
        }

        void start(Context context, OnPowerStatusListener listener) {
            if (Log.INCLUDE) {
                Log.d(TAG, "start");
            }
            this.context = context;
            this.listener = listener;
            PowerManager powerManager = null;
            if (powerManager == null) {
                powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                isScreenOff = !powerManager.isInteractive();
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "start: isScreenOff="+isScreenOff);
            }
            try {
                IntentFilter filter = new IntentFilter();
                filter.addAction(Intent.ACTION_SCREEN_ON);
                filter.addAction(Intent.ACTION_SCREEN_OFF);
                context.registerReceiver(this, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * 대기모드 여부를 리턴합니다.
         *
         * @return
         */
        boolean isScreenOff() {
            return isScreenOff;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                boolean value = false;
                if (action.equals(SCREEN_ON)) {
                    value = false;
                } else if (action.equals(SCREEN_OFF)) {
                    value = true;
                }
                if(value == isScreenOff) return;
                isScreenOff = value;
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReceive: intent="+intent);
                }
                if(listener != null){
                    if(isScreenOff) {
                        listener.onScreenOff();
                    }else {
                        listener.onScreenOn();
                    }
                }
            }
        }
    }
}