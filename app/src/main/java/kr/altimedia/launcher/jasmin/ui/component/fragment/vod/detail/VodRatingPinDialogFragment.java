/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public class VodRatingPinDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = VodRatingPinDialogFragment.class.getName();
    private final String TAG = VodRatingPinDialogFragment.class.getSimpleName();

    private VodRatingPinFragment.onCompleteCheckPinListener onCompleteCheckPinListener;

    public VodRatingPinDialogFragment() {
    }

    public static VodRatingPinDialogFragment newInstance() {
        return new VodRatingPinDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {
                onCompleteCheckPinListener.onCompleteCheckPin(false);
            }
        };
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_vod_rating_pin, container, false);
    }

    public void setOnCompleteCheckPinListener(VodRatingPinFragment.onCompleteCheckPinListener onCompleteCheckPinListener) {
        this.onCompleteCheckPinListener = onCompleteCheckPinListener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        VodRatingPinFragment vodRatingPinFragment = VodRatingPinFragment.newInstance();
        vodRatingPinFragment.setOnCompleteCheckPinListener(onCompleteCheckPinListener);

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_layout, vodRatingPinFragment, VodRatingPinFragment.CLASS_NAME)
                .commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        onCompleteCheckPinListener = null;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }
}