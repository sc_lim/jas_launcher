/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;

public class TimeUtil {
    public static final long SEC = 1000;
    public static final long MIN = 60 * SEC;
    public static final long HOUR = 60 * MIN;
    public static final long DAY = 24 * HOUR;

    private static final String DEFAULT_DATE_FORMAT = "dd.MM.yyyy";

    public static long getHour(long duration) {
        return duration / HOUR;
    }

    public static long getMin(long duration) {
        return (duration % HOUR) / MIN;
    }

    public static long getSec(long duration) {
        return ((duration % HOUR) % MIN) / SEC;
    }

    public static long getHourTime(int hour) {
        return hour * HOUR;
    }

    public static long getMinTime(int min) {
        return min * MIN;
    }

    public static String getModifiedDate(long modified) {
        return getModifiedDate(modified, DEFAULT_DATE_FORMAT);
    }

    public static String getModifiedDate(long modified, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(new Date(modified));
    }

    public static String getDuration(long duration) {
        return getDuration(duration, "%02d:%02d:%02d");
    }

    public static String getDuration(long duration, String format) {
        if (duration <= 0) {
            return String.format(format, 0, 0, 0);
        }

        long h = getHour(duration);
        long m = getMin(duration);
        long s = m > 0 ? (duration % h) % m : 0;

        return String.format(format, h, m, s);
    }

    public static int getLeftDay(long endDate) {
        return getLeftDay(JasmineEpgApplication.SystemClock().currentTimeMillis(), endDate);
    }

    public static int getLeftDay(long startData, long endDate) {
        long diff = endDate - startData;

        if (diff <= 0) {
            return 0;
        }

        int daysLeft = (int) (diff / 24 * HOUR);
        return daysLeft;
    }
}
