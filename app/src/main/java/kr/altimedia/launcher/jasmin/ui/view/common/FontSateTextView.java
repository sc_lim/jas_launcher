/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;

public class FontSateTextView extends androidx.appcompat.widget.AppCompatTextView {
    private Typeface mFocusFont;
    private Typeface mSelectedFont;
    private Typeface mDefaultFont;

    public FontSateTextView(Context context) {
        this(context, null);
    }

    public FontSateTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FontSateTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getAttrs(attrs);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.FontSateTextView);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        int focusFontId = typedArray.getResourceId(R.styleable.FontSateTextView_font_state_focus, -1);
        int selectedFontId = typedArray.getResourceId(R.styleable.FontSateTextView_font_state_selected, -1);
        int defaultFontId = typedArray.getResourceId(R.styleable.FontSateTextView_font_state_default, -1);

        mFocusFont = getFontById(focusFontId);
        mSelectedFont = getFontById(selectedFontId);
        mDefaultFont = getFontById(defaultFontId);

        typedArray.recycle();
    }

    private Typeface getFontById(int id) {
        if (id == -1) {
            return null;
        }
        return getResources().getFont(id);
    }


    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();

        boolean isSelected = isSelectedState(this);
        boolean isFocused = isFocusedState(this);

        final Typeface font;
        if (isSelected && isFocused) {
            font = mFocusFont;
        } else if (!isSelected && isFocused) {
            font = mFocusFont;
        } else if (isSelected && !isFocused) {
            font = mSelectedFont;
        } else {
            font = mDefaultFont;
        }

        setTypeface(font);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                ViewGroup.LayoutParams param = getLayoutParams();
                if (param != null) {
                    setLayoutParams(param);
                }
            }
        });
    }

    private boolean isFocusedState(View view) {
        if (mFocusFont == null) {
            return false;
        }
        if (view.isFocused()) {
            return true;
        }

        if (view.isDuplicateParentStateEnabled()) {
            return isFocusedState((View) view.getParent());
        }

        return false;
    }

    private boolean isSelectedState(View view) {
        if (mSelectedFont == null) {
            return false;
        }

        if (view.isSelected()) {
            return true;
        }

        if (view.isDuplicateParentStateEnabled()) {
            return isSelectedState((View) view.getParent());
        }

        return false;
    }
}
