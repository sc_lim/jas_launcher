/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data;

import android.os.Parcel;
import android.os.Parcelable;

import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;

public class UserProfile implements Parcelable {

    public static final Creator<UserProfile> CREATOR = new Creator<UserProfile>() {
        @Override
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        @Override
        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };

    private int type;
    private ProfileInfo profileInfo;
    private String id;
    private String name;
    private boolean selected;

    UserProfile(ProfileInfo profileInfo) {
        this.type = UserProfileBuilder.TYPE_USER_PROFILE;
        this.profileInfo = profileInfo;
        this.id = profileInfo.getProfileId();
        this.name = profileInfo.getProfileName();
        this.selected = false;
    }

    UserProfile(int type, String name) {
        this.type = type;
        this.id = "";
        this.name = name;
        this.selected = false;
    }

    protected UserProfile(Parcel in) {
        type = in.readInt();
        profileInfo = in.readParcelable(ProfileInfo.class.getClassLoader());
        id = in.readString();
        name = in.readString();
        selected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(type);
        parcel.writeParcelable(profileInfo, i);
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeByte((byte) (selected ? 1 : 0));
    }

    public int getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ProfileInfo getProfileInfo() {
        return profileInfo;
    }

    public boolean isAddOption() {
        return type == UserProfileBuilder.TYPE_ADD_OPTION;
    }

    public boolean isManageOption() {
        return type == UserProfileBuilder.TYPE_MANAGE_OPTION;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("User{");
        buffer.append("type='" + type + '\'');
        buffer.append(", id='" + id + '\'');
        buffer.append(", name='" + name + '\'');
        buffer.append(", selected='" + selected + '\'');
        buffer.append('}');
        return buffer.toString();
    }
}
