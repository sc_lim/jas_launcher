/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.edit;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.TextButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.TextButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.edit.SettingDeviceEditDialogFragment.KEY_DEVICE;

public class SettingDeviceEditDeleteFragment extends SettingDeviceEditBaseFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton<SettingDeviceEditDeleteFragment.DeviceEditButton> {
    public static final String CLASS_NAME = SettingDeviceEditDeleteFragment.class.getName();
    private final String TAG = SettingDeviceEditDeleteFragment.class.getSimpleName();

    private final int DELETE_TYPE = 0;
    private final int CANCEL_TYPE = 1;

    private UserDevice deviceItem;

    private MbsDataTask editNameTask;

    private SettingDeviceEditDeleteFragment() {
    }

    public static SettingDeviceEditDeleteFragment newInstance(Bundle bundle) {
        SettingDeviceEditDeleteFragment fragment = new SettingDeviceEditDeleteFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getResourceId() {
        return R.layout.fragment_device_edit_delete;
    }

    @Override
    protected void initView(View view) {
        deviceItem = getArguments().getParcelable(KEY_DEVICE);
        TextView deviceName = view.findViewById(R.id.device_name);
        deviceName.setText(deviceItem.getDeviceName());

        initEditButton(view);
    }

    private void initEditButton(View view) {
        TextButtonPresenter textButtonPresenter = new TextButtonPresenter();
        textButtonPresenter.setResourceId(R.layout.presenter_text_button);
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(textButtonPresenter);
        objectAdapter.add(new DeviceEditButton(DELETE_TYPE, R.string.delete));
        objectAdapter.add(new DeviceEditButton(CANCEL_TYPE, R.string.cancel));

        TextButtonBridgeAdapter buttonBridgeAdapter = new TextButtonBridgeAdapter(objectAdapter);
        buttonBridgeAdapter.setListener(this);
        VerticalGridView gridView = view.findViewById(R.id.gridView);
        gridView.setAdapter(buttonBridgeAdapter);

        int gap = (int) getResources().getDimension(R.dimen.setting_profile_vertical_gap);
        gridView.setVerticalSpacing(gap);
    }

    private boolean isRequest = false;
    @Override
    public boolean onClickButton(DeviceEditButton item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        if (item.getType() == DELETE_TYPE) {
            if (isRequest) {
                return true;
            }

            isRequest = true;
            deleteDevice();
        } else {
            getOnChangeFragmentListener().onBackFragment();
        }

        return true;
    }

    private void deleteDevice() {
        OnChangeFragmentListener onChangeFragmentListener = getOnChangeFragmentListener();
        SettingTaskManager settingTaskManager = onChangeFragmentListener.getSettingTaskManager();

        editNameTask = settingTaskManager.deleteDevice(
                deviceItem.getDeviceId(),
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "deleteDevice, needLoading, loading : " + loading);
                        }

                        onChangeFragmentListener.showProgressbar(loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "deleteDevice, onFailed");
                        }

                        isRequest = false;
                        onChangeFragmentListener.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "deleteDevice, onSuccess, result : " + result);
                        }

                        if (result) {
                            getOnChangeFragmentListener().onEditDelete(deviceItem);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "deleteDevice, onError");
                        }

                        isRequest = false;
                        settingTaskManager.showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingDeviceEditDeleteFragment.this.getContext();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (editNameTask != null) {
            editNameTask.cancel(true);
        }
    }

    protected class DeviceEditButton implements TextButtonPresenter.TextButtonItem {
        private int type;
        private int title;

        public DeviceEditButton(int type, int title) {
            this.type = type;
            this.title = title;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public int getTitle() {
            return title;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }
}
