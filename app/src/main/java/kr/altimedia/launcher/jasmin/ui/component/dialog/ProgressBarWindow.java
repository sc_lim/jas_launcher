package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;

public class ProgressBarWindow {
    public static final String CLASS_NAME = ProgressBarWindow.class.getName();
    private final String TAG = ProgressBarWindow.class.getSimpleName();


    private WindowManager wm;

    private View parentsView;

    final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    final Context mContext;

    public ProgressBarWindow(Context context) {
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(context);
        parentsView = inflater.inflate(R.layout.dialog_progress, null, false);
        init(context);
    }

    private void init(Context context) {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                PixelFormat.TRANSLUCENT);
        mParams.gravity = Gravity.CENTER;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.addView(parentsView, mParams);
                ViewGroup loadingView = (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.view_loading, parentsView.findViewById(R.id.progressRoot));
                mProgressBarManager.setProgressBarView(loadingView);
            }
        });
    }

    public void dismiss() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.removeView(parentsView);
            }
        });
    }

    private Context getContext() {
        return mContext;
    }

    public void show() {
        mProgressBarManager.show();
    }

    public void hide(boolean withDismiss) {
        mProgressBarManager.hide();
        if (withDismiss) {
            dismiss();
        }
    }
}
