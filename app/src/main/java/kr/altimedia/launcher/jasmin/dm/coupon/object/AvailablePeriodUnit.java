/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import kr.altimedia.launcher.jasmin.R;
import com.altimedia.util.Log;

public enum AvailablePeriodUnit {
    DAY("21105", R.string.day), MONTH("21106", R.string.month), YEAR("21107", R.string.year);

    private final String type;
    private final int mName;

    AvailablePeriodUnit(String type, int mName) {
        this.type = type;
        this.mName = mName;
    }

    public static AvailablePeriodUnit findByName(String name) {
//        Log.d("AvailablePeriodUnit", "findByName: name="+name);
        AvailablePeriodUnit[] statuses = values();
        for (AvailablePeriodUnit status : statuses) {
            if (status.type.equalsIgnoreCase(name)) {
                return status;
            }
        }

        return YEAR;
    }

    public int getName() {
        return mName;
    }
}
