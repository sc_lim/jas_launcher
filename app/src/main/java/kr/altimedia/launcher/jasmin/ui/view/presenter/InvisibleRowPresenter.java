/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.presenter;

import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class InvisibleRowPresenter extends RowPresenter {
    public InvisibleRowPresenter() {
        this.setHeaderPresenter(null);
    }

    protected ViewHolder createRowViewHolder(ViewGroup parent) {
        RelativeLayout root = new RelativeLayout(parent.getContext());
        root.setLayoutParams(new RelativeLayout.LayoutParams(0, 0));
        return new ViewHolder(root);
    }
}
