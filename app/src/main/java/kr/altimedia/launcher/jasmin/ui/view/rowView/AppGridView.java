/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.rowView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

/**
 * Created by mc.kim on 09,12,2019
 */
public class AppGridView extends LinearLayout {
    private final String TAG = AppGridView.class.getSimpleName();

    private VerticalGridView mGridView;

    public AppGridView(Context context) {
        this(context, null);
    }

    public AppGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AppGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.list_vertical_row_view, this);

        mGridView = (VerticalGridView) findViewById(R.id.row_content);
        // since we use WRAP_CONTENT for height in lb_list_row, we need set fixed size to false
//        mGridView.setScrollEnabled(false);

        mGridView.setHasFixedSize(true);
        mGridView.setHorizontalSpacing(getResources().getDimensionPixelSize(R.dimen.app_horizontal_space));
        mGridView.setVerticalSpacing(getResources().getDimensionPixelSize(R.dimen.app_vertical_space));
        // Uncomment this to experiment with page-based scrolling.
        // mGridView.setFocusScrollStrategy(HorizontalGridView.FOCUS_SCROLL_PAGE);

//        int paddingBottom = mGridView.getPaddingBottom();
//        int paddingTop = mGridView.getPaddingTop();
//        int paddingLeft = mGridView.getPaddingLeft();
//        int paddingRight = mGridView.getPaddingRight();
//
//        Log.d(TAG,"paddingBottom : "+paddingBottom+", paddingTop : "+paddingTop+", paddingLeft : "+paddingLeft+", paddingRight : "+paddingRight);
//        mGridView.setPadding(218, 16, 218, 16);


        setOrientation(LinearLayout.VERTICAL);
        setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
    }

    private int mRowHeight = 0;

    public void setRowHeight(int height) {
        mRowHeight = height;
    }

    public int getRowHeight() {
        return mRowHeight;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
//        Log.d(TAG, "dispatchKeyEvent : " + event.getKeyCode());
        if (mOnDispatchKeyListener != null) {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                mOnDispatchKeyListener.onKeyDown(event);
            }
        }
        return super.dispatchKeyEvent(event);
    }

    private ContentsRowView.OnDispatchKeyListener mOnDispatchKeyListener = null;

    public void setOnDispatchKeyListener(ContentsRowView.OnDispatchKeyListener onDispatchKeyListener) {
        this.mOnDispatchKeyListener = onDispatchKeyListener;
    }

    public void disposeListener() {
        this.mOnDispatchKeyListener = null;
    }

    /**
     * Returns the HorizontalGridView.
     */
    public VerticalGridView getGridView() {
        return mGridView;
    }


    public interface OnDispatchKeyListener {
        void onKeyDown(int keyCode);
    }

}
