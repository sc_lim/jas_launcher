/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.data;

public enum MenuType {
    PURCHASED("Purchased", 0), COLLECTION("Collection", 1),
    SUBSCRIPTION("Subscription", 2), REMINDER("Reminder", 3), COUPON("Coupon", 4);

    final String name;
    final int id;

    MenuType(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public static MenuType findByName(String name) {
        MenuType[] types = values();
        for (MenuType type : types) {

            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return null;
    }
}