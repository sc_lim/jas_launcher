/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile;

import android.view.View;

import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.PageBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SettingProfilePresenter;

public class SettingProfileItemBridgeAdapter extends PageBridgeAdapter {
    private OnClickProfileListener onClickProfileListener;

    public SettingProfileItemBridgeAdapter(ObjectAdapter adapter, int visibleCount) {
        super(adapter, visibleCount);
    }

    public void setOnClickProfileListener(OnClickProfileListener onClickProfileListener) {
        this.onClickProfileListener = onClickProfileListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        SettingProfilePresenter.ViewHolder vh = (SettingProfilePresenter.ViewHolder) viewHolder.getViewHolder();
        ProfileInfo profileInfo = (ProfileInfo) viewHolder.getItem();

        vh.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickProfileListener != null) {
                    onClickProfileListener.onClickProfile(profileInfo);
                }
            }
        });
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);

        SettingProfilePresenter.ViewHolder vh = (SettingProfilePresenter.ViewHolder) viewHolder.getViewHolder();
        vh.view.setOnClickListener(null);
    }

    public interface OnClickProfileListener {
        void onClickProfile(ProfileInfo profileInfo);
    }
}
