/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter;

import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SettingMenuPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class SettingMenuItemBridgeAdapter extends ItemBridgeAdapter {
    private VerticalGridView verticalGridView;
    private OnClickMenuListener onClickMenuListener;

    public SettingMenuItemBridgeAdapter(ObjectAdapter adapter, PresenterSelector presenterSelector, VerticalGridView verticalGridView) {
        super(adapter, presenterSelector);

        this.verticalGridView = verticalGridView;

        ArrayObjectAdapter newAdapter = new ArrayObjectAdapter();
        for (int i = 0; i < adapter.size(); i++) {
            ListRow row = (ListRow) adapter.get(i);
            HeaderItem header = row.getHeaderItem();
            if (header != null) {
                newAdapter.add(header);
            }

            ArrayObjectAdapter items = (ArrayObjectAdapter) row.getAdapter();
            for (int j = 0; j < items.size(); j++) {
                SettingMenu menu = (SettingMenu) items.get(j);
                newAdapter.add(menu);
            }
        }

        setAdapter(newAdapter);
    }

    public void setOnClickMenuListener(OnClickMenuListener onClickMenuListener) {
        this.onClickMenuListener = onClickMenuListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);
        Presenter presenter = viewHolder.getPresenter();

        if (presenter instanceof SettingMenuPresenter) {
            SettingMenuPresenter.ViewHolder vh = (SettingMenuPresenter.ViewHolder) viewHolder.getViewHolder();
            View view = vh.view;
            int size = getAdapter().size();

            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    int index = verticalGridView.getSelectedPosition();
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                            if (onClickMenuListener != null) {
                                onClickMenuListener.onClickMenu();
                                return true;
                            }

                            return false;
                        case KeyEvent.KEYCODE_DPAD_UP:
                            v.setActivated(false);

                            if (index == 0) {
                                verticalGridView.scrollToPosition(size - 1);
                                return true;
                            }

                            return false;
                        case KeyEvent.KEYCODE_DPAD_DOWN:
                            v.setActivated(false);

                            if (index == (size - 1)) {
                                verticalGridView.scrollToPosition(0);
                                return true;
                            }

                            return false;
                    }
                    return false;
                }
            });

            view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        v.setActivated(true);
                        vh.setParentFocus(true);
                    }
                }
            });
        }
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);

        Presenter.ViewHolder vh = viewHolder.getViewHolder();
        vh.view.setOnKeyListener(null);
        vh.view.setOnFocusChangeListener(null);
    }

    public interface OnClickMenuListener {
        void onClickMenu();
    }
}