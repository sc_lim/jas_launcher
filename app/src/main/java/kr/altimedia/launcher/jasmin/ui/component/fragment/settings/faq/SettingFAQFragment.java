/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.faq;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.List;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.FAQInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.PageBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.FAQPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;

public class SettingFAQFragment extends SettingBaseFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton<FAQInfo>, SettingBaseButtonItemBridgeAdapter.OnKeyListener {
    public static final String CLASS_NAME = SettingFAQFragment.class.getName();
    private final String TAG = SettingFAQFragment.class.getSimpleName();

    private final int VISIBLE_COUNT = 7;

    private PagingVerticalGridView mVerticalGridView;
    private ThumbScrollbar scrollbar;

    private MbsDataTask faqTask;
    private List<FAQInfo> faqInfoList;

    private SettingFAQFragment() {
    }

    public static SettingFAQFragment newInstance() {

        Bundle args = new Bundle();

        SettingFAQFragment fragment = new SettingFAQFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_faq;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        mVerticalGridView = view.findViewById(R.id.faqGrid);
        scrollbar = view.findViewById(R.id.messageScrollbar);
        loadFAQList();
    }

    private void loadFAQList() {
        faqTask = onFragmentChange.getSettingTaskManager().getFaqList(
                new MbsDataProvider<String, List<FAQInfo>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadFAQList, needLoading, loading : " + loading);
                        }

                        onFragmentChange.showProgressbar(loading);
                        mVerticalGridView.setFocusable(!loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed");
                        }

                        setBlockFocus();
                        onFragmentChange.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, List<FAQInfo> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSuccess, result : " + result);
                        }

                        if (result == null || result.size() == 0) {
                            setBlockFocus();
                            return;
                        }

                        setFAQList(result);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError, errorCode : " + errorCode);
                        }

                        setBlockFocus();
                        onFragmentChange.getSettingTaskManager()
                                .showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingFAQFragment.this.getContext();
                    }
                });
    }

    private void setBlockFocus() {
        mVerticalGridView.setFocusable(false);
        mVerticalGridView.setFocusableInTouchMode(false);
        mVerticalGridView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
    }

    private void setFAQList(List<FAQInfo> faqList) {
        this.faqInfoList = faqList;
        int size = faqList != null ? faqList.size() : 0;

        FAQPresenter faqPresenter = new FAQPresenter();
        ArrayObjectAdapter faqAdapter = new ArrayObjectAdapter(faqPresenter);
        faqAdapter.addAll(0, faqList);

        setView(size);
        PageBridgeAdapter itemBridgeAdapter = new PageBridgeAdapter(faqAdapter, VISIBLE_COUNT);
        itemBridgeAdapter.setListener(this);
        itemBridgeAdapter.setOnKeyListener(this);
        mVerticalGridView.setAdapter(itemBridgeAdapter);

        initScrollbar(size);
    }

    private void setView(int size) {
        View view = getView();
        view.findViewById(R.id.layout).setVisibility(View.VISIBLE);
        String count = size > 0 ? "(" + size + ")" : "";
        ((TextView) getView().findViewById(R.id.size)).setText(count);
        mVerticalGridView.initVertical(VISIBLE_COUNT);

        int gap = (int) getResources().getDimension(R.dimen.setting_faq_vertical_gap);
        mVerticalGridView.setVerticalSpacing(gap);
    }

    private void initScrollbar(int size) {
        scrollbar.setList(size, VISIBLE_COUNT);

        mVerticalGridView.setNumColumns(1);
        mVerticalGridView.addOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {
            @Override
            public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                super.onChildViewHolderSelected(parent, child, position, subposition);
                scrollbar.moveFocus(position);
            }
        });
        scrollbar.setFocusable(false);
    }

    @Override
    public boolean onClickButton(FAQInfo faqInfo) {
        SettingFAQDetailDialogFragment settingFAQDetailDialogFragment = SettingFAQDetailDialogFragment.newInstance(faqInfo);
        onFragmentChange.getTvOverlayManager().showDialogFragment(settingFAQDetailDialogFragment);

        return true;
    }

    @Override
    public void onDestroyView() {
        if (faqTask != null) {
            faqTask.cancel(true);
        }

        super.onDestroyView();
    }

    @Override
    public boolean onKey(int keyCode, boolean isConsumed, ItemBridgeAdapter.ViewHolder viewHolder) {
        FAQInfo mFaqInfo = (FAQInfo) viewHolder.getItem();
        int index = faqInfoList.indexOf(mFaqInfo);
        int size = faqInfoList.size();

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_UP:
                if (index == 0) {
                    mVerticalGridView.scrollToPosition(size - 1);
                    return true;
                }

                return false;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                if (index == (size - 1)) {
                    mVerticalGridView.scrollToPosition(0);
                    return true;
                }

                return false;

        }
        return false;
    }
}
