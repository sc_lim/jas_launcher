/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object.SideOptionCategory;
import kr.altimedia.launcher.jasmin.ui.view.row.SideOptionButtonRow;

/**
 * Created by mc.kim on 18,02,2020
 */
public class SideOptionButtonPresenter extends Presenter {
    private final String TAG = SideOptionButtonPresenter.class.getSimpleName();
    public SideOptionButtonPresenter() {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_side_option_button, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBindViewHolder");
        }
        View parentsView = viewHolder.view;
        TextView keyText = parentsView.findViewById(R.id.key);
        if (item instanceof SideOptionButtonRow) {
            SideOptionButtonRow optionRow = (SideOptionButtonRow) item;
            parentsView.setOnClickListener(optionRow);
            parentsView.setOnKeyListener(optionRow);
            SideOptionCategory type = optionRow.getOptionType();
            keyText.setText(type.getOptionName());
        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onUnbindViewHolder");
        }
        View parentsView = viewHolder.view;
        parentsView.setOnKeyListener(null);
        parentsView.setOnClickListener(null);
    }
}
