/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.FavoriteContent;

/**
 * Created by mc.kim on 08,05,2020
 */
public class FavoriteContentListResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private FavoriteContentListResult result;

    protected FavoriteContentListResponse(Parcel in) {
        super(in);
        result = in.readTypedObject(FavoriteContentListResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(result, flags);
    }

    public List<FavoriteContent> getContentList() {
        return result.contentList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FavoriteContentListResponse> CREATOR = new Creator<FavoriteContentListResponse>() {
        @Override
        public FavoriteContentListResponse createFromParcel(Parcel in) {
            return new FavoriteContentListResponse(in);
        }

        @Override
        public FavoriteContentListResponse[] newArray(int size) {
            return new FavoriteContentListResponse[size];
        }
    };

    @Override
    public String toString() {
        return "FavoriteContentListResponse{" +
                "result=" + result +
                '}';
    }

    private static class FavoriteContentListResult implements Parcelable {
        @SerializedName("contentList")
        @Expose
        private List<FavoriteContent> contentList;
        @Expose
        @SerializedName("totalContentNo")
        private int totalContentNo;

        protected FavoriteContentListResult(Parcel in) {
            contentList = in.createTypedArrayList(FavoriteContent.CREATOR);
            totalContentNo = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(contentList);
            dest.writeInt(totalContentNo);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<FavoriteContentListResult> CREATOR = new Creator<FavoriteContentListResponse.FavoriteContentListResult>() {
            @Override
            public FavoriteContentListResponse.FavoriteContentListResult createFromParcel(Parcel in) {
                return new FavoriteContentListResponse.FavoriteContentListResult(in);
            }

            @Override
            public FavoriteContentListResponse.FavoriteContentListResult[] newArray(int size) {
                return new FavoriteContentListResponse.FavoriteContentListResult[size];
            }
        };
    }
}
