/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.util.task;

import android.os.AsyncTask;

import com.altimedia.util.Log;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.dm.contents.ContentDataManager;
import kr.altimedia.launcher.jasmin.dm.coupon.CouponDataManager;
import kr.altimedia.launcher.jasmin.dm.recommend.RecommendDataManager;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;

public abstract class TaskManager {
    private static final String TAG = TaskManager.class.getSimpleName();

    protected final ContentDataManager mContentDataManager = new ContentDataManager();
    protected final UserDataManager mUserDataManager = new UserDataManager();
    protected final RecommendDataManager mRecommendDataManager = new RecommendDataManager();
    protected final CouponDataManager mCouponDataManager = new CouponDataManager();

    protected ArrayList<AsyncTask> taskList = new ArrayList<>();

    public boolean isEmpty() {
        return getTaskSize() == 0;
    }

    public int getTaskSize() {
        return taskList.size();
    }

    protected void addTask(AsyncTask task) {
        if (!taskList.contains(task)) {
            taskList.add(task);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "addTask, task : " + task + ", taskList : " + taskList.size());
        }
    }

    protected void completeTask(AsyncTask task) {
        if (task != null) {
            taskList.remove(task);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "completeTask, task : " + task + ", taskList : " + taskList.size());
        }
    }

    protected void cancelTask(AsyncTask task) {
        if (Log.INCLUDE) {
            Log.d(TAG, "cancelTask, task : " + task);
        }

        if (task != null) {
            task.cancel(true);
            completeTask(task);
        }
    }

    public void cancelAllTask() {
        if (Log.INCLUDE) {
            Log.d(TAG, "cancelAllTask");
        }

        ArrayList<AsyncTask> cloneTask = (ArrayList<AsyncTask>) taskList.clone();
        for (AsyncTask task : cloneTask) {
            cancelTask(task);
        }
    }
}
