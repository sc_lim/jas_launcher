package kr.altimedia.launcher.jasmin.ui.component.fragment.sleep;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.altimedia.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ProcessLifecycleOwner;

public class SleepModeReceiver extends BroadcastReceiver {
    private final String TAG = SleepModeReceiver.class.getSimpleName();
    public static final String ACTION_SCREEN_SAVER_MODE = "com.innopia.innocontroller.ACTION_DO_SCREEN_OFF";
    public static final String ACTION_SLEEP_MODE = "com.innopia.innocontroller.ACTION_SLEEP";

    private SleepModeDialog sleepModeDialog;

    @Override
    public void onReceive(Context context, Intent intent) {
        Lifecycle.State mState = ProcessLifecycleOwner.get().getLifecycle().getCurrentState();

        if (Log.INCLUDE) {
            Log.d(TAG, "onReceive, " + intent.getAction() + "mState : " + mState);
        }

        if (mState == Lifecycle.State.RESUMED) {
            if (sleepModeDialog == null) {
                sleepModeDialog = new SleepModeDialog(context);
            }

            boolean isShow = sleepModeDialog.isShow();
            if (Log.INCLUDE) {
                Log.d(TAG, "sleepModeDialog isShow : " + isShow);
            }

            if (!isShow) {
                sleepModeDialog.show(context);
            }
        }
    }
}
