/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.view.KeyEvent;
import android.view.View;

import com.altimedia.tvmodule.dao.Program;

import androidx.annotation.NonNull;

/**
 * Created by mc.kim on 15,07,2020
 */
class TimeShiftProgramRow implements View.OnClickListener, View.OnKeyListener {
    private final Program mProgram;
    private final Program mCurrentPlayingProgram;
    private final TimeShiftPanelLayout.TimeShiftProgramKeyListener mTimeShiftProgramKeyListener;

    public Program getProgram() {
        return mProgram;
    }

    public TimeShiftProgramRow(@NonNull Program program, @NonNull Program currentPlayingProgram,
                               @NonNull TimeShiftPanelLayout.TimeShiftProgramKeyListener timeShiftProgramKeyListener) {
        this.mProgram = program;
        this.mCurrentPlayingProgram = currentPlayingProgram;
        this.mTimeShiftProgramKeyListener = timeShiftProgramKeyListener;
    }

    public Program getCurrentPlayingProgram() {
        return mCurrentPlayingProgram;
    }

    public TimeShiftPanelLayout.TimeShiftProgramKeyListener getInterface() {
        return mTimeShiftProgramKeyListener;
    }

    @Override
    public void onClick(View v) {
        mTimeShiftProgramKeyListener.onItemSelected(mProgram);
    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return true;
        }
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                mTimeShiftProgramKeyListener.onBackPressed(mProgram);
                return true;
            case KeyEvent.KEYCODE_INFO:
                mTimeShiftProgramKeyListener.onBackPressed(mProgram);
                return false;

            case KeyEvent.KEYCODE_DPAD_CENTER:
                mTimeShiftProgramKeyListener.onItemSelected(mProgram);
                return true;
        }
        return false;
    }
}
