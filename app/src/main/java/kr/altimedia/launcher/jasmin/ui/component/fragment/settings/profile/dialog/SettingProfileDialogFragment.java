/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.app.ProgressBarManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.OnFragmentChange;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseProfileDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingProfileMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.SettingProfileBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.lock.SettingProfileLockFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.SettingProfilePinFragment;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PROFILE;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_PROFILE_ID;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_TYPE;

public class SettingProfileDialogFragment extends SettingBaseProfileDialogFragment implements OnFragmentChange, SettingProfileBaseFragment.OnPushProfileChangeMessage {
    public static final String CLASS_NAME = SettingProfileDialogFragment.class.getName();
    private final String TAG = SettingProfileDialogFragment.class.getSimpleName();

    public static final String KEY_PROFILE = "PROFILE";
    public static final String KEY_FROM_SETTING = "FROM_SETTING";

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private final int MESSAGE_ID = 0;
    private final long KEY_DELAY_TIME = 150;

    private FrameLayout frameLayout;
    private SettingProfileMenuListFragment settingProfileMenuListFragment;
    private OnProfileChangeListener mOnProfileChangeListener;

    private final Handler keyHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (Log.INCLUDE) {
                Log.d(TAG, "call handleMessage");
            }

            getChildFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            SettingProfileMenu menu = (SettingProfileMenu) msg.obj;
            showMenu(menu);
        }
    };

    private SettingProfileDialogFragment() {
    }

    public static SettingProfileDialogFragment newInstance(ProfileInfo profileInfo) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_PROFILE, profileInfo);

        SettingProfileDialogFragment fragment = new SettingProfileDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {

                boolean menuHasFocus = settingProfileMenuListFragment == null || settingProfileMenuListFragment.getView().hasFocus();
                if (Log.INCLUDE) {
                    Log.d(TAG, "menuHasFocus :" + menuHasFocus);
                }

                if (menuHasFocus) {
                    super.onBackPressed();
                } else {
                    settingProfileMenuListFragment.getView().requestFocus();
                }
            }
        };
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_setting_profile, container, false);
    }

    public void setOnProfileChangeListener(OnProfileChangeListener mOnProfileChangeListener) {
        this.mOnProfileChangeListener = mOnProfileChangeListener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
    }

    private void initView(View view) {
        frameLayout = view.findViewById(R.id.frame_menu);

        initProgressbar(view);
        addMenuList();
    }

    protected void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void addMenuList() {
        settingProfileMenuListFragment = SettingProfileMenuListFragment.newInstance(getArguments());
        settingProfileMenuListFragment.setOnFragmentChange(this);
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.setting_profile_menu, settingProfileMenuListFragment, SettingProfileMenuListFragment.CLASS_NAME).commit();
    }

    private void showMenu(SettingProfileMenu menu) {
        switch (menu) {
            case PARENTAL:
                SettingProfilePinFragment settingProfilePinFragment = SettingProfilePinFragment.newInstance(getArguments());
                settingProfilePinFragment.setOnFragmentChange(this);
                settingProfilePinFragment.setOnPushProfileChangeMessage(this);
                showFragment(settingProfilePinFragment, SettingProfilePinFragment.CLASS_NAME);
                break;
            case LOCKS:
                SettingProfileLockFragment settingProfileLockFragment = SettingProfileLockFragment.newInstance(getArguments());
                settingProfileLockFragment.setOnFragmentChange(this);
                settingProfileLockFragment.setOnPushProfileChangeMessage(this);
                showFragment(settingProfileLockFragment, SettingProfileLockFragment.CLASS_NAME);
                break;
        }
    }

    private void showFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
//        if (addBackStack) {
//            ft.addToBackStack(tag);
//        }

        ft.replace(R.id.frame_menu, fragment, tag).commit();
    }

    private void setMenuListFocusable(Boolean isFocusable) {
        settingProfileMenuListFragment.setFocusable(isFocusable);
    }

    public void updateProfileLockIcon(boolean isLock) {
        settingProfileMenuListFragment.setProfileLockIcon(isLock);
    }

    @Override
    public void onChangeMenu(SettingMenu menu) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onChangeMenu : " + menu);
        }

        keyHandler.removeMessages(MESSAGE_ID);

        Message message = Message.obtain(keyHandler, MESSAGE_ID, menu);
        keyHandler.sendMessageDelayed(message, KEY_DELAY_TIME);
    }

    @Override
    public void onNextFragment(Fragment fragment, String tag) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onNextFragment : " + tag);
        }

        setMenuListFocusable(false);
        showFragment(fragment, tag);
    }

    @Override
    public void onClickMenu() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickMenu");
        }

        frameLayout.requestFocus();
    }

    @Override
    public void onUpdateMenuItem(int index, Object item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onUpdateMenuItem : " + item);
        }
    }

    @Override
    public void onMenuFocus() {
        getDialog().onBackPressed();
    }

    @Override
    public void notifyFrameAttached() {
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyFrameAttached");
        }

        setMenuListFocusable(true);
    }

    @Override
    public SettingTaskManager getSettingTaskManager() {
        return new SettingTaskManager(getFragmentManager(), mTvOverlayManager);
    }

    @Override
    public void showProgressbar(boolean isLoading) {
        if (isLoading) {
            mProgressBarManager.show();
        } else {
            mProgressBarManager.hide();
        }
    }

    @Override
    protected ProfileInfo getProfileInfo() {
        return getArguments().getParcelable(KEY_PROFILE);
    }

    @Override
    protected void updateProfile(PushMessageReceiver.ProfileUpdateType type) {
        super.updateProfile(type);

        if (Log.INCLUDE) {
            Log.d(TAG, "updateProfile, type : " + type);
        }

        mOnProfileChangeListener.updateProfileList();
        dismiss();
    }

    @Override
    public TvOverlayManager getTvOverlayManager() {
        return super.getTvOverlayManager();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mOnProfileChangeListener = null;
        keyHandler.removeMessages(MESSAGE_ID);
    }

    @Override
    public void onPushProfile(PushMessageReceiver.ProfileUpdateType profileUpdateType, ProfileInfo profileInfo) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onChangeProfile, profileUpdateType : " + profileUpdateType);
        }

        mOnProfileChangeListener.onChangeProfile(profileUpdateType, profileInfo);

        Intent intent = new Intent(getContext(), PushMessageReceiver.class);
        intent.setAction(ACTION_UPDATED_PROFILE);
        intent.putExtra(KEY_TYPE, profileUpdateType);
        intent.putExtra(KEY_PROFILE_ID, profileInfo.getProfileId());
        intent.putExtra(KEY_FROM_SETTING, true);
        intent.putExtra(PushMessageReceiver.KEY_CHANGED_LOGIN_USER_INFO, profileInfo.getProfileId().equalsIgnoreCase(AccountManager.getInstance().getProfileId()));
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    public interface OnProfileChangeListener {
        void onChangeProfile(PushMessageReceiver.ProfileUpdateType profileUpdateType, ProfileInfo profileInfo);

        void updateProfileList();
    }
}
