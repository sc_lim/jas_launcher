/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOption;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOptionCallback;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOptionProvider;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object.SideOptionCategory;
import kr.altimedia.launcher.jasmin.ui.view.row.SideOptionMenuRow;

/**
 * Created by mc.kim on 18,02,2020
 */
public class SideOptionMenuPresenter extends Presenter {
    private final String TAG = SideOptionMenuPresenter.class.getSimpleName();
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_side_option_menu, parent, false);
        return new ViewHolder(view);
    }

    public SideOptionMenuPresenter() {
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBindViewHolder");
        }
        View parentsView = viewHolder.view;
        TextView keyText = parentsView.findViewById(R.id.key);
        TextView valueText = parentsView.findViewById(R.id.value);
        if (item instanceof SideOptionMenuRow) {
            SideOptionMenuRow optionRow = (SideOptionMenuRow) item;
            SideOptionCategory type = optionRow.getOptionType();
            parentsView.setOnKeyListener(optionRow);
            parentsView.setOnClickListener(optionRow);
            keyText.setText(type.getOptionName());
            if (type.getOptionType() == SidePanelOption.SubTitle) {
                SidePanelOptionProvider provider = optionRow.getPanelOptionProvider();
                if (provider instanceof SidePanelOptionCallback) {
                    SidePanelOptionCallback mSidePanelOptionCallback = (SidePanelOptionCallback) provider;
                    String lang = mSidePanelOptionCallback.getCurrentSubtitleLanguage();
                    valueText.setText((lang != null ? lang : ""));
                }
            } else if (type.getOptionType() == SidePanelOption.Audio) {
                SidePanelOptionProvider provider = optionRow.getPanelOptionProvider();
                if (provider instanceof SidePanelOptionCallback) {
                    SidePanelOptionCallback mSidePanelOptionCallback = (SidePanelOptionCallback) provider;
                    String lang = mSidePanelOptionCallback.getCurrentAudioLanguage();
                    valueText.setText((lang != null ? lang : ""));
                }
            }
        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onUnbindViewHolder");
        }
        View parentsView = viewHolder.view;
        parentsView.setOnKeyListener(null);
        parentsView.setOnClickListener(null);
    }
}
