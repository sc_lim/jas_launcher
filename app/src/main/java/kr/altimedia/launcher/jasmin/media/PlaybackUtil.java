/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.media;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.altimedia.player.AltiMediaSource;
import com.altimedia.player.AltiPlayer;
import com.altimedia.player.MonitorInfo;
import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.contents.ContentDataManager;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PlacementDecision;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamType;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.OTUResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;

/**
 * Created by mc.kim on 07,01,2020
 */
public class PlaybackUtil {
    private static final String TAG = PlaybackUtil.class.getSimpleName();
    public static final int REQUEST_CODE_VOD = 100;
    public static final int REQUEST_CODE_LIVE = 200;
    public static final String ACTION_START_VOD = "altimedia.intent.action.StartVideo";
    public static final String ACTION_START_LIVE = "altimedia.intent.action.StartLiveTv";
    public static final String ACTION_START_GUIDE = "altimedia.intent.action.ProgramGuide";
    public static final String ACTION_START_FAV_GUIDE = "altimedia.intent.action.FavoriteProgramGuide";

    public static final String EXTRA_LIVETV_TUNE_CHANNEL_ID = "livetv.channel_id";
    public static final String EXTRA_LIVETV_TUNE_PIN_CHECKED = "livetv.pin_checked";
    private static final String ERROR_CODE_NOT_PURCHASED = "MBS_1060";


    public static boolean isNotPurchasedContents(String errorCode) {
        return errorCode.equalsIgnoreCase(ERROR_CODE_NOT_PURCHASED);
    }

    private static boolean isFreeContents(Content content) {
        List<Product> productList = content.getProductList();
        for (Product product : productList) {
            if (product.getPrice() == 0) {
                return true;
            }
        }
        return false;
    }

    private static boolean isPurchasedContents(Content content) {
        List<Product> productList = content.getProductList();
        for (Product product : productList) {
            if (product.isPurchased()) {
                return true;
            }
        }
        return false;
    }


    /*forMain*/
    public static void getPlayResource(@NonNull final Context context, @NonNull final Content content, long resumeTime,
                                       @NonNull final PlayResourceCallback callback) {

        if (!isPurchasedContents(content) && !isFreeContents(content)) {
            callback.onError("code", "wrong request");
            return;
        }
        AccountManager accountManager = AccountManager.getInstance();
        ContentDataManager contentDataManager = new ContentDataManager();
        contentDataManager.getOneTimeUrl(accountManager.getSaId(),
                accountManager.getProfileId(), isPurchasedContents(content) ? "Y" : "N",
                "VOD", content.getMainStreamInfo().getVideoPath(0), content.getMovieAssetId(), resumeTime != 0 ? "Y" : "N", content.getCategoryId(), false,
                new MbsDataProvider<String, OTUResponse>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                    }

                    @Override
                    public void onSuccess(String id, OTUResponse result) {
                        String otu = result.getOTU();
                        List<String> pathList = result.getOTUList();
                        AltiMediaSource[] mediaSourceList = new AltiMediaSource[pathList.size()];
                        for (int i = 0; i < pathList.size(); i++) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, " get OneTimeUrl : " + pathList.get(i));
                            }
                            Uri contentUrl = Uri.parse(pathList.get(i));
                            Uri laUrl = Uri.parse(getLaUrl(content.getMovieAssetId(), otu));
                            AltiMediaSource mediaSource = new AltiMediaSource(context, contentUrl.toString(), laUrl.toString(),
                                    new MonitorInfo(accountManager.getSaId(), accountManager.getAccountId(), accountManager.getProfileId(),
                                            result.getTransactionId()));
                            mediaSourceList[i] = mediaSource;
                        }
                        callback.onResult(mediaSourceList, result.getAdvertisementList());
                    }

                    @Override
                    public void onError(String errorCode, String message) {

                        callback.onError(errorCode, message);
                    }
                });
    }


    /*forTrailer,Preview*/
    public static void getPlayResource(@NonNull final Context context, String contentsId, String movieAssetId, @NonNull final StreamInfo streamInfo, String categoryId,
                                       @NonNull final PlayResourceCallback callback) {
        if (streamInfo.getStreamType() == StreamType.Trailer) {
            String[] videoPathArray = streamInfo.getVideoPath();
            if (videoPathArray == null) {
                callback.onFail(callback.REASON_NULL_CONTENTS);
                return;
            }
            AccountManager accountManager = AccountManager.getInstance();
            AltiMediaSource[] mediaSourceList = new AltiMediaSource[videoPathArray.length];
            for (int i = 0; i < videoPathArray.length; i++) {
                AltiMediaSource mediaSource = new AltiMediaSource(context, videoPathArray[i], new MonitorInfo(accountManager.getSaId(), accountManager.getAccountId(), accountManager.getProfileId()));
                mediaSourceList[i] = mediaSource;
            }
            callback.onResult(mediaSourceList, new ArrayList<>());
        } else {
            AccountManager accountManager = AccountManager.getInstance();
            ContentDataManager contentDataManager = new ContentDataManager();
            contentDataManager.getOneTimeUrl(accountManager.getSaId(),
                    accountManager.getProfileId(), "N",
                    "VOD", streamInfo.getVideoPath(0), movieAssetId, "N", categoryId, true, new MbsDataProvider<String, OTUResponse>() {
                        @Override
                        public void needLoading(boolean loading) {
                            callback.needLoading(loading);
                        }

                        @Override
                        public void onFailed(int key) {
                        }

                        @Override
                        public void onSuccess(String id, OTUResponse result) {
                            String otu = result.getOTU();
                            List<String> pathList = result.getOTUList();

                            AltiMediaSource[] mediaSourceList = new AltiMediaSource[pathList.size()];
                            for (int i = 0; i < pathList.size(); i++) {
                                if (Log.INCLUDE) {
                                    Log.d(TAG, " get OneTimeUrl : " + pathList.get(i));
                                }
                                Uri contentUrl = Uri.parse(pathList.get(i));
                                Uri laUrl = Uri.parse(getLaUrl(movieAssetId, otu));
                                AltiMediaSource mediaSource = new AltiMediaSource(context, contentUrl.toString(), laUrl.toString(),
                                        new MonitorInfo(accountManager.getSaId(), accountManager.getAccountId(), accountManager.getProfileId(), result.getTransactionId()));
                                mediaSourceList[i] = mediaSource;
                            }
                            callback.onResult(mediaSourceList, result.getAdvertisementList());
                        }

                        @Override
                        public void onError(String errorCode, String message) {

                            callback.onError(errorCode, message);
                        }
                    });
        }
    }

    public static void getPlayResource(@NonNull final Context context, @NonNull final Banner banner,
                                       @NonNull final PlayResourceCallback callback) {
        if (context == null) {
            String errorCode = ErrorMessageManager.getInstance().getPlayerErrorCode(context, AltiPlayer.PLAYER_ERROR_TYPE_SOURCE);
            callback.onError(errorCode, ErrorMessageManager.getInstance().getErrorMessage(errorCode));
            return;
        }
        AccountManager accountManager = AccountManager.getInstance();
        List<String> streamUrl = banner.getBannerClipAsset().getFileName();
        AltiMediaSource[] resultSource = new AltiMediaSource[streamUrl.size()];
        for (int i = 0; i < streamUrl.size(); i++) {
            if (Log.INCLUDE) {
                Log.d(TAG, "getPlayResource : " + streamUrl.get(i));
            }
            AltiMediaSource mediaSource = new AltiMediaSource(context, streamUrl.get(i), new MonitorInfo(accountManager.getSaId(), accountManager.getAccountId(), accountManager.getProfileId()));
            resultSource[i] = mediaSource;
        }
        callback.onResult(resultSource, new ArrayList<>());
    }

    @Deprecated
    public static Uri generateMovie(Context context, String resName) {
        String uri = "android.resource://" + context.getPackageName() + "/" + getRawResIdByName(context, resName);
        return Uri.parse(uri);
    }

    private static int getRawResIdByName(Context context, String resName) {
        String pkgName = context.getPackageName();
        // Return 0 if not found.
        int index = resName.indexOf(".");
        int resID = context.getResources().getIdentifier(resName.substring(0, index), "raw", pkgName);
        return resID;
    }

    public static final void startLiveTvActivityWithServiceId(Context context, String serviceId) {
        if (context != null) {
            Channel channel = TvSingletons.getSingletons(context).getChannelDataManager().getChannelByServiceId(serviceId);
            if (channel == null) {
                return;
            }
            Intent intent = new Intent(PlaybackUtil.ACTION_START_LIVE);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle bundle = new Bundle();
            bundle.putLong(EXTRA_LIVETV_TUNE_CHANNEL_ID, channel.getId());
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    public static final void startLiveTvActivityWithChannel(Context context, long channelId) {
        if (context != null) {
            Intent intent = new Intent(PlaybackUtil.ACTION_START_LIVE);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle bundle = new Bundle();
            bundle.putLong(EXTRA_LIVETV_TUNE_CHANNEL_ID, channelId);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    public static long getChannelIdFromIntent(Intent intent, long defaultValue) {
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                return bundle.getLong(EXTRA_LIVETV_TUNE_CHANNEL_ID, defaultValue);
            }
        }
        return defaultValue;
    }

    public static boolean isVodOverRating(int contentRating) {
        ProfileInfo profileInfo = AccountManager.getInstance().getSelectedProfile();
        if (profileInfo == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "isVodOverRating, there is no selected profileInfo");
            }
            return false;
        }

        int userRating = profileInfo.getRating();

        if (Log.INCLUDE) {
            Log.d(TAG, "isVodOverRating, contentRating : " + contentRating + ", userRating : " + userRating);
        }

        return userRating != 0 && contentRating > userRating;
    }

    public static String getLaUrl(String contentID, String otu) {
        AccountManager am = AccountManager.getInstance();
        String serviceType = "0"; //VOD:0, Channel:1
        String loginToken = am.getLoginToken();
        String profileID = am.getProfileId();
        String deviceType = "001"; //STB, OTA, OTI, OTW, OTS
        String laUrl = am.getLicenseUrl()
                + "?ContentID=" + contentID
                + "&ServiceType=" + serviceType
                + "&LoginToken=" + loginToken
                + "&ProfileID=" + profileID
                + "&OTU=" + otu
                + "&DownloadYN=" + "N"
                + "&DeviceType=" + deviceType;
        if (Log.INCLUDE) {
            Log.d(TAG, "LA URL : " + laUrl);
        }
        return laUrl;
    }

    public static abstract class PlayResourceCallback {
        public final int REASON_NULL_CONTENTS = 1;

        public void needLoading(boolean loading) {

        }

        public abstract void onResult(AltiMediaSource[] mediaSource, List<PlacementDecision> placementDecisions);

        public abstract void onError(String errorCode, String errorMessage);

        public void onFail(int reason) {

        }

    }
}
