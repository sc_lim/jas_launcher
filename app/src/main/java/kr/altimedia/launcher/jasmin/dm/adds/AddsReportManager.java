package kr.altimedia.launcher.jasmin.dm.adds;

import java.io.IOException;

import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.adds.module.ADDSModule;
import kr.altimedia.launcher.jasmin.dm.adds.remote.AddsRemote;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;

/**
 * Created by mc.kim on 04,08,2020
 */
public class AddsReportManager extends BaseDataManager {
    private final String TAG = AddsReportManager.class.getSimpleName();
    private AddsRemote mAddsRemote;

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        ADDSModule addsModule = new ADDSModule();
        Retrofit retrofit = addsModule.provideAddsModule(mOkHttpClient);
        mAddsRemote = new AddsRemote(addsModule.provideAddsApi(retrofit));
    }

    @Override
    public String getLoginToken() {
        return AccountManager.getInstance().getLoginToken();
    }

    public MbsDataTask reportAdPlay(String url, MbsDataProvider<String, String> mbsDataProvider) {
        MbsDataTask mbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, String>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException {
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = mOkHttpClient.newCall(request).execute();
                String result = response.body().string();
                return result;
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onTaskPostExecute(String key, String result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mbsDataTask.execute(url);
        return mbsDataTask;
    }


}
