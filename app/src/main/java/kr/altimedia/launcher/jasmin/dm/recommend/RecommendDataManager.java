/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.recommend;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.recommend.module.RecommendModule;
import kr.altimedia.launcher.jasmin.dm.recommend.remote.RecommendRemote;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by mc.kim on 02,06,2020
 */
public class RecommendDataManager extends BaseDataManager {
    private final String TAG = RecommendDataManager.class.getSimpleName();
    private RecommendRemote mRecommendRemote;

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        RecommendModule recommendModule = new RecommendModule();
        Retrofit contentRetrofit = recommendModule.provideRecommendModule(mOkHttpClient);
        mRecommendRemote = new RecommendRemote(recommendModule.provideRecommendApi(contentRetrofit));
    }

    @Override
    public String getLoginToken() {
        return AccountManager.getInstance().getLoginToken();
    }


    public MbsDataTask getRelatedContents(String language, String said, String profileId, String contentsId,
                                          int contentCnt, String productCode, String categoryId, int rating,
                                          MbsDataProvider<String, List<Content>> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, List<Content>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mRecommendRemote.getRelatedContents(language, said, profileId, productCode,
                        contentsId, categoryId, contentCnt, true, rating);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<Content> result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(contentsId);
        return mMbsDataTask;
    }

    public MbsDataTask getCurationContents(String language, String said, String profileId, String categoryId,
                                           int contentCnt, String productCode, int rating,
                                           MbsDataProvider<String, List<Content>> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, List<Content>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException , AuthenticationException{
                return mRecommendRemote.getCurationRecommendContents(language, said, profileId, productCode, categoryId,
                        contentCnt, true, rating);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<Content> result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }
}
