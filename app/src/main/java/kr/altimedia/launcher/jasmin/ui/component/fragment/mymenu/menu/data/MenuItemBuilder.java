/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.List;

import kr.altimedia.launcher.jasmin.R;

public class MenuItemBuilder {
    public static final String MENU_DATA = "{\n" +
            "\t\"MenuList\": [{\n" +
            "\t\t\"type\": \"Purchased\",\n" +
            "\t\t\"title\": \"" + R.string.purchased + "\"\n" +
            "\t}, {\n" +
            "\t\t\"type\": \"Collection\",\n" +
            "\t\t\"title\": \"" + R.string.library + "\"\n" +
            "\t}, {\n" +
            "\t\t\"type\": \"Subscription\",\n" +
            "\t\t\"title\": \"" + R.string.subscription + "\"\n" +
            "\t}, {\n" +
            "\t\t\"type\": \"Reminder\",\n" +
            "\t\t\"title\": \"" + R.string.reminder + "\"\n" +
            "\t}, {\n" +
            "\t\t\"type\": \"Coupon\",\n" +
            "\t\t\"title\": \"" + R.string.coupon + "\"\n" +
            "\t}]\n" +
            "}";


    public Gson getGsonBuilder() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(MenuType.class, new CustomDeserializer());
        return builder.create();
    }

    public List<MenuItem> getMenuList() {
        return getGsonBuilder().fromJson(MENU_DATA, MenuList.class).getMenuList();
    }

    /**
     * CustomDeserializer
     */
    private class CustomDeserializer implements JsonDeserializer<MenuType> {
        @Override
        public MenuType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return MenuType.findByName(json.getAsString());
        }
    }

    /**
     * MenuList
     */
    private class MenuList implements Parcelable {

        @SerializedName("MenuList")
        @Expose
        private List<MenuItem> list = null;
        public final Creator<MenuList> CREATOR = new Creator<MenuList>() {
            @SuppressWarnings({
                    "unchecked"
            })
            public MenuList createFromParcel(Parcel in) {
                return new MenuList(in);
            }

            public MenuList[] newArray(int size) {
                return (new MenuList[size]);
            }
        };

        protected MenuList(Parcel in) {
            in.readList(this.list, (MenuItem.class.getClassLoader()));
        }

        public MenuList() {
        }

        public List<MenuItem> getMenuList() {
            return list;
        }

        public void setMenuList(List<MenuItem> list) {
            this.list = list;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(list);
        }

        public int describeContents() {
            return 0;
        }
    }
}
