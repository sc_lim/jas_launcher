/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel;

import android.content.Context;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.util.Log;
import com.google.auto.factory.AutoFactory;

import java.util.List;

import androidx.annotation.MainThread;

@AutoFactory
@MainThread
public class ChannelLoader {
    private final String TAG = ChannelLoader.class.getSimpleName();

    private Context mContext = null;

    public ChannelLoader(Context context) {
        mContext = context;
    }

    private SyncListener mSyncListener = null;

    public void startSync(SyncListener syncListener) {
        mSyncListener = syncListener;
        startChannelScan(mContext);
        startChannelMonitor(mContext);
    }

    public void clearSync() {
        mSyncListener = null;
    }

    private void startChannelScan(Context context) {
//        TifController.getInstance().startScan(context, TvInputManagerHelper.ALTI_TIS);
//        TifController.getInstance().startScan(context,"kr.altimedia.jasmine.tis","kr.altimedia.agent.AgentService");
    }

    @MainThread
    private void startChannelMonitor(final Context context) {
        final ChannelDataManager mChannelDataManager = TvSingletons.getSingletons(context).getChannelDataManager();
        final ProgramDataManager mProgramDataManager = TvSingletons.getSingletons(context).getProgramDataManager();
        final BackendKnobsFlags mBackendKnobsFlags = TvSingletons.getSingletons(context).getBackendKnobs();

        mChannelDataManager.addListener(new ChannelDataManager.Listener() {
            @Override
            public void onLoadFinished() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onLoadFinished");
                }
                List<Channel> channels = mChannelDataManager.getChannelList();
                if (Log.INCLUDE) {
                    Log.d(TAG, "onLoadFinished  channel Size : " + channels.size());
                }

                mProgramDataManager.startListening();

                if (mSyncListener != null) {
                    mSyncListener.onScanFinished(channels);
                }
            }

            @Override
            public void onChannelListUpdated() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onChannelListUpdated");
                }
                List<Channel> channels = mChannelDataManager.getChannelList();
                if (mSyncListener != null) {
                    mSyncListener.onUpdated(channels);
                }
            }

            @Override
            public void onChannelBrowsableChanged() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onChannelBrowsableChanged");
                }

            }

            @Override
            public void onUserBasedChannelListUpdated() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onUserBasedChannelListUpdated");
                }
            }
        });
        if (mSyncListener != null) {
            mSyncListener.onScanStart();
        }
        mChannelDataManager.start();
//        mChannelDataManager.updateChannels(new Runnable() {
//            @Override
//            public void run() {
//                List<Channel> channels = mChannelDataManager.getChannelList();
//                if (Log.INCLUDE) {
//                    Log.d(TAG, "updateChannels  channel Size : " + channels.size());
//                }
//            }
//        });
    }


    public interface SyncListener {

        void onUpdated(List<Channel> channels);

        void onScanFinished(List<Channel> channels);

        void onScanStart();

        void onScanError(int errorCode);
    }


}
