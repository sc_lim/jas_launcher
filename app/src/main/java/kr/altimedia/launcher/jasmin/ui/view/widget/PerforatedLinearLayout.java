/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 05,06,2020
 */
public class PerforatedLinearLayout extends LinearLayout {
    private final String TAG = PerforatedLinearLayout.class.getSimpleName();

    public PerforatedLinearLayout(Context context) {
        super(context);
        Log.d(TAG, "PerforatedLinearLayout(Context context) ");
    }

    public PerforatedLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Log.d(TAG, "PerforatedLinearLayout(Context context, @Nullable AttributeSet attrs)");
    }

    public PerforatedLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Log.d(TAG, "PerforatedLinearLayout(Context context, AttributeSet attrs, int defStyleAttr)");
    }


    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        Log.d(TAG, "dispatchDraw");
        final int count = getChildCount();
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            LayoutParams params = (LayoutParams) child.getLayoutParams();
            Log.d(TAG, "params: isPerformed : " + params.isPerformed());
            if (params.isPerformed()) {
                RectF performRect = new RectF(params.leftMargin, params.topMargin, params.rightMargin, params.bottomMargin);
                if (performRect != null) {
                    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                    paint.setColor(getResources().getColor(android.R.color.transparent));
                    paint.setStyle(Paint.Style.FILL);
                    canvas.drawPaint(paint);
                    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
                    canvas.drawRoundRect(performRect, 0, 0, paint);
                }
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG, "onDraw");
        super.onDraw(canvas);

    }


    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams;
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }

    @Override
    protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return new LayoutParams(p.width, p.height);
    }

    public static class LayoutParams extends LinearLayout.LayoutParams {
        boolean performed = false;

        public boolean isPerformed() {
            return performed;
        }

        public LayoutParams(Context context, AttributeSet attrs) {
            super(context, attrs);
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Perforated);
            try {
                performed = a.getBoolean(R.styleable.Perforated_perforated, false);
            } finally {
                a.recycle();
            }
        }

        public LayoutParams(int w, int h) {
            super(w, h);
        }
    }
}
