/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import kr.altimedia.launcher.jasmin.dm.def.PaymentTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import kr.altimedia.launcher.jasmin.dm.payment.api.PaymentApi;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class PaymentModule {
    public Retrofit providePaymentModule(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(ServerConfig.MBS_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(PaymentType.class, new PaymentTypeDeserializer());
        return builder.create();
    }

    @Singleton
    public PaymentApi providePaymentApi(@Named("Payment") Retrofit retrofit) {
        return retrofit.create(PaymentApi.class);
    }
}
