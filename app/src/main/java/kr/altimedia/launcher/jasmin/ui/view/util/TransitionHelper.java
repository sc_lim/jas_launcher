/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.animation.TimeInterpolator;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.transition.AutoTransition;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.ui.view.browse.listener.TransitionEpicenterCallback;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.TransitionListener;

public class TransitionHelper {
    public static final int FADE_IN = 1;
    public static final int FADE_OUT = 2;

    public static boolean systemSupportsEntranceTransitions() {
        return Build.VERSION.SDK_INT >= 21;
    }

    public static Object getSharedElementEnterTransition(Window window) {
        return Build.VERSION.SDK_INT >= 21 ? window.getSharedElementEnterTransition() : null;
    }

    public static void setSharedElementEnterTransition(Window window, Object transition) {
        if (Build.VERSION.SDK_INT >= 21) {
            window.setSharedElementEnterTransition((Transition) transition);
        }

    }

    public static Object getSharedElementReturnTransition(Window window) {
        return Build.VERSION.SDK_INT >= 21 ? window.getSharedElementReturnTransition() : null;
    }

    public static void setSharedElementReturnTransition(Window window, Object transition) {
        if (Build.VERSION.SDK_INT >= 21) {
            window.setSharedElementReturnTransition((Transition) transition);
        }

    }

    public static Object getSharedElementExitTransition(Window window) {
        return Build.VERSION.SDK_INT >= 21 ? window.getSharedElementExitTransition() : null;
    }

    public static Object getSharedElementReenterTransition(Window window) {
        return Build.VERSION.SDK_INT >= 21 ? window.getSharedElementReenterTransition() : null;
    }

    public static Object getEnterTransition(Window window) {
        return Build.VERSION.SDK_INT >= 21 ? window.getEnterTransition() : null;
    }

    public static void setEnterTransition(Window window, Object transition) {
        if (Build.VERSION.SDK_INT >= 21) {
            window.setEnterTransition((Transition) transition);
        }

    }

    public static Object getReturnTransition(Window window) {
        return Build.VERSION.SDK_INT >= 21 ? window.getReturnTransition() : null;
    }

    public static void setReturnTransition(Window window, Object transition) {
        if (Build.VERSION.SDK_INT >= 21) {
            window.setReturnTransition((Transition) transition);
        }

    }

    public static Object getExitTransition(Window window) {
        return Build.VERSION.SDK_INT >= 21 ? window.getExitTransition() : null;
    }

    public static Object getReenterTransition(Window window) {
        return Build.VERSION.SDK_INT >= 21 ? window.getReenterTransition() : null;
    }

    public static Object createScene(ViewGroup sceneRoot, Runnable r) {
        if (Build.VERSION.SDK_INT >= 19) {
            Scene scene = new Scene(sceneRoot);
            scene.setEnterAction(r);
            return scene;
        } else {
            return r;
        }
    }

    public static Object createChangeBounds(boolean reparent) {
        if (Build.VERSION.SDK_INT >= 19) {
            CustomChangeBounds changeBounds = new CustomChangeBounds();
            changeBounds.setReparent(reparent);
            return changeBounds;
        } else {
            return new TransitionHelper.TransitionStub();
        }
    }

    public static Object createChangeTransform() {
        return Build.VERSION.SDK_INT >= 21 ? new ChangeTransform() : new TransitionHelper.TransitionStub();
    }

    public static void setChangeBoundsStartDelay(Object changeBounds, View view, int startDelay) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((CustomChangeBounds) changeBounds).setStartDelay(view, startDelay);
        }

    }

    public static void setChangeBoundsStartDelay(Object changeBounds, int viewId, int startDelay) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((CustomChangeBounds) changeBounds).setStartDelay(viewId, startDelay);
        }

    }

    public static void setChangeBoundsStartDelay(Object changeBounds, String className, int startDelay) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((CustomChangeBounds) changeBounds).setStartDelay(className, startDelay);
        }

    }

    public static void setChangeBoundsDefaultStartDelay(Object changeBounds, int startDelay) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((CustomChangeBounds) changeBounds).setDefaultStartDelay(startDelay);
        }

    }

    public static Object createTransitionSet(boolean sequential) {
        if (Build.VERSION.SDK_INT >= 19) {
            TransitionSet set = new TransitionSet();
            set.setOrdering(sequential ? 1 : 0);
            return set;
        } else {
            return new TransitionHelper.TransitionStub();
        }
    }

    public static Object createSlide(int slideEdge) {
        if (Build.VERSION.SDK_INT >= 19) {
            SlideKitkat slide = new SlideKitkat();
            slide.setSlideEdge(slideEdge);
            return slide;
        } else {
            return new TransitionHelper.TransitionStub();
        }
    }

    public static Object createScale() {
        if (Build.VERSION.SDK_INT >= 21) {
            return new ChangeTransform();
        } else {
            return Build.VERSION.SDK_INT >= 19 ? new Scale() : new TransitionStub();
        }
    }

    public static void addTransition(Object transitionSet, Object transition) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((TransitionSet) transitionSet).addTransition((Transition) transition);
        }

    }

    public static void exclude(Object transition, int targetId, boolean exclude) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((Transition) transition).excludeTarget(targetId, exclude);
        }

    }

    public static void exclude(Object transition, View targetView, boolean exclude) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((Transition) transition).excludeTarget(targetView, exclude);
        }

    }

    public static void excludeChildren(Object transition, int targetId, boolean exclude) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((Transition) transition).excludeChildren(targetId, exclude);
        }

    }

    public static void excludeChildren(Object transition, View targetView, boolean exclude) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((Transition) transition).excludeChildren(targetView, exclude);
        }

    }

    public static void include(Object transition, int targetId) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((Transition) transition).addTarget(targetId);
        }

    }

    public static void include(Object transition, View targetView) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((Transition) transition).addTarget(targetView);
        }

    }

    public static void setStartDelay(Object transition, long startDelay) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((Transition) transition).setStartDelay(startDelay);
        }

    }

    public static void setDuration(Object transition, long duration) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((Transition) transition).setDuration(duration);
        }

    }

    public static Object createAutoTransition() {
        return Build.VERSION.SDK_INT >= 19 ? new AutoTransition() : new TransitionHelper.TransitionStub();
    }

    public static Object createFadeTransition(int fadeMode) {
        return Build.VERSION.SDK_INT >= 19 ? new Fade(fadeMode) : new TransitionHelper.TransitionStub();
    }

    public static void addTransitionListener(Object transition, final TransitionListener listener) {
        if (listener != null) {
            if (Build.VERSION.SDK_INT >= 19) {
                Transition t = (Transition) transition;
                Transition.TransitionListener  transitionListener = new Transition.TransitionListener() {
                    public void onTransitionStart(Transition transition11) {
                        listener.onTransitionStart(transition11);
                    }

                    public void onTransitionResume(Transition transition11) {
                        listener.onTransitionResume(transition11);
                    }

                    public void onTransitionPause(Transition transition11) {
                        listener.onTransitionPause(transition11);
                    }

                    public void onTransitionEnd(Transition transition11) {
                        listener.onTransitionEnd(transition11);
                    }

                    public void onTransitionCancel(Transition transition11) {
                        listener.onTransitionCancel(transition11);
                    }
                };

                listener.mImpl = transitionListener;
                t.addListener(transitionListener);
            } else {
                TransitionHelper.TransitionStub stub = (TransitionHelper.TransitionStub) transition;
                if (stub.mTransitionListeners == null) {
                    stub.mTransitionListeners = new ArrayList();
                }

                stub.mTransitionListeners.add(listener);
            }
        }
    }


    public static void removeTransitionListener(Object transition, TransitionListener listener) {
        if (Build.VERSION.SDK_INT >= 19) {
            if (listener == null || listener.mImpl == null) {
                return;
            }

            Transition t = (Transition) transition;
            t.removeListener((Transition.TransitionListener) listener.mImpl);
            listener.mImpl = null;
        } else {
            TransitionHelper.TransitionStub stub = (TransitionHelper.TransitionStub) transition;
            if (stub.mTransitionListeners != null) {
                stub.mTransitionListeners.remove(listener);
            }
        }

    }

    public static void runTransition(Object scene, Object transition) {
        if (Build.VERSION.SDK_INT >= 19) {
            TransitionManager.go((Scene) scene, (Transition) transition);
        } else {
            TransitionHelper.TransitionStub transitionStub = (TransitionHelper.TransitionStub) transition;
//            int k;
            if (transitionStub != null && transitionStub.mTransitionListeners != null) {
//                int i = 0;
//
//                for (i = transitionStub.mTransitionListeners.size(); i < i; ++i) {
//                    ((TransitionListener) transitionStub.mTransitionListeners.get(i)).onTransitionStart(transition);
//                }

                for(TransitionListener mTransitionListener : transitionStub.mTransitionListeners){
                    mTransitionListener.onTransitionStart(transition);
                }

            }

            Runnable r = (Runnable) scene;
            if (r != null) {
                r.run();
            }

            if (transitionStub != null && transitionStub.mTransitionListeners != null) {
//                i = 0;
//
//                for (int size = transitionStub.mTransitionListeners.size(); i < size; ++i) {
//                    ((TransitionListener) transitionStub.mTransitionListeners.get(i)).onTransitionEnd(transition);
//                }

                for(TransitionListener mTransitionListener : transitionStub.mTransitionListeners){
                    mTransitionListener.onTransitionEnd(transition);
                }

            }
        }

    }

    public static void setInterpolator(Object transition, Object timeInterpolator) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((Transition) transition).setInterpolator((TimeInterpolator) timeInterpolator);
        }

    }

    public static void addTarget(Object transition, View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            ((Transition) transition).addTarget(view);
        }

    }

    public static Object createDefaultInterpolator(Context context) {

        return Build.VERSION.SDK_INT >= 21 ? AnimationUtils.loadInterpolator(context, android.R.anim.linear_interpolator) : null;
    }

    public static Object loadTransition(Context context, int resId) {
        return Build.VERSION.SDK_INT >= 19 ? TransitionInflater.from(context).inflateTransition(resId) : new TransitionHelper.TransitionStub();
    }

    public static void setEnterTransition(Fragment fragment, Object transition) {
        if (Build.VERSION.SDK_INT >= 21) {
            fragment.setEnterTransition((Transition) transition);
        }

    }

    public static void setExitTransition(Fragment fragment, Object transition) {
        if (Build.VERSION.SDK_INT >= 21) {
            fragment.setExitTransition((Transition) transition);
        }

    }

    public static void setSharedElementEnterTransition(Fragment fragment, Object transition) {
        if (Build.VERSION.SDK_INT >= 21) {
            fragment.setSharedElementEnterTransition((Transition) transition);
        }

    }

    public static void addSharedElement(FragmentTransaction ft, View view, String transitionName) {
        if (Build.VERSION.SDK_INT >= 21) {
            ft.addSharedElement(view, transitionName);
        }

    }

    public static Object createFadeAndShortSlide(int edge) {
        return Build.VERSION.SDK_INT >= 21 ? new FadeAndShortSlide(edge) : new TransitionHelper.TransitionStub();
    }

    public static Object createFadeAndShortSlide(int edge, float distance) {
        if (Build.VERSION.SDK_INT >= 21) {
            FadeAndShortSlide slide = new FadeAndShortSlide(edge);
            slide.setDistance(distance);
            return slide;
        } else {
            return new TransitionHelper.TransitionStub();
        }
    }

    public static void beginDelayedTransition(ViewGroup sceneRoot, Object transitionObject) {
        if (Build.VERSION.SDK_INT >= 21) {
            Transition transition = (Transition) transitionObject;
            TransitionManager.beginDelayedTransition(sceneRoot, transition);
        }

    }

    public static void setTransitionGroup(ViewGroup viewGroup, boolean transitionGroup) {
        if (Build.VERSION.SDK_INT >= 21) {
            viewGroup.setTransitionGroup(transitionGroup);
        }

    }

    public static void setEpicenterCallback(Object transition, final TransitionEpicenterCallback callback) {
        if (Build.VERSION.SDK_INT >= 21) {
            if (callback == null) {
                ((Transition) transition).setEpicenterCallback((Transition.EpicenterCallback) null);
            } else {
                ((Transition) transition).setEpicenterCallback(new Transition.EpicenterCallback() {
                    public Rect onGetEpicenter(Transition transition11) {
                        return callback.onGetEpicenter(transition11);
                    }
                });
            }
        }

    }

    private TransitionHelper() {
    }

    private static class TransitionStub {
        ArrayList<TransitionListener> mTransitionListeners;

        TransitionStub() {
        }
    }

}
