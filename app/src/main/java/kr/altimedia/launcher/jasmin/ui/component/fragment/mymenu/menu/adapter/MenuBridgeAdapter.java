/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.adapter;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.data.MenuItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.presenter.MenuItemPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;

public class MenuBridgeAdapter extends ItemBridgeAdapter {
    private static final String TAG = MenuBridgeAdapter.class.getSimpleName();

    public MenuBridgeAdapter() {
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        MenuItemPresenter presenter = (MenuItemPresenter) viewHolder.mPresenter;
        MenuItemPresenter.OnKeyListener onKeyListener = presenter.getOnKeyListener();

        MenuItemPresenter.MenuItemViewHolder itemViewHolder = (MenuItemPresenter.MenuItemViewHolder) viewHolder.getViewHolder();

        int index = (int) viewHolder.itemView.getTag(R.id.KEY_INDEX);
        if (onKeyListener != null) {
            viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    MenuItem menuItem = (MenuItem) viewHolder.mItem;
                    String name = menuItem.getType().getName();
                    Log.d(TAG, "onKey() item=" + name + ", index=" + index);

                    return onKeyListener.onKey(keyCode, index, menuItem, itemViewHolder);
                }
            });
        }
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        viewHolder.mHolder.view.setOnKeyListener(null);
    }
}
