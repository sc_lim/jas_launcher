/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.def.PaymentTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class MyCoupon extends Coupon implements Parcelable {
    public static final Creator<MyCoupon> CREATOR = new Creator<MyCoupon>() {
        @Override
        public MyCoupon createFromParcel(Parcel in) {
            return new MyCoupon(in);
        }

        @Override
        public MyCoupon[] newArray(int size) {
            return new MyCoupon[size];
        }
    };

    // discount
    @Expose
    @SerializedName("dcType")
    @JsonAdapter(DiscountTypeDeserializer.class)
    private DiscountType discountType; // 할인유형
    @Expose
    @SerializedName("cpnDcAmt")
    private int discountAmount;
    @Expose
    @SerializedName("cpnDcRate")
    private int discountRate;
    @Expose
    @SerializedName("maxDcAmt")
    private int discountMaxAmount;
    @Expose
    @SerializedName("minVodAmt")
    private String discountMinVodAmount;

    // point(cash)
    @Expose
    @SerializedName("bnsType")
    @JsonAdapter(BonusTypeDeserializer.class)
    private BonusType pointBonusType; //보너스 구분
    @Expose
    @SerializedName("bnsAmt")
    private int pointBonusAmount; // 보너스 금액
    @Expose
    @SerializedName("bnsVal")
    private int pointBonusValue; // 보너스 요율
    @Expose
    @SerializedName("chargAmt")
    private int pointChargedAmount; // 충전포인트
    @Expose
    @SerializedName("pointBlncVal")
    private int pointBalance; // 잔액값
    @Expose
    @SerializedName("buyMethod")
    @JsonAdapter(PaymentTypeDeserializer.class)
    private PaymentType paymentType;


    protected MyCoupon(Parcel in) {
        super(in);

        discountType = (DiscountType) in.readValue(DiscountType.class.getClassLoader());
        discountAmount = in.readInt();
        discountRate = in.readInt();
        discountMaxAmount = in.readInt();
        discountMinVodAmount = in.readString();

        pointBonusType = ((BonusType) in.readValue((BonusType.class.getClassLoader())));
        pointBonusAmount = in.readInt();
        pointBonusValue = in.readInt();
        pointChargedAmount = in.readInt();
        pointBalance = in.readInt();
        paymentType = PaymentType.findByName((String) in.readValue(String.class.getClassLoader()));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeValue(discountType);
        dest.writeInt(discountAmount);
        dest.writeInt(discountRate);
        dest.writeInt(discountMaxAmount);
        dest.writeString(discountMinVodAmount);

        dest.writeValue(pointBonusType);
        dest.writeInt(pointBonusAmount);
        dest.writeInt(pointBonusValue);
        dest.writeInt(pointChargedAmount);
        dest.writeInt(pointBalance);
        dest.writeValue(paymentType.getCode());
    }

    public DiscountType getDiscountType(){
        return discountType;
    }

    public BonusType getPointBonusType(){
        return pointBonusType;
    }

    public DiscountCoupon getDiscountCoupon(){
        return new DiscountCoupon(this, discountType, discountAmount, discountRate, discountMaxAmount, discountMinVodAmount);
    }

    public PointCoupon getPointCoupon(){
        return new PointCoupon(this, pointBonusType, pointBonusAmount, pointBonusValue, pointChargedAmount, pointBalance, paymentType);
    }

    @NonNull
    @Override
    public String toString() {
        return "MyCoupon{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                "registerDate='" + registerDate + '\'' +
                "expireDate='" + expireDate + '\'' +
                "couponStatus='" + couponStatus + '\'' +
                "couponType='" + couponType + '\'' +
                "expiringYn='" + expiringYn + '\'' +
                "discountType='" + discountType + '\'' +
                "discountAmount='" + discountAmount + '\'' +
                "discountRate='" + discountRate + '\'' +
                "discountMaxAmount='" + discountMaxAmount + '\'' +
                "discountMinVodAmount='" + discountMinVodAmount + '\'' +
                "pointBonusAmount='" + pointBonusAmount + '\'' +
                "pointBonusValue='" + pointBonusValue + '\'' +
                "pointChargedAmount='" + pointChargedAmount + '\'' +
                "pointBalance='" + pointBalance + '\'' +
                "paymentType='" + paymentType + '\'' +
                "}";
    }
}
