/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.altimedia.util.Log;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PURCHASE_INFORM;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.BUNDLE_KEY_ID;

public abstract class SubscriptionPaymentBaseFragment extends Fragment {
    private final String TAG = SubscriptionPaymentBaseFragment.class.getSimpleName();

    protected int layoutResourceId = -1;

    private OnPaymentListener onPaymentListener;

    private LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver mPushMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive, Action : " + intent.getAction());
            }

            String id = intent.getExtras().getString(BUNDLE_KEY_ID);
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive, id : " + id);
            }

            getPaymentResult(id);
        }
    };

    public void setOnPaymentListener(OnPaymentListener onPaymentListener) {
        this.onPaymentListener = onPaymentListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutResourceId = initLayoutResourceId();
        return inflater.inflate(layoutResourceId, container, false);
    }

    protected abstract int initLayoutResourceId();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerPurchaseIdReceiver();
        initView(view);
    }

    protected void initView(View view) {
    }

    private void registerPurchaseIdReceiver() {
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        mLocalBroadcastManager.registerReceiver(mPushMessageReceiver, new IntentFilter(ACTION_UPDATED_PURCHASE_INFORM));
    }

    private void unRegisterPurchaseIdReceiver() {
        mLocalBroadcastManager.unregisterReceiver(mPushMessageReceiver);
    }

    protected void getPaymentResult(String resultId) {
    }


    public OnPaymentListener getOnPaymentListener() {
        return onPaymentListener;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unRegisterPurchaseIdReceiver();
        onPaymentListener = null;
    }

    public interface OnPaymentListener {
        void onPaymentSubscription(boolean isSuccess);

        void onPaymentError(String paymentErrorCode);

        void onDismissPaymentDialog();

        void onDismissPurchaseDialog();
    }
}
