/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 11,05,2020
 */
public class MbsProgram implements Parcelable {
    @Expose
    @SerializedName("end")
    private String end;
    @Expose
    @SerializedName("programId")
    private String programId;
    @Expose
    @SerializedName("programMultiAudio")
    private String programMultiAudio;
    @Expose
    @SerializedName("programSynopsis")
    private String programSynopsis;
    @Expose
    @SerializedName("programTitle")
    private String programTitle;
    @Expose
    @SerializedName("rating")
    private String rating;
    @Expose
    @SerializedName("start")
    private String start;
    @Expose
    @SerializedName("videoType")
    private String videoType;


    protected MbsProgram(Parcel in) {
        end = in.readString();
        programId = in.readString();
        programMultiAudio = in.readString();
        programSynopsis = in.readString();
        programTitle = in.readString();
        rating = in.readString();
        start = in.readString();
        videoType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(end);
        dest.writeString(programId);
        dest.writeString(programMultiAudio);
        dest.writeString(programSynopsis);
        dest.writeString(programTitle);
        dest.writeString(rating);
        dest.writeString(start);
        dest.writeString(videoType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MbsProgram> CREATOR = new Creator<MbsProgram>() {
        @Override
        public MbsProgram createFromParcel(Parcel in) {
            return new MbsProgram(in);
        }

        @Override
        public MbsProgram[] newArray(int size) {
            return new MbsProgram[size];
        }
    };

    public String getEnd() {
        return end;
    }

    public String getProgramId() {
        return programId;
    }

    public String getProgramMultiAudio() {
        return programMultiAudio;
    }

    public String getProgramSynopsis() {
        return programSynopsis;
    }

    public String getProgramTitle() {
        return programTitle;
    }

    public String getRating() {
        return rating;
    }

    public String getStart() {
        return start;
    }

    public String getVideoType() {
        return videoType;
    }

    @Override
    public String toString() {
        return "MbsProgram{" +
                "end='" + end + '\'' +
                ", programId='" + programId + '\'' +
                ", programMultiAudio='" + programMultiAudio + '\'' +
                ", programSynopsis='" + programSynopsis + '\'' +
                ", programTitle='" + programTitle + '\'' +
                ", rating='" + rating + '\'' +
                ", start='" + start + '\'' +
                ", videoType='" + videoType + '\'' +
                '}';
    }
}
