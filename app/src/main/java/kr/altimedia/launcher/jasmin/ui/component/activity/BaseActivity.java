/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.activity;

import android.content.pm.PackageManager;
import android.os.Bundle;

import com.altimedia.tvmodule.dao.Program;
import com.altimedia.util.Log;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel;
import kr.altimedia.launcher.jasmin.tv.viewModel.OSDViewModel;
import kr.altimedia.launcher.jasmin.tv.viewModel.TVGuideViewModel;
import kr.altimedia.launcher.jasmin.tv.viewModel.ViewModelFactory;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.Reminder;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager;
import kr.altimedia.launcher.jasmin.ui.util.PermissionUtils;

/**
 * Created by mc.kim on 23,12,2019
 */
public class BaseActivity extends FragmentActivity implements ReminderAlertManager.OnReminderListener {
    protected final ReminderAlertManager reminderAlertManager = ReminderAlertManager.getInstance();
    private final String TAG = BaseActivity.class.getSimpleName();
    public final static int PERMISSION_REQUEST_CODE = 0;
    private EpgViewModel mEpgViewModel = null;
    private OSDViewModel mOSDViewModel = null;
    private TVGuideViewModel mTVGuideViewModel = null;

    private boolean isResumed = false;
    private boolean isStarted = false;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        isResumed = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isResumed = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        isStarted = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isStarted = false;
    }

    public boolean isActivityResumed() {
        return isResumed;
    }

    public boolean isActivityStarted() {
        return isStarted;
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        final ViewModelFactory viewModelFactory = ViewModelFactory.getInstance(getApplication(), this);
        mEpgViewModel = ViewModelProviders.of(this, viewModelFactory).get(EpgViewModel.class);
        mOSDViewModel = ViewModelProviders.of(this, viewModelFactory).get(OSDViewModel.class);
        mTVGuideViewModel = ViewModelProviders.of(this, viewModelFactory).get(TVGuideViewModel.class);

        reminderAlertManager.addOnReminderListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        reminderAlertManager.removeOnReminderListener(this);
    }

    public EpgViewModel getEpgViewModel() {
        return mEpgViewModel;
    }

    public OSDViewModel getOSDViewModel() {
        return mOSDViewModel;
    }

    public TVGuideViewModel getTVGuideViewModel() {
        return mTVGuideViewModel;
    }

    protected void reminderWatch(Reminder reminder) {
        PlaybackUtil.startLiveTvActivityWithChannel(getApplicationContext(), reminder.getChannelId());
    }

    protected void reminderConflict(Program program, Reminder conflictedReminder) {

    }

    protected void reminderListChange(List<Reminder> list) {

    }

    @Override
    public void onReminderWatch(Reminder reminder) {
        reminderWatch(reminder);
    }

    @Override
    public void onReminderConflict(Program program, Reminder conflictedReminder) {
        reminderConflict(program, conflictedReminder);
    }

    @Override
    public void onReminderListChange(List<Reminder> list, Reminder targetProgram) {
        reminderListChange(list);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    for (String permission : permissions) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPermittedListeningEpgPermission | permission : " + permission);
                        }
                        if (permission.equalsIgnoreCase(PermissionUtils.PERMISSION_READ_TV_LISTINGS)) {
                            checkPermittedListeningEpgPermission(permission);
                        }
                    }
                }
                return;
        }

    }

    private void checkPermittedListeningEpgPermission(String permissions) {
        if (Log.INCLUDE) {
            Log.d(TAG, "checkPermittedListeningEpgPermission : " + permissions);

        }
        if (!AccountManager.getInstance().isAuthenticatedUser()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "checkPermittedListeningEpgPermission but not login so return");
            }
            return;
        }
        JasChannelManager.getInstance().updateChannelList();
    }
}
