/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.user.object.FavoriteContent;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;


public class VodContentPresenter extends RowPresenter {
    private final String TAG = VodContentPresenter.class.getSimpleName();

    private VodContentPresenter.LoadWorkPosterListener mLoadWorkPosterListener;
    private HashMap<String, Integer> flagMap = new HashMap<>();

    public VodContentPresenter() {
        super();

        flagMap.put(Content.FLAG_EVENT, R.id.flagEvent);
        flagMap.put(Content.FLAG_HOT, R.id.flagHot);
        flagMap.put(Content.FLAG_NEW, R.id.flagNew);
        flagMap.put(Content.FLAG_PREMIUM, R.id.flagPremium);
        flagMap.put(Content.FLAG_UHD, R.id.flagUhd);
    }

    public VodContentPresenter(int headerLayoutId) {
        super(headerLayoutId);
    }

    public void setLoadWorkPosterListener(VodContentPresenter.LoadWorkPosterListener loadMoviePosterListener) {
        mLoadWorkPosterListener = loadMoviePosterListener;
    }

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        VodItemViewHolder mVodItemViewHolder = new VodItemViewHolder(inflater.inflate(R.layout.item_mymenu_vod_content, null, false));
        return mVodItemViewHolder;
    }

    @Override
    protected void onBindRowViewHolder(ViewHolder viewHolder, Object item) {
        super.onBindRowViewHolder(viewHolder, item);

        VodContentPresenter.VodItemViewHolder cardView = (VodContentPresenter.VodItemViewHolder) viewHolder;

        Object workItem = null;
        String title = "";
        List<String> eventFlags = null;
        int rating = 0;
        String posterUrl = "";
        if(item instanceof Content) {
            Content content = (Content) item;
            title = content.getTitle();
            eventFlags = content.getEventFlag();
            rating = content.getRating();
            posterUrl = content.getPosterSourceUrl(Content.mPosterDefaultRect.width(),
                    Content.mPosterDefaultRect.height());
            workItem = content;
        }else if(item instanceof FavoriteContent) {
            FavoriteContent content = (FavoriteContent) item;
            title = content.getTitle();
            eventFlags = content.getEventFlag();
            rating = content.getRating();
            posterUrl = content.getPosterSourceUrl(Content.mPosterDefaultRect.width(),
                    Content.mPosterDefaultRect.height());
            workItem = content;
        }
        cardView.setText(title);

        initFlag(eventFlags, rating, viewHolder.view);

        Glide.with(viewHolder.view.getContext())
                .load(posterUrl).diskCacheStrategy(DiskCacheStrategy.DATA).placeholder(R.drawable.vod_list_default)
                .centerCrop()
                .into(cardView.getMainImageView());

        if (mLoadWorkPosterListener != null) {
            mLoadWorkPosterListener.onLoadWorkPoster(workItem, cardView.getMainImageView());
        }
    }

    private void initFlag(List<String> eventFlags, int rating, View view) {
        if (eventFlags == null) {
            removeFlag(view);
            return;
        }
        Iterator<String> keyIterator = flagMap.keySet().iterator();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            boolean hasFlag = eventFlags != null && eventFlags.contains(key);
            View flagView = view.findViewById(flagMap.get(key));
            flagView.setVisibility(hasFlag ? View.VISIBLE : View.GONE);
        }

        View lockIcon = view.findViewById(R.id.iconVodLock);
        if (lockIcon != null) {
            boolean isLocked = isLockedByRating(rating);
            lockIcon.setVisibility(isLocked ? View.VISIBLE : View.GONE);
        }
    }

    private void removeFlag(View view) {
        Iterator<String> keyIterator = flagMap.keySet().iterator();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            View flagView = view.findViewById(flagMap.get(key));
            flagView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onUnbindRowViewHolder(ViewHolder viewHolder) {
        super.onUnbindRowViewHolder(viewHolder);

        VodItemViewHolder cardView = (VodItemViewHolder) viewHolder;
        removeFlag(cardView.view);
        cardView.removeBadgeImage();
        cardView.removeMainImage();
    }

    private boolean isLockedByRating(int rating) {
        return PlaybackUtil.isVodOverRating(rating);
    }

    public interface LoadWorkPosterListener {
        void onLoadWorkPoster(Object item, ImageView imageView);
    }

    private static class VodItemViewHolder extends ViewHolder {
        private final String TAG = VodItemViewHolder.class.getSimpleName();
        private ImageView mainImageView = null;
        private TextView titleView = null;

        public VodItemViewHolder(View view) {
            super(view);
            mainImageView = view.findViewById(R.id.mainImage);
            titleView = view.findViewById(R.id.titleLayer);

        }

        public ImageView getMainImageView() {
            return mainImageView;
        }

        public void setText(String title) {
            titleView.setText(title);
        }

        public void removeBadgeImage() {
        }

        public void removeMainImage() {
            mainImageView.setImageDrawable(null);
        }

    }
}