/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 11,05,2020
 */
public class MbsChannel implements Parcelable {
    @Expose
    @SerializedName("channelLogo")
    private String channelLogo;

    @Expose
    @SerializedName("eventId")
    private String eventId;

    @Expose
    @SerializedName("extendedDescription")
    private String extendedDescription;

    @Expose
    @SerializedName("freeYN")
    private String freeYN;

    @Expose
    @SerializedName("isTimeShift")
    private boolean isTimeShift;


    @Expose
    @SerializedName("multiplexMode")
    private String multiplexMode;


    @Expose
    @SerializedName("rating")
    private String rating;

    @Expose
    @SerializedName("serviceGenre")
    private String serviceGenre;

    @Expose
    @SerializedName("serviceName")
    private String serviceName;

    @Expose
    @SerializedName("streamingType")
    private String streamingType;

    protected MbsChannel(Parcel in) {
        channelLogo = in.readString();
        eventId = in.readString();
        extendedDescription = in.readString();
        freeYN = in.readString();
        isTimeShift = in.readByte() != 0;
        multiplexMode = in.readString();
        rating = in.readString();
        serviceGenre = in.readString();
        serviceName = in.readString();
        streamingType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(channelLogo);
        dest.writeString(eventId);
        dest.writeString(extendedDescription);
        dest.writeString(freeYN);
        dest.writeByte((byte) (isTimeShift ? 1 : 0));
        dest.writeString(multiplexMode);
        dest.writeString(rating);
        dest.writeString(serviceGenre);
        dest.writeString(serviceName);
        dest.writeString(streamingType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MbsChannel> CREATOR = new Creator<MbsChannel>() {
        @Override
        public MbsChannel createFromParcel(Parcel in) {
            return new MbsChannel(in);
        }

        @Override
        public MbsChannel[] newArray(int size) {
            return new MbsChannel[size];
        }
    };

    @Override
    public String toString() {
        return "MbsChannel{" +
                "channelLogo='" + channelLogo + '\'' +
                ", eventId='" + eventId + '\'' +
                ", extendedDescription='" + extendedDescription + '\'' +
                ", freeYN='" + freeYN + '\'' +
                ", isTimeShift=" + isTimeShift +
                ", multiplexMode='" + multiplexMode + '\'' +
                ", rating='" + rating + '\'' +
                ", serviceGenre='" + serviceGenre + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", streamingType='" + streamingType + '\'' +
                '}';
    }
}
