package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.system.ota.SystemUpdateManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system.SettingSystemInfoUpdateDialogFragment.KEY_UPDATE_VERSION_TYPE;

public class SettingSystemInfoUpdateVersionFragment extends SettingBaseFragment {
    public static final String CLASS_NAME = SettingSystemInfoUpdateVersionFragment.class.getName();
    private final String TAG = SettingSystemInfoUpdateVersionFragment.class.getSimpleName();

    private final long INTERVAL = TimeUtil.SEC;
    private final long TOTAL_TIME = 30 * TimeUtil.SEC;
    private CountDownTimer countDownTimer;

    private SettingSystemInfoUpdateDialogFragment.UpdateVersionType type;

    public static SettingSystemInfoUpdateVersionFragment newInstance(Bundle bundle) {
        SettingSystemInfoUpdateVersionFragment fragment = new SettingSystemInfoUpdateVersionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_system_info_update_version;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        type = (SettingSystemInfoUpdateDialogFragment.UpdateVersionType) getArguments().getSerializable(KEY_UPDATE_VERSION_TYPE);
        initText(view);
        initButton(view);
        startCountDown();
    }

    private void initText(View view) {
        int titleId = R.string.sysinfo_available_update_apk;
        int descId = R.string.sysinfo_available_update_apk_desc;
        if (type == SettingSystemInfoUpdateDialogFragment.UpdateVersionType.FIRMWARE) {
            titleId = R.string.sysinfo_available_update_firmware;
            descId = R.string.sysinfo_available_update_firmware_desc;
        }

        ((TextView) view.findViewById(R.id.title)).setText(getString(titleId));
        ((TextView) view.findViewById(R.id.desc)).setText(getString(descId));
    }

    private void initButton(View view) {
        TextView updateButton = view.findViewById(R.id.update_button);
        TextView closeButton = view.findViewById(R.id.close_button);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (type) {
                    case FIRMWARE:
                        onClickFirmwareUpdate();
                        break;
                    case APK:
                        onClickAPKUpdate();
                        break;
                }
                ((SafeDismissDialogFragment) getParentFragment()).dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SafeDismissDialogFragment) getParentFragment()).dismiss();
            }
        });
    }

    private void startCountDown() {
        TextView updateButton = getView().findViewById(R.id.count_down);
        countDownTimer = new CountDownTimer(TOTAL_TIME, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                long min = TimeUtil.getMin(millisUntilFinished);
                long sec = TimeUtil.getSec(millisUntilFinished);

                String minText = min < 10 ? "0" + min : String.valueOf(min);
                String secText = sec < 10 ? "0" + sec : String.valueOf(sec);
                String left = minText + ":" + secText;
                updateButton.setText(left);
            }

            @Override
            public void onFinish() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onFinish, timer time out....... so update type : " + type);
                }

                switch (type) {
                    case FIRMWARE:
                        onClickFirmwareUpdate();
                        break;
                    case APK:
                        onClickAPKUpdate();
                        break;
                }
                ((SafeDismissDialogFragment) getParentFragment()).dismiss();
            }
        };

        countDownTimer.start();
    }

    private void onClickFirmwareUpdate() {
        SystemUpdateManager.getInstance().startFwUpgrade();
    }

    private void onClickAPKUpdate() {
        SystemUpdateManager.getInstance().startApkUpgrade();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }
}
