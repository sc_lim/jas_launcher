/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;


/**
 * Adapts a program list for a specific channel from {@link ProgramManager} to a row of the program
 * guide table.
 */
class ProgramListAdapter extends RecyclerView.Adapter<ProgramListAdapter.ProgramItemViewHolder> implements ProgramManager.TableEntriesUpdatedListener {
    private static final String TAG = "ProgramListAdapter";

    private final ProgramGuide mProgramGuide;
    private final ProgramManager mProgramManager;
    private int mChannelIndex;
    private final String mNoInfoProgramTitle;
    private final String mBlockedProgramTitle;
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private long mChannelId;
    private OnListUpdatedCallback mOnListUpdatedCallback;

    ProgramListAdapter(Resources res, ProgramGuide programGuide, int channelIndex) {
        setHasStableIds(true);
        mProgramGuide = programGuide;
        mProgramManager = programGuide.getProgramManager();
        mChannelIndex = channelIndex;
        mNoInfoProgramTitle = res.getString(R.string.program_title_for_no_information);
        mBlockedProgramTitle = res.getString(R.string.program_title_for_blocked_channel);

        initialize();
    }

    private List<ProgramManager.TableEntry> tableEntries;


    private void initialize() {
        Channel channel = mProgramManager.getChannel(mChannelIndex);
        tableEntries = mProgramManager.getTableEntryList(channel.getId());
        if (channel == null) {
            // The channel has just been removed. Do nothing.
        } else {
            mChannelId = channel.getId();
            if (Log.INCLUDE) {
                Log.d(TAG, "initialize for channel " + mChannelId);
            }
//            mHandler.post(mUpdateRunnable);
        }
    }

    public void clearListener() {
        mOnListUpdatedCallback = null;
    }

    public void setChannelInfo(int channelPosition, OnListUpdatedCallback onListUpdatedCallback) {
        mChannelIndex = channelPosition;
        Channel channel = mProgramManager.getChannel(mChannelIndex);
        tableEntries = mProgramManager.getTableEntryList(channel.getId());
        mOnListUpdatedCallback = onListUpdatedCallback;
        if (channel == null) {
            // The channel has just been removed. Do nothing.
        } else {
            mChannelId = channel.getId();
            tableEntries = mProgramManager.getTableEntryList(channel.getId());
        }
    }

    @Override
    public void onTableEntriesUpdated() {
        Channel channel = mProgramManager.getChannel(mChannelIndex);
        boolean mNeedUpdate = needToUpdate();
        tableEntries = mProgramManager.getTableEntryList(channel.getId());
        if (channel == null) {
            // The channel has just been removed. Do nothing.
        } else {
            mChannelId = channel.getId();
            if (Log.INCLUDE) {
                Log.d(TAG, "update for channel mNeedUpdate | " + mNeedUpdate + "," + mChannelId);
            }
            if (mNeedUpdate) {
                mHandler.post(mUpdateRunnable);
            }

        }
    }


    public int getChannelIndex() {
        return mChannelIndex;
    }

    public boolean needToUpdate() {
        Channel channel = mProgramManager.getChannel(mChannelIndex);
        List<ProgramManager.TableEntry> currentTableEntry = mProgramManager.getTableEntryList(channel.getId());
        if (tableEntries == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "needToUpdate : tableEntries == nul : " + true);
            }
            return true;
        }

        if (currentTableEntry == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "needToUpdate : currentTableEntry == nul : " + true);
            }
            return false;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "needToUpdate : tableEntries.size() : " + tableEntries.size());
            Log.d(TAG, "needToUpdate : currentTableEntry.size() : " + currentTableEntry.size());
        }
        return tableEntries.size() != currentTableEntry.size();
    }

    private Runnable mUpdateRunnable = new Runnable() {
        @Override
        public void run() {
//            notifyDataSetChanged();
            if (mOnListUpdatedCallback != null) {
                mOnListUpdatedCallback.notifyUpdate(ProgramListAdapter.this);
            }
        }
    };

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (mHandler != null) {
            mHandler.removeCallbacks(mUpdateRunnable);
        }
    }

    @Override
    public int getItemCount() {
        return mProgramManager.getTableEntryCount(mChannelId);
    }

    @Override
    public int getItemViewType(int position) {
        if (mProgramManager.getTableEntry(mChannelId, position).isIsAudioChannel()) {
            return R.layout.program_guide_table_audio_item;
        } else {
            return R.layout.program_guide_table_item;
        }
    }

    @Override
    public long getItemId(int position) {
        return mProgramManager.getTableEntry(mChannelId, position).getId();
    }

    @Override
    public void onBindViewHolder(ProgramItemViewHolder holder, int position) {
        ProgramManager.TableEntry tableEntry = mProgramManager.getTableEntry(mChannelId, position);
        String gapTitle = tableEntry.isBlocked() ? mBlockedProgramTitle : mNoInfoProgramTitle;
        tableEntry.setChannelId(mChannelId);
        holder.onBind(tableEntry, mProgramGuide, gapTitle, false);
    }

    @Override
    public void onViewRecycled(ProgramItemViewHolder holder) {
        holder.onUnbind();
    }

    @Override
    public ProgramItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new ProgramItemViewHolder(itemView);
    }

    static class ProgramItemViewHolder extends RecyclerView.ViewHolder {
        // Should be called from main thread.
        ProgramItemViewHolder(View itemView) {
            super(itemView);
        }

        private boolean isBinding = false;
        void onBind(ProgramManager.TableEntry entry, ProgramGuide programGuide, String gapTitle, boolean focus) {
            isBinding = true;
            ProgramManager programManager = programGuide.getProgramManager();

            if (itemView instanceof ProgramAudioItemView) {
                ((ProgramAudioItemView) itemView)
                        .setValues(
                                programGuide,
                                entry,
                                programManager,
                                gapTitle);
            } else {
                ((ProgramItemView) itemView)
                        .setValues(
                                programGuide,
                                entry,
                                programManager,
                                gapTitle);
            }

        }

        void onUnbind() {
            isBinding = false;
            if (itemView instanceof ProgramAudioItemView) {
                ((ProgramAudioItemView) itemView).clearValues();
            } else {
                ((ProgramItemView) itemView).clearValues();
            }

        }
    }

    public interface OnListUpdatedCallback {
        void notifyUpdate(ProgramListAdapter listAdapter);
    }
}
