package kr.altimedia.launcher.jasmin.ui.component.fragment.playback;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.player.AltiMediaSource;
import com.altimedia.player.MonitorInfo;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PlacementDecision;
import kr.altimedia.launcher.jasmin.media.MediaPlayerAdapter;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackSupportFragment;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.AdsPlaybackSupportFragmentGlueHost;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.AdsPlaybackTransportControlGlue;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackGlue;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSupportDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Created by mc.kim on 19,08,2020
 */
public class AdvertisementVideoFragment extends VideoPlaybackSupportDialogFragment {
    public final static String KEY_DECISION = "placementDecision";
    public final static String KEY_TITLE = "title";
    public final static String KEY_DURATION = "duration";
    public final static String KEY_CURRENT_TIME = "currentTime";
    public static final String CLASS_NAME = AdvertisementVideoFragment.class.getName();
    private final String TAG = AdvertisementVideoFragment.class.getSimpleName();
    static final int SURFACE_NOT_CREATED = 0;
    static final int SURFACE_CREATED = 1;

    SurfaceView mVideoSurface;
    SurfaceHolder.Callback mMediaPlaybackCallback;
    int mState = SURFACE_NOT_CREATED;
    private MediaPlayerAdapter mPlayerAdapter = null;
    private AdsPlaybackTransportControlGlue<MediaPlayerAdapter> mTransportControlGlue;
    private OnAdvertisementListener mOnAdvertisementListener;
    private int mCurrentIndex = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogBlackTheme);
    }

    private final int WHAT_FINISH = 1;
    private final long CHECK_PAUSE_TIME = 30 * TimeUtil.MIN;
    private Handler mPauseHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == WHAT_FINISH) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call finish");
                }
                dismiss();
            }
        }
    };

    public static AdvertisementVideoFragment newInstance(String title, long currentTime, long duration, PlacementDecision placementDecision, OnAdvertisementListener onAdvertisementListener) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_DECISION, placementDecision);
        args.putString(KEY_TITLE, title);
        args.putLong(KEY_DURATION, duration);
        args.putLong(KEY_CURRENT_TIME, currentTime);
        AdvertisementVideoFragment fragment = new AdvertisementVideoFragment();
        fragment.setArguments(args);
        fragment.setOnAdvertisementListener(onAdvertisementListener);
        fragment.setIsEnableUseVideoSetting(false);
        return fragment;
    }

    public static AdvertisementVideoFragment newInstance(Bundle bundle, OnAdvertisementListener onAdvertisementListener) {
        AdvertisementVideoFragment fragment = new AdvertisementVideoFragment();
        fragment.setArguments(bundle);
        fragment.setOnAdvertisementListener(onAdvertisementListener);
        fragment.setIsEnableUseVideoSetting(false);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnAdvertisementListener) {
            mOnAdvertisementListener = (OnAdvertisementListener) context;
        }
    }

    public void setOnAdvertisementListener(OnAdvertisementListener onAdvertisementListener) {
        this.mOnAdvertisementListener = onAdvertisementListener;
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mTransportControlGlue != null) {
            mTransportControlGlue.pause();
        }
    }

    private SurfaceHolder.Callback mSurfaceCallback = null;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreateView");
        }

        ViewGroup root = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        mVideoSurface = (SurfaceView) LayoutInflater.from(getContext()).inflate(
                R.layout.layer_vo_surface, root, false);
        root.addView(mVideoSurface, 0);

        return root;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_MEDIA_STOP:
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            dismiss();
                        }
                        return true;
                }
                return false;
            }
        });
        return dialog;
    }


    private AdPlayerCallback mAdPlayerCallback = null;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated");
        }
        String title = getArguments().getString(KEY_TITLE);
        PlacementDecision placementDecision = getArguments().getParcelable(KEY_DECISION);
        long duration = getArguments().getLong(KEY_DURATION);
        long currentTime = getArguments().getLong(KEY_CURRENT_TIME);
        mPlayerAdapter = new MediaPlayerAdapter(view.getContext());
        mVideoSurface.getHolder().setFormat(PixelFormat.RGB_888);
        mVideoSurface.getHolder().addCallback(mPlayerAdapter.getVideoPlayerSurfaceHolderCallback());
        mSurfaceCallback = new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceCreated(holder);
                }
                mState = SURFACE_CREATED;
                mPlayerAdapter.setDisplay(holder);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceChanged(holder, format, width, height);
                }
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceDestroyed(holder);
                }
                mState = SURFACE_NOT_CREATED;
                mPlayerAdapter.setDisplay(null);
            }
        };


        mVideoSurface.getHolder().addCallback(mSurfaceCallback);
        setBackgroundType(VideoPlaybackSupportFragment.BG_NONE);

        AdsPlaybackSupportFragmentGlueHost glueHost =
                new AdsPlaybackSupportFragmentGlueHost(AdvertisementVideoFragment.this);
        mAdPlayerCallback = new AdPlayerCallback(this);
        mTransportControlGlue = new AdsPlaybackTransportControlGlue<>(getContext(), mPlayerAdapter, currentTime, duration);
        mTransportControlGlue.addPlayerCallback(mAdPlayerCallback);
        mTransportControlGlue.setSeekEnabled(true);
        mTransportControlGlue.setHost(glueHost);
        mTransportControlGlue.setTitle(title);
        setDataSource(placementDecision);
    }

    private static class AdPlayerCallback extends VideoPlaybackGlue.PlayerCallback {
        private final AdvertisementVideoFragment mAdvertisementVideoFragment;

        public AdPlayerCallback(AdvertisementVideoFragment advertisementVideoFragment) {
            this.mAdvertisementVideoFragment = advertisementVideoFragment;
        }

        @Override
        public void onPreparedStateChanged(VideoPlaybackGlue glue) {
            super.onPreparedStateChanged(glue);
        }

        @Override
        public void onPlayStateChanged(VideoPlaybackGlue glue) {
            super.onPlayStateChanged(glue);
            mAdvertisementVideoFragment.mPauseHandler.removeMessages(mAdvertisementVideoFragment.WHAT_FINISH);
            if (!glue.isPlaying()) {
                mAdvertisementVideoFragment.mPauseHandler.sendEmptyMessageDelayed(mAdvertisementVideoFragment.WHAT_FINISH,
                        mAdvertisementVideoFragment.CHECK_PAUSE_TIME);
            }
        }

        @Override
        public void onPlayCompleted(VideoPlaybackGlue glue) {
            super.onPlayCompleted(glue);
            mAdvertisementVideoFragment.requestComplete(mAdvertisementVideoFragment.mMediaList.get(mAdvertisementVideoFragment.mCurrentIndex),
                    false, mAdvertisementVideoFragment.hasNext(mAdvertisementVideoFragment.mCurrentIndex));
            if (mAdvertisementVideoFragment.hasNext(mAdvertisementVideoFragment.mCurrentIndex)) {
                mAdvertisementVideoFragment.mCurrentIndex += 1;
                mAdvertisementVideoFragment.requestPlay(mAdvertisementVideoFragment.mPlayerAdapter,
                        mAdvertisementVideoFragment.mMediaList.get(mAdvertisementVideoFragment.mCurrentIndex));
            } else {
                mAdvertisementVideoFragment.advertisementComplete();
            }
        }

        @Override
        public void onAlertTime(VideoPlaybackGlue glue) {
            super.onAlertTime(glue);
        }

        @Override
        public void onUpdateProgress(VideoPlaybackGlue glue, long time, long duration) {
            super.onUpdateProgress(glue, time, duration);
        }
    }


    @Override
    protected boolean isPlaying() {
        return mPlayerAdapter.isPlaying();
    }

    /**
     * Adds {@link SurfaceHolder.Callback} to {@link android.view.SurfaceView}.
     */

    @Override
    public void onVideoSizeChanged(int width, int height) {
        int screenWidth = getView().getWidth();
        int screenHeight = getView().getHeight();

        ViewGroup.LayoutParams p = mVideoSurface.getLayoutParams();
        if (screenWidth * height > width * screenHeight) {
            // fit in screen height
            p.height = screenHeight;
            p.width = screenHeight * width / height;
        } else {
            // fit in screen width
            p.width = screenWidth;
            p.height = screenWidth * height / width;
        }
        mVideoSurface.setLayoutParams(p);
    }


    public void setDataSource(PlacementDecision placementDecision) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setDataSource");
        }
        generateMediaSource(placementDecision);
        requestPlay(mPlayerAdapter, mMediaList.get(mCurrentIndex));
    }

    private void advertisementComplete() {
        isEof = true;
        dismiss();
    }


    private HashMap<AltiMediaSource[], Long> mediaStartTimeMap = new HashMap<>();

    private void requestPlay(MediaPlayerAdapter mediaPlayerAdapter, AltiMediaSource[] mediaSources) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestPlay");
        }
        mediaStartTimeMap.put(mediaSources, JasmineEpgApplication.SystemClock().currentTimeMillis());
        mediaPlayerAdapter.setDataSource(mediaSources);
        if (mOnAdvertisementListener != null) {
            mOnAdvertisementListener.started(placementHashMap.get(mediaSources));
        }
    }

    private boolean isEof = false;

    private void requestComplete(AltiMediaSource[] mediaSources, boolean error, boolean eof) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestComplete");
        }
        Long startTime = mediaStartTimeMap.get(mediaSources);
        if (mOnAdvertisementListener != null) {
            mOnAdvertisementListener.finished(placementHashMap.get(mediaSources), eof, startTime);
        }
    }

    private boolean hasNext(int currentIndex) {
        int size = mMediaList.size();
        return currentIndex + 1 < size;
    }

    private List<AltiMediaSource[]> mMediaList = new ArrayList<>();
    private HashMap<AltiMediaSource[], PlacementDecision.Placement> placementHashMap = new HashMap<>();

    private void generateMediaSource(PlacementDecision placementDecision) {
        if (Log.INCLUDE) {
            Log.d(TAG, "generateMediaSource");
        }
        mMediaList.clear();
        placementHashMap.clear();
        AccountManager accountManager = AccountManager.getInstance();
        List<PlacementDecision.Placement> placeList = placementDecision.getPlacement();
        for (PlacementDecision.Placement placement : placeList) {
            AltiMediaSource[] mediaSourceList = new AltiMediaSource[1];
            mediaSourceList[0] = new AltiMediaSource(getContext(), placement.getAdContentUrl(), new MonitorInfo(accountManager.getSaId(), accountManager.getAccountId(), accountManager.getProfileId()));
            mMediaList.add(mediaSourceList);
            placementHashMap.put(mediaSourceList, placement);
        }
    }

    private OnAdsListener mOnAdsListener;

    public void setOnAdsListener(OnAdsListener onAdsListener) {
        this.mOnAdsListener = onAdsListener;
    }

    public interface OnAdvertisementListener {
        void started(PlacementDecision.Placement placement);

        void finished(PlacementDecision.Placement placement, boolean endOfAd, long startTimeMillis);

    }

    public interface OnAdsListener {
        void advertisementShow();

        void advertisementComplete(boolean eof);
    }

    public interface OnAdvertisementCallback {
        void onComplete();
    }

    @Override
    public void show(@NonNull FragmentManager manager, @Nullable String tag) {
        super.show(manager, tag);
        mOnAdsListener.advertisementShow();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (Log.INCLUDE) {
            Log.d(TAG, "onDestroyView");
        }

    }


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onDismiss");
        }
        mPauseHandler.removeMessages(WHAT_FINISH);
        mPlayerAdapter.stop();
        isRequestedDismiss = true;
        mPlayerAdapter.setCallback(null);
        mPlayerAdapter.release();
        mVideoSurface.getHolder().removeCallback(mPlayerAdapter.getVideoPlayerSurfaceHolderCallback());
        mVideoSurface.getHolder().removeCallback(mSurfaceCallback);
        mTransportControlGlue.removePlayerCallback(mAdPlayerCallback);
        mVideoSurface = null;
        mState = SURFACE_NOT_CREATED;
        mOnAdsListener.advertisementComplete(isEof);
        mOnAdsListener = null;
        mOnAdvertisementListener = null;
        super.onDismiss(dialog);
    }

    private boolean isRequestedDismiss = false;

    private void requestDialogDismiss() {
        if (isRequestedDismiss) {
            return;
        }

    }

    @Override
    protected boolean checkEnableSidePanel() {
        return false;
    }


}