/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.presenter.BaseHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;

public class ProfileIconHeaderPresenter extends BaseHeaderPresenter {
    private final String TAG = ProfileIconHeaderPresenter.class.getSimpleName();

    public ProfileIconHeaderPresenter(){
        super(R.layout.presenter_mymenu_profile_icon_header, false);
    }

    @Override
    protected ViewHolder createHeaderViewHolder(ViewGroup viewGroup) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.presenter_mymenu_profile_icon_header, viewGroup, false);
        return new ProfileIconHeaderPresenterViewHolder(view);
    }

    @Override
    protected void onBindHeaderViewHolder(ViewHolder vh, Object item) {
        ListRow listRow = (ListRow) item;
        ProfileIconHeaderPresenterViewHolder viewHolder = (ProfileIconHeaderPresenterViewHolder) vh;

        String name = (String) listRow.getHeaderItem().getName();
        viewHolder.setData(name);
    }

    public class ProfileIconHeaderPresenterViewHolder extends ViewHolder {
        TextView row_header1;

        public ProfileIconHeaderPresenterViewHolder(View view) {
            super(view);

            row_header1 = view.findViewById(R.id.row_header1);
        }

        public void setData(String header){
            row_header1.setText(header);
            row_header1.setSelected(false);
        }

        @Override
        public TextView getHeaderTextView() {
            return row_header1;
        }

        public void setSelected(boolean selected){
            row_header1.setSelected(selected);
        }
    }
}
