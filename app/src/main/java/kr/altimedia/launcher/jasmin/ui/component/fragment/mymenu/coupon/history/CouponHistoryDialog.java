
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildLaidOutListener;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.coupon.CouponDataManager;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountType;
import kr.altimedia.launcher.jasmin.dm.coupon.object.MyCoupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.PointCoupon;
import kr.altimedia.launcher.jasmin.dm.def.CouponType;
import kr.altimedia.launcher.jasmin.dm.user.object.Membership;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.adapter.ButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.ButtonItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.presenter.ButtonItemPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.adapter.CouponTabButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.presenter.CouponTabButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.register.CouponRegistrationDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.list.CouponListDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.CouponPurchaseResultListener;
import kr.altimedia.launcher.jasmin.ui.util.task.UserTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class CouponHistoryDialog extends SafeDismissDialogFragment implements UserTaskManager.OnUpdateUserDiscountListener, View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = CouponHistoryDialog.class.getName();
    private static final String TAG = CouponHistoryDialog.class.getSimpleName();

    public static final String KEY_COUPON_LIST = "KEY_COUPON_LIST";
    public static final String KEY_TOTAL_BALANCE = "KEY_TOTAL_BALANCE";
    public static final String KEY_TOTAL_SIZE = "KEY_TOTAL_SIZE";

    private final Integer TYPE_SUMMARY_INFO = new Integer(0);
    private final Integer TYPE_COUPON_LIST = new Integer(1);

    private final int TAB_DISCOUNT_COUPON = ButtonItem.TYPE_BUTTON0;
    private final int TAB_CASH_COUPON = ButtonItem.TYPE_BUTTON1;
    private final int TAB_EXPIRING_COUPON = ButtonItem.TYPE_BUTTON2;

    private final int BUTTON_REDEEM = ButtonItem.TYPE_BUTTON0;
    private final int BUTTON_BUY = ButtonItem.TYPE_BUTTON1;

    private final int[] CASH_COUPON_BGs = new int[]{R.drawable.cash_coupon_bg1, R.drawable.cash_coupon_bg2, R.drawable.cash_coupon_bg3, R.drawable.cash_coupon_bg4};
    private final int[] DISCOUNT_COUPON_BGs = new int[]{R.drawable.discount_coupon_bg1, R.drawable.discount_coupon_bg2, R.drawable.discount_coupon_bg3, R.drawable.discount_coupon_bg4};

    private ArrayList<Integer> taskList = new ArrayList<>();
    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private TextView totalDiscountCouponView;
    private TextView totalCashCouponView;
    private TextView totalExpiringDiscountCouponView;
    private TextView totalExpiringCashCouponView;

    private HorizontalGridView buttonGridView;
    private HorizontalGridView tabButtonGirdView;
    private LinearLayout tabLineFocus;
    private FrameLayout tabListLayer;
    private int focusedTabButtonType = 0;
    private View rootView;

    private ArrayList<Coupon> discountCouponList = new ArrayList<>();
    private ArrayList<Coupon> cashCouponList = new ArrayList<>();
    private ArrayList<Coupon> expiringCouponList = new ArrayList<>();
    private MbsDataTask dataTask;

    private int totalDiscountSize = 0;
    private long totalPointBalance = 0;
    private int totalExpiringDiscountSize = 0;
    private long totalExpiringPointBalance = 0;
    private boolean dataReloaded;

    private final UserTaskManager userTaskManager = UserTaskManager.getInstance();
    private TvOverlayManager mTvOverlayManager;

    private CouponHistoryDialog() {
    }

    public static CouponHistoryDialog newInstance() {

        Bundle args = new Bundle();

        CouponHistoryDialog fragment = new CouponHistoryDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private void showSafeDialog(SafeDismissDialogFragment dialog) {
        if (mTvOverlayManager == null) {
            return;
        }
        mTvOverlayManager.showDialogFragment(dialog);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (dataTask != null) {
            dataTask.cancel(true);
        }
        if (discountCouponList != null && !discountCouponList.isEmpty()) {
            discountCouponList.clear();
            cashCouponList = null;
        }
        if (cashCouponList != null && !cashCouponList.isEmpty()) {
            cashCouponList.clear();
            cashCouponList = null;
        }
        if (expiringCouponList != null && !expiringCouponList.isEmpty()) {
            expiringCouponList.clear();
            expiringCouponList = null;
        }

        if (userTaskManager != null) {
            userTaskManager.removeUserDiscountListener(this);
        }

        if (mProgressBarManager != null) {
            mProgressBarManager.hide();
            mProgressBarManager.setRootView(null);
        }

        if (rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach: " + mTvOverlayManager);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_coupon_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        initView(view);
    }

    private void initView(View view) {
        dataReloaded = false;
        focusedTabButtonType = TAB_DISCOUNT_COUPON;

        initProgress(view);

        initSummary(view);
        initButton(view);
        initTab(view);

        loadSummary();
        loadMyCouponList();
    }

    private void initProgress(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void initSummary(View view) {

        totalDiscountCouponView = view.findViewById(R.id.totalDiscountCoupon);
        totalCashCouponView = view.findViewById(R.id.totalCashCoupon);
        totalExpiringDiscountCouponView = view.findViewById(R.id.totalExpiringDiscountCoupon);
        totalExpiringCashCouponView = view.findViewById(R.id.totalExpiringCashCoupon);
    }

    private void initButton(View view) {

        buttonGridView = view.findViewById(R.id.buttonGridView);

        ArrayList<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(new ButtonItem(BUTTON_REDEEM, getResources().getString(R.string.redeem)));
        buttonItems.add(new ButtonItem(BUTTON_BUY, getResources().getString(R.string.buy)));
        ButtonItemPresenter presenter = new ButtonItemPresenter(false, new ButtonItemPresenter.OnKeyListener() {
            @Override
            public boolean onKey(int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "ButtonItem.onKey: keyCode=" + keyCode + ", index=" + index);
                }
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER: {
                        if (item instanceof ButtonItem) {
                            ButtonItem button = (ButtonItem) item;
                            int type = button.getType();
                            if (type == BUTTON_REDEEM) { // redeem
                                CouponRegistrationDialog couponRegistrationDialog = CouponRegistrationDialog.newInstance();
                                couponRegistrationDialog.setCouponRegisterListener(new CouponRegistrationDialog.CouponRegisterListener() {
                                    @Override
                                    public void onSuccess(Coupon coupon) {
                                        dataReloaded = true;
                                        if (coupon instanceof DiscountCoupon) {
                                            focusedTabButtonType = TAB_DISCOUNT_COUPON;
                                        } else {
                                            focusedTabButtonType = TAB_CASH_COUPON;
                                        }
                                        loadSummary();
                                        loadMyCouponList();
                                    }

                                    @Override
                                    public void onFailed() {
                                    }
                                });
                                showSafeDialog(couponRegistrationDialog);

                            } else if (type == BUTTON_BUY) { // buy
                                showCouponShop();
                            }
                            return true;
                        }
                    }
                    case KeyEvent.KEYCODE_DPAD_DOWN: {
                        if (item instanceof ButtonItem) {
                            setTabButtonFocus(focusedTabButtonType);
                            return true;
                        }
                    }
                }
                return false;
            }
        });
        presenter.setSize(R.dimen.coupon_button_width1, R.dimen.coupon_button_height1);
        ArrayObjectAdapter buttonObjectAdapter = new ArrayObjectAdapter(presenter);
        buttonObjectAdapter.addAll(0, buttonItems);
        ButtonBridgeAdapter bridgeAdapter = new ButtonBridgeAdapter();
        bridgeAdapter.setAdapter(buttonObjectAdapter);
        buttonGridView.setAdapter(bridgeAdapter);

        int horizontalSpacing = (int) view.getResources().getDimension(R.dimen.coupon_button_horizontal_space1);
        buttonGridView.setHorizontalSpacing(horizontalSpacing);
    }

    private void initTab(View view) {

        tabButtonGirdView = view.findViewById(R.id.tabButtonGirdView);
        tabLineFocus = view.findViewById(R.id.tabLineFocus);
        tabListLayer = view.findViewById(R.id.tabListPager);

        ArrayList<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(new ButtonItem(TAB_DISCOUNT_COUPON, getResources().getString(R.string.discount_coupon)));
        buttonItems.add(new ButtonItem(TAB_CASH_COUPON, getResources().getString(R.string.cash_coupon)));
        buttonItems.add(new ButtonItem(TAB_EXPIRING_COUPON, getResources().getString(R.string.expiring_coupon)));
        CouponTabButtonPresenter presenter = new CouponTabButtonPresenter(false,
                new CouponTabButtonPresenter.OnKeyListener() {
                    @Override
                    public boolean onKey(int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "TabItem.onKey: keyCode=" + keyCode + ", index=" + index);
                        }
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_DPAD_CENTER:
                            case KeyEvent.KEYCODE_DPAD_DOWN: {
                                if (item instanceof ButtonItem) {
                                    ButtonItem button = (ButtonItem) item;
                                    setTabListFocus(button.getType());
                                    return true;
                                }
                            }
                            case KeyEvent.KEYCODE_DPAD_UP: {
                                if (item instanceof ButtonItem) {
                                    setTabButtonSelected(focusedTabButtonType);
                                    setTabButtonActivated(false);

                                    buttonGridView.requestFocus();
                                    View child = buttonGridView.getChildAt(BUTTON_REDEEM);
                                    if (child != null) {
                                        child.requestFocus();
                                    }
                                    return true;
                                }
                            }
                        }
                        return false;
                    }
                },
                new CouponTabButtonPresenter.OnFocusListener() {
                    @Override
                    public void onFocus(View view, boolean hasFocus, Object item) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "TabItem.onFocus: hasFocus=" + hasFocus);
                        }
                        if (item instanceof ButtonItem) {
                            if (hasFocus) {
                                ButtonItem button = (ButtonItem) item;
                                int type = button.getType();
                                focusedTabButtonType = type;
                                view.setSelected(false);
                                setTabList(type, false);
                                tabLineFocus.setSelected(true);
                            } else {
                                tabLineFocus.setSelected(false);
                            }
                        }
                    }
                });
        presenter.setSize(R.dimen.coupon_button_width2, R.dimen.coupon_button_height2);
        ArrayObjectAdapter tabButtonObjectAdapter = new ArrayObjectAdapter(presenter);
        tabButtonObjectAdapter.addAll(0, buttonItems);
        CouponTabButtonBridgeAdapter bridgeAdapter = new CouponTabButtonBridgeAdapter();
        bridgeAdapter.setAdapter(tabButtonObjectAdapter);
        tabButtonGirdView.setAdapter(bridgeAdapter);

        int horizontalSpacing = (int) view.getResources().getDimension(R.dimen.coupon_button_horizontal_space2);
        tabButtonGirdView.setHorizontalSpacing(horizontalSpacing);

        tabButtonGirdView.setOnChildLaidOutListener(new OnChildLaidOutListener() {
            @Override
            public void onChildLaidOut(ViewGroup parent, View view, int position, long id) {
                setTabView(TAB_DISCOUNT_COUPON, false);
            }
        });
    }

    private void showProgress() {
        mProgressBarManager.show();
    }

    private void hideProgress() {
        mProgressBarManager.hide();
    }

    private void checkLoadList(Integer task, boolean added) {
        if (added) {
            if (taskList.contains(task)) {
                return;
            }
            taskList.add(task);
        } else {
            taskList.remove(task);
        }
        if (taskList.isEmpty()) {
            hideProgress();
        }
    }

    private void loadSummary() {
        userTaskManager.addUserDiscountListener(this);
        userTaskManager.loadUserDiscountInfo(getContext());
    }

    private void setSummary(int totalDiscountSize, long totalPointPrice, int expiringDiscountSize, long expiringPointPrice) {
        totalDiscountCouponView.setText(StringUtil.getFormattedNumber(totalDiscountSize));
        totalCashCouponView.setText(StringUtil.getFormattedNumber(totalPointPrice));
        totalExpiringDiscountCouponView.setText(StringUtil.getFormattedNumber(expiringDiscountSize));
        totalExpiringCashCouponView.setText(StringUtil.getFormattedNumber(expiringPointPrice));
    }

    private void loadMyCouponList() {
        showProgress();

        CouponDataManager couponDataManager = new CouponDataManager();
        dataTask = couponDataManager.getAllCouponList(
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                true,
                new MbsDataProvider<String, List<MyCoupon>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(TYPE_COUPON_LIST, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadMyCouponList: onFailed");
                        }
                        setTabView(TAB_DISCOUNT_COUPON, false);
                    }

                    @Override
                    public void onSuccess(String key, List<MyCoupon> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadMyCouponList: onSuccess: result=" + result);
                        }

                        discountCouponList.clear();
                        cashCouponList.clear();
                        expiringCouponList.clear();

                        if (result != null) {
                            int dcBgIdx = 0;
                            int pcBgIdx = 0;
                            for (MyCoupon coupon : result) {
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "loadMyCouponList: onSuccess: coupon=" + coupon);
                                }
                                CouponType couponType = coupon.getType();
                                if (couponType != null) {
                                    if (couponType == CouponType.DISCOUNT && coupon.getDiscountType() != DiscountType.AMOUNT) {
                                        DiscountCoupon dc = coupon.getDiscountCoupon();
                                        if (dcBgIdx >= DISCOUNT_COUPON_BGs.length) {
                                            dcBgIdx = 0;
                                        }
                                        dc.setBgImage(DISCOUNT_COUPON_BGs[dcBgIdx]);
                                        dcBgIdx++;
                                        try {
                                            if (dc.isExpiringYn()) { // || isThisMonth(dc.getExpireDate())
                                                expiringCouponList.add(dc);
                                            }
                                            discountCouponList.add(dc);
                                        } catch (Exception e) {
                                            discountCouponList.add(dc);
                                        }

                                    } else {
                                        PointCoupon pc = coupon.getPointCoupon();
                                        if (pcBgIdx >= CASH_COUPON_BGs.length) {
                                            pcBgIdx = 0;
                                        }
                                        pc.setBgImage(CASH_COUPON_BGs[pcBgIdx]);
                                        pcBgIdx++;
                                        try {
                                            if (pc.isExpiringYn()) { // isThisMonth(pc.getExpireDate())
                                                expiringCouponList.add(pc);
                                            }
                                            cashCouponList.add(pc);
                                        } catch (Exception e) {
                                            cashCouponList.add(pc);
                                        }
                                    }
                                }
                            }
                        }

                        if (dataReloaded) {
                            setTabView(focusedTabButtonType, true);
                        } else {
                            setTabView(TAB_DISCOUNT_COUPON, false);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        setTabView(TAB_DISCOUNT_COUPON, false);

                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }
                });
    }

    private void setTabButtonActivated(boolean activated) {
        try {
            for (int i = 0; i < tabButtonGirdView.getChildCount(); i++) {
                View child = tabButtonGirdView.getChildAt(i);
                if (child != null) {
                    child.setActivated(activated);
                }
            }
        }catch (Exception e){
        }
    }

    private void setTabButtonSelected(int type) {
        try {
            View child = tabButtonGirdView.getChildAt(type);
            if (child != null) {
                child.setSelected(true);
            }
        }catch (Exception e){
        }
    }

    private void setTabButtonFocus(int type) {
        tabButtonGirdView.requestFocus();
        View child = tabButtonGirdView.getChildAt(type);
        if (child != null) {
            child.requestFocus();
        }

        setTabButtonActivated(true);
    }

    private void setTabListFocus(int type) {
        if (isDataAvailable(type)) {
            setTabButtonSelected(type);
            setTabButtonActivated(false);

            tabListLayer.requestFocus();
        }
    }

    private void setTabView(int type, boolean hasFocus) {
        dataReloaded = false;
        setTabButtonFocus(type);
        setTabList(type, hasFocus);
    }

    private void setTabList(int type, boolean hasFocus) {
        getChildFragmentManager().beginTransaction().replace(R.id.tabListPager, getTabList(type, hasFocus)).commit();
    }

    private Fragment getTabList(int type, boolean hasFocus) {
        if (Log.INCLUDE) {
            Log.d(TAG, "getTabList: type=" + type);
        }
        switch (type) {
            case TAB_DISCOUNT_COUPON:
                CouponDiscountListFragment discountListFragment = CouponDiscountListFragment.newInstance(discountCouponList, totalDiscountSize);
                discountListFragment.setOnFocusRequestListener(new CouponBaseListFragment.OnFocusRequestListener() {
                    @Override
                    public void onFocusRequest(int direction) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getTabList: onFocusRequest: direction=" + direction);
                        }
                        if (direction == View.FOCUS_UP) {
                            setTabButtonFocus(TAB_DISCOUNT_COUPON);
                        }
                    }
                });
                if (hasFocus) {
                    discountListFragment.setOnVisibleCompleteListener(new CouponBaseListFragment.OnVisibleCompleteListener() {
                        @Override
                        public void onVisibleComplete() {
                            discountListFragment.setOnVisibleCompleteListener(null);
                            setTabListFocus(TAB_DISCOUNT_COUPON);
                        }
                    });
                }
                return discountListFragment;

            case TAB_CASH_COUPON:
                CouponCashListFragment cashListFragment = CouponCashListFragment.newInstance(cashCouponList, totalPointBalance);
                cashListFragment.setOnFocusRequestListener(new CouponBaseListFragment.OnFocusRequestListener() {
                    @Override
                    public void onFocusRequest(int direction) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getTabList: onFocusRequest: direction=" + direction);
                        }
                        if (direction == View.FOCUS_UP) {
                            setTabButtonFocus(TAB_CASH_COUPON);
                        }
                    }
                });
                if (hasFocus) {
                    cashListFragment.setOnVisibleCompleteListener(new CouponBaseListFragment.OnVisibleCompleteListener() {
                        @Override
                        public void onVisibleComplete() {
                            cashListFragment.setOnVisibleCompleteListener(null);
                            setTabListFocus(TAB_CASH_COUPON);
                        }
                    });
                }
                return cashListFragment;

            case TAB_EXPIRING_COUPON:
                CouponExpiringListFragment expiringListFragment = CouponExpiringListFragment.newInstance(expiringCouponList, totalExpiringDiscountSize, totalExpiringPointBalance);
                expiringListFragment.setOnFocusRequestListener(new CouponBaseListFragment.OnFocusRequestListener() {
                    @Override
                    public void onFocusRequest(int direction) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getTabList: onFocusRequest: direction=" + direction);
                        }
                        if (direction == View.FOCUS_UP) {
                            setTabButtonFocus(TAB_EXPIRING_COUPON);
                        }
                    }
                });
                return expiringListFragment;
        }

        return null;
    }

    private boolean isThisMonth(Date tagetDate) {
        if (tagetDate != null) {
            long targetTime = tagetDate.getTime();
            Calendar currCal = Calendar.getInstance();
            currCal.setTimeInMillis(JasmineEpgApplication.SystemClock().currentTimeMillis());
            int currYear = currCal.get(Calendar.YEAR);
            int currMonth = currCal.get(Calendar.MONTH) + 1;

            Calendar targetCal = Calendar.getInstance();
            targetCal.setTimeInMillis(targetTime);
            int targetYear = targetCal.get(Calendar.YEAR);
            int targetMonth = targetCal.get(Calendar.MONTH) + 1;

            int month1 = currYear * 12 + currMonth;
            int month2 = targetYear * 12 + targetMonth;
            return month1 == month2;
        }
        return false;
    }

    private boolean isDataAvailable(int type) {
        switch (type) {
            case TAB_DISCOUNT_COUPON:
                if (discountCouponList != null && !discountCouponList.isEmpty()) {
                    return true;
                }
                break;
            case TAB_CASH_COUPON:
                if (cashCouponList != null && !cashCouponList.isEmpty()) {
                    return true;
                }
                break;
            case TAB_EXPIRING_COUPON:
                if (expiringCouponList != null && !expiringCouponList.isEmpty()) {
                    return true;
                }
                break;
        }
        return false;
    }

    @Override
    public void notifyUserDiscountChange(int pointCouponSize, int discountCouponSize, ArrayList<DiscountCoupon> discountCoupons, Membership membership) {
        totalDiscountSize = userTaskManager.getDiscountCouponSize();
        totalPointBalance = userTaskManager.getPointCoupon();
        totalExpiringDiscountSize = userTaskManager.getExpiringDiscountCouponSize();
        totalExpiringPointBalance = userTaskManager.getExpiringPointCouponPrice();

        setSummary(totalDiscountSize, totalPointBalance, totalExpiringDiscountSize, totalExpiringPointBalance);
    }

    @Override
    public void notifyFail(int key) {
        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getContext());
        failNoticeDialogFragment.show(mTvOverlayManager,getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
    }

    @Override
    public void notifyError(String errorCode, String message) {
        ErrorDialogFragment dialogFragment =
                ErrorDialogFragment.newInstance(TAG, errorCode, message);
        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
    }

    private void showCouponShop() {
        CouponListDialog couponListDialog = CouponListDialog.newInstance();
        couponListDialog.setCouponPurchaseResultListener(new CouponPurchaseResultListener() {
            @Override
            public void onSuccess(String couponId) {
                dataReloaded = true;
                focusedTabButtonType = TAB_CASH_COUPON;
                loadSummary();
                loadMyCouponList();
            }

            @Override
            public void onFailure() {
            }
        });
        showSafeDialog(couponListDialog);
    }
}
