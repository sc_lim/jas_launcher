/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by mc.kim on 2018-02-01.
 */
public class PermissionUtils {
    private static final String TAG = PermissionUtils.class.getSimpleName();
    public static final int CODE_HOT_KEY = 3;
    public static final int CODE_CSP = 5;
    public static final int CODE_BOOT = 4;
    /**
     * Permission to read the TV listings.
     */

    public static final String PERMISSION_RECORD_AUDIO = Manifest.permission.RECORD_AUDIO;
    public static final String PERMISSION_READ_EXTERNAL = Manifest.permission.READ_EXTERNAL_STORAGE;
    public static final String PERMISSION_READ_TV_LISTINGS = "android.permission.READ_TV_LISTINGS";
    public final static String PERMISSION_NOTIFICATION = "android.permission.ACCESS_NOTIFICATIONS";
    public final static String PERMISSION_FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;

    private static Boolean sHasAccessAllEpgPermission;
    private static Boolean sHasAccessWatchedHistoryPermission;
    private static Boolean sHasModifyParentalControlsPermission;

    public static boolean hasAccessAllEpg(Context context) {
        if (sHasAccessAllEpgPermission == null) {
            sHasAccessAllEpgPermission = context.checkSelfPermission(
                    "com.android.providers.tv.permission.ACCESS_ALL_EPG_DATA")
                    == PackageManager.PERMISSION_GRANTED;
        }
        return sHasAccessAllEpgPermission;
    }

    public static boolean hasAccessWatchedHistory(Context context) {
        if (sHasAccessWatchedHistoryPermission == null) {
            sHasAccessWatchedHistoryPermission = context.checkSelfPermission(
                    "com.android.providers.tv.permission.ACCESS_WATCHED_PROGRAMS")
                    == PackageManager.PERMISSION_GRANTED;
        }
        return sHasAccessWatchedHistoryPermission;
    }

    public static boolean hasModifyParentalControls(Context context) {
        if (sHasModifyParentalControlsPermission == null) {
            sHasModifyParentalControlsPermission = context.checkSelfPermission(
                    "android.permission.MODIFY_PARENTAL_CONTROLS")
                    == PackageManager.PERMISSION_GRANTED;
        }
        return sHasModifyParentalControlsPermission;
    }

    public static boolean hasReadTvListings(Context context) {
        return context.checkSelfPermission(PERMISSION_READ_TV_LISTINGS)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasInternet(Context context) {
        return context.checkSelfPermission("android.permission.INTERNET")
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasAccessExternalStorage(Context context) {
        return context.checkSelfPermission(PERMISSION_READ_EXTERNAL)
                == PackageManager.PERMISSION_GRANTED;
    }


    public static boolean hasRecordAudio(Context context) {
        return context.checkSelfPermission(PERMISSION_RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasNotification(Context context) {
        return context.checkSelfPermission(PERMISSION_NOTIFICATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasFineLocation(Context context) {
        return context.checkSelfPermission(PERMISSION_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static String[] getNeededBasicPermission(Context context) {
        ArrayList<String> permissionGroup = new ArrayList<>();
        boolean hasExternal = hasAccessExternalStorage(context);
        boolean hasRecordAudio = hasRecordAudio(context);

        Log.d(TAG, "getNeededBasicPermission | hasExternal : " + hasExternal);
        Log.d(TAG, "getNeededBasicPermission | hasRecordAudio : " + hasRecordAudio);
        if (!hasRecordAudio) {
            permissionGroup.add(PERMISSION_RECORD_AUDIO);
        }
        if (!hasExternal) {
            permissionGroup.add(PERMISSION_READ_EXTERNAL);
        }

        if (!permissionGroup.isEmpty()) {
            String[] array = permissionGroup.toArray(new String[permissionGroup.size()]);
            return array;
        } else {
            return null;
        }
    }

    public static void requestPermissions(Activity context, String[] permission, int requestCode) {
        Log.d(TAG, "requestPermissions  permission : " + permission + ", resultCode" + requestCode);
        context.requestPermissions(permission, requestCode);
//        ActivityCompat.requestPermissions(context, permission, resultCode);
    }

    public static void requestPermissions(Activity context, String permission, int resultCode) {
        requestPermissions(context, new String[]{permission}, resultCode);
    }

    public static boolean hasAccessPermission(Context context, String permission) {
        Log.d(TAG, "hasAccessPermission | permission : " + permission);

        return context.checkSelfPermission(permission)
                == PackageManager.PERMISSION_GRANTED;
    }
}