/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter.CouponPresenter;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class CouponSelectDialogFragment extends SafeDismissDialogFragment implements CouponItemBridgeAdapter.OnCouponListener {
    public static final String CLASS_NAME = CouponSelectDialogFragment.class.getName();
    public static final String TAG = CouponSelectDialogFragment.class.getSimpleName();

    private static final String KEY_PRICE = "PRICE";
    private static final String KEY_COUPON_LIST = "COUPON_LIST";

    private final int VISIBLE_COUPON_COUNT = 4;

    private TextView coupons;
    private TextView discount;
    private TextView discountAmountInfo;
    private ImageIndicator leftIndicator;
    private ImageIndicator rightIndicator;
    private HorizontalGridView couponGridView;

    private float price;
    private ArrayList<Coupon> couponList;

    private CouponItemBridgeAdapter couponItemBridgeAdapter;
    private OnSelectCouponDiscountListener onSelectCouponDiscountListener;

    public CouponSelectDialogFragment() {

    }

    public static CouponSelectDialogFragment newInstance(float price, ArrayList<DiscountCoupon> couponList) {

        Bundle args = new Bundle();
        args.putFloat(KEY_PRICE, price);
        args.putParcelableArrayList(KEY_COUPON_LIST, couponList);

        CouponSelectDialogFragment fragment = new CouponSelectDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnSelectCouponDiscountListener(OnSelectCouponDiscountListener onSelectCouponDiscountListener) {
        this.onSelectCouponDiscountListener = onSelectCouponDiscountListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogBlackTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_coupon_select, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        Bundle args = getArguments();
        price = args.getFloat(KEY_PRICE);
        couponList = args.getParcelableArrayList(KEY_COUPON_LIST);

        initStatus(view);
        initButton(view);
        initCouponGirdView(view);
    }

    private void initStatus(View view) {
        TextView price = view.findViewById(R.id.price);
        price.setText(StringUtil.getFormattedPrice(this.price));

        coupons = view.findViewById(R.id.coupons);
        discount = view.findViewById(R.id.discount);
        discountAmountInfo = view.findViewById(R.id.discount_amount_info);

        leftIndicator = view.findViewById(R.id.leftIndicator);
        rightIndicator = view.findViewById(R.id.rightIndicator);
    }

    private void initCouponGirdView(View view) {
        int size = couponList.size();
        this.coupons.setText(String.valueOf(size));
        leftIndicator.initIndicator(size, VISIBLE_COUPON_COUNT);
        rightIndicator.initIndicator(size, VISIBLE_COUPON_COUNT);

        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter(new CouponPresenter());
        arrayObjectAdapter.addAll(0, couponList);

        couponGridView = getGridView(view);
        couponItemBridgeAdapter = new CouponItemBridgeAdapter(arrayObjectAdapter, couponGridView);
        couponItemBridgeAdapter.setOnCouponListener(this);

        couponGridView.setAdapter(couponItemBridgeAdapter);
        couponGridView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int focus = couponGridView.getChildPosition(recyclerView.getFocusedChild());

                leftIndicator.updateArrow(focus);
                rightIndicator.updateArrow(focus);
            }
        });
    }

    private HorizontalGridView getGridView(View view) {
        HorizontalGridView couponGridView = view.findViewById(R.id.coupon_grid_view);

        int size = couponList.size();
        if (size == 0) {
            couponGridView.setEnabled(false);
        }

        int gap = (int) view.getResources().getDimension(R.dimen.coupon_subscription_button_spacing);
        int dimen = (int) view.getResources().getDimension(R.dimen.coupon_subscription_box_width) + gap;
        int width = size < VISIBLE_COUPON_COUNT ? (size * dimen - gap) : (VISIBLE_COUPON_COUNT * dimen - gap);
        ViewGroup.LayoutParams params = couponGridView.getLayoutParams();
        params.width = width;
        couponGridView.setLayoutParams(params);
        couponGridView.setHorizontalSpacing(gap);

        return couponGridView;
    }

    private void initButton(View view) {
        TextView cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectCouponDiscountListener.onSelectCouponDiscount(null);
                dismiss();
            }
        });
        cancel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                discount.setText("0");
                discountAmountInfo.setText("");
            }
        });
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onSelectCouponDiscountListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        couponGridView.clearOnScrollListeners();
        couponItemBridgeAdapter.setOnCouponListener(null);
    }

    @Override
    public void onFocusCoupon(DiscountCoupon coupon) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onFocusCoupon, coupon : " + coupon);
        }

        float f = CouponUtil.getCouponDiscountValue(price, coupon);
        int value = Math.round(f);
        int maxAmount = coupon.getMaxAmount();
        int originalValue = CouponUtil.getCouponOriginalDiscountValue(price, coupon);

        if (Log.INCLUDE) {
            Log.d(TAG, "value : " + value + ", maxAmount : " + maxAmount + ", originalValue : " + originalValue);
        }

        String str = "";
        String amountInfo = "";
        if (maxAmount == 0 || value < maxAmount || (originalValue == maxAmount && value == originalValue)) {
            amountInfo = value > 0 ? "-" : "";
            str = StringUtil.getFormattedPrice(value);
        } else {
            amountInfo = getString(R.string.up_to);
            str = StringUtil.getFormattedPrice(maxAmount);
        }

        discount.setText(str);
        discountAmountInfo.setText(amountInfo);
    }

    @Override
    public void onSelectCoupon(DiscountCoupon coupon) {
        onSelectCouponDiscountListener.onSelectCouponDiscount(coupon);
        dismiss();
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public interface OnSelectCouponDiscountListener {
        void onSelectCouponDiscount(DiscountCoupon coupon);
    }
}
