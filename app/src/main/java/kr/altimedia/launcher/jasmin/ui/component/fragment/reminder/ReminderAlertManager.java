/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.reminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ProcessLifecycleOwner;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

import static android.app.AlarmManager.RTC;
import static android.content.Context.ALARM_SERVICE;

public class ReminderAlertManager {
    public static final String BOOK_EVENT_CHANGE = "kr.altimedia.intent.action.Update.Reminder";
    public static final String KEY_PROGRAM_ID = "programId";
    public static final String CLASS_NAME = ReminderAlertManager.class.getName();
    private final String TAG = ReminderAlertManager.class.getSimpleName();

    public static final String REMINDER_ALERT = "REMINDER_ALTER";

    private static final ReminderAlertManager INSTANCE = new ReminderAlertManager();

    //private HashMap<Long, ProgramImpl> reminderList = new HashMap<>();
    private ArrayList<Reminder> reminderList = new ArrayList<>();

    private ArrayList<OnReminderListener> onReminderListeners = new ArrayList<>();

    private ReminderAlertManager() {
    }

    public static ReminderAlertManager getInstance() {
        return INSTANCE;
    }

    private static long getChannelId(int serviceId) {
        Channel channel = JasChannelManager.getInstance().getChannel(String.valueOf(serviceId));
        return channel != null ? channel.getId() : -1;
    }
    private static String getChannelName(int serviceId) {
        Channel channel = JasChannelManager.getInstance().getChannel(String.valueOf(serviceId));
        return channel != null ? channel.getDisplayName() : null;
    }
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("(MM/dd)HH:mm");
    private String getType(long startTime, long endTime) {
        long now = ReminderAlertManager.now();
        if (endTime < now) {
            return "PAST:-"+TimeUnit.MILLISECONDS.toMinutes(now - endTime) + "m";
        } else if (now < startTime) {
            return "FUTURE:+"+TimeUnit.MILLISECONDS.toMinutes(startTime - now) + "m";
        } else {
            return "PRESENT:"+TimeUnit.MILLISECONDS.toMinutes(now - startTime) + "m";
        }
    }
    private String programToString(Program prog) {
        return prog == null ? null :
                "Program{" +
                "serviceId='" + prog.getServiceId() + '\'' +
                ", channelId='" + getChannelId(prog.getServiceId()) + '\'' +
                ", channelName='" + getChannelName(prog.getServiceId()) + '\'' +
                ", programId='" + prog.getProgramId() + '\'' +
                ", time=" + FORMAT.format(new Date(prog.getStartTimeUtcMillis())) +
                "~" + FORMAT.format(new Date(prog.getEndTimeUtcMillis())) +
                " (" + getType(prog.getStartTimeUtcMillis(), prog.getEndTimeUtcMillis()) + ")" +
                ", duration=" + TimeUnit.MILLISECONDS.toMinutes(prog.getEndTimeUtcMillis() - prog.getStartTimeUtcMillis()) + "m" +
                ", title='" + prog.getTitle() + '\'' +
                ", program=" + "0x" + Integer.toHexString(prog.hashCode()) +
                '}';
    }

    static long now() {
        return JasmineEpgApplication.SystemClock().currentTimeMillis();
    }
    private String timeToString(long ms) {
        return FORMAT.format(new Date(ms));
    }

    private void printReminderList() {
        if (Log.INCLUDE) {
            Log.d(TAG, "printReminderList() size:"+reminderList.size());

            Reminder reminder;

            for (int i = 0 ; i < reminderList.size() ; i++) {
                reminder = reminderList.get(i);
                Log.d(TAG, "printReminderList() list["+i+"] reminder:"+reminder);
            }
        }
    }

    public Reminder getReminder(Program program) {
        for (Reminder reminder : reminderList) {
            if (reminder.equals(program)) {
                return reminder;
            }
        }
        return null;
    }

    public boolean isReserved(Program program) {
        boolean reserved = getReminder(program) != null;
        if (Log.INCLUDE) {
            Log.d(TAG, "isReserved():"+reserved+", program:"+programToString(program));
        }
        return reserved;
    }

    public void initReminderList(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "initReminderLister(), now:"+timeToString(now()));
        }

        ArrayList<Reminder> checkedPrograms = new ArrayList<>();
        ArrayList<Reminder> reminderPrograms = readReminderList(context);
        ProgramDataManager mProgramDataManager = TvSingletons.getSingletons(context).getProgramDataManager();

        //#1 check reminder programs in EPG
        for (Reminder reminder : reminderPrograms) {
            long channelId = reminder.getChannelId();
            long startTime = reminder.getStartTime();
            List<Program> programList = mProgramDataManager.getPrograms(channelId, startTime);

            if (Log.INCLUDE) {
                Log.d(TAG, "initReminderList, channelId : " + channelId + ", startTime : " + startTime
                        + ", programList : " + (programList != null ? programList.size() : "0"));
            }

            boolean hasRemindedProgram = false;
            for (Program program : programList) {
                if (reminder.equals(program)) {
                    hasRemindedProgram = true;
                    break;
                }
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "initReminderList, reminded program in EPG"
                        + ", hasRemindedProgram : " + hasRemindedProgram
                        + ", reminderProgram : " + reminder);
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "initReminderLister"
                    + ", reminderPrograms : " + reminderPrograms.size()
                    + ", checkedPrograms : " + checkedPrograms.size());
        }

        //#2 clear old list
        reminderList.clear();
        writeReminderList(context);

        //#3 add & set checked reminder list
        for (Reminder reminder : reminderPrograms) {
            if (isFutureReminder(reminder)) {
                reminderList.add(reminder);
                setReminderToAlarm(context, reminder);
            } else {
                if (Log.INCLUDE) {
                    Log.d(TAG, "requestAddReminder, it is not future program");
                }
            }
        }
        writeReminderList(context);
    }

    /*
    public void initReminderList(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "initReminderLister");
        }

        ArrayList<Reminder> checkedPrograms = new ArrayList<>();
        ArrayList<Reminder> reminderPrograms = readReminderList(context);
        ProgramDataManager mProgramDataManager = TvSingletons.getSingletons(context).getProgramDataManager();

        //#1 check reminder programs in EPG
        for (Reminder reminder : reminderPrograms) {
            String serviceId = reminder.getServiceId();
            long startTime = reminder.getStartTime();
            List<Program> programList = mProgramDataManager.getPrograms(channelId, startTime);

            if (Log.INCLUDE) {
                Log.d(TAG, "initReminderList, channelId : " + channelId + ", startTime : " + startTime
                        + ", programList : " + (programList != null ? programList.size() : "0"));
            }

            boolean hasRemindedProgram = false;
            for (Program program : programList) {
                if (reminderProgram.getId() == program.getId()) {
                    hasRemindedProgram = true;
                    checkedPrograms.add((ProgramImpl) program.toParcelable());
                    break;
                }
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "initReminderList, reminded program in EPG"
                        + ", hasRemindedProgram : " + hasRemindedProgram
                        + ", reminderProgram : " + reminderProgram);
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "initReminderLister"
                    + ", reminderPrograms : " + reminderPrograms.size()
                    + ", checkedPrograms : " + checkedPrograms.size());
        }

        //#2 clear old list
        reminderList.clear();
        writeReminderList(context);

        //#3 add & set checked reminder list
        for (ProgramImpl program : checkedPrograms) {
            requestAddReminder(context, program);
        }
    }*/

    public List<Reminder> getReminderList() {
        return Collections.unmodifiableList(reminderList);
    }

    public boolean requestAddReminder(Context context, Program program) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestAddReminder(), now:"+timeToString(now())+", program:" + programToString(program));
        }

        if (!isFutureProgram(program)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "requestAddReminder, it is not future program");
            }
            return false;
        }

        if (isConflictReminder(program)) {
            return false;
        }

        Reminder reminder = addReminder(program);
        if (reminder != null) {
            setReminderToAlarm(context, reminder);
            writeReminderList(context);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "requestAddReminder, reminder : " + reminder + ", program : " + program);
        }

        return reminder != null;
    }

    public boolean requestRemoveReminder(Context context, Program program) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestRemoveReminder(), program " + programToString(program));
        }

        Reminder ret = removeReminder(program);
        if (ret != null) {
            cancelReminderToAlarm(context, ret);
            writeReminderList(context);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "requestRemoveReminder, isRemove : " + (ret != null) + ", program : " + program);
        }

        return (ret != null);
    }

    public boolean requestRemoveReminder(Context context, Reminder reminder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestRemoveReminder, reminder:" + reminder);
        }

        Reminder ret = removeReminder(reminder);
        if (ret != null) {
            cancelReminderToAlarm(context, ret);
            writeReminderList(context);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "requestRemoveReminder, isRemove : " + (ret != null) + ", reminder : " + reminder);
        }

        return (ret != null);
    }

    private Reminder addReminder(Program program) {
        Reminder reminder = new Reminder(program);
        reminderList.add(reminder);
        Collections.sort(reminderList);

        if (Log.INCLUDE) {
            Log.d(TAG, "addReminder reminderList size : " + reminderList.size());
        }
        notifyReminderListChange(reminder);

        return reminder;
    }

    private Reminder removeReminder(Program program) {
        return removeReminder(getReminder(program));
    }

    private Reminder removeReminder(Reminder reminder) {
        reminderList.remove(reminder);

        if (reminder != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "removeReminder reminderList size : " + reminderList.size());
            }
            notifyReminderListChange(reminder);
            return reminder;
        }

        return null;
    }

    private boolean isFutureReminder(Reminder reminder) {
        return now() < reminder.getStartTime();
    }

    private boolean isFutureProgram(Program program) {
        return now() < program.getStartTimeUtcMillis();
    }

    private boolean isConflictReminder(Program program) {
        Reminder conflictedReminder = getConflictReminder(program);
        boolean isConflict = conflictedReminder != null;

        if (isConflict) {
            if (Log.INCLUDE) {
                Log.d(TAG, "isConflictReminder, conflict reminder : " + conflictedReminder);
            }
            notifyReminderConflict(program, conflictedReminder);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "isConflictReminder, isConflict : " + isConflict);
        }

        return isConflict;
    }

    private Reminder getConflictReminder(Reminder reminder) {
        return getConflictReminder(reminder.getProgram());
    }

    private Reminder getConflictReminder(Program program) {
        long programStart = (program.getStartTimeUtcMillis() / TimeUtil.MIN) * TimeUtil.MIN;

        if (Log.INCLUDE) {
            Log.d(TAG, "getConflictReminder, " + program.getTitle() + ", programStart : " + programStart + "(" + program.getStartTimeUtcMillis() + ")");
        }

        for (Reminder reminder : reminderList) {
            long remindedStart = (reminder.getStartTime() / TimeUtil.MIN) * TimeUtil.MIN;
            if (programStart == remindedStart) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "getConflictReminder, remindedStart : " + remindedStart + "(" + reminder.getStartTime() + ")");
                }
                return reminder;
            }
        }
        return null;
    }

    private void setReminderToAlarm(Context context, Reminder reminder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setReminderToAlarm, program : " + reminder);
        }

        Bundle bundle = new Bundle();
        bundle.putParcelable(REMINDER_ALERT, reminder);
        Intent alarmIntent = new Intent(context, ReminderAlertService.class);
        alarmIntent.putExtra(REMINDER_ALERT, bundle);

        long alarmTime = reminder.getStartTime() - TimeUtil.MIN;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, reminder.hashCode(), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.setExact(RTC, alarmTime, pendingIntent);
    }

    private void cancelReminderToAlarm(Context context, Reminder reminder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "cancelReminderToAlarm, reminder : " + reminder);
        }

        Intent intent = new Intent(context, ReminderAlertService.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, reminder.hashCode(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (pendingIntent != null) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();
        }
    }

    private List<ReminderAlertDialog> reminderAlertDialogList = new ArrayList<>();

    public void clearAllDialog() {

        if (Log.INCLUDE) {
            Log.d(TAG, "clearAllDialog size | before : " + reminderAlertDialogList.size());
        }
        for (ReminderAlertDialog mReminderAlertDialog : reminderAlertDialogList) {
            mReminderAlertDialog.dismiss();
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "clearAllDialog size | after : " + reminderAlertDialogList.size());
        }
    }

    public void showReminderAlertDialog(Context context, Reminder reminder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showReminderAlertDialog, reminderInfo : " + reminder);
        }

        ReminderAlertDialog reminderAlertDialog = new ReminderAlertDialog(context, reminder);
        reminderAlertDialog.setOnReminderAlertActionListener(new ReminderAlertDialog.OnReminderAlertActionListener() {
            @Override
            public void onReminderAlertWatch(Reminder reminder) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReminderAlertWatch");
                }
                reminderAlertDialog.dismiss();
                notifyReminderWatch(reminder);
            }

            @Override
            public void onReminderAlertCancel() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReminderAlertCancel");
                }
                reminderAlertDialog.dismiss();
            }

            @Override
            public void onShow() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onShowing");
                }
                reminderAlertDialogList.add(reminderAlertDialog);
            }

            @Override
            public void onDismiss() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onDismiss");
                }

                reminderAlertDialogList.remove(reminderAlertDialog);
            }
        });
        reminderAlertDialog.show(context);
    }

    public void showReminderConflictDialog(Context context, FragmentManager fragmentManager, Program program, Reminder conflictedReminder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showReminderConflictDialog");
        }

        ReminderConflictDialog reminderConflictDialog = ReminderConflictDialog.newInstance(conflictedReminder);
        reminderConflictDialog.setOnReminderConflictListener(new ReminderConflictDialog.OnReminderConflictListener() {
            @Override
            public void onRemoveConflict() {
                reminderConflictDialog.dismiss();
                requestRemoveReminder(context, conflictedReminder);
                boolean isSuccess = requestAddReminder(context, program);
                if (isSuccess) {
                    toastReminderSuccess(context);
                }
            }

            @Override
            public void onCancelReminder() {
                reminderConflictDialog.dismiss();
            }
        });
        reminderConflictDialog.show(fragmentManager, ReminderConflictDialog.CLASS_NAME);
    }

    public void toastReminderSuccess(Context context) {
        JasminToast.makeToast(context, R.string.reminder_set_toast);
    }

    public void toastReminderCancel(Context context) {
        JasminToast.makeToast(context, R.string.reminder_cancel_toast);
    }

    public void alertReminder(Context context, Reminder reminder) {
        removeReminder(reminder);

        Lifecycle.State mState = ProcessLifecycleOwner.get().getLifecycle().getCurrentState();
        if (Log.INCLUDE) {
            Log.d(TAG, "alertReminder, mState : " + mState);
        }

        if (mState == Lifecycle.State.RESUMED) {
            showReminderAlertDialog(context, reminder);
        }
    }

    public void addOnReminderListener(OnReminderListener listener) {
        if (!onReminderListeners.contains(listener)) {
            onReminderListeners.add(listener);
        }
    }

    public void removeOnReminderListener(OnReminderListener listener) {
        onReminderListeners.remove(listener);
    }

    public void notifyReminderWatch(Reminder reminder) {
        for (OnReminderListener listener : onReminderListeners) {
            listener.onReminderWatch(reminder);
        }
    }

    public void notifyReminderConflict(Program program, Reminder conflictedReminder) {
        for (OnReminderListener listener : onReminderListeners) {
            listener.onReminderConflict(program, conflictedReminder);
        }
    }

    public void notifyReminderListChange(Reminder reminder) {
        for (OnReminderListener listener : onReminderListeners) {
            listener.onReminderListChange(getReminderList(), reminder);
        }
        if (Log.INCLUDE) {
            printReminderList();
        }
    }

    private void writeReminderList(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "writeReminderList");
        }

        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        userPreferenceManagerImp.setReminderList(reminderList);
    }

    private ArrayList<Reminder> readReminderList(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "readReminderList");
        }
        UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(context);
        return userPreferenceManagerImp.getReminderList();
    }

    public interface OnReminderListener {
        default void onReminderWatch(Reminder reminder) {}

        default void onReminderConflict(Program program, Reminder conflictedReminder) {}

        void onReminderListChange(List<Reminder> list, Reminder reminder);
    }
}
