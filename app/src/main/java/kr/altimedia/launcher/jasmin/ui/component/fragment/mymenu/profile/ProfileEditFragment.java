/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileIcon;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfile;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

public class ProfileEditFragment extends Fragment {
    public static final String CLASS_NAME = ProfileEditFragment.class.getName();
    private static final String TAG = ProfileEditFragment.class.getSimpleName();

    private ImageView profileIconView;
    private TextView profileName;
    private TextView btnChangeProfile;
    private TextView btnChangeProfileIcon;
    private TextView btnDeleteProfile;
    private TextView btnDone;

    private UserProfile profile;
    private ProfileIcon profileIcon;
    private int profileSize = 0;
    private int lastFocus = 0;

    @NotNull
    public static ProfileEditFragment newInstance(UserProfile profile, int profileSize) {
        ProfileEditFragment fragment = new ProfileEditFragment();
        Bundle args = new Bundle();
        args.putParcelable(ProfileManageDialog.KEY_PROFILE, profile);
        args.putInt(ProfileManageDialog.KEY_PROFILE_SIZE, profileSize);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        lastFocus = 0;
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(Log.INCLUDE) {
            Log.d(TAG, "onCreateView");
        }
        return inflater.inflate(R.layout.fragment_mymenu_profile_edit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if(bundle != null){
            profile = (UserProfile) bundle.getParcelable(ProfileManageDialog.KEY_PROFILE);
            profileSize = bundle.getInt(ProfileManageDialog.KEY_PROFILE_SIZE);
        }
        if(Log.INCLUDE) {
            Log.d(TAG, "onViewCreated: profile="+profile+", profileSize="+profileSize);
        }

        initView(view);
    }

    private void initView(View view) {

        profileIconView = view.findViewById(R.id.profileIcon);
        profileName = view.findViewById(R.id.profileName);
        btnChangeProfile = view.findViewById(R.id.btnChangeProfile);
        btnChangeProfileIcon = view.findViewById(R.id.btnChangeProfileIcon);
        btnDeleteProfile = view.findViewById(R.id.btnDeleteProfile);
        btnDone = view.findViewById(R.id.btnDone);

        try {
            profile.getProfileInfo().setProfileIcon(profileIconView);
        }catch (Exception e){
        }

        boolean isCurrentProfile = false;
        try {
            if (profile != null) {
                profileName.setText(profile.getName());
                String currentProfileId = AccountManager.getInstance().getProfileId();
                if(profile.getId().equals(currentProfileId)){
                    isCurrentProfile = true;
                }
            }
        }catch(Exception e){
        }

        btnChangeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ProfileManageDialog) getParentFragment()).showProfileView(ProfileManageDialog.TYPE_CHANGE, profile, null);
            }
        });
        btnChangeProfile.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    lastFocus = 0;
                }
            }
        });

        btnChangeProfileIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileIconSelectDialog dialog = ProfileIconSelectDialog.newInstance(profile.getId());
                dialog.setOnProfileIconSelectedListener(new ProfileIconSelectDialog.OnProfileIconSelectedListener() {
                    @Override
                    public void onProfileIconSelected(ProfileIcon profileIcon) {
                        if(profileIcon != null){
                            ProfileEditFragment.this.profileIcon = profileIcon;
                            profileIconView.post(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Glide.with(profileIconView.getContext())
                                                .load(profileIcon.getPath()).placeholder(R.drawable.profile_default).
                                                error(R.drawable.profile_default).diskCacheStrategy(DiskCacheStrategy.DATA)
                                                .centerCrop()
                                                .into(profileIconView);
                                    }catch(Exception e){
                                    }
                                }
                            });
                        }
                    }
                });
                dialog.showDialog(((ProfileManageDialog) getParentFragment()).getTvOverlayManager());
            }
        });
        btnChangeProfileIcon.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    lastFocus = 1;
                }
            }
        });

        if(profileSize <= 1 || isCurrentProfile){
            btnDeleteProfile.setEnabled(false);
        } else{
            btnDeleteProfile.setEnabled(true);
            btnDeleteProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ProfileManageDialog) getParentFragment()).showProfileView(ProfileManageDialog.TYPE_DELETE, profile, null);
                }
            });
            btnDeleteProfile.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if(hasFocus) {
                        lastFocus = 2;
                    }
                }
            });
        }

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(profileIcon != null){
                    changeProfileIcon();
                }else {
                    hideView();
                }
            }
        });
        btnDone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    lastFocus = 3;
                }
            }
        });

        setFocus();
    }

    private void setFocus(){
        if(lastFocus == 0){
            btnChangeProfile.requestFocus();
        } else if(lastFocus == 1){
            btnChangeProfileIcon.requestFocus();
        }  else if(lastFocus == 2){
            btnDeleteProfile.requestFocus();
        } else if(lastFocus == 3){
            btnDone.requestFocus();
        }
    }

    private void changeProfileIcon() {
        if (profile == null || profileIcon == null) {
            return;
        }

        final ProfileInfo profileInfo = profile.getProfileInfo();
        String profileId = profileInfo.getProfileId();
        String profileName = profileInfo.getProfileName();
        String tmpIconId = profileInfo.getProfileIconId();
        String tmpIconPath = profileInfo.getProfileIconPath();
        if(profileIcon != null){
            tmpIconId = profileIcon.getId();
            tmpIconPath = profileIcon.getPath();
        }
        String profileIconId = tmpIconId;
        String profileIconPath = tmpIconPath;
        if (Log.INCLUDE) {
            Log.d(TAG, "changeProfileIcon: id=" + profileId + ", name=" + profileName + ", icon=" + profileIconId + ", icon path=" + profileIconPath);
        }

        UserDataManager userDataManager = new UserDataManager();
        userDataManager.putProfile(
                AccountManager.getInstance().getSaId(),
                profileId,
                profileName,
                profileIconId,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
//                            showProgress();
                        } else {
//                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        hideView();
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        Context context = ProfileEditFragment.this.getContext();
                        JasminToast.makeToast(context, R.string.saved);

                        notifyProfileIconChanged(profileId, profileName, profileIconId, profileIconPath);

                        ProfileManageDialog manageDialog = ((ProfileManageDialog) getParentFragment());
                        if(manageDialog.isLinkFromMyMenu()){
                            ((ProfileManageDialog) getParentFragment()).hideProfileView(profileName);
                        }else {
                            ((ProfileManageDialog) getParentFragment()).showProfileView(ProfileManageDialog.TYPE_LIST, profile, null);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);

                        hideView();
                    }
                });
    }

    private void notifyProfileIconChanged(String id, String name, String iconId, String iconPath){
        Intent intent = new Intent();
        intent.setAction(ProfileManageDialog.ACTION_MY_PROFILE_UPDATED);
        intent.putExtra(ProfileManageDialog.KEY_PROFILE_ID, id);
        intent.putExtra(ProfileManageDialog.KEY_PROFILE_NAME, name);
        intent.putExtra(ProfileManageDialog.KEY_PROFILE_ICON_ID, iconId);
        intent.putExtra(ProfileManageDialog.KEY_PROFILE_ICON_PATH, iconPath);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    private void hideView(){
        ProfileManageDialog manageDialog = ((ProfileManageDialog) getParentFragment());
        if (manageDialog.isLinkFromMyMenu()) {
            manageDialog.dismiss();
        } else {
            manageDialog.backToFragment();
        }
    }
}
