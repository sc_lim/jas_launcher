/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.view.View;

import androidx.leanback.widget.OnActionClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;

/**
 * Created by mc.kim on 05,02,2020
 */
public abstract class VideoPlaybackGlueHost {

    VideoPlaybackGlue mGlue;

    public abstract static class HostCallback {
        public void onHostStart() {
        }

        /**
         * Client triggered once the host(fragment) has stopped.
         */
        public void onHostStop() {
        }

        /**
         * Client triggered once the host(fragment) has paused.
         */
        public void onHostPause() {
        }

        /**
         * Client triggered once the host(fragment) has resumed.
         */
        public void onHostResume() {
        }

        /**
         * Client triggered once the host(fragment) has been destroyed.
         */
        public void onHostDestroy() {
        }

        public void onTimerStop(){

        }
    }

    /**
     * Optional Client that implemented by PlaybackGlueHost to respond to player event.
     */
    public static class PlayerCallback {
        /**
         * Size of the video changes, the Host should adjust SurfaceView's layout width and height.
         *
         * @param videoWidth
         * @param videoHeight
         */
        public void onVideoSizeChanged(int videoWidth, int videoHeight) {
        }

        /**
         * notify media starts/stops buffering/preparing. The Host could start or stop
         * progress bar.
         *
         * @param start True for buffering start, false otherwise.
         */
        public void onBufferingStateChanged(boolean start) {
        }

        /**
         * notify media has error. The Host could show error dialog.
         *
         * @param errorCode    Optional error code for specific implementation.
         * @param errorMessage Optional error message for specific implementation.
         */
        public void onError(int errorCode, CharSequence errorMessage) {
        }
    }

    /**
     * Enables or disables view fading.  If enabled, the view will be faded in when the
     * fragment starts and will fade out after a time period.
     *
     * @deprecated Use {@link #setControlsOverlayAutoHideEnabled(boolean)}
     */
    @Deprecated
    public void setFadingEnabled(boolean enable) {
    }

    /**
     * Enables or disables controls overlay auto hidden.  If enabled, the view will be faded out
     * after a time period.
     *
     * @param enabled True to enable auto hidden of controls overlay.
     */
    public void setControlsOverlayAutoHideEnabled(boolean enabled) {
        setFadingEnabled(enabled);
    }

    /**
     * Returns true if auto hides controls overlay.
     *
     * @return True if auto hiding controls overlay.
     */
    public boolean isControlsOverlayAutoHideEnabled() {
        return false;
    }

    /**
     * Fades out the playback overlay immediately.
     *
     * @deprecated Call {@link #hideControlsOverlay(boolean)}
     */
    @Deprecated
    public void fadeOut() {
    }

    /**
     * Returns true if controls overlay is visible, false otherwise.
     *
     * @return True if controls overlay is visible, false otherwise.
     * @see #showControlsOverlay(boolean)
     * @see #hideControlsOverlay(boolean)
     */
    public boolean isControlsOverlayVisible() {
        return true;
    }

    /**
     * Hide controls overlay.
     *
     * @param runAnimation True to run animation, false otherwise.
     */
    public void hideControlsOverlay(boolean runAnimation) {
    }

    /**
     * Show controls overlay.
     *
     * @param runAnimation True to run animation, false otherwise.
     */
    public void showControlsOverlay(boolean runAnimation) {
    }

    public void showStateOverlay(boolean runAnimation) {

    }

    public boolean isStateOverlayShowing() {
        return true;
    }

    public void hideStateOverlay(boolean runAnimation) {

    }

    public void showIconView(boolean runAnimation) {

    }

    public void hideIconView(boolean runAnimation) {

    }

    public void changeTrailerMode(boolean selectionMode, boolean runAnimation) {

    }

    public boolean canUsingTrailerMode(){
        return true;
    }

    public void updateRemainTime(long millisUntilFinished) {

    }

    public void onFinish() {

    }

    /**
     * Sets the {@link android.view.View.OnKeyListener} on the host. This would trigger
     * the listener when a {@link android.view.KeyEvent} is unhandled by the host.
     */
    public void setOnKeyInterceptListener(View.OnKeyListener onKeyListener) {
    }

    public void setOnActionClickedListener(OnActionClickedListener listener) {
    }

    public void setHostCallback(HostCallback callback) {
    }

    /**
     * Notifies host about a change so it can update the view.
     */
    public void notifyPlaybackRowChanged() {
    }

    /**
     * Sets {@link kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackRowPresenter} for rendering the playback controls.
     */
    public void setPlaybackRowPresenter(VideoPlaybackRowPresenter presenter) {
    }

    /**
     * Sets the {@link Row} that represents the information on control items that needs
     * to be rendered.
     */
    public void setPlaybackRow(Row row) {
    }

    final void attachToGlue(VideoPlaybackGlue glue) {
        if (mGlue != null) {
            mGlue.onDetachedFromHost();
        }
        mGlue = glue;
        if (mGlue != null) {
            mGlue.onAttachedToHost(this);
        }
    }

    /**
     * Implemented by PlaybackGlueHost for responding to player events. Such as showing a spinning
     * wheel progress bar when {@link PlayerCallback#onBufferingStateChanged(boolean)}.
     *
     * @return PlayerEventCallback that Host supports, null if not supported.
     */
    public PlayerCallback getPlayerCallback() {
        return null;
    }
}
