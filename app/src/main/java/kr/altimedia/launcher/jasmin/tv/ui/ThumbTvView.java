package kr.altimedia.launcher.jasmin.tv.ui;

import android.content.Context;
import android.media.tv.TvView;
import android.util.AttributeSet;

import com.altimedia.tvmodule.dao.Channel;

import java.util.function.Consumer;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.JasTvView;

public class ThumbTvView extends JasTvView {
    private ChannelPreviewIndicator thumb_preview_indicator;
    private BlockScreen thumbBlockScreen;

    public ThumbTvView(Context context) {
        this(context, null);
    }

    public ThumbTvView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ThumbTvView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ThumbTvView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        inflate(getContext(), R.layout.thumb_tv_view, this);
        TvView tvView = findViewById(R.id.tv_view_thumb);

        thumb_preview_indicator = findViewById(R.id.thumb_preview_indicator);

        thumbBlockScreen = findViewById(R.id.thumb_block_screen);
        init(JasTvView.TYPE_THUMBNAIL, tvView, thumbBlockScreen);
    }

    public void setTuneAction(Consumer<Channel> consumer) {
        thumb_preview_indicator.setTuneAction(consumer);
    }

    @Override
    public void setTuningResult(TuningResult tuningResult) {
        super.setTuningResult(tuningResult);
    }

    @Override
    public void updateInfo(TuningResult tuningResult) {
    }

    @Override
    protected void setPreviewIndicator(Channel channel) {
        thumb_preview_indicator.updateChannelPreview(channel);
    }

    @Override
    protected void updatePopularChannelData(String json) {
    }

    @Override
    protected void updateMuting() {
        tvView.setStreamVolume(0);
    }
}