/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series;

import android.view.View;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.BaseShadowHelper;

public class SeriesDetailShadowHelper extends BaseShadowHelper {
    private final String TAG = SeriesDetailShadowHelper.class.getSimpleName();
    private final int topShadowResId;
    private final float mTargetLimitAlpha;
    private final View leftShadowLayer;
    private final View bottomShadowLayer;
    private View mTopLayer = null;

    public SeriesDetailShadowHelper(int topShadowResId, float targetLimitAlpha, View leftShadowLayer, View bottomShadowLayer) {
        super();

        this.topShadowResId = topShadowResId;
        this.mTargetLimitAlpha = targetLimitAlpha;
        this.leftShadowLayer = leftShadowLayer;
        this.bottomShadowLayer = bottomShadowLayer;
    }

    public void resetAlpha() {
        if (Log.INCLUDE) {
            Log.d(TAG, "resetAlpha : ");
        }

        float f_offset = (float) 0;
        float total = (float) 100;

        if (total == 0) {
            return;
        }
        float alphaOffset = f_offset / total;
        if (parentView != null && mTopLayer == null) {
            mTopLayer = parentView.findViewById(topShadowResId);
        }
        float resultOffset = 1f - alphaOffset;
        if (mTopLayer != null && resultOffset >= mTargetLimitAlpha) {
            mTopLayer.setAlpha(resultOffset);
            leftShadowLayer.setAlpha(resultOffset);
        }
        bottomShadowLayer.setAlpha(resultOffset);
    }

    @Override
    public void onScrolledOffsetCallback(int offset, int remainScroll, int maxScroll) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onScrolledOffsetCallback : " + offset + ", remainScroll : " + remainScroll + ", maxScroll : " + maxScroll);
        }

        float f_offset = (float) offset;
        float total = (float) maxScroll;

        if (total <= 0) {
            return;
        }
        float alphaOffset = f_offset / total;
        if (parentView != null && mTopLayer == null) {
            mTopLayer = parentView.findViewById(topShadowResId);
        }
        float resultOffset = 1f - alphaOffset;
        if (mTopLayer != null && resultOffset >= mTargetLimitAlpha) {
            mTopLayer.setAlpha(resultOffset);
            leftShadowLayer.setAlpha(resultOffset);
        }
        bottomShadowLayer.setAlpha(resultOffset);
    }
}
