package kr.altimedia.launcher.jasmin.dm;

import android.content.Context;

import com.altimedia.player.AltiPlayer;
import com.altimedia.util.NetworkUtil;

import java.util.HashMap;

import kr.altimedia.launcher.jasmin.R;

public class ErrorMessageManager {
    private static final ErrorMessageManager instance = new ErrorMessageManager();
    private static final HashMap<String, String> errorCodeMap = new HashMap<>();

    private ErrorMessageManager() {
    }

    public static ErrorMessageManager getInstance() {
        return instance;
    }

    public void initialize(Context context) {
        if (!errorCodeMap.isEmpty()) {
            return;
        }
        errorCodeMap.put("LNC_0001", context.getString(R.string.error_message_lnc_0001));
        errorCodeMap.put("LNC_0002", context.getString(R.string.error_message_lnc_0002));
        errorCodeMap.put("LNC_0003", context.getString(R.string.error_message_lnc_0003));
        errorCodeMap.put("LNC_0004", context.getString(R.string.error_message_lnc_0004));
        errorCodeMap.put("LNC_0007", context.getString(R.string.error_message_lnc_0007));
        errorCodeMap.put("LNC_0008", context.getString(R.string.error_message_lnc_0008));
        errorCodeMap.put("LNC_0009", context.getString(R.string.error_message_lnc_0009));
        errorCodeMap.put("LNC_0010", context.getString(R.string.error_message_lnc_0010));
        errorCodeMap.put("LNC_0011", context.getString(R.string.error_message_lnc_0011));
        errorCodeMap.put("LNC_0012", context.getString(R.string.error_message_lnc_0012));
        errorCodeMap.put("LNC_0013", context.getString(R.string.error_message_lnc_0013));
        errorCodeMap.put("LNC_0014", context.getString(R.string.error_message_lnc_0014));
        errorCodeMap.put("LNC_0015", context.getString(R.string.error_message_lnc_0015));
        errorCodeMap.put("LNC_0016", context.getString(R.string.error_message_lnc_0016));
        errorCodeMap.put("LNC_0017", context.getString(R.string.error_message_lnc_0017));
        errorCodeMap.put("LNC_0018", context.getString(R.string.error_message_lnc_0018));
        errorCodeMap.put("LNC_0019", context.getString(R.string.error_message_lnc_0019));
        errorCodeMap.put("LNC_0020", context.getString(R.string.error_message_lnc_0020));
        errorCodeMap.put("LNC_0021", context.getString(R.string.error_message_lnc_0021));
        errorCodeMap.put("LNC_0022", context.getString(R.string.error_message_lnc_0022));
        errorCodeMap.put("LNC_0023", context.getString(R.string.error_message_lnc_0023));
        errorCodeMap.put("LNC_0024", context.getString(R.string.error_message_lnc_0024));
        errorCodeMap.put("LNC_0025", context.getString(R.string.error_message_lnc_0025));
        errorCodeMap.put("LNC_0026", context.getString(R.string.error_message_lnc_0026));
        errorCodeMap.put("LNC_0027", context.getString(R.string.error_message_lnc_0027));
        errorCodeMap.put("LNC_0028", context.getString(R.string.error_message_lnc_0028));

        errorCodeMap.put("LNC_0009_1", context.getString(R.string.error_message_lnc_0009_1));
    }

    public String getPopUpTitle(Context context, String errorCode) {
        switch (errorCode) {
            case "LNC_0009":
                return context.getString(R.string.notification);
            default:
                return context.getString(R.string.error);
        }
    }

    public String getErrorMessage(String errorCode) {
        return errorCodeMap.get(errorCode);
    }

    public String getPlayerErrorCode(Context context, int reason) {
        switch (reason) {
            case AltiPlayer.PLAYER_ERROR_TYPE_SOURCE:
                if (NetworkUtil.hasNetworkConnection(context)) {
                    return "LNC_0028";
                } else {
                    return "LNC_0018";
                }
            case AltiPlayer.PLAYER_ERROR_TYPE_RENDERER:
                return "LNC_0017";
            case AltiPlayer.PLAYER_ERROR_TYPE_UNEXPECTED:
                return "LNC_0019";
            case AltiPlayer.PLAYER_ERROR_TYPE_REMOTE:
                return "LNC_0016";
            case AltiPlayer.PLAYER_ERROR_TYPE_OUT_OF_MEMORY:
                return "LNC_0015";
            case AltiPlayer.PLAYER_ERROR_TYPE_USER_DEFINED_1:
                return "LNC_0022";
            default:
                return "LNC_0013";
        }
    }
}
