/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonInfo;
import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.dm.payment.type.ContentType;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.DetailFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodDetailsDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.managerProvider;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.ProductButtonPresenterSelector;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.RelatedVodHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.VodPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonManager2;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.VodProductType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter.SeriesDetailRowPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter.SeriesVodListRowPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.PaymentItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPurchaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPurchaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentButton;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentWrapper;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment.SubscriptionPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.util.task.UserTaskManager;
import kr.altimedia.launcher.jasmin.ui.util.task.VodTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewKeyListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.row.VodOverviewRow;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

import static kr.altimedia.launcher.jasmin.dm.contents.obj.Content.mPosterPurchaseRect;


public class SeriesVodFragment extends DetailFragment implements managerProvider {

    public static final String CLASS_NAME = SeriesVodFragment.class.getName();
    private static final String TAG = SeriesVodFragment.class.getSimpleName();

    private static final String KEY_SERIES_ASSET_ID = "SERIES_ASSET_ID";
    private static final String KEY_CATEGORY_ID = "CATEGORY_ID";
    private static final String KEY_CONTENT_ID = "CONTENT_ID";

    private final int DETAIL_ROW = 0;
    private final int RELATED_ROW = 1;

    private final float TARGET_ALPHA = 0.3f;

    private View focusedView;

    private Content content;
    private String seriesAssetId;
    private String categoryId;
    private String contentId;

    private ArrayObjectAdapter mAdapter;
    private ClassPresenterSelector mPresenterSelector;
    private ProductButtonManager2 productButtonManager;

    private EpisodeVodVerticalFragment mEpisodeVodVerticalFragment;

    private final int FOCUS_UPDATE_HANDLER_ID = 0;
    private final long FOCUS_UPDATE_HANDLER_DELAY_TIME = 300;
    private final Handler focusUpdateHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (Log.INCLUDE) {
                Log.d(TAG, "call handleMessage");
            }

            vodTaskManager.cancelTask(VodTaskManager.TaskType.DETAIL);
            vodTaskManager.cancelTask(VodTaskManager.TaskType.RELATE);

            mAdapter.clear();

            Bundle bundle = msg.getData();
            String contentId = bundle.getString(KEY_CONTENT_ID);
            String categoryId = bundle.getString(KEY_CATEGORY_ID);
            if (Log.INCLUDE) {
                Log.d(TAG, "keyHandler, contentId : " + contentId);
            }
            loadContent(contentId, categoryId);
        }
    };

    public SeriesVodFragment() {
        super();
    }

    public static SeriesVodFragment newInstance(String seriesAssetId, String categoryId, String contentId) {
        Bundle args = new Bundle();
        args.putString(KEY_SERIES_ASSET_ID, seriesAssetId);
        args.putString(KEY_CATEGORY_ID, categoryId);
        args.putString(KEY_CONTENT_ID, contentId);
        SeriesVodFragment fragment = new SeriesVodFragment();
        fragment.setResourceId(R.layout.fragment_series_vod_details);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        seriesAssetId = getArguments().getString(KEY_SERIES_ASSET_ID);
        categoryId = getArguments().getString(KEY_CATEGORY_ID);
        contentId = getArguments().getString(KEY_CONTENT_ID);
    }

    @Override
    protected Rect getPadding() {
        Resources resource = getContext().getResources();
        int detailLeft = resource.getDimensionPixelSize(R.dimen.series_vod_padding_left);
        int detailTop = resource.getDimensionPixelSize(R.dimen.series_vod_detail_padding_top);
        Rect padding = new Rect(detailLeft, detailTop, 0, detailTop);
        return padding;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mEpisodeVodVerticalFragment == null) {
            mEpisodeVodVerticalFragment = (EpisodeVodVerticalFragment) getChildFragmentManager().findFragmentById(R.id.details_left_rows_dock);
            if (mEpisodeVodVerticalFragment == null) {
                mEpisodeVodVerticalFragment = EpisodeVodVerticalFragment.newInstance(seriesAssetId, categoryId, contentId);
                mEpisodeVodVerticalFragment.setVodTaskManager(vodTaskManager);
                mEpisodeVodVerticalFragment.setListener(mChangeVodDetailListener);
            }
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (focusedView != null) {
            focusedView.requestFocus();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        productButtonManager = new ProductButtonManager2(view.getContext());

        initAdapter();
        setOnItemViewClickedListener(new SeriesVodFragment.ItemViewClickedListener());

        initShadowCallback(R.id.layout, view.findViewById(R.id.details_left_rows_dock),
                view.findViewById(R.id.bottomShadow));

        attachEpisodeFragment();
    }

    private void initAdapter() {
        mPresenterSelector = new ClassPresenterSelector();

        //detail
        SeriesDetailRowPresenter detailsPresenter = new SeriesDetailRowPresenter();
        detailsPresenter.setManagerProvider(this);

        detailsPresenter.setBaseOnItemViewKeyListener(new BaseOnItemViewKeyListener() {
            @Override
            public boolean onItemKey(int keyCode, Presenter.ViewHolder viewHolder, Object var2, Presenter.ViewHolder var3, Object var4) {
                int index = (int) viewHolder.view.getTag(R.id.KEY_INDEX);
                return requestFocusEpisodeList(keyCode, index);
            }
        });


        //related
        SeriesVodListRowPresenter seriesVodListRowPresenter = new SeriesVodListRowPresenter(R.layout.presenter_vod_horizontal_row);
        seriesVodListRowPresenter.setOnKeyVodListener(new SeriesVodListRowPresenter.OnKeyVodListener() {
            @Override
            public boolean onKey(int keyCode, int index) {
                return requestFocusEpisodeList(keyCode, index);
            }
        });
        seriesVodListRowPresenter.setHeaderPresenter(new RelatedVodHeaderPresenter());

        mPresenterSelector.addClassPresenter(VodOverviewRow.class, detailsPresenter);
        mPresenterSelector.addClassPresenter(ListRow.class, seriesVodListRowPresenter);
        mAdapter = new ArrayObjectAdapter(mPresenterSelector);
        setAdapter(mAdapter);
    }

    private void attachEpisodeFragment() {
        getChildFragmentManager().beginTransaction()
                .replace(R.id.details_left_rows_dock, mEpisodeVodVerticalFragment).commit();
    }


    public void replaceEpisodeDetail(String contentId, String categoryId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "replaceEpisodeDetail, contentId : " + contentId);
        }

        content = null;

        focusUpdateHandler.removeMessages(FOCUS_UPDATE_HANDLER_ID);
        Message message = Message.obtain(focusUpdateHandler, FOCUS_UPDATE_HANDLER_ID);
        Bundle bundle = new Bundle();
        bundle.putString(KEY_CONTENT_ID, contentId);
        bundle.putString(KEY_CATEGORY_ID, categoryId);
        message.setData(bundle);

        focusUpdateHandler.sendMessageDelayed(message, FOCUS_UPDATE_HANDLER_DELAY_TIME);
    }

    public void replaceSeasonDetail(SeasonInfo seasonInfo) {
        if (Log.INCLUDE) {
            Log.d(TAG, "replaceSeasonDetail, seasonInfo : " + seasonInfo);
        }

        vodTaskManager.cancelAllTask();
        mAdapter.clear();

        mEpisodeVodVerticalFragment = mEpisodeVodVerticalFragment.getSeasonInstance(seasonInfo.getSeriesAssetId());
        mEpisodeVodVerticalFragment.setVodTaskManager(vodTaskManager);
        mEpisodeVodVerticalFragment.setListener(mChangeVodDetailListener);

        attachEpisodeFragment();
    }

    private void loadContent(String contentId, String categoryId) {
        vodTaskManager.setOnCompleteLoadContentDetailListener(new VodTaskManager.OnCompleteLoadContentDetailListener<String, ContentDetailInfo>() {
            @Override
            public void needLoading(boolean isLoading) {
                if (isLoading) {
                    showProgress();
                } else {
                    hideProgress();
                }
            }

            @Override
            public void onSuccessLoadContentDetail(String id, ContentDetailInfo result) {
                ContentDetailInfo contentDetailInfo = result;
                if (Log.INCLUDE) {
                    Log.d(TAG, "onCompleteLoadContentDetail, contentDetailInfo : " + contentDetailInfo);
                }

                content = contentDetailInfo.getContent();

                updatePurchaseButtons();
                loadRelatedMovieListRow();
//                if (Log.INCLUDE) {
//                    Log.d(TAG, "onItemSelected : " + mBackgroundUri);
//                }
//                mBackgroundUri = content.getPosterSourceUrl(Content.mPosterDefaultRect.width(),
//                        Content.mPosterDefaultRect.height());
//                startBackgroundTimer();
                VodPaymentDialogFragment vodPaymentDialogFragment = (VodPaymentDialogFragment) findFragmentByTag(VodPaymentDialogFragment.CLASS_NAME);
                SubscriptionPaymentDialogFragment subscriptionPaymentDialogFragment = (SubscriptionPaymentDialogFragment) findFragmentByTag(SubscriptionPaymentDialogFragment.CLASS_NAME);
                if (vodPaymentDialogFragment != null || subscriptionPaymentDialogFragment != null) {
                    if (mVodRowFragment != null && mVodRowFragment.getVerticalGridView() != null) {
                        mVodRowFragment.getVerticalGridView().requestFocus();
                    }

                    if (vodPaymentDialogFragment != null) {
                        vodPaymentDialogFragment.showPurchaseSuccessFragment(mSenderId, vodPaymentDialogFragment.WORK_DETAIL_CALL);
                    } else if (subscriptionPaymentDialogFragment != null) {
                        subscriptionPaymentDialogFragment.showPurchaseSuccessFragment(true, mSenderId, subscriptionPaymentDialogFragment.WORK_DETAIL_CALL);
                    }
                }
            }

            @Override
            public void onFailLoadContentDetail(int key) {
                VodPaymentDialogFragment vodPaymentDialogFragment = (VodPaymentDialogFragment) findFragmentByTag(VodPaymentDialogFragment.CLASS_NAME);
                if (vodPaymentDialogFragment != null) {
                    vodPaymentDialogFragment.dismiss();
                }

                showLoadFailDialog(true, key);
            }

            @Override
            public void onError(String id, String errorCode, String message) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "loadContent, onError");
                }

                VodPaymentDialogFragment vodPaymentDialogFragment = (VodPaymentDialogFragment) findFragmentByTag(VodPaymentDialogFragment.CLASS_NAME);
                if (vodPaymentDialogFragment != null) {
                    vodPaymentDialogFragment.dismiss();
                }

                showLoadErrorDialog(true, errorCode, message);
            }
        });
        vodTaskManager.loadContentDetail(contentId, categoryId);
    }

    private void loadRelatedMovieListRow() {
        vodTaskManager.setOnCompleteLoadRelateListener(new VodTaskManager.OnCompleteLoadRelateListener<String, List<Content>>() {
            @Override
            public void needLoading(boolean isLoading) {
                if (isLoading) {
                    showProgress();
                } else {
                    hideProgress();
                }
            }

            @Override
            public void onSuccessLoadRelate(String id, List<Content> result) {
                updateRelatedList(result);
            }

            @Override
            public void onFailLoadRelate(int key) {
                showLoadFailDialog(false, key);
            }

            @Override
            public void onError(String id, String errorCode, String message) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "loadRelatedMovieListRow, onError");
                }

                showLoadErrorDialog(false, errorCode, message);
            }
        });

        vodTaskManager.loadRelatedList(content.getContentGroupId(), content.getCategoryId());
    }

    private ArrayObjectAdapter getRelatedRowAdapter(List<Content> movieDataList) {
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new VodPresenter());

        if (movieDataList != null) {
            listRowAdapter.addAll(0, movieDataList);
        }

        return listRowAdapter;
    }

    private boolean requestFocusEpisodeList(int keyCode, int index) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestFocusEpisodeList, index : " + index);
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (index == 0) {
                    if (mEpisodeVodVerticalFragment != null) {
                        if (vodTaskManager.isEmpty()) {
                            mEpisodeVodVerticalFragment.requestFocus();
                            mSeriesDetailShadowHelper.resetAlpha();
                        }

                        return true;
                    }
                }

                return false;
        }
        return false;
    }


    private void purchaseVod(Product product) {
        Bundle bundle = new Bundle();
        bundle.putString(VodPurchaseFragment.KEY_CONTENT_TITLE, content.getTitle());
        bundle.putString(VodPurchaseFragment.KEY_CONTENT_POSTER_URL,
                content.getPosterSourceUrl(mPosterPurchaseRect.width(), mPosterPurchaseRect.height()));
        bundle.putParcelable(VodPurchaseFragment.KEY_PRODUCT, product);

        TvOverlayManager tvOverlayManager = mChangeVodDetailListener.getTvOverlayManager();
        VodPurchaseDialogFragment vodPurchaseDialogFragment = VodPurchaseDialogFragment.newInstance(bundle);
        vodPurchaseDialogFragment.setOnPaymentClickListener(new PaymentItemBridgeAdapter.OnPaymentClickListener() {
            @Override
            public void onPaymentClick(Object item) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onPaymentClick, item : " + item);
                }

                if (item == null) {
                    vodPurchaseDialogFragment.dismiss();
                    return;
                }

                PaymentButton paymentButton = (PaymentButton) item;
                PaymentType paymentType = paymentButton.getType();
                PaymentWrapper paymentWrapper = vodPurchaseDialogFragment.getPaymentWrapper();
                PurchaseInfo purchaseInfo = new PurchaseInfo(ContentType.VOD, content.getTitle(),
                        product, paymentType, paymentWrapper);
                Parcelable option = (Parcelable) vodPurchaseDialogFragment.getPaymentOption(paymentType);

                VodPaymentDialogFragment vodPaymentDialogFragment = VodPaymentDialogFragment.newInstance(mSenderId, purchaseInfo, option);
                vodPaymentDialogFragment.setOnPaymentListener(new VodPaymentDialogFragment.OnPaymentCompleteListener() {
                    @Override
                    public void onComplete(boolean isSuccess) {
                        vodPurchaseDialogFragment.dismiss();
                        if (isSuccess) {
//                            updatePaymentResult();
                        }
                    }

                    @Override
                    public void onRequestWatch(boolean isRequest) {
                        vodPaymentDialogFragment.dismiss();
                        if (isRequest) {
                            mChangeVodDetailListener.watchMainVideo(content);
                        }
                    }

                    @Override
                    public void onCancelPayment() {
                        vodPaymentDialogFragment.dismiss();
                    }
                });

                tvOverlayManager.showDialogFragment(vodPaymentDialogFragment);
            }
        });
        tvOverlayManager.showDialogFragment(vodPurchaseDialogFragment);
    }

    private void purchaseSingle(Product product) {
        if (Log.INCLUDE) {
            Log.d(TAG, "purchaseSingle, product : " + product);
        }

        purchaseVod(product);
    }

    private void purchaseSeries() {
        ArrayList<ProductButtonItem> seriesList = productButtonManager.getSeriesButtonList();
        int size = seriesList.size();

        if (Log.INCLUDE) {
            Log.d(TAG, "purchaseSeries, size : " + size);
        }

        showSelectProductDialog(seriesList);
    }

    private void showSelectProductDialog(ArrayList<ProductButtonItem> productList) {
        SeriesProductSelectDialogFragment seriesProductSelectDialogFragment = SeriesProductSelectDialogFragment.newInstance(
                mEpisodeVodVerticalFragment.getMasterSeriesTitle(), content.getEpisodeCnt(), productList);
        seriesProductSelectDialogFragment.setOnClickProductListener(new SeriesProductSelectDialogFragment.OnClickProductListener() {
            @Override
            public void onClickProduct(ProductButtonItem productButtonItem) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onClickProduct, productButtonItem : " + productButtonItem);
                }

                if (productButtonItem != null) {
                    purchaseSingle(productButtonItem.getProduct());
                }
                seriesProductSelectDialogFragment.dismiss();
            }
        });
        mChangeVodDetailListener.getTvOverlayManager().showDialogFragment(seriesProductSelectDialogFragment);
    }

    protected Content getContent() {
        return content;
    }

    @Override
    protected void onPurchasePushMessage(String offerId) {
        super.onPurchasePushMessage(offerId);

        ((VodDetailsDialogFragment) getParentFragment()).loadUserDiscountInfo();

        if (content == null) {
            return;
        }

        ArrayList<Product> products = (ArrayList<Product>) content.getProductList();
        boolean isContains = isContainsOfferId(offerId, products);

        if (Log.INCLUDE) {
            Log.d(TAG, "onPurchasePushMessage, offerId : " + offerId + ", isContains : " + isContains);
        }

        if (isContains) {
            vodTaskManager.cancelAllTask();
            mAdapter.clear();

            mEpisodeVodVerticalFragment = mEpisodeVodVerticalFragment.getPushInstance();
            mEpisodeVodVerticalFragment.setVodTaskManager(vodTaskManager);
            mEpisodeVodVerticalFragment.setListener(mChangeVodDetailListener);

            attachEpisodeFragment();
        }
    }

    private void updateRelatedList(List<Content> relatedList) {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateRelatedList");
        }

        HeaderItem header = new HeaderItem(0, R.string.related_contents);
        ArrayObjectAdapter relatedObjectAdapter = getRelatedRowAdapter(relatedList);
        ListRow row = new ListRow(header, relatedObjectAdapter);

        if (mAdapter.size() >= 2) {
            mAdapter.replace(RELATED_ROW, row);
        } else {
            mAdapter.add(RELATED_ROW, row);
        }
    }

    private void updatePurchaseButtons() {
        if (Log.INCLUDE) {
            Log.d(TAG, "updatePurchaseButtons");
        }

        VodOverviewRow vodOverviewRow = new VodOverviewRow(ProductType.SERIES, content);

        SeriesDetailRowPresenter detailsPresenter = (SeriesDetailRowPresenter) mPresenterSelector.getPresenter(vodOverviewRow);
        detailsPresenter.setEpisodeInfo(mEpisodeVodVerticalFragment.getMasterSeriesTitle(), content.getEpisodeCnt());

        ArrayObjectAdapter objectAdapter = getButtonAction();
        vodOverviewRow.setActionsAdapter(objectAdapter);

        if (mAdapter.size() >= 1) {
            mAdapter.replace(DETAIL_ROW, vodOverviewRow);
        } else {
            mAdapter.add(DETAIL_ROW, vodOverviewRow);
        }

        mEpisodeVodVerticalFragment.updateThumbnailWatchable(content);

    }

    @Override
    protected void updateDetail(Content content) {
        super.updateDetail(content);

        if (content == null) {
            return;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "updateDetail");
        }

        loadContent(content.getContentGroupId(), content.getCategoryId());
    }

    @Override
    protected void setContentLike(Content mContent) {
        super.setContentLike(mContent);

        boolean isLike = mContent.isLike();
        if (Log.INCLUDE) {
            Log.d(TAG, "setContentLike, isLike : " + isLike);
        }

        if (content != null) {
            if (content.isLike() == mContent.isLike()) {
                return;
            }

            content.setLike(isLike);
        }

        CheckBox vodLikeView = getView().findViewById(R.id.my_list_button);
        vodLikeView.setChecked(isLike);

        mChangeVodDetailListener.onVodLikeChange(isLike);
    }

    private void onClickContentLike(Presenter.ViewHolder viewHolder) {
        ProductButtonPresenterSelector.MyListButtonPresenter.ViewHolder vh = (ProductButtonPresenterSelector.MyListButtonPresenter.ViewHolder) viewHolder;
        vodTaskManager.setOnCompleteContentFavoriteListener(
                new VodTaskManager.OnCompleteContentFavoriteListener() {
                    @Override
                    public void needLoading(boolean isLoading) {
                        if (isLoading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onSuccessAddFavorite(Object id, Object result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onCompleteAddFavorite");
                        }

                        content.setLike(true);
                        vh.toggleMyList(content);
                        int textId = R.string.my_list_add;
                        JasminToast.makeToast(getContext(), textId);

                        mChangeVodDetailListener.onVodLikeChange(true);
                    }

                    @Override
                    public void onSuccessRemoveFavorite(Object id, Object result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onCompleteRemoveFavorite");
                        }

                        content.setLike(false);
                        int textId = R.string.my_list_remove;
                        JasminToast.makeToast(getContext(), textId);
                        vh.toggleMyList(content);

                        mChangeVodDetailListener.onVodLikeChange(false);
                    }

                    @Override
                    public void onFailFavorite(int key) {
                        showLoadFailDialog(false, key);
                    }

                    @Override
                    public void onError(Object id, String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onClickContentLike, onError");
                        }

                        showLoadErrorDialog(false, errorCode, message);
                    }
                });

        boolean isLike = content.isLike();
        vodTaskManager.onClickLikeContent(isLike, content.getSeriesAssetId(), VodTaskManager.VodLikeType.Series);
    }

    private SeriesDetailShadowHelper mSeriesDetailShadowHelper = null;

    private void initShadowCallback(final int layer, final View leftShadow, final View bottomShadow) {
        mSeriesDetailShadowHelper = new SeriesDetailShadowHelper(layer, TARGET_ALPHA, leftShadow, bottomShadow);
        setOnScrollOffsetCallback(mSeriesDetailShadowHelper);
    }

    public void showProgress() {
        VodDetailsDialogFragment vodDetailsDialogFragment = (VodDetailsDialogFragment) getParentFragment();
        vodDetailsDialogFragment.showProgress();
    }

    public void hideProgress() {
        if (vodTaskManager.isEmpty() && UserTaskManager.getInstance().isEmpty()) {
            VodDetailsDialogFragment vodDetailsDialogFragment = (VodDetailsDialogFragment) getParentFragment();
            vodDetailsDialogFragment.hideProgress();
        }
    }

    private Handler mFocusChangeHandler = new Handler();
    private FocusChangeWorker mFocusChangeWorker = new FocusChangeWorker();

    @Override
    protected void setupFocusSearchListener() {
        mRootView.setOnChildFocusListener(new BrowseFrameLayout.OnChildFocusListener() {
            @Override
            public boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
                return true;
            }

            @Override
            public void onRequestChildFocus(View child, View focused) {
                focusedView = focused;

                if (mEpisodeVodVerticalFragment == null || mVodRowFragment == null) {
                    return;
                }
                if (mVodRowFragment.getVerticalGridView() == null) {
                    return;
                }
                boolean episodeHasFocus = mEpisodeVodVerticalFragment.hasFocus();
                if (episodeHasFocus) {
                    VerticalGridView verticalGridView = mVodRowFragment.getVerticalGridView();
                    mFocusChangeWorker.setIndex(0, verticalGridView);
                    mFocusChangeHandler.post(mFocusChangeWorker);
                }
                mEpisodeVodVerticalFragment.setEnable(episodeHasFocus);
            }
        });

        mRootView.setOnFocusSearchListener(new BrowseFrameLayout.OnFocusSearchListener() {
            @Override
            public View onFocusSearch(View focused, int direction) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onFocusSearch, focused : " + focused + ", direction : " + direction);
                }

                if (mEpisodeVodVerticalFragment != null && mEpisodeVodVerticalFragment.hasFocus()) {
                    if (direction == View.FOCUS_RIGHT && vodTaskManager.isEmpty()) {
                        SeriesDetailRowPresenter.VodDetailsRowPresenterViewHolder viewHolder = (SeriesDetailRowPresenter.VodDetailsRowPresenterViewHolder) mVodRowFragment.getRowViewHolder(DETAIL_ROW);
                        HorizontalGridView mHorizontalGridView = viewHolder != null ? viewHolder.getActionsRow() : null;
                        if (mHorizontalGridView != null) {
                            mHorizontalGridView.setSelectedPosition(0);
                        }
                        // move focus product buttons
                        return null;
                    }
                }

                //none focus change
                return focused;
            }
        });
    }

    private static class FocusChangeWorker implements Runnable {
        private int mIndex;
        private VerticalGridView mVerticalGridView;

        public void setIndex(int index, VerticalGridView verticalGridView) {
            this.mIndex = index;
            this.mVerticalGridView = verticalGridView;
        }

        @Override
        public void run() {
            if (mVerticalGridView == null) {
                return;
            }
            mVerticalGridView.smoothScrollToPosition(mIndex);
            reset();
        }

        private void reset() {
            mVerticalGridView = null;
            mIndex = 0;
        }
    }

    private ArrayObjectAdapter getButtonAction() {
        ArrayObjectAdapter actionAdapter = productButtonManager.getProductButtons(content, (ArrayList<Product>) content.getProductList());
        return actionAdapter;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        focusUpdateHandler.removeMessages(FOCUS_UPDATE_HANDLER_ID);

        mRootView.setOnChildFocusListener(null);
        mRootView.setOnFocusSearchListener(null);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        mAdapter = null;
        mEpisodeVodVerticalFragment = null;
        super.onDestroy();
    }

    @Override
    public TvOverlayManager getTvOverlayManager() {
        return mChangeVodDetailListener.getTvOverlayManager();
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder var3, Row row) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onItemClicked, item: " + item + ", row : " + row);
            }

            if (item == null) {
                return;
            }

            if (row instanceof VodOverviewRow) {
                if (item instanceof ProductButtonItem) {
                    ProductButtonItem productButtonItem = (ProductButtonItem) item;
                    VodProductType type = productButtonItem.getProductType();
                    Product product = productButtonItem.getProduct();

                    if (Log.INCLUDE) {
                        Log.d(TAG, "onItemClicked, productButtonItem : " + productButtonItem + ", product : " + product);
                    }

                    if (type == VodProductType.WATCH) {
                        mChangeVodDetailListener.watchMainVideo(content);
                    } else if (type == VodProductType.RENT || type == VodProductType.BUY || type == VodProductType.RENT_ALL_EPISODE || type == VodProductType.BUY_ALL_EPISODE) {
                        purchaseSingle(product);
                    } else if (type == VodProductType.ALL_EPISODE) {
                        purchaseSeries();
                    } else if (type == VodProductType.PACKAGE) {
                        mChangeVodDetailListener.showPackage(categoryId, productButtonManager.getPackageMainList());
                    } else if (type == VodProductType.SUBSCRIBE) {
                        purchaseSubscription(productButtonManager, content.getTermsAndConditions());
                    }
                } else if (item instanceof Content) {
                    onClickContentLike(itemViewHolder);
                }
            } else if (item instanceof Content) {
                mChangeVodDetailListener.replaceDetail((Content) item);
            }
        }
    }
}



