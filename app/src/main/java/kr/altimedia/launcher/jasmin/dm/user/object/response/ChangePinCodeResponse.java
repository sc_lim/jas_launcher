/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class ChangePinCodeResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private PinCodeResult result;

    public static final Creator<ChangePinCodeResponse> CREATOR = new Creator<ChangePinCodeResponse>() {
        @Override
        public ChangePinCodeResponse createFromParcel(Parcel in) {
            return new ChangePinCodeResponse(in);
        }

        @Override
        public ChangePinCodeResponse[] newArray(int size) {
            return new ChangePinCodeResponse[size];
        }
    };

    protected ChangePinCodeResponse(Parcel in) {
        super(in);
        result = in.readParcelable(PinCodeResult.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(result, flags);
    }

    public boolean isSuccess() {
        return result.isSuccess();
    }

    public PinCodeResult getResult() {
        return result;
    }

    public static class PinCodeResult implements Parcelable {
        public static final Creator<PinCodeResult> CREATOR = new Creator<PinCodeResult>() {
            @Override
            public PinCodeResult createFromParcel(Parcel in) {
                return new PinCodeResult(in);
            }

            @Override
            public PinCodeResult[] newArray(int size) {
                return new PinCodeResult[size];
            }
        };

        protected PinCodeResult(Parcel in) {
            isSuccess = in.readByte() != 0;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isSuccess ? 1 : 0));
        }

        @Expose
        @SerializedName("yn")
        @JsonAdapter(YNBooleanDeserializer.class)
        private boolean isSuccess;

        public boolean isSuccess() {
            return isSuccess;
        }
    }
}
