/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.PurchasedContent;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public class PurchasedListDialog extends SafeDismissDialogFragment implements View.OnUnhandledKeyEventListener{
    public static final String CLASS_NAME = PurchasedListDialog.class.getName();
    private final String TAG = PurchasedListDialog.class.getSimpleName();

    public static final String KEY_VIEW_TYPE = "KEY_VIEW_TYPE";
    public static final String KEY_PURCHASED_LIST = "KEY_PURCHASED_LIST";
    public static final String KEY_HIDDEN_LIST = "KEY_HIDDEN_LIST";

    public static final String DATE_FORMAT = "MMM dd EEEE";
    public static final String TIME_FORMAT = "kk:mm";

    public static final Integer TYPE_LIST = 0;
    public static final Integer TYPE_EDIT = 1;

    public static final int NUMBER_COLUMNS = 5;
    public static final int NUMBER_ROW = 2;

    private boolean created = false;

    private int currViewType;
    private int prevViewType;
    private int newViewType;
    private Fragment fragment;
    private String fragmentClassName;
    private View rootView;

    PurchasedListDialog() {
        created = true;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public static PurchasedListDialog newInstance() {
        PurchasedListDialog fragment = new PurchasedListDialog();

        Bundle args = new Bundle();
        args.putInt(KEY_VIEW_TYPE, TYPE_LIST);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {
                boolean available = backToFragment();
                Log.d(TAG, "onBackPressed: back fragment available=" + available);
                if (!available) {
                    super.onBackPressed();
                }
            }
        };
    }

    public boolean backToFragment() {
        FragmentManager manager = getChildFragmentManager();
        int stackCount = manager.getBackStackEntryCount();
        Log.d(TAG, "backToFragment: back fragment size=" + stackCount);
        if (stackCount > 0) {
            manager.popBackStack();
            return true;
        }
        return false;
    }

    public void removeAllBackFragments() {
        getChildFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_mymenu_common, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        Bundle args = getArguments();
        int type = TYPE_LIST;
        if (args != null) {
            type = args.getInt(KEY_VIEW_TYPE);
        }

        setPurchasedList(null, null);
        showFragment(type);
        created = false;
    }

    private void showFragment(int type) {
        prevViewType = currViewType;
        currViewType = type;
        Log.d(TAG, "showFragment: currViewType=" + currViewType + ", prevViewType=" + prevViewType);
        if (this.fragment == null) {
            return;
        }
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.mymenu_container, fragment, fragmentClassName);
        addToBackStack(ft);
        ft.commit();
    }

    private void addToBackStack(FragmentTransaction ft) {
        Log.d(TAG, "addToBackStack: currViewType=" + currViewType);
        if (!created) {
            ft.addToBackStack(null);
        }
    }

    public void showView(int type, ArrayList<PurchasedContent> pList, ArrayList<PurchasedContent> hList) {
        newViewType = type;
        Log.d(TAG, "showView: newViewType=" + newViewType);
        if (type == TYPE_LIST) {
            created = true;
            removeAllBackFragments();
            setPurchasedList(pList, hList);
            showFragment(type);
            created = false;
        } else if (type == TYPE_EDIT) {
            created = false;
            setPurchasedEdit(pList, hList);
            showFragment(type);
        }
    }

    private void setPurchasedList(ArrayList<PurchasedContent> pList, ArrayList<PurchasedContent> hList) {
        this.fragment = PurchasedListFragment.newInstance(pList, hList);
        this.fragmentClassName = PurchasedListFragment.CLASS_NAME;
    }

    private void setPurchasedEdit(ArrayList<PurchasedContent> pList, ArrayList<PurchasedContent> hList) {
        this.fragment = PurchasedEditFragment.newInstance(pList, hList);
        this.fragmentClassName = PurchasedEditFragment.CLASS_NAME;
    }
}
