/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.verticalGrid;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.OnChildLaidOutListener;
import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.presenter.VerticalGridPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.util.StateMachine;
import kr.altimedia.launcher.jasmin.ui.view.util.TransitionHelper;


public class VerticalBaseGridFragment extends VerticalBaseFragment {

    static final String TAG = "VuxVerticalBaseGridSupportFragment";
    static final boolean DEBUG = false;
    private ObjectAdapter mAdapter;
    private VerticalGridPresenter mGridPresenter;
    VerticalGridPresenter.ViewHolder mGridViewHolder;
    OnItemViewSelectedListener mOnItemViewSelectedListener;
    private OnItemViewClickedListener mOnItemViewClickedListener;
    private Object mSceneAfterEntranceTransition;
    private int mSelectedPosition = -1;
    final StateMachine.State STATE_SET_ENTRANCE_START_STATE = new StateMachine.State("SET_ENTRANCE_START_STATE") {
        public void run() {
            VerticalBaseGridFragment.this.setEntranceTransitionState(false);
        }
    };
    private final OnItemViewSelectedListener mViewSelectedListener = new OnItemViewSelectedListener() {


        @Override
        public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder rowViewHolder, Row row) {
            int position = VerticalBaseGridFragment.this.mGridViewHolder.getGridView().getSelectedPosition();
            VerticalBaseGridFragment.this.gridOnItemSelected(position);
            if (VerticalBaseGridFragment.this.mOnItemViewSelectedListener != null) {
                VerticalBaseGridFragment.this.mOnItemViewSelectedListener.onItemSelected(itemViewHolder, item, rowViewHolder, row);
            }
        }

    };
    private final OnChildLaidOutListener mChildLaidOutListener = new OnChildLaidOutListener() {
        public void onChildLaidOut(ViewGroup parent, View view, int position, long id) {
            if (position == 0) {
                VerticalBaseGridFragment.this.showOrHideTitle();
            }

        }
    };

    public VerticalBaseGridFragment() {
    }

    void createStateMachineStates() {
        super.createStateMachineStates();
        this.mStateMachine.addState(this.STATE_SET_ENTRANCE_START_STATE);
    }

    void createStateMachineTransitions() {
        super.createStateMachineTransitions();
        this.mStateMachine.addTransition(this.STATE_ENTRANCE_ON_PREPARED, this.STATE_SET_ENTRANCE_START_STATE, this.EVT_ON_CREATEVIEW);
    }

    public void setGridPresenter(VerticalGridPresenter gridPresenter) {
        if (gridPresenter == null) {
            throw new IllegalArgumentException("Grid presenter may not be null");
        } else {
            this.mGridPresenter = gridPresenter;
            this.mGridPresenter.setOnItemViewSelectedListener(this.mViewSelectedListener);
            if (this.mOnItemViewClickedListener != null) {
                this.mGridPresenter.setOnItemViewClickedListener(this.mOnItemViewClickedListener);
            }

        }
    }

    public VerticalGridPresenter getGridPresenter() {
        return this.mGridPresenter;
    }

    public void setAdapter(ObjectAdapter adapter) {
        this.mAdapter = adapter;
        this.updateAdapter();
    }

    public ObjectAdapter getAdapter() {
        return this.mAdapter;
    }

    public void setOnItemViewSelectedListener(OnItemViewSelectedListener listener) {
        this.mOnItemViewSelectedListener = listener;
    }

    void gridOnItemSelected(int position) {
        if (position != this.mSelectedPosition) {
            this.mSelectedPosition = position;
            this.showOrHideTitle();
        }

    }

    void showOrHideTitle() {
        if (this.mGridViewHolder.getGridView().findViewHolderForAdapterPosition(this.mSelectedPosition) != null) {
            if (!this.mGridViewHolder.getGridView().hasPreviousViewInSameRow(this.mSelectedPosition)) {
                this.showTitle(true);
            } else {
                this.showTitle(false);
            }

        }
    }

    public void setOnItemViewClickedListener(OnItemViewClickedListener listener) {
        this.mOnItemViewClickedListener = listener;
        if (this.mGridPresenter != null) {
            this.mGridPresenter.setOnItemViewClickedListener(this.mOnItemViewClickedListener);
        }

    }

    public OnItemViewClickedListener getOnItemViewClickedListener() {
        return this.mOnItemViewClickedListener;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_vertical_grid, container, false);
        ViewGroup gridFrame = (ViewGroup) root.findViewById(R.id.grid_frame);
        this.installTitleView(inflater, gridFrame, savedInstanceState);
        this.getProgressBarManager().setRootView(root);
        ViewGroup gridDock = (ViewGroup) root.findViewById(R.id.browse_grid_dock);
        this.mGridViewHolder = this.mGridPresenter.onCreateViewHolder(gridDock);
        gridDock.addView(this.mGridViewHolder.view);
        this.mGridViewHolder.getGridView().setOnChildLaidOutListener(this.mChildLaidOutListener);
        this.mSceneAfterEntranceTransition = TransitionHelper.createScene(gridDock, new Runnable() {
            public void run() {
                VerticalBaseGridFragment.this.setEntranceTransitionState(true);
            }
        });
        this.updateAdapter();
        return root;
    }

    private void setupFocusSearchListener() {
        BrowseFrameLayout browseFrameLayout = (BrowseFrameLayout) this.getView().findViewById(R.id.grid_frame);
        browseFrameLayout.setOnFocusSearchListener(this.getTitleHelper().getOnFocusSearchListener());
    }

    public void onStart() {
        super.onStart();
        this.setupFocusSearchListener();
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.mGridViewHolder = null;
    }

    public void setSelectedPosition(int position) {
        this.mSelectedPosition = position;
        if (this.mGridViewHolder != null && this.mGridViewHolder.getGridView().getAdapter() != null) {
            this.mGridViewHolder.getGridView().setSelectedPositionSmooth(position);
        }

    }

    private void updateAdapter() {
        if (this.mGridViewHolder != null) {
            this.mGridPresenter.onBindViewHolder(this.mGridViewHolder, this.mAdapter);
            if (this.mSelectedPosition != -1) {
                this.mGridViewHolder.getGridView().setSelectedPosition(this.mSelectedPosition);
            }
        }

    }

    protected Object createEntranceTransition() {
        return TransitionHelper.loadTransition(this.getContext(), R.transition.lb_vertical_grid_entrance_transition);
    }

    protected void runEntranceTransition(Object entranceTransition) {
        TransitionHelper.runTransition(this.mSceneAfterEntranceTransition, entranceTransition);
    }

    void setEntranceTransitionState(boolean afterTransition) {
        this.mGridPresenter.setEntranceTransitionState(this.mGridViewHolder, afterTransition);
    }
}
