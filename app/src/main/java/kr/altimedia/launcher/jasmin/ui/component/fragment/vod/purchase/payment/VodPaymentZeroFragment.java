package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.cwmp.service.def.CWMPEtcCode;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.payment.VodPaymentDataManager;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Payment;
import kr.altimedia.launcher.jasmin.dm.payment.obj.RequestVodPaymentResultInfo;
import kr.altimedia.launcher.jasmin.dm.payment.obj.VodPaymentResult;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentWrapper;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class VodPaymentZeroFragment extends VodPaymentBaseFragment {
    public static final String CLASS_NAME = VodPaymentZeroFragment.class.getName();
    private final String TAG = VodPaymentZeroFragment.class.getSimpleName();

    private static final String KEY_PURCHASED_INFO = "PURCHASED_INFO";

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private final UserDataManager mUserDataManager = new UserDataManager();
    private MbsDataTask checkPinTask;

    private PasswordView passwordView;
    private TextView pinTextView;
    private TextView cancel;

    private PurchaseInfo purchaseInfo;

    public VodPaymentZeroFragment() {
    }

    public static VodPaymentZeroFragment newInstance(PurchaseInfo purchaseInfo) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_PURCHASED_INFO, purchaseInfo);

        VodPaymentZeroFragment fragment = new VodPaymentZeroFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_vod_payment_zero;
    }

    @Override
    protected void initView(View view) {
        initText(view);
        initPasswordView(view);
        initButton(view);

        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void initText(View view) {
        purchaseInfo = getArguments().getParcelable(KEY_PURCHASED_INFO);
        String paymentTitle = purchaseInfo.getTitle();

        TextView title = view.findViewById(R.id.title);
        TextView total = view.findViewById(R.id.total);

        title.setText(paymentTitle);
        String totalValue = StringUtil.getFormattedPrice(purchaseInfo.getPaymentWrapper().getAccountPayment());
        total.setText(totalValue);
        pinTextView = view.findViewById(R.id.pin_text);
    }

    private void initPasswordView(View view) {
        passwordView = view.findViewById(R.id.password);
        passwordView.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (isFull) {
                    checkPinCode(input);
                }
            }
        });
    }

    private TvOverlayManager mTvOverlayManager = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttached : context");
        }
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
    }

    private void checkPinCode(String password) {
        checkPinTask = mUserDataManager.checkAccountPinCode(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), password,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onSuccess, result : " + result);
                        }

                        if (result) {
                            requestPayment();
                        } else {
                            setPinError();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPaymentZeroFragment.this.getContext();
                    }
                });
    }

    private void setPinError() {
        String text = getResources().getString(R.string.incorrect_pin_desc);
        pinTextView.setText(text);
        passwordView.clear();

        CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_207);
    }

    private void requestPayment() {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestPayment");
        }

        PurchaseInfo purchaseInfo = getArguments().getParcelable(KEY_PURCHASED_INFO);
        Product product = purchaseInfo.getProduct();
        PaymentWrapper paymentWrapper = purchaseInfo.getPaymentWrapper();

        ArrayList<Payment> payments = getPaymentList(purchaseInfo);

        VodPaymentDataManager vodPaymentDataManager = new VodPaymentDataManager();
        paymentRequestTask = vodPaymentDataManager.requestVodPayment(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), purchaseInfo.getContentType(),
                product.getOfferID(), payments, paymentWrapper.getPrice(),
                new MbsDataProvider<String, RequestVodPaymentResultInfo>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
//                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onSuccess, onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, RequestVodPaymentResultInfo result) {
                        purchaseId = result.getPurchaseId();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onSuccess, purchasedId : " + purchaseId);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPaymentZeroFragment.this.getContext();
                    }
                });
    }

    @Override
    protected void getPaymentResult(String resultId) {
        String offerID = purchaseInfo.getProduct().getOfferID();
        if (Log.INCLUDE) {
            Log.d(TAG, "getPaymentResult, offerID : " + offerID);
        }

        if (purchaseId == null || !resultId.equalsIgnoreCase(offerID)) {
            return;
        }

        if (paymentResultTask != null && paymentResultTask.isRunning()) {
            paymentResultTask.cancel(true);
        }

        paymentResultTask = vodPaymentDataManager.getVodPaymentResult(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), purchaseId,
                new MbsDataProvider<String, VodPaymentResult>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPaymentResult, onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, VodPaymentResult result) {
                        boolean isSuccess = result.isSuccess();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPaymentResult, onSuccess, isSuccess : " + isSuccess);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentComplete(false, isSuccess);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPaymentResult, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPaymentZeroFragment.this.getContext();
                    }
                });
    }

    private void initButton(View view) {
        cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getOnPaymentListener() != null) {
                    getOnPaymentListener().onDismissFragment();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (checkPinTask != null) {
            checkPinTask.cancel(true);
        }

        passwordView.setOnPasswordComplete(null);
        cancel.setOnClickListener(null);
        mProgressBarManager.hide();
    }
}