package kr.altimedia.launcher.jasmin.tv.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;


/**
 * Created by mc.kim on 2018-01-05.
 */

public class PipManagerDialogFragment extends SafeDismissDialogFragment implements DialogInterface.OnKeyListener {
    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_PIP;
    }

    public enum ViewLocate {
        LEFT_TOP(94, 64, -1, -1, 2),
        RIGHT_TOP(-1, 64, 94, -1, 3),
        LEFT_BOTTOM(94, -1, -1, 135, 1),
        RIGHT_BOTTOM(-1, -1, 94, 135, 0);
        final int left;
        final int top;
        final int right;
        final int bottom;
        final int index;

        ViewLocate(int left, int top, int right, int bottom, int index) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
            this.index = index;
        }

        public static ViewLocate findByIndex(int index) {
            ViewLocate[] locateArray = values();
            for (ViewLocate locate : locateArray) {
                if (locate.index == index) {
                    return locate;
                }
            }
            return RIGHT_BOTTOM;
        }
    }

    public enum ViewSize {
        Small(592, 333, 0),
        Medium(704, 396, 1),
        Large(816, 459, 2);
        final int width;
        final int height;
        final int index;

        ViewSize(int width, int height, int index) {
            this.width = width;
            this.height = height;
            this.index = index;
        }

        public static ViewSize findByIndex(int index) {
            ViewSize[] sizeArray = values();
            for (ViewSize size : sizeArray) {
                if (size.index == index) {
                    return size;
                }
            }
            return Small;
        }
    }

    private static final String SELECTED_CHANNEL_ID = "selectedSrcId";
    public static final String CLASS_NAME = PipManagerDialogFragment.class.getName();
    private static final String TAG = PipManagerDialogFragment.class.getSimpleName();
    private Channel mSelectedChannel;
    private static final String START_ORDER = "startOrder";
    private ChannelDataManager mChannelDataManager;

    private int mLastLocateIndex = ViewLocate.RIGHT_BOTTOM.index;
    private int mLastSizeIndex = ViewSize.Medium.index;


    private PipManagerDialogFragment() {
        super();
    }

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static PipManagerDialogFragment newInstance(String srcId) {
        PipManagerDialogFragment f = new PipManagerDialogFragment();
        Bundle args = new Bundle();
        args.putString(SELECTED_CHANNEL_ID, srcId);
        f.setArguments(args);
        return f;
    }

    public static PipManagerDialogFragment newInstance(String srcId, ViewLocate startLocate) {
        PipManagerDialogFragment f = new PipManagerDialogFragment();
        Bundle args = new Bundle();
        args.putString(SELECTED_CHANNEL_ID, srcId);
        args.putSerializable(START_ORDER, startLocate);
        f.setArguments(args);
        return f;
    }

    private boolean showing = false;

    public boolean isShowing() {

        return showing;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        showing = true;
        super.show(manager, tag);
    }


    @Override
    public void dismiss() {
        showing = false;
        super.dismiss();

    }


    private OnPIPInteractor mOnPIPInteractor = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPIPInteractor) {
            mOnPIPInteractor = (OnPIPInteractor) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreate");
        }
        mChannelDataManager = TvSingletons.getSingletons(getContext()).getChannelDataManager();
        if (getArguments() == null) {
            return;
        }
        Bundle channelBundle = getArguments();
        String srcId = channelBundle.getString(SELECTED_CHANNEL_ID, "");
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreate srcId : " + srcId);
        }
        if (StringUtils.nullToEmpty(srcId).isEmpty()) {
            return;
        }
        mSelectedChannel = mChannelDataManager.getChannelByServiceId(srcId);
        if (mSelectedChannel == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "channel info null");
            }
            return;
        }
        if (channelBundle.getSerializable(START_ORDER) != null) {
            ViewLocate selectedLocate = (ViewLocate) channelBundle.getSerializable(START_ORDER);
            mLastLocateIndex = selectedLocate.index;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG);
        dialog.setOnKeyListener(this);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Channel currentChannel = mPipPlayer.getCurrentChannel();
        tuneChannel(currentChannel == null ? mSelectedChannel : currentChannel);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPipPlayer.reset();
    }


    private void slideTo(int keyCode, boolean smoothSlide) {
        int currentIndex = mLastLocateIndex;
        int size = ViewLocate.values().length;
        if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
            currentIndex = mLastLocateIndex + 1;
            if (currentIndex >= size) {
                currentIndex = 0;
            }
            startSlide(currentIndex, smoothSlide);
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
            currentIndex = mLastLocateIndex - 1;
            if (currentIndex < 0) {
                currentIndex = size - 1;
            }
            startSlide(currentIndex, smoothSlide);
        }
        mLastLocateIndex = currentIndex;
    }


    private void startSlide(int index, boolean smoothSlide) {
        ViewLocate requestedLocate = ViewLocate.findByIndex(index);
        ViewSize requestedSize = ViewSize.findByIndex(mLastSizeIndex);

        int width = requestedSize.width;
        int height = requestedSize.height;

        int left = requestedLocate.left;
        int top = requestedLocate.top;
        int right = requestedLocate.right;
        int bottom = requestedLocate.bottom;

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height); // or wrap_content
        switch (requestedLocate) {
            case LEFT_BOTTOM:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.leftMargin = left;
                layoutParams.bottomMargin = bottom;

                break;

            case RIGHT_BOTTOM:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.bottomMargin = bottom;
                layoutParams.rightMargin = right;
                break;

            case LEFT_TOP:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.topMargin = top;
                layoutParams.leftMargin = left;
                break;


            case RIGHT_TOP:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.topMargin = top;
                layoutParams.rightMargin = right;

                break;
        }
        mPipPlayer.setLayoutParams(layoutParams);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreateView");
        }
        return inflater.inflate(R.layout.dialog_pip_container, container, false);
    }

    private PipTvView mPipPlayer = null;
    private View mPipOptionView = null;

    private void setVideoSize(int index) {
        mLastSizeIndex = index;
        ViewLocate requestedLocate = ViewLocate.findByIndex(mLastLocateIndex);
        ViewSize requestedSize = ViewSize.findByIndex(index);
        int width = requestedSize.width;
        int height = requestedSize.height;

        int left = requestedLocate.left;
        int top = requestedLocate.top;
        int right = requestedLocate.right;
        int bottom = requestedLocate.bottom;

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height); // or wrap_content
        switch (requestedLocate) {
            case LEFT_BOTTOM:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.leftMargin = left;
                layoutParams.bottomMargin = bottom;

                break;

            case RIGHT_BOTTOM:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.bottomMargin = bottom;
                layoutParams.rightMargin = right;
                break;

            case LEFT_TOP:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.topMargin = top;
                layoutParams.leftMargin = left;
                break;


            case RIGHT_TOP:
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.topMargin = top;
                layoutParams.rightMargin = right;

                break;
        }
        mPipPlayer.setLayoutParams(layoutParams);
    }

    private void changeVideoSize(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_PROG_YELLOW:
            case KeyEvent.KEYCODE_MEDIA_STOP:
                int size = ViewLocate.values().length;
                int currentIndex = mLastSizeIndex + 1;
                if (currentIndex >= size) {
                    currentIndex = 0;
                }
                setVideoSize(currentIndex);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated");
        }
        mPipPlayer = view.findViewById(R.id.pip_tv_view);
        setVideoSize(mLastSizeIndex);
        mPipOptionView = view.findViewById(R.id.pip_option_view);

    }


    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    public void tuneChannel(Channel channel) {
        mPipPlayer.tuneChannel(channel);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
        if (keyEvent.getAction() == KeyEvent.ACTION_UP) {
            return true;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                slideTo(keyCode, false);
                return true;

            case KeyEvent.KEYCODE_DPAD_UP:
                mPipPlayer.tuneChannel(mPipPlayer.getNextChannel(true));
                return true;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                mPipPlayer.tuneChannel(mPipPlayer.getNextChannel(false));
                return true;

            case KeyEvent.KEYCODE_MEDIA_STOP:
            case KeyEvent.KEYCODE_PROG_YELLOW:
                changeVideoSize(keyCode);
                return true;

            case KeyEvent.KEYCODE_BACK:
                dismiss();
                return true;
            case KeyEvent.KEYCODE_PROG_GREEN:
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                Channel pipChannel = mPipPlayer.getCurrentChannel();
                Channel mainChannel = mOnPIPInteractor.swapChannel(pipChannel);
                mPipPlayer.tuneChannel(mainChannel);
                return true;
        }
        return mOnPIPInteractor.onPipKeyEvent(keyCode, keyEvent);
    }


    public interface OnPIPInteractor {
        boolean onPipKeyEvent(int keyCode, KeyEvent keyEvent);

        Channel swapChannel(Channel mPipChannel);
    }
}
