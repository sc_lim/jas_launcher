/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscribedContent;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class SubscriptionResultFragment extends Fragment {
    public static final String CLASS_NAME = SubscriptionDetailDialog.class.getName();
    private final String TAG = SubscriptionDetailDialog.class.getSimpleName();

    private SubscribedContent subscriptionItem;

    private TextView title;
    private TextView remainedDate;
    private TextView btnOk;

    public static SubscriptionResultFragment newInstance(SubscribedContent item) {
        Bundle args = new Bundle();
        args.putParcelable(SubscriptionDetailDialog.KEY_SUBSCRIPTION, item);
        SubscriptionResultFragment fragment = new SubscriptionResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreateView");
        }
        return inflater.inflate(R.layout.fragment_mymenu_subscription_result, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            subscriptionItem = args.getParcelable(SubscriptionDetailDialog.KEY_SUBSCRIPTION);
        }

        initContent(view);
        initButton(view);

        showView();
    }

    private void initContent(View view) {
        title = view.findViewById(R.id.title);
        remainedDate = view.findViewById(R.id.date);
    }

    private void initButton(View view) {
        btnOk = view.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SubscriptionDetailDialog) getParentFragment()).hideDetailView();
            }
        });
    }

    private void showView() {
        if (subscriptionItem != null) {
            title.setText(subscriptionItem.getProductName());
            String dateStr = "";
            try {
                dateStr = TimeUtil.getModifiedDate(subscriptionItem.getRenewalDate().getTime());
            }catch (Exception e){
            }
            remainedDate.setText(dateStr);
        }

        btnOk.requestFocus();
    }
}
