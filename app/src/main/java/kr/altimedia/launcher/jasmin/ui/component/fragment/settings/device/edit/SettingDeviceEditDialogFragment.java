/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.edit;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.DeviceEditType;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

public class SettingDeviceEditDialogFragment extends SettingBaseDialogFragment implements SettingDeviceEditBaseFragment.OnChangeFragmentListener {
    public static final String CLASS_NAME = SettingDeviceEditDialogFragment.class.getName();
    private final String TAG = SettingDeviceEditDialogFragment.class.getSimpleName();

    public static final String KEY_DEVICE = "DEVICE";

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private OnChangeDeviceListener onChangeDeviceListener;

    private SettingDeviceEditDialogFragment() {
    }

    public static SettingDeviceEditDialogFragment newInstance(UserDevice item) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_DEVICE, item);
        SettingDeviceEditDialogFragment fragment = new SettingDeviceEditDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnChangeDeviceListener(OnChangeDeviceListener onChangeDeviceListener) {
        this.onChangeDeviceListener = onChangeDeviceListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {
                boolean hasBackFragment = backToLastFragment();
                if (!hasBackFragment) {
                    super.onBackPressed();
                }
            }
        };
    }

    public boolean backToLastFragment() {
        FragmentManager manager = getChildFragmentManager();
        int stackCount = manager.getBackStackEntryCount();

        if (stackCount > 1) {
            manager.popBackStack();
            return true;
        }

        return false;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_device_edit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initProgressbar(view);
        showDeviceEditFragment();
    }

    protected void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    @Override
    public void showProgressbar(boolean isLoading) {
        if (isLoading) {
            mProgressBarManager.show();
        } else {
            mProgressBarManager.hide();
        }
    }

    private void showDeviceEditFragment() {
        SettingDeviceEditFragment settingDeviceEditFragment = SettingDeviceEditFragment.newInstance(getArguments());
        settingDeviceEditFragment.setOnChangeFragmentListener(this);
        showFragment(settingDeviceEditFragment, SettingDeviceEditFragment.CLASS_NAME);
    }

    private void showDeviceEditNameFragment() {
        SettingDeviceEditNameFragment settingDeviceEditNameFragment = SettingDeviceEditNameFragment.newInstance(getArguments());
        settingDeviceEditNameFragment.setOnChangeFragmentListener(this);
        showFragment(settingDeviceEditNameFragment, SettingDeviceEditNameFragment.CLASS_NAME);
    }

    private void showDeviceEditDeleteFragment() {
        SettingDeviceEditDeleteFragment settingDeviceEditDeleteFragment = SettingDeviceEditDeleteFragment.newInstance(getArguments());
        settingDeviceEditDeleteFragment.setOnChangeFragmentListener(this);
        showFragment(settingDeviceEditDeleteFragment, SettingDeviceEditDeleteFragment.CLASS_NAME);
    }

    private void showFragment(SettingDeviceEditBaseFragment fragment, String tag) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.addToBackStack(tag);
        ft.replace(R.id.frame_layout, fragment).commit();
    }

    @Override
    public void onNextFragment(DeviceEditType type) {
        switch (type) {
            case CHANGE:
                showDeviceEditNameFragment();
                break;
            case DELETE:
                showDeviceEditDeleteFragment();
                break;
        }
    }

    private void clearBackStack() {
        FragmentManager fragmentManager = getChildFragmentManager();
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
            fragmentManager.popBackStack();
        }
    }

    @Override
    public void onBackFragment() {
        backToLastFragment();
    }

    @Override
    public void onEditName(UserDevice deviceItem, String editName) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onEditName, name : " + editName);
        }

        JasminToast.makeToast(getContext(), R.string.saved);

        Bundle bundle = getArguments();
        UserDevice userDevice = bundle.getParcelable(KEY_DEVICE);
        userDevice.setDeviceName(editName);

        clearBackStack();
        showDeviceEditFragment();

        onChangeDeviceListener.onChangeDeviceName(userDevice);
    }

    @Override
    public void onEditDelete(UserDevice deviceItem) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onEditDelete, deviceItem : " + deviceItem.getDeviceName());
        }

        JasminToast.makeToast(getContext(), R.string.deleted);
        onChangeDeviceListener.onDeleteDevice(deviceItem);

        dismiss();
    }

    @Override
    public void onDismiss() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onDismiss");
        }

        dismiss();
    }

    @Override
    public SettingTaskManager getSettingTaskManager() {
        return new SettingTaskManager(getFragmentManager(), mTvOverlayManager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onChangeDeviceListener = null;
    }

    public interface OnChangeDeviceListener {
        void onChangeDeviceName(UserDevice userDevice);

        void onDeleteDevice(UserDevice userDevice);
    }
}
