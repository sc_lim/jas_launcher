/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.def;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;

public enum CouponType {
    DISCOUNT("20001", R.string.discount), POINT("20002", R.string.point);

    private final String type;
    private final int mName;

    CouponType(String type, int mName) {
        this.type = type;
        this.mName = mName;
    }

    public static CouponType findByName(String name) {
        if (Log.INCLUDE) {
            Log.d("CouponType", "findByName: name=" + name);
        }
        CouponType[] types = values();
        for (CouponType type : types) {
            if (type.type.equalsIgnoreCase(name)) {
                return type;
            }
        }

        return POINT;
    }

    public int getName() {
        return mName;
    }
}
