/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.category.MenuManager;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.ui.view.row.MenuRow;

public class MenuPresenter extends Presenter {
    private static final String TAG = "MenuPresenter";


    public MenuPresenter() {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreateViewHolder");
        }
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_main_menu, null, false);
        return new ViewHolder(itemView);
    }

    private boolean mItemSelected = false;

    public void setSelected(boolean selected) {
        mItemSelected = selected;
    }

    public boolean isItemSelected() {
        return mItemSelected;
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        MenuRow menuRow = (MenuRow) item;

        Category menu = menuRow.getItem();
        setSelected(viewHolder.view.isSelected());
        View menuBg = viewHolder.view.findViewById(R.id.menuBg);
        ImageView icon = viewHolder.view.findViewById(R.id.iconMenu);
        TextView title = viewHolder.view.findViewById(R.id.titleMenu);
        title.setVisibility(menuRow.getRecyclerView().isEnabled() ? View.VISIBLE : View.INVISIBLE);
        menuBg.setBackgroundResource(menuRow.getRecyclerView().isEnabled() ? R.drawable.selector_home_focus : R.drawable.bg_transparent);
        if (Log.INCLUDE) {
            Log.d(TAG,"viewHolder.view.isSelected() : "+menuRow.isSelected());
        }
        icon.setAlpha(menuRow.getRecyclerView().isEnabled() ? 1f : menuRow.isSelected() ? 1f : 0.4f);
        title.setText(menu.getCategoryName());
        MenuManager.getInstance().loadMenuIcon(viewHolder.view.getContext(), icon, menu);
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuRow.getOnMenuSelectedListener().onItemSelected(v, menu);
            }
        });


        viewHolder.view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(menuRow.getOnMenuSelectedListener().dispatchKey(event)){
                    return true;
                }

                if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        menuRow.getOnMenuSelectedListener().onCanceled(v, menu);
                        return true;
                    }
                }

                return false;
            }
        });

    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onUnbindViewHolder");
        }

        viewHolder.view.setOnClickListener(null);
        viewHolder.view.setOnKeyListener(null);
    }


}