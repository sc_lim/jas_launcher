/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.SinglePresenterSelector;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter.WidgetPreviewPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

/**
 * Created by mc.kim on 18,12,2019
 */
public class WidgetListFragment extends Fragment {
    private static final String TAG = WidgetListFragment.class
            .getSimpleName();
    public static final String CLASS_NAME = WidgetListFragment.class.getName();

    public WidgetListFragment() {
        super();
    }

    public static WidgetListFragment newInstance() {
        WidgetListFragment listFragment = new WidgetListFragment();
        return listFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_widget_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        Log.d(TAG, "onViewCreated");
        initChildView(view);
        initData(view);
//        final Context context = view.getContext();
//        initRecording(context);
//        Button button = view.findViewById(R.id.btnRecord);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startRecording(context);
//            }
//        });


    }

    private SpeechRecognizer mSpeechRecognizer = null;

    private void initRecording(Context context) {
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {
                Log.d(TAG, "onReadyForSpeech");
            }

            @Override
            public void onBeginningOfSpeech() {
                Log.d(TAG, "onBeginningOfSpeech");

            }

            @Override
            public void onRmsChanged(float v) {
                Log.d(TAG, "onRmsChanged : " + v);

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

                Log.d(TAG, "onBufferReceived : " + bytes);
                Log.d(TAG, "onBufferReceived : " + new String(bytes));
            }

            @Override
            public void onEndOfSpeech() {
                Log.d(TAG, "onEndOfSpeech");

            }

            @Override
            public void onError(int i) {

                Log.d(TAG, "onError : " + i);
            }

            @Override
            public void onResults(Bundle bundle) {
                ArrayList<String> matches = bundle
                        .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                for (int i = 0; i < matches.size(); i++) {
                    Log.d(TAG, "onResults text : " + matches.get(i));
                }

            }

            @Override
            public void onPartialResults(Bundle bundle) {
                ArrayList<String> matches = bundle
                        .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                for (int i = 0; i < matches.size(); i++) {
                    Log.d(TAG, "onPartialResults text : " + matches.get(i));
                }
            }

            @Override
            public void onEvent(int i, Bundle bundle) {
                Log.d(TAG, "onEvent");
            }
        });
    }

    private void startRecording(Context context) {
        mSpeechRecognizer.cancel();
        Intent recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ko-KR");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, context.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
//        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, false);
        mSpeechRecognizer.startListening(recognizerIntent);
    }

    private ArrayObjectAdapter mWidgetAdapter = null;
    private VerticalGridView mGridView = null;

    private void initChildView(View view) {
        mGridView = view.findViewById(R.id.gridView);
        mWidgetAdapter = new ArrayObjectAdapter(new SinglePresenterSelector(new WidgetPreviewPresenter()));
        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter();
        itemBridgeAdapter.setAdapter(mWidgetAdapter);
        mGridView.setAdapter(itemBridgeAdapter);
    }

    private void initData(View view) {
        AppWidgetManager mAppWidgetManager = AppWidgetManager.getInstance(view.getContext());
        List<AppWidgetProviderInfo> list = mAppWidgetManager.getInstalledProviders();
        Log.d(TAG, "onViewCreated : " + list.size());
        for (AppWidgetProviderInfo providerInfo : list) {
            Log.d(TAG, "onViewCreated : " + providerInfo.toString());
        }
        mWidgetAdapter.addAll(0, list);
    }
}
