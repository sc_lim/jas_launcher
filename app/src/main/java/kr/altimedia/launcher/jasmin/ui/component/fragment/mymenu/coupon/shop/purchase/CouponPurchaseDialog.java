/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.coupon.object.BonusType;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProduct;
import kr.altimedia.launcher.jasmin.dm.payment.VodPaymentDataManager;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.BankResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.CreditCardResponse;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.adapter.CouponPaymentButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.CouponPurchaseResultListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment.CouponPaymentDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.presenter.CouponPaymentButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.util.task.TaskManager;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class CouponPurchaseDialog extends SafeDismissDialogFragment implements View.OnUnhandledKeyEventListener{
    public static final String CLASS_NAME = CouponPurchaseDialog.class.getName();
    public static final String TAG = CouponPurchaseDialog.class.getSimpleName();

    private static final String KEY_COUPON = "COUPON";
    private static final String KEY_TERMS_CONDITION = "TERMS_CONDITION";

    private LinearLayout paymentGridViewLayer;
    private HorizontalGridView paymentGridView;
    private ArrayObjectAdapter paymentObjectAdapter = null;
    private LinearLayout couponInfoLayer;
    private LinearLayout paymentButtonLayer;
    private View rootView;

    private CouponProduct couponItem;

    private ArrayList<PaymentType> paymentButtons = new ArrayList<>();
    private CouponPurchaseResultListener mCouponPurchaseResultListener;
    private TvOverlayManager mTvOverlayManager;
    private String termsAndConditions = "";

    private final PurchaseTaskManager mPurchaseTaskManager = new PurchaseTaskManager();
    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private ArrayList<PaymentType> availablePaymentTypeList;
    private CreditCardResponse.CreditCardResult creditCardResult;
    private BankResponse.BankResult bankResult;

    private DialogInterface.OnDismissListener mOnDismissListener = null;

    private CouponPurchaseDialog() {
    }

    public static CouponPurchaseDialog newInstance(CouponProduct couponItem, String termsAndConditions) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_COUPON, couponItem);
        args.putString(KEY_TERMS_CONDITION, termsAndConditions);
        CouponPurchaseDialog fragment = new CouponPurchaseDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private void showSafeDialog(SafeDismissDialogFragment dialog) {
        if (mTvOverlayManager == null) {
            return;
        }
        mTvOverlayManager.showDialogFragment(dialog);
    }

    @Override
    public void onDestroyView() {
        mPurchaseTaskManager.cancelAllTask();

        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }

        if (mProgressBarManager != null) {
            mProgressBarManager.hide();
            mProgressBarManager.setRootView(null);
        }

        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach: "+mTvOverlayManager);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_coupon_shop_purchase, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        initProgressbar(view);
        initData();
        initPurchaseInfo(view);
        initPaymentGridView(view);

        loadAvailablePaymentMethod();
    }

    private void initData() {
        Bundle arguments = getArguments();
        if(arguments != null) {
            couponItem = arguments.getParcelable(KEY_COUPON);
            termsAndConditions = arguments.getString(KEY_TERMS_CONDITION);
        }
    }

    private void initPurchaseInfo(View view) {
        couponInfoLayer = view.findViewById(R.id.couponInfoLayer);
        paymentButtonLayer = view.findViewById(R.id.paymentButtonLayer);

        RelativeLayout couponBgImage = view.findViewById(R.id.couponBgImage);
        TextView couponPrice = view.findViewById(R.id.couponPrice);

        LinearLayout bonusValueLayer = view.findViewById(R.id.bonusValueLayer);
        TextView bonusValue = view.findViewById(R.id.bonusValue);
        TextView bonusUnit = view.findViewById(R.id.bonusUnit);

        TextView totalPaymentValue = view.findViewById(R.id.totalPaymentValue);

        TextView chargedCouponPrice = view.findViewById(R.id.chargedCouponPrice);

        LinearLayout chargedBonusValueLayer = view.findViewById(R.id.chargedBonusValueLayer);
        TextView chargedBonusValue = view.findViewById(R.id.chargedBonusValue);
        TextView chargedBonusUnit = view.findViewById(R.id.chargedBonusUnit);

        TextView expirationDate = view.findViewById(R.id.expirationDate);
        TextView totalCouponPrice = view.findViewById(R.id.totalCouponPrice);

        couponBgImage.setBackground(view.getResources().getDrawable(couponItem.getBgImage()));
        try {
            couponPrice.setText(StringUtil.getFormattedNumber(couponItem.getChargedAmount()));
        }catch (Exception e){
        }
        try {
            if(couponItem.getBonusValue() > 0 && (couponItem.getBonusType() == BonusType.DC_RATE || couponItem.getBonusType() == BonusType.DC_AMOUNT)) {
                if (couponItem.getBonusType() != null) {
                    bonusValue.setText(StringUtil.getFormattedNumber(couponItem.getBonusValue()));
                    chargedBonusValue.setText(StringUtil.getFormattedNumber(couponItem.getBonusValue()));
                    if (couponItem.getBonusType() == BonusType.DC_RATE) {
                        bonusUnit.setText("%");
                        chargedBonusUnit.setText("%");
                    } else if (couponItem.getBonusType() == BonusType.DC_AMOUNT) {
                        bonusUnit.setText(R.string.thb);
                        chargedBonusUnit.setText(R.string.thb);
                    }
                    bonusValueLayer.setVisibility(View.VISIBLE);
                    chargedBonusValueLayer.setVisibility(View.VISIBLE);
                }
            }else{
                bonusValueLayer.setVisibility(View.GONE);
                chargedBonusValueLayer.setVisibility(View.GONE);
            }
        }catch (Exception e){
        }

        totalPaymentValue.setText(StringUtil.getFormattedNumber(couponItem.getPrice()));

        int chargedAmt = couponItem.getChargedAmount(); // + couponItem.getBonusAmount();
        chargedCouponPrice.setText(StringUtil.getFormattedNumber(chargedAmt));

        String availablePeriod = this.getResources().getString(R.string.no_expiration_limit);
        try{
            String availablePeriodValue = couponItem.getAvailablePeriodValue();
            if(availablePeriodValue != null && !availablePeriodValue.isEmpty()) {
                availablePeriod = availablePeriodValue + " " +
                        this.getResources().getString(couponItem.getAvailablePeriodUnit().getName()) + " " +
                        this.getResources().getString(R.string.product_coupon_period_desc);
            }
        } catch (Exception e) {
        }

        expirationDate.setText(availablePeriod);

        totalCouponPrice.setText(StringUtil.getFormattedNumber(couponItem.getPrice()));

        TextView btnDetail = view.findViewById(R.id.btnDetail);
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CouponTermsDialog couponTermsDialogFragment = CouponTermsDialog.newInstance(termsAndConditions);
                showSafeDialog(couponTermsDialogFragment);
            }
        });
        btnDetail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    couponInfoLayer.setBackgroundColor(view.getResources().getColor(R.color.color_FF191919, null));
                }else{
                    couponInfoLayer.setBackgroundColor(view.getResources().getColor(R.color.transparent, null));
                }
            }
        });

        TextView btnCancel = view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CouponPurchaseDialog.this.dismiss();
            }
        });
    }

    private void initPaymentGridView(View view) {
        paymentGridViewLayer = view.findViewById(R.id.paymentGridViewLayer);
        paymentGridView = view.findViewById(R.id.paymentGridView);

        int horizontalSpacing = (int) view.getResources().getDimension(R.dimen.coupon_payment_button_horizontal_space);
        paymentGridView.setHorizontalSpacing(horizontalSpacing);
    }

    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
        mProgressBarManager.setInitialDelay(0);
    }

    private void showProgress() {
        mProgressBarManager.show();
    }

    private void hideProgress() {
        if (mPurchaseTaskManager.isEmpty()) {
            mProgressBarManager.hide();
        }
    }

    private void loadAvailablePaymentMethod() {
        if (Log.INCLUDE) {
            Log.d(TAG, "loadAvailablePaymentMethod: couponItem=" + couponItem);
        }
        mPurchaseTaskManager.getAvailablePaymentMethod(
                couponItem.getId(),
                new MbsDataProvider<String, List<PaymentType>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            mPurchaseTaskManager.completeLoadPaymentMethodTask();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, List<PaymentType> result) {
                        availablePaymentTypeList = (ArrayList<PaymentType>) result;

                        boolean needMoreLoad = false;
                        for (PaymentType paymentType : availablePaymentTypeList) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "loadAvailablePaymentMethod: paymentType=" + paymentType);
                            }

                            if (paymentType == PaymentType.CREDIT_CARD) {
                                needMoreLoad = true;
                                loadCreditCardList();
                            } else if (paymentType == PaymentType.INTERNET_MOBILE_BANK) {
                                needMoreLoad = true;
                                loadBankList();
                            }
                        }
                        if (!needMoreLoad) {
                            setPaymentGridView();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return getContext();
                    }
                });
    }

    private void loadCreditCardList() {
        if (Log.INCLUDE) {
            Log.d(TAG, "loadCreditCardList");
        }

        mPurchaseTaskManager.getCreditCardListResult(
                new MbsDataProvider<String, CreditCardResponse.CreditCardResult>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            mPurchaseTaskManager.completeLoadCreditListTask();
                            setPaymentGridView();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, CreditCardResponse.CreditCardResult result) {
                        creditCardResult = result;

                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadAvailablePaymentMethod: onSuccess: creditCardResult=" + creditCardResult);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return getContext();
                    }
                });
    }

    private void loadBankList() {
        if (Log.INCLUDE) {
            Log.d(TAG, "loadBankList");
        }

        mPurchaseTaskManager.getBankListResult(
                new MbsDataProvider<String, BankResponse.BankResult>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            showProgress();
                        } else {
                            mPurchaseTaskManager.completeLoadBankListTask();
                            setPaymentGridView();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, BankResponse.BankResult result) {
                        bankResult = result;

                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadAvailablePaymentMethod: onSuccess: bankResult=" + bankResult);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss();
                            }
                        });
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return getContext();
                    }
                });
    }

    private void setPaymentGridView() {
        hideProgress();

        if(paymentButtons != null){
            paymentButtons.clear();
        }
        for(PaymentType ptype : availablePaymentTypeList){
            if(ptype.getName() > 0){
                paymentButtons.add(ptype);
            }
        }

        try {
            int size = paymentButtons.size();
            if(size < 4) {
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) paymentGridViewLayer.getLayoutParams();
                layoutParams.width = (int) paymentGridViewLayer.getResources().getDimension(R.dimen.coupon_payment_button_width) * size + (int) paymentGridViewLayer.getResources().getDimension(R.dimen.coupon_payment_button_horizontal_space) * (size - 1);
                paymentGridViewLayer.setLayoutParams(layoutParams);
            }
        }catch (Exception e){
        }

        CouponPaymentButtonPresenter presenter = new CouponPaymentButtonPresenter(false, new CouponPaymentButtonPresenter.OnKeyListener() {
            @Override
            public boolean onKey(int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "CouponPaymentButton: onKey: keyCode=" + keyCode + ", index=" + index);
                }
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER: {
                        if (item instanceof PaymentType) {
                            PaymentType paymentType = (PaymentType) item;
                            onPaymentSelected(paymentType);
                        }
                    }
                }
                return false;
            }},
            new CouponPaymentButtonPresenter.OnFocusListener(){
                @Override
                public void onFocus(View view, boolean hasFocus, Object item) {
                    if(hasFocus){
                        paymentButtonLayer.setBackgroundColor(view.getResources().getColor(R.color.color_FF191919, null));
                    }else{
                        paymentButtonLayer.setBackgroundColor(view.getResources().getColor(R.color.transparent, null));
                    }
                }
            });
        paymentObjectAdapter = new ArrayObjectAdapter(presenter);
        paymentObjectAdapter.addAll(0, paymentButtons);
        CouponPaymentButtonBridgeAdapter bridgeAdapter = new CouponPaymentButtonBridgeAdapter();
        bridgeAdapter.setAdapter(paymentObjectAdapter);
        paymentGridView.setAdapter(bridgeAdapter);

        // set default focus
        paymentGridView.requestFocus();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                paymentGridView.setSelectedPosition(0);
                View child = paymentGridView.getChildAt(0);
                if (child != null) {
                    child.requestFocus();
                    paymentButtonLayer.setBackgroundColor(getResources().getColor(R.color.color_FF191919, null));
                }
            }
        });
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (Log.INCLUDE) {
            Log.d(TAG, "onDismiss");
        }
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
            mOnDismissListener = null;
        }
    }

    private void onPaymentSelected(PaymentType paymentType){
        PurchaseInfo purchaseInfo = new PurchaseInfo(couponItem, paymentType);
        Parcelable option = (Parcelable) this.getPaymentOption(paymentType);
        CouponPaymentDialog couponPaymentDialog = CouponPaymentDialog.newInstance(purchaseInfo, option);
        couponPaymentDialog.setOnPaymentListener(new CouponPaymentDialog.OnPaymentCompleteListener() {
            @Override
            public void onSuccess() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onPaymentSelected: onSuccess");
                }
                if(mCouponPurchaseResultListener != null){
                    String couponId = "";
                    if(couponItem != null){
                        couponId = couponItem.getId();
                    }
                    mCouponPurchaseResultListener.onSuccess(couponId);
                }
                CouponPurchaseDialog.this.dismiss();
            }
            @Override
            public void onFailure() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onPaymentSelected: onFailure");
                }
                if(mCouponPurchaseResultListener != null){
                    mCouponPurchaseResultListener.onFailure();
                }
                CouponPurchaseDialog.this.dismiss();
            }

            @Override
            public void onCancel() {
                couponPaymentDialog.dismiss();
            }
        });
        showSafeDialog(couponPaymentDialog);
    }

    private Object getPaymentOption(PaymentType paymentType) {
        if (paymentType == null) {
            return null;
        }
        switch (paymentType) {
            case CREDIT_CARD:
                return creditCardResult;
            case INTERNET_MOBILE_BANK:
                return bankResult;
        }
        return null;
    }

    public void setOnDismissListener(@NonNull DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    public void setCouponPurchaseResultListener(CouponPurchaseResultListener listener) {
        this.mCouponPurchaseResultListener = listener;
    }

    private static class PurchaseTaskManager extends TaskManager {
        private final VodPaymentDataManager mVodPaymentDataManager = new VodPaymentDataManager();

        private MbsDataTask paymentMethodTask;
        private MbsDataTask creditCardListTask;
        private MbsDataTask bankListTask;

        public PurchaseTaskManager() {
        }

        public void getAvailablePaymentMethod(String offerId, MbsDataProvider<String, List<PaymentType>> mbsDataProvider) {
            paymentMethodTask = mVodPaymentDataManager.getAvailablePaymentType(offerId, "coupon", mbsDataProvider);
            addTask(paymentMethodTask);
        }


        public void getCreditCardListResult(MbsDataProvider<String, CreditCardResponse.CreditCardResult> mbsDataProvider) {
            creditCardListTask = mVodPaymentDataManager.getCreditCardListResult(mbsDataProvider);
            addTask(creditCardListTask);
        }

        public void getBankListResult(MbsDataProvider<String, BankResponse.BankResult> mbsDataProvider) {
            bankListTask = mVodPaymentDataManager.getBankListResult(mbsDataProvider);
            addTask(bankListTask);
        }

        public void completeLoadPaymentMethodTask() {
            completeTask(paymentMethodTask);
        }

        public void completeLoadCreditListTask() {
            completeTask(creditCardListTask);
        }

        public void completeLoadBankListTask() {
            completeTask(bankListTask);
        }
    }
}
