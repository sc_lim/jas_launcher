/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.presenter.SubscriptionBoxPresenter;
import kr.altimedia.launcher.jasmin.ui.view.adapter.IndicatedItemAdapter;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class SubscriptionSelectDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = SubscriptionSelectDialogFragment.class.getName();
    private static final String TAG = SubscriptionSelectDialogFragment.class.getSimpleName();

    private static final String KEY_PRODUCT_LIST = "PRODUCT_LIST";

    private final int VISIBLE_BOX_COUNT = 4;

    private ArrayList<Product> productList;

    private SubscriptionInfoDialogFragment.OnSelectSubscriptionListener onSelectSubscriptionListener;

    public SubscriptionSelectDialogFragment() {

    }

    public static SubscriptionSelectDialogFragment newInstance(ArrayList<Product> productList) {

        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_PRODUCT_LIST, productList);

        SubscriptionSelectDialogFragment fragment = new SubscriptionSelectDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    public void setOnSelectSubscriptionListener(SubscriptionInfoDialogFragment.OnSelectSubscriptionListener onSelectSubscriptionListener) {
        this.onSelectSubscriptionListener = onSelectSubscriptionListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_subscription_select, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        productList = getArguments().getParcelableArrayList(KEY_PRODUCT_LIST);

        initSubscriptionGirdView(view);

        view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void initSubscriptionGirdView(View view) {
        int size = productList.size();
        ImageIndicator leftIndicator = view.findViewById(R.id.leftIndicator);
        ImageIndicator rightIndicator = view.findViewById(R.id.rightIndicator);

        leftIndicator.initIndicator(size, VISIBLE_BOX_COUNT);
        rightIndicator.initIndicator(size, VISIBLE_BOX_COUNT);

        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter(new SubscriptionBoxPresenter());
        arrayObjectAdapter.addAll(0, productList);

        HorizontalGridView subscriptionGridView = getGridView(view);
        SubscriptionItemBridgeAdapter itemBridgeAdapter = new SubscriptionItemBridgeAdapter(arrayObjectAdapter, subscriptionGridView, getTvOverlayManager(), this);
        itemBridgeAdapter.setOnSelectSubscriptionListener(onSelectSubscriptionListener);
        subscriptionGridView.setAdapter(itemBridgeAdapter);

        subscriptionGridView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int focus = subscriptionGridView.getSelectedPosition();

                leftIndicator.updateArrow(focus);
                rightIndicator.updateArrow(focus);
            }
        });
    }

    private HorizontalGridView getGridView(View view) {
        HorizontalGridView subscriptionGridView = view.findViewById(R.id.subscription_grid_view);

        int size = productList.size();
        if (size == 0) {
            subscriptionGridView.setEnabled(false);
        }

        int gap = (int) view.getResources().getDimension(R.dimen.coupon_subscription_button_spacing);
        int dimen = (int) (view.getResources().getDimension(R.dimen.coupon_subscription_box_width) + gap);
        int width = size < VISIBLE_BOX_COUNT ? (size * dimen - gap) : (VISIBLE_BOX_COUNT * dimen - gap);
        ViewGroup.LayoutParams params = subscriptionGridView.getLayoutParams();
        params.width = width;
        subscriptionGridView.setLayoutParams(params);
        subscriptionGridView.setHorizontalSpacing(gap);

        return subscriptionGridView;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onSelectSubscriptionListener = null;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private static class SubscriptionItemBridgeAdapter extends IndicatedItemAdapter {
        private final float ALIGNMENT = 87.8f;

        private TvOverlayManager tvOverlayManager;
        private HorizontalGridView mHorizontalGridView;
        private SubscriptionInfoDialogFragment.OnSelectSubscriptionListener onSelectSubscriptionListener;
        private SafeDismissDialogFragment dialogFragment;

        public SubscriptionItemBridgeAdapter(ObjectAdapter adapter, HorizontalGridView mHorizontalGridView,
                                             TvOverlayManager tvOverlayManager, SafeDismissDialogFragment dialogFragment) {
            super(adapter);
            this.mHorizontalGridView = mHorizontalGridView;
            this.tvOverlayManager = tvOverlayManager;
            this.dialogFragment = dialogFragment;

            initAlignment();
        }

        @SuppressLint("RestrictedApi")
        private void initAlignment() {
            if (mHorizontalGridView == null) {
                return;
            }

            mHorizontalGridView.setWindowAlignmentOffsetPercent(ALIGNMENT);
        }

        public void setOnSelectSubscriptionListener(SubscriptionInfoDialogFragment.OnSelectSubscriptionListener onSelectSubscriptionListener) {
            this.onSelectSubscriptionListener = onSelectSubscriptionListener;
        }

        @Override
        protected void onBind(IndicatedItemAdapter.ViewHolder viewHolder) {
            super.onBind(viewHolder);

            Product product = (Product) viewHolder.mItem;
            int lastIndex = getAdapter().size() - 1;
            viewHolder.itemView.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    int index = mHorizontalGridView.getSelectedPosition();
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_LEFT:
                            if (index == 0) {
                                mHorizontalGridView.setSelectedPosition(lastIndex);
                                return true;
                            }

                            break;
                        case KeyEvent.KEYCODE_DPAD_RIGHT:
                            if (index == lastIndex) {
                                mHorizontalGridView.setSelectedPosition(0);
                                return true;
                            }

                            break;
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                            SubscriptionInfoDialogFragment subscriptionInfoDialogFragment = SubscriptionInfoDialogFragment.newInstance(product);
                            subscriptionInfoDialogFragment.setOnSelectSubscriptionListener(new SubscriptionInfoDialogFragment.OnSelectSubscriptionListener() {
                                @Override
                                public void onSelect(Product product) {
                                    if (Log.INCLUDE) {
                                        Log.d(TAG, "onSelect, product : " + product);
                                    }

                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (onSelectSubscriptionListener != null) {
                                                onSelectSubscriptionListener.onSelect(product);
                                                subscriptionInfoDialogFragment.dismiss();
                                            }
                                        }
                                    });

                                    if (product != null) {
                                        dialogFragment.dismiss();
                                    }
                                }
                            });

                            tvOverlayManager.showDialogFragment(subscriptionInfoDialogFragment);
                            return true;
                    }

                    return false;
                }
            });
        }

        @Override
        protected void onUnbind(ViewHolder viewHolder) {
            super.onUnbind(viewHolder);
            viewHolder.itemView.setOnKeyListener(null);
        }
    }
}
