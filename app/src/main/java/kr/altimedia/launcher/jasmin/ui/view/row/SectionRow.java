/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;

import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class SectionRow extends Row {

    public SectionRow(HeaderItem headerItem) {
        super(headerItem);
    }

    public SectionRow(long id, int name) {
        super(new HeaderItem(id, name));
    }

    public SectionRow(int name) {
        super(new HeaderItem(name));
    }

    @Override
    final public boolean isRenderedAsRowView() {
        return false;
    }
}