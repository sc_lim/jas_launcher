/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog.playbck.presenter;

import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;

/**
 * Created by mc.kim on 04,03,2020
 */
public interface VodEndDataProvider {
    Content getContents();

    boolean hasSeries();

    boolean isEOF();

    boolean isPurchasedNextMovie();

    void onAction(VodEndActionPresenter.Action action);
}
