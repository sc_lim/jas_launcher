/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */
package kr.altimedia.launcher.jasmin.ui.app.module;

import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.util.Log;

import java.util.ArrayList;

/**
 * Flags for tuning non ui behavior.
 */
public abstract class DefaultBackendKnobsFlags
        implements BackendKnobsFlags {
    private final String TAG = DefaultBackendKnobsFlags.class.getSimpleName();
    private boolean mSystemDialogVisibility = false;
    private final ArrayList<String> mUpdatedCategoryList = new ArrayList<>();
    private final ArrayList<String> mEpgInitializingList = new ArrayList<>();
    public static final String WORK_BOOT = "boot";
    public static final String WORK_EPG_SCANNING = "epgScanning";
    public static final String WORK_EPG_PERMISSION_GRANTED = "epgPermissionGranted";
    private boolean mLaunchAppKeBlock = false;


    public DefaultBackendKnobsFlags() {
        mEpgInitializingList.add(WORK_BOOT);
        mEpgInitializingList.add(WORK_EPG_SCANNING);
    }

    public void removeInitializeWork(String work) {
        if (Log.INCLUDE) {
            Log.d(TAG, "removeInitializeWork : " + work);
        }
        mEpgInitializingList.remove(work);
        if (mEpgInitializingList.isEmpty()) {
            notifyEpgInitializeComplete();
        }
    }

    public abstract void notifyEpgInitializeComplete();

    @Override
    public boolean compiled() {
        return true;
    }

    @Override
    public long epgFetcherIntervalHour() {
        return 25;
    }

    @Override
    public long programGuideInitialFetchHours() {
        return 4;
    }

    @Override
    public long programGuideMaxHours() {
        return 168;
    }

    @Override
    public boolean isEpgReady(boolean containUserData) {
        return false;
    }

    @Override
    public long epgTargetChannelCount() {
        return 100;
    }

    @Override
    public boolean needMenuUpdated(boolean flush) {
        return false;
    }

    @Override
    public void notifyMenuVersionChecked() {

    }

    @Override
    public boolean checkCategoryUpdated(String categoryId, boolean flush) {
        boolean contain = mUpdatedCategoryList.contains(categoryId);
        if (flush) {
            mUpdatedCategoryList.remove(categoryId);
        }
        return contain;
    }

    @Override
    public void putCategoryUpdated(String categoryId) {
        boolean contain = mUpdatedCategoryList.contains(categoryId);
        if (!contain) {
            mUpdatedCategoryList.add(categoryId);
        }
    }

    @Override
    public void setSystemDialogVisibility(boolean visibility) {
        mSystemDialogVisibility = visibility;
    }

    @Override
    public boolean getSystemDialogVisibility() {
        return mSystemDialogVisibility;
    }

    public boolean isLaunchAppKeBlock() {
        return mLaunchAppKeBlock;
    }

    public void setLaunchAppKeBlock(boolean mLaunchAppKeBlock) {
        this.mLaunchAppKeBlock = mLaunchAppKeBlock;
    }
}
