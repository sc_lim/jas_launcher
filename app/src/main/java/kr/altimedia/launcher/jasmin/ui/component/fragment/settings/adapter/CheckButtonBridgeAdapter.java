/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter;

import androidx.leanback.widget.ObjectAdapter;

public class CheckButtonBridgeAdapter extends SettingBaseButtonItemBridgeAdapter {
    public CheckButtonBridgeAdapter(ObjectAdapter adapter) {
        super(adapter);
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
    }
}
