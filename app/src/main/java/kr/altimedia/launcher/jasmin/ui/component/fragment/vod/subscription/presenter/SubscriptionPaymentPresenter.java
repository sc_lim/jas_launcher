/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.presenter;

import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.single.presenter.SubscriptionButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.SubscriptionButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionPriceInfo;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class SubscriptionPaymentPresenter extends Presenter {
    private static final int SUBSCRIBE_BUTTON = 0;
    private static final int CANCEL_BUTTON = 1;

    private OnClickPaymentButtonListener onClickPaymentButtonListener;

    public SubscriptionPaymentPresenter() {
    }

    public void setOnClickPaymentButtonListener(OnClickPaymentButtonListener onClickPaymentButtonListener) {
        this.onClickPaymentButtonListener = onClickPaymentButtonListener;
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_subscription_payment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setOnClickPaymentButtonListener(onClickPaymentButtonListener);
        vh.setData((SubscriptionPriceInfo) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setOnClickPaymentButtonListener(null);
    }

    public static class ViewHolder extends Presenter.ViewHolder implements SubscriptionButtonBridgeAdapter.OnButtonClickListener {
        private TextView value;
        private TextView unit;
        private TextView price;

        private HorizontalGridView gridView;
        private ArrayObjectAdapter objectAdapter;

        private OnClickPaymentButtonListener onClickPaymentButtonListener;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            value = view.findViewById(R.id.duration_value);
            unit = view.findViewById(R.id.duration_unit);
            price = view.findViewById(R.id.price);

            initGridView();
        }

        private void initGridView() {
            gridView = view.findViewById(R.id.grid_view);

            objectAdapter = new ArrayObjectAdapter(new SubscriptionButtonPresenter());
            objectAdapter.add(SUBSCRIBE_BUTTON, new SubscriptionButtonPresenter.ButtonItem(R.string.subscribe, false, false));
            objectAdapter.add(CANCEL_BUTTON, new SubscriptionButtonPresenter.ButtonItem(R.string.cancel, false, true));

            SubscriptionButtonBridgeAdapter mSubscriptionButtonBridgeAdapter = new SubscriptionButtonBridgeAdapter(objectAdapter, gridView);
            mSubscriptionButtonBridgeAdapter.setOnButtonClickListener(this);
            gridView.setAdapter(mSubscriptionButtonBridgeAdapter);
        }

        public void updateSubscribeButton(boolean isAgree) {
            SubscriptionButtonPresenter.ButtonItem subscribeButtonItem = (SubscriptionButtonPresenter.ButtonItem) objectAdapter.get(SUBSCRIBE_BUTTON);

            if (isAgree != subscribeButtonItem.isEnabled()) {
                subscribeButtonItem.setEnabled(isAgree);
                objectAdapter.replace(SUBSCRIBE_BUTTON, subscribeButtonItem);
            }

            Handler mHandler = new Handler(Looper.getMainLooper());
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    gridView.getChildAt(SUBSCRIBE_BUTTON).requestFocus();
                }
            });
        }

        private void setOnClickPaymentButtonListener(OnClickPaymentButtonListener onClickPaymentButtonListener) {
            this.onClickPaymentButtonListener = onClickPaymentButtonListener;
        }

        public void setData(SubscriptionPriceInfo subscriptionPriceInfo) {
            int duration = subscriptionPriceInfo.getDuration();
            int monthsVisibility = duration == 1 ? View.INVISIBLE : View.VISIBLE;

            view.findViewById(R.id.months_layout).setVisibility(monthsVisibility);
            if (duration > 1) {
                value.setText(String.valueOf(duration));
                unit.setText(view.getContext().getString(R.string.months));
            }
            price.setText(subscriptionPriceInfo.getPrice());
        }

        @Override
        public void onClick(int index, boolean isChecked) {
            if (index == SUBSCRIBE_BUTTON) {
                onClickPaymentButtonListener.onClickSubscribe();
            } else if (index == CANCEL_BUTTON) {
                onClickPaymentButtonListener.onClickCancel();
            }
        }
    }

    public interface OnClickPaymentButtonListener {
        void onClickSubscribe();

        void onClickCancel();
    }
}
