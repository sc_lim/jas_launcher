/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;

public class SettingProfilePresenter extends Presenter {

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_setting_profile, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        if (item == null) {
            viewHolder.view.setVisibility(View.INVISIBLE);
            return;
        }

        ViewHolder vh = (ViewHolder) viewHolder;
        vh.view.setVisibility(View.VISIBLE);
        vh.setData((ProfileInfo) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private ImageView pic;
        private ImageView lock;
        private TextView name;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            pic = view.findViewById(R.id.profile_pic);
            lock = view.findViewById(R.id.lock);
            name = view.findViewById(R.id.profile_name);
        }

        public void setData(ProfileInfo profileInfo) {
            name.setText(profileInfo.getProfileName());
            try {
                profileInfo.setProfileIcon(pic);
            }catch (Exception e){
            }
            int visibility = profileInfo.isLock() ? View.VISIBLE : View.INVISIBLE;
            lock.setVisibility(visibility);
        }
    }
}
