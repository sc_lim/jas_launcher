/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.message;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.message.module.MessageModule;
import kr.altimedia.launcher.jasmin.dm.message.obj.Message;
import kr.altimedia.launcher.jasmin.dm.message.remote.MessageRemote;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by mc.kim on 20,05,2020
 */
public class MessageDataManager extends BaseDataManager {
    private final String TAG = MessageDataManager.class.getSimpleName();
    private MessageRemote mMessageRemote;

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        MessageModule messageModule = new MessageModule();
        Retrofit contentRetrofit = messageModule.provideMessageModule(mOkHttpClient);
        mMessageRemote = new MessageRemote(messageModule.provideMessageApi(contentRetrofit));
    }

    @Override
    public String getLoginToken() {
        return AccountManager.getInstance().getLoginToken();
    }

    public MbsDataTask getMessageList(String language, String said, String profileId,
                                      MbsDataProvider<String, List<Message>> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, List<Message>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mMessageRemote.getMessageList(language, said, profileId);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);

                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onTaskPostExecute(String key, List<Message> result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask postMessageView(String accountId, String profileId, String messageId,
                                       MbsDataProvider<String, Boolean> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Boolean>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mMessageRemote.postMessageView(accountId, profileId, messageId);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(accountId);
        return mMbsDataTask;
    }
}
