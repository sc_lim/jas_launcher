package kr.altimedia.launcher.jasmin.dm.payment.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Bank;

public class BankResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private BankResult result;

    protected BankResponse(Parcel in) {
        super(in);
        result = in.readParcelable(BankResponse.class.getClassLoader());
    }

    public static final Creator<BankResponse> CREATOR = new Creator<BankResponse>() {
        @Override
        public BankResponse createFromParcel(Parcel in) {
            return new BankResponse(in);
        }

        @Override
        public BankResponse[] newArray(int size) {
            return new BankResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(result, flags);
    }

    public BankResult getResult() {
        return result;
    }

    public static class BankResult implements Parcelable {
        @Expose
        @SerializedName("bankList")
        private List<Bank> bankList;

        protected BankResult(Parcel in) {
            bankList = in.createTypedArrayList(Bank.CREATOR);
        }

        public static final Creator<BankResult> CREATOR = new Creator<BankResult>() {
            @Override
            public BankResult createFromParcel(Parcel in) {
                return new BankResult(in);
            }

            @Override
            public BankResult[] newArray(int size) {
                return new BankResult[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(bankList);
        }

        public List<Bank> getBankList() {
            return bankList;
        }
    }
}
