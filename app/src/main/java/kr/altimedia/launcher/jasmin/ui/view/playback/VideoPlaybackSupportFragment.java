/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.ItemAlignmentFacet;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.leanback.widget.SparseArrayObjectAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.RowsSupportFragment;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackGlueHost;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.TrailerSelectUi;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekDataProvider;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekUi;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;

/**
 * Created by mc.kim on 05,02,2020
 */
public class VideoPlaybackSupportFragment extends Fragment {
    static final String BUNDLE_CONTROL_VISIBLE_ON_CREATEVIEW = "controlvisible_oncreateview";

    /**
     * No background.
     */
    public static final int BG_NONE = 0;

    /**
     * A dark translucent background.
     */
    public static final int BG_DARK = 1;
    VideoPlaybackGlueHost.HostCallback mHostCallback;

    VideoPlaybackSeekUi.Client mSeekUiClient;
    TrailerSelectUi.Client mVideoSelectUiClient;
    boolean mInSeek;
    ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private boolean mIsEnableUseVideoSetting = false;

    private SidePanelLayout.SidePanelActionListener mSidePanelActionListener = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mSidePanelActionListener = (SidePanelLayout.SidePanelActionListener) context;
    }

    public void resetFocus() {
        if(Log.INCLUDE){
            Log.d(TAG,"resetFocus");
        }
        ItemBridgeAdapter.ViewHolder vh = (ItemBridgeAdapter.ViewHolder) getVerticalGridView()
                .findViewHolderForAdapterPosition(0);
        if (vh != null && vh.getPresenter() instanceof VideoPlaybackRowPresenter) {
            ((VideoPlaybackRowPresenter) vh.getPresenter()).onReappear(
                    (RowPresenter.ViewHolder) vh.getViewHolder());
        }
    }

    public void onHide() {
        ItemBridgeAdapter.ViewHolder vh = (ItemBridgeAdapter.ViewHolder) getVerticalGridView()
                .findViewHolderForAdapterPosition(0);
        if (vh != null && vh.getPresenter() instanceof VideoPlaybackRowPresenter) {
            ((VideoPlaybackRowPresenter) vh.getPresenter()).onHide(
                    (RowPresenter.ViewHolder) vh.getViewHolder());
        }
    }

    private class SetSelectionRunnable implements Runnable {
        int mPosition;
        boolean mSmooth = true;

        SetSelectionRunnable() {
        }

        @Override
        public void run() {
            if (mRowsSupportFragment == null) {
                return;
            }
            mRowsSupportFragment.setSelectedPosition(mPosition, mSmooth);
        }
    }

    public static final int BG_LIGHT = 2;
    RowsSupportFragment mRowsSupportFragment;
    ObjectAdapter mAdapter;
    VideoPlaybackRowPresenter mPresenter;
    Row mRow;
    BaseOnItemViewSelectedListener mExternalItemSelectedListener;
    BaseOnItemViewClickedListener mExternalItemClickedListener;
    BaseOnItemViewClickedListener mPlaybackItemClickedListener;

    private final BaseOnItemViewClickedListener mOnItemViewClickedListener =
            new BaseOnItemViewClickedListener() {

                @Override
                public void onItemClicked(Presenter.ViewHolder itemViewHolder,
                                          Object item,
                                          Presenter.ViewHolder rowViewHolder,
                                          Object row) {
                    if (mPlaybackItemClickedListener != null
                            && rowViewHolder instanceof VideoPlaybackRowPresenter.ViewHolder) {
                        mPlaybackItemClickedListener.onItemClicked(
                                itemViewHolder, item, rowViewHolder, row);
                    }
                    if (mExternalItemClickedListener != null) {
                        mExternalItemClickedListener.onItemClicked(
                                itemViewHolder, item, rowViewHolder, row);
                    }
                }
            };

    private final BaseOnItemViewSelectedListener mOnItemViewSelectedListener =
            new BaseOnItemViewSelectedListener() {
                @Override
                public void onItemSelected(Presenter.ViewHolder itemViewHolder,
                                           Object item,
                                           Presenter.ViewHolder rowViewHolder,
                                           Object row) {
                    if (mExternalItemSelectedListener != null) {
                        mExternalItemSelectedListener.onItemSelected(
                                itemViewHolder, item, rowViewHolder, row);
                    }
                }
            };

    private final SetSelectionRunnable mSetSelectionRunnable = new SetSelectionRunnable();

    public ObjectAdapter getAdapter() {
        return mAdapter;
    }

    public static class OnFadeCompleteListener {
        public void onFadeInComplete() {
        }

        public void onFadeOutComplete() {
        }
    }

    private static final String TAG = "VideoPlaybackSupportFragment";
    private static final int ANIMATION_MULTIPLIER = 1;

    private static final int START_FADE_OUT = 1;

    // Fading status
    private static final int IDLE = 0;
    private static final int ANIMATING = 1;

    int mPaddingBottom;
    int mOtherRowsCenterToBottom;
    View mRootView;
    View mBackgroundView;
    View mIconView;
    int mBackgroundType = BG_DARK;
    int mBgDarkColor;
    int mBgLightColor;
    int mAutohideTimerAfterPlayingInMs;
    int mAutohideTimerAfterTickleInMs;
    int mMajorFadeTranslateY, mMinorFadeTranslateY;
    int mAnimationTranslateY;
    OnFadeCompleteListener mFadeCompleteListener;
    View.OnKeyListener mInputEventHandler;
    boolean mFadingEnabled = true;
    boolean mControlVisibleBeforeOnCreateView = true;
    boolean mControlVisible = true;
    boolean mCurrentTrailerMode = false;
    boolean mStateViewVisible = true;
    boolean mShowOrHideControlsOverlayOnUserInteraction = true;
    int mBgAlpha;

    ValueAnimator mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator;
    ValueAnimator mControlRowFadeInAnimator, mControlRowFadeOutAnimator;
    ValueAnimator mOtherRowFadeInAnimator, mOtherRowFadeOutAnimator;

    private final Animator.AnimatorListener mFadeListener =
            new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    enableVerticalGridAnimations(false);
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onAnimationEnd " + mBgAlpha);
                    }
                    if (mBgAlpha > 0) {
                        enableVerticalGridAnimations(true);
                        if (mFadeCompleteListener != null) {
                            mFadeCompleteListener.onFadeInComplete();
                        }
                    } else {
                        VerticalGridView verticalView = getVerticalGridView();
                        // reset focus to the primary actions only if the selected row was the controls row
                        if (verticalView != null && verticalView.getSelectedPosition() == 0) {
                            ItemBridgeAdapter.ViewHolder vh = (ItemBridgeAdapter.ViewHolder)
                                    verticalView.findViewHolderForAdapterPosition(0);
                            if (vh != null && vh.getPresenter() instanceof VideoPlaybackRowPresenter) {
                                ((VideoPlaybackRowPresenter) vh.getPresenter()).onReappear(
                                        (RowPresenter.ViewHolder) vh.getViewHolder());
                            }
                        }
                        if (mFadeCompleteListener != null) {
                            mFadeCompleteListener.onFadeOutComplete();
                        }
                    }
                }
            };

    public VideoPlaybackSupportFragment() {
        mProgressBarManager.setInitialDelay(500);
    }

    VerticalGridView getVerticalGridView() {
        if (mRowsSupportFragment == null) {
            return null;
        }
        return mRowsSupportFragment.getVerticalGridView();
    }

//    private final Handler mHandler = new Handler() {
//        @Override
//        public void handleMessage(Message message) {
//            if (message.what == START_FADE_OUT && mFadingEnabled) {
//                hideControlsOverlay(true);
//                hideStateOverlay(true);
//            }
//        }
//    };

    private final VerticalGridView.OnTouchInterceptListener mOnTouchInterceptListener =
            new VerticalGridView.OnTouchInterceptListener() {
                @Override
                public boolean onInterceptTouchEvent(MotionEvent event) {
                    return onInterceptInputEvent(event);
                }
            };

    private final VerticalGridView.OnKeyInterceptListener mOnKeyInterceptListener =
            new VerticalGridView.OnKeyInterceptListener() {
                @Override
                public boolean onInterceptKeyEvent(KeyEvent event) {
                    return onInterceptInputEvent(event);
                }
            };

    @SuppressWarnings("WeakerAccess") /* synthetic access */
    void setBgAlpha(int alpha) {
        mBgAlpha = alpha;
        Log.d(TAG, "setAlpha mBackgroundView != null : " + (mBackgroundView != null ? "Not Null" : "null"));
        if (mBackgroundView != null) {
            mBackgroundView.getBackground().setAlpha(alpha);
        }
    }

    @SuppressWarnings("WeakerAccess") /* synthetic access */
    void enableVerticalGridAnimations(boolean enable) {
        if (getVerticalGridView() != null) {
            getVerticalGridView().setAnimateChildLayout(enable);
        }
    }

    public void setShowOrHideControlsOverlayOnUserInteraction(boolean
                                                                      showOrHideControlsOverlayOnUserInteraction) {
        mShowOrHideControlsOverlayOnUserInteraction = showOrHideControlsOverlayOnUserInteraction;
    }

    public boolean isShowOrHideControlsOverlayOnUserInteraction() {
        return mShowOrHideControlsOverlayOnUserInteraction;
    }

    public void setControlsOverlayAutoHideEnabled(boolean enabled) {
//        if (DEBUG) {
//            Log.v(TAG, "setControlsOverlayAutoHideEnabled " + enabled);
//        }
//        if (enabled != mFadingEnabled) {
//            mFadingEnabled = enabled;
//            if (isResumed() && getView().hasFocus()) {
//                showControlsOverlay(true);
//                if (enabled) {
//                    // StateGraph 7->2 5->2
//                    startFadeTimer(mAutohideTimerAfterPlayingInMs);
//                } else {
//                    // StateGraph 4->5 2->5
//                    stopFadeTimer();
//                }
//            } else {
//                // StateGraph 6->1 1->6
//            }
//        }
    }

    public boolean isControlsOverlayAutoHideEnabled() {
        return mFadingEnabled;
    }

    public void setFadingEnabled(boolean enabled) {
        setControlsOverlayAutoHideEnabled(enabled);
    }

    public boolean isFadingEnabled() {
        return isControlsOverlayAutoHideEnabled();
    }

    public void setFadeCompleteListener(OnFadeCompleteListener listener) {
        mFadeCompleteListener = listener;
    }

    public OnFadeCompleteListener getFadeCompleteListener() {
        return mFadeCompleteListener;
    }

    /**
     * Sets the input event handler.
     */
    public final void setOnKeyInterceptListener(View.OnKeyListener handler) {
        mInputEventHandler = handler;
    }

    private final ItemBridgeAdapter.AdapterListener mAdapterListener =
            new ItemBridgeAdapter.AdapterListener() {
                @Override
                public void onAttachedToWindow(ItemBridgeAdapter.ViewHolder vh) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onAttachedToWindow " + vh.getViewHolder().view);
                    }
                    if (!mControlVisible) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "setting alpha to 0");
                        }
                        vh.getViewHolder().view.setAlpha(0);
                    }
                }

                @Override
                public void onCreate(ItemBridgeAdapter.ViewHolder vh) {
                    Presenter.ViewHolder viewHolder = vh.getViewHolder();
                    if (viewHolder instanceof VideoPlaybackSeekUi) {
                        ((VideoPlaybackSeekUi) viewHolder).setPlaybackSeekUiClient(mChainedClient);
                    }
                }

                @Override
                public void onDetachedFromWindow(ItemBridgeAdapter.ViewHolder vh) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onDetachedFromWindow " + vh.getViewHolder().view);
                    }
                    // Reset animation state
                    vh.getViewHolder().view.setAlpha(1f);
                    vh.getViewHolder().view.setTranslationY(0);
                    vh.getViewHolder().view.setAlpha(1f);
                }

                @Override
                public void onBind(ItemBridgeAdapter.ViewHolder vh) {
                }
            };

    public void tickle() {
        if (Log.INCLUDE) {
            Log.d(TAG, "tickle enabled " + mFadingEnabled + " isResumed " + isResumed());
        }
        //StateGraph 2->4
        stopFadeTimer();
        if (!isControlsOverlayVisible()) {
            showControlsOverlay(true);
        }
        // Optionally start fading out timer if it's currently playing (mFadingEnabled is true)
        if (mAutohideTimerAfterTickleInMs > 0 && mFadingEnabled) {
            startFadeTimer(mAutohideTimerAfterTickleInMs);
        }
    }

    final VideoPlaybackSeekUi.Client mChainedClient = new VideoPlaybackSeekUi.Client() {
        @Override
        public boolean isSeekEnabled() {
            return mSeekUiClient != null && mSeekUiClient.isSeekEnabled();
        }

        @Override
        public void onSeekStarted() {
            if (mSeekUiClient != null) {
                mSeekUiClient.onSeekStarted();
            }
            setSeekMode(true);
        }

        @Override
        public VideoPlaybackSeekDataProvider getPlaybackSeekDataProvider() {
            return mSeekUiClient == null ? null : mSeekUiClient.getPlaybackSeekDataProvider();
        }

        @Override
        public void onSeekPositionChanged(long pos) {
            if (mSeekUiClient != null) {
                mSeekUiClient.onSeekPositionChanged(pos);
            }
        }

        @Override
        public void onSeekFinished(boolean cancelled) {
            if (mSeekUiClient != null) {
                mSeekUiClient.onSeekFinished(cancelled);
            }
            setSeekMode(false);
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        if (mControlVisible) {
            //StateGraph: 6->5 1->2
            if (mFadingEnabled) {
                // StateGraph 1->2
                startFadeTimer(mAutohideTimerAfterPlayingInMs);
            }
        } else {
            //StateGraph: 6->7 1->3
        }
        getVerticalGridView().setOnTouchInterceptListener(mOnTouchInterceptListener);
        getVerticalGridView().setOnKeyInterceptListener(mOnKeyInterceptListener);
        if (mHostCallback != null) {
            mHostCallback.onHostResume();
        }
    }

    public boolean sendKeyEvent(KeyEvent event) {
        return mOnKeyInterceptListener.onInterceptKeyEvent(event);
    }

    private void stopFadeTimer() {
//        if (mHandler != null) {
//            mHandler.removeMessages(START_FADE_OUT);
//        }
    }

    private void startFadeTimer(int fadeOutTimeout) {
//        if (mHandler != null) {
//            mHandler.removeMessages(START_FADE_OUT);
//            mHandler.sendEmptyMessageDelayed(START_FADE_OUT, fadeOutTimeout);
//        }
    }

    private static ValueAnimator loadAnimator(Context context, int resId) {
        ValueAnimator animator = (ValueAnimator) AnimatorInflater.loadAnimator(context, resId);
        animator.setDuration(animator.getDuration() * ANIMATION_MULTIPLIER);
        return animator;
    }

    private TimeInterpolator mLogDecelerateInterpolator = new TimeInterpolator() {
        @Override
        public float getInterpolation(float t) {
            float mLogScale = 1f / computeLog(1, 100, 0);
            return computeLog(t, 100, 0) * mLogScale;
        }
    };

    private TimeInterpolator mLogAccelerateInterpolator = new TimeInterpolator() {
        @Override
        public float getInterpolation(float t) {
            float mLogScale = 1f / computeLog(1, 100, 0);
            return 1 - computeLog(1 - t, 100, 0) * mLogScale;
        }
    };

    private float computeLog(float t, int base, int drift) {
        return (float) -Math.pow(base, -t) + 1 + (drift * t);
    }

    private void loadTransportAnimator() {

        final float mTransportTranslateY = getResources().getDimension(R.dimen.trailer_translate_value);
        final ValueAnimator.AnimatorUpdateListener updateListener = new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator arg0) {
                if (getVerticalGridView() == null) {
                    return;
                }
                RecyclerView.ViewHolder vh = getVerticalGridView()
                        .findViewHolderForAdapterPosition(0);
                if (vh == null) {
                    return;
                }
                View view = vh.itemView;
                View transportLayer = view.findViewById(R.id.transport_row);
                final float fraction = (Float) arg0.getAnimatedValue();
                if (transportLayer != null) {

                    float translateValue = mTransportTranslateY * (1f - fraction);
                    transportLayer.setTranslationY(translateValue);
                }

                View progressLayer = view.findViewById(R.id.progressLayer);
                if (progressLayer != null) {
                    progressLayer.setAlpha(1f - fraction);
                }

                View descriptionLayer = view.findViewById(R.id.trailerLayer);
                if (descriptionLayer != null) {
                    descriptionLayer.setAlpha(fraction);
                }
            }
        };
        Context context = getContext();
        mTransportRowFadeInAnimator = loadAnimator(context, R.animator.trailer_controls_fade_in);
        mTransportRowFadeInAnimator.addUpdateListener(updateListener);
        mTransportRowFadeInAnimator.setInterpolator(mLogDecelerateInterpolator);
        mTransportRowFadeOutAnimator = loadAnimator(context, R.animator.trailer_controls_fade_out);
        mTransportRowFadeOutAnimator.addUpdateListener(updateListener);
        mTransportRowFadeOutAnimator.setInterpolator(mLogAccelerateInterpolator);
    }

    private void loadDescriptionAnimator() {
        final ValueAnimator.AnimatorUpdateListener updateListener = new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator arg0) {
                if (getVerticalGridView() == null) {
                    return;
                }
                RecyclerView.ViewHolder vh = getVerticalGridView()
                        .findViewHolderForAdapterPosition(0);
                if (vh == null) {
                    return;
                }
                View view = vh.itemView;
                View descriptionLayer = view.findViewById(R.id.descriptionLayer);
                if (descriptionLayer != null) {
                    final float fraction = (Float) arg0.getAnimatedValue();
                    descriptionLayer.setAlpha(fraction);
                    float translateValue = (float) mAnimationTranslateY * (1f - fraction);
                    descriptionLayer.setTranslationY(translateValue);
                }
            }
        };


        final ValueAnimator.AnimatorUpdateListener iconShowListener = new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator arg0) {
                mIconView.setAlpha(0);
            }
        };
        final ValueAnimator.AnimatorUpdateListener iconHideListener = new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator arg0) {
                final float fraction = (Float) arg0.getAnimatedValue();
                if (isKeyBlock()) {
                    return;
                }
                if (!isPlaying()) {
                    mIconView.setAlpha(1 - fraction);
                }
            }
        };
        Context context = getContext();


        mControlRowFadeInAnimator = loadAnimator(context, R.animator.lb_playback_controls_fade_in);
        mControlRowFadeInAnimator.addUpdateListener(updateListener);
        mControlRowFadeInAnimator.addUpdateListener(iconShowListener);
        mControlRowFadeInAnimator.setInterpolator(mLogDecelerateInterpolator);

        mControlRowFadeOutAnimator = loadAnimator(context,
                R.animator.lb_playback_controls_fade_out);
        mControlRowFadeOutAnimator.addUpdateListener(updateListener);
        mControlRowFadeOutAnimator.addUpdateListener(iconHideListener);
        mControlRowFadeOutAnimator.setInterpolator(mLogAccelerateInterpolator);
    }

    private void loadVideoStateAnimator() {
        final ValueAnimator.AnimatorUpdateListener updateListener = new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator arg0) {

                if (getVerticalGridView() == null) {
                    return;
                }
                RecyclerView.ViewHolder vh = getVerticalGridView()
                        .findViewHolderForAdapterPosition(0);
                if (vh == null) {
                    return;
                }
                View view = vh.itemView;
                View descriptionLayer = view.findViewById(R.id.controls_dock);
                if (descriptionLayer != null) {
                    final float fraction = (Float) arg0.getAnimatedValue();
                    descriptionLayer.setAlpha(fraction);
                }
            }
        };

        Context context = getContext();
        mOtherRowFadeInAnimator = loadAnimator(context, R.animator.lb_playback_controls_fade_in);
        mOtherRowFadeInAnimator.addUpdateListener(updateListener);
        mOtherRowFadeInAnimator.setInterpolator(mLogDecelerateInterpolator);

        mOtherRowFadeOutAnimator = loadAnimator(context, R.animator.lb_playback_controls_fade_out);
        mOtherRowFadeOutAnimator.addUpdateListener(updateListener);
        mOtherRowFadeOutAnimator.setInterpolator(new AccelerateInterpolator());
    }

    @Deprecated
    public void fadeOut() {
        showControlsOverlay(false, false);
    }

    public void showControlsOverlay(boolean runAnimation) {
        showControlsOverlay(true, runAnimation);
    }

    public void changeTrailerMode(boolean selectionMode, boolean runAnimation) {
        Log.d(TAG, "changeTrailerMode : " + selectionMode);
        RecyclerView.ViewHolder vh = getVerticalGridView()
                .findViewHolderForAdapterPosition(0);
        if (vh == null) {
            return;
        }
        ViewGroup descriptionDock = vh.itemView.findViewById(R.id.description_dock);
        View progressView = vh.itemView.findViewById(R.id.playback_progress);
        if (selectionMode) {
            if (descriptionDock != null) {
                descriptionDock.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
                descriptionDock.requestFocus();
            }
        } else {
            if (descriptionDock != null) {
                descriptionDock.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            }
            if (progressView != null) {
                progressView.requestFocus();
            }
        }
        showTrailerView(selectionMode, runAnimation);
    }

    public boolean isControlsOverlayVisible() {
        return mControlVisible;
    }

    public void hideControlsOverlay(boolean runAnimation) {

        showControlsOverlay(false, runAnimation);
    }

    public void showStateOverlay(boolean runAnimation) {
        showStateOverlay(true, runAnimation);
    }

    public void hideStateOverlay(boolean runAnimation) {
        showStateOverlay(false, runAnimation);
    }

    public void showIconView(boolean runAnimation) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showIconView");
        }
        mIconView.setAlpha(1f);
    }

    public void hideIconView(boolean runAnimation) {
        if (Log.INCLUDE) {
            Log.d(TAG, "hideIconView");
        }
        mIconView.setAlpha(0f);
    }

    public void updateRemainTime(long millisUntilFinished) {
        RecyclerView.ViewHolder vh = getVerticalGridView()
                .findViewHolderForAdapterPosition(0);
        if (vh == null) {
            return;
        }
        TextView timeView = vh.itemView.findViewById(R.id.remainTime);
        if (timeView != null) {
            StringBuilder builder = new StringBuilder();
            formatTime(millisUntilFinished, builder);
            timeView.setText(builder.toString());
            builder.setLength(0);
        }
    }

    private void formatTime(long ms, StringBuilder sb) {
        sb.setLength(0);
        if (ms < 0) {
            sb.append("--");
            return;
        }
        long seconds = ms / 1000;
        long minutes = seconds / 60;
        seconds -= minutes * 60;

        sb.append(minutes).append(':');
        if (seconds < 10) {
            sb.append('0');
        }
        sb.append(seconds);
    }

    static void reverseFirstOrStartSecond(ValueAnimator first, ValueAnimator second,
                                          boolean runAnimation) {
        if (first.isStarted()) {
            first.reverse();
            if (!runAnimation) {
                first.end();
            }
        } else {
            second.start();
            if (!runAnimation) {
                second.end();
            }
        }
    }

    /**
     * End first or second animator if they are still running.
     */
    static void endAll(ValueAnimator first, ValueAnimator second) {
        if (first.isStarted()) {
            first.end();
        } else if (second.isStarted()) {
            second.end();
        }
    }

    protected boolean checkEnableSidePanel() {
        return true;
    }

    protected boolean isKeyBlock() {
        return false;
    }

    @SuppressWarnings("WeakerAccess") /* synthetic access */
    boolean onInterceptInputEvent(InputEvent event) {
        final boolean controlsHidden = !mControlVisible;
        if (Log.INCLUDE) {
            Log.d(TAG, "onInterceptInputEvent hidden " + controlsHidden + " " + event);
        }
        boolean consumeEvent = false;
        int keyCode = KeyEvent.KEYCODE_UNKNOWN;
        int keyAction = 0;

        if (isKeyBlock()) {
            return consumeEvent;
        }

        if (event instanceof KeyEvent) {
            keyCode = ((KeyEvent) event).getKeyCode();
            keyAction = ((KeyEvent) event).getAction();
            if (mInputEventHandler != null) {
                consumeEvent = mInputEventHandler.onKey(getView(), keyCode, (KeyEvent) event);
            }
        }


        switch (keyCode) {
            case KeyEvent.KEYCODE_INFO:
                setIsEnableUseVideoSetting(checkEnableSidePanel());
                if (!isIsEnableUseVideoSetting()) {
                    return false;
                }
                if (!isPrepared()) {
                    return false;
                }
                if (!controlsHidden) {
                    hideControlsOverlay(true);
                    hideStateOverlay(true);
                }

                mSidePanelActionListener.openMenu();
                return true;

//            case KeyEvent.KEYCODE_DPAD_RIGHT:
//            case KeyEvent.KEYCODE_DPAD_CENTER:
//            case KeyEvent.KEYCODE_DPAD_DOWN:
//            case KeyEvent.KEYCODE_DPAD_UP:
//            case KeyEvent.KEYCODE_DPAD_LEFT:
//
//                // Event may be consumed; regardless, if controls are hidden then these keys will
//                // bring up the controls.
//                if (controlsHidden) {
//                    consumeEvent = true;
//                }
//                if (mShowOrHideControlsOverlayOnUserInteraction
//                        && keyAction == KeyEvent.ACTION_DOWN) {
//                    tickle();
//                }
//                break;
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_ESCAPE:
                // If controls are not hidden, back will be consumed to fade
                // them out (even if the key was consumed by the handler).

                if (Log.INCLUDE) {
                    Log.d(TAG, "");
                }

                if (mShowOrHideControlsOverlayOnUserInteraction && !controlsHidden) {
                    consumeEvent = true;

                    if (((KeyEvent) event).getAction() == KeyEvent.ACTION_UP) {
                        hideControlsOverlay(true);
                        hideStateOverlay(true);
                    }
                }
                break;
            default:
//                if (mShowOrHideControlsOverlayOnUserInteraction && consumeEvent) {
//                    if (keyAction == KeyEvent.ACTION_DOWN) {
//                        tickle();
//                    }
//                }
        }
        return consumeEvent;
    }

    void showStateOverlay(boolean show, boolean animation) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showStateOverlay " + show);
        }
        if (getView() == null) {
            mControlVisibleBeforeOnCreateView = show;
            return;
        }
        // force no animation when fragment is not resumed
        if (!isResumed()) {
            animation = false;
        }
        if (show == mStateViewVisible) {
            if (!animation) {
                // End animation if needed
//                endAll(mBgFadeInAnimator, mBgFadeOutAnimator);
                endAll(mOtherRowFadeInAnimator, mOtherRowFadeOutAnimator);
            }
            return;
        }
        // StateGraph: 7<->5 4<->3 2->3
        mStateViewVisible = show;
        if (!mStateViewVisible) {
            // StateGraph 2->3
            stopFadeTimer();
        }

        mAnimationTranslateY = (getVerticalGridView() == null
                || getVerticalGridView().getSelectedPosition() == 0)
                ? mMajorFadeTranslateY : mMinorFadeTranslateY;

        if (show) {
//            reverseFirstOrStartSecond(mBgFadeOutAnimator, mBgFadeInAnimator, animation);
            reverseFirstOrStartSecond(mOtherRowFadeOutAnimator, mOtherRowFadeInAnimator, animation);
        } else {
//            reverseFirstOrStartSecond(mBgFadeInAnimator, mBgFadeOutAnimator, animation);
            reverseFirstOrStartSecond(mOtherRowFadeInAnimator, mOtherRowFadeOutAnimator, animation);
        }
        if (animation) {
            getView().announceForAccessibility(getString(show
                    ? R.string.lb_playback_controls_shown
                    : R.string.lb_playback_controls_hidden));
        }
    }

    void showTrailerView(boolean show, boolean animation) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showControlsOverlay " + show);
        }
        if (!isResumed()) {
            animation = false;
        }
        resetFocus();
        if (show == mCurrentTrailerMode) {
            if (!animation) {
                // End animation if needed
                endAll(mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator);
                endAll(mControlRowFadeInAnimator, mControlRowFadeOutAnimator);
                endAll(mOtherRowFadeInAnimator, mOtherRowFadeOutAnimator);
            }
            return;
        }
        // StateGraph: 7<->5 4<->3 2->3
        mCurrentTrailerMode = show;
        if (!mCurrentTrailerMode) {
            // StateGraph 2->3
            stopFadeTimer();
        }

        if (show) {
            reverseFirstOrStartSecond(mTransportRowFadeOutAnimator, mTransportRowFadeInAnimator,
                    animation);
        } else {
            reverseFirstOrStartSecond(mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator,
                    animation);
        }
        if (animation) {
            getView().announceForAccessibility(getString(show
                    ? R.string.lb_playback_controls_shown
                    : R.string.lb_playback_controls_hidden));
        }
    }

    public void setSelectedPosition(int position) {
        setSelectedPosition(position, true);
    }

    public void setSelectedPosition(int position, boolean smooth) {
        mSetSelectionRunnable.mPosition = position;
        mSetSelectionRunnable.mSmooth = smooth;
        if (getView() != null && getView().getHandler() != null) {
            getView().getHandler().post(mSetSelectionRunnable);
        }
    }

    private void setupChildFragmentLayout() {
        setVerticalGridViewLayout(mRowsSupportFragment.getVerticalGridView());
    }

    void setVerticalGridViewLayout(VerticalGridView listview) {
        if (listview == null) {
            return;
        }

        // we set the base line of alignment to -paddingBottom
        listview.setWindowAlignmentOffset(-mPaddingBottom);
        listview.setWindowAlignmentOffsetPercent(
                VerticalGridView.WINDOW_ALIGN_OFFSET_PERCENT_DISABLED);

        // align other rows that arent the last to center of screen, since our baseline is
        // -mPaddingBottom, we need subtract that from mOtherRowsCenterToBottom.
        listview.setItemAlignmentOffset(mOtherRowsCenterToBottom - mPaddingBottom);
        listview.setItemAlignmentOffsetPercent(50);

        // Push last row to the bottom padding
        // Padding affects alignment when last row is focused
        listview.setPadding(listview.getPaddingLeft(), listview.getPaddingTop(),
                listview.getPaddingRight(), mPaddingBottom);
        listview.setWindowAlignment(VerticalGridView.WINDOW_ALIGN_HIGH_EDGE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mOtherRowsCenterToBottom = getResources()
                .getDimensionPixelSize(R.dimen.lb_playback_other_rows_center_to_bottom);
        mPaddingBottom =
                getResources().getDimensionPixelSize(R.dimen.lb_playback_controls_padding_bottom);
        mBgDarkColor =
                getResources().getColor(R.color.lb_playback_controls_background_dark);
        mBgLightColor =
                getResources().getColor(R.color.lb_playback_controls_background_light);
        TypedValue outValue = new TypedValue();
        getContext().getTheme().resolveAttribute(
                R.attr.playbackControlsAutoHideTimeout, outValue, true);
        mAutohideTimerAfterPlayingInMs = outValue.data;
        getContext().getTheme().resolveAttribute(
                R.attr.playbackControlsAutoHideTickleTimeout, outValue, true);
        mAutohideTimerAfterTickleInMs = 5000;
        mMajorFadeTranslateY =
                getResources().getDimensionPixelSize(R.dimen.lb_playback_major_fade_translate_y);
        mMinorFadeTranslateY =
                getResources().getDimensionPixelSize(R.dimen.lb_playback_minor_fade_translate_y);

        loadTransportAnimator();
        loadDescriptionAnimator();
        loadVideoStateAnimator();
    }

    /**
     * Sets the background type.
     *
     * @param type One of BG_LIGHT, BG_DARK, or BG_NONE.
     */
    public void setBackgroundType(int type) {
        switch (type) {
            case BG_LIGHT:
            case BG_DARK:
            case BG_NONE:
                if (type != mBackgroundType) {
                    mBackgroundType = type;
                    updateBackground();
                }
                break;
            default:
                throw new IllegalArgumentException("Invalid background type");
        }
    }

    protected boolean isPrepared() {
        return true;
    }

    protected boolean isPlaying() {
        return true;
    }

    /**
     * Returns the background type.
     */
    public int getBackgroundType() {
        return mBackgroundType;
    }

    private void updateBackground() {
        if (mBackgroundView != null) {
            int color = mBgDarkColor;
            switch (mBackgroundType) {
                case BG_DARK:
                    break;
                case BG_LIGHT:
                    color = mBgLightColor;
                    break;
                case BG_NONE:
                    color = Color.TRANSPARENT;
                    break;
            }
            mBackgroundView.setBackground(new ColorDrawable(color));
            setBgAlpha(mBgAlpha);
        }
    }

    void showControlsOverlay(boolean show, boolean animation) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showControlsOverlay " + show);
        }
        if (getView() == null) {
            mControlVisibleBeforeOnCreateView = show;
            return;
        }
        if (show && !mControlVisible) {
            resetFocus();
        } else {
            onHide();
        }
        // force no animation when fragment is not resumed
        if (!isResumed()) {
            animation = false;
        }

        if (show) {
            setIsEnableUseVideoSetting(checkEnableSidePanel());
        }
        if (show == mControlVisible) {
            if (!animation) {
                endAll(mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator);
                endAll(mControlRowFadeInAnimator, mControlRowFadeOutAnimator);
                endAll(mOtherRowFadeInAnimator, mOtherRowFadeOutAnimator);
            }
            return;
        }
        // StateGraph: 7<->5 4<->3 2->3
        mControlVisible = show;
        if (!mControlVisible) {
            // StateGraph 2->3
            stopFadeTimer();
        }

        mAnimationTranslateY = (getVerticalGridView() == null
                || getVerticalGridView().getSelectedPosition() == 0)
                ? mMajorFadeTranslateY : mMinorFadeTranslateY;

        if (show) {


//            reverseFirstOrStartSecond(mBgFadeOutAnimator, mBgFadeInAnimator, animation);
            reverseFirstOrStartSecond(mControlRowFadeOutAnimator, mControlRowFadeInAnimator,
                    animation);
        } else {
//            reverseFirstOrStartSecond(mBgFadeInAnimator, mBgFadeOutAnimator, animation);
            reverseFirstOrStartSecond(mControlRowFadeInAnimator, mControlRowFadeOutAnimator,
                    animation);
        }
        if (animation) {
            getView().announceForAccessibility(getString(show
                    ? R.string.lb_playback_controls_shown
                    : R.string.lb_playback_controls_hidden));
        }
    }

    View mBlackScreen;

    protected void setBlackScreenVisible(boolean visible) {
        mBlackScreen.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.layout_video_playback_fragment, container, false);
        mBackgroundView = mRootView.findViewById(R.id.playback_fragment_background);
        mIconView = mRootView.findViewById(R.id.playback_pause_icon);
        mBlackScreen = mRootView.findViewById(R.id.blackScreen);
        mRowsSupportFragment = (RowsSupportFragment) getChildFragmentManager().findFragmentById(
                R.id.playback_controls_dock);
        if (mRowsSupportFragment == null) {
            mRowsSupportFragment = new RowsSupportFragment();
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.playback_controls_dock, mRowsSupportFragment)
                    .commit();
        }
        if (mAdapter == null) {
            setAdapter(new ArrayObjectAdapter(new ClassPresenterSelector()));
        } else {
            mRowsSupportFragment.setAdapter(mAdapter);
        }
        mRowsSupportFragment.setOnItemViewSelectedListener(mOnItemViewSelectedListener);
        mRowsSupportFragment.setOnItemViewClickedListener(mOnItemViewClickedListener);

        mBgAlpha = 255;
        updateBackground();
        mRowsSupportFragment.setExternalAdapterListener(mAdapterListener);
        return mRootView;
    }

    /**
     * Sets the {@link VideoPlaybackGlueHost.HostCallback}. Implementor of this interface will
     * take appropriate actions to take action when the hosting fragment starts/stops processing.
     */
    public void setHostCallback(VideoPlaybackGlueHost.HostCallback hostCallback) {
        this.mHostCallback = hostCallback;
    }

    @Override
    public void onStart() {
        super.onStart();
        setupChildFragmentLayout();
        mRowsSupportFragment.setAdapter(mAdapter);
        if (mHostCallback != null) {
            mHostCallback.onHostStart();
        }
    }

    @Override
    public void onStop() {
        if (mHostCallback != null) {
            mHostCallback.onHostStop();
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (mHostCallback != null) {
            mHostCallback.onHostPause();
        }
//        if (mHandler.hasMessages(START_FADE_OUT)) {
//            // StateGraph: 2->1
//            mHandler.removeMessages(START_FADE_OUT);
//        } else {
//            // StateGraph: 5->6, 7->6, 4->1, 3->1
//        }
        super.onPause();
    }

    /**
     * This listener is called every time there is a selection in {@link RowsSupportFragment}. This can
     * be used by users to take additional actions such as animations.
     */
    public void setOnItemViewSelectedListener(final BaseOnItemViewSelectedListener listener) {
        mExternalItemSelectedListener = listener;
    }

    /**
     * This listener is called every time there is a click in {@link RowsSupportFragment}. This can
     * be used by users to take additional actions such as animations.
     */
    public void setOnItemViewClickedListener(final BaseOnItemViewClickedListener listener) {
        mExternalItemClickedListener = listener;
    }

    public void setOnPlaybackItemViewClickedListener(final BaseOnItemViewClickedListener listener) {
        mPlaybackItemClickedListener = listener;
    }

    @Override
    public void onDestroyView() {
        mRootView = null;
        mBackgroundView = null;
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (mHostCallback != null) {
            mHostCallback.onHostDestroy();
        }
        super.onDestroy();
    }

    public void setPlaybackRow(Row row) {
        this.mRow = row;
        setupRow();
        setupPresenter();
    }

    public void setVideoPlaybackRowPresenter(VideoPlaybackRowPresenter presenter) {
        this.mPresenter = presenter;
        setupPresenter();
        setPlaybackRowPresenterAlignment();
    }

    public void setPlaybackRowPresenterAlignment() {

        if (mAdapter != null && mAdapter.getPresenterSelector() != null) {
            Presenter[] presenters = mAdapter.getPresenterSelector().getPresenters();
            if (presenters != null) {
                for (int i = 0; i < presenters.length; i++) {
                    if (presenters[i] instanceof VideoPlaybackRowPresenter
                            && presenters[i].getFacet(ItemAlignmentFacet.class) == null) {
                        ItemAlignmentFacet itemAlignment = new ItemAlignmentFacet();
                        ItemAlignmentFacet.ItemAlignmentDef def =
                                new ItemAlignmentFacet.ItemAlignmentDef();
                        def.setItemAlignmentOffset(0);
                        def.setItemAlignmentOffsetPercent(100);
                        itemAlignment.setAlignmentDefs(new ItemAlignmentFacet.ItemAlignmentDef[]
                                {def});
                        presenters[i].setFacet(ItemAlignmentFacet.class, itemAlignment);
                    }
                }
            }
        }
    }

    /**
     * Updates the ui when the row data changes.
     */
    public void notifyPlaybackRowChanged() {
        if (mAdapter == null) {
            return;
        }
        mAdapter.notifyItemRangeChanged(0, 1);
    }

    public void setAdapter(ObjectAdapter adapter) {
        mAdapter = adapter;
        setupRow();
        setupPresenter();
        setPlaybackRowPresenterAlignment();

        if (mRowsSupportFragment != null) {
            mRowsSupportFragment.setAdapter(adapter);
        }
    }

    private void setupRow() {
        if (mAdapter instanceof ArrayObjectAdapter && mRow != null) {
            ArrayObjectAdapter adapter = ((ArrayObjectAdapter) mAdapter);
            if (adapter.size() == 0) {
                adapter.add(mRow);
            } else {
                adapter.replace(0, mRow);
            }
        } else if (mAdapter instanceof SparseArrayObjectAdapter && mRow != null) {
            SparseArrayObjectAdapter adapter = ((SparseArrayObjectAdapter) mAdapter);
            adapter.set(0, mRow);
        }
    }

    private void setupPresenter() {
        if (mAdapter != null && mRow != null && mPresenter != null) {
            PresenterSelector selector = mAdapter.getPresenterSelector();
            if (selector == null) {
                selector = new ClassPresenterSelector();
                ((ClassPresenterSelector) selector).addClassPresenter(mRow.getClass(), mPresenter);
                mAdapter.setPresenterSelector(selector);
            } else if (selector instanceof ClassPresenterSelector) {
                ((ClassPresenterSelector) selector).addClassPresenter(mRow.getClass(), mPresenter);
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // controls view are initially visible, make it invisible
        // if app has called hideControlsOverlay() before view created.
        mControlVisible = true;
        mStateViewVisible = true;
        mCurrentTrailerMode = false;
        if (!mControlVisibleBeforeOnCreateView) {
            showControlsOverlay(false, false);
            mControlVisibleBeforeOnCreateView = true;
        }
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    /**
     * Interface to be implemented by UI widget to support PlaybackSeekUi.
     */
    public void setPlaybackSeekUiClient(VideoPlaybackSeekUi.Client client) {
        mSeekUiClient = client;
    }

    public void setTrailerUiClient(TrailerSelectUi.Client client) {
        mVideoSelectUiClient = client;
    }

    public boolean canUsingChangeMode() {
        return true;
    }

    /**
     * Show or hide other rows other than PlaybackRow.
     *
     * @param inSeek True to make other rows visible, false to make other rows invisible.
     */
    void setSeekMode(boolean inSeek) {
        if (mInSeek == inSeek) {
            return;
        }
        mInSeek = inSeek;
        getVerticalGridView().setSelectedPosition(0);
        if (mInSeek) {
            stopFadeTimer();
        }
        // immediately fade in control row.
        showControlsOverlay(true);
        final int count = getVerticalGridView().getChildCount();
        for (int i = 0; i < count; i++) {
            View view = getVerticalGridView().getChildAt(i);
            if (getVerticalGridView().getChildAdapterPosition(view) > 0) {
                view.setVisibility(mInSeek ? View.INVISIBLE : View.VISIBLE);
            }
        }
    }

    /**
     * Called when size of the video changes. App may override.
     *
     * @param videoWidth  Intrinsic width of video
     * @param videoHeight Intrinsic height of video
     */
    public void onVideoSizeChanged(int videoWidth, int videoHeight) {
    }

    /**
     * Called when media has start or stop buffering. App may override. The default initial state
     * is not buffering.
     *
     * @param start True for buffering start, false otherwise.
     */
    public void onBufferingStateChanged(boolean start) {
        ProgressBarManager progressBarManager = getProgressBarManager();
        if (progressBarManager != null) {
            if (start) {
                progressBarManager.show();
            } else {
                progressBarManager.hide();
            }
        }
    }

    /**
     * Called when media has error. App may override.
     *
     * @param errorCode    Optional error code for specific implementation.
     * @param errorMessage Optional error message for specific implementation.
     */
    public void onError(int errorCode, CharSequence errorMessage) {
    }

    /**
     * Returns the ProgressBarManager that will show or hide progress bar in
     * {@link #onBufferingStateChanged(boolean)}.
     *
     * @return The ProgressBarManager that will show or hide progress bar in
     * {@link #onBufferingStateChanged(boolean)}.
     */
    public ProgressBarManager getProgressBarManager() {
        return mProgressBarManager;
    }

    public boolean isIsEnableUseVideoSetting() {
        return mIsEnableUseVideoSetting;
    }

    public void setIsEnableUseVideoSetting(boolean mIsEnableUseVideoSetting) {
        this.mIsEnableUseVideoSetting = mIsEnableUseVideoSetting;
        if (getVerticalGridView() == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "setIsEnableUseVideoSetting | getVerticalGridView() == null");
            }
            return;
        }

        ItemBridgeAdapter.ViewHolder vh = (ItemBridgeAdapter.ViewHolder) getVerticalGridView()
                .findViewHolderForAdapterPosition(0);
        if (vh != null && vh.getPresenter() instanceof VideoPlaybackRowPresenter) {
            ((VideoPlaybackRowPresenter) vh.getPresenter()).onOptionEnabled(
                    (RowPresenter.ViewHolder) vh.getViewHolder(), mIsEnableUseVideoSetting);
        }
    }
}
