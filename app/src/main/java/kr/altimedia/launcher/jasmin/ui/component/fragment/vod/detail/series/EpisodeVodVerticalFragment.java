/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildSelectedListener;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonListInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContentInfo;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodRatingPinDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodRatingPinFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter.EpisodeVodPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.listenter.OnVodViewChangeListener;
import kr.altimedia.launcher.jasmin.ui.util.task.VodTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.BaseVerticalRowFragment;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class EpisodeVodVerticalFragment extends BaseVerticalRowFragment {
    private static final String TAG = EpisodeVodVerticalFragment.class.getSimpleName();
    private static final String KEY_SERIES_ASSET_ID = "SERIES_ASSET_ID";
    private static final String KEY_CATEGORY_ID = "CATEGORY_ID";
    private static final String KEY_CONTENT_ID = "CONTENT_ID";
    private static final String KEY_SEASON_INFO = "SEASON_INFO"; // history for push update
    private static final String KEY_HISTORY_VIEW_ID = "HISTORY_VIEW_ID"; // history for push update, season change

    // 1 page focus
    private final int CENTER_FOCUS_LIST_SIZE = 3;
    // more than 1 page focus
    private final int FIX_POSITION = 1;

    private String seriesAssetId;
    private String categoryId;
    private String contentId;

    private VodTaskManager vodTaskManager;
    private List<SeriesContent> episodeList = new ArrayList();
    private SeasonListInfo seasonListInfo;

    private SeriesVodFragment parentFragment;
    private OnVodViewChangeListener mChangeVodDetailListener = null;

    private TextView seasonButton;
    private View historyView;

    private final EpisodeItemBridgeAdapter.OnFocusChangeListener onFocusChangeListener = new EpisodeItemBridgeAdapter.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus, SeriesContent seriesContent) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onFocusChange, hasFocus : " + hasFocus + ", seriesContent : " + seriesContent);
            }

            if (parentFragment != null) {
                parentFragment.replaceEpisodeDetail(seriesContent.getContentGroupId(), seriesContent.getCategoryId());
            }
        }

        @Override
        public void onClickEpisode(SeriesContent mSeriesContent) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onClickEpisode, mSeriesContent : " + mSeriesContent);
            }

            if (parentFragment != null) {
                Content mContent = parentFragment.getContent();
                if (mContent == null) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onClickEpisode, vod detail is loading... so ignore click");
                    }
                    return;
                }

                boolean isWatchable = mContent.isWatchable();
                if (Log.INCLUDE) {
                    Log.d(TAG, "onClickEpisode, isWatchable : " + isWatchable + ", mContent : " + mContent);
                }

                if (mChangeVodDetailListener != null) {
                    if (isWatchable) {
                        mChangeVodDetailListener.watchMainVideo(mContent);
                    } else {
                        mChangeVodDetailListener.watchPreviewVideo(mSeriesContent);
                    }
                }
            }
        }
    };

    public EpisodeVodVerticalFragment() {
    }

    public static EpisodeVodVerticalFragment newInstance(String seriesAssetId, String categoryId, String contentId) {
        Bundle args = new Bundle();
        args.putString(KEY_SERIES_ASSET_ID, seriesAssetId);
        args.putString(KEY_CATEGORY_ID, categoryId);
        args.putString(KEY_CONTENT_ID, contentId);
        args.putInt(KEY_HISTORY_VIEW_ID, -1);

        EpisodeVodVerticalFragment fragment = new EpisodeVodVerticalFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public EpisodeVodVerticalFragment getSeasonInstance(String seriesAssetId) {
        Bundle args = new Bundle();
        args.putString(KEY_SERIES_ASSET_ID, seriesAssetId);
        args.putString(KEY_CATEGORY_ID, categoryId);
        args.putInt(KEY_HISTORY_VIEW_ID, historyView != null ? historyView.getId() : -1);

        EpisodeVodVerticalFragment fragment = new EpisodeVodVerticalFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public EpisodeVodVerticalFragment getPushInstance() {
        Bundle args = new Bundle();
        args.putString(KEY_SERIES_ASSET_ID, seriesAssetId);
        args.putString(KEY_CATEGORY_ID, categoryId);
        args.putString(KEY_CONTENT_ID, getEpisodeFocusedId());
        args.putParcelable(KEY_SEASON_INFO, seasonListInfo);
        args.putInt(KEY_HISTORY_VIEW_ID, historyView != null ? historyView.getId() : -1);

        EpisodeVodVerticalFragment fragment = new EpisodeVodVerticalFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(OnVodViewChangeListener mChangeVodDetailListener) {
        this.mChangeVodDetailListener = mChangeVodDetailListener;
    }

    public void setVodTaskManager(VodTaskManager vodTaskManager) {
        this.vodTaskManager = vodTaskManager;
    }

    public String getMasterSeriesTitle() {
        return seasonListInfo != null ? seasonListInfo.getMasterSeriesTitle() : "";
    }

    public String getEpisodeFocusedId() {
        if (mVerticalGridView == null) {
            return "";
        }

        int selectedPosition = mVerticalGridView.getSelectedPosition();
        if (selectedPosition >= episodeList.size()) {
            return "";
        }

        SeriesContent mSeriesContent = episodeList.get(selectedPosition);
        return mSeriesContent.getContentGroupId();
    }

    public boolean hasFocus() {
        return seasonButton != null && seasonButton.hasFocus() || mVerticalGridView != null && mVerticalGridView.hasFocus();
    }

    public void requestFocus() {
        if (mVerticalGridView != null) {
            mVerticalGridView.requestFocus();
        }
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.episode_vod_detail_fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        seasonButton = view.findViewById(R.id.season_button);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle mBundle = getArguments();
        seriesAssetId = mBundle.getString(KEY_SERIES_ASSET_ID);
        categoryId = mBundle.getString(KEY_CATEGORY_ID);
        contentId = mBundle.getString(KEY_CONTENT_ID);
        historyView = mBundle.getInt(KEY_HISTORY_VIEW_ID) == R.id.season_button ? seasonButton : null;
        parentFragment = (SeriesVodFragment) getParentFragment();

        initLayout(view);
        loadSeasonList(seriesAssetId, categoryId);
        loadEpisodeListRow(seriesAssetId, contentId);
    }

    private void initLayout(View view) {
        BrowseFrameLayout layout = view.findViewById(R.id.layout);
        layout.setOnChildFocusListener(new BrowseFrameLayout.OnChildFocusListener() {
            @Override
            public boolean onRequestFocusInDescendants(int var1, Rect var2) {
                return false;
            }

            @Override
            public void onRequestChildFocus(View child, View focused) {
                historyView = focused;
            }
        });
    }

    private void initSeasonButton(SeasonListInfo seasonListInfo) {
        if (Log.INCLUDE) {
            Log.d(TAG, "initSeasonButton, seasonListInfo : " + seasonListInfo);
        }

        if (seasonListInfo == null) {
            return;
        }

        ArrayList<SeasonInfo> seasonList = (ArrayList<SeasonInfo>) seasonListInfo.getSeasonList();
        if (seasonList == null || seasonList.size() <= 1) {
            return;
        }

        String season = getContext().getString(R.string.season);
        seasonButton.setText(season + " " + seasonListInfo.getSeasonId());
        seasonButton.setVisibility(View.VISIBLE);
    }

    private void setKeySeasonButton() {
        seasonButton.setFocusable(true);
        seasonButton.setFocusableInTouchMode(true);
        seasonButton.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                switch (keyCode) {
                    case KeyEvent.KEYCODE_ENTER:
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        VodSeasonSelectDialogFragment seasonSelectDialogFragment = VodSeasonSelectDialogFragment.newInstance(seasonListInfo);
                        parentFragment.getTvOverlayManager().showDialogFragment(seasonSelectDialogFragment);
                        return true;
                    case KeyEvent.KEYCODE_DPAD_UP:
                        if (mVerticalGridView != null) {
                            View view = mVerticalGridView.getChildAt(mVerticalGridView.getSelectedPosition());
                            EpisodeItemBridgeAdapter.scalePoster(view, false);

                            mVerticalGridView.scrollToPosition(episodeList.size() - 1);
                            mVerticalGridView.requestFocus();
                            return true;
                        }
                        break;
                    case KeyEvent.KEYCODE_DPAD_LEFT:
                        return true;
                    case KeyEvent.KEYCODE_DPAD_DOWN:
                        mVerticalGridView.requestFocus();
                        return true;
                }

                return false;
            }
        });
    }

    private void initGridView(int size) {
        if (Log.INCLUDE) {
            Log.d(TAG, "initGridView, size : " + size);
        }

        int gap = (int) getResources().getDimension(R.dimen.episode_vod_gap);
        mVerticalGridView.setVerticalSpacing(gap);

        if (size <= CENTER_FOCUS_LIST_SIZE) {
            mVerticalGridView.setWindowAlignmentOffsetPercent(50);
            mVerticalGridView.setWindowAlignmentOffset(0);
            return;
        }

        int height = (int) (getResources().getDimension(R.dimen.episode_vod_detail_scale_height) + gap);
        int gridHeight = height * 2;
        int paddingBottom = mVerticalGridView.getMeasuredHeight() - gridHeight;

        mVerticalGridView.setPadding(0, 0, 0, paddingBottom);
        mVerticalGridView.setItemAlignmentOffsetPercent(0);
        mVerticalGridView.setItemAlignmentOffset(0);
        mVerticalGridView.setClipToPadding(false);

        mVerticalGridView.setOnChildSelectedListener(new OnChildSelectedListener() {
            @Override
            public void onChildSelected(ViewGroup parent, View view, int position, long id) {
                if (size <= CENTER_FOCUS_LIST_SIZE) {
                    return;
                }

                setOffset(view, position);
            }
        });
    }

    private void setOffset(View view, int index) {
        if (view == null || index < 0) {
            return;
        }

        int height = (int) (getResources().getDimension(R.dimen.episode_vod_detail_scale_height) + mVerticalGridView.getVerticalSpacing());
        float offset = index < FIX_POSITION ? (height * index) : (height * FIX_POSITION);

        mVerticalGridView.setWindowAlignmentOffsetPercent(0);
        mVerticalGridView.setWindowAlignmentOffset((int) offset);
    }

    private void loadSeasonList(String seriesAssetId, String categoryId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "loadSeasonList, seriesAssetId : " + seriesAssetId + ", categoryId : " + categoryId);
        }

        SeasonListInfo mSeasonListInfo = getArguments().getParcelable(KEY_SEASON_INFO);
        if (Log.INCLUDE) {
            Log.d(TAG, "loadSeasonList, mSeasonListInfo : " + mSeasonListInfo);
        }

        if (mSeasonListInfo != null) {
            this.seasonListInfo = mSeasonListInfo;
            initSeasonButton(seasonListInfo);
            return;
        }

        vodTaskManager.setOnCompleteLoadSeasonListListener(new VodTaskManager.OnCompleteLoadSeasonListListener<String, SeasonListInfo>() {
            @Override
            public void needLoading(boolean isLoading) {
                if (isLoading) {
                    parentFragment.showProgress();
                } else {
                    parentFragment.hideProgress();
                }
            }

            @Override
            public void onSuccessLoadSeasonList(String id, SeasonListInfo result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onSuccessLoadSeasonList, result : " + result);
                }

                seasonListInfo = result;
                initSeasonButton(seasonListInfo);
            }

            @Override
            public void onFailLoadSeasonList(int key) {
                parentFragment.showLoadFailDialog(false, key);
            }

            @Override
            public void onError(String id, String errorCode, String message) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "loadSeasonList, onError");
                }

                parentFragment.showLoadErrorDialog(false, errorCode, message);
            }
        });

        if (Log.INCLUDE) {
            Log.d(TAG, "loadSeasonList, seriesAssetId : " + seriesAssetId);
        }
        vodTaskManager.loadSeasonList(seriesAssetId, categoryId);
    }

    private void loadEpisodeListRow(String seriesAssetId, String contentId) {
        vodTaskManager.setOnCompleteLoadEpisodeListener(new VodTaskManager.OnCompleteLoadEpisodeListener<String, SeriesContentInfo>() {
            @Override
            public void needLoading(boolean isLoading) {
                if (isLoading) {
                    parentFragment.showProgress();
                } else {
                    parentFragment.hideProgress();
                }
            }

            @Override
            public void onSuccessLoadEpisode(String id, SeriesContentInfo result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "loadEpisodeListRow, onSuccessLoadEpisode, seriesAssetId : " + seriesAssetId);
                }

                String focusedId = result.getFocusedEpisode();
                SeriesContentInfo seriesContentInfo = result;
                List<SeriesContent> list = seriesContentInfo.getSeriesList();

                initGridView(list.size());
                boolean isOverRating = PlaybackUtil.isVodOverRating(seriesContentInfo.getRating());
                if (isOverRating) {
                    showPinDialog(list, focusedId);
                } else {
                    showEpisodeList(list, focusedId);
                }

                parentFragment.hideProgress();
            }

            @Override
            public void onFailLoadEpisode(int key) {
                parentFragment.hideProgress();
                parentFragment.showLoadFailDialog(true, key);
            }

            @Override
            public void onError(String id, String errorCode, String message) {
                parentFragment.hideProgress();
                parentFragment.showLoadErrorDialog(true, errorCode, message);
            }
        });

        if (Log.INCLUDE) {
            Log.d(TAG, "loadEpisodeListRow, seriesAssetId : " + seriesAssetId);
        }
        vodTaskManager.loadEpisodeList(seriesAssetId, categoryId, contentId);
    }

    private void showPinDialog(List<SeriesContent> list, String focusedId) {
        TvOverlayManager mTvOverlayManager = parentFragment.getTvOverlayManager();
        VodRatingPinDialogFragment mVodRatingPinDialogFragment = VodRatingPinDialogFragment.newInstance();
        mVodRatingPinDialogFragment.setOnCompleteCheckPinListener(new VodRatingPinFragment.onCompleteCheckPinListener() {
            @Override
            public void setProgress(boolean isShow) {
                if (isShow) {
                    parentFragment.showProgress();
                } else {
                    parentFragment.hideProgress();
                }
            }

            @Override
            public void onCompleteCheckPin(boolean isSuccess) {
                if (!isSuccess) {
                    mTvOverlayManager.dismissOverlayDialog(
                            (SafeDismissDialogFragment) parentFragment.getParentFragment());
                }

                mVodRatingPinDialogFragment.dismiss();
            }
        });

        mTvOverlayManager.showDialogFragment(mVodRatingPinDialogFragment);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                showEpisodeList(list, focusedId);
            }
        });

    }

    private void showEpisodeList(List<SeriesContent> list, String focusedId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showEpisodeList, focusedId : " + focusedId + ", list : " + list);
        }

        if (list == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showEpisodeList, list == null, so return");
            }
            return;
        }

        ArrayObjectAdapter objectAdapter = getEpisodeRowAdapter(list);
        EpisodeItemBridgeAdapter itemBridgeAdapter =
                new EpisodeItemBridgeAdapter(objectAdapter, seasonButton, mVerticalGridView, mChangeVodDetailListener, onFocusChangeListener);

        mVerticalGridView.setAdapter(itemBridgeAdapter);

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                boolean isChangedSeason = false;
                int historyViewId = getArguments().getInt(KEY_HISTORY_VIEW_ID);
                if (historyViewId != -1) {
                    if (contentId == null || contentId.isEmpty()) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "showEpisodeList, contentId is replaced to list(0)");
                        }
                        isChangedSeason = true;
                        contentId = list.get(0).getContentGroupId();

                        scrollEpisode(contentId);
                        mVerticalGridView.requestFocus();
                    } else {
                        scrollAndFocusEpisode(contentId);
                    }
                } else {
                    scrollAndFocusEpisode(focusedId);
                }

                if (seasonButton.getVisibility() == View.VISIBLE) {
                    setKeySeasonButton();

                    if (isChangedSeason) {
                        seasonButton.requestFocus();
                    }
                }
            }
        });
    }

    private ArrayObjectAdapter getEpisodeRowAdapter(List<SeriesContent> list) {
        episodeList = list;

        EpisodeVodPresenter episodeVodPresenter = new EpisodeVodPresenter(mChangeVodDetailListener);
        ArrayObjectAdapter rowsAdapter = new ArrayObjectAdapter(episodeVodPresenter);
        rowsAdapter.addAll(0, episodeList);

        return rowsAdapter;
    }

    private static boolean mEnable = false;

    public void setEnable(boolean enable) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setEnable | current state : " + mEnable + ", enable : " + enable);
        }
        if (mEnable != enable) {
            mEnable = enable;
            if (Log.INCLUDE) {
                Log.d(TAG, "setEnable | notifyDataSetChanged");
            }

            RecyclerView.Adapter adapter = mVerticalGridView.getAdapter();
            if (!(adapter instanceof EpisodeItemBridgeAdapter)) {
                return;
            }

            setListEnable();
        }
    }

    private void setListEnable() {
        EpisodeItemBridgeAdapter itemBridgeAdapter = (EpisodeItemBridgeAdapter) mVerticalGridView.getAdapter();
        String focusedContentId = itemBridgeAdapter.getFocusedContentId();

        for (int i = 0; i < mVerticalGridView.getChildCount(); i++) {
            View view = mVerticalGridView.getChildAt(i);
            ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) mVerticalGridView.getChildViewHolder(view);
            SeriesContent seriesContent = (SeriesContent) viewHolder.getItem();

            String checkedContentId = seriesContent.getContentGroupId();
            float alpha = mEnable || focusedContentId.equalsIgnoreCase(checkedContentId) ? 1.0f : 0.5f;
            view.setAlpha(alpha);
        }
    }

    public boolean scrollEpisode(String contentId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "scrollEpisode, contentId : " + contentId);
        }

        EpisodeItemBridgeAdapter itemBridgeAdapter = (EpisodeItemBridgeAdapter) mVerticalGridView.getAdapter();

        String selectedContentId = itemBridgeAdapter.getFocusedContentId();
        if (selectedContentId != null && selectedContentId.equals(contentId)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "scrollEpisode, contentId : " + contentId + " is already selected");
            }

            return false;
        }

        ArrayObjectAdapter rowAdapter = (ArrayObjectAdapter) itemBridgeAdapter.getAdapter();
        for (int i = 0; i < rowAdapter.size(); i++) {
            SeriesContent seriesContent = (SeriesContent) rowAdapter.get(i);
            if (seriesContent.getContentGroupId().equalsIgnoreCase(contentId)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "scrollEpisode, index : " + i);
                }

                mVerticalGridView.scrollToPosition(i);
                return true;
            }
        }

        return false;
    }

    public boolean scrollAndFocusEpisode(String contentId) {
        EpisodeItemBridgeAdapter mItemBridgeAdapter = (EpisodeItemBridgeAdapter) mVerticalGridView.getAdapter();
        String focusedContentId = mItemBridgeAdapter.getFocusedContentId();

        if (Log.INCLUDE) {
            Log.d(TAG, "scrollAndFocusEpisode, contentId : " + contentId + ", focusedContentId : " + focusedContentId);
        }

        if (focusedContentId != null && focusedContentId.equalsIgnoreCase(contentId)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "scrollAndFocusEpisode, request focus to focusedContentId");
            }
            return false;
        }

        mItemBridgeAdapter.setFocusedContentId("");
        scrollEpisode(contentId);

        if (!mVerticalGridView.hasFocus()) {
            mItemBridgeAdapter.notifyDataSetChanged();
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mVerticalGridView.requestFocus();
                }
            });
        }

        return false;
    }

    public void updateThumbnailWatchable(Content content) {
        ItemBridgeAdapter itemBridgeAdapter = (ItemBridgeAdapter) mVerticalGridView.getAdapter();
        ArrayObjectAdapter rowsAdapter = (ArrayObjectAdapter) itemBridgeAdapter.getAdapter();
        if (rowsAdapter == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "updateThumbnailWatchable, EpisodeVodVerticalFragment is loading... so ignore");
            }
            return;
        }

        boolean isWatchable = content.isWatchable();
        int selectedPosition = mVerticalGridView.getSelectedPosition();
        SeriesContent mSeriesContent = episodeList.get(selectedPosition);

        if (mSeriesContent.isWatchable() != isWatchable) {
            if (Log.INCLUDE) {
                Log.d(TAG, "updateThumbnailWatchable, content : " + content);
            }

            mSeriesContent.setWatchable(isWatchable);
            rowsAdapter.replace(selectedPosition, mSeriesContent);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mChangeVodDetailListener = null;
    }

    private static class EpisodeItemBridgeAdapter extends ItemBridgeAdapter {
        private String mFocusedId;

        private RelativeLayout layout;
        private VerticalGridView mVerticalGridView;
        private TextView seasonButton;
        private OnVodViewChangeListener mChangeVodDetailListener;
        private OnFocusChangeListener onFocusChangeListener;

        public EpisodeItemBridgeAdapter(ArrayObjectAdapter arrayObjectAdapter, TextView seasonButton, VerticalGridView mVerticalGirdView,
                                        OnVodViewChangeListener mChangeVodDetailListener, OnFocusChangeListener onFocusChangeListener) {
            super(arrayObjectAdapter);

            this.seasonButton = seasonButton;
            this.mVerticalGridView = mVerticalGirdView;
            this.mChangeVodDetailListener = mChangeVodDetailListener;
            this.onFocusChangeListener = onFocusChangeListener;
        }

        public String getFocusedContentId() {
            return mFocusedId;
        }

        public void setFocusedContentId(String contentId) {
            if (Log.INCLUDE) {
                Log.d(TAG, "setFocusedContentId, contentId : " + contentId);
            }

            this.mFocusedId = contentId;
        }

        @Override
        protected void onBind(ViewHolder viewHolder) {
            super.onBind(viewHolder);

            EpisodeVodPresenter.SeriesVodPresenterViewHolder vh = (EpisodeVodPresenter.SeriesVodPresenterViewHolder) viewHolder.getViewHolder();
            View view = vh.view;
            layout = view.findViewById(R.id.layout);
            SeriesContent seriesContent = (SeriesContent) viewHolder.getItem();
            initListener(layout, seriesContent);
        }

        @Override
        protected void onUnbind(ViewHolder viewHolder) {
            super.onUnbind(viewHolder);

            EpisodeVodPresenter.SeriesVodPresenterViewHolder vh = (EpisodeVodPresenter.SeriesVodPresenterViewHolder) viewHolder.getViewHolder();
            View view = vh.view;
            layout = view.findViewById(R.id.layout);

            scalePoster(layout, false);
            layout.setOnFocusChangeListener(null);
            layout.setOnKeyListener(null);
        }

        private void initListener(View view, SeriesContent seriesContent) {
            view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                String contentId = seriesContent.getContentGroupId();

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "hasFocus : " + hasFocus + ", mFocusedId : " + mFocusedId + ", contentID : " + contentId);
                    }

                    if (contentId.equalsIgnoreCase(mFocusedId)) {
                        return;
                    }

                    if (hasFocus) {
                        setFocusedContentId(contentId);
                        onFocusChangeListener.onFocusChange(v, hasFocus, seriesContent);
                    }

                    scalePoster(v, hasFocus);
                }
            });

            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    int size = getAdapter().size();
                    int lastIndex = size - 1;
                    int index = mVerticalGridView.getChildPosition(mVerticalGridView.getFocusedChild());

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                            if (onFocusChangeListener != null) {
                                onFocusChangeListener.onClickEpisode(seriesContent);
                            }
                            return true;
                        case KeyEvent.KEYCODE_DPAD_UP:
                            if (index == 0) {
                                if (seasonButton.getVisibility() == View.VISIBLE) {
                                    seasonButton.requestFocus();
                                } else {
                                    scalePoster(view, false);
                                    mVerticalGridView.scrollToPosition(lastIndex);
                                }

                                return true;
                            }

                            //위, 아래 스크롤시 포스터 사이즈 리턴
                            //onFocusChangeListener 에서 처리하기에는 detail 시나리오와 구분할 수 있는 정보가 없음.
                            scalePoster(view, false);
                            return false;
                        case KeyEvent.KEYCODE_DPAD_DOWN:
                            if (size == 1) {
                                return true;
                            }

                            scalePoster(view, false);

                            if (index == lastIndex) {
                                mVerticalGridView.scrollToPosition(0);
                                return true;
                            }

                            return false;
                    }

                    return false;
                }
            });
        }

        public static void scalePoster(View view, boolean hasFocus) {
            View layout = view.findViewById(R.id.layout);
            Animator animator = loadAnimator(view.getContext(), hasFocus ? R.animator.scale_up_episode_vod : R.animator.scale_down_50);
            animator.setTarget(layout);
            animator.start();
        }

        private static Animator loadAnimator(Context context, int resId) {
            Animator animator = AnimatorInflater.loadAnimator(context, resId);
            return animator;
        }

        public interface OnFocusChangeListener {
            void onFocusChange(View v, boolean hasFocus, SeriesContent seriesContent);

            void onClickEpisode(SeriesContent mSeriesContent);
        }
    }
}
