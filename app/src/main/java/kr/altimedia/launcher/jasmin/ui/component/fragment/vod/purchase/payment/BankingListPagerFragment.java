package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Bank;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPaymentBankingFragment;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentCreditCardFragment.LIST_NUMBER_COLUMNS;

public class BankingListPagerFragment extends Fragment {
    public static final String KEY_PAGE_LIST = "KEY_PAGE_LIST";

    private final int NUMBER_ROWS = VodPaymentBankingFragment.NUMBER_ROWS;
    public static int INIT_POSITION = 0;

    private ArrayList<Bank> bankList;
    private HorizontalGridView gridView;

    public static BankingListPagerFragment newInstance(ArrayList<Bank> list) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_PAGE_LIST, list);

        BankingListPagerFragment fragment = new BankingListPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static void setSelectedPosition(int selectedPosition) {
        INIT_POSITION = selectedPosition;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_banking_pager, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        bankList = getArguments().getParcelableArrayList(KEY_PAGE_LIST);
        initBankGridView(view);
    }

    private void initBankGridView(View view) {
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(new BankingPresenter());
        objectAdapter.addAll(0, bankList);

        gridView = view.findViewById(R.id.gridView);
        gridView.setNumRows(NUMBER_ROWS);

        ItemBridgeAdapter bridgeAdapter = new ItemBridgeAdapter(objectAdapter);
        gridView.setAdapter(bridgeAdapter);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                gridView.setSelectedPosition(INIT_POSITION);
            }
        });
    }

    public int getSize() {
        return bankList.size();
    }

    public int getFocusedPosition() {
        return gridView.getChildAdapterPosition(gridView.getFocusedChild());
    }

    public Bank getItem(int position) {
        return bankList.get(position);
    }

    public void requestPosition(int position) {
        if (gridView != null) {
            gridView.setSelectedPosition(position);
        }
    }

    public boolean isLastColumn(int position) {
        int size = bankList.size();
        int lastColumn = size / LIST_NUMBER_COLUMNS;
        if (size % LIST_NUMBER_COLUMNS > 0) {
            lastColumn++;
        }

        position += 1;
        int row = position / LIST_NUMBER_COLUMNS;
        if (position % NUMBER_ROWS > 0) {
            row++;
        }

        return row == lastColumn;
    }
}
