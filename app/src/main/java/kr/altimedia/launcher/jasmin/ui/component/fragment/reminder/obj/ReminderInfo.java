/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.tvmodule.dao.ProgramImpl;

public class ReminderInfo implements Parcelable {
    private long channelId;
    private ProgramImpl program;

    public ReminderInfo(long channelId, ProgramImpl program) {
        this.channelId = channelId;
        this.program = program;
    }

    protected ReminderInfo(Parcel in) {
        channelId = in.readLong();
        program = in.readParcelable(ProgramImpl.class.getClassLoader());
    }

    public static final Creator<ReminderInfo> CREATOR = new Creator<ReminderInfo>() {
        @Override
        public ReminderInfo createFromParcel(Parcel in) {
            return new ReminderInfo(in);
        }

        @Override
        public ReminderInfo[] newArray(int size) {
            return new ReminderInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(channelId);
        dest.writeParcelable(program, flags);
    }

    public long getChannelId() {
        return channelId;
    }

    public ProgramImpl getProgram() {
        return program;
    }
}
