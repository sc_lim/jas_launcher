/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscribedContent;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public class SubscriptionDetailDialog extends SafeDismissDialogFragment implements View.OnUnhandledKeyEventListener{
    public static final String CLASS_NAME = SubscriptionDetailDialog.class.getName();
    private final String TAG = SubscriptionDetailDialog.class.getSimpleName();

    public static final String KEY_SUBSCRIPTION = "KEY_SUBSCRIPTION";
    public static final String KEY_VIEW_TYPE = "KEY_VIEW_TYPE";

    public static final Integer TYPE_INFO = 0;
    public static final Integer TYPE_REQUEST = 1;
    public static final Integer TYPE_RESULT = 2;

    private boolean created = false;

    private int currViewType;
    private int prevViewType;
    private int newViewType;
    private Fragment fragment;
    private String fragmentClassName;

    private View rootView;

    private SubscriptionStopResultListener subscriptionStopResultListener;

    SubscriptionDetailDialog() {
        created = true;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public static SubscriptionDetailDialog newInstance(int type, SubscribedContent item) {
        Bundle args = new Bundle();
        SubscriptionDetailDialog fragment = new SubscriptionDetailDialog();
        args.putInt(KEY_VIEW_TYPE, type);
        args.putParcelable(KEY_SUBSCRIPTION, item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {

        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }

        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {
                boolean available = backToFragment();
                Log.d(TAG, "onBackPressed: back fragment available=" + available);
                if (!available) {
                    super.onBackPressed();
                }
            }
        };
    }

    public boolean backToFragment() {
        FragmentManager manager = getChildFragmentManager();
        int stackCount = manager.getBackStackEntryCount();
        Log.d(TAG, "backToFragment: back fragment size=" + stackCount);
        if (stackCount > 0) {
            manager.popBackStack();
            return true;
        }
        return false;
    }

    public void removeAllBackFragments() {
        getChildFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_mymenu_common, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        Bundle args = getArguments();
        int type = TYPE_INFO;
        SubscribedContent item = null;
        if (args != null) {
            type = args.getInt(KEY_VIEW_TYPE);
            item = args.getParcelable(SubscriptionDetailDialog.KEY_SUBSCRIPTION);
        }

        showDetailView(type, item);
        showView(type);
        created = false;
    }

    private void showView(int type) {
        prevViewType = currViewType;
        currViewType = type;
        Log.d(TAG, "showView: currViewType=" + currViewType + ", prevViewType=" + prevViewType);
        if (this.fragment == null) {
            return;
        }
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.mymenu_container, fragment, fragmentClassName);
        addToBackStack(ft);
        ft.commit();
    }

    private void addToBackStack(FragmentTransaction ft) {
        Log.d(TAG, "addToBackStack: currViewType=" + currViewType + ", prevViewType=" + prevViewType);
        if (!created) {
            ft.addToBackStack(null);
        }
    }

    public void hideDetailView() {
        Log.d(TAG, "hideDetailView");
        removeAllBackFragments();
        this.dismiss();
    }

    public void showDetailView(int type, SubscribedContent item) {
        newViewType = type;
        Log.d(TAG, "showDetailView: newViewType=" + newViewType);
        if (type == TYPE_INFO) {
            setInfo(item);
        } else if (type == TYPE_REQUEST) {
            setRequest(item);
        } else if (type == TYPE_RESULT) {
            setResult(item);
        }

        if (!created) {
            showView(type);
        }
    }

    private void setInfo(SubscribedContent item) {
        this.fragment = SubscriptionInfoFragment.newInstance(item);
        this.fragmentClassName = SubscriptionInfoFragment.CLASS_NAME;
    }

    public void setRequest(SubscribedContent item) {
        this.fragment = SubscriptionStopFragment.newInstance(item);
        this.fragmentClassName = SubscriptionStopFragment.CLASS_NAME;
    }

    private void setResult(SubscribedContent item) {
        this.fragment = SubscriptionResultFragment.newInstance(item);
        this.fragmentClassName = SubscriptionResultFragment.CLASS_NAME;
    }

    public SubscriptionStopResultListener getSubscriptionStopResultListener(){
        return subscriptionStopResultListener;
    }

    public void setSubscriptionStopResultListener(SubscriptionStopResultListener listener){
        subscriptionStopResultListener = listener;
    }

    public interface SubscriptionStopResultListener{
        void onSuccess();

        void onFailed();
    }
}
