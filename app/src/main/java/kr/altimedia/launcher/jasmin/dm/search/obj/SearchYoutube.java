
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchYoutube implements Parcelable {
    public static final Creator<SearchYoutube> CREATOR = new Creator<SearchYoutube>() {
        @Override
        public SearchYoutube createFromParcel(Parcel in) {
            return new SearchYoutube(in);
        }

        @Override
        public SearchYoutube[] newArray(int size) {
            return new SearchYoutube[size];
        }
    };
    @Expose
    @SerializedName("thumbnail_height")
    private String thumbnailHeight;
    @Expose
    @SerializedName("liveBroadcastContent")
    private String livebroadcastcontent;
    @Expose
    @SerializedName("channelTitle")
    private String channeltitle;
    @Expose
    @SerializedName("channelId")
    private String channelid;
    @Expose
    @SerializedName("thumbnail_url")
    private String thumbnailUrl;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("thumbnail_width")
    private String thumbnailWidth;
    @Expose
    @SerializedName("publishedAt")
    private String publishedat;

    protected SearchYoutube(Parcel in) {
        thumbnailHeight = in.readString();
        livebroadcastcontent = in.readString();
        channeltitle = in.readString();
        channelid = in.readString();
        thumbnailUrl = in.readString();
        title = in.readString();
        description = in.readString();
        thumbnailWidth = in.readString();
        publishedat = in.readString();
    }

    public String getThumbnailHeight() {
        return thumbnailHeight;
    }

    public String getLivebroadcastcontent() {
        return livebroadcastcontent;
    }

    public String getChanneltitle() {
        return channeltitle;
    }

    public String getChannelid() {
        return channelid;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getThumbnailWidth() {
        return thumbnailWidth;
    }

    public String getPublishedat() {
        return publishedat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(thumbnailHeight);
        parcel.writeString(livebroadcastcontent);
        parcel.writeString(channeltitle);
        parcel.writeString(channelid);
        parcel.writeString(thumbnailUrl);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(thumbnailWidth);
        parcel.writeString(publishedat);
    }
}