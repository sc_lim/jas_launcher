/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.content.Context;
import android.graphics.Rect;
import android.media.tv.TvTrackInfo;
import android.media.tv.TvView;
import android.os.Bundle;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.util.Log;

import java.util.List;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.JasTvView;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.tv.interactor.PopularChannelInteractor;

public class MainTvView extends JasTvView implements PopularChannelInteractor {
    private static final String TAG = MainTvView.class.getSimpleName();

    private ChannelPreviewIndicator channelPreviewIndicator;
    private BlockScreen mainBlockScreen;
    private PopularChannelDataListener popularChannelDataListener;

    public MainTvView(Context context) {
        this(context, null);
    }

    public MainTvView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainTvView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public MainTvView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        inflate(getContext(), R.layout.main_tv_view, this);
        TvView tvView = findViewById(R.id.tv_view_main);

        channelPreviewIndicator = findViewById(R.id.channel_preview_indicator);
        channelPreviewIndicator.setTuneAction((ch) -> getLiveTvActivity().tuneChannel(ch, false));
        mainBlockScreen = findViewById(R.id.main_block_screen);

        init(JasTvView.TYPE_MAIN, tvView, mainBlockScreen);
    }

    @Override
    public void updateInfo(TuningResult tuningResult) {
        LiveTvActivity activity = getLiveTvActivity();
        if (activity != null) {
            activity.updateTuneResult(tuningResult);
        }
    }

    @Override
    protected void setPreviewIndicator(Channel channel) {
        channelPreviewIndicator.updateChannelPreview(channel);
    }

    public void setPreviewListener(ChannelPreviewIndicator.OnPreviewListener onPreviewListener) {
        channelPreviewIndicator.setPreviewListener(onPreviewListener);
    }

    public void resizeTvView(Rect rect) {
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) tvView.getLayoutParams();
        params.leftMargin = rect.left;
        params.topMargin = rect.top;
        params.width = rect.width();
        params.height = rect.height();
        tvView.setLayoutParams(params);
        tvView.invalidate();
    }

    protected void updateMuting() {
    }

    public void setMute(boolean isMute) {
        tvView.setStreamVolume(isMute ? 0.0f : 1.0f);
    }

    //implements PopularChannelInteractor interfaces
    public void setPopularChannelDataUpdateListener(PopularChannelDataListener listener) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setPopularChannelDataUpdateListener() listener:"+listener);
        }
        popularChannelDataListener = listener;
        String data = getPopularChannelData();
        if (data != null) {
            updatePopularChannelData(data);
        }
    }

    //implements PopularChannelInteractor interfaces
    public void setAudioTrack(int ranking) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setAudioTrack() ranking:"+ranking);
        }
        selectAudioTrack(Integer.toString(ranking));
    }

    //implements PopularChannelInteractor interfaces
    public Channel getChannel(String serviceId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "getChannel() serviceId:"+serviceId);
        }
        return mChannelDataManager.getChannelByServiceId(serviceId);
    }

    //implements PopularChannelInteractor interfaces
    public Program getCurrentProgram(String serviceId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "getProgram() serviceId:"+serviceId);
        }
        Channel channel = getChannel(serviceId);
        Program program = mProgramDataManager.getCurrentProgram(channel.getId());
        if (Log.INCLUDE) {
            Log.d(TAG, "getProgram() program:"+program);
        }
        return program;
    }

    private String getPopularChannelData() {
        List<TvTrackInfo> tracks = tvView.getTracks(TvTrackInfo.TYPE_VIDEO);
        TvTrackInfo track;
        for (int i = 0 ; tracks != null && i < tracks.size() ; i++) {
            track = tracks.get(i);
            Bundle extra = track.getExtra();
            if (extra != null) {
                String json = extra.getString(KEY_CHANNEL_INFO);
                if (json != null) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "getPopularChannelData() data:" + json);
                    }
                    return json;
                }
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "getPopularChannelData() data:" + null);
        }
        return null;
    }

    protected void updatePopularChannelData(String json) {
        if (popularChannelDataListener != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "updatePopularChannelData() data:" + json);
            }
            popularChannelDataListener.onPopularChannelDataUpdated(json);
        }
    }
}
