package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.common.CheckTextView;

public class RatingLocksPresenter extends CheckButtonPresenter {
    @Override
    public CheckButtonPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_check_button, parent, false);
        return new ViewHolder(view);
    }

    private class ViewHolder extends CheckButtonPresenter.ViewHolder {
        public ViewHolder(View view) {
            super(view);
        }

        @Override
        public void setData(CheckButtonItem buttonItem) {
            CheckTextView checkTextView = getCheckTextView();

            String title = view.getContext().getString(buttonItem.getTitle());
            checkTextView.setText(title);

            checkTextView.setTextGravity(Gravity.LEFT);
            checkTextView.setWidthTextView(R.dimen.setting_rating_check_text_view_width);
            checkTextView.setCheckIconLayoutAlignment(RelativeLayout.ALIGN_PARENT_LEFT, R.dimen.setting_text_view_icon_left_margin, -1, R.dimen.setting_rating_text_view_icon_right_margin, -1);
            checkTextView.setMaxLineTextView(1);

            if (buttonItem.isEnabledCheck()) {
                checkTextView.setChecked(buttonItem.isChecked());
            } else {
                checkTextView.setVisibilityCheckBox(View.INVISIBLE);
            }
        }
    }
}
