/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.media.TvPlayerAdapter;
import kr.altimedia.launcher.jasmin.tv.JasTvView;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.tv.ParentalController;
import kr.altimedia.launcher.jasmin.tv.interactor.TvViewInteractor;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.TimeShiftPinDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackSupportFragment;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.TimeShiftPlaybackSupportFragmentGlueHost;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.TimeShiftPlaybackTransportControlGlue;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackGlue;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.ThumbnailBar;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekDataProvider;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekUi;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Created by mc.kim on 09,07,2020
 */
public class TimeShiftFragment extends TimeShiftSupportFragment implements TimeShiftPinDialogFragment.OnPinCheckedListener {
    public static final String CLASS_NAME = TimeShiftFragment.class.getName();
    public static final String KEY_CHANNEL_ID = "ChannelId";
    public static final String KEY_PROGRAM = "Program";
    public static final String KEY_START_TIME = "startTime";
    private final String TAG = TimeShiftFragment.class.getSimpleName();
    private final int VISIBLE_SIZE = 6;
    private TvPlayerAdapter mPlayerAdapter = null;
    private final JasTvView mTvView;
    private TimeShiftPlaybackTransportControlGlue<TvPlayerAdapter> mTransportControlGlue;
    private OnTimeShiftInteractor mOnTimeShiftInteractor;

    private TimeShiftFragment(JasTvView tvView) {
        this.mTvView = tvView;
    }

    public static TimeShiftFragment newInstance(Bundle bundle, final JasTvView tvView) {
        TimeShiftFragment fragment = new TimeShiftFragment(tvView);
        fragment.setArguments(bundle);
        fragment.setIsEnableUseVideoSetting(true);
        return fragment;
    }

    private Channel mCurrentChannel;
    private Program mCurrentProgram;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    private void setCurrentFocusedProgram(Program program) {
        if (mCurrentFocusedProgram != null && mCurrentFocusedProgram.getId() == program.getId()) {
            return;
        }
        mCurrentFocusedProgram = program;
        mPlayerAdapter.setProgramInfoChanged(program.getStartTimeUtcMillis(), program.getEndTimeUtcMillis());
        mTransportControlGlue.setTitle(program.getTitle());
    }


    private Program mCurrentFocusedProgram;
    private ThumbnailBar mThumbnailBar;
    private ItemBridgeAdapter mAdapter;

    private VideoPlaybackSeekDataProvider generateSeekDataProvider(final Channel channel, Program program) {
        mCurrentFocusedProgram = program;
        VideoPlaybackSeekDataProvider mPlaybackSeekDataProvider = new VideoPlaybackSeekDataProvider(VideoPlaybackSeekDataProvider.Type.TIMESHIFT, VISIBLE_SIZE) {


            @Override
            public void setThumbnailBar(ThumbnailBar thumbnailBar, ItemBridgeAdapter adapter) {
                super.setThumbnailBar(thumbnailBar, adapter);
                mThumbnailBar = thumbnailBar;
                mAdapter = adapter;
            }

            @Override
            public void detach() {
                super.detach();
                mThumbnailBar = null;
                mAdapter = null;
            }

            @Override
            public void notifyLoadPlayingProgram(ThumbnailBar thumbnailBar, ItemBridgeAdapter adapter) {
                super.notifyLoadPlayingProgram(thumbnailBar, adapter);

                setCurrentFocusedProgram(mCurrentProgram);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void notifyLoadPrevProgram(ThumbnailBar thumbnailBar, ItemBridgeAdapter adapter) {
                super.notifyLoadPrevProgram(thumbnailBar, adapter);
                if (true) {
                    return;
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyLoadPrevProgram");
                }
                ProgramDataManager programDataManager = TvSingletons.getSingletons(getContext()).getProgramDataManager();
                List<Program> programList = programDataManager.getPrograms(channel.getId());
                int index = programList.indexOf(mCurrentFocusedProgram);
                int prevIndex = index - 1;
                if (prevIndex < 0) {
                    return;
                }

                Program prevProgram = programList.get(prevIndex);
                if (isTimeShiftEnabled(channel, prevProgram)) {
                    setCurrentFocusedProgram(prevProgram);
                    adapter.notifyDataSetChanged();
                    thumbnailBar.requestFocus();
                    thumbnailBar.setSelectedPosition(getSeekPositions().length - 1);
                }
            }

            @Override
            public void notifySeekProgram(long timeMs) {
                super.notifySeekProgram(timeMs);
                playProgram(mCurrentFocusedProgram, timeMs, true, false);
            }

            @Override
            public void notifyLoadNextProgram(ThumbnailBar thumbnailBar, ItemBridgeAdapter adapter) {
                super.notifyLoadNextProgram(thumbnailBar, adapter);
                if (true) {
                    return;
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyLoadNextProgram");
                }
                ProgramDataManager programDataManager = TvSingletons.getSingletons(getContext()).getProgramDataManager();
                List<Program> programList = programDataManager.getPrograms(channel.getId());
                int index = programList.indexOf(mCurrentFocusedProgram);

                if (index == -1) {
                    return;
                }

                int nextIndex = index + 1;
                if (nextIndex >= programList.size()) {
                    return;
                }

                Program nextProgram = programList.get(nextIndex);
                if (isTimeShiftEnabled(channel, nextProgram)) {
                    setCurrentFocusedProgram(nextProgram);
                    adapter.notifyDataSetChanged();
                    thumbnailBar.requestFocus();
                    thumbnailBar.setSelectedPosition(0);
                }
            }

            @Override
            public long getSkipTimeMillis() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call getSkipTimeMillis ");
                }
                return -1;
            }

            @Override
            public long[] getSeekPositions() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call getSeekPosition");
                }
                return generatedSeekablePosition(mCurrentFocusedProgram);
            }

            @Override
            public void getThumbnail(long seekTime, ResultCallback callback) {
                super.getThumbnail(seekTime, callback);
                if (Log.INCLUDE) {
                    Log.d(TAG, "getThumbnail | seekTime : " + seekTime);
                }
                if (seekTime == -1) {
                    return;
                }
                String fileName = ChannelImageManager.getTimeShiftThumbnailUrl(channel, seekTime);
                if (Log.INCLUDE) {
                    Log.d(TAG, "getThumbnail | fileName : " + fileName);
                }
                Glide.with(getContext())
                        .load(fileName).placeholder(R.drawable.detail_series_default).error(R.drawable.detail_series_default).diskCacheStrategy(DiskCacheStrategy.DATA)
                        .into(new SimpleTarget<Drawable>() {
                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                callback.onThumbnailLoaded(resource, seekTime);
                            }
                        });
            }

            @Override
            public void reset() {
                super.reset();
            }
        };
        return mPlaybackSeekDataProvider;
    }

    private long[] generatedSeekablePosition(Program program) {
        final long differTime = 5 * TimeUtil.MIN;
        long currentTime = JasmineEpgApplication.getSingletons(getContext()).getClock().currentTimeMillis();
        long enableTime = getTimeShiftEnableTime(mCurrentChannel);
        long limitTime = currentTime - enableTime;
        long startTimeMillis = program.getStartTimeUtcMillis();
        if (startTimeMillis < limitTime) {
            startTimeMillis = limitTime + (10 * TimeUtil.SEC);
        }

        long endTimeMillis = program.getEndTimeUtcMillis();
        if (endTimeMillis > currentTime) {
            endTimeMillis = currentTime - (10 * TimeUtil.SEC);
        }

        long checkTimeMillis = startTimeMillis;
        List<Long> seekTime = new ArrayList<>();
        do {
            seekTime.add(checkTimeMillis);
            checkTimeMillis += differTime;
        } while (checkTimeMillis < endTimeMillis);

        long[] seekTimeArray = new long[seekTime.size()];
        for (int i = 0; i < seekTime.size(); i++) {
            seekTimeArray[i] = seekTime.get(i);
        }
        return seekTimeArray;
    }

    public Channel getCurrentChannel() {
        return mCurrentChannel;
    }

    public Program getCurrentProgram() {
        return mCurrentProgram;
    }

    private void initSeekClient(VideoPlaybackSeekDataProvider mPlaybackSeekDataProvider) {
        setPlaybackSeekUiClient(new VideoPlaybackSeekUi.Client() {

            @Override
            public boolean isSeekEnabled() {
                return true;
            }

            @Override
            public void onSeekStarted() {
                super.onSeekStarted();
            }

            @Override
            public VideoPlaybackSeekDataProvider getPlaybackSeekDataProvider() {
                return mPlaybackSeekDataProvider;
            }

            @Override
            public void onSeekPositionChanged(long pos) {
                super.onSeekPositionChanged(pos);
            }

            @Override
            public void onSeekFinished(boolean cancelled) {
                super.onSeekFinished(cancelled);
            }
        });
        mTransportControlGlue.setSeekProvider(mPlaybackSeekDataProvider);
    }


    @Override
    public void onPause() {
        super.onPause();
//        if (mTransportControlGlue != null) {

    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        mPlayerAdapter = new TvPlayerAdapter(getContext(), getTvView()) {
            @Override
            protected void onSeekPrevious() {
                super.onSeekPrevious();
                tunePrevProgram(mCurrentChannel, mCurrentProgram, true, false);
            }

            @Override
            protected void onSeekNext() {
                super.onSeekNext();
                tuneNextProgram(mCurrentChannel, mCurrentProgram, true, false);
            }
        };
        setBackgroundType(VideoPlaybackSupportFragment.BG_NONE);
        return root;
    }

    private TvOverlayManager mTvOverlayManager = null;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnTimeShiftInteractor) {
            this.mOnTimeShiftInteractor = (OnTimeShiftInteractor) context;
        }

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mOnTimeShiftInteractor = null;
    }


    @Override
    public void closeMenu() {
        if (Log.INCLUDE) {
            Log.d(TAG, "EdgeDrawerLayout closeMenu");
        }
        if (!isOpenMenu()) {
            return;
        }

        mDrawer.closeDrawer(mSidePanel);
    }

    private final int WHAT_FINISH = 1;
    private final long CHECK_PAUSE_TIME = 30 * TimeUtil.MIN;
    private Handler mPauseHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == WHAT_FINISH) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call finish");
                }
//                if (mTransportControlGlue != null) {
//                    mTransportControlGlue.onKey(mTvView, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE));
//                }
                onBackPressed();
            }
        }
    };

    private VideoPlaybackGlue.PlayerCallback mPlayerCallback = new VideoPlaybackGlue.PlayerCallback() {
        @Override
        public void onPreparedStateChanged(VideoPlaybackGlue glue) {
            super.onPreparedStateChanged(glue);
            if (Log.INCLUDE) {
                Log.d(TAG, "onPrepareStateChanged");
            }
        }

        @Override
        public void onBlockedStateChanged(boolean isLocked) {
            super.onBlockedStateChanged(isLocked);
            if (mSidePanel != null) {
                mSidePanel.notifyDataChanged();
            }
            mTransportControlGlue.setControlsOverlayAutoHideEnabled(!isLocked);
        }


        @Override
        public void onPlayStateChanged(VideoPlaybackGlue glue) {
            super.onPlayStateChanged(glue);
            if (mPauseHandler != null) {
                mPauseHandler.removeMessages(WHAT_FINISH);
                if (!glue.isPlaying()) {
                    mPauseHandler.sendEmptyMessageDelayed(WHAT_FINISH,
                            CHECK_PAUSE_TIME);
                }
            }
        }

        @Override
        public void onUpdateProgress(VideoPlaybackGlue glue, long time, long duration) {
            super.onUpdateProgress(glue, time, duration);
//            if (Log.INCLUDE) {
//                Log.d(TAG, "onUpdateProgress : " + time);
//                Log.d(TAG, "onUpdateProgress | getStartTimeUtcMillis : " + mCurrentProgram.getStartTimeUtcMillis());
//                Log.d(TAG, "onUpdateProgress | getEndTimeUtcMillis : " + mCurrentProgram.getEndTimeUtcMillis());
//            }
            if (mPlayerAdapter.isSeeking()) {
                return;
            }

            long currentTime = JasmineEpgApplication.SystemClock().currentTimeMillis();

            if (time >= currentTime) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onUpdateProgress back to live");
                }
                onBackPressed();
            } else if (time < mCurrentProgram.getStartTimeUtcMillis() - (500)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onUpdateProgress tune previous program");
                }
                tunePrevProgram(mCurrentChannel, mCurrentProgram, time, true, false);
            } else if (time > mCurrentProgram.getEndTimeUtcMillis() + (500)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onUpdateProgress tune next program");
                }
                tuneNextProgram(mCurrentChannel, mCurrentProgram, time, true, false);
            }
        }

        @Override
        public void onPlayCompleted(VideoPlaybackGlue glue) {
            super.onPlayCompleted(glue);
            if (Log.INCLUDE) {
                Log.d(TAG, "onPlayCompleted  ");
            }
            onBackPressed();
        }
    };

    private void startTimeShift(Channel channel, Program program, long startTimeMs) {
        this.mCurrentChannel = channel;
        this.mCurrentProgram = program;
        mPlayerAdapter.reset();
        if (Log.INCLUDE) {
            Log.d(TAG, "startTimeShift : " + startTimeMs);
        }
        VideoPlaybackSeekDataProvider mPlaybackSeekDataProvider = generateSeekDataProvider(channel, program);
        TimeShiftPlaybackSupportFragmentGlueHost glueHost =
                new TimeShiftPlaybackSupportFragmentGlueHost(TimeShiftFragment.this);

        mTransportControlGlue = new TimeShiftPlaybackTransportControlGlue<>(getContext(), mPlayerAdapter,
                new TimeShiftPlaybackTransportControlGlue.OnTimeShiftAction() {
                    @Override
                    public void showList() {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "showList");
                        }
                        glueHost.hideControlsOverlay(true);
                        openMenu();
                    }

                    @Override
                    public boolean canSeek(boolean prev) {
                        return checkCanSeek(mCurrentChannel, mCurrentProgram, prev);
                    }

                    @Override
                    public void seekToPreviousProgram() {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "seekToPreviousProgram");
                        }
                        tunePrevProgram(mCurrentChannel, mCurrentProgram, true, false);
                    }

                    @Override
                    public void seekToNextProgram() {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "seekToNextProgram");
                        }
                        tuneNextProgram(mCurrentChannel, mCurrentProgram, true, false);
                    }

                    @Override
                    public void tuneToLive() {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "tuneToLive");
                        }
                        onBackPressed();
                    }

                    @Override
                    public boolean checkUnLockContents() {
                        TimeShiftPinDialogFragment dialog = TimeShiftPinDialogFragment.create(TimeShiftPinDialogFragment.PIN_DIALOG_TYPE_UNLOCK_CHANNEL);
                        dialog.setOnPinCheckedListener(TimeShiftFragment.this::onPinChecked);
                        mTvOverlayManager.showDialogFragment(TimeShiftPinDialogFragment.DIALOG_TAG, dialog, false);
                        return true;
                    }
                });

        mTransportControlGlue.setProgramStartTimeMs(this.mCurrentProgram.getStartTimeUtcMillis());
        mTransportControlGlue.setProgramEndTimeMs(this.mCurrentProgram.getEndTimeUtcMillis());
        mTransportControlGlue.setEnableTimeMs(getTimeShiftEnableTime(getCurrentChannel()));

        mTransportControlGlue.setSeekEnabled(true);
        if (mPlaybackSeekDataProvider != null) {
            initSeekClient(mPlaybackSeekDataProvider);
        }
        mTransportControlGlue.setSeekEnabled(true);


        mTransportControlGlue.addPlayerCallback(mPlayerCallback);
        mTransportControlGlue.setHost(glueHost);
        mTransportControlGlue.setTitle(program.getTitle());
        mTransportControlGlue.setSubtitle(program.getDescription());
        mTransportControlGlue.playWhenPrepared();

        long timeShiftEnableTime = getTimeShiftEnableTime(channel);
        mPlayerAdapter.setDataSource(channel, program, program.getStartTimeUtcMillis(),
                program.getEndTimeUtcMillis(), startTimeMs, timeShiftEnableTime);
    }

    private void tunePrevProgram(Channel channel, Program program, boolean tune, boolean seeking) {
        tunePrevProgram(channel, program, -1, tune, seeking);
    }


    private void tuneNextProgram(Channel channel, Program program, boolean tune, boolean seeking) {
        tuneNextProgram(channel, program, -1, tune, seeking);
    }

    private boolean checkCanSeek(Channel channel, Program program, boolean prev) {
        if (Log.INCLUDE) {
            Log.d(TAG, "checkCanSeek : " + prev);
        }
        ProgramDataManager programDataManager = TvSingletons.getSingletons(getContext()).getProgramDataManager();
        List<Program> programList = programDataManager.getPrograms(channel.getId());

        if (prev) {
            int index = programList.indexOf(program);
            int prevIndex = index - 1;
            if (prevIndex < 0) {
                return false;
            }

            Program prevProgram = programList.get(prevIndex);
            return isTimeShiftEnabled(channel, prevProgram);
        } else {
            int index = programList.indexOf(program);

            if (index == -1) {
                return false;
            }

            int nextIndex = index + 1;
            if (nextIndex >= programList.size()) {
                return false;
            }

            Program nextProgram = programList.get(nextIndex);
            return isTimeShiftEnabled(channel, nextProgram);
        }
    }

    private void tunePrevProgram(Channel channel, Program program, long durationTimeMs, boolean tune, boolean seeking) {
        if (Log.INCLUDE) {
            Log.d(TAG, "tune check : tunePrevProgram : " + durationTimeMs);
        }
        ProgramDataManager programDataManager = TvSingletons.getSingletons(getContext()).getProgramDataManager();
        List<Program> programList = programDataManager.getPrograms(channel.getId());
        int index = programList.indexOf(program);
        int prevIndex = index - 1;
        if (prevIndex < 0) {
            showToast(getString(R.string.cannot_play_timeshift));
            return;
        }

        Program prevProgram = programList.get(prevIndex);
        if (isTimeShiftEnabled(channel, prevProgram)) {
            playProgram(prevProgram, durationTimeMs, tune, seeking);
        } else {
            showToast(getString(R.string.cannot_play_timeshift));
        }
    }


    private void tuneNextProgram(Channel channel, Program program, long durationTimeMs, boolean tune, boolean seeking) {
        if (Log.INCLUDE) {
            Log.d(TAG, "tune check : tuneNextProgram : " + durationTimeMs);
        }
        ProgramDataManager programDataManager = TvSingletons.getSingletons(getContext()).getProgramDataManager();
        List<Program> programList = programDataManager.getPrograms(channel.getId());
        int index = programList.indexOf(program);

        if (index == -1) {
            showToast(getString(R.string.cannot_play_timeshift));
            return;
        }

        int nextIndex = index + 1;
        if (nextIndex >= programList.size()) {
            showToast(getString(R.string.cannot_play_timeshift));
            return;
        }

        Program nextProgram = programList.get(nextIndex);
        if (isTimeShiftEnabled(channel, nextProgram)) {
            playProgram(nextProgram, durationTimeMs, tune, seeking);
        } else {
            showToast(getString(R.string.cannot_play_timeshift));
        }
    }


    private void playProgram(Program program) {
        playProgram(program, -1, true, false);
    }

    private void playProgram(Program program, long startTime, boolean tune, boolean seeking) {


//        Bundle bundle = new Bundle();
//        bundle.putString(TimeShiftFragment.KEY_CHANNEL_ID, mCurrentChannel.getServiceId());
//        bundle.putParcelable(TimeShiftFragment.KEY_PROGRAM, (ProgramImpl) program);
//        bundle.putLong(TimeShiftFragment.KEY_START_TIME, startTime);
//        setArguments(bundle);
//        startTimeShift(mCurrentChannel, program, startTime);

        if (Log.INCLUDE) {
            Log.d(TAG, "playProgram : " + tune + "program  : " + program.toString());
        }

        if (tune) {
            mOnTimeShiftInteractor.tuneTimeShift(mCurrentChannel, program, startTime);
//            Bundle bundle = new Bundle();
//            bundle.putString(TimeShiftFragment.KEY_CHANNEL_ID, mCurrentChannel.getServiceId());
//            bundle.putParcelable(TimeShiftFragment.KEY_PROGRAM, (ProgramImpl) program);
//            bundle.putLong(TimeShiftFragment.KEY_START_TIME, startTime);
//            setArguments(bundle);
//            startTimeShift(mCurrentChannel, program, startTime);
        } else {
            if (seeking) {
                mPlayerAdapter.seekTo(program.getStartTimeUtcMillis() + TimeUtil.SEC);
            }
            mCurrentProgram = program;
            mCurrentFocusedProgram = program;
            mPlayerAdapter.setProgramInfoChanged(program.getStartTimeUtcMillis(), program.getEndTimeUtcMillis());
            mTransportControlGlue.setTitle(program.getTitle());
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }

            if (!isControlVisible()) {
                mTransportControlGlue.showControllerBar();
            }
        }

    }

    @Override
    public void openMenu() {
        if (Log.INCLUDE) {
            Log.d(TAG, "EdgeDrawerLayout openMenu");
        }
        if (isOpenMenu()) {
            return;
        }
        mSidePanel.renewalPanelData(mCurrentChannel, mCurrentProgram, new TimeShiftPanelLayout.TimeShiftProgramKeyListener() {


            @Override
            public boolean isPlayingProgram(Program program) {
                return program.getId() == mCurrentProgram.getId();
            }

            @Override
            public boolean isLocked(Program program) {
                if (mTvView.isUnblockedChannel(mCurrentChannel)) {
                    return false;
                }

                return ParentalController.isProgramRatingBlocked(program);
            }

            @Override
            public void onItemSelected(Program program) {

                if (program.getId() == mCurrentProgram.getId()) {
                    closeMenu();
                } else {
                    if (isTimeShiftEnabled(mCurrentChannel, program)) {
                        closeMenu();
                        playProgram(program);
                    } else {
                        showToast(getString(R.string.cannot_play_timeshift));
                    }
                }


            }

            @Override
            public void onBackPressed(Program program) {
                closeMenu();
            }
        });
        mDrawer.openDrawer(mSidePanel);
    }

    private void showToast(String message) {
        JasminToast.makeToast(getContext(), message);
    }

    private boolean isTImeShiftEnableTime(Channel channel, long playTimeMs) {
        if (playTimeMs == -1) {
            return true;
        }
        long timeShiftTimeMillis = channel != null ? channel.getTimeshiftService() : 0;
        if (Log.INCLUDE) {
            Log.d(TAG, "isTImeShiftEnableTime | timeShiftTimeMillis : " + timeShiftTimeMillis);
        }
        if (timeShiftTimeMillis == 0) {
            return false;
        }

        long currentTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
        long availableTime = currentTime - timeShiftTimeMillis;
        return playTimeMs >= availableTime;
    }

    private boolean isTimeShiftEnabled(Channel channel, Program program) {
        if (program == null) {
            return false;
        }
        long timeShiftTimeMillis = channel != null ? channel.getTimeshiftService() : 0;
        if (Log.INCLUDE) {
            Log.d(TAG, "isTimeShiftEnabled | timeShiftTimeMillis : " + timeShiftTimeMillis);
        }
        long currentTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
        long availableTime = currentTime - timeShiftTimeMillis;
        return program.getEndTimeUtcMillis() > availableTime && program.getStartTimeUtcMillis() < currentTime;
    }

    private long getTimeShiftEnableTime(Channel channel) {
        long timeShiftTimeMillis = channel != null ? channel.getTimeshiftService() : 0;
        if (Log.INCLUDE) {
            Log.d(TAG, "getTimeShiftEnableTime | timeShiftTimeMillis : " + timeShiftTimeMillis);
        }

        return timeShiftTimeMillis;
    }

    @Override
    protected void showControlsOverlay(boolean show, boolean animation) {
        super.showControlsOverlay(show, animation);
        if (show) {
            setCurrentFocusedProgram(mCurrentProgram);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.requestFocus();
        String channelId = getArguments().getString(KEY_CHANNEL_ID);
        ChannelDataManager mChannelDataManager = TvSingletons.getSingletons(view.getContext()).getChannelDataManager();
        final Channel channel =
                mChannelDataManager.getChannelByServiceId(channelId);
        final Program program =
                getArguments().getParcelable(KEY_PROGRAM);
        final long resumeTime = getArguments().getLong(KEY_START_TIME, -1);
        startTimeShift(channel, program, resumeTime);
    }

    @Override
    protected boolean isPlaying() {
        return mPlayerAdapter.isPlaying();
    }

    @Override
    protected boolean isPrepared() {
        return mPlayerAdapter.isPrepared();
    }

    @Override
    public void onVideoSizeChanged(int width, int height) {
    }

    @Override
    public void onDestroyView() {
        if (mTransportControlGlue != null) {
            mTransportControlGlue.removePlayerCallback(mPlayerCallback);
        }
        if (mPlayerAdapter != null) {
            mPlayerAdapter.release();
        }
        if (mPauseHandler != null) {
            mPauseHandler.removeMessages(WHAT_FINISH);
        }
        mThumbnailBar = null;
        mAdapter = null;
        super.onDestroyView();
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        if (Log.INCLUDE) {
            Log.d(TAG, "onBackPressed");
        }
        if (mOnTimeShiftInteractor != null) {
            mOnTimeShiftInteractor.stopTimeShift(mCurrentChannel);
        }
    }

    @Override
    public void onError(int errorCode, CharSequence errorMessage) {
        super.onError(errorCode, errorMessage);

        String title = ErrorMessageManager.getInstance().getPlayerErrorCode(getContext(), errorCode);
        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, title,
                ErrorMessageManager.getInstance().getErrorMessage(title));
        dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                onBackPressed();
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public JasTvView getTvView() {
        return mTvView;
    }


    public interface OnTimeShiftInteractor {
        void stopTimeShift(Channel channel);

        TvViewInteractor.TuneResult tuneTimeShift(Channel channel, Program program, long startTime);
    }

    @Override
    public void onPinChecked(boolean checked, int type, String rating) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPinChecked : " + checked);
        }
        if (checked) {
            mPlayerAdapter.unLock();
        }
    }

    @Override
    public void onFinishedCalled() {
        super.onFinishedCalled();
        getActivity().finish();
    }
}