/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonInfo;
import kr.altimedia.launcher.jasmin.ui.view.common.CheckTextView;

public class VodSeasonPresenter extends Presenter {
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_vod_season, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((SeasonInfo) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public class ViewHolder extends Presenter.ViewHolder {
        private CheckTextView checkTextView;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            checkTextView = view.findViewById(R.id.check_button);
            checkTextView.setWidthTextView(R.dimen.series_vod_season_presenter_text_width);
            checkTextView.setCheckIconLayoutAlignment(RelativeLayout.ALIGN_PARENT_RIGHT, -1, -1, R.dimen.series_vod_season_presenter_icon_right_margin, -1);
            checkTextView.setMaxLineTextView(1);
            checkTextView.setTextLayoutGravity(RelativeLayout.ALIGN_PARENT_LEFT);
        }

        private void setData(SeasonInfo seasonInfo) {
            checkTextView.setText(view.getContext().getString(R.string.season) + " " + seasonInfo.getSeasonID());
        }

        public void setChecked(boolean isChecked) {
            checkTextView.setChecked(isChecked);
        }
    }
}
