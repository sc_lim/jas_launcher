/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.presenter;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.HashMap;

import androidx.leanback.widget.OnChildSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.leanback.widget.ShadowOverlayHelper;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.MyContentRowView;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.BaseGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalHoverCardSwitcher;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.util.FocusHighlightHelper;
import kr.altimedia.launcher.jasmin.ui.view.util.TransitionHelper;

public class MyContentListPresenter extends RowPresenter {
    public static final String CLASS_NAME = MyContentListPresenter.class.getName();
    private final String TAG = MyContentListPresenter.class.getSimpleName();

    private static final boolean DEBUG = false;
    private static final int DEFAULT_RECYCLED_POOL_SIZE = 24;
    private int mNumRows;
    private int mRowHeight;
    private int mExpandedRowHeight;
    private PresenterSelector mHoverCardPresenterSelector;
    private int mFocusZoomFactor;
    private boolean mUseFocusDimmer;
    private boolean mShadowEnabled;
    private int mBrowseRowsFadingEdgeLength;
    private boolean mRoundedCornersEnabled;
    private boolean mKeepChildForeground;
    private HashMap<Presenter, Integer> mRecycledPoolSize;
    private static int sSelectedRowTopPadding;
    private static int sExpandedSelectedRowTopPadding;
    private static int sExpandedRowNoHovercardBottomPadding;
    private OnPresenterDispatchKeyListener mOnPresenterDispatchKeyListener = null;
    private int mFixedItemIndex = 0;
    private float ALIGNMENT_DIFFER = 10.2f;
    private float ALIGNMENT = ALIGNMENT_DIFFER * (mFixedItemIndex + 1);

    public MyContentListPresenter() {
        this(5);
        this.mHeaderPresenter = new MyContentListHeaderPresenter();
    }

    public MyContentListPresenter(int focusZoomFactor) {
        this(focusZoomFactor, false);
        this.mHeaderPresenter = new MyContentListHeaderPresenter();
    }

    public MyContentListPresenter(int focusZoomFactor, boolean useFocusDimmer) {
        this.mNumRows = 1;
        this.mShadowEnabled = true;
        this.mBrowseRowsFadingEdgeLength = -1;
        this.mRoundedCornersEnabled = true;
        this.mKeepChildForeground = true;
        this.mRecycledPoolSize = new HashMap();
        if (!FocusHighlightHelper.isValidZoomIndex(focusZoomFactor)) {
            throw new IllegalArgumentException("Unhandled zoom factor");
        } else {
            this.mFocusZoomFactor = focusZoomFactor;
            this.mUseFocusDimmer = useFocusDimmer;
        }
        this.mHeaderPresenter = new MyContentListHeaderPresenter();
    }

    public void setOnPresenterDispatchKeyListener(OnPresenterDispatchKeyListener onPresenterDispatchKeyListener) {
        this.mOnPresenterDispatchKeyListener = onPresenterDispatchKeyListener;
    }

    public void setRowHeight(int rowHeight) {
        this.mRowHeight = rowHeight;
    }

    public int getRowHeight() {
        return this.mRowHeight;
    }

    public void setExpandedRowHeight(int rowHeight) {
        this.mExpandedRowHeight = rowHeight;
    }

    public int getExpandedRowHeight() {
        return this.mExpandedRowHeight != 0 ? this.mExpandedRowHeight : this.mRowHeight;
    }

    public final int getFocusZoomFactor() {
        return this.mFocusZoomFactor;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public final int getZoomFactor() {
        return this.mFocusZoomFactor;
    }

    public final boolean isFocusDimmerUsed() {
        return this.mUseFocusDimmer;
    }

    public void setNumRows(int numRows) {
        this.mNumRows = numRows;
    }


    protected void initializeRowViewHolder(RowPresenter.ViewHolder holder) {
        super.initializeRowViewHolder(holder);
        final ViewHolder rowViewHolder = (ViewHolder) holder;
        Context context = holder.view.getContext();
//        if (this.mShadowOverlayHelper == null) {
//            this.mShadowOverlayHelper = (new ShadowOverlayHelper.Builder()).needsOverlay(this.needsDefaultListSelectEffect()).needsShadow(this.needsDefaultShadow()).needsRoundedCorner(this.isUsingOutlineClipping(context) && this.areChildRoundedCornersEnabled()).preferZOrder(this.isUsingZOrder(context)).keepForegroundDrawable(this.mKeepChildForeground).options(this.createShadowOverlayOptions()).build(context);
//            if (this.mShadowOverlayHelper.needsWrapper()) {
//                this.mShadowOverlayWrapper = new ItemBridgeAdapterShadowOverlayWrapper(this.mShadowOverlayHelper);
//            }
//        }

        rowViewHolder.mItemBridgeAdapter = new ListRowPresenterItemBridgeAdapter(rowViewHolder);
//        rowViewHolder.mItemBridgeAdapter.setWrapper(this.mShadowOverlayWrapper);
//        this.mShadowOverlayHelper.prepareParentForShadow(rowViewHolder.mViewPager);
        FocusHighlightHelper.setupBrowseItemFocusHighlight(rowViewHolder.mItemBridgeAdapter, this.mFocusZoomFactor, this.mUseFocusDimmer);
//        rowViewHolder.mViewPager.setFocusDrawingOrderEnabled(this.mShadowOverlayHelper.getShadowType() != 3);
        rowViewHolder.mGridView.setOnChildSelectedListener(new OnChildSelectedListener() {
            public void onChildSelected(ViewGroup parent, View view, int position, long id) {
                MyContentListPresenter.this.selectChildView(rowViewHolder, view, true);
            }
        });
        rowViewHolder.mGridView.setOnUnhandledKeyListener(new BaseGridView.OnUnhandledKeyListener() {
            public boolean onUnhandledKey(KeyEvent event) {
                return rowViewHolder.getOnKeyListener() != null && rowViewHolder.getOnKeyListener().onKey(rowViewHolder.view, event.getKeyCode(), event);
            }
        });
        rowViewHolder.mGridView.setNumRows(this.mNumRows);
    }

    final boolean needsDefaultListSelectEffect() {
        return this.isUsingDefaultListSelectEffect() && this.getSelectEffectEnabled();
    }

    public void setRecycledPoolSize(Presenter presenter, int size) {
        this.mRecycledPoolSize.put(presenter, size);
    }

    public int getRecycledPoolSize(Presenter presenter) {
        return this.mRecycledPoolSize.containsKey(presenter) ? (Integer) this.mRecycledPoolSize.get(presenter) : 24;
    }

    public final void setHoverCardPresenterSelector(PresenterSelector selector) {
        this.mHoverCardPresenterSelector = selector;
    }

    public final PresenterSelector getHoverCardPresenterSelector() {
        return this.mHoverCardPresenterSelector;
    }

    void selectChildView(ViewHolder rowViewHolder, View view, boolean fireEvent) {
        if (view != null) {
            if (rowViewHolder.mSelected) {
                ItemBridgeAdapter.ViewHolder ibh = (ItemBridgeAdapter.ViewHolder) rowViewHolder.mGridView.getChildViewHolder(view);
                if (this.mHoverCardPresenterSelector != null) {
                    rowViewHolder.mHoverCardViewSwitcher.select(rowViewHolder.mGridView, view, ibh.mItem);
                }

                if (fireEvent && rowViewHolder.getOnItemViewSelectedListener() != null) {
                    rowViewHolder.getOnItemViewSelectedListener().onItemSelected(ibh.mHolder, ibh.mItem, rowViewHolder, rowViewHolder.mRow);
                }
            }
        } else {
            if (this.mHoverCardPresenterSelector != null) {
                rowViewHolder.mHoverCardViewSwitcher.unselect();
            }

            if (fireEvent && rowViewHolder.getOnItemViewSelectedListener() != null) {
                rowViewHolder.getOnItemViewSelectedListener().onItemSelected((Presenter.ViewHolder) null, (Object) null, rowViewHolder, rowViewHolder.mRow);
            }
        }

    }

    private static void initStatics(Context context) {
        if (sSelectedRowTopPadding == 0) {
            sSelectedRowTopPadding = context.getResources().getDimensionPixelSize(R.dimen.cb_browse_selected_row_top_padding);
            sExpandedSelectedRowTopPadding = context.getResources().getDimensionPixelSize(R.dimen.cb_browse_expanded_selected_row_top_padding);
            sExpandedRowNoHovercardBottomPadding = context.getResources().getDimensionPixelSize(R.dimen.cb_browse_expanded_row_no_hovercard_bottom_padding);
        }
    }

    private int getSpaceUnderBaseline(ViewHolder vh) {
        MyContentListHeaderPresenter.ViewHolder headerViewHolder = (MyContentListHeaderPresenter.ViewHolder) vh.getHeaderViewHolder();
        if (headerViewHolder != null) {
            return vh.getGridView().getContext().getResources().getDimensionPixelSize(R.dimen.poster_header_under_space);
//            return this.getHeaderPresenter() != null ? this.getHeaderPresenter().getSpaceUnderBaseline(headerViewHolder) : headerViewHolder.view.getPaddingBottom();
        } else {
            return 0;
        }
    }

    private void setVerticalPadding(ViewHolder vh) {
        int paddingTop;
        int paddingBottom;


        if (vh.isExpanded()) {
            int headerSpaceUnderBaseline = this.getSpaceUnderBaseline(vh);
            paddingTop = (vh.isSelected() ? sExpandedSelectedRowTopPadding : vh.mPaddingTop) - headerSpaceUnderBaseline;
            paddingBottom = this.mHoverCardPresenterSelector == null ? sExpandedRowNoHovercardBottomPadding : vh.mPaddingBottom;
        } else if (vh.isSelected()) {
            paddingTop = sSelectedRowTopPadding - vh.mPaddingBottom;
            paddingBottom = sSelectedRowTopPadding;
        } else {
            paddingTop = 0;
            paddingBottom = vh.mPaddingBottom;
        }

        vh.getGridView().setPadding(vh.mPaddingLeft, paddingTop, vh.mPaddingRight, paddingBottom);
    }

    protected RowPresenter.ViewHolder createRowViewHolder(ViewGroup parent) {
        initStatics(parent.getContext());
        MyContentRowView rowView = new MyContentRowView(parent.getContext(), R.layout.list_mymenu_menu);
        this.setupFadingEffect(rowView);
        if (this.mRowHeight != 0) {
            rowView.getGridView().setRowHeight(this.mRowHeight);
        }
        int space = parent.getContext().getResources().getDimensionPixelSize(R.dimen.poster_horizontal_space);
        rowView.getGridView().setHorizontalSpacing(space);

        initAlignment(rowView.getGridView());

        RowPresenter.ViewHolder viewHolder = new ViewHolder(rowView, rowView.getGridView(), this);
        rowView.setOnDispatchKeyListener(new MyContentRowView.OnDispatchKeyListener() {
            @Override
            public boolean onKeyDown(KeyEvent event) {
                if (mOnPresenterDispatchKeyListener != null) {
                    int position = rowView.getGridView().getSelectedPosition();
                    return mOnPresenterDispatchKeyListener.notifyKey(event, viewHolder, position);
                }
                return false;
            }
        });
        return viewHolder;
    }

    public void setFixedItemIndex(int fixedItemIndex) {
        this.mFixedItemIndex = fixedItemIndex;
        if (fixedItemIndex < 0) { // normal focus
//            throw new IllegalArgumentException("wrong index");
            ALIGNMENT = 87.3f;

        }else { // fixed focus
            ALIGNMENT = ALIGNMENT_DIFFER * (mFixedItemIndex + 1);
        }
    }

    private void initAlignment(HorizontalGridView mHorizontalGridView) {
        if (mHorizontalGridView == null) {
            return;
        }

        if(mFixedItemIndex >= 0) { // fixed focus
            mHorizontalGridView.setItemAlignmentOffset(0);
            mHorizontalGridView.setItemAlignmentOffsetPercent(
                    0f);
            mHorizontalGridView.setItemAlignmentOffsetWithPadding(false);
            mHorizontalGridView.setWindowAlignmentOffset(0);
            mHorizontalGridView.setWindowAlignmentOffsetPercent(ALIGNMENT_DIFFER);

        }else{ // normal focus
            mHorizontalGridView.setFocusScrollStrategy(BaseGridView.FOCUS_SCROLL_ALIGNED);
            mHorizontalGridView.setWindowAlignmentOffsetPercent(ALIGNMENT);
        }
    }

    private void setAlignment(HorizontalGridView mHorizontalGridView) {
        if (mHorizontalGridView == null) {
            return;
        }

        if (mHorizontalGridView.getWindowAlignmentOffsetPercent() == ALIGNMENT) {
            return;
        }
        float currentOffsetPercent = mHorizontalGridView.getWindowAlignmentOffsetPercent();
        mHorizontalGridView.setWindowAlignmentOffsetPercent(currentOffsetPercent + ALIGNMENT_DIFFER);
        mHorizontalGridView.setWindowAlignment(3);
    }

    private void setAlignmentLeft(HorizontalGridView mHorizontalGridView) {
        if (mHorizontalGridView == null) {
            return;
        }
        if (mHorizontalGridView.getWindowAlignmentOffsetPercent() == ALIGNMENT_DIFFER) {
            return;
        }

        float currentOffsetPercent = mHorizontalGridView.getWindowAlignmentOffsetPercent();
        mHorizontalGridView.setWindowAlignmentOffsetPercent(currentOffsetPercent - ALIGNMENT_DIFFER);
        mHorizontalGridView.setWindowAlignment(BaseGridView.WINDOW_ALIGN_NO_EDGE);
    }

    private void printAlignment(HorizontalGridView mHorizontalGridView) {
        if (mHorizontalGridView == null) {
            return;
        }
        int ItemAlignmentOffset = mHorizontalGridView.getItemAlignmentOffset();
        float ItemAlignmentOffsetPercent = mHorizontalGridView.getItemAlignmentOffsetPercent();
        boolean isItemAlignmentOffsetWithPadding = mHorizontalGridView.isItemAlignmentOffsetWithPadding();
        int WindowAlignmentOffset = mHorizontalGridView.getWindowAlignmentOffset();
        float WindowAlignmentOffsetPercent = mHorizontalGridView.getWindowAlignmentOffsetPercent();
        int WindowAlignment = mHorizontalGridView.getWindowAlignment();

        if (Log.INCLUDE) {
            Log.d(TAG, "printAlignment : ItemAlignmentOffset " + ItemAlignmentOffset);
            Log.d(TAG, "printAlignment : ItemAlignmentOffsetPercent " + ItemAlignmentOffsetPercent);
            Log.d(TAG, "printAlignment : isItemAlignmentOffsetWithPadding " + isItemAlignmentOffsetWithPadding);
            Log.d(TAG, "printAlignment : WindowAlignmentOffset " + WindowAlignmentOffset);
            Log.d(TAG, "printAlignment : WindowAlignmentOffsetPercent " + WindowAlignmentOffsetPercent);
            Log.d(TAG, "printAlignment : WindowAlignment " + WindowAlignment);
        }
    }


    protected void dispatchItemSelectedListener(RowPresenter.ViewHolder holder, boolean selected) {
        ViewHolder vh = (ViewHolder) holder;
        ItemBridgeAdapter.ViewHolder itemViewHolder = (ItemBridgeAdapter.ViewHolder) vh.mGridView.findViewHolderForPosition(vh.mGridView.getSelectedPosition());
        if (itemViewHolder == null) {
            super.dispatchItemSelectedListener(holder, selected);
        } else {
            if (selected && holder.getOnItemViewSelectedListener() != null) {
                holder.getOnItemViewSelectedListener().onItemSelected(itemViewHolder.getViewHolder(), itemViewHolder.mItem, vh, vh.getRow());
            }
        }
    }

    protected void onRowViewSelected(RowPresenter.ViewHolder holder, boolean selected) {
        super.onRowViewSelected(holder, selected);
        ViewHolder vh = (ViewHolder) holder;
        this.setVerticalPadding(vh);
        this.updateFooterViewSwitcher(vh);
    }

    private void updateFooterViewSwitcher(ViewHolder vh) {
        if (vh.mExpanded && vh.mSelected) {
            if (this.mHoverCardPresenterSelector != null) {
                vh.mHoverCardViewSwitcher.init((ViewGroup) vh.view, this.mHoverCardPresenterSelector);
            }

            ItemBridgeAdapter.ViewHolder ibh = (ItemBridgeAdapter.ViewHolder) vh.mGridView.findViewHolderForPosition(vh.mGridView.getSelectedPosition());
            this.selectChildView(vh, ibh == null ? null : ibh.itemView, false);
        } else if (this.mHoverCardPresenterSelector != null) {
            vh.mHoverCardViewSwitcher.unselect();
        }

    }

    private void setupFadingEffect(MyContentRowView rowView) {
        HorizontalGridView gridView = rowView.getGridView();
        if (this.mBrowseRowsFadingEdgeLength < 0) {
            TypedArray ta = gridView.getContext().obtainStyledAttributes(R.styleable.LeanbackTheme);
            this.mBrowseRowsFadingEdgeLength = (int) ta.getDimension(R.styleable.LeanbackTheme_browseRowsFadingEdgeLength, 0.0F);
//            this.mBrowseRowsFadingEdgeLength = 0;
            ta.recycle();
        }

        gridView.setFadingLeftEdgeLength(this.mBrowseRowsFadingEdgeLength);
    }

    protected void onRowViewExpanded(RowPresenter.ViewHolder holder, boolean expanded) {
        super.onRowViewExpanded(holder, expanded);
        ViewHolder vh = (ViewHolder) holder;
        if (this.getRowHeight() != this.getExpandedRowHeight()) {
            int newHeight = expanded ? this.getExpandedRowHeight() : this.getRowHeight();
            vh.getGridView().setRowHeight(newHeight);
        }

        this.setVerticalPadding(vh);
        this.updateFooterViewSwitcher(vh);
    }

    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);
        ViewHolder vh = (ViewHolder) holder;
        ListRow rowItem = (ListRow) item;
        vh.mItemBridgeAdapter.setAdapter(rowItem.getAdapter());
        vh.mGridView.setAdapter(vh.mItemBridgeAdapter);
        vh.mGridView.setContentDescription(rowItem.getContentDescription());
    }

    protected void onUnbindRowViewHolder(RowPresenter.ViewHolder holder) {
        ViewHolder vh = (ViewHolder) holder;
        vh.mGridView.setAdapter((RecyclerView.Adapter) null);
        vh.mItemBridgeAdapter.clear();
        super.onUnbindRowViewHolder(holder);
    }

    public final boolean isUsingDefaultSelectEffect() {
        return false;
    }

    public boolean isUsingDefaultListSelectEffect() {
        return true;
    }

    public boolean isUsingDefaultShadow() {
        return ShadowOverlayHelper.supportsShadow();
    }

    public boolean isUsingZOrder(Context context) {
        return false;
//        return !Settings.getInstance(context).preferStaticShadows();
    }

    public boolean isUsingOutlineClipping(Context context) {
        return false;
//        return !Settings.getInstance(context).isOutlineClippingDisabled();
    }

    public final void setShadowEnabled(boolean enabled) {
        this.mShadowEnabled = enabled;
    }

    public final boolean getShadowEnabled() {
        return this.mShadowEnabled;
    }

    public final void enableChildRoundedCorners(boolean enable) {
        this.mRoundedCornersEnabled = enable;
    }

    public final boolean areChildRoundedCornersEnabled() {
        return this.mRoundedCornersEnabled;
    }

    final boolean needsDefaultShadow() {
        return this.isUsingDefaultShadow() && this.getShadowEnabled();
    }

    public final void setKeepChildForeground(boolean keep) {
        this.mKeepChildForeground = keep;
    }

    public final boolean isKeepChildForeground() {
        return this.mKeepChildForeground;
    }

    protected ShadowOverlayHelper.Options createShadowOverlayOptions() {
        return ShadowOverlayHelper.Options.DEFAULT;
    }

    protected void onSelectLevelChanged(RowPresenter.ViewHolder holder) {
        super.onSelectLevelChanged(holder);
        ViewHolder vh = (ViewHolder) holder;
        int i = 0;

        for (int count = vh.mGridView.getChildCount(); i < count; ++i) {
            this.applySelectLevelToChild(vh, vh.mGridView.getChildAt(i));
        }
    }

    protected void applySelectLevelToChild(ViewHolder rowViewHolder, View childView) {
//        if (this.mShadowOverlayHelper != null && this.mShadowOverlayHelper.needsOverlay()) {
//            int dimmedColor = rowViewHolder.mColorDimmer.getPaint().getColor();
//            this.mShadowOverlayHelper.setOverlayColor(childView, dimmedColor);
//        }

    }

    public void freeze(RowPresenter.ViewHolder holder, boolean freeze) {
        ViewHolder vh = (ViewHolder) holder;
        vh.mGridView.setScrollEnabled(!freeze);
        vh.mGridView.setAnimateChildLayout(!freeze);
    }

    public void setEntranceTransitionState(RowPresenter.ViewHolder holder, boolean afterEntrance) {
        super.setEntranceTransitionState(holder, afterEntrance);
        ((ViewHolder) holder).mGridView.setChildrenVisibility(afterEntrance ? 0 : 4);
    }

    class ListRowPresenterItemBridgeAdapter extends ItemBridgeAdapter {
        MyContentListPresenter.ViewHolder mRowViewHolder;

        ListRowPresenterItemBridgeAdapter(MyContentListPresenter.ViewHolder rowViewHolder) {
            this.mRowViewHolder = rowViewHolder;
        }

        protected void onCreate(ViewHolder viewHolder) {
            if (viewHolder.itemView instanceof ViewGroup) {
                TransitionHelper.setTransitionGroup((ViewGroup) viewHolder.itemView, true);
            }

//            if (ListRowPresenter.this.mShadowOverlayHelper != null) {
//                ListRowPresenter.this.mShadowOverlayHelper.onViewCreated(viewHolder.itemView);
//            }

        }

        public void onBind(final ViewHolder viewHolder) {
            if (this.mRowViewHolder.getOnItemViewClickedListener() != null) {
                viewHolder.mHolder.view.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ViewHolder ibh = (ViewHolder) ListRowPresenterItemBridgeAdapter.this.mRowViewHolder.mGridView.getChildViewHolder(viewHolder.itemView);
                        if (ListRowPresenterItemBridgeAdapter.this.mRowViewHolder.getOnItemViewClickedListener() != null) {
                            ListRowPresenterItemBridgeAdapter.this.mRowViewHolder.getOnItemViewClickedListener().onItemClicked(viewHolder.mHolder, ibh.mItem, ListRowPresenterItemBridgeAdapter.this.mRowViewHolder, (ListRow) ListRowPresenterItemBridgeAdapter.this.mRowViewHolder.mRow);
                        }
                    }
                });
            }
        }

        public void onUnbind(ViewHolder viewHolder) {
            if (this.mRowViewHolder.getOnItemViewClickedListener() != null) {
                viewHolder.mHolder.view.setOnClickListener((View.OnClickListener) null);
            }
        }

        public void onAttachedToWindow(ViewHolder viewHolder) {
            MyContentListPresenter.this.applySelectLevelToChild(this.mRowViewHolder, viewHolder.itemView);
            this.mRowViewHolder.syncActivatedStatus(viewHolder.itemView);
        }

        public void onAddPresenter(Presenter presenter, int type) {
            this.mRowViewHolder.getGridView().getRecycledViewPool().setMaxRecycledViews(type, MyContentListPresenter.this.getRecycledPoolSize(presenter));
        }
    }

    public static class SelectItemViewHolderTask extends ViewHolderTask {
        private int mItemPosition;
        private boolean mSmoothScroll = true;
        ViewHolderTask mItemTask;

        public SelectItemViewHolderTask(int itemPosition) {
            this.setItemPosition(itemPosition);
        }

        public void setItemPosition(int itemPosition) {
            this.mItemPosition = itemPosition;
        }

        public int getItemPosition() {
            return this.mItemPosition;
        }

        public void setSmoothScroll(boolean smoothScroll) {
            this.mSmoothScroll = smoothScroll;
        }

        public boolean isSmoothScroll() {
            return this.mSmoothScroll;
        }

        public ViewHolderTask getItemTask() {
            return this.mItemTask;
        }

        public void setItemTask(ViewHolderTask itemTask) {
            this.mItemTask = itemTask;
        }

        public void run(Presenter.ViewHolder holder) {
            if (holder instanceof ViewHolder) {
                HorizontalGridView gridView = ((ViewHolder) holder).getGridView();
                androidx.leanback.widget.ViewHolderTask task = null;
                if (this.mItemTask != null) {
                    task = new androidx.leanback.widget.ViewHolderTask() {
                        final ViewHolderTask itemTask;

                        {
                            this.itemTask = SelectItemViewHolderTask.this.mItemTask;
                        }

                        public void run(RecyclerView.ViewHolder rvh) {
                            ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder) rvh;
                            this.itemTask.run(ibvh.getViewHolder());
                        }
                    };
                }

                if (this.isSmoothScroll()) {
                    gridView.setSelectedPositionSmooth(this.mItemPosition, task);
                } else {
                    gridView.setSelectedPosition(this.mItemPosition, task);
                }
            }

        }
    }

    public static class ViewHolder extends RowPresenter.ViewHolder {
        final MyContentListPresenter mListRowPresenter;
        final HorizontalGridView mGridView;
        ItemBridgeAdapter mItemBridgeAdapter;
        final HorizontalHoverCardSwitcher mHoverCardViewSwitcher = new HorizontalHoverCardSwitcher();
        final int mPaddingTop;
        final int mPaddingBottom;
        final int mPaddingLeft;
        final int mPaddingRight;

        public ViewHolder(View rootView, HorizontalGridView gridView, MyContentListPresenter p) {
            super(rootView);
            this.mGridView = gridView;
            this.mListRowPresenter = p;
            this.mPaddingTop = this.mGridView.getPaddingTop();
            this.mPaddingBottom = this.mGridView.getPaddingBottom();
            this.mPaddingLeft = this.mGridView.getPaddingLeft();
            this.mPaddingRight = this.mGridView.getPaddingRight();
        }

        public final MyContentListPresenter getListRowPresenter() {
            return this.mListRowPresenter;
        }

        public final HorizontalGridView getGridView() {
            return this.mGridView;
        }

        public final ItemBridgeAdapter getBridgeAdapter() {
            return this.mItemBridgeAdapter;
        }

        public int getSelectedPosition() {
            return this.mGridView.getSelectedPosition();
        }

        public Presenter.ViewHolder getItemViewHolder(int position) {
            ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder) this.mGridView.findViewHolderForAdapterPosition(position);
            return ibvh == null ? null : ibvh.getViewHolder();
        }

        public Presenter.ViewHolder getSelectedItemViewHolder() {
            return this.getItemViewHolder(this.getSelectedPosition());
        }

        public Object getSelectedItem() {
            ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder) this.mGridView.findViewHolderForAdapterPosition(this.getSelectedPosition());
            return ibvh == null ? null : ibvh.getItem();
        }
    }

    public interface OnPresenterDispatchKeyListener {
        boolean notifyKey(KeyEvent event, Presenter.ViewHolder viewHolder, int position);
    }
}