/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.reminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.altimedia.tvmodule.dao.ProgramImpl;
import com.altimedia.util.Log;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager.REMINDER_ALERT;

public class ReminderAlertService extends BroadcastReceiver {
    private final String TAG = ReminderAlertService.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getParcelableExtra(REMINDER_ALERT);
        Reminder reminder = bundle.getParcelable(REMINDER_ALERT);

        if (Log.INCLUDE) {
            Log.d(TAG, "onReceive, reminder reminder : " + reminder);
        }

        if (reminder != null) {
            ReminderAlertManager.getInstance().alertReminder(context, reminder);
        }
    }
}
