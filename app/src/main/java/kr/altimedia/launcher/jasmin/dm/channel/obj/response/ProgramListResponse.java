/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.channel.obj.MbsProgram;

/**
 * Created by mc.kim on 11,05,2020
 */
public class ProgramListResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private ProgramListResult result;

    protected ProgramListResponse(Parcel in) {
        super(in);
        result = in.readTypedObject(ProgramListResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(result, flags);
    }


    private List<MbsProgram> getProgramList(String channelId) {
        List<MbsProgram> resultList = new ArrayList<>();
        if (result == null) {
            return resultList;
        }
        List<ProgramListResult.ProgramByChannel> programByChannels = result.getProgramByChannelList();
        for (ProgramListResult.ProgramByChannel programByChannel : programByChannels) {
            if (programByChannel.channelID.equalsIgnoreCase(channelId)) {
                resultList.addAll(programByChannel.getProgramList());
            }
        }
        return resultList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProgramListResponse> CREATOR = new Creator<ProgramListResponse>() {
        @Override
        public ProgramListResponse createFromParcel(Parcel in) {
            return new ProgramListResponse(in);
        }

        @Override
        public ProgramListResponse[] newArray(int size) {
            return new ProgramListResponse[size];
        }
    };

    private static class ProgramListResult implements Parcelable {
        @Expose
        @SerializedName("channelList")
        private List<ProgramByChannel> programByChannelList;


        private static class ProgramByChannel implements Parcelable {
            @Expose
            @SerializedName("channelID")
            private String channelID;
            @Expose
            @SerializedName("programList")
            private List<MbsProgram> programList;


            protected ProgramByChannel(Parcel in) {
                channelID = in.readString();
                programList = in.createTypedArrayList(MbsProgram.CREATOR);
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(channelID);
                dest.writeTypedList(programList);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<ProgramByChannel> CREATOR = new Creator<ProgramByChannel>() {
                @Override
                public ProgramByChannel createFromParcel(Parcel in) {
                    return new ProgramByChannel(in);
                }

                @Override
                public ProgramByChannel[] newArray(int size) {
                    return new ProgramByChannel[size];
                }
            };

            @Override
            public String toString() {
                return "ProgramByChannel{" +
                        "channelID='" + channelID + '\'' +
                        ", programList=" + programList +
                        '}';
            }

            public String getChannelID() {
                return channelID;
            }

            public List<MbsProgram> getProgramList() {
                return programList;
            }
        }


        protected ProgramListResult(Parcel in) {
            programByChannelList = in.createTypedArrayList(ProgramByChannel.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(programByChannelList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<ProgramListResult> CREATOR = new Creator<ProgramListResult>() {
            @Override
            public ProgramListResult createFromParcel(Parcel in) {
                return new ProgramListResult(in);
            }

            @Override
            public ProgramListResult[] newArray(int size) {
                return new ProgramListResult[size];
            }
        };

        public List<ProgramByChannel> getProgramByChannelList() {
            return programByChannelList;
        }

        @Override
        public String toString() {
            return "ProgramListResult{" +
                    "programByChannelList=" + programByChannelList +
                    '}';
        }
    }
}
