/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import java.text.NumberFormat;
import java.util.Calendar;

public class StringUtil {
    public static String getFormattedPrice(float price) {
        return getFormattedNumber((long) price);
    }

    /**
     * Return formatted number.<br>
     * Put ',' in every 3 digits.<br>
     * Ex] 1234 => "1,234".<br>
     *
     * @param num number
     * @return formatted string
     */
    public static String getFormattedNumber(long num) {
        String formattedNum = null;
        NumberFormat format = NumberFormat.getNumberInstance();
        formattedNum = format.format(num);
        return formattedNum;
    }

    /**
     * Return formatted number.<br>
     * Ex] 4 => "004".<br>
     *
     * @param num_str    number string
     * @param total_size size
     * @return formatted string
     */
    public static String getFormattedNumber(String num_str, int total_size) {
        String formattedNum = "";
        if (num_str != null && !num_str.isEmpty()) {
            int diff = total_size - num_str.length();
            if (diff > 0) {
                for (int i = 0; i < diff; i++)
                    num_str = "0" + num_str;
            }
            formattedNum = num_str;
        }
        return formattedNum;
    }

    public static String getFormattedHour(int type, long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        int hour = calendar.get(type);
        return StringUtil.getFormattedNumber(Integer.toString(hour), 2);
    }

    public static String getFormattedMinute(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        int min = calendar.get(Calendar.MINUTE);
        return StringUtil.getFormattedNumber(Integer.toString(min), 2);
    }
}
