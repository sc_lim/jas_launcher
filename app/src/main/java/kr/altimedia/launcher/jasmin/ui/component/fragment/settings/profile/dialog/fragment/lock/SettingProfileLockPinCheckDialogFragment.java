package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.lock;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingAccountPinCheckDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseProfileDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.TextButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.TextButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment.KEY_PROFILE;

public class SettingProfileLockPinCheckDialogFragment extends SettingBaseProfileDialogFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton<SettingProfileLockPinCheckDialogFragment.OkCancelButton> {
    public static final String CLASS_NAME = SettingProfileLockPinCheckDialogFragment.class.getName();
    private static final String TAG = SettingProfileLockPinCheckDialogFragment.class.getSimpleName();

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private OnPinCheckCompleteListener mOnPinCheckCompleteListener;

    private PasswordView password;

    private SettingTaskManager settingTaskManager;
    private MbsDataTask checkPinTask;
    private MbsDataTask profileLockTask;

    public SettingProfileLockPinCheckDialogFragment() {
    }

    public static SettingProfileLockPinCheckDialogFragment newInstance(Bundle bundle) {
        SettingProfileLockPinCheckDialogFragment fragment = new SettingProfileLockPinCheckDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setOnPinCheckCompleteListener(OnPinCheckCompleteListener mOnPinCheckCompleteListener) {
        this.mOnPinCheckCompleteListener = mOnPinCheckCompleteListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_setting_profile_lock_pin_check, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        settingTaskManager = new SettingTaskManager(getFragmentManager(), getTvOverlayManager());

        initProgressbar(view);
        initPasswordView(view);
        initButtons(view);
    }

    protected void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    public void showProgressbar(boolean isLoading) {
        if (isLoading) {
            mProgressBarManager.show();
        } else {
            mProgressBarManager.hide();
        }
    }

    private void initPasswordView(View view) {
        password = view.findViewById(R.id.pin);
        password.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyInputChange, isFull : " + isFull + ", input : " + input);
                }

                if (isFull) {
                    checkPinCode(input);
                }
            }
        });
    }

    private void initButtons(View view) {
        TextButtonPresenter textButtonPresenter = new TextButtonPresenter();
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(textButtonPresenter);
        objectAdapter.add(OkCancelButton.FORGOT_PIN);
        objectAdapter.add(OkCancelButton.CANCEL);

        TextButtonBridgeAdapter textButtonBridgeAdapter = new TextButtonBridgeAdapter(objectAdapter);
        textButtonBridgeAdapter.setListener(this);
        HorizontalGridView buttonsGridView = view.findViewById(R.id.buttons_gridView);
        buttonsGridView.setAdapter(textButtonBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_horizontal_spacing);
        buttonsGridView.setHorizontalSpacing(spacing);
    }

    private void checkPinCode(String password) {
        ProfileInfo profileInfo = getArguments().getParcelable(KEY_PROFILE);
        checkPinTask = settingTaskManager.checkProfilePinCode(
                profileInfo.getProfileId(), password,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        showProgressbar(loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onFailed");
                        }

                        settingTaskManager.showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onSuccess, result : " + result);
                        }

                        if (result) {
                            offProfileLockCode();
                        } else {
                            setPinError();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        settingTaskManager.showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingProfileLockPinCheckDialogFragment.this.getContext();
                    }
                });
    }

    private void offProfileLockCode() {
        ProfileInfo profileInfo = getArguments().getParcelable(KEY_PROFILE);
        profileLockTask = settingTaskManager.putProfileLock(
                profileInfo.getProfileId(), "N", "",
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "needLoading");
                        }

                        showProgressbar(loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed");
                        }

                        settingTaskManager.showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSuccess, result : " + result);
                        }

                        mOnPinCheckCompleteListener.onPinCheckComplete();
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        settingTaskManager.showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingProfileLockPinCheckDialogFragment.this.getContext();
                    }
                });
    }

    private void setPinError() {
        getView().findViewById(R.id.description).setVisibility(View.VISIBLE);
        password.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (checkPinTask != null) {
            checkPinTask.cancel(true);
        }

        if (profileLockTask != null) {
            profileLockTask.cancel(true);
        }

        mOnPinCheckCompleteListener = null;
    }

    @Override
    protected ProfileInfo getProfileInfo() {
        return getArguments().getParcelable(KEY_PROFILE);
    }

    @Override
    protected void updateProfile(PushMessageReceiver.ProfileUpdateType type) {
        super.updateProfile(type);

        if (Log.INCLUDE) {
            Log.d(TAG, "updateProfile, type : " + type);
        }

        dismiss();
    }

    @Override
    public boolean onClickButton(OkCancelButton item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        int type = item.getType();
        if (type == OkCancelButton.FORGOT_PIN.getType()) {
            showAccountPinCheckDialogFragment();
        } else {
            dismiss();
        }

        return true;
    }

    private void showAccountPinCheckDialogFragment() {
        SettingAccountPinCheckDialogFragment fragment = SettingAccountPinCheckDialogFragment.newInstance(getArguments());
        fragment.setOnPinCheckCompleteListener(new SettingAccountPinCheckDialogFragment.OnPinCheckCompleteListener() {
            @Override
            public void onPinCheckComplete() {
                mOnPinCheckCompleteListener.onPinCheckComplete();
                fragment.dismiss();
                dismiss();
            }
        });
        getTvOverlayManager().showDialogFragment(SettingAccountPinCheckDialogFragment.CLASS_NAME, fragment);
    }

    protected enum OkCancelButton implements TextButtonPresenter.TextButtonItem {
        FORGOT_PIN(0, R.string.forgot_pin), CANCEL(1, R.string.cancel);

        private int type;
        private int title;

        OkCancelButton(int type, int title) {
            this.type = type;
            this.title = title;
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public int getTitle() {
            return title;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }

    public interface OnPinCheckCompleteListener {
        void onPinCheckComplete();
    }
}
