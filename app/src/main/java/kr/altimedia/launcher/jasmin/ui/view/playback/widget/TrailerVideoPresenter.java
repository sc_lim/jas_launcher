/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.widget;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.ContentStreamDeserializer;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Created by mc.kim on 06,05,2020
 */
public class TrailerVideoPresenter extends Presenter implements View.OnFocusChangeListener {
    final TrailerDataProvider mTrailerDataProvider;

    public TrailerVideoPresenter(TrailerDataProvider trailerDataProvider) {
        this.mTrailerDataProvider = trailerDataProvider;
    }

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_trailer_video, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        StreamInfo info = (StreamInfo) item;
        mTrailerDataProvider.getThumbnail(info, new TrailerDataProvider.ResultCallback() {
            @Override
            public void onThumbnailLoaded(Drawable drawable) {
                super.onThumbnailLoaded(drawable);
                vh.poster.setImageDrawable(drawable);
            }
        });


        if (ContentStreamDeserializer.TITLE_MAIN_PREVIEW.equalsIgnoreCase(info.getTitle())
                && info.getPreviewInfo() != null) {
            long previewTime = info.getPreviewInfo().getFreePreviewDuration();
            Context context = vh.titleView.getContext();
            String minData = context.getString(R.string.minPreview);
            vh.titleView.setText(getMinValue(previewTime) + minData);
        } else {
            vh.titleView.setText(info.getTitle());
        }
        vh.view.setOnFocusChangeListener(this);
        vh.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTrailerDataProvider.onSelectedThumbnail(info);
            }
        });

        vh.mPlayingIcon.setVisibility(mTrailerDataProvider.isCurrentPlayingVideo(info) ? View.VISIBLE : View.GONE);
    }

    private long getMinValue(long timeMillis) {
        return timeMillis / TimeUtil.MIN;
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.view.setOnFocusChangeListener(null);
    }

    private class ViewHolder extends Presenter.ViewHolder {
        private ImageView poster;
        private TextView titleView;
        private ImageView mPlayingIcon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            poster = view.findViewById(R.id.poster);
            titleView = view.findViewById(R.id.titleLayer);
            mPlayingIcon = view.findViewById(R.id.playingIcon);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        Animator animator = loadAnimator(v.getContext(), hasFocus ? R.animator.scale_up_episode_vod : R.animator.scale_down_50);
        animator.setTarget(v);
        animator.start();
    }

    private Animator loadAnimator(Context context, int resId) {
        Animator animator = AnimatorInflater.loadAnimator(context, resId);
        return animator;
    }

}
