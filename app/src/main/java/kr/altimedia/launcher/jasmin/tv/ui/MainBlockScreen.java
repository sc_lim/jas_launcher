/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.app.ProgressBarManager;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.JasTvView;

public class MainBlockScreen extends BlockScreen {
    private static final String TAG = MainBlockScreen.class.getSimpleName();
    private static final boolean INCLUDE = true;

    private static final int IFRAME_CLEAN = 0;
    private static final int IFRAME_BLOCK_CHANNEL = 1;
    private static final int IFRAME_BLOCK_PROGRAM = 2;
    private static final int IFRAME_BLOCK_UNSUBSCRIBED = 3;
    private static final int IFRAME_AUDIO_CHANNEL = 4;

    private ImageView main_iframe;
    private TextView main_iframe_text1;
    private TextView main_iframe_text2;
    private TextView main_iframe_text3;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    public MainBlockScreen(Context context) {
        this(context, null);
    }

    public MainBlockScreen(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainBlockScreen(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public MainBlockScreen(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        main_iframe = findViewById(R.id.main_iframe);
        main_iframe_text1 = findViewById(R.id.main_iframe_text1);
        main_iframe_text2 = findViewById(R.id.main_iframe_text2);
        main_iframe_text3 = findViewById(R.id.main_iframe_text3);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View loadingView = inflater.inflate(R.layout.view_loading_eq, findViewById(R.id.audio_progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);

        //setIFrame(IFRAME_BLOCK_CHANNEL);
    }

    public void updateBlockScreen(JasTvView.TuningResult tuningResult) {
        int drawableId;
        String text1, text2, text3;
        boolean enableProgressBar = false;
        switch (tuningResult) {
            case TUNING_BLOCKED_CHANNEL:
                drawableId = R.drawable.iframe_blocked;
                text1 = getContext().getString(R.string.iframe_block_channel_title);
                text2 = "";
                text3 = "";
                break;
            case TUNING_BLOCKED_ADULT_CHANNEL:
                drawableId = R.drawable.iframe_blocked;
                text1 = getContext().getString(R.string.iframe_block_adult_channel_title);
                text2 = "";
                text3 = "";
                break;
            case TUNING_BLOCKED_PROGRAM:
                drawableId = R.drawable.iframe_parental;
                text1 = getContext().getString(R.string.iframe_block_program_title);
                text2 = "";
                text3 = "";
                break;
            case TUNING_BLOCKED_UNSUBSCRIBED:
                drawableId = R.drawable.iframe_unsubscribed;
                text1 = getContext().getString(R.string.iframe_unsubscribed_channel_title);
                text2 = getContext().getString(R.string.iframe_unsubscribed_channel_msg1);
                text3 = "";
                break;
            case TUNING_SUCCESS_AUDIO:
                drawableId = R.drawable.iframe_audio_ch;
                text1 = "";
                text2 = "";
                text3 = "";
                enableProgressBar = true;
                break;
            case TUNING_FAIL:
                drawableId = R.drawable.iframe_error;
                text1 = tuningResult.getErrorTitle();
                text2 = tuningResult.getErrorMessage();
                text3 = "";
                break;
            default:
                drawableId = 0;
                text1 = "";
                text2 = "";
                text3 = "";
                break;
        }

        if (enableProgressBar) {
            mProgressBarManager.show();
        } else {
            mProgressBarManager.hide();
        }

        if (drawableId != 0) {
            main_iframe.setImageResource(drawableId);
            main_iframe_text1.setText(text1);
            main_iframe_text2.setText(text2);
            main_iframe_text3.setText(text3);
            setVisibility(View.VISIBLE);
        } else {
            setVisibility(View.GONE);
        }
    }
}
