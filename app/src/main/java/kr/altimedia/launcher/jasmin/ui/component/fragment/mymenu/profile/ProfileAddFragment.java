/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileIcon;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileIconCategory;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfile;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class ProfileAddFragment extends Fragment implements View.OnUnhandledKeyEventListener{
    public static final String CLASS_NAME = ProfileAddFragment.class.getName();
    private static final String TAG = ProfileAddFragment.class.getSimpleName();

    private final String ERROR_DUPLICATED_NAME = "MBS_1050";
    private final String CAT_RECENTLY_USED = "1";
    private final String CAT_OTHER = "6";

    private ImageView profileIconView;
    private EditText inputProfileName;
    private TextView profileDesc;
    private TextView btnChangeProfileIcon;
    private TextView btnSave;
    private TextView btnCancel;
    private View rootView;

    private MbsDataTask dataTask;
    private ArrayList<UserProfile> profileList = new ArrayList<>();
    private String profileNewName = "";
    private final int MAX_TEXT_LENGTH = 16;

    private boolean disableHotKey = false;
    private ProfileIcon profileIcon;

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
        @Override
        public void afterTextChanged(Editable s) {
            String text = s.toString();
            onInputTextChanged(text);
        }
    };

    private int getNumber(int keyCode){
        int number = keyCode - KeyEvent.KEYCODE_0;
        return number;
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if(keyCode >= KeyEvent.KEYCODE_0 && keyCode <=KeyEvent.KEYCODE_9){
            if(inputProfileName.isFocused()) {
                if (event.getAction() == KeyEvent.ACTION_UP) {
                    try {
                        int newDigit = getNumber(keyCode);
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onUnhandledKeyEvent: newDigit=" + newDigit);
                        }
                        inputProfileName.append(Integer.toString(newDigit));
                        profileNewName = inputProfileName.getText().toString();
                    }catch (Exception e){
                    }
                }
                return true;
            }
        }

        if(disableHotKey){
            return false;
        }
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @NonNull
    public static ProfileAddFragment newInstance(ArrayList<UserProfile> profileList, boolean disableHotKey) {
        ProfileAddFragment fragment = new ProfileAddFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ProfileManageDialog.KEY_PROFILE_LIST, profileList);
        args.putBoolean(ProfileManageDialog.KEY_DISABLE_HOT_KEY, disableHotKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        if (dataTask != null) {
            dataTask.cancel(true);
            dataTask = null;
        }
        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
        if(inputProfileName != null) {
            showSoftKeyboard(false, inputProfileName);
        }

        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mymenu_profile_add, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        ArrayList pList = new ArrayList();
        disableHotKey = false;

        Bundle arguments = getArguments();
        if (arguments != null) {
            pList = arguments.getParcelableArrayList(ProfileManageDialog.KEY_PROFILE_LIST);
            disableHotKey = arguments.getBoolean(ProfileManageDialog.KEY_DISABLE_HOT_KEY);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated: pList=" + pList+", disableHotKey="+disableHotKey);
        }

        if (!profileList.isEmpty()) {
            profileList.clear();
        }
        if (pList != null) {
            profileList.addAll(pList);
        }

        profileIconView = view.findViewById(R.id.profileIcon);
        profileDesc = view.findViewById(R.id.profileDesc);
        loadProfileIconList();

        initButton(view);
        initInputView(view);
    }

    private void initButton(View view){
        btnChangeProfileIcon = view.findViewById(R.id.btnChangeProfileIcon);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);

        btnChangeProfileIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileIconSelectDialog dialog = ProfileIconSelectDialog.newInstance("");
                dialog.setOnProfileIconSelectedListener(new ProfileIconSelectDialog.OnProfileIconSelectedListener() {
                    @Override
                    public void onProfileIconSelected(ProfileIcon profileIcon) {
                        setProfileIcon(profileIcon);
                    }
                });
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        if(btnSave.isEnabled()){
                            btnSave.requestFocus();
                        }
                    }
                });
                dialog.showDialog(((ProfileManageDialog) getParentFragment()).getTvOverlayManager());
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProfile();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileManageDialog manageDialog = ((ProfileManageDialog) getParentFragment());
                if(manageDialog.isLinkFromMyMenu()){
                    ((ProfileManageDialog) ProfileAddFragment.this.getParentFragment()).hideProfileView();
                }else {
                    manageDialog.backToFragment();
                }
            }
        });
    }

    private void setTextMaxLength(){
        try {
            inputProfileName.setEms(MAX_TEXT_LENGTH);
            InputFilter[] currFilters = inputProfileName.getFilters();
            InputFilter[] newFilters = new InputFilter[currFilters.length + 1];
            System.arraycopy(currFilters, 0, newFilters, 0, currFilters.length);
            newFilters[currFilters.length] = new InputFilter.LengthFilter(MAX_TEXT_LENGTH);
            inputProfileName.setFilters(newFilters);
        }catch (Exception e) {
            inputProfileName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_TEXT_LENGTH)});
        }
    }

    private void initInputView(View view) {
        inputProfileName = view.findViewById(R.id.inputProfileName);
        setTextMaxLength();
        inputProfileName.post(new Runnable() {
            @Override
            public void run() {
                inputProfileName.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                inputProfileName.addTextChangedListener(textWatcher);
                inputProfileName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                        if(Log.INCLUDE){
                            Log.d(TAG, "inputProfileName: onEditorAction: actionId="+actionId+", keyEvent=" + keyEvent);
                        }
                        if ( actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_NONE) {
                            showSoftKeyboard(false, textView);
                            return onInputTextKey(KeyEvent.KEYCODE_DPAD_DOWN);
                        }
                        return true;
                    }
                });
                inputProfileName.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                        if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                            return false;
                        }
                        if(Log.INCLUDE){
                            Log.d(TAG, "inputProfileName: onKey: keyCode="+keyCode+", keyEvent=" + keyEvent);
                        }
                        return onInputTextKey(keyCode);
                    }
                });
                inputProfileName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean focused) {
                        if(focused){
                            showSoftKeyboard(true, inputProfileName);
                        }
                    }
                });

                inputProfileName.requestFocus();
            }
        });
    }

    private void showSoftKeyboard(boolean visible, View view){
        if(visible){
            InputMethodManager imm = (InputMethodManager)view.getContext().getSystemService(INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }else{
            InputMethodManager imm = (InputMethodManager)view.getContext().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }

    private void setButtonFocus(){
        if(Log.INCLUDE){
            Log.d(TAG, "setButtonFocus");
        }
        btnChangeProfileIcon.requestFocus();
//        if (btnSave.isEnabled()) {
//            btnSave.requestFocus();
//        } else {
//            btnCancel.requestFocus();
//        }
    }

    public boolean onInputTextKey(int keyCode) {
        if(Log.INCLUDE){
            Log.d(TAG, "onInputTextKey: keyCode="+keyCode);
        }
        if(keyCode == KeyEvent.KEYCODE_DPAD_DOWN){
            setButtonFocus();
            return true;
        }
        return false;
    }

    public void onInputTextChanged(String text) {
        profileNewName = text;
        boolean enable = false;
        if (text != null && !text.isEmpty() && !text.equalsIgnoreCase("")) {
            enable = true;
        }

        if (btnSave.isEnabled() != enable) {
            btnSave.setEnabled(enable);
        }
    }

    public void onInputTextLengthExceeded() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onInputTextLengthExceeded");
        }
    }

    private void setInputText(String s){
        profileNewName = s;
        inputProfileName.setText(s);
    }

    private void setProfileIcon(ProfileIcon icon){
        if(icon != null){
            this.profileIcon = icon;
            profileIconView.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        Glide.with(profileIconView.getContext())
                                .load(profileIcon.getPath()).placeholder(R.drawable.profile_default).
                                error(R.drawable.profile_default).diskCacheStrategy(DiskCacheStrategy.DATA)
                                .centerCrop()
                                .into(profileIconView);
                    }catch(Exception e){
                    }
                }
            });
        }
    }

    private void loadProfileIconList() {
        UserDataManager userDataManager = new UserDataManager();
        dataTask = userDataManager.getProfileIconCategoryList(
                AccountManager.getInstance().getLocalLanguage(),
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                new MbsDataProvider<String, List<ProfileIconCategory>>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfileIconList: onFailed");
                        }
                    }

                    @Override
                    public void onSuccess(String key, List<ProfileIconCategory> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfileIconList: onSuccess: result=" + result);
                        }

                        ProfileIcon icon = null;
                        if(result != null) {
                            for (ProfileIconCategory category : result) {
                                if(CAT_OTHER.equals(category.getCode())){
                                    try {
                                        List<ProfileIcon> list = category.getProfileIconList();
                                        if (list != null && list.size() > 0) {
                                            Random r = new Random();
                                            int idx = r.nextInt(list.size() - 1);
                                            if (idx < list.size()) {
                                                icon = list.get(idx);
                                            }else{
                                                icon = list.get(0);
                                            }
                                            break;
                                        }
                                    }catch(Exception e){
                                    }
                                }
                            }
                        }
                        setProfileIcon(icon);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }
                });
    }

    private void addProfile() {
//        String profilePic = UserProfileBuilder.getUniqueProfileIcon(profileList);
        String profileIconId = "";
        if(profileIcon != null){
            profileIconId = profileIcon.getId();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "addProfile: name=" + profileNewName + ", icon=" + profileIconId);
        }
        UserDataManager userDataManager = new UserDataManager();
        userDataManager.postProfile(
                AccountManager.getInstance().getSaId(),
                profileNewName,
                profileIconId,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        Context context = ProfileAddFragment.this.getContext();
                        JasminToast.makeToast(context, R.string.saved);

                        notifyProfileAdded(profileNewName);

                        ((ProfileManageDialog) getParentFragment()).hideProfileView(profileNewName);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if(ERROR_DUPLICATED_NAME.equals(errorCode)){
                            profileDesc.setTextColor(getResources().getColor(R.color.color_FFA67C52, null));
                            profileDesc.setText(R.string.profile_duplicated_error);

                            setInputText("");

                        }else {
                            profileDesc.setTextColor(getResources().getColor(R.color.color_80E6E6E6, null));
                            profileDesc.setText(R.string.add_profile_desc);

                            setInputText("");

                            ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                            errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                        }
                    }
                });
    }

    private void notifyProfileAdded(String name){
        Intent intent = new Intent();
        intent.setAction(ProfileManageDialog.ACTION_MY_PROFILE_ADDED);
        intent.putExtra(ProfileManageDialog.KEY_PROFILE_NAME, name);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }
}
