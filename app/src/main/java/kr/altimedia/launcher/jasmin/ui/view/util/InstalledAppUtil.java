/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.fragment.installedAppList.AppSortMode;

public class InstalledAppUtil {
    private static final String TAG = InstalledAppUtil.class.getSimpleName();
    public static final String CLASS_NAME = InstalledAppUtil.class.getName();

    public static final int CATEGORY_APP = ApplicationInfo.CATEGORY_UNDEFINED;
    public static final int CATEGORY_GAME = ApplicationInfo.CATEGORY_GAME;

    private final String GOOGLE_MUSIC = "com.google.android.music";
    private final String YOUTUBE_MUSIC = "com.google.android.youtube.tvmusic";

    //1. Play store / 2. Play Movies&TV / 3. YouTube / 4. Play Music / 5. Play Games
    private ArrayList<String> googlePackageList = new ArrayList<>(Arrays.asList(
            "com.android.vending", "com.google.android.videos", "com.google.android.youtube.tv", GOOGLE_MUSIC, "com.google.android.play.games"));

    public class LastTimeUsedComparator implements Comparator<InstalledApplicationInfo> {
        @Override
        public final int compare(InstalledApplicationInfo a, InstalledApplicationInfo b) {
            return (int) ((b.getLastTimeUsed() - a.getLastTimeUsed()) / 1000);
        }
    }

    public InstalledAppUtil() {
    }

    public ArrayList<AppInfo> getInstalledAppList(Context mContext, boolean isLoadIcon, AppSortMode type) {
        if (type == AppSortMode.SORT_EDIT) {
            return getInstalledAppListByUserEdit(mContext, isLoadIcon);
        }

        ArrayList<InstalledApplicationInfo> applicationList = getInstalledApplicationListWithTime(mContext);

        Comparator mComparator = getComparator(mContext, type);
        if (mComparator != null) {
            Collections.sort(applicationList, mComparator);
        }

        return getAppList(mContext, isLoadIcon, applicationList);
    }

    private ArrayList<AppInfo> getInstalledAppListByUserEdit(Context mContext, boolean isLoadIcon) {
        ArrayList<AppInfo> userList = new ArrayList<>();

        //TODO get user list from UP

        if (userList != null && userList.size() > 0) {
            return userList;
        }

        return getInstalledAppList(mContext, isLoadIcon, AppSortMode.SORT_ALPHABET);
    }

    public void setInstalledAppListByUserEdit(ArrayList<AppInfo> list) {
        //TODO set UP
    }

    private Comparator getComparator(Context mContext, AppSortMode type) {
        if (type == AppSortMode.SORT_ALPHABET) {
            return new ApplicationInfo.DisplayNameComparator(mContext.getPackageManager());
        } else if (type == AppSortMode.SORT_RECENCY) {
            return new LastTimeUsedComparator();
        }

        return null;
    }

    private ArrayList<InstalledApplicationInfo> getInstalledApplicationList(Context mContext, boolean isLoadIcon) {
        PackageManager pm = mContext.getPackageManager();
        final Intent appIntent = new Intent(Intent.ACTION_MAIN, null);
        appIntent.addCategory(Intent.CATEGORY_LEANBACK_LAUNCHER);
        List<ResolveInfo> resolveInfoList = pm.queryIntentActivities(appIntent, 0);
        int size = resolveInfoList.size();
        if (Log.INCLUDE) {
            Log.d(TAG, "getInstalledAppList() | size : " + size);
        }

        ArrayList<InstalledApplicationInfo> applicationList = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            ResolveInfo resolveInfo = resolveInfoList.get(i);
            ApplicationInfo appInfo = resolveInfo.activityInfo.applicationInfo;

            InstalledApplicationInfo installedApplicationInfo = new InstalledApplicationInfo(appInfo);
            applicationList.add(installedApplicationInfo);
        }
        return applicationList;
    }

    private ArrayList<InstalledApplicationInfo> getInstalledApplicationListWithTime(Context mContext) {
        UsageStatsManager mUsageStatsManager = (UsageStatsManager) mContext.getSystemService(Context.USAGE_STATS_SERVICE);
        long current = JasmineEpgApplication.SystemClock().currentTimeMillis();
        long begin = current - (TimeUtil.HOUR) * 24;

        List<UsageStats> stats =
                mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, begin, current);

        if (stats == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "stats == null, so return null");
            }

            return null;
        }


        HashMap<String, InstalledApplicationInfo> usageStatsHashMap = new HashMap<>();

        final int statCount = stats.size();
        for (int i = 0; i < statCount; i++) {
            UsageStats pkgStats = stats.get(i);
            try {
                ApplicationInfo appInfo = mContext.getPackageManager().getApplicationInfo(pkgStats.getPackageName(), 0);
                String label = appInfo.loadLabel(mContext.getPackageManager()).toString();

                PackageManager packageManager = mContext.getPackageManager();
                Intent intent = packageManager.getLeanbackLaunchIntentForPackage(appInfo.packageName);

                if (intent != null) {
                    Set<String> categories = intent.getCategories();
                    for (String category : categories) {
                        if (category.equalsIgnoreCase(Intent.CATEGORY_LEANBACK_LAUNCHER)) {
                            InstalledApplicationInfo installedApplicationInfo = new InstalledApplicationInfo(appInfo);
                            installedApplicationInfo.setLastTimeUsed(pkgStats.getLastTimeUsed());

                            if (usageStatsHashMap.containsKey(label)) {
                                usageStatsHashMap.replace(label, installedApplicationInfo);
                            } else {
                                usageStatsHashMap.put(label, installedApplicationInfo);
                            }
                            break;
                        }
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }


        ArrayList<InstalledApplicationInfo> applicationList = getInstalledApplicationList(mContext, true);
        ArrayList<InstalledApplicationInfo> storeList = (ArrayList<InstalledApplicationInfo>) applicationList.clone();

        if (Log.INCLUDE) {
            Log.d(TAG, "usageStatsHashMap size : " + usageStatsHashMap.size() + ", applicationList size : " + applicationList.size());
        }

        boolean hasYoutubeMusic = false;
        for (InstalledApplicationInfo info : usageStatsHashMap.values()) {
            for (InstalledApplicationInfo applicationInfo : applicationList) {
                if (applicationInfo.packageName.equals(YOUTUBE_MUSIC)) {
                    hasYoutubeMusic = true;
                }

                if (applicationInfo.packageName.equalsIgnoreCase(info.packageName)) {
                    boolean isRemoved = storeList.remove(applicationInfo);
                    if (isRemoved) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "replace, app with time : " + info.packageName + ", lastTimeUsed : " + info.getLastTimeUsed());
                        }

                        storeList.add(info);
                        continue;
                    }
                }
            }
        }

        if (hasYoutubeMusic) {
            if (Log.INCLUDE) {
                Log.d(TAG, "replace, [" + GOOGLE_MUSIC + "] to [" + YOUTUBE_MUSIC + "]");
            }

            int index = googlePackageList.indexOf(GOOGLE_MUSIC);
            googlePackageList.remove(GOOGLE_MUSIC);
            googlePackageList.add(index, YOUTUBE_MUSIC);

            for (InstalledApplicationInfo info : storeList) {
                if (info.packageName.equals(GOOGLE_MUSIC)) {
                    storeList.remove(info);
                    break;
                }
            }
        }

        return storeList;
    }

    private ArrayList<AppInfo> getAppList(Context mContext, boolean isLoadIcon, ArrayList<InstalledApplicationInfo> applicationList) {
        ArrayList<AppInfo> appList = new ArrayList<>();
        AppInfo[] googleList = new AppInfo[googlePackageList.size()];

        PackageManager pm = mContext.getPackageManager();
        for (int i = 0; i < applicationList.size(); i++) {
            InstalledApplicationInfo appInfo = applicationList.get(i);
            String pkgName = appInfo.packageName; // 패키지
            if (pkgName.equalsIgnoreCase(mContext.getPackageName())) {
                continue;
            }

            int uid = appInfo.uid;
            CharSequence title = appInfo.loadLabel(pm);
            Drawable icon = null;

            if (isLoadIcon) {
                try {
                    icon = pm.getApplicationBanner(pkgName);
                    if (icon == null) {
                        try {
                            Intent intent = pm.getLeanbackLaunchIntentForPackage(pkgName);
                            if (intent != null) {
                                icon = pm.getActivityBanner(intent);
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                    if (icon == null) {
                        icon = pm.getApplicationLogo(appInfo);
                    }

                    if (icon == null) {
                        try {
                            Intent intent = pm.getLeanbackLaunchIntentForPackage(pkgName);
                            if (intent != null) {
                                icon = pm.getActivityLogo(intent);
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                    if (icon == null) {
                        icon = pm.getApplicationIcon(appInfo);
                    }


                    if (icon == null) {
                        try {
                            Intent intent = pm.getLeanbackLaunchIntentForPackage(pkgName);
                            if (intent != null) {
                                icon = pm.getActivityIcon(intent);
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                    if (icon == null) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "none icon... if you want, set default icon");
                        }
//                        icon = mContext.getResources().getDrawable();
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "getAppList: AppInfo[" + i + "]: " + pkgName + " - " + title);
            }

            AppInfo info = new AppInfo(uid, (String) title, pkgName, appInfo.category, icon, appInfo.getLastTimeUsed());
            int index = googlePackageList.indexOf(pkgName);
            if (index > -1) {
                googleList[index] = info;
            } else {
                appList.add(info);
            }
        }

        mergeList(appList, googleList);

        return appList;
    }

    private void mergeList(ArrayList<AppInfo> appList, AppInfo[] googleList) {
        int idx = 0;
        for (int i = 0; i < googleList.length; i++) {
            AppInfo appInfo = googleList[i];
            if (appInfo != null) {
                appList.add(idx, appInfo);
                idx++;
            } else {
//                Log.d(TAG, "mergeList, google app : " + appInfo.getTitle() + ", doesn't exist");
            }
        }
    }

    private class InstalledApplicationInfo extends ApplicationInfo {
        private long LastTimeUsed = 0L;

        public InstalledApplicationInfo(ApplicationInfo info) {
            super(info);
        }

        public void setLastTimeUsed(long lastTimeUsed) {
            LastTimeUsed = lastTimeUsed;
        }

        public long getLastTimeUsed() {
            return LastTimeUsed;
        }
    }

    public class AppInfo {
        private int uid;
        private String title;
        private String pkgName;
        private int category;
        private Drawable icon;
        private long lastUsedTime;

        public AppInfo(int uid, String title, String pkgName, int category, Drawable icon, long lastUsedTime) {
            this.uid = uid;
            this.title = title;
            this.pkgName = pkgName;
            this.category = category;
            this.icon = icon;
            this.lastUsedTime = lastUsedTime;
        }

        public int getUid() {
            return uid;
        }

        public String getTitle() {
            return title;
        }

        public String getPkgName() {
            return pkgName;
        }

        public int getCategory() {
            return category;
        }

        public Drawable getIcon() {
            return icon;
        }

        public long getLastUsedTime() {
            return lastUsedTime;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AppInfo appInfo = (AppInfo) o;
            return uid == appInfo.uid &&
                    category == appInfo.category &&
                    lastUsedTime == appInfo.lastUsedTime &&
                    Objects.equals(title, appInfo.title) &&
                    Objects.equals(pkgName, appInfo.pkgName) &&
                    Objects.equals(icon, appInfo.icon);
        }

        @Override
        public int hashCode() {
            return Objects.hash(uid, title, pkgName, category, icon, lastUsedTime);
        }
    }
}
