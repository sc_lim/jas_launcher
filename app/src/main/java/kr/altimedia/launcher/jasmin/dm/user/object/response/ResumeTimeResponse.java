/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.ResumeTimeResult;

/**
 * Created by mc.kim on 11,05,2020
 */
public class ResumeTimeResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private ResumeTimeResult resumeTimeResult;

    protected ResumeTimeResponse(Parcel in) {
        super(in);
        resumeTimeResult = in.readTypedObject(ResumeTimeResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(resumeTimeResult, flags);
    }

    public ResumeTimeResult getResumeTimeResult() {
        return resumeTimeResult;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ResumeTimeResponse> CREATOR = new Creator<ResumeTimeResponse>() {
        @Override
        public ResumeTimeResponse createFromParcel(Parcel in) {
            return new ResumeTimeResponse(in);
        }

        @Override
        public ResumeTimeResponse[] newArray(int size) {
            return new ResumeTimeResponse[size];
        }
    };

    @Override
    public String toString() {
        return "ResumeTimeResponse{" +
                "resumeTimeResult=" + resumeTimeResult +
                '}';
    }
}
