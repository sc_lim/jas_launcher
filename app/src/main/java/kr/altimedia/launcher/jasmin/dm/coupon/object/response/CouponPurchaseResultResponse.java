/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseResult;

public class CouponPurchaseResultResponse extends BaseResponse {
    public static final Creator<CouponPurchaseResultResponse> CREATOR = new Creator<CouponPurchaseResultResponse>() {
        @Override
        public CouponPurchaseResultResponse createFromParcel(Parcel in) {
            return new CouponPurchaseResultResponse(in);
        }

        @Override
        public CouponPurchaseResultResponse[] newArray(int size) {
            return new CouponPurchaseResultResponse[size];
        }
    };
    @Expose
    @SerializedName("data")
    private CouponPurchaseResult result;

    protected CouponPurchaseResultResponse(Parcel in) {
        super(in);
        result = in.readParcelable(CouponPurchaseResult.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(result, flags);
    }

    @NonNull
    @Override
    public String toString() {
        return "CouponPurchaseResultResponse{" +
                "result='" + result + '\'' +
                '}';
    }

    public CouponPurchaseResult getResult() {
        return result;
    }
}
