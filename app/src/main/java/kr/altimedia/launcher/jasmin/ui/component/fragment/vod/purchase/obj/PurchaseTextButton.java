package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj;

public class PurchaseTextButton {
    private int value;
    private boolean isEnabled;

    public PurchaseTextButton(int value, boolean isEnabled) {
        this.value = value;
        this.isEnabled = isEnabled;
    }

    public int getValue() {
        return value;
    }

    public boolean isEnabled() {
        return isEnabled;
    }
}
