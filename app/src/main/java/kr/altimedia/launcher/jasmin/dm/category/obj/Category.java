/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.category.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.tvmodule.util.StringUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import kr.altimedia.launcher.jasmin.dm.def.CategoryType;
import kr.altimedia.launcher.jasmin.dm.def.CategoryTypeDeserializer;

/**
 * Created by mc.kim on 08,05,2020
 */
public class Category implements Parcelable {

    @Expose
    @SerializedName("version")
    private String version;
    @Expose
    @SerializedName("adult")
    private String adult;
    @Expose
    @SerializedName("categoryId")
    private String categoryID;
    @Expose
    @SerializedName("categoryName")
    private String categoryName;
    @Expose
    @SerializedName("categoryParentId")
    private String categoryParentID;
    @Expose
    @SerializedName("linkInfo")
    private String linkInfo;
    @Expose
    @SerializedName("linkType")
    @JsonAdapter(CategoryTypeDeserializer.class)
    private CategoryType linkType;
    @Expose
    @SerializedName("orderNumber")
    private int orderNumber;
    @Expose
    @SerializedName("visibility")
    private String visibility;
    @Expose
    @SerializedName("selectedIcon")
    private String selectedIcon;
    @Expose
    @SerializedName("unselectedIcon")
    private String unselectedIcon;

    @Expose
    @SerializedName("isHome")
    private boolean isHome = false;

    @Expose
    @SerializedName("contentBannerFlag")
    private String contentBannerFlag = "N";

    public Category(CategoryType linkType) {
        this.linkType = linkType;
        this.categoryID = "0";
    }


    protected Category(Parcel in) {
        version = in.readString();
        adult = in.readString();
        categoryID = in.readString();
        categoryName = in.readString();
        categoryParentID = in.readString();
        linkInfo = in.readString();
        linkType = (CategoryType) in.readValue(CategoryType.class.getClassLoader());
        orderNumber = in.readInt();
        visibility = in.readString();
        selectedIcon = in.readString();
        unselectedIcon = in.readString();
        isHome = in.readByte() != 0;
        contentBannerFlag = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(version);
        dest.writeString(adult);
        dest.writeString(categoryID);
        dest.writeString(categoryName);
        dest.writeString(categoryParentID);
        dest.writeString(linkInfo);
        dest.writeValue(linkType);
        dest.writeInt(orderNumber);
        dest.writeString(visibility);
        dest.writeString(selectedIcon);
        dest.writeString(unselectedIcon);
        dest.writeByte((byte) (isHome ? 1 : 0));
        dest.writeString(contentBannerFlag);
    }

    public boolean isHome() {
        return isHome;
    }

    public void setHome(boolean home) {
        isHome = home;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public String getCategoryVersion() {
        return version;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getCategoryParentID() {
        return categoryParentID;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLinkInfo() {
        return linkInfo;
    }

    public void setOrderNumber(int orderNumber) {
//        this.orderNumber = orderNumber + 1;
    }

    public CategoryType getLinkType() {
        if (linkType == null) {
            return CategoryType.SubCategory;
        }
        return linkType;
    }

    public String getSelectedIcon() {
        return selectedIcon;
    }

    public String getUnselectedIcon() {
        return unselectedIcon;
    }

    public int getOrderNumber() {
        return orderNumber ;
    }

    public String getVisibility() {
        return visibility;
    }

    public String getContentBannerFlag() {
        return contentBannerFlag;
    }

    public boolean isSupportedBanner() {
        if (StringUtils.nullToEmpty(contentBannerFlag).isEmpty()) {
            return false;
        }
        return contentBannerFlag.equalsIgnoreCase("Y");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return orderNumber == category.orderNumber &&
                Objects.equals(version, category.version) &&
                Objects.equals(adult, category.adult) &&
                Objects.equals(categoryID, category.categoryID) &&
                Objects.equals(categoryName, category.categoryName) &&
                Objects.equals(categoryParentID, category.categoryParentID) &&
                Objects.equals(linkInfo, category.linkInfo) &&
                linkType == category.linkType &&
                Objects.equals(visibility, category.visibility) &&
                Objects.equals(selectedIcon, category.selectedIcon) &&
                Objects.equals(unselectedIcon, category.unselectedIcon) &&
                Objects.equals(contentBannerFlag, category.contentBannerFlag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(version, adult, categoryID, categoryName, categoryParentID, linkInfo, linkType, orderNumber, visibility, selectedIcon, unselectedIcon, contentBannerFlag);
    }

    @Override
    public String toString() {
        return "Category{" +
                "version='" + version + '\'' +
                ", adult='" + adult + '\'' +
                ", categoryID='" + categoryID + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", categoryParentID='" + categoryParentID + '\'' +
                ", linkInfo='" + linkInfo + '\'' +
                ", linkType=" + linkType +
                ", orderNumber=" + orderNumber +
                ", visibility='" + visibility + '\'' +
                ", selectedIcon='" + selectedIcon + '\'' +
                ", unselectedIcon='" + unselectedIcon + '\'' +
                ", isHome=" + isHome +
                ", contentBannerFlag='" + contentBannerFlag + '\'' +
                '}';
    }
}
