/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel;

import java.util.ArrayList;

/**
 * Created by mc.kim on 22,06,2020
 */
public interface SidePanelOptionCallback {
    ArrayList<String> getSubtitleList();

    ArrayList<String> getAudioList();

    void setSubtitle(String lang);

    void setAudio(String lang);

    String getCurrentSubtitleLanguage();

    String getCurrentAudioLanguage();

    String getSubtitleStyle();

    void setSubtitleStyle(String style);

    boolean isFavoriteMenuVisible();

    void requestFavorite(boolean isAdd);

    void reset();
}
