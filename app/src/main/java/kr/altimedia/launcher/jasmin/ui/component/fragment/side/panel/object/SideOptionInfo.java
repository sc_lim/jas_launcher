/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelType;

/**
 * Created by mc.kim on 20,02,2020
 */
public class SideOptionInfo implements Parcelable {
    @Expose
    @SerializedName("OptionType")
    private SidePanelType type;
    @Expose
    @SerializedName("Category")
    private ArrayList<SideOptionCategory> categoryList;

    protected SideOptionInfo(Parcel in) {
        type = (SidePanelType) in.readValue(SidePanelType.class.getClassLoader());
        categoryList = in.createTypedArrayList(SideOptionCategory.CREATOR);
    }

    public SidePanelType getType() {
        return type;
    }

    public ArrayList<SideOptionCategory> getCategoryList() {
        return categoryList;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(type);
        dest.writeTypedList(categoryList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SideOptionInfo> CREATOR = new Creator<SideOptionInfo>() {
        @Override
        public SideOptionInfo createFromParcel(Parcel in) {
            return new SideOptionInfo(in);
        }

        @Override
        public SideOptionInfo[] newArray(int size) {
            return new SideOptionInfo[size];
        }
    };

    @Override
    public String toString() {
        return "SideOptionInfo{" +
                "type=" + type +
                ", categoryList=" + categoryList +
                '}';
    }
}
