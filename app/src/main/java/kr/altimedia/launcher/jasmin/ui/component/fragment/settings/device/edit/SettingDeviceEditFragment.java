/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.edit;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.TextButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.DeviceEditType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.TextButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.edit.SettingDeviceEditDialogFragment.KEY_DEVICE;

public class SettingDeviceEditFragment extends SettingDeviceEditBaseFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton<SettingDeviceEditFragment.DeviceEditButton>,
        SettingBaseButtonItemBridgeAdapter.OnKeyListener {
    public static final String CLASS_NAME = SettingDeviceEditFragment.class.getName();
    private final String TAG = SettingDeviceEditFragment.class.getSimpleName();

    private UserDevice deviceItem;
    private TextView doneButton;

    private SettingDeviceEditFragment() {
    }

    public static SettingDeviceEditFragment newInstance(Bundle bundle) {
        SettingDeviceEditFragment fragment = new SettingDeviceEditFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getResourceId() {
        return R.layout.fragment_device_edit;
    }

    @Override
    protected void initView(View view) {
        deviceItem = getArguments().getParcelable(KEY_DEVICE);

        TextView deviceName = view.findViewById(R.id.device_name);
        deviceName.setText(deviceItem.getDeviceName());

        initEditButton(view);
        initButton(view);
    }

    private void initEditButton(View view) {
        TextButtonPresenter textButtonPresenter = new TextButtonPresenter();
        textButtonPresenter.setResourceId(R.layout.presenter_text_button);
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(textButtonPresenter);
        objectAdapter.add(new DeviceEditButton(DeviceEditType.CHANGE, true));
        objectAdapter.add(new DeviceEditButton(DeviceEditType.DELETE, !deviceItem.isDeleteDisabled()));

        TextButtonBridgeAdapter buttonBridgeAdapter = new TextButtonBridgeAdapter(objectAdapter);
        buttonBridgeAdapter.setListener(this);
        buttonBridgeAdapter.setOnKeyListener(this);
        VerticalGridView gridView = view.findViewById(R.id.gridView);
        gridView.setAdapter(buttonBridgeAdapter);

        int gap = (int) getResources().getDimension(R.dimen.setting_profile_vertical_gap);
        gridView.setVerticalSpacing(gap);
    }

    private void initButton(View view) {
        doneButton = view.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOnChangeFragmentListener().onDismiss();
            }
        });
    }

    @Override
    public boolean onClickButton(SettingDeviceEditFragment.DeviceEditButton item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        OnChangeFragmentListener onChangeFragmentListener = getOnChangeFragmentListener();
        onChangeFragmentListener.onNextFragment(item.getEditType());

        return true;
    }

    @Override
    public boolean onKey(int keyCode, boolean isConsumed, ItemBridgeAdapter.ViewHolder viewHolder) {
        int index = (int) viewHolder.getViewHolder().view.getTag(R.id.KEY_INDEX);
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_DOWN:
                if (index == DeviceEditType.CHANGE.getType()) {
                    if (deviceItem.isDeleteDisabled()) {
                        doneButton.requestFocus();
                        return true;
                    }
                } else if (index == DeviceEditType.DELETE.getType()) {
                    doneButton.requestFocus();
                    return true;
                }
                break;
        }

        return false;
    }

    protected class DeviceEditButton implements TextButtonPresenter.TextButtonItem {
        private DeviceEditType editType;
        private boolean isDeleteDisabled;

        public DeviceEditButton(DeviceEditType editType, boolean isDeleteDisabled) {
            this.editType = editType;
            this.isDeleteDisabled = isDeleteDisabled;
        }

        public DeviceEditType getEditType() {
            return editType;
        }

        @Override
        public int getType() {
            return editType.getType();
        }

        @Override
        public int getTitle() {
            return editType.getTitle();
        }

        @Override
        public boolean isEnabled() {
            return isDeleteDisabled;
        }
    }
}
