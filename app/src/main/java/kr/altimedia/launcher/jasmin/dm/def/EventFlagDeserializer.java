/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.def;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mc.kim on 27,07,2020
 */
public class EventFlagDeserializer implements JsonSerializer<List<String>>,
        JsonDeserializer<List<String>> {
    private final String FLAG_EVENT = "event";
    private final String FLAG_HOT = "hot";
    private final String FLAG_NEW = "new";
    private final String FLAG_PREMIUM = "premium";
    private final String FLAG_UHD = "uhd";

    /* Premium → New → Hot → Event → UHD*/


    @Override
    public JsonElement serialize(List<String> list, Type t,
                                 JsonSerializationContext jsc) {
        if (list.size() == 1) {
            return jsc.serialize(list.get(0));
        } else {
            return jsc.serialize(list);
        }
    }

    @Override
    public List<String> deserialize(JsonElement json, Type typeOfT,
                                    JsonDeserializationContext jsc)
            throws JsonParseException {
        List<String> result;

        if (json.isJsonArray()) {
            result = jsc.deserialize(json, typeOfT);
        } else {
            result = new ArrayList<>();
            String flag = jsc.deserialize(json, String.class);
            String[] flags = flag.split(",");
            for (String flagItem : flags) {
                result.add(flagItem.trim());
            }
        }

        List<String> lowerList = new ArrayList<>();
        for (String eventName : result) {
            String name = eventName.toLowerCase();
            lowerList.add(name);
        }

        List<String> resultList = filteredEvent(lowerList);

        return resultList;
    }

    private List<String> filteredEvent(List<String> lowerList) {
        List<String> resultList = new ArrayList<>();
        if (lowerList.contains(FLAG_PREMIUM)) {
            resultList.add(FLAG_PREMIUM);
        }

        if (lowerList.contains(FLAG_NEW)) {
            resultList.add(FLAG_NEW);
        }

        if (lowerList.contains(FLAG_HOT)) {
            resultList.add(FLAG_HOT);
        }

        if (lowerList.contains(FLAG_EVENT)) {
            resultList.add(FLAG_EVENT);
        }

        if (lowerList.contains(FLAG_UHD)) {
            resultList.add(FLAG_UHD);
        }
        if (resultList.size() > 3) {
            return resultList.subList(0, 3);
        } else {
            return resultList;
        }
    }
}
