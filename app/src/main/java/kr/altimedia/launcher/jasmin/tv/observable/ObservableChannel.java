/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.observable;

import androidx.databinding.BaseObservable;

import com.altimedia.tvmodule.dao.Channel;

public class ObservableChannel extends BaseObservable {
    public static final int FILED_ID_CHCH = 1;
    public static final int FILED_ID_ONLY_DATA = 2;

    private Channel currentChannel = null;
    private String currentChannelNum = "1";


    public void setValue(Channel channel) {
        this.currentChannel = channel;
        notifyPropertyChanged(FILED_ID_CHCH);
    }

    public Channel getCurrentChannel() {
        return currentChannel;
    }

    public void setValueForTest(int channelNum) {
        this.currentChannelNum = String.valueOf(channelNum);
//        notifyPropertyChanged(FILED_ID_ONLY_DATA);
        notifyChange();
    }

    public String getCurrentChannelForTest() {
        return currentChannelNum;
    }

}
