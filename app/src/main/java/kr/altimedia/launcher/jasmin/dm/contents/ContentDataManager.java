/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents;

import android.content.Context;

import com.altimedia.util.Log;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.category.MenuManager;
import kr.altimedia.launcher.jasmin.dm.contents.module.ContentModule;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PackageContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonListInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContentInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.OTUResponse;
import kr.altimedia.launcher.jasmin.dm.contents.remote.ContentRemote;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.util.task.VodTaskManager;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;

public class ContentDataManager extends BaseDataManager {
    private final String TAG = ContentDataManager.class.getSimpleName();
    private ContentRemote mContentRemote;

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        ContentModule contentModule = new ContentModule();
        Retrofit contentRetrofit = contentModule.provideContentModule(mOkHttpClient);
        mContentRemote = new ContentRemote(contentModule.provideContentApi(contentRetrofit));
    }

    @Override
    public String getLoginToken() {
        return AccountManager.getInstance().getLoginToken();
    }

    public MbsDataTask getContentsList(Context context, String language, String said, String profileId, String categoryId, String version, int startNo, int contentCnt, String contentBannerFlag,
                                       MbsDataProvider<String, List<Content>> mbsDataProvider) {

        List<Content> savedList = MenuManager.getInstance().getCategoryContent(context, categoryId, version);
        if (savedList != null) {
            mbsDataProvider.needLoading(false);
            mbsDataProvider.onSuccess(categoryId, savedList);
            return null;
        }

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, List<Content>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.getContentsList(language, said, profileId, categoryId, startNo, contentCnt, contentBannerFlag);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<Content> result) {
                MenuManager.getInstance().saveCategoryContents(context, categoryId, version, result);
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(categoryId);
        return mMbsDataTask;
    }


    public MbsDataTask getContentDetail(String language, String said, String profileId, String contentId, String categoryId, MbsDataProvider mbsDataProvider) {

        MbsDataTask mbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, ContentDetailInfo>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.getContentDetail(language, said, profileId, contentId, categoryId);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, ContentDetailInfo result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mbsDataTask.execute(contentId);
        return mbsDataTask;
    }

    public MbsDataTask getPackageDetail(String language,
                                        String said, String packageId, String profileId, String categoryId, MbsDataProvider<String, PackageContent> mbsDataProvider) {


        MbsDataTask mbsDataTask = new MbsDataTask(mbsDataProvider, new MbsTaskCallback<String, PackageContent>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.getPackageDetail(language, said, packageId, profileId, categoryId);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onTaskPostExecute(String key, PackageContent result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mbsDataTask.execute(packageId);
        return mbsDataTask;
    }


    public MbsDataTask getBannerList(String language, String said, String bannerPositionID, String categoryId, MbsDataProvider<String, List<Banner>> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(mbsDataProvider, new MbsTaskCallback<String, List<Banner>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.getBannerList(language, said, bannerPositionID, categoryId);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<Banner> result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(bannerPositionID);
        return mMbsDataTask;
    }

    public MbsDataTask getPromotionList(Context context, String language, String said, String bannerPositionID, String categoryId, String version, MbsDataProvider<String, List<Banner>> mbsDataProvider) {

        List<Banner> savedList = MenuManager.getInstance().getCategoryBannerList(context,
                categoryId, bannerPositionID, version);
        if (savedList != null) {
            mbsDataProvider.needLoading(false);
            mbsDataProvider.onSuccess(categoryId, savedList);
            return null;
        }

        MbsDataTask mMbsDataTask = new MbsDataTask(mbsDataProvider, new MbsTaskCallback<String, List<Banner>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.getBannerList(language, said, bannerPositionID, categoryId);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<Banner> result) {
                MenuManager.getInstance().saveCategoryBannerList(context, categoryId, bannerPositionID, version, result);
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(bannerPositionID);
        return mMbsDataTask;
    }

    public MbsDataTask getImageBannerList(Context context, String language, String said, String bannerPositionID, String categoryId, String version, MbsDataProvider<String, List<Banner>> mbsDataProvider) {


        List<Banner> savedList = MenuManager.getInstance().getCategoryBannerList(context,
                categoryId, bannerPositionID, version);
        if (savedList != null) {
            mbsDataProvider.needLoading(false);
            mbsDataProvider.onSuccess(categoryId, savedList);
            return null;
        }

        MbsDataTask mMbsDataTask = new MbsDataTask(mbsDataProvider, new MbsTaskCallback<String, List<Banner>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.getImageBannerList(language, said, bannerPositionID, categoryId);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<Banner> result) {
                MenuManager.getInstance().saveCategoryBannerList(context, categoryId, bannerPositionID, version, result);
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(bannerPositionID);
        return mMbsDataTask;
    }


    public MbsDataTask getSeriesList(String language, String said, String profileID, String seriesAssetId, String categoryId, String contentId, MbsDataProvider<String, SeriesContentInfo> contentsLoader) {
        MbsDataTask mMbsDataTask = new MbsDataTask(contentsLoader, new MbsTaskCallback<String, SeriesContentInfo>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.getSeriesList(language, said, profileID, seriesAssetId, categoryId, contentId);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                contentsLoader.needLoading(false);
                contentsLoader.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskFailed(int key) {
                contentsLoader.needLoading(false);
                contentsLoader.onFailed(key);
            }

            @Override
            public void onTaskPostExecute(String key, SeriesContentInfo result) {
                contentsLoader.needLoading(false);
                contentsLoader.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {

                contentsLoader.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                contentsLoader.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(seriesAssetId);
        return mMbsDataTask;
    }

    public MbsDataTask getSeasonList(String language, String said, String profileId, String seriesAssetId, String categoryId, MbsDataProvider<String, SeasonListInfo> contentsLoader) {
        MbsDataTask mMbsDataTask = new MbsDataTask(contentsLoader, new MbsTaskCallback<String, SeasonListInfo>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.getSeasonList(language, said, profileId, seriesAssetId, categoryId);
            }

            @Override
            public void onTaskFailed(int key) {
                contentsLoader.needLoading(false);
                contentsLoader.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                contentsLoader.needLoading(false);
                contentsLoader.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, SeasonListInfo result) {
                contentsLoader.needLoading(false);
                contentsLoader.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                contentsLoader.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                contentsLoader.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(seriesAssetId);
        return mMbsDataTask;
    }

    public MbsDataTask getRecentWatchingList(String language, String said, String profileId,
                                             int contentCnt, String categoryId, MbsDataProvider<String, List<Content>> contentsLoader) {
        MbsDataTask mbsDataTask = new MbsDataTask(contentsLoader, new MbsTaskCallback<String, List<Content>>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.getRecentWatchingList(language, said, profileId, contentCnt, categoryId);
            }

            @Override
            public void onTaskFailed(int key) {
                contentsLoader.needLoading(false);
                contentsLoader.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                contentsLoader.needLoading(false);
                contentsLoader.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<Content> result) {
                contentsLoader.needLoading(false);
                contentsLoader.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {

                contentsLoader.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                contentsLoader.needLoading(false);
                return null;
            }
        });
        mbsDataTask.execute(said);
        return mbsDataTask;
    }

    public MbsDataTask postFavoriteVod(String said, String profileId, String contentId, VodTaskManager.VodLikeType type,
                                       MbsDataProvider<String, Boolean> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Boolean>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.postVodLike(said, profileId, contentId, type);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask deleteFavoriteVod(String said, String profileId, String contentId, VodTaskManager.VodLikeType type,
                                         MbsDataProvider<String, Boolean> mbsDataProvider) {

        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, Boolean>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.deleteVodLike(said, profileId, contentId, type);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, Boolean result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }


    public MbsDataTask getOneTimeUrl(String said, String profileId,
                                     String payYN, String contentType,
                                     String contentPath, String movieId, String resumeYN, String categoryId, boolean isPreview, MbsDataProvider mbsDataProvider) {

        MbsDataTask mbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, OTUResponse>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mContentRemote.getOneTimeUrl(said, profileId, payYN, contentType, contentPath, movieId, resumeYN, categoryId, isPreview);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onTaskPostExecute(String key, OTUResponse result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mbsDataTask.execute(contentPath);
        return mbsDataTask;
    }


    public MbsDataTask getThumbnailData(String url, MbsDataProvider<String, String> mbsDataProvider) {
        if (Log.INCLUDE) {
            Log.d(TAG, "getThumbnailData : " + url);
        }
        MbsDataTask mbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, String>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException {
                Request request = new Request.Builder()
                        .url(url)
                        .build();
                Response response = mOkHttpClient.newCall(request).execute();
                String result = response.body().string();
                return result;
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onTaskPostExecute(String key, String result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mbsDataTask.execute(url);
        return mbsDataTask;
    }
}
