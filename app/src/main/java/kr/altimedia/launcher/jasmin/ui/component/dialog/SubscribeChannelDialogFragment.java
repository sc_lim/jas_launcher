/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.tvmodule.util.SoftPreconditions;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.channel.MbsChannelDataManager;
import kr.altimedia.launcher.jasmin.dm.channel.obj.ChannelProductInfo;
import kr.altimedia.launcher.jasmin.dm.channel.obj.ChannelProductOffer;
import kr.altimedia.launcher.jasmin.dm.payment.type.ContentType;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionPurchaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionPriceInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment.SubscriptionPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.ScrollDescriptionTextButton;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class SubscribeChannelDialogFragment extends SafeDismissDialogFragment {
    public static final String DIALOG_TAG = SubscribeChannelDialogFragment.class.getName();
    private static final String TAG = SubscribeChannelDialogFragment.class.getSimpleName();

    private static final String ARGS_CHANNEL_ID = "args_channelId";

    private MbsDataTask mChannelProductInfoTask;
    private ChannelProductInfo mChannelProductInfo;
    private String serviceId;
    private boolean mGoToPurchase;

    private TextView titleText;
    private TextView descText;

    public static SubscribeChannelDialogFragment create(String serviceId) {
        SubscribeChannelDialogFragment fragment = new SubscribeChannelDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARGS_CHANNEL_ID, serviceId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serviceId = getArguments().getString(ARGS_CHANNEL_ID);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), R.style.Theme_TV_dialog_Fullscreen);
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_livetv_subscription_channel, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        titleText = view.findViewById(R.id.title);
        descText = view.findViewById(R.id.desc);
        descText.setText("");

        view.findViewById(R.id.next).requestFocus();

        //initScrollBar
        TextView desc = view.findViewById(R.id.desc);
        ThumbScrollbar scrollbar = view.findViewById(R.id.scrollbar);
        //scrollbar.setList(desc.getLineCount(), desc.getMaxLines());

        desc.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                scrollbar.setList(desc.getLineCount(), desc.getMaxLines());
            }
        });

        desc.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                boolean isMove = scrollY > oldScrollY;
                scrollbar.moveScroll(isMove);
            }
        });

        //initButtons
        ScrollDescriptionTextButton.OnButtonKeyListener mOnButtonKeyListener = new ScrollDescriptionTextButton.OnButtonKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
                    switch (v.getId()) {
                        case R.id.next:
                            dismiss(true);
                            return true;
                        case R.id.cancel:
                            dismiss(false);
                            return true;
                    }
                }
                return false;
            }
        };

        ScrollDescriptionTextButton next = view.findViewById(R.id.next);
        ScrollDescriptionTextButton cancel = view.findViewById(R.id.cancel);

        next.setDescriptionTextView(desc);
        cancel.setDescriptionTextView(desc);

        next.setOnButtonKeyListener(mOnButtonKeyListener);
        cancel.setOnButtonKeyListener(mOnButtonKeyListener);


        //for check gui
        //String lorem = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci. Aenean nec lorem.";
        //lorem = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci. Aenean nec lorem. In porttitor. Donec laoreet nonummy augue. Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy. Fusce aliquet pede non pede. Suspendisse dapibus lorem pellentesque magna. Integer nulla. Donec blandit feugiat ligula. Donec hendrerit, felis et imperdiet euismod, purus ipsum pretium metus, in lacinia nulla nisl eget sapien.";
        //descText.setText(lorem);

        loadChannelProductInfo();
    }

    private void loadChannelProductInfo() {
        String said = AccountManager.getInstance().getSaId();
        String profiledId = AccountManager.getInstance().getProfileId();
        String language = AccountManager.getInstance().getLocalLanguage();
        MbsChannelDataManager mMbsChannelDataManager = new MbsChannelDataManager();
        mChannelProductInfoTask = mMbsChannelDataManager.getChannelProductInfo(said, profiledId, serviceId, language,
                new MbsDataProvider<String, ChannelProductInfo>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "mChannelProductInfoTask, onFailed");
                        }
                    }

                    @Override
                    public void onSuccess(String id, ChannelProductInfo result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "mChannelProductInfoTask, : " + result);
                        }
                        if (result != null) {
                            mChannelProductInfo = result;
                            titleText.setText(result.getProductTitle());
                            descText.setText(result.getProductDescription());
                            getView().setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                dismiss(false);
                            }
                        });
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                        descText.setText(message);
                    }
                });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mChannelProductInfoTask != null) {
            mChannelProductInfoTask.cancel(true);
            mChannelProductInfoTask = null;
        }
    }

    private void dismiss(boolean goToPurchase) {
        if (Log.INCLUDE) {
            Log.d(TAG, "dismiss() goToPurchase:" + goToPurchase);
        }
        mGoToPurchase = goToPurchase;
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (mGoToPurchase) {
            Activity activity = getActivity();
            SoftPreconditions.checkState(activity instanceof LiveTvActivity);
            if (!(activity instanceof LiveTvActivity)) {
                return;
            }
            LiveTvActivity liveTvActivity = (LiveTvActivity) activity;
            purchaseSubscription(liveTvActivity::onPaidChannelPurchased);
        }

        super.onDismiss(dialog);
    }

    private void purchaseSubscription(Consumer<String> consumer) {
        if (Log.INCLUDE) {
            Log.d(TAG, "purchaseSubscription, mChannelProductInfo : " + mChannelProductInfo);
        }

        if (mChannelProductInfo == null) {
            return;
        }

        SubscriptionInfo mSubscriptionInfo = getSubscriptionInfo();
        if (Log.INCLUDE) {
            Log.d(TAG, "purchaseSubscription, mSubscriptionInfo : " + mSubscriptionInfo);
        }

        if (mSubscriptionInfo == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "purchaseSubscription, mSubscriptionInfo == null so dismiss");
            }
            return;
        }

        SubscriptionPurchaseDialogFragment subscriptionPurchaseDialogFragment = SubscriptionPurchaseDialogFragment.newInstance(0, mSubscriptionInfo);
        subscriptionPurchaseDialogFragment.setOnCompleteSubscribePaymentListener(new SubscriptionPaymentDialogFragment.OnCompleteSubscribePaymentListener() {
            @Override
            public void onCompleteSubscribePayment(boolean isSuccess) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onCompleteSubscribePurchase, isSuccess : " + isSuccess);
                }
                if (isSuccess) {
                    consumer.accept(serviceId);
                }
            }

            @Override
            public void onDismissPurchaseDialog() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onDismissPaymentDialog");
                }
                subscriptionPurchaseDialogFragment.dismiss();
            }
        });
        getTvOverlayManager().showDialogFragment(subscriptionPurchaseDialogFragment);
    }

    private SubscriptionInfo getSubscriptionInfo() {
        List<ChannelProductOffer> offerList = mChannelProductInfo.getOfferList();
        if (offerList == null || offerList.size() == 0) {
            return null;
        }

        ArrayList<SubscriptionPriceInfo> list = new ArrayList<>();
        for (ChannelProductOffer offer : offerList) {
            SubscriptionPriceInfo mSubscriptionPriceInfo = new SubscriptionPriceInfo(
                    offer.getOfferId(), StringUtil.getFormattedPrice(offer.getPrice()), offer.getCurrency(), offer.getOfferDuration());
            list.add(mSubscriptionPriceInfo);
        }

        return new SubscriptionInfo(ContentType.CHANNEL, mChannelProductInfo.getProductTitle(),
                mChannelProductInfo.getProductDescription(), mChannelProductInfo.getTermsAndConditions(), null, list);
    }
}
