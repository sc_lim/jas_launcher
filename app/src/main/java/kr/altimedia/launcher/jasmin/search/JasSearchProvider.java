/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.search;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.SystemClock;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.altimedia.util.Log;

import java.util.Arrays;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.search.obj.SearchChannel;
import kr.altimedia.launcher.jasmin.dm.search.obj.SearchMonomax;
import kr.altimedia.launcher.jasmin.dm.search.obj.SearchVod;

/**
 * Provides global search on the app's movie catalog. The assistant will query this provider for
 * results. <br>
 * Note: If you provide WatchAction feeds to Google, then you do not need this class. You should
 * still handle the playback intent and media controls in your fragment. This class enables <a
 * href="https://developer.android.com/training/tv/discovery/searchable.html">on-device search.</a>.
 */
public class JasSearchProvider extends ContentProvider {

    private static final String TAG = JasSearchProvider.class.getSimpleName();

    private static final String AUTHORITY = "com.android.tv.search";
    private final int DEFAULT_SEARCH_LIMIT = 30;
    private static final String EXPECTED_PATH_PREFIX = "/" + SearchManager.SUGGEST_URI_PATH_QUERY;

    private JasSearchManager jasSearchManager;

    private final String[] SEARCH_COLUMNS =
            new String[]{
                    BaseColumns._ID,
                    JasSearchManager.KEY_NAME,
                    JasSearchManager.KEY_DESCRIPTION,
                    JasSearchManager.KEY_ICON,
                    JasSearchManager.KEY_CONTENT_TYPE,
                    JasSearchManager.KEY_IS_LIVE,
                    JasSearchManager.KEY_VIDEO_WIDTH,
                    JasSearchManager.KEY_VIDEO_HEIGHT,
                    JasSearchManager.KEY_PRODUCTION_YEAR,
                    JasSearchManager.KEY_COLUMN_DURATION,
                    JasSearchManager.KEY_PROGRESS_BAR_PERCENTAGE,
                    JasSearchManager.KEY_INTENT_ACTION,
                    JasSearchManager.KEY_INTENT_DATA,
                    JasSearchManager.KEY_INTENT_EXTRA_DATA
            };

    @Override
    public boolean onCreate() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreate");
        }
        jasSearchManager = JasSearchManager.getInstance();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(
            @NonNull Uri uri,
            @Nullable String[] projection,
            @Nullable String selection,
            @Nullable String[] selectionArgs,
            @Nullable String sortOrder) {

        if (Log.INCLUDE) {
            Log.d(TAG, "###########################################");
            Log.d(TAG, "query(" + uri + ", " + Arrays.toString(projection) + ", " + selection + ", "
                    + Arrays.toString(selectionArgs) + ", " + sortOrder + ")");
            Log.d(TAG, "###########################################");
        }
        long time = SystemClock.elapsedRealtime();

        String query = "";
        // way#1
//        query = uri.getLastPathSegment();
        // way#2
        if (selectionArgs != null && selectionArgs.length > 0) {
            query = selectionArgs[0];
        }

        if (query.equals("") || query.equals("dummy")) {
            if (Log.INCLUDE) {
                Log.d(TAG, "ignore this query since is's dummy");
            }
            return new MatrixCursor(SEARCH_COLUMNS, 0);
        }
        int limit = getQueryParamater(uri, SearchManager.SUGGEST_PARAMETER_LIMIT,
                DEFAULT_SEARCH_LIMIT);
        if (limit <= 0) {
            limit = DEFAULT_SEARCH_LIMIT;
        }

        Cursor c = search(query, limit);
        if (Log.INCLUDE) {
            Log.d(TAG, "Elapsed time(count=" + (c != null ? c.getCount() : 0) + "): " + (SystemClock.elapsedRealtime() - time) + "(msec)");
        }
        return c;
    }

    private int getQueryParamater(Uri uri, String key, int defaultValue) {
        try {
            return Integer.parseInt(uri.getQueryParameter(key));
        } catch (NumberFormatException | UnsupportedOperationException e) {
            // Ignore the exceptions
        }
        return defaultValue;
    }

    private Cursor search(String query, int limit) {
        if (Log.INCLUDE) {
            Log.d(TAG, "search: query=" + query + ", limit=" + limit);
        }
        MatrixCursor matrixCursor = null;
        try {
            boolean done = jasSearchManager.findAllContents(query, limit);
            if (done) {
                int totalSize = 0;
                int vodSize = 0;
                int realtimeVodSize = 0;
                int monomaxSize = 0;
                List<SearchVod> vodList = jasSearchManager.getVodList();
                List<SearchChannel> realtimeVodList = jasSearchManager.getRealtimeVodList();
                // List<SearchMonomax> monomaxList = jasSearchManager.getMonomaxList();
                List<SearchMonomax> monomaxList = null;
                // List<SearchYoutube> youtubeList = jasSearchManager.getYoutubeList();

                if (vodList != null && !vodList.isEmpty()) {
                    vodSize = vodList.size();
                }
                if (realtimeVodList != null && !realtimeVodList.isEmpty()) {
                    realtimeVodSize = realtimeVodList.size();
                }
                if (monomaxList != null && !monomaxList.isEmpty()) {
                    monomaxSize = monomaxList.size();
                }
                totalSize = vodSize + realtimeVodSize + monomaxSize;

                if (Log.INCLUDE) {
                    Log.d(TAG, "search: TotalSize=" + totalSize + "(" + jasSearchManager.getTotalSize() + ")");
                }

                matrixCursor = new MatrixCursor(SEARCH_COLUMNS, totalSize);

                if (vodSize > 0) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "search: vodList=" + vodSize + "(" + jasSearchManager.getVodSize() + ")");
                    }
                    for (SearchVod item : vodList) {
                        matrixCursor.addRow(JasSearchManager.getInstance().getCursorRow(item));
                    }
                }

                if (realtimeVodSize > 0) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "search: realtimeVodList=" + realtimeVodSize + "(" + jasSearchManager.getRealtimeVodSize() + ")");
                    }
                    for (SearchChannel item : realtimeVodList) {
                        matrixCursor.addRow(JasSearchManager.getInstance().getCursorRow(item));
                    }
                }

                /*
                if (monomaxSize > 0) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "search: monomaxList=" + monomaxSize + "(" + jasSearchManager.getMonomaxSize() + ")");
                    }
                    for (SearchMonomax item : monomaxList) {
                        matrixCursor.addRow(JasSearchManager.getInstance().getCursorRow(item));
                    }
                }

                if (youtubeList != null && !youtubeList.isEmpty()) {
                    Log.d(TAG, "search: youtubeList=" + youtubeList.size() + "(" + jasSearchManager.getYoutubeSize() + ")");
                    for (SearchYoutube item : youtubeList) {
                        matrixCursor.addRow(convertIntoCursorRow(item));
                    }
                }
                */
            }
        } catch (Exception e) {
            if (Log.INCLUDE) {
                Log.d(TAG, "error occurred while searching");
            }
        }

        return matrixCursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        if (Log.INCLUDE) {
            Log.d(TAG, "getType: uri=" + uri);
        }
        if (!checkUriCorrect(uri)) return null;
        return SearchManager.SUGGEST_MIME_TYPE;
    }

    private static boolean checkUriCorrect(Uri uri) {
        return uri != null && uri.getPath().startsWith(EXPECTED_PATH_PREFIX);
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        throw new UnsupportedOperationException("Insert is not implemented.");
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        throw new UnsupportedOperationException("Delete is not implemented.");
    }

    @Override
    public int update(
            @NonNull Uri uri,
            @Nullable ContentValues contentValues,
            @Nullable String s,
            @Nullable String[] strings) {
        throw new UnsupportedOperationException("Update is not implemented.");
    }
}
