/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.OnFragmentChange;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingProfileMenuItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingProfileMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SettingMenuPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class SettingProfileMenuListFragment extends Fragment implements SettingProfileMenuItemBridgeAdapter.OnClickMenuListener {
    public static final String CLASS_NAME = SettingProfileMenuListFragment.class.getName();
    private final String TAG = SettingProfileMenuListFragment.class.getSimpleName();

    private ViewGroup parentView = null;
    private VerticalGridView gridView;
    private ImageView lockIcon;

    private int currentMenuPosition = -1;
    private OnFragmentChange onFragmentChange;

    private final ViewTreeObserver.OnGlobalFocusChangeListener
            focusChangeListener = new ViewTreeObserver.OnGlobalFocusChangeListener() {
        @Override
        public void onGlobalFocusChanged(View oldFocus, View newFocus) {
            if (oldFocus == null || newFocus == null || oldFocus.getId() == R.id.settingList) {
                return;
            }

            if (oldFocus.getId() != R.id.setting_menu && newFocus.getId() == R.id.setting_menu) {
                onChangeParentFocusStatus(true);
            } else if (oldFocus.getId() == R.id.setting_menu && newFocus.getId() != R.id.setting_menu) {
                onChangeParentFocusStatus(false);
            } else if (oldFocus.getId() == R.id.setting_menu && newFocus.getId() == R.id.setting_menu) {
                onChangeChildFocusStatus(oldFocus);
            }
        }
    };

    private SettingProfileMenuListFragment() {
    }

    public static SettingProfileMenuListFragment newInstance(Bundle bundle) {
        SettingProfileMenuListFragment fragment = new SettingProfileMenuListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setOnFragmentChange(OnFragmentChange onFragmentChange) {
        this.onFragmentChange = onFragmentChange;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings_profile_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (view instanceof ViewGroup) {
            parentView = (ViewGroup) view;
        }

        initProfile(view);
        initMenus(view);
    }

    private void initProfile(View view) {
        ProfileInfo profileInfo = getArguments().getParcelable(SettingProfileDialogFragment.KEY_PROFILE);

        TextView name = view.findViewById(R.id.profile_name);
        ImageView pic = view.findViewById(R.id.profile_pic);
        lockIcon = view.findViewById(R.id.lock);

        name.setText(profileInfo.getProfileName());
        try {
            profileInfo.setProfileIcon(pic);
        }catch (Exception e){
        }
        setProfileLockIcon(profileInfo.isLock());
    }

    private void initMenus(View view) {
        SettingProfileMenu[] profileMenus = SettingProfileMenu.class.getEnumConstants();
        ArrayList<SettingProfileMenu> menuList = new ArrayList<>(Arrays.asList(profileMenus));

        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(new SettingMenuPresenter());
        objectAdapter.addAll(0, menuList);

        gridView = view.findViewById(R.id.settingList);
        SettingProfileMenuItemBridgeAdapter itemBridgeAdapter = new SettingProfileMenuItemBridgeAdapter(objectAdapter, gridView);
        itemBridgeAdapter.setOnClickMenuListener(this);
        gridView.setAdapter(itemBridgeAdapter);

        getView().getViewTreeObserver().addOnGlobalFocusChangeListener(focusChangeListener);
        gridView.setOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {
            @Override
            public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onChildViewHolderSelected | position :  " + position + ", subposition : " + subposition);
                }

                if (currentMenuPosition != position) {
                    currentMenuPosition = position;

                    ItemBridgeAdapter.ViewHolder itemViewHolder = (ItemBridgeAdapter.ViewHolder) child;
                    SettingProfileMenu menu = (SettingProfileMenu) itemViewHolder.getItem();
                    onFragmentChange.onChangeMenu(menu);
                }
            }
        });
    }

    private void onChangeParentFocusStatus(boolean hasParentFocus) {
        if (gridView == null) {
            return;
        }

        for (int i = 0; i < gridView.getChildCount(); i++) {
            View view = gridView.getChildAt(i);
            ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) gridView.getChildViewHolder(view);
            SettingMenuPresenter.ViewHolder vh = (SettingMenuPresenter.ViewHolder) viewHolder.getViewHolder();
            vh.setParentFocus(hasParentFocus);
        }
    }

    private void onChangeChildFocusStatus(View oldFocus) {
        if (oldFocus != null) {
            SettingMenuPresenter.ViewHolder oldVh = (SettingMenuPresenter.ViewHolder) ((ItemBridgeAdapter.ViewHolder) gridView.getChildViewHolder(oldFocus)).getViewHolder();
            oldVh.setParentFocus(true);
        }
    }

    public void setProfileLockIcon(boolean isLock) {
        int visibility = isLock ? View.VISIBLE : View.INVISIBLE;
        lockIcon.setVisibility(visibility);
    }

    public void setFocusable(boolean isFocusable) {
        int currentDescendants = parentView.getDescendantFocusability();
        int requestDescendants = isFocusable ? ViewGroup.FOCUS_AFTER_DESCENDANTS : ViewGroup.FOCUS_BLOCK_DESCENDANTS;

        android.util.Log.d(TAG, "setFocusable, currentDescendants : " + currentDescendants + ", requestDescendant : " + requestDescendants);

        if (currentDescendants != requestDescendants) {
            parentView.setDescendantFocusability(requestDescendants);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getView().getViewTreeObserver().removeOnGlobalFocusChangeListener(focusChangeListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onFragmentChange = null;
    }

    @Override
    public void onClickMenu() {
        onFragmentChange.onClickMenu();
    }
}
