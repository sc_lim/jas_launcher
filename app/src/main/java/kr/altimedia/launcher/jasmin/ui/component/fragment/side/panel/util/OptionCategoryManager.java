/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.util;

import android.content.Context;

import com.altimedia.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOption;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOptionProvider;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object.SideOptionCategory;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object.SideOptionInfo;

/**
 * Created by mc.kim on 20,02,2020
 */
public class OptionCategoryManager {
    private final String TAG = OptionCategoryManager.class.getSimpleName();
    private HashMap<SideOptionCategory, ArrayList<SideOptionCategory>> categoryMap = new HashMap<>();
    private ArrayList<SideOptionCategory> rootList = new ArrayList<>();
    private final SidePanelOptionProvider optionProvider;
    private final Context mContext;

    public OptionCategoryManager(Context context, SidePanelType type, SidePanelOptionProvider sidePanelOptionProvider) {
        mContext = context;
        optionProvider = sidePanelOptionProvider;
        SideOptionInfo sideOptionInfo = null;
        try {
            sideOptionInfo = loadSideOptionInfo(context, type.getResourceId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (sideOptionInfo == null) {
            return;
        }
        ArrayList<SideOptionCategory> categories = sideOptionInfo.getCategoryList();

        for (SideOptionCategory category : categories) {
            if (category.isRootNode()) {
                rootList.add(category);
            }
            putChildNode(category);
        }
    }


    public ArrayList<SideOptionCategory> getRootList() {
        return rootList;
    }

    public boolean isRootCategory(SideOptionCategory sideOptionCategory) {
        return rootList.contains(sideOptionCategory);
    }

    public ArrayList<SideOptionCategory> getChildList(SideOptionCategory categoryId) {
        return categoryMap.get(categoryId);
    }

    private void putChildNode(SideOptionCategory category) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call putChildNode : " + category);
        }
        switch (category.getOptionType()) {
            case Audio:
                List<String> audioOptionList =
                        (List<String>) optionProvider.getData(SidePanelOptionProvider.KEY_AUDIO_LIST);
                ArrayList<SideOptionCategory> audioAudioCategory = new ArrayList<>();
                for (String language : audioOptionList) {
                    SideOptionCategory sideOptionCategory = new SideOptionCategory(SidePanelOption.LanguageOption, language);
                    audioAudioCategory.add(sideOptionCategory);
                }
                categoryMap.put(category, audioAudioCategory);
                break;
            case SubTitle:
                List<String> subtitle =
                        (List<String>) optionProvider.getData(SidePanelOptionProvider.KEY_SUBTITLE);
                ArrayList<SideOptionCategory> subTitleCategory = new ArrayList<>();
                for (String language : subtitle) {
                    SideOptionCategory sideOptionCategory = new SideOptionCategory(SidePanelOption.LanguageOption, language);
                    subTitleCategory.add(sideOptionCategory);
                }

                if (!subTitleCategory.isEmpty()) {
                    String subTitleSection = mContext.getString(R.string.section_select_language);
                    subTitleCategory.add(0, new SideOptionCategory(SidePanelOption.Section, subTitleSection));
                    String subTitleStyleSection = mContext.getString(R.string.section_subtitle_size);
                    subTitleCategory.add(new SideOptionCategory(SidePanelOption.Section, subTitleStyleSection));
                    String[] styleType = mContext.getResources().getStringArray(R.array.subtitle_style);
                    for (String style : styleType) {
                        subTitleCategory.add(new SideOptionCategory(SidePanelOption.LanguageStyle, style));
                    }
                }

                categoryMap.put(category, subTitleCategory);
                break;
        }
    }

    private SideOptionInfo loadSideOptionInfo(Context context, int fileName) throws IOException {
        InputStream is = context.getResources().openRawResource(fileName);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        Reader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
        int n;
        while ((n = reader.read(buffer)) != -1) {
            writer.write(buffer, 0, n);
        }
        is.close();
        return getGson().fromJson(writer.toString(), SideOptionInfo.class);
    }


    private Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(SidePanelOption.class, new SidePanelOptionDeserializer());
        builder.registerTypeAdapter(SidePanelType.class, new SidePanelTypeDeserializer());
        return builder.create();
    }
}
