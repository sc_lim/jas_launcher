/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.PurchasedContent;

public class PurchaseListResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private PurchaseList data;

    protected PurchaseListResponse(Parcel in) {
        super(in);
        data = in.readParcelable(PurchaseListResponse.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    public List<PurchasedContent> getPurchasedContentList() {
        return data.list;
    }

    private static class PurchaseList implements Parcelable {
        public static final Parcelable.Creator<PurchaseList> CREATOR = new Parcelable.Creator<PurchaseList>() {
            @Override
            public PurchaseList createFromParcel(Parcel in) {
                return new PurchaseList(in);
            }

            @Override
            public PurchaseList[] newArray(int size) {
                return new PurchaseList[size];
            }
        };
        @Expose
        @SerializedName("list")
        public java.util.List<PurchasedContent> list;

        protected PurchaseList(Parcel in) {
            in.readList(list, PurchasedContent.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(list);
        }

        @Override
        public int describeContents() {
            return 0;
        }
    }
}
