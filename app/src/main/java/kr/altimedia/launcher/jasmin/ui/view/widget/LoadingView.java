/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 12,06,2020
 */
public class LoadingView extends AppCompatImageView {
    public LoadingView(Context context) {
        super(context);
        init(context);
    }

    public LoadingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LoadingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    private void init(Context context) {
        final CallbackAnimationDrawable frameAnimation = new CallbackAnimationDrawable((AnimationDrawable) context.getDrawable(R.drawable.loading));
        setImageDrawable(frameAnimation);
        post(new Runnable() {
            public void run() {
                frameAnimation.start();
            }
        });
        frameAnimation.setAnimDrawableListener(new CallbackAnimationDrawable.OnAnimDrawableCallback() {
            @Override
            public void onAnimationOneShotFinished() {
                final AnimationDrawable frameAnimation = (AnimationDrawable) context.getDrawable(R.drawable.loading_rotate);
                setImageDrawable(frameAnimation);
                post(new Runnable() {
                    public void run() {
                        frameAnimation.start();
                    }
                });
            }
        });

    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        final AnimationDrawable frameAnimation = (AnimationDrawable) getBackground();
        if (frameAnimation == null) {
            return;
        }
        post(new Runnable() {
            public void run() {
                frameAnimation.stop();
            }
        });
    }
}
