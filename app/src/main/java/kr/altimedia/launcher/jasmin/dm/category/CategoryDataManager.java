/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.category;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.category.module.CategoryModule;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.dm.category.obj.response.CategoryVersionCheckResponse;
import kr.altimedia.launcher.jasmin.dm.category.remote.CategoryRemote;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by mc.kim on 18,05,2020
 */
public class CategoryDataManager extends BaseDataManager {
    private final String TAG = CategoryDataManager.class.getSimpleName();
    private CategoryRemote mCategoryRemote;

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        CategoryModule categoryModule = new CategoryModule();
        Retrofit contentRetrofit = categoryModule.provideCategoryModule(mOkHttpClient);
        mCategoryRemote = new CategoryRemote(categoryModule.provideCategoryApi(contentRetrofit));
    }

    @Override
    public String getLoginToken() {
        return AccountManager.getInstance().getLoginToken();
    }

    public MbsDataTask checkCategoryVersion(String version, MbsDataProvider<String, CategoryVersionCheckResponse> mbsDataProvider) {
        MbsDataTask mContentsTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, CategoryVersionCheckResponse>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mCategoryRemote.checkCategoryVersion(version);
            }

            @Override
            public void onTaskPostExecute(String key, CategoryVersionCheckResponse result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);

                return null;
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

        });
        mContentsTask.execute(version);
        return mContentsTask;
    }

    public MbsDataTask getCategoryList(String language, String said, String categoryId, String version, MbsDataProvider<String, List<Category>> mbsDataProvider) {
        MbsDataTask mContentsTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, List<Category>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mCategoryRemote.getCategoryList(language, said, categoryId, version);
            }

            @Override
            public void onTaskPostExecute(String key, List<Category> result) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

        });
        mContentsTask.execute(version);
        return mContentsTask;
    }
}
