/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account;

import android.content.Context;

import kr.altimedia.launcher.jasmin.R;
import com.altimedia.util.Log;

public class SettingAccountPinErrorManager {
    public static final String CLASS_NAME = SettingAccountPinErrorManager.class.getName();
    public static final String TAG = SettingAccountPinErrorManager.class.getSimpleName();

    public static final int PIN_CORRECT = -1;
    public static final int PIN_INCORRECT = 0;
    public static final int CHANGE_INCORRECT = 1;
    public static final int CHANGE_SAME_ERROR = 2;
    public static final int CHANGE_MATCH_ERROR = 3;
    public static final int CHANGE_MATCH = 4;

    public SettingAccountPinErrorManager() {
    }

    public int getPinErrorType(String currentPin, String newPin, String confirmPin) {

        if (Log.INCLUDE) {
            Log.d(TAG, "getChangePnErrorText, currentPin : "
                    + currentPin + ", newPin : " + newPin + ", confirmPin : " + confirmPin);
        }

        int type = CHANGE_MATCH;
        if (!newPin.equalsIgnoreCase(confirmPin)) {
            type = SettingAccountPinErrorManager.CHANGE_MATCH_ERROR;
        }

        return type;
    }

    public String getErrorText(Context context, int type) {
        switch (type) {
            case PIN_INCORRECT:
            case CHANGE_INCORRECT:
                return context.getString(R.string.incorrect_pin_desc);
            case CHANGE_SAME_ERROR:
                return context.getString(R.string.change_pin_same_error);
            case CHANGE_MATCH_ERROR:
                return context.getString(R.string.change_pin_match_error);
        }

        return null;
    }
}
