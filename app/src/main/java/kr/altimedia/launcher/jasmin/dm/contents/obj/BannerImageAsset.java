/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.altimedia.tvmodule.util.StringUtils;

/**
 * Created by mc.kim on 08,05,2020
 */
public class BannerImageAsset implements Parcelable {
    @Expose
    @SerializedName("provider")
    private String provider;
    @Expose
    @SerializedName("containerType")
    private String containerType;
    @Expose
    @SerializedName("id")
    private String id;
    @Expose
    @SerializedName("fileName")
    private String fileName;
    @Expose
    @SerializedName("fileSize")
    private String fileSize;


    protected BannerImageAsset(Parcel in) {
        provider = in.readString();
        containerType = in.readString();
        id = in.readString();
        fileName = in.readString();
        fileSize = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(provider);
        dest.writeString(containerType);
        dest.writeString(id);
        dest.writeString(fileName);
        dest.writeString(fileSize);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BannerImageAsset> CREATOR = new Creator<BannerImageAsset>() {
        @Override
        public BannerImageAsset createFromParcel(Parcel in) {
            return new BannerImageAsset(in);
        }

        @Override
        public BannerImageAsset[] newArray(int size) {
            return new BannerImageAsset[size];
        }
    };

    public String getContainerType() {
        return containerType;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public String getId() {
        return id;
    }

    public String getPosterSourceUrl(int width, int height) {
        String posterType = StringUtils.nullToEmpty(containerType).isEmpty() ? "" : containerType;
        return fileName + "_" + width + "x" + height + "." + posterType;
    }

    public String getProvider() {
        return provider;
    }

    @Override
    public String toString() {
        return "BannerImageAsset{" +
                "containerType='" + containerType + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileSize='" + fileSize + '\'' +
                ", id='" + id + '\'' +
                ", provider='" + provider + '\'' +
                '}';
    }
}
