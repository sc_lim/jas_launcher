/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class DevicePresenter extends Presenter {

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_device, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        if (item == null) {
            viewHolder.view.setVisibility(View.INVISIBLE);
            return;
        }

        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((UserDevice) item);
        vh.view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public class ViewHolder extends Presenter.ViewHolder {
        private TextView deviceName;
        private TextView deviceLoginDate;

        public ViewHolder(View view) {
            super(view);

            initView(view);
        }

        private void initView(View view) {
            deviceName = view.findViewById(R.id.device_name);
            deviceLoginDate = view.findViewById(R.id.device_login_date);
        }

        public void setData(UserDevice item) {
            deviceName.setText(item.getDeviceName());
            Date loggedDate = item.getFirstLoginDate();
            if (loggedDate != null) {
                deviceLoginDate.setText(TimeUtil.getModifiedDate(loggedDate.getTime()));
            }
        }
    }
}
