/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.rating;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.system.settings.data.ParentalRating;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseProfileDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.CheckButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.RatingLocksPresenter;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment.KEY_PROFILE;
import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.SettingProfileParentalFragment.KEY_RATING;

public class SettingProfileRatingLocksFragmentDialog extends SettingBaseProfileDialogFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton<RatingLockItem> {
    public static final String CLASS_NAME = SettingProfileRatingLocksFragmentDialog.class.getName();
    private static final String TAG = SettingProfileRatingLocksFragmentDialog.class.getSimpleName();

    private ArrayObjectAdapter objectAdapter;
    private CheckButtonBridgeAdapter checkButtonBridgeAdapter;

    private RatingLockItem currentLimit = null;
    private ArrayList<RatingLockItem> ratingList;

    private OnClickRatingListener onClickRatingListener;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private MbsDataTask ratingTask;
    private SettingTaskManager settingTaskManager;

    private SettingProfileRatingLocksFragmentDialog() {
    }

    public static SettingProfileRatingLocksFragmentDialog newInstance(Bundle bundle) {
        SettingProfileRatingLocksFragmentDialog fragment = new SettingProfileRatingLocksFragmentDialog();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rating_locks, container, false);
    }

    public void setOnClickRatingListener(OnClickRatingListener onClickRatingListener) {
        this.onClickRatingListener = onClickRatingListener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        settingTaskManager = new SettingTaskManager(getFragmentManager(), mTvOverlayManager);
        initView(view);
    }

    private void initView(View view) {
        initProgressbar(view);
        initData(view);
        initList(view);
    }

    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void showProgress() {
        mProgressBarManager.show();
    }

    private void hideProgress() {
        if (mProgressBarManager != null) {
            mProgressBarManager.hide();
        }
    }

    private void initData(View view) {
        Bundle bundle = getArguments();
        ParentalRating parentalRating = (ParentalRating) bundle.getSerializable(KEY_RATING);
        currentLimit = new RatingLockItem(parentalRating.getRating());
        currentLimit.setChecked(true);
    }

    private void initList(View view) {
        ratingList = new ArrayList<>(
                Arrays.asList(
                        new RatingLockItem(ParentalRating.LIMIT_3.getRating()),
                        new RatingLockItem(ParentalRating.LIMIT_6.getRating()),
                        new RatingLockItem(ParentalRating.LIMIT_13.getRating()),
                        new RatingLockItem(ParentalRating.LIMIT_18.getRating()),
                        new RatingLockItem(ParentalRating.NO_LIMIT.getRating())
                ));

        RatingLocksPresenter mRatingLocksPresenter = new RatingLocksPresenter();
        objectAdapter = new ArrayObjectAdapter(mRatingLocksPresenter);
        objectAdapter.addAll(0, ratingList);

        VerticalGridView gridView = view.findViewById(R.id.gridView);
        checkButtonBridgeAdapter = new CheckButtonBridgeAdapter(objectAdapter);
        checkButtonBridgeAdapter.setListener(this);
        gridView.setAdapter(checkButtonBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        gridView.setVerticalSpacing(spacing);

        for (RatingLockItem item : ratingList) {
            if (currentLimit.getRating() == item.getRating()) {
                objectAdapter.replace(item.getType(), currentLimit);
            }
        }
    }

    private boolean isRequest = false;
    @Override
    public boolean onClickButton(RatingLockItem item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        if (currentLimit.getType() == item.getType()) {
            return true;
        }

        if (isRequest) {
            return true;
        }

        isRequest = true;
        putProfileRatingLock(item);

        return true;
    }

    private void putProfileRatingLock(RatingLockItem ratingLockItem) {
        if (Log.INCLUDE) {
            Log.d(TAG, "putProfileRatingLock, ratingLockItem : " + ratingLockItem.getParentalRating());
        }

        ProfileInfo profileInfo = getArguments().getParcelable(KEY_PROFILE);
        String rating = String.valueOf(ratingLockItem.getParentalRating().getRating());

        ratingTask = settingTaskManager.putProfileRating(
                profileInfo.getProfileId(), rating,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "needLoading , loading : " + loading);
                        }

                        if (loading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed");
                        }

                        isRequest = false;
                        settingTaskManager.showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSuccess, result : " + result);
                        }

                        ratingLockItem.setChecked(true);
                        currentLimit.setChecked(false);

                        objectAdapter.replace(currentLimit.getType(), currentLimit);
                        objectAdapter.replace(ratingLockItem.getType(), ratingLockItem);

                        currentLimit = ratingLockItem;

                        onClickRatingListener.onClickRating(currentLimit.getParentalRating());
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError, message : " + message);
                        }

                        isRequest = false;
                        settingTaskManager.showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingProfileRatingLocksFragmentDialog.this.getContext();
                    }
                });
    }

    @Override
    protected ProfileInfo getProfileInfo() {
        return getArguments().getParcelable(KEY_PROFILE);
    }

    @Override
    protected void updateProfile(PushMessageReceiver.ProfileUpdateType type) {
        super.updateProfile(type);

        if (Log.INCLUDE) {
            Log.d(TAG, "updateProfile, type : " + type);
        }

        dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (ratingTask != null) {
            ratingTask.cancel(true);
        }

        onClickRatingListener = null;
        checkButtonBridgeAdapter.setListener(null);
    }

    public interface OnClickRatingListener {
        void onClickRating(ParentalRating rating);
    }
}
