/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.message.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.message.obj.Message;

/**
 * Created by mc.kim on 08,05,2020
 */
public class MessageListResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private MessageListResult messageListResult;

    protected MessageListResponse(Parcel in) {
        super(in);
        messageListResult = in.readTypedObject(MessageListResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(messageListResult, flags);
    }

    public List<Message> getMessageList() {
        if(messageListResult == null){
            return new ArrayList<>();
        }
        return messageListResult.messageList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MessageListResponse> CREATOR = new Creator<MessageListResponse>() {
        @Override
        public MessageListResponse createFromParcel(Parcel in) {
            return new MessageListResponse(in);
        }

        @Override
        public MessageListResponse[] newArray(int size) {
            return new MessageListResponse[size];
        }
    };

    @Override
    public String toString() {
        return "MessageListResponse{" +
                "messageListResult=" + messageListResult +
                '}';
    }

    private static class MessageListResult implements Parcelable {
        @Expose
        @SerializedName("messageList")
        private List<Message> messageList;

        protected MessageListResult(Parcel in) {
            messageList = in.createTypedArrayList(Message.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(messageList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<MessageListResult> CREATOR = new Creator<MessageListResult>() {
            @Override
            public MessageListResult createFromParcel(Parcel in) {
                return new MessageListResult(in);
            }

            @Override
            public MessageListResult[] newArray(int size) {
                return new MessageListResult[size];
            }
        };
    }
}
