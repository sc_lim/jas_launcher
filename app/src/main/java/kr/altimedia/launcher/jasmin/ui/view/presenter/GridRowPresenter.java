/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.presenter;

import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.graphics.ColorOverlayDimmer;
import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.ui.view.browse.GridRowContainerView;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewKeyListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;

/**
 * Created by mc.kim on 12,12,2019
 */
public abstract class  GridRowPresenter extends Presenter  {
    public static final int SYNC_ACTIVATED_CUSTOM = 0;
    public static final int SYNC_ACTIVATED_TO_EXPANDED = 1;
    public static final int SYNC_ACTIVATED_TO_SELECTED = 2;
    public static final int SYNC_ACTIVATED_TO_EXPANDED_AND_SELECTED = 3;
    private BaseHeaderPresenter mHeaderPresenter;
    boolean mSelectEffectEnabled = true;
    int mSyncActivatePolicy = 1;

    public GridRowPresenter() {
        this.mHeaderPresenter = new RowHeaderPresenter();
        this.mHeaderPresenter.setNullItemVisibilityGone(true);
    }

    public GridRowPresenter(int headerLayoutId) {
        this.mHeaderPresenter = new RowHeaderPresenter(headerLayoutId);
        this.mHeaderPresenter.setNullItemVisibilityGone(true);
    }


    public final Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        GridRowPresenter.ViewHolder vh = this.createRowViewHolder(parent);
        vh.mInitialzed = false;
        Object result;
        if (this.needsRowContainerView()) {
            GridRowContainerView containerView = new GridRowContainerView(parent.getContext());
            if (this.mHeaderPresenter != null) {

                vh.mHeaderViewHolder = (BaseHeaderPresenter.ViewHolder) this.mHeaderPresenter.onCreateViewHolder((ViewGroup) vh.view);
            }

            result = new GridRowPresenter.ContainerViewHolder(containerView, vh);
        } else {
            result = vh;
        }

        this.initializeRowViewHolder(vh);
        if (!vh.mInitialzed) {
            throw new RuntimeException("super.initializeRowViewHolder() must be called");
        } else {
            return (Presenter.ViewHolder) result;
        }
    }

    protected abstract GridRowPresenter.ViewHolder createRowViewHolder(ViewGroup var1);

    protected boolean isClippingChildren() {
        return false;
    }

    protected void initializeRowViewHolder(GridRowPresenter.ViewHolder vh) {
        vh.mInitialzed = true;
        if (!this.isClippingChildren()) {
            if (vh.view instanceof ViewGroup) {
                ((ViewGroup) vh.view).setClipChildren(false);
            }

            if (vh.mContainerViewHolder != null) {
                ((ViewGroup) vh.mContainerViewHolder.view).setClipChildren(false);
            }
        }

    }

    public final void setHeaderPresenter(BaseHeaderPresenter headerPresenter) {
        this.mHeaderPresenter = headerPresenter;
    }

    public final BaseHeaderPresenter getHeaderPresenter() {
        return this.mHeaderPresenter;
    }

    public final GridRowPresenter.ViewHolder getRowViewHolder(Presenter.ViewHolder holder) {
        return holder instanceof GridRowPresenter.ContainerViewHolder ? ((GridRowPresenter.ContainerViewHolder) holder).mRowViewHolder : (GridRowPresenter.ViewHolder) holder;
    }

    public final void setRowViewExpanded(Presenter.ViewHolder holder, boolean expanded) {
        GridRowPresenter.ViewHolder rowViewHolder = this.getRowViewHolder(holder);
        rowViewHolder.mExpanded = expanded;
        this.onRowViewExpanded(rowViewHolder, expanded);
    }

    public final void setRowViewSelected(Presenter.ViewHolder holder, boolean selected) {
        GridRowPresenter.ViewHolder rowViewHolder = this.getRowViewHolder(holder);
        rowViewHolder.mSelected = selected;
        this.onRowViewSelected(rowViewHolder, selected);
    }

    protected void onRowViewExpanded(GridRowPresenter.ViewHolder vh, boolean expanded) {
        this.updateHeaderViewVisibility(vh);
        this.updateActivateStatus(vh, vh.view);
    }

    private void updateActivateStatus(GridRowPresenter.ViewHolder vh, View view) {
        switch (this.mSyncActivatePolicy) {
            case 1:
                vh.setActivated(vh.isExpanded());
                break;
            case 2:
                vh.setActivated(vh.isSelected());
                break;
            case 3:
                vh.setActivated(vh.isExpanded() && vh.isSelected());
        }

        vh.syncActivatedStatus(view);
    }

    public final void setSyncActivatePolicy(int syncActivatePolicy) {
        this.mSyncActivatePolicy = syncActivatePolicy;
    }

    public final int getSyncActivatePolicy() {
        return this.mSyncActivatePolicy;
    }

    protected void dispatchItemSelectedListener(GridRowPresenter.ViewHolder vh, boolean selected) {
        if (selected && vh.mOnItemViewSelectedListener != null) {
            vh.mOnItemViewSelectedListener.onItemSelected((Presenter.ViewHolder) null, (Object) null, vh, vh.getRowObject());
        }

    }

    protected void onRowViewSelected(GridRowPresenter.ViewHolder vh, boolean selected) {
        this.dispatchItemSelectedListener(vh, selected);
        this.updateHeaderViewVisibility(vh);
        this.updateActivateStatus(vh, vh.view);
    }

    private void updateHeaderViewVisibility(GridRowPresenter.ViewHolder vh) {
        if (this.mHeaderPresenter != null && vh.mHeaderViewHolder != null) {
            GridRowContainerView containerView = (GridRowContainerView) vh.mContainerViewHolder.view;
            containerView.showHeader(vh.isExpanded());
        }

    }

    public final void setSelectLevel(Presenter.ViewHolder vh, float level) {
        GridRowPresenter.ViewHolder rowViewHolder = this.getRowViewHolder(vh);
        rowViewHolder.mSelectLevel = level;
        this.onSelectLevelChanged(rowViewHolder);
    }

    public final float getSelectLevel(Presenter.ViewHolder vh) {
        return this.getRowViewHolder(vh).mSelectLevel;
    }

    protected void onSelectLevelChanged(GridRowPresenter.ViewHolder vh) {
        if (this.getSelectEffectEnabled()) {
            vh.mColorDimmer.setActiveLevel(vh.mSelectLevel);
            if (vh.mHeaderViewHolder != null) {
                this.mHeaderPresenter.setSelectLevel(vh.mHeaderViewHolder, vh.mSelectLevel);
            }

            if (this.isUsingDefaultSelectEffect()) {
                ((GridRowContainerView) vh.mContainerViewHolder.view).setForegroundColor(vh.mColorDimmer.getPaint().getColor());
            }
        }

    }

    public final void setSelectEffectEnabled(boolean applyDimOnSelect) {
        this.mSelectEffectEnabled = applyDimOnSelect;
    }

    public final boolean getSelectEffectEnabled() {
        return false;
    }

    public boolean isUsingDefaultSelectEffect() {
        return false;
    }

    final boolean needsDefaultSelectEffect() {
        return this.isUsingDefaultSelectEffect() && this.getSelectEffectEnabled();
    }

    final boolean needsRowContainerView() {
        return this.mHeaderPresenter != null || this.needsDefaultSelectEffect();
    }

    public final void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        this.onBindRowViewHolder(this.getRowViewHolder(viewHolder), item);
    }

    protected void onBindRowViewHolder(GridRowPresenter.ViewHolder vh, Object item) {
        vh.mRowObject = item;
        vh.mRow = item instanceof Row ? (Row) item : null;
        if (vh.mHeaderViewHolder != null && vh.getRow() != null) {
            this.mHeaderPresenter.onBindViewHolder(vh.mHeaderViewHolder, item);
        }

    }

    public final void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        this.onUnbindRowViewHolder(this.getRowViewHolder(viewHolder));
    }

    protected void onUnbindRowViewHolder(GridRowPresenter.ViewHolder vh) {
        if (vh.mHeaderViewHolder != null) {
            this.mHeaderPresenter.onUnbindViewHolder(vh.mHeaderViewHolder);
        }

        vh.mRow = null;
        vh.mRowObject = null;
    }

    public final void onViewAttachedToWindow(Presenter.ViewHolder holder) {
        this.onRowViewAttachedToWindow(this.getRowViewHolder(holder));
    }

    protected void onRowViewAttachedToWindow(GridRowPresenter.ViewHolder vh) {
        if (vh.mHeaderViewHolder != null) {
            this.mHeaderPresenter.onViewAttachedToWindow(vh.mHeaderViewHolder);
        }

    }

    public final void onViewDetachedFromWindow(Presenter.ViewHolder holder) {
        this.onRowViewDetachedFromWindow(this.getRowViewHolder(holder));
    }

    protected void onRowViewDetachedFromWindow(GridRowPresenter.ViewHolder vh) {
        if (vh.mHeaderViewHolder != null) {
            this.mHeaderPresenter.onViewDetachedFromWindow(vh.mHeaderViewHolder);
        }

        cancelAnimationsRecursive(vh.view);
    }

    public void freeze(GridRowPresenter.ViewHolder holder, boolean freeze) {
    }

    public void setEntranceTransitionState(GridRowPresenter.ViewHolder holder, boolean afterEntrance) {
        if (holder.mHeaderViewHolder != null && holder.mHeaderViewHolder.view.getVisibility() != View.GONE) {
            holder.mHeaderViewHolder.view.setVisibility(afterEntrance ? View.VISIBLE : View.INVISIBLE);
        }

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private static final int ACTIVATED_NOT_ASSIGNED = 0;
        private static final int ACTIVATED = 1;
        private static final int NOT_ACTIVATED = 2;
        GridRowPresenter.ContainerViewHolder mContainerViewHolder;
        BaseHeaderPresenter.ViewHolder mHeaderViewHolder;
        Row mRow;
        Object mRowObject;
        int mActivated = 0;
        boolean mSelected;
        boolean mExpanded;
        boolean mInitialzed;
        float mSelectLevel = 0.0F;
        protected final ColorOverlayDimmer mColorDimmer;
        private View.OnKeyListener mOnKeyListener;
        BaseOnItemViewSelectedListener mOnItemViewSelectedListener;
        private BaseOnItemViewClickedListener mOnItemViewClickedListener;
        private BaseOnItemViewKeyListener mBaseOnItemViewKeyListener;

        public ViewHolder(View view) {
            super(view);
            this.mColorDimmer = ColorOverlayDimmer.createDefault(view.getContext());
        }

        public final Row getRow() {
            return this.mRow;
        }

        public final Object getRowObject() {
            return this.mRowObject;
        }

        public final boolean isExpanded() {
            return this.mExpanded;
        }

        public final boolean isSelected() {
            return this.mSelected;
        }

        public final float getSelectLevel() {
            return this.mSelectLevel;
        }

        public final BaseHeaderPresenter.ViewHolder getHeaderViewHolder() {
            return this.mHeaderViewHolder;
        }

        public final void setActivated(boolean activated) {
            this.mActivated = activated ? 1 : 2;
        }

        public final void syncActivatedStatus(View view) {
            if (this.mActivated == 1) {
                view.setActivated(true);
            } else if (this.mActivated == 2) {
                view.setActivated(false);
            }

        }

        public void setOnKeyListener(View.OnKeyListener keyListener) {
            this.mOnKeyListener = keyListener;
        }

        public View.OnKeyListener getOnKeyListener() {
            return this.mOnKeyListener;
        }

        public final void setOnItemViewSelectedListener(BaseOnItemViewSelectedListener listener) {
            this.mOnItemViewSelectedListener = listener;
        }

        public final BaseOnItemViewSelectedListener getOnItemViewSelectedListener() {
            return this.mOnItemViewSelectedListener;
        }

        public final void setOnItemViewClickedListener(BaseOnItemViewClickedListener listener) {
            this.mOnItemViewClickedListener = listener;
        }

        public final BaseOnItemViewClickedListener getOnItemViewClickedListener() {
            return this.mOnItemViewClickedListener;
        }

        public void setBaseOnItemKeyListener(BaseOnItemViewKeyListener mBaseOnItemViewKeyListener) {
            this.mBaseOnItemViewKeyListener = mBaseOnItemViewKeyListener;
        }

        public BaseOnItemViewKeyListener getBaseOnItemKeyListener() {
            return mBaseOnItemViewKeyListener;
        }

        public Presenter.ViewHolder getSelectedItemViewHolder() {
            return null;
        }

        public Object getSelectedItem() {
            return null;
        }
    }

    static class ContainerViewHolder extends Presenter.ViewHolder {
        final GridRowPresenter.ViewHolder mRowViewHolder;

        public ContainerViewHolder(GridRowContainerView containerView, GridRowPresenter.ViewHolder rowViewHolder) {
            super(containerView);
//            ((ViewGroup)rowViewHolder.view).setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            containerView.addRowView(rowViewHolder.view);
            if (rowViewHolder.mHeaderViewHolder != null) {
                containerView.addHeaderView(rowViewHolder.mHeaderViewHolder.view);
            }

            this.mRowViewHolder = rowViewHolder;
            this.mRowViewHolder.mContainerViewHolder = this;
//            containerView.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
//            containerView.setFocusable(true);
//            containerView.setFocusableInTouchMode(true);
        }
    }
}
