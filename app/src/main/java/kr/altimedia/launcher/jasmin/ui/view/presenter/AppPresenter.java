/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.presenter;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.util.InstalledAppUtil;

public class AppPresenter extends Presenter {
    public OnKeyListener onKeyListener;

    public AppPresenter(OnKeyListener onKeyListener) {
        this.onKeyListener = onKeyListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_app, parent, false);
        return new AppViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (item == null) {
            viewHolder.view.setVisibility(View.INVISIBLE);
            return;
        }

        InstalledAppUtil.AppInfo appInfo = (InstalledAppUtil.AppInfo) item;
        AppViewHolder vh = (AppViewHolder) viewHolder;
        vh.setData(appInfo);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        AppViewHolder vh = (AppViewHolder) viewHolder;
        vh.view.findViewById(R.id.cardView).setOnFocusChangeListener(null);
    }

    public OnKeyListener getOnKeyListener() {
        return onKeyListener;
    }

    public class AppViewHolder extends Presenter.ViewHolder implements View.OnFocusChangeListener {
        private ImageView imageView;
        private LinearLayout layout;

        public AppViewHolder(View view) {
            super(view);
            layout = view.findViewById(R.id.layout);
            imageView = view.findViewById(R.id.image);
            view.setOnFocusChangeListener(this);
        }

        private void setData(InstalledAppUtil.AppInfo appInfo) {
            Glide.with(view.getContext())
                    .load(appInfo).diskCacheStrategy(DiskCacheStrategy.DATA)
                    .fitCenter()
                    .placeholder(appInfo.getIcon())
                    .into(imageView);
            view.setVisibility(View.VISIBLE);
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            Animator animator = loadAnimator(v.getContext(), hasFocus ? R.animator.scale_up_apps : R.animator.scale_down_50);
            animator.setTarget(layout);
            animator.start();
        }

        private Animator loadAnimator(Context context, int resId) {
            Animator animator = AnimatorInflater.loadAnimator(context, resId);
            return animator;
        }
    }

    public interface OnKeyListener {
        boolean onKey(KeyEvent event, int index, InstalledAppUtil.AppInfo appInfo);
    }
}
