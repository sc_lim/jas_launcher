/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.playback;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.ThumbnailBar;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekDataProvider;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import com.altimedia.util.Log;


/**
 * Created by mc.kim on 03,03,2020
 */
public class ThumbnailPresenter extends RowPresenter {
    private final String TAG = ThumbnailPresenter.class.getSimpleName();
    private final VideoPlaybackSeekDataProvider seekDataProvider;
    private final ThumbnailBar.OnThumbnailItemActionListener mOnThumbnailItemActionListener;

    public ThumbnailPresenter(VideoPlaybackSeekDataProvider videoPlaybackSeekDataProvider
            , ThumbnailBar.OnThumbnailItemActionListener onThumbnailItemActionListener) {
        super();
        seekDataProvider = videoPlaybackSeekDataProvider;
        mOnThumbnailItemActionListener = onThumbnailItemActionListener;
        removeHeader();
    }

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View contentsView = inflater.inflate(R.layout.wiget_thumbnail_presenter, null, false);
        ThumbnailItemViewHolder mVodItemViewHolder
                = new ThumbnailItemViewHolder(contentsView, seekDataProvider, mOnThumbnailItemActionListener);

        return mVodItemViewHolder;
    }


    @Override
    protected void onBindRowViewHolder(ViewHolder viewHolder, Object item) {
        super.onBindRowViewHolder(viewHolder, item);
//        ThumbnailListRow listRow = (ThumbnailListRow)item;
        Long seekTime = (Long) item;
        if (Log.INCLUDE) {
            Log.d(TAG, "onBindRowViewHolder | seekTime : " + seekTime);
        }
        ThumbnailItemViewHolder cardView = (ThumbnailItemViewHolder) viewHolder;

        if (seekTime == -1) {
            cardView.view.setVisibility(View.INVISIBLE);
        } else {
            cardView.view.setVisibility(View.VISIBLE);
            cardView.setSeekTime(seekTime);
        }

        seekDataProvider.getThumbnail(seekTime, new VideoPlaybackSeekDataProvider.ResultCallback() {
            @Override
            public void onThumbnailLoaded(Drawable drawable, long seekTime) {
                super.onThumbnailLoaded(drawable, seekTime);
                cardView.getMainImageView().setBackground(drawable);
            }
        });

    }


    @Override
    protected void onUnbindRowViewHolder(ViewHolder viewHolder) {
        super.onUnbindRowViewHolder(viewHolder);
        ThumbnailItemViewHolder cardView = (ThumbnailItemViewHolder) viewHolder;
        cardView.removeMainImage();
    }


    private static class ThumbnailItemViewHolder extends ViewHolder
            implements View.OnFocusChangeListener, View.OnClickListener {
        private final String TAG = ThumbnailItemViewHolder.class.getSimpleName();
        private RelativeLayout thumbNail = null;
        private TextView thumbNailDescription = null;
        StringBuilder builder = new StringBuilder();
        private final View mView;
        private final ThumbnailBar.OnThumbnailItemActionListener mOnThumbnailItemActionListener;
        private final VideoPlaybackSeekDataProvider seekDataProvider;

        public ThumbnailItemViewHolder(View view, VideoPlaybackSeekDataProvider seekDataProvider,
                                       ThumbnailBar.OnThumbnailItemActionListener onThumbnailItemActionListener) {
            super(view);
            this.seekDataProvider = seekDataProvider;
            mView = view;
            view.setOnFocusChangeListener(this);
            view.setOnClickListener(this);
            mOnThumbnailItemActionListener = onThumbnailItemActionListener;
            thumbNail = view.findViewById(R.id.thumbNail);
            thumbNailDescription = view.findViewById(R.id.thumbNailDescription);

        }

        public View getMainImageView() {
            return thumbNail;
        }

        public void setSeekTime(Long time) {
            formatTime(time, builder);
            mView.setTag(time);
            thumbNailDescription.setText(builder.toString());
        }


        public void removeMainImage() {
            thumbNail.setBackground(null);
        }

        private Animator loadAnimator(Context context, int resId) {
            Animator animator = AnimatorInflater.loadAnimator(context, resId);
//            animator.setDuration(animator.getDuration());
            return animator;
        }

        @Override
        public void onClick(View v) {
            if (mOnThumbnailItemActionListener != null) {
                mOnThumbnailItemActionListener.onItemClicked((Long) v.getTag());
            }
        }

        @Override
        public void onFocusChange(View view, boolean b) {
            if (b) {
                mOnThumbnailItemActionListener.onItemSelected((Long) view.getTag());
            }
            Animator animator = loadAnimator(view.getContext(), b ? R.animator.scale_up_thumb : R.animator.scale_down_50);
            animator.setTarget(view);
            animator.start();
        }

        private void formatTime(long ms, StringBuilder sb) {
            sb.setLength(0);
            if (ms < 0) {
                sb.append("");
                return;
            }
            VideoPlaybackSeekDataProvider.Type type = seekDataProvider.getType();
            if (type == VideoPlaybackSeekDataProvider.Type.TIMESHIFT) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(ms);

                long seconds = calendar.get(Calendar.SECOND);
                long minutes = calendar.get(Calendar.MINUTE);
                long hours = calendar.get(Calendar.HOUR_OF_DAY);

                if (hours < 10) {
                    sb.append('0');
                }
                sb.append(hours).append(':');

                if (minutes < 10) {
                    sb.append('0');
                }

                sb.append(minutes).append(':');
                if (seconds < 10) {
                    sb.append('0');
                }
                sb.append(seconds);
            } else {
                long seconds = ms / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                seconds -= minutes * 60;
                minutes -= hours * 60;

                if (hours < 10) {
                    sb.append('0');
                }
                sb.append(hours).append(':');

                if (minutes < 10) {
                    sb.append('0');
                }

                sb.append(minutes).append(':');
                if (seconds < 10) {
                    sb.append('0');
                }
                sb.append(seconds);
            }
        }
    }


}
