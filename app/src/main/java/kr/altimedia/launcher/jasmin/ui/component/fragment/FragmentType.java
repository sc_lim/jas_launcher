/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment;

import kr.altimedia.launcher.jasmin.tv.ui.MiniGuideDialogFragment;
import kr.altimedia.launcher.jasmin.tv.ui.OSDFragment;

public enum FragmentType {
    MINIGUIDE(MiniGuideDialogFragment.CLASS_NAME, 1, false), DCA(OSDFragment.CLASS_NAME, 2, true);

    private final String tag;
    private final int eventKey;
    private boolean autoHide;

    FragmentType(String tag, int eventKey, boolean autoHide) {
        this.tag = tag;
        this.eventKey = eventKey;
        this.autoHide = autoHide;
    }

    public int getEventKey() {
        return eventKey;
    }

    public boolean isAutoHide() {
        return autoHide;
    }

    public static FragmentType findByTag(String tag) {
        FragmentType[] typeArray = values();
        for (FragmentType type : typeArray) {
            if (type.tag.equalsIgnoreCase(tag)) {
                return type;
            }
        }
        return null;
    }
}
