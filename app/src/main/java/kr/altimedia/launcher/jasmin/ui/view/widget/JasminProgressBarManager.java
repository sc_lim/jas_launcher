/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.leanback.app.ProgressBarManager;

import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 12,06,2020
 */
public final class JasminProgressBarManager {
    private ProgressBarManager mProgressBarManager = new ProgressBarManager();

    public void setRootView(ViewGroup rootView) {
        if (rootView == null) {
            return;
        }
        LayoutInflater inflater = LayoutInflater.from(rootView.getContext());
        ViewGroup loadingView = (ViewGroup) inflater.
                inflate(R.layout.view_loading, (ViewGroup) rootView);
        mProgressBarManager.setProgressBarView(loadingView);
    }

    public void show() {
        mProgressBarManager.show();
    }

    public void hide() {
        mProgressBarManager.hide();
    }

}
