/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.dm.def.RentalType;

public enum VodProductType {
    WATCH("Watch", R.string.vod_watch),
    RENT("Rent", R.string.vod_rent), BUY("Buy", R.string.vod_buy),
    ALL_EPISODE("All Episode", R.string.vod_all_ep), RENT_ALL_EPISODE("Rent All Episode", R.string.vod_rent_all_ep), BUY_ALL_EPISODE("Buy All Episode", R.string.vod_buy_all_ep),
    PACKAGE("Package", R.string.vod_package), RENT_PACKAGE("Rent Package", R.string.vod_rent_package), BUY_PACKAGE("Buy Package", R.string.vod_buy_package),
    SUBSCRIBE("Subscription", R.string.vod_subscribe);

    private final String type;
    private final int mName;

    VodProductType(String type, int mName) {
        this.type = type;
        this.mName = mName;
    }

    public static VodProductType getVodProductType(boolean isSingleButtonType, Product product) {
        VodProductType type = getType(isSingleButtonType, product);
        if (Log.INCLUDE) {
            Log.d("[VodProductType]", "getVodProductType, isSingleButtonType : " + isSingleButtonType
                    + ", product : " + product + ", type : " + type);
        }

        return type;
    }

    private static VodProductType getType(boolean isSingleButtonType, Product product) {
        ProductType productType = product.getProductType();
        RentalType rentalType = product.getRentalType();

        switch (productType) {
            case SERIES:
                if (isSingleButtonType) {
                    if (rentalType == RentalType.SUBSCRIBE) {
                        return SUBSCRIBE;
                    }

                    return ALL_EPISODE;
                }

                if (rentalType == RentalType.RENT) {
                    return RENT_ALL_EPISODE;
                } else if (rentalType == RentalType.BUY) {
                    return BUY_ALL_EPISODE;
                }

                break;
            case PACKAGE:
                if (isSingleButtonType) {
                    return PACKAGE;
                }

                if (rentalType == RentalType.RENT) {
                    return RENT_PACKAGE;
                } else if (rentalType == RentalType.BUY) {
                    return BUY_PACKAGE;
                }

                break;
        }

        switch (rentalType) {
            case RENT:
                return VodProductType.RENT;
            case BUY:
                return VodProductType.BUY;
            case SUBSCRIBE:
                return VodProductType.SUBSCRIBE;
        }

        return null;
    }

    public int getName() {
        return mName;
    }
}
