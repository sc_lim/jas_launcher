/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.system.settings.data.ParentalRating;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.TextButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SettingPreviewButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.SettingProfileBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.block.SettingProfileChannelBlockedSetupDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.rating.RatingLockItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.rating.SettingProfileRatingLocksFragmentDialog;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment.KEY_PROFILE;

public class SettingProfileParentalFragment extends SettingProfileBaseFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton {
    public static final String CLASS_NAME = SettingProfileParentalFragment.class.getName();
    private final String TAG = SettingProfileParentalFragment.class.getSimpleName();

    public static final String KEY_RATING = "RATING";

    private static final int RATING_ROW = 0;
    private static final int BLOCKED_ROW = 1;

    private ProfileInfo profileInfo;

    private ArrayObjectAdapter objectAdapter;
    private List<String> blockedList = new ArrayList<>();

    private ParentalRating parentalRating;
    private MbsDataTask blockedTask;

    private ChannelDataManager mChannelDataManager;

    private SettingProfileParentalFragment() {
    }

    public static SettingProfileParentalFragment newInstance(Bundle bundle) {
        SettingProfileParentalFragment fragment = new SettingProfileParentalFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_setting_profile_parental;
    }

    @Override
    protected void initView(View view) {
        profileInfo = getArguments().getParcelable(KEY_PROFILE);
        parentalRating = new RatingLockItem(profileInfo.getRating()).getParentalRating();

        mChannelDataManager = TvSingletons.getSingletons(getContext()).getChannelDataManager();

        TextView name = view.findViewById(R.id.profile_name);
        name.setText(profileInfo.getProfileName());

        initButtons(view);
        initData();
    }

    private void initButtons(View view) {
        String rating = getRatingString(profileInfo.getRating());

        objectAdapter = new ArrayObjectAdapter(new SettingPreviewButtonPresenter());
        objectAdapter.add(RATING_ROW, new ParentalItem(R.string.rating_locks, rating, true, -1));
        objectAdapter.add(BLOCKED_ROW, new ParentalItem(R.string.blocked_channels, "", false, R.string.no_blocked_channels));

        VerticalGridView gridView = view.findViewById(R.id.gridView);
        TextButtonBridgeAdapter bridgeAdapter = new TextButtonBridgeAdapter(objectAdapter);
        bridgeAdapter.setListener(this);
        gridView.setAdapter(bridgeAdapter);

        int gap = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        gridView.setVerticalSpacing(gap);
    }

    private void initData() {
        loadBlockChannelList();
    }

    private void loadBlockChannelList() {
        blockedTask = onFragmentChange.getSettingTaskManager().getProfileBlockedChannel(
                profileInfo.getProfileId(),
                new MbsDataProvider<String, List<String>>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "initData, onFailed");
                        }

                        onFragmentChange.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, List<String> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "initData, onSuccess, size : " + result.size());
                        }

                        blockedList.clear();
                        blockedList.addAll(0, result);

                        updateBlockedCount(blockedList.size());
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        onFragmentChange.getSettingTaskManager()
                                .showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingProfileParentalFragment.this.getContext();
                    }
                });
    }

    private String getRatingString(int rating) {
        return ParentalRating.findByRating(getContext(), rating);
    }

    private void updateRatingLock(int rating) {
        objectAdapter.replace(RATING_ROW, new ParentalItem(R.string.rating_locks, getRatingString(rating), true, -1));
    }

    private void updateBlockedCount(int size) {
        objectAdapter.replace(BLOCKED_ROW, new ParentalItem(R.string.blocked_channels, String.valueOf(size), !(size == 0), R.string.no_blocked_channels));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (blockedTask != null) {
            blockedTask.cancel(true);
        }
    }

    @Override
    public boolean onClickButton(Object item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        TvOverlayManager tvOverlayManager = onFragmentChange.getTvOverlayManager();
        int index = objectAdapter.indexOf(item);
        if (index == RATING_ROW) {
            Bundle bundle = getArguments();
            bundle.putSerializable(KEY_RATING, parentalRating);
            SettingProfileRatingLocksFragmentDialog settingProfileRatingLocksFragmentDialog = SettingProfileRatingLocksFragmentDialog.newInstance(bundle);
            settingProfileRatingLocksFragmentDialog.setOnClickRatingListener(new SettingProfileRatingLocksFragmentDialog.OnClickRatingListener() {
                @Override
                public void onClickRating(ParentalRating rating) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onClickRating, rating : " + rating);
                    }

                    parentalRating = rating;
                    updateRatingLock(parentalRating.getRating());
                    profileInfo.setRating(parentalRating.getRating());
                    if (Log.INCLUDE) {
                        Log.d(TAG, "profileInfo : " + profileInfo.toString());
                    }
                    AccountManager.getInstance().updateProfile(profileInfo);
                    mOnPushProfileChangeMessage.onPushProfile(PushMessageReceiver.ProfileUpdateType.rating, profileInfo);
                    settingProfileRatingLocksFragmentDialog.dismiss();
                }
            });
            tvOverlayManager.showDialogFragment(settingProfileRatingLocksFragmentDialog);
        } else if (index == BLOCKED_ROW) {
            SettingProfileChannelBlockedSetupDialogFragment settingProfileChannelBlockedSetupDialogFragment =
                    SettingProfileChannelBlockedSetupDialogFragment.newInstance(profileInfo, (ArrayList<String>) blockedList);
            settingProfileChannelBlockedSetupDialogFragment.setOnBlockedChannelChangeResultCallback(new SettingProfileChannelBlockedSetupDialogFragment.OnBlockedChannelChangeResultCallback() {
                @Override
                public void onChangeBlockedChannel(boolean isSuccess, List<String> list) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onChangeBlockedChannel, isSuccess : " + isSuccess);
                    }

                    settingProfileChannelBlockedSetupDialogFragment.dismiss();
                    if (isSuccess) {
                        blockedList = list;
                        mChannelDataManager.setBlockChannelList(blockedList);
                        updateBlockedCount(list.size());
                        mOnPushProfileChangeMessage.onPushProfile(PushMessageReceiver.ProfileUpdateType.blockedChannel, profileInfo);
                    }
                }
            });
            tvOverlayManager.showDialogFragment(settingProfileChannelBlockedSetupDialogFragment);
        }

        return true;
    }
}
