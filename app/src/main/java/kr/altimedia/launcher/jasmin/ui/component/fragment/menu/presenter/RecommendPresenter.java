/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.dm.contents.obj.BannerImageAsset;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;

public class RecommendPresenter extends RowPresenter {

    private static final DateFormat sDateFormat = new SimpleDateFormat("MMM dd, yyyy");

    private RecommendPresenter.LoadWorkPosterListener mLoadWorkPosterListener;

    public RecommendPresenter() {
        super();
    }

    public RecommendPresenter(int headerLayoutId) {
        super(headerLayoutId);
    }

    public void setLoadWorkPosterListener(RecommendPresenter.LoadWorkPosterListener loadMoviePosterListener) {
        mLoadWorkPosterListener = loadMoviePosterListener;
    }

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        RecommendItemViewHolder mVodItemViewHolder = new RecommendItemViewHolder(inflater.inflate(R.layout.item_recommend_contents, null, false));
        return mVodItemViewHolder;
    }


    @Override
    protected void onBindRowViewHolder(ViewHolder viewHolder, Object item) {
        super.onBindRowViewHolder(viewHolder, item);
        viewHolder.view.setAlpha(1f);
        Banner work = (Banner) item;
        BannerImageAsset imageAsset = work.getBannerImageAsset();
        if (imageAsset == null) {
            return;
        }
        RecommendItemViewHolder cardView = (RecommendItemViewHolder) viewHolder;
        cardView.setText(imageAsset);


        Glide.with(viewHolder.view.getContext())
                .load(imageAsset.getPosterSourceUrl(Banner.BANNER.width(),
                        Banner.BANNER.height())).placeholder(R.drawable.home_banner_default).
                diskCacheStrategy(DiskCacheStrategy.DATA).error(R.drawable.home_banner_default)
                .centerCrop()
                .into(cardView.getMainImageView());
    }


    @Override
    protected void onUnbindRowViewHolder(ViewHolder viewHolder) {
        super.onUnbindRowViewHolder(viewHolder);
        RecommendItemViewHolder cardView = (RecommendItemViewHolder) viewHolder;
        cardView.removeBadgeImage();
        cardView.removeMainImage();
    }


    public interface LoadWorkPosterListener {

        void onLoadWorkPoster(Content work, ImageView imageView);

    }


    private static class RecommendItemViewHolder extends ViewHolder implements View.OnFocusChangeListener {
        private final String TAG = RecommendItemViewHolder.class.getSimpleName();
        private ImageView mainImageView = null;

        public RecommendItemViewHolder(View view) {
            super(view);
            view.setOnFocusChangeListener(this);
            mainImageView = view.findViewById(R.id.mainImage);

        }

        public ImageView getMainImageView() {
            return mainImageView;
        }

        public void setText(BannerImageAsset title) {
        }


        public void removeBadgeImage() {

        }

        public void removeMainImage() {
            mainImageView.setImageDrawable(null);
        }


        @Override
        public void onFocusChange(View view, boolean b) {
        }
    }
}
