/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.packages.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PackageContent;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.user.object.Membership;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.managerProvider;
import kr.altimedia.launcher.jasmin.ui.util.task.UserTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.VodOverviewRow;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class PackageDetailRowPresenter extends RowPresenter {
    private static final String TAG = PackageDetailRowPresenter.class.getSimpleName();
    private managerProvider managerProvider;

    public void setManagerProvider(managerProvider managerProvider) {
        this.managerProvider = managerProvider;
    }

    private int getLayoutResourceId() {
        return R.layout.presenter_package_detail;
    }

    @Override
    protected RowPresenter.ViewHolder createRowViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(getLayoutResourceId(), parent, false);
        return new VodDetailsRowPresenterViewHolder(v);
    }

    @Override
    protected void initializeRowViewHolder(ViewHolder vh) {
        super.initializeRowViewHolder(vh);
        VodDetailsRowPresenterViewHolder viewHolder = (VodDetailsRowPresenterViewHolder) vh;
        viewHolder.mActionBridgeAdapter = new ActionsItemBridgeAdapter(viewHolder);
    }

    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);

        VodOverviewRow row = (VodOverviewRow) item;
        VodDetailsRowPresenterViewHolder viewHolder = (VodDetailsRowPresenterViewHolder) holder;
        viewHolder.setData((PackageContent) row.getItem());
        viewHolder.setManagerProvider(managerProvider);
        viewHolder.onBind();
    }

    @Override
    protected void onUnbindRowViewHolder(ViewHolder vh) {
        VodDetailsRowPresenterViewHolder viewHolder = (VodDetailsRowPresenterViewHolder) vh;
        viewHolder.setOnItemViewClickedListener(null);
        viewHolder.setBaseOnItemKeyListener(null);
        viewHolder.onUnbind();

        super.onUnbindRowViewHolder(vh);
    }

    public class VodDetailsRowPresenterViewHolder extends RowPresenter.ViewHolder
            implements UserTaskManager.OnUpdateUserDiscountListener {
        private LinearLayout packageTitleView = null;
        private LinearLayout packageDetailView = null;
        private TextView packageTitle = null;
        private TextView packageCount = null;
        private TextView packageDescription = null;

        private LinearLayout packageItemTitleView = null;
        private LinearLayout packageItemDetailView = null;
        private TextView title = null;
        private TextView releaseDate = null;
        private TextView genre = null;
        private TextView duration = null;
        private TextView director = null;
        private TextView cast = null;
        private TextView description = null;

        private TextView membershipView = null;
        private TextView couponPoint = null;
        private TextView couponDiscount = null;

        private ItemBridgeAdapter mActionBridgeAdapter;
        private HorizontalGridView mActionsRow;

        private VodOverviewRow.Listener mRowListener = createRowListener();
        private managerProvider managerProvider;

        private final UserTaskManager userTaskManager = UserTaskManager.getInstance();

        public VodDetailsRowPresenterViewHolder(View rootView) {
            super(rootView);

            initView(rootView);

            userTaskManager.addUserDiscountListener(this);
        }

        private void initView(View view) {
            //package view
            packageTitleView = view.findViewById(R.id.package_title_view);
            packageDetailView = view.findViewById(R.id.package_detail_view);
            packageTitle = view.findViewById(R.id.package_title);
            packageCount = view.findViewById(R.id.package_count);
            packageDescription = view.findViewById(R.id.package_description);

            //package item view
            packageItemTitleView = view.findViewById(R.id.package_item_title_view);
            packageItemDetailView = view.findViewById(R.id.package_item_detail_view);
            title = view.findViewById(R.id.title);
            releaseDate = view.findViewById(R.id.releaseDate);
            genre = view.findViewById(R.id.genre);
            duration = view.findViewById(R.id.duration);
            director = view.findViewById(R.id.director);
            cast = view.findViewById(R.id.cast);
            description = view.findViewById(R.id.description);

            membershipView = view.findViewById(R.id.membership);
            couponPoint = view.findViewById(R.id.coupon_point);
            couponDiscount = view.findViewById(R.id.coupon_discount);
        }

        public void setData(PackageContent packageDetailData) {
            packageTitle.setText(packageDetailData.getPackageTitle());
            packageCount.setText(String.valueOf(packageDetailData.getContentCnt()));
            packageDescription.setText(packageDetailData.getPackageDescription());

            setMembershipView();
            setCouponView();
        }

        public void setManagerProvider(managerProvider managerProvider) {
            this.managerProvider = managerProvider;
        }

        public void showPackageDetail() {
            packageItemTitleView.setVisibility(View.INVISIBLE);
            packageItemDetailView.setVisibility(View.INVISIBLE);

            packageTitleView.setVisibility(View.VISIBLE);
            packageDetailView.setVisibility(View.VISIBLE);
        }

        public void showPackageItemDetail(Content content) {
            packageItemTitleView.setVisibility(View.VISIBLE);
            packageItemDetailView.setVisibility(View.VISIBLE);

            packageTitleView.setVisibility(View.INVISIBLE);
            packageDetailView.setVisibility(View.INVISIBLE);

            title.setText(content.getTitle());
            releaseDate.setText(content.getReleaseDate());
            genre.setText(content.getGenre().toString()
                    .replace("[", "").replace("]", ""));
            duration.setText(String.valueOf(content.getRunTime() / TimeUtil.MIN));
            director.setText(content.getDirector());
            cast.setText(content.getActor());
            description.setText(content.getSynopsis());
        }

        private void setMembershipView() {
            Membership membership = userTaskManager.getMembership();
            if (membership != null) {
                int membershipPoint = membership.getMembershipPoint();
                membershipView.setText(StringUtil.getFormattedNumber(membershipPoint));
            }
        }

        private void setCouponView() {
            int totalPointCoupon = userTaskManager.getPointCoupon();
            int size = userTaskManager.getDiscountCouponSize();

            couponPoint.setText(StringUtil.getFormattedNumber(totalPointCoupon));
            couponDiscount.setText(StringUtil.getFormattedNumber(size));
        }

        protected VodOverviewRow.Listener createRowListener() {
            return new VodOverviewRowListener();
        }

        void onBind() {
            VodOverviewRow row = (VodOverviewRow) getRow();
            bindActions(row.getActionsAdapter());
            row.addListener(mRowListener);
        }

        void onUnbind() {
            VodOverviewRow row = (VodOverviewRow) getRow();
            row.removeListener(mRowListener);
            mActionsRow.setAdapter(null);
            mRowListener = null;
            mActionBridgeAdapter.clear();
            mActionBridgeAdapter = null;

            userTaskManager.removeUserDiscountListener(this);
            managerProvider = null;
        }

        void bindActions(ObjectAdapter adapter) {
            mActionBridgeAdapter.setAdapter(adapter);
            initProductGridView(mActionBridgeAdapter, adapter.size());
        }

        private void initProductGridView(ItemBridgeAdapter mActionBridgeAdapter, int buttonSize) {
            if (Log.INCLUDE) {
                Log.d(TAG, "initProductGridView, size : " + buttonSize);
            }

            mActionsRow = view.findViewById(R.id.details_overview_actions);

            int productButtonSize = buttonSize - 1; // vod like
            int gap = (int) view.getResources().getDimension(R.dimen.vod_detail_button_gap);
            int dimen = (int) view.getResources().getDimension(R.dimen.vod_product_button_width) + gap;
            int width = productButtonSize < 2 ? (productButtonSize * dimen) : (2 * dimen);

            ViewGroup.LayoutParams params = mActionsRow.getLayoutParams();
            params.width = (int) (width + view.getResources().getDimension(R.dimen.vod_like_button_width));

            mActionsRow.setLayoutParams(params);
            mActionsRow.setHorizontalSpacing(gap);
            mActionsRow.setAdapter(mActionBridgeAdapter);
        }

        @Override
        public void notifyUserDiscountChange(int pointCouponSize, int discountCouponSize, ArrayList<DiscountCoupon> discountCoupons, Membership membership) {
            setCouponView();
            setMembershipView();
        }

        @Override
        public void notifyFail(int key) {
        }

        @Override
        public void notifyError(String errorCode, String message) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + message);
            }

            ErrorDialogFragment dialogFragment =
                    ErrorDialogFragment.newInstance(TAG, errorCode, message);
            dialogFragment.show(managerProvider.getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
        }

        public class VodOverviewRowListener extends VodOverviewRow.Listener {
            @Override
            public void onImageDrawableChanged(VodOverviewRow row) {
            }

            @Override
            public void onItemChanged(VodOverviewRow row) {
            }

            @Override
            public void onActionsAdapterChanged(VodOverviewRow row) {
                bindActions(row.getActionsAdapter());
            }
        }
    }

    private class ActionsItemBridgeAdapter extends ItemBridgeAdapter {
        VodDetailsRowPresenterViewHolder mViewHolder;

        ActionsItemBridgeAdapter(VodDetailsRowPresenterViewHolder viewHolder) {
            mViewHolder = viewHolder;
        }

        @Override
        public void onBind(final ItemBridgeAdapter.ViewHolder ibvh) {
            if (mViewHolder.getOnItemViewClickedListener() != null) {
                Presenter presenter = ibvh.getPresenter();
                presenter.setOnClickListener(
                        ibvh.getViewHolder(), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mViewHolder.getOnItemViewClickedListener() != null) {
                                    mViewHolder.getOnItemViewClickedListener().onItemClicked(
                                            ibvh.getViewHolder(), ibvh.getItem(),
                                            mViewHolder, mViewHolder.getRow());
                                }
                            }
                        });
            }
        }

        @Override
        public void onUnbind(final ItemBridgeAdapter.ViewHolder ibvh) {
            if (mViewHolder.getOnItemViewClickedListener() != null) {
                ibvh.getPresenter().setOnClickListener(ibvh.getViewHolder(), null);
            }
        }
    }
}