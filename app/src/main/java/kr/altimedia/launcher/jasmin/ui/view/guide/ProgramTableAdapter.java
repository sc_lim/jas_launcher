/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityManager.AccessibilityStateChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.util.CommonUtils;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.RecycledViewPool;
import kr.altimedia.launcher.jasmin.R;

/**
 * Adapts the {@link ProgramListAdapter} list to the body of the program guide table.
 */
class ProgramTableAdapter extends RecyclerView.Adapter<ProgramTableAdapter.ProgramRowViewHolder>
        implements ProgramManager.TableEntryChangedListener, ProgramManager.TableEntriesUpdatedListener {
    private static final String TAG = "ProgramTableAdapter";

    private final Context mContext;
    private final TvInputManagerHelper mTvInputManagerHelper;
    private final ProgramManager mProgramManager;
    private final AccessibilityManager mAccessibilityManager;
    private final ProgramGuide mProgramGuide;
    private final Handler mHandler = new Handler();
    private final List<ProgramListAdapter> mProgramListAdapters = new ArrayList<>();
    private final RecycledViewPool mRecycledViewPool;

    private RecyclerView mRecyclerView;

    ProgramTableAdapter(Context context, ProgramGuide programGuide) {
        mContext = context;
        mAccessibilityManager =
                (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
        mTvInputManagerHelper = TvSingletons.getSingletons(context).getTvInputManagerHelper();
        mProgramGuide = programGuide;
        mProgramManager = programGuide.getProgramManager();

        mRecycledViewPool = new RecycledViewPool();
        mRecycledViewPool.setMaxRecycledViews(
                R.layout.program_guide_table_item,
                context.getResources().getInteger(R.integer.max_recycled_view_pool_epg_table_item));
        mProgramManager.addListener(
                new ProgramManager.ListenerAdapter() {
                    @Override
                    public void onChannelsUpdated() {
                        update();
                    }
                });
        update();
        mProgramManager.addTableEntryChangedListener(this);
        mProgramManager.addTableEntriesUpdatedListener(this);
    }


    private void update() {
        if (Log.INCLUDE) {
            Log.d(TAG, "update " + mProgramManager.getChannelCount() + " channels");
        }
        for (ProgramManager.TableEntriesUpdatedListener listener : mProgramListAdapters) {
            mProgramManager.removeTableEntriesUpdatedListener(listener);
        }
        mProgramListAdapters.clear();
        for (int i = 0; i < mProgramManager.getChannelCount(); i++) {
            ProgramListAdapter listAdapter =
                    new ProgramListAdapter(mContext.getResources(), mProgramGuide, i);
            mProgramManager.addTableEntriesUpdatedListener(listAdapter);
            mProgramListAdapters.add(listAdapter);
        }
        if (mRecyclerView != null && mRecyclerView.isComputingLayout()) {
            // it means that RecyclerView is in a lockdown state and any attempt to update adapter
            // contents will result in an exception because adapter contents cannot be changed while
            // RecyclerView is trying to compute the layout
            // postpone the change using a Handler
            mHandler.post(this::notifyDataSetChanged);
        } else {
            notifyDataSetChanged();
        }
    }


    @Override
    public int getItemCount() {
        return mProgramListAdapters.size();
    }


    @Override
    public int getItemViewType(int position) {
        return R.layout.program_guide_table_row;
    }

    @Override
    public void onBindViewHolder(ProgramRowViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public void onBindViewHolder(ProgramRowViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public void onViewRecycled(@NonNull ProgramRowViewHolder holder) {
        super.onViewRecycled(holder);
        holder.unBind(holder);
    }


    @Override
    public ProgramRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        ProgramRow programRow = itemView.findViewById(R.id.row);
        programRow.setRecycledViewPool(mRecycledViewPool);
        return new ProgramRowViewHolder(itemView);
    }

    @Override
    public void onTableEntriesUpdated() {
//        notifyDataSetChanged();
//        notifyItemChanged(1);
    }

    public void notifyChannelUpdate(ProgramListAdapter listAdapter) {

        int channelIndex = listAdapter.getChannelIndex();
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyUpdate : " + channelIndex);
        }

        if (channelIndex >= 0 && channelIndex < mProgramListAdapters.size()) {
            notifyItemChanged(channelIndex, true);
        }
    }


    @Override
    public void onTableEntryChanged(ProgramManager.TableEntry tableEntry) {
        int channelIndex = mProgramManager.getChannelIndex(tableEntry.channelId);
        int pos = mProgramManager.getProgramIdIndex(tableEntry.channelId, tableEntry.getId());
        if (Log.INCLUDE) {
            Log.d(TAG, "update(" + channelIndex + ", " + pos + ")");
        }

        if (channelIndex >= 0 && channelIndex < mProgramListAdapters.size()) {
            mProgramListAdapters.get(channelIndex).notifyItemChanged(pos, tableEntry);
            notifyItemChanged(channelIndex, true);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        mRecyclerView = null;
    }

    class ProgramRowViewHolder extends RecyclerView.ViewHolder
            implements ProgramRow.ChildFocusListener {

        private final ViewGroup mContainer;
        private final ProgramRow mProgramRow;
        private ProgramManager.TableEntry mSelectedEntry;

        private final RecyclerView.OnScrollListener mOnScrollListener =
                new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        onHorizontalScrolled();
                    }
                };

        private final ViewTreeObserver.OnGlobalFocusChangeListener mGlobalFocusChangeListener =
                new ViewTreeObserver.OnGlobalFocusChangeListener() {
                    @Override
                    public void onGlobalFocusChanged(View oldFocus, View newFocus) {
                        onChildFocus(
                                GuideUtils.isDescendant(mContainer, oldFocus) ? oldFocus : null,
                                GuideUtils.isDescendant(mContainer, newFocus) ? newFocus : null);
                    }
                };

        // Members of Channel Header
        private Channel mChannel;
        private final View mChannelHeaderView;
        private final TextView mChannelNumberView;
        private final TextView mChannelNameView;
        private final ImageView mChannelLogoView;
        private final ImageView mInputLogoView;
        private final ImageView mChannelTimeshift;
        private AccessibilityStateChangeListener mAccessibilityStateChangeListener =
                new AccessibilityStateChangeListener() {
                    @Override
                    public void onAccessibilityStateChanged(boolean enable) {
                        enable &= !CommonUtils.isRunningInTest();
                        mChannelHeaderView.setFocusable(enable);
                        mChannelHeaderView.setSelected(enable);
                    }
                };

        ProgramRowViewHolder(View itemView) {
            super(itemView);

            mContainer = (ViewGroup) itemView;
            mContainer.addOnAttachStateChangeListener(
                    new View.OnAttachStateChangeListener() {
                        @Override
                        public void onViewAttachedToWindow(View v) {
                            mContainer
                                    .getViewTreeObserver()
                                    .addOnGlobalFocusChangeListener(mGlobalFocusChangeListener);
                            mAccessibilityManager.addAccessibilityStateChangeListener(
                                    mAccessibilityStateChangeListener);
                        }

                        @Override
                        public void onViewDetachedFromWindow(View v) {
                            mContainer
                                    .getViewTreeObserver()
                                    .removeOnGlobalFocusChangeListener(mGlobalFocusChangeListener);
                            mAccessibilityManager.removeAccessibilityStateChangeListener(
                                    mAccessibilityStateChangeListener);
                        }
                    });
            mProgramRow = mContainer.findViewById(R.id.row);
            mChannelHeaderView = mContainer.findViewById(R.id.header_column);
            mChannelNumberView = mContainer.findViewById(R.id.channel_number);
            mChannelNameView = mContainer.findViewById(R.id.channel_name);
            mChannelLogoView = mContainer.findViewById(R.id.channel_logo);
            mInputLogoView = mContainer.findViewById(R.id.input_logo);
            mChannelTimeshift = mContainer.findViewById(R.id.channel_timeshift);
            boolean accessibilityEnabled =
                    mAccessibilityManager.isEnabled() && !CommonUtils.isRunningInTest();
            mChannelHeaderView.setFocusable(accessibilityEnabled);

        }

        public boolean isTimeShiftEnabled(Channel channel) {
            return (channel != null && channel.getTimeshiftService() > 0);
        }

        public void onBind(int position) {
            mProgramListAdapters.get(position).setChannelInfo(position, new ProgramListAdapter.OnListUpdatedCallback() {
                @Override
                public void notifyUpdate(ProgramListAdapter listAdapter) {
                    notifyChannelUpdate(listAdapter);
                }
            });
            Channel channel = mProgramManager.getChannel(position);
            onBindChannel(channel);
            boolean isTimeShiftEnable = isTimeShiftEnabled(channel);
            mChannelTimeshift.setVisibility(isTimeShiftEnable ? View.VISIBLE : View.INVISIBLE);
            mProgramRow.swapAdapter(mProgramListAdapters.get(position), true);
            mProgramRow.setProgramGuide(mProgramGuide);
            mProgramRow.setChannel(mProgramManager.getChannel(position));
            mProgramRow.setChildFocusListener(this);
            mProgramRow.resetScroll(mProgramGuide.getTimelineRowScrollOffset());
        }

        public void unBind(ProgramRowViewHolder holder) {
            ProgramListAdapter listAdapter = (ProgramListAdapter) holder.mProgramRow.getAdapter();
            listAdapter.clearListener();
        }

        private Drawable getChannelInputLogo(Context context, Channel channel) {
            if (channel == null) {
                return null;
            }
            if (channel.isAdultChannel()) {
                return context.getDrawable(R.drawable.ch_icon_block);
            } else if (channel.isLocked()) {
                return context.getDrawable(R.drawable.ch_icon_block);
            } else if (channel.isFavoriteChannel()) {
                return context.getDrawable(R.drawable.ch_icon_fav_f);
            } else if (channel.isPaidChannel()) {
                return context.getDrawable(R.drawable.ch_icon_paid);
            } else if (channel.isAudioChannel()) {
                return context.getDrawable(R.drawable.ch_icon_audio);
            }
            return null;
        }

        private void onBindChannel(Channel channel) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onBindChannel " + channel);
            }

            mChannel = channel;
            Drawable channelInputLogo = getChannelInputLogo(mContext, channel);
            if (channelInputLogo == null) {
                mInputLogoView.setVisibility(View.INVISIBLE);
            } else {
                mInputLogoView.setVisibility(View.VISIBLE);
                mInputLogoView.setImageDrawable(channelInputLogo);
            }

            if (channel == null) {
                mChannelNumberView.setVisibility(View.GONE);
                mChannelNameView.setVisibility(View.GONE);
                mChannelLogoView.setVisibility(View.GONE);
                return;
            }

            String displayNumber = channel.getDisplayNumber();
            if (displayNumber == null) {
                mChannelNumberView.setVisibility(View.GONE);
            } else {
                mChannelNumberView.setText(String.format("%03d", Integer.parseInt(displayNumber)));
                mChannelNumberView.setVisibility(View.VISIBLE);
            }

            String logo = channel.getLogoUri();
            Glide.with(mContext)
                    .load(logo).diskCacheStrategy(DiskCacheStrategy.DATA)
                    .centerCrop()
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onLoadStarted(@Nullable Drawable placeholder) {
                            super.onLoadStarted(placeholder);
                            mChannelNameView.setText(channel.getDisplayName());
                            mChannelNameView.setVisibility(View.VISIBLE);
                            mChannelLogoView.setVisibility(View.GONE);
                        }

                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            mChannelLogoView.setImageDrawable(resource);
                            mChannelLogoView.setVisibility(View.VISIBLE);
                            mChannelNameView.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            mChannelNameView.setText(channel.getDisplayName());
                            mChannelNameView.setVisibility(View.VISIBLE);
                            mChannelLogoView.setVisibility(View.GONE);
                        }
                    });
        }

        @Override
        public void onChildFocus(View oldFocus, View newFocus) {
            if (newFocus == null) {
                return;
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "onGlobalFocusChanged " + oldFocus + " -> " + newFocus);
            }
            if (newFocus == mChannelHeaderView) {
                View targetView = mProgramRow.getChildAt(0);
                if (targetView instanceof ProgramAudioItemView) {
                    mSelectedEntry = ((ProgramAudioItemView) targetView).getTableEntry();
                } else {
                    mSelectedEntry = ((ProgramItemView) targetView).getTableEntry();
                }

            } else {
                if (newFocus instanceof ProgramAudioItemView) {
                    mSelectedEntry = ((ProgramAudioItemView) newFocus).getTableEntry();
                } else {
                    mSelectedEntry = ((ProgramItemView) newFocus).getTableEntry();
                }


            }

        }


        void updateInputLogo(int lastPosition, boolean forceShow) {
            if (mChannel == null) {
                mInputLogoView.setVisibility(View.INVISIBLE);
                return;
            }

            boolean showLogo = forceShow;
            if (!showLogo) {
                Channel lastChannel = mProgramManager.getChannel(lastPosition);
                if (lastChannel == null
                        || !mChannel.getInputId().equals(lastChannel.getInputId())) {
                    showLogo = true;
                }
            }

            Drawable channelInputLogo = getChannelInputLogo(mContext, mChannel);
            if (channelInputLogo == null) {
                mInputLogoView.setVisibility(View.INVISIBLE);
            } else {
                mInputLogoView.setVisibility(View.VISIBLE);
                mInputLogoView.setImageDrawable(channelInputLogo);
            }

//            if (showLogo) {
//                if (!mIsInputLogoVisible) {
//                    mIsInputLogoVisible = true;
//                    TvInputInfo info = mTvInputManagerHelper.getTvInputInfo(mChannel.getInputId());
//                }
//            } else {
//                mInputLogoView.setVisibility(View.GONE);
//                mInputLogoView.setImageDrawable(null);
//                mIsInputLogoVisible = false;
//            }
        }

        // The return value of this method will indicate the target view is visible (true)
        // or gone (false).
        private boolean updateTextView(TextView textView, String text) {
            if (!TextUtils.isEmpty(text)) {
                textView.setVisibility(View.VISIBLE);
                textView.setText(text);
                return true;
            } else {
                textView.setVisibility(View.GONE);
                return false;
            }
        }


        private void updateChannelLogo(@Nullable Bitmap logo) {
            mChannelLogoView.setImageBitmap(logo);
            mChannelNameView.setVisibility(View.GONE);
            mChannelLogoView.setVisibility(View.VISIBLE);
        }


        private void onHorizontalScrolled() {
        }
    }

//    private static ImageLoaderCallback<ProgramRowViewHolder> createCriticScoreLogoCallback(
//            ProgramRowViewHolder holder, final long programId, ImageView logoView) {
//        return new ImageLoaderCallback<ProgramRowViewHolder>(holder) {
//            @Override
//            public void onBitmapLoaded(ProgramRowViewHolder holder, @Nullable Bitmap logoImage) {
//                if (logoImage == null
//                        || holder.mSelectedEntry == null
//                        || holder.mSelectedEntry.program == null
//                        || holder.mSelectedEntry.program.getId() != programId) {
//                    logoView.setVisibility(View.GONE);
//                } else {
//                    logoView.setImageBitmap(logoImage);
//                    logoView.setVisibility(View.VISIBLE);
//                }
//            }
//        };
//    }

//    private static ImageLoaderCallback<ProgramRowViewHolder> createProgramPosterArtCallback(
//            ProgramRowViewHolder holder, final Program program) {
//        return new ImageLoaderCallback<ProgramRowViewHolder>(holder) {
//            @Override
//            public void onBitmapLoaded(ProgramRowViewHolder holder, @Nullable Bitmap posterArt) {
//                if (posterArt == null
//                        || holder.mSelectedEntry == null
//                        || holder.mSelectedEntry.program == null) {
//                    return;
//                }
//                String posterArtUri = holder.mSelectedEntry.program.getPosterArtUri();
//                if (posterArtUri == null || !posterArtUri.equals(program.getPosterArtUri())) {
//                    return;
//                }
//                holder.updatePosterArt(posterArt);
//            }
//        };
//    }
//
//    private static ImageLoaderCallback<ProgramRowViewHolder> createChannelLogoLoadedCallback(
//            ProgramRowViewHolder holder, final long channelId) {
//        return new ImageLoaderCallback<ProgramRowViewHolder>(holder) {
//            @Override
//            public void onBitmapLoaded(ProgramRowViewHolder holder, @Nullable Bitmap logo) {
//                if (logo == null
//                        || holder.mChannel == null
//                        || holder.mChannel.getId() != channelId) {
//                    return;
//                }
//                holder.updateChannelLogo(logo);
//            }
//        };
//    }

//    private static ImageLoaderCallback<ProgramRowViewHolder> createTvInputLogoLoadedCallback(
//            final TvInputInfo info, ProgramRowViewHolder holder) {
//        return new ImageLoaderCallback<ProgramRowViewHolder>(holder) {
//            @Override
//            public void onBitmapLoaded(ProgramRowViewHolder holder, @Nullable Bitmap logo) {
//                if (logo != null
//                        && holder.mChannel != null
//                        && info.getId().equals(holder.mChannel.getInputId())) {
//                    holder.updateInputLogoInternal(logo);
//                }
//            }
//        };
//    }
}
