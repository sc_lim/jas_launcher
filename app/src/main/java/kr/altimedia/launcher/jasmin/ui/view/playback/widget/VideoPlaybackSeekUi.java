/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.widget;

/**
 * Created by mc.kim on 18,02,2020
 */
public interface VideoPlaybackSeekUi {
    class Client {

        public boolean isSeekEnabled() {
            return false;
        }

        public void onSeekStarted() {
        }

        public VideoPlaybackSeekDataProvider getPlaybackSeekDataProvider() {
            return null;
        }

        /**
         * Called when user seeks to a different location. This callback is called multiple times
         * between {@link #onSeekStarted()} and {@link #onSeekFinished(boolean)}.
         *
         * @param pos Position that user seeks to.
         */
        public void onSeekPositionChanged(long pos) {
        }

        /**
         * Called when cancelled or confirmed. When cancelled, client should restore playing from
         * the position before {@link #onSeekStarted()}. When confirmed, client should seek to
         * last updated {@link #onSeekPositionChanged(long)}.
         *
         * @param cancelled True if cancelled false if confirmed.
         */
        public void onSeekFinished(boolean cancelled) {
        }
    }

    /**
     * Interface to be implemented by UI widget to support PlaybackSeekUi.
     */
    void setPlaybackSeekUiClient(Client client);
}
