/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;

public class OtpResponse extends BaseResponse {
    public static final Creator<OtpResponse> CREATOR = new Creator<OtpResponse>() {
        @Override
        public OtpResponse createFromParcel(Parcel in) {
            return new OtpResponse(in);
        }

        @Override
        public OtpResponse[] newArray(int size) {
            return new OtpResponse[size];
        }
    };
    @Expose
    @SerializedName("data")
    private Otp otp;

    protected OtpResponse(Parcel in) {
        super(in);
        otp = (Otp) in.readValue(Otp.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(otp);
    }

    public Otp getOtp() {
        return otp;
    }

    public static class Otp implements Parcelable {
        public static final Creator<Otp> CREATOR = new Creator<Otp>() {
            @Override
            public Otp createFromParcel(Parcel in) {
                return new Otp(in);
            }

            @Override
            public Otp[] newArray(int size) {
                return new Otp[size];
            }
        };

        @Expose
        @SerializedName("otp")
        private String otp;
        @Expose
        @SerializedName("expireMinute")
        private int expireMinute;

        protected Otp(Parcel in) {
            otp = in.readString();
            expireMinute = in.readInt();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(otp);
            dest.writeInt(expireMinute);
        }

        public String getOtp() {
            return otp;
        }

        public int getExpireMinute() {
            return expireMinute;
        }

        @Override
        public String toString() {
            return "Otp{" +
                    "otp='" + otp + '\'' +
                    ", expireMinute=" + expireMinute +
                    '}';
        }
    }
}
