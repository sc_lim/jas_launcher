/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;

public class VodPaymentFailFragment extends VodPaymentBaseFragment {
    public static final String CLASS_NAME = VodPaymentFailFragment.class.getName();
    private static final String TAG = VodPaymentFailFragment.class.getSimpleName();

    private static final String KEY_PURCHASED_INFO = "PURCHASED_INFO";
    private static final String KEY_PAYMENT_ERROR_MESSAGE = "PAYMENT_ERROR_MESSAGE";

    public VodPaymentFailFragment() {
    }

    public static VodPaymentFailFragment newInstance(PurchaseInfo purchaseInfo, String paymentErrorMessage) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_PURCHASED_INFO, purchaseInfo);
        args.putString(KEY_PAYMENT_ERROR_MESSAGE, paymentErrorMessage);

        VodPaymentFailFragment fragment = new VodPaymentFailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_vod_payment_fail;
    }

    @Override
    protected void initView(View view) {
        initLayout(view);
        initErrorMessage(view);
    }

    private void initLayout(View view) {
        PurchaseInfo purchaseInfo = getArguments().getParcelable(KEY_PURCHASED_INFO);

        String title = purchaseInfo.getTitle();
        ((TextView) view.findViewById(R.id.title)).setText(title);

        view.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOnPaymentListener().onRequestWatch(false);
            }
        });
    }

    private void initErrorMessage(View view) {
        String paymentErrorMessage = getArguments().getString(KEY_PAYMENT_ERROR_MESSAGE);

        TextView errorView = view.findViewById(R.id.error_desc);
        errorView.setText(paymentErrorMessage);
    }
}