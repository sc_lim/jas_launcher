/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service;

public interface CWMPEventListener {

    void onSetChangeChannel(String value);

    void onSetAgeLimit(int value);

    void onSetChannelLimit(String value);

    void onSetFavoriteChannel(String value);

    void onSetSubtitle(int value);

    void onSetFontSize(int value);

    void onSetVisualLanguage(String value);

    void onSetConfigInit(int value);

    void onSetReboot(int value);

    void onSetStandBy(int value);

    void onFactoryReset();

    void onReboot();

    void onAppRestart();

    void onUpgradeTrigger();

    void onNormalPeriodUpdated(int value);

    void onFwUrlUpdated(String value);

    void onApkUrlUpdated(String value);
}