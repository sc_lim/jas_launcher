/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 08,05,2020
 */
public class SeasonInfo implements Parcelable {
    @Expose
    @SerializedName("seasonId")
    private String seasonID;
    @Expose
    @SerializedName("seriesAssetId")
    private String seriesAssetId;
    @Expose
    @SerializedName("titleAssetId")
    private String seasonTitle;


    protected SeasonInfo(Parcel in) {
        seasonID = in.readString();
        seriesAssetId = in.readString();
        seasonTitle = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(seasonID);
        dest.writeValue(seriesAssetId);
        dest.writeString(seasonTitle);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SeasonInfo> CREATOR = new Creator<SeasonInfo>() {
        @Override
        public SeasonInfo createFromParcel(Parcel in) {
            return new SeasonInfo(in);
        }

        @Override
        public SeasonInfo[] newArray(int size) {
            return new SeasonInfo[size];
        }
    };

    public String getSeriesAssetId() {
        return seriesAssetId;
    }

    public String getSeasonID() {
        return seasonID;
    }

    public String getSeasonTitle() {
        return seasonTitle;
    }

    @Override
    public String toString() {
        return "SeasonInfo{" +
                "seasonID='" + seasonID + '\'' +
                ", seriesAssetId='" + seriesAssetId + '\'' +
                ", seasonTitle='" + seasonTitle + '\'' +
                '}';
    }
}
