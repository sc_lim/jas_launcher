/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.packages.presenter;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.VodListRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.rowView.ContentsRowView;
import kr.altimedia.launcher.jasmin.ui.view.util.FocusHighlightHelper;

public class PackageVodListRowPresenter extends VodListRowPresenter {
    public PackageVodListRowPresenter(int mContentsResourceId) {
        super(mContentsResourceId);
    }

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {
        ContentsRowView rowView = mContentsResourceId != -1 ?
                new ContentsRowView(parent.getContext(), mContentsResourceId) : new ContentsRowView(parent.getContext());
        return new PackageVodListRowViewHolder(rowView, rowView.getGridView(), this);
    }

    @Override
    protected void initializeRowViewHolder(RowPresenter.ViewHolder vh) {
        super.initializeRowViewHolder(vh);
        PackageVodListRowViewHolder viewHolder = (PackageVodListRowViewHolder) vh;
        viewHolder.mItemBridgeAdapter = new PackageVodListItemBridgeAdapter(viewHolder);
        FocusHighlightHelper.setupBrowseItemFocusHighlight(viewHolder.mItemBridgeAdapter, this.mFocusZoomFactor, false);
    }

    public static class PackageVodListRowViewHolder extends VodListRowPresenter.ViewHolder {
        private PackageVodHeaderPresenter.PackageVodHeaderPresenterViewHolder headerVh;

        public PackageVodListRowViewHolder(View rootView, HorizontalGridView gridView, PackageVodListRowPresenter p) {
            super(rootView, gridView, p);
        }

        @Override
        public void initGridView() {
            headerVh = (PackageVodHeaderPresenter.PackageVodHeaderPresenterViewHolder) getHeaderViewHolder();
            View headerView = headerVh.view;
            int headerBottom = headerVh.getHeaderViewHeight();

            Resources resources = headerView.getResources();
            int left = (int) resources.getDimension(R.dimen.package_detail_padding_left);
            int right = left;
            int top = (int) (headerBottom + resources.getDimension(R.dimen.package_header_padding_bottom));

            mGridView.setPadding(left, top, right, 0);
            mGridView.setClipChildren(false);

            int gap = (int) resources.getDimension(R.dimen.poster_horizontal_space);
            mGridView.setHorizontalSpacing(gap);
        }

        public void setIndex(int index) {
            headerVh.setFocusedIndex(index);
        }

        public void setCountVisibility(boolean isVisible) {
            headerVh.setCountVisibility(isVisible);
        }
    }

    private static class PackageVodListItemBridgeAdapter extends VodListItemBridgeAdapter {
        public PackageVodListItemBridgeAdapter(PackageVodListRowViewHolder mRowViewHolder) {
            super(mRowViewHolder);
        }

        @Override
        public void onBind(ViewHolder viewHolder) {
            super.onBind(viewHolder);
        }

        @Override
        public void onUnbind(ViewHolder viewHolder) {
            super.onUnbind(viewHolder);
        }
    }
}
