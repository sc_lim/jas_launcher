/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileIconCategory;

public class ProfileIconCategoryListResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private ProfileIconCategoryList data;

    protected ProfileIconCategoryListResponse(Parcel in) {
        super(in);
        data = in.readParcelable(ProfileIconCategoryListResponse.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(data);
    }

    public List<ProfileIconCategory> getProfileIconCategoryList() {
        return data.list;
    }

    private static class ProfileIconCategoryList implements Parcelable {
        public static final Parcelable.Creator<ProfileIconCategoryList> CREATOR = new Parcelable.Creator<ProfileIconCategoryList>() {
            @Override
            public ProfileIconCategoryList createFromParcel(Parcel in) {
                return new ProfileIconCategoryList(in);
            }

            @Override
            public ProfileIconCategoryList[] newArray(int size) {
                return new ProfileIconCategoryList[size];
            }
        };
        @Expose
        @SerializedName("menu")
        public java.util.List<ProfileIconCategory> list;

        protected ProfileIconCategoryList(Parcel in) {
            in.readList(list, ProfileIconCategory.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(list);
        }

        @Override
        public int describeContents() {
            return 0;
        }
    }
}
