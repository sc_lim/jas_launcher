/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.activity.interactor;

import android.os.Parcelable;

import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.media.MediaPlayerAdapter;

/**
 * Created by mc.kim on 01,06,2020
 */
public interface VideoPlaybackInteractor {

    void onErrorWhenReady(Content content, String errorCode);

    void requestTuneVod(Content content);

    void requestVodWatched(Content content);

    void requestShowVodDetail(Content content);

    void requestTuneTrailer(Parcelable sourceData, StreamInfo selectedStream);

    void setSideOptionProvider(Content content, MediaPlayerAdapter adapter);
}
