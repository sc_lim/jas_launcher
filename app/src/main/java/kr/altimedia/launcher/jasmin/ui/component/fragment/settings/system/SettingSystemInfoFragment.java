package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;

import androidx.annotation.NonNull;
import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.BuildConfig;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPCPEManager;
import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import kr.altimedia.launcher.jasmin.system.manager.ExternalApplicationManager;
import kr.altimedia.launcher.jasmin.system.manager.SystemInfoManager;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.PageBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system.presenter.SystemInfoPresenter;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class SettingSystemInfoFragment extends SettingBaseFragment implements View.OnKeyListener {
    public static final String CLASS_NAME = SettingSystemInfoFragment.class.getName();
    private String TAG = SettingSystemInfoFragment.class.getSimpleName();

    private final int DISCONNECT_WIFI = -999;
    private final String INVALID_RSSI = "N/A";

    private final int TOTAL_INFO = 15;
    private final int VISIBLE_ROW = 9;

    private final int ROW_NETWORK = 5;
    private final int ROW_WIFI_RSSI = 6;
    private final int ROW_WIFI_STATUS = 7;

    private final int UPDATE_MESSAGE_ID = 0;
    private final long UPDATE_INTERVAL = 10 * TimeUtil.SEC;
    private final Handler updateHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            if (Log.INCLUDE) {
                Log.d(TAG, "handleMessage, update network info");
            }

            objectAdapter.replace(ROW_NETWORK, new SystemInfoPresenter.SystemInfo(R.string.sysinfo_network, getNetworkStatus()));
            int rssi = getWifiRssi();
            String wifiRssi = !isConnectedByWifi() || rssi == DISCONNECT_WIFI ? INVALID_RSSI : "" + rssi + "dBm";
            String wifiRssiStatus = getWifiRssiSatus(rssi);
            objectAdapter.replace(ROW_WIFI_RSSI, new SystemInfoPresenter.SystemInfo(R.string.sysinfo_wifi_rssi, wifiRssi));
            objectAdapter.replace(ROW_WIFI_STATUS, new SystemInfoPresenter.SystemInfo(R.string.sysinfo_wifi_status, wifiRssiStatus));

            updateHandler.sendEmptyMessageDelayed(UPDATE_MESSAGE_ID, UPDATE_INTERVAL);
        }
    };

    private int totalPage;
    private PagingVerticalGridView gridView;
    private ArrayObjectAdapter objectAdapter;
    private ThumbScrollbar scrollbar;

    private SettingSystemInfoFragment() {
    }

    public static SettingSystemInfoFragment newInstance() {
        return new SettingSystemInfoFragment();
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_setting_system_info;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        initSystemInfos(view);
        initScrollBar(view);
        initButtons();

        updateHandler.sendEmptyMessageDelayed(UPDATE_MESSAGE_ID, UPDATE_INTERVAL);
    }

    private void initSystemInfos(View view) {
        objectAdapter = new ArrayObjectAdapter(new SystemInfoPresenter());
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_account_id, AccountManager.getInstance().getAccountId()));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_said, AccountManager.getInstance().getSaId()));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_mac, NetworkUtil.getMacAddress()));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_firmware_version, SystemInfoManager.getInstance().getFirmwareVersion()));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_launcher_version, getLauncherVersion()));
        objectAdapter.add(ROW_NETWORK, new SystemInfoPresenter.SystemInfo(R.string.sysinfo_network, getNetworkStatus()));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_hbo_app, getAppVersion(ExternalApplicationManager.APP_HBO)));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_monomax_app, getAppVersion(ExternalApplicationManager.APP_MONOMAX)));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_mw_version, getMainSoftwareVersion(SystemInfoManager.getInstance())));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_server_type, ServerConfig.SERVER_TYPE));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_cwmp_lib, getCWMPLibraryVersion()));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_tis_app, getAppVersion(ExternalApplicationManager.APP_TIS)));
        objectAdapter.add(new SystemInfoPresenter.SystemInfo(R.string.sysinfo_4ch_app, getAppVersion(ExternalApplicationManager.APP_4CHANNEL)));

        int rssi = getWifiRssi();
        String wifiRssi = !isConnectedByWifi() || rssi == DISCONNECT_WIFI ? INVALID_RSSI : "" + rssi + "dBm";
        String wifiRssiStatus = getWifiRssiSatus(rssi);
        objectAdapter.add(ROW_WIFI_RSSI, new SystemInfoPresenter.SystemInfo(R.string.sysinfo_wifi_rssi, wifiRssi));
        objectAdapter.add(ROW_WIFI_STATUS, new SystemInfoPresenter.SystemInfo(R.string.sysinfo_wifi_status, wifiRssiStatus));

        gridView = view.findViewById(R.id.grid_view);
        PageBridgeAdapter itemBridgeAdapter = new PageBridgeAdapter(objectAdapter, VISIBLE_ROW);
        gridView.initVertical(VISIBLE_ROW);
        gridView.setNumColumns(1);
        gridView.setAdapter(itemBridgeAdapter);

        gridView.setFocusable(false);
        gridView.setFocusableInTouchMode(false);

        totalPage = itemBridgeAdapter.getAdapter().size();
    }

    private void initScrollBar(View view) {
        scrollbar = view.findViewById(R.id.scrollbar);
        scrollbar.setList(TOTAL_INFO, VISIBLE_ROW);
    }

    private void initButtons() {
        TextView firmwareButton = getView().findViewById(R.id.check_firmware_update_button);
        TextView apkButton = getView().findViewById(R.id.check_apk_update_button);

        firmwareButton.setOnKeyListener(this);
        apkButton.setOnKeyListener(this);
    }

    private void showUpdateDialogFragment(SettingSystemInfoUpdateDialogFragment.UpdateVersionType type) {
        SettingSystemInfoUpdateDialogFragment fragment = SettingSystemInfoUpdateDialogFragment.newInstance(type);
        fragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                isUpdateRequest = false;
            }
        });
        onFragmentChange.getTvOverlayManager().showDialogFragment(fragment);
    }

    private String getMainSoftwareVersion(SystemInfoManager jasSystemManager) {
        String version = null;
        try {
            version = jasSystemManager.getMainSoftwareVersion() + " / " + CWMPCPEManager.getInstance().getVersion();
        } catch (Exception e) {
            version = e.getMessage();
        }
        return version;
    }

    private String getLauncherVersion() {
        return BuildConfig.VERSION_NAME + " / " + BuildConfig.FLAVOR + " / " + BuildConfig.BUILD_TYPE;
    }

    private String getCWMPLibraryVersion() {
        String version = CWMPCPEManager.getInstance().getVersion();
        return version;
    }

    private String getAppVersion(String packageName) {
        try {
            ApplicationInfo appInfo = getContext().getPackageManager().getApplicationInfo(packageName, 0);
            PackageInfo packageInfo = getContext().getPackageManager().getPackageInfo(packageName, 0);

            String debugInfo;
            if (appInfo != null && ((appInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) > 0)) {
                debugInfo = getContext().getString(R.string.sysinfo_app_debug);
            } else {
                debugInfo = getContext().getString(R.string.sysinfo_app_release);
            }
            if (packageInfo != null) {
                return packageInfo.versionName + " / " + debugInfo;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return getContext().getString(R.string.sysinfo_app_not_installed);
    }

    private String getNetworkStatus() {
        boolean isConnected = NetworkUtil.hasNetworkConnection(getContext());
        int resourceId = isConnected ? R.string.sysinfo_ok : R.string.sysinfo_not_ok;
        return getContext().getString(resourceId);
    }

    private boolean isConnectedByWifi() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkCapabilities nextworkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());

        if (Log.INCLUDE) {
            Log.d(TAG, "isConnectedByWifi, nextworkCapabilities : " + nextworkCapabilities);
        }

        if (nextworkCapabilities != null && nextworkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
            return true;
        }

        return false;
    }

    private int getWifiRssi() {
        WifiManager wifiManager = (WifiManager) getContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        if (wifiInfo != null) {
            int rssi = wifiInfo.getRssi();
            if (Log.INCLUDE) {
                Log.d(TAG, "getWifiRssi, rssi : " + rssi);
            }

            return rssi;
        }

        return DISCONNECT_WIFI;
    }

    private String getWifiRssiSatus(int rssi) {
        if (rssi >= -70) {
            return getContext().getString(R.string.sysinfo_ok);
        }

        return getContext().getString(R.string.sysinfo_not_ok);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        updateHandler.removeMessages(UPDATE_MESSAGE_ID);
    }

    private boolean isUpdateRequest = false;

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        int page = gridView.getSelectedPosition() / VISIBLE_ROW;
        int nextPosition = 0;

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (!isUpdateRequest) {
                    isUpdateRequest = true;
                    SettingSystemInfoUpdateDialogFragment.UpdateVersionType type =
                            v.getId() == R.id.check_firmware_update_button ? SettingSystemInfoUpdateDialogFragment.UpdateVersionType.FIRMWARE : SettingSystemInfoUpdateDialogFragment.UpdateVersionType.APK;
                    showUpdateDialogFragment(type);
                }
                return true;
            case KeyEvent.KEYCODE_DPAD_UP:
                nextPosition = (page - 1) * VISIBLE_ROW;
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                nextPosition = (page + 1) * VISIBLE_ROW;
                break;
            default:
                return false;
        }

        if (nextPosition >= 0 && nextPosition < totalPage) {
            gridView.smoothScrollToPosition(nextPosition);
            gridView.setAlignment(nextPosition, v.getMeasuredHeight());
            scrollbar.moveFocus(nextPosition);
        }

        return true;
    }
}
