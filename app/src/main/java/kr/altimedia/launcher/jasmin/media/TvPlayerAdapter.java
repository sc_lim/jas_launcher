/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.media;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.tv.TvContentRating;
import android.media.tv.TvContract;
import android.media.tv.TvInputManager;
import android.media.tv.TvView;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.widget.RelativeLayout;

import com.altimedia.player.AltiPlayer;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.tv.JasTvView;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Created by mc.kim on 27,04,2020
 */
public class TvPlayerAdapter extends VideoPlayerAdapter {
    private static final String TAG = TvPlayerAdapter.class.getSimpleName();
    final Handler mHandler = new Handler();
    final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            getCallback().onCurrentPositionChanged(TvPlayerAdapter.this);
            mHandler.postDelayed(this, getProgressUpdatingInterval());
        }
    };
    final MediaPlayer.OnCompletionListener mOnCompletionListener =
            new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.COMPLETE);
                    getCallback().onPlayCompleted(TvPlayerAdapter.this);
                }
            };
    final MediaPlayer.OnVideoSizeChangedListener mOnVideoSizeChangedListener =
            new MediaPlayer.OnVideoSizeChangedListener() {
                @Override
                public void onVideoSizeChanged(MediaPlayer mediaPlayer, int width, int height) {
                    getCallback().onVideoSizeChanged(TvPlayerAdapter.this, width, height);
                }
            };
    final MediaPlayer.OnSeekCompleteListener mOnSeekCompleteListener =
            new MediaPlayer.OnSeekCompleteListener() {
                @Override
                public void onSeekComplete(MediaPlayer mp) {
                    TvPlayerAdapter.this.onSeekComplete();
                }
            };
    Context mContext;
    RelativeLayout mRelativeLayout;
    JasTvView mTvView;
    FingerPrinting mFingerPrinting;
    boolean mInitialized = false; // true when the MediaPlayer is prepared/initialized
    SeekBlockPairData mSeekBlockedPair = new SeekBlockPairData(false, -1L);
    String mInputId = null;
    Uri mMediaSourceUri = null;
    boolean mHasDisplay;
    long mBufferedProgress;
    final MediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener =
            new MediaPlayer.OnBufferingUpdateListener() {
                @Override
                public void onBufferingUpdate(MediaPlayer mp, int percent) {
                    mBufferedProgress = getDuration() * percent / 100;
                    getCallback().onBufferedPositionChanged(TvPlayerAdapter.this);
                }
            };
    AltiPlayer.EventListener mEventListener = new AltiPlayer.EventListener() {


        @Override
        public void onLoadingChanged(boolean isLoading) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onLoadingChanged : " + isLoading);
            }
        }


        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

            if (Log.INCLUDE) {
                Log.d(TAG, "onPlayerStateChanged : " + playWhenReady + ", playbackState : "
                        + AltiPlayer.EventListener.stateToString(playbackState) + "(" + playbackState + ")");
            }
            String stateString = AltiPlayer.EventListener.stateToString(playbackState);
            switch (stateString) {
                case "STATE_IDLE":
                    break;
                case "STATE_BUFFERING":
                    break;
                case "STATE_READY":
                    break;
                case "STATE_ENDED":
                    onComplete();
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onPlaybackSuppressionReasonChanged(int playbackSuppressionReason) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onPlaybackSuppressionReasonChanged : " + playbackSuppressionReason);
            }
        }

        @Override
        public void onIsPlayingChanged(boolean isPlaying) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onIsPlayingChanged : " + isPlaying);
            }
            if (mFingerPrinting != null) {
                if (isPlaying) {
                    mFingerPrinting.startPF();
                } else {
                    mFingerPrinting.stopPF();
                }
            }
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onRepeatModeChanged : " + repeatMode);
            }
        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onShuffleModeEnabledChanged : " + shuffleModeEnabled);
            }
        }

        @Override
        public void onPositionDiscontinuity(int reason) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onPositionDiscontinuity : " + reason);
            }
        }


        @Override
        public void onSeekProcessed() {

            TvPlayerAdapter.this.onSeekComplete();
            if (Log.INCLUDE) {
                Log.d(TAG, "onSeekProcessed");
            }
        }

        @Override
        public void onPlayerError(int type, String msg, Exception exception) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onPlayerError : type:" + type + ", msg:" + msg);
            }
            TvPlayerAdapter.this.onError(type, msg, exception);
        }
    };
    AltiPlayer.VideoListener mOnVideoListener = new AltiPlayer.VideoListener() {
        @Override
        public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

        }

        @Override
        public void onSurfaceSizeChanged(int width, int height) {
            if (mFingerPrinting != null) {
                mFingerPrinting.resizePF(width, height);
            }
        }

        @Override
        public void onRenderedFirstFrame() {

        }
    };
    boolean mBufferingStart;
    final MediaPlayer.OnInfoListener mOnInfoListener = new MediaPlayer.OnInfoListener() {
        @Override
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            boolean handled = false;
            switch (what) {
                case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                    mBufferingStart = true;
                    notifyBufferingStartEnd();
                    handled = true;
                    break;
                case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                    mBufferingStart = false;
                    notifyBufferingStartEnd();
                    handled = true;
                    break;
            }
            boolean thisHandled = TvPlayerAdapter.this.onInfo(what, extra);
            return handled || thisHandled;
        }
    };
    private long mTuneDelayTime = 0L;
    MediaPlayer.OnPreparedListener mOnPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mInitialized = true;
                    mp.start();
                    getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.READY);
                    notifyBufferingStartEnd();
                    if (mHasDisplay) {
                        getCallback().onPreparedStateChanged(TvPlayerAdapter.this);
                    }
                }
            }, mTuneDelayTime);
        }
    };
    private TimeShiftPlayCallback mTimeShiftPositionCallback = new TimeShiftPlayCallback();

    public TvPlayerAdapter(Context context, JasTvView tvView) {
        mContext = context;
        if (Log.INCLUDE) {
            Log.d(TAG, "called player generated");
        }
        mTvView = tvView;
        mTvView.setTimeShiftPositionCallback(mTimeShiftPositionCallback);
    }

    private void onPrepare(JasTvView altiPlayer) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mInitialized = true;
                altiPlayer.timeShiftResume();
                getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.READY);
                notifyBufferingStartEnd();
                if (mHasDisplay) {
                    getCallback().onPreparedStateChanged(TvPlayerAdapter.this);
                }
            }
        }, mTuneDelayTime);
    }

    private void onComplete() {
        getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.COMPLETE);
        getCallback().onPlayCompleted(TvPlayerAdapter.this);
    }



    void notifyBufferingStartEnd() {
        getCallback().onBufferingStateChanged(TvPlayerAdapter.this,
                mBufferingStart || !mInitialized);
    }

    public void onAttachedToHost() {
    }

    /**
     * Will reset the {@link MediaPlayer} and the glue such that a new file can be played. You are
     * not required to call this method before playing the first file. However you have to call it
     * before playing a second one.
     */
    public void reset() {
        changeToUnitialized();
//        mPlayer.reset();
    }

    public boolean isLocked() {
        return mTvView.isBlocked();
    }

    public void unLock() {
        mTvView.unblockContent((TvContentRating) mTvView.getTuningResult().getData());
        resume();
        getCallback().onReady(true);
        getCallback().onBlockedStateChanged(false);
    }

    void changeToUnitialized() {
        if (mInitialized) {
            mInitialized = false;
            notifyBufferingStartEnd();
            if (mHasDisplay) {
                getCallback().onPreparedStateChanged(TvPlayerAdapter.this);
            }
            getCallback().onReady(false);
        }
    }

    /**
     * Release internal MediaPlayer. Should not use the object after call release().
     */
    public void release() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call release");
        }
        mTimeShiftPositionCallback.clearRunnable();
//        changeToUnitialized();
        mSeekBlockedPair.reset();
        if (mTvView != null) {
            mTvView.clearPosition();
            mTvView.setTimeShiftCallback(null);
            mTvView.setTimeShiftPositionCallback(null);
        }
        mHasDisplay = false;
        if (mFingerPrinting != null) {
            mFingerPrinting.finalizePF();
            mFingerPrinting = null;
        }
        mTvView = null;
    }

    public void onDetachedFromHost() {
//        reset();
        if (Log.INCLUDE) {
            Log.d(TAG, "detachedFromHost");
        }
        release();


    }

    protected boolean onError(int type, String msg, Exception exception) {
        getCallback().onError(this, type, msg);
        return true;
    }

    /**
     * Called to indicate the completion of a seek operation.
     */
    protected void onSeekComplete() {
    }

    protected boolean onInfo(int what, int extra) {
        return false;
    }

    @Override
    public void setProgressUpdatingEnabled(final boolean enabled) {
        mHandler.removeCallbacks(mRunnable);
        if (!enabled) {
            return;
        }
        mHandler.postDelayed(mRunnable, getProgressUpdatingInterval());
    }

    /**
     * Return updating interval of progress UI in milliseconds. Subclass may override.
     *
     * @return Update interval of progress UI in milliseconds.
     */
    public int getProgressUpdatingInterval() {
        return 16;
    }

    @Override
    public boolean isPlaying() {
        return mInitialized && isPlaying;
    }

    @Override
    public long getDuration() {
        return mProgramEndTimeMs;
    }

    @Override
    public long getCurrentPosition() {
        return mInitialized ? mTimeShiftPositionCallback.getCurrentPosition() : -1;
    }

    public long getProgramStartTimeMs() {
        return mProgramStartTimeMs;
    }

    public long getProgramEndTimeMs() {
        return mProgramEndTimeMs;
    }

    public long getTimeShiftEnableTime() {
        return mTimeShiftEnableTime;
    }

    private boolean isPlaying = false;

    @Override
    public void play() {

        if (Log.INCLUDE) {
            Log.d(TAG, "call play | mInitialized : " + mInitialized);
        }
        if (!mInitialized) {
            return;
        }
        mTvView.timeShiftPlay(currentChannel, currentProgram, mInputId, mMediaSourceUri);

        getCallback().onCurrentPositionChanged(TvPlayerAdapter.this);
        getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.PLAY);
    }

    @Override
    public void pause() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call Pause");
        }
        if (isPlaying()) {
            isPlaying = false;
            mTvView.timeShiftPause();
            getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.PAUSE);
        }
    }

    @Override
    public void stop() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call stop");
        }
        if (isPlaying()) {
            isPlaying = false;
            mMediaSourceUri = null;
            mTvView.timeShiftPause();
            getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.STOP);
        }
    }

    protected void onSeekPrevious() {

    }

    protected void onSeekNext() {

    }

    private final long LIMIT_BUFFER_TIM_MS = 10 * TimeUtil.SEC;

    @Override
    public void seekTo(long newPosition) {
        if (!mInitialized) {
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "seekTo : " + newPosition);
        }
        long timeMs = JasmineEpgApplication.SystemClock().currentTimeMillis();
        long limitStartTimeMs = timeMs - mTimeShiftEnableTime + LIMIT_BUFFER_TIM_MS;
        if (newPosition >= timeMs) {
            onComplete();
        } else if (newPosition < mProgramStartTimeMs) {
            onSeekPrevious();
        } else if (mProgramEndTimeMs < newPosition) {
            onSeekNext();
        } else if (newPosition < limitStartTimeMs) {
            mSeekBlockedPair.setIsSeeking(true);
            mSeekBlockedPair.setSeekTimeMs(limitStartTimeMs);
            mTvView.timeShiftSeekTo(limitStartTimeMs);
        } else {
            mSeekBlockedPair.setIsSeeking(true);
            mSeekBlockedPair.setSeekTimeMs(newPosition);
            mTvView.timeShiftSeekTo(newPosition);
        }
    }


    @Override
    public long getBufferedPosition() {
        return mBufferedProgress;
    }

    private long mTimeShiftEnableTime = -1;
    private long mProgramStartTimeMs = -1;
    private long mProgramEndTimeMs = -1;
    private long mSeekTimeMs = -1;
    private Channel currentChannel;
    private Program currentProgram;

    /**
     * Sets the media source of the player witha given URI.
     *
     * @return Returns <code>true</code> if uri represents a new media; <code>false</code>
     * otherwise.
     * @see MediaPlayer#setDataSource(String)
     */
    public boolean setDataSource(Channel channel, Program program, long programStartTime, long programEndTime, long seekTimeMs, long timeShiftEnableTime) {

        String inputId = channel.getInputId();
        Uri recordedProgramUri = TvContract.buildProgramUri(program.getId());
        if (recordedProgramUri.equals(mMediaSourceUri)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "isSame request so return : " + recordedProgramUri.toString());
            }
            return false;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "setDataSource : " + inputId + ", recordedProgramUri : " + recordedProgramUri.toString());
        }
        currentChannel = channel;
        currentProgram = program;
        mTimeShiftEnableTime = timeShiftEnableTime;
        mSeekTimeMs = seekTimeMs;
        mProgramStartTimeMs = programStartTime;
        mProgramEndTimeMs = programEndTime;
        mInputId = inputId;
        mMediaSourceUri = recordedProgramUri;
        prepareMediaForPlaying();
        return true;
    }

    public void setProgramInfoChanged(long programStartTime, long programEndTime) {
        mProgramStartTimeMs = programStartTime;
        mProgramEndTimeMs = programEndTime;
        getCallback().onProgressInfoChanged(this, programStartTime, programEndTime);
        getCallback().onDurationChanged(this);
    }

    public void resetInfo() {
        mMediaSourceUri = null;
    }

    private JasTvView.TimeShiftCallback mTimeShiftCallback = new JasTvView.TimeShiftCallback() {

        @Override
        public void onChannelRetuned(String inputId, Uri channelUri) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onChannelRetuned | inputId : " + inputId + ", channelUri : " + channelUri);
            }
            if (mSeekBlockedPair.isIsSeeking()) {
                return;
            }
//            int timeShiftError = 99;
//            getCallback().onError(TvPlayerAdapter.this, timeShiftError, "can not play Time Shift, so return back live");
        }

        @Override
        public void onTimeShiftStatusChanged(String inputId, int status) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onTimeShiftStatusChanged | inputId : " + inputId + ", status : " + status);
            }
            switch (status) {
                case TvInputManager.TIME_SHIFT_STATUS_UNSUPPORTED:
                    isPlaying = false;
                    getCallback().onError(TvPlayerAdapter.this, AltiPlayer.PLAYER_ERROR_TYPE_SOURCE, "TIME_SHIFT_STATUS_UNSUPPORTED");
                    break;
                case TvInputManager.TIME_SHIFT_STATUS_UNAVAILABLE:
                    isPlaying = false;
                    getCallback().onError(TvPlayerAdapter.this, AltiPlayer.PLAYER_ERROR_TYPE_USER_DEFINED_1, "TIME_SHIFT_STATUS_UNAVAILABLE");
                    break;
                case TvInputManager.TIME_SHIFT_STATUS_AVAILABLE:
//                    mInitialized = true;
//                    isPlaying = true;
//                    getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.READY);
//                    getCallback().onDurationChanged(TvPlayerAdapter.this);
//                    notifyBufferingStartEnd();
                    break;

            }
        }
    };

    public boolean initialized() {
        return mInitialized;
    }

    private void onReady() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call onReady : " + mInitialized);
        }
        if (mInitialized) {
            return;
        }
        mInitialized = true;
        isPlaying = true;
        getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.READY);
        getCallback().onDurationChanged(TvPlayerAdapter.this);
        notifyBufferingStartEnd();

        if (Log.INCLUDE) {
            Log.d(TAG, "call onReady | isLocked : " + isLocked());
        }
        if (isLocked()) {
            isPlaying = false;
            mTvView.timeShiftPause();
//            pause();
        } else {
            isPlaying = true;
            mTvView.timeShiftResume();
//            resume();
        }
        getCallback().onReady(true);
        getCallback().onBlockedStateChanged(isLocked());
    }


    private void prepareMediaForPlaying() {
        reset();
        if (Log.INCLUDE) {
            Log.d(TAG, "mMediaSourceUri : " + mMediaSourceUri.toString());
        }
        if (mMediaSourceUri == null) {
            return;
        }
        long BUFFER_TIME = 1 * TimeUtil.SEC;
        long timeMs = JasmineEpgApplication.SystemClock().currentTimeMillis();
        long limitStartTimeMs = timeMs - mTimeShiftEnableTime;
        long checkTime = mSeekTimeMs != -1 ? mSeekTimeMs + BUFFER_TIME : (limitStartTimeMs > mProgramStartTimeMs) ?
                limitStartTimeMs + LIMIT_BUFFER_TIM_MS : mProgramStartTimeMs + BUFFER_TIME;
        mSeekBlockedPair.setIsSeeking(true);
        mSeekBlockedPair.setSeekTimeMs(checkTime);
        mTvView.setTimeShiftPositionCallback(mTimeShiftPositionCallback);
        mTvView.setTimeShiftCallback(mTimeShiftCallback);

        mTvView.timeShiftPlay(currentChannel, currentProgram, mInputId, mMediaSourceUri, mSeekTimeMs);
        notifyBufferingStartEnd();
        getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.PREPARE);
    }

    @Override
    public void resume() {
        if (Log.INCLUDE) {
            Log.d(TAG, "call resume");
        }
        isPlaying = true;
        mTvView.timeShiftResume();
        getCallback().onCurrentPositionChanged(TvPlayerAdapter.this);
        getCallback().onPlayStateChanged(TvPlayerAdapter.this, VideoState.PLAY);
    }

    @Override
    public boolean isPrepared() {
        return mInitialized;
    }


    private class TimeShiftPlayCallback extends TvView.TimeShiftPositionCallback {
        private long mCurrentPosition = 0;
        private long mStartPosition = 0;
        private boolean isReady = false;
        private Handler handler = new Handler(Looper.getMainLooper());
        private Runnable mReadyRunnable = new Runnable() {
            @Override
            public void run() {
                onReady();
            }
        };

        public TimeShiftPlayCallback() {
        }

        public void clearRunnable() {
            handler.removeCallbacks(mReadyRunnable);
        }

        @Override
        public void onTimeShiftStartPositionChanged(String inputId, long timeMs) {
            super.onTimeShiftStartPositionChanged(inputId, timeMs);
            if (Log.INCLUDE) {
                Log.d(TAG, "onTimeShiftStartPositionChanged : " + timeMs);
            }
            mStartPosition = timeMs;
        }

        @Override
        public void onTimeShiftCurrentPositionChanged(String inputId, long timeMs) {
            super.onTimeShiftCurrentPositionChanged(inputId, timeMs);
            if (Log.INCLUDE) {
                Log.d(TAG, "onTimeShiftCurrentPositionChanged | " +
                        "mCurrentPosition : " + timeMs +
                        ", mProgramStartTimeMs : " + mProgramStartTimeMs);
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "onTimeShiftCurrentPositionChanged | " +
                        "mSeekBlockedPair.mIsSeeking : " + mSeekBlockedPair.mIsSeeking +
                        ", mProgramStartTimeMs : " + mSeekBlockedPair.getSeekTimeMs());
            }

            getCallback().onBlockedStateChanged(isLocked());
            if (mSeekBlockedPair.mIsSeeking) {
                mCurrentPosition = mSeekBlockedPair.mSeekTimeMs;
                if (mSeekBlockedPair.mSeekTimeMs <= timeMs + (5 * TimeUtil.SEC) &&
                        mSeekBlockedPair.mSeekTimeMs >= timeMs - (5 * TimeUtil.SEC)) {
                    mSeekBlockedPair.reset();
                    handler.postDelayed(mReadyRunnable, 100);
                }
            } else {
                mCurrentPosition = timeMs;
                mTvView.putCurrentPosition(mCurrentPosition);
            }
        }

        public long getCurrentPosition() {
            return mCurrentPosition;
        }

        public long getStartPosition() {
            return mStartPosition;
        }
    }

    @Override
    public boolean isSeeking() {
        return mSeekBlockedPair.mIsSeeking;
    }

    private static class SeekBlockPairData {
        private boolean mIsSeeking;
        private long mSeekTimeMs;

        public SeekBlockPairData(boolean mIsSeeking, long mSeekTimeMs) {
            this.mIsSeeking = mIsSeeking;
            this.mSeekTimeMs = mSeekTimeMs;
        }

        public boolean isIsSeeking() {
            return mIsSeeking;
        }

        public void setIsSeeking(boolean mIsSeeking) {
            this.mIsSeeking = mIsSeeking;
        }

        public void reset() {
            this.mIsSeeking = false;
            this.mSeekTimeMs = -1L;
        }

        public long getSeekTimeMs() {
            return mSeekTimeMs;
        }

        public void setSeekTimeMs(long mSeekTimeMs) {
            this.mSeekTimeMs = mSeekTimeMs;
        }
    }

    @Override
    public boolean isKeyBlock() {
        return !mInitialized || isLocked();
    }

    @Override
    public boolean isReady() {
        return mInitialized;
    }

    @Override
    public boolean whenPlayWithControlBar() {
        return true;
    }
}