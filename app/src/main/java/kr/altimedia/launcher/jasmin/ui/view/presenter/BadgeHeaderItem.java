/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.presenter;

import android.graphics.drawable.Drawable;

import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;


public class BadgeHeaderItem extends HeaderItem {
    private Drawable badgeDrawable;

    public BadgeHeaderItem(long id, String name, Drawable drawable) {
        super(id, name);
        this.badgeDrawable = drawable;
    }

    public Drawable getBadgeDrawable() {
        return badgeDrawable;
    }

    public BadgeHeaderItem(String name) {
        super(name);
    }

}
