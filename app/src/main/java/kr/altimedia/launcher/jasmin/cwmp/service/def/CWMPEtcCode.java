/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service.def;

public class CWMPEtcCode {
    public static final String ERROR_CODE_001 = "001";
    public static final String ERROR_CODE_151 = "151"; // WAN포트 물리적 링크 단절 시 발생
    public static final String ERROR_CODE_152 = "152"; // 부팅 중 IP 미 할당
    public static final String ERROR_CODE_154 = "154"; // DNS 2차 서버와도 통신 실패
    public static final String ERROR_CODE_207 = "207"; // 구매 PIN 인증 실패를 알림
    public static final String ERROR_CODE_301 = "301"; // 검색(Search)서버 연결실패 및 오류
    public static final String ERROR_CODE_302 = "302"; // 추천(REC)서버 연결실패 및 오류
    public static final String ERROR_CODE_303 = "303"; // 광고(ADS)서버 연결실패 및 오류
    public static final String ERROR_CODE_341 = "341"; // MBS 서버로부터 응답이 없을 경우
    public static final String ERROR_CODE_601 = "601"; // (Player)OutOfMemoryError 발생
    public static final String ERROR_CODE_602 = "602"; // (Player)원격 구성 요소에서 오류 발생
    public static final String ERROR_CODE_603 = "603"; // (Player)렌더러에서 오류 발생
    public static final String ERROR_CODE_604 = "604"; // (Player)예기치 않은 RuntimeException 발생
    public static final String ERROR_CODE_807 = "807"; // 채널리스트 빌드 후 Promo채널이 없는 경우
    public static final String ERROR_CODE_827 = "827"; // VOD 절체 시도가 있었 을 경우 (노드간)

    public static String getErrorDescription(String errorCode){
        if(errorCode != null){
            if(ERROR_CODE_001.equals(errorCode)){
                return "TYPE_UNEXPECTED";
            } else if(ERROR_CODE_151.equals(errorCode)){
                return "NET-001_NW_LINK_FAIL";
            } else if(ERROR_CODE_152.equals(errorCode)){
                return "NET-002_IP_GET_FAIL";
            } else if(ERROR_CODE_154.equals(errorCode)){
                return "NET-004_DNS_FAIL";
            } else if(ERROR_CODE_207.equals(errorCode)){
                return "PIN_AUTH_FAIL";
            } else if(ERROR_CODE_301.equals(errorCode)){
                return "RCS_NODE_CONN_FAIL_SER";
            } else if(ERROR_CODE_302.equals(errorCode)){
                return "RCS_NODE_CONN_FAIL_REC";
            } else if(ERROR_CODE_303.equals(errorCode)){
                return "RCS_NODE_CONN_FAIL_ADS";
            } else if(ERROR_CODE_341.equals(errorCode)){
                return "MBSE-000_MBS_FAIL";
            } else if(ERROR_CODE_601.equals(errorCode)){
                return "TYPE_OUT_OF_MEMORY";
            } else if(ERROR_CODE_602.equals(errorCode)){
                return "TYPE_REMOTE";
            } else if(ERROR_CODE_603.equals(errorCode)){
                return "TYPE_RENDERER";
            } else if(ERROR_CODE_604.equals(errorCode)){
                return "TYPE_SOURCE";
            } else if(ERROR_CODE_807.equals(errorCode)){
                return "NAV007_CHBUILD_NO_PROMO CHANNEL";
            } else if(ERROR_CODE_827.equals(errorCode)){
                return "VOD-chgE-001_VOD_CHANGE_FAIL";
            }
        }
        return "";
    }
}
