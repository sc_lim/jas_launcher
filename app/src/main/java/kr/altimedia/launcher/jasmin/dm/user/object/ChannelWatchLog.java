/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChannelWatchLog implements Parcelable {
    public static final Creator<ChannelWatchLog> CREATOR = new Creator<ChannelWatchLog>() {
        @Override
        public ChannelWatchLog createFromParcel(Parcel in) {
            return new ChannelWatchLog(in);
        }

        @Override
        public ChannelWatchLog[] newArray(int size) {
            return new ChannelWatchLog[size];
        }
    };
    @Expose
    @SerializedName("channelId")
    private String channelId;
    @Expose
    @SerializedName("entryTime")
    private String entryTime;

    public ChannelWatchLog(String channelId, String entryTime) {
        this.channelId = channelId;
        this.entryTime = entryTime;
    }

    protected ChannelWatchLog(Parcel in) {
        channelId = in.readString();
        entryTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(channelId);
        dest.writeString(entryTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "ChannelWatchLog{" +
                "channelId='" + channelId + '\'' +
                ", entryTime='" + entryTime + '\'' +
                '}';
    }

    public String getChannelId() {
        return channelId;
    }

    public String getEntryTime() {
        return entryTime;
    }
}