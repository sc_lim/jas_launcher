/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.ParentalItem;

public class SettingPreviewButtonPresenter extends Presenter {

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_setting_preview_button, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((ParentalItem) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private TextView button;
        private TextView count;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            button = view.findViewById(R.id.button);
            count = view.findViewById(R.id.count);
        }

        public void setData(ParentalItem item) {
            boolean hasValue = item.hasValue();
            if (hasValue) {
                String title = view.getContext().getString(item.getTitle()) + " : ";
                button.setText(title);
                count.setText(item.getCount());
                count.setVisibility(View.VISIBLE);
            } else {
                String value = view.getContext().getString(item.getInvalidText());
                button.setText(value);
                count.setVisibility(View.GONE);
            }
        }
    }
}
