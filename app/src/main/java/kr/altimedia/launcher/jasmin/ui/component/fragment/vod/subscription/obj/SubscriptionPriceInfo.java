/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class SubscriptionPriceInfo implements Parcelable {
    public static final Creator<SubscriptionPriceInfo> CREATOR = new Creator<SubscriptionPriceInfo>() {
        @Override
        public SubscriptionPriceInfo createFromParcel(Parcel in) {
            return new SubscriptionPriceInfo(in);
        }

        @Override
        public SubscriptionPriceInfo[] newArray(int size) {
            return new SubscriptionPriceInfo[size];
        }
    };
    private String offerId;
    private String price;
    private String currency;
    private int duration;

    public SubscriptionPriceInfo(String offerId, String price, String currency, int duration) {
        this.offerId = offerId;
        this.price = price;
        this.currency = currency;
        this.duration = duration;
    }

    protected SubscriptionPriceInfo(Parcel in) {
        offerId = in.readString();
        price = in.readString();
        currency = in.readString();
        duration = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(offerId);
        dest.writeString(price);
        dest.writeString(currency);
        dest.writeInt(duration);
    }

    @NonNull
    @Override
    public String toString() {
        return "SubscriptionPriceInfo{" +
                "offerId='" + offerId + '\'' +
                ", price='" + price + '\'' +
                ", currency=" + currency +
                ", duration=" + duration +
                '}';
    }

    public String getOfferId() {
        return offerId;
    }

    public String getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public int getDuration() {
        return duration;
    }
}
