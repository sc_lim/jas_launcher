/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data;

import android.content.res.Resources;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;

public class UserProfileBuilder {

    public static final int TYPE_USER_PROFILE = 0;
    public static final int TYPE_ADD_OPTION = 1;
    public static final int TYPE_MANAGE_OPTION = 2;

    public static UserProfile createUserProfile(ProfileInfo profileInfo) {
        return new UserProfile(profileInfo);
    }

    public static UserProfile createAddOption(Resources rsc) {
        return new UserProfile(TYPE_ADD_OPTION, rsc.getString(R.string.add));
    }

    public static UserProfile createManageOption(Resources rsc) {
        return new UserProfile(TYPE_MANAGE_OPTION, rsc.getString(R.string.manage));
    }

    public static boolean checkProfileOptionIncluded(ArrayList<UserProfile> list, int type) {
        for (UserProfile userProfile : list) {
            if (userProfile.getType() == type) {
                return true;
            }
        }
        return false;
    }

    public static String getUniqueProfileIcon(ArrayList<UserProfile> list) {
        for (String fileName : ProfileInfo.ICON_FILE_NAMEs) {
            boolean existed = false;
            for (UserProfile userProfile : list) {
                ProfileInfo profileInfo = userProfile.getProfileInfo();
                if (profileInfo != null) {
                    if (fileName.equalsIgnoreCase(profileInfo.getProfilePic())) {
                        existed = true;
                        break;
                    }
                }
            }
            if (!existed) {
                return fileName;
            }
        }
        return ProfileInfo.ICON_FILE_NAMEs[0];
    }

    public static String getUniqueProfileInfoIcon(ArrayList<ProfileInfo> list) {
        for (String fileName : ProfileInfo.ICON_FILE_NAMEs) {
            boolean existed = false;
            for (ProfileInfo profileInfo : list) {
                if (profileInfo != null) {
                    if (fileName.equalsIgnoreCase(profileInfo.getProfilePic())) {
                        existed = true;
                        break;
                    }
                }
            }
            if (!existed) {
                return fileName;
            }
        }
        return ProfileInfo.ICON_FILE_NAMEs[0];
    }
}
