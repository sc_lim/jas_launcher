/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.playback;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.player.AltiMediaSource;
import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PlacementDecision;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PreviewInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.media.MediaPlayerAdapter;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.interactor.VideoPlaybackInteractor;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.playbck.EndWatchingVodDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.playbck.presenter.VodEndActionPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelLayout;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackSupportFragment;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.TrailerPlaybackSupportFragmentGlueHost;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.TrailerPlaybackTransportControlGlue;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackGlue;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.TrailerDataProvider;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.TrailerSelectUi;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Created by mc.kim on 29,04,2020
 */
public class TrailerVideoFragment extends VideoPlaybackSupportFragment
        implements EndWatchingVodDialogFragment.OnVodWatchEndListener {
    private final String TAG = TrailerVideoFragment.class.getSimpleName();


    static final int SURFACE_NOT_CREATED = 0;
    static final int SURFACE_CREATED = 1;

    SurfaceView mVideoSurface;
    SurfaceHolder.Callback mMediaPlaybackCallback;

    int mState = SURFACE_NOT_CREATED;
    private final int VISIBLE_SIZE = 4;
    private MediaPlayerAdapter mPlayerAdapter = null;
    private TrailerPlaybackTransportControlGlue<MediaPlayerAdapter> mTransportControlGlue;
    private TrailerPlaybackSupportFragmentGlueHost glueHost;

    public static TrailerVideoFragment newInstance(Bundle bundle) {
        TrailerVideoFragment fragment = new TrailerVideoFragment();
        fragment.setArguments(bundle);
        fragment.setIsEnableUseVideoSetting(false);
        return fragment;
    }

    private SidePanelLayout.SidePanelActionListener mSidePanelActionListener = null;
    private VideoPlaybackInteractor mVideoPlaybackInteractor = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof SidePanelLayout.SidePanelActionListener) {
            mSidePanelActionListener = (SidePanelLayout.SidePanelActionListener) context;
        }
        if (context instanceof VideoPlaybackInteractor) {
            mVideoPlaybackInteractor = (VideoPlaybackInteractor) context;
        }
    }

    private int streamSize = 0;
    List<StreamInfo> streamInfoList;
    private final int WHAT_FINISH = 1;
    private final long CHECK_PAUSE_TIME = 30 * TimeUtil.MIN;
    private Handler mPauseHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == WHAT_FINISH) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call finish");
                }
                onBackPressed();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Parcelable movie =
                getArguments().getParcelable(VideoPlaybackActivity.KEY_MOVIE);
        final int trailerIndex = getArguments().getInt(VideoPlaybackActivity.KEY_MOVIE_INDEX, 0);
        VideoPlaybackActivity.PlayerType type = (VideoPlaybackActivity.PlayerType) getArguments().getSerializable(VideoPlaybackActivity.KEY_TYPE);
        final String title;
        final String synopsis;
        final String contentId;
        final String movieId;
        final String categoryId;
        if (movie instanceof Content) {
            Content content = (Content) movie;
            contentId = content.getContentGroupId();
            movieId = content.getMovieAssetId();
            title = content.getTitle();
            synopsis = content.getSynopsis();
            streamInfoList = content.getTrailerStreamInfo();
            categoryId = content.getCategoryId();
        } else {
            SeriesContent content = (SeriesContent) movie;
            contentId = content.getContentGroupId();
            movieId = content.getMovieAssetId();
            title = content.getSeriesName();
            synopsis = "";
            streamInfoList = content.getTrailer();
            categoryId = content.getCategoryId();
        }
        streamSize = streamInfoList.size();
        TrailerDataProvider dataProvider = new TrailerDataProvider(VISIBLE_SIZE) {

            @Override
            public void onSelectedThumbnail(StreamInfo info) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call onSelectedThumbnail : " + info.toString());
                }

                StreamInfo currentStream = streamInfoList.get(trailerIndex);
                if (currentStream.equals(info)) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "is Same Trailer Selected so return");
                    }
                    mTransportControlGlue.forceControllerHide();
                    return;
                }
                mVideoPlaybackInteractor.requestTuneTrailer(movie, info);
            }

            @Override
            public boolean isCurrentPlayingVideo(StreamInfo info) {
                StreamInfo currentStream = streamInfoList.get(trailerIndex);
                return currentStream.equals(info);
            }

            @Override
            public List<StreamInfo> getTrailerDataList() {
                Log.d(TAG, "call getTrailerDataList");
                return streamInfoList;
            }

            @Override
            public void getThumbnail(StreamInfo info, ResultCallback callback) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call getThumbnail");
                }
                loadThumbnailPoster(info, callback);
            }

            @Override
            public String getThumbnailTitle(StreamInfo info) {
                return info.getTitle();
            }

            @Override
            public void reset() {
                super.reset();
            }
        };

        glueHost =
                new TrailerPlaybackSupportFragmentGlueHost(TrailerVideoFragment.this);

        mPlayerAdapter = new MediaPlayerAdapter(getContext());
        mTransportControlGlue = new TrailerPlaybackTransportControlGlue<>(getContext(), mPlayerAdapter);
        mTransportControlGlue.setSeekEnabled(true);
        initVideoClient(dataProvider);
        mTransportControlGlue.addPlayerCallback(new VideoPlaybackGlue.PlayerCallback() {

            @Override
            public void onReady(boolean ready) {
                super.onReady(ready);
                if (ready) {
                    getProgressBarManager().hide();
                } else {
                    getProgressBarManager().show();
                }
            }

            @Override
            public void onPreparedStateChanged(VideoPlaybackGlue glue) {
                super.onPreparedStateChanged(glue);
            }

            @Override
            public void onPlayStateChanged(VideoPlaybackGlue glue) {
                super.onPlayStateChanged(glue);
                mPauseHandler.removeMessages(WHAT_FINISH);
                if (!glue.isPlaying()) {
                    mPauseHandler.sendEmptyMessageDelayed(WHAT_FINISH,
                            CHECK_PAUSE_TIME);
                }
            }

            @Override
            public void onPlayCompleted(VideoPlaybackGlue glue) {
                super.onPlayCompleted(glue);
                onBackPressed();
            }

            @Override
            public void onUpdateProgress(VideoPlaybackGlue glue, long time, long duration) {
                super.onUpdateProgress(glue, time, duration);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onUpdateProgress : " + time + ", duration : " + duration);
                }
                if (channelPreviewProgress != null && channelPreviewTime != null) {
                    long remainTime = duration - time;
                    updateTime(remainTime, channelPreviewProgress, channelPreviewTime, channelPreviewTimeUnit);
                }

            }
        });
        mTransportControlGlue.setHost(glueHost);
        mTransportControlGlue.setTitle(title);
        mTransportControlGlue.setSubtitle(synopsis);

        PlaybackUtil.getPlayResource(getContext(), contentId, movieId, streamInfoList.get(trailerIndex), categoryId, new PlaybackUtil.PlayResourceCallback() {
            @Override
            public void onResult(AltiMediaSource[] mediaSource, List<PlacementDecision> placementDecisions) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "url : " + mediaSource.toString());
                }
                if (type == VideoPlaybackActivity.PlayerType.Preview) {
                    PreviewInfo previewInfo = streamInfoList.get(trailerIndex).getPreviewInfo();
                    mPlayerAdapter.setDataSourceLimitMode(mediaSource, previewInfo.getFreePreviewStartPosition(),
                            previewInfo.getFreePreviewDuration());
                } else {
                    mPlayerAdapter.setDataSource(mediaSource);
                }
            }

            @Override
            public void onFail(int reason) {
                String title = getString(R.string.error_title_mbs);
                String errorMessage = ErrorMessageManager.getInstance().getErrorMessage(title);
                ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, title, errorMessage);
                dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        getActivity().finish();
                    }
                });
            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, errorMessage);
                dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        getActivity().finish();
                    }
                });
            }
        });
    }

    private void updateTime(long timesLeft, View bg, TextView textView, TextView unitView) {
        int sec = (int) (timesLeft / 1000L);
        int min = (sec - 1) / 60;//correct display number (61~120:1min, 121~180:2min, 241~300:5min)

        int resid;
        if (sec < 1) {//0s : preview_05
            resid = R.drawable.preview_05;
        } else if (min < 1) {//01s~60s : preview_04
            resid = R.drawable.preview_04;
        } else if (min < 2) {//1m~2m : preview_03
            resid = R.drawable.preview_03;
        } else if (min < 3) {//2m~3m : preview_02
            resid = R.drawable.preview_02;
        } else if (min < 4) {//3~4m : preview_01
            resid = R.drawable.preview_01;
        } else {//4m~ : none
            resid = 0;
        }
        bg.setBackgroundResource(resid);

        if (min > 0) {
            textView.setText("" + (min + 1));
            unitView.setText(R.string.channel_preview_time_min);
        } else {
            textView.setText("" + sec);
            unitView.setText(R.string.channel_preview_time_sec);
        }
    }

    @Override
    public boolean canUsingChangeMode() {
        return streamSize > 1;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mTransportControlGlue != null) {
            mTransportControlGlue.pause();
        }
    }


    private void initVideoClient(TrailerDataProvider mPlaybackSeekDataProvider) {
        setTrailerUiClient(new TrailerSelectUi.Client() {

            @Override
            public boolean isSelectionEnabled() {
                return true;
            }

            @Override
            public void onSelectStarted() {
                super.onSelectStarted();
            }

            @Override
            public TrailerDataProvider getTrailerDataProvider() {
                return mPlaybackSeekDataProvider;
            }

            @Override
            public void onSelectFinished(boolean cancelled) {
                super.onSelectFinished(cancelled);
            }

        });
        mTransportControlGlue.setVideoSelectProvider(mPlaybackSeekDataProvider);
    }

    private View channelPreviewProgress;
    private TextView channelPreviewTime;
    private TextView channelPreviewTimeUnit;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        VideoPlaybackActivity.PlayerType type = (VideoPlaybackActivity.PlayerType) getArguments().getSerializable(VideoPlaybackActivity.KEY_TYPE);
        ViewGroup root = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        mVideoSurface = (SurfaceView) LayoutInflater.from(getContext()).inflate(
                R.layout.layer_vo_surface, root, false);
        root.addView(mVideoSurface, 0);
        if (type == VideoPlaybackActivity.PlayerType.Preview) {
            View previewIndicator = LayoutInflater.from(getContext()).inflate(
                    R.layout.view_preview_indicator, root, false);
            root.addView(previewIndicator, 1);
            channelPreviewProgress = previewIndicator.findViewById(R.id.channel_preview_progress);
            channelPreviewTime = previewIndicator.findViewById(R.id.channel_preview_time);
            channelPreviewTimeUnit = previewIndicator.findViewById(R.id.channel_preview_time_unit);
            final int trailerIndex = getArguments().getInt(VideoPlaybackActivity.KEY_MOVIE_INDEX, 0);
            PreviewInfo previewInfo = streamInfoList.get(trailerIndex).getPreviewInfo();
            updateTime(previewInfo.getFreePreviewDuration(), channelPreviewProgress, channelPreviewTime, channelPreviewTimeUnit);
        }


        mVideoSurface.getHolder().setFormat(PixelFormat.RGB_888);
        mVideoSurface.getHolder().addCallback(mPlayerAdapter.getVideoPlayerSurfaceHolderCallback());

        mVideoSurface.getHolder().addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceCreated(holder);
                }
                mState = SURFACE_CREATED;
                mPlayerAdapter.setDisplay(holder);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceChanged(holder, format, width, height);
                }
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceDestroyed(holder);
                }
                mState = SURFACE_NOT_CREATED;
                mPlayerAdapter.setDisplay(null);
            }
        });

        setBackgroundType(VideoPlaybackSupportFragment.BG_NONE);
        return root;
    }

    @Override
    protected boolean isPlaying() {
        return mPlayerAdapter.isPlaying();
    }

    /**
     * Adds {@link SurfaceHolder.Callback} to {@link android.view.SurfaceView}.
     */

    @Override
    public void onVideoSizeChanged(int width, int height) {
        int screenWidth = getView().getWidth();
        int screenHeight = getView().getHeight();

        ViewGroup.LayoutParams p = mVideoSurface.getLayoutParams();
        if (screenWidth * height > width * screenHeight) {
            // fit in screen height
            p.height = screenHeight;
            p.width = screenHeight * width / height;
        } else {
            // fit in screen width
            p.width = screenWidth;
            p.height = screenWidth * height / width;
        }
        mVideoSurface.setLayoutParams(p);
    }

    /**
     * Returns the surface view.
     */
    public SurfaceView getSurfaceView() {
        return mVideoSurface;
    }


    @Override
    public void onDestroyView() {
        if (mTransportControlGlue != null) {
            mTransportControlGlue.clearPlayerCallback();
        }
        mPauseHandler.removeMessages(WHAT_FINISH);
        mPlayerAdapter.release();
        mVideoSurface = null;
        mState = SURFACE_NOT_CREATED;
        super.onDestroyView();
    }


    private void dismissEndWatchingVodDialog() {
        EndWatchingVodDialogFragment dialogFragment =
                (EndWatchingVodDialogFragment) getFragmentManager()
                        .findFragmentByTag(EndWatchingVodDialogFragment.CLASS_NAME);
        dialogFragment.dismiss();
    }

    public void onBackPressed() {
        getActivity().finish();
    }

    @Override
    public void requestCancel(DialogFragment dialogFragment, VodEndActionPresenter.FinishType type) {
        dismissEndWatchingVodDialog();
    }

    @Override
    public void requestFinish(DialogFragment dialogFragment, Content content, VodEndActionPresenter.FinishType type) {

        switch (type) {
            case Exit:
                dialogFragment.dismiss();
                getActivity().finish();
                break;
            case NextEpisodeDetail:
                dialogFragment.dismiss();
                break;
        }
    }

    @Override
    public void onError(int errorCode, CharSequence errorMessage) {
        super.onError(errorCode, errorMessage);

        String title = ErrorMessageManager.getInstance().getPlayerErrorCode(getContext(), errorCode);
        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, title,
                ErrorMessageManager.getInstance().getErrorMessage(title));
        dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getActivity().finish();
            }
        });
    }

    @Override
    protected boolean isKeyBlock() {
        return !mPlayerAdapter.isReady();
    }

    private void loadThumbnailPoster(StreamInfo info, TrailerDataProvider.ResultCallback callback) {
        String url = info.getPosterUrl();


        Glide.with(getContext())
                .load(url).diskCacheStrategy(DiskCacheStrategy.DATA)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        callback.onThumbnailLoaded(resource);
                    }
                });
    }

    @Override
    protected boolean checkEnableSidePanel() {
        return false;
    }
}