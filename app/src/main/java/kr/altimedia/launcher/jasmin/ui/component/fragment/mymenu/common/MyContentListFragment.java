/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.ui.view.browse.RowsSupportFragment;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class MyContentListFragment extends RowsSupportFragment {
    private final String TAG = MyContentListFragment.class.getSimpleName();
    private static final String KEY_PADDING_RECT = "padding";

    private VerticalGridView.OnScrollOffsetCallback mOnScrollOffsetCallback;
    private BaseOnItemViewSelectedListener baseOnItemViewSelectedListener;

    public static MyContentListFragment newInstance(Rect rect) {
        MyContentListFragment fragment = new MyContentListFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_PADDING_RECT, rect);
        fragment.setArguments(args);
        return fragment;
    }

    public MyContentListFragment() {
    }

    @Override
    public void onDestroyView() {
        mVerticalGridView.setOnScrollOffsetCallback(null);
        setOnItemViewSelectedListener(null);

        super.onDestroyView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Rect rect = getArguments().getParcelable(KEY_PADDING_RECT);
        mVerticalGridView.setPadding(rect.left, rect.top, rect.right, rect.bottom);

        setOnScrollOffsetCallback(mOnScrollOffsetCallback);
        setOnItemViewSelectedListener(baseOnItemViewSelectedListener);
    }

    public void setOnScrollOffsetCallback(VerticalGridView.OnScrollOffsetCallback onScrollOffsetCallback) {
        if (mVerticalGridView == null) {
            mOnScrollOffsetCallback = onScrollOffsetCallback;
            return;
        }
        mVerticalGridView.setOnScrollOffsetCallback(onScrollOffsetCallback);

        if (onScrollOffsetCallback instanceof MyContentListShadowHelper) {
            ((MyContentListShadowHelper) onScrollOffsetCallback).setParentView(mVerticalGridView);
        }
    }

    @Override
    public void setOnItemViewSelectedListener(BaseOnItemViewSelectedListener listener) {
        if (mVerticalGridView == null) {
            baseOnItemViewSelectedListener = listener;
        }

        super.setOnItemViewSelectedListener(listener);
    }

    public void resetFocus() {
        if(mVerticalGridView != null) {
            mVerticalGridView.post(new Runnable() {
                @Override
                public void run() {
                    mVerticalGridView.scrollToPosition(0);
                }
            });
        }
    }
}
