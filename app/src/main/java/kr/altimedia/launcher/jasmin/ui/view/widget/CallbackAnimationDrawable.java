/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget;

import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by mc.kim on 18,06,2020
 */
class CallbackAnimationDrawable extends AnimationDrawable {
    private OnAnimDrawableCallback adListener;
    private Handler timingHandler = null;
    private Runnable resultListenerRunnable = new Runnable() {
        @Override
        public void run() {
            if (adListener != null) adListener.onAnimationOneShotFinished();
        }
    };

    public CallbackAnimationDrawable(AnimationDrawable aniDrawable) {
        for (int i = 0; i < aniDrawable.getNumberOfFrames(); i++) {
            this.addFrame(aniDrawable.getFrame(i), aniDrawable.getDuration(i));
        }
        timingHandler = new Handler(Looper.getMainLooper());
    }

    public OnAnimDrawableCallback getAnimDrawableListener() {
        return adListener;
    }

    public void setAnimDrawableListener(OnAnimDrawableCallback callback) {
        this.adListener = callback;
    }

    @Override
    public boolean selectDrawable(int index) {
        if (index != 0 && index == getNumberOfFrames() - 1) {
            timingHandler.postDelayed(resultListenerRunnable, this.getDuration(index));
        }
        return super.selectDrawable(index);
    }

    public interface OnAnimDrawableCallback {
        void onAnimationOneShotFinished();
    }
}