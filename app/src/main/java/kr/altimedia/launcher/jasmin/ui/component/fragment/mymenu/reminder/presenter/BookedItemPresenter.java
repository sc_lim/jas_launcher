/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.reminder.presenter;

import android.media.tv.TvContentRating;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.util.Log;
import com.google.common.collect.ImmutableList;

import java.util.Calendar;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.ParentalController;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.Reminder;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class BookedItemPresenter extends Presenter {
    private static final String TAG = BookedItemPresenter.class.getSimpleName();
    private OnKeyListener onKeyListener;

    public BookedItemPresenter(OnKeyListener onKeyListener) {
        this.onKeyListener = onKeyListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_mymenu_booked_list, parent, false);
        BookedEventViewHolder viewHolder = new BookedEventViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (item == null) {
            viewHolder.view.setVisibility(View.INVISIBLE);
            return;
        }

        BookedEventViewHolder vh = (BookedEventViewHolder) viewHolder;
        Reminder bookedItem = (Reminder) item;
        vh.setData(bookedItem);
        viewHolder.view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }

    public OnKeyListener getOnKeyListener() {
        return onKeyListener;
    }

    private int getDiffDay(long targetTime){
        Calendar currCal = Calendar.getInstance();
        currCal.setTimeInMillis(JasmineEpgApplication.SystemClock().currentTimeMillis());
        int currYear = currCal.get(Calendar.YEAR);
        int currMonth = currCal.get(Calendar.MONTH) + 1;
        int currDay = currCal.get(Calendar.DAY_OF_MONTH);

        Calendar targetCal = Calendar.getInstance();
        targetCal.setTimeInMillis(targetTime);
        int targetYear = targetCal.get(Calendar.YEAR);
        int targetMonth = targetCal.get(Calendar.MONTH) + 1;
        int targetDay = targetCal.get(Calendar.DAY_OF_MONTH);

        int currDate = currYear * 12 + currMonth + currDay;
        int targetDate = targetYear * 12 + targetMonth + targetDay;
        return targetDate - currDate;
    }

    public interface OnKeyListener {
        boolean onKey(int keyCode, int index, Object item);
    }

    public class BookedEventViewHolder extends ViewHolder {
        TextView channelNumber;
        TextView channelName;
        TextView programName;
        TextView programStartDay;
        TextView programStartTime;
        ImageView lockIcon;

        public BookedEventViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            channelNumber = view.findViewById(R.id.channelNumber);
            channelName = view.findViewById(R.id.channelName);
            programName = view.findViewById(R.id.programName);
            programStartDay = view.findViewById(R.id.programStartDay);
            programStartTime = view.findViewById(R.id.programStartTime);
            lockIcon = view.findViewById(R.id.lockIcon);
        }

        public void setData(Reminder item) {

            try {
                long channelId = item.getChannelId();
                Channel channel = TvSingletons.getSingletons(view.getContext()).getChannelDataManager().getChannel(channelId);
                if (Log.INCLUDE) {
                    Log.d(TAG, "setData: channelId=" + channelId + ", channel=" + channel);
                }
                String chNum = "";
                String chName = "";
                boolean isLocked = false;
                if (channel != null) {
                    chNum = StringUtil.getFormattedNumber(channel.getDisplayNumber(), 3);
                    chName = channel.getDisplayName();
                    isLocked = channel.isLocked();
                }
                String pgmName = item.getTitle();
                int diffDay = getDiffDay(item.getStartTime());
                if (Log.INCLUDE) {
                    Log.d(TAG, "setData: program: start=" + item.getStartTime() + ", end=" + item.getEndTime());
                }
                String startDay = "Today";
                if (diffDay == 0) {
                    startDay = view.getResources().getString(R.string.today2);
                } else if (diffDay == 1) {
                    startDay = view.getResources().getString(R.string.tomorrow);
                } else {
                    startDay = TimeUtil.getModifiedDate(item.getStartTime(), "E dd MMM");
                }
                String startTime = StringUtil.getFormattedHour(Calendar.HOUR_OF_DAY, item.getStartTime())
                        + ":" + StringUtil.getFormattedMinute(item.getStartTime());
//            String endTime = StringUtil.getFormattedHour(item.getEndTimeUtcMillis())
//                    + ":" + StringUtil.getFormattedMinute(item.getEndTimeUtcMillis());

                channelNumber.setText(chNum);
                channelName.setText(chName);
                programName.setText(pgmName);
                programStartDay.setText(startDay);
                programStartTime.setText(startTime);

                if(!isLocked) {
                    Program program = item.getProgram();
                    if (program != null) {
                        isLocked = isRatingBlocked(program.getContentRatings());
                    }
                }
                if (isLocked) {
                    lockIcon.setImageResource(R.drawable.icon_grid_lock);
                }
            }catch (Exception e){
            }
        }

        private boolean isRatingBlocked(ImmutableList<TvContentRating> ratings) {
            try {
                return ParentalController.isProgramRatingBlocked(ratings);
            }catch (Exception e){
            }
            return false;
        }
    }
}
