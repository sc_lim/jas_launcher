/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data;

import android.os.Parcel;
import android.os.Parcelable;

import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProduct;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Bank;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class PurchaseInfo implements Parcelable {
    private CouponProduct couponProduct;
    private PaymentType paymentType;
    private CreditCard creditCard;
    private Bank bank;

    public PurchaseInfo(CouponProduct couponItem, PaymentType paymentType) {
        this.couponProduct = couponItem;
        this.paymentType = paymentType;
    }

    protected PurchaseInfo(Parcel in) {
        couponProduct = in.readParcelable(CouponProduct.class.getClassLoader());
        paymentType = PaymentType.findByName((String) in.readValue(String.class.getClassLoader()));
        creditCard = (CreditCard) in.readValue(CreditCard.class.getClassLoader());
        bank = (Bank) in.readValue(Bank.class.getClassLoader());
    }

    public static final Creator<PurchaseInfo> CREATOR = new Creator<PurchaseInfo>() {
        @Override
        public PurchaseInfo createFromParcel(Parcel in) {
            return new PurchaseInfo(in);
        }

        @Override
        public PurchaseInfo[] newArray(int size) {
            return new PurchaseInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(couponProduct);
        dest.writeValue(paymentType.getCode());
        dest.writeValue(creditCard);
        dest.writeValue(bank);
    }

    public CouponProduct getCouponProduct() {
        return couponProduct;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public Bank getBank() {
        return bank;
    }
}
