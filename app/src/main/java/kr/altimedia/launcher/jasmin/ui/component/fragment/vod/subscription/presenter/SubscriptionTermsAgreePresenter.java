/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.single.presenter.SubscriptionButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.SubscriptionButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.row.TermsRow;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class SubscriptionTermsAgreePresenter extends Presenter {
    private static final int AGREE_BUTTON = 0;
    private static final int DETAIL_BUTTON = 1;

    private OnClickTermsButtonListener onClickTermsButtonListener;

    public SubscriptionTermsAgreePresenter() {
    }

    public void setOnClickTermsButtonListener(OnClickTermsButtonListener onClickTermsButtonListener) {
        this.onClickTermsButtonListener = onClickTermsButtonListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_subscription_terms_agree, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        TermsRow termsRow = (TermsRow) item;
        vh.setData((String) termsRow.getAdapter().get(0));
        vh.setOnClickTermsButtonListener(onClickTermsButtonListener);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setOnClickTermsButtonListener(null);
    }

    public static class ViewHolder extends Presenter.ViewHolder implements SubscriptionButtonBridgeAdapter.OnButtonClickListener {
        private TextView description;
        private HorizontalGridView gridView;
        private ArrayObjectAdapter objectAdapter;

        private OnClickTermsButtonListener onClickTermsButtonListener;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            description = view.findViewById(R.id.terms_description);

            initGridView();
        }

        private void initGridView() {
            gridView = view.findViewById(R.id.grid_view);
            gridView.setEnabled(false);

            objectAdapter = new ArrayObjectAdapter(new SubscriptionButtonPresenter());
            objectAdapter.add(AGREE_BUTTON, new SubscriptionButtonPresenter.ButtonItem(R.string.agree, true, false));
            objectAdapter.add(DETAIL_BUTTON, new SubscriptionButtonPresenter.ButtonItem(R.string.detail, false, false));

            SubscriptionButtonBridgeAdapter mSubscriptionButtonBridgeAdapter = new SubscriptionButtonBridgeAdapter(objectAdapter, gridView);
            mSubscriptionButtonBridgeAdapter.setOnButtonClickListener(this);
            gridView.setAdapter(mSubscriptionButtonBridgeAdapter);
        }

        private void setData(String desc) {
            description.setText(desc);
        }

        public void setOnClickTermsButtonListener(OnClickTermsButtonListener onClickTermsButtonListener) {
            this.onClickTermsButtonListener = onClickTermsButtonListener;
        }

        public void updateTermsButtons(boolean isEnable, boolean isChecked) {
            gridView.setEnabled(isEnable);

            SubscriptionButtonPresenter.ButtonItem agreeButtonItem = (SubscriptionButtonPresenter.ButtonItem) objectAdapter.get(AGREE_BUTTON);
            SubscriptionButtonPresenter.ButtonItem detailButtonItem = (SubscriptionButtonPresenter.ButtonItem) objectAdapter.get(DETAIL_BUTTON);

            agreeButtonItem.setChecked(isEnable && isChecked);
            agreeButtonItem.setEnabled(isEnable);
            detailButtonItem.setEnabled(isEnable);

            objectAdapter.replace(AGREE_BUTTON, agreeButtonItem);
            objectAdapter.replace(DETAIL_BUTTON, detailButtonItem);

            if (isEnable) {
                gridView.scrollToPosition(AGREE_BUTTON);
                gridView.requestFocus();
            }
        }

        public boolean isEnabled() {
            return gridView.isEnabled();
        }

        public void setCheckTermsAgree(boolean isAgree) {
            SubscriptionButtonPresenter.ButtonItem buttonItem = (SubscriptionButtonPresenter.ButtonItem) objectAdapter.get(AGREE_BUTTON);
            buttonItem.setChecked(isAgree);
            objectAdapter.replace(AGREE_BUTTON, buttonItem);
        }

        @Override
        public void onClick(int index, boolean isChecked) {
            if (index == AGREE_BUTTON) {
                updateTermsButtons(true, isChecked);
                onClickTermsButtonListener.onClickAgree(isChecked);
            } else if (index == DETAIL_BUTTON) {
                onClickTermsButtonListener.onClickDetail();
            }
        }
    }

    public interface OnClickTermsButtonListener {
        void onClickAgree(boolean isAgree);

        void onClickDetail();
    }
}
