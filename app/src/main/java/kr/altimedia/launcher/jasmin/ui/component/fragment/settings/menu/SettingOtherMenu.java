/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu;

import kr.altimedia.launcher.jasmin.R;

public enum SettingOtherMenu implements SettingMenu {
    DEVICE_MANAGE(R.string.device_manage, 0), SYSTEM_SETTING(R.string.system_settings, 1),
    SYSTEM_INFO(R.string.system_info, 2), FAQ(R.string.faq, 3);

    private int menuName;
    private int menuId;

    SettingOtherMenu(int menuName, int menuId) {
        this.menuName = menuName;
        this.menuId = menuId;
    }

    @Override
    public int getMenuName() {
        return menuName;
    }

    @Override
    public int getMenuId() {
        return menuId;
    }

    @Override
    public boolean isEnable() {
        return true;
    }
}
