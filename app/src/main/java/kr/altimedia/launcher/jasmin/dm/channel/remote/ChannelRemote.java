/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel.remote;


import com.altimedia.util.Log;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.MbsUtil;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.channel.api.ChannelApi;
import kr.altimedia.launcher.jasmin.dm.channel.obj.ChannelProductInfo;
import kr.altimedia.launcher.jasmin.dm.channel.obj.MbsChannel;
import kr.altimedia.launcher.jasmin.dm.channel.obj.response.ChannelListResponse;
import kr.altimedia.launcher.jasmin.dm.channel.obj.response.ChannelProductInfoResponse;
import kr.altimedia.launcher.jasmin.dm.channel.obj.response.ProgramListResponse;
import kr.altimedia.launcher.jasmin.dm.channel.obj.response.SubscribedChannelListResponse;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mc.kim on 11,05,2020
 */
public class ChannelRemote {
    private static final String TAG = ChannelRemote.class.getSimpleName();
    private ChannelApi mChannelApi;

    public ChannelRemote(ChannelApi channelApi) {
        mChannelApi = channelApi;
    }

    public List<MbsChannel> getChannelList(String said) throws IOException, ResponseException, AuthenticationException {
        Call<ChannelListResponse> call = mChannelApi.getChannelList(said);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ChannelListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ChannelListResponse mChannelListResponse = response.body();
        return mChannelListResponse.getChannelList();
    }

    public ProgramListResponse getProgramList(String lang, String said,
                                              String channelid, String startTime, String endTime) throws IOException, ResponseException, AuthenticationException {
        Call<ProgramListResponse> call = mChannelApi.getProgramList(lang, said, channelid, startTime, endTime);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ProgramListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }

        MbsUtil.checkValid(response);
        ProgramListResponse mProgramListResponse = response.body();
        return mProgramListResponse;
    }

    public List<String> getSubscribedChannelList(String said, String profiledId) throws IOException, ResponseException , AuthenticationException{
        Call<SubscribedChannelListResponse> call = mChannelApi.getSubscribedChannelList(said, profiledId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<SubscribedChannelListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        SubscribedChannelListResponse mSubscribedChannelListResponse = response.body();
        return mSubscribedChannelListResponse.getSubscribedChannelListResult();
    }

    public ChannelProductInfo getChannelProductInfo(String said, String profiledId, String channelId, String language) throws IOException, ResponseException, AuthenticationException {
        Call<ChannelProductInfoResponse> call = mChannelApi.getChannelProductInfo(said, profiledId, channelId, language);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ChannelProductInfoResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ChannelProductInfoResponse mChannelProductInfoResponse = response.body();
        return mChannelProductInfoResponse.getChannelProductInfo();
    }
}
