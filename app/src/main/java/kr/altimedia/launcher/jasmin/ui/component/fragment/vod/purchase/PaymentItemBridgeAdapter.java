/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase;

import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ObjectAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentButton;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;

public class PaymentItemBridgeAdapter extends ItemBridgeAdapter {
    private static final String TAG = PaymentItemBridgeAdapter.class.getSimpleName();

    private OnPaymentClickListener onPaymentClickListener;

    public PaymentItemBridgeAdapter(ObjectAdapter adapter) {
        super(adapter);
    }

    public void setOnPaymentClickListener(OnPaymentClickListener onPaymentClickListener) {
        this.onPaymentClickListener = onPaymentClickListener;
    }

    @Override
    protected void onBind(ItemBridgeAdapter.ViewHolder viewHolder) {
        super.onBind(viewHolder);

        PaymentButton paymentButton = (PaymentButton) viewHolder.mItem;
        viewHolder.itemView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                switch (keyCode) {
                    case KeyEvent.KEYCODE_ENTER:
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        if (onPaymentClickListener != null) {
                            onPaymentClickListener.onPaymentClick(paymentButton);
                        }

                        return true;
                }
                return false;
            }
        });
    }

    public void initView() {
    }

    @Override
    protected void onUnbind(ItemBridgeAdapter.ViewHolder viewHolder) {
        super.onUnbind(viewHolder);

        viewHolder.itemView.setOnKeyListener(null);
    }

    public interface OnPaymentClickListener {
        void onPaymentClick(Object item);
    }
}