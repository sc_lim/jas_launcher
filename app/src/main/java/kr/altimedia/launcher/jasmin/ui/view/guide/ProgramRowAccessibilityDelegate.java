/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerViewAccessibilityDelegate;

/** AccessibilityDelegate for {@link ProgramRow} */
class ProgramRowAccessibilityDelegate extends RecyclerViewAccessibilityDelegate {
    private final ItemDelegate mItemDelegate;

    ProgramRowAccessibilityDelegate(RecyclerView recyclerView) {
        super(recyclerView);

        mItemDelegate =
                new ItemDelegate(this) {
                    @Override
                    public boolean performAccessibilityAction(View host, int action, Bundle args) {
                        // Prevent Accessibility service to move the Program Row elements
                        // Ignoring Accessibility action above Set Text
                        // (accessibilityActionShowOnScreen)
                        if (action > AccessibilityNodeInfo.ACTION_SET_TEXT) {
                            return false;
                        }

                        return super.performAccessibilityAction(host, action, args);
                    }
                };
    }

    @Override
    public ItemDelegate getItemDelegate() {
        return mItemDelegate;
    }

    @Override
    public boolean onRequestSendAccessibilityEvent(
            ViewGroup host, View child, AccessibilityEvent event) {
        // Forcing the next item to be visible for scrolling in forward direction
        if (event.getEventType() == AccessibilityEvent.TYPE_VIEW_FOCUSED) {
            ((ProgramRow) host).focusSearchAccessibility(child, View.FOCUS_FORWARD);
        }
        return super.onRequestSendAccessibilityEvent(host, child, event);
    }
}
