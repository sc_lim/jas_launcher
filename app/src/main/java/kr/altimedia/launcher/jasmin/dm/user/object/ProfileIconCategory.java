/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.annotation.NonNull;

public class ProfileIconCategory implements Parcelable {

    public static final Creator<ProfileIconCategory> CREATOR = new Creator<ProfileIconCategory>() {
        @Override
        public ProfileIconCategory createFromParcel(Parcel in) {
            return new ProfileIconCategory(in);
        }

        @Override
        public ProfileIconCategory[] newArray(int size) {
            return new ProfileIconCategory[size];
        }
    };

    @Expose
    @SerializedName("code")
    private String code;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("profileIconList")
    private List<ProfileIcon> iconList;

    public ProfileIconCategory(String code, String name, List<ProfileIcon> iconList){
        this.code = code;
        this.name = name;
        this.iconList = iconList;
    }

    protected ProfileIconCategory(Parcel in) {
        code = in.readString();
        name = in.readString();
        in.readList(iconList, ProfileIcon.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(name);
        dest.writeList(iconList);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public List<ProfileIcon> getProfileIconList() {
        return iconList;
    }

    @NonNull
    @Override
    public String toString() {
        return "ProfileIconCategory{" +
                "code=" + code +
                ", name=" + name +
                ", iconList=" + iconList +
                "}";
    }
}
