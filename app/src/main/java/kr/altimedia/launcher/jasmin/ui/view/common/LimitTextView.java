/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;

public class LimitTextView extends RelativeLayout {
    private RelativeLayout bg;
    private TextView textView;
    private String mText = "";

    private int inputMaxLength = 0;
    private int inputDigitLimit = 0;

    private View.OnKeyListener onKeyListener;

    public LimitTextView(Context context) {
        super(context);
        initView();
    }

    public LimitTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);
    }

    public LimitTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
        getAttrs(attrs, defStyleAttr);
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.common_digit_text, this, false);
        addView(v);

        bg = v.findViewById(R.id.bg);
        textView = v.findViewById(R.id.textView);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.LimitTextViewTheme);
        setTypeArray(typedArray);
    }


    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.LimitTextViewTheme, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        inputMaxLength = typedArray.getInt(R.styleable.LimitTextViewTheme_limit_text_max_length, -1);
        inputDigitLimit = typedArray.getInt(R.styleable.LimitTextViewTheme_limit_text_input_limit, -1);

        int size = typedArray.getResourceId(R.styleable.LimitTextViewTheme_limit_text_size, R.dimen.digit_text_view_text_size);
        int color = typedArray.getResourceId(R.styleable.LimitTextViewTheme_limit_text_color, R.drawable.selector_common_bg_text);
        int bgColor = typedArray.getResourceId(R.styleable.LimitTextViewTheme_limit_text_bg_drawable, R.drawable.selector_button_focus_w);

        setLength(inputMaxLength);
        setTextSize(size);
        setTextColor(color);
        bg.setBackgroundResource(bgColor);

        typedArray.recycle();
    }

    public void setHint(String hint) {
        textView.setHint(hint);
    }

    public void setText(String text) {
        mText = text;
        textView.setText(mText);
    }

    public void setTextSize(int dimen) {
        int size = (int) getResources().getDimension(dimen);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    }

    public void setTextGravity(int gravity) {
        textView.setGravity(gravity);
    }

    public void setLength(int limit) {
        if (limit > 0) {
            inputMaxLength = limit;
            textView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(limit)});
        }
    }

    public void setLimit(int limit) {
        inputDigitLimit = limit;
    }

    public void setTextColor(int resourceId) {
        textView.setTextColor(getResources().getColorStateList(resourceId, null));
    }

    @Override
    public void setOnKeyListener(OnKeyListener onKeyListener) {
        this.onKeyListener = onKeyListener;
    }

    public String getString() {
        return mText;
    }

    public int getLimit() {
        return inputDigitLimit;
    }

    public int getInputMaxLength() {
        return inputMaxLength;
    }

    public void add(int num) {
        int total = isEmpty() ? 0 : Integer.parseInt(getString());

        if (total >= getLimit()) {
            return;
        }

        int addedTotal = (total * 10) + num;
        if (isOverLimit(addedTotal)) {
            return;
        }

        input(addedTotal);
    }

    public void delete(boolean zeroIgnored) {
        String current = getString();
        int size = current.length();

        if (size == 0) {
            return;
        }

        int last = size > 0 ? size - 1 : 0;
        current = current.substring(0, last);

        if (current.equals("")) {
            clear();
        } else {
            if(zeroIgnored) {
                input(Integer.parseInt(current));
            }else {
                forceInput(current);
            }
        }
    }

    public void input(int inputDigit) {
        mText = String.valueOf(inputDigit);
        textView.setText(mText);
    }

    //for 0 & Char
    public void forceInput(String text) {
        mText = text;
        textView.setText(text);
    }

    public void clear() {
        mText = "";
        textView.setText(mText);
    }

    public boolean isEmpty() {
        return mText.isEmpty();
    }

    public boolean isFull() {
        if (inputMaxLength < 0) return false;

        return mText.length() == inputMaxLength;
    }

    public boolean isLimit(int digit) {
        if (inputDigitLimit < 0) return false;

        return digit == inputDigitLimit;
    }

    public boolean isOverLimit(int digit) {
        return digit > inputDigitLimit;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();

        if (action != KeyEvent.ACTION_DOWN) {
            return super.dispatchKeyEvent(event);
        }

        if (onKeyListener != null) {
            boolean isConsumed = onKeyListener.onKey(this, keyCode, event);
            if (isConsumed) {
                return true;
            }
        }

        return super.dispatchKeyEvent(event);
    }

    public void addTextChangedListener(TextWatcher watcher) {
        textView.addTextChangedListener(watcher);
    }
}
