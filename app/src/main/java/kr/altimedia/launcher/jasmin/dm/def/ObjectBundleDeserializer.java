package kr.altimedia.launcher.jasmin.dm.def;

import android.os.Bundle;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Iterator;

public class ObjectBundleDeserializer implements
        JsonDeserializer<Bundle> {

    @Override
    public Bundle deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        Bundle result = new Bundle();
        if (json.isJsonArray()) {
            JsonArray jsonArray = json.getAsJsonArray();
            int size = jsonArray.size();
            for (int i = 0; i < size; i++) {
                JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                Iterator<String> keySetIterator = jsonObject.keySet().iterator();
                while (keySetIterator.hasNext()) {
                    String key = keySetIterator.next();
                    if (jsonObject.has(key) && !jsonObject.get(key).isJsonNull()) {
                        String value = jsonObject.get(key).getAsString();
                        result.putString(key, value);
                    }
                }
            }
        } else if (json.isJsonObject()) {
            JsonObject jsonObject = json.getAsJsonObject();
            Iterator<String> keySetIterator = jsonObject.keySet().iterator();
            while (keySetIterator.hasNext()) {
                String key = keySetIterator.next();
                if (jsonObject.has(key) && !jsonObject.get(key).isJsonNull()) {
                    String value = jsonObject.get(key).getAsString();
                    result.putString(key, value);
                }
            }
        }
        return result;
    }

}
