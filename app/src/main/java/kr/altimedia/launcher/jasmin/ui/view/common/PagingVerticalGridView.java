/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class PagingVerticalGridView extends VerticalGridView {
    private static String TAG = PagingVerticalGridView.class.getSimpleName();

    private int visibleCount = 0;
    private int numColumns = 1;

    public PagingVerticalGridView(Context context) {
        super(context);
    }

    public PagingVerticalGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PagingVerticalGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void initVertical(int visibleCount) {
        this.visibleCount = visibleCount;
        setItemAlignmentOffsetPercent(0);
    }

    @Override
    public void setNumColumns(int numColumns) {
        super.setNumColumns(numColumns);
        this.numColumns = numColumns;
    }

    public int getRowIndex(int index) {
        index = index + 1;
        int row = index / numColumns;
        if (index % numColumns > 0) {
            row += 1;
        }

        return row;
    }

    @Override
    public void requestChildFocus(View child, View focused) {
        int index = getChildAdapterPosition(focused);
        int height = child.getMeasuredHeight();
        setAlignment(index, height);

        super.requestChildFocus(child, focused);
    }

    public void setAlignment(int index, int height) {
        if (numColumns > 1) {
            index = getRowIndex(index) - 1;
        }

        float value = index % visibleCount;
        int gap = getVerticalSpacing();
        int offset = (int) ((height + gap) * value);

        setWindowAlignmentOffsetPercent(0);
        setWindowAlignmentOffset(offset);
    }
}
