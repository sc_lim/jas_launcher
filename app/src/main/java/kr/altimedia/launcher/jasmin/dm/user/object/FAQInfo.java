/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FAQInfo implements Parcelable {
    @Expose
    @SerializedName("faqTitle")
    private String faqTitle;
    @Expose
    @SerializedName("faqDescription")
    private String faqDescription;

    protected FAQInfo(Parcel in) {
        faqTitle = in.readString();
        faqDescription = in.readString();
    }

    public static final Creator<FAQInfo> CREATOR = new Creator<FAQInfo>() {
        @Override
        public FAQInfo createFromParcel(Parcel in) {
            return new FAQInfo(in);
        }

        @Override
        public FAQInfo[] newArray(int size) {
            return new FAQInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(faqTitle);
        dest.writeString(faqDescription);
    }

    public String getFaqTitle() {
        return faqTitle;
    }

    public String getFaqDescription() {
        return faqDescription;
    }

    @NonNull
    @Override
    public String toString() {
        return "FAQInfo{" +
                "faqTitle=" + faqTitle +
                ", faqDescription=" + faqDescription +
                "}";
    }
}
