/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv;

import android.content.Context;
import android.media.tv.TvContentRating;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.util.Log;
import com.google.common.collect.ImmutableList;

import java.util.HashMap;

import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;

public class ParentalController {
    private static final String SYSTEM_RATING_TH = "TH_TV";

    private static final String TV_4 = "TH_TV_4";
    private static final String TV_6 = "TH_TV_6";
    private static final String TV_10 = "TH_TV_10";
    private static final String TV_13 = "TH_TV_13";
    private static final String TV_18 = "TH_TV_18";
    private static final String TV_19 = "TH_TV_19";

    private static HashMap<String, Integer> mRatingMap = new HashMap<>();

    static {
        mRatingMap.put(TV_4, 3);
        mRatingMap.put(TV_6, 6);
        mRatingMap.put(TV_10, 0);
        mRatingMap.put(TV_13, 13);
        mRatingMap.put(TV_18, 18);
        mRatingMap.put(TV_19, 20);
    }

    public enum BlockType {
        CLEAN_CHANNEL(false),
        PREVIEW_CHANNEL(false),
        BLOCK_CHANNEL(true),
        BLOCK_PROGRAM(true),
        BLOCK_UNSUBSCRIBED(true),
        BLOCK_ADULT_CHANNEL(true),
        BLOCK_UHD(true);

        private boolean blocked;

        BlockType(boolean blocked) {
            this.blocked = blocked;
        }

        public boolean isBlocked() {
            return blocked;
        }

        public boolean isPreview() {
            return this == PREVIEW_CHANNEL;
        }

        public boolean isUnsubscribed() {
            return this == BLOCK_UNSUBSCRIBED;
        }

        @Override
        public String toString() {
            return "BlockType{" +
                    "name=" + name() +
                    ", blocked=" + blocked +
                    ", preview=" + isPreview() +
                    '}';
        }
    }

    private static final String TAG = ParentalController.class.getSimpleName();
    private static final boolean DEBUG = true;
    private static final boolean TEST = false;

    //private UserPreferenceManagerImp userPreferenceManagerImp;

//    ParentalController(Context context) {
//        userPreferenceManagerImp = new UserPreferenceManagerImp(context);
//    }

    private ParentalController() {
    }

    public static BlockType checkBlock(Channel channel, Program program, boolean previewEnabled, @JasTvView.TvViewType int tvViewType) {
        BlockType blockType = _checkBlock(channel, program, previewEnabled, tvViewType);
        return blockType;
    }

    static BlockType _checkBlock(Channel channel, Program program, boolean previewEnabled, @JasTvView.TvViewType int tvViewType) {
//        ParentalRating pr = userPreferenceManagerImp.getParentalRating();
        if (TEST) {
            BlockType type = getDummyInfo(channel);
            if (type != BlockType.CLEAN_CHANNEL) {
                return type;
            }
        }

        if (channel == null) return BlockType.CLEAN_CHANNEL;
        if (channel.isAdultChannel()) return BlockType.BLOCK_ADULT_CHANNEL;
        if (channel.isLocked()) return BlockType.BLOCK_CHANNEL;
        if (channel.isUHDChannel() && tvViewType != JasTvView.TYPE_MAIN) return BlockType.BLOCK_UHD;
        if (isPaidChannel(channel)) {
            return checkPaidChannel(channel, previewEnabled);
        }
//        if (program != null && isProgramRatingBlocked(program.getContentRatings()))
//            return BlockType.BLOCK_PROGRAM;
        return BlockType.CLEAN_CHANNEL;
    }

    static boolean isPaidChannel(Channel channel) {
        return (channel.isPaidChannel());
    }

    static BlockType checkPaidChannel(Channel channel, boolean previewEnabled) {
        if (channel.isAdultChannel()) {
            return BlockType.BLOCK_ADULT_CHANNEL;
        }
        if (channel.isPaidChannel()) {
            if (previewEnabled && JasChannelManager.getInstance().isPreviewAvailable(channel)) {
                return BlockType.PREVIEW_CHANNEL;
            } else {
                return BlockType.BLOCK_UNSUBSCRIBED;
            }
        }
        return BlockType.CLEAN_CHANNEL;
    }

    static BlockType getDummyInfo(Channel channel) {
        int chNum = channel != null ? Integer.parseInt(channel.getDisplayNumber()) : 0;
        switch (chNum) {
            case 265:
            case 74:
                return BlockType.BLOCK_UNSUBSCRIBED;
            case 264:
                return BlockType.BLOCK_PROGRAM;
            case 262:
                return BlockType.BLOCK_CHANNEL;
            default:
                return BlockType.CLEAN_CHANNEL;
        }
    }

    private static TvContentRating findBySystemRating(ImmutableList<TvContentRating> ratings, String systemRating) {
        if (ratings == null || ratings.isEmpty()) {
            return null;
        }
        for (TvContentRating contentRating : ratings) {
            if (Log.INCLUDE) {
                Log.d(TAG, "findBySystemRating : " + contentRating.flattenToString());
            }
            if (contentRating.getRatingSystem().equalsIgnoreCase(systemRating)) {
                return contentRating;
            }
        }
        return null;
    }

    private static int getRatingFromMap(String mainRating) {
        if (mRatingMap.containsKey(mainRating)) {
            return mRatingMap.get(mainRating);
        }
        return 0;
    }

    public static boolean isProgramRatingBlocked(Context context, Channel channel) {
        ProgramDataManager programDataManager = TvSingletons.getSingletons(context).getProgramDataManager();
        Program program = programDataManager.getCurrentProgram(channel.getId());
        return isProgramRatingBlocked(program);
    }

    public static boolean isProgramRatingBlocked(Program program) {
        return program != null && isProgramRatingBlocked(program.getContentRatings());
    }

    public static boolean isProgramRatingBlocked(ImmutableList<TvContentRating> ratings) {
        TvContentRating contentRating = findBySystemRating(ratings, SYSTEM_RATING_TH);
        if (contentRating == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "isProgramRatingBlocked() contentRating:null - return false");
            }
            return false;
        }
        String mainRating = contentRating.getMainRating();
        int rating = getRatingFromMap(mainRating);
        ProfileInfo profileInfo = AccountManager.getInstance().getSelectedProfile();
        int userRating = profileInfo.getRating();
        if (Log.INCLUDE) {
            Log.d(TAG, "isProgramRatingBlocked() mainRating:" + mainRating+", prog.rating:"+ rating+", userRating:" + userRating+", isBlocked:" + (userRating != 0 && rating >= userRating));
        }
        return userRating != 0 && rating > userRating;
    }

    public static void updateParentalRating() {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateParentalRating() rating:" + AccountManager.getInstance().getSelectedProfile().getRating() + ", profile:" + AccountManager.getInstance().getSelectedProfile());
        }

        int rating = AccountManager.getInstance().getSelectedProfile().getRating();
        int convertedRating;

        switch (rating) {
            case 20:
                convertedRating = 19;
                break;
            case 3:
                convertedRating = 4;
                break;
            case 6:
            case 13:
            case 18:
            default:
                convertedRating = rating;
                break;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "updateParentalRating() settings.setParentalRating(rating:" + convertedRating + ")");
        }
        SettingControl.getInstance().setParentalRating(convertedRating);
    }

    public static String getRatingStringForDebug(Program prog) {
        String rating = "";
        if (prog != null) {
            ImmutableList<TvContentRating> list = prog.getContentRatings();
//            Log.d("SHLEE", "list.size:"+(list==null ? null : list.size()));
            for (int i = 0 ; i < list.size() ; i++) {
                rating += list.get(i).getMainRating() +"/";
//                Log.d("SHLEE", "list["+i+"]:"+list.get(i).flattenToString());
            }
//            Log.d("SHLEE", "rating:["+rating+"]");
        }
        return rating;
    }
}
