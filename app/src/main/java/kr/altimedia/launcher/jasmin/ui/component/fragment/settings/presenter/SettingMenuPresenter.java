/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingMenu;

public class SettingMenuPresenter extends Presenter {
    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_setting_menu, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((SettingMenu) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private final int PARENT_FOCUS_TEXT_COLOR = R.color.color_FFB4B4B4;

        private final int DEFAULT_TEXT_COLOR = R.color.color_66B4B4B4;
        private final int DEFAULT_TEXT_FONT = R.font.font_prompt_regular;

        private final int ACTIVITY_TEXT_COLOR = R.color.color_FFFFFFFF;
        private final int ACTIVITY_TEXT_FONT = R.font.font_prompt_medium;

        private TextView textView;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            textView = view.findViewById(R.id.setting_menu);
        }

        public void setData(SettingMenu settingMenu) {
            String title = view.getContext().getString(settingMenu.getMenuName());
            textView.setText(title);
            textView.setVisibility(View.VISIBLE);
            textView.setEnabled(settingMenu.isEnable());
        }

        public void setParentFocus(boolean hasParentFocus) {
            Resources resources = view.getResources();
            if (view.isActivated()) {
                textView.setTextColor(resources.getColor(ACTIVITY_TEXT_COLOR));
                textView.setTypeface(resources.getFont(ACTIVITY_TEXT_FONT), Typeface.NORMAL);
                return;
            }

            int color = hasParentFocus ? PARENT_FOCUS_TEXT_COLOR : DEFAULT_TEXT_COLOR;
            textView.setTextColor(view.getResources().getColor(color));
            textView.setTypeface(resources.getFont(DEFAULT_TEXT_FONT), Typeface.NORMAL);
        }
    }
}
