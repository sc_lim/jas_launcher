package kr.altimedia.launcher.jasmin.system.manager.reboot;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.altimedia.util.Log;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Random;

import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class AutoRebootManager {
    private static final AutoRebootManager INSTANCE = new AutoRebootManager();
    private final String TAG = AutoRebootManager.class.getSimpleName();

    public static final String KEY_REBOOT_TIME = "REBOOT_TIME";

    private final int ALARM_REQUEST_CODE = 1001;
    private final int ACTIVATION_HOUR = 4;

    private AutoRebootManager() {
    }

    public static AutoRebootManager getInstance() {
        return INSTANCE;
    }

    public void setRebootTimer(Context context) {
        UserPreferenceManagerImp up = new UserPreferenceManagerImp(context);
        boolean isAutoRebootEnable = up.isAutoRebootEnable();
        if (!isAutoRebootEnable) {
            if (Log.INCLUDE) {
                Log.d(TAG, "setRebootTimer, isAutoRebootEnable == false, so return");
            }
            return;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "setRebootTimer, ACTIVATION_HOUR : " + ACTIVATION_HOUR);
        }

        resetRebootTimer(context);

        long now = JasmineEpgApplication.getSingletons(context).getClock().currentTimeMillis();
        LocalDate date = Instant.ofEpochMilli(now).atZone(ZoneId.systemDefault()).toLocalDate().plusDays(1);
        LocalTime time = LocalTime.of(ACTIVATION_HOUR, 0, 0);
        LocalDateTime standardDateTime = LocalDateTime.of(date, time);
        long standardTime = standardDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        int random = new Random().nextInt(60);
        long randomTime = random * TimeUtil.MIN;
        long rebootTime = standardTime + randomTime;

        if (Log.INCLUDE) {
            Log.d(TAG, "setRebootTimer"
                    + ", random : " + random
                    + ", randomTime : " + randomTime
                    + ", standardDateTime : " + standardDateTime + "(" + standardTime + ")"
                    + ", rebootTime : " + rebootTime);
        }

        Intent rebootIntent = new Intent(context, AutoRebootService.class);
        rebootIntent.putExtra(KEY_REBOOT_TIME, rebootTime);
        PendingIntent pendingIntent = PendingIntent.getService(context, ALARM_REQUEST_CODE, rebootIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, rebootTime, pendingIntent);
    }

    public void resetRebootTimer(Context context) {
        Intent rebootIntent = new Intent(context, AutoRebootService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, ALARM_REQUEST_CODE, rebootIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Log.INCLUDE) {
            Log.d(TAG, "resetRebootTimer, pendingIntent : " + pendingIntent);
        }

        if (pendingIntent != null) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();
        }
    }
}
