/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.animation.AnimatorInflater;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.Arrays;

import androidx.annotation.ColorInt;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.leanback.widget.Action;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.OnActionClickedListener;
import androidx.leanback.widget.PlaybackSeekDataProvider;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.ScaleFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.ThumbnailBar;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoControlBarPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekDataProvider;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekUi;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackTransportRowView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;

/**
 * Created by mc.kim on 06,02,2020
 */
public class VideoPlaybackTransportRowPresenter extends VideoPlaybackRowPresenter {
    private final String TAG = VideoPlaybackTransportRowView.class.getSimpleName();
    public static final long SEEK_DIFFER_TIME = 10 * 1000;

    static class BoundData extends VideoPlaybackControlsPresenter.BoundData {
        ViewHolder mRowViewHolder;
    }

    private void initRow(final ViewHolder vh) {
//        vh.mProgressBar.setProgressColor(mProgressColorSet ? mProgressColor
//                : getDefaultProgressColor(vh.mControlsDock.getContext()));
        vh.mControlsVh = (VideoControlBarPresenter.ViewHolder) mPlaybackControlsPresenter
                .onCreateViewHolder(vh.mControlsDock);
//        vh.mProgressBar.setSecondaryProgressColor(mSecondaryProgressColorSet
//                ? mSecondaryProgressColor
//                : getDefaultSecondaryProgressColor(vh.mControlsDock.getContext()));
        vh.mControlsDock.addView(vh.mControlsVh.view);

        vh.mSecondaryControlsVh = (VideoControlBarPresenter.ViewHolder) mSecondaryControlsPresenter
                .onCreateViewHolder(vh.mSecondaryControlsDock);
        vh.mSecondaryControlsDock.addView(vh.mSecondaryControlsVh.view);
        ((VideoPlaybackTransportRowView) vh.view.findViewById(R.id.transport_row))
                .setOnUnhandledKeyListener(new VideoPlaybackTransportRowView.OnUnhandledKeyListener() {
                    @Override
                    public boolean onUnhandledKey(KeyEvent event) {
                        if (vh.getOnKeyListener() != null) {
                            return vh.getOnKeyListener().onKey(vh.view, event.getKeyCode(), event);
                        }
                        return false;
                    }
                });
    }


    static void formatTime(long ms, StringBuilder sb) {
        sb.setLength(0);
        if (ms < 0) {
            sb.append("--");
            return;
        }
        long seconds = ms / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        seconds -= minutes * 60;
        minutes -= hours * 60;

        if (hours < 10) {
            sb.append('0');
        }
        sb.append(hours).append(':');

        if (minutes < 10) {
            sb.append('0');
        }

        sb.append(minutes).append(':');
        if (seconds < 10) {
            sb.append('0');
        }
        sb.append(seconds);
    }

    float mDefaultSeekIncrement = 0.01f;
    int mProgressColor = Color.TRANSPARENT;
    int mSecondaryProgressColor = Color.TRANSPARENT;
    boolean mProgressColorSet;
    boolean mSecondaryProgressColorSet;
    Presenter mDescriptionPresenter;
    VideoControlBarPresenter mPlaybackControlsPresenter;
    VideoControlBarPresenter mSecondaryControlsPresenter;
    OnActionClickedListener mOnActionClickedListener;

    private final VideoControlBarPresenter.OnControlSelectedListener mOnControlSelectedListener =
            new VideoControlBarPresenter.OnControlSelectedListener() {
                @Override
                public void onControlSelected(Presenter.ViewHolder itemViewHolder, Object item,
                                              VideoControlBarPresenter.BoundData data) {
                    ViewHolder vh = ((BoundData) data).mRowViewHolder;
                    if (vh.mSelectedViewHolder != itemViewHolder || vh.mSelectedItem != item) {
                        vh.mSelectedViewHolder = itemViewHolder;
                        vh.mSelectedItem = item;
                        vh.dispatchItemSelection();
                    }
                }
            };

    private final VideoControlBarPresenter.OnControlClickedListener mOnControlClickedListener =
            new VideoControlBarPresenter.OnControlClickedListener() {
                @Override
                public void onControlClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                             VideoControlBarPresenter.BoundData data) {
                    ViewHolder vh = ((BoundData) data).mRowViewHolder;
                    if (vh.getOnItemViewClickedListener() != null) {
                        vh.getOnItemViewClickedListener().onItemClicked(itemViewHolder, item,
                                vh, vh.getRow());
                    }
                    if (mOnActionClickedListener != null && item instanceof Action) {
                        mOnActionClickedListener.onActionClicked((Action) item);
                    }
                }
            };

    public VideoPlaybackTransportRowPresenter() {
        setHeaderPresenter(null);
        setSelectEffectEnabled(false);

        mPlaybackControlsPresenter = new VideoControlBarPresenter(R.layout.widget_video_control_bar);
        mPlaybackControlsPresenter.setDefaultFocusToMiddle(false);
        mSecondaryControlsPresenter = new VideoControlBarPresenter(R.layout.widget_video_control_bar);
        mSecondaryControlsPresenter.setDefaultFocusToMiddle(false);

        mPlaybackControlsPresenter.setOnControlSelectedListener(mOnControlSelectedListener);
        mSecondaryControlsPresenter.setOnControlSelectedListener(mOnControlSelectedListener);
        mPlaybackControlsPresenter.setOnControlClickedListener(mOnControlClickedListener);
        mSecondaryControlsPresenter.setOnControlClickedListener(mOnControlClickedListener);
    }

    /**
     * @param descriptionPresenter Presenter for displaying item details.
     */
    public void setDescriptionPresenter(Presenter descriptionPresenter) {
        mDescriptionPresenter = descriptionPresenter;
    }

    /**
     * Sets the listener for {@link Action} click events.
     */
    public void setOnActionClickedListener(OnActionClickedListener listener) {
        mOnActionClickedListener = listener;
    }

    /**
     * Returns the listener for {@link Action} click events.
     */
    public OnActionClickedListener getOnActionClickedListener() {
        return mOnActionClickedListener;
    }

    /**
     * Sets the primary color for the progress bar.  If not set, a default from
     * the theme will be used.
     */
    public void setProgressColor(@ColorInt int color) {
        mProgressColor = color;
        mProgressColorSet = true;
    }

    /**
     * Returns the primary color for the progress bar.  If no color was set, transparent
     * is returned.
     */
    @ColorInt
    public int getProgressColor() {
        return mProgressColor;
    }

    /**
     * Sets the secondary color for the progress bar.  If not set, a default from
     * the theme {@link R.attr#playbackProgressSecondaryColor} will be used.
     *
     * @param color Color used to draw secondary progress.
     */
    public void setSecondaryProgressColor(@ColorInt int color) {
        mSecondaryProgressColor = color;
        mSecondaryProgressColorSet = true;
    }

    /**
     * Returns the secondary color for the progress bar.  If no color was set, transparent
     * is returned.
     */
    @ColorInt
    public int getSecondaryProgressColor() {
        return mSecondaryProgressColor;
    }

    @Override
    public void setEnable(RowPresenter.ViewHolder rowViewHolder, boolean enable) {

    }

    @Override
    public void onReappear(RowPresenter.ViewHolder rowViewHolder) {
        ViewHolder vh = (ViewHolder) rowViewHolder;
        if(Log.INCLUDE){
            Log.d(TAG,"onReappear");
        }
        if (vh.view.hasFocus()) {
            if(Log.INCLUDE){
                Log.d(TAG,"onReappear | vh.view.hasFocus()");
            }
            vh.mInSeek = false;

            View skipBtn = vh.mDescriptionDock.findViewById(R.id.btnSkipIntro);
            if (skipBtn != null && skipBtn.getVisibility() == View.VISIBLE) {
                if(Log.INCLUDE){
                    Log.d(TAG,"onReappear | vh.view.hasFocus() : skipBtn");
                }
                skipBtn.requestFocus();
                return;
            }
            if(Log.INCLUDE){
                Log.d(TAG,"onReappear | vh.view.hasFocus() : mProgressBar");
            }
            vh.mProgressBar.requestFocus();
        }
    }

    @Override
    public void onHide(RowPresenter.ViewHolder rowViewHolder) {
        super.onHide(rowViewHolder);
        ViewHolder vh = (ViewHolder) rowViewHolder;
        if (vh.view.hasFocus()) {
            vh.mInSeek = false;
            vh.mProgressBar.requestFocus();
        }
    }


    @Override
    public void onOptionEnabled(RowPresenter.ViewHolder rowViewHolder, boolean enable) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onOptionEnabled : " + enable);
        }
        ViewHolder vh = (ViewHolder) rowViewHolder;
        View optionLayer = vh.mDescriptionDock.findViewById(R.id.optionLayer);
        View optionGuideLayer = vh.mDescriptionDock.findViewById(R.id.optionGuideText);
        View btnSkipIntro = vh.mDescriptionDock.findViewById(R.id.btnSkipIntro);
        if (optionLayer != null) {
            optionLayer.setTag(R.id.option_enable, enable);
            optionLayer.setVisibility(enable ? View.VISIBLE : View.GONE);
        }
        if (optionGuideLayer != null && btnSkipIntro != null) {
            if (btnSkipIntro.getVisibility() == View.VISIBLE) {
                optionGuideLayer.setVisibility(View.GONE);
            } else {
                optionGuideLayer.setVisibility(enable ? View.VISIBLE : View.GONE);
            }
        }
    }


    private static int getDefaultProgressColor(Context context) {
        TypedValue outValue = new TypedValue();
        if (context.getTheme()
                .resolveAttribute(R.attr.playbackProgressPrimaryColor, outValue, true)) {
            return context.getResources().getColor(outValue.resourceId);
        }
        return context.getResources().getColor(R.color.lb_playback_progress_color_no_theme);
    }

    private static int getDefaultSecondaryProgressColor(Context context) {
        TypedValue outValue = new TypedValue();
        if (context.getTheme()
                .resolveAttribute(R.attr.playbackProgressSecondaryColor, outValue, true)) {
            return context.getResources().getColor(outValue.resourceId);
        }
        return context.getResources().getColor(
                R.color.lb_playback_progress_secondary_color_no_theme);
    }

    @Override
    protected RowPresenter.ViewHolder createRowViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_video_playback_transport_controls_row, parent, false);
        ViewHolder vh = new ViewHolder(v, mDescriptionPresenter);
        initRow(vh);
        return vh;
    }

    private static class OnProgressKeyListener implements View.OnKeyListener {
        final ViewHolder mViewHolder;

        public OnProgressKeyListener(ViewHolder mViewHolder) {
            this.mViewHolder = mViewHolder;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent keyEvent) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP:
//                        case KeyEvent.KEYCODE_DPAD_DOWN:
                    // eat DPAD UP/DOWN in seek mode

                    if (mViewHolder.mSeekDataProvider == null || mViewHolder.mSeekDataProvider.getSeekPositions() == null || mViewHolder.mSeekDataProvider.getSeekPositions().length == 0) {
                        return mViewHolder.mInSeek;
                    }

                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                        mViewHolder.mThumbNailFrame.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
                        mViewHolder.setThumbnailCurrentPosition(mViewHolder.mThumbsBar, mViewHolder.getCurrentTimeInMs());
                    }
                    return mViewHolder.mInSeek;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    mViewHolder.onProgressbarKeyPressed(mViewHolder, keyEvent);
                    return true;

//                case KeyEvent.KEYCODE_MINUS:
//                case KeyEvent.KEYCODE_MEDIA_REWIND:
//                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
//                        mViewHolder.onBackward();
//                    }
//                    return true;
//                case KeyEvent.KEYCODE_PLUS:
//                case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
//                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
//                        mViewHolder.onForward();
//                    }
//                    return true;
                case KeyEvent.KEYCODE_DPAD_CENTER:
                case KeyEvent.KEYCODE_ENTER:
                    if (!mViewHolder.mInSeek) {
                        return false;
                    }
                    if (keyEvent.getAction() == KeyEvent.ACTION_UP) {
                        mViewHolder.stopSeek(false);
                    }
                    return true;
                case KeyEvent.KEYCODE_BACK:
                case KeyEvent.KEYCODE_ESCAPE:
                    if (!mViewHolder.mInSeek) {
                        return false;
                    }
                    if (keyEvent.getAction() == KeyEvent.ACTION_UP) {
                        // SeekBar does not support cancel in accessibility mode, so always
                        // "confirm" if accessibility is on.
                        mViewHolder.stopSeek(Build.VERSION.SDK_INT < 21 || !mViewHolder.mProgressBar.isAccessibilityFocused());
                    }

            }
            return false;
        }
    }

    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);

        ViewHolder vh = (ViewHolder) holder;
        VideoPlaybackControlsRow row = (VideoPlaybackControlsRow) vh.getRow();

        if (row.getItem() == null) {
            vh.mDescriptionDock.setVisibility(View.GONE);
        } else {
            vh.mDescriptionDock.setVisibility(View.VISIBLE);
            if (vh.mDescriptionViewHolder != null) {
                mDescriptionPresenter.onBindViewHolder(vh.mDescriptionViewHolder, row.getItem());
            }
        }

        if (row.getImageDrawable() == null) {
            vh.mImageView.setVisibility(View.GONE);
        } else {
            vh.mImageView.setVisibility(View.VISIBLE);
        }
        vh.mImageView.setImageDrawable(row.getImageDrawable());

        vh.mControlsBoundData.adapter = row.getPrimaryActionsAdapter();
        vh.mControlsBoundData.presenter = vh.getPresenter(true);
        vh.mControlsBoundData.mRowViewHolder = vh;
        mPlaybackControlsPresenter.onBindViewHolder(vh.mControlsVh, vh.mControlsBoundData);

        vh.mSecondaryBoundData.adapter = row.getSecondaryActionsAdapter();
        vh.mSecondaryBoundData.presenter = vh.getPresenter(false);
        vh.mSecondaryBoundData.mRowViewHolder = vh;
        mSecondaryControlsPresenter.onBindViewHolder(vh.mSecondaryControlsVh,
                vh.mSecondaryBoundData);

        vh.setTotalTime(row.getDuration());
        vh.setCurrentPosition(row.getCurrentPosition());
        vh.setBufferedPosition(row.getBufferedPosition());
        row.setOnPlaybackProgressChangedListener(vh.mListener);
    }

    @Override
    protected void onUnbindRowViewHolder(RowPresenter.ViewHolder holder) {
        ViewHolder vh = (ViewHolder) holder;
        VideoPlaybackControlsRow row = (VideoPlaybackControlsRow) vh.getRow();

        if (vh.mDescriptionViewHolder != null) {
            mDescriptionPresenter.onUnbindViewHolder(vh.mDescriptionViewHolder);
        }
        mPlaybackControlsPresenter.onUnbindViewHolder(vh.mControlsVh);
        mSecondaryControlsPresenter.onUnbindViewHolder(vh.mSecondaryControlsVh);
        row.setOnPlaybackProgressChangedListener(null);

        super.onUnbindRowViewHolder(holder);
    }

    /**
     * Client of progress bar is clicked, default implementation delegate click to
     * PlayPauseAction.
     *
     * @param vh ViewHolder of PlaybackTransportRowPresenter
     */

    protected void onProgressBarKey(ViewHolder vh, KeyEvent keyEvent) {

    }

    protected void onSkipButtonHide() {

    }

    protected void onProgressBarClicked(ViewHolder vh) {
        if (vh != null) {
            if (vh.mPlayPauseAction == null) {
                vh.mPlayPauseAction = new VideoPlaybackControlsRow.PlayPauseAction(vh.view.getContext());
            }

            if (vh.getOnItemViewClickedListener() != null) {
                vh.getOnItemViewClickedListener().onItemClicked(vh, vh.mPlayPauseAction,
                        vh, vh.getRow());
            }
            if (mOnActionClickedListener != null) {
                mOnActionClickedListener.onActionClicked(vh.mPlayPauseAction);
            }
        }
    }

    /**
     * Set default seek increment if {@link PlaybackSeekDataProvider} is null.
     *
     * @param ratio float value between 0(inclusive) and 1(inclusive).
     */
    public void setDefaultSeekIncrement(float ratio) {
        mDefaultSeekIncrement = ratio;
    }

    /**
     * Get default seek increment if {@link PlaybackSeekDataProvider} is null.
     *
     * @return float value between 0(inclusive) and 1(inclusive).
     */
    public float getDefaultSeekIncrement() {
        return mDefaultSeekIncrement;
    }

    @Override
    protected void onRowViewSelected(RowPresenter.ViewHolder vh, boolean selected) {
        super.onRowViewSelected(vh, selected);
        if (selected) {
            ((ViewHolder) vh).dispatchItemSelection();
        }
    }

    @Override
    protected void onRowViewAttachedToWindow(RowPresenter.ViewHolder vh) {
        super.onRowViewAttachedToWindow(vh);
        if (mDescriptionPresenter != null) {
            mDescriptionPresenter.onViewAttachedToWindow(
                    ((ViewHolder) vh).mDescriptionViewHolder);
        }
    }

    @Override
    protected void onRowViewDetachedFromWindow(RowPresenter.ViewHolder vh) {
        super.onRowViewDetachedFromWindow(vh);
        if (mDescriptionPresenter != null) {
            mDescriptionPresenter.onViewDetachedFromWindow(
                    ((ViewHolder) vh).mDescriptionViewHolder);
        }
    }

    /**
     * A ViewHolder for the PlaybackControlsRow supporting seek UI.
     */
    public class ViewHolder extends VideoPlaybackRowPresenter.ViewHolder
            implements VideoPlaybackSeekUi, ThumbnailBar.OnThumbnailItemActionListener {
        final View mRootView;
        final BrowseFrameLayout mDescriptionParentLayer;
        final Presenter.ViewHolder mDescriptionViewHolder;
        final ImageView mImageView;
        final ViewGroup mDescriptionDock;
        final ViewGroup mControlsDock;
        final ViewGroup mSecondaryControlsDock;
        final TextView mTotalTime;
        final TextView mCurrentTime;
        final AppCompatSeekBar mProgressBar;
        final ThumbnailBar mThumbsBar;
        final ImageIndicator leftIndicator;
        final ImageIndicator rightIndicator;

        final Drawable mThumbnailFocus;
        final Drawable mThumbnailUnFocus;
        final int mThumbnailOffset;

        final ScaleFrameLayout mThumbNailFrame;

        ValueAnimator mThumbnailRowFadeInAnimator, mThumbnailRowFadeOutAnimator;

        long mTotalTimeInMs = Long.MIN_VALUE;
        long mCurrentTimeInMs = Long.MIN_VALUE;
        long mSecondaryProgressInMs;
        final StringBuilder mTempBuilder = new StringBuilder();
        VideoControlBarPresenter.ViewHolder mControlsVh;
        VideoControlBarPresenter.ViewHolder mSecondaryControlsVh;
        BoundData mControlsBoundData = new BoundData();
        BoundData mSecondaryBoundData = new BoundData();
        Presenter.ViewHolder mSelectedViewHolder;
        Object mSelectedItem;
        VideoPlaybackControlsRow.PlayPauseAction mPlayPauseAction;
        int mThumbHeroIndex = -1;

        Client mSeekClient;
        boolean mInSeek;
        VideoPlaybackSeekDataProvider mSeekDataProvider;
        long[] mPositions;
        int mPositionsLength;
        final VideoPlaybackControlsRow.OnPlaybackProgressCallback mListener =
                new VideoPlaybackControlsRow.OnPlaybackProgressCallback() {

                    @Override
                    public void onReadyStateChanged(VideoPlaybackControlsRow row, boolean ready) {
                        super.onReadyStateChanged(row, ready);
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onReadyStateChanged : " + ready);
                        }
                        mRootView.setVisibility(ready ? View.VISIBLE : View.INVISIBLE);
                        if(ready){
                            mThumbsBar.scrollCurrentPosition(getCurrentTimeInMs(), true);
                            View skipButton = mDescriptionDock.findViewById(R.id.btnSkipIntro);
                            if (skipButton != null ) {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        skipButton.requestFocus();
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onCurrentPositionChanged(VideoPlaybackControlsRow row, long ms, boolean seeking) {
                        setCurrentPosition(ms);
                        if (seeking) {
                            return;
                        }
                        View optionLayer = mDescriptionDock.findViewById(R.id.optionLayer);
                        View optionGuideText = mDescriptionDock.findViewById(R.id.optionGuideText);
                        boolean optionEnable = optionLayer.getTag(R.id.option_enable) != null ?
                                (Boolean) optionLayer.getTag(R.id.option_enable) : true;
                        View skipButton = mDescriptionDock.findViewById(R.id.btnSkipIntro);
                        if (skipButton != null) {
                            Long skipTime = skipButton.getTag(R.id.skip_time_millis) != null ?
                                    (Long) skipButton.getTag(R.id.skip_time_millis) : 0;
                            if (skipTime == 0) {
                                return;
                            }
                            if (skipTime <= ms) {
                                if (optionGuideText.getVisibility() != View.VISIBLE && optionEnable) {
                                    optionGuideText.setVisibility(View.VISIBLE);
                                }
                                if (skipButton.getVisibility() != View.GONE) {
                                    skipButton.setVisibility(View.GONE);
                                    onSkipButtonHide();
                                }
                            } else {
                                if (optionGuideText.getVisibility() != View.GONE) {
                                    optionGuideText.setVisibility(View.GONE);
                                }
                                if (skipButton.getVisibility() != View.VISIBLE) {
                                    skipButton.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }

                    @Override
                    public void onDurationChanged(VideoPlaybackControlsRow row, long ms) {
                        setTotalTime(ms);
                    }

                    @Override
                    public void onBufferedPositionChanged(VideoPlaybackControlsRow row, long ms) {
                        setBufferedPosition(ms);
                    }
                };

        //        boolean onForward() {
//            return onForward(false);
//        }
//
//        boolean onForward(boolean withIcon) {
//            long seekTimeInMs = mCurrentTimeInMs + SEEK_DIFFER_TIME;
//            if (seekTimeInMs > mTotalTimeInMs) {
//                seekTimeInMs = mTotalTimeInMs;
//            }
//            seekTime(seekTimeInMs);
//            return true;
//        }
//
        boolean onProgressbarKeyPressed(ViewHolder vh, KeyEvent keyEvent) {
            onProgressBarKey(vh, keyEvent);
            return true;
        }
//
//        boolean onBackward(boolean withIcon) {
//            long seekTimeInMs = mCurrentTimeInMs - SEEK_DIFFER_TIME;
//            if (seekTimeInMs < 0) {
//                seekTimeInMs = 0;
//            }
//            seekTime(seekTimeInMs);
//            return true;
//        }

        public void setThumbnailCurrentPosition(ThumbnailBar thumbNailBar, long currentPosition) {
            if (thumbNailBar == null) {
                return;
            }
            int focusPosition = thumbNailBar.initCurrentPosition(currentPosition);
            leftIndicator.updateArrow(focusPosition);
            rightIndicator.updateArrow(focusPosition);
        }

        public long getCurrentTimeInMs() {
            if (Log.INCLUDE) {
                Log.d(TAG, "call getCurrentTimeInMs : " + mCurrentTimeInMs);
            }
            return mCurrentTimeInMs;
        }

        public void setCurrentTimeInMs(long currentTimeInMs) {
            this.mCurrentTimeInMs = currentTimeInMs;
        }

        public ViewHolder(View rootView, Presenter descriptionPresenter) {
            super(rootView);
            loadDescriptionAnimator(rootView.getContext());
            mRootView = rootView;
            mDescriptionParentLayer = rootView.findViewById(R.id.descriptionParentLayer);
            mImageView = rootView.findViewById(R.id.image);
            mDescriptionDock = rootView.findViewById(R.id.description_dock);
            mCurrentTime = rootView.findViewById(R.id.current_time);
            mTotalTime = rootView.findViewById(R.id.total_time);
            mThumbNailFrame = rootView.findViewById(R.id.thumbNailFrame);
            mProgressBar = rootView.findViewById(R.id.playback_progress);
            mThumbsBar = rootView.findViewById(R.id.thumbs_row);
            leftIndicator = rootView.findViewById(R.id.leftIndicator);
            rightIndicator = rootView.findViewById(R.id.rightIndicator);
            mThumbnailFocus = rootView.getContext().getDrawable(R.drawable.play_point);
            mThumbnailUnFocus = rootView.getContext().getDrawable(R.drawable.play_point_unfocused);
            mThumbnailOffset = rootView.getResources().getDimensionPixelOffset(R.dimen.video_thumbnail_offset);
            mProgressBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onProgressBarClicked(ViewHolder.this);
                }
            });
            OnProgressKeyListener mOnProgressKeyListener = new OnProgressKeyListener(this);
            mProgressBar.setOnKeyListener(mOnProgressKeyListener);
            mProgressBar.setMax(Integer.MAX_VALUE); //current progress will be a fraction of this
            mControlsDock = rootView.findViewById(R.id.controls_dock);
            mSecondaryControlsDock =
                    rootView.findViewById(R.id.secondary_controls_dock);
            mDescriptionViewHolder = descriptionPresenter == null ? null :
                    descriptionPresenter.onCreateViewHolder(mDescriptionDock);
            if (mDescriptionViewHolder != null) {
                mDescriptionDock.addView(mDescriptionViewHolder.view);
            }
            mRootView.setVisibility(View.INVISIBLE);

            mDescriptionParentLayer.setOnChildFocusListener(new BrowseFrameLayout.OnChildFocusListener() {
                @Override
                public boolean onRequestFocusInDescendants(int var1, Rect var2) {
                    return false;
                }

                @Override
                public void onRequestChildFocus(View child, View focused) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onRequestChildFocus : toView : " + focused.getClass().getSimpleName() +
                                ", fromView : " + child.getClass().getSimpleName());
                    }

                    switch (focused.getId()) {
                        case R.id.playback_progress:
                        case R.id.btnSkipIntro:
                            if (Log.INCLUDE) {
                                Log.d(TAG, "make disable");
                            }
                            leftIndicator.setArrowVisible(View.INVISIBLE);
                            rightIndicator.setArrowVisible(View.INVISIBLE);

                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressBar.setThumb(focused.getId() == R.id.playback_progress ? mThumbnailFocus : mThumbnailUnFocus);
                                    mProgressBar.setThumbOffset(-mThumbnailOffset);
                                }
                            });


                            if (mThumbNailFrame.getAlpha() == 0.9f) {
                                break;
                            }
                            reverseFirstOrStartSecond(mThumbnailRowFadeInAnimator, mThumbnailRowFadeOutAnimator,
                                    true);


                            break;


                        default:

                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressBar.setThumb(mThumbnailFocus);
                                    mProgressBar.setThumbOffset(-mThumbnailOffset);
                                }
                            });


                            if (mThumbNailFrame.getAlpha() == 1f) {
                                break;
                            }
                            reverseFirstOrStartSecond(mThumbnailRowFadeOutAnimator, mThumbnailRowFadeInAnimator,
                                    true);

                            break;
                    }
                }
            });
        }

        private ValueAnimator loadAnimator(Context context, int resId) {
            ValueAnimator animator = (ValueAnimator) AnimatorInflater.loadAnimator(context, resId);
            animator.setDuration(animator.getDuration());
            return animator;
        }

        private TimeInterpolator mLogDecelerateInterpolator = new TimeInterpolator() {
            @Override
            public float getInterpolation(float t) {
                float mLogScale = 1f / computeLog(1, 100, 0);
                return computeLog(t, 100, 0) * mLogScale;
            }
        };

        private TimeInterpolator mLogAccelerateInterpolator = new TimeInterpolator() {
            @Override
            public float getInterpolation(float t) {
                float mLogScale = 1f / computeLog(1, 100, 0);
                return 1 - computeLog(1 - t, 100, 0) * mLogScale;
            }
        };

        private float computeLog(float t, int base, int drift) {
            return (float) -Math.pow(base, -t) + 1 + (drift * t);
        }


        private void reverseFirstOrStartSecond(ValueAnimator first, ValueAnimator second,
                                               boolean runAnimation) {
            if (first.isStarted()) {
                first.reverse();
                if (!runAnimation) {
                    first.end();
                }
            } else {
                second.start();
                if (!runAnimation) {
                    second.end();
                }
            }
        }


        private void loadDescriptionAnimator(Context context) {
            final ValueAnimator.AnimatorUpdateListener updateListener = new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator arg0) {

                    final float fraction = (Float) arg0.getAnimatedValue();
                    mThumbNailFrame.setAlpha(fraction);
                    mThumbNailFrame.setChildScale(fraction);
                }
            };


            mThumbnailRowFadeInAnimator = loadAnimator(context, R.animator.playback_thumbnail_focus_in);
            mThumbnailRowFadeInAnimator.addUpdateListener(updateListener);
            mThumbnailRowFadeInAnimator.setInterpolator(mLogDecelerateInterpolator);

            mThumbnailRowFadeOutAnimator = loadAnimator(context,
                    R.animator.playback_thumbnail_focus_out);
            mThumbnailRowFadeOutAnimator.addUpdateListener(updateListener);
            mThumbnailRowFadeOutAnimator.setInterpolator(mLogAccelerateInterpolator);
        }

        public final Presenter.ViewHolder getDescriptionViewHolder() {
            return mDescriptionViewHolder;
        }

        @Override
        public void setPlaybackSeekUiClient(Client client) {
            mSeekClient = client;
            mSeekDataProvider = mSeekClient.getPlaybackSeekDataProvider();
            mThumbsBar.setSeekDataProvider(mSeekDataProvider,
                    this);

            if (mSeekDataProvider != null && mSeekDataProvider.getSeekPositions() != null) {
                leftIndicator.initIndicator(mSeekDataProvider.getSeekPositions().length, mSeekDataProvider.getVisibleSize());
                rightIndicator.initIndicator(mSeekDataProvider.getSeekPositions().length, mSeekDataProvider.getVisibleSize());
            } else {
                leftIndicator.setVisibility(View.INVISIBLE);
                rightIndicator.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onBackPressed() {
            stopSeek(true);
        }

        private void seekTime(long time) {
            onItemSelected(time);
            onItemClicked(time);
        }

        public void onItemSelected(long time) {
            double ratio = (double) time / mTotalTimeInMs;     // Range: [0, 1]
            mProgressBar.setProgress((int) (ratio * Integer.MAX_VALUE), true); // Could safely cast to int
            onSetCurrentPositionLabel(time);
            mSeekClient.onSeekPositionChanged(time);
            int focus = mThumbsBar.getSelectedPosition();
            leftIndicator.updateArrow(focus);
            rightIndicator.updateArrow(focus);
        }

        @Override
        public void onItemClicked(long time) {
            setCurrentTimeInMs(time);
            stopSeek(false);
            double ratio = (double) time / mTotalTimeInMs;
            mProgressBar.setProgress((int) (ratio * Integer.MAX_VALUE), true);
            mProgressBar.setSecondaryProgress((int) (ratio * Integer.MAX_VALUE));
        }

        boolean startSeek() {
            if (mInSeek) {
                return true;
            }
            if (mSeekClient == null || !mSeekClient.isSeekEnabled()
                    || mTotalTimeInMs <= 0) {
                return false;
            }
            mInSeek = true;
            mSeekClient.onSeekStarted();

            mPositions = mSeekDataProvider != null ? mSeekDataProvider.getSeekPositions() : null;
            if (mPositions != null) {
                int pos = Arrays.binarySearch(mPositions, mTotalTimeInMs);
                if (pos >= 0) {
                    mPositionsLength = pos + 1;
                } else {
                    mPositionsLength = -1 - pos;
                }
            } else {
                mPositionsLength = 0;
            }
            mControlsVh.view.setVisibility(View.GONE);
//            mSecondaryControlsVh.view.setVisibility(View.INVISIBLE);
//            mDescriptionViewHolder.view.setVisibility(View.INVISIBLE);
//            mThumbsBar.setVisibility(View.VISIBLE);
            return true;
        }

        void stopSeek(boolean cancelled) {
//            if (!mInSeek) {
//                return;
//            }
            mInSeek = false;
            mSeekClient.onSeekFinished(cancelled);
            if (mSeekDataProvider != null) {
                mSeekDataProvider.reset();
            }
            mThumbHeroIndex = -1;
            mThumbsBar.clearThumbBitmaps();
            mSeekDataProvider = null;
            mPositions = null;
            mPositionsLength = 0;
            mControlsVh.view.setVisibility(View.VISIBLE);
//            mSecondaryControlsVh.view.setVisibility(View.VISIBLE);
//            mDescriptionViewHolder.view.setVisibility(View.VISIBLE);
//            mThumbsBar.setVisibility(View.INVISIBLE);
        }

        void dispatchItemSelection() {
            if (!isSelected()) {
                return;
            }
            if (mSelectedViewHolder == null) {
                if (getOnItemViewSelectedListener() != null) {
                    getOnItemViewSelectedListener().onItemSelected(null, null,
                            ViewHolder.this, getRow());
                }
            } else {
                if (getOnItemViewSelectedListener() != null) {
                    getOnItemViewSelectedListener().onItemSelected(mSelectedViewHolder,
                            mSelectedItem, ViewHolder.this, getRow());
                }
            }
        }

        Presenter getPresenter(boolean primary) {
            ObjectAdapter adapter = primary
                    ? ((VideoPlaybackControlsRow) getRow()).getPrimaryActionsAdapter()
                    : ((VideoPlaybackControlsRow) getRow()).getSecondaryActionsAdapter();
            if (adapter == null) {
                return null;
            }
            if (adapter.getPresenterSelector() instanceof ControlButtonPresenterSelector) {
                ControlButtonPresenterSelector selector =
                        (ControlButtonPresenterSelector) adapter.getPresenterSelector();
                return selector.getPrimaryPresenter();
            }
            return adapter.getPresenter(adapter.size() > 0 ? adapter.get(0) : null);
        }

        /**
         * Returns the TextView that showing total time label. This method might be used in
         * {@link #onSetDurationLabel}.
         *
         * @return The TextView that showing total time label.
         */
        public final TextView getDurationView() {
            return mTotalTime;
        }

        /**
         * Called to update total time label. Default implementation updates the TextView
         * {@link #getDurationView()}. Subclass might override.
         *
         * @param totalTimeMs Total duration of the media in milliseconds.
         */
        protected void onSetDurationLabel(long totalTimeMs) {
            if (mTotalTime != null) {
                formatTime(totalTimeMs, mTempBuilder);
                mTotalTime.setText(mTempBuilder.toString());
            }
        }

        void setTotalTime(long totalTimeMs) {
            if (mTotalTimeInMs != totalTimeMs) {
                mTotalTimeInMs = totalTimeMs;
                onSetDurationLabel(totalTimeMs);
            }
        }

        /**
         * Returns the TextView that showing current position label. This method might be used in
         * {@link #onSetCurrentPositionLabel}.
         *
         * @return The TextView that showing current position label.
         */
        public final TextView getCurrentPositionView() {
            return mCurrentTime;
        }

        /**
         * Called to update current time label. Default implementation updates the TextView
         * {@link #getCurrentPositionView}. Subclass might override.
         *
         * @param currentTimeMs Current playback position in milliseconds.
         */
        protected void onSetCurrentPositionLabel(long currentTimeMs) {
            if (mCurrentTime != null) {
                formatTime(currentTimeMs, mTempBuilder);
                mCurrentTime.setText(mTempBuilder.toString());
            }
        }

        void setCurrentPosition(long currentTimeMs) {
            boolean needToUpdate = false;
            if (currentTimeMs != mCurrentTimeInMs) {
                needToUpdate = true;
                setCurrentTimeInMs(currentTimeMs);
            }


            int progressRatio = 0;
            if (mTotalTimeInMs > 0) {
                // Use ratio to represent current progres
                double ratio = (double) mCurrentTimeInMs / mTotalTimeInMs;     // Range: [0, 1]
                progressRatio = (int) (ratio * Integer.MAX_VALUE);  // Could safely cast to int
            }

            if (!needToUpdate) {
                return;
            }
            if (currentTimeMs == -1) {
                return;
            }


            if (mThumbsBar.hasFocus()) {
                return;
            }
            mThumbsBar.scrollCurrentPosition(currentTimeMs, false);
            onSetCurrentPositionLabel(currentTimeMs);
            mProgressBar.setSecondaryProgress(progressRatio);
            mProgressBar.setProgress(progressRatio);
        }

        void setBufferedPosition(long progressMs) {
            mSecondaryProgressInMs = progressMs;
            // Solve the progress bar by using ratio
            double ratio = (double) progressMs / mTotalTimeInMs;           // Range: [0, 1]
            double progressRatio = ratio * Integer.MAX_VALUE;   // Could safely cast to int
        }
    }
}
