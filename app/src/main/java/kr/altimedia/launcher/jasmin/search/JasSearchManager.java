/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.search;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.tv.TvContract;
import android.net.Uri;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.fragment.app.FragmentManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.contents.ContentDataManager;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import kr.altimedia.launcher.jasmin.dm.search.SearchRequester;
import kr.altimedia.launcher.jasmin.dm.search.obj.SearchChannel;
import kr.altimedia.launcher.jasmin.dm.search.obj.SearchData;
import kr.altimedia.launcher.jasmin.dm.search.obj.SearchMonomax;
import kr.altimedia.launcher.jasmin.dm.search.obj.SearchVod;
import kr.altimedia.launcher.jasmin.dm.search.obj.SearchYoutube;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.activity.BaseActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodDetailsDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class JasSearchManager {
    private final String TAG = JasSearchManager.class.getSimpleName();

    // The columns we'll include in the table for the search framework.
    public static final String KEY_NAME = SearchManager.SUGGEST_COLUMN_TEXT_1; // mandatory for deep link of search framework
    public static final String KEY_DESCRIPTION = SearchManager.SUGGEST_COLUMN_TEXT_2;
    public static final String KEY_ICON = SearchManager.SUGGEST_COLUMN_RESULT_CARD_IMAGE;
    public static final String KEY_CONTENT_TYPE = SearchManager.SUGGEST_COLUMN_CONTENT_TYPE;
    public static final String KEY_IS_LIVE = SearchManager.SUGGEST_COLUMN_IS_LIVE;
    public static final String KEY_VIDEO_WIDTH = SearchManager.SUGGEST_COLUMN_VIDEO_WIDTH;
    public static final String KEY_VIDEO_HEIGHT = SearchManager.SUGGEST_COLUMN_VIDEO_HEIGHT;
    public static final String KEY_PRODUCTION_YEAR = SearchManager.SUGGEST_COLUMN_PRODUCTION_YEAR;  // mandatory for deep link of search framework
    public static final String KEY_COLUMN_DURATION = SearchManager.SUGGEST_COLUMN_DURATION;  // mandatory for deep link of search framework
    public static final String KEY_PROGRESS_BAR_PERCENTAGE = "progress_bar_percentage";
    public static final String KEY_INTENT_ACTION = SearchManager.SUGGEST_COLUMN_INTENT_ACTION;
    public static final String KEY_INTENT_DATA = SearchManager.SUGGEST_COLUMN_INTENT_DATA;
    public static final String KEY_INTENT_EXTRA_DATA = SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA;

    public static final String SEARCH_ACTION_VOD = "altimedia.intent.search.action.VOD";
    public static final String SEARCH_ACTION_CHANNEL = "altimedia.intent.search.action.CHANNEL";
    public static final String SEARCH_ACTION_MONOMAX = "altimedia.intent.search.action.MONOMAX";
    public static final String SEARCH_ACTION_YOUTUBE = "altimedia.intent.search.action.YOUTUBE";

    public static final String PKG_MONOMAX = "com.doonung.dtv";

    private final int SEARCH_DIALOG_VOICE = 1;
    private final int SEARCH_DIALOG_KEYBOARD = 2;
    private final String EXTRA_SEARCH_TYPE = "search_type";
    private int searchType = -1;

    private final String REQ_SORT_TYPE = "1"; // ascending
    private final String REQ_START_POSITION = "0";
    private final String REQ_SORT_FIELD = "";
    private final String REQ_ADULT_YN = "true";
    private final String REQ_DEVICE_TYPE = "STB";
    private final String REQ_SEARCH_OPTION = "";
    private final String REQ_EXPOSURE_TIME = "";
    private final String REQ_STB_VER = "0.5";

    private final String NO_LIVE_CONTENTS = "0";
    private final String LIVE_CONTENTS = "1";
    private final int PROGRESS_PERCENTAGE_HIDE = -1;

    private SearchData searchResult;

    private static JasSearchManager instance = null;

    public static JasSearchManager getInstance() {
        if (instance == null) {
            instance = new JasSearchManager();
        }
        return instance;
    }

    public void reset(){
        if (Log.INCLUDE) {
            Log.d(TAG, "reset");
        }
        searchType = -1;
    }

    public void startVoiceSearch(Activity activity) {
        searchType = SEARCH_DIALOG_VOICE;
        launchSearch(activity);
    }

    public void startKeyboardSearch(Activity activity) {
        searchType = SEARCH_DIALOG_KEYBOARD;
        launchSearch(activity);
    }

    private void launchSearch(Activity activity) {
        if (Log.INCLUDE) {
            Log.d(TAG, "launchSearch: type=" + searchType);
        }
        final Intent searchIntent = new Intent(Intent.ACTION_ASSIST); // google assist
        searchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        searchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        searchIntent.putExtra(EXTRA_SEARCH_TYPE, searchType);
        try {
            activity.startActivity(searchIntent);
        } catch (ActivityNotFoundException e) {
            if (Log.INCLUDE) {
                Log.e(TAG, "Exception launching intent " + searchIntent);
            }
            JasminToast.makeToast(activity, activity.getString(R.string.app_unavailable));
        }
    }

    public boolean findAllContents(String query, int limit) {
        query = query.toLowerCase();
        if (Log.INCLUDE) {
            Log.d(TAG, "findAllContents: query=" + query);
        }

        String searchCount = "100"; //Integer.toString(limit);
        String searchWord = query;

        try {
            SearchRequester searchRequester = new SearchRequester();
            AccountManager accountMgr = AccountManager.getInstance();
            String categoryID = "null";
            String productID = accountMgr.getProductCode();
            String saID = accountMgr.getSaId();
            String language = accountMgr.getLocalLanguage();
            String transactionID = searchRequester.getTransactionId();
            String rating = String.valueOf(AccountManager.getInstance().getCurrentRating());
            searchResult = searchRequester.getTotalSearch(
                    SearchRequester.SEARCH_ALL,
                    REQ_SORT_TYPE,
                    REQ_START_POSITION,
                    searchCount,
                    searchWord,
                    REQ_SORT_FIELD,
                    categoryID,
                    productID,
                    REQ_ADULT_YN,
                    saID,
                    REQ_DEVICE_TYPE,
                    REQ_EXPOSURE_TIME,
                    REQ_STB_VER,
                    SearchRequester.FILTER_FULL,
                    language,
                    transactionID,
                    rating
            );
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<SearchVod> getVodList() {
        if (searchResult != null) {
            return searchResult.getVodList();
        }
        return null;
    }

//    public List<SearchYoutube> getYoutubeList() {
//        if (searchResult != null) {
//            return searchResult.getYoutubeList();
//        }
//        return null;
//    }

    public List<SearchChannel> getRealtimeVodList() {
        if (searchResult != null) {
            return searchResult.getRealtimeVodList();
        }
        return null;
    }

    public List<SearchMonomax> getMonomaxList() {
        if (searchResult != null) {
            return searchResult.getMonomaxList();
        }
        return null;
    }

//    public List<SearchOption> getSrchOptList() {
//        if (searchResult != null) {
//            return searchResult.getSrchOptList();
//        }
//        return null;
//    }

    public int getTotalSize() {
        if (searchResult != null) {
            return searchResult.getTotalNum();
        }
        return 0;
    }

    public int getVodSize() {
        if (searchResult != null) {
            return searchResult.getVodNum();
        }
        return 0;
    }

    public int getRealtimeVodSize() {
        if (searchResult != null) {
            return searchResult.getEpgNum();
        }
        return 0;
    }

//    public int getYoutubeSize() {
//        if (searchResult != null) {
//            return searchResult.getYoutubeNum();
//        }
//        return 0;
//    }

    public int getMonomaxSize() {
        if (searchResult != null) {
            return searchResult.getMonomaxNum();
        }
        return 0;
    }

    public Object[] getCursorRow(SearchVod item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "SearchVod: " + item);
        }
        long duration = getDuration(item.getRuntime());
        String id = item.getConstId();
        String title = item.getTitle();
        String desc = "";
        if(item.getActor() != null && !item.getActor().isEmpty()){
            desc += item.getActor();
        }
        if(item.getDirector() != null && !item.getDirector().isEmpty()){
            desc += ", "+item.getDirector();
        }
        String isSeries = "N";
        if (item.isSeries()) {
            // The content id is series id in case the content is series.
            isSeries = "Y";
            desc = item.getCategoryName();
        }

        return new Object[]{
                id,
                title,
                desc,
                item.getPosterSourceUrl(Content.mPosterDefaultRect.width(), Content.mPosterDefaultRect.height()),
                TvContract.Channels.CONTENT_ITEM_TYPE,
                NO_LIVE_CONTENTS,
                0,
                0,
                "0",
                duration,
                PROGRESS_PERCENTAGE_HIDE,
                SEARCH_ACTION_VOD,
                id,
                isSeries
        };
    }

    public Object[] getCursorRow(SearchChannel item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "SearchChannel: " + item);
        }
        long duration = getDuration(item.getStartDate(), item.getEndDate());
        float progress = getProgressPercent(item.getStartDate(), item.getEndDate(), duration);
        return new Object[]{
                item.getProId(),
                item.getProName(),
                item.getChnlName(),
                item.getImgUrl(),
                TvContract.Programs.CONTENT_ITEM_TYPE,
                LIVE_CONTENTS,
                0,
                0,
                "0",
                duration,
                progress,
                SEARCH_ACTION_CHANNEL,
                item.getServiceId(),
                ""
        };
    }

    public Object[] getCursorRow(SearchYoutube item) {
        return new Object[]{
                item.getChannelid(),
                item.getTitle(),
                item.getDescription(),
                item.getThumbnailUrl(),
                TvContract.Channels.CONTENT_ITEM_TYPE,
                NO_LIVE_CONTENTS,
                0,
                0,
                "0",
                0,
                PROGRESS_PERCENTAGE_HIDE,
                SEARCH_ACTION_YOUTUBE,
                item.getChannelid(),
                ""
        };
    }

    public Object[] getCursorRow(SearchMonomax item) {
        return new Object[]{
                item.getId(),
                item.getTitle(),
                item.getDetail(),
                item.getPoster(),
                TvContract.Channels.CONTENT_ITEM_TYPE,
                NO_LIVE_CONTENTS,
                0,
                0,
                item.getYear(),
                0,
                PROGRESS_PERCENTAGE_HIDE,
                SEARCH_ACTION_MONOMAX,
                item.getId(),
                ""
        };
    }

    private long getDuration(String formattedTime){
        long duration = 0;
        try {
            if (Log.INCLUDE) {
                Log.d(TAG, "getDuration: formattedTime=" + formattedTime);
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = dateFormat.parse(formattedTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime( date );
            int hours = calendar.get(Calendar.HOUR_OF_DAY);
            int minutes = calendar.get(Calendar.MINUTE);
            int seconds = calendar.get(Calendar.SECOND);
            duration = TimeUtil.getHourTime(hours) + TimeUtil.getMinTime(minutes) + (seconds*TimeUtil.SEC);
            if(duration < 0){
                return 0;
            }
        } catch (Exception e) {
        }
        return duration;
    }

    private long getDuration(Date startDate, Date endDate){
        long duration = 0;
        try {
            if (Log.INCLUDE) {
                Log.d(TAG, "getDuration: startDate=" + startDate+", endDate=" + endDate);
            }
            duration = endDate.getTime() - startDate.getTime();
            if(duration < 0){
                return 0;
            }
        } catch (Exception e) {
        }
        return duration;
    }

    private float getProgressPercent(Date startDate, Date endDate, long duration){
        float progress = PROGRESS_PERCENTAGE_HIDE;
        try {
            long currTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
            if(startDate.getTime() <= currTime && currTime <= endDate.getTime()) {
                long progressTime = currTime-startDate.getTime();
                progress = (progressTime * 100) / duration;
                if (progress <= 0 || progress >= 100) {
                    progress = PROGRESS_PERCENTAGE_HIDE;
                }
            }
        }catch(Exception e){
        }
        return progress;
    }

    public void goToVodDetail(BaseActivity activity, String contentId, String seriesFlag) {
        if (Log.INCLUDE) {
            Log.d(TAG, "goToVodDetail: contentId=" + contentId+", seriesFlag="+seriesFlag);
        }

        TvOverlayManager overlayManager = null;
        if(activity instanceof JasSearchActivity) {
            overlayManager = ((JasSearchActivity) activity).getTvOverlayManager();
        }
        final TvOverlayManager mTvOverlayManager = overlayManager;

        boolean isSeries = false;
        if(seriesFlag != null && !seriesFlag.equals("")){
            if(seriesFlag.equals("Y")){
                isSeries = true;
            }
        }

        if(isSeries){
            try {
                // The content id is series id in case the content is series.
                VodDetailsDialogFragment dialogFragment = VodDetailsDialogFragment.newInstance(contentId, "", "", 1);
                showVodDetail(activity, mTvOverlayManager, dialogFragment);
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
            activity.finish();
            return;
        }

        AccountManager accountMgr = AccountManager.getInstance();

        ContentDataManager dataManager = new ContentDataManager();
        dataManager.getContentDetail(
                accountMgr.getLocalLanguage(),
                accountMgr.getSaId(),
                accountMgr.getProfileId(),
                contentId, "",
                new MbsDataProvider<String, ContentDetailInfo>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        Log.d(TAG, "onFailed: key=" + key);
                        activity.finish();
                    }

                    @Override
                    public void onSuccess(String id, ContentDetailInfo result) {
                        try {
                            Content content = result.getContent();
                            VodDetailsDialogFragment dialogFragment = VodDetailsDialogFragment.newInstance(content, 1);
                            showVodDetail(activity, mTvOverlayManager, dialogFragment);
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        activity.finish();
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        FragmentManager fragmentManager = activity.getSupportFragmentManager();
                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        fragmentManager.executePendingTransactions();
                        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                activity.finish();
                            }
                        });
                        dialogFragment.show(fragmentManager, ErrorDialogFragment.CLASS_NAME);
                    }
                });
    }

    private void showVodDetail(BaseActivity activity, TvOverlayManager mTvOverlayManager, VodDetailsDialogFragment dialogFragment){
        try {
            if (mTvOverlayManager != null) {
                mTvOverlayManager.showDialogFragment(dialogFragment);

                FragmentManager fragmentManager = mTvOverlayManager.getCurrentDialog().getFragmentManager();
                fragmentManager.executePendingTransactions();
                if(dialogFragment.getDialog() != null) {
                    dialogFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onDismiss");
                            }
                            activity.finish();
                        }
                    });
                }
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        activity.finish();
    }

    public boolean tuneToChannel(BaseActivity activity, String serviceId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "tuneToChannel: serviceId=" + serviceId);
        }
        try {
            TvSingletons mTvSingletons = TvSingletons.getSingletons(activity);
            ChannelDataManager channelDataManager = mTvSingletons.getChannelDataManager();
            Channel channel = channelDataManager.getChannelByServiceId(serviceId);
            if(channel != null) {
                PlaybackUtil.startLiveTvActivityWithChannel(activity, channel.getId());
                activity.finish();
                return true;
            }
        } catch (Exception e) {
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "tuneToChannel: there is no channel(serviceId=" + serviceId+")");
        }
        activity.finish();
        return false;
    }

    public void startApp(BaseActivity activity, String packageID) {
        if (Log.INCLUDE) {
            Log.d(TAG, "startApp: packageID=" + packageID);
        }
        Intent actionIntent = activity.getPackageManager().getLeanbackLaunchIntentForPackage(packageID);
        if (actionIntent == null) {
            Intent marketIntent = new Intent(Intent.ACTION_VIEW);
            marketIntent.setData(Uri.parse("market://details?id=" + packageID));
            actionIntent = marketIntent;
        }

        if (actionIntent != null) {
            actionIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(actionIntent);
        }
        activity.finish();
    }
}
