/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mc.kim on 22,05,2020
 */
public class Subtitle implements Parcelable {
    @Expose
    @SerializedName("filename")
    private String filename;
    @Expose
    @SerializedName("language")
    private List<String> language = new ArrayList<>();

    protected Subtitle(Parcel in) {
        filename = in.readString();
        in.readList(language, String.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(filename);
        dest.writeList(language);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Subtitle> CREATOR = new Creator<Subtitle>() {
        @Override
        public Subtitle createFromParcel(Parcel in) {
            return new Subtitle(in);
        }

        @Override
        public Subtitle[] newArray(int size) {
            return new Subtitle[size];
        }
    };

    @Override
    public String toString() {
        return "Subtitle{" +
                "filename='" + filename + '\'' +
                ", language='" + language + '\'' +
                '}';
    }

    public String getFilename() {
        return filename;
    }

    public List<String> getLanguage() {
        return language;
    }
}
