/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.coupon.CouponDataManager;
import kr.altimedia.launcher.jasmin.dm.coupon.object.BonusType;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProduct;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseRequest;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class CouponPaymentBillingFragment extends CouponPaymentBaseFragment {
    public static final String CLASS_NAME = CouponPaymentBillingFragment.class.getName();
    private final String TAG = CouponPaymentBillingFragment.class.getSimpleName();

    private static final String KEY_PURCHASE_INFO = "PURCHASE_INFO";

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private final UserDataManager mUserDataManager = new UserDataManager();
    private MbsDataTask checkPinTask;

    private PasswordView pincodeView;
    private TextView pincodeMessageView;
    private TextView cancel;

    private PurchaseInfo purchaseInfo;

    private CouponPaymentBillingFragment() {
    }

    public static CouponPaymentBillingFragment newInstance(PurchaseInfo purchaseInfo) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_PURCHASE_INFO, purchaseInfo);

        CouponPaymentBillingFragment fragment = new CouponPaymentBillingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_coupon_shop_payment_billing;
    }

    @Override
    protected void initView(View view) {
        initText(view);
        initPasswordView(view);
        initButton(view);

        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void initText(View view) {
        Bundle bundle = getArguments();
        purchaseInfo = bundle.getParcelable(KEY_PURCHASE_INFO);

        TextView couponPrice = view.findViewById(R.id.couponPrice);
        LinearLayout bonusValueLayer = view.findViewById(R.id.bonusValueLayer);
        TextView bonusValue = view.findViewById(R.id.bonusValue);
        TextView bonusUnit = view.findViewById(R.id.bonusUnit);
        TextView total = view.findViewById(R.id.total);
        pincodeMessageView = view.findViewById(R.id.pincodeMessage);

        CouponProduct couponProduct = purchaseInfo.getCouponProduct();
        couponPrice.setText(StringUtil.getFormattedNumber(couponProduct.getChargedAmount()));

        try {
            if(couponProduct.getBonusValue() > 0 && (couponProduct.getBonusType() == BonusType.DC_RATE || couponProduct.getBonusType() == BonusType.DC_AMOUNT)) {
                bonusValue.setText(StringUtil.getFormattedNumber(couponProduct.getBonusValue()));
                if (couponProduct.getBonusType() == BonusType.DC_RATE) {
                    bonusUnit.setText("%");
                } else if (couponProduct.getBonusType() == BonusType.DC_AMOUNT) {
                    bonusUnit.setText(R.string.thb);
                }
                bonusValueLayer.setVisibility(View.VISIBLE);
            }else{
                bonusValueLayer.setVisibility(View.GONE);
            }
        }catch (Exception e){
        }

        total.setText(StringUtil.getFormattedNumber(couponProduct.getPrice()));
    }

    private void initPasswordView(View view) {
        pincodeView = view.findViewById(R.id.pincode);
        pincodeView.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (isFull) {
                    checkPinCode(input);
                }
            }
        });
    }

    private void initButton(View view) {
        cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getOnPaymentListener() != null) {
                    getOnPaymentListener().onFragmentDismiss();
                }
            }
        });
    }

    private void checkPinCode(String password) {
        checkPinTask = mUserDataManager.checkAccountPinCode(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), password,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onFailed");
                        }

                        setPinError();
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onSuccess, result : " + result);
                        }

                        if (result) {
                            requestPurchaseCoupon();
                        } else {
                            setPinError();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onError, errorCode : " + errorCode + ", message : " + message);
                        }
                    }
                });
    }

    private void setPinError() {
        String text = getResources().getString(R.string.incorrect_pin_desc);
        pincodeMessageView.setText(text);
        pincodeView.clear();
        pincodeMessageView.setVisibility(View.VISIBLE);
    }

    private void requestPurchaseCoupon() {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestPurchaseCoupon");
        }

        CouponProduct couponProduct = purchaseInfo.getCouponProduct();
        PaymentType paymentType = purchaseInfo.getPaymentType();
        String paymentDetail = "";
        String creditCardType = "";
        String bankCode = "";

        CouponDataManager couponDataManager = new CouponDataManager();
        couponDataManager.requestPurchaseCoupon(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(),
                couponProduct.getId(), paymentType.getCode(), Integer.toString(couponProduct.getPrice()),
                paymentDetail, creditCardType, bankCode,
                new MbsDataProvider<String, CouponPurchaseRequest>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPurchaseCoupon: onFailed");
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentComplete(false);
                        }
                    }

                    @Override
                    public void onSuccess(String id, CouponPurchaseRequest result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPurchaseCoupon: onSuccess: result=" + result);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentComplete(true);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPurchaseCoupon: onError: errorCode=" + errorCode + ", message=" + message);
                        }
                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (checkPinTask != null) {
            checkPinTask.cancel(true);
        }

        pincodeView.setOnPasswordComplete(null);
        cancel.setOnClickListener(null);
    }
}

