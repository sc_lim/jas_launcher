/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.altimedia.util.Log;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 05,06,2020
 */
public class PerforatedRelativeLayout extends RelativeLayout {

    private ArrayList<PerforateRect> mPerforateRectList = new ArrayList<>();

    private final String TAG = PerforatedRelativeLayout.class.getSimpleName();
    private Drawable backgroundDrawable = null;

    public PerforatedRelativeLayout(Context context) {
        super(context);
        if (Log.INCLUDE) {
            Log.d(TAG, "PerforatedRelativeLayout");
        }
    }

    public PerforatedRelativeLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
        if (Log.INCLUDE) {
            Log.d(TAG, "PerforatedRelativeLayout");
        }
    }

    public PerforatedRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttribute(context, attrs, defStyleAttr);
        if (Log.INCLUDE) {
            Log.d(TAG, "PerforatedRelativeLayout");
        }
    }

    private void initAttribute(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.PerforatedRelativeLayout, defStyleAttr, 0);
        if (typedArray.hasValue(R.styleable.PerforatedRelativeLayout_background)) {
            int imageId = typedArray.getResourceId(R.styleable.PerforatedRelativeLayout_background, -1);
            backgroundDrawable = getResources().getDrawable(imageId);
        }
        typedArray.recycle();
    }

    private OnPerforatedCallback mOnPerforatedCallback;

    public void setOnPerforatedCallback(OnPerforatedCallback onPerforatedCallback) {
        this.mOnPerforatedCallback = onPerforatedCallback;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

    }

    private void makePerforate(View view, Canvas canvas) {

        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            final int count = viewGroup.getChildCount();
            for (int i = 0; i < count; i++) {
                final View child = viewGroup.getChildAt(i);
                makePerforate(child, canvas);
            }
        }
        if (view instanceof PerforatedView) {
            PerforatedView.PerforateType type = ((PerforatedView) view).getType();
            Drawable image = ((PerforatedView) view).getPerforatedImage();
            float radius = ((PerforatedView) view).getRadius();
            RectF performRect = getRect(view);
            mPerforateRectList.add(new PerforateRect(type, performRect, image, radius));
        }
    }

    private RectF getRect(View view) {
        int[] originalPos = new int[2];
        view.getLocationInWindow(originalPos);
        int left = originalPos[0];
        int top = originalPos[1];
        int right = originalPos[0] + view.getWidth();
        int bottom = originalPos[1] + view.getHeight();
        RectF rectF = new RectF(left, top, right, bottom);
        return rectF;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPerforateRectList.clear();
        makePerforate(getRootView(), canvas);
        requestPerforated(mPerforateRectList, canvas);
    }

    private void requestPerforated(ArrayList<PerforateRect> perforateRectList, Canvas canvas) {

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        Bitmap backgroundBitmap = ((BitmapDrawable) backgroundDrawable).getBitmap();
        Rect backgroundRect = new Rect(0, 0, 1920, 1080);

        for (PerforateRect performRect : perforateRectList) {
            RectF rectF = performRect.mRect;
            float radius = performRect.radius;

            PerforatedView.PerforateType type = performRect.type;

            switch (type) {
                case PIP:
                case PUNCH: {

                    Bitmap bitmap = Bitmap.createBitmap(backgroundBitmap.getWidth(), backgroundBitmap.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas punchCanvas = new Canvas(bitmap);
                    Paint punchPaint = new Paint();
                    punchCanvas.drawBitmap(backgroundBitmap, 0, 0, punchPaint);
                    punchPaint.setAntiAlias(true);
                    punchPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                    punchCanvas.drawRoundRect(rectF, radius, radius, punchPaint);
                    backgroundBitmap = bitmap;
                }
                break;
                case IMAGE: {
                    Bitmap combinedBitmap = Bitmap.createBitmap(backgroundBitmap.getWidth(), backgroundBitmap.getHeight(), backgroundBitmap.getConfig());
                    Canvas combinedCanvas = new Canvas(combinedBitmap);
                    Paint imagePaint = new Paint(Paint.FILTER_BITMAP_FLAG);
                    combinedCanvas.drawBitmap(backgroundBitmap, 0, 0, imagePaint);
                    Bitmap imageBitmap = ((BitmapDrawable) performRect.image).getBitmap();
                    Rect imageRect = new Rect(0, 0, imageBitmap.getWidth(), imageBitmap.getHeight());
                    combinedCanvas.drawBitmap(imageBitmap, imageRect, rectF, imagePaint);
                    backgroundBitmap = combinedBitmap;
                }
                break;
            }
            if (mOnPerforatedCallback != null) {
                mOnPerforatedCallback.onPerforated(rectF, type);
            }
        }
        canvas.drawBitmap(backgroundBitmap, backgroundRect, backgroundRect, paint);
    }


    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams;
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }

    @Override
    protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return new LayoutParams(p.width, p.height);
    }

    public static class LayoutParams extends RelativeLayout.LayoutParams {
        boolean performed = false;

        public boolean isPerformed() {
            return performed;
        }

        public LayoutParams(Context context, AttributeSet attrs) {
            super(context, attrs);
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Perforated);
            try {
                performed = a.getBoolean(R.styleable.Perforated_perforated, false);
            } finally {
                a.recycle();
            }
        }

        public LayoutParams(int w, int h) {
            super(w, h);
        }
    }

    public interface OnPerforatedCallback {
        void onPerforated(RectF rectF, PerforatedView.PerforateType type);
    }

    private static class PerforateRect {
        final PerforatedView.PerforateType type;
        final RectF mRect;
        final Drawable image;
        final float radius;


        public PerforateRect(PerforatedView.PerforateType type, RectF mRect, Drawable image, float radius) {
            this.type = type;
            this.mRect = mRect;
            this.image = image;
            this.radius = radius;
        }

        public PerforatedView.PerforateType getType() {
            return type;
        }

        public RectF getRect() {
            return mRect;
        }

        public Drawable getImage() {
            return image;
        }

        public float getRadius() {
            return radius;
        }

        @Override
        public String toString() {
            return "PerforateRect{" +
                    "type=" + type +
                    ", mRect=" + mRect +
                    ", image=" + image +
                    ", radius=" + radius +
                    '}';
        }
    }
}
