/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.system.manager.ExternalApplicationManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SideOptionButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SideOptionMenuPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SideOptionSectionPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SideOptionValuePresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object.SideOptionCategory;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.util.OptionCategoryManager;
import kr.altimedia.launcher.jasmin.ui.util.task.VodTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.row.SideOptionButtonRow;
import kr.altimedia.launcher.jasmin.ui.view.row.SideOptionMenuRow;
import kr.altimedia.launcher.jasmin.ui.view.row.SideOptionSectionRow;
import kr.altimedia.launcher.jasmin.ui.view.row.SideOptionValueRow;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

/**
 * Created by mc.kim on 18,02,2020
 */
public class SidePanelLayout extends LinearLayout {

    private final String TAG = SidePanelLayout.class.getSimpleName();

    public SidePanelLayout(Context context) {
        super(context);
        initView(context);
    }

    public SidePanelLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public SidePanelLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private HashMap<SidePanelOption, Integer> mOptionMinMap = new HashMap<>();

    private TextView panelTitle = null;
    private TextView titleView = null;
    private View historyLayer = null;
    private ImageView iconBI = null;
    private TextView liveChannelNumber = null;
    private VerticalGridView mMenuList;
    final ItemBridgeAdapter mBridgeAdapter = new ItemBridgeAdapter();
    private ArrayObjectAdapter itemRowAdapter = null;
    private SidePanelType mCurrentType = SidePanelType.VOD;
    private SidePanelOptionProvider mSidePanelOptionProvider;
    private SidePanelOptionCallback mSidePanelOptionCallback;
    private VodTaskManager mVodTaskManager;

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.layout_side_panel, this, true);
        panelTitle = findViewById(R.id.panelTitle);
        titleView = findViewById(R.id.optionTitle);
        historyLayer = findViewById(R.id.historyLayer);
        iconBI = findViewById(R.id.iconBI);
        liveChannelNumber = findViewById(R.id.liveChName);
        mMenuList = findViewById(R.id.menuList);
        int space = context.getResources().getDimensionPixelSize(R.dimen.side_panel_space_vertical);
        mMenuList.setVerticalSpacing(space);
        setEnabled(false);
        mVodTaskManager = new VodTaskManager(context);
        initMinMap();
    }

    private void initMinMap() {
        mOptionMinMap.put(SidePanelOption.SubTitle, 1);
        mOptionMinMap.put(SidePanelOption.Audio, 1);
    }


    public void setSidePanelType(Context context, SidePanelType type, SidePanelOptionProvider sidePanelOptionProvider) {
        setEnabled(true);
        mCurrentType = type;
        mSidePanelOptionProvider = sidePanelOptionProvider;
        if (sidePanelOptionProvider instanceof SidePanelOptionCallback) {
            mSidePanelOptionCallback = (SidePanelOptionCallback) sidePanelOptionProvider;
        }
        initialPresenterSelector();
    }

    public void renewalPanelData(Context context) {

        if (mSidePanelOptionCallback != null) {
            mSidePanelOptionCallback.reset();
        }

        OptionCategoryManager optionCategoryManager = new OptionCategoryManager(context, mCurrentType, mSidePanelOptionProvider);
        SideOptionAction mSideOptionAction = new SideOptionAction(context, optionCategoryManager, mSidePanelOptionProvider);
        ArrayList<SideOptionCategory> rootCategory = optionCategoryManager.getRootList();
        initialPanelData(rootCategory, mSideOptionAction);
    }

    public void clearPanelData() {
        if (itemRowAdapter != null) {
            itemRowAdapter.clear();
        }
    }

    private void initialPresenterSelector() {
        ClassPresenterSelector presenterSelector = new ClassPresenterSelector();
        presenterSelector.addClassPresenter(SideOptionSectionRow.class, new SideOptionSectionPresenter());
        presenterSelector.addClassPresenter(SideOptionMenuRow.class, new SideOptionMenuPresenter());
        presenterSelector.addClassPresenter(SideOptionButtonRow.class, new SideOptionButtonPresenter());
        presenterSelector.addClassPresenter(SideOptionValueRow.class, new SideOptionValuePresenter());
        mBridgeAdapter.setPresenter(presenterSelector);
        itemRowAdapter = new ArrayObjectAdapter();
    }

    private void initialPanelData(ArrayList<SideOptionCategory> categories, SideOptionAction mSideOptionAction) {
        initialPanelData(null, categories, mSideOptionAction);
    }

    private void initialPanelData(SideOptionCategory parent, ArrayList<SideOptionCategory> categories, SideOptionAction mSideOptionAction) {
        boolean isRoot = parent == null;

        if (mCurrentType == SidePanelType.VOD) {
            if (isRoot) {
                if (mSidePanelOptionProvider != null && mSidePanelOptionProvider.getItem() != null && mSidePanelOptionProvider.getItem() instanceof Content) {
                    Content content = (Content) mSidePanelOptionProvider.getItem();
                    titleView.setText("");
                    panelTitle.setText(content.getTitle());
                    panelTitle.setVisibility(View.VISIBLE);
                    historyLayer.setVisibility(View.INVISIBLE);
                    iconBI.setVisibility(View.GONE);
                } else {
                    titleView.setText("");
                    panelTitle.setVisibility(View.GONE);
                    historyLayer.setVisibility(View.INVISIBLE);
                    iconBI.setVisibility(View.VISIBLE);
                }
            } else {
                historyLayer.setVisibility(View.VISIBLE);
                panelTitle.setVisibility(View.GONE);
                iconBI.setVisibility(View.INVISIBLE);
                titleView.setText(parent.getOptionName());
            }
        } else {
            if (isRoot) {
                if (mSidePanelOptionProvider != null && mSidePanelOptionProvider.getItem() != null) {
                    Channel channel = (Channel) mSidePanelOptionProvider.getItem();
                    panelTitle.setText(StringUtil.getFormattedNumber(channel.getDisplayNumber(), 3));
                    panelTitle.setVisibility(View.VISIBLE);
                    int channelLogoId = -1;//get channel logo later
                    if (channelLogoId != -1) {
                        iconBI.setVisibility(View.VISIBLE);
                        liveChannelNumber.setVisibility(View.INVISIBLE);
                    } else {
                        iconBI.setVisibility(View.GONE);
                        liveChannelNumber.setText(channel.getDisplayName());
                        liveChannelNumber.setVisibility(View.VISIBLE);
                    }
                } else {
                    panelTitle.setVisibility(View.VISIBLE);
                    iconBI.setVisibility(View.VISIBLE);
                }

                titleView.setText("");
                historyLayer.setVisibility(View.INVISIBLE);
            } else {
                iconBI.setVisibility(View.GONE);
                liveChannelNumber.setVisibility(View.GONE);
                panelTitle.setVisibility(View.GONE);
                historyLayer.setVisibility(View.VISIBLE);
                iconBI.setVisibility(View.INVISIBLE);
                titleView.setText(parent.getOptionName());
            }
        }


        itemRowAdapter.clear();
        for (SideOptionCategory category : categories) {
            category.setParentsCategory(parent);
            if (category.getOptionType().isMenu()) {
                if (category.getOptionType() == SidePanelOption.SubTitle) {
                    if (mSidePanelOptionCallback.getSubtitleList().size() > mOptionMinMap.get(SidePanelOption.SubTitle)) {
                        itemRowAdapter.add(new SideOptionMenuRow(category, mSideOptionAction, mSidePanelOptionProvider));
                    }
                } else if (category.getOptionType() == SidePanelOption.Audio) {
                    if (mSidePanelOptionCallback.getAudioList().size() > mOptionMinMap.get(SidePanelOption.Audio)) {
                        itemRowAdapter.add(new SideOptionMenuRow(category, mSideOptionAction, mSidePanelOptionProvider));
                    }
                } else {
                    itemRowAdapter.add(new SideOptionMenuRow(category, mSideOptionAction, mSidePanelOptionProvider));
                }
            } else if (category.getOptionType().isButton()) {
                itemRowAdapter.add(new SideOptionButtonRow(category, mSideOptionAction, mSidePanelOptionProvider));
            } else if (category.getOptionType().isValue()) {
                if (category.getOptionType() == SidePanelOption.Fav) {
                    if (mSidePanelOptionCallback.isFavoriteMenuVisible()) {
                        itemRowAdapter.add(new SideOptionValueRow(category, mSideOptionAction, mSidePanelOptionProvider));
                    }
                } else {
                    itemRowAdapter.add(new SideOptionValueRow(category, mSideOptionAction, mSidePanelOptionProvider));
                }
            } else if (category.getOptionType().isSection()) {
                if (itemRowAdapter.size() > 0) {
                    itemRowAdapter.add(new SideOptionSectionRow(category));
                }
            }
        }
        mBridgeAdapter.setAdapter(itemRowAdapter);
        mMenuList.setAdapter(mBridgeAdapter);
    }

    private SidePanelActionListener mSidePanelActionListener = null;

    public void setSidePanelActionListener(SidePanelActionListener sidePanelActionListener) {
        this.mSidePanelActionListener = sidePanelActionListener;
    }

    public interface SidePanelActionListener {
        void openMenu();

        void closeMenu();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mVodTaskManager.cancelAllTask();
    }

    private class SideOptionAction implements SideOptionKeyListener {
        final String TAG = SideOptionAction.class.getSimpleName();
        final OptionCategoryManager optionCategoryManager;
        final SidePanelOptionProvider sidePanelOptionProvider;
        final Context context;

        public SideOptionAction(Context context,
                                OptionCategoryManager optionCategoryManager,
                                SidePanelOptionProvider sidePanelOptionProvider) {
            this.sidePanelOptionProvider = sidePanelOptionProvider;
            this.optionCategoryManager = optionCategoryManager;
            this.context = context;
        }

        public SidePanelOptionProvider getSidePanelOptionProvider() {
            return sidePanelOptionProvider;
        }

        @Override
        public void onItemSelected(SideOptionCategory category) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onItemSelected, category=" + category);
            }

            if (!category.isLeaf()) {
                ArrayList<SideOptionCategory> optionCategories = optionCategoryManager.getChildList(category);
                if (optionCategories == null || optionCategories.isEmpty()) {
                    return;
                }
                initialPanelData(category, optionCategories, this);
            } else {
                if (category.getOptionType().isButton()) {
                    onButtonAction(category);
                } else if (category.getOptionType().isValue()) {
                    onButtonAction(category);
                } else {
                    SideOptionCategory parentsCategory = category.getParentsCategory();
                    initialPanelData(optionCategoryManager.getRootList(), this);
                }
            }
        }

        private void onButtonAction(SideOptionCategory category) {
            SidePanelOption sidePanelOption = category.getOptionType();
            if (sidePanelOption == SidePanelOption.Setting) {
                Intent intent = new Intent(category.getAction());
                intent.setPackage("com.android.tv.settings");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.context.startActivity(intent);
            } else if (sidePanelOption == SidePanelOption.AppLink) {
                mSidePanelActionListener.closeMenu();
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                                                    @Override
                                                                    public void run() {

                                                                        ExternalApplicationManager.getInstance().launchApp(getContext(), category.getAction());
                                                                    }
                                                                }, 500
                );
            } else if (sidePanelOption == SidePanelOption.BroadCast) {
                Intent intent = new Intent(category.getAction());
                LocalBroadcastManager.getInstance(this.context).sendBroadcast(intent);
            } else if (sidePanelOption == SidePanelOption.Fav) {
                Object selectedObject = mSidePanelOptionProvider.getItem();
                if (selectedObject instanceof Channel) {
                    Channel selectedChannel = (Channel) selectedObject;
                    boolean isFavorite = selectedChannel.isFavoriteChannel();
                    if (mSidePanelOptionCallback != null) {
                        mSidePanelOptionCallback.requestFavorite(!isFavorite);
                        initialPanelData(optionCategoryManager.getRootList(), this);
                    }
                } else if (selectedObject instanceof Content) {
                    Content selectedContent = (Content) selectedObject;
                    setContentLike(selectedContent, this);
                }
            } else if (sidePanelOption == SidePanelOption.LanguageOption) {
                SideOptionCategory parentsCategory = category.getParentsCategory();
                if (parentsCategory.getOptionType() == SidePanelOption.SubTitle) {
                    if (mSidePanelOptionCallback != null) {
                        mSidePanelOptionCallback.setSubtitle(category.getOptionName());
                    }
                } else if (parentsCategory.getOptionType() == SidePanelOption.Audio) {
                    if (mSidePanelOptionCallback != null) {
                        mSidePanelOptionCallback.setAudio(category.getOptionName());
                    }
                }
                mBridgeAdapter.notifyDataSetChanged();
//                mSidePanelActionListener.closeMenu();
            } else if (sidePanelOption == SidePanelOption.LanguageStyle) {
                if (mSidePanelOptionCallback != null) {
                    mSidePanelOptionCallback.setSubtitleStyle(category.getOptionName());
                }
                mBridgeAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onBackPressed(SideOptionCategory category) {
            if (optionCategoryManager.isRootCategory(category)) {
                mSidePanelActionListener.closeMenu();
            } else {
                initialPanelData(optionCategoryManager.getRootList(), this);
            }
        }

        private void setContentLike(Content mContent, SideOptionAction mSideOptionAction) {
            if (Log.INCLUDE) {
                Log.d(TAG, "setContentLike, isLike : " + mContent.isLike());
            }

            if (!(context instanceof VideoPlaybackActivity)) {
                return;
            }

            VideoPlaybackActivity mVideoPlaybackActivity = (VideoPlaybackActivity) context;
            FragmentManager mFragmentManager = mVideoPlaybackActivity.getSupportFragmentManager();

            mVodTaskManager.setOnCompleteContentFavoriteListener(
                    new VodTaskManager.OnCompleteContentFavoriteListener() {
                        @Override
                        public void needLoading(boolean isLoading) {
                        }

                        @Override
                        public void onSuccessAddFavorite(Object id, Object result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onCompleteAddFavorite");
                            }

                            initialPanelData(optionCategoryManager.getRootList(), mSideOptionAction);

                            mContent.setLike(true);
                            JasminToast.makeToast(getContext(), R.string.my_list_add);
                        }

                        @Override
                        public void onSuccessRemoveFavorite(Object id, Object result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onCompleteRemoveFavorite");
                            }

                            initialPanelData(optionCategoryManager.getRootList(), mSideOptionAction);

                            mContent.setLike(false);
                            JasminToast.makeToast(getContext(), R.string.my_list_remove);
                        }

                        @Override
                        public void onFailFavorite(int key) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onFailFavorite");
                            }
                        }

                        @Override
                        public void onError(Object id, String errorCode, String message) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onClickContentLike, onError, errorCode : " + errorCode + ", message : " + message);
                            }

                            ErrorDialogFragment dialogFragment =
                                    ErrorDialogFragment.newInstance(TAG, errorCode, message);
                            dialogFragment.show(mFragmentManager, ErrorDialogFragment.CLASS_NAME);
                        }
                    });

            String contentId = mContent.isSeries() ? mContent.getSeriesAssetId() : mContent.getContentGroupId();
            VodTaskManager.VodLikeType vodLikeType = mContent.isSeries() ? VodTaskManager.VodLikeType.Series : VodTaskManager.VodLikeType.VOD;
            mVodTaskManager.onClickLikeContent(mContent.isLike(), contentId, vodLikeType);
        }
    }

    public interface SideOptionKeyListener {
        void onItemSelected(SideOptionCategory category);

        void onBackPressed(SideOptionCategory category);
    }

}
