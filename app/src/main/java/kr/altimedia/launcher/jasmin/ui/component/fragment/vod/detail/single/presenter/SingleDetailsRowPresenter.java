/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.single.presenter;

import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.core.content.ContextCompat;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.user.object.Membership;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.managerProvider;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonManager2;
import kr.altimedia.launcher.jasmin.ui.util.task.UserTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewKeyListener;
import kr.altimedia.launcher.jasmin.ui.view.common.VodTrailerPosterView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.VodOverviewRow;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class SingleDetailsRowPresenter extends RowPresenter {
    private static final String TAG = SingleDetailsRowPresenter.class.getSimpleName();

    private View.OnClickListener onClickPosterListener;
    private managerProvider managerProvider;

    public SingleDetailsRowPresenter() {
        super();
    }

    public void setManagerProvider(managerProvider managerProvider) {
        this.managerProvider = managerProvider;
    }

    private int getLayoutResourceId() {
        return R.layout.presenter_single_detail;
    }

    @Override
    protected RowPresenter.ViewHolder createRowViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(getLayoutResourceId(), parent, false);
        return new VodDetailsRowPresenterViewHolder(v);
    }

    @Override
    protected void initializeRowViewHolder(ViewHolder vh) {
        super.initializeRowViewHolder(vh);
        VodDetailsRowPresenterViewHolder viewHolder = (VodDetailsRowPresenterViewHolder) vh;
        viewHolder.mActionBridgeAdapter = new ActionsItemBridgeAdapter(viewHolder);
    }

    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);

        VodOverviewRow row = (VodOverviewRow) item;
        VodDetailsRowPresenterViewHolder viewHolder = (VodDetailsRowPresenterViewHolder) holder;
        Content content = (Content) row.getItem();

        viewHolder.setData(content, onClickPosterListener);
        viewHolder.setManagerProvider(managerProvider);
        viewHolder.onBind();
    }

    @Override
    protected void onUnbindRowViewHolder(ViewHolder vh) {
        VodDetailsRowPresenterViewHolder viewHolder = (VodDetailsRowPresenterViewHolder) vh;
        viewHolder.setOnItemViewClickedListener(null);
        viewHolder.setBaseOnItemKeyListener(null);
        viewHolder.onUnbind();

        super.onUnbindRowViewHolder(vh);
    }

    public void setOnPosterClickListener(View.OnClickListener onPosterClickListener) {
        this.onClickPosterListener = onPosterClickListener;
    }

    public static class VodDetailsRowPresenterViewHolder extends RowPresenter.ViewHolder
            implements UserTaskManager.OnUpdateUserDiscountListener {
        private VodTrailerPosterView poster = null;
        private TextView title = null;
        private TextView releaseDate = null;
        private TextView genre = null;
        private TextView duration = null;
        private TextView director = null;
        private TextView cast = null;
        private TextView description = null;

        private TextView membershipView = null;
        private TextView couponPoint = null;
        private TextView couponDiscount = null;

        private ItemBridgeAdapter mActionBridgeAdapter;
        private HorizontalGridView mActionsRow;

        private VodOverviewRow.Listener mRowListener = createRowListener();
        private managerProvider managerProvider;

        private final UserTaskManager userTaskManager = UserTaskManager.getInstance();

        public VodDetailsRowPresenterViewHolder(View rootView) {
            super(rootView);

            initView(rootView);

            userTaskManager.addUserDiscountListener(this);
        }

        private void initView(View view) {
            poster = view.findViewById(R.id.poster);
            title = view.findViewById(R.id.title);
            releaseDate = view.findViewById(R.id.releaseDate);
            genre = view.findViewById(R.id.genre);
            duration = view.findViewById(R.id.duration);
            director = view.findViewById(R.id.director);
            cast = view.findViewById(R.id.cast);
            description = view.findViewById(R.id.description);

            membershipView = view.findViewById(R.id.membership);
            couponPoint = view.findViewById(R.id.coupon_point);
            couponDiscount = view.findViewById(R.id.coupon_discount);

            mActionsRow = view.findViewById(R.id.details_overview_actions);
            int gap = (int) view.getResources().getDimension(R.dimen.vod_detail_button_gap);
            mActionsRow.setHorizontalSpacing(gap);
        }

        public void setData(Content content, View.OnClickListener onClickPosterListener) {
            title.setText(content.getTitle());
            releaseDate.setText(content.getReleaseDate());
            genre.setText(content.getGenre().toString()
                    .replace("[", "").replace("]", ""));
            duration.setText(String.valueOf(content.getRunTime() / TimeUtil.MIN));
            director.setText(content.getDirector());
            cast.setText(content.getActor());
            description.setText(content.getSynopsis());

            initPoster(content, onClickPosterListener);
            setMembershipView();
            setCouponView();

            setBaseOnItemKeyListener(new BaseOnItemViewKeyListener() {
                @Override
                public boolean onItemKey(int keyCode, Presenter.ViewHolder var1, Object var2, Presenter.ViewHolder var3, Object var4) {
                    View focusedView = mActionsRow.getFocusedChild();
                    int index = mActionsRow.getChildPosition(focusedView);

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_LEFT:
                            boolean hasTrailer = poster.getTrailerVisibility() == View.VISIBLE;
                            if (index == 0 && hasTrailer) {
                                poster.requestFocus();
                                return true;
                            }
                            break;
                    }
                    return false;
                }
            });
        }

        public void setManagerProvider(managerProvider managerProvider) {
            this.managerProvider = managerProvider;
        }

        private void initPoster(Content content, View.OnClickListener onClickPosterListener) {
            int padding = (int) view.getResources().getDimension(R.dimen.focus_padding);
            poster.setPadding(padding, padding, padding, padding);
            poster.setDefaultImage(R.drawable.poster_default);
            poster.setBlockImage(R.drawable.poster_block);
            poster.setPoster(content.getPosterSourceUrl(Content.mPosterPurchaseRect.width(),
                    Content.mPosterPurchaseRect.height()));

            ArrayList<StreamInfo> streams = (ArrayList<StreamInfo>) content.getTrailerStreamInfo();
            boolean isWatchable = ProductButtonManager2.isWatchable(content.getProductList());
            if (Log.INCLUDE) {
                Log.d(TAG, "initPoster, isWatchable : " + isWatchable + ", streams : " + (streams != null ? "streamSize  : " + streams.size() : "null"));
            }
            boolean isVisible = isWatchable || (streams != null && streams.size() > 0);
            int visibility = isVisible ? View.VISIBLE : View.INVISIBLE;
            poster.setTrailerVisibility(visibility);
            poster.setFocusable(isVisible);
            poster.setFocusableInTouchMode(isVisible);

            int textResource = content.isWatchable() ? R.string.vod_watch : R.string.video;
            String textValue = view.getContext().getString(textResource);
            poster.setTrailerText(textValue);

            Drawable bottomShadow = ContextCompat.getDrawable(poster.getContext(), R.drawable.detail_poster_dim);
            poster.setBottomShadow(bottomShadow);

            poster.setOnClickListener(onClickPosterListener);
        }

        private void setMembershipView() {
            Membership membership = userTaskManager.getMembership();
            if (membership != null) {
                int membershipPoint = membership.getMembershipPoint();
                membershipView.setText(StringUtil.getFormattedNumber(membershipPoint));
            }
        }

        private void setCouponView() {
            int totalPointCoupon = userTaskManager.getPointCoupon();
            int size = userTaskManager.getDiscountCouponSize();

            couponPoint.setText(StringUtil.getFormattedNumber(totalPointCoupon));
            couponDiscount.setText(StringUtil.getFormattedNumber(size));
        }

        protected VodOverviewRow.Listener createRowListener() {
            return new VodOverviewRowListener();
        }

        void onBind() {
            VodOverviewRow row = (VodOverviewRow) getRow();
            bindActions(row.getActionsAdapter());
            row.addListener(mRowListener);
        }

        void onUnbind() {
            VodOverviewRow row = (VodOverviewRow) getRow();
            row.removeListener(mRowListener);
            poster.setOnClickListener(null);
            mActionsRow.setAdapter(null);
            mRowListener = null;
            mActionBridgeAdapter.clear();
            mActionBridgeAdapter = null;

            userTaskManager.removeUserDiscountListener(this);
            managerProvider = null;
        }

        void bindActions(ObjectAdapter adapter) {
            mActionBridgeAdapter.setAdapter(adapter);
            mActionsRow.setAdapter(mActionBridgeAdapter);
            mActionsRow.requestFocus();
        }

        @Override
        public void notifyUserDiscountChange(int pointCouponSize, int discountCouponSize, ArrayList<DiscountCoupon> discountCoupons, Membership membership) {
            setCouponView();
            setMembershipView();
        }

        @Override
        public void notifyFail(int key) {
        }

        @Override
        public void notifyError(String errorCode, String message) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + message);
            }

            ErrorDialogFragment dialogFragment =
                    ErrorDialogFragment.newInstance(TAG, errorCode, message);
            dialogFragment.show(managerProvider.getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
        }

        public class VodOverviewRowListener extends VodOverviewRow.Listener {
            @Override
            public void onImageDrawableChanged(VodOverviewRow row) {
            }

            @Override
            public void onItemChanged(VodOverviewRow row) {
            }

            @Override
            public void onActionsAdapterChanged(VodOverviewRow row) {
                bindActions(row.getActionsAdapter());
            }
        }
    }

    private class ActionsItemBridgeAdapter extends ItemBridgeAdapter {
        VodDetailsRowPresenterViewHolder mViewHolder;

        ActionsItemBridgeAdapter(VodDetailsRowPresenterViewHolder viewHolder) {
            mViewHolder = viewHolder;
        }

        @Override
        public void onBind(final ItemBridgeAdapter.ViewHolder ibvh) {
            if (mViewHolder.getOnItemViewClickedListener() != null) {
                Presenter presenter = ibvh.getPresenter();
                presenter.setOnClickListener(
                        ibvh.getViewHolder(), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mViewHolder.getOnItemViewClickedListener() != null) {
                                    mViewHolder.getOnItemViewClickedListener().onItemClicked(
                                            ibvh.getViewHolder(), ibvh.getItem(),
                                            mViewHolder, mViewHolder.getRow());
                                }
                            }
                        });
            }

            if (mViewHolder.getBaseOnItemKeyListener() != null) {
                ibvh.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() != KeyEvent.ACTION_DOWN) {
                            return false;
                        }

                        ItemBridgeAdapter.ViewHolder ibh = (ItemBridgeAdapter.ViewHolder) mViewHolder.mActionsRow.getChildViewHolder(ibvh.itemView);
                        return mViewHolder.getBaseOnItemKeyListener().onItemKey(keyCode, ibvh.mHolder, ibh.mItem, mViewHolder, mViewHolder.getRow());
                    }
                });
            }
        }

        @Override
        public void onUnbind(final ItemBridgeAdapter.ViewHolder ibvh) {
            if (mViewHolder.getOnItemViewClickedListener() != null) {
                ibvh.getPresenter().setOnClickListener(ibvh.getViewHolder(), null);
            }

            ibvh.mHolder.view.setOnKeyListener(null);
        }
    }
}