/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.obj;

import android.os.Parcel;
import android.os.Parcelable;

public class Payment implements Parcelable {
    public static final Creator<Payment> CREATOR = new Creator<Payment>() {
        @Override
        public Payment createFromParcel(Parcel in) {
            return new Payment(in);
        }

        @Override
        public Payment[] newArray(int size) {
            return new Payment[size];
        }
    };

    private String cpnId;
    private int price;
    private String method;
    private String paymentDetail;
    private String creditCardType;
    private String bankCode;

    public Payment(String cpnId, int price, String method, String paymentDetail) {
        this.cpnId = cpnId;
        this.price = price;
        this.method = method;
        this.paymentDetail = paymentDetail;
    }

    public void setPaymentDetail(String paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    protected Payment(Parcel in) {
        cpnId = in.readString();
        price = in.readInt();
        method = in.readString();
        paymentDetail = in.readString();
        creditCardType = in.readString();
        bankCode = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cpnId);
        dest.writeValue(price);
        dest.writeValue(method);
        dest.writeValue(paymentDetail);
        dest.writeValue(creditCardType);
        dest.writeValue(bankCode);
    }

    @Override
    public String toString() {
        return "Payment{" +
                "cpnId='" + cpnId + '\'' +
                ", price=" + price +
                ", method='" + method + '\'' +
                ", paymentDetail='" + paymentDetail + '\'' +
                ", creditCardType='" + creditCardType + '\'' +
                ", bankCode='" + bankCode + '\'' +
                '}';
    }

    public String getCpnId() {
        return cpnId;
    }

    public int getPrice() {
        return price;
    }

    public String getMethod() {
        return method;
    }

    public String getPaymentDetail() {
        return paymentDetail;
    }

    public String getCreditCardType() {
        return creditCardType;
    }

    public String getBankCode() {
        return bankCode;
    }
}
