/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.page;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;

public class PromotionVideoFragment extends Fragment {
    private final String TAG = PromotionVideoFragment.class.getSimpleName();
    private static final String KEY_BANNER = "banner";


    public PromotionVideoFragment() {
    }

    public static PromotionVideoFragment newInstance(Banner banner) {
        PromotionVideoFragment fragment = new PromotionVideoFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_BANNER, banner);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreateView");
        }

        return inflater.inflate(R.layout.item_promotion_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated");
        }
        ((ViewGroup) view).setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        Bundle bundle = getArguments();
        Banner banner = bundle.getParcelable(KEY_BANNER);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Log.INCLUDE) {
            Log.d(TAG, "onCreate");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Log.INCLUDE) {
            Log.d(TAG, "onResume");
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach");
        }

    }


    @Override
    public void onDetach() {
        super.onDetach();

        if (Log.INCLUDE) {
            Log.d(TAG, "onDetach");
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        if (Log.INCLUDE) {
            Log.d(TAG, "onStart");
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (Log.INCLUDE) {
            Log.d(TAG, "onStop");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Log.INCLUDE) {
            Log.d(TAG, "onPause");
        }
    }

}