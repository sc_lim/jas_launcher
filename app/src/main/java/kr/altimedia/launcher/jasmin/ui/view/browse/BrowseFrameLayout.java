/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;

import com.altimedia.util.Log;

public class BrowseFrameLayout extends FrameLayout {
    private BrowseFrameLayout.OnFocusSearchListener mListener;
    private BrowseFrameLayout.OnChildFocusListener mOnChildFocusListener;
    private OnKeyListener mOnDispatchKeyListener;

    public BrowseFrameLayout(Context context) {
        this(context, null, 0);
    }

    public BrowseFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BrowseFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setOnFocusSearchListener(BrowseFrameLayout.OnFocusSearchListener listener) {
        this.mListener = listener;
    }

    public BrowseFrameLayout.OnFocusSearchListener getOnFocusSearchListener() {
        return this.mListener;
    }

    public void setOnChildFocusListener(BrowseFrameLayout.OnChildFocusListener listener) {
        this.mOnChildFocusListener = listener;
    }

    public BrowseFrameLayout.OnChildFocusListener getOnChildFocusListener() {
        return this.mOnChildFocusListener;
    }

    protected boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
        return this.mOnChildFocusListener != null && this.mOnChildFocusListener.onRequestFocusInDescendants(direction, previouslyFocusedRect) || super.onRequestFocusInDescendants(direction, previouslyFocusedRect);
    }

    public View focusSearch(View focused, int direction) {
        if (this.mListener != null) {
            View view = this.mListener.onFocusSearch(focused, direction);
            if (view != null) {
                return view;
            }
        }

        return super.focusSearch(focused, direction);
    }

    private final String TAG = BrowseFrameLayout.class.getSimpleName();

    public void requestChildFocus(View child, View focused) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestChildFocus : " + child + ", focused : " + focused);
        }
        if (this.mOnChildFocusListener != null) {
            this.mOnChildFocusListener.onRequestChildFocus(child, focused);
        }

        super.requestChildFocus(child, focused);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        boolean consumed = super.dispatchKeyEvent(event);
        return this.mOnDispatchKeyListener != null && !consumed ? this.mOnDispatchKeyListener.onKey(this.getRootView(), event.getKeyCode(), event) : consumed;
    }

    public void setOnDispatchKeyListener(OnKeyListener listener) {
        this.mOnDispatchKeyListener = listener;
    }

    public interface OnChildFocusListener {
        boolean onRequestFocusInDescendants(int var1, Rect var2);

        void onRequestChildFocus(View child, View focused);
    }

    public interface OnFocusSearchListener {
        View onFocusSearch(View var1, int var2);
    }
}

