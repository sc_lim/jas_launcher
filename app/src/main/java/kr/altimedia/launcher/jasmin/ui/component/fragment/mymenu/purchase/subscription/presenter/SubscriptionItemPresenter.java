/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscribedContent;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class SubscriptionItemPresenter extends Presenter {
    private static final String TAG = SubscriptionItemPresenter.class.getSimpleName();
    private OnKeyListener onKeyListener;
    private final int MONTHLY_SUBSCRIPTION = 0;

    public SubscriptionItemPresenter(OnKeyListener onKeyListener) {
        this.onKeyListener = onKeyListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_mymenu_subscription_list, parent, false);
        SubscriptionItemViewHolder viewHolder = new SubscriptionItemViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (item == null) {
            viewHolder.view.setVisibility(View.INVISIBLE);
            return;
        }

        SubscriptionItemViewHolder vh = (SubscriptionItemViewHolder) viewHolder;
        SubscribedContent subscriptionItem = (SubscribedContent) item;
        vh.setData(subscriptionItem);

        viewHolder.view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }

    public OnKeyListener getOnKeyListener() {
        return onKeyListener;
    }

    public class SubscriptionItemViewHolder extends ViewHolder {
        TextView date;
        TextView title;
        TextView price;
        TextView priceUnit;
        TextView period;
        TextView periodUnit;

        public SubscriptionItemViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            date = view.findViewById(R.id.pkgDate);
            title = view.findViewById(R.id.pkgTitle);
            price = view.findViewById(R.id.pkgPrice);
            priceUnit = view.findViewById(R.id.pkgPriceUnit);
            period = view.findViewById(R.id.pkgPeriod);
            periodUnit = view.findViewById(R.id.pkgPeriodUnit);
        }

        public void setData(SubscribedContent subscriptionItem) {
            if(subscriptionItem == null) {
                return;
            }

            String dateStr = "";
            try {
                dateStr = TimeUtil.getModifiedDate(subscriptionItem.getPurchaseDateTime().getTime());
            }catch (Exception e){
            }
            date.setText(dateStr);
            if(subscriptionItem.getProductName() != null) {
                title.setText(subscriptionItem.getProductName());
            }
            String priceStr = "";
            try {
                priceStr = StringUtil.getFormattedNumber(Long.parseLong(subscriptionItem.getPrice()));
            }catch (Exception e){
            }
            price.setText(priceStr);
            if(subscriptionItem.getCurrency() != null) {
                priceUnit.setText(subscriptionItem.getCurrency());
            }

            String periodValue = "";
            try {
                periodUnit.setVisibility(View.GONE);
                if(subscriptionItem.getPeriod() >= 0) {
                    if (subscriptionItem.getPeriod() == MONTHLY_SUBSCRIPTION) {
                        periodValue = view.getResources().getString(R.string.monthly);
                    } else {
                        periodValue = Integer.toString(subscriptionItem.getPeriod());
                        String periodUnitValue = view.getResources().getString(R.string.months);
                        if (subscriptionItem.getPeriod() == 1) {
                            periodUnitValue = view.getResources().getString(R.string.month);
                        }
                        periodUnit.setText(periodUnitValue);
                        periodUnit.setVisibility(View.VISIBLE);
                    }
                }
            } catch (Exception e) {
            }
            period.setText(periodValue);
        }
    }

    public interface OnKeyListener {
        boolean onKey(int keyCode, int index, Object item);
    }
}
