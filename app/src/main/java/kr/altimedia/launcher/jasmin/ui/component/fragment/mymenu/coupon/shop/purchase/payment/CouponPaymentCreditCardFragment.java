/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.HandleViewPager;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;

public class CouponPaymentCreditCardFragment extends CouponPaymentBaseFragment implements ViewPager.OnPageChangeListener {
    public static final String CLASS_NAME = CouponPaymentCreditCardFragment.class.getName();
    private String TAG = CouponPaymentCreditCardFragment.class.getSimpleName();

    private static final String KEY_CREDIT_CARD_LIST = "CREDIT_CARD_LIST";
    public static final String KEY_OTHER_CARD_LIST = "OTHER_CARD_LIST";

    public static final int MAIN_PAGE_ITEMS = 6;

    public static final int LIST_NUMBER_COLUMNS = 3;
    public static final int LIST_NUMBER_ROWS = 3;
    public static final int LIST_PAGE_ITEMS = LIST_NUMBER_COLUMNS * LIST_NUMBER_ROWS;

    private TextView cancelButton;
    private HandleViewPager viewPager;
    private PagerAdapter mPagerAdapter;
    private ImageIndicator leftIndicator;
    private ImageIndicator rightIndicator;

    private int totalPage = 1;
    private ArrayList<CreditCard> otherCardList;
    private ArrayList<CreditCard> creditCardList;

    private OnClickCreditCardListener mOnClickCreditCardListener;

    public static CouponPaymentCreditCardFragment newInstance(ArrayList<CreditCard> otherList, ArrayList<CreditCard> optionList) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_OTHER_CARD_LIST, otherList);
        args.putParcelableArrayList(KEY_CREDIT_CARD_LIST, optionList);

        CouponPaymentCreditCardFragment fragment = new CouponPaymentCreditCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnClickCreditCardListener(OnClickCreditCardListener mOnClickCreditCardListener) {
        this.mOnClickCreditCardListener = mOnClickCreditCardListener;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_coupon_shop_payment_credit_card;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        otherCardList = getArguments().getParcelableArrayList(KEY_OTHER_CARD_LIST);
        creditCardList = getArguments().getParcelableArrayList(KEY_CREDIT_CARD_LIST);

        calcPage(creditCardList.size());
        CreditCardPageBaseFragment.setSelectedPosition(0);
        initButton(view);
        initViewPage(view);
    }

    private void calcPage(int size) {
        boolean hasNextPage = size - 1 > MAIN_PAGE_ITEMS;
        if (hasNextPage) {
            size -= MAIN_PAGE_ITEMS;

            float mod = (float) (size % LIST_PAGE_ITEMS);
            totalPage = mod > 0 ? size / LIST_PAGE_ITEMS + 1 : creditCardList.size() / LIST_PAGE_ITEMS;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "calcPage, size : " + size + ", totalPage : " + totalPage);
        }
    }

    private void initButton(View view) {
        cancelButton = view.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SafeDismissDialogFragment) getParentFragment()).dismiss();
            }
        });
    }

    private void initViewPage(View view) {
        viewPager = view.findViewById(R.id.viewPager);
        leftIndicator = view.findViewById(R.id.leftIndicator);
        rightIndicator = view.findViewById(R.id.rightIndicator);

        mPagerAdapter = new PagerAdapter(
                getFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, totalPage, otherCardList, creditCardList);
        viewPager.setAdapter(mPagerAdapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setOnKeyEventListener(new HandleViewPager.OnKeyEventListener() {
            @Override
            public boolean onKeyEvent(KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                CreditCardPageBaseFragment currentFragment = (CreditCardPageBaseFragment) getFragment(viewPager.getId(), mPagerAdapter.getItemId(viewPager.getCurrentItem()));
                int cardGridIndex = currentFragment.getFocusedPosition();

                if (Log.INCLUDE) {
                    Log.d(TAG, "onKeyEvent, cardGridIndex : " + cardGridIndex);
                }

                int keyCode = event.getKeyCode();
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        if (mOnClickCreditCardListener != null) {
                            CreditCard creditCard = (CreditCard) currentFragment.getItem(cardGridIndex);
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onClick, creditCard : " + creditCard);
                            }

                            mOnClickCreditCardListener.onClickCreditCard(creditCard);
                            return true;
                        }

                        return false;
                    case KeyEvent.KEYCODE_DPAD_DOWN:
                        if (cardGridIndex == -1) {
                            CreditCardMainPagerFragment fragment = (CreditCardMainPagerFragment) currentFragment;
                            if (fragment.getOtherCardFocusedPosition() == (otherCardList.size() - 1)) {
                                cancelButton.requestFocus();
                                return true;
                            }
                        }

                        return false;
                    case KeyEvent.KEYCODE_DPAD_LEFT:
                        if (cardGridIndex == -1) {
                            return false;
                        }

                        if (cardGridIndex < LIST_NUMBER_COLUMNS) {
                            updateNextPageFocus(false, cardGridIndex);
                        }

                        return false;
                    case KeyEvent.KEYCODE_DPAD_RIGHT:
                        if (cardGridIndex == -1) {
                            if (creditCardList.size() == 0) {
                                return true;
                            }

                            return false;
                        }

                        if (viewPager.getCurrentItem() == (totalPage - 1)) {
                            int lastIndex = currentFragment.getSize() - 1;
                            if (!currentFragment.isLastColumn(cardGridIndex) && cardGridIndex + LIST_NUMBER_ROWS > lastIndex) {
                                currentFragment.requestPosition(lastIndex);
                                return true;
                            }

                            return false;
                        }

                        if (currentFragment instanceof CreditCardMainPagerFragment) {
                            cardGridIndex += (LIST_PAGE_ITEMS - MAIN_PAGE_ITEMS);
                        }

                        if (cardGridIndex >= (LIST_NUMBER_COLUMNS * 2)) {
                            updateNextPageFocus(true, cardGridIndex);
                        }

                        return false;
                }

                return false;
            }
        });
    }

    private void updateNextPageFocus(boolean moveRight, int position) {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateNextPageFocus, position : " + position);
        }

        if (position < 0 || position >= LIST_PAGE_ITEMS) {
            return;
        }

        int currentPage = viewPager.getCurrentItem();
        CreditCardPageBaseFragment nextFragment = (CreditCardPageBaseFragment) getFragment(viewPager.getId(), mPagerAdapter.getItemId(moveRight ? currentPage + 1 : currentPage - 1));

        int moveFocusIndex = moveRight ? position - (LIST_NUMBER_COLUMNS * 2) : position + (LIST_NUMBER_COLUMNS * 2);

        //for focus sync
        CreditCardPageBaseFragment.setSelectedPosition(moveFocusIndex);
        if (nextFragment != null) {
            nextFragment.requestPosition(moveFocusIndex);
        }
    }

    private Fragment getFragment(int viewId, long id) {
        String name = "android:switcher:" + viewId + ":" + id;
        return getFragmentManager().findFragmentByTag(name);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        leftIndicator.setArrowVisible(position == 0 ? View.INVISIBLE : View.VISIBLE);
        rightIndicator.setArrowVisible(position == (totalPage - 1) ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private static class PagerAdapter extends FragmentPagerAdapter {
        private int totalPage;
        private ArrayList<CreditCard> otherCardList;
        private ArrayList<CreditCard> creditCardList;

        public PagerAdapter(@NonNull FragmentManager fm, int behavior, int totalPage,
                            ArrayList<CreditCard> otherCardList, ArrayList<CreditCard> creditCardList) {
            super(fm, behavior);
            this.totalPage = totalPage;
            this.otherCardList = otherCardList;
            this.creditCardList = creditCardList;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            ArrayList<CreditCard> pageList = new ArrayList<>();

            if (position == 0) {
                for (int i = 0; i < MAIN_PAGE_ITEMS && i < creditCardList.size(); i++) {
                    pageList.add(creditCardList.get(i));
                }

                return CreditCardMainPagerFragment.newInstance(otherCardList, pageList);
            }

            int start = MAIN_PAGE_ITEMS + ((position - 1) * 9);
            int end = start + LIST_PAGE_ITEMS;
            for (int i = start; i < end && i < creditCardList.size(); i++) {
                pageList.add(creditCardList.get(i));
            }

            return CreditCardListPagerFragmentCreditCard.newInstance(pageList);
        }

        @Override
        public int getCount() {
            return totalPage;
        }
    }

    public interface OnClickCreditCardListener {
        void onClickCreditCard(CreditCard creditCard);
    }
}
