/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv;

import android.content.Context;
import android.media.tv.TvTrackInfo;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOptionCallback;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOptionProvider;

public class LiveSidePanelOptionProvider
        extends SidePanelOptionProvider<Channel>
        implements SidePanelOptionCallback {

    private final String TAG = LiveSidePanelOptionProvider.class.getSimpleName();
    private static final Locale LOCALE_THAI = new Locale("th");

    private Context context;
    private JasTvView jasTvView;
    private Map<String, String> audioTracks;
    private Map<String, String> subtitleTracks;
    private String selectedAudioTrackId;
    private String selectedSubtitleTrackId;

    public LiveSidePanelOptionProvider(Context context, JasTvView jasTvView) {
        super(jasTvView.getCurrentChannel());
        this.context = context;
        this.jasTvView = jasTvView;

        selectedAudioTrackId = jasTvView.getSelectedAudioTrackId();
        List<TvTrackInfo> audioList = jasTvView.getAudioTracks();
        audioTracks = new HashMap<String, String>();
        String id, lang;
        TvTrackInfo info;
        for (int i = 0; audioList != null && i < audioList.size(); i++) {
            info = audioList.get(i);
            //Log.d("SHLEE", "info.type:"+info.getType()+", id:"+info.getId()+", lang:"+info.getLanguage()+", desc:"+info.getDescription());
            id = info.getId();
            lang = (info.getLanguage() != null ? info.getLanguage() : "id:" + id);

            if ("th".equalsIgnoreCase(lang)) {
                lang = context.getString(R.string.audio_thai);
            } else if ("nar".equalsIgnoreCase(lang)) {
                lang = context.getString(R.string.audio_thai) + context.getString(R.string.audio_ad);
            } else {
                lang = context.getString(R.string.audio_original);
            }
            audioTracks.put(id, lang);
        }
        //audioTracks.forEach((key, value) -> Log.d("SHLEE", "audioTracks("+key+","+value+")"));

        selectedSubtitleTrackId = jasTvView.getSelectedSubtitleTrackId();
        List<TvTrackInfo> subtitleList = jasTvView.getSubtitleTracks();
        subtitleTracks = new HashMap<String, String>();
        for (int i = 0; subtitleList != null && i < subtitleList.size(); i++) {
            info = subtitleList.get(i);
            //Log.d("SHLEE", "info.type:"+info.getType()+", id:"+info.getId()+", lang:"+info.getLanguage()+", desc:"+info.getDescription());
            id = info.getId();
            lang = (info.getLanguage() != null ? info.getLanguage() : "null:" + id);
            Locale locale = Locale.forLanguageTag(lang);
            if (locale != null) {
                lang = locale.getDisplayName();
            }
            subtitleTracks.put(id, lang);
        }
        //subtitleTracks.forEach((key, value) -> Log.d("SHLEE", "subtitleTracks("+key+","+value+")"));
    }

    @Override
    public Object getData(int key) {
        switch (key) {
            case KEY_SUBTITLE:
                return getSubtitleList();
            case KEY_AUDIO_LIST:
                return getAudioList();
        }

        return null;
    }

    @Override
    public ArrayList<String> getSubtitleList() {
        ArrayList<String> subtitleList = new ArrayList<>();
        if (subtitleTracks.size() > 0) {
            subtitleList.add("OFF");
            subtitleTracks.forEach((key, value) -> subtitleList.add(value));
        }
        return subtitleList;
    }

    @Override
    public ArrayList<String> getAudioList() {
        ArrayList<String> audioList = new ArrayList<>();
        if (audioTracks.size() > 1) {
            audioTracks.forEach((key, value) -> audioList.add(value));
        }
        return audioList;
    }

    @Override
    public void setSubtitle(String lang) {
        //Log.d("SHLEE", "setSubtitle : " + lang);
        String id = null;
        for (String key : subtitleTracks.keySet()) {
            if (lang.equalsIgnoreCase(subtitleTracks.get(key))) {
                id = key;
            }
        }
        //Log.d("SHLEE", "setSubtitle() id:" + id);

        jasTvView.selectSubtitleTrack(id);
        selectedSubtitleTrackId = id;
    }

    @Override
    public void setAudio(String lang) {
        //Log.d("SHLEE", "setAudio() lang:" + lang);
        String id = null;
        for (String key : audioTracks.keySet()) {
            if (lang.equalsIgnoreCase(audioTracks.get(key))) {
                id = key;
            }
        }
        //Log.d("SHLEE", "setAudio() id:" + id);

        jasTvView.selectAudioTrack(id);
        selectedAudioTrackId = id;
    }

    @Override
    public String getCurrentSubtitleLanguage() {
        if (subtitleTracks.size() > 0 && selectedSubtitleTrackId != null) {
            return subtitleTracks.get(selectedSubtitleTrackId);
        } else {
            return "OFF";
        }
    }

    @Override
    public String getCurrentAudioLanguage() {
        if (audioTracks.size() > 1 && selectedAudioTrackId != null) {
            return audioTracks.get(selectedAudioTrackId);
        } else {
            return "OFF";
        }
    }

    @Override
    public boolean isFavoriteMenuVisible() {
        return (getItem() != null && !getItem().isHiddenChannel());
    }

    @Override
    public void requestFavorite(boolean isAdd) {
        JasChannelManager.getInstance().setFavoriteChannel(context, getItem(), isAdd);
    }

    @Override
    public String getSubtitleStyle() {
        return null;
    }

    @Override
    public void setSubtitleStyle(String style) {

    }

    @Override
    public void reset() {

    }
}
