package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.tvmodule.util.StringUtils;

import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 03,09,2020
 */
public class AlertSystemDialog implements View.OnKeyListener {
    public static final String CLASS_NAME = AlertSystemDialog.class.getName();
    private final String TAG = AlertSystemDialog.class.getSimpleName();

    private WindowManager wm;
    private final View parentsView;
    private final String mPopUpTitle;
    private final String mErrorCode;
    private final String mDescription;
    private final BackendKnobsFlags mBackendKnobsFlags;

    public AlertSystemDialog(Context context, String errorCode, String description) {
       this(context, "", errorCode, description);
    }

    public AlertSystemDialog(Context context, String popupTitle, String errorCode, String description) {
        mBackendKnobsFlags = TvSingletons.getSingletons(context).getBackendKnobs();
        mPopUpTitle = popupTitle;
        mErrorCode = errorCode;
        mDescription = description;
        LayoutInflater inflater = LayoutInflater.from(context);
        parentsView = inflater.inflate(R.layout.dialog_error, null, false);
        initView(parentsView);
        initButton(parentsView);

    }

    public void show(Context context) {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                PixelFormat.TRANSLUCENT);
        mParams.gravity = Gravity.CENTER;
        wm.addView(parentsView, mParams);
        mBackendKnobsFlags.setSystemDialogVisibility(true);
    }

    public void dismiss() {
        mBackendKnobsFlags.setSystemDialogVisibility(false);
        wm.removeViewImmediate(parentsView);
    }

    private void initView(View view) {
        TextView popupTitleView = view.findViewById(R.id.popupTitle);
        TextView errorCodeView = view.findViewById(R.id.error_code);
        TextView errorMessageView = view.findViewById(R.id.error_message);

        if (!StringUtils.nullToEmpty(mPopUpTitle).isEmpty()) {
            popupTitleView.setText(mPopUpTitle);
        }
        errorCodeView.setText(mErrorCode);
        errorMessageView.setText(mDescription);
    }


    private void initButton(View view) {
        TextView closeButton = view.findViewById(R.id.close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        return false;
    }
}