package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;

public class CouponUtil {
    private static final String TAG = CouponUtil.class.getSimpleName();

    public static final int getCouponOriginalDiscountValue(float price, DiscountCoupon discountCoupon) {
        if (discountCoupon != null) {
            float originalValue = price * discountCoupon.getRate() / 100;
            return (int) Math.ceil(originalValue);
        }

        return 0;
    }

    public static int getCouponDiscountValue(float price, DiscountCoupon discountCoupon) {
        int discountValue = 0;
        if (discountCoupon != null) {
            int ceilDiscount = getCouponOriginalDiscountValue(price, discountCoupon);
            discountValue = ceilDiscount;

            int maxAmount = discountCoupon.getMaxAmount();
            if (maxAmount != 0 && ceilDiscount > maxAmount) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "getCouponDiscountValue, ceilDiscount : " + ceilDiscount + ", replace to maxAmount...");
                }

                discountValue = maxAmount;
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "getCouponDiscountValue, price : " + price + ", rate : " + discountCoupon.getRate() + "%"
                        + ", maxAmount : " + maxAmount + ", ceilDiscount : " + ceilDiscount + ", discountValue : " + discountValue);
            }
        }

        return discountValue;
    }
}
