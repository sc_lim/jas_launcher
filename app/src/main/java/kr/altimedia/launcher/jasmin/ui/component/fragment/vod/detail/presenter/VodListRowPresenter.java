/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter;

import android.content.res.Resources;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewKeyListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.BaseGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.BaseHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.rowView.ContentsRowView;
import kr.altimedia.launcher.jasmin.ui.view.util.FocusHighlightHelper;
import kr.altimedia.launcher.jasmin.ui.view.util.TransitionHelper;

public class VodListRowPresenter extends RowPresenter {
    protected final int mFocusZoomFactor = 5;
    protected int mContentsResourceId = -1;
    private View historyView;

    public VodListRowPresenter(int mContentsResourceId) {
        this.mContentsResourceId = mContentsResourceId;
    }

    public void setHistoryView(View historyView) {
        this.historyView = historyView;
    }

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {
        ContentsRowView rowView = mContentsResourceId != -1 ?
                new ContentsRowView(parent.getContext(), mContentsResourceId) : new ContentsRowView(parent.getContext());
        return new VodListRowPresenter.ViewHolder(rowView, rowView.getGridView(), this);
    }

    @Override
    protected void initializeRowViewHolder(RowPresenter.ViewHolder vh) {
        super.initializeRowViewHolder(vh);
        ViewHolder viewHolder = (ViewHolder) vh;
        viewHolder.mItemBridgeAdapter = new VodListItemBridgeAdapter(viewHolder);
        viewHolder.setHistoryView(historyView);
        FocusHighlightHelper.setupBrowseItemFocusHighlight(viewHolder.mItemBridgeAdapter, this.mFocusZoomFactor, false);
    }

    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder vh, Object item) {
        super.onBindRowViewHolder(vh, item);

        ViewHolder viewHolder = (ViewHolder) vh;
        ListRow rowItem = (ListRow) item;
        viewHolder.setData(rowItem);
    }

    @Override
    protected void onUnbindRowViewHolder(RowPresenter.ViewHolder vh) {
        ViewHolder viewHolder = (ViewHolder) vh;
        viewHolder.layout.setOnFocusSearchListener(null);
        viewHolder.mGridView.setAdapter(null);
        viewHolder.mItemBridgeAdapter.clear();
        viewHolder.setBaseOnItemKeyListener(null);

        super.onUnbindRowViewHolder(viewHolder);
    }

    public static class ViewHolder extends RowPresenter.ViewHolder {
        protected final BrowseFrameLayout layout;
        protected final HorizontalGridView mGridView;
        protected final VodListRowPresenter mListRowPresenter;
        public VodListItemBridgeAdapter mItemBridgeAdapter;

        private View historyView;

        public ViewHolder(View rootView, HorizontalGridView gridView, VodListRowPresenter p) {
            super(rootView);
            this.layout = rootView.findViewById(R.id.layout);
            this.mGridView = gridView;
            this.mListRowPresenter = p;

            if (layout != null) {
                layout.setOnFocusSearchListener(new BrowseFrameLayout.OnFocusSearchListener() {
                    @Override
                    public View onFocusSearch(View child, int direction) {
                        if (direction == View.FOCUS_UP) {
                            return historyView;
                        }

                        return null;
                    }
                });
            }
        }

        public void setData(ListRow rowItem) {
            mItemBridgeAdapter.setAdapter(rowItem.getAdapter());
            mGridView.setAdapter(mItemBridgeAdapter);
            mGridView.setContentDescription(rowItem.getContentDescription());

            initGridView();
            initKeyListener();
        }

        protected void initGridView() {
            // 포커스 시 확대되는 부분의 패딩 클립을 위
            BaseHeaderPresenter.ViewHolder headerViewHolder = getHeaderViewHolder();
            View headerView = headerViewHolder.view;
            int headerBottom = headerViewHolder.getHeaderViewHeight();

            Resources resources = headerView.getResources();
            int top = headerBottom + resources.getDimensionPixelSize(R.dimen.vod_header_padding_bottom);
            int left = resources.getDimensionPixelSize(R.dimen.vod_detail_padding_left);
            int right = resources.getDimensionPixelSize(R.dimen.vod_detail_padding_right);

            headerView.setPadding(left, headerView.getPaddingTop(), headerView.getPaddingRight(), headerView.getPaddingBottom());
            mGridView.setPadding(left, top, right, 0);
            mGridView.setClipChildren(false);

            int gap = (int) resources.getDimension(R.dimen.poster_horizontal_space);
            mGridView.setHorizontalSpacing(gap);
        }

        protected void initKeyListener() {
            setBaseOnItemKeyListener(new VodListKeyListener(mGridView));
        }

        public final HorizontalGridView getGridView() {
            return this.mGridView;
        }

        public void setHistoryView(View historyView) {
            this.historyView = historyView;
        }
    }

    protected static class VodListKeyListener implements BaseOnItemViewKeyListener<Row> {
        protected HorizontalGridView mGridView;

        public VodListKeyListener(HorizontalGridView mGridView) {
            this.mGridView = mGridView;
        }

        @Override
        public boolean onItemKey(int keyCode, Presenter.ViewHolder var1, Object var2, Presenter.ViewHolder var3, Row var4) {
            int lastIndex = mGridView.getAdapter().getItemCount() - 1;
            int index = mGridView.getChildPosition(mGridView.getFocusedChild());

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    if (index == lastIndex) {
                        mGridView.scrollToPosition(0);
                        return true;
                    }

                    return false;
            }

            return false;
        }
    }

    protected static class VodListItemBridgeAdapter extends ItemBridgeAdapter {
        private float ALIGNMENT = 87.3f;
        private VodListRowPresenter.ViewHolder mRowViewHolder;
        private HorizontalGridView mHorizontalGridView;

        public VodListItemBridgeAdapter(VodListRowPresenter.ViewHolder mRowViewHolder) {
            this.mRowViewHolder = mRowViewHolder;
            this.mHorizontalGridView = mRowViewHolder.getGridView();

            initAlignment();
        }

        public void setAlignment(float alignment) {
            this.ALIGNMENT = alignment;

            if (mHorizontalGridView == null) {
                return;
            }

            mHorizontalGridView.setFocusScrollStrategy(BaseGridView.FOCUS_SCROLL_ALIGNED);
            mHorizontalGridView.setWindowAlignmentOffsetPercent(ALIGNMENT);
        }

        protected void onCreate(ItemBridgeAdapter.ViewHolder viewHolder) {
            if (viewHolder.itemView instanceof ViewGroup) {
                TransitionHelper.setTransitionGroup((ViewGroup) viewHolder.itemView, true);
            }
        }

        protected void initAlignment() {
            if (mHorizontalGridView == null) {
                return;
            }

            setAlignment(ALIGNMENT);
        }

        public void onBind(final ItemBridgeAdapter.ViewHolder viewHolder) {
            if (this.mRowViewHolder.getOnItemViewClickedListener() != null) {
                viewHolder.mHolder.view.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ItemBridgeAdapter.ViewHolder ibh = (ItemBridgeAdapter.ViewHolder) mRowViewHolder.mGridView.getChildViewHolder(viewHolder.itemView);
                        if (mRowViewHolder.getOnItemViewClickedListener() != null) {
                            mRowViewHolder.getOnItemViewClickedListener().onItemClicked(viewHolder.mHolder, ibh.mItem, mRowViewHolder, mRowViewHolder.getRow());
                        }

                    }
                });
            }

            if (this.mRowViewHolder.getBaseOnItemKeyListener() != null) {
                viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() != KeyEvent.ACTION_DOWN) {
                            return false;
                        }

                        ItemBridgeAdapter.ViewHolder ibh = (ItemBridgeAdapter.ViewHolder) mRowViewHolder.mGridView.getChildViewHolder(viewHolder.itemView);
                        return mRowViewHolder.getBaseOnItemKeyListener().onItemKey(keyCode, viewHolder.mHolder, ibh.mItem, mRowViewHolder, mRowViewHolder.getRow());
                    }
                });
            }
        }

        public void onUnbind(ItemBridgeAdapter.ViewHolder viewHolder) {
            viewHolder.mHolder.view.setOnClickListener(null);
            viewHolder.mHolder.view.setOnKeyListener(null);
        }

        public void onAttachedToWindow(ItemBridgeAdapter.ViewHolder viewHolder) {
            this.mRowViewHolder.syncActivatedStatus(viewHolder.itemView);
        }
    }
}
