/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.CallSuper;

/**
 * Created by mc.kim on 05,02,2020
 */
public abstract class VideoPlaybackGlue {

    private final Context mContext;
    private VideoPlaybackGlueHost mPlaybackGlueHost;

    public abstract static class PlayerCallback {
        private final long remainAlertTime;
        private boolean isAlerted = false;

        public PlayerCallback() {
            this.remainAlertTime = 0;
        }

        public PlayerCallback(long remainAlertTime) {
            this.remainAlertTime = remainAlertTime;
        }

        public void onPreparedStateChanged(VideoPlaybackGlue glue) {
        }

        public void onPlayStateChanged(VideoPlaybackGlue glue) {
        }

        public void onPlayCompleted(VideoPlaybackGlue glue) {
        }

        public void onAlertTime(VideoPlaybackGlue glue) {

        }

        public void onUpdateProgress(VideoPlaybackGlue glue, long time, long duration) {

        }

        public void onReady(boolean ready) {

        }

        public void onBlockedStateChanged(boolean isLocked) {

        }

        public boolean isAlerted() {
            return isAlerted;
        }

        public void setAlerted(boolean alerted) {
            isAlerted = alerted;
        }

        public long getRemainAlertTime() {
            return remainAlertTime;
        }
    }

    ArrayList<PlayerCallback> mPlayerCallbacks;

    public VideoPlaybackGlue(Context context) {
        this.mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    public boolean isPrepared() {
        return true;
    }

    public boolean isReady() {
        return true;
    }

    public void addPlayerCallback(PlayerCallback playerCallback) {
        if (mPlayerCallbacks == null) {
            mPlayerCallbacks = new ArrayList();
        }
        mPlayerCallbacks.add(playerCallback);
    }

    public void clearPlayerCallback() {
        if (mPlayerCallbacks != null) {
            mPlayerCallbacks.clear();
        }
    }


    public void removePlayerCallback(PlayerCallback callback) {
        if (mPlayerCallbacks != null) {
            mPlayerCallbacks.remove(callback);
        }
    }

    protected List<PlayerCallback> getPlayerCallbacks() {
        if (mPlayerCallbacks == null) {
            return null;
        }
        return new ArrayList(mPlayerCallbacks);
    }

    public boolean isPlaying() {
        return false;
    }

    public void play() {
    }

    public void playWhenPrepared() {
        if (isPrepared()) {
            play();
        } else {
            addPlayerCallback(new PlayerCallback() {
                @Override
                public void onPreparedStateChanged(VideoPlaybackGlue glue) {
                    if (glue.isPrepared()) {
                        removePlayerCallback(this);
                        play();
                    }
                }
            });
        }
    }

    public void pause() {
    }

    public void next() {
    }

    public void previous() {
    }


    public final void setHost(VideoPlaybackGlueHost host) {
        if (mPlaybackGlueHost == host) {
            return;
        }
        if (mPlaybackGlueHost != null) {
            mPlaybackGlueHost.attachToGlue(null);
        }
        mPlaybackGlueHost = host;
        if (mPlaybackGlueHost != null) {
            mPlaybackGlueHost.attachToGlue(this);
        }
    }

    protected void onHostStart() {
    }

    protected void onHostStop() {
    }

    protected void onHostResume() {
    }

    protected void onHostPause() {
    }

    protected  void onTimerStop(){

    }

    @CallSuper
    protected void onAttachedToHost(VideoPlaybackGlueHost host) {
        mPlaybackGlueHost = host;
        mPlaybackGlueHost.setHostCallback(new VideoPlaybackGlueHost.HostCallback() {
            @Override
            public void onHostStart() {
                VideoPlaybackGlue.this.onHostStart();
            }

            @Override
            public void onHostStop() {
                VideoPlaybackGlue.this.onHostStop();
            }

            @Override
            public void onHostResume() {
                VideoPlaybackGlue.this.onHostResume();
            }

            @Override
            public void onHostPause() {
                VideoPlaybackGlue.this.onHostPause();
            }

            @Override
            public void onHostDestroy() {
                setHost(null);
            }

            @Override
            public void onTimerStop() {
                VideoPlaybackGlue.this.onTimerStop();
            }
        });
    }

    @CallSuper
    protected void onDetachedFromHost() {
        if (mPlaybackGlueHost != null) {
            mPlaybackGlueHost.setHostCallback(null);
            mPlaybackGlueHost = null;
        }
    }

    public VideoPlaybackGlueHost getHost() {
        return mPlaybackGlueHost;
    }
}
