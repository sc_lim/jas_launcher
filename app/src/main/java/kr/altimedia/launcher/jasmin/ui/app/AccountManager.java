/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import autovalue.shaded.com.google$.common.annotations.$VisibleForTesting;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.LoginResult;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileLoginResult;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PROFILE;

/**
 * Created by mc.kim on 09,06,2020
 */
public class AccountManager implements SettingControl.SettingsEventListener {
    public static final String KEY_SAID = "saId";
    public static final String KEY_LOGIN_INFO = "loginInfo";
    public static final String KEY_SUBSCRIBER_INFO = "subscriberInfo";
    public static final String KEY_CURRENT_PROFILE = "currentProfile";
    public static final String KEY_PROFILE_LIST = "profileList";
    public static final String KEY_FCM_TOKEN = "fcmToken";
    public static final String KEY_ANDROID_ID = "androidId";
    public static final String KEY_AUTO_LOGIN_APP_LIST = "autoLoginAppList";

    private static final AccountManager ourInstance = new AccountManager();
    private final String TAG = AccountManager.class.getSimpleName();
    private HashMap<String, Object> accountData = new HashMap<>();
    private String mLaUrl = null;

    private AccountManager() {
        String[] keyFilter = new String[]{
                SettingControl.KEY_LA_URL
        };
        SettingControl.getInstance().addSettingsEventListener(this, keyFilter);
        mLaUrl = SettingControl.getInstance().getLaUrl();
    }

    public static AccountManager getInstance() {
        return ourInstance;
    }

    public void putData(String key, Object data) {
        this.putData(key, data, null);
    }

    public void putData(String key, Object data, @Nullable DataPutListener callback) {
        if (Log.INCLUDE) {
            Log.d(TAG, "putData  key : " + key + ", data  : " + data);
        }
        accountData.put(key, data);
        if (key.equalsIgnoreCase(KEY_LOGIN_INFO)) {
            LoginResult info = (LoginResult) accountData.get(KEY_LOGIN_INFO);
            updateLoginInfo(info, callback);
        } else if (key.equalsIgnoreCase(KEY_SAID)) {
            String saId = (String) accountData.get(KEY_SAID);
            SettingControl.getInstance().setSaid(saId);
        }
        if (callback != null) {
            if (data == null) {
                callback.onFail(key);
            } else {
                callback.onSuccess(key);
            }
        }
    }

    public boolean updateProfileList(List<ProfileInfo> list) {
        return updateProfileList(list, true);
    }

    public boolean updateProfileList(List<ProfileInfo> list, boolean ignoreLock) {
        if (!containData(KEY_LOGIN_INFO)) {
            return false;
        }
        LoginResult loginResult = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        loginResult.setProfileInfoList(list);
        accountData.put(AccountManager.KEY_LOGIN_INFO, loginResult);
        SettingControl.getInstance().setAccountId(loginResult.getAccountId());
        SettingControl.getInstance().setLoginToken(loginResult.getLoginToken());
        SettingControl.getInstance().setRegionCode(loginResult.getRegionCode());
        putData(KEY_PROFILE_LIST, list);
        ProfileInfo lastLoginProfile = findLoginProfile(list, ignoreLock);
        if (lastLoginProfile != null) {
            putData(KEY_CURRENT_PROFILE, lastLoginProfile);
            SettingControl.getInstance().setProfileId(lastLoginProfile.getProfileId());
        }
        return true;
    }

    public boolean updateProfile(ProfileInfo info) {
        if (!containData(KEY_LOGIN_INFO)) {
            return false;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "updateProfile : " + info.getProfileId());
            Log.d(TAG, "getProfileId() : " + getProfileId());
        }
        info.setLastLoginYN(getProfileId() == null || info.getProfileId().equalsIgnoreCase(getProfileId()));
        LoginResult loginResult = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        loginResult.updateProfileInfo(info);
        accountData.put(AccountManager.KEY_LOGIN_INFO, loginResult);
        SettingControl.getInstance().setAccountId(loginResult.getAccountId());
        SettingControl.getInstance().setLoginToken(loginResult.getLoginToken());
        SettingControl.getInstance().setRegionCode(loginResult.getRegionCode());
        List<ProfileInfo> list = loginResult.getProfileInfoList();
        putData(KEY_PROFILE_LIST, list);
        ProfileInfo lastLoginProfile = findLoginProfile(list, true);
        putData(KEY_CURRENT_PROFILE, lastLoginProfile);
        if (lastLoginProfile != null) {
            SettingControl.getInstance().setProfileId(lastLoginProfile.getProfileId());
        }
        return true;
    }

    private void updateLoginInfo(LoginResult loginResult, DataPutListener callback) {
        if (loginResult == null) {
            return;
        }
        SettingControl.getInstance().setAccountId(loginResult.getAccountId());
        SettingControl.getInstance().setLoginToken(loginResult.getLoginToken());
        SettingControl.getInstance().setRegionCode(loginResult.getRegionCode());

        List<ProfileInfo> list = loginResult.getProfileInfoList();
        if (Log.INCLUDE) {
            Log.d(TAG, "updateLoginInfo: list=" + list);
        }
        putData(KEY_PROFILE_LIST, list, callback);
        ProfileInfo lastLoginProfile = findLoginProfile(list);
        selectProfile(lastLoginProfile, callback);
        if (lastLoginProfile != null) {
            SettingControl.getInstance().setProfileId(lastLoginProfile.getProfileId());
        }
    }

    private ProfileInfo findLoginProfile(List<ProfileInfo> list) {
        return findLoginProfile(list, false);
    }

    private ProfileInfo findLoginProfile(List<ProfileInfo> list, boolean ignoreLock) {
        for (ProfileInfo profileInfo : list) {
            if (profileInfo.isLastLoginYN() && (ignoreLock || !profileInfo.isLock())) {
                ProfileInfo lastLoginProfile = profileInfo;
                return lastLoginProfile;
            }
        }

        return null;
    }

    public Object getData(String key) {
        return accountData.get(key);
    }

    public boolean containData(String key) {
        return accountData.containsKey(key) && accountData.get(key) != null;
    }

    public String getSaId() {
        if (!containData(KEY_SAID)) {
            return "";
        }
        return (String) accountData.get(KEY_SAID);
    }

    public String getLoginToken() {
        if (!containData(KEY_LOGIN_INFO)) {
            return "";
        }
        LoginResult loginResult = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        return loginResult.getLoginToken();
    }

    public String getProfileId() {
        if (!containData(KEY_CURRENT_PROFILE)) {
            return "";
        }
        ProfileInfo info = (ProfileInfo) getData(KEY_CURRENT_PROFILE);
        return info.getProfileId();
    }

    private void selectProfile(ProfileInfo info, DataPutListener callback) {
        putData(KEY_CURRENT_PROFILE, info, callback);
    }

    public String getLoginId() {
        if (!containData(KEY_LOGIN_INFO)) {
            return "";
        }
        LoginResult info = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        return info.getLoginId();
    }

    public String getProductCode() {
        if (!containData(KEY_LOGIN_INFO)) {
            return "";
        }
        LoginResult info = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        return info.getProductCode();
    }

    public String getProductName() {
        if (!containData(KEY_LOGIN_INFO)) {
            return "";
        }
        LoginResult info = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        return info.getProductName();
    }

    public String getAccountId() {
        if (!containData(KEY_LOGIN_INFO)) {
            return "";
        }
        LoginResult info = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        return info.getAccountId();
    }

    public boolean isLoginUser() {
        return !StringUtils.nullToEmpty(AccountManager.getInstance().getAccountId()).isEmpty();
    }

    public boolean isAuthenticatedUser() {
        return !StringUtils.nullToEmpty(AccountManager.getInstance().getProfileId()).isEmpty();
    }


    public String getAccountName() {
        if (!containData(KEY_LOGIN_INFO)) {
            return "";
        }
        LoginResult info = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        return info.getAccountName();
    }

    public String getBaseUrl() {
        if (!containData(KEY_LOGIN_INFO)) {
            return null;
        }
        LoginResult info = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        return info.getBaseUrl();
    }

    public String getLocalLanguage() {
        String language = Locale.getDefault().getLanguage();
        if (Log.INCLUDE) {
            Log.d(TAG, "getLocalLanguage : " + language);
        }
        if (language.equalsIgnoreCase("th")) {
            return language.toUpperCase();
        } else {
            return "EN";
        }
    }


    public void onNewProfileLogin(String newProfileId, ProfileLoginResult profileLoginResult) {
        String currProfileId = getProfileId();
        if (Log.INCLUDE) {
            Log.d(TAG, "onNewProfileLogin : " + currProfileId + ", newProfileId : " + newProfileId + ", loginResult : " + profileLoginResult.toString());
        }
        LoginResult info = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        info.setLastProfile(newProfileId);
        info.setLoginToken(profileLoginResult.getLoginToken());
        accountData.put(AccountManager.KEY_LOGIN_INFO, info);
        SettingControl.getInstance().setAccountId(info.getAccountId());
        SettingControl.getInstance().setLoginToken(info.getLoginToken());
        SettingControl.getInstance().setRegionCode(info.getRegionCode());
        List<ProfileInfo> list = info.getProfileInfoList();
        putData(KEY_PROFILE_LIST, list);
        ProfileInfo lastLoginProfile = findLoginProfile(list, true);
        putData(KEY_CURRENT_PROFILE, lastLoginProfile);
        if (lastLoginProfile != null) {
            SettingControl.getInstance().setProfileId(lastLoginProfile.getProfileId());
        }
    }

    public ArrayList<ProfileInfo> getProfileList() {
        if (!containData(KEY_PROFILE_LIST)) {
            return null;
        }
        ArrayList<ProfileInfo> info = (ArrayList<ProfileInfo>) accountData.get(KEY_PROFILE_LIST);
        return info;
    }

    public ProfileInfo getSelectedProfile() {
        if (!containData(KEY_CURRENT_PROFILE)) {
            return null;
        }
        ProfileInfo info = (ProfileInfo) accountData.get(KEY_CURRENT_PROFILE);
        return info;
    }

    public int getCurrentRating() {
        ProfileInfo info = getSelectedProfile();
        if (info == null) {
            return 0;
        }
        return info.getRating();
    }

    public boolean isSupportedAutoLogin(String packageId) {
        if (!containData(KEY_AUTO_LOGIN_APP_LIST)) {
            return false;
        }

        ArrayList<String> info = (ArrayList<String>) accountData.get(KEY_AUTO_LOGIN_APP_LIST);
        return info.contains(packageId);
    }

    @VisibleForTesting
    public void currentProfileRemove(Context context) {
        UserDataManager userDataManager = new UserDataManager();
        userDataManager.deleteProfile(AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(), new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int reason) {

                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        PushMessageReceiver.checkAndDefineActionSendForTest(context, new Intent(ACTION_UPDATED_PROFILE));
                    }

                    @Override
                    public void onError(String errorCode, String message) {

                    }
                });

    }

    public long getPreviewTime() {
        if (!containData(KEY_LOGIN_INFO)) {
            long previewTime = 5;//5 minutes for default
            if (Log.INCLUDE) {
                Log.d(TAG, "getPreviewTime() loginResult is null - use default value, previewTime: " + previewTime + " minutes");
            }
            return previewTime;
        }
        LoginResult info = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        long previewTime = info.getChannelPreviewTime();
        if (Log.INCLUDE) {
            Log.d(TAG, "getPreviewTime() previewTime: " + previewTime + " minutes");
        }
        return previewTime;
    }

    public long getPreviewResetTime() {
        if (!containData(KEY_LOGIN_INFO)) {
            long previewResetTime = 60;//60 minutes for default
            if (Log.INCLUDE) {
                Log.d(TAG, "getPreviewResetTime() loginResult is null - use default value, previewResetTime: " + previewResetTime + " minutes");
            }
            return previewResetTime;
        }
        LoginResult info = (LoginResult) accountData.get(KEY_LOGIN_INFO);
        long previewResetTime = info.getChannelPreviewLimitTime();
        if (Log.INCLUDE) {
            Log.d(TAG, "getPreviewResetTime() previewResetTime: " + previewResetTime + " minutes");
        }
        return previewResetTime;
    }

    public float getVat(PaymentType type) {
        switch (type) {
            case BILLING:
            case AIR_PAY:
            case DISCOUNT_COUPON:
            case CASH_COUPON:
            case QR:
            case MEMBERSHIP_APP:
            case MEMBERSHIP:
            case PROMPT_PAY:
            case INTERNET_MOBILE_BANK:
            case CREDIT_CARD:
            default:
                return 0f;
        }
    }


    @$VisibleForTesting
    public void printAccountInfo() {
        if (Log.INCLUDE) {
            Log.d(TAG, "printAccountInfo | " + accountData.toString());
        }
    }

    public String getLicenseUrl() {
        if (StringUtils.nullToEmpty(mLaUrl).isEmpty()) {
            mLaUrl = ServerConfig.LA_URL;
        }
        return mLaUrl;
    }

    @Override
    public void onSettingChanged(String s) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSettingChanged | " + s);
        }
        switch (s) {
            case SettingControl.KEY_LA_URL:
                mLaUrl = SettingControl.getInstance().getLaUrl();
                break;
        }
    }

    public void initialize(Context context) {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onComplete");
                    }
                    return;
                }

                String id = task.getResult().getId();
                String token = task.getResult().getToken();

                if (Log.INCLUDE) {
                    Log.d(TAG, "onComplete | id : " + id + ", token : " + token);
                }
                putFcmToken(token);
            }
        });
        String idByANDROID_ID =
                Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        putData(KEY_ANDROID_ID, idByANDROID_ID);
    }

    public String getAndroidId() {
        if (!accountData.containsKey(KEY_ANDROID_ID)) {
            return "";
        }
        return (String) accountData.get(KEY_ANDROID_ID);
    }

    public void putFcmToken(String token) {
        if (Log.INCLUDE) {
            Log.d(TAG, "putFcmToken | token : " + token);
        }
        accountData.put(KEY_FCM_TOKEN, token);
    }

    public String getFcmToken() {
        if (!accountData.containsKey(KEY_FCM_TOKEN)) {
            return "";
        }
        return (String) accountData.get(KEY_FCM_TOKEN);
    }

    private int mResultCode = Activity.RESULT_OK;

    public void putResultCode(int resultCode) {
        mResultCode = resultCode;
    }

    public long getMenuUpdatePeriod() {
        return 1 * TimeUtil.HOUR;
    }

    public int popResultCode() {
        int resultCode = mResultCode;
        mResultCode = Activity.RESULT_OK;
        return resultCode;
    }

    public interface DataPutListener {
        void onSuccess(String key);

        void onFail(String key);
    }

    private final String DUMMY_DATA = "{\n" +
            "\t\"data\": [{\n" +
            "\t\t\"packageId\": \"com.doonung.dtv\",\n" +
            "\t\t\"intentData\": [{\n" +
            "\t\t\t\"token\": \"tokenValue\"\n" +
            "\t\t}]\n" +
            "\t}, {\n" +
            "\t\t\"packageId\": \"com.hbo.asia.androidtv\",\n" +
            "\t\t\"intentData\": [{\n" +
            "\t\t\t\"token\": \"tokenValue\"\n" +
            "\t\t}]\n" +
            "\t}],\n" +
            "\t\"returnCode\": \"S\"\n" +
            "}";
}
