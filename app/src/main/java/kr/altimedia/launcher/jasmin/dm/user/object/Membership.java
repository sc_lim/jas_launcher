/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Membership implements Parcelable {
    public static final Creator<Membership> CREATOR = new Creator<Membership>() {
        @Override
        public Membership createFromParcel(Parcel in) {
            return new Membership(in);
        }

        @Override
        public Membership[] newArray(int size) {
            return new Membership[size];
        }
    };

    //TB return wrong type
//    @Expose
//    @SerializedName("pointRate")
//    private int pointRate;
    @Expose
    @SerializedName("membershipPoint")
    private int membershipPoint;

    public Membership(int membershipPoint) {
        this.membershipPoint = membershipPoint;
    }

    protected Membership(Parcel in) {
        membershipPoint = in.readInt();
//        pointRate = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(membershipPoint);
//        dest.writeInt(pointRate);
    }

    @NonNull
    @Override
    public String toString() {
        return "Membership{" +
                "membershipPoint=" + membershipPoint +
//                ", pointRate=" + pointRate +
                "}";
    }

    public int getMembershipPoint() {
        return membershipPoint;
    }

//    public int getPointRate() {
//        return pointRate;
//    }
}