/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

/**
 * An AnimatorListenerAdapter subclass that conveniently sets the layer type to hardware during the
 * animation.
 */
public class HardwareLayerAnimatorListenerAdapter extends AnimatorListenerAdapter {
    private final View mView;
    private boolean mLayerTypeChanged;

    public HardwareLayerAnimatorListenerAdapter(View view) {
        mView = view;
    }

    @Override
    public void onAnimationStart(Animator animator) {
        if (mView.hasOverlappingRendering() && mView.getLayerType() == View.LAYER_TYPE_NONE) {
            mLayerTypeChanged = true;
            mView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
    }

    @Override
    public void onAnimationEnd(Animator animator) {
        if (mLayerTypeChanged) {
            mLayerTypeChanged = false;
            mView.setLayerType(View.LAYER_TYPE_NONE, null);
        }
    }
}
