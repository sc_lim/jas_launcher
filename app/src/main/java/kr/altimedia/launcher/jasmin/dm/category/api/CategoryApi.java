/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.category.api;

import kr.altimedia.launcher.jasmin.dm.category.obj.response.CategoryResponse;
import kr.altimedia.launcher.jasmin.dm.category.obj.response.CategoryVersionCheckResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by mc.kim on 08,05,2020
 */
public interface CategoryApi {
    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("category")
    Call<CategoryResponse> getCategoryList(@Query("language") String language, @Query("said") String said,
                                           @Query("categoryId") String categoryID, @Query("deviceType") String deviceType,
                                           @Query("version") String version);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("categoryversioncheck")
    Call<CategoryVersionCheckResponse> getCategoryVersionCheckResult(
            @Query("deviceType") String deviceType, @Query("version") String version);

}
