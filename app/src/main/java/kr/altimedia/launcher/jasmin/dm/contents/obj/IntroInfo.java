/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by mc.kim on 18,06,2020
 */
public class IntroInfo implements Parcelable {
    public static final Creator<IntroInfo> CREATOR = new Creator<IntroInfo>() {
        @Override
        public IntroInfo createFromParcel(Parcel in) {
            return new IntroInfo(in);
        }

        @Override
        public IntroInfo[] newArray(int size) {
            return new IntroInfo[size];
        }
    };
    @Expose
    @SerializedName("skipIntroStartPosition")
    private long skipIntroStartPosition;
    @Expose
    @SerializedName("skipIntroEndPosition")
    private long skipIntroEndPosition;

    public IntroInfo(long skipIntroStartPosition, long skipIntroEndPosition) {
        this.skipIntroStartPosition = skipIntroStartPosition;
        this.skipIntroEndPosition = skipIntroEndPosition;
    }

    protected IntroInfo(Parcel in) {
        skipIntroStartPosition = in.readLong();
        skipIntroEndPosition = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(skipIntroStartPosition);
        dest.writeLong(skipIntroEndPosition);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public long getSkipIntroStartPosition() {
        return skipIntroStartPosition;
    }

    public long getSkipIntroEndPosition() {
        return skipIntroEndPosition;
    }

    @Override
    public String toString() {
        return "IntroInfo{" +
                "skipIntroStartPosition=" + skipIntroStartPosition +
                ", skipIntroEndPosition=" + skipIntroEndPosition +
                '}';
    }
}
