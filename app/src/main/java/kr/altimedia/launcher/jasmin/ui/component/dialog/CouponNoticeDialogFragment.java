/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.message.MessageDataManager;
import kr.altimedia.launcher.jasmin.dm.message.obj.Message;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public class CouponNoticeDialogFragment extends SafeDismissDialogFragment implements View.OnUnhandledKeyEventListener, View.OnKeyListener {
    public static final String CLASS_NAME = CouponNoticeDialogFragment.class.getName();
    public static final String TAG = CouponNoticeDialogFragment.class.getSimpleName();

    private static final String KEY_TYPE = "KEY_TYPE";
    private static final String KEY_MESSAGE = "KEY_MESSAGE";
    private DialogInterface.OnDismissListener mOnDismissListener = null;
    private ImageView couponStatusIcon = null;
    private TextView couponMessage = null;
    private View rootView;

    private PushMessageReceiver.CouponUpdateType type;
    private Message message;
    private TvOverlayManager mTvOverlayManager;

    private OnButtonActionListener onButtonActionListener;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private MbsDataTask messageViewTask;

    public CouponNoticeDialogFragment() {
    }

    public static CouponNoticeDialogFragment newInstance(PushMessageReceiver.CouponUpdateType type, Message message) {

        CouponNoticeDialogFragment fragment = new CouponNoticeDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_TYPE, type);
        args.putParcelable(KEY_MESSAGE, message);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onDestroyView() {
        if (messageViewTask != null && messageViewTask.isRunning()) {
            messageViewTask.cancel(true);
        }

        onButtonActionListener = null;
        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
            mOnDismissListener = null;
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach: "+mTvOverlayManager);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnDismissListener(this);
        return dialog;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_coupon_notification, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        Bundle bundle = getArguments();
        if(bundle != null){
            type = (PushMessageReceiver.CouponUpdateType)bundle.getSerializable(KEY_TYPE);
            message = bundle.getParcelable(KEY_MESSAGE);
        }
        initView(view);
        initButton(view);
    }

    private void initView(View view) {
        if(Log.INCLUDE) {
            Log.d(TAG, "initView: type=" + type);
        }

        initProgressbar(view);

        couponStatusIcon = view.findViewById(R.id.couponStatusIcon);
        couponMessage = view.findViewById(R.id.couponMessage);

        try {
            if(type != null){
                if(PushMessageReceiver.CouponUpdateType.issued.equals(type)){
                    couponStatusIcon.setImageResource(R.drawable.icon_coupon_new);
                }else if(PushMessageReceiver.CouponUpdateType.expired.equals(type)){
                    couponStatusIcon.setImageResource(R.drawable.icon_coupon_expiring);
                }
            }
            if (message != null) {
                couponMessage.setText(message.getTitle());
            }
        }catch (Exception e){
        }
    }

    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void initButton(View view) {
        TextView btnDetail = view.findViewById(R.id.btnDetail);
        btnDetail.setOnKeyListener(this);

        TextView btnClose = view.findViewById(R.id.btnClose);
        btnClose.setOnKeyListener(this);

        btnClose.requestFocus();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if(Log.INCLUDE) {
            Log.d(TAG, "onKey : " + keyCode);
        }
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (v.getId() == R.id.btnDetail) {
                    showMessage(type, message);
                } else if (v.getId() == R.id.btnClose) {
                    if (onButtonActionListener != null) {
                        onButtonActionListener.onClosed(type, message);
                    }
                }
                return true;
            case KeyEvent.KEYCODE_BACK:
                if (onButtonActionListener != null) {
                    onButtonActionListener.onClosed(type, message);
                }
                return true;
        }
        return false;
    }

    private void showMessage(PushMessageReceiver.CouponUpdateType type, Message message) {
        if (messageViewTask != null && messageViewTask.isRunning()) {
            return;
        }

        MessageDataManager mMessageDataManager = new MessageDataManager();
        AccountManager accountManager = AccountManager.getInstance();
        messageViewTask = mMessageDataManager.postMessageView(accountManager.getAccountId(), accountManager.getProfileId(), message.getId(),
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSuccess");
                        }

                        if (onButtonActionListener != null) {
                            onButtonActionListener.onLinkToMessage(type, message);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String msg) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + msg);
                        }


                        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, msg);
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return CouponNoticeDialogFragment.this.getContext();
                    }
                });
    }

    public void setOnDismissListener(@NonNull DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    public void setOnButtonActionListener(OnButtonActionListener onButtonActionListener) {
        this.onButtonActionListener = onButtonActionListener;
    }

    public interface OnButtonActionListener {
        void onLinkToMessage(PushMessageReceiver.CouponUpdateType type, Message message);
        void onClosed(PushMessageReceiver.CouponUpdateType type, Message message);
    }
}
