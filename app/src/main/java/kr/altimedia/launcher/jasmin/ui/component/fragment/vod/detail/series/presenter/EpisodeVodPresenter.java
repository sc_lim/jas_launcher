/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.leanback.widget.Presenter;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.listenter.OnVodViewChangeListener;
import kr.altimedia.launcher.jasmin.ui.view.common.VodTrailerPosterView;

public class EpisodeVodPresenter extends Presenter {
    private static final String TAG = EpisodeVodPresenter.class.getSimpleName();
    private OnVodViewChangeListener mChangeVodDetailListener = null;

    public EpisodeVodPresenter(OnVodViewChangeListener mChangeVodDetailListener) {
        this.mChangeVodDetailListener = mChangeVodDetailListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_series_vod, parent, false);
        return new SeriesVodPresenterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        SeriesContent seriesContent = (SeriesContent) item;

        SeriesVodPresenterViewHolder vh = (SeriesVodPresenterViewHolder) viewHolder;
        vh.setDate(seriesContent);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
    }

    public static class SeriesVodPresenterViewHolder extends ViewHolder {
        private VodTrailerPosterView poster;
        private TextView episodeNumView;

        public SeriesVodPresenterViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            poster = view.findViewById(R.id.poster);
            episodeNumView = view.findViewById(R.id.num);
        }

        public void setDate(SeriesContent seriesContent) {
            episodeNumView.setText(String.valueOf(seriesContent.getEpisodeId()));
            initPoster(seriesContent);
        }

        private void initPoster(SeriesContent seriesContent) {
            boolean isLock = false;

            if (isLock) {
                poster.setBlockImage(R.drawable.thumb_lock);
                poster.setBlock();
            } else {
                poster.setDefaultImage(R.drawable.detail_series_default);
                String posterUrl = seriesContent.getPosterSourceUrl(
                        SeriesContent.mPosterDefaultRect.width(),
                        SeriesContent.mPosterDefaultRect.height());
                poster.setPoster(posterUrl);
            }

            ArrayList<StreamInfo> streams = (ArrayList<StreamInfo>) seriesContent.getTrailer();
            int visibility = (streams != null && streams.size() > 0) ? View.VISIBLE : View.INVISIBLE;
            poster.setTrailerVisibility(visibility);

            int textResource = seriesContent.isWatchable() ? R.string.vod_watch : R.string.video;
            String textValue = view.getContext().getString(textResource);
            poster.setTrailerText(textValue);

            Drawable bottomShadow = ContextCompat.getDrawable(poster.getContext(), R.drawable.detail_poster_dim);
            poster.setBottomShadow(bottomShadow);
        }
    }
}
