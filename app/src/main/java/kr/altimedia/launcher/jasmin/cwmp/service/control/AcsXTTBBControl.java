/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service.control;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.cwmplibrary.common.CWMPParameter;
import com.altimedia.cwmplibrary.common.core.CPEManager;
import com.altimedia.cwmplibrary.common.method.GetParameterListener;
import com.altimedia.cwmplibrary.common.method.RPCListener;
import com.altimedia.cwmplibrary.common.method.SetParameterListener;
import com.altimedia.cwmplibrary.ttbb.object.InternetGatewayDevice;
import com.altimedia.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPCallback;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPController;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * AcsXTTBBControl
 */
public class AcsXTTBBControl implements RPCListener {
    private final String CLASS_NAME = AcsXTTBBControl.class.getName();
    private final String TAG = AcsXTTBBControl.class.getSimpleName();

    private InternetGatewayDevice root;
    private Context context;

    private String URL_FW_UPDATE_INFO;
    private String URL_APK_META_INFO;
    private String URL_DEFAULT_SETTING;

    private int normalPeriod = 0; // min

    public AcsXTTBBControl(){
    }

    public void dispose() {
        root = null;
        context = null;
    }

    public void init(Context context, String fwUpdateInfoUrl, String apkMetaInfoUrl, String defaultSettingUrl) {
        if (Log.INCLUDE) {
            Log.d(TAG, "init");
        }

        this.context = context;
        this.URL_FW_UPDATE_INFO = fwUpdateInfoUrl;
        this.URL_APK_META_INFO = apkMetaInfoUrl;
        this.URL_DEFAULT_SETTING = defaultSettingUrl;
    }

    public void start(InternetGatewayDevice root){
        if (Log.INCLUDE) {
            Log.d(TAG, "start");
        }
        this.root = root;

        if(this.root == null || this.root.x_ttbb_control == null){
            if (Log.INCLUDE) {
                Log.d(TAG, "start: root or x_ttbb_control is NOT available");
            }
            return;
        }

        setFwUpdateInfoUrl();
        setApkMetaInfoUrl();
        setDefaultSettingUrl();
        setNormalPeriod();

        setUpgradeTrigger();
        setFwUrlListener();
        setForceApkUpdateListener();
        setAcsUrlListener();

        setConfigInit();
        setReboot();
        setAppRestart();
        setStandBy();

        // not supported
//        setScreenResolution();
//        setHdmiSet();
    }

    /**
     * 공장초기화를 수행합니다.
     * RPC Method(onFactoryReset)
     */
    @Override
    public void onFactoryReset() {
        if (Log.INCLUDE) {
            Log.d(TAG, "RPC.onFactoryReset");
        }

        try {
            CWMPController.getInstance().getCWMPEventListener().onFactoryReset();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * STB 리부팅을 수행합니다.
     * RPC Method(onReboot)
     */
    @Override
    public void onReboot() {
        if (Log.INCLUDE) {
            Log.d(TAG, "RPC.onReboot");
        }

        try {
            CWMPController.getInstance().getCWMPEventListener().onReboot();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getNormalPeriod() {
        return normalPeriod * 60 * 1000;
    }

    private void setConfigInit() {
        CWMPParameter param = root.x_ttbb_control.X_TTBB_ConfigInit;
        if(param != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "ConfigInit: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
            }
            param.addSetListener(new SetParameterListener() {
                @Override
                public void onSet(Object value) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "ConfigInit: onSet: value=" + value);
                    }
                    if(value instanceof Integer) {
                        Integer command = (Integer) value;
//                        if(command == 0) { // default
//                        } else if(command == 1) { // App/설정 초기화
//                        } else if(command == 2) { // 사용자 정보 제외 설정 초기화
//                        } else if(command == 3) { // 개통정보를 제외한 초기화
//                        } else if(command == 4) { // Unbound Application 초기화
//                        }
                        try {
                            root.x_ttbb_control.X_TTBB_ConfigInit.setValue(new Integer(0));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            CWMPController.getInstance().getCWMPEventListener().onSetConfigInit(command);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private void setReboot() {
        CWMPParameter param = root.x_ttbb_control.X_TTBB_Reboot;
        if(param != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "Reboot: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
            }
            param.addSetListener(new SetParameterListener() {
                @Override
                public void onSet(Object value) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "Reboot: onSet: value=" + value);
                    }
                    if(value instanceof Integer) {
                        Integer command = (Integer) value;
//                        if(command == 0) { // 단말(앱) Reboot 명령 수행
//                        } else if(command == 1) { // 단말(앱)이 대기상태일 경우 Reboot 명령 수행
//                            // 단말(앱)이 대기모드(Stand By)이고 다른 서비스앱들의 미사용상태가 10분 이상 유지될 경우(IDLE상태) Reboot 명령을 수행.
//                        }
                        try {
                            root.x_ttbb_control.X_TTBB_Reboot.setValue(new Integer(0));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            CWMPController.getInstance().getCWMPEventListener().onSetReboot(command);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private void setStandBy() {
        CWMPParameter param = root.x_ttbb_control.StandBy;
        if(param != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "StandBy: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
            }
            param.addGetListener(new GetParameterListener() {
                @Override
                public void onGet() {
                    boolean isScreenOff = CWMPServiceProvider.getInstance().isScreenOff();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "StandBy: onGet: isScreenOff="+isScreenOff);
                    }
                    int value = 1;
                    if(isScreenOff){
                        value = 2;
                    }
                    try {
                        root.x_ttbb_control.StandBy.setValue(value);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            param.addSetListener(new SetParameterListener() {
                @Override
                public void onSet(Object value) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "StandBy: onSet: value=" + value);
                    }
                    if(value instanceof Integer) {
                        Integer command = (Integer) value;
//                        if(command == 1) { // 대기모드에서 시청모드로 ON
//                        } else if(command == 2) { // 시청모드에서 대기모드로 OFF
//                        }
                        try {
                            CWMPController.getInstance().getCWMPEventListener().onSetStandBy(command);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

//    private void setScreenResolution() {
//        CWMPParameter param = root.x_ttbb_control.ScreenResolution;
//        if(param != null) {
//            if (Log.INCLUDE) {
//                Log.d(TAG, "ScreenResolution: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
//            }
//            param.addSetListener(new SetParameterListener() {
//                @Override
//                public void onSet(Object value) {
//                    if (Log.INCLUDE) {
//                        Log.d(TAG, "ScreenResolution: onSet: value=" + value);
//                    }
//                    if(value instanceof Integer) {
//                        Integer command = (Integer) value;
////                        if(command == 1) { // 480i
////                        } else if(command == 2) { // 480p
////                        } else if(command == 3) { // 720p
////                        } else if(command == 4) { // 1080i
////                        } else if(command == 5) { // 1080p
////                        } else if(command == 6) { // 2160P30
////                        } else if(command == 7) { // 2160P60
////                        }
//
//                        try {
//                            CWMPController.getInstance().getCWMPEventListener().onSetScreenResolution(command);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            });
//        }
//    }

//    private void setHdmiSet() {
//        CWMPParameter param = root.x_ttbb_control.HdmiSet;
//        if(param != null) {
//            if (Log.INCLUDE) {
//                Log.d(TAG, "HdmiSet: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
//            }
//            param.addSetListener(new SetParameterListener() {
//                @Override
//                public void onSet(Object value) {
//                    if (Log.INCLUDE) {
//                        Log.d(TAG, "HdmiSet: onSet: value=" + value);
//                    }
//                    if(value instanceof Integer) {
//                        Integer command = (Integer) value;
//                        try {
//                            CWMPController.getInstance().getCWMPEventListener().onSetHdmiSet(command);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            });
//        }
//    }

    private void setAppRestart() {
        CWMPParameter param = root.x_ttbb_control.AppRestart;
        if (param != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "AppRestart: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
            }
            param.addSetListener(new SetParameterListener() {
                @Override
                public void onSet(Object value) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "AppRestart: onSet: value=" + value);
                    }
                    if (value != null && value instanceof String) {
                        String command = (String) value;
                        if (command.equals("0")) { // default
                        } else if (command.equals("1")) { // 앱 즉시 재시작
                            try {
                                root.x_ttbb_control.AppRestart.setValue("0");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                CWMPController.getInstance().getCWMPEventListener().onAppRestart();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }
    }

    private void setFwUpdateInfoUrl() {
        CWMPParameter param = root.x_ttbb_control.httpApi.FwUpdateInfoUrl;
        if (param != null) {
            try {
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwUpdateInfoUrl: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
                }
                Object object = param.getValue();
                String value = "";
                if (object != null && object instanceof String) {
                    value = (String) object;
                    if (value.isEmpty() || param.isDefault()) {
                        param.setValue(URL_FW_UPDATE_INFO);
                    } else {
                        URL_FW_UPDATE_INFO = value;
                    }
                }else{
                    param.setValue(URL_FW_UPDATE_INFO);
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "FwUpdateInfoUrl: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():""));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setApkMetaInfoUrl() {
        CWMPParameter param = root.x_ttbb_control.httpApi.ApkMetaInfoUrl;
        if (param != null) {
            try {
                if (Log.INCLUDE) {
                    Log.d(TAG, "apkMetaInfoUrl: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
                }
                Object object = param.getValue();
                String value = "";
                if (object != null && object instanceof String) {
                    value = (String) object;
                    if (value.isEmpty() || param.isDefault()) {
                        param.setValue(URL_APK_META_INFO);
                    } else {
                        URL_APK_META_INFO = value;
                    }
                }else{
                    param.setValue(URL_APK_META_INFO);
                }

                if (Log.INCLUDE) {
                    Log.d(TAG, "apkMetaInfoUrl: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():""));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setNormalPeriod() {
        CWMPParameter param = root.x_ttbb_diagMon.fwUpdate.NormalPeriod;

        if (param != null) {
            try {
                Object object = param.getValue();

                if (Log.INCLUDE) {
                    Log.d(TAG, "NormalPeriod: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
                }

                if (object != null && object instanceof Integer) {
                    Integer value = (Integer) object;

                    if (value > 0) {
                        normalPeriod = value;
                    }
                }

                if (Log.INCLUDE) {
                    Log.d(TAG, "NormalPeriod: " + normalPeriod);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            param.addSetListener(new SetParameterListener() {
                @Override
                public void onSet(Object value) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "NormalPeriod: onSet: value=" + value);
                    }

                    if (value instanceof Integer) {
                        try {
                            if ((Integer) value > 0) {
                                normalPeriod = (Integer) value;
                            }

                            CWMPController.getInstance().getCWMPEventListener().onNormalPeriodUpdated(normalPeriod);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private void setUpgradeTrigger() {
        CWMPParameter param = root.x_ttbb_control.X_TTBB_UpgradeTrigger;
        if(param != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "UpgradeTrigger: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
            }
            param.addSetListener(new SetParameterListener() {
                @Override
                public void onSet(Object value) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "UpgradeTrigger: onSet: value=" + value);
                    }
                    if(value instanceof Integer) {
                        Integer command = (Integer) value;
                        if(command == 0) { // default
                        } else if(command == 1) { // upgrade
                            try {
                                root.x_ttbb_control.X_TTBB_UpgradeTrigger.setValue(new Integer(0));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                CWMPController.getInstance().getCWMPEventListener().onUpgradeTrigger();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }
    }

    private void setFwUrlListener() {
        try {
            CWMPParameter param = root.managementServer.X_TTBB_FwUrl;
            if(param != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "X_TTBB_FwUrl: " + param.getFullName() + " >> value:" + (param.getValue() != null ? param.getValue().toString() : "") + ", isDefault:" + param.isDefault());
                }
                param.addSetListener(new SetParameterListener() {
                    @Override
                    public void onSet(Object value) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "X_TTBB_FwUrl: onSet: value=" + value);
                        }
                        if (value instanceof String) {
                            try {
                                CWMPController.getInstance().getCWMPEventListener().onFwUrlUpdated((String) value);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setForceApkUpdateListener() {
        try {
            CWMPParameter param = root.x_ttbb_diagMon.forceApkUpdate.UpdateState;
            if(param != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "X_TTBB_ForceApkUpdate: " + param.getFullName() + " >> value:" + (param.getValue() != null ? param.getValue().toString() : "") + ", isDefault:" + param.isDefault());
                }
                param.addSetListener(new SetParameterListener() {
                    @Override
                    public void onSet(Object value) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "X_TTBB_ForceApkUpdate: onSet: value=" + value);
                        }
                        try {
                            if(value instanceof String) {
                                String updateState = (String) value;
                                if (updateState.equals("Requested")) {
                                    CWMPController.getInstance().getCWMPEventListener().onApkUrlUpdated(updateState);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAcsUrlListener() {
        try {
            CWMPParameter param1 = root.managementServer.URL;
            if(param1 != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "URL: " + param1.getFullName() + " >> value:" + (param1.getValue() != null ? param1.getValue().toString() : "") + ", isDefault:" + param1.isDefault());
                }
                param1.addSetListener(new SetParameterListener() {
                    @Override
                    public void onSet(Object value) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "URL: onSet: value=" + value);
                        }
                        try {
                            CWMPController.getInstance().notifyBooting();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            CWMPParameter param2 = root.managementServer.SubURL;
            if(param2 != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "SubURL: " + param2.getFullName() + " >> value:" + (param2.getValue() != null ? param2.getValue().toString() : "") + ", isDefault:" + param2.isDefault());
                }
                param2.addSetListener(new SetParameterListener() {
                    @Override
                    public void onSet(Object value) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "SubURL: onSet: value=" + value);
                        }
                        try {
                            CWMPController.getInstance().notifyBooting();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDefaultSettingUrl() {
        CWMPParameter param = root.x_ttbb_control.httpApi.DefaultSettingUrl;
        if (param != null) {
            try {
                if (Log.INCLUDE) {
                    Log.d(TAG, "DefaultSettingUrl: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
                }
                Object object = param.getValue();
                String value = "";
                if (object != null && object instanceof String) {
                    value = (String) object;
                    if (value.isEmpty() || param.isDefault()) {
                        param.setValue(URL_DEFAULT_SETTING);
                    } else {
                        URL_DEFAULT_SETTING = value;
                    }
                }else{
                    param.setValue(URL_DEFAULT_SETTING);
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "DefaultSettingUrl: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():""));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void requestDefaultSetting(CWMPCallback cwmpCallback){
        if (Log.INCLUDE) {
            Log.d(TAG, "requestDefaultSetting");
        }
        if(this.root == null){
            if (Log.INCLUDE) {
                Log.d(TAG, "requestDefaultSetting: root is NOT available");
            }
            if(cwmpCallback != null) {
                cwmpCallback.onFailure();
            }
            return;
        }

        final String url = root.x_ttbb_control.httpApi.DefaultSettingUrl.getValue();
        if(url.isEmpty() || !url.contains("/")) {
            if (Log.INCLUDE) {
                Log.d(TAG, "requestDefaultSetting: DefaultSettingUrl is wrong: " + url);
            }
            if(cwmpCallback != null) {
                cwmpCallback.onFailure();
            }
            return;
        }

        String baseUrl = url.substring(0, url.lastIndexOf("/") + 1);
        String endUrl = url.substring(url.lastIndexOf("/") + 1);

        Retrofit retrofit;
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(getUnsafeOkHttpClient().build())
                .build();
        DefaultSettingApi defaultSettingApi = retrofit.create(DefaultSettingApi.class);

        String macAddress = root.wanDevices.getWanDevice(1).wanConnectionDevices.getWANConnectionDevice(1).wanipConnections.getWANIPConnection(1).MACAddress.getValue();
        String provisioningCode = root.deviceInfo.ProvisioningCode.getValue();
        String productClass = root.deviceInfo.ProductClass.getValue();
        String company = root.deviceInfo.Manufacturer.getValue();
        String model = root.deviceInfo.ModelName.getValue();
        String fwVersion = root.deviceInfo.SoftwareVersion.getValue();
        if(fwVersion.isEmpty()) fwVersion = "0.0.1";

        if (Log.INCLUDE) {
            Log.d(TAG, "requestDefaultSetting: baseUrl="+baseUrl);
            Log.d(TAG, "requestDefaultSetting: api="+endUrl);
            Log.d(TAG, "requestDefaultSetting: mac=" + macAddress + ", provisioningCode=" + provisioningCode + ", productClass=" + productClass + ", company=" + company + ", model=" + model + ", fwVersion=" + fwVersion);
        }

        defaultSettingApi.requestDefaultSetting(endUrl, macAddress, provisioningCode, productClass, company, model, fwVersion).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, @NonNull Response<String> response) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "requestDefaultSetting: onResponse");
                }
                if(response != null && response.isSuccessful()) {
                    try {
                        String body = response.body();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestDefaultSetting: body=" + body);
                        }
                        if(body != null) {
                            Gson builder = new GsonBuilder().create();
                            ResultDefaultSetting resultDefaultSetting = builder.fromJson(body, ResultDefaultSetting.class);
                            if(resultDefaultSetting != null) {
                                String code = resultDefaultSetting.getResultCode();
                                String message = resultDefaultSetting.getResultMessage();
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "requestDefaultSetting: code=" + code);
                                    Log.d(TAG, "requestDefaultSetting: message=" + message);
                                }
                                if("T".equalsIgnoreCase(code) && "SUCCESS".equalsIgnoreCase(message)){
                                    JSONObject dataJObject = new JSONObject(body).getJSONObject("data");
                                    Iterator<String> keys = dataJObject.keys();
                                    while (keys.hasNext()) {
                                        try {
                                            String key = keys.next();
                                            Object value = dataJObject.get(key);
                                            CWMPParameter cwmpParameter = CPEManager.getInstance().getParameter(key, root);
                                            if (Log.INCLUDE) {
                                                Log.d(TAG, "requestDefaultSetting: key=" + key + ", value=" + value+", cwmp parameter="+cwmpParameter);
                                            }
                                            if (value != null && cwmpParameter != null && cwmpParameter.isWritable()) {
                                                CWMPParameter.TYPE type = cwmpParameter.getType();
                                                if(type == CWMPParameter.TYPE.INT || type == CWMPParameter.TYPE.UNSIGNED_INT || type == CWMPParameter.TYPE.BOOLEAN){
                                                    cwmpParameter.setValue(Integer.parseInt((String) value));
                                                }else {
                                                    cwmpParameter.setValue(value);
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    if(cwmpCallback != null) {
                                        cwmpCallback.onSuccess();;
                                    }
                                    return;
                                }
                            }
                        }
                        else {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "requestDefaultSetting: Response body is null");
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                else {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "requestDefaultSetting: Response fail");
                    }
                }
                if(cwmpCallback != null) {
                    cwmpCallback.onFailure();
                }
            }

            @Override
            public void onFailure(Call<String> call, @NonNull Throwable t) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "requestDefaultSetting: onFailure : " + t.getMessage() + " (" + url + ")");
                }
                if(cwmpCallback != null) {
                    cwmpCallback.onFailure();
                }
            }
        });
    }

    private OkHttpClient.Builder getUnsafeOkHttpClient() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @NonNull
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }};
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init((KeyManager[])null, trustAllCerts, new SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            return builder;
        } catch (Exception var4) {
            throw new RuntimeException(var4);
        }
    }

    private interface DefaultSettingApi {
        @NonNull
        @POST
        @Headers("Content-Type: application/x-www-form-urlencoded")
        Call<String> requestDefaultSetting(@Url String url,
                                           @Query("macaddress") String macaddress,
                                           @Query("provisioningCode") String provisioningCode,
                                           @Query("productClass") String productClass,
                                           @Query("company") String company,
                                           @Query("model") String model,
                                           @Query("fwversion") String fwversion);
    }

    private class ResultDefaultSetting implements Parcelable {

        private final Creator<ResultDefaultSetting> CREATOR = new Creator<ResultDefaultSetting>() {
            @Override
            public ResultDefaultSetting createFromParcel(Parcel in) {
                return new ResultDefaultSetting(in);
            }

            @Override
            public ResultDefaultSetting[] newArray(int size) {
                return new ResultDefaultSetting[size];
            }
        };

        @SerializedName("result")
        @Expose
        private String code;
        @SerializedName("resultmessage")
        @Expose
        private String message;

        private ResultDefaultSetting(Parcel in) {
            code = in.readString();
            message = in.readString();
        }

        private String getResultCode() {
            return code;
        }

        private String getResultMessage() {
            return message;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(code);
            parcel.writeString(message);
        }
    }
}
