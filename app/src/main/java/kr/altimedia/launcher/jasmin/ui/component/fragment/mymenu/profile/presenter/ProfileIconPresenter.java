/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileIcon;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;

public class ProfileIconPresenter extends RowPresenter {
    private final String TAG = ProfileIconPresenter.class.getSimpleName();

    private LoadWorkPosterListener mLoadWorkPosterListener;

    public ProfileIconPresenter() {
        super();
    }

    public void setLoadWorkPosterListener(LoadWorkPosterListener loadMoviePosterListener) {
        mLoadWorkPosterListener = loadMoviePosterListener;
    }

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ProfileIconViewHolder mProfileIconViewHolder = new ProfileIconViewHolder(inflater.inflate(R.layout.item_mymenu_profile_icon, null, false));
        return mProfileIconViewHolder;
    }

    @Override
    protected void onBindRowViewHolder(ViewHolder viewHolder, Object item) {
        super.onBindRowViewHolder(viewHolder, item);

        ProfileIcon work = (ProfileIcon) item;

        if (Log.INCLUDE) {
            Log.d(TAG, "poster Url " + work.getPath());
        }
        ProfileIconViewHolder cardView = (ProfileIconViewHolder) viewHolder;

        Glide.with(viewHolder.view.getContext())
                .load(work.getPath()).placeholder(R.drawable.profile_default).
                error(R.drawable.profile_default).diskCacheStrategy(DiskCacheStrategy.DATA)
                .centerCrop()
                .into(cardView.getMainImageView());

        if (mLoadWorkPosterListener != null) {
            mLoadWorkPosterListener.onLoadWorkPoster(work, cardView.getMainImageView());
        }
    }

    @Override
    protected void onUnbindRowViewHolder(ViewHolder viewHolder) {
        super.onUnbindRowViewHolder(viewHolder);

        ProfileIconViewHolder cardView = (ProfileIconViewHolder) viewHolder;
        cardView.removeMainImage();
    }

    public interface LoadWorkPosterListener {
        void onLoadWorkPoster(ProfileIcon work, ImageView imageView);
    }

    private static class ProfileIconViewHolder extends ViewHolder {
        private final String TAG = ProfileIconViewHolder.class.getSimpleName();
        private ImageView profileIcon = null;

        public ProfileIconViewHolder(View view) {
            super(view);
            profileIcon = view.findViewById(R.id.profileIcon);
        }

        public ImageView getMainImageView() {
            return profileIcon;
        }

        public void removeMainImage() {
            profileIcon.setImageDrawable(null);
        }
    }
}