/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtpId implements Parcelable {
    public static final Creator<OtpId> CREATOR = new Creator<OtpId>() {
        @Override
        public OtpId createFromParcel(Parcel in) {
            return new OtpId(in);
        }

        @Override
        public OtpId[] newArray(int size) {
            return new OtpId[size];
        }
    };
    @Expose
    @SerializedName("loginId")
    private String loginId;
    @Expose
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @Expose
    @SerializedName("email")
    private String email;

    protected OtpId(Parcel in) {
        loginId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(loginId);
    }

    public String getOtpId() {
        if (email != null && !email.isEmpty()) {
            return email;
        } else {
            return phoneNumber;
        }
    }

    @Override
    public String toString() {
        return "OtpId{" +
                "loginId='" + loginId + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", otpId='" + getOtpId() + '\'' +
                '}';
    }
}