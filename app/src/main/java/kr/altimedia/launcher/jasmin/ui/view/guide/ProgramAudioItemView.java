package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.util.Log;
import com.altimedia.util.time.Clock;

import java.util.Date;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.tv.interactor.TvViewInteractor;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class ProgramAudioItemView extends RelativeLayout implements ProgramGuide.ChannelPropertyChangeListener {
    private static final String TAG = ProgramAudioItemView.class.getSimpleName();
    private final OnKeyListener ON_KEY = new OnKeyListener() {
        @Override
        public boolean onKey(View view, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (event.getAction() == KeyEvent.ACTION_UP) {
                    view.post(
                            () -> {
                                mProgramGuide.requestBackPressed(view);
                            });
                }

                return true;
            }
            return false;
        }
    };
    private final OnClickListener ON_CLICKED =
            new OnClickListener() {
                @Override
                public void onClick(final View view) {
                    ProgramManager.TableEntry entry = ((ProgramAudioItemView) view).mTableEntry;
                    if (entry == null) {
                        return;
                    }

                    if (!(view.getContext() instanceof TvViewInteractor)) {
                        return;
                    }

                    final LiveTvActivity tvActivity = (LiveTvActivity) view.getContext();
                    final Channel channel =
                            tvActivity.getChannelDataManager().getChannel(entry.channelId);

                    view.post(
                            () -> {
                                mProgramGuide.requestTuneChannel(channel, entry.program, new Handler(Looper.getMainLooper()) {
                                });
                            });
                }
            };
    private static int sVisibleThreshold;
    private static int sCompoundDrawablePadding;

    ChannelDataManager mChannelDataManager;
    ProgramManager mProgramManager;
    private ProgramGuide mProgramGuide;
    private ProgramManager.TableEntry mTableEntry;
    private String mGapTitle = null;
//    private int mMaxWidthForRipple;
//    private int mTextWidth;

    // If set this flag disables requests to re-layout the parent view as a result of changing
    // this view, improving performance. This also prevents the parent view to lose child focus
    // as a result of the re-layout (see b/21378855).
    private boolean mPreventParentRelayout;
    private final UpdateFocus mUpdateFocus = new UpdateFocus(this);
    private final NotifyFocus mNotifyFocus = new NotifyFocus(this);

    private final long UPDATE_DELAY_TIME = 50;
    private final long FOCUS_DELAY_TIME = 300;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private final OnFocusChangeListener ON_FOCUS_CHANGED =
            new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    mUpdateFocus.setFocus(hasFocus);
                    mNotifyFocus.setFocus(hasFocus);
                    mHandler.removeCallbacks(mUpdateFocus);

                    if (hasFocus) {
                        mHandler.post(mUpdateFocus);
                        mHandler.postDelayed(mNotifyFocus, FOCUS_DELAY_TIME);
                    } else {
                        mHandler.post(mUpdateFocus);
                        mHandler.post(mNotifyFocus);
                    }
                }


            };
    private Clock mClock;
    private final View mBgLayout;
    private final String emptyProgramTitle;


    private final TextView programTitle;
    private final View iconAudioPlay;

    private final int COLOR_FFFFFFFF;
    private final int COLOR_FFA67C52;
    private final int COLOR_CCE6E6E6;
    private final Context mContext;
    private final ProgramDataManager programDataManager;

    public ProgramAudioItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        mClock = TvSingletons.getSingletons(context).getClock();
        mChannelDataManager = TvSingletons.getSingletons(context).getChannelDataManager();
        programDataManager = TvSingletons.getSingletons(context).getProgramDataManager();
        setOnClickListener(ON_CLICKED);
        setOnKeyListener(ON_KEY);
        setOnFocusChangeListener(ON_FOCUS_CHANGED);
        LayoutInflater inflater = LayoutInflater.from(context);
        mProgramItemView = inflater.inflate(R.layout.view_audio_program_item, this, true);
        mBgLayout = mProgramItemView.findViewById(R.id.bgLayout);
        sCompoundDrawablePadding = context.getResources().getDimensionPixelOffset(R.dimen.program_guide_title_badge);
        emptyProgramTitle = getResources().getString(R.string.program_title_for_no_information);

        programTitle = mProgramItemView.findViewById(R.id.programTitle);
        iconAudioPlay = mProgramItemView.findViewById(R.id.iconAudioPlay);

        COLOR_FFFFFFFF = getContext().getColor(R.color.color_FFFFFFFF);
        COLOR_FFA67C52 = getContext().getColor(R.color.color_FFA67C52);
        COLOR_CCE6E6E6 = getContext().getColor(R.color.color_CCE6E6E6);

    }

    public ChannelDataManager getChannelDataManager() {
        return mChannelDataManager;
    }

    public void notifyFocusChanged(boolean focus) {
        if (mProgramGuide == null || mProgramGuide.getProgramManager() == null) {
            return;
        }
        if (focus) {
            mProgramGuide.getProgramManager().getOnInformationInteractor().onItemSelected(this);
        } else {
            mProgramGuide.getProgramManager().getOnInformationInteractor().onItemUnSelected(this);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyFocusChanged : " + focus);
        }
    }


    public ProgramAudioItemView(Context context) {
        this(context, null);
    }

    public ProgramAudioItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    private View mProgramItemView;

    public void setVisibleSubTitle(boolean focus) {
        if (mProgramGuide == null || mProgramGuide.getProgramManager() == null) {
            return;
        }
        ProgramManager.TableEntry tableEntry = getTableEntry();
        if (tableEntry.isBlocked()) {
            programTitle.setTextColor(focus ? COLOR_FFFFFFFF : COLOR_FFA67C52);
        } else {
            programTitle.setTextColor(focus ? COLOR_FFFFFFFF : COLOR_CCE6E6E6);
        }
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        return super.onCreateDrawableState(extraSpace);
    }

    public ProgramManager.TableEntry getTableEntry() {
        return mTableEntry;
    }

    private final int WHAT_UPDATED_PROGRAM = 12;
    private Handler mProgramUpdateHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case WHAT_UPDATED_PROGRAM:
                    setValues(mProgramGuide, mTableEntry, mProgramManager, mGapTitle);
                    break;
            }
        }
    };
    private final long mDefaultUpdateTime = 3 * TimeUtil.MIN;

    public void setValues(
            ProgramGuide programGuide,
            ProgramManager.TableEntry entry,
            ProgramManager programManager,
            String gapTitle) {
        mProgramGuide = programGuide;
        mTableEntry = entry;
        mProgramManager = programManager;
        mGapTitle = gapTitle;
        long fromUtcMillis = programManager.getFromUtcMillis();
        long toUtcMillis = programManager.getToUtcMillis();
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams != null) {
            layoutParams.width = entry.getWidth();
            setLayoutParams(layoutParams);
        }
        programGuide.addChannelPropertyChangeListener(this);
        Channel currentChannel = programGuide.getTuneChannel();
        iconAudioPlay.setVisibility(currentChannel.getId() == mTableEntry.channelId ? View.VISIBLE : View.INVISIBLE);
        Program program = programDataManager.getCurrentProgram(mTableEntry.channelId);
        if (Log.INCLUDE) {
            Log.d(TAG, "program : " + (program != null ? "programTitle" + program.getTitle() : "not null"));
        }
        long currentTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
        mProgramUpdateHandler.removeMessages(WHAT_UPDATED_PROGRAM);
        if (program != null) {
            long delayTime = program.getEndTimeUtcMillis() - currentTime;
            mProgramUpdateHandler.sendEmptyMessageDelayed(WHAT_UPDATED_PROGRAM, delayTime);
        } else {
            mProgramUpdateHandler.sendEmptyMessageDelayed(WHAT_UPDATED_PROGRAM, mDefaultUpdateTime);
        }
        String title = program != null ? program.getTitle() : null;

        if (TextUtils.isEmpty(title)) {
            title = emptyProgramTitle;
        }
        mBgLayout.setBackgroundResource(R.drawable.selector_grid_live_bg);
        if (Log.INCLUDE) {
            Log.d(TAG, "setValues | fromUtcMillis : " + new Date(fromUtcMillis));
            Log.d(TAG, "setValues | toUtcMillis : " + new Date(toUtcMillis));
        }
        mUiUpdater = new UiUpdater(this, programTitle, mTableEntry, title, COLOR_FFFFFFFF, COLOR_FFA67C52, COLOR_CCE6E6E6, mChannelDataManager);
        mHandler.removeCallbacks(mUiUpdater);
        mHandler.postDelayed(mUiUpdater, 300);
    }


    private UiUpdater mUiUpdater = null;

    private static class UiUpdater implements Runnable {
        private final ProgramAudioItemView itemView;
        private final TextView titleView;
        private final ProgramManager.TableEntry entry;
        private final String title;
        private final int COLOR_FFA67C52;
        private final int COLOR_CCE6E6E6;
        private final int COLOR_FFFFFFFF;
        private final ChannelDataManager mChannelDataManager;

        public UiUpdater(ProgramAudioItemView itemView, TextView titleView, ProgramManager.TableEntry entry, String title, int COLOR_FFFFFFFF,
                         int COLOR_FFA67C52, int COLOR_CCE6E6E6, ChannelDataManager channelDataManager) {
            this.itemView = itemView;
            this.titleView = titleView;
            this.entry = entry;
            this.title = title;
            this.COLOR_FFFFFFFF = COLOR_FFFFFFFF;
            this.COLOR_FFA67C52 = COLOR_FFA67C52;
            this.COLOR_CCE6E6E6 = COLOR_CCE6E6E6;
            this.mChannelDataManager = channelDataManager;
        }

        @Override
        public void run() {
            if (Log.INCLUDE) {
                Log.d(TAG, "call UiUpdater run");
            }
            updateText(title, entry);
        }

        private void updateText(String title, ProgramManager.TableEntry tableEntry) {
            titleView.setText(title);
            if (tableEntry.isBlocked()) {
                titleView.setTextColor(COLOR_FFA67C52);
            } else {
                titleView.setTextColor(COLOR_CCE6E6E6);
            }

            boolean isFocused = itemView.isFocused();


            if (tableEntry.isBlocked()) {
                titleView.setTextColor(isFocused ? COLOR_FFFFFFFF : COLOR_FFA67C52);
            } else {
                titleView.setTextColor(isFocused ? COLOR_FFFFFFFF : COLOR_CCE6E6E6);
            }
        }

    }


    public void clearValues() {
        if (mUiUpdater != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "call clearValue so remove Callback");
            }
            mHandler.removeCallbacks(mUiUpdater);
            mUiUpdater = null;
        }
        mProgramUpdateHandler.removeMessages(WHAT_UPDATED_PROGRAM);
        mProgramGuide.removeChannelPropertyChangeListener(this);
        programTitle.setText("");
        programTitle.setCompoundDrawables(null, null, null, null);
        programTitle.setTextColor(COLOR_CCE6E6E6);
        setTag(null);
        mProgramGuide = null;
        mTableEntry = null;

    }

    private static class UpdateFocus implements Runnable {
        private final ProgramAudioItemView itemView;
        private boolean focus = false;

        public UpdateFocus(ProgramAudioItemView itemView) {
            this.itemView = itemView;
        }

        public void setFocus(boolean focus) {
            this.focus = focus;
        }

        @Override
        public void run() {
            itemView.setVisibleSubTitle(focus);
        }
    }

    private static class NotifyFocus implements Runnable {
        private final ProgramAudioItemView itemView;
        private boolean focus = false;

        public NotifyFocus(ProgramAudioItemView itemView) {
            this.itemView = itemView;
        }

        public void setFocus(boolean focus) {
            this.focus = focus;
        }

        @Override
        public void run() {
            itemView.notifyFocusChanged(focus);
        }
    }

    @Override
    public void requestChildFocus(View child, View focused) {
        super.requestChildFocus(child, focused);
        if (Log.INCLUDE) {
            Log.d(TAG, "requestChildFocus : " + focused);
        }
    }

    @Override
    public void requestLayout() {
        if (mPreventParentRelayout) {
            // Trivial layout, no need to tell parent.
            forceLayout();
        } else {
            super.requestLayout();
        }
    }

    @Override
    public void onChannelChanged(Channel channel) {
        iconAudioPlay.setVisibility(channel.getId() == mTableEntry.channelId ? View.VISIBLE : View.INVISIBLE);
    }
}