package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;

public class CreditCardOtherPresenter extends Presenter {

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_credit_card_other, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((CreditCard) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private ImageView logo;
        private TextView name;

        public ViewHolder(View view) {
            super(view);

            initView(view);
        }

        private void initView(View view) {
            logo = view.findViewById(R.id.logo);
            name = view.findViewById(R.id.name);
        }

        public void setData(CreditCard creditCard) {
            String logoUri = creditCard.getCreditCardLogo();
            if (logoUri != null && !logoUri.isEmpty()) {
                Glide.with(view.getContext())
                        .load(logoUri).diskCacheStrategy(DiskCacheStrategy.DATA)
                        .into(logo);
                logo.setVisibility(View.VISIBLE);
            } else {
                name.setText(creditCard.getCreditCardTypeName());
                name.setVisibility(View.VISIBLE);
            }
        }
    }
}