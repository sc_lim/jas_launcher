/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PlacementDecision;

/**
 * Created by mc.kim on 16,06,2020
 */
public class OTUResponse extends BaseResponse {
    public static final Creator<OTUResponse> CREATOR = new Creator<OTUResponse>() {
        @Override
        public OTUResponse createFromParcel(Parcel in) {
            return new OTUResponse(in);
        }

        @Override
        public OTUResponse[] newArray(int size) {
            return new OTUResponse[size];
        }
    };
    @Expose
    @SerializedName("data")
    private OTUResult mOTUResult;

    protected OTUResponse(Parcel in) {
        super(in);
        mOTUResult = in.readParcelable(OTUResult.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(mOTUResult, flags);
    }


    @Override
    public String toString() {
        return "OTUResponse{" +
                "mOTUResult=" + mOTUResult +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public List<String> getOTUList() {
        return mOTUResult.pathList;
    }

    public void setOTUList(List<String> urlList) {
        mOTUResult.pathList = urlList;
    }

    public String getOTU() {
        return mOTUResult.pOtu;
    }

    public List<PlacementDecision> getAdvertisementList() {
        return mOTUResult.getPlacementDecision();
    }

    private static class OTUResult implements Parcelable {
        public static final Creator<OTUResult> CREATOR = new Creator<OTUResult>() {
            @Override
            public OTUResult createFromParcel(Parcel in) {
                return new OTUResult(in);
            }

            @Override
            public OTUResult[] newArray(int size) {
                return new OTUResult[size];
            }
        };
        @Expose
        @SerializedName("fullPath")
        private List<String> pathList;
        @Expose
        @SerializedName("otu")
        private String pOtu;

        @SerializedName("placementDecision")
        @Expose
        private List<PlacementDecision> placementDecision = null;


        protected OTUResult(Parcel in) {
            pathList = in.createStringArrayList();
            pOtu = in.readString();
            in.readList(this.placementDecision, (kr.altimedia.launcher.jasmin.dm.contents.obj.PlacementDecision.class.getClassLoader()));
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeStringList(pathList);
            dest.writeString(pOtu);
            dest.writeList(placementDecision);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public List<String> getPathList() {
            return pathList;
        }

        public String getOtu() {
            return pOtu;
        }

        public List<PlacementDecision> getPlacementDecision() {
            return placementDecision;
        }

        @Override
        public String toString() {
            return "OTUResult{" +
                    "pathList=" + pathList +
                    ", pOtu='" + pOtu + '\'' +
                    '}';
        }
    }
}
