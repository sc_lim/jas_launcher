/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.util.Log;
import com.altimedia.util.time.Clock;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.tv.ParentalController;
import kr.altimedia.launcher.jasmin.tv.interactor.TvViewInteractor;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;


public class ProgramItemView extends RelativeLayout implements ProgramManager.TableEntriesUpdatedListener {
    private static final String TAG = ProgramItemView.class.getSimpleName();
    private final OnKeyListener ON_KEY = new OnKeyListener() {
        @Override
        public boolean onKey(View view, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (event.getAction() == KeyEvent.ACTION_UP) {
                    view.post(
                            () -> {
                                mProgramGuide.requestBackPressed(view);
                            });
                }

                return true;
            }
            return false;
        }
    };
    private final OnClickListener ON_CLICKED =
            new OnClickListener() {
                @Override
                public void onClick(final View view) {
                    ProgramManager.TableEntry entry = ((ProgramItemView) view).mTableEntry;
                    if (entry == null) {
                        return;
                    }

                    if (!(view.getContext() instanceof TvViewInteractor)) {
                        return;
                    }

                    final LiveTvActivity tvActivity = (LiveTvActivity) view.getContext();
                    final Channel channel =
                            tvActivity.getChannelDataManager().getChannel(entry.channelId);

                    if (isBlocked(entry)) {
                        view.post(
                                () -> {
                                    mProgramGuide.requestTuneChannel(channel, entry.program, new Handler(Looper.getMainLooper()) {
                                    });
                                });
                    } else {
                        if (entry.isCurrentProgram()) {
                            view.post(
                                    () -> {
                                        mProgramGuide.requestTuneChannel(channel, entry.program, new Handler(Looper.getMainLooper()) {
                                        });
                                    });
                        } else if (entry.isFutureProgram()) {
                            view.post(
                                    () -> {
                                        mProgramGuide.setKeyBlock(true);
                                        mProgramGuide.requestReserveProgram(entry.program, new Handler(Looper.getMainLooper()) {
                                            @Override
                                            public void handleMessage(@NonNull Message msg) {
                                                super.handleMessage(msg);
                                                notifyFocusChanged(true);
                                                mProgramGuide.setKeyBlock(false);
                                            }
                                        });
                                    });
                        } else if (entry.isPastProgram()) {
                            view.post(
                                    () -> {
                                        mProgramGuide.requestTuneTimeShift(channel, entry.program, new Handler(Looper.getMainLooper()));
                                    });
                        }
                    }


                }
            };
    private static int sVisibleThreshold;
    private static int sCompoundDrawablePadding;

    ChannelDataManager mChannelDataManager;
    private ProgramGuide mProgramGuide;
    private ProgramManager.TableEntry mTableEntry;
    private ProgramManager mProgramManager;
//    private int mMaxWidthForRipple;
//    private int mTextWidth;

    // If set this flag disables requests to re-layout the parent view as a result of changing
    // this view, improving performance. This also prevents the parent view to lose child focus
    // as a result of the re-layout (see b/21378855).
    private boolean mPreventParentRelayout;
    private final UpdateFocus mUpdateFocus = new UpdateFocus(this);
    private final NotifyFocus mNotifyFocus = new NotifyFocus(this);

    private final long UPDATE_DELAY_TIME = 50;
    private final long FOCUS_DELAY_TIME = 300;
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private final OnFocusChangeListener ON_FOCUS_CHANGED =
            new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    mUpdateFocus.setFocus(hasFocus);
                    mNotifyFocus.setFocus(hasFocus);
                    mHandler.removeCallbacks(mUpdateFocus);

                    if (hasFocus) {
                        mHandler.post(mUpdateFocus);
                        mHandler.postDelayed(mNotifyFocus, FOCUS_DELAY_TIME);
                    } else {
                        mHandler.post(mUpdateFocus);
                        mHandler.post(mNotifyFocus);
                    }
                }


            };
    private final Clock mClock;
    private final View mBgLayout;
    private final String emptyProgramTitle;
    private final String mBlockedChannelTitle;
    private final String mRestrictChannelTitle;

    private final TextView programTitle;
    private final TextView programSubTitle;

    private final int COLOR_FFFFFFFF;
    private final int COLOR_FFA67C52;
    private final int COLOR_CCE6E6E6;
    private final Context mContext;

    public ProgramItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        mClock = TvSingletons.getSingletons(context).getClock();
        mChannelDataManager = TvSingletons.getSingletons(context).getChannelDataManager();
        setOnClickListener(ON_CLICKED);
        setOnKeyListener(ON_KEY);
        setOnFocusChangeListener(ON_FOCUS_CHANGED);
        LayoutInflater inflater = LayoutInflater.from(context);
        mProgramItemView = inflater.inflate(R.layout.view_program_item, this, true);

        mBgLayout = mProgramItemView.findViewById(R.id.bgLayout);
        sCompoundDrawablePadding = context.getResources().getDimensionPixelOffset(R.dimen.program_guide_title_badge);
        emptyProgramTitle = getResources().getString(R.string.program_title_for_no_information);
        mBlockedChannelTitle = getResources().getString(R.string.blocked_channel);
        mRestrictChannelTitle = getResources().getString(R.string.restricted_channel);
        programTitle = mProgramItemView.findViewById(R.id.programTitle);
        programSubTitle = mProgramItemView.findViewById(R.id.programSubTitle);

        COLOR_FFFFFFFF = getContext().getColor(R.color.color_FFFFFFFF);
        COLOR_FFA67C52 = getContext().getColor(R.color.color_FFA67C52);
        COLOR_CCE6E6E6 = getContext().getColor(R.color.color_CCE6E6E6);

    }

    public ChannelDataManager getChannelDataManager() {
        return mChannelDataManager;
    }

    public void notifyFocusChanged(boolean focus) {
        if (mProgramGuide == null || mProgramGuide.getProgramManager() == null) {
            return;
        }
        if (focus) {
            mProgramGuide.getProgramManager().getOnInformationInteractor().onItemSelected(this);
        } else {
            mProgramGuide.getProgramManager().getOnInformationInteractor().onItemUnSelected(this);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyFocusChanged : " + focus);
        }
    }

//    private final Runnable mUpdateFocus =
//            new Runnable() {
//                @Override
//                public void run() {
//                    ProgramManager.TableEntry entry = mTableEntry;
//                    if (entry == null) {
//                        // do nothing
//                        return;
//                    }
//                    Log.d(TAG, "mFocused : ");
//                }
//            };

    public ProgramItemView(Context context) {
        this(context, null);
    }

    public ProgramItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    private final View mProgramItemView;

    public void setVisibleSubTitle(boolean focus) {
        if (mProgramGuide == null || mProgramGuide.getProgramManager() == null) {
            return;
        }
        ProgramManager.TableEntry tableEntry = getTableEntry();
        Program program = tableEntry.program;

        if (isBlocked(tableEntry)) {
            programTitle.setTextColor(focus ? COLOR_FFFFFFFF : COLOR_FFA67C52);
            programSubTitle.setVisibility(View.GONE);
        } else {
            programTitle.setTextColor(focus ? COLOR_FFFFFFFF : COLOR_CCE6E6E6);
            if (program != null) {
                programSubTitle.setText(program.getDurationString(getContext()));
                programSubTitle.setVisibility(focus ? View.VISIBLE : View.GONE);
                if (focus) {
                    updateProgramVideoTypeIcon(programSubTitle, program);
                }
            } else {
                programSubTitle.setVisibility(View.GONE);
            }
        }
    }

    private static void updateProgramVideoTypeIcon(TextView textView, Program program) {
        int resId = getProgramVideoTypeIcon(program);
        if (resId != -1) {
            textView.setCompoundDrawablePadding(resId != 0 ? sCompoundDrawablePadding : 0);
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, resId, 0);
        }
    }

    private static int getProgramVideoTypeIcon(Program program) {
        int videoType = (program != null) ? program.getVideoType() : Program.VIDEO_TYPE_UNKNOWN;
        switch (videoType) {
            case Program.VIDEO_TYPE_SD:
                return R.drawable.icon_sd;
            case Program.VIDEO_TYPE_HD:
                return R.drawable.icon_hd;
            case Program.VIDEO_TYPE_UHD:
                return R.drawable.icon_uhd;
            default:
                return -1;
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        return super.onCreateDrawableState(extraSpace);
    }

    public ProgramManager.TableEntry getTableEntry() {
        return mTableEntry;
    }

    public void setValues(
            ProgramGuide programGuide,
            ProgramManager.TableEntry entry,
            ProgramManager programManager,
            String gapTitle) {
        mProgramGuide = programGuide;
        mTableEntry = entry;
        mProgramManager = programManager;
        long fromUtcMillis = programManager.getFromUtcMillis();
        long toUtcMillis = programManager.getToUtcMillis();
        mProgramManager.addTableEntriesUpdatedListener(this);
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams != null) {
            layoutParams.width = entry.getWidth();
            setLayoutParams(layoutParams);
        }
        String title = mTableEntry.program != null ? mTableEntry.program.getTitle() : null;
        if (mTableEntry.isGap()) {
            title = gapTitle;
        }

        if (isBlocked(mTableEntry)) {
            title = getBlockedTitle(mTableEntry);
        }
        if (TextUtils.isEmpty(title)) {
            title = emptyProgramTitle;
        }

        if (isBlocked(mTableEntry)) {
            mBgLayout.setBackgroundResource(R.drawable.selector_grid_live_bg);
        } else {
            if (mTableEntry.isCurrentProgram()) {
                mBgLayout.setBackgroundResource(R.drawable.selector_grid_live_bg);
            } else if (mTableEntry.isFutureProgram()) {
                mBgLayout.setBackgroundResource(R.drawable.selector_next_bg);
            } else {
                mBgLayout.setBackgroundResource(R.drawable.selector_grid_catchup_bg);
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "setValues | fromUtcMillis : " + new Date(fromUtcMillis));
            Log.d(TAG, "setValues | toUtcMillis : " + new Date(toUtcMillis));
        }
        if (mTableEntry.program != null) {
            Program program = mTableEntry.program;
            long startTimeUtcMillis = program.getStartTimeUtcMillis();
            long endTimeUtcMillis = program.getEndTimeUtcMillis();
            Log.d(TAG, "setValues | startTimeUtcMillis : " + new Date(startTimeUtcMillis));
            Log.d(TAG, "setValues | endTimeUtcMillis : " + new Date(endTimeUtcMillis));

            if (isNeedToShow(fromUtcMillis, toUtcMillis, startTimeUtcMillis, endTimeUtcMillis) || isBlocked(mTableEntry)) {
                mUiUpdater = new UiUpdater(this, programTitle, programSubTitle, mTableEntry, title, COLOR_FFFFFFFF, COLOR_FFA67C52, COLOR_CCE6E6E6, mChannelDataManager, mBroadcastReceiver);
                mHandler.removeCallbacks(mUiUpdater);
                mHandler.postDelayed(mUiUpdater, 300);
            }
        } else {
            mUiUpdater = new UiUpdater(this, programTitle, programSubTitle, mTableEntry, title, COLOR_FFFFFFFF, COLOR_FFA67C52, COLOR_CCE6E6E6, mChannelDataManager, mBroadcastReceiver);
            mHandler.removeCallbacks(mUiUpdater);
            mHandler.postDelayed(mUiUpdater, 300);
        }
    }

    public void clearValues() {
        if (mUiUpdater != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "call clearValue so remove Callback");
            }
            mHandler.removeCallbacks(mUiUpdater);
            mUiUpdater = null;
        }
        mProgramManager.removeTableEntriesUpdatedListener(this);
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mBroadcastReceiver);
        programTitle.setText("");
        programTitle.setCompoundDrawables(null, null, null, null);
        programTitle.setTextColor(COLOR_CCE6E6E6);
        programSubTitle.setText("");
        programSubTitle.setCompoundDrawables(null, null, null, null);

        setTag(null);
        mProgramGuide = null;
        mTableEntry = null;
    }

    private String getBlockedTitle(ProgramManager.TableEntry entry) {
        Channel channel = mChannelDataManager.getChannel(entry.channelId);
        if (mProgramGuide.isRestrictedChannel(channel)) {
            return mRestrictChannelTitle;
        } else if (mProgramGuide.isLockedChannel(channel)) {
            return mBlockedChannelTitle;
        }
        return "";
    }

    private boolean isUnLocked(ProgramManager.TableEntry entry) {
        Channel channel = mChannelDataManager.getChannel(entry.channelId);
        if (mProgramGuide == null) {
            return false;
        }
        return mProgramGuide.isUnLockedChannel(channel);
    }

    private boolean isBlocked(ProgramManager.TableEntry entry) {
        Channel channel = mChannelDataManager.getChannel(entry.channelId);
        if (mProgramGuide == null) {
            return false;
        }
        if (mProgramGuide.isRestrictedChannel(channel)) {
            return true;
        } else return mProgramGuide.isLockedChannel(channel);
    }

    private boolean isNeedToShow(long fromUtcMillis,
                                 long toUtcMillis, long startTimeUtcMillis, long endTimeUtcMillis) {
        long bufferedFromTime = fromUtcMillis - (90 * TimeUtil.MIN);
        long bufferedToTime = toUtcMillis + (90 * TimeUtil.MIN);

        if (bufferedFromTime <= endTimeUtcMillis && bufferedFromTime >= startTimeUtcMillis) {
            return true;
        } else if (bufferedFromTime <= startTimeUtcMillis && bufferedToTime >= endTimeUtcMillis) {
            return true;
        } else return bufferedToTime >= startTimeUtcMillis && bufferedToTime <= endTimeUtcMillis;
    }

    private UiUpdater mUiUpdater = null;

    private static class UiUpdater implements Runnable {
        private final ProgramItemView itemView;
        private final TextView titleView;
        private final TextView subTextView;
        private final ProgramManager.TableEntry entry;
        private final String title;
        private final int COLOR_FFA67C52;
        private final int COLOR_CCE6E6E6;
        private final int COLOR_FFFFFFFF;
        private final ChannelDataManager mChannelDataManager;
        private final BroadcastReceiver mBroadcastReceiver;

        public UiUpdater(ProgramItemView itemView, TextView titleView, TextView subTextView, ProgramManager.TableEntry entry, String title, int COLOR_FFFFFFFF,
                         int COLOR_FFA67C52, int COLOR_CCE6E6E6, ChannelDataManager channelDataManager, BroadcastReceiver broadcastReceiver) {
            this.itemView = itemView;
            this.titleView = titleView;
            this.subTextView = subTextView;
            this.entry = entry;
            this.title = title;
            this.mBroadcastReceiver = broadcastReceiver;
            this.COLOR_FFFFFFFF = COLOR_FFFFFFFF;
            this.COLOR_FFA67C52 = COLOR_FFA67C52;
            this.COLOR_CCE6E6E6 = COLOR_CCE6E6E6;
            this.mChannelDataManager = channelDataManager;
        }

        @Override
        public void run() {
            if (Log.INCLUDE) {
                Log.d(TAG, "call UiUpdater run");
            }
            LocalBroadcastManager.getInstance(titleView.getContext()).registerReceiver(mBroadcastReceiver, new IntentFilter(ReminderAlertManager.BOOK_EVENT_CHANGE));
            updateText(title, entry);
            itemView.updateIcons();
        }

        private void updateText(String title, ProgramManager.TableEntry tableEntry) {
            titleView.setText(title);
            if (itemView.isBlocked(tableEntry)) {
                titleView.setTextColor(COLOR_FFA67C52);
            } else {
                titleView.setTextColor(COLOR_CCE6E6E6);
            }

            boolean isFocused = itemView.isFocused();


            if (itemView.isBlocked(tableEntry)) {
                titleView.setTextColor(isFocused ? COLOR_FFFFFFFF : COLOR_FFA67C52);
                subTextView.setVisibility(View.GONE);
            } else {
                titleView.setTextColor(isFocused ? COLOR_FFFFFFFF : COLOR_CCE6E6E6);
                if (tableEntry.program != null) {
                    subTextView.setVisibility(isFocused ? View.VISIBLE : View.GONE);
                    if (isFocused) {
                        updateProgramVideoTypeIcon(subTextView, tableEntry.program);
                    }
                } else {
                    subTextView.setVisibility(View.GONE);
                }
            }
        }

    }


    private boolean isEntryWideEnough() {
        return mTableEntry != null && mTableEntry.getWidth() >= sVisibleThreshold;
    }

    private void updateIcons() {
        // Sets recording icons if needed.
        if (mTableEntry == null || mTableEntry.program == null) {
            return;
        }
        Program program = mTableEntry.program;
        boolean isPastProgram = mTableEntry.isPastProgram();
        final Channel channel = mChannelDataManager.getChannel(mTableEntry.channelId);
        boolean isWatchableTimeShift = isPastProgram && isTimeShiftEnabled(channel, program);
        boolean isReserved = ReminderAlertManager.getInstance().isReserved(program);
        boolean isParentalLock = !isUnLocked(mTableEntry) && ParentalController.isProgramRatingBlocked(program.getContentRatings());
        int iconResId = isReserved ? R.drawable.selector_icon_grid_alarm : (isParentalLock ? R.drawable.selector_icon_grid_lock : (isWatchableTimeShift ? R.drawable.selector_icon_grid_watch : 0));
        programTitle.setCompoundDrawablePadding(iconResId != 0 ? sCompoundDrawablePadding : 0);
        programTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, iconResId, 0);
    }

    private boolean isTimeShiftEnabled(Channel channel, Program program) {
        if (program == null) {
            return false;
        }
        if (channel == null) {
            return false;
        }
        if (channel.isPaidChannel()) {
            return false;
        }

        long timeShiftTimeMillis = channel != null ? channel.getTimeshiftService() : 0;
        if (Log.INCLUDE) {
            Log.d(TAG, "isTimeShiftEnabled | timeShiftTimeMillis : " + timeShiftTimeMillis);
        }
        long currentTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
        long availableTime = currentTime - timeShiftTimeMillis;
        return program.getEndTimeUtcMillis() > availableTime;
    }

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case ReminderAlertManager.BOOK_EVENT_CHANGE:
                    updateIcons();
                    break;
            }
        }
    };



    private static class UpdateFocus implements Runnable {
        private final ProgramItemView itemView;
        private boolean focus = false;

        public UpdateFocus(ProgramItemView itemView) {
            this.itemView = itemView;
        }

        public void setFocus(boolean focus) {
            this.focus = focus;
        }

        @Override
        public void run() {
            itemView.setVisibleSubTitle(focus);
        }
    }

    private static class NotifyFocus implements Runnable {
        private final ProgramItemView itemView;
        private boolean focus = false;

        public NotifyFocus(ProgramItemView itemView) {
            this.itemView = itemView;
        }

        public void setFocus(boolean focus) {
            this.focus = focus;
        }

        @Override
        public void run() {
            itemView.notifyFocusChanged(focus);
        }
    }

    @Override
    public void requestChildFocus(View child, View focused) {
        super.requestChildFocus(child, focused);
        if (Log.INCLUDE) {
            Log.d(TAG, "requestChildFocus : " + focused);
        }
    }

    @Override
    public void requestLayout() {
        if (mPreventParentRelayout) {
            // Trivial layout, no need to tell parent.
            forceLayout();
        } else {
            super.requestLayout();
        }
    }

    @Override
    public void onTableEntriesUpdated() {
        updateIcons();
    }
}
