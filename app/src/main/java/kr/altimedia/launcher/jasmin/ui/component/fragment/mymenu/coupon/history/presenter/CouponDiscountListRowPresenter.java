/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponStatus;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class CouponDiscountListRowPresenter extends Presenter {

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_coupon_history_discount_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        if (item == null) {
            viewHolder.view.setVisibility(View.INVISIBLE);
            return;
        }

        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((Coupon)item);
        viewHolder.view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private TextView purchaseDateView;
        private TextView discountPercentView;
        private TextView balanceView;
        private TextView balanceUnitView;
        private TextView expireDateView;
        private TextView expireUntilView;

        private final String UNLIMITED = "31.12.9999";

        public ViewHolder(View view) {
            super(view);

            initView(view);
        }

        private void initView(View view) {
            purchaseDateView = view.findViewById(R.id.purchaseDate);
            discountPercentView = view.findViewById(R.id.discountPercent);
            balanceView = view.findViewById(R.id.balance);
            balanceUnitView = view.findViewById(R.id.balanceUnit);
            expireDateView = view.findViewById(R.id.expireDate);
            expireUntilView = view.findViewById(R.id.expireUntil);
        }

        public void setData(Coupon item) {
            DiscountCoupon coupon = (DiscountCoupon)item;
            if(coupon.getRegisterDate() != null) {
                purchaseDateView.setText(TimeUtil.getModifiedDate(coupon.getRegisterDate().getTime()));
            }
            discountPercentView.setText(StringUtil.getFormattedNumber(coupon.getRate()));
            balanceUnitView.setVisibility(View.GONE);
            expireUntilView.setVisibility(View.GONE);
            CouponStatus status = coupon.getStatus();
            if(status != null) {
                switch (status) {
                    case USED: {
                        balanceView.setText(R.string.used);
                        setExpireDateView(coupon.getExpireDate());
                        break;
                    }
                    case EXPIRED:
                    case DESTROYED: {
                        balanceView.setText("-");
                        expireDateView.setText(R.string.expired);
                        break;
                    }
                    default: {
                        setBalanceView(coupon.getRate());
                        setExpireDateView(coupon.getExpireDate());
                    }
                }
            }
        }

        private void setBalanceView(int balance){
            balanceUnitView.setVisibility(View.VISIBLE);
            balanceView.setText(StringUtil.getFormattedNumber(balance));
        }

        private void setExpireDateView(Date date){
            if(date != null) {
                String dateStr = TimeUtil.getModifiedDate(date.getTime());
                if(UNLIMITED.equals(dateStr)){
                    expireDateView.setText(R.string.unlimited);
                }else {
                    expireUntilView.setVisibility(View.VISIBLE);
                    expireDateView.setText(dateStr);
                }
            }
        }
    }
}