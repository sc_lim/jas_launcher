package kr.altimedia.launcher.jasmin.ui.view.row;

import android.view.KeyEvent;
import android.view.View;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

import kr.altimedia.launcher.jasmin.system.settings.SettingControl;

public abstract class SideOptionRow implements View.OnKeyListener {
    private static final String TAG = SideOptionRow.class.getSimpleName();

    private final ArrayList<Integer> DEBUG_ON_KEY_LIST = new ArrayList<>(
            Arrays.asList(KeyEvent.KEYCODE_INFO, KeyEvent.KEYCODE_INFO, KeyEvent.KEYCODE_INFO, KeyEvent.KEYCODE_INFO));
    private final int DEBUG_ON_KEY = KeyEvent.KEYCODE_MEDIA_FAST_FORWARD;
    private final int DEBUG_OFF_KEY = KeyEvent.KEYCODE_MEDIA_REWIND;
    private final int DEBUG_KEY_LIST_SIZE = DEBUG_ON_KEY_LIST.size() + 1;

    private LinkedList<Integer> inputKeyList = new LinkedList<>();

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onKey() key:" + KeyEvent.keyCodeToString(keyCode) + ", getAction:" + event.getAction());
        }

        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return true;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_INFO:
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                checkDebugKey(keyCode);
                return true;
        }

        inputKeyList.clear();
        return false;
    }

    private void checkDebugKey(int keyCode) {
        inputKeyList.add(keyCode);
        int inputKeySize = inputKeyList.size();

        if (inputKeySize < DEBUG_KEY_LIST_SIZE) {
            return;
        }

        if (inputKeySize > DEBUG_KEY_LIST_SIZE) {
            inputKeyList.poll();
        }

        for (int i = 0; i < DEBUG_ON_KEY_LIST.size(); i++) {
            int inputKey = inputKeyList.get(i);
            if (inputKey != DEBUG_ON_KEY_LIST.get(i)) {
                return;
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "checkDebugKey, keyCode : " + keyCode);
        }

        if (keyCode == DEBUG_ON_KEY) {
            SettingControl.getInstance().setDebugInfo(true);
        } else if (keyCode == DEBUG_OFF_KEY) {
            SettingControl.getInstance().setDebugInfo(false);
        }
    }
}
