/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.recommend.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.def.EventFlagDeserializer;

/**
 * Created by mc.kim on 01,06,2020
 */
public class RcmdContent implements Parcelable {
    @Expose
    @SerializedName("SCORE")
    private String score;
    @Expose
    @SerializedName("SERIES_ID")
    private String seriesId;
    @Expose
    @SerializedName("ITEM_ID")
    private String itemId;
    @Expose
    @SerializedName("WON_YN")
    private String wonYn;
    @Expose
    @SerializedName("CAT_NAME")
    private String catName;
    @Expose
    @SerializedName("PARENT_CAT_ID")
    private String parentCatId;
    @Expose
    @SerializedName("ITEM_NAME")
    private String itemName;
    @Expose
    @SerializedName("FULL_PATH")
    private String fullPath;
    @Expose
    @SerializedName("IMG_URL")
    private String imgUrl;
    @Expose
    @SerializedName("NEW_HOT")
    @JsonAdapter(EventFlagDeserializer.class)
    private List<String> newHot;
    @Expose
    @SerializedName("ITEM_TYPE")
    private String itemType;
    @Expose
    @SerializedName("CAT_ID")
    private String catId;
    @Expose
    @SerializedName("RATING")
    private int rating;
    @Expose
    @SerializedName("SERIES_YN")
    private String seriesYn;
    @Expose
    @SerializedName("CONTS_TYPE")
    private String contentsType;
    @Expose
    @SerializedName("IS_HD")
    private String isHd;
    @Expose
    @SerializedName("HDR_YN")
    private String hdrYn;
    @Expose
    @SerializedName("IMG_EXT")
    private String imgExt;


    protected RcmdContent(Parcel in) {
        fullPath = in.readString();
        imgUrl = in.readString();
        parentCatId = in.readString();
        score = in.readString();
        this.newHot = new ArrayList<>();
        in.readList(this.newHot, String.class.getClassLoader());
        itemName = in.readString();
        itemType = in.readString();
        wonYn = in.readString();
        itemId = in.readString();
        isHd = in.readString();
        rating = in.readInt();
        seriesYn = in.readString();
        seriesId = in.readString();
        catName = in.readString();
        catId = in.readString();
        contentsType = in.readString();
        hdrYn = in.readString();
        imgExt = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fullPath);
        dest.writeString(imgUrl);
        dest.writeString(parentCatId);
        dest.writeString(score);
        dest.writeList(newHot);
        dest.writeString(itemName);
        dest.writeString(itemType);
        dest.writeString(wonYn);
        dest.writeString(itemId);
        dest.writeString(isHd);
        dest.writeInt(rating);
        dest.writeString(seriesYn);

        dest.writeString(seriesId);
        dest.writeString(catName);
        dest.writeString(catId);
        dest.writeString(contentsType);
        dest.writeString(hdrYn);
        dest.writeString(imgExt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RcmdContent> CREATOR = new Creator<RcmdContent>() {
        @Override
        public RcmdContent createFromParcel(Parcel in) {
            return new RcmdContent(in);
        }

        @Override
        public RcmdContent[] newArray(int size) {
            return new RcmdContent[size];
        }
    };

    public String getFullPath() {
        return fullPath;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getParentCatId() {
        return parentCatId;
    }

    public String getScore() {
        return score;
    }

    public List<String> getNewHot() {
        return newHot;
    }

    public String getItemName() {
        return itemName;
    }

    public String getItemType() {
        return itemType;
    }

    public String getWonYn() {
        return wonYn;
    }

    public String getItemId() {
        return itemId;
    }

    public String getIsHd() {
        return isHd;
    }

    public int getRating() {
        return rating;
    }

    public String getSeriesYn() {
        return seriesYn;
    }

    public String getSeriesId() {
        return seriesId;
    }

    public String getCatName() {
        return catName;
    }

    public String getCatId() {
        return catId;
    }

    public String getContentsType() {
        return contentsType;
    }

    public String getHdrYn() {
        return hdrYn;
    }

    public String getImgExt() {
        return imgExt;
    }

    @Override
    public String toString() {
        return "RcmdContent{" +
                "fullPath='" + fullPath + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", parentCatId='" + parentCatId + '\'' +
                ", score='" + score + '\'' +
                ", newHot=" + newHot +
                ", itemName='" + itemName + '\'' +
                ", itemType='" + itemType + '\'' +
                ", wonYn='" + wonYn + '\'' +
                ", itemId='" + itemId + '\'' +
                ", isHd='" + isHd + '\'' +
                ", rating='" + rating + '\'' +
                ", seriesYn='" + seriesYn + '\'' +
                '}';
    }
}
