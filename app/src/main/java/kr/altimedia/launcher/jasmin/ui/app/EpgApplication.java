/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.app;

import android.content.ContentResolver;
import android.media.tv.TvContract;
import android.os.AsyncTask;

import java.util.concurrent.Executor;

import com.altimedia.tvmodule.BaseApplication;
import com.altimedia.tvmodule.TifController;
import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.TvInputManagerHelper;

import static kr.altimedia.launcher.jasmin.ui.app.AppConfig.USE_CACHED_EPG_INFO;

public abstract class EpgApplication extends BaseApplication implements TvSingletons {
    ChannelDataManager mChannelDataManager;
    private volatile ProgramDataManager mProgramDataManager = null;
    private ContentResolver mContentResolver = null;
    TvInputManagerHelper mLazyTvInputManagerHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        mContentResolver = getContentResolver();
        if (!USE_CACHED_EPG_INFO) {
            mContentResolver.delete(TvContract.Channels.CONTENT_URI, null, null);
            mContentResolver.delete(TvContract.Programs.CONTENT_URI, null, null);
        }
        TifController.getInstance().init(this);
    }

    @Override
    public ChannelDataManager getChannelDataManager() {
        if (mChannelDataManager == null) {
            mChannelDataManager =
                    new ChannelDataManager(this,
                            getTvInputManagerHelper(), getDbExecutor(), getContentResolver());
        }
        return mChannelDataManager;
    }

    @Override
    public ProgramDataManager getProgramDataManager() {
        if (mProgramDataManager == null) {
            mProgramDataManager = new ProgramDataManager(getApplicationContext());
        }
        return mProgramDataManager;
    }


    @Override
    public boolean isRunningInMainProcess() {
        return true;
    }

    @Override
    public TvInputManagerHelper getTvInputManagerHelper() {
        if (mLazyTvInputManagerHelper == null) {
            mLazyTvInputManagerHelper = new TvInputManagerHelper(this);
            mLazyTvInputManagerHelper.start();
        }
        return mLazyTvInputManagerHelper;
    }

    @Override
    public Executor getDbExecutor() {
        return AsyncTask.SERIAL_EXECUTOR;
    }
}
