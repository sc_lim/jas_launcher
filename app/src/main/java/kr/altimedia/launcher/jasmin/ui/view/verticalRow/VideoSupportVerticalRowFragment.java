/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.verticalRow;


import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.altimedia.player.AltiMediaSource;
import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.WeakHashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.media.MediaPlayerAdapter;
import kr.altimedia.launcher.jasmin.media.VideoPlayerAdapter;
import kr.altimedia.launcher.jasmin.media.VideoState;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.BaseGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.ResourceLoader;

public class VideoSupportVerticalRowFragment extends VerticalRowsFragment {
    private final String TAG = VideoSupportVerticalRowFragment.class.getSimpleName();
    static final int SURFACE_NOT_CREATED = 0;
    static final int SURFACE_CREATED = 1;

    View mVideoRootView;

    SurfaceView mVideoSurface;
    ImageView mLoadingFrame;
    SurfaceHolder.Callback mMediaPlaybackCallback;

    int mState = SURFACE_NOT_CREATED;
    private ViewGroup root = null;

    private MediaPlayerAdapter playerAdapter = null;

    private TuneInfo currentTuneInfo;

    public void tryRequestTuneVod(boolean autoPlay, Drawable drawable, AltiMediaSource[] altiMediaSource) {
        mLoadingFrame.setImageDrawable(drawable);
        currentTuneInfo = generateTuneInfo(drawable, altiMediaSource);
        if (autoPlay) {
            setDataSource(altiMediaSource);
        }
    }

    public void clearPromotionVod() {
        currentTuneInfo = null;
        mLoadingFrame.setImageDrawable(null);
        playerAdapter.stop();
        mIframHandler.removeMessages(WHAT_SHOW);
        mIframHandler.removeMessages(WHAT_HIDE);
    }

    public void requestTuneVod(boolean autoPlay, String resourceUrl) {
        this.requestTuneVod(autoPlay, resourceUrl, null);
    }

    public void requestTuneVod(boolean autoPlay, String resourceUrl, AltiMediaSource[] altiMediaSource) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestTuneVod : " + resourceUrl);
        }
        ResourceLoader<String> mResourceLoader = new ResourceLoader<>();
        mResourceLoader.loadResource(this, resourceUrl, new ResourceLoader.ResourceCallback() {
            @Override
            public void onLoaded(Drawable drawable) {
                tryRequestTuneVod(autoPlay, drawable, altiMediaSource);
            }
        });
    }

    private TuneInfo getCurrentTuneInfo() {
        return currentTuneInfo;
    }


    private TuneInfo generateTuneInfo(Drawable drawable, AltiMediaSource[] altiMediaSource) {
        return new TuneInfo(drawable, altiMediaSource);
    }


    private void setLoadingResource(String loadingFrame) {
        Glide.with(getContext()).asDrawable().load(loadingFrame).diskCacheStrategy(DiskCacheStrategy.DATA).into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                mLoadingFrame.setImageDrawable(resource);
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                mLoadingFrame.setImageDrawable(placeholder);
            }
        });
    }

//    final ColorDrawable blackColorDrawable = new ColorDrawable(0xFF000000);

    private void setDataSource(AltiMediaSource[] altiMediaSource) {
        if (Log.INCLUDE) {
            Log.d(TAG, "called setDataSource");
        }
        if (altiMediaSource == null) {
//            mVideoSurface.setBackground(blackColorDrawable);
            if (playerAdapter.isPlaying()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "called setDataSource altiMediaSource == null playerAdapter.isPlaying()");
                }
                playerAdapter.stop();
            } else {
                if (Log.INCLUDE) {
                    Log.d(TAG, "called setDataSource altiMediaSource == null not playerAdapter.isPlaying() : " + (mOnVideoEventCallback != null));
                }
                if (mOnVideoEventCallback != null) {
                    mOnVideoEventCallback.onPlayStop(playerAdapter);
                }
            }
        } else {
            if (Log.INCLUDE) {
                Log.d(TAG, "called setDataSource altiMediaSource != null");
            }
            if (mVideoSurface != null) {
                mVideoSurface.setBackground(null);
            }
            playerAdapter.resetInfo();
            playerAdapter.setDataSource(altiMediaSource);
        }
    }

    private VideoState mCurrentState = null;

    public boolean isPlaying() {
        if (playerAdapter != null) {
            return playerAdapter.isPlaying();
        }
        return false;
    }

    public void showLastThumbnail() {
        if (mLoadingFrame.getAlpha() == 0) {
            mIframHandler.removeMessages(WHAT_SHOW);
            mIframHandler.removeMessages(WHAT_HIDE);
            mIframHandler.sendEmptyMessage(WHAT_SHOW);
        }
    }

    public void stopPlayer() {
        if (playerAdapter != null) {
            playerAdapter.stop();
        }
    }

    public void mute(boolean mute) {
        if (playerAdapter != null) {
            playerAdapter.muteToggle(mute);
        }
    }

    private WeakHashMap<View, Animator> viewAnimator = new WeakHashMap<>();


    public void releasePlayer() {
        if (playerAdapter != null) {
            if (mOnVideoEventCallback != null) {
                mOnVideoEventCallback.onRelease(playerAdapter);
            }
            playerAdapter.setCallback(null);
            playerAdapter.release();
        }
    }

    private OnVideoEventCallback mOnVideoEventCallback = null;

    public void setOnVideoEventCallback(OnVideoEventCallback onVideoEventCallback) {
        this.mOnVideoEventCallback = onVideoEventCallback;
    }

    private final int WHAT_HIDE = 1;
    private final int WHAT_SHOW = 2;
    private Handler mIframHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            int what = msg.what;
            if (Log.INCLUDE) {
                Log.d(TAG, "handleMessage : " + what);
            }
            switch (what) {
                case WHAT_HIDE:
                    startFadeAnimation(mLoadingFrame, R.animator.fadeout);
                    break;

                case WHAT_SHOW:
                    startFadeAnimation(mLoadingFrame, R.animator.fadein);
                    break;
            }
        }
    };

    protected float currentScrollOffset = 0;

    public void resumePlayer(boolean checkState) {
        if (playerAdapter != null && currentTuneInfo != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "resumePlayer : " + playerAdapter.isPlaying() +
                        ", mCurrentState : " + mCurrentState + ", checkState : " + checkState);
            }

            if (playerAdapter.isPlaying()) {
                if (checkState && mCurrentState != VideoState.STOP) {
                    return;
                }
            }
            if (!isPlayerVisible()) {
                return;
            }
            onAttachedPromotion();
            tryRequestTuneVod(true, currentTuneInfo.mLoadingFrame, currentTuneInfo.getAssetFileDescriptor());
        }
    }

    public void onAttachedPromotion() {
        if (mOnVideoEventCallback != null) {
            mOnVideoEventCallback.onAttached(playerAdapter);
        }
    }

    public void onDetachedPromotion() {
        if (mOnVideoEventCallback != null) {
            mOnVideoEventCallback.onDetached(playerAdapter);
        }
    }

    private void startFadeAnimation(View target, int resourceId) {
        if (target == null) {
            return;
        }

        if (viewAnimator.containsKey(target)) {
            viewAnimator.get(target).cancel();
        }

        boolean isShowing = resourceId == R.animator.fadein;
        if (isShowing) {
            target.setAlpha(1f);
            return;
        } else {
            target.setAlpha(1f);
        }
        Animator targetAnimation =
                AnimatorInflater.loadAnimator(target.getContext(), resourceId);

        targetAnimation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                viewAnimator.remove(target);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                viewAnimator.put(target, animation);
            }
        });

        targetAnimation.setTarget(target);
        targetAnimation.start();
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);

        mVideoRootView = LayoutInflater.from(getContext()).inflate(
                R.layout.layout_video_surface, root, false);
        mVideoSurface = mVideoRootView.findViewById(R.id.video_surface);
        mVideoSurface.setBackgroundColor(Color.argb(0x00, 0, 0, 0));
        mVideoSurface.getHolder().setFormat(PixelFormat.TRANSPARENT);
        mLoadingFrame = mVideoRootView.findViewById(R.id.loadingFrame);
        root.addView(mVideoRootView, 0);
        return root;
    }

    private void fireVideoEvent(VideoState state) {
        if (Log.INCLUDE) {
            Log.d(TAG, "fireVideoEvent : " + state);
        }
        mCurrentState = state;
        switch (state) {
            case READY:
                mIframHandler.removeMessages(WHAT_HIDE);
                mIframHandler.sendEmptyMessageDelayed(WHAT_HIDE, 2000);
                break;
            case STOP:
            case PREPARE:
                mIframHandler.removeMessages(WHAT_SHOW);
                mIframHandler.removeMessages(WHAT_HIDE);
                mIframHandler.sendEmptyMessage(WHAT_SHOW);
                break;
            case PAUSE:

            case COMPLETE:

                break;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        playerAdapter = new MediaPlayerAdapter(view.getContext());
        playerAdapter.setSurfaceView(mVideoSurface);
        playerAdapter.setScaleMode(false);
        mVideoSurface.getHolder().addCallback(playerAdapter.getVideoPlayerSurfaceHolderCallback());
        mVideoSurface.getHolder().addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceCreated(holder);
                }
                mState = SURFACE_CREATED;
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceChanged(holder, format, width, height);
                }
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceDestroyed(holder);
                }
                mState = SURFACE_NOT_CREATED;
            }
        });

        playerAdapter.setCallback(new VideoPlayerAdapter.Callback() {

            @Override
            public void onPlayStateChanged(VideoPlayerAdapter adapter, VideoState state) {
                super.onPlayStateChanged(adapter, state);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onPlayStateChanged : " + state);
                }
                if (isPlayerVisible()) {
                    fireVideoEvent(state);
                }
                if (mOnVideoEventCallback != null) {
                    switch (state) {
                        case READY:
                            mOnVideoEventCallback.onReady(adapter);
                            break;
                        case COMPLETE:
                            mOnVideoEventCallback.onPlayCompleted(adapter);
                            break;
                        case STOP:
                            mOnVideoEventCallback.onPlayStop(adapter);
                            break;
                    }
                }
            }

            @Override
            public void onPreparedStateChanged(VideoPlayerAdapter adapter) {
                super.onPreparedStateChanged(adapter);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onPreparedStateChanged");
                }
            }

            @Override
            public void onPlayCompleted(VideoPlayerAdapter adapter) {
                super.onPlayCompleted(adapter);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onPlayCompleted");
                }
            }

            @Override
            public void onCurrentPositionChanged(VideoPlayerAdapter adapter) {
                super.onCurrentPositionChanged(adapter);
            }

            @Override
            public void onBufferedPositionChanged(VideoPlayerAdapter adapter) {
                super.onBufferedPositionChanged(adapter);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onBufferedPositionChanged");
                }
            }

            @Override
            public void onDurationChanged(VideoPlayerAdapter adapter) {
                super.onDurationChanged(adapter);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onDurationChanged");
                }
            }

            @Override
            public void onVideoSizeChanged(VideoPlayerAdapter adapter, int width, int height) {
                super.onVideoSizeChanged(adapter, width, height);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onVideoSizeChanged");
                }
            }

            @Override
            public void onError(VideoPlayerAdapter adapter, int errorCode, String errorMessage) {
                super.onError(adapter, errorCode, errorMessage);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onError");
                }
                if (mOnVideoEventCallback != null) {
                    mOnVideoEventCallback.onPlayStop(adapter);
                }
            }

            @Override
            public void onBufferingStateChanged(VideoPlayerAdapter adapter, boolean start) {
                super.onBufferingStateChanged(adapter, start);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onBufferingStateChanged");
                }
            }

            @Override
            public void onMetadataChanged(VideoPlayerAdapter adapter) {
                super.onMetadataChanged(adapter);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onMetadataChanged");
                }
            }
        });
    }

    protected void onSurfaceDestroyed() {

    }

    /**
     * Adds {@link SurfaceHolder.Callback} to {@link android.view.SurfaceView}.
     */
    public void setSurfaceHolderCallback(SurfaceHolder.Callback callback) {
        mMediaPlaybackCallback = callback;

        if (callback != null) {
            if (mState == SURFACE_CREATED) {
                mMediaPlaybackCallback.surfaceCreated(mVideoSurface.getHolder());
            }
        }
    }

    protected void onSurfaceCreated() {

    }

    public boolean isPlayerVisible() {
        return mVideoRootView != null && mVideoRootView.getVisibility() == View.VISIBLE;
    }

    public void dispatchScrollEvent(VerticalGridView verticalGridView, final int topViewId) {

        verticalGridView.setOnScrollOffsetCallback(new BaseGridView.OnScrollOffsetCallback() {
            @Override
            public void onScrolledOffsetCallback(int offset, int remainScroll, int totalScroll) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onScrolledOffsetCallback | offset : " + offset + ", remainScroll : " + remainScroll + ", totalScroll : " + totalScroll);
                }
                currentScrollOffset = offset;
                if (remainScroll == 0) {
                    notifyLoadMore();
                }
                View archerView = root.findViewById(topViewId);
                archerViewChanged(offset);
                if (archerView == null) {
                    return;
                }
                onVideoSizeChanged(archerView.getLeft(), archerView.getTop(), mVideoSurface.getWidth(), mVideoSurface.getHeight());
            }
        });
    }

    protected void notifyLoadMore() {

    }


    private void archerViewChanged(int topLocate) {
        boolean show = topLocate <= 70;
        if (Log.INCLUDE) {
            Log.d(TAG, "archerViewChanged | show : " + show + ",mVideoRootView.getVisibility()  : " + mVideoRootView.getVisibility());
        }
        if (show) {
            if (mVideoRootView.getVisibility() != View.VISIBLE) {
                mVideoRootView.setVisibility(View.VISIBLE);
                onSurfaceCreated();
            }
        } else {
            if (mVideoRootView.getVisibility() != View.INVISIBLE) {
                mVideoRootView.setVisibility(View.INVISIBLE);
                onSurfaceDestroyed();
                stopPlayer();
            }
        }
    }

    public void setArcherView(int viewId) {
        root.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                View archerView = root.findViewById(viewId);
                if (archerView == null) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "setArcherView archerView null");
                    }
                    return;
                }
                Rect archerViewRect = getArcherViewLocation(archerView);
                onVideoSizeChanged(archerViewRect.left, archerViewRect.top, archerViewRect.width(), archerViewRect.height());
            }
        });
    }

    private Rect getArcherViewLocation(View archerView) {
        if (archerView == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "setArcherView archerView null");
            }
            return null;
        }

        int[] originalPos = new int[2];
        archerView.getLocationInWindow(originalPos);
        int x = originalPos[0];
        int y = originalPos[1];
        int width = archerView.getWidth();
        int height = archerView.getHeight();

        return new Rect(x, y, width + x, height + y);
    }

    private void onVideoSizeChanged(int x, int y, int width, int height) {
        ViewGroup.LayoutParams p = mVideoSurface.getLayoutParams();


        float px = mVideoSurface.getX();
        float py = mVideoSurface.getY();
        if (px != x || py != y) {
            mVideoSurface.setX(x);
            mVideoSurface.setY(y);
        }

        int pHeight = p.height;
        int pWidth = p.width;


        if (pHeight != height || pWidth != width) {
            p.height = height;
            p.width = width;
            mVideoSurface.setLayoutParams(p);
        }

    }

    /**
     * Returns the surface view.
     */
    public SurfaceView getSurfaceView() {
        return mVideoSurface;
    }

    @Override
    public void onDestroyView() {
        mVideoSurface = null;
        mState = SURFACE_NOT_CREATED;
        super.onDestroyView();
    }

    public interface OnVideoEventCallback {
        void onReady(VideoPlayerAdapter adapter);

        void onPlayCompleted(VideoPlayerAdapter adapter);

        void onPlayStop(VideoPlayerAdapter adapter);

        void onRelease(VideoPlayerAdapter adapter);

        void onDetached(VideoPlayerAdapter adapter);

        void onAttached(VideoPlayerAdapter adapter);
    }

    private static class TuneInfo {
        final Drawable mLoadingFrame;
        final AltiMediaSource[] mAssetFileDescriptor;

        public TuneInfo(Drawable loadingFrame, AltiMediaSource[] assetFileDescriptor) {
            this.mLoadingFrame = loadingFrame;
            this.mAssetFileDescriptor = assetFileDescriptor;
        }

        public Drawable getLoadingFrame() {
            return mLoadingFrame;
        }

        public AltiMediaSource[] getAssetFileDescriptor() {
            return mAssetFileDescriptor;
        }
    }
}