
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.def.CouponType;
import kr.altimedia.launcher.jasmin.dm.def.PaymentTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class PointCoupon extends Coupon implements Parcelable {
    public static final Creator<PointCoupon> CREATOR = new Creator<PointCoupon>() {
        @Override
        public PointCoupon createFromParcel(Parcel in) {
            return new PointCoupon(in);
        }

        @Override
        public PointCoupon[] newArray(int size) {
            return new PointCoupon[size];
        }
    };

    @Expose
    @SerializedName("bnsType")
    @JsonAdapter(BonusTypeDeserializer.class)
    private BonusType bonusType; //보너스 구분
    @Expose
    @SerializedName("bnsAmt")
    private int bonusAmount; // 보너스 금액
    @Expose
    @SerializedName("bnsVal")
    private int bonusValue; // 보너스 요율
    @Expose
    @SerializedName("chargAmt")
    private int chargedAmount; // 충전포인트
    @Expose
    @SerializedName("pointBlncVal")
    private int balance; // 잔액값
    @Expose
    @SerializedName("buyMethod")
    @JsonAdapter(PaymentTypeDeserializer.class)
    private PaymentType paymentType;

    private boolean isPurchased = true;

    protected PointCoupon(Parcel in) {
        super(in);

        bonusType = ((BonusType) in.readValue((BonusType.class.getClassLoader())));
        bonusAmount = in.readInt();
        bonusValue = in.readInt();
        chargedAmount = in.readInt();
        balance = in.readInt();
        paymentType = PaymentType.findByName((String) in.readValue(String.class.getClassLoader()));
        isPurchased = true;
    }

    protected PointCoupon(Coupon coupon, BonusType bonusType, int bonusAmount, int bonusValue, int chargedAmount, int balance, PaymentType paymentType) {
        this.id = coupon.id;
        this.name = coupon.name;
        this.registerDate = coupon.registerDate;
        this.expireDate = coupon.expireDate;
        this.couponStatus = coupon.couponStatus;
        if(coupon.couponType == CouponType.DISCOUNT){
            isPurchased = false;
        }
        this.couponType = CouponType.POINT;
        this.expiringYn = coupon.expiringYn;

        this.bonusType = bonusType;
        this.bonusAmount = bonusAmount;
        this.bonusValue = bonusValue;
        this.chargedAmount = chargedAmount;
        this.balance = balance;
        this.paymentType = paymentType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeValue(bonusType);
        dest.writeInt(bonusAmount);
        dest.writeInt(bonusValue);
        dest.writeInt(chargedAmount);
        dest.writeInt(balance);
        dest.writeValue(paymentType);
    }

    public BonusType getBonusType() {
        return bonusType;
    }

    public int getBonusAmount() {
        return bonusAmount;
    }

    public int getBonusValue() {
        return bonusValue;
    }

    public int getChargedAmount() {
        return chargedAmount;
    }

    public int getBalance() {
        return balance;
    }

    public int getPointPrice() {
        return chargedAmount;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public boolean isPurchased() {
        return isPurchased;
    }

    @NonNull
    @Override
    public String toString() {
        return "PointCoupon{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                "registerDate='" + registerDate + '\'' +
                "expireDate='" + expireDate + '\'' +
                "couponStatus='" + couponStatus + '\'' +
                "couponType='" + couponType + '\'' +
                "bonusType='" + bonusType + '\'' +
                "bonusAmount='" + bonusAmount + '\'' +
                "bonusValue='" + bonusValue + '\'' +
                "pointChargedAmount='" + chargedAmount + '\'' +
                "balance='" + balance + '\'' +
                "paymentType='" + paymentType + '\'' +
                "isPurchased='" + isPurchased + '\'' +
                "}";
    }
}
