package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

/**
 * Created by mc.kim on 04,09,2020
 */
public class ProfileLoginNoticeDialogFragment extends SafeDismissDialogFragment implements View.OnClickListener {
    public static final String CLASS_NAME = ProfileLoginNoticeDialogFragment.class.getName();
    private final String TAG = TwoButtonDialogFragment.class.getSimpleName();
    private View.OnClickListener mOkButtonClickedListener;

    protected ProfileLoginNoticeDialogFragment(View.OnClickListener onClickListener) {
        this.mOkButtonClickedListener = onClickListener;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_NOTICE;
    }

    public static ProfileLoginNoticeDialogFragment newInstance(View.OnClickListener okButtonClickedListener) {
        ProfileLoginNoticeDialogFragment fragment = new ProfileLoginNoticeDialogFragment(okButtonClickedListener);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return keyCode == KeyEvent.KEYCODE_BACK
                        || keyCode == KeyEvent.KEYCODE_ESCAPE;
            }
        });
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }


    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_error_network, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initVieW(view);
        initButton(view);
    }

    private void initVieW(View view) {
        String title = view.getContext().getString(R.string.error);
        String subtitle = view.getContext().getString(R.string.error_login_title);
        String description = view.getContext().getString(R.string.error_login_description);
        if (Log.INCLUDE) {
            Log.d(TAG, "title : " + title + ", description : " + description);
        }

        TextView titleView = view.findViewById(R.id.title);
        TextView subTitleView = view.findViewById(R.id.subTitle);
        TextView descriptionView = view.findViewById(R.id.description);

        titleView.setText(title);
        subTitleView.setText(subtitle);
        descriptionView.setText(description);
    }

    private void initButton(View view) {
        String buttonName = view.getContext().getString(R.string.button_retry);
        TextView okButton = view.findViewById(R.id.okButton);
        okButton.setText(buttonName);
        TextView closeButton = view.findViewById(R.id.closeButton);
        closeButton.setOnClickListener(this);
        okButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        mOkButtonClickedListener.onClick(v);
        dismiss();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (Log.INCLUDE) {
            Log.d(TAG, "onDismiss");
        }
        mOkButtonClickedListener = null;
    }
}

