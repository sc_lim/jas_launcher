/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.util.task;

import android.content.Context;

import java.util.List;

import androidx.fragment.app.FragmentManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.FAQInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.OtpId;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileList;
import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ChangePinCodeResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.response.OtpResponse;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public class SettingTaskManager extends TaskManager {
    private final String TAG = SettingTaskManager.class.getSimpleName();

    private AccountManager accountManager = AccountManager.getInstance();
    private FragmentManager fragmentManager;
    private final TvOverlayManager mTvOverlayManager;

    public SettingTaskManager(FragmentManager fragmentManager, TvOverlayManager tvOverlayManager) {
        this.fragmentManager = fragmentManager;
        this.mTvOverlayManager = tvOverlayManager;
    }

    public MbsDataTask checkAccountPinCode(String profileId, String password, MbsDataProvider<String, Boolean> provider) {
        return mUserDataManager.checkAccountPinCode(
                accountManager.getSaId(), profileId, password, provider);
    }

    public MbsDataTask checkProfilePinCode(String profileId, String password, MbsDataProvider<String, Boolean> provider) {
        return mUserDataManager.checkProfilePinCode(
                accountManager.getSaId(), profileId, password, provider);
    }

    public MbsDataTask changeAccountPinCode(String currentPin, String newPin, MbsDataProvider<String, ChangePinCodeResponse> provider) {
        return mUserDataManager.changeAccountPinCode(
                accountManager.getSaId(), accountManager.getProfileId(), currentPin, newPin, provider);
    }

    public MbsDataTask resetAccountPinCode(MbsDataProvider<String, Boolean> provider) {
        return mUserDataManager.resetAccountPinCode(
                accountManager.getSaId(), accountManager.getProfileId(), provider);
    }

    public MbsDataTask getOtpId(MbsDataProvider<String, OtpId> provider) {
        return mUserDataManager.getOtpId(
                accountManager.getSaId(), accountManager.getProfileId(), provider);
    }

    public MbsDataTask getOtp(String loginId, MbsDataProvider<String, OtpResponse.Otp> provider) {
        return mUserDataManager.getOtp(loginId, provider);
    }

    public MbsDataTask checkOtp(String loginId, String otp, MbsDataProvider<String, Boolean> provider) {
        return mUserDataManager.checkOtp(loginId, otp, provider);
    }

    public MbsDataTask getProfileList(MbsDataProvider<String, ProfileList> provider) {
        return mUserDataManager.getProfileList(accountManager.getSaId(), provider);
    }

    public MbsDataTask putProfileRating(String profileId, String rating, MbsDataProvider<String, Boolean> provider) {
        return mUserDataManager.putProfileRating(
                accountManager.getSaId(), profileId, rating, provider);
    }

    public MbsDataTask putProfileLock(String profileId, String lockYn, String pinCode, MbsDataProvider<String, Boolean> provider) {
        return mUserDataManager.putProfileLock(
                accountManager.getSaId(), profileId, lockYn, pinCode, provider);
    }

    public MbsDataTask postProfileBlockedChannel(String profileId, String[] channels, MbsDataProvider<String, List<String>> provider) {
        return mUserDataManager.postBlockedChannelList(
                accountManager.getSaId(), profileId, channels, provider);
    }

    public MbsDataTask getProfileBlockedChannel(String profileId, MbsDataProvider<String, List<String>> provider) {
        return mUserDataManager.getBlockedChannelList(
                accountManager.getSaId(), profileId, provider);
    }

    public MbsDataTask getDeviceList(MbsDataProvider<String, List<UserDevice>> provider) {
        return mUserDataManager.getDeviceList(
                accountManager.getSaId(), accountManager.getProfileId(), provider);
    }

    public MbsDataTask putDevice(String deviceId, String deviceName, MbsDataProvider<String, Boolean> provider) {
        return mUserDataManager.putDevice(
                accountManager.getSaId(), accountManager.getProfileId(), deviceId, deviceName, provider);
    }

    public MbsDataTask deleteDevice(String deviceId, MbsDataProvider<String, Boolean> provider) {
        return mUserDataManager.deleteDevice(
                accountManager.getSaId(), accountManager.getProfileId(), deviceId, provider);
    }

    public MbsDataTask getFaqList(MbsDataProvider<String, List<FAQInfo>> provider) {
        return mUserDataManager.getFAQList(accountManager.getSaId(), accountManager.getProfileId(), provider);
    }

    public void showFailDialog(Context context, int key) {
        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, context);
        failNoticeDialogFragment.show(mTvOverlayManager, fragmentManager, FailNoticeDialogFragment.CLASS_NAME);
    }

    public void showErrorDialog(Context context, String tag, String errorCode, String message) {
        if (context == null) {
            return;
        }

        ErrorDialogFragment dialogFragment =
                ErrorDialogFragment.newInstance(tag, errorCode, message);
        dialogFragment.show(fragmentManager, ErrorDialogFragment.CLASS_NAME);
    }
}
