/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;


import androidx.fragment.app.Fragment;

import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class FragmentRow extends Row {
    private final Fragment mFragment;
    private CharSequence mContentDescription;

    public final Fragment getFragment() {
        return this.mFragment;
    }

    public FragmentRow(HeaderItem header, Fragment fragment) {
        super(header);
        this.mFragment = fragment;
        this.verify();
    }

    public FragmentRow(long id, HeaderItem header, Fragment fragment) {
        super(id, header);
        this.mFragment = fragment;
        this.verify();
    }

    public FragmentRow(Fragment fragment) {
        this.mFragment = fragment;
        this.verify();
    }

    private void verify() {
        if (this.mFragment == null) {
            throw new IllegalArgumentException("ObjectAdapter cannot be null");
        }
    }

    public CharSequence getContentDescription() {
        if (this.mContentDescription != null) {
            return this.mContentDescription;
        } else {
            HeaderItem headerItem = this.getHeaderItem();
            if (headerItem != null) {
                CharSequence contentDescription = headerItem.getContentDescription();
                return (CharSequence)(contentDescription != null ? contentDescription : headerItem.getName());
            } else {
                return null;
            }
        }
    }

    public void setContentDescription(CharSequence contentDescription) {
        this.mContentDescription = contentDescription;
    }
}
