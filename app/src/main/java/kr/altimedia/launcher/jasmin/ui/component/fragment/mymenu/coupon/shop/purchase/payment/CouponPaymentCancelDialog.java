/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class CouponPaymentCancelDialog extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = CouponPaymentCancelDialog.class.getName();

    private static final String KEY_PURCHASE_INFO = "PURCHASE_INFO";

    private OnClickButtonListener onClickButtonListener;

    public static CouponPaymentCancelDialog newInstance(PurchaseInfo purchaseInfo) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_PURCHASE_INFO, purchaseInfo);

        CouponPaymentCancelDialog fragment = new CouponPaymentCancelDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnClickButtonListener(OnClickButtonListener onClickButtonListener) {
        this.onClickButtonListener = onClickButtonListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {
                if (onClickButtonListener != null) {
                    onClickButtonListener.onclickDoNotCancelPayment();
                }
            }
        };
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_vod_payment_cancel, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        PurchaseInfo purchaseInfo = bundle.getParcelable(KEY_PURCHASE_INFO);

        initView(purchaseInfo, view);
        initButton(view);
    }

    private void initView(PurchaseInfo purchaseInfo, View view) {
        TextView titleView = view.findViewById(R.id.title);
        TextView paymentType = view.findViewById(R.id.payment_type);

        String title = view.getResources().getString(R.string.coupon);
        try {
            title += " " + StringUtil.getFormattedNumber(purchaseInfo.getCouponProduct().getChargedAmount()) + " " + view.getResources().getString(R.string.thb);
        }catch (Exception e){
        }
        titleView.setText(title);
        paymentType.setText(purchaseInfo.getPaymentType().getDescriptionName());
    }

    private void initButton(View view) {
        TextView cancelPaymentButton = view.findViewById(R.id.cancel_payment_button);
        TextView doNotCancelPaymentButton = view.findViewById(R.id.do_not_cancel_payment_button);

        cancelPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickButtonListener != null) {
                    onClickButtonListener.onClickCancelPayment();
                }
            }
        });

        doNotCancelPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickButtonListener != null) {
                    onClickButtonListener.onclickDoNotCancelPayment();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onClickButtonListener = null;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public interface OnClickButtonListener {
        void onClickCancelPayment();

        void onclickDoNotCancelPayment();
    }
}
