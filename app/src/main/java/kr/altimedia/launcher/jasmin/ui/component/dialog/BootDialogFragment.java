/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.cwmp.service.def.CWMPEtcCode;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.dm.user.AuthenticationHandler;
import kr.altimedia.launcher.jasmin.dm.user.AuthenticationManager;
import kr.altimedia.launcher.jasmin.dm.user.object.LoginResult;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscriberInfo;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.activity.BaseActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.profile.ProfileSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.util.PermissionUtils;

import static kr.altimedia.launcher.jasmin.ui.util.PermissionUtils.PERMISSION_FINE_LOCATION;
import static kr.altimedia.launcher.jasmin.ui.util.PermissionUtils.PERMISSION_NOTIFICATION;
import static kr.altimedia.launcher.jasmin.ui.util.PermissionUtils.PERMISSION_READ_TV_LISTINGS;


public class BootDialogFragment extends SafeDismissDialogFragment {
    private final String TAG = BootDialogFragment.class.getSimpleName();
    public static final String CLASS_NAME = BootDialogFragment.class.getName();
    public static final int TYPE_BOOT = 1;
    public static final int TYPE_RE_BOOT = 2;
    private final int mType;
    public static final int WHAT_NOT_CONNECTED = 1;
    public static final int WHAT_CONNECTED = 0;
    private static final String WRONG_SAID = "MBS_0002";
    private int retryCnt = 0;

    public BootDialogFragment() {
        this(TYPE_BOOT);
    }

    public BootDialogFragment(int type) {
        mType = type;
        retryCnt = 0;
    }

    public static BootDialogFragment newInstance(int type) {
        BootDialogFragment f = new BootDialogFragment(type);
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return keyCode == KeyEvent.KEYCODE_BACK
                        || keyCode == KeyEvent.KEYCODE_ESCAPE;
            }
        });
        return dialog;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mType == TYPE_BOOT) {
            return inflater.inflate(R.layout.dialog_boot, container, false);
        } else {
            return inflater.inflate(R.layout.dialog_reboot, container, false);
        }
    }


    private void checkNetworkState(Context context, Handler handler) {
        NetworkCheckCallbackHandler networkCheckHandler = new NetworkCheckCallbackHandler(Looper.getMainLooper(), context, handler);
        networkCheckHandler.sendEmptyMessage(0);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        checkPermission();
        checkNetworkState(view.getContext(), new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                int what = msg.what;
                if (what == WHAT_NOT_CONNECTED) {
                    CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_152);
                }else if(what == WHAT_CONNECTED){
                    JasmineEpgApplication.startCWMPService(view.getContext().getApplicationContext());
                }
                startAuthentication();
            }
        });
    }

    private void showProfileSelectDialog(ArrayList<ProfileInfo> profileInfoList) {
        ProfileSelectDialogFragment profileSelectDialogFragment = ProfileSelectDialogFragment.newInstance(profileInfoList);
        profileSelectDialogFragment.setOnSelectProfileListener(new ProfileSelectDialogFragment.OnSelectProfileListener() {
            @Override
            public void onSelectProfile(ProfileInfo profileInfo) {
                profileSelectDialogFragment.dismiss();
                onRequestDismiss();
            }
        });
        profileSelectDialogFragment.show(getFragmentManager(), ProfileSelectDialogFragment.CLASS_NAME);
    }

    private void startAuthentication() {
        AuthenticationManager authenticationManager = new AuthenticationManager(getContext());
        authenticationManager.requestAuthentication(new AuthenticationHandler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                if (Log.INCLUDE) {
                    Log.d(TAG, "WHAT   " + msg.what);
                }
                switch (msg.what) {
                    case WHAT_COMPLETE_AUTHENTICATION: {
                        Bundle bundle = msg.getData();
                        saveUserInfo(bundle, new AccountManager.DataPutListener() {
                            @Override
                            public void onSuccess(String key) {
                                if (key.equalsIgnoreCase(AccountManager.KEY_CURRENT_PROFILE)) {
                                    onRequestDismiss();
                                }
                                CWMPServiceProvider.getInstance().notifyBooting(true, null);
                            }

                            @Override
                            public void onFail(String key) {
                                if (key.equalsIgnoreCase(AccountManager.KEY_CURRENT_PROFILE)) {
                                    if (Log.INCLUDE) {
                                        Log.d(TAG, "profile not selected");
                                    }
                                    showProfileSelectDialog(AccountManager.getInstance().getProfileList());
                                }
                                CWMPServiceProvider.getInstance().notifyBooting(false, null);
                            }
                        });
                    }
                    break;
                    case WHAT_ERROR_AUTHENTICATION: {
                        Bundle bundle = msg.getData();
                        saveUserInfo(bundle);
                        onRequestDismissWithError(bundle);

                        String errorMessage = null;
                        if(bundle != null) {
                            errorMessage = bundle.getString(AuthenticationHandler.KEY_ERROR_MESSAGE);
                        }
                        CWMPServiceProvider.getInstance().notifyBooting(false, errorMessage);
                        CWMPServiceProvider.getInstance().notifyWatchAuthority();
                    }
                    break;
                    case WHAT_ERROR_AUTHENTICATION_NOT_CONNECTED: {
                        Bundle bundle = new Bundle();
                        String errorCode = getContext().getString(R.string.error_ip_not_allocate);
                        String errorMessage = ErrorMessageManager.getInstance().getErrorMessage(errorCode);
                        bundle.putInt(AuthenticationHandler.KEY_ERROR_TYPE, AuthenticationHandler.WHAT_ERROR_AUTHENTICATION_NOT_CONNECTED);
                        bundle.putString(AuthenticationHandler.KEY_ERROR_CODE, errorCode);
                        bundle.putString(AuthenticationHandler.KEY_ERROR_MESSAGE, errorMessage);
                        onRequestDismissWithError(bundle);

                        CWMPServiceProvider.getInstance().notifyBooting(false, errorMessage);
                        CWMPServiceProvider.getInstance().notifyWatchAuthority();
                    }
                    break;
                }
            }
        });
    }

    private void checkPermission() {
        boolean hasReadTVPermission = PermissionUtils.hasReadTvListings(getContext());
        boolean hasNotificationPermission = PermissionUtils.hasNotification(getContext());
        boolean hasFineLocationPermission = PermissionUtils.hasFineLocation(getContext());

        if (Log.INCLUDE) {
            Log.d(TAG, "checkPermission,"
                    + ", hasReadTVPermission : " + hasReadTVPermission
                    + ", hasNotificationPermission : " + hasNotificationPermission
                    + ", hasFineLocationPermission : " + hasFineLocationPermission);
        }

        ArrayList<String> requestPermissions = new ArrayList<>();

        if (!hasReadTVPermission) {
            requestPermissions.add(PERMISSION_READ_TV_LISTINGS);
        }

        if (!hasNotificationPermission) {
            requestPermissions.add(PERMISSION_NOTIFICATION);
        }

        if (!hasFineLocationPermission) {
            requestPermissions.add(PERMISSION_FINE_LOCATION);
        }

        if (requestPermissions.size() == 0) {
            return;
        }

        PermissionUtils.requestPermissions(getActivity(), requestPermissions.toArray(new String[]{}), BaseActivity.PERMISSION_REQUEST_CODE);
    }


    private void saveUserInfo(Bundle bundle) {
        this.saveUserInfo(bundle, null);
    }

    private void saveUserInfo(Bundle bundle, AccountManager.DataPutListener callback) {
        String saId = bundle.getString(AuthenticationHandler.KEY_ACTIVATION);
        LoginResult loginResult = bundle.getParcelable(AuthenticationHandler.KEY_LOGIN_INFO);
        SubscriberInfo subscriberInfo = bundle.getParcelable(AuthenticationHandler.KEY_SUBSCRIBER_INFO);
        if (Log.INCLUDE) {
            Log.d(TAG, "success : " + saId);
            Log.d(TAG, "loginResult : " + loginResult);
            Log.d(TAG, "subscriberInfo : " + subscriberInfo);
        }
        AccountManager accountManager = AccountManager.getInstance();
        if (saId != null) {
            accountManager.putData(AccountManager.KEY_SAID, saId, callback);
        }
        if (loginResult != null) {
            accountManager.putData(AccountManager.KEY_LOGIN_INFO, loginResult, callback);
        }
        if (subscriberInfo != null) {
            accountManager.putData(AccountManager.KEY_SUBSCRIBER_INFO, subscriberInfo, callback);
        }
    }

    private BootProcessListener onBootProcessListener = null;

    public void addBootProcessListener(BootProcessListener bootProcessListener) {
        onBootProcessListener = bootProcessListener;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

    }

    private void onRequestDismiss() {
        if (onBootProcessListener != null) {
            onBootProcessListener.onFinished(this);
            onBootProcessListener = null;
        }
    }

    private void onRequestDismissWithError(Bundle bundle) {
        int type = bundle.getInt(AuthenticationHandler.KEY_ERROR_TYPE);
        String errorCode = bundle.getString(AuthenticationHandler.KEY_ERROR_CODE);
        String errorMessage = bundle.getString(AuthenticationHandler.KEY_ERROR_MESSAGE);

        if (type == AuthenticationHandler.WHAT_ERROR_LOGIN_TOKEN) {
            if (isInvalidSaId(errorCode) && retryCnt == 0) {
                retryCnt++;
                SettingControl.getInstance().setSaid("");
                startAuthentication();
                return;
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "auth error : " + type + ", errorCode : " + errorCode + ", errorMessage : " + errorMessage);
        }
        if (onBootProcessListener != null) {
            onBootProcessListener.onFinishedWithError(this, type, errorCode, errorMessage);
            onBootProcessListener = null;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    public interface BootProcessListener {
        void onFinished(DialogFragment dialogFragment);

        void onFinishedWithError(DialogFragment dialogFragment, int type,
                                 String errorCode,
                                 String errorMessage);
    }

    private static class NetworkCheckCallbackHandler extends Handler {
        private final Handler mCallbackHandler;
        private final Context mContext;
        private final long CHECK_DELAY = 500;
        private final String TAG = NetworkCheckCallbackHandler.class.getSimpleName();
        private final long MAX_WAITING_TIME = 30 * 1000;
        private long waitingTime = 0;

        public NetworkCheckCallbackHandler(@NonNull Looper looper,
                                           Context context,
                                           Handler callbackHandler) {
            super(looper);
            this.mCallbackHandler = callbackHandler;
            this.mContext = context;
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            boolean hasConnection = NetworkUtil.hasNetworkConnection(mContext);
            if (Log.INCLUDE) {
                Log.d(TAG, "hasConnection : " + hasConnection + ", waitingTime : " + waitingTime);
            }
            if (hasConnection || waitingTime >= MAX_WAITING_TIME) {
                mCallbackHandler.sendEmptyMessage(hasConnection ? WHAT_CONNECTED : WHAT_NOT_CONNECTED);
            } else {
                waitingTime += CHECK_DELAY;
                sendEmptyMessageDelayed(0, CHECK_DELAY);
            }
        }
    }

    private static boolean isInvalidSaId(String errorCode) {
        return errorCode.equalsIgnoreCase(WRONG_SAID);
    }
}
