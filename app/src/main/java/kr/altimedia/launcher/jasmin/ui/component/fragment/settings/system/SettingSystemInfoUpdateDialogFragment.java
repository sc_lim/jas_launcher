/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.update.apk.RequestApkUpdateInfo;
import com.altimedia.update.fw.RequestFwUpdateInfo;
import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.system.ota.SystemUpdateManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;

public class SettingSystemInfoUpdateDialogFragment extends SettingBaseDialogFragment {
    public enum UpdateVersionType {FIRMWARE, APK}

    public static final String CLASS_NAME = SettingSystemInfoUpdateDialogFragment.class.getName();
    private String TAG = SettingSystemInfoUpdateDialogFragment.class.getSimpleName();

    public static final String KEY_UPDATE_VERSION_TYPE = "UPDATE_VERSION_TYPE";

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private final SystemUpdateManager mSystemUpdateManager = SystemUpdateManager.getInstance();

    private Object updateInfo;

    private DialogInterface.OnDismissListener mOnDismissListener = null;

    public SettingSystemInfoUpdateDialogFragment() {
    }

    public static SettingSystemInfoUpdateDialogFragment newInstance(UpdateVersionType type) {
        Bundle args = new Bundle();
        args.putSerializable(KEY_UPDATE_VERSION_TYPE, type);

        SettingSystemInfoUpdateDialogFragment fragment = new SettingSystemInfoUpdateDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_system_info_check_update, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initProgressbar(view);
        initView(view);
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener mOnDismissListener) {
        this.mOnDismissListener = mOnDismissListener;
    }

    protected void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
        mProgressBarManager.show();
    }

    private void initView(View view) {
        UpdateVersionType type = (UpdateVersionType) getArguments().getSerializable(KEY_UPDATE_VERSION_TYPE);
        checkVersionUpdate(type);
    }

    private void checkVersionUpdate(UpdateVersionType type) {
        switch (type) {
            case APK:
                updateInfo = mSystemUpdateManager.checkSoftwareUpdate(SystemUpdateManager.TARGET_APK, new SystemUpdateManager.OnSWUpdateCheckResult() {
                    @Override
                    public void onResult(boolean hasUpdate) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkVersionUpdate, APK hasUpdate : " + hasUpdate);
                        }

                        if (hasUpdate) {
                            showUpdateVersionFragment();
                        } else {
                            showLatestVersionFragment();
                        }

                        updateInfo = null;
                        mProgressBarManager.hide();
                    }

                    @Override
                    public void onError() {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkVersionUpdate, onError");
                        }

                        updateInfo = null;
                        mProgressBarManager.hide();
                        dismiss();
                    }
                });
                break;

            case FIRMWARE:
                updateInfo = mSystemUpdateManager.checkSoftwareUpdate(SystemUpdateManager.TARGET_FW, new SystemUpdateManager.OnSWUpdateCheckResult() {
                    @Override
                    public void onResult(boolean hasUpdate) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkVersionUpdate, FIRMWARE hasUpdate : " + hasUpdate);
                        }

                        if (hasUpdate) {
                            showUpdateVersionFragment();
                        } else {
                            showLatestVersionFragment();
                        }

                        updateInfo = null;
                        mProgressBarManager.hide();
                    }

                    @Override
                    public void onError() {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkVersionUpdate, onError");
                        }

                        updateInfo = null;
                        mProgressBarManager.hide();
                        dismiss();
                    }
                });
                break;
        }
    }

    private void showLatestVersionFragment() {
        SettingSystemInfoLatestVersionFragment fragment = SettingSystemInfoLatestVersionFragment.newInstance(getArguments());
        attachFragment(fragment, SettingSystemInfoLatestVersionFragment.CLASS_NAME);
    }

    private void showUpdateVersionFragment() {
        SettingSystemInfoUpdateVersionFragment fragment = SettingSystemInfoUpdateVersionFragment.newInstance(getArguments());
        attachFragment(fragment, SettingSystemInfoUpdateVersionFragment.CLASS_NAME);
    }

    private void attachFragment(SettingBaseFragment fragment, String tag) {
        getChildFragmentManager().beginTransaction().add(R.id.frame_layout, fragment, tag).commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (updateInfo != null) {
            if (updateInfo instanceof RequestApkUpdateInfo) {
                RequestApkUpdateInfo mRequestApkUpdateInfo = (RequestApkUpdateInfo) updateInfo;
                mSystemUpdateManager.cancelRequestApkUpdateInfo(mRequestApkUpdateInfo);
            } else if (updateInfo instanceof RequestFwUpdateInfo) {
                RequestFwUpdateInfo mRequestFwUpdateInfo = (RequestFwUpdateInfo) updateInfo;
                mSystemUpdateManager.cancelRequestFwUpdateInfo(mRequestFwUpdateInfo);
            }
        }

        if (mProgressBarManager != null) {
            mProgressBarManager.hide();
            mProgressBarManager.setRootView(null);
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
            mOnDismissListener = null;
        }
    }
}
