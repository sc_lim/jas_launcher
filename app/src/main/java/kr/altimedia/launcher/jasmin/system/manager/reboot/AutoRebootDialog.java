package kr.altimedia.launcher.jasmin.system.manager.reboot;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.altimedia.util.DeviceUtil;
import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class AutoRebootDialog implements View.OnKeyListener {
    private final String TAG = AutoRebootDialog.class.getSimpleName();

    private Context context;
    private WindowManager wm;
    private View parentsView;

    private final int HANDLER_ID = 0;
    private final long HANDLER_DELAY_TIME = 30 * TimeUtil.SEC;
    private final Handler rebootHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (Log.INCLUDE) {
                Log.d(TAG, "call handleMessage");
            }

            rebootSTB();
        }
    };

    public AutoRebootDialog(Context context) {
        this.context = context;

        LayoutInflater inflater = LayoutInflater.from(context);
        parentsView = inflater.inflate(R.layout.dialog_auto_reboot, null, false);
        initView(parentsView);
    }

    private void initView(View view) {
        TextView okButton = view.findViewById(R.id.ok_button);
        TextView cancelButton = view.findViewById(R.id.cancel_button);

        okButton.setOnKeyListener(this);
        cancelButton.setOnKeyListener(this);
    }

    public void show() {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                PixelFormat.TRANSLUCENT);
        mParams.gravity = Gravity.CENTER;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.addView(parentsView, mParams);
            }
        });

        startRebootHandler();
    }

    private void startRebootHandler() {
        rebootHandler.sendEmptyMessageDelayed(HANDLER_ID, HANDLER_DELAY_TIME);
    }

    private void rebootSTB() {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isInteractive();

        if (Log.INCLUDE) {
            Log.d(TAG, "rebootSTB, isScreenOn : " + isScreenOn);
        }

        if (isScreenOn) {
            DeviceUtil.reboot(context);
        } else {
            DeviceUtil.rebootQuiescent(context);
        }
    }

    private void setNextRebootTimer() {
        if (Log.INCLUDE) {
            Log.d(TAG, "setNextRebootTimer");
        }

        AutoRebootManager.getInstance().setRebootTimer(context);
    }

    private void dismiss() {
        if (Log.INCLUDE) {
            Log.d(TAG, "dismiss");
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                wm.removeView(parentsView);
            }
        });
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                rebootHandler.removeMessages(HANDLER_ID);

                int viewId = v.getId();
                if (viewId == R.id.ok_button) {
                    rebootSTB();
                } else if (viewId == R.id.cancel_button) {
                    setNextRebootTimer();
                }

                dismiss();
                return true;
            case KeyEvent.KEYCODE_BACK:
                rebootHandler.removeMessages(HANDLER_ID);

                setNextRebootTimer();
                dismiss();
                return true;
        }

        return false;
    }
}