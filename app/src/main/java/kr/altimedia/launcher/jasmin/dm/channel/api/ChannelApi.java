/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.channel.api;

import kr.altimedia.launcher.jasmin.dm.channel.obj.response.ChannelListResponse;
import kr.altimedia.launcher.jasmin.dm.channel.obj.response.ChannelProductInfoResponse;
import kr.altimedia.launcher.jasmin.dm.channel.obj.response.ProgramListResponse;
import kr.altimedia.launcher.jasmin.dm.channel.obj.response.SubscribedChannelListResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by mc.kim on 11,05,2020
 */
public interface ChannelApi {
    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("channellist")
    Call<ChannelListResponse> getChannelList(@Query("said") String said);


    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("epg")
    Call<ProgramListResponse> getProgramList(@Query("language") String language,
                                             @Query("said") String said,
                                             @Query("channelid") String channelid,
                                             @Query("startTime") String startTime,
                                             @Query("endTime") String endTime);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("subscribedchannellist")
    Call<SubscribedChannelListResponse> getSubscribedChannelList(@Query("said") String said,
                                                                 @Query("profileId") String profiledId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("channelproductinfo")
    Call<ChannelProductInfoResponse> getChannelProductInfo(@Query("said") String said,
                                                           @Query("profileId") String profiledId,
                                                           @Query("channelId") String channelId,
                                                           @Query("language") String language);
}
