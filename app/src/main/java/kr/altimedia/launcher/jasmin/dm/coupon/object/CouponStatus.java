/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import kr.altimedia.launcher.jasmin.R;
import com.altimedia.util.Log;

public enum CouponStatus {
    REGISTRATION("20304", R.string.registration), USING("20305", R.string.using),
    USED("20306", R.string.used), EXPIRED("20307", R.string.expired), DESTROYED("20308", R.string.destroyed), UNKNOWN("UNKNOWN", -1);

    private final String type;
    private final int mName;

    CouponStatus(String type, int mName) {
        this.type = type;
        this.mName = mName;
    }

    public static CouponStatus findByName(String name) {
//        Log.d("CouponStatus", "findByName: name="+name);
        CouponStatus[] statuses = values();
        for (CouponStatus status : statuses) {
            if (status.type.equalsIgnoreCase(name)) {
                return status;
            }
        }

        return UNKNOWN;
    }

    public int getName() {
        return mName;
    }
}
