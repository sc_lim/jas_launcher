/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.message;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.message.MessageDataManager;
import kr.altimedia.launcher.jasmin.dm.message.obj.Message;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.OnMenuInteractor;
import kr.altimedia.launcher.jasmin.ui.component.fragment.message.presenter.MessagePresenter;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;

public class MessageListFragment extends Fragment implements View.OnUnhandledKeyEventListener {
    private static final String TAG = MessageListFragment.class.getSimpleName();
    public static final String CLASS_NAME = MessageListFragment.class.getName();

    private final int VISIBLE_COUNT = 7;

    private MessageItemBridgeAdapter mBridgeAdapter;
    private List<Message> messageList;

    private TextView count;
    private PagingVerticalGridView mVerticalGridView;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private final MessageDataManager mMessageDataManager = new MessageDataManager();
    private final AccountManager accountManager = AccountManager.getInstance();
    private MbsDataTask messageTask;
    private MbsDataTask messageViewTask;

    private OnMenuInteractor menuVisibleInteractor;
    private final MessagePresenter.onKeyListener onKeyListener = new MessagePresenter.onKeyListener() {
        @Override
        public boolean onKey(int keyCode, Message message) {
            int last = messageList.size() - 1;
            int index = messageList.indexOf(message);

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    if (index == 0) {
                        mVerticalGridView.scrollToPosition(last);
                        return true;
                    }
                    break;
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    if (index == last) {
                        mVerticalGridView.scrollToPosition(0);
                        return true;
                    }
                    break;
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    showMessage(message);
                    return true;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                case KeyEvent.KEYCODE_BACK:
                    menuVisibleInteractor.openSideMenu(true);
                    return true;
            }

            return false;
        }
    };

    public static MessageListFragment newInstance() {

        Bundle args = new Bundle();

        MessageListFragment fragment = new MessageListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    private TvOverlayManager mTvOverlayManager = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach: " + mTvOverlayManager);
        }
        if (getParentFragment() instanceof OnMenuInteractor) {
            this.menuVisibleInteractor = (OnMenuInteractor) getParentFragment();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_message, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initProgressbar(view);
        count = view.findViewById(R.id.count);
        mVerticalGridView = view.findViewById(R.id.messageGrid);
        mVerticalGridView.initVertical(VISIBLE_COUNT);
        int gap = (int) getResources().getDimension(R.dimen.message_list_row_gap);
        mVerticalGridView.setVerticalSpacing(gap);
        view.addOnUnhandledKeyEventListener(this);

        initList(view);
    }

    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void initList(View view) {
        messageTask = mMessageDataManager.getMessageList(accountManager.getLocalLanguage(), accountManager.getSaId(), accountManager.getProfileId(),
                new MbsDataProvider<String, List<Message>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, List<Message> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSuccess, result " + result);
                        }

                        menuVisibleInteractor.closeSideMenu(false, true);
                        messageList = result;
                        setMessageList(messageList);
                        initScrollbar(view);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return MessageListFragment.this.getContext();
                    }
                });
    }

    private void setMessageList(List<Message> messageList) {
        String countSize = messageList.size() > 0 ? "(" + messageList.size() + ")" : "";
        count.setText(countSize);

        MessagePresenter messagePresenter = new MessagePresenter(onKeyListener);
        ArrayObjectAdapter messageAdapter = new ArrayObjectAdapter(messagePresenter);

        for (int i = 0; i < messageList.size(); i++) {
            messageAdapter.add(messageList.get(i));
        }

        mBridgeAdapter = new MessageItemBridgeAdapter(messageAdapter, mVerticalGridView, VISIBLE_COUNT);
        mVerticalGridView.setAdapter(mBridgeAdapter);
    }

    private void initScrollbar(View view) {
        ThumbScrollbar messageScrollbar = view.findViewById(R.id.messageScrollbar);
        messageScrollbar.setList(messageList.size(), 7);

        mVerticalGridView.setNumColumns(1);
        mVerticalGridView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int focus = mVerticalGridView.getChildPosition(recyclerView.getFocusedChild());
                messageScrollbar.moveFocus(focus);
            }
        });
        messageScrollbar.setFocusable(false);
    }

    private void showMessage(Message message) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showMessage, message id : " + message.getId() + ", message title : " + message.getTitle() + ", isView : " + message.isView());
        }

        if (message.isView()) {
            mTvOverlayManager.showDialogFragment(MessageDetailDialogFragment.newInstance(message));
            return;
        }

        if (messageViewTask != null && messageViewTask.isRunning()) {
            return;
        }

        messageViewTask = mMessageDataManager.postMessageView(accountManager.getAccountId(), accountManager.getProfileId(), message.getId(),
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSuccess");
                        }

                        message.setView(true);
                        mTvOverlayManager.showDialogFragment(MessageDetailDialogFragment.newInstance(message));
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return MessageListFragment.this.getContext();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (messageTask != null && messageTask.isRunning()) {
            messageTask.cancel(true);
        }

        if (messageViewTask != null && messageViewTask.isRunning()) {
            messageViewTask.cancel(true);
        }

        getView().removeOnUnhandledKeyEventListener(this);
        menuVisibleInteractor = null;
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_BACK:
                // Defect #2430
                if (!menuVisibleInteractor.isMenuShowing()) {
                    menuVisibleInteractor.openSideMenu(true);
                    return true;
                }
        }

        return false;
    }
}
