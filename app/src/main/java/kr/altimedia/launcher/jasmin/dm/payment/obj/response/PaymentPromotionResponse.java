package kr.altimedia.launcher.jasmin.dm.payment.obj.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.PaymentPromotion;

public class PaymentPromotionResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private List<PaymentPromotion> promotionList;

    protected PaymentPromotionResponse(Parcel in) {
        super(in);
        promotionList = in.createTypedArrayList(PaymentPromotion.CREATOR);
    }

    public static final Creator<PaymentPromotionResponse> CREATOR = new Creator<PaymentPromotionResponse>() {
        @Override
        public PaymentPromotionResponse createFromParcel(Parcel in) {
            return new PaymentPromotionResponse(in);
        }

        @Override
        public PaymentPromotionResponse[] newArray(int size) {
            return new PaymentPromotionResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(promotionList);
    }

    public List<PaymentPromotion> getPromotionList() {
        return promotionList;
    }
}
