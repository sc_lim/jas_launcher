/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ChannelRingStorage;
import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.ObjectKey;

import java.util.ArrayList;
import java.util.Collections;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.Observable;
import androidx.leanback.widget.OnChildLaidOutListener;
import androidx.leanback.widget.OnChildSelectedListener;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.databinding.DialogMiniGuideBinding;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.tv.JasTvView;
import kr.altimedia.launcher.jasmin.tv.ParentalController;
import kr.altimedia.launcher.jasmin.tv.observable.ChannelLiveData;
import kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel;
import kr.altimedia.launcher.jasmin.ui.app.AppConfig;
import kr.altimedia.launcher.jasmin.ui.component.activity.BaseActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.interactor.OnFragmentVisibilityInteractor;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.view.adapter.RepeatArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class MiniGuideDialogFragment extends SafeDismissDialogFragment
        implements ValueAnimator.AnimatorUpdateListener {
    public static final String CLASS_NAME = MiniGuideDialogFragment.class.getName();
    private OnFragmentVisibilityInteractor mOnFragmentVisibilityInteractor = null;

    private static final String TAG = MiniGuideDialogFragment.class.getSimpleName();

    private VerticalGridView channelList;
    private ArrayList<Channel> allChannelList;

    private ValueAnimator visibleAnimator;
    private ValueAnimator invisibleAnimator;

    private boolean mIsVisible = false;
    private boolean isFullyVisible = false;

    private final Observable.OnPropertyChangedCallback mWatchingChannelPropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            Channel watchingChannel = getEpgViewModel().watchingChannel.getCurrentChannel();
            if (!watchingChannel.isHiddenChannel()) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        updateChannels();
                    }
                });
            }
        }
    };

    public MiniGuideDialogFragment() {
        // Required empty public constructor
    }

    public static MiniGuideDialogFragment newInstance() {
        return new MiniGuideDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog mDialog = super.onCreateDialog(savedInstanceState);
        mDialog.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        return mDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        DialogMiniGuideBinding miniGuideBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_mini_guide, container, false);
        miniGuideBinding.setEpgViewModel(getEpgViewModel());
        getEpgViewModel().currentChannel.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            public void onPropertyChanged(Observable sender, int propertyId) {
                ChannelLiveData currentChannel = (ChannelLiveData) sender;
                updateChannelListIndex(currentChannel.getCurrentChannel());

            }
        });
        return miniGuideBinding.getRoot();
    }

    private ViewGroup mParentView;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mParentView = (ViewGroup) view;
        channelList = view.findViewById(R.id.channel_list);
        channelList.setFocusable(false);
        channelList.setWindowAlignmentOffset(0);
        channelList.setWindowAlignmentOffsetPercent(83.8f);
        channelList.setWindowAlignment(VerticalGridView.WINDOW_ALIGN_HIGH_EDGE);
        channelList.setOnChildLaidOutListener(new OnChildLaidOutListener() {
            @Override
            public void onChildLaidOut(ViewGroup parent, View view, int position, long id) {
                if (view != null) {
                    view.setScaleX(0.8f);
                    view.setScaleY(0.8f);
                }
            }
        });
        channelList.setOnChildSelectedListener(new OnChildSelectedListener() {
            private View mSelectedView = null;

            @Override
            public void onChildSelected(ViewGroup parent, View view, int position, long id) {
                if (!isVisibleMiniGuide()) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "wrong item selected");
                    }
                    return;
                }
                if (view != mSelectedView) {
                    if (mSelectedView != null) {
                        Animator animator = loadAnimator(mSelectedView.getContext(), R.animator.channel_list_unselected);
                        animator.setTarget(mSelectedView);
                        if (!isFullyVisible) {
                            animator.setDuration(0L);
                        }
                        animator.start();
                    }
                    mSelectedView = view;
                    Animator animator = loadAnimator(view.getContext(), R.animator.channel_list_selected);
                    animator.setTarget(view);
                    mSelectedView.setAlpha(0.7f);
                    if (!isFullyVisible) {
                        animator.setDuration(0L);
                    }
                    animator.start();
                }
            }
        });
        initData();
        initViewAnimator();
    }

    private ThumbnailTvAnimatorUpdateListener thumbnailTvAnimatorUpdateListener;

    private void initViewAnimator() {
        visibleAnimator = (ValueAnimator) loadAnimator(getContext(), R.animator.reminder_alert_move_down);
        visibleAnimator.setInterpolator(new DecelerateInterpolator());
        visibleAnimator.addUpdateListener(this);

        invisibleAnimator = (ValueAnimator) loadAnimator(getContext(), R.animator.reminder_alert_move_up);
        invisibleAnimator.setInterpolator(new AccelerateInterpolator());
        invisibleAnimator.addUpdateListener(this);

        if (AppConfig.FEATURE_THUMBNAIL_VIDEO_ENABLED) {
            if (thumbnailTvAnimatorUpdateListener != null) {
                thumbnailTvAnimatorUpdateListener.release();
                thumbnailTvAnimatorUpdateListener = null;
            }
            thumbnailTvAnimatorUpdateListener = new ThumbnailTvAnimatorUpdateListener(mParentView, channelList, getEpgViewModel(), mTvOverlayManager, this);
            visibleAnimator.addUpdateListener(thumbnailTvAnimatorUpdateListener);
            invisibleAnimator.addUpdateListener(thumbnailTvAnimatorUpdateListener);
        }
    }

    private Animator loadAnimator(Context context, int resId) {
        Animator animator = AnimatorInflater.loadAnimator(context, resId);
        return animator;
    }

    private void updateChannelListIndex(Channel channel) {
        int size = allChannelList.size();

        int fromIndex = channelList.getSelectedPosition();
        int selectedIndex = findNearestIndex(fromIndex, channel, size);

        channelList.setSelectedPositionSmooth(selectedIndex);
    }

    private int findNearestIndex(int fromIndex, Channel channel, int size) {
        int halfSize = (int) ((double) size / 2 + 0.5);
        //Log.d("SHLEE", "findNearestIndex() from:"+fromIndex+", size:"+size+", ch:"+channel.getDisplayNumber()+", half:"+halfSize);
        int found = -1;
        for (int i = 0; i <= halfSize; i++) {
            if (i == 0) {
                if (equals(fromIndex, channel)) {
                    found = fromIndex;
                    break;
                }
            } else {
                if (equals(fromIndex + i, channel)) {
                    found = fromIndex + i;
                    break;
                } else if (equals(fromIndex - i, channel)) {
                    found = fromIndex - i;
                    break;
                }
            }
        }
        //Log.d("SHLEE", "found index:"+found);
        return found;
    }

    private boolean equals(int index, Channel channel) {
        RepeatArrayObjectAdapter adapter = (RepeatArrayObjectAdapter) ((ItemBridgeAdapter) channelList.getAdapter()).getAdapter();
        if (0 <= index && index < adapter.size()) {
            return adapter.get(index).equals(channel);
        } else {
            return false;
        }
    }

    private final ChannelDataManager.Listener mChannelDataManagerListener =
        new ChannelDataManager.Listener() {
            @Override
            public void onLoadFinished() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onLoadFinished");
                }
                validateEPGChannelRing();
                updateChannels();
            }

            @Override
            public void onChannelListUpdated() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onChannelListUpdated");
                }
                validateEPGChannelRing();
                updateChannels();
            }

            @Override
            public void onChannelBrowsableChanged() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onChannelBrowsableChanged");
                }
                validateEPGChannelRing();
                updateChannels();
            }

            @Override
            public void onUserBasedChannelListUpdated() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onUserBasedChannelListUpdated");
                }
            }

            private void validateEPGChannelRing() {
                //If the EPG channel ring does not contain the watching channel, add it.
                Channel watchingChannel = getEpgViewModel().watchingChannel.getCurrentChannel();
                if (!TvSingletons.getSingletons(getContext()).getChannelDataManager().containsEpgChannelRing(watchingChannel)) {
                    TvSingletons.getSingletons(getContext()).getChannelDataManager().addChannelToEpgChannelRing(watchingChannel);
                }
            }
        };

    private void initData() {
        updateChannels();
    }


    private void updateChannels() {
        RepeatArrayObjectAdapter objectAdapter = new RepeatArrayObjectAdapter(new MiniGuideChannelListPresenter(this));

        TvSingletons mTvSingletons = TvSingletons.getSingletons(getContext());
        allChannelList = (ArrayList<Channel>) mTvSingletons.getChannelDataManager().getChannelRingStorage().getChannelRing(ChannelRingStorage.RING_MINIEPG).clone();
        Collections.reverse(allChannelList);

        objectAdapter.addAll(0, allChannelList);

        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(objectAdapter);
        if (channelList.getAdapter() != null) {
            channelList.swapAdapter(itemBridgeAdapter, true);
        } else {
            channelList.setAdapter(itemBridgeAdapter);
        }

        setCurrentSelectedPosition();
    }

    private void acquireData() {
        if (Log.INCLUDE) {
            Log.d(TAG, "acquireData");
        }
        TvSingletons.getSingletons(getContext()).getChannelDataManager().addListener(mChannelDataManagerListener);
        getEpgViewModel().watchingChannel.addOnPropertyChangedCallback(mWatchingChannelPropertyChangedCallback);
    }

    private void disposeData() {
        if (Log.INCLUDE) {
            Log.d(TAG, "disposeData");
        }
        TvSingletons.getSingletons(getContext()).getChannelDataManager().removeListener(mChannelDataManagerListener);
        getEpgViewModel().watchingChannel.removeOnPropertyChangedCallback(mWatchingChannelPropertyChangedCallback);
    }

    EpgViewModel getEpgViewModel() {
        return ((BaseActivity) getActivity()).getEpgViewModel();
    }

    private void setCurrentSelectedPosition() {
        Channel channel = getEpgViewModel().currentChannel.getCurrentChannel();
        int index = allChannelList.indexOf(channel);

        RepeatArrayObjectAdapter adapter = (RepeatArrayObjectAdapter) ((ItemBridgeAdapter) channelList.getAdapter()).getAdapter();
        channelList.setSelectedPosition(adapter.getMidPosition() + index);
    }

    public void setVisibleMiniGuideDialog(boolean isVisible, boolean showAnimation) {
        if (isVisible == mIsVisible) {
            return;
        }
        mIsVisible = isVisible;

        if (isVisible) {
            endAll(invisibleAnimator);

            channelList.getAdapter().notifyDataSetChanged();
            updateChannels();
            if (showAnimation) {
                visibleAnimator.start();
            } else {
                getView().setAlpha(1.0f);
                isFullyVisible = true;
            }
        } else {
            endAll(visibleAnimator);
            if (showAnimation) {
                invisibleAnimator.start();
            } else {
                if (thumbnailTvAnimatorUpdateListener != null) {
                    thumbnailTvAnimatorUpdateListener.hideView();
                }
                getView().setAlpha(0.0f);
                isFullyVisible = false;
            }
        }
    }

    public boolean isVisibleMiniGuide() {
        return mIsVisible;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_MINI_GUIDE;
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        float value = (float) animation.getAnimatedValue();
        if (mParentView != null) {
            mParentView.setAlpha(value);
        }
        isFullyVisible = (value == 1.0f);
    }

    private static class ThumbnailTvAnimatorUpdateListener implements ValueAnimator.AnimatorUpdateListener {
        private final String TAG = ThumbnailTvAnimatorUpdateListener.class.getSimpleName();
        private final ViewGroup mParentView;
        private final RecyclerView mRecyclerView;
        private final EpgViewModel mEpgViewModel;
        private final TvOverlayManager mTvOverlayManager;


        /*Thumbnail*/
        private ThumbTvView thumbTvView;
        private final ImageView thumbImgView;
        private final View thumbLayer;
        private final TextView thumbText;

        /*Info*/
        private final TextView chNum;
        private final TextView chName;
        private final ImageView chIcon;


        private final long THUMB_DELAY_TIME = 200;
        private final long TUNE_DELAY_TIME = 500;
        private final long VISIBLE_DELAY_TIME = 2000;
        private final Handler mHandler = new Handler(Looper.getMainLooper());

        private final Observable.OnPropertyChangedCallback mOnPropertyChangedCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        updateTvView();
                    }
                });
            }
        };

        private final RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (thumbTvView == null) {
                    return;
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "onScrollStateChanged : " + newState);
                }
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:

                        break;

                    case RecyclerView.SCROLL_STATE_SETTLING:
                        if (mMiniGuideDialogFragment != null && !mMiniGuideDialogFragment.isVisibleMiniGuide()) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "wrong item selected");
                            }
                            return;
                        }
                        thumbLayer.setAlpha(1f);
                        thumbImgView.setImageResource(R.drawable.mini_epg_ch_thumb_default);
//                            hideView();
//                            videoReset();
                        break;
                }
            }
        };

        private boolean isHasThumbnail(Context context, Channel channel) {

            if (channel.isLocked()) {
                return false;
            } else if (channel.isAdultChannel()) {
                return false;
            } else if (channel.isPaidChannel()) {
                return false;
            } else if (channel.isAudioChannel()) {
                return false;
            } else return !ParentalController.isProgramRatingBlocked(context, channel);
        }

        private final Runnable mTuneRunnable = new Runnable() {
            @Override
            public void run() {
                tuneThumbnail(thumbTvView);
            }
        };
        private final Runnable mVisibleRunnable = new Runnable() {
            @Override
            public void run() {
                showTvView();
            }
        };
        ValueAnimator mTransportRowFadeInAnimator;
        private MiniGuideDialogFragment mMiniGuideDialogFragment;

        public ThumbnailTvAnimatorUpdateListener(ViewGroup parentView, RecyclerView recyclerView,
                                                 EpgViewModel mEpgViewModel, TvOverlayManager tvOverlayManager, MiniGuideDialogFragment miniGuideDialogFragment) {
            final Context context = parentView.getContext();
            this.mMiniGuideDialogFragment = miniGuideDialogFragment;
            this.mParentView = parentView.findViewById(R.id.thumbVideoLayer);
            this.mRecyclerView = recyclerView;
            this.mEpgViewModel = mEpgViewModel;
            this.mTvOverlayManager = tvOverlayManager;

            this.mRecyclerView.addOnScrollListener(onScrollListener);

            LayoutInflater layoutInflater = LayoutInflater.from(parentView.getContext());
            layoutInflater.inflate(R.layout.view_thumbnail_video_view, mParentView, true);
            thumbTvView = parentView.findViewById(R.id.thumb_tv_view);
            thumbTvView.setTuneAction((ch) -> updateTvView());
            thumbImgView = parentView.findViewById(R.id.thumb_img_view);
            thumbLayer = parentView.findViewById(R.id.thumbLayer);
            thumbText = parentView.findViewById(R.id.thumb_text);

            chNum = parentView.findViewById(R.id.thumb_channel_number);
            chName = parentView.findViewById(R.id.thumb_channel_name);
            chIcon = parentView.findViewById(R.id.thumb_channel_icon);

            mEpgViewModel.currentChannel.addOnPropertyChangedCallback(mOnPropertyChangedCallback);

            loadTransportAnimator(context);
            hideView();
        }

        private void updateTvView() {
            if (Log.INCLUDE) {
                Log.d(TAG, "call updateTvView");
            }
            if (mMiniGuideDialogFragment != null && !mMiniGuideDialogFragment.isVisibleMiniGuide()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "wrong item updateTvView");
                }
                return;
            }
            cancelAll(mTransportRowFadeInAnimator);
            thumbLayer.setAlpha(1f);
            clearRunnable();
            JasTvView.TuningResult tuningResult = makeThumbnailInfo(thumbTvView);
            updateInfo(mEpgViewModel.getCurrentChannel());
            thumbTvView.setPreviewIndicator(null);
            thumbTvView.setTuningResult(tuningResult);
            if (tuningResult != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "updateTvView | tuningResult : " + tuningResult);
                }
                boolean success = setGuideThumbnail(tuningResult);

                if (!success) {
                    sendRunnable();
                    Glide.with(mParentView.getContext())
                            .load(ChannelImageManager.getChannelThumbnailUrl(mEpgViewModel.getCurrentChannel()))
                            .diskCacheStrategy(DiskCacheStrategy.DATA)
                            .signature(new ObjectKey(String.valueOf(System.currentTimeMillis() / 60000L)))
                            .placeholder(R.drawable.mini_epg_ch_thumb_default)
                            .error(R.drawable.mini_epg_ch_thumb_default)
                            .into(thumbImgView);
                } else {
//                    videoReset();
                }
            } else {
                clearResource(false);
                sendRunnable();
                Glide.with(mParentView.getContext())
                        .load(ChannelImageManager.getChannelThumbnailUrl(mEpgViewModel.getCurrentChannel()))
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .signature(new ObjectKey(String.valueOf(System.currentTimeMillis() / 60000L)))
                        .placeholder(R.drawable.mini_epg_ch_thumb_default)
                        .error(R.drawable.mini_epg_ch_thumb_default)
                        .into(thumbImgView);
            }
        }

        private void updateWatchTvView() {
            clearRunnable();
            videoReset();
            JasTvView.TuningResult tuningResult = JasTvView.TuningResult.TUNING_BLOCKED_THUMB_WATCHING_CH;
            updateInfo(mEpgViewModel.getWatchingChannel());
            thumbTvView.setTuningResult(tuningResult);
            if (Log.INCLUDE) {
                Log.d(TAG, "updateWatchTvView : " + tuningResult);
            }
            thumbLayer.setAlpha(1f);
            boolean success = setGuideThumbnail(tuningResult);
        }

        private void clearResource(boolean thumbNailClear) {
            thumbText.setText("");
            if (thumbNailClear) {
                thumbImgView.setImageResource(R.drawable.bg_transparent);
            }
        }


        private void videoReset() {
            if (Log.INCLUDE) {
                Log.d(TAG, "call videoReset");
            }
            thumbTvView.reset();
        }

        private boolean setGuideThumbnail(JasTvView.TuningResult tuningResult) {
            int textId = 0;
            switch (tuningResult) {
                case TUNING_BLOCKED_THUMB_WATCHING_CH:
                    textId = R.string.channel_thumb_watching;
                    break;
                case TUNING_BLOCKED_THUMB_AUDIO:
                    textId = R.string.channel_thumb_audio;
                    break;
                case TUNING_BLOCKED_THUMB_UHD:
                    textId = R.string.iframe_block_uhd_title;
                    break;
                case TUNING_BLOCKED_CHANNEL:
                    textId = R.string.channel_thumb_blocked;
                    break;
                case TUNING_BLOCKED_ADULT_CHANNEL:
                    textId = R.string.channel_thumb_adult_blocked;
                    break;
                case TUNING_BLOCKED_THUMB_NOT_AVAILABLE:
                    textId = R.string.channel_thumb_not_available;
                    break;
                case TUNING_BLOCKED_PROGRAM:
                    textId = R.string.channel_thumb_blocked_program;
                    break;
                case TUNING_BLOCKED_UNSUBSCRIBED:
                    textId = R.string.channel_thumb_unsubscribed;
                    break;
                default:
                    break;
            }

            if (textId == R.string.channel_thumb_audio) {
                thumbText.setText("");
                thumbImgView.setImageResource(R.drawable.audio_ch_img);
                return true;
            } else if (textId != 0) {
                thumbText.setText(textId);
                thumbImgView.setImageResource(R.drawable.channel_img_iframe);
                return true;
            } else {
                thumbText.setText("");
                thumbImgView.setImageResource(R.drawable.mini_epg_ch_thumb_default);
                return false;
            }
        }

        private void updateInfo(Channel currentChannel) {
            if (currentChannel != null) {
                chNum.setText(StringUtil.getFormattedNumber(currentChannel.getDisplayNumber(), 3));
                chNum.setVisibility(currentChannel.isHiddenChannel() ? View.GONE : View.VISIBLE);
                chName.setText(currentChannel.getDisplayName());
                setChannelIcon(currentChannel);
            } else {
                chNum.setText("");
                chName.setText("");
                setChannelIcon(null);
            }
        }

        private void setChannelIcon(Channel channel) {
            int icon = 0;
            if (channel == null) {
                icon = 0;
            } else if (channel.isLocked() || channel.isAdultChannel()) {
                icon = R.drawable.ch_icon_lock;
            } else if (channel.isFavoriteChannel()) {
                icon = R.drawable.ch_icon_fav_f;
            } else if (channel.isPaidChannel()) {
                icon = R.drawable.ch_icon_pay;
            } else if (channel.isAudioChannel()) {
                icon = R.drawable.ch_icon_audio;
            }

            if (icon != 0) {
                chIcon.setImageResource(icon);
                chIcon.setVisibility(View.VISIBLE);
            } else {
                chIcon.setVisibility(View.GONE);
            }
        }


        private void clearRunnable() {
            mHandler.removeCallbacks(mTuneRunnable);
            mHandler.removeCallbacks(mVisibleRunnable);
        }

        private void sendRunnable() {
            mHandler.postDelayed(mTuneRunnable, TUNE_DELAY_TIME);
            mHandler.postDelayed(mVisibleRunnable, VISIBLE_DELAY_TIME);
        }

        private void loadTransportAnimator(Context context) {
            final ValueAnimator.AnimatorUpdateListener updateListener = new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator arg0) {
                    final float fraction = (Float) arg0.getAnimatedValue();
                    thumbLayer.setAlpha(1 - fraction);
                }
            };
            this.mTransportRowFadeInAnimator = loadAnimator(context, R.animator.miniguie_pip_fade_in);
            this.mTransportRowFadeInAnimator.addUpdateListener(updateListener);
            this.mTransportRowFadeInAnimator.setInterpolator(mLogDecelerateInterpolator);
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            float value = (float) animation.getAnimatedValue();
            TimeInterpolator interpolator = animation.getInterpolator();
            if (interpolator instanceof AccelerateInterpolator) {
                hideView();
            } else {
                thumbLayer.setAlpha(value);
            }
        }

        private void hideView() {
            if (Log.INCLUDE) {
                Log.d(TAG, "hideView");
            }
            endAll(mTransportRowFadeInAnimator);
            clearRunnable();
            if (thumbTvView != null) {
                videoReset();
                thumbTvView.setVisibility(View.GONE);
            }
        }

        private void showTvView() {
            if (Log.INCLUDE) {
                Log.d(TAG, "showTvView");
            }
            if (thumbTvView != null) {
                thumbTvView.setVisibility(View.VISIBLE);
            }
            reverseFirstOrStartSecond(mTransportRowFadeInAnimator, true);
        }

        private JasTvView.TuningResult makeThumbnailInfo(ThumbTvView thumbTvView) {
            if (thumbTvView == null) {
                return null;
            }
            Channel currentChannel = mEpgViewModel.getCurrentChannel();
            if (Log.INCLUDE) {
                Log.d(TAG, "tuneThumbnail() > start (ch:" + (currentChannel != null ? currentChannel.getDisplayNumber() : "null") + ")");
            }
            Channel watchingChannel = mEpgViewModel.getWatchingChannel();
            JasTvView.TuningResult tuningResult = null;
            if (watchingChannel == currentChannel) {
                tuningResult = JasTvView.TuningResult.TUNING_BLOCKED_THUMB_WATCHING_CH;
            } else if (currentChannel.isUHDChannel()) {
                tuningResult = JasTvView.TuningResult.TUNING_BLOCKED_THUMB_UHD;
            } else if (currentChannel.isAudioChannel()) {
                tuningResult = JasTvView.TuningResult.TUNING_BLOCKED_THUMB_AUDIO;
            } else if (currentChannel.isAdultChannel()) {
                tuningResult = JasTvView.TuningResult.TUNING_BLOCKED_ADULT_CHANNEL;
            } else if (currentChannel.isLocked()) {
                tuningResult = JasTvView.TuningResult.TUNING_BLOCKED_CHANNEL;
            } else if (currentChannel.isPaidChannel() && !JasChannelManager.getInstance().isPreviewAvailable(currentChannel)) {
                tuningResult = JasTvView.TuningResult.TUNING_BLOCKED_UNSUBSCRIBED;
            } else if (ParentalController.isProgramRatingBlocked(mParentView.getContext(), currentChannel)) {
                tuningResult = JasTvView.TuningResult.TUNING_BLOCKED_PROGRAM;
            } else if (mTvOverlayManager.isPipShowing()) {
                tuningResult = JasTvView.TuningResult.TUNING_BLOCKED_THUMB_NOT_AVAILABLE;
            }
            return tuningResult;
        }

        private void endAll(ValueAnimator first) {
            if (Log.INCLUDE) {
                Log.d(TAG, "endAll");
            }
            if (first.isStarted()) {
                first.end();
            }
        }

        private void cancelAll(ValueAnimator first) {
            if (Log.INCLUDE) {
                Log.d(TAG, "endAll");
            }
            if (first.isStarted()) {
                first.cancel();
            }
        }

        private void reverseFirstOrStartSecond(ValueAnimator second,
                                               boolean runAnimation) {
            if (Log.INCLUDE) {
                Log.d(TAG, "reverseFirstOrStartSecond");
            }
            second.start();
            if (!runAnimation) {
                second.end();
            }
        }

        private void tuneThumbnail(ThumbTvView thumbTvView) {
            if (thumbTvView == null) {
                return;
            }
            Channel currentChannel = mEpgViewModel.getCurrentChannel();

            if (currentChannel != thumbTvView.getCurrentChannel()) {
                thumbTvView.tuneChannel(currentChannel);
                if (Log.INCLUDE) {
                    Log.d(TAG, "tuneThumbnail()  | try tune.");
                }
            } else {
                if (Log.INCLUDE) {
                    Log.d(TAG, "tuneThumbnail()  | ignore tune.");
                }
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "tuneThumbnail() < end");
            }
        }

        public void release() {
            if (Log.INCLUDE) {
                Log.d(TAG, "release");
            }
            if (mMiniGuideDialogFragment != null) {
                mMiniGuideDialogFragment = null;
            }
            if (thumbTvView != null) {

                videoReset();
                thumbTvView.setVisibility(View.GONE);
                mParentView.setVisibility(View.GONE);
                mParentView.removeView(thumbTvView);
                thumbTvView = null;
            }
            if (mEpgViewModel != null && mEpgViewModel.currentChannel != null) {

                mEpgViewModel.currentChannel.removeOnPropertyChangedCallback(mOnPropertyChangedCallback);
            }
            if (this.mRecyclerView != null) {
                this.mRecyclerView.removeOnScrollListener(onScrollListener);
            }
        }

        private final TimeInterpolator mLogAccelerateInterpolator = new TimeInterpolator() {
            @Override
            public float getInterpolation(float t) {
                float mLogScale = 1f / computeLog(1, 100, 0);
                return 1 - computeLog(1 - t, 100, 0) * mLogScale;
            }
        };
        private final TimeInterpolator mLogDecelerateInterpolator = new TimeInterpolator() {
            @Override
            public float getInterpolation(float t) {
                float mLogScale = 1f / computeLog(1, 100, 0);
                return computeLog(t, 100, 0) * mLogScale;
            }
        };

        private float computeLog(float t, int base, int drift) {
            return (float) -Math.pow(base, -t) + 1 + (drift * t);
        }

        private static ValueAnimator loadAnimator(Context context, int resId) {
            ValueAnimator animator = (ValueAnimator) AnimatorInflater.loadAnimator(context, resId);
            animator.setDuration(animator.getDuration());
            return animator;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mOnFragmentVisibilityInteractor != null) {
            mOnFragmentVisibilityInteractor.show(CLASS_NAME);
        }
        acquireData();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mOnFragmentVisibilityInteractor != null) {
            mOnFragmentVisibilityInteractor.hide(CLASS_NAME);
        }
        disposeData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentVisibilityInteractor) {
            mOnFragmentVisibilityInteractor = (OnFragmentVisibilityInteractor) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        endAll(visibleAnimator);
        endAll(invisibleAnimator);
        if (visibleAnimator != null && thumbnailTvAnimatorUpdateListener != null) {
            visibleAnimator.removeUpdateListener(thumbnailTvAnimatorUpdateListener);

        }
        if (invisibleAnimator != null && thumbnailTvAnimatorUpdateListener != null) {
            invisibleAnimator.removeUpdateListener(thumbnailTvAnimatorUpdateListener);
        }
        if (thumbnailTvAnimatorUpdateListener != null) {
            thumbnailTvAnimatorUpdateListener.release();
            thumbnailTvAnimatorUpdateListener = null;
        }

        mOnFragmentVisibilityInteractor = null;
    }

    private void endAll(ValueAnimator first) {
        if (Log.INCLUDE) {
            Log.d(TAG, "endAll");
        }
        if (first.isStarted()) {
            first.end();
        }
    }


}
