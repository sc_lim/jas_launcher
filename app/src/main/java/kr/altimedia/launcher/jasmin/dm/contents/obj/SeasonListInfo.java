/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.List;

public class SeasonListInfo implements Parcelable {
    private List<SeasonInfo> seasonList;
    private String masterSeriesTitle;
    private String seasonId;

    public SeasonListInfo(List<SeasonInfo> seasonList, String masterSeriesTitle, String seasonId) {
        this.seasonList = seasonList;
        this.masterSeriesTitle = masterSeriesTitle;
        this.seasonId = seasonId;
    }

    protected SeasonListInfo(Parcel in) {
        seasonList = in.createTypedArrayList(SeasonInfo.CREATOR);
        masterSeriesTitle = in.readString();
        seasonId = in.readString();
    }

    public static final Creator<SeasonListInfo> CREATOR = new Creator<SeasonListInfo>() {
        @Override
        public SeasonListInfo createFromParcel(Parcel in) {
            return new SeasonListInfo(in);
        }

        @Override
        public SeasonListInfo[] newArray(int size) {
            return new SeasonListInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(seasonList);
        dest.writeString(masterSeriesTitle);
        dest.writeString(seasonId);
    }

    @NonNull
    @Override
    public String toString() {
        return "Content{" +
                "seasonId=" + seasonId + '\'' +
                ", masterSeriesTitle=" + masterSeriesTitle + '\'' +
                ", seasonList=" + seasonList + '\'' +
                '}';
    }

    public List<SeasonInfo> getSeasonList() {
        return seasonList;
    }

    public String getMasterSeriesTitle() {
        return masterSeriesTitle;
    }

    public String getSeasonId() {
        return seasonId;
    }
}
