/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.installedAppList;

import kr.altimedia.launcher.jasmin.R;

public enum AppSortMode {
    SORT_ALPHABET(0, R.string.sort_alphabet), SORT_RECENCY(1, R.string.sort_recently), SORT_EDIT(2, R.string.sort_edit);

    int type;
    int title;

    AppSortMode(int type, int title) {
        this.type = type;
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public int getTitle() {
        return title;
    }
}
