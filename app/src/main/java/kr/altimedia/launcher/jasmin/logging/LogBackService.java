/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.logging;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;

import com.altimedia.util.Log;

import androidx.annotation.Nullable;

/**
 * LogBackService
 */
public class LogBackService extends Service {
    private final String TAG = LogBackService.class.getSimpleName();
    private LogBackHandler logBackHandler;

    @Override
    public void onDestroy() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onDestroy");
        }

        if (logBackHandler != null) {
            logBackHandler.dispose();
        }

        super.onDestroy();
    }

    @Override
    public void onCreate() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCreate");
        }
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBind: intent=" + intent);
        }

        logBackHandler = new LogBackHandler(this);

        return new LogBackServiceBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onStartCommand: intent=" + intent + ", flags=" + flags + ", startId=" + startId);
        }

        logBackHandler = new LogBackHandler(this);

        return super.onStartCommand(intent, flags, startId);
    }

    public void startLogBack(String deviceType) {
        if (logBackHandler != null) {
            logBackHandler.start(deviceType);
        }
    }

    public void addChannelLog(String channelId, long entryTime) throws RemoteException {
        if (logBackHandler != null) {
            logBackHandler.addChannelLog(channelId, entryTime);
        }
    }

    class LogBackServiceBinder extends Binder {
        LogBackService getService() {
            return LogBackService.this;
        }
    }
}
