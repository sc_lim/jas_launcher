/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.type;

import kr.altimedia.launcher.jasmin.R;

public enum PaymentType {
    DISCOUNT_COUPON("01", R.string.discount_coupon, R.string.discount_coupon), CASH_COUPON("02", R.string.cash_coupon, R.string.cash_coupon), QR("03", R.string.qr_code, R.string.qr_code),
    MEMBERSHIP_APP("04", R.string.ttbb_membership_app, R.string.ttbb_membership_app), MEMBERSHIP("05", R.string.membership, R.string.membership), BILLING("06", R.string.billing, R.string.billing),
    CREDIT_CARD("07", R.string.credit_card, R.string.credit_card), PROMPT_PAY("08", R.string.prompt_pay, R.string.prompt_pay), AIR_PAY("09", R.string.airpay, R.string.airpay),
    INTERNET_MOBILE_BANK("10", R.string.internet_banking_button, R.string.internet_banking), IN_APP("11", R.string.in_app, R.string.in_app);

    private String code;
    private int name;
    private int descriptionName;
    private String promotion;

    PaymentType(String code, int name, int descriptionName) {
        this.code = code;
        this.name = name;
        this.descriptionName = descriptionName;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public static PaymentType findByName(String code) {
        PaymentType[] types = values();
        for (PaymentType paymentType : types) {
            if (paymentType.code.equalsIgnoreCase(code)) {
                return paymentType;
            }
        }

        return BILLING;
    }

    public int getName() {
        return name;
    }

    public int getDescriptionName() {
        return descriptionName;
    }

    public String getCode() {
        return code;
    }

    public boolean hasPromotion() {
        return promotion != null && !promotion.isEmpty();
    }

    public String getPromotion() {
        return promotion;
    }

    @Override
    public String toString() {
        return "PaymentType{" +
                "code='" + code + '\'' +
                ", name=" + name +
                ", descriptionName=" + descriptionName +
                ", promotion='" + promotion + '\'' +
                '}';
    }
}
