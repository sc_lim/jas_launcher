/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;

import com.altimedia.util.Log;

import java.lang.ref.WeakReference;

import androidx.leanback.widget.Action;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.media.TvPlayerAdapter;
import kr.altimedia.launcher.jasmin.media.VideoPlayerAdapter;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.TimeShiftDetailsDescriptionPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekDataProvider;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekUi;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;

/**
 * Created by mc.kim on 06,02,2020
 */
public class TimeShiftPlaybackTransportControlGlue<T extends VideoPlayerAdapter>
        extends VideoPlaybackBaseControlGlue<T> {

    static final String TAG = "VideoPlaybackTransportControlGlue";

    static final int MSG_UPDATE_PLAYBACK_STATE = 100;
    static final int MSG_HIDE_CONTROL_STATE = 101;
    static final int MSG_HIDE_PLAYBACK_STATE = 102;
    static final int MSG_HIDE_PLAY_CONTROL_STATE = 103;

    static final int HIDE_CONTROL_STATE_DELAY_MS = 1500;
    static final int HIDE_PLAYBACK_STATE_DELAY_MS = 3500;
    static final int HIDE_PLAY_CONTROLLER_STATE_DELAY_MS = HIDE_CONTROL_STATE_DELAY_MS + HIDE_PLAYBACK_STATE_DELAY_MS;
    final Handler sHandler = new UpdatePlaybackStateHandler();
    final WeakReference<VideoPlaybackBaseControlGlue> mGlueWeakReference = new WeakReference(this);
    final SeekUiClient mPlaybackSeekUiClient = new SeekUiClient();
    VideoPlaybackSeekDataProvider mSeekProvider;
    boolean mSeekEnabled;

    /**
     * Constructor for the glue.
     *
     * @param context
     * @param impl    Implementation to underlying media player.
     */

    private long mProgramStartTimeMs;
    private long mProgramEndTimeMs;
    private long mProgramEnableTimeMs;

    public TimeShiftPlaybackTransportControlGlue(Context context, T impl, OnTimeShiftAction onTimeShiftAction) {
        super(context, impl);
        this.mOnTimeShiftAction = onTimeShiftAction;
    }

    public void setProgramStartTimeMs(long mProgramStartTimeMs) {
        this.mProgramStartTimeMs = mProgramStartTimeMs;
    }

    public void setProgramEndTimeMs(long mProgramEndTimeMs) {
        this.mProgramEndTimeMs = mProgramEndTimeMs;
    }

    public void setEnableTimeMs(long mProgramEnableTimeMs) {
        this.mProgramEnableTimeMs = mProgramEnableTimeMs;
    }

    @Override
    public void setControlsRow(VideoPlaybackControlsRow controlsRow) {
        super.setControlsRow(controlsRow);
        clearSHandler();
        onUpdatePlaybackState();
    }

    private void clearSHandler() {
        sHandler.removeMessages(MSG_HIDE_PLAY_CONTROL_STATE, mGlueWeakReference);
        sHandler.removeMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference);
        sHandler.removeMessages(MSG_HIDE_PLAYBACK_STATE, mGlueWeakReference);
        sHandler.removeMessages(MSG_HIDE_CONTROL_STATE, mGlueWeakReference);
    }

    @Override
    protected void onCreatePrimaryActions(ArrayObjectAdapter primaryActionsAdapter) {
        primaryActionsAdapter.add(mPlayPauseAction =
                new VideoPlaybackControlsRow.PlayPauseAction(getContext()));
    }

    private OnTimeShiftAction mOnTimeShiftAction;

    @Override
    protected void onAttachedToHost(VideoPlaybackGlueHost host) {
        super.onAttachedToHost(host);

        if (host instanceof VideoPlaybackSeekUi) {
            ((VideoPlaybackSeekUi) host).setPlaybackSeekUiClient(mPlaybackSeekUiClient);
        }
    }

    @Override
    protected void onDetachedFromHost() {
        super.onDetachedFromHost();
        clearSHandler();
        if (getHost() instanceof VideoPlaybackSeekUi) {
            ((VideoPlaybackSeekUi) getHost()).setPlaybackSeekUiClient(null);
        }
    }

    @Override
    protected void onUpdateProgress() {

        if (!mPlaybackSeekUiClient.mIsSeek && !mPlayerAdapter.isSeeking()) {
            super.onUpdateProgress();
        }
    }

    @Override
    public void onActionClicked(Action action) {
        dispatchAction(action, null);
    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_ESCAPE:
                if (isContentsLocked()) {
                    getHost().onFinish();
                    return true;
                }
                return false;
        }
        if (mControlsRow == null) {
            return false;
        }
        if (mPlayerAdapter.isKeyBlock()) {
            return false;
        }

        if (!mPlayerAdapter.isReady()) {
            return false;
        }
        if (isContentsLocked()) {
            return false;
        }
        if (isDPADKeyEvent(event)) {

            return true;
        }

        final ObjectAdapter primaryActionsAdapter = mControlsRow.getPrimaryActionsAdapter();
        Action action = mControlsRow.getActionForKeyCode(primaryActionsAdapter, keyCode);
        if (action == null) {
            action = mControlsRow.getActionForKeyCode(mControlsRow.getSecondaryActionsAdapter(),
                    keyCode);
        }

        if (action != null) {
            dispatchAction(action, event);
            return true;
        }


        sendHandlerMsg(MSG_HIDE_CONTROL_STATE);

        return false;
    }

    boolean isDPADKeyEvent(KeyEvent keyEvent) {


        boolean isCenter = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER;
        boolean forward = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT;
        boolean left = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;

        boolean up = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP;
        boolean down = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN;
        if (isCenter) {
            if (getHost().isControlsOverlayVisible()) {
                return false;
            }
            if (keyEvent.getAction() == KeyEvent.ACTION_UP) {
                return true;
            }
            if (isPlaying()) {
                mPlayerAdapter.pause();
                mIsPlaying = false;
            } else {
                mPlayerAdapter.resume();
                mIsPlaying = true;
            }
            updatePlaybackState(mIsPlaying);
            getHost().showStateOverlay(true);
            getHost().showControlsOverlay(true);
            sendHandlerMsg(MSG_HIDE_CONTROL_STATE);
            return true;
        } else if (forward || left) {
            if (getHost().isControlsOverlayVisible() && !mPlaybackSeekUiClient.mIsSeek) {
                return false;
            }
            final boolean result;
            if (forward) {
                result = onForward(keyEvent.getAction() == KeyEvent.ACTION_UP);
            } else {
                result = onBackward(keyEvent.getAction() == KeyEvent.ACTION_UP);
            }
            int actionIndex = forward ?
                    VideoPlaybackControlsRow.PlayPauseAction.INDEX_FAST_FORWARD : VideoPlaybackControlsRow.PlayPauseAction.INDEX_REWIND;

            if (result) {
                if (keyEvent.getAction() == KeyEvent.ACTION_UP && !getHost().isStateOverlayShowing()) {
                    return true;
                }
                onUpdatePlaybackStatusAfterUserAction(actionIndex, MSG_HIDE_CONTROL_STATE, keyEvent.getAction());
                getHost().showStateOverlay(true);
            } else {
                if (mPlaybackSeekUiClient.mIsSeekWorking) {
                    onUpdatePlaybackStatusAfterUserAction(actionIndex, MSG_HIDE_CONTROL_STATE, KeyEvent.ACTION_UP);
                    mPlaybackSeekUiClient.setIsSeekWorking(false);
                }
            }
            getHost().showControlsOverlay(true);
            return true;
        } else if (up || down) {
            if (getHost().isControlsOverlayVisible()) {
                return false;
            }
            getHost().showControlsOverlay(true);
            sendHandlerMsg(MSG_UPDATE_PLAYBACK_STATE);
            return true;
        }
        return false;
    }

    @Override
    protected void onUpdatePlaybackStatusAfterUserAction(final int msgWhat) {
        updatePlaybackState(mIsPlaying);
        sendHandlerMsg(msgWhat);

    }

    void sendHandlerMsg(int msgWhat) {
        clearSHandler();
        switch (msgWhat) {
            case MSG_UPDATE_PLAYBACK_STATE:
                sHandler.sendMessage(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference));
                break;

            case MSG_HIDE_CONTROL_STATE:
                sHandler.sendMessageDelayed(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference), HIDE_CONTROL_STATE_DELAY_MS);
                break;

            case MSG_HIDE_PLAYBACK_STATE:
                sHandler.sendMessageDelayed(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference), HIDE_PLAYBACK_STATE_DELAY_MS);
                break;

            case MSG_HIDE_PLAY_CONTROL_STATE:
                sHandler.sendMessageDelayed(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference), HIDE_PLAY_CONTROLLER_STATE_DELAY_MS);
                break;
        }
    }


    void onUpdatePlaybackStatusAfterUserAction(int index, int msgWhat, int keyAction) {
        updatePlaybackState(mIsPlaying, index, keyAction);
        sendHandlerMsg(msgWhat);
    }


    boolean dispatchProgressClicked() {
        if (isContentsLocked()) {
            return mOnTimeShiftAction.checkUnLockContents();
        } else {
            VideoPlaybackControlsRow.PlayPauseAction playPauseAction = new VideoPlaybackControlsRow.PlayPauseAction(getContext());
            return dispatchAction(playPauseAction, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE));
        }
    }

    public void showControllerBar() {
        onUpdatePlaybackStatusAfterUserAction(MSG_HIDE_CONTROL_STATE);
        getHost().showControlsOverlay(true);
    }

    /**
     * Called when the given action is invoked, either by click or keyevent.
     */
    boolean dispatchAction(Action action, KeyEvent keyEvent) {

        boolean handled = false;
        if (keyEvent == null) {
            return false;
        }

        if (action instanceof VideoPlaybackControlsRow.PlayPauseAction) {


            boolean canPlay = keyEvent == null
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY;
            boolean canPause = keyEvent == null
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PAUSE;
            //            PLAY_PAUSE    PLAY      PAUSE
            // playing    paused                  paused
            // paused     playing       playing
            // ff/rw      playing       playing   paused
            if (canPause && mIsPlaying) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                    return true;
                }
                mPlayerAdapter.pause();
                mIsPlaying = false;
                onUpdatePlaybackStatusAfterUserAction(MSG_HIDE_CONTROL_STATE);
                getHost().showStateOverlay(true);
                getHost().showControlsOverlay(true);
            } else if (canPlay && !mIsPlaying) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                    return true;
                }
                mPlayerAdapter.resume();
                mIsPlaying = true;
                onUpdatePlaybackStatusAfterUserAction(MSG_HIDE_CONTROL_STATE);
                getHost().showStateOverlay(true);
                getHost().showControlsOverlay(true);
            } else {
                boolean forward = (keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) || (keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT);
//                if (!isPlaying()) {
//                    play();
//                    mIsPlaying = true;
//                    updatePlaybackState(mIsPlaying);
//                }
                final boolean result;
                if (forward) {
                    result = onForward(keyEvent.getAction() == KeyEvent.ACTION_UP);
                } else {
                    result = onBackward(keyEvent.getAction() == KeyEvent.ACTION_UP);
                }
                int actionIndex = forward ?
                        VideoPlaybackControlsRow.PlayPauseAction.INDEX_FAST_FORWARD : VideoPlaybackControlsRow.PlayPauseAction.INDEX_REWIND;

                if (result) {
                    if (keyEvent.getAction() == KeyEvent.ACTION_UP && !getHost().isStateOverlayShowing()) {
                        return true;
                    }
                    onUpdatePlaybackStatusAfterUserAction(actionIndex, MSG_HIDE_CONTROL_STATE, keyEvent.getAction());
                    getHost().showStateOverlay(true);
                } else {
                    if (mPlaybackSeekUiClient.mIsSeekWorking) {
                        onUpdatePlaybackStatusAfterUserAction(actionIndex, MSG_HIDE_CONTROL_STATE, KeyEvent.ACTION_UP);
                        mPlaybackSeekUiClient.setIsSeekWorking(false);
                    }
                }
                getHost().showControlsOverlay(true);
            }

            handled = true;
        } else if (action instanceof VideoPlaybackControlsRow.SkipNextAction) {
            next();
            handled = true;
        } else if (action instanceof VideoPlaybackControlsRow.SkipPreviousAction) {
            previous();
            handled = true;
        }
        return handled;
    }


    boolean onForward(boolean keyUp) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onForward : " + keyUp);
        }
        if (keyUp) {
            mPlaybackSeekUiClient.onSeekFinished(false);
            return true;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, " onForward : " + mPlaybackSeekUiClient.mIsSeek);
        }
        if (!mPlaybackSeekUiClient.mIsSeek) {
            mPlaybackSeekUiClient.onSeekStarted();
        }
        long currentPosition = mPlaybackSeekUiClient.mLastUserPosition != -1 ? mPlaybackSeekUiClient.mLastUserPosition : mPlayerAdapter.getCurrentPosition();
        long seekTimeInMs = currentPosition + VideoPlaybackTransportRowPresenter.SEEK_DIFFER_TIME;
        if (seekTimeInMs > getDuration()) {
            seekTimeInMs = getDuration();
        }
        mPlaybackSeekUiClient.onSeekPositionChanged(seekTimeInMs);
        return true;
    }

    boolean onBackward(boolean keyUp) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBackward : " + keyUp);
        }

        long currentPosition = mPlaybackSeekUiClient.mLastUserPosition != -1 ? mPlaybackSeekUiClient.mLastUserPosition : mPlayerAdapter.getCurrentPosition();
        long seekTimeInMs = currentPosition - VideoPlaybackTransportRowPresenter.SEEK_DIFFER_TIME;
        if (seekTimeInMs < 0) {
            seekTimeInMs = 0;
        }
        long currentTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
        long enableTimeMs = currentTime - mProgramEnableTimeMs;
        if (Log.INCLUDE) {
            Log.d(TAG, "onBackward | seekTimeInMs : " + seekTimeInMs);
            Log.d(TAG, "onBackward | mProgramEnableTimeMs : " + enableTimeMs);
        }

        boolean canSeek = mPlaybackSeekUiClient.canSeeking(seekTimeInMs);

        if (!mPlaybackSeekUiClient.mIsSeek && !canSeek) {
            return false;
        }

        if (keyUp) {
            mPlaybackSeekUiClient.onSeekFinished(false);
            return true;
        }


        if (!mPlaybackSeekUiClient.mIsSeek) {
            mPlaybackSeekUiClient.onSeekStarted();
        }


        if (seekTimeInMs < enableTimeMs) {
//            mPlaybackSeekUiClient.onSeekPositionChanged(enableTimeMs);
            return false;
        } else {
            mPlaybackSeekUiClient.onSeekPositionChanged(seekTimeInMs);
            return true;
        }
    }

    @Override
    protected void onPlayStateChanged() {
        if (DEBUG) {
            Log.d(TAG, "onStateChanged");
        }

        if (sHandler.hasMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference)) {
            sHandler.removeMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference);
            if (mPlayerAdapter.isPlaying() != mIsPlaying) {
                if (DEBUG) {
                    Log.d(TAG, "Status expectation mismatch, delaying update");
                }
                sHandler.sendMessageDelayed(sHandler.obtainMessage(MSG_UPDATE_PLAYBACK_STATE,
                        mGlueWeakReference), HIDE_CONTROL_STATE_DELAY_MS);
            } else {
                if (DEBUG) {
                    Log.d(TAG, "Update state matches expectation");
                }
                onUpdatePlaybackState();
            }
        } else {
            onUpdatePlaybackState();
        }

        super.onPlayStateChanged();
    }

    void onUpdatePlaybackState() {
        mIsPlaying = mPlayerAdapter.isPlaying();
        updatePlaybackState(mIsPlaying);

    }
    private void updatePlaybackState(boolean isPlaying) {
        int index = !isPlaying
                ? VideoPlaybackControlsRow.PlayPauseAction.INDEX_PLAY
                : VideoPlaybackControlsRow.PlayPauseAction.INDEX_PAUSE;
        updatePlaybackState(isPlaying, index, KeyEvent.ACTION_DOWN);
        if (isPlaying) {
            getHost().hideIconView(true);
        }
    }


    private void updatePlaybackState(boolean isPlaying, int iconIndex, int keyAction) {
        if (mControlsRow == null) {
            return;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "updatePlaybackState : " + isPlaying);
        }


        if (!isPlaying) {
            onUpdateProgress();
            mPlayerAdapter.setProgressUpdatingEnabled(mPlaybackSeekUiClient.mIsSeek);
        } else {
            mPlayerAdapter.setProgressUpdatingEnabled(true);
        }

//        if (mFadeWhenPlaying && getHost() != null) {
//            getHost().setControlsOverlayAutoHideEnabled(isPlaying);
//        }

        if (mPlayPauseAction != null) {
            int index = iconIndex;
            int mPlayPauseActionIndex = mPlayPauseAction.getIndex();
            if (mPlayPauseActionIndex != index
                    || mPlayPauseActionIndex == VideoPlaybackControlsRow.PlayPauseAction.INDEX_REWIND
                    || mPlayPauseActionIndex == VideoPlaybackControlsRow.PlayPauseAction.INDEX_FAST_FORWARD) {
                mPlayPauseAction.setIndex(index);
                mPlayPauseAction.setAction(keyAction);
                notifyItemChanged((ArrayObjectAdapter) getControlsRow().getPrimaryActionsAdapter(),
                        mPlayPauseAction);
            }
        }
    }

    public final VideoPlaybackSeekDataProvider getSeekProvider() {
        return mSeekProvider;
    }

    public final void setSeekProvider(VideoPlaybackSeekDataProvider seekProvider) {
        mSeekProvider = seekProvider;
    }

    public final boolean isSeekEnabled() {
        return mSeekEnabled;
    }

    public final void setSeekEnabled(boolean seekEnabled) {
        mSeekEnabled = seekEnabled;
    }

    @Override
    protected TimeShiftPlaybackTransportRowPresenter onCreateRowPresenter() {
        final TimeShiftDetailsDescriptionPresenter detailsPresenter =
                new TimeShiftDetailsDescriptionPresenter() {
                    @Override
                    protected void onBindDescription(ViewHolder
                                                             viewHolder, Object obj) {
                        VideoPlaybackBaseControlGlue glue = (VideoPlaybackBaseControlGlue) obj;
                        viewHolder.getTitle().setText(glue.getTitle());
                        viewHolder.setSeekEnable(mOnTimeShiftAction.canSeek(true), mOnTimeShiftAction.canSeek(false));
                        viewHolder.setOnControlButtonClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mOnTimeShiftAction == null) {
                                    return;
                                }
                                switch (v.getId()) {
                                    case R.id.btnTimeShiftList:
                                        mOnTimeShiftAction.showList();
                                        break;
                                    case R.id.btnTimeShiftRW:
                                        mOnTimeShiftAction.seekToPreviousProgram();
                                        break;
                                    case R.id.btnTimeShiftFF:
                                        mOnTimeShiftAction.seekToNextProgram();
                                        break;
                                    case R.id.btnTimeShiftLive:
                                        mOnTimeShiftAction.tuneToLive();
                                        break;
                                }
                            }
                        });
                    }
                };

        TimeShiftPlaybackTransportRowPresenter rowPresenter = new TimeShiftPlaybackTransportRowPresenter() {
            @Override
            protected void onBindRowViewHolder(RowPresenter.ViewHolder vh, Object item) {
                super.onBindRowViewHolder(vh, item);
                vh.setOnKeyListener(TimeShiftPlaybackTransportControlGlue.this);
            }

            @Override
            protected void onUnbindRowViewHolder(RowPresenter.ViewHolder vh) {
                super.onUnbindRowViewHolder(vh);
                vh.setOnKeyListener(null);
            }

            @Override
            protected void onProgressBarClicked(ViewHolder vh) {
                super.onProgressBarClicked(vh);
                dispatchProgressClicked();

            }

            @Override
            protected void onProgressBarKey(ViewHolder vh, KeyEvent keyEvent) {
                super.onProgressBarKey(vh, keyEvent);
                VideoPlaybackControlsRow.PlayPauseAction playPauseAction = new VideoPlaybackControlsRow.PlayPauseAction(getContext());
                dispatchAction(playPauseAction, keyEvent);
            }
        };


        rowPresenter.setProgramStartTimeMs(mProgramStartTimeMs);
        rowPresenter.setProgramEndTimeMs(mProgramEndTimeMs);
        rowPresenter.setTimeShiftEnableTime(mProgramEnableTimeMs);
        rowPresenter.setDescriptionPresenter(detailsPresenter);
        return rowPresenter;
    }

    public final void setOnTimeShiftActionListener(OnTimeShiftAction onTimeShiftAction) {
        this.mOnTimeShiftAction = onTimeShiftAction;
    }

    private class UpdatePlaybackStateHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_PLAYBACK_STATE:
                    TimeShiftPlaybackTransportControlGlue glue =
                            ((WeakReference<TimeShiftPlaybackTransportControlGlue>) msg.obj).get();
                    if (glue != null) {
                        glue.onUpdatePlaybackState();
                    }
                    sendHandlerMsg(MSG_HIDE_PLAY_CONTROL_STATE);
                    break;
                case MSG_HIDE_CONTROL_STATE:
                    getHost().hideStateOverlay(true);
                    if (getHost().isControlsOverlayVisible()) {
                        sendHandlerMsg(MSG_HIDE_PLAYBACK_STATE);
                    } else {
                        if (isPlaying()) {
                            getHost().hideIconView(true);
                        } else {
                            getHost().showIconView(true);
                        }
                    }
                    break;
                case MSG_HIDE_PLAYBACK_STATE:
                    getHost().hideControlsOverlay(true);
                    break;

                case MSG_HIDE_PLAY_CONTROL_STATE:
                    getHost().hideStateOverlay(true);
                    getHost().hideControlsOverlay(true);
                    break;
            }

        }
    }

    class SeekUiClient extends VideoPlaybackSeekUi.Client {
        boolean mPausedBeforeSeek;
        long mPositionBeforeSeek;
        long mLastUserPosition;
        boolean mIsSeek;

        boolean mIsSeekWorking;

        public void setIsSeekWorking(boolean isSeekWorking) {
            this.mIsSeekWorking = isSeekWorking;
        }

        @Override
        public VideoPlaybackSeekDataProvider getPlaybackSeekDataProvider() {
            return mSeekProvider;
        }

        @Override
        public boolean isSeekEnabled() {
            return mSeekProvider != null || mSeekEnabled;
        }

        @Override
        public void onSeekStarted() {
            mPositionBeforeSeek = mSeekProvider == null ? mPlayerAdapter.getCurrentPosition() : -1;
            mIsSeek = true;
            mPausedBeforeSeek = !isPlaying();
            mPlayerAdapter.setProgressUpdatingEnabled(true);
            mLastUserPosition = -1;
            mPlayerAdapter.pause();
            mIsPlaying = false;
            mIsSeekWorking = true;
        }

        public boolean canSeeking(long pos) {
            if (!(mPlayerAdapter instanceof TvPlayerAdapter)) {
                return true;
            }
            if (pos == 0) {
                return true;
            }
            TvPlayerAdapter tvPlayerAdapter = (TvPlayerAdapter) mPlayerAdapter;
            long timeShiftEnableTime = tvPlayerAdapter.getTimeShiftEnableTime();


            long timeMs = JasmineEpgApplication.SystemClock().currentTimeMillis();
            long limitStartTimeMs = timeMs - timeShiftEnableTime;

            return pos >= limitStartTimeMs;
        }

        @Override
        public void onSeekPositionChanged(long pos) {
            mLastUserPosition = pos;
            if (mControlsRow != null) {
                mControlsRow.setCurrentPosition(pos, true);
            }
        }

        @Override
        public void onSeekFinished(boolean cancelled) {
            if (!cancelled) {
                if (mLastUserPosition >= 0) {
                    seekTo(mLastUserPosition);
                }
            } else {
                if (mPositionBeforeSeek >= 0) {
                    seekTo(mPositionBeforeSeek);
                }
            }
            mIsSeekWorking = false;
            mIsSeek = false;
            if (!mPausedBeforeSeek) {
                mPlayerAdapter.resume();
                mIsPlaying = true;
            } else {
                mPlayerAdapter.setProgressUpdatingEnabled(false);
                // we neeed update UI since PlaybackControlRow still saves previous position.
                onUpdateProgress();
            }

        }
    }

    public interface OnTimeShiftAction {
        void showList();

        void seekToPreviousProgram();

        void seekToNextProgram();

        void tuneToLive();

        boolean checkUnLockContents();

        boolean canSeek(boolean prev);

    }


}
