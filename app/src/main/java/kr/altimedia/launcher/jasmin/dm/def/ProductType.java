/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.def;

import kr.altimedia.launcher.jasmin.R;

public enum ProductType {
    SINGLE("single", R.string.product_single), SERIES("series", R.string.product_series), PACKAGE("package", R.string.product_package),
    ERROR("ERROR", -1);

    private final String type;
    private final int mName;

    ProductType(String type, int mName) {
        this.type = type;
        this.mName = mName;
    }

    public static ProductType findByName(String name) {
        ProductType[] types = values();
        for (ProductType productType : types) {
            if (productType.type.equalsIgnoreCase(name)) {
                return productType;
            }
        }

        return ERROR;
    }

    public int getName() {
        return mName;
    }
}
