/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * An action contains one or two lines of text, an optional image and an optional id. It may also
 * be invoked by one or more keycodes.
 */
public class Action<T, K> {

    /** Indicates that an id has not been set. */
    public static final long NO_ID = -1;

    private long mId = NO_ID;
    private Drawable mIcon;
    private T mLabel1;
    private K mLabel2;
    private ArrayList<Integer> mKeyCodes = new ArrayList<>();

    /**
     * Constructor for an Action.
     *
     * @param id The id of the Action.
     */
    public Action(long id) {
        this(id, null);
    }

    /**
     * Constructor for an Action.
     *
     * @param id The id of the Action.
     * @param label The label to display for the Action.
     */
    public Action(long id, T label) {
        this(id, label, null);
    }

    /**
     * Constructor for an Action.
     *
     * @param id The id of the Action.
     * @param label1 The label to display on the first line of the Action.
     * @param label2 The label to display on the second line of the Action.
     */
    public Action(long id, T label1, K label2) {
        this(id, label1, label2, null);
    }

    /**
     * Constructor for an Action.
     *
     * @param id The id of the Action.
     * @param label1 The label to display on the first line of the Action.
     * @param label2 The label to display on the second line of the Action.
     * @param icon The icon to display for the Action.
     */
    public Action(long id, T label1, K label2, Drawable icon) {
        setId(id);
        setLabel1(label1);
        setLabel2(label2);
        setIcon(icon);
    }

    /**
     * Sets the id for this Action.
     */
    public final void setId(long id) {
        mId = id;
    }

    /**
     * Returns the id for this Action.
     */
    public final long getId() {
        return mId;
    }

    /**
     * Sets the first line label for this Action.
     */
    public final void setLabel1(T label) {
        mLabel1 = label;
    }

    /**
     * Returns the first line label for this Action.
     */
    public final T getLabel1() {
        return mLabel1;
    }

    /**
     * Sets the second line label for this Action.
     */
    public final void setLabel2(K label) {
        mLabel2 = label;
    }

    /**
     * Returns the second line label for this Action.
     */
    public final K getLabel2() {
        return mLabel2;
    }

    /**
     * Sets the icon drawable for this Action.
     */
    public final void setIcon(Drawable icon) {
        mIcon = icon;
    }

    /**
     * Returns the icon drawable for this Action.
     */
    public final Drawable getIcon() {
        return mIcon;
    }

    /**
     * Adds a keycode used to invoke this Action.
     */
    public final void addKeyCode(int keyCode) {
        mKeyCodes.add(keyCode);
    }

    /**
     * Removes a keycode used to invoke this Action.
     */
    public final void removeKeyCode(int keyCode) {
        mKeyCodes.remove(keyCode);
    }

    /**
     * Returns true if the Action should respond to the given keycode.
     */
    public final boolean respondsToKeyCode(int keyCode) {
        return mKeyCodes.contains(keyCode);
    }
}
