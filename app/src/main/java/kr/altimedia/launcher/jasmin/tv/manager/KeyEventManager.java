/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.manager;

import android.view.KeyEvent;

import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import com.altimedia.util.Log;

import static android.view.KeyEvent.KEYCODE_0;
import static android.view.KeyEvent.KEYCODE_1;
import static android.view.KeyEvent.KEYCODE_2;
import static android.view.KeyEvent.KEYCODE_3;
import static android.view.KeyEvent.KEYCODE_4;
import static android.view.KeyEvent.KEYCODE_5;
import static android.view.KeyEvent.KEYCODE_6;
import static android.view.KeyEvent.KEYCODE_7;
import static android.view.KeyEvent.KEYCODE_8;
import static android.view.KeyEvent.KEYCODE_9;
import static android.view.KeyEvent.KEYCODE_BACK;
import static android.view.KeyEvent.KEYCODE_CHANNEL_DOWN;
import static android.view.KeyEvent.KEYCODE_CHANNEL_UP;
import static android.view.KeyEvent.KEYCODE_DPAD_CENTER;
import static android.view.KeyEvent.KEYCODE_DPAD_DOWN;
import static android.view.KeyEvent.KEYCODE_DPAD_LEFT;
import static android.view.KeyEvent.KEYCODE_DPAD_RIGHT;
import static android.view.KeyEvent.KEYCODE_DPAD_UP;
import static android.view.KeyEvent.KEYCODE_ENTER;
import static android.view.KeyEvent.KEYCODE_ESCAPE;
import static android.view.KeyEvent.KEYCODE_GUIDE;
import static android.view.KeyEvent.KEYCODE_HOME;
import static android.view.KeyEvent.KEYCODE_INFO;
import static android.view.KeyEvent.KEYCODE_MEDIA_FAST_FORWARD;
import static android.view.KeyEvent.KEYCODE_MEDIA_NEXT;
import static android.view.KeyEvent.KEYCODE_MEDIA_PAUSE;
import static android.view.KeyEvent.KEYCODE_MEDIA_PLAY;
import static android.view.KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE;
import static android.view.KeyEvent.KEYCODE_MEDIA_PREVIOUS;
import static android.view.KeyEvent.KEYCODE_MEDIA_REWIND;
import static android.view.KeyEvent.KEYCODE_MEDIA_SKIP_BACKWARD;
import static android.view.KeyEvent.KEYCODE_MEDIA_SKIP_FORWARD;
import static android.view.KeyEvent.KEYCODE_MEDIA_STOP;
import static android.view.KeyEvent.KEYCODE_PROG_BLUE;
import static android.view.KeyEvent.KEYCODE_PROG_GREEN;
import static android.view.KeyEvent.KEYCODE_PROG_RED;
import static android.view.KeyEvent.KEYCODE_PROG_YELLOW;
import static android.view.KeyEvent.KEYCODE_TV;
import static android.view.KeyEvent.KEYCODE_UNKNOWN;
import static android.view.KeyEvent.keyCodeToString;

public class KeyEventManager {
    private static final boolean DEBUG = false;
    private static final boolean CORRECT_KEYEVENT = false;
    private static final String TAG = LiveTvActivity.class.getSimpleName() + "|" + KeyEventManager.class.getSimpleName();

    private static final int[][] UNKNOWN_KEY_CORRECT_TABLE = {
            //num keys
            {KEYCODE_UNKNOWN, 32, KEYCODE_0},
            {KEYCODE_UNKNOWN, 18, KEYCODE_1},
            {KEYCODE_UNKNOWN, 33, KEYCODE_2},
            {KEYCODE_UNKNOWN, 34, KEYCODE_3},
            {KEYCODE_UNKNOWN, 35, KEYCODE_4},
            {KEYCODE_UNKNOWN, 23, KEYCODE_5},
            {KEYCODE_UNKNOWN, 36, KEYCODE_6},
            {KEYCODE_UNKNOWN, 37, KEYCODE_7},
            {KEYCODE_UNKNOWN, 38, KEYCODE_8},
            {KEYCODE_UNKNOWN, 50, KEYCODE_9},

            //others
            {KEYCODE_UNKNOWN, 63, KEYCODE_INFO},
            {KEYCODE_UNKNOWN, 164, KEYCODE_PROG_GREEN},
            {KEYCODE_UNKNOWN, 166, KEYCODE_PROG_YELLOW},
            {KEYCODE_UNKNOWN, 168, KEYCODE_PROG_RED},
            {KEYCODE_UNKNOWN, 208, KEYCODE_PROG_BLUE},
            {KEYCODE_UNKNOWN, 358, KEYCODE_INFO},
            {KEYCODE_UNKNOWN, 362, KEYCODE_GUIDE},
            {KEYCODE_UNKNOWN, 377, KEYCODE_HOME},
            {KEYCODE_UNKNOWN, 402, KEYCODE_CHANNEL_UP},
            {KEYCODE_UNKNOWN, 403, KEYCODE_CHANNEL_DOWN},
            {KEYCODE_UNKNOWN, 580, KEYCODE_HOME},
    };

    private static final int[][] KEY_CORRECT_TABLE = {
            {KEYCODE_ENTER, KEYCODE_DPAD_CENTER},
    };

    public static boolean isNumberKey(int keyCode) {
        return keyCode >= KEYCODE_0 && keyCode <= KEYCODE_9;
    }

    public static boolean isTvKey(int keyCode) {
        return keyCode == KEYCODE_TV || keyCode == KEYCODE_GUIDE;
    }

    public static boolean isMediaStartKey(int keyCode) {
        switch (keyCode) {
            case KEYCODE_MEDIA_PLAY_PAUSE:
            case KEYCODE_MEDIA_PLAY:
            case KEYCODE_MEDIA_PAUSE:
            case KEYCODE_MEDIA_NEXT:
            case KEYCODE_MEDIA_PREVIOUS:
            case KEYCODE_MEDIA_REWIND:
            case KEYCODE_MEDIA_FAST_FORWARD:
            case KEYCODE_MEDIA_SKIP_FORWARD:
            case KEYCODE_MEDIA_SKIP_BACKWARD:
            case KEYCODE_MEDIA_STOP:
                return true;
            default:
                return false;
        }
    }

    public static boolean isNavigationKey(int keyCode) {
        switch (keyCode) {
            case KEYCODE_DPAD_CENTER:
            case KEYCODE_DPAD_UP:
            case KEYCODE_DPAD_DOWN:
            case KEYCODE_DPAD_RIGHT:
            case KEYCODE_DPAD_LEFT:
                return true;
            default:
                return false;
        }
    }

    public static boolean isExitKey(int keyCode) {
        switch (keyCode) {
            case KEYCODE_BACK:
            case KEYCODE_ESCAPE:
                return true;
            default:
                return false;
        }
    }

    public static int correctKeyCode(int keyCode, KeyEvent event) {
        if (CORRECT_KEYEVENT) {
            if (Log.INCLUDE && DEBUG) {
                Log.d(TAG, "correctKeyCode() org "+keyCodeToString(keyCode) + ", keyCode:" + keyCode + ", scancode:" + event.getScanCode());
            }

            int scanCode = event.getScanCode();
            if (keyCode == KEYCODE_UNKNOWN) {
                for (int[] t : UNKNOWN_KEY_CORRECT_TABLE) {
                    if (t[0] == keyCode && t[1] == scanCode) {
                        keyCode = t[2];
                        break;
                    }
                }
            } else {
                for (int[] t : KEY_CORRECT_TABLE) {
                    if (t[0] == keyCode) {
                        keyCode = t[1];
                        break;
                    }
                }
            }

            if (Log.INCLUDE && DEBUG) {
                Log.d(TAG, "correctKeyCode() new "+keyCodeToString(keyCode) + ", keyCode:" + keyCode + ", scancode:" + event.getScanCode());
            }
        }
        return keyCode;
    }
}
