/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

/**
 * Created by mc.kim on 09,06,2020
 */
public enum DurationUnit {
    UNIT_UNLIMITED("unLimited"), UNIT_MONTH("M"),
    UNIT_YEAR("Y"), UNIT_DAY("D");

    final String unitName;

    DurationUnit(String unitName) {
        this.unitName = unitName;
    }

    public static final DurationUnit findByName(String name) {
        DurationUnit[] types = values();
        for (DurationUnit durationUnit : types) {
            if (durationUnit.unitName.equalsIgnoreCase(name)) {
                return durationUnit;
            }
        }
        return UNIT_UNLIMITED;
    }
}
