/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.transition.Visibility;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import kr.altimedia.launcher.jasmin.R;

public class FadeAndShortSlide extends Visibility {
    private static final TimeInterpolator sDecelerate = new DecelerateInterpolator();
    private static final String PROPNAME_SCREEN_POSITION = "android:fadeAndShortSlideTransition:screenPosition";
    private FadeAndShortSlide.CalculateSlide mSlideCalculator;
    private Visibility mFade;
    private float mDistance;
    static final CalculateSlide sCalculateStart = new CalculateSlide() {
        public float getGoneX(FadeAndShortSlide t, ViewGroup sceneRoot, View view, int[] position) {
            boolean isRtl = sceneRoot.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;
            float x;
            if (isRtl) {
                x = view.getTranslationX() + t.getHorizontalDistance(sceneRoot);
            } else {
                x = view.getTranslationX() - t.getHorizontalDistance(sceneRoot);
            }

            return x;
        }
    };
    static final FadeAndShortSlide.CalculateSlide sCalculateEnd = new FadeAndShortSlide.CalculateSlide() {
        public float getGoneX(FadeAndShortSlide t, ViewGroup sceneRoot, View view, int[] position) {
            boolean isRtl = sceneRoot.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;
            float x;
            if (isRtl) {
                x = view.getTranslationX() - t.getHorizontalDistance(sceneRoot);
            } else {
                x = view.getTranslationX() + t.getHorizontalDistance(sceneRoot);
            }

            return x;
        }
    };
    static final FadeAndShortSlide.CalculateSlide sCalculateStartEnd = new FadeAndShortSlide.CalculateSlide() {
        public float getGoneX(FadeAndShortSlide t, ViewGroup sceneRoot, View view, int[] position) {
            int viewCenter = position[0] + view.getWidth() / 2;
            sceneRoot.getLocationOnScreen(position);
            Rect center = t.getEpicenter();
            int sceneRootCenter = center == null ? position[0] + sceneRoot.getWidth() / 2 : center.centerX();
            return viewCenter < sceneRootCenter ? view.getTranslationX() - t.getHorizontalDistance(sceneRoot) : view.getTranslationX() + t.getHorizontalDistance(sceneRoot);
        }
    };
    static final FadeAndShortSlide.CalculateSlide sCalculateBottom = new FadeAndShortSlide.CalculateSlide() {
        public float getGoneY(FadeAndShortSlide t, ViewGroup sceneRoot, View view, int[] position) {
            return view.getTranslationY() + t.getVerticalDistance(sceneRoot);
        }
    };
    static final FadeAndShortSlide.CalculateSlide sCalculateTop = new FadeAndShortSlide.CalculateSlide() {
        public float getGoneY(FadeAndShortSlide t, ViewGroup sceneRoot, View view, int[] position) {
            return view.getTranslationY() - t.getVerticalDistance(sceneRoot);
        }
    };
    final FadeAndShortSlide.CalculateSlide sCalculateTopBottom;

    float getHorizontalDistance(ViewGroup sceneRoot) {
        return this.mDistance >= 0.0F ? this.mDistance : (float)(sceneRoot.getWidth() / 4);
    }

    float getVerticalDistance(ViewGroup sceneRoot) {
        return this.mDistance >= 0.0F ? this.mDistance : (float)(sceneRoot.getHeight() / 4);
    }

    public FadeAndShortSlide() {
        this(8388611);
    }

    public FadeAndShortSlide(int slideEdge) {
        this.mFade = new Fade();
        this.mDistance = -1.0F;
        this.sCalculateTopBottom = new FadeAndShortSlide.CalculateSlide() {
            public float getGoneY(FadeAndShortSlide t, ViewGroup sceneRoot, View view, int[] position) {
                int viewCenter = position[1] + view.getHeight() / 2;
                sceneRoot.getLocationOnScreen(position);
                Rect center = FadeAndShortSlide.this.getEpicenter();
                int sceneRootCenter = center == null ? position[1] + sceneRoot.getHeight() / 2 : center.centerY();
                return viewCenter < sceneRootCenter ? view.getTranslationY() - t.getVerticalDistance(sceneRoot) : view.getTranslationY() + t.getVerticalDistance(sceneRoot);
            }
        };
        this.setSlideEdge(slideEdge);
    }

    public FadeAndShortSlide(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mFade = new Fade();
        this.mDistance = -1.0F;
        this.sCalculateTopBottom = new FadeAndShortSlide.CalculateSlide() {
            public float getGoneY(FadeAndShortSlide t, ViewGroup sceneRoot, View view, int[] position) {
                int viewCenter = position[1] + view.getHeight() / 2;
                sceneRoot.getLocationOnScreen(position);
                Rect center = FadeAndShortSlide.this.getEpicenter();
                int sceneRootCenter = center == null ? position[1] + sceneRoot.getHeight() / 2 : center.centerY();
                return viewCenter < sceneRootCenter ? view.getTranslationY() - t.getVerticalDistance(sceneRoot) : view.getTranslationY() + t.getVerticalDistance(sceneRoot);
            }
        };
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.lbSlide);
        int edge = a.getInt(R.styleable.lbSlide_lb_slideEdge, 8388611);
        this.setSlideEdge(edge);
        a.recycle();
    }

    public void setEpicenterCallback(EpicenterCallback epicenterCallback) {
        this.mFade.setEpicenterCallback(epicenterCallback);
        super.setEpicenterCallback(epicenterCallback);
    }

    private void captureValues(TransitionValues transitionValues) {
        View view = transitionValues.view;
        int[] position = new int[2];
        view.getLocationOnScreen(position);
        transitionValues.values.put("android:fadeAndShortSlideTransition:screenPosition", position);
    }

    public void captureStartValues(TransitionValues transitionValues) {
        this.mFade.captureStartValues(transitionValues);
        super.captureStartValues(transitionValues);
        this.captureValues(transitionValues);
    }

    public void captureEndValues(TransitionValues transitionValues) {
        this.mFade.captureEndValues(transitionValues);
        super.captureEndValues(transitionValues);
        this.captureValues(transitionValues);
    }

    public void setSlideEdge(int slideEdge) {
        switch(slideEdge) {
            case 48:
                this.mSlideCalculator = sCalculateTop;
                break;
            case 80:
                this.mSlideCalculator = sCalculateBottom;
                break;
            case 112:
                this.mSlideCalculator = this.sCalculateTopBottom;
                break;
            case 8388611:
                this.mSlideCalculator = sCalculateStart;
                break;
            case 8388613:
                this.mSlideCalculator = sCalculateEnd;
                break;
            case 8388615:
                this.mSlideCalculator = sCalculateStartEnd;
                break;
            default:
                throw new IllegalArgumentException("Invalid slide direction");
        }

    }

    public Animator onAppear(ViewGroup sceneRoot, View view, TransitionValues startValues, TransitionValues endValues) {
        if (endValues == null) {
            return null;
        } else if (sceneRoot == view) {
            return null;
        } else {
            int[] position = (int[])((int[])endValues.values.get("android:fadeAndShortSlideTransition:screenPosition"));
            int left = position[0];
            int top = position[1];
            float endX = view.getTranslationX();
            float startX = this.mSlideCalculator.getGoneX(this, sceneRoot, view, position);
            float endY = view.getTranslationY();
            float startY = this.mSlideCalculator.getGoneY(this, sceneRoot, view, position);
            Animator slideAnimator = TranslationAnimationCreator.createAnimation(view, endValues, left, top, startX, startY, endX, endY, sDecelerate, this);
            Animator fadeAnimator = this.mFade.onAppear(sceneRoot, view, startValues, endValues);
            if (slideAnimator == null) {
                return fadeAnimator;
            } else if (fadeAnimator == null) {
                return slideAnimator;
            } else {
                AnimatorSet set = new AnimatorSet();
                set.play(slideAnimator).with(fadeAnimator);
                return set;
            }
        }
    }

    public Animator onDisappear(ViewGroup sceneRoot, View view, TransitionValues startValues, TransitionValues endValues) {
        if (startValues == null) {
            return null;
        } else if (sceneRoot == view) {
            return null;
        } else {
            int[] position = (int[])((int[])startValues.values.get("android:fadeAndShortSlideTransition:screenPosition"));
            int left = position[0];
            int top = position[1];
            float startX = view.getTranslationX();
            float endX = this.mSlideCalculator.getGoneX(this, sceneRoot, view, position);
            float startY = view.getTranslationY();
            float endY = this.mSlideCalculator.getGoneY(this, sceneRoot, view, position);
            Animator slideAnimator = TranslationAnimationCreator.createAnimation(view, startValues, left, top, startX, startY, endX, endY, sDecelerate, this);
            Animator fadeAnimator = this.mFade.onDisappear(sceneRoot, view, startValues, endValues);
            if (slideAnimator == null) {
                return fadeAnimator;
            } else if (fadeAnimator == null) {
                return slideAnimator;
            } else {
                AnimatorSet set = new AnimatorSet();
                set.play(slideAnimator).with(fadeAnimator);
                return set;
            }
        }
    }

    public Transition addListener(TransitionListener listener) {
        this.mFade.addListener(listener);
        return super.addListener(listener);
    }

    public Transition removeListener(TransitionListener listener) {
        this.mFade.removeListener(listener);
        return super.removeListener(listener);
    }

    public float getDistance() {
        return this.mDistance;
    }

    public void setDistance(float distance) {
        this.mDistance = distance;
    }

    public Transition clone() {
        FadeAndShortSlide clone = null;
        clone = (FadeAndShortSlide)super.clone();
        clone.mFade = (Visibility)this.mFade.clone();
        return clone;
    }

    private abstract static class CalculateSlide {
        CalculateSlide() {
        }

        float getGoneX(FadeAndShortSlide t, ViewGroup sceneRoot, View view, int[] position) {
            return view.getTranslationX();
        }

        float getGoneY(FadeAndShortSlide t, ViewGroup sceneRoot, View view, int[] position) {
            return view.getTranslationY();
        }
    }
}
