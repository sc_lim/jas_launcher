/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail;

import android.view.View;

import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class BaseShadowHelper implements VerticalGridView.OnScrollOffsetCallback {
    protected View parentView = null;

    public BaseShadowHelper() {
    }

    @Override
    public void onScrolledOffsetCallback(int offset, int remainScroll, int totalScroll) {

    }

    public void setParentView(View parentView) {
        this.parentView = parentView;
    }
}
