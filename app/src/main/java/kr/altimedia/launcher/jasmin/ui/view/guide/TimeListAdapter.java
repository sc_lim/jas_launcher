/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.res.Resources;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import com.altimedia.tvmodule.util.Utils;
import com.altimedia.util.Log;

/**
 * Adapts the time range from {@link ProgramManager} to the timeline header row of the program guide
 * table.
 */
class TimeListAdapter extends RecyclerView.Adapter<TimeListAdapter.TimeViewHolder> {
    private final String TAG= TimeListAdapter.class.getSimpleName();
    private static final long TIME_UNIT_MS = TimeUnit.MINUTES.toMillis(30);

    // Ex. 3:00 AM
    private static final String TIME_PATTERN_SAME_DAY = "HH:mm";
    // Ex. Oct 21, 3:00 AM

    private static int sRowHeaderOverlapping;

    // Nearest half hour at or before the start time.
    private long mStartUtcMs;
    private final String mTimePatternSameDay;
    private final String mTimePatternDifferentDay;

    TimeListAdapter(Resources res) {
//        if (sRowHeaderOverlapping == 0) {
//            sRowHeaderOverlapping =
//                    Math.abs(
//                            res.getDimensionPixelOffset(
//                                    R.dimen.program_guide_table_header_row_overlap));
//        }

        String format = res.getString(R.string.EEE_dd_MMM);
        Locale locale = res.getConfiguration().locale;
        mTimePatternSameDay = DateFormat.getBestDateTimePattern(locale, TIME_PATTERN_SAME_DAY);
        mTimePatternDifferentDay =  format;
    }

    public void update(long startTimeMs) {
        mStartUtcMs = startTimeMs;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.program_guide_table_header_row_item;
    }

    @Override
    public void onBindViewHolder(TimeViewHolder holder, int position) {
        long startTime = mStartUtcMs + position * TIME_UNIT_MS;
        long endTime = startTime + TIME_UNIT_MS;

        if(Log.INCLUDE){
            Log.d(TAG,"onBindViewHolder : "+position +", startTime : "+new Date(startTime));
        }

        View itemView = holder.itemView;
        Date timeDate = new Date(startTime);
        String timeString;
        timeString = DateFormat.format(mTimePatternSameDay, timeDate).toString();
        String dateTime = DateFormat.format(mTimePatternDifferentDay, timeDate).toString();
        if (Utils.isInGivenDay(JasmineEpgApplication.SystemClock().currentTimeMillis(), startTime)) {
            dateTime = itemView.getResources().getString(R.string.today);
        }
        ((TextView) itemView.findViewById(R.id.time)).setText(timeString);
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        lp.width = GuideUtils.convertMillisToPixel(startTime, endTime);
//        if (position == 0) {
//            // Adjust width for the first entry to make the item starts from the fading edge.
//            lp.setMarginStart(sRowHeaderOverlapping - lp.width / 2);
//        } else {
//            lp.setMarginStart(0);
//        }
        itemView.setLayoutParams(lp);
    }

    @Override
    public TimeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new TimeViewHolder(itemView);
    }

    static class TimeViewHolder extends RecyclerView.ViewHolder {
        TimeViewHolder(View itemView) {
            super(itemView);
        }
    }

}
