/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.NetworkUtil;

import androidx.fragment.app.FragmentManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.contents.ContentDataManager;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodDetailsDialogFragment;

public class Linker {
    private static final String TAG = Linker.class.getSimpleName();

    public static final String ACTION_START_MYMENU = "altimedia.intent.action.StartMyMenu";

    public static void gotoMyMenu(TvOverlayManager mTvOverlayManager, FragmentManager fragmentManager, Context context){
        if (mTvOverlayManager == null) {
            return;
        }

        if (!NetworkUtil.hasNetworkConnection(context)) {
            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(MbsTaskCallback.REASON_NETWORK, context);
            failNoticeDialogFragment.show(mTvOverlayManager, fragmentManager, FailNoticeDialogFragment.CLASS_NAME);
            return;
        }

        if (!AccountManager.getInstance().isAuthenticatedUser()) {
            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(MbsTaskCallback.REASON_LOGIN, context);
            failNoticeDialogFragment.show(mTvOverlayManager, fragmentManager, FailNoticeDialogFragment.CLASS_NAME);
            return;
        }

        Intent intent = new Intent(ACTION_START_MYMENU);
        intent.setPackage(context.getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void goToVodDetail(TvOverlayManager mTvOverlayManager, FragmentManager fragmentManager, String contentId) {
        AccountManager accountMgr = AccountManager.getInstance();
        ContentDataManager dataManager = new ContentDataManager();
        dataManager.getContentDetail(
                accountMgr.getLocalLanguage(),
                accountMgr.getSaId(),
                accountMgr.getProfileId(),
                contentId,"",
                new MbsDataProvider<String, ContentDetailInfo>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        Log.d(TAG, "onFailed: key=" + key);
                    }

                    @Override
                    public void onSuccess(String id, ContentDetailInfo result) {
                        try {
                            VodDetailsDialogFragment dialogFragment;
                            Content content = result.getContent();
//                            if (content.isSeries()) {
//                                dialogFragment = VodDetailsDialogFragment.newInstance(content.getSeriesAssetId(), content.getCategoryId(), content.getContentGroupId(), 1);
//                            } else {
                                dialogFragment = VodDetailsDialogFragment.newInstance(content, 1);
//                            }
                            if (mTvOverlayManager != null) {
                                mTvOverlayManager.showDialogFragment(dialogFragment);
                            }
                        }catch (Exception e){
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(fragmentManager, ErrorDialogFragment.CLASS_NAME);
                    }
                });
    }

    public static void goToVodDetail(TvOverlayManager mTvOverlayManager, Content content) {
        VodDetailsDialogFragment dialogFragment = VodDetailsDialogFragment.newInstance(content, 1);
        if (mTvOverlayManager != null) {
            mTvOverlayManager.showDialogFragment(dialogFragment);
        }
    }

    public static void goToSeriesVodDetail(TvOverlayManager mTvOverlayManager, String seriesAssetId, String categoryId, String contentId) {
        VodDetailsDialogFragment dialogFragment = VodDetailsDialogFragment.newInstance(seriesAssetId, categoryId, contentId, 1);
        if (mTvOverlayManager != null) {
            mTvOverlayManager.showDialogFragment(dialogFragment);
        }
    }

    public static void goToPackageVodDetail(TvOverlayManager mTvOverlayManager, String packageId, int rating) {
        VodDetailsDialogFragment dialogFragment = VodDetailsDialogFragment.newInstance(packageId, "-1", rating, 1);
        if (mTvOverlayManager != null) {
            mTvOverlayManager.showDialogFragment(dialogFragment);
        }
    }

}
