/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.MainThread;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

/**
 * Created by mc.kim on 07,06,2020
 */
class DateListAdapter extends RecyclerView.Adapter<DateListAdapter.DateRowHolder> {
    private static final String TAG = "DateListAdapter";
    private static final boolean DEBUG = false;

    private final Context mContext;
    private final ProgramManager mProgramManager;
    private final ProgramGuide mProgramGuide;
    private ArrayList<Long> mDateList = new ArrayList<>();
    private static long mLastRequestedDate = 0;
    private final VerticalGridView mRecyclerView;

    DateListAdapter(Context context, ProgramManager programManager, ProgramGuide guide, VerticalGridView recyclerView) {
        mContext = context;
        mRecyclerView = recyclerView;
        mProgramManager = programManager;
        mProgramManager.addListener(
                new ProgramManager.ListenerAdapter() {
                    @Override
                    public void onTimeRangeUpdated(boolean keepFocus) {
                        super.onTimeRangeUpdated(keepFocus);
                        initialDateList();
                        notifyDataSetChanged();
                    }
                });
        mProgramGuide = guide;
        initialDateList();
    }

    private ArrayList<Long> makeTimeRange(long fromUtcMillis, long toUtcMillis) {
        if (Log.INCLUDE) {
            Log.d(TAG, "makeTimeRange | fromUtcMillis : " + fromUtcMillis + ", toUtcMillis : " + toUtcMillis);
        }
        Calendar fromUtcMillisDate = Calendar.getInstance();
        fromUtcMillisDate.setTimeInMillis(fromUtcMillis);
        Calendar checkDate = Calendar.getInstance();
        checkDate.setTimeInMillis(fromUtcMillis);
        ArrayList<Long> filerDate = new ArrayList<>();
        while (true) {
            if (checkDate.getTimeInMillis() < toUtcMillis) {
                filerDate.add(checkDate.getTimeInMillis());
                checkDate.add(Calendar.DATE, 1);
            } else {
                break;
            }
        }
        return filerDate;
    }

    private void initialDateList() {
        mDateList.clear();
        ArrayList<Long> timeRange = makeTimeRange(
                mProgramManager.getStartUtcMillis(), mProgramManager.getEndUtcMillis());
        mDateList.addAll(timeRange);
    }

    public void setLastRequestedDate(long lastRequestedDate) {
        mLastRequestedDate = lastRequestedDate;
        notifyDataSetChanged();

        int index = findIndex(lastRequestedDate);
        mRecyclerView.setSelectedPosition(index);
    }

    private int findIndex(long lastRequestedDate) {
        String format = mContext.getString(R.string.EEE_dd_MMM);
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        String selectedValue = dateFormat.format(new Date(mLastRequestedDate));
        for (int i = 0; i < mDateList.size(); i++) {
            long date = mDateList.get(i);
            String filterValue = dateFormat.format(new Date(date));
            if (selectedValue.equals(filterValue)) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return mDateList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.program_guide_date_panel_row;
    }

    @Override
    public void onBindViewHolder(DateRowHolder holder, int position) {
        holder.onBind(mDateList.get(position), mDateList.size());
    }

    @Override
    public DateRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        itemView.addOnAttachStateChangeListener(
                new View.OnAttachStateChangeListener() {
                    @Override
                    public void onViewAttachedToWindow(View view) {
                    }

                    @Override
                    public void onViewDetachedFromWindow(View view) {
                    }
                });
        return new DateRowHolder(itemView, mProgramGuide, mRecyclerView);
    }

    public static String getFilterDate(Context context, long time) {
        String format = context.getString(R.string.EEE_dd_MMM);
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        String filterValue = dateFormat.format(new Date(time));
        String today = dateFormat.format(new Date());
        if (filterValue.equalsIgnoreCase(today)) {
            filterValue = context.getResources().getString(R.string.today);
        }
        return filterValue;
    }

    static class DateRowHolder extends RecyclerView.ViewHolder
            implements View.OnFocusChangeListener, View.OnClickListener, View.OnKeyListener {
        private final ProgramGuide mProgramGuide;
        private final TextView filterName;
        private final ImageView filterSelected;
        private long mDateLongTime;
        private final Context mContext;
        private final VerticalGridView mRecyclerView;

        @MainThread
        DateRowHolder(View itemView, ProgramGuide programGuide, VerticalGridView recyclerView) {
            super(itemView);
            mRecyclerView = recyclerView;
            mContext = itemView.getContext();
            mProgramGuide = programGuide;
            filterName = itemView.findViewById(R.id.filterName);
            filterSelected = itemView.findViewById(R.id.filterSelected);
        }

        private int mMaxSize = 0;

        public void onBind(long dateLong, int maxSize) {
            mDateLongTime = dateLong;
            mMaxSize = maxSize;
            String format = mContext.getString(R.string.EEE_dd_MMM);
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            String filterValue = dateFormat.format(new Date(dateLong));
            String selectedValue = dateFormat.format(new Date(mLastRequestedDate));
            filterSelected.setVisibility(selectedValue.equalsIgnoreCase(filterValue) ? View.VISIBLE : View.INVISIBLE);
            filterName.setText(getFilterDate(itemView.getContext(), dateLong));
            itemView.setOnFocusChangeListener(this);
            itemView.setOnClickListener(this);
            itemView.setOnKeyListener(this);
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                int oldPosition = getAdapterPosition();
                if (Log.INCLUDE) {
                    Log.d(TAG, "old position : " + oldPosition);
                }
                if (oldPosition == 0 && keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                    mRecyclerView.setSelectedPosition(mMaxSize - 1);
                    return true;
                } else if (oldPosition == mMaxSize - 1
                        && keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                    mRecyclerView.setSelectedPosition(0);
                    return true;
                }
            }
            return false;
        }

        @Override
        public void onClick(View view) {
            mProgramGuide.requestDateChange(mDateLongTime);
        }

        @Override
        public void onFocusChange(View view, boolean hasFocus) {
        }
    }
}