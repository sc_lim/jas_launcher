/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by mc.kim on 18,05,2020
 */
public interface MbsTaskCallback<K, V> {
    String ERROR_IO = "IO_Exception";
    String ERROR_JSON = "Json_Exception";
    String ERROR_NPE = "NullPoint_Exception";
    int REASON_NETWORK = 500;
    int REASON_NULL_OBJECT = 600;
    int REASON_LOGIN = 400;
    int REASON_PROFILE_LOGIN = 500;

    Object onTaskBackGround(K key) throws IOException, ResponseException, AuthenticationException;

    void onError(String resultCode, String responseMessage);

    void onTaskPostExecute(K key, V result);

    void onTaskFailed(int reason);

    void onPreExecute();

    Object onCanceled(K key);
}
