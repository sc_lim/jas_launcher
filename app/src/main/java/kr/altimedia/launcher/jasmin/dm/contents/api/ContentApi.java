/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.api;

import com.google.gson.JsonObject;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.BannerListResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.ContentDetailResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.ContentsListResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.OTUResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.PackageDetailResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.SeasonListResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.SeriesListResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by mc.kim on 08,05,2020
 */
public interface ContentApi {
    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("contentlist")
    Call<ContentsListResponse> getContentsList(@Query("language") String language,
                                               @Query("said") String said, @Query("profileId") String profileID,
                                               @Query("categoryId") String categoryID,
                                               @Query("startNo") int startNo,
                                               @Query("contentCnt") int contentCnt,
                                               @Query("contentBannerFlag") String contentBannerFlag);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("voddetail")
    Call<ContentDetailResponse> getContentDetail(@Query("language") String language,
                                                 @Query("said") String said, @Query("profileId") String profileID,
                                                 @Query("contentId") String contentId);


    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("packagedetail")
    Call<PackageDetailResponse> getPackageDetail(@Query("language") String language,
                                                 @Query("said") String said,
                                                 @Query("packageId") String packageId,
                                                 @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("bannerlist")
    Call<BannerListResponse> getBannerList(@Query("language") String language,
                                           @Query("said") String said,
                                           @Query("bannerPositionId") String bannerPositionID);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("serieslist")
    Call<SeriesListResponse> getSeriesList(@Query("language") String language,
                                           @Query("said") String said,
                                           @Query("profileId") String profileID,
                                           @Query("seriesAssetId") String seriesAssetId,
                                           @Query("contentId") String contentId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("seasonlist")
    Call<SeasonListResponse> getSeasonList(@Query("language") String language,
                                           @Query("said") String said,
                                           @Query("profileId") String profileID,
                                           @Query("seriesAssetId") String seriesAssetId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("recentwatchinglist")
    Call<ContentsListResponse> getRecentWatchingList(@Query("language") String language,
                                                     @Query("said") String said,
                                                     @Query("profileId") String profileID,
                                                     @Query("contentCnt") int contentCnt);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("vodlike")
    Call<BaseResponse> postVodLike(@Body JsonObject bodyData);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @DELETE("vodlike")
    Call<BaseResponse> deleteVodLike(@Query("said") String said,
                                     @Query("profileId") String profileID,
                                     @Query("contentId") String contentId,
                                     @Query("type") String type);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("nodeip")
    Call<OTUResponse> getOneTimeUrl(@Query("said") String said, @Query("profileId") String profileId
            , @Query("payYN") String payYN, @Query("contentType") String contentType
            , @Query("contentPath") String contentPath, @Query("movieId") String contentId
            , @Query("resumeYn") String resumeYn
            , @Query("categoryId") String categoryId
            , @Query("isPreview") String isPreview);

}
