/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

/**
 * Created by mc.kim on 01,06,2020
 */
public enum StreamType {
    Main("Main"), Trailer("Trailer"), Preview("Preview");
    private final String name;

    StreamType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static StreamType containsByName(String name) {
        StreamType[] typeList = values();
        for (StreamType type : typeList) {
            if(name.toLowerCase().contains(type.name.toLowerCase())){
                return type;
            }
        }
        return null;
    }

}
