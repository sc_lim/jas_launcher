/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.view.View;
import android.view.ViewGroup;

public class Scale extends Transition {
    private static final String PROPNAME_SCALE = "android:leanback:scale";

    public Scale() {
    }

    private void captureValues(TransitionValues values) {
        View view = values.view;
        values.values.put("android:leanback:scale", view.getScaleX());
    }

    public void captureStartValues(TransitionValues transitionValues) {
        this.captureValues(transitionValues);
    }

    public void captureEndValues(TransitionValues transitionValues) {
        this.captureValues(transitionValues);
    }

    public Animator createAnimator(ViewGroup sceneRoot, TransitionValues startValues, TransitionValues endValues) {
        if (startValues != null && endValues != null) {
            float startScale = (Float)startValues.values.get("android:leanback:scale");
            float endScale = (Float)endValues.values.get("android:leanback:scale");
            final View view = startValues.view;
            view.setScaleX(startScale);
            view.setScaleY(startScale);
            ValueAnimator animator = ValueAnimator.ofFloat(startScale, endScale);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator animation) {
                    float scale = (Float)animation.getAnimatedValue();
                    view.setScaleX(scale);
                    view.setScaleY(scale);
                }
            });
            return animator;
        } else {
            return null;
        }
    }
}
