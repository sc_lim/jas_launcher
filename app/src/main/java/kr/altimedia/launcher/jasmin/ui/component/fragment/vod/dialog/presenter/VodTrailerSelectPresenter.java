/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.ContentStreamDeserializer;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class VodTrailerSelectPresenter extends Presenter {
    private final static String TAG = VodTrailerSelectPresenter.class.getSimpleName();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_vod_trailer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((StreamInfo) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }


    private void loadThumbnailPoster(StreamInfo info, ImageView poster) {
        String url = info.getPosterUrl();
        if (Log.INCLUDE) {
            Log.d(TAG, "url : " + url);
        }
        Glide.with(poster.getContext())
                .load(url).diskCacheStrategy(DiskCacheStrategy.DATA)
                .centerCrop()
                .placeholder(R.drawable.detail_series_default).
                error(R.drawable.detail_series_default)
                .into(poster);
    }


    private class ViewHolder extends Presenter.ViewHolder {
        private ImageView poster;
        private TextView title;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            poster = view.findViewById(R.id.poster);
            title = view.findViewById(R.id.title);
        }

        private long getMinValue(long timeMillis) {
            return timeMillis / TimeUtil.MIN;
        }

        public void setData(StreamInfo previewStreamInfo) {
            if (ContentStreamDeserializer.TITLE_MAIN_PREVIEW.equalsIgnoreCase(previewStreamInfo.getTitle())
                    && previewStreamInfo.getPreviewInfo() != null) {
                long previewTime = previewStreamInfo.getPreviewInfo().getFreePreviewDuration();
                Context context = title.getContext();
                String minData = context.getString(R.string.minPreview);
                title.setText(getMinValue(previewTime) + minData);
            } else {
                title.setText(previewStreamInfo.getTitle());
            }
            loadThumbnailPoster(previewStreamInfo, poster);
        }


    }
}
