/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.obj.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.RequestVodPaymentResultInfo;

public class RequestVodPaymentResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private RequestVodPaymentResultInfo requestVodPaymentResultInfo;

    protected RequestVodPaymentResponse(Parcel in) {
        super(in);
        requestVodPaymentResultInfo = in.readParcelable(RequestVodPaymentResultInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(requestVodPaymentResultInfo, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RequestVodPaymentResponse> CREATOR = new Creator<RequestVodPaymentResponse>() {
        @Override
        public RequestVodPaymentResponse createFromParcel(Parcel in) {
            return new RequestVodPaymentResponse(in);
        }

        @Override
        public RequestVodPaymentResponse[] newArray(int size) {
            return new RequestVodPaymentResponse[size];
        }
    };

    public RequestVodPaymentResultInfo getRequestVodPaymentResultInfo() {
        return requestVodPaymentResultInfo;
    }
}
