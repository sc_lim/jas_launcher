/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.playback;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.player.AltiMediaSource;
import com.altimedia.util.Log;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PlacementDecision;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PreviewInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.media.MediaPlayerAdapter;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.playbck.EndWatchingVodDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.playbck.presenter.VodEndActionPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelLayout;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackSupportFragment;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.PreviewPlaybackSupportFragmentGlueHost;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.PreviewPlaybackTransportControlGlue;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackGlue;

/**
 * Created by mc.kim on 07,05,2020
 */
public class PreviewVideoFragment extends VideoPlaybackSupportFragment
        implements EndWatchingVodDialogFragment.OnVodWatchEndListener {
    private final String TAG = PreviewVideoFragment.class.getSimpleName();


    static final int SURFACE_NOT_CREATED = 0;
    static final int SURFACE_CREATED = 1;

    SurfaceView mVideoSurface;
    SurfaceHolder.Callback mMediaPlaybackCallback;

    int mState = SURFACE_NOT_CREATED;
    private MediaPlayerAdapter mPlayerAdapter = null;
    private PreviewPlaybackTransportControlGlue<MediaPlayerAdapter> mTransportControlGlue;


    public static PreviewVideoFragment newInstance(Bundle bundle) {
        PreviewVideoFragment fragment = new PreviewVideoFragment();
        fragment.setArguments(bundle);
        fragment.setIsEnableUseVideoSetting(false);
        return fragment;
    }

    private SidePanelLayout.SidePanelActionListener mSidePanelActionListener = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof SidePanelLayout.SidePanelActionListener) {
            mSidePanelActionListener = (SidePanelLayout.SidePanelActionListener) context;
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Parcelable movie =
                getArguments().getParcelable(VideoPlaybackActivity.KEY_MOVIE);
        final int trailerIndex = getArguments().getInt(VideoPlaybackActivity.KEY_MOVIE_INDEX, 0);
        final String title;
        final String contentsId;
        final String categoryId;
        final String movieId;
        final String synopsis;
        final List<StreamInfo> streamInfoList;
        if (movie instanceof Content) {
            Content content = (Content) movie;
            contentsId = content.getContentGroupId();
            categoryId = content.getCategoryId();
            movieId = content.getMovieAssetId();
            title = content.getTitle();
            synopsis = content.getSynopsis();
            streamInfoList = content.getTrailerStreamInfo();
        } else {
            SeriesContent content = (SeriesContent) movie;
            contentsId = content.getContentGroupId();
            categoryId = content.getCategoryId();
            movieId = content.getMovieAssetId();
            title = content.getSeriesName();
            synopsis = "";
            streamInfoList = content.getTrailer();
        }
        PreviewPlaybackSupportFragmentGlueHost glueHost =
                new PreviewPlaybackSupportFragmentGlueHost(PreviewVideoFragment.this);

        mPlayerAdapter = new MediaPlayerAdapter(getContext());

        mTransportControlGlue = new PreviewPlaybackTransportControlGlue<>(getContext(), mPlayerAdapter);
        PreviewInfo previewInfo = streamInfoList.get(trailerIndex).getPreviewInfo();
        mTransportControlGlue.readyPreviewTimer(previewInfo.getFreePreviewDuration());
        mTransportControlGlue.setSeekEnabled(false);

        mTransportControlGlue.addPlayerCallback(new VideoPlaybackGlue.PlayerCallback() {
            @Override
            public void onPreparedStateChanged(VideoPlaybackGlue glue) {
                super.onPreparedStateChanged(glue);
            }

            @Override
            public void onPlayStateChanged(VideoPlaybackGlue glue) {
                super.onPlayStateChanged(glue);
            }

            @Override
            public void onPlayCompleted(VideoPlaybackGlue glue) {
                super.onPlayCompleted(glue);
                onBackPressed();
            }
        });
        mTransportControlGlue.setHost(glueHost);
        mTransportControlGlue.setTitle(title);
        mTransportControlGlue.setSubtitle(synopsis);
        mTransportControlGlue.playWhenPrepared();
        PlaybackUtil.getPlayResource(getContext(), contentsId, movieId, streamInfoList.get(trailerIndex), categoryId, new PlaybackUtil.PlayResourceCallback() {
            @Override
            public void onResult(AltiMediaSource[] mediaSource, List<PlacementDecision> placementDecisions) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "url : " + mediaSource.toString());
                }
                mPlayerAdapter.setDataSource(mediaSource, previewInfo.getFreePreviewStartPosition(), true);
            }

            @Override
            public void onFail(int reason) {

            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, errorMessage);
                dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        getActivity().finish();
                    }
                });
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mTransportControlGlue != null) {
            mTransportControlGlue.pause();
        }
    }


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        mVideoSurface = (SurfaceView) LayoutInflater.from(getContext()).inflate(
                R.layout.layer_vo_surface, root, false);
        root.addView(mVideoSurface, 0);
        mVideoSurface.getHolder().setFormat(PixelFormat.RGB_888);
        mVideoSurface.getHolder().addCallback(mPlayerAdapter.getVideoPlayerSurfaceHolderCallback());

        mVideoSurface.getHolder().addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceCreated(holder);
                }
                mState = SURFACE_CREATED;
                mPlayerAdapter.setDisplay(holder);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceChanged(holder, format, width, height);
                }
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceDestroyed(holder);
                }
                mState = SURFACE_NOT_CREATED;
                mPlayerAdapter.setDisplay(null);
            }
        });

        setBackgroundType(VideoPlaybackSupportFragment.BG_NONE);
        return root;
    }

    @Override
    protected boolean isPlaying() {
        return mPlayerAdapter.isPlaying();
    }

    /**
     * Adds {@link SurfaceHolder.Callback} to {@link android.view.SurfaceView}.
     */

    @Override
    public void onVideoSizeChanged(int width, int height) {
        int screenWidth = getView().getWidth();
        int screenHeight = getView().getHeight();

        ViewGroup.LayoutParams p = mVideoSurface.getLayoutParams();
        if (screenWidth * height > width * screenHeight) {
            // fit in screen height
            p.height = screenHeight;
            p.width = screenHeight * width / height;
        } else {
            // fit in screen width
            p.width = screenWidth;
            p.height = screenWidth * height / width;
        }
        mVideoSurface.setLayoutParams(p);
    }

    /**
     * Returns the surface view.
     */
    public SurfaceView getSurfaceView() {
        return mVideoSurface;
    }

    @Override
    public void onDestroyView() {
        if (mTransportControlGlue != null) {
            mTransportControlGlue.clearPlayerCallback();
        }
        super.onDestroyView();
        mPlayerAdapter.release();
        mVideoSurface = null;
        mState = SURFACE_NOT_CREATED;
    }

    private void dismissEndWatchingVodDialog() {
        EndWatchingVodDialogFragment dialogFragment =
                (EndWatchingVodDialogFragment) getFragmentManager()
                        .findFragmentByTag(EndWatchingVodDialogFragment.CLASS_NAME);
        dialogFragment.dismiss();
    }

    public void onBackPressed() {
//        showEndWatchingVodDialog(EndWatchingVodDialogFragment.FinishType.Back);
        getActivity().finish();
    }

    @Override
    public void requestCancel(DialogFragment dialogFragment, VodEndActionPresenter.FinishType type) {
        dismissEndWatchingVodDialog();
    }

    @Override
    public void requestFinish(DialogFragment dialogFragment, Content content, VodEndActionPresenter.FinishType type) {
        long currentPosition = mPlayerAdapter.getCurrentPosition();
        long duration = mPlayerAdapter.getDuration();

        switch (type) {
            case Exit:
                dialogFragment.dismiss();
                getActivity().finish();
                break;
            case NextEpisodeDetail:
                dialogFragment.dismiss();
                break;
        }
    }

    @Override
    public void onError(int errorCode, CharSequence errorMessage) {
        super.onError(errorCode, errorMessage);

        String title = ErrorMessageManager.getInstance().getPlayerErrorCode(getContext(), errorCode);
        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, title,
                ErrorMessageManager.getInstance().getErrorMessage(title));
        dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                getActivity().finish();
            }
        });
    }

}