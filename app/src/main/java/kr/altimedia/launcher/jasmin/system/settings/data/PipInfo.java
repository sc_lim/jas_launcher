package kr.altimedia.launcher.jasmin.system.settings.data;

import com.altimedia.util.Log;
import com.google.gson.Gson;

public class PipInfo {
    private static final String TAG = PipInfo.class.getSimpleName();
    private String serviceId;
    private int position;
    private int size;

    public PipInfo(String serviceId, boolean visible, int position, int size) {
        this.serviceId = serviceId;
        this.position = position;
        this.size = size;
    }

    public static PipInfo getDataFromJson(String json) {
        PipInfo pipInfo = new Gson().fromJson(json, PipInfo.class);
        if (Log.INCLUDE) {
            Log.d(TAG, "getDataFromJson() json:"+json+", data:"+pipInfo);
        }
        return pipInfo;
    }

    public String getDataAsJson() {
        String json = new Gson().toJson(this);
        if (Log.INCLUDE) {
            Log.d(TAG, "getDataAsJson() json:"+json+", this:"+this);
        }
        return json;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "PipInfo["
                + "serviceId:"+serviceId
                + ", position:"+position
                + ", size:"+size
                +"]";
    }
}
