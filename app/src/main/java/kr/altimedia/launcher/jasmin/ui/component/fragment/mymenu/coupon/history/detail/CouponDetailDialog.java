/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.detail;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.PointCoupon;
import kr.altimedia.launcher.jasmin.dm.def.CouponType;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public class CouponDetailDialog extends SafeDismissDialogFragment implements View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = CouponDetailDialog.class.getName();
    private final String TAG = CouponDetailDialog.class.getSimpleName();

    private static final String KEY_DETAIL_ITEM = "DETAIL_ITEM";
    private View rootView;

    private CouponDetailDialog() {
    }

    public static CouponDetailDialog newInstance(Coupon coupon) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_DETAIL_ITEM, coupon);

        CouponDetailDialog fragment = new CouponDetailDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_coupon_history_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        initView(view);
    }

    private void initView(View view) {
        Coupon coupon = getArguments().getParcelable(KEY_DETAIL_ITEM);
        try {
            CouponType type = coupon.getType();
            switch (type) {
                case DISCOUNT:
                    showDiscountDetail(coupon);
                    break;
                case POINT:
                    if (((PointCoupon) coupon).isPurchased()) {
                        showCashBonusDetail(coupon);
                    } else {
                        showCashDetail(coupon);
                    }
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showDiscountDetail(Coupon item) {
        CouponDiscountDetailFragment fragment = CouponDiscountDetailFragment.newInstance(item);
        attachFragment(fragment, CouponDiscountDetailFragment.CLASS_NAME);
    }

    private void showCashDetail(Coupon item) {
        CouponCashDetailFragment fragment = CouponCashDetailFragment.newInstance(item);
        attachFragment(fragment, CouponCashDetailFragment.CLASS_NAME);
    }

    private void showCashBonusDetail(Coupon item) {
        CouponCashBonusDetailFragment fragment = CouponCashBonusDetailFragment.newInstance(item);
        attachFragment(fragment, CouponCashBonusDetailFragment.CLASS_NAME);
    }

    private void attachFragment(Fragment fragment, String tag) {
        getChildFragmentManager().beginTransaction().replace(R.id.frame_layout, fragment, tag).commit();
    }
}
