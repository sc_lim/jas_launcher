package kr.altimedia.launcher.jasmin.dm.contents.obj;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import kr.altimedia.launcher.jasmin.dm.def.CouponType;

/**
 * Created by mc.kim on 04,08,2020
 */
public class OpportunityTypeDeserializer implements JsonDeserializer<OpportunityType> {

    @Override
    public OpportunityType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return OpportunityType.findByName(json.getAsString());
    }
}
