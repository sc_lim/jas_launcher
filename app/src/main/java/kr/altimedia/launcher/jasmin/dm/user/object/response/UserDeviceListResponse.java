/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;

public class UserDeviceListResponse extends BaseResponse {
    public static final Creator<UserDeviceListResponse> CREATOR = new Creator<UserDeviceListResponse>() {
        @Override
        public UserDeviceListResponse createFromParcel(Parcel in) {
            return new UserDeviceListResponse(in);
        }

        @Override
        public UserDeviceListResponse[] newArray(int size) {
            return new UserDeviceListResponse[size];
        }
    };
    @Expose
    @SerializedName("data")
    private List<UserDevice> userDeviceList;

    protected UserDeviceListResponse(Parcel in) {
        super(in);
        userDeviceList = in.createTypedArrayList(UserDevice.CREATOR);
    }

    @NonNull
    @Override
    public String toString() {
        return "UserDeviceListResponse{" +
                "userDeviceList='" + userDeviceList + '\'' +
                "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(userDeviceList);
    }

    public List<UserDevice> getUserDeviceList() {
        return userDeviceList;
    }
}
