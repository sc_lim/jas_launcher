/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.viewModel;

import android.app.Activity;
import android.app.Application;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.util.Log;

import androidx.databinding.Observable;
import androidx.lifecycle.ViewModel;
import kr.altimedia.launcher.jasmin.tv.interactor.TvViewInteractor;
import kr.altimedia.launcher.jasmin.tv.observable.ChannelLiveData;
import kr.altimedia.launcher.jasmin.tv.observable.ChannelTuneResult;
import kr.altimedia.launcher.jasmin.tv.observable.ProgramLiveData;

public class TVGuideViewModel extends ViewModel {
    private Observable.OnPropertyChangedCallback mOnPropertyChangedCallback = null;
    private Activity mActivity = null;
    private Application mApplication = null;

    public TVGuideViewModel(Activity mActivity, Application mApplication) {
        this(null, mActivity, mApplication);
    }

    public TVGuideViewModel(Observable.OnPropertyChangedCallback mOnPropertyChangedCallback, Activity mActivity, Application mApplication) {
        this.mOnPropertyChangedCallback = mOnPropertyChangedCallback;
        this.mActivity = mActivity;
        this.mApplication = mApplication;
    }

    public final ChannelLiveData currentChannel = new ChannelLiveData();
    public final ProgramLiveData currentProgram = new ProgramLiveData();
    public final ChannelTuneResult currentTuneResult = new ChannelTuneResult();

    public void setValue(Channel channel) {
        currentChannel.setValue(channel);
        currentChannel.notifyPropertyChanged(0);
        if (Log.INCLUDE) {
            Log.d("TVGuideViewModel", "setChannel() ch:" + channel);
        }
    }

    public void setValue(Program program) {
        currentProgram.setValue(program);
        currentProgram.notifyPropertyChanged(0);
        if (Log.INCLUDE) {
            Log.d("TVGuideViewModel", "setProgram() prog:" + program);
        }
    }

    public void setValue(TvViewInteractor.TuneResult tuneResult) {
        currentTuneResult.setValue(tuneResult);
        currentTuneResult.notifyPropertyChanged(0);
        if (Log.INCLUDE) {
            Log.d("TVGuideViewModel", "setTuneResult() tuneResult:" + tuneResult);
        }
    }
}
