/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util.background;


import android.util.Log;

import androidx.fragment.app.Fragment;

/**
 * Created by mc.kim on 16,04,2020
 */
public class BackgroundFragment extends Fragment {
    private static final String TAG = BackgroundFragment.class.getSimpleName();
    private BackgroundManager mBackgroundManager;

    public BackgroundFragment(){}

    public static BackgroundFragment newInstance() {
        Log.d(TAG, "newInstance");
        BackgroundFragment fragment = new BackgroundFragment();
        return fragment;
    }

    void setBackgroundManager(BackgroundManager backgroundManager) {
        Log.d(TAG, "setBackgroundManager");
        mBackgroundManager = backgroundManager;
    }


    BackgroundManager getBackgroundManager() {
        return mBackgroundManager;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        if (mBackgroundManager != null) {
            mBackgroundManager.onActivityStart();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (mBackgroundManager != null) {
            mBackgroundManager.onResume();
        }
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        if (mBackgroundManager != null) {
            mBackgroundManager.onStop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (mBackgroundManager != null) {
            mBackgroundManager.detach();
        }
    }
}
