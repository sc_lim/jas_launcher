/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mc.kim on 08,06,2020
 */
public class SubscriberInfo implements Parcelable {
    @Expose
    @SerializedName("lastProfileId")
    private String lastProfileId;
    @Expose
    @SerializedName("loginId")
    private String loginId;
    @Expose
    @SerializedName("paidServiceId")
    private List<String> paidServiceId;
    @Expose
    @SerializedName("productId")
    private String productId;
    @Expose
    @SerializedName("subscriber")
    private SubscriberState subscriberState;

    protected SubscriberInfo(Parcel in) {
        lastProfileId = in.readString();
        loginId = in.readString();
        paidServiceId = in.createStringArrayList();
        productId = in.readString();
        subscriberState = in.readParcelable(SubscriberState.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(lastProfileId);
        dest.writeString(loginId);
        dest.writeStringList(paidServiceId);
        dest.writeString(productId);
        dest.writeParcelable(subscriberState, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<SubscriberInfo> CREATOR = new Parcelable.Creator<SubscriberInfo>() {
        @Override
        public SubscriberInfo createFromParcel(Parcel in) {
            return new SubscriberInfo(in);
        }

        @Override
        public SubscriberInfo[] newArray(int size) {
            return new SubscriberInfo[size];
        }
    };

    @Override
    public String toString() {
        return "SubscriberInfo{" +
                "lastProfileId='" + lastProfileId + '\'' +
                ", loginId='" + loginId + '\'' +
                ", paidServiceId=" + paidServiceId +
                ", productId='" + productId + '\'' +
                ", subscriberState='" + subscriberState + '\'' +
                '}';
    }

    public String getLastProfileId() {
        return lastProfileId;
    }

    public String getLoginId() {
        return loginId;
    }

    public List<String> getPaidServiceId() {
        return paidServiceId;
    }

    public String getProductId() {
        return productId;
    }

    public SubscriberState getStatus() {
        return subscriberState;
    }

    public static class SubscriberState implements Parcelable {
        public static final Creator<SubscriberState> CREATOR = new Creator<SubscriberState>() {
            @Override
            public SubscriberState createFromParcel(Parcel in) {
                return new SubscriberState(in);
            }

            @Override
            public SubscriberState[] newArray(int size) {
                return new SubscriberState[size];
            }
        };
        @Expose
        @SerializedName("status")
        private String status;

        protected SubscriberState(Parcel in) {
            status = in.readString();
        }

        public String getStatus() {
            return status;
        }

        @Override
        public String toString() {
            return "SubscriberState{" +
                    "status='" + status + '\'' +
                    '}';
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(status);
        }

        @Override
        public int describeContents() {
            return 0;
        }
    }
}