/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device;

import kr.altimedia.launcher.jasmin.R;

public enum DeviceEditType {
    CHANGE(0, R.string.change_device_name), DELETE(1, R.string.delete_device);

    private int type;
    private int title;

    DeviceEditType(int type, int title) {
        this.type = type;
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public int getTitle() {
        return title;
    }
}
