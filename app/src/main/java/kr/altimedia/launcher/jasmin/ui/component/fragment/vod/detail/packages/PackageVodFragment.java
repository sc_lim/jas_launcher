/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.packages;


import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.view.View;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PackageContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.dm.payment.type.ContentType;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.DetailFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodDetailsDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.managerProvider;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.packages.presenter.PackageDetailRowPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.packages.presenter.PackagePresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.packages.presenter.PackageVodHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.packages.presenter.PackageVodListRowPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.ProductButtonPresenterSelector;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonManager2;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.VodProductType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.VodWatchPackageDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.PaymentItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPurchaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPurchaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentButton;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentWrapper;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment.SubscriptionPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.util.task.UserTaskManager;
import kr.altimedia.launcher.jasmin.ui.util.task.VodTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.row.VodOverviewRow;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;


public class PackageVodFragment extends DetailFragment {
    public static final String CLASS_NAME = PackageVodFragment.class.getName();
    private static final String TAG = PackageVodFragment.class.getSimpleName();

    private static final String KEY_PRODUCT_ID = "PRODUCT_ID";
    private static final String KEY_CATEGORY_ID = "CATEGORY_ID";

    private final int PACKAGE_ROW = 0;
    private final int DETAIL_ROW = 1;

    private PackageContent packageContent;

    private View focusedView;

    private ArrayObjectAdapter mAdapter;
    private ClassPresenterSelector mPresenterSelector;
    private ProductButtonManager2 productButtonManager;

    private ArrayList<Content> packageList;

    private final int TASK_ID = 0;
    private final long KEY_DELAY_TIME = 300;
    private final Handler keyHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (Log.INCLUDE) {
                Log.d(TAG, "call handleMessage");
            }

            PackageVodListRowPresenter.PackageVodListRowViewHolder vh = (PackageVodListRowPresenter.PackageVodListRowViewHolder) mVodRowFragment.getRowViewHolder(PACKAGE_ROW);
            updateDetailView(vh);
        }
    };

    public PackageVodFragment() {
        super();
    }

    public static PackageVodFragment newInstance(String productId, String categoryId) {
        Bundle args = new Bundle();
        args.putString(KEY_PRODUCT_ID, productId);
        args.putString(KEY_CATEGORY_ID, categoryId);

        PackageVodFragment fragment = new PackageVodFragment();
        fragment.setResourceId(R.layout.fragment_package_vod_details);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (focusedView != null) {
            focusedView.requestFocus();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initChildFocusListener(view);

        mPresenterSelector = new ClassPresenterSelector();
        mAdapter = new ArrayObjectAdapter(mPresenterSelector);
        setAdapter(mAdapter);

        loadPackageContent(true);

        setOnItemViewClickedListener(new PackageVodFragment.ItemViewClickedListener());
    }

    private void initChildFocusListener(View view) {
        mRootView.setOnChildFocusListener(new BrowseFrameLayout.OnChildFocusListener() {
            @Override
            public boolean onRequestFocusInDescendants(int var1, Rect var2) {
                return true;
            }

            @Override
            public void onRequestChildFocus(View child, View focused) {
                focusedView = focused;
                if (focused.getId() == R.id.package_layout) {
                    keyHandler.removeMessages(TASK_ID);
                    keyHandler.sendEmptyMessageDelayed(TASK_ID, KEY_DELAY_TIME);
                }
            }
        });
    }

    private void initList() {
        setupDetailsOverviewRow();
        setupPackageListRow();
        mVodRowFragment.getVerticalGridView().scrollToPosition(DETAIL_ROW);

        mVodRowFragment.setOnItemViewSelectedListener(new BaseOnItemViewSelectedListener() {
            @Override
            public void onItemSelected(Presenter.ViewHolder var1, Object item, Presenter.ViewHolder rowViewHolder, Object row) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onItemSelected, rowViewHolder : " + rowViewHolder + ", row : " + row);
                }

                if (mVodRowFragment.getVerticalGridView().getChildCount() > DETAIL_ROW && mVodRowFragment.getSelectedPosition() == DETAIL_ROW) {
                    PackageDetailRowPresenter.VodDetailsRowPresenterViewHolder viewHolder = (PackageDetailRowPresenter.VodDetailsRowPresenterViewHolder) mVodRowFragment.getRowViewHolder(DETAIL_ROW);
                    updateDetailView(viewHolder);
                }
            }
        });
    }

    private void loadPackageContent(boolean isInit) {
        vodTaskManager.setOnCompleteLoadPackageListener(new VodTaskManager.OnCompleteLoadPackageListener<String, PackageContent>() {
            @Override
            public void needLoading(boolean isLoading) {
                if (isLoading) {
                    showProgress();
                } else {
                    hideProgress();
                }
            }

            @Override
            public void onSuccessLoadPackage(String id, PackageContent result) {
                packageContent = result;
                packageList = (ArrayList<Content>) packageContent.getContentList();

                if (isInit) {
                    initList();
                } else {
                    updatePurchaseButtons();
                }

                VodPaymentDialogFragment vodPaymentDialogFragment = (VodPaymentDialogFragment) findFragmentByTag(VodPaymentDialogFragment.CLASS_NAME);
                SubscriptionPaymentDialogFragment subscriptionPaymentDialogFragment = (SubscriptionPaymentDialogFragment) findFragmentByTag(SubscriptionPaymentDialogFragment.CLASS_NAME);
                if (vodPaymentDialogFragment != null || subscriptionPaymentDialogFragment != null) {
                    if (mVodRowFragment != null && mVodRowFragment.getVerticalGridView() != null) {
                        mVodRowFragment.getVerticalGridView().requestFocus();
                    }

                    if (vodPaymentDialogFragment != null) {
                        vodPaymentDialogFragment.showPurchaseSuccessFragment(mSenderId, vodPaymentDialogFragment.WORK_DETAIL_CALL);
                    } else if (subscriptionPaymentDialogFragment != null) {
                        subscriptionPaymentDialogFragment.showPurchaseSuccessFragment(true, mSenderId, subscriptionPaymentDialogFragment.WORK_DETAIL_CALL);
                    }
                }
            }

            @Override
            public void onFailLoadPackage(int key) {
                showLoadFailDialog(true, key);

                VodPaymentDialogFragment vodPaymentDialogFragment = (VodPaymentDialogFragment) findFragmentByTag(VodPaymentDialogFragment.CLASS_NAME);
                if (vodPaymentDialogFragment != null) {
                    vodPaymentDialogFragment.dismiss();
                }
            }

            @Override
            public void onError(String id, String errorCode, String message) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "loadPackageContent, onError");
                }

                VodPaymentDialogFragment vodPaymentDialogFragment = (VodPaymentDialogFragment) findFragmentByTag(VodPaymentDialogFragment.CLASS_NAME);
                if (vodPaymentDialogFragment != null) {
                    vodPaymentDialogFragment.dismiss();
                }

                showLoadErrorDialog(true, errorCode, message);
            }
        });

        String productId = getArguments().getString(KEY_PRODUCT_ID);
        String categoryId = getArguments().getString(KEY_CATEGORY_ID);
        vodTaskManager.loadPackageDetail(productId, categoryId);
    }

    private void updateDetailView(Presenter.ViewHolder rowViewHolder) {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateDetailView, rowViewHolder : " + rowViewHolder);
        }

        PackageDetailRowPresenter.VodDetailsRowPresenterViewHolder detailVh;
        PackageVodListRowPresenter.PackageVodListRowViewHolder packageListVh;

        if (rowViewHolder instanceof PackageDetailRowPresenter.VodDetailsRowPresenterViewHolder) {
            keyHandler.removeMessages(TASK_ID);

            detailVh = (PackageDetailRowPresenter.VodDetailsRowPresenterViewHolder) rowViewHolder;
            packageListVh = (PackageVodListRowPresenter.PackageVodListRowViewHolder) mVodRowFragment.getRowViewHolder(PACKAGE_ROW);
            detailVh.showPackageDetail();
            packageListVh.setCountVisibility(false);

//            mBackgroundManager.setDrawable(mDefaultBackground);
//            mBackgroundManager.updateImmediate();
        } else {
            packageListVh = (PackageVodListRowPresenter.PackageVodListRowViewHolder) rowViewHolder;
            detailVh = (PackageDetailRowPresenter.VodDetailsRowPresenterViewHolder) mVodRowFragment.getRowViewHolder(DETAIL_ROW);

            int index = packageListVh.getGridView().getSelectedPosition();
            packageListVh.setIndex(index);
            Content content = packageList.get(index);
            detailVh.showPackageItemDetail(content);

//            mBackgroundUri = content.getPosterSourceUrl(Content.mPosterDefaultRect.width(),
//                    Content.mPosterDefaultRect.height());
//            if (Log.INCLUDE) {
//                Log.d(TAG, "onItemSelected : " + mBackgroundUri);
//            }
//            startBackgroundTimer();
        }
    }

    @Override
    protected Rect getPadding() {
        Resources resource = getContext().getResources();
        int top = (int) resource.getDimension(R.dimen.package_detail_padding_top);
        Rect padding = new Rect(0, top, 0, 0);
        return padding;
    }

    private ArrayObjectAdapter getPackageRowAdapter(ArrayList<Content> packageList) {
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new PackagePresenter());

        if (packageList != null) {
            listRowAdapter.addAll(0, packageList);
        }

        return listRowAdapter;
    }

    private void setupPackageListRow() {
        HeaderItem header = new HeaderItem(0, packageContent.getPackageTitle());
        ArrayObjectAdapter packageObjectAdapter = getPackageRowAdapter(packageList);

        int focusedIndex = 0;
        int size = packageList != null ? packageList.size() : 0;
        PackageVodHeaderPresenter headerPresenter = new PackageVodHeaderPresenter(size, focusedIndex);
        PackageVodListRowPresenter mListRowPresenter = new PackageVodListRowPresenter(R.layout.presenter_vod_horizontal_row);
        mListRowPresenter.setHeaderPresenter(headerPresenter);

        mAdapter.add(PACKAGE_ROW, new ListRow(header, packageObjectAdapter));
        mPresenterSelector.addClassPresenter(ListRow.class, mListRowPresenter);
    }

    private void setupDetailsOverviewRow() {
        PackageDetailRowPresenter detailsPresenter = new PackageDetailRowPresenter();
        detailsPresenter.setManagerProvider(
                new managerProvider() {
                    @Override
                    public TvOverlayManager getTvOverlayManager() {
                        return mChangeVodDetailListener.getTvOverlayManager();
                    }

                    @Override
                    public FragmentManager getFragmentManager() {
                        return PackageVodFragment.this.getFragmentManager();
                    }
                });
        setPurchaseButtons(detailsPresenter);
    }

    private void setPurchaseButtons(RowPresenter detailsPresenter) {
        productButtonManager = new ProductButtonManager2(getContext());
        ArrayObjectAdapter objectAdapter = getButtonAction();

        VodOverviewRow row = new VodOverviewRow(ProductType.PACKAGE, packageContent);
        row.setActionsAdapter(objectAdapter);

        mAdapter.add(row);
        mPresenterSelector.addClassPresenter(VodOverviewRow.class, detailsPresenter);
    }

    private void watchVod() {
        if (Log.INCLUDE) {
            Log.d(TAG, "watchVod");
        }

        if (packageList.size() == 1) {
            loadAndWatchPackageStream(packageList.get(0));
        } else {
            VodWatchPackageDialogFragment vodWatchPackageDialogFragment
                    = VodWatchPackageDialogFragment.newInstance(packageContent.getPackageTitle(), packageList);
            vodWatchPackageDialogFragment.setOnSelectWatchPackageListener(new VodWatchPackageDialogFragment.OnSelectWatchPackageListener() {
                @Override
                public void onWatchPackage(Content content) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onWatchPackage, content : " + content);
                    }

                    vodWatchPackageDialogFragment.dismiss();

                    if (content.isSeries()) {
                        mChangeVodDetailListener.replaceDetail(content);
                    } else {
                        loadAndWatchPackageStream(content);
                    }
                }
            });
            mChangeVodDetailListener.getTvOverlayManager().showDialogFragment(vodWatchPackageDialogFragment);
        }
    }

    //package list안에 있는 content는 stream 정보가 부족하여, 요청 한 뒤 play
    private void loadAndWatchPackageStream(Content content) {
        vodTaskManager.setOnCompleteLoadContentDetailListener(new VodTaskManager.OnCompleteLoadContentDetailListener<String, ContentDetailInfo>() {
            @Override
            public void needLoading(boolean isLoading) {
                if (isLoading) {
                    showProgress();
                } else {
                    hideProgress();
                }
            }

            @Override
            public void onSuccessLoadContentDetail(String id, ContentDetailInfo result) {
                ContentDetailInfo contentDetailInfo = result;
                if (Log.INCLUDE) {
                    Log.d(TAG, "loadAndWatchPackageStream, contentDetailInfo : " + contentDetailInfo);
                }

                if (mChangeVodDetailListener != null) {
                    Content watchContent = result.getContent();
                    mChangeVodDetailListener.watchMainVideo(watchContent);
                }
            }

            @Override
            public void onFailLoadContentDetail(int key) {
                showLoadFailDialog(false, key);
            }

            @Override
            public void onError(String id, String errorCode, String message) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "loadAndWatchPackageStream, onError");
                }

                showLoadErrorDialog(false, errorCode, message);
            }
        });
        vodTaskManager.loadContentDetail(content.getContentGroupId(), content.getCategoryId());
    }

    private void purchaseVod(Product product) {
        Bundle bundle = new Bundle();
        bundle.putString(VodPurchaseFragment.KEY_CONTENT_TITLE, packageContent.getPackageTitle());
        bundle.putString(VodPurchaseFragment.KEY_CONTENT_POSTER_URL, ""/*packageDetailData.getPosterSourceUrl()*/);
        bundle.putParcelable(VodPurchaseFragment.KEY_PRODUCT, product);

        TvOverlayManager tvOverlayManager = mChangeVodDetailListener.getTvOverlayManager();
        VodPurchaseDialogFragment vodPurchaseDialogFragment = VodPurchaseDialogFragment.newInstance(bundle);
        vodPurchaseDialogFragment.setOnPaymentClickListener(new PaymentItemBridgeAdapter.OnPaymentClickListener() {
            @Override
            public void onPaymentClick(Object item) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onPaymentClick, item : " + item);
                }

                if (item == null) {
                    vodPurchaseDialogFragment.dismiss();
                    return;
                }

                PaymentButton paymentButton = (PaymentButton) item;
                PaymentType paymentType = paymentButton.getType();
                PaymentWrapper paymentWrapper = vodPurchaseDialogFragment.getPaymentWrapper();
                String title = packageContent.getPackageTitle();
                PurchaseInfo purchaseInfo = new PurchaseInfo(ContentType.VOD, title, product, paymentType, paymentWrapper);
                Parcelable option = (Parcelable) vodPurchaseDialogFragment.getPaymentOption(paymentType);

                VodPaymentDialogFragment vodPaymentDialogFragment = VodPaymentDialogFragment.newInstance(mSenderId, purchaseInfo, option);
                vodPaymentDialogFragment.setOnPaymentListener(new VodPaymentDialogFragment.OnPaymentCompleteListener() {
                    @Override
                    public void onComplete(boolean isSuccess) {
                        vodPurchaseDialogFragment.dismiss();
                        if (isSuccess) {
//                            updatePaymentResult();
                        }
                    }

                    @Override
                    public void onRequestWatch(boolean isRequest) {
                        vodPaymentDialogFragment.dismiss();
                        if (isRequest) {
                            watchVod();
                        }
                    }

                    @Override
                    public void onCancelPayment() {
                        vodPaymentDialogFragment.dismiss();
                    }
                });

                tvOverlayManager.showDialogFragment(vodPaymentDialogFragment);
            }
        });
        tvOverlayManager.showDialogFragment(vodPurchaseDialogFragment);
    }

    private void purchasePackage(Product product) {
        if (Log.INCLUDE) {
            Log.d(TAG, "purchaseSingle, product : " + product);
        }

        purchaseVod(product);
    }

    @Override
    protected void onPurchasePushMessage(String offerId) {
        super.onPurchasePushMessage(offerId);

        ((VodDetailsDialogFragment) getParentFragment()).loadUserDiscountInfo();

        if (packageContent == null) {
            return;
        }

        boolean isContains = isContainsOfferId(offerId, (ArrayList<Product>) packageContent.getProductList());

        if (Log.INCLUDE) {
            Log.d(TAG, "onPurchasePushMessage, offerId : " + offerId + ", isContains : " + isContains);
        }

        if (isContains) {
            updatePaymentResult();
        }
    }

    private void updatePaymentResult() {
        loadPackageContent(false);
    }

    private void updatePurchaseButtons() {
        if (Log.INCLUDE) {
            Log.d(TAG, "updatePurchaseButtons");
        }

        VodOverviewRow vodOverviewRow = new VodOverviewRow(ProductType.PACKAGE, packageContent);
        ArrayObjectAdapter objectAdapter = getButtonAction();
        vodOverviewRow.setActionsAdapter(objectAdapter);
        mAdapter.replace(DETAIL_ROW, vodOverviewRow);
    }

    @Override
    protected void updateDetail(Content content) {
        super.updateDetail(content);

        if (Log.INCLUDE) {
            Log.d(TAG, "updateDetail");
        }

        loadPackageContent(false);
    }

    @Override
    protected void setContentLike(Content mContent) {
        super.setContentLike(mContent);

        boolean isLike = mContent.isLike();
        if (Log.INCLUDE) {
            Log.d(TAG, "setContentLike, isLike : " + isLike);
        }

        mChangeVodDetailListener.onVodLikeChange(isLike);
    }

    private void onClickContentLike(Presenter.ViewHolder viewHolder) {
        ProductButtonPresenterSelector.MyListButtonPresenter.ViewHolder vh = (ProductButtonPresenterSelector.MyListButtonPresenter.ViewHolder) viewHolder;
        vodTaskManager.setOnCompleteContentFavoriteListener(
                new VodTaskManager.OnCompleteContentFavoriteListener() {
                    @Override
                    public void needLoading(boolean isLoading) {
                        if (isLoading) {
                            showProgress();
                        } else {
                            hideProgress();
                        }
                    }

                    @Override
                    public void onSuccessAddFavorite(Object id, Object result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onCompleteAddFavorite");
                        }

                        packageContent.setLike(true);
                        vh.toggleMyList(packageContent);
                        int textId = R.string.my_list_add;
                        JasminToast.makeToast(getContext(), textId);

                        mChangeVodDetailListener.onVodLikeChange(true);
                    }

                    @Override
                    public void onSuccessRemoveFavorite(Object id, Object result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onCompleteRemoveFavorite");
                        }

                        packageContent.setLike(false);
                        vh.toggleMyList(packageContent);
                        int textId = R.string.my_list_remove;
                        JasminToast.makeToast(getContext(), textId);

                        mChangeVodDetailListener.onVodLikeChange(false);
                    }

                    @Override
                    public void onFailFavorite(int key) {
                        showLoadFailDialog(false, key);
                    }

                    @Override
                    public void onError(Object id, String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onClickContentLike, onError");
                        }

                        showLoadErrorDialog(false, errorCode, message);
                    }
                });

        boolean isLike = packageContent.isLike();
        vodTaskManager.onClickLikeContent(isLike, packageContent.getPackageId(), VodTaskManager.VodLikeType.Package);
    }

    private void showProgress() {
        VodDetailsDialogFragment vodDetailsDialogFragment = (VodDetailsDialogFragment) getParentFragment();
        vodDetailsDialogFragment.showProgress();
    }

    private void hideProgress() {
        if (vodTaskManager.isEmpty() && UserTaskManager.getInstance().isEmpty()) {
            VodDetailsDialogFragment vodDetailsDialogFragment = (VodDetailsDialogFragment) getParentFragment();
            vodDetailsDialogFragment.hideProgress();
        }
    }

    private ArrayObjectAdapter getButtonAction() {
        return productButtonManager.getPackageProductButtons(packageContent, (ArrayList<Product>) packageContent.getProductList());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        keyHandler.removeMessages(TASK_ID);
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder var3, Row row) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onItemClicked, item: " + item + ", row : " + row);
            }

            if (item == null) {
                return;
            }

            if (row instanceof VodOverviewRow) {
                if (item instanceof ProductButtonItem) {
                    ProductButtonItem productButtonItem = (ProductButtonItem) item;
                    VodProductType type = productButtonItem.getProductType();
                    Product product = productButtonItem.getProduct();

                    if (Log.INCLUDE) {
                        Log.d(TAG, "onItemClicked, productButtonItem : " + productButtonItem + ", product : " + product);
                    }

                    if (type == VodProductType.WATCH) {
                        watchVod();
                    } else if (type == VodProductType.RENT_PACKAGE || type == VodProductType.BUY_PACKAGE) {
                        purchasePackage(product);
                    } else if (type == VodProductType.SUBSCRIBE) {
                        purchaseSubscription(productButtonManager, packageContent.getTermsAndConditions());
                    }
                } else if (item instanceof PackageContent) {
                    onClickContentLike(itemViewHolder);
                }
            } else if (item instanceof Content) {
                mChangeVodDetailListener.replaceDetail((Content) item);
            }
        }
    }
}


