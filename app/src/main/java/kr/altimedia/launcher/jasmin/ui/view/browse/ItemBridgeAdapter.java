/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;


import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.leanback.widget.FacetProvider;
import androidx.leanback.widget.FacetProviderAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.FocusHighlightHandler;

public class ItemBridgeAdapter extends RecyclerView.Adapter implements FacetProviderAdapter {
    static final String TAG = "ItemBridgeAdapter";
    static final boolean DEBUG = false;
    private ObjectAdapter mAdapter;
    ItemBridgeAdapter.Wrapper mWrapper;
    private PresenterSelector mPresenterSelector;
    FocusHighlightHandler mFocusHighlight;
    private ItemBridgeAdapter.AdapterListener mAdapterListener;
    private ArrayList<Presenter> mPresenters;
    private ObjectAdapter.DataObserver mDataObserver;

    public ItemBridgeAdapter(ObjectAdapter adapter, PresenterSelector presenterSelector) {
        this.mPresenters = new ArrayList();
        this.mDataObserver = new ObjectAdapter.DataObserver() {
            public void onChanged() {
                ItemBridgeAdapter.this.notifyDataSetChanged();
            }

            public void onItemRangeChanged(int positionStart, int itemCount) {
                ItemBridgeAdapter.this.notifyItemRangeChanged(positionStart, itemCount);
            }

            public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
                ItemBridgeAdapter.this.notifyItemRangeChanged(positionStart, itemCount, payload);
            }

            public void onItemRangeInserted(int positionStart, int itemCount) {
                ItemBridgeAdapter.this.notifyItemRangeInserted(positionStart, itemCount);
            }

            public void onItemRangeRemoved(int positionStart, int itemCount) {
                ItemBridgeAdapter.this.notifyItemRangeRemoved(positionStart, itemCount);
            }

            public void onItemMoved(int fromPosition, int toPosition) {
                ItemBridgeAdapter.this.notifyItemMoved(fromPosition, toPosition);
            }
        };
        this.setAdapter(adapter);
        this.mPresenterSelector = presenterSelector;
    }

    public ItemBridgeAdapter(ObjectAdapter adapter) {
        this(adapter, null);
    }


    public ItemBridgeAdapter() {
        this.mPresenters = new ArrayList();
        this.mDataObserver = new ObjectAdapter.DataObserver() {
            public void onChanged() {
                ItemBridgeAdapter.this.notifyDataSetChanged();
            }

            public void onItemRangeChanged(int positionStart, int itemCount) {
                ItemBridgeAdapter.this.notifyItemRangeChanged(positionStart, itemCount);
            }

            public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
                ItemBridgeAdapter.this.notifyItemRangeChanged(positionStart, itemCount, payload);
            }

            public void onItemRangeInserted(int positionStart, int itemCount) {
                ItemBridgeAdapter.this.notifyItemRangeInserted(positionStart, itemCount);
            }

            public void onItemRangeRemoved(int positionStart, int itemCount) {
                ItemBridgeAdapter.this.notifyItemRangeRemoved(positionStart, itemCount);
            }

            public void onItemMoved(int fromPosition, int toPosition) {
                ItemBridgeAdapter.this.notifyItemMoved(fromPosition, toPosition);
            }
        };
    }

    public ObjectAdapter getAdapter(){
        return mAdapter;
    }

    public void setAdapter(ObjectAdapter adapter) {
        if (adapter != this.mAdapter) {
            if (this.mAdapter != null) {
                this.mAdapter.unregisterObserver(this.mDataObserver);
            }

            this.mAdapter = adapter;
            if (this.mAdapter == null) {
                this.notifyDataSetChanged();
            } else {
                this.mAdapter.registerObserver(this.mDataObserver);
                if (this.hasStableIds() != this.mAdapter.hasStableIds()) {
                    this.setHasStableIds(this.mAdapter.hasStableIds());
                }

                this.notifyDataSetChanged();
            }
        }
    }

    public void setPresenter(PresenterSelector presenterSelector) {

        this.mPresenterSelector = presenterSelector;
        this.notifyDataSetChanged();
    }

    public void setWrapper(ItemBridgeAdapter.Wrapper wrapper) {
        this.mWrapper = wrapper;
    }

    public ItemBridgeAdapter.Wrapper getWrapper() {
        return this.mWrapper;
    }

    public void setFocusHighlight(FocusHighlightHandler listener) {
        this.mFocusHighlight = listener;
    }

    public void clear() {
        this.setAdapter(null);
    }

    public void setPresenterMapper(ArrayList<Presenter> presenters) {
        this.mPresenters = presenters;
    }

    public ArrayList<Presenter> getPresenterMapper() {
        return this.mPresenters;
    }

    @Override
    public int getItemCount() {
        return this.mAdapter != null ? this.mAdapter.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        PresenterSelector presenterSelector = this.mPresenterSelector != null ? this.mPresenterSelector : this.mAdapter.getPresenterSelector();
        Object item = this.mAdapter.get(position);
        Presenter presenter = presenterSelector.getPresenter(item);
        int type = this.mPresenters.indexOf(presenter);
        if (type < 0) {
            this.mPresenters.add(presenter);
            type = this.mPresenters.indexOf(presenter);
            this.onAddPresenter(presenter, type);
            if (this.mAdapterListener != null) {
                this.mAdapterListener.onAddPresenter(presenter, type);
            }
        }

        return type;
    }

    protected void onAddPresenter(Presenter presenter, int type) {
    }

    protected void onCreate(ItemBridgeAdapter.ViewHolder viewHolder) {
    }

    protected void onBind(ItemBridgeAdapter.ViewHolder viewHolder) {
    }

    protected void onUnbind(ItemBridgeAdapter.ViewHolder viewHolder) {
    }

    protected void onAttachedToWindow(ItemBridgeAdapter.ViewHolder viewHolder) {
    }

    protected void onDetachedFromWindow(ItemBridgeAdapter.ViewHolder viewHolder) {
    }

    public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Presenter presenter = this.mPresenters.get(viewType);
        Presenter.ViewHolder presenterVh;
        View view;
        if (this.mWrapper != null) {
            view = this.mWrapper.createWrapper(parent);
            presenterVh = presenter.onCreateViewHolder(parent);
            this.mWrapper.wrap(view, presenterVh.view);
        } else {
            presenterVh = presenter.onCreateViewHolder(parent);
            view = presenterVh.view;
        }

        ItemBridgeAdapter.ViewHolder viewHolder = new ItemBridgeAdapter.ViewHolder(presenter, view, presenterVh);
        this.onCreate(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onCreate(viewHolder);
        }

        View presenterView = viewHolder.mHolder.view;
        if (presenterView != null) {
            viewHolder.mFocusChangeListener.mChainedListener = presenterView.getOnFocusChangeListener();
            presenterView.setOnFocusChangeListener(viewHolder.mFocusChangeListener);
        }

        if (this.mFocusHighlight != null) {
            this.mFocusHighlight.onInitializeView(view);
        }

        return viewHolder;
    }

    public void setAdapterListener(ItemBridgeAdapter.AdapterListener listener) {
        this.mAdapterListener = listener;
    }

    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) holder;
        viewHolder.itemView.setTag(R.id.KEY_INDEX,position);
        viewHolder.mItem = this.mAdapter.get(position);
        viewHolder.mPresenter.onBindViewHolder(viewHolder.mHolder, viewHolder.mItem);
        this.onBind(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onBind(viewHolder);
        }

    }


    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position, List payloads) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBindViewHolder : " + holder + ", position : " + position);
        }
        ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) holder;
        viewHolder.itemView.setTag(R.id.KEY_INDEX, position);
        ((ViewHolder) holder).mHolder.view.setSelected(false);
        viewHolder.mItem = this.mAdapter.get(position);
        viewHolder.mPresenter.onBindViewHolder(viewHolder.mHolder, viewHolder.mItem, payloads);
        this.onBind(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onBind(viewHolder, payloads);
        }

    }

    public final void onViewRecycled(RecyclerView.ViewHolder holder) {
        ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) holder;
        viewHolder.mPresenter.onUnbindViewHolder(viewHolder.mHolder);
        viewHolder.mHolder.view.setOnKeyListener(null);
        this.onUnbind(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onUnbind(viewHolder);
        }

        viewHolder.mItem = null;
    }

    public final boolean onFailedToRecycleView(RecyclerView.ViewHolder holder) {
        this.onViewRecycled(holder);
        return false;
    }

    public final void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) holder;
        this.onAttachedToWindow(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onAttachedToWindow(viewHolder);
        }

        viewHolder.mPresenter.onViewAttachedToWindow(viewHolder.mHolder);
    }

    public final void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) holder;
        viewHolder.mPresenter.onViewDetachedFromWindow(viewHolder.mHolder);
        this.onDetachedFromWindow(viewHolder);
        if (this.mAdapterListener != null) {
            this.mAdapterListener.onDetachedFromWindow(viewHolder);
        }

    }

    public long getItemId(int position) {
        return this.mAdapter.getId(position);
    }

    public FacetProvider getFacetProvider(int type) {
        return this.mPresenters.get(type);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements FacetProvider {
        public final Presenter mPresenter;
        public final Presenter.ViewHolder mHolder;
        public final ItemBridgeAdapter.OnFocusChangeListener mFocusChangeListener = ItemBridgeAdapter.this.new OnFocusChangeListener();
        public Object mItem;
        public Object mExtraObject;

        public final Presenter getPresenter() {
            return this.mPresenter;
        }

        public final Presenter.ViewHolder getViewHolder() {
            return this.mHolder;
        }

        public final Object getItem() {
            return this.mItem;
        }

        public final Object getExtraObject() {
            return this.mExtraObject;
        }

        public void setExtraObject(Object object) {
            this.mExtraObject = object;
        }

        public Object getFacet(Class<?> facetClass) {
            return this.mHolder.getFacet(facetClass);
        }

        ViewHolder(Presenter presenter, View view, Presenter.ViewHolder holder) {
            super(view);
            this.mPresenter = presenter;
            this.mHolder = holder;
        }

        public OnFocusChangeListener getFocusChangeListener() {
            return mFocusChangeListener;
        }
    }

    public class OnFocusChangeListener implements View.OnFocusChangeListener {
        View.OnFocusChangeListener mChainedListener;

        OnFocusChangeListener() {
        }

        public void setChainedListener(View.OnFocusChangeListener mChainedListener) {
            this.mChainedListener = mChainedListener;
        }

        public void onFocusChange(View view, boolean hasFocus) {
            if (ItemBridgeAdapter.this.mWrapper != null) {
                view = (View) view.getParent();
            }

            if (ItemBridgeAdapter.this.mFocusHighlight != null) {
                ItemBridgeAdapter.this.mFocusHighlight.onItemFocused(view, hasFocus);
            }

            if (this.mChainedListener != null) {
                this.mChainedListener.onFocusChange(view, hasFocus);
            }

        }
    }

    public abstract static class Wrapper {
        public Wrapper() {
        }

        public abstract View createWrapper(View var1);

        public abstract void wrap(View var1, View var2);
    }

    public static class AdapterListener {
        public AdapterListener() {
        }

        public void onAddPresenter(Presenter presenter, int type) {
        }

        public void onCreate(ItemBridgeAdapter.ViewHolder viewHolder) {
        }

        public void onBind(ItemBridgeAdapter.ViewHolder viewHolder) {
        }

        public void onBind(ItemBridgeAdapter.ViewHolder viewHolder, List payloads) {
            this.onBind(viewHolder);
        }

        public void onUnbind(ItemBridgeAdapter.ViewHolder viewHolder) {
        }

        public void onAttachedToWindow(ItemBridgeAdapter.ViewHolder viewHolder) {
        }

        public void onDetachedFromWindow(ItemBridgeAdapter.ViewHolder viewHolder) {
        }
    }

}
