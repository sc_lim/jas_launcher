/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponPurchaseRequest implements Parcelable {
    @Expose
    @SerializedName("purchaseId")
    private String purchaseId;
    @Expose
    @SerializedName("qrCode")
    private String qrCode;

    protected CouponPurchaseRequest(Parcel in) {
        purchaseId = in.readString();
        qrCode = in.readString();
    }

    public static final Creator<CouponPurchaseRequest> CREATOR = new Creator<CouponPurchaseRequest>() {
        @Override
        public CouponPurchaseRequest createFromParcel(Parcel in) {
            return new CouponPurchaseRequest(in);
        }

        @Override
        public CouponPurchaseRequest[] newArray(int size) {
            return new CouponPurchaseRequest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(purchaseId);
        dest.writeString(qrCode);
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public String getQrCode() {
        return qrCode;
    }
}
