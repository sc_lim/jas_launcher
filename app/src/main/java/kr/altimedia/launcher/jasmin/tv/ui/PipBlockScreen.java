/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.JasTvView;

public class PipBlockScreen extends BlockScreen {
    private static final String TAG = PipBlockScreen.class.getSimpleName();
    private static final boolean INCLUDE = true;

    private ImageView pip_iframe;
    private TextView pip_iframe_text;

    public PipBlockScreen(Context context) {
        this(context, null);
    }

    public PipBlockScreen(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PipBlockScreen(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public PipBlockScreen(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();

        pip_iframe = findViewById(R.id.pip_iframe);
        pip_iframe_text = findViewById(R.id.pip_iframe_text);
    }

    public boolean isVisible() {
        return getVisibility() == View.VISIBLE;
    }

    public void setVisible(boolean visible) {
        if (visible) {
            setVisibility(View.VISIBLE);
        } else {
            setVisibility(View.GONE);
        }
    }

    public void updateBlockScreen(JasTvView.TuningResult tuningResult) {
        int blockImgId = -1;
        String text;
        switch (tuningResult) {
            case TUNING_BLOCKED_CHANNEL:
                blockImgId = R.drawable.pip_blocked;
                text = getContext().getString(R.string.iframe_block_channel_title);
                break;
            case TUNING_BLOCKED_ADULT_CHANNEL:
                blockImgId = R.drawable.pip_blocked;
                text = getContext().getString(R.string.iframe_block_adult_channel_title);
                break;
//            case TUNING_BLOCKED_NOTSUPPORTED:
//                blockImgId = R.drawable.pip_nonpip;
//                textId = R.string.iframe_non_pip_channel_title;
//                break;
            case TUNING_BLOCKED_PROGRAM:
                blockImgId = R.drawable.pip_parental;
                text = getContext().getString(R.string.iframe_block_program_title);
                break;
            case TUNING_BLOCKED_UNSUBSCRIBED:
                blockImgId = R.drawable.pip_unsubscribed;
                text = getContext().getString(R.string.iframe_unsubscribed_channel_title);
                break;
            case TUNING_BLOCKED_UHD_IN_PIP:
                blockImgId = R.drawable.pip_uhd;
                text = getContext().getString(R.string.iframe_block_uhd_title);
                break;
            case TUNING_SUCCESS_AUDIO:
                blockImgId = R.drawable.pip_audio;
                text = getContext().getString(R.string.empty);
                break;
            case TUNING_FAIL:
                blockImgId = R.drawable.pip_error;
                text = getContext().getString(R.string.iframe_error_title);
                break;
            default:
                blockImgId = -1;
                text = "";
                break;
        }
        if (blockImgId != -1) {
            pip_iframe.setImageResource(blockImgId);
            pip_iframe_text.setText(text);
            setVisible(true);
        } else {
            setVisible(false);
        }
    }

    public void setBlockTextSize(int size) {
        if (pip_iframe_text != null) {
            pip_iframe_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        }
    }
}
