/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.category.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;

/**
 * Created by mc.kim on 08,05,2020
 */
public class CategoryResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private List<Category> categoryList;


    protected CategoryResponse(Parcel in) {
        super(in);
        categoryList = in.createTypedArrayList(Category.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(categoryList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CategoryResponse> CREATOR = new Creator<CategoryResponse>() {
        @Override
        public CategoryResponse createFromParcel(Parcel in) {
            return new CategoryResponse(in);
        }

        @Override
        public CategoryResponse[] newArray(int size) {
            return new CategoryResponse[size];
        }
    };

    @Override
    public String toString() {
        return "CategoryResponse{" +
                "categoryResult=" + categoryList +
                '}';
    }


    public List<Category> getCategoryList() {
        return categoryList;
    }


    public CategoryResult buildCategoryInfo(String version, List<Category> categories) {
        CategoryResult result = new CategoryResult(version, categoryList);
        return result;
    }


    public static class CategoryResult implements Parcelable {
        @Expose
        @SerializedName("categoryVersion")
        private String categoryVersion;
        @Expose
        @SerializedName("list")
        private List<Category> categoryList;

        public CategoryResult(String categoryVersion, List<Category> categoryList) {
            this.categoryVersion = categoryVersion;
            this.categoryList = categoryList;
        }

        protected CategoryResult(Parcel in) {
            categoryVersion = in.readString();
            categoryList = in.createTypedArrayList(Category.CREATOR);
        }

        public List<Category> getCategoryList() {
            return categoryList;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(categoryVersion);
            dest.writeTypedList(categoryList);
        }

        public String getCategoryVersion() {
            return categoryVersion;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<CategoryResult> CREATOR = new Creator<CategoryResult>() {
            @Override
            public CategoryResult createFromParcel(Parcel in) {
                return new CategoryResult(in);
            }

            @Override
            public CategoryResult[] newArray(int size) {
                return new CategoryResult[size];
            }
        };

        @Override
        public String toString() {
            return "CategoryResult{" +
                    "categoryVersion=" + categoryVersion +
                    ", categoryList=" + categoryList +
                    '}';
        }
    }
}
