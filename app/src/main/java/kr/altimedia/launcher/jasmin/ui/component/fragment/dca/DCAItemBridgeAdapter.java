/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.dca;

import android.view.View;

import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.ui.component.fragment.dca.presenter.DCAPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import com.altimedia.tvmodule.dao.Channel;

public class DCAItemBridgeAdapter extends ItemBridgeAdapter {
    private OnDCAClickListener onDCAClickListener;

    public DCAItemBridgeAdapter(ObjectAdapter adapter) {
        super(adapter);
    }

    public void setOnDCAClickListener(OnDCAClickListener onDCAClickListener) {
        this.onDCAClickListener = onDCAClickListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        DCAPresenter.ViewHolder vh = (DCAPresenter.ViewHolder) viewHolder.getViewHolder();
        Channel channel = (Channel) viewHolder.getItem();
        vh.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onDCAClickListener != null) {
                    onDCAClickListener.onClickDCAItem(channel);
                }
            }
        });
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);

        DCAPresenter.ViewHolder vh = (DCAPresenter.ViewHolder) viewHolder.getViewHolder();
        vh.view.setOnClickListener(null);
    }

    public interface OnDCAClickListener {
        void onClickDCAItem(Channel channel);
    }
}
