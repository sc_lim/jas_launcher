/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.observable;

import androidx.databinding.BaseObservable;

import kr.altimedia.launcher.jasmin.tv.interactor.TvViewInteractor;

public class ChannelTuneResult extends BaseObservable {

    private TvViewInteractor.TuneResult currentTuneResult;

    public void setValue(TvViewInteractor.TuneResult tuneResult) {
        currentTuneResult = tuneResult;
    }

    public TvViewInteractor.TuneResult getTuneResult() {
        return currentTuneResult;
    }
}
