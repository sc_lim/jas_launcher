/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.row;

import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.ui.view.row.Row;

public class DurationRow extends Row {
    private final ObjectAdapter mAdapter;

    public DurationRow(ObjectAdapter mAdapter) {
        this.mAdapter = mAdapter;
    }

    public ObjectAdapter getAdapter() {
        return mAdapter;
    }
}
