/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.adapter;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import java.io.Serializable;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.ButtonItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.presenter.ButtonItemPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;

public class ButtonBridgeAdapter extends ItemBridgeAdapter implements Serializable {
    private static final String TAG = ButtonBridgeAdapter.class.getSimpleName();

    public ButtonBridgeAdapter() {
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        ButtonItemPresenter presenter = (ButtonItemPresenter) viewHolder.mPresenter;
        ButtonItemPresenter.OnKeyListener onKeyListener = presenter.getOnKeyListener();

        ButtonItemPresenter.ItemViewHolder itemViewHolder = (ButtonItemPresenter.ItemViewHolder) viewHolder.getViewHolder();

        int index = (int) viewHolder.itemView.getTag(R.id.KEY_INDEX);
        if (onKeyListener != null) {
            viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    ButtonItem item = (ButtonItem) viewHolder.mItem;
                    int type = item.getType();
                    String name = item.getName();
                    Log.d(TAG, "onKey() name=" + name + ", index=" + index);

                    return onKeyListener.onKey(keyCode, index, item, itemViewHolder);
                }
            });
        }
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        viewHolder.mHolder.view.setOnKeyListener(null);
    }
}