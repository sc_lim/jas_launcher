/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.collection.presenter;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.widget.ArrayObjectAdapter;

import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.collection.view.PromotionPageFragment;
import kr.altimedia.launcher.jasmin.ui.view.presenter.ViewPagerPresenter;
import kr.altimedia.launcher.jasmin.ui.view.rowView.PageRowView;

public class PromotionPagerPresenter extends ViewPagerPresenter {
    private final String TAG = PromotionPagerPresenter.class.getSimpleName();

    public PromotionPagerPresenter(boolean autoPaging, int indicatorLayoutId, FragmentManager fragmentManager) {
        super(autoPaging, indicatorLayoutId, fragmentManager);
    }

    @Override
    protected ViewPagerAdapter createPagerAdapter(FragmentManager fragmentManager, int behavior, Context context) {
        return new PromotionPagerAdapter(fragmentManager, behavior, context);
    }

    @Override
    protected PageRootView createPageView(Context context) {
        return new PageRowView(context);
    }

    private class PromotionPagerAdapter extends ViewPagerAdapter {
        private Context context;
        private ArrayObjectAdapter mItemAdapter = null;

        public PromotionPagerAdapter(@NonNull FragmentManager fm, int behavior, Context context) {
            super(fm, behavior);
            this.context = context;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            return super.instantiateItem(container, position);
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            super.destroyItem(container, position, object);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return PromotionPageFragment.newInstance((Banner) getDataItem(position));
        }


        @Override
        public int getCount() {
            if (this.mItemAdapter == null) {
                Log.d(TAG, "getCount: 0");
                return 0;
            } else {
                Log.d(TAG, "getCount: " + this.mItemAdapter.size());
                return this.mItemAdapter.size();
            }
        }

        @Override
        public void setItemAdapter(ArrayObjectAdapter objectAdapter) {
            this.mItemAdapter = objectAdapter;
            notifyDataSetChanged();
        }

        @Override
        public Object getDataItem(int position) {
            return this.mItemAdapter.get(position);
        }
    }
}
