/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.KeyEvent;

import androidx.leanback.widget.Action;
import androidx.leanback.widget.ObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;

/**
 * Created by mc.kim on 06,02,2020
 */
public class VideoPlaybackControlsRow extends Row {
    private Object mItem;
    private Drawable mImageDrawable;
    private ObjectAdapter mPrimaryActionsAdapter;
    private ObjectAdapter mSecondaryActionsAdapter;
    private long mTotalTimeMs;
    private long mCurrentTimeMs;
    private long mBufferedProgressMs;
    private VideoPlaybackControlsRow.OnPlaybackProgressCallback mListener;

    static Bitmap createBitmap(Bitmap bitmap, int color) {
        Bitmap dst = bitmap.copy(bitmap.getConfig(), true);
        Canvas canvas = new Canvas(dst);
        Paint paint = new Paint();
        paint.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
        canvas.drawBitmap(bitmap, 0.0F, 0.0F, paint);
        return dst;
    }

    static int getIconHighlightColor(Context context) {
        TypedValue outValue = new TypedValue();
        return context.getTheme().resolveAttribute(R.attr.playbackControlsIconHighlightColor, outValue, true) ? outValue.data : context.getResources().getColor(R.color.lb_playback_icon_highlight_no_theme);
    }

    static Drawable getStyledDrawable(Context context, int index) {
        TypedValue outValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(R.attr.playbackControlsActionIcons, outValue, false)) {
            return null;
        } else {
            TypedArray array = context.getTheme().obtainStyledAttributes(outValue.data, R.styleable.lbPlaybackControlsActionIcons);
            Drawable drawable = array.getDrawable(index);
            array.recycle();
            return drawable;
        }
    }

    public VideoPlaybackControlsRow(Object item) {
        this.mItem = item;
    }

    public VideoPlaybackControlsRow() {
    }

    public final Object getItem() {
        return this.mItem;
    }

    public final void setImageDrawable(Drawable drawable) {
        this.mImageDrawable = drawable;
    }

    public final void setImageBitmap(Context context, Bitmap bm) {
        this.mImageDrawable = new BitmapDrawable(context.getResources(), bm);
    }

    public final Drawable getImageDrawable() {
        return this.mImageDrawable;
    }

    public final void setPrimaryActionsAdapter(ObjectAdapter adapter) {
        this.mPrimaryActionsAdapter = adapter;
    }

    public final void setSecondaryActionsAdapter(ObjectAdapter adapter) {
        this.mSecondaryActionsAdapter = adapter;
    }

    public final ObjectAdapter getPrimaryActionsAdapter() {
        return this.mPrimaryActionsAdapter;
    }

    public final ObjectAdapter getSecondaryActionsAdapter() {
        return this.mSecondaryActionsAdapter;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setTotalTime(int ms) {
        this.setDuration(ms);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setTotalTimeLong(long ms) {
        this.setDuration(ms);
    }

    public void setDuration(long ms) {
        if (this.mTotalTimeMs != ms) {
            this.mTotalTimeMs = ms;
            if (this.mListener != null) {
                this.mListener.onDurationChanged(this, this.mTotalTimeMs);
            }
        }
    }

    public void setProgramInfo(long playTimeMs, long startTimeMs, long endTimeMs) {
        if (this.mListener != null) {
            this.mListener.onProgramInfoChanged(this, playTimeMs, startTimeMs, endTimeMs);
        }
    }

    private int safeLongToInt(long numLong) {
        if ((int) numLong != numLong) {
            throw new ArithmeticException("Input overflows int.\n");
        }
        return (int) numLong;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public int getTotalTime() {

        return safeLongToInt(this.getTotalTimeLong());
    }

    /**
     * @deprecated
     */
    @Deprecated
    public long getTotalTimeLong() {
        return this.mTotalTimeMs;
    }

    public long getDuration() {
        return this.mTotalTimeMs;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setCurrentTime(int ms) {
        this.setCurrentTimeLong(ms);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setCurrentTimeLong(long ms) {
        this.setCurrentPosition(ms);
    }

    public void setCurrentPosition(long ms, boolean seeking) {
        if (this.mCurrentTimeMs != ms) {
            this.mCurrentTimeMs = ms;
            if (this.mListener != null) {
                this.mListener.onCurrentPositionChanged(this, this.mCurrentTimeMs, seeking);
            }
        }
    }

    public void readyStateChanged(boolean ready) {
        if (this.mListener != null) {
            this.mListener.onReadyStateChanged(this, ready);
        }
    }

    public void onBlockedStateChanged(boolean isLocked) {

        if (this.mListener != null) {
            this.mListener.onBlockedStateChanged(this, isLocked);
        }
    }

    public void setCurrentPosition(long ms) {
        setCurrentPosition(ms, false);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public int getCurrentTime() {
        return safeLongToInt(this.getCurrentTimeLong());
    }

    /**
     * @deprecated
     */
    @Deprecated
    public long getCurrentTimeLong() {
        return this.mCurrentTimeMs;
    }

    public long getCurrentPosition() {
        return this.mCurrentTimeMs;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setBufferedProgress(int ms) {
        this.setBufferedPosition(ms);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setBufferedProgressLong(long ms) {
        this.setBufferedPosition(ms);
    }

    public void setBufferedPosition(long ms) {
        if (this.mBufferedProgressMs != ms) {
            this.mBufferedProgressMs = ms;
            if (this.mListener != null) {
                this.mListener.onBufferedPositionChanged(this, this.mBufferedProgressMs);
            }
        }

    }

    /**
     * @deprecated
     */
    @Deprecated
    public int getBufferedProgress() {
        return safeLongToInt(this.getBufferedPosition());
    }

    /**
     * @deprecated
     */
    @Deprecated
    public long getBufferedProgressLong() {
        return this.mBufferedProgressMs;
    }

    public long getBufferedPosition() {
        return this.mBufferedProgressMs;
    }

    public Action getActionForKeyCode(int keyCode) {
        Action action = this.getActionForKeyCode(this.getPrimaryActionsAdapter(), keyCode);
        return action != null ? action : this.getActionForKeyCode(this.getSecondaryActionsAdapter(), keyCode);
    }

    public Action getActionForKeyCode(ObjectAdapter adapter, int keyCode) {
        if (adapter != this.mPrimaryActionsAdapter && adapter != this.mSecondaryActionsAdapter) {
            throw new IllegalArgumentException("Invalid adapter");
        } else {
            for (int i = 0; i < adapter.size(); ++i) {
                Action action = (Action) adapter.get(i);
                if (action.respondsToKeyCode(keyCode)) {
                    return action;
                }
            }

            return null;
        }
    }

    public void setOnPlaybackProgressChangedListener(VideoPlaybackControlsRow.OnPlaybackProgressCallback listener) {
        this.mListener = listener;
    }

    public static class ClosedCaptioningAction extends VideoPlaybackControlsRow.MultiAction {
        /**
         * @deprecated
         */
        @Deprecated
        public static final int OFF = 0;
        /**
         * @deprecated
         */
        @Deprecated
        public static final int ON = 1;
        public static final int INDEX_OFF = 0;
        public static final int INDEX_ON = 1;

        public ClosedCaptioningAction(Context context) {
            this(context, VideoPlaybackControlsRow.getIconHighlightColor(context));
        }

        public ClosedCaptioningAction(Context context, int highlightColor) {
            super(R.id.lb_control_closed_captioning);
            BitmapDrawable uncoloredDrawable = (BitmapDrawable) VideoPlaybackControlsRow.getStyledDrawable(context, R.styleable.lbPlaybackControlsActionIcons_closed_captioning);
            Drawable[] drawables = new Drawable[]{uncoloredDrawable, new BitmapDrawable(context.getResources(), VideoPlaybackControlsRow.createBitmap(uncoloredDrawable.getBitmap(), highlightColor))};
            this.setDrawables(drawables);
            String[] labels = new String[drawables.length];
            labels[0] = context.getString(R.string.lb_playback_controls_closed_captioning_enable);
            labels[1] = context.getString(R.string.lb_playback_controls_closed_captioning_disable);
            this.setLabels(labels);
        }
    }

    public static class HighQualityAction extends VideoPlaybackControlsRow.MultiAction {
        /**
         * @deprecated
         */
        @Deprecated
        public static final int OFF = 0;
        /**
         * @deprecated
         */
        @Deprecated
        public static final int ON = 1;
        public static final int INDEX_OFF = 0;
        public static final int INDEX_ON = 1;

        public HighQualityAction(Context context) {
            this(context, VideoPlaybackControlsRow.getIconHighlightColor(context));
        }

        public HighQualityAction(Context context, int highlightColor) {
            super(R.id.lb_control_high_quality);
            BitmapDrawable uncoloredDrawable = (BitmapDrawable) VideoPlaybackControlsRow.getStyledDrawable(context, R.styleable.lbPlaybackControlsActionIcons_high_quality);
            Drawable[] drawables = new Drawable[]{uncoloredDrawable, new BitmapDrawable(context.getResources(), VideoPlaybackControlsRow.createBitmap(uncoloredDrawable.getBitmap(), highlightColor))};
            this.setDrawables(drawables);
            String[] labels = new String[drawables.length];
            labels[0] = context.getString(R.string.lb_playback_controls_high_quality_enable);
            labels[1] = context.getString(R.string.lb_playback_controls_high_quality_disable);
            this.setLabels(labels);
        }
    }

    public static class ShuffleAction extends VideoPlaybackControlsRow.MultiAction {
        /**
         * @deprecated
         */
        @Deprecated
        public static final int OFF = 0;
        /**
         * @deprecated
         */
        @Deprecated
        public static final int ON = 1;
        public static final int INDEX_OFF = 0;
        public static final int INDEX_ON = 1;

        public ShuffleAction(Context context) {
            this(context, VideoPlaybackControlsRow.getIconHighlightColor(context));
        }

        public ShuffleAction(Context context, int highlightColor) {
            super(R.id.lb_control_shuffle);
            BitmapDrawable uncoloredDrawable = (BitmapDrawable) VideoPlaybackControlsRow.getStyledDrawable(context, R.styleable.lbPlaybackControlsActionIcons_shuffle);
            Drawable[] drawables = new Drawable[]{uncoloredDrawable, new BitmapDrawable(context.getResources(), VideoPlaybackControlsRow.createBitmap(uncoloredDrawable.getBitmap(), highlightColor))};
            this.setDrawables(drawables);
            String[] labels = new String[drawables.length];
            labels[0] = context.getString(R.string.lb_playback_controls_shuffle_enable);
            labels[1] = context.getString(R.string.lb_playback_controls_shuffle_disable);
            this.setLabels(labels);
        }
    }

    public static class RepeatAction extends VideoPlaybackControlsRow.MultiAction {
        /**
         * @deprecated
         */
        @Deprecated
        public static final int NONE = 0;
        /**
         * @deprecated
         */
        @Deprecated
        public static final int ALL = 1;
        /**
         * @deprecated
         */
        @Deprecated
        public static final int ONE = 2;
        public static final int INDEX_NONE = 0;
        public static final int INDEX_ALL = 1;
        public static final int INDEX_ONE = 2;

        public RepeatAction(Context context) {
            this(context, VideoPlaybackControlsRow.getIconHighlightColor(context));
        }

        public RepeatAction(Context context, int highlightColor) {
            this(context, highlightColor, highlightColor);
        }

        public RepeatAction(Context context, int repeatAllColor, int repeatOneColor) {
            super(R.id.lb_control_repeat);
            Drawable[] drawables = new Drawable[3];
            BitmapDrawable repeatDrawable = (BitmapDrawable) VideoPlaybackControlsRow.getStyledDrawable(context, R.styleable.lbPlaybackControlsActionIcons_repeat);
            BitmapDrawable repeatOneDrawable = (BitmapDrawable) VideoPlaybackControlsRow.getStyledDrawable(context, R.styleable.lbPlaybackControlsActionIcons_repeat_one);
            drawables[0] = repeatDrawable;
            drawables[1] = repeatDrawable == null ? null : new BitmapDrawable(context.getResources(), VideoPlaybackControlsRow.createBitmap(repeatDrawable.getBitmap(), repeatAllColor));
            drawables[2] = repeatOneDrawable == null ? null : new BitmapDrawable(context.getResources(), VideoPlaybackControlsRow.createBitmap(repeatOneDrawable.getBitmap(), repeatOneColor));
            this.setDrawables(drawables);
            String[] labels = new String[drawables.length];
            labels[0] = context.getString(R.string.lb_playback_controls_repeat_all);
            labels[1] = context.getString(R.string.lb_playback_controls_repeat_one);
            labels[2] = context.getString(R.string.lb_playback_controls_repeat_none);
            this.setLabels(labels);
        }
    }

    public static class ThumbsDownAction extends VideoPlaybackControlsRow.ThumbsAction {
        public ThumbsDownAction(Context context) {
            super(R.id.lb_control_thumbs_down, context, R.styleable.lbPlaybackControlsActionIcons_thumb_down, R.styleable.lbPlaybackControlsActionIcons_thumb_down_outline);
            String[] labels = new String[this.getActionCount()];
            labels[0] = context.getString(R.string.lb_playback_controls_thumb_down);
            labels[1] = context.getString(R.string.lb_playback_controls_thumb_down_outline);
            this.setLabels(labels);
        }
    }

    public static class ThumbsUpAction extends VideoPlaybackControlsRow.ThumbsAction {
        public ThumbsUpAction(Context context) {
            super(R.id.lb_control_thumbs_up, context, R.styleable.lbPlaybackControlsActionIcons_thumb_up, R.styleable.lbPlaybackControlsActionIcons_thumb_up_outline);
            String[] labels = new String[this.getActionCount()];
            labels[0] = context.getString(R.string.lb_playback_controls_thumb_up);
            labels[1] = context.getString(R.string.lb_playback_controls_thumb_up_outline);
            this.setLabels(labels);
        }
    }

    public abstract static class ThumbsAction extends VideoPlaybackControlsRow.MultiAction {
        /**
         * @deprecated
         */
        @Deprecated
        public static final int SOLID = 0;
        /**
         * @deprecated
         */
        @Deprecated
        public static final int OUTLINE = 1;
        public static final int INDEX_SOLID = 0;
        public static final int INDEX_OUTLINE = 1;

        public ThumbsAction(int id, Context context, int solidIconIndex, int outlineIconIndex) {
            super(id);
            Drawable[] drawables = new Drawable[]{VideoPlaybackControlsRow.getStyledDrawable(context, solidIconIndex), VideoPlaybackControlsRow.getStyledDrawable(context, outlineIconIndex)};
            this.setDrawables(drawables);
        }
    }

    public static class MoreActions extends Action {
        public MoreActions(Context context) {
            super(R.id.lb_control_more_actions);
            this.setIcon(context.getResources().getDrawable(R.drawable.lb_ic_more));
            this.setLabel1(context.getString(R.string.lb_playback_controls_more_actions));
        }
    }

    public static class PictureInPictureAction extends Action {
        public PictureInPictureAction(Context context) {
            super(R.id.lb_control_picture_in_picture);
            this.setIcon(VideoPlaybackControlsRow.getStyledDrawable(context, R.styleable.lbPlaybackControlsActionIcons_picture_in_picture));
            this.setLabel1(context.getString(R.string.lb_playback_controls_picture_in_picture));
            this.addKeyCode(171);
        }
    }

    public static class SkipPreviousAction extends Action {
        public SkipPreviousAction(Context context) {
            super(R.id.lb_control_skip_previous);
            this.setIcon(VideoPlaybackControlsRow.getStyledDrawable(context, R.styleable.lbPlaybackControlsActionIcons_skip_previous));
            this.setLabel1(context.getString(R.string.lb_playback_controls_skip_previous));
            this.addKeyCode(KeyEvent.KEYCODE_MEDIA_PREVIOUS);
        }
    }

    public static class SkipNextAction extends Action {
        public SkipNextAction(Context context) {
            super(R.id.lb_control_skip_next);
            this.setIcon(VideoPlaybackControlsRow.getStyledDrawable(context, R.styleable.lbPlaybackControlsActionIcons_skip_next));
            this.setLabel1(context.getString(R.string.lb_playback_controls_skip_next));
            this.addKeyCode(KeyEvent.KEYCODE_MEDIA_NEXT);
        }
    }

    public static class RewindAction extends VideoPlaybackControlsRow.MultiAction {
        public RewindAction(Context context) {
            this(context, 1);
        }

        public RewindAction(Context context, int numSpeeds) {
            super(R.id.lb_control_fast_rewind);
            if (numSpeeds < 1) {
                throw new IllegalArgumentException("numSpeeds must be > 0");
            } else {
                Drawable[] drawables = new Drawable[numSpeeds + 1];
                drawables[0] = VideoPlaybackControlsRow.getStyledDrawable(context, R.styleable.lbPlaybackControlsActionIcons_rewind);
                this.setDrawables(drawables);
                String[] labels = new String[this.getActionCount()];
                labels[0] = context.getString(R.string.lb_playback_controls_rewind);
                String[] labels2 = new String[this.getActionCount()];
                labels2[0] = labels[0];

                for (int i = 1; i <= numSpeeds; ++i) {
                    int multiplier = i + 1;
                    labels[i] = labels[i] = context.getResources().getString(R.string.lb_control_display_rewind_multiplier, multiplier);
                    labels2[i] = context.getResources().getString(R.string.lb_playback_controls_rewind_multiplier, multiplier);
                }

                this.setLabels(labels);
                this.setSecondaryLabels(labels2);
                this.addKeyCode(KeyEvent.KEYCODE_MEDIA_REWIND);
            }
        }
    }


    public static class FastForwardAction extends VideoPlaybackControlsRow.MultiAction {
        public FastForwardAction(Context context) {
            this(context, 1);
        }

        public FastForwardAction(Context context, int numSpeeds) {
            super(R.id.lb_control_fast_forward);
            if (numSpeeds < 1) {
                throw new IllegalArgumentException("numSpeeds must be > 0");
            } else {
                Drawable[] drawables = new Drawable[numSpeeds + 1];
                drawables[0] = VideoPlaybackControlsRow.getStyledDrawable(context, R.styleable.lbPlaybackControlsActionIcons_fast_forward);
                this.setDrawables(drawables);
                String[] labels = new String[this.getActionCount()];
                labels[0] = context.getString(R.string.lb_playback_controls_fast_forward);
                String[] labels2 = new String[this.getActionCount()];
                labels2[0] = labels[0];

                for (int i = 1; i <= numSpeeds; ++i) {
                    int multiplier = i + 1;
                    labels[i] = context.getResources().getString(R.string.lb_control_display_fast_forward_multiplier, multiplier);
                    labels2[i] = context.getResources().getString(R.string.lb_playback_controls_fast_forward_multiplier, multiplier);
                }

                this.setLabels(labels);
                this.setSecondaryLabels(labels2);
                this.addKeyCode(KeyEvent.KEYCODE_MEDIA_FAST_FORWARD);
            }
        }
    }

    public static class PlayPauseAction extends VideoPlaybackControlsRow.MultiAction {
        /**
         * @deprecated
         */
        @Deprecated
        public static final int PLAY = 0;
        /**
         * @deprecated
         */
        @Deprecated
        public static final int PAUSE = 1;
        public static final int INDEX_PLAY = 0;
        public static final int INDEX_PAUSE = 1;
        public static final int INDEX_REWIND = 2;
        public static final int INDEX_FAST_FORWARD = 3;

        public PlayPauseAction(Context context) {
            super(R.id.lb_control_play_pause);
            Drawable[] drawables = new Drawable[]{context.getResources().getDrawable(R.drawable.vod_pause2, null),
                    context.getResources().getDrawable(R.drawable.vod_play2, null),
                    context.getResources().getDrawable(R.drawable.vod_rewind2, null),
                    context.getResources().getDrawable(R.drawable.vod_ff2, null)};

            this.setDrawables(drawables);
            String[] labels = new String[drawables.length];
            labels[0] = "";
            labels[1] = "";
            labels[2] = "10";
            labels[3] = "10";
            this.setLabels(labels);
            this.addKeyCode(KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE);
            this.addKeyCode(KeyEvent.KEYCODE_MEDIA_PLAY);
            this.addKeyCode(KeyEvent.KEYCODE_MEDIA_PAUSE);

            this.addKeyCode(KeyEvent.KEYCODE_MEDIA_FAST_FORWARD);
            this.addKeyCode(KeyEvent.KEYCODE_MEDIA_REWIND);
        }
    }

    public abstract static class MultiAction extends Action {
        private int mIndex;
        private int action;
        private Drawable[] mDrawables;
        private String[] mLabels;
        private String[] mLabels2;

        public MultiAction(int id) {
            super(id);
        }

        public void setDrawables(Drawable[] drawables) {
            this.mDrawables = drawables;
            this.setIndex(0);
        }

        public void setLabels(String[] labels) {
            this.mLabels = labels;
            this.setIndex(0);
        }

        public void setSecondaryLabels(String[] labels) {
            this.mLabels2 = labels;
            this.setIndex(0);
        }

        public int getActionCount() {
            if (this.mDrawables != null) {
                return this.mDrawables.length;
            } else {
                return this.mLabels != null ? this.mLabels.length : 0;
            }
        }

        public Drawable getDrawable(int index) {
            return this.mDrawables == null ? null : this.mDrawables[index];
        }

        public String getLabel(int index) {
            return this.mLabels == null ? null : this.mLabels[index];
        }

        public String getSecondaryLabel(int index) {
            return this.mLabels2 == null ? null : this.mLabels2[index];
        }

        public void nextIndex() {
            this.setIndex(this.mIndex < this.getActionCount() - 1 ? this.mIndex + 1 : 0);
        }

        public void setIndex(int index) {
            this.mIndex = index;
            if (this.mDrawables != null) {
                this.setIcon(this.mDrawables[this.mIndex]);
            }

            if (this.mLabels != null) {
                this.setLabel1(this.mLabels[this.mIndex]);
            }

            if (this.mLabels2 != null) {
                this.setLabel2(this.mLabels2[this.mIndex]);
            }
        }

        public int getAction() {
            return action;
        }

        public void setAction(int action) {
            this.action = action;
        }

        public int getIndex() {
            return this.mIndex;
        }
    }

    public static class OnPlaybackProgressCallback {

        public OnPlaybackProgressCallback() {
        }

        public void onReadyStateChanged(VideoPlaybackControlsRow row, boolean ready) {

        }

        public void onRequestedShowControlBar(VideoPlaybackControlsRow row) {

        }

        public void onBlockedStateChanged(VideoPlaybackControlsRow row, boolean isLocked) {

        }

        public void onCurrentPositionChanged(VideoPlaybackControlsRow row, long currentTimeMs, boolean seeking) {
        }

        public void onDurationChanged(VideoPlaybackControlsRow row, long totalTime) {
        }

        public void onBufferedPositionChanged(VideoPlaybackControlsRow row, long bufferedProgressMs) {
        }

        public void onProgramInfoChanged(VideoPlaybackControlsRow row, long playTimeMs, long startTimeMs, long endTimeMs) {

        }
    }
}
