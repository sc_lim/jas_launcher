/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;

import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class Row {

    private static final int FLAG_ID_USE_MASK = 1;
    private static final int FLAG_ID_USE_HEADER = 1;
    private static final int FLAG_ID_USE_ID = 0;

    private int mFlags = FLAG_ID_USE_HEADER;
    private HeaderItem mHeaderItem;
    private long mId = -1;

    /**
     * Constructor for a Row.
     *
     * @param id         The id of the row.
     * @param headerItem The {@link HeaderItem} for this Row, or null if there
     *                   is no header.
     */
    public Row(long id, HeaderItem headerItem) {
        setId(id);
        setHeaderItem(headerItem);
    }

    /**
     * Constructor for a Row.
     *
     * @param headerItem The {@link HeaderItem} for this Row, or null if there
     *                   is no header.
     */
    public Row(HeaderItem headerItem) {
        setHeaderItem(headerItem);
    }

    /**
     * Constructor for a Row.
     */
    public Row() {
    }

    /**
     * Returns the {@link HeaderItem} that represents metadata for the row.
     *
     * @return The HeaderItem for this row, or null if unset.
     */
    public final HeaderItem getHeaderItem() {
        return mHeaderItem;
    }

    /**
     * Sets the {@link HeaderItem} that represents metadata for the row.
     *
     * @param headerItem The HeaderItem for this Row, or null if there is no
     *                   header.
     */
    public final void setHeaderItem(HeaderItem headerItem) {
        mHeaderItem = headerItem;
    }

    /**
     * Sets the id for this row.
     *
     * @param id The id of the row.
     */
    public final void setId(long id) {
        mId = id;
        setFlags(FLAG_ID_USE_ID, FLAG_ID_USE_MASK);
    }

    public final long getId() {
        if ((mFlags & FLAG_ID_USE_MASK) == FLAG_ID_USE_HEADER) {
            HeaderItem header = getHeaderItem();
            if (header != null) {
                return header.getId();
            }
            return -1;
        } else {
            return mId;
        }
    }

    final void setFlags(int flags, int mask) {
        mFlags = (mFlags & ~mask) | (flags & mask);
    }

    final int getFlags() {
        return mFlags;
    }

    public boolean isRenderedAsRowView() {
        return true;
    }
}