/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.CollectedContent;

public class CollectionListResponse extends BaseResponse {
    public static final Creator<CollectionListResponse> CREATOR = new Creator<CollectionListResponse>() {
        @Override
        public CollectionListResponse createFromParcel(Parcel in) {
            return new CollectionListResponse(in);
        }

        @Override
        public CollectionListResponse[] newArray(int size) {
            return new CollectionListResponse[size];
        }
    };
    @Expose
    @SerializedName("data")
    private List<CollectedContent> collectionList;

    protected CollectionListResponse(Parcel in) {
        super(in);
        in.readList(collectionList, CollectedContent.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeList(collectionList);
    }

    public List<CollectedContent> getCollectionList() {
        return collectionList;
    }
}
