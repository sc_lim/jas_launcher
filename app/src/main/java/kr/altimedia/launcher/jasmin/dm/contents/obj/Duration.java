/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.altimedia.tvmodule.util.StringUtils;

/**
 * Created by mc.kim on 09,06,2020
 */
public class Duration implements Parcelable {
    public static final Creator<Duration> CREATOR = new Creator<Duration>() {
        @Override
        public Duration createFromParcel(Parcel in) {
            return new Duration(in);
        }

        @Override
        public Duration[] newArray(int size) {
            return new Duration[size];
        }
    };
    @Expose
    @SerializedName("unit")
    private String unit;
    @Expose
    @SerializedName("value")
    private int value;

    public Duration(DurationUnit unit) {
        this.unit = unit.unitName;
        this.value = 0;
    }

    public Duration(DurationUnit unit, int value) {
        this.unit = unit.unitName;
        this.value = value;
    }

    protected Duration(Parcel in) {
        unit = in.readString();
        value = in.readInt();
    }

    public static Duration deserialize(String durationString)
            throws JsonParseException {

        final Pattern yearPattern = Pattern.compile("\\d+Y");
        final Pattern monthPattern = Pattern.compile("\\d+M");
        final Pattern dayPattern = Pattern.compile("\\d+D");
        if (StringUtils.nullToEmpty(durationString).isEmpty()) {
            return new Duration(DurationUnit.UNIT_UNLIMITED);
        }
        Matcher yearMatcher = yearPattern.matcher(durationString);
        if (yearMatcher.find()) {
            String durationDateString = yearMatcher.group().replace("Y", "");
            return new Duration(DurationUnit.UNIT_YEAR, Integer.parseInt(durationDateString));
        }
        Matcher monthMatcher = monthPattern.matcher(durationString);
        if (monthMatcher.find()) {
            String durationDateString = monthMatcher.group().replace("M", "");
            return new Duration(DurationUnit.UNIT_MONTH, Integer.parseInt(durationDateString));
        }

        Matcher dayMatcher = dayPattern.matcher(durationString);
        if (dayMatcher.find()) {
            String durationDateString = dayMatcher.group().replace("D", "");
            return new Duration(DurationUnit.UNIT_DAY, Integer.parseInt(durationDateString));
        }
        return new Duration(DurationUnit.UNIT_UNLIMITED);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(unit);
        dest.writeInt(value);
    }

    public int getValue() {
        return value;
    }

    public DurationUnit getUnit() {
        return DurationUnit.findByName(unit);
    }

    @Override
    public String toString() {
        if (DurationUnit.findByName(unit) == DurationUnit.UNIT_UNLIMITED) {
            return "0";
        }
        return value + unit;
    }
}
