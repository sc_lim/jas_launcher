package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

public class HandleViewPager extends ViewPager {
    private OnKeyEventListener mOnKeyEventListener;

    public HandleViewPager(@NonNull Context context) {
        super(context);
    }

    public HandleViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setOnKeyEventListener(OnKeyEventListener mOnKeyEventListener) {
        this.mOnKeyEventListener = mOnKeyEventListener;
    }

    @Override
    public boolean executeKeyEvent(@NonNull KeyEvent event) {
        if (mOnKeyEventListener != null) {
            boolean isConsumed = mOnKeyEventListener.onKeyEvent(event);
            if (isConsumed) {
                return true;
            }
        }

        return super.executeKeyEvent(event);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mOnKeyEventListener = null;
    }

    public interface OnKeyEventListener {
        boolean onKeyEvent(KeyEvent event);
    }
}
