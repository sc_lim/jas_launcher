
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search.obj;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import kr.altimedia.launcher.jasmin.dm.def.NormalDateTimeDeserializer;

public class SearchChannel implements Parcelable {
    public static final Creator<SearchChannel> CREATOR = new Creator<SearchChannel>() {
        @Override
        public SearchChannel createFromParcel(Parcel in) {
            return new SearchChannel(in);
        }

        @Override
        public SearchChannel[] newArray(int size) {
            return new SearchChannel[size];
        }
    };
//    @Expose
//    @SerializedName("DURATION")
//    private String duration;
@Expose
@SerializedName("BROAD_DATE")
private String broadDate;
    @Expose
    @SerializedName("SERVICE_ID")
    private String serviceId;
    @Expose
    @SerializedName("CONTENT_TYPE")
    private String contentType;
    @Expose
    @SerializedName("PRO_NAME")
    private String proName;
    @Expose
    @SerializedName("CHNL_NAME")
    private String chnlName;
    @Expose
    @SerializedName("IMG_URL")
    private String imgUrl;
    @Expose
    @SerializedName("BRO_TIME")
    private String broTime;
    @Expose
    @SerializedName("PRO_ENDTIME")
    private String proEndtime;
    @Expose
    @SerializedName("CONTENTS_NAME")
    private String contentsName;
    @Expose
    @SerializedName("AGE_GRD")
    private String ageGrd;
    @Expose
    @SerializedName("PID")
    private String proId;
    @Expose
    @SerializedName("MID")
    private String mid;
    @Expose
    @SerializedName("START_DATE")
    @JsonAdapter(NormalDateTimeDeserializer.class)
    private Date startDate;
    @Expose
    @SerializedName("END_DATE")
    @JsonAdapter(NormalDateTimeDeserializer.class)
    private Date endDate;

    protected SearchChannel(Parcel in) {
//        duration = in.readString();
        broadDate = in.readString();
        serviceId = in.readString();
        contentType = in.readString();
        proName = in.readString();
        chnlName = in.readString();
        imgUrl = in.readString();
        broTime = in.readString();
        proEndtime = in.readString();
        contentsName = in.readString();
        ageGrd = in.readString();
        proId = in.readString();
        mid = in.readString();
        startDate = ((Date) in.readValue((Date.class.getClassLoader())));
        endDate = ((Date) in.readValue((Date.class.getClassLoader())));
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
//        parcel.writeString(duration);
        parcel.writeString(broadDate);
        parcel.writeString(serviceId);
        parcel.writeString(contentType);
        parcel.writeString(proName);
        parcel.writeString(chnlName);
        parcel.writeString(imgUrl);
        parcel.writeString(broTime);
        parcel.writeString(proEndtime);
        parcel.writeString(contentsName);
        parcel.writeString(ageGrd);
        parcel.writeString(proId);
        parcel.writeString(mid);
        parcel.writeValue(startDate);
        parcel.writeValue(endDate);
    }

//    public String getDuration() {
//        return duration;
//    }

    public String getBroadDate() {
        return broadDate;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getContentType() {
        return contentType;
    }

    public String getProName() {
        return proName;
    }

    public String getChnlName() {
        return chnlName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getBroTime() {
        return broTime;
    }

    public String getProEndtime() {
        return proEndtime;
    }

    public String getContentsName() {
        return contentsName;
    }

    public String getAgeGrd() {
        return ageGrd;
    }

    public String getProId() {
        return proId;
    }

    public String getMid() {
        return mid;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @NonNull
    @Override
    public String toString() {
        return "SearchChannel{" +
                "broadDate=" + broadDate +
                ", serviceId=" + serviceId +
                ", contentType=" + contentType +
                ", proName=" + proName +
                ", chnlName=" + chnlName +
                ", imgUrl=" + imgUrl +
                ", broTime=" + broTime +
                ", proEndtime=" + proEndtime +
                ", contentsName=" + contentsName +
                ", ageGrd=" + ageGrd +
                ", proId=" + proId +
                ", mid=" + mid +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                "}";
    }
}