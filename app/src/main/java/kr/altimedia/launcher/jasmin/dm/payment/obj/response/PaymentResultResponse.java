/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.obj.response;

import android.os.Parcel;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.VodPaymentResult;

public class PaymentResultResponse extends BaseResponse {
    public static final Creator<PaymentResultResponse> CREATOR = new Creator<PaymentResultResponse>() {
        @Override
        public PaymentResultResponse createFromParcel(Parcel in) {
            return new PaymentResultResponse(in);
        }

        @Override
        public PaymentResultResponse[] newArray(int size) {
            return new PaymentResultResponse[size];
        }
    };
    @Expose
    @SerializedName("data")
    private VodPaymentResult result;

    protected PaymentResultResponse(Parcel in) {
        super(in);
        result = in.readParcelable(VodPaymentResult.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(result, flags);
    }

    @NonNull
    @Override
    public String toString() {
        return "PaymentResultResponse{" +
                "result='" + result + '\'' +
                '}';
    }

    public VodPaymentResult getResult() {
        return result;
    }
}
