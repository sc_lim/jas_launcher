/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 08,05,2020
 */
public class BaseResponse implements Parcelable {
    @Expose
    @SerializedName("returnCode")
    private String returnCode;
    @Expose
    @SerializedName("errorCode")
    private String errorCode;
    @Expose
    @SerializedName("errorMessage")
    private String errorMessage;
    @Expose
    @SerializedName("transactionId")
    private String transactionId;

    public String getReturnCode() {
        return returnCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getTransactionId() {
        return transactionId;
    }

    protected BaseResponse(Parcel in) {
        returnCode = in.readString();
        errorCode = in.readString();
        errorMessage = in.readString();
        transactionId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(returnCode);
        dest.writeString(errorCode);
        dest.writeString(errorMessage);
        dest.writeString(transactionId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BaseResponse> CREATOR = new Creator<BaseResponse>() {
        @Override
        public BaseResponse createFromParcel(Parcel in) {
            return new BaseResponse(in);
        }

        @Override
        public BaseResponse[] newArray(int size) {
            return new BaseResponse[size];
        }
    };

}
