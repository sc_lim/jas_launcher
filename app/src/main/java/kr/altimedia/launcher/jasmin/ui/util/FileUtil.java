/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.util;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import com.altimedia.util.Log;

/**
 * Created by mc.kim on 03,06,2020
 */
public class FileUtil {
    private static final String TAG = FileUtil.class.getSimpleName();
    private static final String SOURCE_FOLDER_NAME = "3bbData";
    private static final String ROOT_PATH = "/storage/taurusDb";

    public static boolean saveFile(String fileName, String data) {
        File dir = new File(ROOT_PATH + File.separator + SOURCE_FOLDER_NAME);
        boolean doSave = true;
        if (!dir.exists()) {
            doSave = dir.mkdirs();
        }
        Log.d(TAG, "dir.path : " + dir.getAbsolutePath() + ", data : " + data);
        if (doSave) {
            File saveFile = new File(dir, fileName);
            if (!saveFile.exists()) {
                try {
                    saveFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return writeToFile(saveFile, data);
        }
        return false;
    }

    public static String loadFile(String fileName) throws IOException {
        File file = new File(ROOT_PATH + File.separator + SOURCE_FOLDER_NAME, fileName);
        if (!file.exists()) {
            return "";
        }
        Log.d(TAG, "loadFile dir.path : " + file.getAbsolutePath());
        String line = null;
        StringBuilder builder = new StringBuilder();
        BufferedReader buf = new BufferedReader(new FileReader(file));
        while ((line = buf.readLine()) != null) {
            builder.append(line);
            builder.append("\n");
        }
        buf.close();
        return builder.toString();
    }

    private static boolean writeToFile(File file, String vale) {
        FileWriter fw;
        try {
            fw = new FileWriter(file);
            fw.write(vale);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if (fw != null) {
            try {
                fw.close();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public static String getRawFile(Context context, int resourceId) throws IOException {
        InputStream is = context.getResources().openRawResource(resourceId);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        Reader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
        int n;
        while ((n = reader.read(buffer)) != -1) {
            writer.write(buffer, 0, n);
        }
        is.close();
        return writer.toString();
    }
}
