/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter;

import android.content.res.Resources;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter.VodListRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.BaseHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.rowView.ContentsRowView;
import kr.altimedia.launcher.jasmin.ui.view.util.FocusHighlightHelper;

public class SeriesVodListRowPresenter extends VodListRowPresenter {
    private OnKeyVodListener onKeyVodListener;

    public SeriesVodListRowPresenter(int mContentsResourceId) {
        super(mContentsResourceId);
    }

    public void setOnKeyVodListener(OnKeyVodListener onKeyVodListener) {
        this.onKeyVodListener = onKeyVodListener;
    }

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {
        ContentsRowView rowView = mContentsResourceId != -1 ?
                new ContentsRowView(parent.getContext(), mContentsResourceId) : new ContentsRowView(parent.getContext());
        return new SeriesVodListRowViewViewHolder(rowView, rowView.getGridView(), this, onKeyVodListener);
    }

    @Override
    protected void initializeRowViewHolder(RowPresenter.ViewHolder vh) {
        super.initializeRowViewHolder(vh);
        SeriesVodListRowViewViewHolder viewHolder = (SeriesVodListRowViewViewHolder) vh;
        viewHolder.mItemBridgeAdapter = new SeriesVodListItemBridgeAdapter(viewHolder);
        FocusHighlightHelper.setupBrowseItemFocusHighlight(viewHolder.mItemBridgeAdapter, this.mFocusZoomFactor, false);
    }

    private static class SeriesVodListRowViewViewHolder extends VodListRowPresenter.ViewHolder {
        private OnKeyVodListener onKeyVodListener;

        public SeriesVodListRowViewViewHolder(View rootView, HorizontalGridView gridView, VodListRowPresenter p, OnKeyVodListener onKeyVodListener) {
            super(rootView, gridView, p);
            this.onKeyVodListener = onKeyVodListener;
        }

        @Override
        protected void initKeyListener() {
            setBaseOnItemKeyListener(new SeriesVodListKeyListener(mGridView, onKeyVodListener));
        }

        @Override
        protected void initGridView() {
            // 포커스 시 확대되는 부분의 패딩 클립을 위
            BaseHeaderPresenter.ViewHolder headerViewHolder = getHeaderViewHolder();
            View headerView = headerViewHolder.view;
            int headerBottom = headerViewHolder.getHeaderViewHeight();

            Resources resources = headerView.getResources();
            int top = headerBottom + resources.getDimensionPixelSize(R.dimen.vod_header_padding_bottom);
            int right = resources.getDimensionPixelSize(R.dimen.vod_detail_padding_right);

            mGridView.setPadding(0, top, right, 0);
            mGridView.setClipChildren(false);

            int gap = (int) resources.getDimension(R.dimen.poster_horizontal_space);
            mGridView.setHorizontalSpacing(gap);
        }
    }

    private static class SeriesVodListKeyListener extends VodListKeyListener {
        private OnKeyVodListener onKeyVodListener;

        public SeriesVodListKeyListener(HorizontalGridView mGridView, OnKeyVodListener onKeyVodListener) {
            super(mGridView);

            this.onKeyVodListener = onKeyVodListener;
        }

        @Override
        public boolean onItemKey(int keyCode, Presenter.ViewHolder var1, Object var2, Presenter.ViewHolder var3, Row var4) {
            int lastIndex = mGridView.getAdapter().getItemCount() - 1;
            int index = mGridView.getChildPosition(mGridView.getFocusedChild());

            boolean isConsumed = onKeyVodListener != null && onKeyVodListener.onKey(keyCode, index);

            if (isConsumed) {
                return true;
            }

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    if (index == lastIndex) {
                        mGridView.scrollToPosition(0);
                        return true;
                    }

                    return false;
            }

            return false;
        }
    }

    private static class SeriesVodListItemBridgeAdapter extends VodListItemBridgeAdapter {
        private final float ALIGNMENT = 81.5f;

        public SeriesVodListItemBridgeAdapter(VodListRowPresenter.ViewHolder mRowViewHolder) {
            super(mRowViewHolder);
            setAlignment(ALIGNMENT);
        }

        @Override
        public void onBind(ViewHolder viewHolder) {
            super.onBind(viewHolder);
        }

        @Override
        public void onUnbind(ViewHolder viewHolder) {
            super.onUnbind(viewHolder);
        }
    }

    public interface OnKeyVodListener {
        boolean onKey(int keyCode, int index);
    }
}
