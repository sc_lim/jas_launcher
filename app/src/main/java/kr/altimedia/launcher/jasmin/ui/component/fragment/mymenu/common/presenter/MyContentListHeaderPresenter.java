/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowHeaderPresenter;

public class MyContentListHeaderPresenter extends RowHeaderPresenter {
    private final String TAG = MyContentListHeaderPresenter.class.getSimpleName();

    public MyContentListHeaderPresenter() {
        this(R.layout.item_mymenu_vod_content_list_header);
    }

    public MyContentListHeaderPresenter(int layoutResourceId) {
        this(layoutResourceId, true);
    }

    public MyContentListHeaderPresenter(int layoutResourceId, boolean animateSelect) {
        super(layoutResourceId, animateSelect);
    }
}
