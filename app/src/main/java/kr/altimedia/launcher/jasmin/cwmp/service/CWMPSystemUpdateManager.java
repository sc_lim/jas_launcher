/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service;

import com.altimedia.update.UpdateManager;
import com.altimedia.update.apk.RequestApkDownloadCallback;
import com.altimedia.update.apk.RequestApkUpdateCallback;
import com.altimedia.update.apk.RequestApkUpdateInfo;
import com.altimedia.update.apk.RequestApkUpdateInfoCallback;
import com.altimedia.update.apk.UpdateApkData;
import com.altimedia.update.fw.RequestFwDownloadCallback;
import com.altimedia.update.fw.RequestFwUpdateCallback;
import com.altimedia.update.fw.RequestFwUpdateInfo;
import com.altimedia.update.fw.RequestFwUpdateInfoCallback;
import com.altimedia.update.fw.UpdateFwData;

public class CWMPSystemUpdateManager {

    private static final CWMPSystemUpdateManager instance = new CWMPSystemUpdateManager();
    private UpdateManager mUpdateManager;

    private CWMPSystemUpdateManager() {
        try {
            mUpdateManager = UpdateManager.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static CWMPSystemUpdateManager getInstance() {
        return instance;
    }

    public RequestFwUpdateInfo requestFwUpdateInfo(RequestFwUpdateInfoCallback callback) {
        try {
            return mUpdateManager.requestFwUpdateInfo(callback);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public RequestApkUpdateInfo requestApkUpdateInfo(RequestApkUpdateInfoCallback callback) {
        try {
            return mUpdateManager.requestApkUpdateInfo(callback);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void cancelRequestFwUpdateInfo(RequestFwUpdateInfo requestFwUpdateInfo) {
        try {
            mUpdateManager.cancelRequestFwUpdateInfo(requestFwUpdateInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelRequestApkUpdateInfo(RequestApkUpdateInfo requestApkUpdateInfo) {
        try {
            mUpdateManager.cancelRequestApkUpdateInfo(requestApkUpdateInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelFwUpdate(UpdateFwData updateFwData) {
        try {
            mUpdateManager.cancelFwUpdate(updateFwData);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void requestFwDownload(UpdateFwData updateFwData, RequestFwDownloadCallback callback) {
        try {
            mUpdateManager.requestFwDownload(updateFwData, callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public UpdateFwData requestFwDownload(String url, RequestFwDownloadCallback callback) {
        try {
            return mUpdateManager.requestFwDownload(url, callback);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void cancelFwDownload(UpdateFwData updateFwData) {
        try {
            mUpdateManager.cancelFwDownload(updateFwData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestFwUpdate(UpdateFwData updateFwData, RequestFwUpdateCallback callback) {
        try {
            mUpdateManager.requestFwUpdate(updateFwData, callback);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void requestApkList(RequestApkUpdateInfoCallback callback) {
        try {
            mUpdateManager.requestApkList(callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelApkUpdate(UpdateApkData updateApkData) {
        try {
            mUpdateManager.cancelApkUpdate(updateApkData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestApkDownload(UpdateApkData updateApkData, RequestApkDownloadCallback callback) {
        try {
            mUpdateManager.requestApkDownload(updateApkData, callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelApkDownload(UpdateApkData updateApkData) {
        try {
            mUpdateManager.cancelApkDownload(updateApkData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestApkUpdate(UpdateApkData updateApkData, RequestApkUpdateCallback callback) {
        try {
            mUpdateManager.requestApkUpdate(updateApkData, callback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
