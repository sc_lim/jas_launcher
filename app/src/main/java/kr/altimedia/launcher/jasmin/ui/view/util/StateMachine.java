/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;

public final class StateMachine {
    static final boolean DEBUG = false;
    static final String TAG = "StateMachine";
    public static final int STATUS_ZERO = 0;
    public static final int STATUS_INVOKED = 1;
    final ArrayList<State> mStates = new ArrayList();
    final ArrayList<State> mFinishedStates = new ArrayList();
    final ArrayList<State> mUnfinishedStates = new ArrayList();

    public StateMachine() {
    }

    public void addState(StateMachine.State state) {
        if (!this.mStates.contains(state)) {
            this.mStates.add(state);
        }

    }

    public void addTransition(StateMachine.State fromState, StateMachine.State toState, StateMachine.Event event) {
        StateMachine.Transition transition = new StateMachine.Transition(fromState, toState, event);
        toState.addIncoming(transition);
        fromState.addOutgoing(transition);
    }

    public void addTransition(StateMachine.State fromState, StateMachine.State toState, StateMachine.Condition condition) {
        StateMachine.Transition transition = new StateMachine.Transition(fromState, toState, condition);
        toState.addIncoming(transition);
        fromState.addOutgoing(transition);
    }

    public void addTransition(StateMachine.State fromState, StateMachine.State toState) {
        StateMachine.Transition transition = new StateMachine.Transition(fromState, toState);
        toState.addIncoming(transition);
        fromState.addOutgoing(transition);
    }

    public void start() {
        this.mUnfinishedStates.addAll(this.mStates);
        this.runUnfinishedStates();
    }

    void runUnfinishedStates() {
        boolean changed;
        do {
            changed = false;

            for (int i = this.mUnfinishedStates.size() - 1; i >= 0; --i) {
                StateMachine.State state = (StateMachine.State) this.mUnfinishedStates.get(i);
                if (state.runIfNeeded()) {
                    this.mUnfinishedStates.remove(i);
                    this.mFinishedStates.add(state);
                    changed = true;
                }
            }
        } while (changed);

    }

    public void fireEvent(StateMachine.Event event) {
        Log.d(TAG,"event : "+event.mName);
        for (int i = 0; i < this.mFinishedStates.size(); ++i) {
            StateMachine.State state = (StateMachine.State) this.mFinishedStates.get(i);
            if (state.mOutgoings != null && (state.mBranchStart || state.mInvokedOutTransitions <= 0)) {
                Iterator var4 = state.mOutgoings.iterator();

                while (var4.hasNext()) {
                    StateMachine.Transition t = (StateMachine.Transition) var4.next();
                    if (t.mState != 1 && t.mEvent == event) {
                        t.mState = 1;
                        ++state.mInvokedOutTransitions;
                        if (!state.mBranchStart) {
                            break;
                        }
                    }
                }
            }
        }

        this.runUnfinishedStates();
    }

    public void reset() {
        this.mUnfinishedStates.clear();
        this.mFinishedStates.clear();
        Iterator var1 = this.mStates.iterator();

        while (true) {
            StateMachine.State state;
            do {
                if (!var1.hasNext()) {
                    return;
                }

                state = (StateMachine.State) var1.next();
                state.mStatus = 0;
                state.mInvokedOutTransitions = 0;
            } while (state.mOutgoings == null);

            StateMachine.Transition t;
            for (Iterator var3 = state.mOutgoings.iterator(); var3.hasNext(); t.mState = 0) {
                t = (StateMachine.Transition) var3.next();
            }
        }
    }

    public static class State {
        final String mName;
        final boolean mBranchStart;
        final boolean mBranchEnd;
        int mStatus;
        int mInvokedOutTransitions;
        ArrayList<Transition> mIncomings;
        ArrayList<Transition> mOutgoings;

        public String toString() {
            return "[" + this.mName + " " + this.mStatus + "]";
        }

        public State(String name) {
            this(name, false, true);
        }

        public State(String name, boolean branchStart, boolean branchEnd) {
            this.mStatus = 0;
            this.mInvokedOutTransitions = 0;
            this.mName = name;
            this.mBranchStart = branchStart;
            this.mBranchEnd = branchEnd;
        }

        void addIncoming(StateMachine.Transition t) {
            if (this.mIncomings == null) {
                this.mIncomings = new ArrayList();
            }

            this.mIncomings.add(t);
        }

        void addOutgoing(StateMachine.Transition t) {
            if (this.mOutgoings == null) {
                this.mOutgoings = new ArrayList();
            }

            this.mOutgoings.add(t);
        }

        public void run() {
        }

        final boolean checkPreCondition() {
            if (this.mIncomings == null) {
                return true;
            } else {
                Iterator var1;
                StateMachine.Transition t;
                if (this.mBranchEnd) {
                    var1 = this.mIncomings.iterator();

                    do {
                        if (!var1.hasNext()) {
                            return true;
                        }

                        t = (StateMachine.Transition) var1.next();
                    } while (t.mState == 1);

                    return false;
                } else {
                    var1 = this.mIncomings.iterator();

                    do {
                        if (!var1.hasNext()) {
                            return false;
                        }

                        t = (StateMachine.Transition) var1.next();
                    } while (t.mState != 1);

                    return true;
                }
            }
        }

        final boolean runIfNeeded() {
            if (this.mStatus != 1 && this.checkPreCondition()) {
                this.mStatus = 1;
                this.run();
                this.signalAutoTransitionsAfterRun();
                return true;
            } else {
                return false;
            }
        }

        final void signalAutoTransitionsAfterRun() {
            if (this.mOutgoings != null) {
                Iterator var1 = this.mOutgoings.iterator();

                do {
                    StateMachine.Transition t;
                    do {
                        do {
                            if (!var1.hasNext()) {
                                return;
                            }

                            t = (StateMachine.Transition) var1.next();
                        } while (t.mEvent != null);
                    } while (t.mCondition != null && !t.mCondition.canProceed());

                    ++this.mInvokedOutTransitions;
                    t.mState = 1;
                } while (this.mBranchStart);
            }

        }

        public final int getStatus() {
            return this.mStatus;
        }
    }

    static class Transition {
        final StateMachine.State mFromState;
        final StateMachine.State mToState;
        final StateMachine.Event mEvent;
        final StateMachine.Condition mCondition;
        int mState = 0;

        Transition(StateMachine.State fromState, StateMachine.State toState, StateMachine.Event event) {
            if (event == null) {
                throw new IllegalArgumentException();
            } else {
                this.mFromState = fromState;
                this.mToState = toState;
                this.mEvent = event;
                this.mCondition = null;
            }
        }

        Transition(StateMachine.State fromState, StateMachine.State toState) {
            this.mFromState = fromState;
            this.mToState = toState;
            this.mEvent = null;
            this.mCondition = null;
        }

        Transition(StateMachine.State fromState, StateMachine.State toState, StateMachine.Condition condition) {
            if (condition == null) {
                throw new IllegalArgumentException();
            } else {
                this.mFromState = fromState;
                this.mToState = toState;
                this.mEvent = null;
                this.mCondition = condition;
            }
        }

        public String toString() {
            String signalName;
            if (this.mEvent != null) {
                signalName = this.mEvent.mName;
            } else if (this.mCondition != null) {
                signalName = this.mCondition.mName;
            } else {
                signalName = "auto";
            }

            return "[" + this.mFromState.mName + " -> " + this.mToState.mName + " <" + signalName + ">]";
        }
    }

    public static class Condition {
        final String mName;

        public Condition(String name) {
            this.mName = name;
        }

        public boolean canProceed() {
            return true;
        }
    }

    public static class Event {
        final String mName;

        public Event(String name) {
            this.mName = name;
        }
    }
}
