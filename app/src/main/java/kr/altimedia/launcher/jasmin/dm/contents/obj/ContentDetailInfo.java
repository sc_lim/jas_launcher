/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 13,05,2020
 */
public class ContentDetailInfo implements Parcelable {
    @Expose
    @SerializedName("seriesInfo")
    private SeriesContent seriesContent;
    @SerializedName("content")
    @Expose
    private Content content;

    protected ContentDetailInfo(Parcel in) {
        seriesContent = in.readParcelable(SeriesContent.class.getClassLoader());
        content = in.readParcelable(Content.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(seriesContent, flags);
        dest.writeParcelable(content, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContentDetailInfo> CREATOR = new Creator<ContentDetailInfo>() {
        @Override
        public ContentDetailInfo createFromParcel(Parcel in) {
            return new ContentDetailInfo(in);
        }

        @Override
        public ContentDetailInfo[] newArray(int size) {
            return new ContentDetailInfo[size];
        }
    };

    public Content getContent() {
        return content;
    }

    public SeriesContent getSeriesContent() {
        return seriesContent;
    }

    public void setSeriesContent(SeriesContent seriesContent) {
        this.seriesContent = seriesContent;
    }

    public void setCategoryId(String categoryId) {
        if(this.content == null){
            return;
        }
        this.content.setCategoryId(categoryId);
    }

    public ContentDetailInfo(Content content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ContentDetailInfo{" +
                "seriesContent=" + seriesContent +
                ", content=" + content +
                '}';
    }
}
