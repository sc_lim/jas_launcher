/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.tutorial;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.tutorial.presenter.TutorialViewPagerPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.PagerRow;

public class TutorialDialogFragment extends DialogFragment {
    public static final String CLASS_NAME = TutorialDialogFragment.class.getName();
    private static final String TAG = TutorialDialogFragment.class.getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_tutorial, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter();
        objectAdapter.addAll(0, getItems());
        PagerRow row = new PagerRow(objectAdapter);

        TutorialViewPagerPresenter pagerPresenter = new TutorialViewPagerPresenter(false, R.layout.view_tutorial_indicator, getChildFragmentManager());
        Presenter.ViewHolder viewHolder = pagerPresenter.onCreateViewHolder((ViewGroup) view);
        pagerPresenter.onBindViewHolder(viewHolder, row);

        LinearLayout layout = view.findViewById(R.id.tutorial_layout);
        layout.addView(viewHolder.view);
    }

    private ArrayList<TutorialItem> getItems() {
        ArrayList<TutorialItem> list = new ArrayList<>();

        list.add(new TutorialItem(R.string.tutorial_step1_title, R.string.tutorial_step1_desc, null));
        list.add(new TutorialItem(R.string.tutorial_step2_title, R.string.tutorial_step2_desc, null));
        list.add(new TutorialItem(R.string.tutorial_step3_title, R.string.tutorial_step3_desc, null));
        list.add(new TutorialItem(R.string.tutorial_step4_title, R.string.tutorial_step4_desc, null));

        return list;
    }
}
