/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon;

import com.altimedia.util.Log;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseDataManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.coupon.module.CouponModule;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponHomeInfo;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProductList;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseRequest;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseResult;
import kr.altimedia.launcher.jasmin.dm.coupon.object.MyCoupon;
import kr.altimedia.launcher.jasmin.dm.coupon.remote.CouponRemote;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class CouponDataManager extends BaseDataManager {
    private final String TAG = CouponDataManager.class.getSimpleName();
    private CouponRemote mCouponRemote;

    private final String MY_COUPON_ALL = "ALL";
    private final String MY_COUPON_POINT = "THB";
    private final String MY_COUPON_DISCOUNT = "RATE";
    private final String MY_COUPON_EXPIRING = "EXPIRING";

    @Override
    public void initializeManager(OkHttpClient mOkHttpClient) {
        CouponModule couponModule = new CouponModule();
        Retrofit couponRetrofit = couponModule.provideCouponModule(mOkHttpClient);
        mCouponRemote = new CouponRemote(couponModule.provideCouponApi(couponRetrofit));
    }

    public MbsDataTask getCouponHomeInfo(String said, String profileId, boolean includeCoupons, MbsDataProvider<String, CouponHomeInfo> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, CouponHomeInfo>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mCouponRemote.getCouponHomeInfo(said, profileId, includeCoupons);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, CouponHomeInfo result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    private MbsDataTask getMyCouponList(String said, String profileId, String searchTarget, boolean includeHistory,
                                        MbsDataProvider<String, List<MyCoupon>> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, List<MyCoupon>>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mCouponRemote.getMyCouponList(said, profileId, searchTarget, includeHistory);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, List<MyCoupon> result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(profileId);
        return mMbsDataTask;
    }

    public MbsDataTask getAllCouponList(String said, String profileId, boolean includeHistory,
                                        MbsDataProvider<String, List<MyCoupon>> provider) {
        return getMyCouponList(said, profileId, MY_COUPON_ALL, includeHistory, provider);
    }

    public MbsDataTask getMyDiscountCouponList(String said, String profileId, boolean includeHistory,
                                               MbsDataProvider<String, List<MyCoupon>> provider) {
        return getMyCouponList(said, profileId, MY_COUPON_DISCOUNT, includeHistory, provider);
    }

    public MbsDataTask getMyPointCouponList(String said, String profileId, boolean includeHistory,
                                            MbsDataProvider<String, List<MyCoupon>> provider) {
        return getMyCouponList(said, profileId, MY_COUPON_POINT, includeHistory, provider);
    }

    public MbsDataTask getMyExpiringCouponList(String said, String profileId, boolean includeHistory,
                                               MbsDataProvider<String, List<MyCoupon>> provider) {
        return getMyCouponList(said, profileId, MY_COUPON_EXPIRING, includeHistory, provider);
    }

    public MbsDataTask postRegisterCoupon(String said, String profileId, String couponNumber,
                                          MbsDataProvider<String, MyCoupon> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(provider, new MbsTaskCallback<String, MyCoupon>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException , AuthenticationException{
                return mCouponRemote.postRegisterCoupon(said, profileId, couponNumber);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, MyCoupon result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(said);
        return mMbsDataTask;
    }

    public MbsDataTask getCouponProductList(String said, String profileId,
                                            MbsDataProvider<String, CouponProductList> provider) {
        MbsDataTask mMbsDataTask = new MbsDataTask(provider, new MbsTaskCallback<String, CouponProductList>() {

            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mCouponRemote.getCouponProductList(said, profileId);
            }

            @Override
            public void onTaskFailed(int key) {
                provider.needLoading(false);
                provider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                provider.needLoading(false);
                provider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, CouponProductList result) {
                provider.needLoading(false);
                provider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                provider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                provider.needLoading(false);
                return null;
            }
        });

        mMbsDataTask.execute(profileId);
        return mMbsDataTask;
    }

    public MbsDataTask requestPurchaseCoupon(String said, String profileId, String cpnTypeId, String paymentMethod, String totalPrice,
                                             String paymentDetail, String creditCardType, String bankCode,
                                             MbsDataProvider<String, CouponPurchaseRequest> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, CouponPurchaseRequest>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mCouponRemote.requestPurchaseCoupon(said, profileId, cpnTypeId, paymentMethod, totalPrice, paymentDetail, creditCardType, bankCode);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, CouponPurchaseRequest result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "requestPurchaseCoupon: onTaskPostExecute: result=" + result);
                }

                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(cpnTypeId);
        return mMbsDataTask;
    }

    public MbsDataTask getPurchaseCouponResult(String purchaseId,
                                               MbsDataProvider<String, CouponPurchaseResult> mbsDataProvider) {
        MbsDataTask mMbsDataTask = new MbsDataTask<String>(mbsDataProvider, new MbsTaskCallback<String, CouponPurchaseResult>() {
            @Override
            public Object onTaskBackGround(String key) throws IOException, ResponseException, AuthenticationException {
                return mCouponRemote.getPurchaseCouponResult(purchaseId);
            }

            @Override
            public void onTaskFailed(int key) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onFailed(key);
            }

            @Override
            public void onError(String resultCode, String responseMessage) {
                mbsDataProvider.needLoading(false);
                mbsDataProvider.onError(resultCode, responseMessage);
            }

            @Override
            public void onTaskPostExecute(String key, CouponPurchaseResult result) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "getPurchaseCouponResult: onTaskPostExecute: result=" + result);
                }

                mbsDataProvider.needLoading(false);
                mbsDataProvider.onSuccess(key, result);
            }

            @Override
            public void onPreExecute() {
                mbsDataProvider.needLoading(true);
            }

            @Override
            public Object onCanceled(String key) {
                mbsDataProvider.needLoading(false);
                return null;
            }
        });
        mMbsDataTask.execute(purchaseId);
        return mMbsDataTask;
    }
}
