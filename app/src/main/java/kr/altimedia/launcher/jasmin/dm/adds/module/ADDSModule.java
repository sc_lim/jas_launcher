package kr.altimedia.launcher.jasmin.dm.adds.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import kr.altimedia.launcher.jasmin.dm.adds.api.ADDSApi;
import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mc.kim on 04,08,2020
 */
public class ADDSModule {
    public Retrofit provideAddsModule(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(ServerConfig.AD_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        return builder.create();
    }

    @Singleton
    public ADDSApi provideAddsApi(@Named("ADDS") Retrofit retrofit) {
        return retrofit.create(ADDSApi.class);
    }
}
