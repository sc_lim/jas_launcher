/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;

/**
 * Created by mc.kim on 11,05,2020
 */
public class FavoriteChannelResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private FavoriteChannelResult favoriteChannelResult;

    protected FavoriteChannelResponse(Parcel in) {
        super(in);
        favoriteChannelResult = in.readTypedObject(FavoriteChannelResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(favoriteChannelResult, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FavoriteChannelResponse> CREATOR = new Creator<FavoriteChannelResponse>() {
        @Override
        public FavoriteChannelResponse createFromParcel(Parcel in) {
            return new FavoriteChannelResponse(in);
        }

        @Override
        public FavoriteChannelResponse[] newArray(int size) {
            return new FavoriteChannelResponse[size];
        }
    };

    public List<String> getFavoriteChannelResult() {
        return favoriteChannelResult.channelIdList;
    }

    @Override
    public String toString() {
        return "FavoriteChannelResponse{" +
                "FavoriteChannelResult=" + favoriteChannelResult +
                '}';
    }

    private static class FavoriteChannelResult implements Parcelable {
        @Expose
        @SerializedName("channelId")
        private List<String> channelIdList;

        protected FavoriteChannelResult(Parcel in) {
            channelIdList = in.readArrayList(Long.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(channelIdList);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<FavoriteChannelResult> CREATOR = new Creator<FavoriteChannelResult>() {
            @Override
            public FavoriteChannelResult createFromParcel(Parcel in) {
                return new FavoriteChannelResult(in);
            }

            @Override
            public FavoriteChannelResult[] newArray(int size) {
                return new FavoriteChannelResult[size];
            }
        };

        @Override
        public String toString() {
            return "FavoriteChannelResult{" +
                    "channelIdList=" + channelIdList +
                    '}';
        }
    }
}
