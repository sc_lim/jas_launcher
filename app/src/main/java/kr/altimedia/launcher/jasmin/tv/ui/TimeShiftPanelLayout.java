/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.util.Log;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

/**
 * Created by mc.kim on 15,07,2020
 */
public class TimeShiftPanelLayout extends LinearLayout {
    final ItemBridgeAdapter mBridgeAdapter = new ItemBridgeAdapter();
    private final String TAG = TimeShiftPanelLayout.class.getSimpleName();
    private final int VISIBLE_COUNT = 7;
    private TextView channelNum = null;
    private TextView channelName = null;
    private ImageIndicator topIndicator = null;
    private ImageIndicator bottomIndicator = null;
    private VerticalGridView mMenuList;
    private ArrayObjectAdapter itemRowAdapter = null;

    public TimeShiftPanelLayout(Context context) {
        super(context);
        initView(context);
    }

    public TimeShiftPanelLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public TimeShiftPanelLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private ChannelDataManager mChannelDataManager;
    private ProgramDataManager mProgramDataManager;

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.layout_timeshift_side_panel, this, true);
        channelNum = findViewById(R.id.channelNum);
        channelName = findViewById(R.id.channelName);

        topIndicator = findViewById(R.id.topIndicator);
        bottomIndicator = findViewById(R.id.bottomIndicator);

        mMenuList = findViewById(R.id.menuList);
//        ProgramRowDecoration programRowDecoration = new ProgramRowDecoration(context);
//        programRowDecoration.setDrawable(getResources().getDrawable(R.drawable.line_timeshift_divider, null));
//        mMenuList.addItemDecoration(programRowDecoration);
        mChannelDataManager = TvSingletons.getSingletons(context).getChannelDataManager();
        mProgramDataManager = TvSingletons.getSingletons(context).getProgramDataManager();


        initialPresenterSelector();


        mMenuList.setOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {
            @Override
            public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                super.onChildViewHolderSelected(parent, child, position, subposition);
                topIndicator.updateArrow(position);
                bottomIndicator.updateArrow(position);
            }
        });


    }


    private void initialPresenterSelector() {
        ClassPresenterSelector presenterSelector = new ClassPresenterSelector();
        presenterSelector.addClassPresenter(TimeShiftProgramRow.class, new TimeShiftProgramPresenter());
        mBridgeAdapter.setPresenter(presenterSelector);
        itemRowAdapter = new ArrayObjectAdapter();
    }

    public void notifyDataChanged() {
        if (mBridgeAdapter != null) {
            mBridgeAdapter.notifyDataSetChanged();
        }
    }

    public void renewalPanelData(Channel channel, Program program, TimeShiftProgramKeyListener mTimeShiftProgramKeyListener) {
        channelNum.setText(String.format("%03d", Integer.parseInt(channel.getDisplayNumber())));
        channelName.setText(channel.getDisplayName());
        if (itemRowAdapter != null) {
            itemRowAdapter.clear();
        }
        long timeShiftEnableTime = channel.getTimeshiftService();
        List<Program> programList = mProgramDataManager.getPreviousPrograms(channel.getId(),
                JasmineEpgApplication.SystemClock().currentTimeMillis(), JasmineEpgApplication.SystemClock().currentTimeMillis() - timeShiftEnableTime);
        if (Log.INCLUDE) {
            Log.d(TAG, "programList size : " + programList.size());
        }
        int index = 0;
        for (int i = programList.size() - 1; i >= 0; i--) {
            Program programItem = programList.get(i);
            if (Log.INCLUDE) {
                Log.d(TAG, "programItem : " + programItem);
            }
            if (programItem.getId() == program.getId()) {
                index = i;
            }
            itemRowAdapter.add(new TimeShiftProgramRow(programItem, program, mTimeShiftProgramKeyListener));
        }
        mBridgeAdapter.setAdapter(itemRowAdapter);
        mMenuList.setAdapter(mBridgeAdapter);

        mMenuList.setItemAlignmentOffset(0);
        mMenuList.setWindowAlignmentOffset(0);
        mMenuList.setWindowAlignmentOffsetPercent(93f);

        topIndicator.initIndicator(itemRowAdapter.size(), VISIBLE_COUNT);
        bottomIndicator.initIndicator(itemRowAdapter.size(), VISIBLE_COUNT);


        mMenuList.setSelectedPosition((programList.size() - 1) - index);

    }

    private static class ProgramRowDecoration extends RecyclerView.ItemDecoration {
        private Drawable divider;
        private final Context mContext;

        public ProgramRowDecoration(Context mContext) {
            this.mContext = mContext;
        }

        public void setDrawable(Drawable divider) {
            this.divider = divider;
        }

        @Override
        public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth();

            Paint paint = new Paint();
            paint.setColor(mContext.getResources().getColor(R.color.color_17FFFFFF, null));

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount - 1; i++) {
                View child = parent.getChildAt(i);

                int top = child.getBottom();
                int bottom = top + divider.getIntrinsicHeight();

                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        }
    }

    interface TimeShiftProgramKeyListener {
        void onItemSelected(Program program);

        void onBackPressed(Program program);

        boolean isLocked(Program program);

        boolean isPlayingProgram(Program program);
    }
}
