/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.playback;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.TrailerDataProvider;

/**
 * Created by mc.kim on 06,05,2020
 */
public class TrailerObjectAdapter extends ArrayObjectAdapter {
    private final String TAG = ThumbnailObjectAdapter.class.getSimpleName();
    private TrailerDataProvider mTrailerDataProvider = null;

    public TrailerObjectAdapter(Presenter presenter) {
        super(presenter);
    }

    public TrailerObjectAdapter(Presenter presenter, TrailerDataProvider trailerDataProvider) {
        super(presenter);
        this.mTrailerDataProvider = trailerDataProvider;
    }


    @Override
    public int size() {
        if (mTrailerDataProvider != null) {
            List<StreamInfo> seekPositions = mTrailerDataProvider.getTrailerDataList();
            return seekPositions.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object get(int position) {
        List<StreamInfo> seekPositions = mTrailerDataProvider.getTrailerDataList();
        if (seekPositions.size() > position) {
            return seekPositions.get(position);
        } else {
            return -1L;
        }
    }
}
