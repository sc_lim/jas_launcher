package kr.altimedia.launcher.jasmin.dm.payment.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.def.PaymentTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class PaymentPromotion implements Parcelable {
    @Expose
    @SerializedName("paymentMethod")
    @JsonAdapter(PaymentTypeDeserializer.class)
    private PaymentType paymentType;
    @Expose
    @SerializedName("promotionDescription")
    private String promotionDescription;

    protected PaymentPromotion(Parcel in) {
        paymentType = PaymentType.findByName(in.readString());
        promotionDescription = in.readString();
    }

    public static final Creator<PaymentPromotion> CREATOR = new Creator<PaymentPromotion>() {
        @Override
        public PaymentPromotion createFromParcel(Parcel in) {
            return new PaymentPromotion(in);
        }

        @Override
        public PaymentPromotion[] newArray(int size) {
            return new PaymentPromotion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(paymentType.getCode());
        dest.writeString(promotionDescription);
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public String getPromotionDescription() {
        return promotionDescription;
    }
}
