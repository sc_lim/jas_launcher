/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;

import androidx.annotation.NonNull;

import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object.SideOptionCategory;

/**
 * Created by mc.kim on 06,04,2020
 */
public class SideOptionSectionRow extends SideOptionRow {
    private final SideOptionCategory mSideOptionCategory;

    public SideOptionSectionRow(@NonNull SideOptionCategory sideOptionCategory) {
        this.mSideOptionCategory = sideOptionCategory;
    }

    public SideOptionCategory getOptionType() {
        return mSideOptionCategory;
    }
}
