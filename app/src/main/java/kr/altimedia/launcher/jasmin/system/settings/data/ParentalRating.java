/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.system.settings.data;

import android.content.Context;

import kr.altimedia.launcher.jasmin.R;

public enum ParentalRating {
    LIMIT_3(0, R.string.rating_3, R.string.rating_3_title, 3),
    LIMIT_6(1, R.string.rating_6, R.string.rating_6_title, 6),
    LIMIT_13(2, R.string.rating_13, R.string.rating_13_title, 13),
    LIMIT_18(3, R.string.rating_18, R.string.rating_18_title, 18),
    NO_LIMIT(4, R.string.rating_no_limit, R.string.rating_no_limit_title, 0);

    private int type;
    private int value;
    private int title;
    private int rating;

    ParentalRating(int type, int value, int title, int rating) {
        this.type = type;
        this.value = value;
        this.title = title;
        this.rating = rating;
    }

    public int getType() {
        return type;
    }

    public int getValue() {
        return value;
    }

    public int getTitle() {
        return title;
    }

    public int getRating() {
        return rating;
    }

    public static ParentalRating findByName(String rating) {
        ParentalRating[] types = values();

        for (ParentalRating parentalRating : types) {
            String type = parentalRating.toString();
            if (type.equalsIgnoreCase(rating)) {
                return parentalRating;
            }
        }

        return NO_LIMIT;
    }

    public static String findByRating(Context context, int rating) {
        ParentalRating[] types = values();

        for (ParentalRating parentalRating : types) {
            int ratingValue = parentalRating.getRating();
            if (rating == ratingValue) {
                return context.getString(parentalRating.getValue());
            }
        }

        return context.getString(NO_LIMIT.getValue());
    }
}
