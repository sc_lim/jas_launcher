/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class CouponPresenter extends Presenter {
    private final int[] COUPON_BGs = new int[]{R.drawable.coupon_s_bg1, R.drawable.coupon_s_bg2, R.drawable.coupon_s_bg3, R.drawable.coupon_s_bg4};
    private final int UNLIMIT_YEAR = 1999;
    private final int UNLIMIT_MONTH = 12;
    private final int UNLIMIT_DAY = 31;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_coupon, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        DiscountCoupon coupon = (DiscountCoupon) item;
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData(coupon);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public class ViewHolder extends Presenter.ViewHolder {
        private static final String COUPON_DATE_FORMAT = "dd.MM.yyyy";

        private LinearLayout couponLayout;
        private TextView name;
        private TextView value;
        private TextView expire;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            name = view.findViewById(R.id.name);
            value = view.findViewById(R.id.value);
            expire = view.findViewById(R.id.expire);
            couponLayout = view.findViewById(R.id.layout);
        }

        private void setData(DiscountCoupon coupon) {
            int index = (int) view.getTag(R.id.KEY_INDEX) % 4;
            int bg = COUPON_BGs[index];
            couponLayout.setBackgroundResource(bg);

            name.setText(coupon.getName());
            value.setText(String.valueOf(coupon.getRate()));

            Calendar cal = Calendar.getInstance();
            cal.setTime(coupon.getExpireDate());
            String end = (cal.get(Calendar.YEAR) == UNLIMIT_YEAR && (cal.get(Calendar.MONTH) + 1) == UNLIMIT_MONTH && cal.get(Calendar.DAY_OF_MONTH) == UNLIMIT_DAY) ?
                    view.getContext().getString(R.string.unlimited) : view.getContext().getString(R.string.until) + " " + TimeUtil.getModifiedDate(coupon.getExpireDate().getTime(), COUPON_DATE_FORMAT);
            expire.setText(end);
        }

        public void setOnFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
            view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    view.setSelected(hasFocus);
                    onFocusChangeListener.onFocusChange(v, hasFocus);
                }
            });
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            view.setOnClickListener(onClickListener);
        }
    }
}
