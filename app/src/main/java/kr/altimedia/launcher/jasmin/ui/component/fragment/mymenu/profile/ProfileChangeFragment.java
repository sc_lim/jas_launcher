/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.altimedia.util.Log;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfile;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class ProfileChangeFragment extends Fragment implements View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = ProfileChangeFragment.class.getName();
    private static final String TAG = ProfileChangeFragment.class.getSimpleName();

    private final String ERROR_DUPLICATED_NAME = "MBS_1050";

    private TextView profileName;
    private TextView profileError;
    private EditText inputProfileName;
    private TextView btnSave;
    private TextView btnCancel;
    private View rootView;

    private UserProfile profile;
    private String profileNewName = "";
    private final int MAX_TEXT_LENGTH = 16;

    private boolean disableHotKey = false;

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = s.toString();
            onInputTextChanged(text);
        }
    };

    private int getNumber(int keyCode){
        int number = keyCode - KeyEvent.KEYCODE_0;
        return number;
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if(keyCode >= KeyEvent.KEYCODE_0 && keyCode <=KeyEvent.KEYCODE_9){
            if(inputProfileName.isFocused()) {
                if (event.getAction() == KeyEvent.ACTION_UP) {
                    try {
                        int newDigit = getNumber(keyCode);
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onUnhandledKeyEvent: newDigit=" + newDigit);
                        }
                        inputProfileName.append(Integer.toString(newDigit));
                        profileNewName = inputProfileName.getText().toString();
                    }catch (Exception e){
                    }
                }
                return true;
            }
        }

        if(disableHotKey){
            return false;
        }
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @NotNull
    public static ProfileChangeFragment newInstance(UserProfile profile, boolean disableHotKey) {
        ProfileChangeFragment fragment = new ProfileChangeFragment();
        Bundle args = new Bundle();
        args.putParcelable(ProfileManageDialog.KEY_PROFILE, profile);
        args.putBoolean(ProfileManageDialog.KEY_DISABLE_HOT_KEY, disableHotKey);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
        if(inputProfileName != null) {
            showSoftKeyboard(false, inputProfileName);
        }

        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mymenu_profile_change, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        disableHotKey = false;

        Bundle arguments = this.getArguments();
        if (arguments != null) {
            profile = arguments.getParcelable(ProfileManageDialog.KEY_PROFILE);
            disableHotKey = arguments.getBoolean(ProfileManageDialog.KEY_DISABLE_HOT_KEY);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated: profile=" + profile+", disableHotKey="+disableHotKey);
        }

        profileName = view.findViewById(R.id.profileName);
        profileError = view.findViewById(R.id.profileError);

        profileName.setText(profile.getName());

        initButton(view);
        initInputView(view);
    }

    private void initButton(View view){
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeProfile();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ProfileManageDialog) getParentFragment()).backToFragment();
            }
        });
    }

    private void setTextMaxLength(){
        try {
            inputProfileName.setEms(MAX_TEXT_LENGTH);
            InputFilter[] currFilters = inputProfileName.getFilters();
            InputFilter[] newFilters = new InputFilter[currFilters.length + 1];
            System.arraycopy(currFilters, 0, newFilters, 0, currFilters.length);
            newFilters[currFilters.length] = new InputFilter.LengthFilter(MAX_TEXT_LENGTH);
            inputProfileName.setFilters(newFilters);
        }catch (Exception e) {
            inputProfileName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_TEXT_LENGTH)});
        }
    }

    private void initInputView(View view) {
        inputProfileName = view.findViewById(R.id.inputProfileName);
        setTextMaxLength();
        inputProfileName.post(new Runnable() {
            @Override
            public void run() {
                inputProfileName.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                inputProfileName.addTextChangedListener(textWatcher);
                inputProfileName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                        if(Log.INCLUDE){
                            Log.d(TAG, "inputProfileName: onEditorAction: actionId="+actionId+", keyEvent=" + keyEvent);
                        }
                        if ( actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_NONE) {
                            showSoftKeyboard(false, textView);
                            return onInputTextKey(KeyEvent.KEYCODE_DPAD_DOWN);
                        }
                        return true;
                    }
                });
                inputProfileName.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                        if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                            return false;
                        }
                        if(Log.INCLUDE){
                            Log.d(TAG, "inputProfileName: onKey: keyCode="+keyCode+", keyEvent=" + keyEvent);
                        }
                        return onInputTextKey(keyCode);
                    }
                });
                inputProfileName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean focused) {
                        if(focused){
                            showSoftKeyboard(true, inputProfileName);
                        }
                    }
                });

                inputProfileName.requestFocus();
            }
        });
    }

    private void showSoftKeyboard(boolean visible, View view){
        if(visible){
            InputMethodManager imm = (InputMethodManager)view.getContext().getSystemService(INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }else{
            InputMethodManager imm = (InputMethodManager)view.getContext().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }

    private void setButtonFocus(){
        if(Log.INCLUDE){
            Log.d(TAG, "setButtonFocus");
        }
        if (btnSave.isEnabled()) {
            btnSave.requestFocus();
        } else {
            btnCancel.requestFocus();
        }
    }

    public boolean onInputTextKey(int keyCode) {
        if(Log.INCLUDE){
            Log.d(TAG, "onInputTextKey: keyCode="+keyCode);
        }
        if(keyCode == KeyEvent.KEYCODE_DPAD_DOWN){
            setButtonFocus();
            return true;
        }
        return false;
    }

    public void onInputTextChanged(String text) {
        profileNewName = text;
        boolean enable = false;
        if (text != null && !text.isEmpty() && !text.equalsIgnoreCase("")) {
            enable = true;
        }

        if (btnSave.isEnabled() != enable) {
            btnSave.setEnabled(enable);
        }
    }

    public void onInputTextLengthExceeded() {
    }

    private void setInputText(String s){
        profileNewName = s;
        inputProfileName.setText(s);
    }

    private void changeProfile() {
        if (profile == null) {
            return;
        }

        final ProfileInfo profileInfo = profile.getProfileInfo();
        String profileId = profileInfo.getProfileId();
        String profileName = profileInfo.getProfileName();
        String profileIconId = profileInfo.getProfileIconId();
        String profileIconPath = profileInfo.getProfileIconPath();
        if (Log.INCLUDE) {
            Log.d(TAG, "changeProfile: id=" + profileId + ", name=" + profileName + ", icon=" + profileIconId + ", icon path=" + profileIconPath);
        }

        if(profileNewName != null && !profileNewName.isEmpty() && profileNewName.trim().equals(profile.getProfileInfo().getProfileName())){
            profileError.setText(R.string.profile_duplicated_error);
            profileError.setVisibility(View.VISIBLE);

            setInputText("");
            return;
        }

        UserDataManager userDataManager = new UserDataManager();
        userDataManager.putProfile(
                AccountManager.getInstance().getSaId(),
                profileId,
                profileNewName,
                profileIconId,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
//                            showProgress();
                        } else {
//                            hideProgress();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        Context context = ProfileChangeFragment.this.getContext();
                        JasminToast.makeToast(context, R.string.saved);

                        notifyProfileChanged(profileId, profileNewName, profileIconId, profileIconPath);

                        ProfileManageDialog manageDialog = ((ProfileManageDialog) getParentFragment());
                        if(manageDialog.isLinkFromMyMenu()){
                            ((ProfileManageDialog) getParentFragment()).hideProfileView(profileNewName);
                        }else {
                            ((ProfileManageDialog) getParentFragment()).showProfileView(ProfileManageDialog.TYPE_LIST, profile, null);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if(ERROR_DUPLICATED_NAME.equals(errorCode)){
                            profileError.setText(R.string.profile_duplicated_error);
                            profileError.setVisibility(View.VISIBLE);

                            setInputText("");

                        }else {
                            profileError.setVisibility(View.GONE);
                            ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                            errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                        }
                    }
                });
    }

    private void notifyProfileChanged(String id, String name, String iconId, String iconPath){
        Intent intent = new Intent();
        intent.setAction(ProfileManageDialog.ACTION_MY_PROFILE_UPDATED);
        intent.putExtra(ProfileManageDialog.KEY_PROFILE_ID, id);
        intent.putExtra(ProfileManageDialog.KEY_PROFILE_NAME, name);
        intent.putExtra(ProfileManageDialog.KEY_PROFILE_ICON_ID, iconId);
        intent.putExtra(ProfileManageDialog.KEY_PROFILE_ICON_PATH, iconPath);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }
}
