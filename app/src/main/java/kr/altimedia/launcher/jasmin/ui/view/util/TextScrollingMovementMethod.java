package kr.altimedia.launcher.jasmin.ui.view.util;

import android.text.Spannable;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class TextScrollingMovementMethod extends ScrollingMovementMethod {
    public boolean scrollUp(TextView widget, Spannable buffer) {
        return up(widget, buffer);
    }

    public boolean scrollDown(TextView widget, Spannable buffer) {
        return down(widget, buffer);
    }
}
