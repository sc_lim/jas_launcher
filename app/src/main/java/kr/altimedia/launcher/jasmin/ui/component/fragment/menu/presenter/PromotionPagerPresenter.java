/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.media.VideoPlayerAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.page.PromotionVideoFragment;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.ViewPagerPresenter;
import kr.altimedia.launcher.jasmin.ui.view.rowView.PageRowView;
import kr.altimedia.launcher.jasmin.ui.view.verticalRow.VideoSupportVerticalRowFragment;


/**
 * Created by mc.kim on 17,03,2020
 */
public class PromotionPagerPresenter extends ViewPagerPresenter implements VideoSupportVerticalRowFragment.OnVideoEventCallback {
    private final String TAG = PromotionPagerPresenter.class.getSimpleName();
    private final long AUTO_PAGING_TIME_LONG = 3 * 1000;

    public PromotionPagerPresenter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public PromotionPagerPresenter(boolean autoPaging, FragmentManager fragmentManager) {
        super(autoPaging, fragmentManager);
    }

    public PromotionPagerPresenter(boolean autoPaging, int indicatorLayoutId, FragmentManager fragmentManager, OnPromotionChangedListener onPromotionChangedListener) {
        super(autoPaging, indicatorLayoutId, fragmentManager);
        setOnPromotionChangedListener(onPromotionChangedListener);
    }


    @Override
    protected ViewPagerAdapter createPagerAdapter(FragmentManager fragmentManager, int behavior, Context context) {
        return new PromotionPagerAdapter(fragmentManager, behavior, context);
    }

    @Override
    protected PageRootView createPageView(Context context) {
        return new PageRowView(context);
    }

    @Override
    public void onReady(VideoPlayerAdapter adapter) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onReady");
        }
        setAutoPaging(true);
        if (mNextTimerHandler != null) {
            mNextTimerHandler.removeMessages(0);
        }
    }


    @Override
    public void onRelease(VideoPlayerAdapter adapter) {
        if (mNextTimerHandler != null) {
            mNextTimerHandler.removeMessages(0);
        }
    }


    @Override
    protected boolean notifyPromotionKeyEvent(int keyCode) {


        if (Log.INCLUDE) {
            Log.d(TAG, "onPlayCompleted");
        }
        if (mViewPagerChangeListener != null) {
            int currentPosition = mViewPagerChangeListener.getCurrentPosition();
            int size = mViewPagerChangeListener.getSize();

            if (Log.INCLUDE) {
                Log.d(TAG, "currentPosition : " + currentPosition);
                Log.d(TAG, "size : " + size);
            }
            if (size == 1) {
                return true;
            } else {
                final int nextPosition;
                if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                    nextPosition = currentPosition + 1 < size ? currentPosition + 1 : 0;
                } else {
                    nextPosition = currentPosition - 1 >= 0 ? currentPosition - 1 : size - 1;
                }
                mViewPagerChangeListener.setCurrentItem(nextPosition, true);
            }
        }


        return super.notifyPromotionKeyEvent(keyCode);
    }

    @Override
    public void onPlayCompleted(VideoPlayerAdapter adapter) {

        if (Log.INCLUDE) {
            Log.d(TAG, "onPlayCompleted");
        }
        if (mViewPagerChangeListener != null) {
            int currentPosition = mViewPagerChangeListener.getCurrentPosition();
            int size = mViewPagerChangeListener.getSize();

            if (Log.INCLUDE) {
                Log.d(TAG, "currentPosition : " + currentPosition);
                Log.d(TAG, "size : " + size);
            }
            if (size == 1) {
                mViewPagerChangeListener.onVideoComplete(currentPosition);
            } else {
                int nextPosition = currentPosition + 1 < size ? currentPosition + 1 : 0;
                mViewPagerChangeListener.setCurrentItem(nextPosition, true);
            }
        }
    }

    @Override
    public void onPlayStop(VideoPlayerAdapter adapter) {

        if (Log.INCLUDE) {
            Log.d(TAG, "call onPlayStop : " + mAutoPaging);
        }
        if (!mAutoPaging) {
            return;
        }
        if (mNextTimerHandler != null) {
            mNextTimerHandler.removeMessages(0);
            mNextTimerHandler.setAdapter(adapter);
            mNextTimerHandler.sendEmptyMessageDelayed(0, AUTO_PAGING_TIME_LONG);
        }
    }

    @Override
    public void onAttached(VideoPlayerAdapter adapter) {
        super.onStartedPresenter();
        setAutoPaging(true);
    }

    @Override
    public void onDetached(VideoPlayerAdapter adapter) {
        super.onStoppedPresenter();
        setAutoPaging(false);

        if (mNextTimerHandler != null) {
            mNextTimerHandler.removeMessages(0);
        }
    }

    private AutoPagingHandler mNextTimerHandler = null;

    private static class AutoPagingHandler extends Handler {
        protected VideoPlayerAdapter mAdapter = null;

        public void setAdapter(VideoPlayerAdapter mAdapter) {
            this.mAdapter = mAdapter;
        }
    }

    @Override
    protected void onRowViewAttachedToWindow(RowPresenter.ViewHolder vh) {
        super.onRowViewAttachedToWindow(vh);
        if (mNextTimerHandler != null) {
            mNextTimerHandler.removeMessages(0);
            mNextTimerHandler = null;
        }

        mNextTimerHandler = new AutoPagingHandler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                onPlayCompleted(mAdapter);
            }
        };
    }

    @Override
    protected void onRowViewDetachedFromWindow(RowPresenter.ViewHolder vh) {
        super.onRowViewDetachedFromWindow(vh);
        if (mNextTimerHandler != null) {
            mNextTimerHandler.removeMessages(0);
        }
    }

    private class PromotionPagerAdapter extends ViewPagerAdapter {
        private final Context context;
        private ArrayObjectAdapter mItemAdapter = null;

        public PromotionPagerAdapter(@NonNull FragmentManager fm, int behavior, Context context) {
            super(fm, behavior);
            this.context = context;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            return super.instantiateItem(container, position);
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            super.destroyItem(container, position, object);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return PromotionVideoFragment.newInstance((Banner) getDataItem(position));
        }


        @Override
        public int getCount() {
            if (this.mItemAdapter == null) {

                if (Log.INCLUDE) {
                    Log.d(TAG, "getCunt : 0");
                }
                return 0;
            } else {

                if (Log.INCLUDE) {
                    Log.d(TAG, "getCunt : " + this.mItemAdapter.size());
                }
                return this.mItemAdapter.size();
            }
        }

        @Override
        public void setItemAdapter(ArrayObjectAdapter objectAdapter) {
            this.mItemAdapter = objectAdapter;
            notifyDataSetChanged();
        }

        @Override
        public Object getDataItem(int position) {
            return this.mItemAdapter.get(position);
        }
    }
}
