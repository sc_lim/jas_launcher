/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Range;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.util.Log;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.ui.view.guide.listener.OnRowRepeatedKeyInterceptListener;
import kr.altimedia.launcher.jasmin.ui.view.util.GenreUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class ProgramRow extends TimelineGridView {
    private static final String TAG = "ProgramRow";

    private static final long ONE_HOUR_MILLIS = TimeUnit.HOURS.toMillis(1);
    private static final long HALF_HOUR_MILLIS = ONE_HOUR_MILLIS / 2;

    private ProgramGuide mProgramGuide;
    private ProgramManager mProgramManager;

    private boolean mKeepFocusToCurrentProgram;
    private ChildFocusListener mChildFocusListener;

    interface ChildFocusListener {
        /**
         * Is called after focus is moved. Caller should check if old and new focuses are listener's
         * children. See {@code ProgramRow#setChildFocusListener(ChildFocusListener)}.
         */
        void onChildFocus(View oldFocus, View newFocus);
    }

    /**
     * Used only for debugging.
     */
    private Channel mChannel;

    private final OnGlobalLayoutListener mLayoutListener =
            new OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            };

    public ProgramRow(Context context) {
        this(context, null);
    }

    public ProgramRow(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgramRow(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ProgramRowAccessibilityDelegate rowAccessibilityDelegate =
                new ProgramRowAccessibilityDelegate(this);
        this.setAccessibilityDelegateCompat(rowAccessibilityDelegate);
//        DividerItemDecoration dividerItemDecoration
//                = new DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL);
//
//        dividerItemDecoration.setDrawable(context.getResources().getDrawable(R.drawable.line_transparents));
//        this.addItemDecoration(dividerItemDecoration);

    }

    /**
     * Registers a listener focus events occurring on children to the {@code ProgramRow}.
     */
    public void setChildFocusListener(ChildFocusListener childFocusListener) {
        mChildFocusListener = childFocusListener;
    }

    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);
    }

    @Override
    public void onScrolled(int dx, int dy) {
        getViewTreeObserver().removeOnGlobalLayoutListener(mLayoutListener);
        super.onScrolled(dx, dy);
    }

    /**
     * Moves focus to the current program.
     */
    public void focusCurrentProgram() {
        View currentProgram = getCurrentProgramView();
        if (currentProgram == null) {
            currentProgram = getChildAt(0);
        }
        if (mChildFocusListener != null) {
            mChildFocusListener.onChildFocus(null, currentProgram);
        }
    }


    public void forceFocusCurrentProgram() {
        View currentProgram = getCurrentProgramView();
        if (currentProgram == null) {
            currentProgram = getChildAt(0);
        }
//        if (mChildFocusListener != null) {
//            mChildFocusListener.onChildFocus(null, currentProgram);
        if (currentProgram != null && currentProgram.isAttachedToWindow()) {
            currentProgram.requestFocus();
            mKeepFocusToCurrentProgram = false;
        }
//        }
    }

    public void forceFocusProgram() {
        View currentProgram = getChildAt(0);
        if (currentProgram != null && currentProgram.isAttachedToWindow()) {
            currentProgram.requestFocus();
            mKeepFocusToCurrentProgram = false;
        }
    }

    public void makeKeepFocusToCurrentProgram() {
        mKeepFocusToCurrentProgram = true;
        View currentProgram = getCurrentProgramView();
        if (currentProgram == null) {
            currentProgram = getChildAt(0);
        }
        if (currentProgram != null && currentProgram.isAttachedToWindow()) {
            currentProgram.requestFocus();
            mKeepFocusToCurrentProgram = false;
        }

    }

    public void makeKeepFocusToProgram() {
        mKeepFocusToCurrentProgram = true;
        View currentProgram = getChildAt(0);
        if (currentProgram != null && currentProgram.isAttachedToWindow()) {
            currentProgram.requestFocus();
            mKeepFocusToCurrentProgram = false;
        }

    }

    // Call this API after RTL is resolved. (i.e. View is measured.)
    private boolean isDirectionStart(int direction) {
        return getLayoutDirection() == LAYOUT_DIRECTION_LTR
                ? direction == View.FOCUS_LEFT
                : direction == View.FOCUS_RIGHT;
    }

    // Call this API after RTL is resolved. (i.e. View is measured.)
    private boolean isDirectionEnd(int direction) {
        return getLayoutDirection() == LAYOUT_DIRECTION_LTR
                ? direction == View.FOCUS_RIGHT
                : direction == View.FOCUS_LEFT;
    }

    // When Accessibility is enabled, this API will keep next node visible
    void focusSearchAccessibility(View focused, int direction) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call focusSearchAccessibility");
        }
        if (focused instanceof ProgramAudioItemView) {
            ProgramManager.TableEntry focusedEntry = ((ProgramAudioItemView) focused).getTableEntry();
            long toMillis = mProgramManager.getToUtcMillis();

            if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
                if (focusedEntry.entryEndUtcMillis >= toMillis) {
                    scrollByTime(focusedEntry.entryEndUtcMillis - toMillis + HALF_HOUR_MILLIS);
                }
            }
        } else {
            ProgramManager.TableEntry focusedEntry = ((ProgramItemView) focused).getTableEntry();
            long toMillis = mProgramManager.getToUtcMillis();

            if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
                if (focusedEntry.entryEndUtcMillis >= toMillis) {
                    scrollByTime(focusedEntry.entryEndUtcMillis - toMillis + HALF_HOUR_MILLIS);
                }
            }
        }


    }

    private View focusLeftView(View view) {
        return mProgramGuide.focusLeftView(view);
    }

    private final long SCROLL_DIFFER = TimeUtil.HOUR + (TimeUtil.MIN * 30);

    private class OnScrollListenerImpl extends OnScrollListener {
        private View mTargetView;
        private int mDirection;

        public void setTargetView(View targetView, int direction) {
            this.mTargetView = targetView;
            this.mDirection = direction;
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (Log.INCLUDE) {
                Log.d(TAG, "onScrolled");
            }
            View targetView = focusSearch(mTargetView, mDirection);
            if (targetView != null) {
                targetView.requestFocus();
            }
        }
    }

    private OnScrollListenerImpl mOnScrollListenerImpl = new OnScrollListenerImpl();

    @Override
    public View focusSearch(View focused, int direction) {
        if (Log.INCLUDE) {
            Log.d(TAG, "call focusSearch : ");
        }
        removeOnScrollListener(mOnScrollListenerImpl);
        int lastLineUp = mProgramGuide.getSelectedGenreId();
        if (lastLineUp == GenreUtil.getId(GenreUtil.AUDIO_CHANNELS)) {
            if ((isDirectionStart(direction) || direction == View.FOCUS_BACKWARD)) {
                return focusLeftView(focused);
            } else if ((isDirectionEnd(direction) || direction == View.FOCUS_FORWARD)) {
                return focused;
            }
        }
        if (mProgramGuide.isKeyBlock()) {
            return focused;
        }

        final ProgramManager.TableEntry focusedEntry;
        if (focused instanceof ProgramAudioItemView) {
            focusedEntry = ((ProgramAudioItemView) focused).getTableEntry();
        } else {
            focusedEntry = ((ProgramItemView) focused).getTableEntry();
        }
        long fromMillis = mProgramManager.getFromUtcMillis();
        long toMillis = mProgramManager.getToUtcMillis();

        if (Log.INCLUDE) {
            Log.d(TAG, "focusSearch focus fromMillis : " + new Date(fromMillis));
            Log.d(TAG, "focusSearch focus toMillis : " + new Date(toMillis));
            Log.d(TAG, "focusSearch focusedEntry.entryStartUtcMillis : " + new Date(focusedEntry.entryStartUtcMillis));
            Log.d(TAG, "focusSearch focusedEntry.entryEndUtcMillis : " + new Date(focusedEntry.entryEndUtcMillis));
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "focusSearch (isDirectionStart(direction) : " + (isDirectionStart(direction)));
            Log.d(TAG, "focusSearch (isDirectionEnd(direction) : " + (isDirectionEnd(direction)));
            Log.d(TAG, "focusSearch direction == View.FOCUS_BACKWARD : " + (direction == View.FOCUS_BACKWARD));
            Log.d(TAG, "focusSearch direction == View.FOCUS_FORWARD : " + (direction == View.FOCUS_FORWARD));
        }
        View target = super.focusSearch(focused, direction);
        if (!(target instanceof ProgramItemView) && !(target instanceof ProgramAudioItemView)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "not target instanceof ProgramItemView : " + target);
            }
            if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
                if (focusedEntry.entryEndUtcMillis != mProgramManager.getEndUtcMillis()) {
                    // The focused entry is the last entry; Align to the right edge.
//                    scrollByTime(focusedEntry.entryEndUtcMillis - toMillis);
                    mOnScrollListenerImpl.setTargetView(focused, direction);
                    addOnScrollListener(mOnScrollListenerImpl);
                    scrollByTime(SCROLL_DIFFER);
                    return focused;
                }
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "not programItemView");
            }
            if (direction == View.FOCUS_LEFT) {
                if (focusedEntry.entryStartUtcMillis
                        == mProgramManager.getStartUtcMillis()) {
                    return focusLeftView(target);
                } else {
                    mOnScrollListenerImpl.setTargetView(focused, direction);
                    addOnScrollListener(mOnScrollListenerImpl);
                    scrollByTime(-SCROLL_DIFFER);
                    return focused;
                }
            }
            return target;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "(isDirectionStart(direction) || direction == View.FOCUS_BACKWARD) : " + (isDirectionStart(direction) || direction == View.FOCUS_BACKWARD));
            Log.d(TAG, "(isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) : " + (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD));
        }
        if ((isDirectionStart(direction) || direction == View.FOCUS_BACKWARD)) {

            if (focusedEntry.entryStartUtcMillis < fromMillis) {
                // The current entry starts outside of the view; Align or scroll to the left.
                scrollByTime(-SCROLL_DIFFER);
                if (Log.INCLUDE) {
                    Log.d(TAG, "focusSearch focus remain left | focusedEntry.entryStartUtcMillis < fromMillis");
                }
                return focused;
            } else if (focusedEntry.entryStartUtcMillis == fromMillis) {
                scrollByTime(-SCROLL_DIFFER);
                if (Log.INCLUDE) {
                    Log.d(TAG, "focusSearch focus remain left | focusedEntry.entryStartUtcMillis == fromMillis");
                }
                return target;
            } else if (focusedEntry.entryStartUtcMillis > fromMillis) {
                return target;
            }
        } else if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
            if (focusedEntry.entryEndUtcMillis > toMillis) {
                // The current entry ends outside of the view; Scroll to the right.
                scrollByTime(SCROLL_DIFFER);
                if (Log.INCLUDE) {
                    Log.d(TAG, "focusSearch focus remain right | focusedEntry.entryEndUtcMillis > toMillis");
                }
                return focused;
            } else if (focusedEntry.entryEndUtcMillis == toMillis) {
                scrollByTime(SCROLL_DIFFER);
                if (Log.INCLUDE) {
                    Log.d(TAG, "focusSearch focus remain right | focusedEntry.entryEndUtcMillis == toMillis");
                }
                return target;
            } else if (focusedEntry.entryEndUtcMillis < toMillis) {
                return target;
            }
        }


//        ProgramManager.TableEntry targetEntry = ((ProgramItemView) target).getTableEntry();
//
//        if (isDirectionStart(direction) || direction == View.FOCUS_BACKWARD) {
//            if (mProgramGuide.isAccessibilityEnabled()) {
//                scrollByTime(targetEntry.entryStartUtcMillis - fromMillis);
//            } else if (targetEntry.entryStartUtcMillis < fromMillis
//                    && targetEntry.entryEndUtcMillis < fromMillis + HALF_HOUR_MILLIS) {
//                // The target entry starts outside the view; Align or scroll to the left.
////                scrollByTime(
////                        Math.max(-ONE_HOUR_MILLIS, targetEntry.entryStartUtcMillis - fromMillis));
//                scrollByTime(targetEntry.entryStartUtcMillis - fromMillis - HALF_HOUR_MILLIS);
//            }
//        } else if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
//            if (targetEntry.entryStartUtcMillis > fromMillis + ONE_HOUR_MILLIS) {
//                // The target entry starts outside the view; Align or scroll to the right.
////                scrollByTime(
////                        Math.min(
////                                ONE_HOUR_MILLIS,
////                                targetEntry.entryStartUtcMillis - fromMillis - ONE_HOUR_MILLIS));
//                scrollByTime(targetEntry.entryStartUtcMillis - fromMillis - HALF_HOUR_MILLIS);
//            }
//        }
//        if (Log.INCLUDE) {
//            Log.d(TAG, "focusSearch focus target");
//        }


        if ((isDirectionStart(direction) || direction == View.FOCUS_BACKWARD)) {
            return focused;
        } else if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
            return focused;
        } else {
            return target;
        }
    }

//    @Override
//    public View focusSearch(View focused, int direction) {
//        if (Log.INCLUDE) {
//            Log.d(TAG, "call focusSearch : ");
//        }
//
//        ProgramManager.TableEntry focusedEntry = ((ProgramItemView) focused).getTableEntry();
//        long fromMillis = mProgramManager.getFromUtcMillis();
//        long toMillis = mProgramManager.getToUtcMillis();
//
//        if (Log.INCLUDE) {
//            Log.d(TAG, "focusSearch focus fromMillis : " + fromMillis);
//            Log.d(TAG, "focusSearch focus toMillis : " + toMillis);
//            Log.d(TAG, "focusSearch focusedEntry.entryStartUtcMillis : " + focusedEntry.entryStartUtcMillis);
//            Log.d(TAG, "focusSearch focusedEntry.entryEndUtcMillis : " + focusedEntry.entryEndUtcMillis);
//        }
//
//        if (Log.INCLUDE) {
//            Log.d(TAG, "focusSearch (isDirectionStart(direction) : " + (isDirectionStart(direction)));
//            Log.d(TAG, "focusSearch (isDirectionEnd(direction) : " + (isDirectionEnd(direction)));
//            Log.d(TAG, "focusSearch direction == View.FOCUS_BACKWARD : " + (direction == View.FOCUS_BACKWARD));
//            Log.d(TAG, "focusSearch direction == View.FOCUS_FORWARD : " + (direction == View.FOCUS_FORWARD));
//        }
//        if (/*!mProgramGuide.isAccessibilityEnabled()
//                && */(isDirectionStart(direction) || direction == View.FOCUS_BACKWARD)) {
//
//            if (focusedEntry.entryStartUtcMillis < fromMillis) {
//                // The current entry starts outside of the view; Align or scroll to the left.
//                scrollByTime(
//                        Math.min(-ONE_HOUR_MILLIS, focusedEntry.entryStartUtcMillis - fromMillis));
//                if (Log.INCLUDE) {
//                    Log.d(TAG, "focusSearch focus remain left");
//                }
//                return focused;
//            }
//        } else if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
//            if (focusedEntry.entryEndUtcMillis >= toMillis) {
//                // The current entry ends outside of the view; Scroll to the right.
//                if (focusedEntry.isBlocked()) {
//                    return focused;
//                }
//                scrollByTime(
//                        Math.max(ONE_HOUR_MILLIS, focusedEntry.entryEndUtcMillis - toMillis));
//                if (Log.INCLUDE) {
//                    Log.d(TAG, "focusSearch focus remain right");
//                }
//                return focused;
//            }
//        }
//
//        View target = super.focusSearch(focused, direction);
//        if (!(target instanceof ProgramItemView)) {
//            if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
//                if (focusedEntry.entryEndUtcMillis != toMillis) {
//                    // The focused entry is the last entry; Align to the right edge.
//                    scrollByTime(focusedEntry.entryEndUtcMillis - toMillis);
//                    return focused;
//                }
//            }
//            if (Log.INCLUDE) {
//                Log.d(TAG, "not programItemView");
//            }
//            if (direction == View.FOCUS_LEFT) {
//                return focusLeftView(target);
//            }
//            return target;
//        }
//
//        ProgramManager.TableEntry targetEntry = ((ProgramItemView) target).getTableEntry();
//
//        if (isDirectionStart(direction) || direction == View.FOCUS_BACKWARD) {
//            if (mProgramGuide.isAccessibilityEnabled()) {
//                scrollByTime(targetEntry.entryStartUtcMillis - fromMillis);
//            } else if (targetEntry.entryStartUtcMillis < fromMillis
//                    && targetEntry.entryEndUtcMillis < fromMillis + HALF_HOUR_MILLIS) {
//                scrollByTime(targetEntry.entryStartUtcMillis - fromMillis - HALF_HOUR_MILLIS);
//            }
//        } else if (isDirectionEnd(direction) || direction == View.FOCUS_FORWARD) {
//            if (targetEntry.entryStartUtcMillis > fromMillis + ONE_HOUR_MILLIS) {
//                scrollByTime(targetEntry.entryStartUtcMillis - fromMillis - HALF_HOUR_MILLIS);
//            }
//        }
//        if (Log.INCLUDE) {
//            Log.d(TAG, "focusSearch focus target");
//        }
//        return target;
//    }


    private void scrollByTime(long timeToScroll) {
        if (Log.INCLUDE) {
            Log.d(
                    TAG,
                    "scrollByTime(timeToScroll="
                            + TimeUnit.MILLISECONDS.toMinutes(timeToScroll)
                            + "min)");
        }
        mProgramManager.shiftTime(timeToScroll, false);
    }

    @Override
    public void onChildDetachedFromWindow(View child) {
//        if (child.hasFocus()) {
//            // Focused view can be detached only if it's updated.
//            ProgramManager.TableEntry entry = ((ProgramItemView) child).getTableEntry();
//            if (entry.program == null) {
//                // The focus is lost due to information loaded. Requests focus immediately.
//                // (Because this entry is detached after real entries attached, we can't take
//                // the below approach to resume focus on entry being attached.)
//                post(
//                        new Runnable() {
//                            @Override
//                            public void run() {
//                                requestFocus();
//                            }
//                        });
//            } else if (entry.isCurrentProgram()) {
//                if (Log.INCLUDE) {
//                    Log.d(TAG, "Keep focus to the current program");
//                }
//                // Current program is visible in the guide.
//                // Updated entries including current program's will be attached again soon
//                // so give focus back in onChildAttachedToWindow().
////                mKeepFocusToCurrentProgram = true;
//            }
//        }
        mKeepFocusToCurrentProgram = false;
        super.onChildDetachedFromWindow(child);
    }

    @Override
    public void onChildAttachedToWindow(View child) {
        super.onChildAttachedToWindow(child);
        if (Log.INCLUDE) {
            Log.d(TAG, "mKeepFocusToCurrentProgram : " + mKeepFocusToCurrentProgram);
        }
        if (mKeepFocusToCurrentProgram) {
            final ProgramManager.TableEntry entry;

            if (child instanceof ProgramAudioItemView) {
                entry = ((ProgramAudioItemView) child).getTableEntry();
            } else {
                entry = ((ProgramItemView) child).getTableEntry();
            }
            if (entry.isCurrentProgram()) {
                mKeepFocusToCurrentProgram = false;
                post(
                        new Runnable() {
                            @Override
                            public void run() {
                                requestFocus();
                            }
                        });
            }
        }
    }

    @Override
    public boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onRequestFocusInDescendants");
        }
        ProgramGrid programGrid = mProgramGuide.getProgramGrid();

        // Give focus according to the previous focused range
        Range<Integer> focusRange = programGrid.getFocusRange();
        View nextFocus =
                GuideUtils.findNextFocusedProgram(
                        this,
                        focusRange.getLower(),
                        focusRange.getUpper(),
                        programGrid.isKeepCurrentProgramFocused());

        if (nextFocus != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onRequestFocusInDescendants nextFocus");
            }
            return nextFocus.requestFocus();
        }


        boolean result = super.onRequestFocusInDescendants(direction, previouslyFocusedRect);
        if (!result) {
            for (int i = 0; i < getChildCount(); ++i) {
                View child = getChildAt(i);
                if (child.isShown() && child.hasFocusable()) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onRequestFocusInDescendants isShown");
                    }
                    return child.requestFocus();
                }
            }
        }
        return result;
    }

    private View getCurrentProgramView() {
        Program currentPlayingProgram = mProgramGuide.findCurrentProgram();
        for (int i = 0; i < getChildCount(); ++i) {
            View targetView = getChildAt(i);
            if (targetView instanceof ProgramAudioItemView) {
                ProgramManager.TableEntry entry = ((ProgramAudioItemView) targetView).getTableEntry();

                if (entry != null && (currentPlayingProgram == null ? entry.isCurrentProgram() : entry.getId() == currentPlayingProgram.getId())) {
                    return getChildAt(i);
                }
            } else {
                ProgramManager.TableEntry entry = ((ProgramItemView) targetView).getTableEntry();

                if (entry != null && (currentPlayingProgram == null ? entry.isCurrentProgram() : entry.getId() == currentPlayingProgram.getId())) {
                    return getChildAt(i);
                }
            }

        }
        return null;
    }

    public void setChannel(Channel channel) {
        mChannel = channel;
    }

    /**
     * Sets the instance of {@link ProgramGuide}
     */
    public void setProgramGuide(ProgramGuide programGuide) {
        mProgramGuide = programGuide;
        mProgramManager = programGuide.getProgramManager();

        OnRowRepeatedKeyInterceptListener mOnRowRepeatedKeyInterceptListener = new OnRowRepeatedKeyInterceptListener(programGuide);
        setOnKeyInterceptListener(mOnRowRepeatedKeyInterceptListener);
    }

    /**
     * Resets the scroll with the initial offset {@code scrollOffset}.
     */
    public void resetScroll(int scrollOffset) {
        long startTime =
                GuideUtils.convertPixelToMillis(scrollOffset) + mProgramManager.getStartTime();
        int position =
                mChannel == null
                        ? -1
                        : mProgramManager.getProgramIndexAtTime(mChannel.getId(), startTime);
        if (position < 0) {
            getLayoutManager().scrollToPosition(0);
        } else {
            ProgramManager.TableEntry entry = mProgramManager.getTableEntry(mChannel.getId(), position);
            int offset =
                    GuideUtils.convertMillisToPixel(
                            mProgramManager.getStartTime(), entry.entryStartUtcMillis)
                            - scrollOffset;
            ((LinearLayoutManager) getLayoutManager()).scrollToPositionWithOffset(position, offset);
            getViewTreeObserver().addOnGlobalLayoutListener(mLayoutListener);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setOnKeyInterceptListener(null);
    }
}
