/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;

import com.altimedia.util.Log;

import java.util.List;

import androidx.annotation.CallSuper;
import androidx.leanback.widget.Action;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnActionClickedListener;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.media.VideoPlayerAdapter;
import kr.altimedia.launcher.jasmin.media.VideoState;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow;

import static kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackTransportControlGlue.MSG_HIDE_CONTROL_STATE;

/**
 * Created by mc.kim on 06,02,2020
 */
public abstract class VideoPlaybackBaseControlGlue<T extends VideoPlayerAdapter> extends VideoPlaybackGlue
        implements OnActionClickedListener, View.OnKeyListener {

    /**
     * The adapter key for the first custom control on the left side
     * of the predefined primary controls.
     */
    public static final int ACTION_CUSTOM_LEFT_FIRST = 0x1;

    /**
     * The adapter key for the skip to previous control.
     */
    public static final int ACTION_SKIP_TO_PREVIOUS = 0x10;

    /**
     * The adapter key for the rewind control.
     */
    public static final int ACTION_REWIND = 0x20;

    /**
     * The adapter key for the play/pause control.
     */
    public static final int ACTION_PLAY_PAUSE = 0x40;

    /**
     * The adapter key for the fast forward control.
     */
    public static final int ACTION_FAST_FORWARD = 0x80;

    /**
     * The adapter key for the skip to next control.
     */
    public static final int ACTION_SKIP_TO_NEXT = 0x100;

    /**
     * The adapter key for the repeat control.
     */
    public static final int ACTION_REPEAT = 0x200;

    /**
     * The adapter key for the shuffle control.
     */
    public static final int ACTION_SHUFFLE = 0x400;

    /**
     * The adapter key for the first custom control on the right side
     * of the predefined primary controls.
     */
    public static final int ACTION_CUSTOM_RIGHT_FIRST = 0x1000;

    static final String TAG = "VideoPlaybackBaseControlGlue";
    static final boolean DEBUG = Log.INCLUDE;

    final T mPlayerAdapter;
    VideoPlaybackControlsRow mControlsRow;
    VideoPlaybackRowPresenter mControlsRowPresenter;
    VideoPlaybackControlsRow.PlayPauseAction mPlayPauseAction;
    protected boolean mIsPlaying = false;
    boolean mFadeWhenPlaying = true;

    CharSequence mSubtitle;
    CharSequence mTitle;
    Drawable mCover;

    VideoPlaybackGlueHost.PlayerCallback mPlayerCallback;
    boolean mBuffering = false;
    int mVideoWidth = 0;
    int mVideoHeight = 0;
    boolean mErrorSet = false;
    int mErrorCode;
    String mErrorMessage;


    private boolean isContentsLocked = false;

    public boolean isContentsLocked() {
        return isContentsLocked;
    }

    final VideoPlayerAdapter.Callback mAdapterCallback = new VideoPlayerAdapter
            .Callback() {

        @Override
        public void setIsPlaying(boolean playing) {
            super.setIsPlaying(playing);
            mIsPlaying = playing;
        }

        @Override
        public void onReady(boolean ready) {
            super.onReady(ready);
            if (DEBUG) Log.d(TAG, "playWhenReady : " + ready);
            VideoPlaybackBaseControlGlue.this.onReady(ready);
        }


        @Override
        public void onBlockedStateChanged(boolean blocked) {
            super.onBlockedStateChanged(blocked);
            if (DEBUG) Log.d(TAG, "onBlockedStateChanged : blocked : " + blocked);
            VideoPlaybackBaseControlGlue.this.onBlockedStateChanged(blocked);
        }

        @Override
        public void onPlayStateChanged(VideoPlayerAdapter wrapper, VideoState state) {
            if (DEBUG) Log.d(TAG, "onPlayStateChanged");
            VideoPlaybackBaseControlGlue.this.onPlayStateChanged();
        }

        @Override
        public void onCurrentPositionChanged(VideoPlayerAdapter wrapper) {
            VideoPlaybackBaseControlGlue.this.onUpdateProgress();
        }

        @Override
        public void onBufferedPositionChanged(VideoPlayerAdapter wrapper) {
            if (DEBUG) Log.d(TAG, "onBufferedPositionChanged");
            VideoPlaybackBaseControlGlue.this.onUpdateBufferedProgress();
        }

        @Override
        public void onDurationChanged(VideoPlayerAdapter wrapper) {
            if (DEBUG) Log.d(TAG, "onDurationChanged");
            VideoPlaybackBaseControlGlue.this.onUpdateDuration();
        }

        @Override
        public void onProgressInfoChanged(VideoPlayerAdapter adapter, long startTimeMs, long endTimeMs) {
//            super.onProgressInfoChanged(adapter, startTimeMs, endTimeMs);
            if (DEBUG) Log.d(TAG, "onProgressInfoChanged");
            VideoPlaybackBaseControlGlue.this.onUpdateProgramInfo(adapter.getCurrentPosition(), startTimeMs, endTimeMs);
        }

        @Override
        public void onPlayCompleted(VideoPlayerAdapter wrapper) {
            if (DEBUG) Log.v(TAG, "onPlayCompleted");
            VideoPlaybackBaseControlGlue.this.onPlayCompleted();
        }

        @Override
        public void onPreparedStateChanged(VideoPlayerAdapter wrapper) {
            if (DEBUG) Log.v(TAG, "onPreparedStateChanged");
            VideoPlaybackBaseControlGlue.this.onPreparedStateChanged();
        }

        @Override
        public void onVideoSizeChanged(VideoPlayerAdapter wrapper, int width, int height) {
            mVideoWidth = width;
            mVideoHeight = height;
            if (mPlayerCallback != null) {
                mPlayerCallback.onVideoSizeChanged(width, height);
            }
        }

        @Override
        public void onError(VideoPlayerAdapter wrapper, int errorCode, String errorMessage) {
            if (DEBUG) {
                Log.d(TAG, "onError : " + errorMessage);
                Log.d(TAG, "onError : mPlayerCallback != null " + (mPlayerCallback != null ? "nonNull" : "Null"));
            }
            mErrorSet = true;
            mErrorCode = errorCode;
            mErrorMessage = errorMessage;
            if (mPlayerCallback != null) {
                mPlayerCallback.onError(errorCode, errorMessage);
            }
        }

        @Override
        public void onBufferingStateChanged(VideoPlayerAdapter wrapper, boolean start) {
            mBuffering = start;
            if (mPlayerCallback != null) {
                mPlayerCallback.onBufferingStateChanged(start);
            }
        }

        @Override
        public void onMetadataChanged(VideoPlayerAdapter wrapper) {
            VideoPlaybackBaseControlGlue.this.onMetadataChanged();
        }
    };

    /**
     * Constructor for the glue.
     *
     * @param context
     * @param impl    Implementation to underlying media player.
     */
    public VideoPlaybackBaseControlGlue(Context context, T impl) {
        super(context);
        mPlayerAdapter = impl;
        mPlayerAdapter.setCallback(mAdapterCallback);
    }

    public final T getPlayerAdapter() {
        return mPlayerAdapter;
    }

    @Override
    protected void onAttachedToHost(VideoPlaybackGlueHost host) {
        super.onAttachedToHost(host);

        host.setOnKeyInterceptListener(this);
        host.setOnActionClickedListener(this);
        onCreateDefaultControlsRow();
        onCreateDefaultRowPresenter();
        host.setPlaybackRowPresenter(getPlaybackRowPresenter());
        host.setPlaybackRow(getControlsRow());

        mPlayerCallback = host.getPlayerCallback();
        onAttachHostCallback();
        mPlayerAdapter.onAttachedToHost(host);
    }

    void onAttachHostCallback() {
        if (mPlayerCallback != null) {
            if (mVideoWidth != 0 && mVideoHeight != 0) {
                mPlayerCallback.onVideoSizeChanged(mVideoWidth, mVideoHeight);
            }
            if (mErrorSet) {
                mPlayerCallback.onError(mErrorCode, mErrorMessage);
            }
            mPlayerCallback.onBufferingStateChanged(mBuffering);
        }
    }

    void onDetachHostCallback() {
        mErrorSet = false;
        mErrorCode = 0;
        mErrorMessage = null;
        if (mPlayerCallback != null) {
            mPlayerCallback.onBufferingStateChanged(false);
        }
    }

    @Override
    protected void onHostStart() {
        mPlayerAdapter.setProgressUpdatingEnabled(true);
    }

    @Override
    protected void onHostStop() {
        mPlayerAdapter.setProgressUpdatingEnabled(false);
    }

    @Override
    protected void onDetachedFromHost() {
        onDetachHostCallback();
        mPlayerCallback = null;
        mPlayerAdapter.onDetachedFromHost();
        mPlayerAdapter.setProgressUpdatingEnabled(false);
        super.onDetachedFromHost();
    }

    void onReadyControlsRow() {
        if (mControlsRow == null) {
            VideoPlaybackControlsRow controlsRow = new VideoPlaybackControlsRow(this);
            setControlsRow(controlsRow);
        }
    }

    void onCreateDefaultControlsRow() {
        if (mControlsRow == null) {
            VideoPlaybackControlsRow controlsRow = new VideoPlaybackControlsRow(this);
            setControlsRow(controlsRow);
        }
    }

    void onCreateDefaultRowPresenter() {
        if (mControlsRowPresenter == null) {
            setPlaybackRowPresenter(onCreateRowPresenter());
        }
    }

    protected abstract VideoPlaybackRowPresenter onCreateRowPresenter();

    public void setControlsOverlayAutoHideEnabled(boolean enable) {
        mFadeWhenPlaying = enable;
        if (getHost() != null) {
            getHost().setControlsOverlayAutoHideEnabled(enable);
        }
    }

    public boolean isControlsOverlayAutoHideEnabled() {
        return mFadeWhenPlaying;
    }

    /**
     * Sets the controls row to be managed by the glue layer. If
     * {@link VideoPlaybackControlsRow#getPrimaryActionsAdapter()} is not provided, a default
     * {@link ArrayObjectAdapter} will be created and initialized in
     * {@link #onCreatePrimaryActions(ArrayObjectAdapter)}. If
     * {@link VideoPlaybackControlsRow#getSecondaryActionsAdapter()} is not provided, a default
     * {@link ArrayObjectAdapter} will be created and initialized in
     * {@link #onCreateSecondaryActions(ArrayObjectAdapter)}.
     * The primary actions and playback state related aspects of the row
     * are updated by the glue.
     */
    public void setControlsRow(VideoPlaybackControlsRow controlsRow) {
        mControlsRow = controlsRow;
        mControlsRow.setCurrentPosition(-1);
        mControlsRow.setDuration(-1);
        mControlsRow.setBufferedPosition(-1);
        if (mControlsRow.getPrimaryActionsAdapter() == null) {
            ArrayObjectAdapter adapter = new ArrayObjectAdapter(
                    new ControlButtonPresenterSelector());
            onCreatePrimaryActions(adapter);
            mControlsRow.setPrimaryActionsAdapter(adapter);
        }
        // Add secondary actions
        if (mControlsRow.getSecondaryActionsAdapter() == null) {
            ArrayObjectAdapter secondaryActions = new ArrayObjectAdapter(
                    new ControlButtonPresenterSelector());
            onCreateSecondaryActions(secondaryActions);
            getControlsRow().setSecondaryActionsAdapter(secondaryActions);
        }
        updateControlsRow();
    }

    /**
     * Sets the controls row Presenter to be managed by the glue layer.
     */
    public void setPlaybackRowPresenter(VideoPlaybackRowPresenter presenter) {
        mControlsRowPresenter = presenter;
    }

    /**
     * Returns the playback controls row managed by the glue layer.
     */
    public VideoPlaybackControlsRow getControlsRow() {
        return mControlsRow;
    }

    /**
     * Returns the playback controls row Presenter managed by the glue layer.
     */
    public VideoPlaybackRowPresenter getPlaybackRowPresenter() {
        return mControlsRowPresenter;
    }

    /**
     * Handles action clicks.  A subclass may override this add support for additional actions.
     */
    @Override
    public abstract void onActionClicked(Action action);

    /**
     * Handles key events and returns true if handled.  A subclass may override this to provide
     * additional support.
     */
    @Override
    public abstract boolean onKey(View v, int keyCode, KeyEvent event);

    private void updateControlsRow() {
        onMetadataChanged();
    }

    @Override
    public final boolean isPlaying() {
        return mPlayerAdapter.isPlaying();
    }

    @Override
    public void play() {
        mPlayerAdapter.play();
    }

    @Override
    public void pause() {
        mPlayerAdapter.pause();
    }

    @Override
    public void next() {
        mPlayerAdapter.next();
    }

    @Override
    public void previous() {
        mPlayerAdapter.previous();
    }

    protected static void notifyItemChanged(ArrayObjectAdapter adapter, Object object) {
        int index = adapter.indexOf(object);
        if (index >= 0) {
            adapter.notifyArrayItemRangeChanged(index, 1);
        }
    }

    /**
     * May be overridden to add primary actions to the adapter. Default implementation add
     * {@link VideoPlaybackControlsRow.PlayPauseAction}.
     *
     * @param primaryActionsAdapter The adapter to add primary {@link Action}s.
     */
    protected void onCreatePrimaryActions(ArrayObjectAdapter primaryActionsAdapter) {
    }

    /**
     * May be overridden to add secondary actions to the adapter.
     *
     * @param secondaryActionsAdapter The adapter you need to add the {@link Action}s to.
     */
    protected void onCreateSecondaryActions(ArrayObjectAdapter secondaryActionsAdapter) {
    }

    protected void onUpdatePlaybackStatusAfterUserAction(final int msgWhat) {

    }

    @CallSuper
    protected void onReady(boolean ready) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onReady : " + ready);
        }
        if (mControlsRow != null) {

            mControlsRow.readyStateChanged(ready);

            if (ready) {
                if (mPlayerAdapter.whenPlayWithControlBar()) {
                    getHost().showControlsOverlay(true);
                    onUpdatePlaybackStatusAfterUserAction(MSG_HIDE_CONTROL_STATE);
                } else {
                    getHost().hideControlsOverlay(false);
                    onUpdatePlaybackStatusAfterUserAction(MSG_HIDE_CONTROL_STATE);
                }
                getHost().hideIconView(false);
                getHost().hideStateOverlay(false);
            }

            List<PlayerCallback> callbacks = getPlayerCallbacks();
            if (callbacks != null) {
                for (int i = 0, size = callbacks.size(); i < size; i++) {
                    PlayerCallback callback = callbacks.get(i);
                    callback.onReady(ready);
                }
            }
        }
    }


    @CallSuper
    protected void onBlockedStateChanged(boolean isLocked) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onBlockedStateChanged : isLocked : " + isLocked);
        }
        if (isContentsLocked == isLocked) {
            return;
        }
        isContentsLocked = isLocked;
        if (mControlsRow != null) {
            mControlsRow.onBlockedStateChanged(isLocked);
            if (isLocked) {
                getHost().showControlsOverlay(true);
                onUpdatePlaybackStatusAfterUserAction(MSG_HIDE_CONTROL_STATE);
            }
            List<PlayerCallback> callbacks = getPlayerCallbacks();
            if (callbacks != null) {
                for (int i = 0, size = callbacks.size(); i < size; i++) {
                    PlayerCallback callback = callbacks.get(i);
                    callback.onBlockedStateChanged(isLocked);
                }
            }
        }
    }

    protected void onUpdateProgress() {
        if (mControlsRow != null) {
            mControlsRow.setCurrentPosition(mPlayerAdapter.isPrepared()
                    ? getCurrentPosition() : -1);
            notifyCurrentTime(mControlsRow.getCurrentPosition(), mControlsRow.getDuration());
            checkRemainTime(mControlsRow.getCurrentPosition(), mControlsRow.getDuration());
        }
    }

    protected void notifyCurrentTime(long currentTime, long duration) {
        if (currentTime < 0 || duration < 0) {
            return;
        }
        List<PlayerCallback> callbacks = getPlayerCallbacks();
        if (callbacks != null) {
            for (int i = 0, size = callbacks.size(); i < size; i++) {
                PlayerCallback callback = callbacks.get(i);
                callback.onUpdateProgress(this, currentTime, duration);
            }
        }
    }

    protected void checkRemainTime(long currentTime, long duration) {
        if (currentTime < 0 || duration < 0) {
            return;
        }
        List<PlayerCallback> callbacks = getPlayerCallbacks();
        if (callbacks != null) {
            for (int i = 0, size = callbacks.size(); i < size; i++) {
                PlayerCallback callback = callbacks.get(i);
                long remainTime = duration - currentTime;
                long settleTime = callback.getRemainAlertTime();

                if (settleTime == 0) {
                    continue;
                }
                if (remainTime <= settleTime && !callback.isAlerted()) {
                    callback.setAlerted(true);
                    callback.onAlertTime(this);
                }
            }
        }
    }

    @CallSuper
    protected void onUpdateBufferedProgress() {
        if (mControlsRow != null) {
            mControlsRow.setBufferedPosition(mPlayerAdapter.getBufferedPosition());
        }
    }

    @CallSuper
    protected void onUpdateDuration() {
        if (mControlsRow != null) {
            mControlsRow.setDuration(
                    mPlayerAdapter.isPrepared() ? mPlayerAdapter.getDuration() : -1);
        }
    }

    @CallSuper
    protected void onUpdateProgramInfo(long playTimeMs, long startTimeMs, long endTimeMs) {
        if (mControlsRow != null) {
            mControlsRow.setProgramInfo(playTimeMs,
                    startTimeMs, endTimeMs);
        }
    }

    /**
     * @return The duration of the media item in milliseconds.
     */
    protected long getDuration() {
        return mPlayerAdapter.getDuration();
    }

    /**
     * @return The current position of the media item in milliseconds.
     */
    protected long getCurrentPosition() {
        return mPlayerAdapter.getCurrentPosition();
    }

    /**
     * @return The current buffered position of the media item in milliseconds.
     */
    public final long getBufferedPosition() {
        return mPlayerAdapter.getBufferedPosition();
    }

    @Override
    public final boolean isPrepared() {
        return mPlayerAdapter.isPrepared();
    }

    @Override
    public final boolean isReady() {
        return mPlayerAdapter.isReady();
    }

    /**
     * Event when ready state for play changes.
     */
    @CallSuper
    protected void onPreparedStateChanged() {
        onUpdateDuration();
        List<PlayerCallback> callbacks = getPlayerCallbacks();
        if (callbacks != null) {
            for (int i = 0, size = callbacks.size(); i < size; i++) {
                callbacks.get(i).onPreparedStateChanged(this);
            }
        }
    }

    /**
     * Sets the drawable representing cover image. The drawable will be rendered by default
     * description presenter in
     * {@link VideoPlaybackTransportRowPresenter#setDescriptionPresenter(Presenter)}.
     *
     * @param cover The drawable representing cover image.
     */
    public void setArt(Drawable cover) {
        if (mCover == cover) {
            return;
        }
        this.mCover = cover;
        mControlsRow.setImageDrawable(mCover);
        if (getHost() != null) {
            getHost().notifyPlaybackRowChanged();
        }
    }

    /**
     * @return The drawable representing cover image.
     */
    public Drawable getArt() {
        return mCover;
    }

    /**
     * Sets the media subtitle. The subtitle will be rendered by default description presenter
     * {@link VideoPlaybackTransportRowPresenter#setDescriptionPresenter(Presenter)}.
     *
     * @param subtitle Subtitle to set.
     */
    public void setSubtitle(CharSequence subtitle) {
        if (TextUtils.equals(subtitle, mSubtitle)) {
            return;
        }
        mSubtitle = subtitle;
        if (getHost() != null) {
            getHost().notifyPlaybackRowChanged();
        }
    }

    /**
     * Return The media subtitle.
     */
    public CharSequence getSubtitle() {
        return mSubtitle;
    }

    /**
     * Sets the media title. The title will be rendered by default description presenter
     * {@link VideoPlaybackTransportRowPresenter#setDescriptionPresenter(Presenter)}.
     */
    public void setTitle(CharSequence title) {
        if (TextUtils.equals(title, mTitle)) {
            return;
        }
        mTitle = title;

        if (Log.INCLUDE) {
            Log.d(TAG, "setTitle : " + title);
        }
        if (getHost() != null) {
            getHost().notifyPlaybackRowChanged();
        }
    }

    /**
     * Returns the title of the media item.
     */
    public CharSequence getTitle() {
        return mTitle;
    }

    /**
     * Event when metadata changed
     */
    protected void onMetadataChanged() {
        if (mControlsRow == null) {
            return;
        }

        if (DEBUG) Log.v(TAG, "updateRowMetadata");

        mControlsRow.setImageDrawable(getArt());
        mControlsRow.setDuration(getDuration());
        mControlsRow.setCurrentPosition(getCurrentPosition());

        if (getHost() != null) {
            getHost().notifyPlaybackRowChanged();
        }
    }

    /**
     * Event when play state changed.
     */
    @CallSuper
    protected void onPlayStateChanged() {
        List<PlayerCallback> callbacks = getPlayerCallbacks();
        if (callbacks != null) {
            for (int i = 0, size = callbacks.size(); i < size; i++) {
                callbacks.get(i).onPlayStateChanged(this);
            }
        }
    }

    /**
     * Event when play finishes, subclass may handling repeat mode here.
     */
    @CallSuper
    protected void onPlayCompleted() {
        List<PlayerCallback> callbacks = getPlayerCallbacks();
        if (callbacks != null) {
            for (int i = 0, size = callbacks.size(); i < size; i++) {
                callbacks.get(i).onPlayCompleted(this);
            }
        }
    }


    /**
     * Seek media to a new position.
     *
     * @param position New position.
     */
    public final void seekTo(long position) {
        mPlayerAdapter.seekTo(position);
    }

    /**
     * Returns a bitmask of actions supported by the media player.
     */
    public long getSupportedActions() {
        return mPlayerAdapter.getSupportedActions();
    }
}
