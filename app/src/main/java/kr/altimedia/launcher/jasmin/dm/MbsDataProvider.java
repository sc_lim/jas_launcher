/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm;

import android.content.Context;

/**
 * Created by mc.kim on 18,05,2020
 */
public abstract class MbsDataProvider<K, V> {

    public abstract void needLoading(boolean loading);

    public abstract void onFailed(int reason);

    public abstract void onSuccess(K id, V result);

    public abstract void onError(String errorCode, String message);

    public Context getTaskContext() {
        return null;
    }

    public boolean ignoreUserLogin() {
        return false;
    }

}
