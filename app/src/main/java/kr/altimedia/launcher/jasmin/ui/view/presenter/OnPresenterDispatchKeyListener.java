/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.presenter;

import android.view.KeyEvent;

/**
 * Created by mc.kim on 05,03,2020
 */
public interface OnPresenterDispatchKeyListener {
    boolean notifyKey(int position, KeyEvent event);
}
