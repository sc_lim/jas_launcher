/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;
import java.util.Iterator;

import androidx.annotation.NonNull;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;

public class VodWatchPackagePresenter extends Presenter {

    private HashMap<String, Integer> flagMap = new HashMap<>();

    public VodWatchPackagePresenter() {
        flagMap.put(Content.FLAG_EVENT, R.id.flagEvent);
        flagMap.put(Content.FLAG_HOT, R.id.flagHot);
        flagMap.put(Content.FLAG_NEW, R.id.flagNew);
        flagMap.put(Content.FLAG_PREMIUM, R.id.flagPremium);
        flagMap.put(Content.FLAG_UHD, R.id.flagUhd);
    }

    @Override
    public VodViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_vod_package, parent, false);
        return new VodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        VodViewHolder vh = (VodViewHolder) viewHolder;
        vh.setData((Content) item);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }

    private class VodViewHolder extends Presenter.ViewHolder {
        private ImageView poster;
        private TextView title;

        public VodViewHolder(@NonNull View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View view) {
            poster = view.findViewById(R.id.poster);
            title = view.findViewById(R.id.title);
        }

        public void setData(Content content) {
            title.setText(content.getTitle());

            boolean isLocked = false;
            Object drawable = isLocked ? R.drawable.poster_block : content.getPosterSourceUrl(Content.mPosterDefaultRect.width(),
                    Content.mPosterDefaultRect.height());
            setPoster(drawable);
            initFlag(view, content);
        }

        private void setPoster(Object drawable) {
            Glide.with(poster.getContext())
                    .load(drawable).diskCacheStrategy(DiskCacheStrategy.DATA)
                    .centerCrop()
                    .placeholder(R.drawable.poster_default)
                    .error(R.drawable.poster_default)
                    .into(poster);
        }

        private void initFlag(View view, Content content) {
            if (content.getEventFlag() == null) {
                removeFlag(view);
                return;
            }
            Iterator<String> keyIterator = flagMap.keySet().iterator();
            while (keyIterator.hasNext()) {
                String key = keyIterator.next();
                boolean hasFlag = content.getEventFlag().contains(key);
                View flagView = view.findViewById(flagMap.get(key));
                flagView.setVisibility(hasFlag ? View.VISIBLE : View.GONE);
            }

            View lockIcon = view.findViewById(R.id.iconVodLock);
            lockIcon.setVisibility(View.INVISIBLE);
        }

        private void removeFlag(View view) {
            Iterator<String> keyIterator = flagMap.keySet().iterator();
            while (keyIterator.hasNext()) {
                String key = keyIterator.next();
                View flagView = view.findViewById(flagMap.get(key));
                flagView.setVisibility(View.GONE);
            }
            View lockIcon = view.findViewById(R.id.iconVodLock);
            if (lockIcon != null) {
                lockIcon.setVisibility(View.GONE);
            }
        }
    }
}
