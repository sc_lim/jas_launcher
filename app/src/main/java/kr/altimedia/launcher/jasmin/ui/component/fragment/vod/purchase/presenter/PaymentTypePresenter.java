/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentButton;

public class PaymentTypePresenter extends Presenter {
    private final float DEFAULT_ALPHA = 1.0f;
    private final float DIM_ALPHA = 0.4f;

    public PaymentTypePresenter() {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_payment_type, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((PaymentButton) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public class ViewHolder extends Presenter.ViewHolder {
        private TextView paymentButton;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            paymentButton = view.findViewById(R.id.payment_button);
        }

        public void setData(PaymentButton payment) {
            String name = view.getContext().getString(payment.getType().getName());
            paymentButton.setText(name);

            boolean isEnabled = payment.isEnable();
            view.setEnabled(isEnabled);
            view.setFocusable(isEnabled);
            view.setFocusableInTouchMode(isEnabled);

            float alpha = isEnabled ? DEFAULT_ALPHA : DIM_ALPHA;
            view.setAlpha(alpha);
        }
    }
}
