/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.list.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.BonusType;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProduct;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class CouponProductPresenter extends RowPresenter {

    public CouponProductPresenter() {
        super();
    }

    @Override
    public ViewHolder createRowViewHolder(ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        CouponItemViewHolder mVodItemViewHolder = new CouponItemViewHolder(inflater.inflate(R.layout.item_coupon_shop_product, null, false));
        return mVodItemViewHolder;
    }

    @Override
    public void onBindRowViewHolder(ViewHolder viewHolder, Object object) {
        super.onBindRowViewHolder(viewHolder, object);

        CouponItemViewHolder itemViewHolder = (CouponItemViewHolder) viewHolder;
        CouponProduct item = (CouponProduct) object;

        itemViewHolder.setItem(item);
    }

    @Override
    public void onUnbindRowViewHolder(ViewHolder viewHolder) {
        super.onUnbindRowViewHolder(viewHolder);

        CouponItemViewHolder itemViewHolder = (CouponItemViewHolder) viewHolder;
        itemViewHolder.dispose();
    }

    public class CouponItemViewHolder extends ViewHolder {
        private final String TAG = CouponItemViewHolder.class.getSimpleName();

        private CouponProduct item;

        private RelativeLayout couponBgImage;
        private TextView couponPrice;
        private LinearLayout bonusValueLayer;
        private TextView bonusValue;
        private TextView bonusUnit;

        private TextView totalPaymentValue;

        public CouponItemViewHolder(View view) {
            super(view);

            couponBgImage = view.findViewById(R.id.couponBgImage);
            couponPrice = view.findViewById(R.id.couponPrice);
            bonusValueLayer = view.findViewById(R.id.bonusValueLayer);
            bonusValue = view.findViewById(R.id.bonusValue);
            bonusUnit = view.findViewById(R.id.bonusUnit);
            totalPaymentValue = view.findViewById(R.id.totalPaymentValue);
        }

        public void dispose() {

        }

        public void setItem(CouponProduct item) {
            this.item = item;

            couponBgImage.setBackground(view.getResources().getDrawable(item.getBgImage()));
            try {
                couponPrice.setText(StringUtil.getFormattedNumber(item.getChargedAmount()));
            }catch (Exception e){
            }
            try {
                if(item.getBonusValue() > 0 && (item.getBonusType() == BonusType.DC_RATE || item.getBonusType() == BonusType.DC_AMOUNT)) {
                    bonusValue.setText(StringUtil.getFormattedNumber(item.getBonusValue()));
                    if (item.getBonusType() == BonusType.DC_RATE) {
                        bonusUnit.setText("%");
                    } else if (item.getBonusType() == BonusType.DC_AMOUNT) {
                        bonusUnit.setText(R.string.thb);
                    }
                    bonusValueLayer.setVisibility(View.VISIBLE);
                }else{
                    bonusValueLayer.setVisibility(View.GONE);
                }
            }catch (Exception e){
            }

            try {
                totalPaymentValue.setText(StringUtil.getFormattedNumber(item.getPrice()));
            }catch (Exception e){
            }
        }
    }
}
