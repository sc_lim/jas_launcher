/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 20,06,2020
 */
public class ThumbnailInfo implements Parcelable {
    public static final Creator<ThumbnailInfo> CREATOR = new Creator<ThumbnailInfo>() {
        @Override
        public ThumbnailInfo createFromParcel(Parcel in) {
            return new ThumbnailInfo(in);
        }

        @Override
        public ThumbnailInfo[] newArray(int size) {
            return new ThumbnailInfo[size];
        }
    };
    @Expose
    @SerializedName("thumbnailFolder")
    private String thumbnailFolder;
    @Expose
    @SerializedName("thumbnailRootUrl")
    private String thumbnailRootUrl;

    protected ThumbnailInfo(Parcel in) {
        thumbnailFolder = in.readString();
        thumbnailRootUrl = in.readString();
    }

    public ThumbnailInfo(String thumbnailFolder, String thumbnailRootUrl) {
        this.thumbnailFolder = thumbnailFolder;
        this.thumbnailRootUrl = thumbnailRootUrl;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(thumbnailFolder);
        dest.writeString(thumbnailRootUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getThumbnailFolder() {
        return thumbnailFolder;
    }

    public String getThumbnailRootUrl() {
        return thumbnailRootUrl;
    }

    @Override
    public String toString() {
        return "ThumbnailInfo{" +
                "thumbnailFolder='" + thumbnailFolder + '\'' +
                ", thumbnailRootUrl='" + thumbnailRootUrl + '\'' +
                '}';
    }
}
