/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter;

import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.widget.LauncherAppWidgetHost;
import kr.altimedia.launcher.jasmin.ui.view.widget.LauncherAppWidgetProviderInfo;


/**
 * Created by mc.kim on 19,12,2019
 */
public class WidgetPreviewPresenter extends Presenter {
    private final String TAG = WidgetPreviewPresenter.class.getSimpleName();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        Log.d(TAG, "onCreateViewHolder ");
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewGroup parentsView = (ViewGroup) inflater.inflate(R.layout.item_remote_widget, null);

        return new WidgetViewHolder(parentsView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        Log.d(TAG, "onBindViewHolder ");
        Context context = viewHolder.view.getContext();
        WidgetViewHolder widgetViewHolder = (WidgetViewHolder) viewHolder;
        AppWidgetProviderInfo mAppWidgetProviderInfo = (AppWidgetProviderInfo) item;
        LauncherAppWidgetProviderInfo mLauncherAppWidgetProviderInfo = LauncherAppWidgetProviderInfo.fromProviderInfo(context, mAppWidgetProviderInfo);
        LauncherAppWidgetHost launcherAppWidgetHost = new LauncherAppWidgetHost(context);
        launcherAppWidgetHost.startListening();
        AppWidgetHostView launcherAppWidgetHostView = launcherAppWidgetHost.createView(context, mLauncherAppWidgetProviderInfo.autoAdvanceViewId, mLauncherAppWidgetProviderInfo);
        widgetViewHolder.getRemoteParents().addView(launcherAppWidgetHostView);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        Log.d(TAG, "onUnbindViewHolder ");


    }

    private static class WidgetViewHolder extends ViewHolder {

        public WidgetViewHolder(View view) {
            super(view);
        }

        public ViewGroup getRemoteParents() {
            return (ViewGroup) view;
        }
    }
}
