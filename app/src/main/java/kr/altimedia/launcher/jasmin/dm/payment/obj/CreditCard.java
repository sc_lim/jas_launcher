package kr.altimedia.launcher.jasmin.dm.payment.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreditCard implements Parcelable {
    @Expose
    @SerializedName("creditCardId")
    private String creditCardId;
    @Expose
    @SerializedName("cardNumber")
    private String cardNumber;
    @Expose
    @SerializedName("holderName")
    private String holderName;
    @Expose
    @SerializedName("creditCardLogo")
    private String creditCardLogo;
    @Expose
    @SerializedName("creditCardType")
    private String creditCardType;
    @Expose
    @SerializedName("creditCardTypeName")
    private String creditCardTypeName;

    protected CreditCard(Parcel in) {
        creditCardId = in.readString();
        cardNumber = in.readString();
        holderName = in.readString();
        creditCardLogo = in.readString();
        creditCardType = in.readString();
        creditCardTypeName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(creditCardId);
        dest.writeString(cardNumber);
        dest.writeString(holderName);
        dest.writeString(creditCardLogo);
        dest.writeString(creditCardType);
        dest.writeString(creditCardTypeName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CreditCard> CREATOR = new Creator<CreditCard>() {
        @Override
        public CreditCard createFromParcel(Parcel in) {
            return new CreditCard(in);
        }

        @Override
        public CreditCard[] newArray(int size) {
            return new CreditCard[size];
        }
    };

    public String getCreditCardId() {
        return creditCardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getHolderName() {
        return holderName;
    }

    public String getCreditCardLogo() {
        return creditCardLogo;
    }

    public void setCreditCardLogo(String creditCardLogo) {
        this.creditCardLogo = creditCardLogo;
    }

    public String getCreditCardType() {
        return creditCardType;
    }

    public String getCreditCardTypeName() {
        return creditCardTypeName;
    }
}
