/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.presenter;


import android.animation.AnimatorInflater;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.viewpager.widget.ViewPager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.ui.view.pager.NotifyKeyViewPager;
import kr.altimedia.launcher.jasmin.ui.view.row.PagerRow;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;


public abstract class ViewPagerPresenter extends RowPresenter {
    private static final String TAG = ViewPagerPresenter.class.getSimpleName();
    private FragmentManager mFragmentManager = null;

    private static final int defaultPosition = 0;
    private int mIndicatorLayoutId = -1;
    protected boolean mAutoPaging;

    public ViewPagerPresenter(FragmentManager fragmentManager) {
        this(true, fragmentManager);
    }

    public ViewPagerPresenter(boolean autoPaging, FragmentManager fragmentManager) {
        this(autoPaging, -1, fragmentManager);
    }


    public ViewPagerPresenter(boolean autoPaging, int indicatorLayoutId, FragmentManager fragmentManager) {
        this.mAutoPaging = autoPaging;
        this.mIndicatorLayoutId = indicatorLayoutId;
        this.mFragmentManager = fragmentManager;
    }

    protected void setAutoPaging(boolean autoPaging) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setAutoPaging : " + autoPaging);
        }
        this.mAutoPaging = autoPaging;
    }

    private boolean hasIndicator() {
        return mIndicatorLayoutId != -1;
    }

    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);
        ViewPagerPresenter.ViewHolder vh = (ViewPagerPresenter.ViewHolder) holder;
        vh.setIndicatorView(mIndicatorLayoutId);
        PagerRow rowItem = (PagerRow) item;
        vh.mPromotionPagerAdapter.setItemAdapter((ArrayObjectAdapter) rowItem.getAdapter());
        if (rowItem.getAdapter().size() > 0) {
            Object selectedItem = rowItem.getAdapter().get(0);
            if (selectedItem instanceof Banner) {
                Banner banner = (Banner) selectedItem;
                vh.actionButtonEnable(!StringUtils.nullToEmpty(banner.getLinkType()).isEmpty());
            } else {
                vh.actionButtonEnable(false);
            }
        }
    }


    @Override
    protected void onUnbindRowViewHolder(RowPresenter.ViewHolder holder) {
        super.onUnbindRowViewHolder(holder);
        ViewPagerPresenter.ViewHolder vh = (ViewPagerPresenter.ViewHolder) holder;
        vh.removeObserver();
    }

    protected abstract PageRootView createPageView(Context context);

    protected boolean notifyPromotionKeyEvent(int keyCode) {

        return false;
    }

    protected void onStartedPresenter() {
        if (mViewPagerChangeListener != null) {
            mViewPagerChangeListener.onAttached();
        }
    }

    protected void onStoppedPresenter() {
        if (mViewPagerChangeListener != null) {
            mViewPagerChangeListener.onDetached();
        }
    }

    @Override
    protected void initializeRowViewHolder(RowPresenter.ViewHolder holder) {
        super.initializeRowViewHolder(holder);
        final ViewPagerPresenter.ViewHolder pagerHolder = (ViewPagerPresenter.ViewHolder) holder;
        Context context = pagerHolder.view.getContext();
        ViewPagerAdapter promotionPagerAdapter = createPagerAdapter(mFragmentManager, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,
                context);

        pagerHolder.setPromotionPagerAdapter(promotionPagerAdapter);
        pagerHolder.mViewPager.setAdapter(promotionPagerAdapter);

        mViewPagerChangeListener = pagerHolder.getOnViewPagerChangeInteractor();
        pagerHolder.mParentsView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyEvent.getAction() == KeyEvent.ACTION_UP || mOnPromotionChangedListener == null) {
                    return false;
                }


                int selectedPosition = pagerHolder.getSelectedPosition();
                switch (keyEvent.getKeyCode()) {
                    case KeyEvent.KEYCODE_DPAD_LEFT: {
                        if (selectedPosition == 0) {
                            if (keyEvent.getRepeatCount() > 0) {
                                return true;
                            }
                            return mOnPromotionChangedListener.onNotifyPromotionOutEvent(selectedPosition, keyEvent);
                        } else {
                            return notifyPromotionKeyEvent(keyCode);
                        }
                    }
                    case KeyEvent.KEYCODE_BACK: {
                        return mOnPromotionChangedListener.onNotifyPromotionOutEvent(selectedPosition, keyEvent);
                    }
                    case KeyEvent.KEYCODE_DPAD_RIGHT: {
                        return notifyPromotionKeyEvent(keyCode);
                    }
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER: {
                        Object selectedItem = pagerHolder.mPromotionPagerAdapter.getDataItem(selectedPosition);
                        mOnPromotionChangedListener.onPromotionSelected(selectedItem);
                        return true;
                    }
                }
                return false;
            }
        });

        pagerHolder.mViewPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnPromotionChangedListener == null) {
                    return;
                }

                int selectedPosition = pagerHolder.getSelectedPosition();
                Object selectedItem = pagerHolder.mPromotionPagerAdapter.getDataItem(selectedPosition);
                mOnPromotionChangedListener.onPromotionSelected(selectedItem);
            }
        });


        pagerHolder.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (mOnPromotionChangedListener != null) {
                    mOnPromotionChangedListener.onPromotionChanged(position, pagerHolder.mPromotionPagerAdapter.getDataItem(position));
                }
                pagerHolder.actionViewButtonEnabledChange(true);
                Object selectedItem = pagerHolder.mPromotionPagerAdapter.getDataItem(position);
                if (selectedItem instanceof Banner) {
                    Banner banner = (Banner) selectedItem;
                    pagerHolder.actionButtonEnable(!StringUtils.nullToEmpty(banner.getLinkType()).isEmpty());
                } else {
                    pagerHolder.actionButtonEnable(false);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (mOnPromotionChangedListener != null) {
                    mOnPromotionChangedListener.onPageScrollStateChanged(state);
                }
            }
        });

    }

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {
        PageRootView rootView = createPageView(parent.getContext());
        return new ViewHolder(rootView.getRootView(), rootView.getParentsView(), rootView.getPager(), rootView.getIndicator(), rootView.getActionView(), rootView.getBlackScreen(), this, mOnPromotionChangedListener);
    }


    protected ViewHolder.OnViewPagerChangeListener mViewPagerChangeListener;


    protected abstract ViewPagerAdapter createPagerAdapter(FragmentManager fragmentManager, int behavior, Context context);

    public interface PageRootView {
        View getRootView();

        View getParentsView();

        NotifyKeyViewPager getPager();

        View getIndicator();

        View getActionView();

        View getBlackScreen();
    }


    private OnPromotionChangedListener mOnPromotionChangedListener = null;

    protected void setOnPromotionChangedListener(OnPromotionChangedListener onPromotionChangedListener) {
        this.mOnPromotionChangedListener = onPromotionChangedListener;
    }

    public interface OnPromotionChangedListener {
        void onPageScrollStateChanged(int state);

        void onPromotionChanged(int position, Object object);

        boolean onNotifyPromotionOutEvent(int position, KeyEvent event);

        void onPromotionSelected(Object object);

        void onPromotionComplete(int position);
    }


    private static class ViewHolder extends RowPresenter.ViewHolder {
        final ViewPagerPresenter mViewPagerPresenter;
        final NotifyKeyViewPager mViewPager;
        final View mParentsView;
        final View mActionButton;
        final View mBlackScreen;
        final View mIndicator;
        private int mIndicatorItemView = -1;
        ViewPagerAdapter mPromotionPagerAdapter = null;
        DataSetObserver mItemObserver;
        final ViewPager.OnPageChangeListener mOnPageChangeListener;
        final OnViewPagerChangeListener mOnViewPagerChangeInteractor;
        final Handler mButtonHandler = new Handler(Looper.getMainLooper());
        final ButtonShowRunnable mButtonShowRunnable;
        final ButtonHideRunnable mButtonHideRunnable;
        final OnPromotionChangedListener mOnPromotionChangedListener;

        private void notifyIndicatorSelectedChanged(ImageView indicator, boolean selected) {
            if (indicator.isSelected() == selected) {
                return;
            }
            indicator.setSelected(selected);
            if (selected) {
                indicator.setImageResource(R.drawable.selector_promo_selected);
            } else {
                indicator.setImageResource(R.drawable.promo_dot);
            }
        }

        public ViewHolder(View rootView, View parentsView, NotifyKeyViewPager gridView, View indicator,
                          View actionView, View blackScreen, ViewPagerPresenter p, OnPromotionChangedListener onPromotionChangedListener) {
            super(rootView);
            this.mParentsView = parentsView;
            this.mViewPager = gridView;
            this.mViewPagerPresenter = p;
            this.mIndicator = indicator;
            this.mActionButton = actionView;
            this.mBlackScreen = blackScreen;
            this.mOnPromotionChangedListener = onPromotionChangedListener;
            mButtonShowRunnable = new ButtonShowRunnable(mActionButton);
            mButtonHideRunnable = new ButtonHideRunnable(mActionButton);


            mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    for (int i = 0; i < mIndicatorView.size(); i++) {
                        notifyIndicatorSelectedChanged(mIndicatorView.get(i), position == i);
                    }
                }
            };
            mOnViewPagerChangeInteractor = new OnViewPagerChangeListener() {
                @Override
                public void setCurrentItem(int position, boolean smooth) {
                    mViewPager.setCurrentItem(position, smooth);
                }

                @Override
                public int getCurrentPosition() {
                    return mViewPager.getCurrentItem();
                }

                @Override
                public int getSize() {
                    if (mViewPager.getAdapter() != null) {
                        return mViewPager.getAdapter().getCount();
                    } else {
                        return 0;
                    }
                }

                @Override
                public void onVideoComplete(int position) {
                    mOnPromotionChangedListener.onPromotionComplete(position);
                }

                @Override
                public void onAttached() {
                    int currentPosition = getCurrentPosition();
                    if (getPromotionPagerAdapter().getCount() <= currentPosition) {
                        return;
                    }
                    Object selectedItem = getPromotionPagerAdapter().getDataItem(currentPosition);
                    if (selectedItem == null) {
                        return;
                    }
                    if (selectedItem instanceof Banner) {
                        Banner banner = (Banner) selectedItem;
                        actionButtonEnable(!StringUtils.nullToEmpty(banner.getLinkType()).isEmpty());
                    } else {
                        actionButtonEnable(false);
                    }
                }

                @Override
                public void onDetached() {
                    mButtonHandler.removeCallbacks(mButtonHideRunnable);
                    mButtonHandler.removeCallbacks(mButtonShowRunnable);
                }
            };
            gridView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (Log.INCLUDE) {
                        Log.d("test : ", "onFocusChanged : " + hasFocus);
                    }
                    actionViewButtonEnabledChange(hasFocus);
                }
            });

        }

        public void showBlackScreen() {
            if (mBlackScreen == null) {
                return;
            }
            mBlackScreen.setVisibility(View.VISIBLE);
        }

        public void hideBlackScreen() {
            mBlackScreen.setVisibility(View.GONE);
        }

        public void actionViewButtonEnabledChange(boolean enable) {
            mActionButton.setEnabled(enable);
            mButtonHandler.removeCallbacks(mButtonHideRunnable);
            mButtonHandler.removeCallbacks(mButtonShowRunnable);
            if (enable) {
                mButtonHandler.post(mButtonShowRunnable);
            }
            mButtonHandler.postDelayed(mButtonHideRunnable, ButtonRunnable.DELAY_TIME);
        }

        public void actionButtonEnable(boolean enable) {
            if (!enable) {
                mButtonHandler.removeCallbacks(mButtonHideRunnable);
                mButtonHandler.removeCallbacks(mButtonShowRunnable);
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "actionButtonEnable : " + enable);
            }
            mActionButton.setAlpha(enable ? 1f : 0f);

            if (enable) {
                mButtonHandler.postDelayed(mButtonHideRunnable, ButtonRunnable.DELAY_TIME);
            }

        }


        public OnViewPagerChangeListener getOnViewPagerChangeInteractor() {
            return mOnViewPagerChangeInteractor;
        }

        public ViewPagerAdapter getPromotionPagerAdapter() {
            return mPromotionPagerAdapter;
        }

        public void setPromotionPagerAdapter(ViewPagerAdapter mPromotionPagerAdapter) {
            this.mPromotionPagerAdapter = mPromotionPagerAdapter;
        }

        public final ViewPagerPresenter getPagerPresenter() {
            return this.mViewPagerPresenter;
        }

        protected final ViewPager getViewPager() {
            return this.mViewPager;
        }

        public final View getIndicator() {
            return this.mIndicator;
        }

        public int getSelectedPosition() {
            return this.mViewPager.getCurrentItem();
        }


        public void setIndicatorView(int viewId) {
            mIndicatorItemView = viewId;

            if (mItemObserver != null) {
                mViewPager.getAdapter().unregisterDataSetObserver(mItemObserver);
                mItemObserver = null;
            }

            mItemObserver = new DataSetObserver() {
                @Override
                public void onChanged() {
                    super.onChanged();
                    initIndicatorView();
                }

                @Override
                public void onInvalidated() {
                    super.onInvalidated();
                }
            };
            mViewPager.getAdapter().registerDataSetObserver(mItemObserver);
            mViewPager.addOnPageChangeListener(mOnPageChangeListener);
        }

        public void removeObserver() {
            if (mItemObserver != null) {
                mViewPager.getAdapter().unregisterDataSetObserver(mItemObserver);
                mItemObserver = null;
            }
            mViewPager.removeOnPageChangeListener(mOnPageChangeListener);
        }


        private final ArrayList<ImageView> mIndicatorView = new ArrayList<>();

        private void initIndicatorView() {
            if (mIndicatorItemView == -1) {
                return;
            }
            mIndicatorView.clear();
            ((ViewGroup) getIndicator()).removeAllViews();
            int count = mViewPager.getAdapter().getCount();
            if (count <= 1) {
                return;
            }
            for (int i = 0; i < count; i++) {
                LayoutInflater inflater = LayoutInflater.from(mViewPager.getContext());
                ViewGroup indicator = (ViewGroup) inflater.inflate(mIndicatorItemView, null);
                ImageView imageView = indicator.findViewById(R.id.indicatorView);
                notifyIndicatorSelectedChanged(imageView, defaultPosition == i);
                mIndicatorView.add(imageView);
                ((ViewGroup) getIndicator()).addView(indicator);
            }
        }


        public interface OnViewPagerChangeListener {

            void setCurrentItem(int position, boolean smooth);

            int getCurrentPosition();

            int getSize();

            void onVideoComplete(int position);

            void onAttached();

            void onDetached();
        }

        private static class ButtonShowRunnable extends ButtonRunnable {
            public ButtonShowRunnable(View targetView) {
                super(targetView, true);
            }
        }

        private static class ButtonHideRunnable extends ButtonRunnable {
            public ButtonHideRunnable(View targetView) {
                super(targetView, false);
            }
        }

        private abstract static class ButtonRunnable implements Runnable {
            private final String TAG = ButtonRunnable.class.getSimpleName();
            public static final long DELAY_TIME = 5 * TimeUtil.SEC;
            private final View mTargetView;
            private final boolean requestShow;
            ValueAnimator mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator;

            public ButtonRunnable(View targetView, boolean show) {
                requestShow = show;
                mTargetView = targetView;
                loadTransportAnimator(targetView.getContext());
            }

            @Override
            public void run() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call : " + requestShow);
                }
                endAll(mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator);
                if (requestShow) {
                    if (mTargetView.getAlpha() == 1f) {
                        return;
                    }
                    mTargetView.setAlpha(0f);
                    reverseFirstOrStartSecond(mTransportRowFadeOutAnimator, mTransportRowFadeInAnimator, true);
                } else {
                    if (mTargetView.getAlpha() == 0f) {
                        return;
                    }
                    reverseFirstOrStartSecond(mTransportRowFadeInAnimator, mTransportRowFadeOutAnimator, true);
                }
            }


            private final TimeInterpolator mLogDecelerateInterpolator = new TimeInterpolator() {
                @Override
                public float getInterpolation(float t) {
                    float mLogScale = 1f / computeLog(1, 100, 0);
                    return computeLog(t, 100, 0) * mLogScale;
                }
            };

            private final TimeInterpolator mLogAccelerateInterpolator = new TimeInterpolator() {
                @Override
                public float getInterpolation(float t) {
                    float mLogScale = 1f / computeLog(1, 100, 0);
                    return 1 - computeLog(1 - t, 100, 0) * mLogScale;
                }
            };

            private float computeLog(float t, int base, int drift) {
                return (float) -Math.pow(base, -t) + 1 + (drift * t);
            }


            private ValueAnimator loadAnimator(Context context, int resId) {
                ValueAnimator animator = (ValueAnimator) AnimatorInflater.loadAnimator(context, resId);
                animator.setDuration(animator.getDuration() * 1);
                return animator;
            }

            private void loadTransportAnimator(Context context) {
                final ValueAnimator.AnimatorUpdateListener updateListener = new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator arg0) {
                        final float fraction = (Float) arg0.getAnimatedValue();
                        float translateValue = (1f - fraction);
                        if (mTargetView != null) {
                            mTargetView.setAlpha(fraction);
                        }
                    }
                };
                mTransportRowFadeInAnimator = loadAnimator(context, R.animator.pip_control_fade_in);
                mTransportRowFadeInAnimator.addUpdateListener(updateListener);
                mTransportRowFadeInAnimator.setInterpolator(mLogDecelerateInterpolator);
                mTransportRowFadeOutAnimator = loadAnimator(context, R.animator.pip_control_fade_out);
                mTransportRowFadeOutAnimator.addUpdateListener(updateListener);
                mTransportRowFadeOutAnimator.setInterpolator(mLogAccelerateInterpolator);
            }

            private void endAll(ValueAnimator first, ValueAnimator second) {
                if (first.isStarted()) {
                    first.end();
                } else if (second.isStarted()) {
                    second.end();
                }
            }

            private void reverseFirstOrStartSecond(ValueAnimator first, ValueAnimator second,
                                                   boolean runAnimation) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "first.isStarted() : " + first.isStarted());
                }
                if (first.isStarted()) {
                    first.reverse();
                    if (!runAnimation) {
                        first.end();
                    }
                } else {
                    second.start();
                    if (!runAnimation) {
                        second.end();
                    }
                }
            }
        }

    }


    protected static abstract class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public abstract Fragment getItem(int position);

        @Override
        public abstract int getCount();


        public abstract void setItemAdapter(ArrayObjectAdapter objectAdapter);

        public abstract Object getDataItem(int position);
    }


}
