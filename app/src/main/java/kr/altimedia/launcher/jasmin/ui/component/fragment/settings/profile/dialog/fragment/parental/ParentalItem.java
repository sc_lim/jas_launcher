/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental;

import android.os.Parcel;
import android.os.Parcelable;

public class ParentalItem implements Parcelable {
    public static final Creator<ParentalItem> CREATOR = new Creator<ParentalItem>() {
        @Override
        public ParentalItem createFromParcel(Parcel in) {
            return new ParentalItem(in);
        }

        @Override
        public ParentalItem[] newArray(int size) {
            return new ParentalItem[size];
        }
    };

    private int title;
    private String count;
    private boolean hasValue;
    private int invalidText;

    public ParentalItem(int title, String count, boolean hasValue, int invalidText) {
        this.title = title;
        this.count = count;
        this.hasValue = hasValue;
        this.invalidText = invalidText;
    }

    protected ParentalItem(Parcel in) {
        title = in.readInt();
        count = in.readString();
        hasValue = in.readByte() != 0;
        invalidText = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(title);
        dest.writeString(count);
        dest.writeByte((byte) (hasValue ? 1 : 0));
        dest.writeInt(invalidText);
    }

    public int getTitle() {
        return title;
    }

    public String getCount() {
        return count;
    }

    public boolean hasValue() {
        return hasValue;
    }

    public int getInvalidText() {
        return invalidText;
    }
}
