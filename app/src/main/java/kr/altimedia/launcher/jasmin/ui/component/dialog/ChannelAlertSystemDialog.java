package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.util.StringUtils;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;

public class ChannelAlertSystemDialog implements View.OnKeyListener, ChannelDataManager.Listener {
    public static final String CLASS_NAME = ChannelAlertSystemDialog.class.getName();
    private final String TAG = ChannelAlertSystemDialog.class.getSimpleName();

    private WindowManager wm;
    private final View parentsView;
    private final Context mContext;
    private final int mKeyCode;
    private final String mPopUpTitle;
    private final String mErrorCode;
    private final String mDescription;
    private final BackendKnobsFlags mBackendKnobsFlags;
    private final ChannelDataManager mChannelDataManager;


    public ChannelAlertSystemDialog(Context context, int keyCode, String popupTitle, String errorCode) {
        mChannelDataManager = TvSingletons.getSingletons(context).getChannelDataManager();
        mBackendKnobsFlags = TvSingletons.getSingletons(context).getBackendKnobs();
        mContext = context;
        mKeyCode = keyCode;
        mPopUpTitle = popupTitle;
        mErrorCode = errorCode;
        mDescription = mBackendKnobsFlags.isEpgReady(false) ?
                context.getString(R.string.error_message_lnc_0009_1) : context.getString(R.string.error_message_lnc_0009);
        LayoutInflater inflater = LayoutInflater.from(context);
        parentsView = inflater.inflate(R.layout.dialog_error_channel, null, false);
        initView(parentsView);


    }

    public void show(Context context) {
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                PixelFormat.TRANSLUCENT);
        mParams.gravity = Gravity.CENTER;
        wm.addView(parentsView, mParams);
        mBackendKnobsFlags.setSystemDialogVisibility(true);
        mChannelDataManager.addListener(this);
    }

    public void dismiss() {
        mBackendKnobsFlags.setSystemDialogVisibility(false);
        wm.removeViewImmediate(parentsView);
        mChannelDataManager.removeListener(this);
    }

    private void initView(View view) {
        TextView popupTitleView = view.findViewById(R.id.popupTitle);
        TextView errorCodeView = view.findViewById(R.id.error_code);
        TextView errorMessageView = view.findViewById(R.id.error_message);

        if (!StringUtils.nullToEmpty(mPopUpTitle).isEmpty()) {
            popupTitleView.setText(mPopUpTitle);
        }
        errorCodeView.setText(mErrorCode);
        errorMessageView.setText(mDescription);
        errorMessageView.setFocusable(true);
        errorMessageView.setFocusableInTouchMode(true);
        errorMessageView.requestFocus();
        errorMessageView.setOnKeyListener(this);
    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                dismiss();
                return true;
            }
        }
        return false;
    }

    @Override
    public void onLoadFinished() {
        dismiss();

        if (mBackendKnobsFlags.isEpgReady(true)) {
            startLiveTv(mKeyCode);
        }
    }

    @Override
    public void onChannelListUpdated() {
        dismiss();
        if (mBackendKnobsFlags.isEpgReady(true)) {
            startLiveTv(mKeyCode);
        }

    }

    @Override
    public void onChannelBrowsableChanged() {

    }

    @Override
    public void onUserBasedChannelListUpdated() {
        dismiss();
        if (mBackendKnobsFlags.isEpgReady(true)) {
            startLiveTv(mKeyCode);
        }
    }

    private void startLiveTv(int keyCode) {
        Intent tvIntent = new Intent(keyCode == KeyEvent.KEYCODE_GUIDE ?
                PlaybackUtil.ACTION_START_GUIDE : PlaybackUtil.ACTION_START_LIVE);
        tvIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(tvIntent);
    }
}