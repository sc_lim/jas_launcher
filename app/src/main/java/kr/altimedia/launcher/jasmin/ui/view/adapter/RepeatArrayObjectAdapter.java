/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.adapter;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;

public class RepeatArrayObjectAdapter extends ArrayObjectAdapter {
    private final int REPEAT_CNT = 1000000;
    private final int VISIBLE_SIZE;

    public RepeatArrayObjectAdapter(Presenter presenter) {
        this(presenter, -1);
    }

    public RepeatArrayObjectAdapter(Presenter presenter, int visibleSize) {
        super(presenter);
        VISIBLE_SIZE = visibleSize;
    }


    @Override
    public int size() {
        int realSize = super.size();
        if (VISIBLE_SIZE > 0 && realSize <= VISIBLE_SIZE) {
            return realSize;
        }
        return realSize * REPEAT_CNT;
    }

    public int getRealSize() {
        if (VISIBLE_SIZE > 0 && size() <= VISIBLE_SIZE) {
            return size();
        }
        return size() / REPEAT_CNT;
    }

    public int getMidPosition() {
        int realSize = super.size();
        return realSize * (REPEAT_CNT / 2);
    }

    @Override
    public Object get(int index) {
        int realIndex = (index % super.size());
        return super.get(realIndex);
    }
}
