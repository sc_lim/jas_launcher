/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account.SettingAccountPinManageFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.autoplay.SettingSeriesAutoplayFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.closedcaption.SettingClosedCaptionFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.SettingDeviceManageFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.faq.SettingFAQFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingChannelVodMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingOtherMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu.SettingUserMenu;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.SettingProfileFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.subtitles.SettingSubtitlesFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system.SettingSystemInfoFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system.SettingSystemSettingFragment;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;

public class SettingMenuDialogFragment extends SettingBaseDialogFragment implements OnFragmentChange {
    public static final String CLASS_NAME = SettingMenuDialogFragment.class.getName();
    private final String TAG = SettingMenuDialogFragment.class.getSimpleName();

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private SettingMenuListFragment settingMenuListFragment;
    private FrameLayout frameLayout;

    private final int MESSAGE_ID = 0;
    private final long KEY_DELAY_TIME = 150;

    private final Handler keyHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (Log.INCLUDE) {
                Log.d(TAG, "call handleMessage");
            }

            getChildFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            SettingMenu menu = (SettingMenu) msg.obj;
            showMenu(menu);
        }
    };

    public SettingMenuDialogFragment() {

    }

    public static SettingMenuDialogFragment newInstance() {
        SettingMenuDialogFragment fragment = new SettingMenuDialogFragment();
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {

                SettingMenuListFragment menuListFragment = (SettingMenuListFragment) getChildFragmentManager().findFragmentById(R.id.setting_menu);
                boolean menuHasFocus = menuListFragment == null || menuListFragment.getView().hasFocus();
                if (Log.INCLUDE) {
                    Log.d(TAG, "menuHasFocus :" + menuHasFocus);
                }

                if (menuHasFocus) {
                    boolean isBackFragment = backToLastFragment();
                    if (!isBackFragment) {
                        super.onBackPressed();
                    }
                } else {
                    menuListFragment.getView().requestFocus();
                }
            }
        };
    }

    public boolean backToLastFragment() {
        FragmentManager manager = getChildFragmentManager();
        int stackCount = manager.getBackStackEntryCount();

        if (stackCount > 1) {
            manager.popBackStack();
            return true;
        }

        return false;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        frameLayout = view.findViewById(R.id.frame_menu);

        initProgressbar(view);
        addMenuList();
    }

    protected void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void addMenuList() {
        settingMenuListFragment = SettingMenuListFragment.newInstance(this);
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.setting_menu, settingMenuListFragment, SettingMenuListFragment.CLASS_NAME).commit();
    }

    private void showMenu(SettingMenu menu) {
        AttachedFragment mAttachedFragment = null;

        if (menu instanceof SettingUserMenu) {
            mAttachedFragment = getFragmentGroupByUser((SettingUserMenu) menu);
        } else if (menu instanceof SettingChannelVodMenu) {
            mAttachedFragment = getFragmentGroupByChannelVod((SettingChannelVodMenu) menu);
        } else if (menu instanceof SettingOtherMenu) {
            mAttachedFragment = getFragmentGroupByOthers((SettingOtherMenu) menu);
        }

        if (mAttachedFragment != null) {
            Fragment fragment = mAttachedFragment.getFragment();
            String tag = mAttachedFragment.getTag();

            showFragment(fragment, tag, false);
        }
    }

    private void showFragment(Fragment fragment, String tag, boolean addBackStack) {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();

        if (addBackStack) {
            ft.addToBackStack(tag);
        }

        ft.replace(R.id.frame_menu, fragment).commit();
    }

    private AttachedFragment getFragmentGroupByUser(SettingUserMenu menu) {
        switch (menu) {
            case ACCOUNT:
                SettingAccountPinManageFragment settingAccountPinManageFragment = SettingAccountPinManageFragment.newInstance();
                settingAccountPinManageFragment.setOnFragmentChange(this);
                return new AttachedFragment(settingAccountPinManageFragment, SettingAccountPinManageFragment.CLASS_NAME);
            case PROFILE:
                SettingProfileFragment settingProfilePinFragment = SettingProfileFragment.newInstance();
                settingProfilePinFragment.setOnFragmentChange(this);
                return new AttachedFragment(settingProfilePinFragment, SettingProfileFragment.CLASS_NAME);
        }

        return null;
    }

    private AttachedFragment getFragmentGroupByChannelVod(SettingChannelVodMenu menu) {
        switch (menu) {
            case SUBTITLES:
                SettingSubtitlesFragment settingSubtitlesFragment = SettingSubtitlesFragment.newInstance();
                settingSubtitlesFragment.setOnFragmentChange(this);
                return new AttachedFragment(settingSubtitlesFragment, SettingSubtitlesFragment.CLASS_NAME);

            case SERIES_AUTO_PLAY:
                SettingSeriesAutoplayFragment settingSeriesAutoplayFragment = SettingSeriesAutoplayFragment.newInstance();
                return new AttachedFragment(settingSeriesAutoplayFragment, SettingSeriesAutoplayFragment.CLASS_NAME);
        }

        return null;
    }

    private AttachedFragment getFragmentGroupByOthers(SettingOtherMenu menu) {
        switch (menu) {
            case DEVICE_MANAGE:
                SettingDeviceManageFragment settingDeviceManageFragment = SettingDeviceManageFragment.newInstance();
                settingDeviceManageFragment.setOnFragmentChange(this);
                return new AttachedFragment(settingDeviceManageFragment, SettingDeviceManageFragment.CLASS_NAME);
            case SYSTEM_SETTING:
                SettingSystemSettingFragment settingSystemSettingFragment = SettingSystemSettingFragment.newInstance();
                return new AttachedFragment(settingSystemSettingFragment, SettingSystemSettingFragment.CLASS_NAME);
            case SYSTEM_INFO:
                SettingSystemInfoFragment settingSystemInfoFragment = SettingSystemInfoFragment.newInstance();
                settingSystemInfoFragment.setOnFragmentChange(this);
                return new AttachedFragment(settingSystemInfoFragment, SettingSystemInfoFragment.CLASS_NAME);
            case FAQ:
                SettingFAQFragment settingFAQFragment = SettingFAQFragment.newInstance();
                settingFAQFragment.setOnFragmentChange(this);
                return new AttachedFragment(settingFAQFragment, SettingFAQFragment.CLASS_NAME);
        }

        return null;
    }


    @Override
    public void onChangeMenu(SettingMenu menu) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onChangeMenu : " + menu);
        }

        keyHandler.removeMessages(MESSAGE_ID);

        Message message = Message.obtain(keyHandler, MESSAGE_ID, menu);
        keyHandler.sendMessageDelayed(message, KEY_DELAY_TIME);
    }

    @Override
    public void onNextFragment(Fragment fragment, String tag) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onNextFragment : " + tag);
        }

        setMenuListFocusable(false);
        showFragment(fragment, tag, true);
    }

    @Override
    public void onClickMenu() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickMenu");
        }

        frameLayout.requestFocus();
    }

    @Override
    public void onUpdateMenuItem(int index, Object item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onUpdateMenuItem : " + item);
        }

        settingMenuListFragment.onUpdateMenuItem(index, item);
    }

    @Override
    public void onMenuFocus() {
        getDialog().onBackPressed();
    }

    @Override
    public void notifyFrameAttached() {
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyFrameAttached");
        }

        setMenuListFocusable(true);
    }

    @Override
    public SettingTaskManager getSettingTaskManager() {
        return new SettingTaskManager(getFragmentManager(), mTvOverlayManager);
    }

    @Override
    public void showProgressbar(boolean isLoading) {
        if (isLoading) {
            mProgressBarManager.show();
        } else {
            mProgressBarManager.hide();
        }
    }

    @Override
    public TvOverlayManager getTvOverlayManager() {
        return super.getTvOverlayManager();
    }

    private void setMenuListFocusable(Boolean isFocusable) {
        settingMenuListFragment.setFocusable(isFocusable);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        keyHandler.removeMessages(MESSAGE_ID);
    }

    private static class AttachedFragment {
        private Fragment fragment;
        private String tag;

        public AttachedFragment(Fragment fragment, String tag) {
            this.fragment = fragment;
            this.tag = tag;
        }

        public Fragment getFragment() {
            return fragment;
        }

        public String getTag() {
            return tag;
        }
    }
}
