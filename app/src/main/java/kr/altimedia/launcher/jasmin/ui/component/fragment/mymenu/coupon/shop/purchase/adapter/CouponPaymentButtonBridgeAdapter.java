/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.adapter;

import android.view.KeyEvent;
import android.view.View;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.presenter.CouponPaymentButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import com.altimedia.util.Log;

public class CouponPaymentButtonBridgeAdapter extends ItemBridgeAdapter {
    private static final String TAG = CouponPaymentButtonBridgeAdapter.class.getSimpleName();

    public CouponPaymentButtonBridgeAdapter() {
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        CouponPaymentButtonPresenter presenter = (CouponPaymentButtonPresenter) viewHolder.mPresenter;
        CouponPaymentButtonPresenter.OnKeyListener onKeyListener = presenter.getOnKeyListener();

        CouponPaymentButtonPresenter.ItemViewHolder itemViewHolder = (CouponPaymentButtonPresenter.ItemViewHolder) viewHolder.getViewHolder();

        int index = (int) viewHolder.itemView.getTag(R.id.KEY_INDEX);
        if (onKeyListener != null) {
            viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    PaymentType item = (PaymentType) viewHolder.mItem;
                    String name = v.getResources().getString(item.getName());
                    if(Log.INCLUDE) {
                        Log.d(TAG, "onKey() index=" + index +", payment type=" + item.getCode());
                    }
                    return onKeyListener.onKey(keyCode, index, item, itemViewHolder);
                }
            });
        }
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        viewHolder.mHolder.view.setOnKeyListener(null);
    }
}