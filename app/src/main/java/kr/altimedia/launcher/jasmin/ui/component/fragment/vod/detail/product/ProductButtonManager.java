/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product;

public class ProductButtonManager {
/*    private static final String TAG = ProductButtonManager.class.getSimpleName();
    private Context context;

    private ArrayList<Product> productList = new ArrayList<>();
    private ArrayList<Product> packageList = new ArrayList<>();
    private ArrayList<Product> subscriptionList = new ArrayList<>();

    private final ArrayList<ProductType> VIEW_LIMITED_PERIOD_PRIORITY = new ArrayList<>(
            Arrays.asList(ProductType.BUY, ProductType.SUBSCRIBE, ProductType.PACKAGE, ProductType.ALL_EPISODE, ProductType.RENT, ProductType.FREE));

    public ProductButtonManager(Context context) {
        this.context = context;
    }

    public ArrayList<Product> getPackageList() {
        return packageList;
    }

    public ArrayList<Product> getSubscriptionList() {
        return subscriptionList;
    }

    public ArrayObjectAdapter getProducts(Object content, ArrayList<Product> list) {
        productList.clear();
        packageList.clear();
        subscriptionList.clear();

        for (Product product : list) {
            if (product.getProductType() == ProductType.PACKAGE) {
                packageList.add(product);
                if (product.isPurchased()) {
                    productList.add(product);
                }
            } else if (product.getProductType() == ProductType.SUBSCRIBE) {
                if (product.isPurchased()) {
                    productList.add(product);
                } else {
                    subscriptionList.add(product);
                }
            } else {
                productList.add(product);
            }
        }

        ArrayObjectAdapter actionAdapter = getProductButtons(productList);

        if (packageList.size() > 0) {
            actionAdapter.add(getPackageButton(packageList));
        }

        if (subscriptionList.size() > 0) {
            ProductButtonItem productButtonItem = getSubscribeButtons(subscriptionList);
            if (productButtonItem != null) {
                actionAdapter.add(productButtonItem);
            }
        }

        //for My List
        actionAdapter.add(content);

        return actionAdapter;
    }

    public static String getWatchLeftUnit(Context context, Duration duration) {
        int value = duration.getValue();
        DurationUnit durationUnit = duration.getUnit();

        if (Log.INCLUDE) {
            Log.d(TAG, "getWatchLeftUnit, duration : " + duration);
        }

        int id;
        switch (durationUnit) {
            case UNIT_DAY:
                id = value > 1 ? R.string.days : R.string.day;
                break;
            case UNIT_MONTH:
                id = value > 1 ? R.string.months : R.string.month;
                break;
            case UNIT_YEAR:
                id = value > 1 ? R.string.years : R.string.year;
                break;
            default:
                return context.getString(R.string.vod_watch_days_unlimited);
        }

        return context.getString(id) + " " + context.getString(R.string.vod_watch_left);
    }

    private ProductButtonItem getSubscribeButtons(ArrayList<Product> subscribeList) {
        if (subscribeList.size() > 1) {
            String value = context.getString(R.string.subscriptions);
            return new ProductButtonItem(ProductType.SUBSCRIBE, subscribeList.size(), value);
        } else {
            Product product = subscribeList.get(0);
            if (product.isPurchased()) {
                return null;
            } else {
                return new ProductButtonItem(product.getProductType(), R.string.vod_subscribing, null);
            }
        }
    }

    private ArrayObjectAdapter getProductButtons(ArrayList<Product> productList) {
        if (Log.INCLUDE) {
            Log.d(TAG, "getProductButtons, productList : " + productList.size());
        }

        ArrayObjectAdapter actionAdapter = new ArrayObjectAdapter();

        ArrayList<Product> purchasedProductList = new ArrayList<>();
        ArrayList<ProductButtonItem> buttonList = new ArrayList();

        for (int i = 0; i < productList.size(); i++) {
            Product product = productList.get(i);
            ProductType productType = product.getProductType();
            int price = product.getPrice();

            if (product.isPurchased() || price == 0) {
                purchasedProductList.add(product);

                if (productType == ProductType.PACKAGE || productType == ProductType.SUBSCRIBE) {
                    continue;
                }
            } else {
                buttonList.add(new ProductButtonItem(product, product.getProductType(), product.getPrice(), context.getString(R.string.thb)));
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "getProductButtons, purchasedProductList : " + purchasedProductList.size());
        }

        ProductButtonItem watchButton = null;
        if (purchasedProductList.size() > 0) {
            watchButton = getWatchButton(context, purchasedProductList);
            if (Log.INCLUDE) {
                Log.d(TAG, "getProductButtons, watchButton : " + watchButton);
            }
            actionAdapter.add(0, watchButton);
        }

        //Rent 이외 구매된 이력이 있으면 Rent 버튼은 노출하지 않는다.
        if (watchButton != null) {
            if (watchButton.getProductType() != ProductType.RENT) {
                for (ProductButtonItem productButtonItem : buttonList) {
                    ProductType type = productButtonItem.getProductType();
                    if (type == ProductType.RENT) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getProductButtons, rentButton is removed : " + watchButton);
                        }
                        buttonList.remove(productButtonItem);
                        break;
                    }
                }
            }
        }

        actionAdapter.addAll(actionAdapter.size(), buttonList);
        return actionAdapter;
    }

    private ProductButtonItem getWatchButton(Context context, ArrayList<Product> purchasedProductList) {
        Product[] arrangedList = new Product[VIEW_LIMITED_PERIOD_PRIORITY.size()];

        for (int i = 0; i < purchasedProductList.size(); i++) {
            Product product = purchasedProductList.get(i);
            ProductType productType = product.getProductType();

            if (Log.INCLUDE) {
                Log.d(TAG, "getWatchButton, productType : " + productType);
            }

            int index = VIEW_LIMITED_PERIOD_PRIORITY.indexOf(productType);
            arrangedList[index] = product;
        }

        for (int j = 0; j < arrangedList.length; j++) {
            if (arrangedList[j] != null) {
                Product product = arrangedList[j];
                return getButtonItem(product, context);
            }
        }

        return null;
    }

    private ProductButtonItem getPackageButton(ArrayList<Product> packageList) {
        if (packageList.size() > 1) {
            String value = context.getString(R.string.packages);
            return new ProductButtonItem(ProductType.PACKAGE, packageList.size(), value);
        } else {
            Product product = packageList.get(0);
            if (product.isPurchased()) {
                Duration duration = product.getDuration();
                String unit = getWatchLeftUnit(context, duration);
                return new ProductButtonItem(product, product.getProductType(), duration.getValue(), unit);
            } else {
                return new ProductButtonItem(product, product.getProductType(), product.getPrice(), product.getCurrency());
            }
        }
    }

    private ProductButtonItem getButtonItem(Product product, Context context) {
        ProductType productType = product.getProductType();

        if (Log.INCLUDE) {
            Log.d(TAG, "getValueUnit, product : " + product + ", productType : " + productType);
        }

        int valueId = -1;

        if (product.getPrice() == 0) {
            String unit = context.getString(R.string.vod_free);
            return new ProductButtonItem(ProductType.WATCH, -1, unit);
        }

        switch (productType) {
            case BUY:
                valueId = R.string.vod_watch_days_unlimited;
                break;
            case SUBSCRIBE:
                valueId = R.string.vod_subscribing;
                break;
            default:
                Duration duration = product.getDuration();
                String unit = getWatchLeftUnit(context, duration);
                return new ProductButtonItem(product, ProductType.WATCH, duration.getValue(), unit);
        }

        String unit = context.getString(valueId);
        return new ProductButtonItem(ProductType.WATCH, -1, unit);
    }*/
}
