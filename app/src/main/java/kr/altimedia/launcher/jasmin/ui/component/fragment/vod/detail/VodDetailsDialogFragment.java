/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamType;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.Membership;
import kr.altimedia.launcher.jasmin.dm.user.object.ResumeTimeResult;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.packages.PackageVodFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.EpisodeVodVerticalFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.SeriesVodFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.single.SingleVodFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.PackageVodSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.VodResumeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.dialog.VodTrailerSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.listenter.OnVodViewChangeListener;
import kr.altimedia.launcher.jasmin.ui.util.task.UserTaskManager;
import kr.altimedia.launcher.jasmin.ui.util.task.VodTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATE_VOD_FAVORITE;


public class VodDetailsDialogFragment extends SafeDismissDialogFragment
        implements OnVodViewChangeListener, UserTaskManager.OnUpdateUserDiscountListener {
    public enum VodDetailType {
        UNDEFINED, SINGLE, SERIES, PACKAGE
    }

    public static final String CLASS_NAME = VodDetailsDialogFragment.class.getName();
    public static final String TAG = VodDetailsDialogFragment.class.getSimpleName();
    private static final String KEY_CATEGORY_ID = "CATEGORY_ID";
    private static final String KEY_CONTENT = "CONTENT";
    private static final String KEY_CONTENT_ID = "CONTENT_ID";
    private static final String KEY_SERIES_ASSET_ID = "SERIES_ASSET_ID";
    private static final String KEY_DETAIL_TYPE = "DETAIL_TYPE";
    private static final String KEY_COUNT = "COUNT";
    private static final String KEY_PACKAGE_PRODUCT_ID = "PACKAGE_PRODUCT_ID";
    private static final String KEY_PACKAGE_RATING = "PACKAGE_RATING";

    private static final int VOD_OVERLAY_MAX = 10;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private final UserTaskManager userTaskManager = UserTaskManager.getInstance();

    private int visibleCount = 0;
    private String categoryId;
    private String contentId;

    public VodDetailsDialogFragment() {
    }

    public static VodDetailsDialogFragment newInstance(String seriesAssetId, String categoryId, String contentId, int visibleCount) {
        Bundle args = new Bundle();

        args.putString(KEY_SERIES_ASSET_ID, seriesAssetId);
        args.putString(KEY_CATEGORY_ID, categoryId);
        args.putSerializable(KEY_DETAIL_TYPE, VodDetailType.SERIES);
        args.putString(KEY_CONTENT_ID, contentId);
        args.putInt(KEY_COUNT, visibleCount);

        VodDetailsDialogFragment fragment = new VodDetailsDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static VodDetailsDialogFragment newInstance(Content content, int visibleCount) {
        return VodDetailsDialogFragment.newInstance(content, VodDetailType.UNDEFINED, visibleCount);
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private static VodDetailsDialogFragment newInstance(Content content, VodDetailType type, int visibleCount) {
        if (type == VodDetailType.UNDEFINED) {
            type = content.isSeries() ? VodDetailType.SERIES : VodDetailType.SINGLE;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "showVodDetail : newInstance : " + content);
        }
        Bundle args = new Bundle();
        args.putString(KEY_SERIES_ASSET_ID, content.getSeriesAssetId());
        args.putParcelable(KEY_CONTENT, content);
        args.putString(KEY_CONTENT_ID, content.getContentGroupId());
        args.putString(KEY_CATEGORY_ID, content.getCategoryId());
        args.putSerializable(KEY_DETAIL_TYPE, type);
        args.putInt(KEY_COUNT, visibleCount);

        VodDetailsDialogFragment fragment = new VodDetailsDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static VodDetailsDialogFragment newInstance(String productId, String categoryId, int rating, int visibleCount) {
        Bundle args = new Bundle();

        args.putString(KEY_PACKAGE_PRODUCT_ID, productId);
        args.putString(KEY_CATEGORY_ID, categoryId);
        args.putSerializable(KEY_DETAIL_TYPE, VodDetailType.PACKAGE);
        args.putInt(KEY_PACKAGE_RATING, rating);
        args.putInt(KEY_COUNT, visibleCount);

        VodDetailsDialogFragment fragment = new VodDetailsDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_vod_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        initProgressbar(view);

        Bundle bundle = getArguments();
        visibleCount = bundle.getInt(KEY_COUNT);
        categoryId = bundle.getString(KEY_CATEGORY_ID);
        contentId = bundle.getString(KEY_CONTENT_ID);
        VodDetailType type = (VodDetailType) bundle.getSerializable(KEY_DETAIL_TYPE);
        Object object = null;

        switch (type) {
            case SINGLE:
                object = bundle.getParcelable(KEY_CONTENT);
                break;
            case SERIES:
                object = bundle.getString(KEY_SERIES_ASSET_ID);
                break;
            case PACKAGE:
                object = bundle.getString(KEY_PACKAGE_PRODUCT_ID);
                break;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "initView, type : " + type + ", visibleCount : " + visibleCount + ", object : " + object);
        }

        showDetailFragment(categoryId, contentId, type, object);

        if (visibleCount == 1) {
            userTaskManager.addUserDiscountListener(this);
            loadUserDiscountInfo();
        }
    }

    public void loadUserDiscountInfo() {
        //for init & update after purchase vod
        if (visibleCount == 1) {
            userTaskManager.loadUserDiscountInfo(getContext());
        }
    }

    private void showDetailFragment(String categoryId, String contentId, VodDetailType type, Object object) {
        int rating = 0;
        if (type == VodDetailType.SINGLE && object instanceof Content) {
            Content content = (Content) object;
            rating = content.getRating();
        } else if (type == VodDetailType.PACKAGE) {
            rating = getArguments().getInt(KEY_PACKAGE_RATING);
        }

        boolean isOverRating = PlaybackUtil.isVodOverRating(rating);

        if (Log.INCLUDE) {
            Log.d(TAG, "showDetailFragment, type : " + type + ", isOverRating : " + isOverRating);
        }

        if (isOverRating) {
            attachPinFragment(categoryId, contentId, type, object);
        } else {
            attachFragment(categoryId, contentId, type, object);
        }
    }


    private void attachPinFragment(String categoryId, String contentId, VodDetailType type, Object object) {
        if (Log.INCLUDE) {
            Log.d(TAG, "attachPinFragment, object : " + object);
        }

        VodRatingPinFragment vodRatingPinFragment = VodRatingPinFragment.newInstance();
        vodRatingPinFragment.setOnCompleteCheckPinListener(new VodRatingPinFragment.onCompleteCheckPinListener() {
            @Override
            public void setProgress(boolean isShow) {
                if (isShow) {
                    showProgress();
                } else {
                    hideProgress();
                }
            }

            @Override
            public void onCompleteCheckPin(boolean isSuccess) {
                if (isSuccess) {
                    attachFragment(categoryId, contentId, type, object);
                } else {
                    dismiss();
                }
            }
        });

        getChildFragmentManager().beginTransaction()
                .replace(R.id.detailFrame, vodRatingPinFragment, VodRatingPinFragment.CLASS_NAME).commit();
    }

    private void attachFragment(String categoryId, String contentId, VodDetailType type, Object object) {
        if (Log.INCLUDE) {
            Log.d(TAG, "attachFragment, type : " + type + ", object : " + object);
        }

        showProgress();

        DetailFragment fragment;
        String tag = "";

        switch (type) {
            case SINGLE:
                fragment = SingleVodFragment.newInstance((Content) object);
                tag = SingleVodFragment.CLASS_NAME;
                break;
            case SERIES:
                fragment = SeriesVodFragment.newInstance((String) object, categoryId, contentId);
                tag = SeriesVodFragment.CLASS_NAME;
                break;
            case PACKAGE:
                String productId = (String) object;
                fragment = PackageVodFragment.newInstance(productId, categoryId);
                tag = PackageVodFragment.CLASS_NAME;
                break;
            default:
                throw new IllegalStateException("Unexpected type: " + type);
        }

        fragment.setChangeVodDetailListener(this);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.detailFrame, fragment, tag).addToBackStack(tag).commit();
    }

    private void showVodDetailDialog(VodDetailType type, Content content, int visibleCount) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showVodDetailDialog, visibleCount : " + visibleCount);
        }

        if (mTvOverlayManager == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showVodDetailDialog mTvOverlayManager == null so return");
            }
            return;
        }

        mTvOverlayManager.showDialogFragment(VodDetailsDialogFragment.newInstance(content, type, visibleCount));
    }

    private void toastIgnoreShowDetail() {
        JasminToast.makeToast(getContext(), R.string.vod_detail_max_ignore);
    }

    @Override
    public void replaceDetail(Content content) {
        if (Log.INCLUDE) {
            Log.d(TAG, "replaceDetail, content : " + content);
        }

        if (visibleCount == VOD_OVERLAY_MAX) {
            if (Log.INCLUDE) {
                Log.d(TAG, "replaceDetail, visibleCount : " + visibleCount + ", visibleCount == VOD_OVERLAY_MAX, so return");
            }

            toastIgnoreShowDetail();
            return;
        }

        showVodDetailDialog(VodDetailType.UNDEFINED, content, visibleCount + 1);
    }

    @Override
    public void showPackage(String categoryId, ArrayList<Product> packageMainList) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showPackage");
        }

        if (visibleCount == VOD_OVERLAY_MAX) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showPackage, visibleCount : " + visibleCount + ", visibleCount == VOD_OVERLAY_MAX, so return");
            }

            toastIgnoreShowDetail();
            return;
        }

        if (packageMainList != null && packageMainList.size() > 1) {
            PackageVodSelectDialogFragment packageVodSelectDialogFragment = PackageVodSelectDialogFragment.newInstance(packageMainList);
            packageVodSelectDialogFragment.setOnClickPackageListener(new PackageVodSelectDialogFragment.OnClickPackageListener() {
                @Override
                public void onClickPackage(Product product) {
                    checkPackageRating(product, categoryId, packageVodSelectDialogFragment);
                }
            });
            mTvOverlayManager.showDialogFragment(packageVodSelectDialogFragment);
        } else {
            Product product = packageMainList.get(0);
            checkPackageRating(product, categoryId, null);
        }
    }

    private void checkPackageRating(Product product, String categoryId, PackageVodSelectDialogFragment packageVodSelectDialogFragment) {
        if (Log.INCLUDE) {
            Log.d(TAG, "checkPackageRating, product : " + product);
        }

        boolean isOverRating = PlaybackUtil.isVodOverRating(product.getRating());
        if (isOverRating) {
            VodRatingPinDialogFragment mVodRatingPinDialogFragment = VodRatingPinDialogFragment.newInstance();
            mVodRatingPinDialogFragment.setOnCompleteCheckPinListener(new VodRatingPinFragment.onCompleteCheckPinListener() {
                @Override
                public void setProgress(boolean isShow) {
                    if (isShow) {
                        showProgress();
                    } else {
                        hideProgress();
                    }
                }

                @Override
                public void onCompleteCheckPin(boolean isSuccess) {
                    mVodRatingPinDialogFragment.dismiss();
                    if (isSuccess) {
                        if (packageVodSelectDialogFragment != null) {
                            packageVodSelectDialogFragment.dismiss();
                        }
                        showPackageDetailFragment(product, categoryId);
                    }
                }
            });
            mTvOverlayManager.showDialogFragment(mVodRatingPinDialogFragment);
        } else {
            if (packageVodSelectDialogFragment != null) {
                packageVodSelectDialogFragment.dismiss();
            }

            showPackageDetailFragment(product, categoryId);
        }
    }

    private void showPackageDetailFragment(Product product, String categoryId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showPackageDetailFragment");
        }

        if (mTvOverlayManager == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showPackageDetailFragment mTvOverlayManager == null so return");
            }
            return;
        }

        VodDetailsDialogFragment vodDetailsDialogFragment = VodDetailsDialogFragment.newInstance(product.getProductId(), categoryId, 0, visibleCount + 1);
        mTvOverlayManager.showDialogFragment(vodDetailsDialogFragment);
    }

    private void refreshResumeTime(Content content, Handler handler) {
        if (Log.INCLUDE) {
            Log.d(TAG, "refreshResumeTime, content : " + content);
        }

        AccountManager accountManager = AccountManager.getInstance();
        UserDataManager userDataManager = new UserDataManager();
        userDataManager.getResumeTime(accountManager.getSaId(),
                accountManager.getProfileId(), content.getContentGroupId(),
                new MbsDataProvider<String, ResumeTimeResult>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        handler.sendEmptyMessage(0);
                    }

                    @Override
                    public void onSuccess(String id, ResumeTimeResult result) {
                        content.setResumeTime(result.getResumeTime());
                        handler.sendEmptyMessage(0);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        handler.sendEmptyMessage(0);
                    }
                });
    }

    private boolean isDuplicatedKeyBlock = false;
    private final Handler duplicatedBlock = new Handler();
    private final Runnable keyBlockRun = new Runnable() {
        @Override
        public void run() {
            isDuplicatedKeyBlock = false;
        }
    };
    private final long KEY_BLOCK_TIME = 500;

    @Override
    public void watchMainVideo(Content content) {
        if (Log.INCLUDE) {
            Log.d(TAG, "watchMainVideo, content : " + content);
        }
        if (isDuplicatedKeyBlock) {
            return;
        }

        isDuplicatedKeyBlock = true;
        duplicatedBlock.removeCallbacks(keyBlockRun);
        duplicatedBlock.postDelayed(keyBlockRun, KEY_BLOCK_TIME);

        refreshResumeTime(content, new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);

                Bundle bundle = new Bundle();
                bundle.putSerializable(VideoPlaybackActivity.KEY_TYPE, VideoPlaybackActivity.PlayerType.Movie);
                bundle.putParcelable(VideoPlaybackActivity.KEY_MOVIE, content);
                if (content.getResumeTime() != 0) {
                    final VodResumeDialogFragment vodResumeDialogFragment = VodResumeDialogFragment.newInstance(content, new VodResumeDialogFragment.OnButtonsAction() {
                        @Override
                        public void onClickButtons(DialogFragment dialogFragment, @VodResumeDialogFragment.ResumeType int position) {
                            dialogFragment.dismiss();
                            switch (position) {
                                case VodResumeDialogFragment.BEGINNING:
                                    requestTuneVod(bundle);
                                    break;

                                case VodResumeDialogFragment.RESUME:
                                    bundle.putLong(VideoPlaybackActivity.KEY_START_TIME, content.getResumeTime());
                                    requestTuneVod(bundle);
                                    break;

                                case VodResumeDialogFragment.CANCEL:
                                    break;
                            }
                        }
                    });
                    mTvOverlayManager.showDialogFragment(vodResumeDialogFragment);
                } else {
                    requestTuneVod(bundle);
                }


            }
        });

    }

    @Override
    public void watchPreviewVideo(Parcelable object) {
        if (Log.INCLUDE) {
            Log.d(TAG, "watchPreviewVideo, object : " + object);
        }
        if (isDuplicatedKeyBlock) {
            return;
        }

        isDuplicatedKeyBlock = true;
        duplicatedBlock.removeCallbacks(keyBlockRun);
        duplicatedBlock.postDelayed(keyBlockRun, KEY_BLOCK_TIME);
        tryWatchTrailerVideo(object);
    }

    @Override
    public void watchTrailerVideo(Parcelable object) {
        if (Log.INCLUDE) {
            Log.d(TAG, "watchTrailerVideo, object : " + object);
        }
        if (isDuplicatedKeyBlock) {
            return;
        }

        isDuplicatedKeyBlock = true;
        duplicatedBlock.removeCallbacks(keyBlockRun);
        duplicatedBlock.postDelayed(keyBlockRun, KEY_BLOCK_TIME);
        tryWatchTrailerVideo(object);
    }

    private void tryWatchTrailerVideo(Parcelable object) {
        if (Log.INCLUDE) {
            Log.d(TAG, "watchPreview, object : " + object);
        }
        if (object == null) {
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putParcelable(VideoPlaybackActivity.KEY_MOVIE, object);

        boolean isContent = object instanceof Content;
        boolean isSeries = object instanceof SeriesContent;
        if (!isContent && !isSeries) {
            return;
        }

        final List<StreamInfo> streamInfoList;
        if (isContent) {
            streamInfoList = ((Content) object).getTrailerStreamInfo();
        } else {
            streamInfoList = ((SeriesContent) object).getTrailer();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, " streamInfo : " + streamInfoList);
        }
        if (streamInfoList == null) {
            return;
        }
        int trailerCount = streamInfoList.size();
        if (trailerCount == 0) {
            //TODO:implements later
        } else if (trailerCount == 1) {
            bundle.putInt(VideoPlaybackActivity.KEY_MOVIE_INDEX, 0);
            boolean preview = streamInfoList.get(0).getStreamType() == StreamType.Preview;
            bundle.putSerializable(VideoPlaybackActivity.KEY_TYPE, preview ?
                    VideoPlaybackActivity.PlayerType.Preview : VideoPlaybackActivity.PlayerType.Trailer);
            requestTuneVod(bundle);
        } else if (trailerCount >= 2) {
            VodTrailerSelectDialogFragment vodTrailerSelectDialogFragment = VodTrailerSelectDialogFragment.newInstance(object,
                    new ArrayList(streamInfoList));
            vodTrailerSelectDialogFragment.setOnVodViewChangeListener(this);
            mTvOverlayManager.showDialogFragment(vodTrailerSelectDialogFragment);
        }
    }

    @Override
    public void requestTuneVod(Bundle bundle) {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestTuneVod");
        }

        Intent intent = new Intent(PlaybackUtil.ACTION_START_VOD);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtras(bundle);
        startActivityForResult(intent, PlaybackUtil.REQUEST_CODE_VOD);
    }

    @Override
    public void onVodLikeChange(boolean isLike) {
        if (Log.INCLUDE) {
            Log.d(TAG, "vodLikeChange, isLike : " + isLike);
        }

        Intent mIntent = new Intent(getContext(), PushMessageReceiver.class);
        mIntent.setAction(ACTION_UPDATE_VOD_FAVORITE);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(mIntent);
    }

    @Override
    public TvOverlayManager getTvOverlayManager() {
        return super.getTvOverlayManager();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Log.INCLUDE) {
            Log.d(TAG, "onActivityResult | requestCode : " + requestCode + ", resultCode : " + resultCode);
        }
        if (resultCode != LauncherActivity.RESULT_CODE_SHOW_VOD_DETAIL
                && resultCode != LauncherActivity.RESULT_CODE_WATCH_FINISHED
                && resultCode != LauncherActivity.RESULT_CODE_ERROR_READY) {
            return;
        }

        Content content = data.getParcelableExtra(LauncherActivity.KEY_VOD_DETAIL);
        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.detailFrame);
        DetailFragment detailFragment = (fragment instanceof DetailFragment) ? (DetailFragment) fragment : null;

        if (Log.INCLUDE) {
            Log.d(TAG, "onActivityResult | content : " + content.toString());
        }

        if (resultCode == LauncherActivity.RESULT_CODE_SHOW_VOD_DETAIL
                || resultCode == LauncherActivity.RESULT_CODE_WATCH_FINISHED) {

            scrollNextEpisode(content);

            if (detailFragment != null) {
                detailFragment.setContentLike(content);
            }
        } else if (resultCode == LauncherActivity.RESULT_CODE_ERROR_READY) {
            String errorCode = data.getStringExtra(LauncherActivity.KEY_ERROR_CODE);
            if (Log.INCLUDE) {
                Log.d(TAG, "onActivityResult RESULT_CODE_ERROR_READY | errorCode : " + errorCode);
                Log.d(TAG, "onActivityResult RESULT_CODE_ERROR_READY | isNotPurchasedContents : " + PlaybackUtil.isNotPurchasedContents(errorCode));
            }

            if (detailFragment != null) {
                detailFragment.updateDetail(content);
            }
        }
    }

    private void scrollNextEpisode(Content content) {
        if (Log.INCLUDE) {
            Log.d(TAG, "changeNextEpisodeDetail");
        }

        DetailFragment detailFragment = (DetailFragment) getChildFragmentManager().findFragmentById(R.id.detailFrame);
        if (detailFragment != null && detailFragment instanceof SeriesVodFragment) {
            SeriesVodFragment seriesVodFragment = (SeriesVodFragment) detailFragment;
            EpisodeVodVerticalFragment episodeVodVerticalFragment = (EpisodeVodVerticalFragment) seriesVodFragment.getChildFragmentManager().findFragmentById(R.id.details_left_rows_dock);

            if (episodeVodVerticalFragment != null) {
                boolean hasEpisode = episodeVodVerticalFragment.scrollAndFocusEpisode(content.getContentGroupId());
                if (Log.INCLUDE) {
                    Log.d(TAG, "changeNextEpisodeDetail, hasEpisode : " + hasEpisode);
                }
            }
        }
    }

    protected void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    public void showProgress() {
        if (Log.INCLUDE) {
            Log.d(TAG, "showProgress");
        }

        mProgressBarManager.show();
    }

    public void hideProgress() {
        if (Log.INCLUDE) {
            Log.d(TAG, "hideProgress");
        }

        if (mProgressBarManager != null) {
            mProgressBarManager.hide();
        }
    }

    @Override
    public void notifyUserDiscountChange(int pointCouponSize, int discountCouponSize, ArrayList<DiscountCoupon> discountCoupons, Membership membership) {
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyUserDiscountChange");
        }

        Fragment fragment = getChildFragmentManager().findFragmentById(R.id.detailFrame);
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyUserDiscountChange, fragment : " + fragment);
        }

        if (fragment instanceof DetailFragment) {
            DetailFragment detailFragment = (DetailFragment) fragment;
            VodTaskManager vodTaskManager = detailFragment.getVodTaskManager();

            if (vodTaskManager != null) {
                if (vodTaskManager.isEmpty()) {
                    hideProgress();
                }

                return;
            }

            hideProgress();
        }
    }

    @Override
    public void notifyFail(int key) {
        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getContext());
        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
    }

    @Override
    public void notifyError(String errorCode, String message) {
        ErrorDialogFragment dialogFragment =
                ErrorDialogFragment.newInstance(TAG, errorCode, message);
        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
    }

    @Override
    public void onDestroyView() {
        duplicatedBlock.removeCallbacks(keyBlockRun);
        userTaskManager.cancelAllTask();
        userTaskManager.removeUserDiscountListener(this);

        if (mProgressBarManager != null) {
            mProgressBarManager.hide();
            mProgressBarManager.setRootView(null);
        }

        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Log.INCLUDE) {
            Log.d(TAG, "onResume");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Log.INCLUDE) {
            Log.d(TAG, "onPause");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Log.INCLUDE) {
            Log.d(TAG, "onStart");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Log.INCLUDE) {
            Log.d(TAG, "onStop");
        }
    }
}
