package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;

public class CreditCardPresenter extends Presenter {

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_credit_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((CreditCard) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private ImageView logo;
        private TextView cardNum;

        public ViewHolder(View view) {
            super(view);

            initView(view);
        }

        private void initView(View view) {
            logo = view.findViewById(R.id.logo);
            cardNum = view.findViewById(R.id.card_number);
        }

        public void setData(CreditCard creditCard) {
            String name = creditCard.getCardNumber();
            cardNum.setText(name.substring(0, 4));

            String logoUri = creditCard.getCreditCardLogo();
            if (logoUri != null && !logoUri.isEmpty()) {
                Glide.with(view.getContext())
                        .load(logoUri).diskCacheStrategy(DiskCacheStrategy.DATA)
                        .centerCrop()
                        .into(logo);
            }
        }
    }
}
