/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.util.GenreUtil;

/**
 * Adapts the genre items obtained from {@link GenreUtil} to the program guide side panel.
 */
class GenreListAdapter extends RecyclerView.Adapter<GenreListAdapter.GenreRowHolder> {
    private static final String TAG = "GenreListAdapter";
    public static final String CANONICAL_NAME = GenreListAdapter.class.getCanonicalName();
    private static final boolean DEBUG = false;

    private final Context mContext;
    private final ProgramManager mProgramManager;
    private final ProgramGuide mProgramGuide;
    private ArrayList<String> mGenreLabels = new ArrayList<>();
    //    private String[] mGenreLabels;
    private static int mSelectedGenreId = 0;

    GenreListAdapter(Context context, ProgramManager programManager, ProgramGuide guide) {
        mContext = context;
        initialGenreLabels(GenreUtil.getCanonicalGenres());
        mProgramManager = programManager;
        mProgramManager.addListener(
                new ProgramManager.ListenerAdapter() {
                    @Override
                    public void onGenresUpdated(int selectedGenreId) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onGenresUpdated : " + selectedGenreId);
                        }
                        mSelectedGenreId = selectedGenreId;
                        notifyDataSetChanged();
                    }
                });
        mProgramGuide = guide;
    }

    private void initialGenreLabels(String[] genreLabels) {
        mGenreLabels.clear();
        if (genreLabels != null) {
            for (String label : genreLabels) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "label : " + label);
                }
                mGenreLabels.add(label);
            }
        }
    }


    @Override
    public int getItemCount() {
        return mGenreLabels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.program_guide_side_panel_row;
    }

    @Override
    public void onBindViewHolder(GenreRowHolder holder, int position) {
        holder.onBind(holder.itemView, mGenreLabels.get(position), position, mGenreLabels.size());
    }

    @Override
    public void onViewRecycled(@NonNull GenreRowHolder holder) {
        super.onViewRecycled(holder);
        holder.unBind(holder.itemView);
    }

    @Override
    public GenreRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        itemView.addOnAttachStateChangeListener(
                new View.OnAttachStateChangeListener() {
                    @Override
                    public void onViewAttachedToWindow(View view) {
                        // Animation is not meaningful now, skip it.
//                        view.getStateListAnimator().jumpToCurrentState();
                    }

                    @Override
                    public void onViewDetachedFromWindow(View view) {
                        // Do nothing
                    }
                });
        return new GenreRowHolder(itemView, mProgramGuide);
    }


    static class GenreRowHolder extends RecyclerView.ViewHolder
            implements View.OnFocusChangeListener, View.OnClickListener, View.OnKeyListener {
        private final ProgramGuide mProgramGuide;
        private final TextView filterName;
        private final ImageView filterSelected;
        private String mGenreCanonical;

        @MainThread
        GenreRowHolder(View itemView, ProgramGuide programGuide) {
            super(itemView);
            mProgramGuide = programGuide;
            filterName = itemView.findViewById(R.id.filterName);
            filterSelected = itemView.findViewById(R.id.filterSelected);
        }

        private int mMaxSize = 0;

        public void onBind(View view, String canonical, int position, int maxSize) {
            mGenreCanonical = canonical;
            mMaxSize = maxSize;
            Context context = view.getContext();
            if (Log.INCLUDE) {
                Log.d(TAG, "onBind : " + mGenreCanonical);
            }
            int selectedId = GenreUtil.getId(mGenreCanonical);
            if (Log.INCLUDE) {
                Log.d(TAG, "onBind selectedId : " + selectedId);
            }
            filterSelected.setVisibility(mSelectedGenreId == selectedId ? View.VISIBLE : View.INVISIBLE);
            filterName.setText(GenreUtil.getGenreLabel(context, selectedId));
            itemView.setOnFocusChangeListener(this);
            itemView.setOnClickListener(this);
//            if (position == 0) {
            itemView.setOnKeyListener(this);
//            }
        }

        public void unBind(View view) {
            itemView.setOnKeyListener(null);
        }


        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                int oldPosition = getAdapterPosition();
                if (Log.INCLUDE) {
                    Log.d(TAG, "old position : " + oldPosition);
                }
                if (oldPosition == 0 && keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                    mProgramGuide.notifyOutKey(CANONICAL_NAME);
                    return true;
                } else if (oldPosition == mMaxSize-1
                        && keyCode == KeyEvent.KEYCODE_DPAD_DOWN){
                    mProgramGuide.setCategorySelection(0);
                    return true;
                }else if(keyCode == KeyEvent.KEYCODE_DPAD_RIGHT){
                    if(mProgramGuide.requestGridFocus()){
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public void onClick(View view) {
            mProgramGuide.requestGenreChange(GenreUtil.getId(mGenreCanonical));
        }

        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {

            }
        }
    }

}
