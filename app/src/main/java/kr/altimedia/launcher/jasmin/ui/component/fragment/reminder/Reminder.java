/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.reminder;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class Reminder implements Parcelable, Comparable<Reminder> {
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("(MM/dd)HH:mm");

    private String serviceId;
    private long channelId;
    private String channelName;

    private String programId;
    private String title;

    private long startTime;
    private long endTime;

    private transient Program program;

    private static long getChannelId(String serviceId) {
        Channel channel = JasChannelManager.getInstance().getChannel(serviceId);
        return channel != null ? channel.getId() : -1;
    }

    private static String getChannelName(String serviceId) {
        Channel channel = JasChannelManager.getInstance().getChannel(serviceId);
        return channel != null ? channel.getDisplayName() : null;
    }

    public Reminder(String serviceId, String programId, String title, long startTime, long endTime) {
        this.serviceId = serviceId;
        this.channelId = getChannelId(serviceId);
        this.channelName = getChannelName(serviceId);
        this.programId = programId;
        this.title = title;
        this.startTime = startTime;
        this.endTime = endTime;

        program = null;
    }

    public Reminder(Program program) {
        this.serviceId = String.valueOf(program.getServiceId());
        this.channelId = getChannelId(serviceId);
        this.channelName = getChannelName(serviceId);
        this.programId = program.getProgramId();
        this.title = program.getTitle();
        this.startTime = program.getStartTimeUtcMillis();
        this.endTime = program.getEndTimeUtcMillis();

        this.program = program;
    }

    protected Reminder(Parcel in) {
        serviceId = in.readString();
        channelId = in.readLong();
        channelName = in.readString();
        programId = in.readString();
        title = in.readString();
        startTime = in.readLong();
        endTime = in.readLong();
    }

    public void updateProgram(Program program) {
        this.serviceId = String.valueOf(program.getServiceId());
        this.channelId = getChannelId(serviceId);
        this.channelName = getChannelName(serviceId);
        this.programId = program.getProgramId();
        this.title = program.getTitle();
        this.startTime = program.getStartTimeUtcMillis();
        this.endTime = program.getEndTimeUtcMillis();

        this.program = program;
    }

    public static final Creator<Reminder> CREATOR = new Creator<Reminder>() {
        @Override
        public Reminder createFromParcel(Parcel in) {
            return new Reminder(in);
        }

        @Override
        public Reminder[] newArray(int size) {
            return new Reminder[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceId);
        dest.writeLong(channelId);
        dest.writeString(channelName);
        dest.writeString(programId);
        dest.writeString(title);
        dest.writeLong(startTime);
        dest.writeLong(endTime);
    }

    public String getServiceId() {
        return serviceId;
    }

    public long getChannelId() {
        return channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public String getChannelNumber() {
        return StringUtil.getFormattedNumber(serviceId, 3);
    }

    public String getProgramId() {
        return programId;
    }

    public String getTitle() {
        return title;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public Program getProgram() {
        return program;
    }

    public static Reminder createFromJson(String json) {
        Reminder reminder = new Gson().fromJson(json, Reminder.class);
        return reminder;
    }

    public String toJson() {
        String json = new Gson().toJson(this);
        return json;
    }

    public boolean equals(Program program) {
        boolean channelIdlEquals = Objects.equals(channelId, program.getChannelId());
        boolean programIdEquals = Objects.equals(programId, program.getProgramId());
        boolean startTimeEquals = startTime == program.getStartTimeUtcMillis();
        boolean endTimeEqual = endTime == program.getEndTimeUtcMillis();
        boolean equals = channelIdlEquals && programIdEquals && startTimeEquals && endTimeEqual;

        if (equals && this.program == null) {
            updateProgram(program);
        }

        return equals;
    }

    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null || this.getClass() != that.getClass()) return false;
        return equals((Reminder)that);
    }

    public boolean equals(Reminder that) {
        if (this == that) {
            return true;
        } else if (that == null) {
            return false;
        } else {
            return equalsIgnoreCase(this.serviceId, that.serviceId) &&
                    this.channelId == that.channelId &&
                    equalsIgnoreCase(this.channelName, that.channelName) &&
                    equalsIgnoreCase(this.programId, that.programId) &&
                    equalsIgnoreCase(this.title, that.title) &&
                    this.startTime == that.startTime &&
                    this.endTime == that.endTime;
        }
    }

    private static boolean equalsIgnoreCase(String a, String b) {
        return (a == b) || (a != null && a.equalsIgnoreCase(b));
    }

    @Override
    public int hashCode() {
        return Objects.hash(programId);
    }

    public int compareTo(Reminder that) {
        if (this.startTime == that.startTime) {
            return this.endTime > that.endTime ? 1 : this.endTime < that.endTime ? -1 : 0;
        } else {
            return this.startTime > that.startTime ? 1 : -1;
        }
    }

    private String getType() {
        long now = ReminderAlertManager.now();
        if (endTime < now) {
            return "PAST:-"+TimeUnit.MILLISECONDS.toMinutes(now - endTime) + "m";
        } else if (now < startTime) {
            return "FUTURE:+"+TimeUnit.MILLISECONDS.toMinutes(startTime - now) + "m";
        } else {
            return "PRESENT:"+TimeUnit.MILLISECONDS.toMinutes(now - startTime) + "m";
        }
    }

    @Override
    public String toString() {
        return "Reminder{" +
                "serviceId='" + serviceId + '\'' +
                ", channelId='" + channelId + '\'' +
                ", channelName='" + channelName + '\'' +
                ", programId='" + programId + '\'' +
                ", time=" + FORMAT.format(new Date(startTime)) +
                "~" + FORMAT.format(new Date(endTime)) +
                " (" + getType() + ")" +
                ", duration=" + TimeUnit.MILLISECONDS.toMinutes(endTime - startTime) + "m" +
                ", title='" + title + '\'' +
                ", program=" + (program != null ? "0x"+Integer.toHexString(program.hashCode()) : null) +
                '}';
    }
}