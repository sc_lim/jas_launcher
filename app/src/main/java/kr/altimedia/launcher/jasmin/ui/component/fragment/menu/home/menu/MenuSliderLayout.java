/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.menu;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.dm.def.CategoryType;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.BootDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.OnMenuInteractor;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter.MenuPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter.UserMenuPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.MyMenuDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingMenuDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.drawer.EdgeDrawerLayout;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.row.MenuRow;
import kr.altimedia.launcher.jasmin.ui.view.row.UserMenuRow;
import kr.altimedia.launcher.jasmin.ui.view.util.FontUtil;
import kr.altimedia.launcher.jasmin.ui.view.widget.NtpTextClock;


public class MenuSliderLayout extends LinearLayout implements MenuRow.OnMenuSelectedListener {
    private final String TAG = MenuSliderLayout.class.getSimpleName();

    private TvOverlayManager mTvOverlayManager;

    public MenuSliderLayout(Context context) {
        this(context, null);
    }

    public MenuSliderLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MenuSliderLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
    }

    public FragmentManager fragmentManager;

    public void setFragmentManager(FragmentManager manager) {
        this.fragmentManager = manager;
    }

    private OnMenuInteractor mOnMenuInteractor = null;

    private UserMenuRow mUserMenuRow = null;

    private final List<Category> currentCategoryList = new ArrayList<>();

    public void showMyMenu(boolean isHotKey) {
        if (mTvOverlayManager == null) {
            return;
        }

        if (!NetworkUtil.hasNetworkConnection(getContext())) {
            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(MbsTaskCallback.REASON_NETWORK, getContext());
            failNoticeDialogFragment.show(mTvOverlayManager, fragmentManager, FailNoticeDialogFragment.CLASS_NAME);
            return;
        }

        if (!AccountManager.getInstance().isAuthenticatedUser()) {

            mTvOverlayManager.showBootDialog(BootDialogFragment.TYPE_RE_BOOT, new BootDialogFragment.BootProcessListener() {
                @Override
                public void onFinished(DialogFragment dialogFragment) {
                    dialogFragment.dismiss();

                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(PushMessageReceiver.ACTION_LOGIN));

                    authenticationListener(true);
                    if (isHotKey) {
                        mRecyclerView.setSelectedPosition(0);
                    }
                    mTvOverlayManager.showDialogFragment(MyMenuDialog.newInstance(isHotKey));
                }

                @Override
                public void onFinishedWithError(DialogFragment dialogFragment, int type, String errorCode, String errorMessage) {
                    dialogFragment.dismiss();
                    authenticationListener(false);
                    FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(MbsTaskCallback.REASON_LOGIN, getContext());
                    failNoticeDialogFragment.show(mTvOverlayManager, fragmentManager, FailNoticeDialogFragment.CLASS_NAME);
                }
            });
            return;
        }

        if (isHotKey) {
            mRecyclerView.setSelectedPosition(0);
        }
        mTvOverlayManager.showDialogFragment(MyMenuDialog.newInstance(isHotKey));
    }

    private void authenticationListener(boolean success) {
        if (Log.INCLUDE) {
            Log.d(TAG, "authenticationListener() success:" + success);
            Log.d(TAG, "authenticationListener() Said:" + AccountManager.getInstance().getSaId());
            Log.d(TAG, "authenticationListener() ProfileId:" + AccountManager.getInstance().getProfileId());
        }
        JasChannelManager.getInstance().initMBS(success);
    }

    private boolean intentAvailable(Intent intent) {
        PackageManager manager = getContext().getPackageManager();
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        return !infos.isEmpty();
    }

    public List<Category> getCurrentCategoryList() {
        return currentCategoryList;
    }

    public void updateList(Category updatedCategory) {

        if (Log.INCLUDE) {
            Log.d(TAG, "updateList : " + updatedCategory);
        }

        ArrayList<Category> updatedList = new ArrayList();
        updatedList.addAll(currentCategoryList);
        for (int i = 0; i < updatedList.size(); i++) {
            Category category = updatedList.get(i);
            if (category.getCategoryID().equalsIgnoreCase(updatedCategory.getCategoryID())) {
                int index = i;
                currentCategoryList.remove(index);
                currentCategoryList.add(index, updatedCategory);
                MenuRow menuRow = new MenuRow(mRecyclerView, updatedCategory, MenuSliderLayout.this);
                itemRowAdapter.replace(index + 1, menuRow);
            }
        }
    }

    private void setUserMenuRow(OnMenuInteractor mOnMenuInteractor) {
        mUserMenuRow = new UserMenuRow(new UserMenuRow.OnUserMenuClickListener() {

            @Override
            public boolean dispatchKey(KeyEvent keyEvent) {
                int keyCode = keyEvent.getKeyCode();
                int keyAction = keyEvent.getAction();
                if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                    int selectedPosition = mRecyclerView.getSelectedPosition();
                    int size = itemRowAdapter.size();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "selectedPosition : " + selectedPosition);
                        Log.d(TAG, "size : " + size);
                    }
                    if (selectedPosition == size - 1) {
                        if (keyAction == KeyEvent.ACTION_DOWN) {
                            mRecyclerView.setSelectedPosition(1);
                        }
                        return true;
                    }
                } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                    int selectedPosition = mRecyclerView.getSelectedPosition();
                    int size = itemRowAdapter.size();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "selectedPosition : " + selectedPosition);
                        Log.d(TAG, "size : " + size);
                    }
                    if (selectedPosition == 0) {
                        if (keyAction == KeyEvent.ACTION_DOWN) {
                            mRecyclerView.setSelectedPosition(size - 1);
                        }
                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onMenuClicked(UserMenuRow.UserMenu menu) {
                switch (menu) {
                    case USER: {
                        showMyMenu(false);
                    }
                    break;
                    case SETTINGS: {
                        mTvOverlayManager.showDialogFragment(SettingMenuDialogFragment.newInstance());
                    }
                    break;

                    case NOTIFICATION:
                        break;
                }
            }
        });
        mUserMenuRow.setInteractor(mOnMenuInteractor);
        mUserMenuRow.setMenuSlider(this);
        itemRowAdapter.add(0, mUserMenuRow);

    }


    final ItemBridgeAdapter mBridgeAdapter = new ItemBridgeAdapter();
    private VerticalGridView mRecyclerView = null;
    private View mMenuLayer = null;
    private View mTextUpperClockLayer = null;
    private View mTextLowerClockLayer = null;
    private ArrayObjectAdapter itemRowAdapter = null;
    /*TextClock*/
    private NtpTextClock mTextClockYear = null;
    private NtpTextClock mTextClockTime = null;

    private final float TRANSLATE_MENU_RATE = 35;
    /*Profile*/
    private View mIconBI;
    private View mLine;
    private View mItemLayer;
    private View mProfileLayer;
    private ImageView mSelectedProfileImg = null;
    private TextView mSelectedProfileName = null;


    private final HashMap<EdgeDrawerLayout.DrawerState, TextClockInfo> yearFormat = new HashMap<>();
    private final HashMap<EdgeDrawerLayout.DrawerState, TextClockInfo> timeFormat = new HashMap<>();
    private View mProfileClockTime;


    public void loadMainMenuWithVersionCheck(CategoryType requestedCategoryType, OnMenuInteractor mOnMenuInteractor, boolean forceUpdate) {
        this.mOnMenuInteractor = mOnMenuInteractor;
        mOnMenuInteractor.needMenuUpdate(new MbsDataProvider<String, Boolean>() {
            @Override
            public void needLoading(boolean loading) {
                if (loading) {
                    mOnMenuInteractor.getProgressBarManager().show();
                } else {
                    mOnMenuInteractor.getProgressBarManager().hide();
                }
            }

            @Override
            public void onFailed(int reason) {

            }

            @Override
            public void onSuccess(String id, Boolean result) {
                if (result.booleanValue()) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, " need MenuUpdate");
                    }
                    loadMainCategory(requestedCategoryType, mOnMenuInteractor, false);
                } else {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "not need MenuUpdate");
                    }
                    mOnMenuInteractor.renewalMenu(findCategoryByType(requestedCategoryType));
                }
            }

            @Override
            public void onError(String errorCode, String message) {

            }
        });
    }

    private Category findCategoryByType(CategoryType type) {
        if (Log.INCLUDE) {
            Log.d(TAG, "findCategoryByType | type : " + type);
        }
        for (Category category : currentCategoryList) {
            if (category.getLinkType() == type) {
                return category;
            }
        }
        for (Category category : currentCategoryList) {
            if (category.isHome()) {
                return category;
            }
        }
        return currentCategoryList.get(1);
    }

    //    public void loadMainMenuWithVersionCheck(OnMenuInteractor mOnMenuInteractor) {
//        loadMainMenuWithVersionCheck(mOnMenuInteractor, false);
//    }
    public void loadMainCategory(CategoryType type, OnMenuInteractor mOnMenuInteractor, boolean clearUserMenu) {
        this.mOnMenuInteractor = mOnMenuInteractor;

        mOnMenuInteractor.onReadyToLoad(type, new MbsDataProvider<String, List<Category>>() {
            @Override
            public void needLoading(boolean loading) {
                if (loading) {
                    mOnMenuInteractor.getProgressBarManager().show();
                } else {
                    mOnMenuInteractor.getProgressBarManager().hide();
                }
            }

            @Override
            public void onFailed(int key) {

            }

            @Override
            public void onSuccess(String id, List<Category> result) {

                if (!currentCategoryList.equals(result)) {
                    if (clearUserMenu) {
                        itemRowAdapter.clear();
                        setUserMenuRow(mOnMenuInteractor);
                    } else {
                        itemRowAdapter.removeItems(1, itemRowAdapter.size() - 1);
                    }

                    for (Category menu : result) {
                        MenuRow menuRow = new MenuRow(mRecyclerView, menu, MenuSliderLayout.this);
                        itemRowAdapter.add(menuRow);
                    }
                }
                currentCategoryList.clear();
                currentCategoryList.addAll(result);

                int[] itemIndex = new int[result.size()];
                for (int i = 0; i < itemIndex.length; i++) {
                    itemIndex[i] = i;
                }
            }

            @Override
            public void onError(String errorCode, String message) {

            }
        });
    }

    public void loadMainCategory(OnMenuInteractor mOnMenuInteractor, boolean clearUserMenu) {
        loadMainCategory(null, mOnMenuInteractor, clearUserMenu);
    }

    private Category findCategoryByIndex(int index) {
        Object o = itemRowAdapter.get(index);
        if (o instanceof MenuRow) {
            MenuRow menuRow = (MenuRow) o;
            return menuRow.getItem();
        }
        return null;
    }

    private int findByCategoryIndex(Category category, boolean makeSelected) {
        int size = itemRowAdapter.size();
        int index = -1;

        for (int i = 0; i < size; i++) {
            Object o = itemRowAdapter.get(i);
            if (o instanceof MenuRow) {
                MenuRow menuRow = (MenuRow) o;
                if (category.getCategoryID().equalsIgnoreCase(menuRow.getItem().getCategoryID())) {
                    index = i;
                    break;
                }
            }
        }
        if (makeSelected) {
            for (int i = 0; i < size; i++) {
                Object o = itemRowAdapter.get(i);
                if (o instanceof MenuRow) {
                    ((MenuRow) o).setSelected(i == index);
                }
            }
        }
        return index;
    }

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_menu_slider, this, true);
        mIconBI = view.findViewById(R.id.iconBI);
        mLine = view.findViewById(R.id.line);
        mItemLayer = view.findViewById(R.id.itemLayer);
        mProfileLayer = view.findViewById(R.id.profileLayer);
        mSelectedProfileImg = view.findViewById(R.id.selectedProfileImg);
        mSelectedProfileName = view.findViewById(R.id.selectedProfileName);
        mProfileClockTime = view.findViewById(R.id.profileClockTime);

        mTextClockYear = view.findViewById(R.id.textClockYear);
        mTextClockTime = view.findViewById(R.id.textClockTime);
        mTextUpperClockLayer = view.findViewById(R.id.textUpperClockLayer);
        mTextLowerClockLayer = view.findViewById(R.id.textLowerClockLayer);
        mRecyclerView = view.findViewById(R.id.menuList);
        mMenuLayer = view.findViewById(R.id.menuLayer);
        ClassPresenterSelector presenterSelector = new ClassPresenterSelector();
        presenterSelector.addClassPresenter(MenuRow.class, new MenuPresenter());
        presenterSelector.addClassPresenter(UserMenuRow.class, new UserMenuPresenter());
        mBridgeAdapter.setPresenter(presenterSelector);
        itemRowAdapter = new ArrayObjectAdapter();
        initTimeFormat(context);
        mBridgeAdapter.setAdapter(itemRowAdapter);
        mRecyclerView.setAdapter(mBridgeAdapter);
        mRecyclerView.setOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {

            @Override
            public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subPosition) {
                super.onChildViewHolderSelected(parent, child, position, subPosition);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onChildViewHolderSelected : currentState : " + mCurrentState);
                }
                if (child == null || child.itemView == null) {
                    return;
                }
                child.itemView.setSelected(mCurrentState == EdgeDrawerLayout.DrawerState.CLOSING);
            }

            @Override
            public void onChildViewHolderSelectedAndPositioned(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                super.onChildViewHolderSelectedAndPositioned(parent, child, position, subposition);
            }
        });
        renewalProfileInfo();
    }

    public void renewalProfileInfo() {
        ProfileInfo mProfileInfo = AccountManager.getInstance().getSelectedProfile();
        if (mProfileInfo == null) {
            mSelectedProfileImg.setImageDrawable(null);
            mSelectedProfileName.setText("");
        } else {
            try {
                mProfileInfo.setProfileIcon(mSelectedProfileImg);
            }catch (Exception e){
            }
            String profileName = mProfileInfo.getProfileName();
            mSelectedProfileName.setText(profileName);
        }

    }

    public Category getSelectedCategory() {
        int selectedPosition = mRecyclerView.getSelectedPosition();
        return findCategoryByIndex(selectedPosition);
    }

    public void notifyPositionChanged() {
        int prevIndex = mRecyclerView.getSelectedPosition();
        int index = findByCategoryIndex(mOnMenuInteractor.getCurrentCategory(), true);
        mRecyclerView.setSelectedPosition(index);
        mBridgeAdapter.notifyItemChanged(prevIndex);
        mBridgeAdapter.notifyItemChanged(index);
    }

    @Override
    public void onItemSelected(View view, Category menu) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onItemSelected : " + menu);
        }
        mOnMenuInteractor.menuSelected(menu, false, true);
        findByCategoryIndex(mOnMenuInteractor.getCurrentCategory(), true);
    }

    @Override
    public void onCanceled(View view, Category menu) {
        mOnMenuInteractor.closeSideMenu(true, false);
        findByCategoryIndex(mOnMenuInteractor.getCurrentCategory(), true);
        int index = findByCategoryIndex(mOnMenuInteractor.getCurrentCategory(), true);
        if (index != -1) {
            mRecyclerView.setSelectedPosition(index);
        }
    }

    @Override
    public boolean dispatchKey(KeyEvent keyEvent) {
        if (Log.INCLUDE) {
            Log.d(TAG, "keyEvent=" + KeyEvent.keyCodeToString(keyEvent.getKeyCode()));
        }

        int keyCode = keyEvent.getKeyCode();
        int keyAction = keyEvent.getAction();
        if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
            int selectedPosition = mRecyclerView.getSelectedPosition();
            int size = itemRowAdapter.size();
            if (Log.INCLUDE) {
                Log.d(TAG, "selectedPosition : " + selectedPosition);
                Log.d(TAG, "size : " + size);
            }
            if (selectedPosition == size - 1) {
                if (keyAction == KeyEvent.ACTION_DOWN) {
                    mRecyclerView.setSelectedPosition(1);
                }
                return true;
            }
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            int selectedPosition = mRecyclerView.getSelectedPosition();
            int size = itemRowAdapter.size();
            if (Log.INCLUDE) {
                Log.d(TAG, "selectedPosition : " + selectedPosition);
                Log.d(TAG, "size : " + size);
            }
            if (selectedPosition == 0) {
                if (keyAction == KeyEvent.ACTION_DOWN) {
                    mRecyclerView.setSelectedPosition(size - 1);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();

        if (action != KeyEvent.ACTION_DOWN) {
            return super.dispatchKeyEvent(event);
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                mOnMenuInteractor.closeSideMenu(true, false);
                int index = findByCategoryIndex(mOnMenuInteractor.getCurrentCategory(), true);
                if (index != -1) {
                    mRecyclerView.setSelectedPosition(index);
                }
                return true;

        }

        return super.dispatchKeyEvent(event);
    }

    private final float TRANSLATE_UPPER_ClOCK_RATE = 450;
    private final float TRANSLATE_LOWER_ClOCK_RATE = 435;
    private final float TRANSLATE_LOWER_Y_ClOCK_RATE = 50;

    private void initTimeFormat(Context context) {
        yearFormat.put(EdgeDrawerLayout.DrawerState.OPENING, new TextClockInfo(getResources().getDimensionPixelSize(R.dimen.open_year_text_size), context.getString(R.string.EEE_dd_MMM)
                , R.color.open_year_text_color, FontUtil.Font.regular));
        yearFormat.put(EdgeDrawerLayout.DrawerState.CLOSING, new TextClockInfo(getResources().getDimensionPixelSize(R.dimen.close_year_text_size),
                "", R.color.close_year_text_color, FontUtil.Font.medium));

        timeFormat.put(EdgeDrawerLayout.DrawerState.OPENING, new TextClockInfo(getResources().getDimensionPixelSize(R.dimen.open_time_text_size),
                "kk:mm", R.color.open_time_text_color, FontUtil.Font.regular));
        timeFormat.put(EdgeDrawerLayout.DrawerState.CLOSING, new TextClockInfo(getResources().getDimensionPixelSize(R.dimen.close_time_text_size),
                "mm", R.color.close_time_text_color, FontUtil.Font.light));

        yearFormat.put(EdgeDrawerLayout.DrawerState.OPENED, new TextClockInfo(getResources().getDimensionPixelSize(R.dimen.open_year_text_size), context.getString(R.string.EEE_dd_MMM)
                , R.color.open_year_text_color, FontUtil.Font.regular));
        yearFormat.put(EdgeDrawerLayout.DrawerState.CLOSED, new TextClockInfo(getResources().getDimensionPixelSize(R.dimen.close_year_text_size),
                "", R.color.close_year_text_color, FontUtil.Font.medium));

        timeFormat.put(EdgeDrawerLayout.DrawerState.OPENED, new TextClockInfo(getResources().getDimensionPixelSize(R.dimen.open_time_text_size),
                "kk:mm", R.color.open_time_text_color, FontUtil.Font.regular));
        timeFormat.put(EdgeDrawerLayout.DrawerState.CLOSED, new TextClockInfo(getResources().getDimensionPixelSize(R.dimen.close_time_text_size),
                "mm", R.color.close_time_text_color, FontUtil.Font.light));
    }

    public void setCurrentMenu(CategoryType categoryType) {

        if (Log.INCLUDE) {
            Log.d(TAG, "setCurrentMenu : " + categoryType);
        }
//        List<Category> mMenuList = MenuManager.getInstance().getMainMenu(getContext());
//        Category dummy = new Category(categoryType);
//        int index = mMenuList.indexOf(dummy);
//        mRecyclerView.setSelectedPosition(index + 1);
    }

    public void clearSelection() {
        mRecyclerView.clearAllSelection();
    }

    private EdgeDrawerLayout.DrawerState mCurrentState = EdgeDrawerLayout.DrawerState.OPENED;

    private boolean isCloseState(EdgeDrawerLayout.DrawerState state) {
        return state == EdgeDrawerLayout.DrawerState.CLOSED || state == EdgeDrawerLayout.DrawerState.CLOSING;
    }

    public void onDrawerStateChanged(EdgeDrawerLayout.DrawerState state, float slideOffset) {
        if (Log.INCLUDE) {
            Log.d(TAG, "state != EdgeDrawerLayout.DrawerState.CLOSING : " + (state != EdgeDrawerLayout.DrawerState.CLOSING));
        }
        onDrawerSlide(state, slideOffset);
    }

    public void onDrawerSlide(EdgeDrawerLayout.DrawerState state, float slideOffset) {
        if (state == EdgeDrawerLayout.DrawerState.INVALID) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onDrawerSlide : state : " + state + " : so return");
            }
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onDrawerSlide : state : " + state + ", slideOffset : " + slideOffset);
        }
        mCurrentState = state;
        float calcOffset = 1 - slideOffset;
        changeTimeFormat(state);
        if (checkNeedTranslateClock(state)) {
            float computeUpperX = isCloseState(state) ? calcOffset * TRANSLATE_UPPER_ClOCK_RATE : -calcOffset * TRANSLATE_UPPER_ClOCK_RATE;
            mTextUpperClockLayer.setTranslationX(computeUpperX);
            float computeLowerX = isCloseState(state) ? calcOffset * TRANSLATE_LOWER_ClOCK_RATE : -calcOffset * TRANSLATE_LOWER_ClOCK_RATE;
            float computeLowerY = isCloseState(state) ? -calcOffset * TRANSLATE_LOWER_Y_ClOCK_RATE : calcOffset * TRANSLATE_LOWER_Y_ClOCK_RATE;
            mTextLowerClockLayer.setTranslationX(computeLowerX);
            mTextLowerClockLayer.setTranslationY(computeLowerY);
        }

        if (checkNeedToSetVisibleButton(state)) {
            int visible = isCloseState(state) ? View.INVISIBLE : View.VISIBLE;
            View view = mRecyclerView.findViewById(R.id.buttonLayer);
            if (view != null) {
                view.setVisibility(visible);
            }
            mLine.setVisibility(visible);
        }

        if (checkNeedTranslateMenuList(state)) {
            float computeX = isCloseState(state) ? calcOffset * TRANSLATE_MENU_RATE : -calcOffset * TRANSLATE_MENU_RATE;
            boolean closing = isCloseState(state);
            if (mRecyclerView.isEnabled() != (!closing)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "state != EdgeDrawerLayout.DrawerState.CLOSING : " + (state != EdgeDrawerLayout.DrawerState.CLOSING));
                }
                mRecyclerView.setEnabled(!closing);
                itemRowAdapter.notifyArrayItemRangeChanged(0, itemRowAdapter.size());

                mItemLayer.setBackgroundResource(closing ? R.drawable.bg_f5121212 : R.drawable.bg_f50f1017);
            }
            mMenuLayer.setTranslationX(computeX);
            mIconBI.setTranslationX(-computeX * 10);
        }
    }

    private void changeTimeFormat(EdgeDrawerLayout.DrawerState state) {
        switch (state) {
            case OPENING:
            case OPENED:
                mProfileLayer.setVisibility(View.VISIBLE);
                mProfileClockTime.setVisibility(View.INVISIBLE);
                break;

            case CLOSING:
            case CLOSED:
                mProfileLayer.setVisibility(View.INVISIBLE);
                mProfileClockTime.setVisibility(View.VISIBLE);
                break;
        }

        if (state == EdgeDrawerLayout.DrawerState.INVALID) {
            return;
        }

        CharSequence yearFormatString = mTextClockYear.getFormat24Hour();
        TextClockInfo newYearInfo = yearFormat.get(state);
        String newYearFormatString = newYearInfo.getTimeFormat();
        if (!yearFormatString.equals(newYearFormatString)) {
            mTextClockYear.setFormat12Hour(newYearFormatString);
            mTextClockYear.setFormat24Hour(newYearFormatString);
            mTextClockYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, newYearInfo.getTextSize());
            mTextClockYear.setTextColor(getContext().getColor(newYearInfo.getTextColor()));
            mTextClockYear.setTypeface(FontUtil.getInstance().getTypeface(newYearInfo.getFont()));
        }

        CharSequence timeFormatString = mTextClockTime.getFormat24Hour();
        TextClockInfo newTimeInfo = timeFormat.get(state);
        String newTimeFormatString = newTimeInfo.getTimeFormat();
        if (!timeFormatString.equals(newTimeFormatString)) {
            mTextClockTime.setFormat12Hour(newTimeFormatString);
            mTextClockTime.setFormat24Hour(newTimeFormatString);
            mTextClockTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, newTimeInfo.getTextSize());
            mTextClockTime.setTextColor(getContext().getColor(newTimeInfo.getTextColor()));
            mTextClockTime.setTypeface(FontUtil.getInstance().getTypeface(newTimeInfo.getFont()));
        }
    }

    private boolean checkNeedToSetVisibleButton(EdgeDrawerLayout.DrawerState state) {
        View parentsView = mRecyclerView.findViewById(R.id.buttonLayer);
        if (parentsView == null) {
            return false;
        }
        boolean isVisible = parentsView.getVisibility() == View.VISIBLE;
        if (state == EdgeDrawerLayout.DrawerState.OPENING || state == EdgeDrawerLayout.DrawerState.OPENED) {
            return !isVisible;
        } else if (state == EdgeDrawerLayout.DrawerState.CLOSING || state == EdgeDrawerLayout.DrawerState.CLOSED) {
            return isVisible;
        }
        return false;
    }

    private boolean checkNeedTranslateClock(EdgeDrawerLayout.DrawerState state) {
        float preTranslateX = mTextUpperClockLayer.getTranslationX();
        if (!isCloseState(state)) {
            return preTranslateX != 0;
        }
        return true;
    }

    private boolean checkNeedTranslateMenuList(EdgeDrawerLayout.DrawerState state) {
        float preTranslateX = mMenuLayer.getTranslationX();
        if (!isCloseState(state)) {
            return preTranslateX != 0;
        }
        return true;
    }

    private static class TextClockInfo {
        final float textSize;
        final String timeFormat;
        final int textColor;
        final FontUtil.Font font;

        public TextClockInfo(float textSize, String timeFormat, int textColor, FontUtil.Font font) {
            this.textSize = textSize;
            this.timeFormat = timeFormat;
            this.textColor = textColor;
            this.font = font;
        }

        public FontUtil.Font getFont() {
            return font;
        }

        public float getTextSize() {
            return textSize;
        }

        public String getTimeFormat() {
            return timeFormat;
        }

        public int getTextColor() {
            return textColor;
        }
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (Log.INCLUDE) {
            Log.d(TAG, "call OnDetachedFromWindow");
        }
        this.fragmentManager = null;
        this.mOnMenuInteractor = null;
    }
}
