/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Handler;
import android.os.Looper;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager;
import kr.altimedia.launcher.jasmin.ui.view.util.FontUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Created by mc.kim on 03,07,2020
 */
public class ProgramInformationLayout extends BaseBubbleProvider {
    private final String TAG = ProgramInformationLayout.class.getSimpleName();
    private final int LEFT_LIMIT = 338;
    private final Context mContext;
    private final Resources mResource;
    private final NinePatchDrawable mBubbleResource;

    private final int mBubbleTopPadding;
    private final int mBubbleHeight;

    private final Paint mTextPaint;

    private final Rect paddingRect;
    private final Paint mDrawingPaint;
    private final long TOOL_TIP_DURATION = 15 * TimeUtil.MIN;
    private final Bitmap mAlarmIcon;
    private final int mAlarmPadding;
    private final long DELAY_TIME = 100;
    private LayoutInflater inflater = null;
    private BubbleUpdater mBubbleUpdater = new BubbleUpdater();
    private Handler mUpdaterHandler = new Handler(Looper.getMainLooper());

    private final int GRAVITY_LEFT = 0;
    private final int GRAVITY_CENTER = 1;
    private final int GRAVITY_RIGHT = 2;

    public ProgramInformationLayout(Context context) {
        this(context, null);
    }

    public ProgramInformationLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgramInformationLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        mResource = context.getResources();
        mBubbleResource = (NinePatchDrawable) mResource.getDrawable(R.drawable.title_balloon, null);
        Drawable alarmIcon = mResource.getDrawable(R.drawable.icon_grid_alarm_b, null);
        mAlarmIcon = ((BitmapDrawable) alarmIcon).getBitmap();
        mBubbleTopPadding = mResource.getDimensionPixelSize(R.dimen.program_guide_bubble_padding_top);
        mBubbleHeight = mResource.getDimensionPixelSize(R.dimen.program_guide_bubble_padding_height);

        mAlarmPadding = mResource.getDimensionPixelOffset(R.dimen.program_guide_bubble_alarm_padding);

        int mBubblePaddingLeft = mResource.getDimensionPixelSize(R.dimen.program_guide_bubble_font_padding_left);
        int mBubblePaddingTop = mResource.getDimensionPixelSize(R.dimen.program_guide_bubble_font_padding_top);
        paddingRect = new Rect(mBubblePaddingLeft, mBubblePaddingTop, mBubblePaddingLeft, mBubblePaddingTop);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(mResource.getColor(R.color.color_FF222535));
        mTextPaint.setTextSize(mResource.getDimensionPixelSize(R.dimen.program_guide_bubble_font_size));
        mTextPaint.setTypeface(FontUtil.getInstance().getTypeface(FontUtil.Font.medium));

        mDrawingPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mDrawingPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    }

    @Override
    public void onDrawForeground(Canvas canvas) {
        super.onDrawForeground(canvas);
        if (Log.INCLUDE) {
            Log.d(TAG, "onDrawForeground");
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (Log.INCLUDE) {
            Log.d(TAG, "onDraw");
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (Log.INCLUDE) {
            Log.d(TAG, "dispatchDraw");
        }


    }

    @Override
    public void onProgramUnSelected(View itemView) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onProgramUnSelected");
        }
        removeAllViews();
    }

    @Override
    public void onProgramSelected(View targetView) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onProgramSelected");
        }
        if (!(targetView instanceof ProgramItemView)) {
            return;
        }
        ProgramItemView itemView = (ProgramItemView) targetView;
        removeAllViews();
        ProgramManager.TableEntry entry = itemView.getTableEntry();
        Program program = entry.program;
        if (program == null) {
            return;
        }

        TextView titleVie = itemView.findViewById(R.id.programTitle);
        Layout l = titleVie.getLayout();
        boolean isEllipsis = false;
        if (l != null) {
            int lines = l.getLineCount();
            if (lines > 0) {
                isEllipsis = l.getEllipsisCount(lines - 1) > 0;
            }
        }
        if (!isEllipsis) {
            return;
        }

        int[] position = new int[2];
        getLocationInWindow(position);
        int left = position[0];
        int top = position[1];

        int[] itemPosition = new int[2];
        itemView.getLocationInWindow(itemPosition);
        int itemLeft = itemPosition[0];
        int itemTop = itemPosition[1];

        int layoutLeft = itemLeft - left;

        if (layoutLeft < LEFT_LIMIT) {
            return;
        }


        View bubbleView = inflater.inflate(R.layout.component_program_bubble, null);
        TextView titleView = bubbleView.findViewById(R.id.bubbleTitle);
        ImageView iconView = bubbleView.findViewById(R.id.bubbleIcon);
        ImageView courseView = bubbleView.findViewById(R.id.course);


        titleView.setText(program.getTitle());

        if (Log.INCLUDE) {
            Log.d(TAG, "onProgramSelected | left : " + (itemLeft - left));
            Log.d(TAG, "onProgramSelected | top : " + (itemTop - top + mBubbleTopPadding));
        }

        int width = getTextWidth(mTextPaint, program.getTitle()) + 10;
        int textWidth = getTextWidth(mTextPaint, program.getTitle());
        int iconResource = getIconResource(entry, itemView.getChannelDataManager());
        iconView.setImageResource(iconResource);
        iconView.setVisibility(iconResource != -1 ? View.VISIBLE : View.GONE);
        addView(bubbleView);
        LayoutParams params = (RelativeLayout.LayoutParams) bubbleView.getLayoutParams();
        boolean isOverSize = false;
        if (titleView.getMaxWidth() < width) {
            width = titleView.getMaxWidth();
            isOverSize = true;
        }
        setLayoutParamWithWidth(titleView, width);

        int parentsWidth = itemView.getMeasuredWidth();
        int sourceMargin = mResource.getDimensionPixelOffset(R.dimen.bubble_limit_margin);
        int gravityType = (layoutLeft < LEFT_LIMIT * 2 ? GRAVITY_LEFT : layoutLeft < LEFT_LIMIT * 3 ? GRAVITY_CENTER : GRAVITY_RIGHT);

        if (Log.INCLUDE) {
            Log.d(TAG, "gravityType : " + gravityType);
        }


        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) courseView.getLayoutParams(); // or wrap_content

        switch (gravityType) {
            case GRAVITY_LEFT: {
                layoutParams.gravity = Gravity.LEFT;
                params.setMargins(layoutLeft, (itemTop - top) + (2 * mBubbleTopPadding) + mBubbleHeight, 0, 0);
            }
            break;
            case GRAVITY_CENTER: {
                layoutParams.gravity = Gravity.CENTER;
                int margin = (width - parentsWidth) / 2;
                if (parentsWidth > width) {
                    margin = 0;
                }
                params.setMargins(isOverSize ? layoutLeft : layoutLeft - margin, (itemTop - top) + (2 * mBubbleTopPadding) + mBubbleHeight, 0, 0);
            }
            break;
            case GRAVITY_RIGHT: {
                layoutParams.gravity = Gravity.RIGHT;
                int margin = getWidth() - width;
                if ((margin + textWidth) > layoutLeft) {
                    margin = layoutLeft - width + sourceMargin;
                }
                params.setMargins(margin, (itemTop - top) + (2 * mBubbleTopPadding) + mBubbleHeight, 0, 0);
            }
            break;
        }

    }

    private void setLayoutParamWithWidth(View view, int width) {
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        layoutParams.width = width;
        view.setLayoutParams(layoutParams);
    }

    private int getIconResource(ProgramManager.TableEntry mTableEntry, ChannelDataManager channelDataManager) {
        if (mTableEntry == null || mTableEntry.program == null) {
            return -1;
        }
        Program program = mTableEntry.program;
        boolean isPastProgram = mTableEntry.isPastProgram();
        final Channel channel = channelDataManager.getChannel(mTableEntry.channelId);
        boolean isWatchableTimeShift = isPastProgram && isTimeShiftEnabled(channel, program);
        boolean isReserved = ReminderAlertManager.getInstance().isReserved(program);
//        boolean isParentalLock = ParentalController.isProgramRatingBlocked(program.getContentRatings());
        int iconResId = isReserved ? R.drawable.icon_grid_alarm_b : (isWatchableTimeShift ? R.drawable.icon_watch_b : 0);
        return iconResId;
    }

    private boolean isTimeShiftEnabled(Channel channel, Program program) {
        if (program == null) {
            return false;
        }
        long timeShiftTimeMillis = channel != null ? channel.getTimeshiftService() : 0;
        if (Log.INCLUDE) {
            Log.d(TAG, "isTimeShiftEnabled | timeShiftTimeMillis : " + timeShiftTimeMillis);
        }
        long currentTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
        long availableTime = currentTime - timeShiftTimeMillis;
        return program.getEndTimeUtcMillis() > availableTime;
    }

    private RecyclerView findRecyclerViewByChild() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childView = getChildAt(i);
            if (childView instanceof RecyclerView) {
                return (RecyclerView) childView;
            }
        }
        return null;
    }

    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int[] originalPos = new int[2];
        parent.getLocationInWindow(originalPos);
        int x = originalPos[0];
        int y = originalPos[1];
        if (Log.INCLUDE) {
            Log.d(TAG, "parent x : " + x + ", y : " + y);
        }


        int childCount = parent.getChildCount();

        for (int i = 0; i < childCount; i++) {
            View view = parent.getChildAt(i);
            ProgramRow row = null;
            if ((view instanceof LinearLayout)) {
                row = view.findViewById(R.id.row);
            } else if (view instanceof ProgramRow) {
                row = (ProgramRow) view;
            }
            if (row == null) {
                continue;
            }
            LinearLayoutManager layoutManager = ((LinearLayoutManager) row.getLayoutManager());
            int childSize = layoutManager.getChildCount();
            for (int index = 0; index < childSize; index++) {
                View childView = layoutManager.getChildAt(index);
                if (childView instanceof ProgramItemView && childView.isFocused()) {
                    drawBubbleItem(c, (ProgramItemView) childView, state, originalPos);
                }
            }
        }
    }

    private void drawBubbleItem(Canvas canvas, ProgramItemView itemView, RecyclerView.State state, int[] parentPosition) {
        ProgramManager.TableEntry entry = itemView.getTableEntry();
        if (entry.program == null) {
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "entry.program channel id() : " + entry.program.getChannelId());
            Log.d(TAG, "entry.program id() : " + entry.program.getId());
            Log.d(TAG, "entry.program getStartTimeUtcMillis() : " + entry.program.getStartTimeUtcMillis());
            Log.d(TAG, "entry.program getEndTimeUtcMillis() : " + entry.program.getEndTimeUtcMillis());
            Log.d(TAG, "entry.program duration () : " + (entry.program.getEndTimeUtcMillis() - entry.program.getStartTimeUtcMillis()));
            Log.d(TAG, "entry.program getDurationMillis () : " + entry.program.getDurationMillis());
        }


        int[] originalPos = new int[2];
        itemView.getLocationInWindow(originalPos);
        int x = originalPos[0];
        int y = originalPos[1];
        if (Log.INCLUDE) {
            Log.d(TAG, "getLocationInWindow parent x : " + parentPosition[0] + ", y : " + parentPosition[1]);
            Log.d(TAG, "getLocationInWindow x : " + x + ", y : " + y);
        }
        int childViewLeft = x - parentPosition[0];
        int childViewTop = y - parentPosition[1] + mBubbleTopPadding;
        int childViewRight = childViewLeft + itemView.getMeasuredWidth();
        int childViewBottom = childViewTop + mBubbleHeight;

        Rect npdBounds = new Rect(childViewLeft, childViewTop, childViewRight, childViewBottom);
        drawBubble(canvas, mBubbleResource, npdBounds, entry.program);
    }

    private void drawBubble(Canvas canvas, NinePatchDrawable mBubbleResource, Rect rect, Program program) {
        boolean isReserved = ReminderAlertManager.getInstance().isReserved(program);
        int textWidth = getTextWidth(mTextPaint, program.getTitle());
        int resultWidth = textWidth + (isReserved ? (mAlarmPadding + mAlarmIcon.getWidth() + mAlarmPadding) : 0);
        Rect boundRect = calculationBubbleRect(rect, resultWidth);

        mBubbleResource.setBounds(boundRect);
        mBubbleResource.draw(canvas);
        canvas.drawText(program.getTitle(), boundRect.left + paddingRect.left, boundRect.top + paddingRect.top, mTextPaint);
        if (isReserved) {
            int left = boundRect.left + paddingRect.left + textWidth;
            Rect iconRect = new Rect(left,
                    boundRect.top + paddingRect.top,
                    left + mAlarmIcon.getWidth(), boundRect.top + paddingRect.top + mAlarmIcon.getHeight());
            if (Log.INCLUDE) {
                Log.d(TAG, "iconRect : " + iconRect.toString());
            }
            canvas.drawBitmap(mAlarmIcon, iconRect, iconRect, mDrawingPaint);
        }
    }

    private Rect calculationBubbleRect(Rect imageBounds, int textWidth) {
        int left = imageBounds.left;
        int top = imageBounds.top;
        int width = imageBounds.width();
        int height = imageBounds.height();


        if (width < textWidth) {
            return imageBounds;
        } else {
//            left += (width - textWidth) / 2;
            Rect boundRect = new Rect(left,
                    top,
                    left + textWidth + (2 * paddingRect.right),
                    top + height);
            return boundRect;
        }
    }

    private int getTextWidth(Paint textPaint, String message) {
        Rect bounds = new Rect();
        textPaint.getTextBounds(message, 0, message.length(), bounds);
        return bounds.width();
    }

    private class BubbleUpdater implements Runnable {
        private Canvas canvas;

        public void setCanvas(Canvas canvas) {
            this.canvas = canvas;
        }

        @Override
        public void run() {
            RecyclerView childRecycler = findRecyclerViewByChild();
            if (childRecycler != null) {
                onDrawOver(canvas, childRecycler, null);
            }
        }
    }

}
