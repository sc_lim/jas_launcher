/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget.icons;

import android.content.ComponentName;
import android.content.Context;
import android.os.UserHandle;

/**
 * Created by mc.kim on 23,12,2019
 */
public interface CachingLogic<T> {

    ComponentName getComponent(ComponentWithLabel object);

    UserHandle getUser(ComponentWithLabel object);

    CharSequence getLabel(ComponentWithLabel object);

    void loadIcon(Context context,
                  ComponentWithLabel object, BitmapInfo target);
}
