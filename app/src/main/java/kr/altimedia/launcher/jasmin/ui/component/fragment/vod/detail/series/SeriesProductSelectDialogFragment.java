package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ItemBridgeAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.VodProductType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.series.presenter.SeriesProductSelectPresenter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class SeriesProductSelectDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = SeriesProductSelectDialogFragment.class.getName();
    private final String TAG = SeriesProductSelectDialogFragment.class.getSimpleName();

    public static final String KEY_EPISODE_TITLE = "EPISODE_TITLE";
    public static final String KEY_EPISODE_SIZE = "EPISODE_SIZE";
    public static final String KEY_SERIES_PRODUCT_LIST = "SERIES_PRODUCT_LIST";

    private OnClickProductListener mOnClickProductListener;

    public SeriesProductSelectDialogFragment() {
    }

    public static SeriesProductSelectDialogFragment newInstance(String title, int episodeSize, ArrayList<ProductButtonItem> productButtonItems) {
        Bundle args = new Bundle();
        args.putString(KEY_EPISODE_TITLE, title);
        args.putInt(KEY_EPISODE_SIZE, episodeSize);
        args.putParcelableArrayList(KEY_SERIES_PRODUCT_LIST, productButtonItems);

        SeriesProductSelectDialogFragment fragment = new SeriesProductSelectDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnClickProductListener(OnClickProductListener mOnClickProductListener) {
        this.mOnClickProductListener = mOnClickProductListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_series_product_select, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        Bundle mBundle = getArguments();

        String title = mBundle.getString(KEY_EPISODE_TITLE);
        int size = mBundle.getInt(KEY_EPISODE_SIZE);
        String episodeSize = String.valueOf(size);
        String unit = size > 1 ? getString(R.string.episodes) : getString(R.string.episode);
        ((TextView) view.findViewById(R.id.title)).setText(title);
        ((TextView) view.findViewById(R.id.count)).setText(episodeSize);
        ((TextView) view.findViewById(R.id.episode)).setText(unit);

        initGirdView(view);
        initButtons(view);
    }

    private void initGirdView(View view) {
        ArrayList<ProductButtonItem> list = getArguments().getParcelableArrayList(KEY_SERIES_PRODUCT_LIST);

        ArrayList productList = new ArrayList();
        ArrayList buyList = new ArrayList();
        for (ProductButtonItem productButtonItem : list) {
            ArrayList l = productButtonItem.getProductType() == VodProductType.RENT_ALL_EPISODE ? productList : buyList;
            l.add(productButtonItem);
        }
        productList.addAll(buyList);

        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter(new SeriesProductSelectPresenter());
        arrayObjectAdapter.addAll(0, productList);

        SeriesProductBridgeAdapter itemBridgeAdapter = new SeriesProductBridgeAdapter(arrayObjectAdapter, mOnClickProductListener);
        HorizontalGridView gridView = view.findViewById(R.id.grid_view);
        gridView.setAdapter(itemBridgeAdapter);
    }

    private void initButtons(View view) {
        TextView cancelButton = view.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickProductListener != null) {
                    mOnClickProductListener.onClickProduct(null);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mOnClickProductListener = null;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private static final class SeriesProductBridgeAdapter extends ItemBridgeAdapter {
        private OnClickProductListener mOnClickProductListener;

        public SeriesProductBridgeAdapter(ObjectAdapter adapter, OnClickProductListener mOnClickProductListener) {
            super(adapter);
            this.mOnClickProductListener = mOnClickProductListener;
        }

        @Override
        protected void onBind(ViewHolder viewHolder) {
            super.onBind(viewHolder);

            Presenter.ViewHolder vh = viewHolder.getViewHolder();
            vh.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnClickProductListener != null) {
                        ProductButtonItem productButtonItem = (ProductButtonItem) viewHolder.getItem();
                        mOnClickProductListener.onClickProduct(productButtonItem);
                    }
                }
            });
        }

        @Override
        protected void onUnbind(ViewHolder viewHolder) {
            super.onUnbind(viewHolder);

            Presenter.ViewHolder vh = viewHolder.getViewHolder();
            vh.view.setOnClickListener(null);
        }
    }

    public interface OnClickProductListener {
        void onClickProduct(ProductButtonItem productButtonItem);
    }
}
