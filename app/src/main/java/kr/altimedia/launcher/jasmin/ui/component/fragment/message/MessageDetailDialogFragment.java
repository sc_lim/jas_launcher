/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.message;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.message.obj.Message;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.view.common.ScrollDescriptionTextButton;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class MessageDetailDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = MessageDetailDialogFragment.class.getName();
    private final String TAG = MessageDetailDialogFragment.class.getSimpleName();

    private static final String KEY_MESSAGE = "message";
    private final int MAX_LINE_COUNT = 7;

    final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private DialogInterface.OnDismissListener mOnDismissListener = null;

    public static MessageDetailDialogFragment newInstance(Message msg) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_MESSAGE, msg);
        MessageDetailDialogFragment fragment = new MessageDetailDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public MessageDetailDialogFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_message_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initProgressbar(view);
        Message message = getArguments().getParcelable(KEY_MESSAGE);

        initView(view, message);
        initListener(view, message);
    }


    private void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
        mProgressBarManager.show();
    }

    private void initView(View view, Message message) {
        TextView desc = view.findViewById(R.id.description);
        if (message != null) {
            ((TextView) view.findViewById(R.id.title)).setText(message.getTitle());
            String date = TimeUtil.getModifiedDate(message.getDate(), "dd. MM. yyyy");
            ((TextView) view.findViewById(R.id.date)).setText(date);
            desc.setText(message.getDescription());
        }

        ScrollDescriptionTextButton close = view.findViewById(R.id.close);
        close.setDescriptionTextView(desc);
        close.setOnButtonKeyListener(new ScrollDescriptionTextButton.OnButtonKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        dismiss();
                        return true;
                }
                return false;
            }
        });
    }

    private void initListener(View view, Message message) {
        TextView desc = view.findViewById(R.id.description);
        ThumbScrollbar mThumbScrollbar = view.findViewById(R.id.scrollbar);

        desc.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                mThumbScrollbar.setList(desc.getLineCount(), MAX_LINE_COUNT);
            }
        });

        desc.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                mThumbScrollbar.moveScroll((scrollY > oldScrollY));
            }
        });

        view.findViewById(R.id.layout).setVisibility(View.VISIBLE);
        mProgressBarManager.hide();
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public void setOnDismissListener(@NonNull DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
            mOnDismissListener = null;
        }
    }
}
