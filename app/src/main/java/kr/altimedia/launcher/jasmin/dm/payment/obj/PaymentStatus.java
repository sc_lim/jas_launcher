/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class PaymentStatus implements Parcelable {
    public static final Creator<PaymentStatus> CREATOR = new Creator<PaymentStatus>() {
        @Override
        public PaymentStatus createFromParcel(Parcel in) {
            return new PaymentStatus(in);
        }

        @Override
        public PaymentStatus[] newArray(int size) {
            return new PaymentStatus[size];
        }
    };
    @Expose
    @SerializedName("paymentMethod")
    private PaymentType paymentType;
    @Expose
    @SerializedName("successYn")
    private String isSuccess;

    protected PaymentStatus(Parcel in) {
        paymentType = PaymentType.findByName((String) in.readValue(String.class.getClassLoader()));
        isSuccess = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(paymentType.getCode());
        dest.writeValue(isSuccess());
    }

    @NonNull
    @Override
    public String toString() {
        return "PaymentStatus{" +
                "paymentType='" + paymentType + '\'' +
                ", successYn='" + isSuccess + '\'' +
                '}';
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public boolean isSuccess() {
        return isSuccess.equalsIgnoreCase("y");
    }
}
