/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.widget;

import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.media.PlayerAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 24,04,2020
 */
public abstract class TimeShiftDetailsDescriptionPresenter extends Presenter {

    private final int mResourceId = R.layout.view_details_description_timeshift;

    @Override
    public final ViewHolder onCreateViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(mResourceId, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public final void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        onBindDescription(vh, item);
        boolean hasTitle = true;
        if (TextUtils.isEmpty(vh.mTitle.getText())) {
            vh.mTitle.setVisibility(View.GONE);
            hasTitle = false;
        } else {
            vh.mTitle.setVisibility(View.VISIBLE);
            vh.mTitle.setLineSpacing(vh.mTitleLineSpacing - vh.mTitle.getLineHeight()
                    + vh.mTitle.getLineSpacingExtra(), vh.mTitle.getLineSpacingMultiplier());
            vh.mTitle.setMaxLines(vh.mTitleMaxLines);
        }
        setTopMargin(vh.mTitle, vh.mTitleMargin);
    }

    protected abstract void onBindDescription(ViewHolder vh, Object item);

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
    }

    @Override
    public void onViewAttachedToWindow(Presenter.ViewHolder holder) {
        ViewHolder vh = (ViewHolder) holder;
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(Presenter.ViewHolder holder) {
        ViewHolder vh = (ViewHolder) holder;
        super.onViewDetachedFromWindow(holder);
    }

    private void setTopMargin(TextView textView, int topMargin) {
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) textView.getLayoutParams();
        lp.topMargin = topMargin;
        textView.setLayoutParams(lp);
    }

    public static class ViewHolder extends Presenter.ViewHolder {

        final ImageView mBtnTimeShiftList;
        final ImageView mBtnTimeShiftRW;
        final ImageView mBtnTimeShiftFF;
        final ImageView mBtnTimeShiftLive;
        final TextView mTitle;
        final int mTitleMargin;
        final int mUnderTitleBaselineMargin;
        final int mUnderSubtitleBaselineMargin;
        final int mTitleLineSpacing;
        final int mBodyLineSpacing;
        final int mBodyMaxLines;
        final int mBodyMinLines;
        final Paint.FontMetricsInt mTitleFontMetricsInt;
        final int mTitleMaxLines;
        final PlayerAdapter.Callback callback = new PlayerAdapter.Callback();

        public ViewHolder(final View view) {
            super(view);
            mTitle = view.findViewById(R.id.lb_details_description_title);
            mBtnTimeShiftList = view.findViewById(R.id.btnTimeShiftList);
            mBtnTimeShiftRW = view.findViewById(R.id.btnTimeShiftRW);
            mBtnTimeShiftFF = view.findViewById(R.id.btnTimeShiftFF);
            mBtnTimeShiftLive = view.findViewById(R.id.btnTimeShiftLive);


            Paint.FontMetricsInt titleFontMetricsInt = getFontMetricsInt(mTitle);
            final int titleAscent = view.getResources().getDimensionPixelSize(
                    R.dimen.lb_details_description_title_baseline);
            // Ascent is negative
            mTitleMargin = titleAscent + titleFontMetricsInt.ascent;

            mUnderTitleBaselineMargin = view.getResources().getDimensionPixelSize(
                    R.dimen.lb_details_description_under_title_baseline_margin);
            mUnderSubtitleBaselineMargin = view.getResources().getDimensionPixelSize(
                    R.dimen.lb_details_description_under_subtitle_baseline_margin);

            mTitleLineSpacing = view.getResources().getDimensionPixelSize(
                    R.dimen.lb_details_description_title_line_spacing);
            mBodyLineSpacing = view.getResources().getDimensionPixelSize(
                    R.dimen.lb_details_description_body_line_spacing);

            mBodyMaxLines = view.getResources().getInteger(
                    R.integer.lb_details_description_body_max_lines);
            mBodyMinLines = view.getResources().getInteger(
                    R.integer.lb_details_description_body_min_lines);
            mTitleMaxLines = mTitle.getMaxLines();
            mTitleFontMetricsInt = getFontMetricsInt(mTitle);
        }


        public PlayerAdapter.Callback getCallback() {
            return callback;
        }

        public TextView getTitle() {
            return mTitle;
        }

        private boolean mCanSeekFF = true;
        private boolean mCanSeekRW = true;

        public void setSeekEnable(boolean canSeekRW, boolean canSeekFF) {
            mCanSeekRW = canSeekRW;
            mCanSeekFF = canSeekFF;
            if (canSeekFF) {
                mBtnTimeShiftFF.setAlpha(1f);
                mBtnTimeShiftFF.setFocusable(true);
                mBtnTimeShiftFF.setFocusableInTouchMode(true);
            } else {
                mBtnTimeShiftFF.setAlpha(0.3f);
                mBtnTimeShiftFF.setFocusable(false);
                mBtnTimeShiftFF.setFocusableInTouchMode(false);
            }
            if (canSeekRW) {
                mBtnTimeShiftRW.setAlpha(1f);
                mBtnTimeShiftRW.setFocusable(true);
                mBtnTimeShiftRW.setFocusableInTouchMode(true);
            } else {
                mBtnTimeShiftRW.setAlpha(0.3f);
                mBtnTimeShiftRW.setFocusable(false);
                mBtnTimeShiftRW.setFocusableInTouchMode(false);
            }
        }

        public void setOnControlButtonClickListener(View.OnClickListener onControlButtonClickListener) {
            if (mCanSeekFF) {
                mBtnTimeShiftFF.setOnClickListener(onControlButtonClickListener);
            }
            if (mCanSeekRW) {
                mBtnTimeShiftRW.setOnClickListener(onControlButtonClickListener);
            }

            mBtnTimeShiftList.setOnClickListener(onControlButtonClickListener);
            mBtnTimeShiftLive.setOnClickListener(onControlButtonClickListener);
        }

        private Paint.FontMetricsInt getFontMetricsInt(TextView textView) {
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setTextSize(textView.getTextSize());
            paint.setTypeface(textView.getTypeface());
            return paint.getFontMetricsInt();
        }
    }


}
