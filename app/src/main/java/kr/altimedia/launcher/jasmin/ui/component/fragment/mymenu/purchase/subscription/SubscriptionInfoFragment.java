/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.SubscribedContent;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class SubscriptionInfoFragment extends Fragment {
    public static final String CLASS_NAME = SubscriptionDetailDialog.class.getName();
    private final String TAG = SubscriptionDetailDialog.class.getSimpleName();

    private final int MONTHLY_SUBSCRIPTION = 0;

    private SubscribedContent subscriptionItem;

    private TextView title;
    private TextView subscribedDate;
    private TextView price;
    private TextView priceUnit;
    private TextView period;
    private TextView periodUnit;
    private TextView renewalDateTitle;
    private TextView renewalDate;
    private TextView btnClose;
    private TextView btnStopSubscription;
    private LinearLayout stopSubscriptionInfo;

    public static SubscriptionInfoFragment newInstance(SubscribedContent item) {
        Bundle args = new Bundle();
        args.putParcelable(SubscriptionDetailDialog.KEY_SUBSCRIPTION, item);
        SubscriptionInfoFragment fragment = new SubscriptionInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        return inflater.inflate(R.layout.fragment_mymenu_subscription_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            subscriptionItem = args.getParcelable(SubscriptionDetailDialog.KEY_SUBSCRIPTION);
        }

        initContent(view);
        initButton(view);

        showView();
    }

    private void initContent(View view) {
        title = view.findViewById(R.id.title);
        subscribedDate = view.findViewById(R.id.subscribedDate);
        price = view.findViewById(R.id.price);
        priceUnit = view.findViewById(R.id.priceUnit);
        period = view.findViewById(R.id.period);
        periodUnit = view.findViewById(R.id.periodUnit);
        renewalDateTitle = view.findViewById(R.id.renewalDateTitle);
        renewalDate = view.findViewById(R.id.renewalDate);
    }

    private void initButton(View view) {
        btnClose = view.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SubscriptionDetailDialog) getParentFragment()).hideDetailView();
            }
        });

        btnStopSubscription = view.findViewById(R.id.btnStopSubscription);
        stopSubscriptionInfo = view.findViewById(R.id.stopSubscriptionInfo);

        btnStopSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SubscriptionDetailDialog) getParentFragment()).showDetailView(SubscriptionDetailDialog.TYPE_REQUEST, subscriptionItem);
            }
        });
    }

    private void showView() {
        if (subscriptionItem != null) {
            Log.d(TAG, "showView: " + subscriptionItem);

            title.setText(subscriptionItem.getProductName());

            String dateStr1 = "";
            try {
                dateStr1 = TimeUtil.getModifiedDate(subscriptionItem.getPurchaseDateTime().getTime());
            }catch (Exception e){
            }
            subscribedDate.setText(dateStr1);

            String priceStr = "";
            try {
                priceStr = StringUtil.getFormattedNumber(Long.parseLong(subscriptionItem.getPrice()));
            }catch (Exception e){
            }
            price.setText(priceStr);
            priceUnit.setText(subscriptionItem.getCurrency());

            String periodValue = "";
            try {
                periodUnit.setVisibility(View.GONE);
                if(subscriptionItem.getPeriod() >= 0) {
                    if (subscriptionItem.getPeriod() == MONTHLY_SUBSCRIPTION) {
                        periodValue = getResources().getString(R.string.monthly);
                    } else {
                        periodValue = Integer.toString(subscriptionItem.getPeriod());
                        String periodUnitValue = getResources().getString(R.string.months);
                        if (subscriptionItem.getPeriod() == 1) {
                            periodUnitValue = getResources().getString(R.string.month);
                        }
                        periodUnit.setText(periodUnitValue);
                        periodUnit.setVisibility(View.VISIBLE);
                    }
                }
            } catch (Exception e) {
            }
            period.setText(periodValue);

            String title = getResources().getString(R.string.next_renewal_date2);
            long currTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
            long renewalTime = 0;
            try {
                renewalTime = subscriptionItem.getRenewalDate().getTime();
            }catch (Exception e){
            }
            if(isThisMonth(renewalTime) || subscriptionItem.isRefunded()){
                title = getResources().getString(R.string.expiration_date2);
            }
            renewalDateTitle.setText(title);

            String dateStr2 = "";
            try {
                dateStr2 = TimeUtil.getModifiedDate(renewalTime);
            }catch (Exception e){
            }
            renewalDate.setText(dateStr2);

            if(subscriptionItem.isRefunded()){
                btnStopSubscription.setVisibility(View.GONE);
            }else {
                btnStopSubscription.setVisibility(View.VISIBLE);
            }
            stopSubscriptionInfo.setVisibility(View.GONE);
        } else {
            btnStopSubscription.setVisibility(View.GONE);
        }
        btnClose.requestFocus();
    }

    private boolean isThisMonth(long targetTime){
        Calendar currCal = Calendar.getInstance();
        currCal.setTimeInMillis(JasmineEpgApplication.SystemClock().currentTimeMillis());
        int currYear = currCal.get(Calendar.YEAR);
        int currMonth = currCal.get(Calendar.MONTH) + 1;

        Calendar targetCal = Calendar.getInstance();
        targetCal.setTimeInMillis(targetTime);
        int targetYear = targetCal.get(Calendar.YEAR);
        int targetMonth = targetCal.get(Calendar.MONTH) + 1;

        int month1 = currYear * 12 + currMonth;
        int month2 = targetYear * 12 + targetMonth;
        if (month1 == month2) {
            return true;
        }
        return false;
    }
}
