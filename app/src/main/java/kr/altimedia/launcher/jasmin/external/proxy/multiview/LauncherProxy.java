/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.external.proxy.multiview;

import android.content.Intent;
import android.os.Parcelable;

import kr.altimedia.launcher.jasmin.external.proxy.multiview.data.ChannelInfo;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;

public class LauncherProxy {

    public static final String LAUNCHER_PACKAGE_NAME = "kr.altimedia.launcher.jasmin";
    public static final String LAUNCHER_SELECT_ACTION = "altimedia.intent.action.StartLiveTv";

    public static final String INTENT_NAME_SAID = "said";
    public static final String INTENT_NAME_PROFILE_ID = "profileId";
    public static final String INTENT_NAME_LA_URL = "laUrl";
    public static final String INTENT_NAME_CHANNEL = "channelData";
    public static final String INTENT_NAME_LOGIN_TOKEN = "loginToken";
    public static final String INTENT_NAME_RATING = "rating";
    public static final String INTENT_NAME_SELECT_CHANNEL = PlaybackUtil.EXTRA_LIVETV_TUNE_CHANNEL_ID;

    private Intent intent;

    public LauncherProxy(Intent intent) {
        this.intent = intent;
    }

    public String getLaURL() {
        return intent.getStringExtra(INTENT_NAME_LA_URL);
    }

    public String getSAID() {
        return intent.getStringExtra(INTENT_NAME_SAID);
    }

    public String getProfileID() {
        return intent.getStringExtra(INTENT_NAME_PROFILE_ID);
    }

    public int getRating() {
        return intent.getIntExtra(INTENT_NAME_RATING, -1);
    }

    public ChannelInfo[] getChannels() {
        ChannelInfo[] result = null;

        Parcelable[] ps = intent.getParcelableArrayExtra(INTENT_NAME_CHANNEL);

        if (ps != null && ps.length > 0) {
            result = new ChannelInfo[ps.length];
            System.arraycopy(ps, 0, result, 0, ps.length);
        }

        return result;
    }

    public String getLoginToken() {
        return intent.getStringExtra(INTENT_NAME_LOGIN_TOKEN);
    }

    public Intent getSelectChannelIntent(long channelId) {
        Intent selectIntent = new Intent(LAUNCHER_SELECT_ACTION);

        selectIntent.setPackage(LAUNCHER_PACKAGE_NAME);
        selectIntent.putExtra(INTENT_NAME_SELECT_CHANNEL, channelId);

        return selectIntent;
    }

}
