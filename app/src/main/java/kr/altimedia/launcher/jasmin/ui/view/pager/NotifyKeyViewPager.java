/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.pager;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

/**
 * Created by mc.kim on 07,01,2020
 */
public class NotifyKeyViewPager extends ViewPager {
    public NotifyKeyViewPager(@NonNull Context context) {
        super(context);
    }

    public NotifyKeyViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (executeKeyEvent(event)) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }
}
