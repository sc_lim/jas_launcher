package kr.altimedia.launcher.jasmin.ui.view.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;

import com.altimedia.util.Log;

import androidx.appcompat.widget.AppCompatSeekBar;
import kr.altimedia.launcher.jasmin.R;

public class RangeAppCompatSeekBar extends AppCompatSeekBar {
    private final String TAG = RangeAppCompatSeekBar.class.getSimpleName();
    private Drawable mBaseDrawable;
    private Drawable mTertiaryDrawable;
    private int mBaseProgress = 0;
    private int mTertiaryProgress = 0;

    public RangeAppCompatSeekBar(Context context) {
        this(context, null);
    }

    public RangeAppCompatSeekBar(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.seekBarStyle);
    }

    public RangeAppCompatSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Drawable progressDrawable = getProgressDrawable();
        if (progressDrawable instanceof LayerDrawable) {
            Drawable baseDrawable = new ColorDrawable(getResources().getColor(R.color.color_FF7B7B7B));
            Drawable tertiaryDrawable = new ColorDrawable(getResources().getColor(R.color.color_FF7B7B7B));
            setBaseDrawable(baseDrawable);
            setTertiaryDrawable(tertiaryDrawable);
        }
    }

    private void setBaseDrawable(Drawable drawable) {
        if (mBaseDrawable != null) {
            mBaseDrawable.setCallback(null);
        }

        mBaseDrawable = drawable;

        if (drawable != null) {
            drawable.setCallback(this);
        }
    }

    private void setTertiaryDrawable(Drawable drawable) {
        if (mTertiaryDrawable != null) {
            mTertiaryDrawable.setCallback(null);
        }

        mTertiaryDrawable = drawable;

        if (drawable != null) {
            drawable.setCallback(this);
        }
    }

    @Override
    public synchronized void setProgress(int progress) {
        super.setProgress(progress);
        invalidate();
    }

    @Override
    public synchronized void setSecondaryProgress(int secondaryProgress) {
        super.setSecondaryProgress(secondaryProgress);
        invalidate();
    }

    public void setBaseProgress(int baseProgress) {
        this.mBaseProgress = baseProgress;
        if (Log.INCLUDE) {
            Log.d(TAG, "setBaseProgress | baseProgress : " + baseProgress);
        }

        invalidate();
    }


    public void setTertiaryProgress(int tertiaryProgress) {
        this.mTertiaryProgress = tertiaryProgress;
        if (Log.INCLUDE) {
            Log.d(TAG, "setTertiaryProgress | tertiaryProgress : " + tertiaryProgress);
        }
        invalidate();
    }


    @Override
    protected synchronized void onDraw(Canvas canvas) {
//        super.onDraw(canvas);

        drawBaseProgress(canvas);
        getThumb().draw(canvas);

    }

    void drawBaseProgress(Canvas canvas) {
        float secondaryRatio = (float) getSecondaryProgress() / (float) getMax();
        float baseRatio = (float) mBaseProgress / (float) getMax();
        if (Log.INCLUDE) {
            Log.d(TAG, "drawBaseProgress | mBaseProgress : " + mBaseProgress + ", baseRatio : " + baseRatio);

        }

        float tertiaryRatio = (float) mTertiaryProgress / (float) getMax();
        if (Log.INCLUDE) {
            Log.d(TAG, "drawBaseProgress | mTertiaryProgress : " + mTertiaryProgress + ", baseRatio : " + baseRatio);

        }
        int baseProgressWidth = (int) (getWidth() * baseRatio);
        int tertiaryProgressWidth = (int) (getWidth() * tertiaryRatio);


        if (Log.INCLUDE) {
            Log.d(TAG, "drawBaseProgress | baseProgressWidth : " + baseProgressWidth + ", tertiaryProgressWidth : " + tertiaryProgressWidth);

            Log.d(TAG, "drawBaseProgress | getWidth() : " + getWidth());
            Log.d(TAG, "drawBaseProgress | getMax() : " + getMax());
        }

        int padding = getResources().getDimensionPixelOffset(R.dimen.progress_padding);


        if (mBaseDrawable != null) {
            final Rect rect;
            if (baseProgressWidth == 0) {
                rect = new Rect(0, padding, 0, getHeight() - padding);
            } else {
                rect = new Rect(baseProgressWidth, padding, getWidth(), getHeight() - padding);
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "onDraw : " + rect.toString());
            }
            mBaseDrawable.setBounds(rect);
        }


        if (mTertiaryDrawable != null) {
            Rect rect = new Rect(getPaddingLeft(), padding, tertiaryProgressWidth, getHeight() - padding);
            if (Log.INCLUDE) {
                Log.d(TAG, "onDraw : " + rect.toString());
            }
            mTertiaryDrawable.setBounds(rect);
        }

        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{getProgressDrawable(), mBaseDrawable, mTertiaryDrawable});

        layerDrawable.draw(canvas);

    }

}
