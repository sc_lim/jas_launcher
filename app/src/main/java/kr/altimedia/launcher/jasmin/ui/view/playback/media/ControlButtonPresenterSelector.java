/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.animation.AnimatorInflater;
import android.animation.ValueAnimator;
import android.content.Context;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.leanback.widget.Action;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow;

import static kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow.PlayPauseAction.INDEX_FAST_FORWARD;
import static kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow.PlayPauseAction.INDEX_REWIND;

/**
 * Created by mc.kim on 27,04,2020
 */
public class ControlButtonPresenterSelector extends PresenterSelector {
    private static final String TAG = ControlButtonPresenterSelector.class.getSimpleName();

    private final Presenter mPrimaryPresenter =
            new ControlButtonPresenter(R.layout.view_control_button_primary);
    private final Presenter[] mPresenters = new Presenter[]{mPrimaryPresenter};

    /**
     * Returns the presenter for primary controls.
     */
    public Presenter getPrimaryPresenter() {
        return mPrimaryPresenter;
    }

    /**
     * Returns the presenter for secondary controls.
     */

    /**
     * Always returns the presenter for primary controls.
     */
    @Override
    public Presenter getPresenter(Object item) {
        return mPrimaryPresenter;
    }

    @Override
    public Presenter[] getPresenters() {
        return mPresenters;
    }

    static class ActionViewHolder extends Presenter.ViewHolder implements ValueAnimator.AnimatorUpdateListener {
        private ValueAnimator rotationLeftAnimator;
        private ValueAnimator rotationRightAnimator;

        ImageView mIcon;
        TextView mLabel;
        TextView mLabelSuffix;
        View mFocusableView;

        public ActionViewHolder(View view) {
            super(view);
            mIcon = view.findViewById(R.id.icon);
            mLabel = view.findViewById(R.id.label);
            mLabelSuffix = view.findViewById(R.id.labelSuffix);
            mFocusableView = view.findViewById(R.id.button);

            rotationLeftAnimator = loadAnimator(view.getContext(), R.animator.rotation_left_90);
            rotationRightAnimator = loadAnimator(view.getContext(), R.animator.rotation_right_90);
            rotationLeftAnimator.addUpdateListener(this);
            rotationRightAnimator.addUpdateListener(this);
        }

        private ValueAnimator loadAnimator(Context context, int resId) {
            ValueAnimator animator = (ValueAnimator) AnimatorInflater.loadAnimator(context, resId);
            return animator;
        }

        public void reset() {
            if (rotationLeftAnimator.isRunning()) {
                rotationLeftAnimator.cancel();
            }

            if (rotationRightAnimator.isRunning()) {
                rotationRightAnimator.cancel();
            }
        }

        public void startAnimation(VideoPlaybackControlsRow.PlayPauseAction mAction) {
            reset();

            int index = mAction.getIndex();
            int action = mAction.getAction();


            ValueAnimator animator = null;

            if (index == INDEX_REWIND) {
                animator = rotationLeftAnimator;
            } else if (index == INDEX_FAST_FORWARD) {
                animator = rotationRightAnimator;
            }

            if (animator != null && action != KeyEvent.ACTION_DOWN) {
                animator.start();

            } else {
                if (mIcon.getRotation() != 0) {
                    mIcon.setRotation(0);
                }
            }
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            float value = (Float) animation.getAnimatedValue();
            mIcon.setRotation(value);
        }
    }

    static class ControlButtonPresenter extends Presenter {
        private int mLayoutResourceId;

        ControlButtonPresenter(int layoutResourceId) {
            mLayoutResourceId = layoutResourceId;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(mLayoutResourceId, parent, false);
            return new ActionViewHolder(v);
        }

        @Override
        public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
            Action action = (Action) item;
            ActionViewHolder vh = (ActionViewHolder) viewHolder;
            vh.mIcon.setImageDrawable(action.getIcon());
            vh.startAnimation((VideoPlaybackControlsRow.PlayPauseAction) action);

            if (Log.INCLUDE) {
                Log.d(TAG, "getLabel1 : " + action.getLabel1());
                Log.d(TAG, "getLabel2 : " + action.getLabel2());
                Log.d(TAG, "getIndex : " + ((VideoPlaybackControlsRow.PlayPauseAction) action).getIndex());
                Log.d(TAG, "getAction : " + ((VideoPlaybackControlsRow.PlayPauseAction) action).getAction());
            }


            if (vh.mLabel != null) {

                if (action.getLabel1().length() != 0) {

                    if (Log.INCLUDE) {
                        Log.d(TAG, "setText : " + action.getLabel1());
                    }
                    vh.mLabel.setVisibility(View.VISIBLE);
                    vh.mLabelSuffix.setVisibility(View.VISIBLE);
                } else {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "setText : empty");
                    }
                    vh.mLabel.setVisibility(View.INVISIBLE);
                    vh.mLabelSuffix.setVisibility(View.INVISIBLE);
                }
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "getText : " + vh.mLabel.getText());
            }
            CharSequence contentDescription = TextUtils.isEmpty(action.getLabel2())
                    ? action.getLabel1() : action.getLabel2();
            if (!TextUtils.equals(vh.mFocusableView.getContentDescription(), contentDescription)) {
                vh.mFocusableView.setContentDescription(contentDescription);
                vh.mFocusableView.sendAccessibilityEvent(
                        AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED);
            }
        }

        @Override
        public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
            ActionViewHolder vh = (ActionViewHolder) viewHolder;
            vh.reset();
            vh.mIcon.setImageDrawable(null);
            if (vh.mLabel != null) {
                vh.mLabel.setText(null);
            }
            vh.mFocusableView.setContentDescription(null);
        }

        @Override
        public void setOnClickListener(Presenter.ViewHolder viewHolder,
                                       View.OnClickListener listener) {
            ((ActionViewHolder) viewHolder).mFocusableView.setOnClickListener(listener);
        }
    }
}
