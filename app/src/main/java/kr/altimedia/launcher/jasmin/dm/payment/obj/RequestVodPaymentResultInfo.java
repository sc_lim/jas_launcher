/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.payment.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestVodPaymentResultInfo implements Parcelable {
    @Expose
    @SerializedName("purchaseId")
    private String purchaseId;
    @Expose
    @SerializedName("qrCode")
    private String qrCode;

    protected RequestVodPaymentResultInfo(Parcel in) {
        purchaseId = in.readString();
        qrCode = in.readString();
    }

    public static final Creator<RequestVodPaymentResultInfo> CREATOR = new Creator<RequestVodPaymentResultInfo>() {
        @Override
        public RequestVodPaymentResultInfo createFromParcel(Parcel in) {
            return new RequestVodPaymentResultInfo(in);
        }

        @Override
        public RequestVodPaymentResultInfo[] newArray(int size) {
            return new RequestVodPaymentResultInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(purchaseId);
        dest.writeString(qrCode);
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public String getQrCode() {
        return qrCode;
    }
}
