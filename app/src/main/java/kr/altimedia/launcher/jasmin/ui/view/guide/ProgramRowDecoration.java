/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager;
import kr.altimedia.launcher.jasmin.ui.view.util.FontUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.util.Log;

/**
 * Created by mc.kim on 01,07,2020
 */
class ProgramRowDecoration extends RecyclerView.ItemDecoration {
    private final static String TAG = ProgramRowDecoration.class.getSimpleName();
    private final Context mContext;
    private final Resources mResource;
    private final NinePatchDrawable mBubbleResource;

    private final int mBubbleTopPadding;
    private final int mBubbleHeight;

    private final Paint mTextPaint;

    private final Rect paddingRect;
    private final Paint mDrawingPaint;
    private final long TOOL_TIP_DURATION = 15 * TimeUtil.MIN;
    private final Bitmap mAlarmIcon;
    private final int mAlarmPadding;

    public ProgramRowDecoration(Context context) {
        mContext = context;
        mResource = context.getResources();
        mBubbleResource = (NinePatchDrawable) mResource.getDrawable(R.drawable.title_balloon, null);
        Drawable alarmIcon = mResource.getDrawable(R.drawable.icon_grid_alarm_b, null);
        mAlarmIcon = ((BitmapDrawable) alarmIcon).getBitmap();
        mBubbleTopPadding = mResource.getDimensionPixelSize(R.dimen.program_guide_bubble_padding_top);
        mBubbleHeight = mResource.getDimensionPixelSize(R.dimen.program_guide_bubble_padding_height);

        mAlarmPadding = mResource.getDimensionPixelOffset(R.dimen.program_guide_bubble_alarm_padding);

        int mBubblePaddingLeft = mResource.getDimensionPixelSize(R.dimen.program_guide_bubble_font_padding_left);
        int mBubblePaddingTop = mResource.getDimensionPixelSize(R.dimen.program_guide_bubble_font_padding_top);
        paddingRect = new Rect(mBubblePaddingLeft, mBubblePaddingTop, mBubblePaddingLeft, mBubblePaddingTop);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(mResource.getColor(R.color.color_FF222535));
        mTextPaint.setTextSize(mResource.getDimensionPixelSize(R.dimen.program_guide_bubble_font_size));
        mTextPaint.setTypeface(FontUtil.getInstance().getTypeface(FontUtil.Font.medium));

        mDrawingPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mDrawingPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    }

    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
        int[] originalPos = new int[2];
        parent.getLocationInWindow(originalPos);
        int x = originalPos[0];
        int y = originalPos[1];
        if (Log.INCLUDE) {
            Log.d(TAG, "parent x : " + x + ", y : " + y);
        }


        int childCount = parent.getChildCount();

        for (int i = 0; i < childCount; i++) {
            View view = parent.getChildAt(i);
            ProgramRow row = null;
            if ((view instanceof LinearLayout)) {
                row = view.findViewById(R.id.row);
            } else if (view instanceof ProgramRow) {
                row = (ProgramRow) view;
            }
            if (row == null) {
                continue;
            }
            LinearLayoutManager layoutManager = ((LinearLayoutManager) row.getLayoutManager());
            int childSize = layoutManager.getChildCount();
            for (int index = 0; index < childSize; index++) {
                View childView = layoutManager.getChildAt(index);
                if (childView instanceof ProgramItemView && childView.isFocused()) {
                    drawBubbleItem(c, (ProgramItemView) childView, state, originalPos);
                }
            }
        }
    }

    private void drawBubbleItem(Canvas canvas, ProgramItemView itemView, RecyclerView.State state, int[] parentPosition) {
        ProgramManager.TableEntry entry = itemView.getTableEntry();
        if (entry.program == null) {
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "entry.program channel id() : " + entry.program.getChannelId());
            Log.d(TAG, "entry.program id() : " + entry.program.getId());
            Log.d(TAG, "entry.program getStartTimeUtcMillis() : " + entry.program.getStartTimeUtcMillis());
            Log.d(TAG, "entry.program getEndTimeUtcMillis() : " + entry.program.getEndTimeUtcMillis());
            Log.d(TAG, "entry.program duration () : " + (entry.program.getEndTimeUtcMillis() - entry.program.getStartTimeUtcMillis()));
            Log.d(TAG, "entry.program getDurationMillis () : " + entry.program.getDurationMillis());
        }

//        if (entry.program.getDurationMillis() > TOOL_TIP_DURATION) {
//            return;
//        }

        int[] originalPos = new int[2];
        itemView.getLocationInWindow(originalPos);
        int x = originalPos[0];
        int y = originalPos[1];
        if (Log.INCLUDE) {
            Log.d(TAG, "getLocationInWindow parent x : " + parentPosition[0] + ", y : " + parentPosition[1]);
            Log.d(TAG, "getLocationInWindow x : " + x + ", y : " + y);
        }
        int childViewLeft = x - parentPosition[0];
        int childViewTop = y - parentPosition[1] + mBubbleTopPadding;
        int childViewRight = childViewLeft + itemView.getMeasuredWidth();
        int childViewBottom = childViewTop + mBubbleHeight;

        Rect npdBounds = new Rect(childViewLeft, childViewTop, childViewRight, childViewBottom);
        drawBubble(canvas, mBubbleResource, npdBounds, entry.program);
    }

        private void drawBubble(Canvas canvas, NinePatchDrawable mBubbleResource, Rect rect, Program program) {
            boolean isReserved = ReminderAlertManager.getInstance().isReserved(program);

            int textWidth = getTextWidth(mTextPaint, program.getTitle());
            int resultWidth = textWidth + +(isReserved ? (mAlarmPadding + mAlarmIcon.getWidth() + mAlarmPadding) : 0);
            Rect boundRect = calculationBubbleRect(rect, resultWidth);

            mBubbleResource.setBounds(boundRect);
            mBubbleResource.draw(canvas);
            canvas.drawText(program.getTitle(), boundRect.left + paddingRect.left, boundRect.top + paddingRect.top, mTextPaint);
            if (isReserved) {
                int left = boundRect.left + paddingRect.left + textWidth;
                Rect iconRect = new Rect(left,
                        boundRect.top + paddingRect.top,
                        left + mAlarmIcon.getWidth(), boundRect.top + paddingRect.top + mAlarmIcon.getHeight());
                if (Log.INCLUDE) {
                    Log.d(TAG, "iconRect : " + iconRect.toString());
                }
                canvas.drawBitmap(mAlarmIcon, iconRect, iconRect, mDrawingPaint);
            }
        }

        private Rect calculationBubbleRect(Rect imageBounds, int textWidth) {
            int left = imageBounds.left;
            int top = imageBounds.top;
            int width = imageBounds.width();
            int height = imageBounds.height();


        if (width < textWidth) {
            return imageBounds;
        } else {
//            left += (width - textWidth) / 2;
            Rect boundRect = new Rect(left,
                    top,
                    left + textWidth + (2 * paddingRect.right),
                    top + height);
            return boundRect;
        }
    }

    private int getTextWidth(Paint textPaint, String message) {
        Rect bounds = new Rect();
        textPaint.getTextBounds(message, 0, message.length(), bounds);
        return bounds.width();
    }
}
