/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class CouponDiscountDetailFragment extends Fragment {
    public static final String CLASS_NAME = CouponDiscountDetailFragment.class.getName();
    private final String TAG = CouponDiscountDetailFragment.class.getSimpleName();

    private static final String KEY_ITEM = "ITEM";
    private final String UNLIMITED = "31.12.9999";

    private CouponDiscountDetailFragment() {
    }

    public static CouponDiscountDetailFragment newInstance(Coupon item) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_ITEM, item);

        CouponDiscountDetailFragment fragment = new CouponDiscountDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_coupon_history_detail_discount, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Coupon item = getArguments().getParcelable(KEY_ITEM);

        initView(view, item);
    }

    private void initView(View view, Coupon item) {
        TextView couponPercent = view.findViewById(R.id.couponPercent);
        TextView discountValue = view.findViewById(R.id.discountValue);
        TextView discountUnit = view.findViewById(R.id.discountUnit);
        TextView expirationDate = view.findViewById(R.id.expirationDate);
        RelativeLayout couponBgImage = view.findViewById(R.id.couponBgImage);
        LinearLayout useConditionLayer = view.findViewById(R.id.useConditionLayer);
        TextView useAmountValue = view.findViewById(R.id.useAmountValue);
        TextView useAmountUnit = view.findViewById(R.id.useAmountUnit);

        DiscountCoupon dc = (DiscountCoupon)item;
        couponBgImage.setBackground(view.getResources().getDrawable(dc.getBgImage()));
        couponPercent.setText(String.valueOf(dc.getRate()));
        discountValue.setText(String.valueOf(dc.getRate()));
        discountUnit.setText("%");
        if(dc.getExpireDate() != null) {
            String dateStr = TimeUtil.getModifiedDate(dc.getExpireDate().getTime());
            if(UNLIMITED.equals(dateStr)){
                expirationDate.setText(R.string.unlimited);
            }else {
                expirationDate.setText(dateStr);
            }
        }

        useConditionLayer.setVisibility(View.GONE);
        try{
            if(dc.getMaxAmount() > 0) {
                useAmountValue.setText(StringUtil.getFormattedNumber(dc.getMaxAmount()));
                useConditionLayer.setVisibility(View.VISIBLE);
            }
        }catch(Exception e){
        }

        TextView close = view.findViewById(R.id.btnClose);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CouponDetailDialog) getParentFragment()).dismiss();
            }
        });
    }
}
