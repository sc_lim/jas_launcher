/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.reminder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.util.Date;

import kr.altimedia.launcher.jasmin.R;

public class ReminderConflictDialog extends DialogFragment {
    public static final String CLASS_NAME = ReminderConflictDialog.class.getName();
    private final String TAG = ReminderConflictDialog.class.getSimpleName();

    private static final String KEY_REMINDER_INFO = "REMINDER_INFO";

    private OnReminderConflictListener onReminderConflictListener;

    private ReminderConflictDialog() {
    }

    public static ReminderConflictDialog newInstance(Reminder reminder) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_REMINDER_INFO, reminder);

        ReminderConflictDialog fragment = new ReminderConflictDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnReminderConflictListener(OnReminderConflictListener onReminderConflictListener) {
        this.onReminderConflictListener = onReminderConflictListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_reminder_conflict, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Reminder reminder = getArguments().getParcelable(KEY_REMINDER_INFO);

        initView(view, reminder);
        initButtons(view, reminder);
    }

    private void initView(View view, Reminder reminder) {
        TextView channelNumber = view.findViewById(R.id.channel_number);
        TextView channelName = view.findViewById(R.id.channel_name);
        TextView title = view.findViewById(R.id.title);

        long channelId = reminder.getChannelId();
        Channel channel = TvSingletons.getSingletons(view.getContext()).getChannelDataManager().getChannel(channelId);
        if (Log.INCLUDE) {
            Log.d(TAG, "initView, channelId : " + channelId + ", channel : " + channel);
        }

        if (channel != null) {
            String num = String.format("%03d", Integer.parseInt(channel.getDisplayNumber()));
            channelNumber.setText(num);
            channelName.setText(channel.getDisplayName());
        }

        title.setText(reminder.getTitle());
        setTime(view, reminder);
    }

    private void setTime(View view, Reminder reminder) {
        TextView startTime = view.findViewById(R.id.start_time);
        TextView endTime = view.findViewById(R.id.end_time);

        Date startDate = new Date(reminder.getStartTime());
        Date endDate = new Date(reminder.getEndTime());

        String start = getFormattedTime(startDate.getHours()) + ":" + getFormattedTime(startDate.getMinutes());
        String end = getFormattedTime(endDate.getHours()) + ":" + getFormattedTime(endDate.getMinutes());
        startTime.setText(start);
        endTime.setText(end);
    }

    private String getFormattedTime(int t) {
        return t < 10 ? "0" + t : String.valueOf(t);
    }

    private void initButtons(View view, Reminder reminder) {
        TextView confirm = view.findViewById(R.id.confirm);
        TextView cancel = view.findViewById(R.id.cancel);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onReminderConflictListener != null) {
                    onReminderConflictListener.onRemoveConflict();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onReminderConflictListener != null) {
                    onReminderConflictListener.onCancelReminder();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onReminderConflictListener = null;
    }

    public interface OnReminderConflictListener {
        void onRemoveConflict();

        void onCancelReminder();
    }
}
