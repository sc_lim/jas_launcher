/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.DividerPresenter;
import androidx.leanback.widget.DividerRow;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.util.FocusHighlightHelper;

import static android.view.ViewGroup.FOCUS_AFTER_DESCENDANTS;
import static android.view.ViewGroup.FOCUS_BEFORE_DESCENDANTS;

public class HeadersFragment extends BaseRowFragment {
    private HeadersFragment.OnHeaderViewSelectedListener mOnHeaderViewSelectedListener;
    HeadersFragment.OnHeaderClickedListener mOnHeaderClickedListener;
    private boolean mHeadersEnabled = true;
    private boolean mHeadersGone = false;
    private int mBackgroundColor;
    private boolean mBackgroundColorSet;
    private static final PresenterSelector sHeaderPresenter;
    private final ItemBridgeAdapter.AdapterListener mAdapterListener = new ItemBridgeAdapter.AdapterListener() {
        public void onCreate(final ItemBridgeAdapter.ViewHolder viewHolder) {
            View headerView = viewHolder.getViewHolder().view;
            headerView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (HeadersFragment.this.mOnHeaderClickedListener != null) {
                        HeadersFragment.this.mOnHeaderClickedListener.onHeaderClicked((RowHeaderPresenter.ViewHolder) viewHolder.getViewHolder(), (Row) viewHolder.getItem());
                    }

                }
            });
            if (HeadersFragment.this.mWrapper != null) {
                viewHolder.itemView.addOnLayoutChangeListener(HeadersFragment.sLayoutChangeListener);
            } else {
                headerView.addOnLayoutChangeListener(HeadersFragment.sLayoutChangeListener);
            }

        }
    };
    static View.OnLayoutChangeListener sLayoutChangeListener;
    final ItemBridgeAdapter.Wrapper mWrapper = new ItemBridgeAdapter.Wrapper() {
        public void wrap(View wrapper, View wrapped) {
            ((FrameLayout) wrapper).addView(wrapped);
        }

        public View createWrapper(View root) {
            return new HeadersFragment.NoOverlappingFrameLayout(root.getContext());
        }
    };

    public HeadersFragment() {
        this.setPresenterSelector(sHeaderPresenter);
        FocusHighlightHelper.setupHeaderItemFocusHighlight(this.getBridgeAdapter());
    }

    public void setOnHeaderClickedListener(HeadersFragment.OnHeaderClickedListener listener) {
        this.mOnHeaderClickedListener = listener;
    }

    public void setOnHeaderViewSelectedListener(HeadersFragment.OnHeaderViewSelectedListener listener) {
        this.mOnHeaderViewSelectedListener = listener;
    }

    VerticalGridView findGridViewFromRoot(View view) {
        return (VerticalGridView) view.findViewById(R.id.browse_headers);
    }

    void onRowSelected(RecyclerView parent, RecyclerView.ViewHolder viewHolder, int position, int subposition) {
        if (this.mOnHeaderViewSelectedListener != null) {
            if (viewHolder != null && position >= 0) {
                ItemBridgeAdapter.ViewHolder vh = (ItemBridgeAdapter.ViewHolder) viewHolder;
                this.mOnHeaderViewSelectedListener.onHeaderSelected((RowHeaderPresenter.ViewHolder) vh.getViewHolder(), (Row) vh.getItem());
            } else {
                this.mOnHeaderViewSelectedListener.onHeaderSelected((RowHeaderPresenter.ViewHolder) null, (Row) null);
            }
        }

    }

    int getLayoutResourceId() {
        return R.layout.fragment_header;
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        VerticalGridView listView = this.getVerticalGridView();
        if (listView != null) {
            if (this.mBackgroundColorSet) {
                listView.setBackgroundColor(this.mBackgroundColor);
                this.updateFadingEdgeToBrandColor(this.mBackgroundColor);
            } else {
                Drawable d = listView.getBackground();
                if (d instanceof ColorDrawable) {
                    this.updateFadingEdgeToBrandColor(((ColorDrawable) d).getColor());
                }
            }

            this.updateListViewVisibility();
        }
    }

    private void updateListViewVisibility() {
        VerticalGridView listView = this.getVerticalGridView();
        if (listView != null) {
            this.getView().setVisibility(this.mHeadersGone ? View.GONE : View.VISIBLE);
            if (!this.mHeadersGone) {
                if (this.mHeadersEnabled) {
                    listView.setChildrenVisibility(View.VISIBLE);
                } else {
                    listView.setChildrenVisibility(View.INVISIBLE);
                }
            }
        }

    }

    void setHeadersEnabled(boolean enabled) {
        this.mHeadersEnabled = enabled;
        this.updateListViewVisibility();
    }

    void setHeadersGone(boolean gone) {
        this.mHeadersGone = gone;
        this.updateListViewVisibility();
    }

    void updateAdapter() {
        super.updateAdapter();
        ItemBridgeAdapter adapter = this.getBridgeAdapter();
        adapter.setAdapterListener(this.mAdapterListener);
        adapter.setWrapper(this.mWrapper);
    }

    void setBackgroundColor(int color) {
        this.mBackgroundColor = color;
        this.mBackgroundColorSet = true;
        if (this.getVerticalGridView() != null) {
            this.getVerticalGridView().setBackgroundColor(this.mBackgroundColor);
            this.updateFadingEdgeToBrandColor(this.mBackgroundColor);
        }

    }

    private void updateFadingEdgeToBrandColor(int backgroundColor) {
        View fadingView = this.getView().findViewById(R.id.fade_out_edge);
        Drawable background = fadingView.getBackground();
        if (background instanceof GradientDrawable) {
            background.mutate();
            ((GradientDrawable) background).setColors(new int[]{0, backgroundColor});
        }

    }

    public void onTransitionStart() {
        super.onTransitionStart();
        if (!this.mHeadersEnabled) {
            VerticalGridView listView = this.getVerticalGridView();
            if (listView != null) {
                listView.setDescendantFocusability(FOCUS_BEFORE_DESCENDANTS);
                if (listView.hasFocus()) {
                    listView.requestFocus();
                }
            }
        }

    }

    public void onTransitionEnd() {
        if (this.mHeadersEnabled) {
            VerticalGridView listView = this.getVerticalGridView();
            if (listView != null) {
                listView.setDescendantFocusability(FOCUS_AFTER_DESCENDANTS);
                if (listView.hasFocus()) {
                    listView.requestFocus();
                }
            }
        }

        super.onTransitionEnd();
    }

    public boolean isScrolling() {
        return this.getVerticalGridView().getScrollState() != 0;
    }

    static {
        sHeaderPresenter = (new ClassPresenterSelector()).addClassPresenter(DividerRow.class, new DividerPresenter());
//               .addClassPresenter(Row.class, new RowHeaderPresenter(R.layout.item_menu_row_header,true)) .addClassPresenter(SectionRow.class, new RowHeaderPresenter(R.layout.item_row_header,false));
        sLayoutChangeListener = new View.OnLayoutChangeListener() {
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
//                v.setPivotX(v.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL ? (float) v.getWidth() : 0.0F);
//                v.setPivotY((float) (v.getMeasuredHeight() / 2));
            }
        };
    }

    static class NoOverlappingFrameLayout extends FrameLayout {
        public NoOverlappingFrameLayout(Context context) {
            super(context);
        }

        public boolean hasOverlappingRendering() {
            return false;
        }
    }

    public interface OnHeaderViewSelectedListener {
        void onHeaderSelected(RowHeaderPresenter.ViewHolder var1, Row var2);
    }

    public interface OnHeaderClickedListener {
        void onHeaderClicked(RowHeaderPresenter.ViewHolder var1, Row var2);
    }
}
