/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.category.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;

/**
 * Created by mc.kim on 08,05,2020
 */
public class CategoryVersionCheckResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private CategoryVersionResult result;

    protected CategoryVersionCheckResponse(Parcel in) {
        super(in);
        result = in.readTypedObject(CategoryVersionResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(result, flags);
    }


    @Override
    public String toString() {
        return "CategoryVersionCheckResponse{" +
                "result=" + result +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CategoryVersionCheckResponse> CREATOR = new Creator<CategoryVersionCheckResponse>() {
        @Override
        public CategoryVersionCheckResponse createFromParcel(Parcel in) {
            return new CategoryVersionCheckResponse(in);
        }

        @Override
        public CategoryVersionCheckResponse[] newArray(int size) {
            return new CategoryVersionCheckResponse[size];
        }
    };

    public CategoryVersionResult getResult() {
        return result;
    }

    public static class CategoryVersionResult implements Parcelable {
        @Expose
        @SerializedName("version")
        private String version;
        @Expose
        @SerializedName("updateYn")
        private String updateYN;
        @Expose
        @SerializedName("rootCategoryId")
        private String rootCategoryId;
        @Expose
        @SerializedName("categoryList")
        private ArrayList<CategoryVersionList> categoryList;

        protected CategoryVersionResult(Parcel in) {
            version = in.readString();
            updateYN = in.readString();
            rootCategoryId = in.readString();
            categoryList = in.createTypedArrayList(CategoryVersionList.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(version);
            dest.writeString(updateYN);
            dest.writeString(rootCategoryId);
            dest.writeTypedList(categoryList);
        }


        public ArrayList<CategoryVersionList> getCategoryList() {
            return categoryList;
        }

        public boolean isContainCategory(String categoryId) {
            if (categoryList == null) {
                return false;
            }
            for (CategoryVersionList list : categoryList) {
                if (list.categoryId.equalsIgnoreCase(categoryId)) {
                    return true;
                }
            }
            return false;
        }

        public String getCategoryVersion(String categoryId) {
            if (categoryList == null) {
                return "-1";
            }
            for (CategoryVersionList list : categoryList) {
                if (list.categoryId.equalsIgnoreCase(categoryId)) {
                    return list.getVersion();
                }
            }
            return "-1";
        }

        @Override
        public String toString() {
            return "CategoryVersionResult{" +
                    "version='" + version + '\'' +
                    ", updateYN='" + updateYN + '\'' +
                    ", rootCategoryId='" + rootCategoryId + '\'' +
                    ", categoryList=" + categoryList +
                    '}';
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<CategoryVersionResult> CREATOR = new Creator<CategoryVersionResult>() {
            @Override
            public CategoryVersionResult createFromParcel(Parcel in) {
                return new CategoryVersionResult(in);
            }

            @Override
            public CategoryVersionResult[] newArray(int size) {
                return new CategoryVersionResult[size];
            }
        };

        public String getVersion() {
            return version;
        }

        public boolean isUpdateYN() {
            return updateYN.equalsIgnoreCase("Y");
        }

        public String getRootCategoryId() {
            return rootCategoryId;
        }
    }

    private static class CategoryVersionList implements Parcelable {
        @Expose
        @SerializedName("categoryId")
        private String categoryId;
        @Expose
        @SerializedName("version")
        private String version;

        protected CategoryVersionList(Parcel in) {
            categoryId = in.readString();
            version = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(categoryId);
            dest.writeString(version);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<CategoryVersionList> CREATOR = new Creator<CategoryVersionList>() {
            @Override
            public CategoryVersionList createFromParcel(Parcel in) {
                return new CategoryVersionList(in);
            }

            @Override
            public CategoryVersionList[] newArray(int size) {
                return new CategoryVersionList[size];
            }
        };

        public String getCategoryId() {
            return categoryId;
        }

        public String getVersion() {
            return version;
        }

        @Override
        public String toString() {
            return "CategoryVersionList{" +
                    "categoryId='" + categoryId + '\'' +
                    ", version='" + version + '\'' +
                    '}';
        }
    }
}
