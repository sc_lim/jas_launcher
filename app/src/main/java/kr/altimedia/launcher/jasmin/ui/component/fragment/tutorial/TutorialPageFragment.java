/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.tutorial;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import kr.altimedia.launcher.jasmin.R;

public class TutorialPageFragment extends Fragment {
    public static final String CLASS_NAME = TutorialPageFragment.class.getName();

    private static final String KEY_ITEM = "TUTORIAL_ITEM";

    public static TutorialPageFragment newInstance(TutorialItem item) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_ITEM, item);

        TutorialPageFragment fragment = new TutorialPageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tutorial_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        TutorialItem tutorialItem = getArguments().getParcelable(KEY_ITEM);

        TextView title = view.findViewById(R.id.title);
        TextView desc = view.findViewById(R.id.desc);
        ImageView image = view.findViewById(R.id.image);

        Context context = view.getContext();
        title.setText(context.getString(tutorialItem.getTitle()));
        desc.setText(context.getString(tutorialItem.getDesc()));
        Glide.with(context)
                .load(tutorialItem.getImage()).diskCacheStrategy(DiskCacheStrategy.DATA)
                .centerCrop()
                .placeholder(R.drawable.default_background)
                .error(R.drawable.default_background)
                .into(image);
    }
}
