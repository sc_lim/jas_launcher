/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;

import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

public class RecommendRow extends Row {
    private final ObjectAdapter mAdapter;
    private CharSequence mContentDescription;

    /**
     * Returns the {@link ObjectAdapter} that represents a list of objects.
     */
    public final ObjectAdapter getAdapter() {
        return mAdapter;
    }

    public RecommendRow(HeaderItem header, ObjectAdapter adapter) {
        super(header);
        mAdapter = adapter;
        verify();
    }

    public RecommendRow(long id, HeaderItem header, ObjectAdapter adapter) {
        super(id, header);
        mAdapter = adapter;
        verify();
    }

    public RecommendRow(ObjectAdapter adapter) {
        super();
        mAdapter = adapter;
        verify();
    }

    private void verify() {
        if (mAdapter == null) {
            throw new IllegalArgumentException("ObjectAdapter cannot be null");
        }
    }

    /**
     * Returns content description for the ListRow.  By default it returns
     * {@link HeaderItem#getContentDescription()} or {@link HeaderItem#getName()},
     * unless {@link #setContentDescription(CharSequence)} was explicitly called.
     *
     * @return Content description for the ListRow.
     */
    public CharSequence getContentDescription() {
        if (mContentDescription != null) {
            return mContentDescription;
        }
        final HeaderItem headerItem = getHeaderItem();
        if (headerItem != null) {
            CharSequence contentDescription = headerItem.getContentDescription();
            return contentDescription;
//            return headerItem.getName();
        }
        return null;
    }

    /**
     * Explicitly set content description for the ListRow, {@link #getContentDescription()} will
     * ignore values from HeaderItem.
     *
     * @param contentDescription Content description sets on the ListRow.
     */
    public void setContentDescription(CharSequence contentDescription) {
        mContentDescription = contentDescription;
    }
}
