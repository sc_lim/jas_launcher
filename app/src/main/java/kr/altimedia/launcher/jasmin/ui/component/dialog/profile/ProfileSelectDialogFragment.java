/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog.profile;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.ItemBridgeAdapter;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileList;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileLoginResult;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.presenter.AddProfilePresenter;
import kr.altimedia.launcher.jasmin.ui.component.dialog.presenter.ProfilePresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.ProfileManageDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.ProfilePinCheckDialog;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class ProfileSelectDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = ProfileSelectDialogFragment.class.getName();
    private final String TAG = ProfileSelectDialogFragment.class.getSimpleName();

    protected final UserDataManager mUserDataManager = new UserDataManager();
    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private final AccountManager accountManager = AccountManager.getInstance();
    private MbsDataTask profileTask;
    private OnSelectProfileListener mOnSelectProfileListener;
    private final static String KEY_PROFILE_LIST = "profileInfoList";

    private int maxProfileSize = ProfileManageDialog.MAX_PROFILE_SIZE;

    public ProfileSelectDialogFragment() {
    }

    public static ProfileSelectDialogFragment newInstance(ArrayList<ProfileInfo> profileInfoList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(KEY_PROFILE_LIST, profileInfoList);
        ProfileSelectDialogFragment profileSelectDialogFragment = new ProfileSelectDialogFragment();
        profileSelectDialogFragment.setArguments(bundle);
        return profileSelectDialogFragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    public void setOnSelectProfileListener(OnSelectProfileListener mOnSelectProfileListener) {
        this.mOnSelectProfileListener = mOnSelectProfileListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_profile_select, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated");
        }

        initView(view);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return keyCode == KeyEvent.KEYCODE_BACK
                        || keyCode == KeyEvent.KEYCODE_ESCAPE;
            }
        });
        return dialog;
    }

    private ArrayList<ProfileInfo> mLoadedProfileList = new ArrayList<>();
    private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
        @Override
        public void onSelectProfile(ProfileInfo profileInfo) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onSelectProfile : " + profileInfo.toString());
            }
            if (profileInfo.isLock()) {
                showPinCheckDialog(profileInfo, new ProfilePinCheckDialog.OnPinCheckResultListener() {
                    @Override
                    public void onPinCheckResult(boolean correct, Object object) {
                        if (correct) {
                            setProfileSelection(profileInfo);
                        }
                    }
                });
            } else {
                setProfileSelection(profileInfo);
            }
        }

        @Override
        public void onAddProfile() {
            ProfileManageDialog profileManageDialog = ProfileManageDialog.newInstance(getContext(), ProfileManageDialog.TYPE_ADD, mLoadedProfileList, true);
            profileManageDialog.setProfileUpdateListener(new ProfileManageDialog.ProfileUpdateListener() {
                @Override
                public void onUpdated(String profileNewName) {
                    loadProfileList();
                }
            });
            profileManageDialog.show(getChildFragmentManager(), ProfileManageDialog.CLASS_NAME);
        }

        @Override
        public void onManageProfile() {
            int viewType = ProfileManageDialog.TYPE_LIST;
            ProfileManageDialog profileManageDialog = null;
            if (mLoadedProfileList != null && mLoadedProfileList.size() == 1) {
                viewType = ProfileManageDialog.TYPE_EDIT;
            }
            profileManageDialog = ProfileManageDialog.newInstance(getContext(), viewType, mLoadedProfileList, true);
            profileManageDialog.setProfileUpdateListener(new ProfileManageDialog.ProfileUpdateListener() {
                @Override
                public void onUpdated(String profileNewName) {
                    loadProfileList();
                }
            });
            profileManageDialog.show(getChildFragmentManager(), ProfileManageDialog.CLASS_NAME);

        }
    };

    protected void initProgressbar(View view) {
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void showPinCheckDialog(ProfileInfo profileInfo, ProfilePinCheckDialog.OnPinCheckResultListener listener) {
        ProfilePinCheckDialog pinCheckDialog = ProfilePinCheckDialog.newInstance(profileInfo, listener);
        pinCheckDialog.show(getFragmentManager(), ProfilePinCheckDialog.CLASS_NAME);
    }

    private void setProfileSelection(ProfileInfo profileInfo) {

        mUserDataManager.requestProfileLogin(accountManager.getLoginId(),
                accountManager.getSaId(),
                profileInfo.getProfileId(),
                new MbsDataProvider<String, ProfileLoginResult>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestProfileLogin, onFailed : "+key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, ProfileLoginResult result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestProfileLogin, onSuccess : " + id);
                        }
                        AccountManager.getInstance().onNewProfileLogin(id, result);
                        if (mOnSelectProfileListener != null) {
                            mOnSelectProfileListener.onSelectProfile(profileInfo);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestProfileLogin, onError");
                        }
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(
                                TAG, errorCode, message);
                        errorDialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return ProfileSelectDialogFragment.this.getContext();
                    }
                });
    }

    private void initView(View view) {
        initProgressbar(view);

        ArrayList list = getArguments().getParcelableArrayList(KEY_PROFILE_LIST);
        if (list != null && !list.isEmpty()) {
            initProfiles((ArrayList<ProfileInfo>) list);
        } else {
            loadProfileList();
        }
        initButtons(view);
    }

    private void initProfiles(ArrayList<ProfileInfo> profileInfoList) {
        mLoadedProfileList.clear();
        mLoadedProfileList.addAll(profileInfoList);
        AccountManager.getInstance().updateProfileList(profileInfoList, false);
        if (Log.INCLUDE) {
            for (ProfileInfo profileInfo : profileInfoList) {
                Log.d(TAG, "initProfiles : " + profileInfo);
            }
        }
        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter();
        arrayObjectAdapter.addAll(0, profileInfoList);

        ClassPresenterSelector presenterSelector = new ClassPresenterSelector();
        presenterSelector.addClassPresenter(ProfileInfo.class, new ProfilePresenter());

        if (profileInfoList.size() < maxProfileSize) {
            arrayObjectAdapter.add("Add Profile"); // for add icon
            presenterSelector.addClassPresenter(String.class, new AddProfilePresenter());
        }

        ProfileItemBridgeAdapter profileItemBridgeAdapter = new ProfileItemBridgeAdapter(
                arrayObjectAdapter, presenterSelector, mOnItemClickListener);

        HorizontalGridView gridView = getView().findViewById(R.id.grid_view);
        gridView.setAdapter(profileItemBridgeAdapter);
    }

    private void initButtons(View view) {
        TextView mangeButtons = view.findViewById(R.id.manage_button);
        mangeButtons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onManageProfile();
                }
            }
        });
    }

    private void loadProfileList() {
        profileTask = mUserDataManager.getProfileList(
                accountManager.getSaId(),
                new MbsDataProvider<String, ProfileList>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfileList, onFailed");
                        }
                    }

                    @Override
                    public void onSuccess(String id, ProfileList result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfileList, onSuccess, result : " + result);
                        }

                        initProfiles((ArrayList<ProfileInfo>) result.getProfileInfoList());
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfileList, onError");
                        }

                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(
                                TAG, errorCode, message);
                        errorDialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return ProfileSelectDialogFragment.this.getContext();
                    }
                });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mOnSelectProfileListener = null;
        if (profileTask != null) {
            profileTask.cancel(true);
        }
    }

    private interface OnItemClickListener {
        void onSelectProfile(ProfileInfo profileInfo);

        void onAddProfile();

        void onManageProfile();
    }

    public interface OnSelectProfileListener {
        void onSelectProfile(ProfileInfo profileInfo);
    }

    private static class ProfileItemBridgeAdapter extends ItemBridgeAdapter {
        private OnItemClickListener mOnSelectProfileListener;

        public ProfileItemBridgeAdapter(ObjectAdapter adapter, PresenterSelector presenterSelector, OnItemClickListener mOnSelectProfileListener) {
            super(adapter, presenterSelector);
            this.mOnSelectProfileListener = mOnSelectProfileListener;
        }

        @Override
        protected void onBind(ViewHolder viewHolder) {
            super.onBind(viewHolder);

            Presenter.ViewHolder vh = viewHolder.getViewHolder();
            if (vh instanceof ProfilePresenter.ViewHolder) {
                vh.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (mOnSelectProfileListener != null) {
                            Object item = viewHolder.getItem();
                            ProfileInfo profileInfo = (ProfileInfo) viewHolder.getItem();
                            mOnSelectProfileListener.onSelectProfile(profileInfo);
                        }
                    }
                });
            } else if (vh instanceof AddProfilePresenter.ViewHolder) {
                vh.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnSelectProfileListener.onAddProfile();
                    }
                });
            }
        }

        @Override
        protected void onUnbind(ViewHolder viewHolder) {
            super.onUnbind(viewHolder);
            viewHolder.itemView.setOnClickListener(null);
        }
    }


}
