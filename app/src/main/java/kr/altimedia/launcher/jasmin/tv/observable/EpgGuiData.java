/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.observable;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.ParentalController;
import kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel;
import kr.altimedia.launcher.jasmin.ui.app.AppConfig;

import static kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel.FLAG_HAS_PROGRAM;
import static kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel.FLAG_IS_CURRENT_PROGRAM;
import static kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel.FLAG_IS_WATCHING_CHANNEL;
import static kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel.FLAG_PIP_VISIBLE;

public class EpgGuiData extends BaseObservable {
    private Channel selectedChannel = null;
    private ParentalController.BlockType selectedChBlockType = null;
    private ParentalController.BlockType blockType = null;
    private int flags;

    private boolean hasFlag(int bit) { return EpgViewModel.hasFlag(flags, bit); }

    public void setValue(Channel selectedChannel, ParentalController.BlockType selectedChBlockType, ParentalController.BlockType blockType, int flags) {
        this.selectedChannel = selectedChannel;
        this.selectedChBlockType = selectedChBlockType;
        this.blockType = blockType;
        this.flags = flags;
    }

    public int getPressTextVisibility() {
        return (getTimeShiftButtonVisibility() == View.VISIBLE
                || getBlockButtonVisibility() == View.VISIBLE
                || getSetButtonVisibility() == View.VISIBLE)
                ? View.VISIBLE
                : View.GONE;
    }

    public int getTimeShiftButtonVisibility() {
        return (AppConfig.FEATURE_TIME_SHIFT_ENABLED
                && !blockType.isBlocked()
                && !blockType.isPreview()
                && hasFlag(FLAG_IS_WATCHING_CHANNEL)
                && selectedChannel != null
                && selectedChannel.getTimeshiftService() > 0
                && hasFlag(FLAG_HAS_PROGRAM))
                ? View.VISIBLE
                : View.GONE;
    }

    public int getSetButtonVisibility() {
        return hasFlag(FLAG_IS_WATCHING_CHANNEL)
                && !selectedChannel.isHiddenChannel()
                && !hasFlag(FLAG_PIP_VISIBLE)
                ? View.VISIBLE
                : View.GONE;
    }

    public int getBlockButtonVisibility() {
        if (hasFlag(FLAG_IS_CURRENT_PROGRAM) && (blockType.isBlocked() || blockType.isPreview())) {
            return View.VISIBLE;
        } else{
            return View.GONE;
        }
    }

    public String getBlockButtonText(Context context) {
        if (blockType == null) {
            return "";
        } else {
            int resId;
            switch (blockType) {
                case BLOCK_ADULT_CHANNEL:
                    resId = selectedChannel.isPaidChannel() ? R.string.channel_subscribe : R.string.channel_unlock;
                    break;
                case BLOCK_CHANNEL:
                case BLOCK_PROGRAM:
                    resId = R.string.channel_unlock;
                    break;
                case PREVIEW_CHANNEL:
                case BLOCK_UNSUBSCRIBED:
                    resId = R.string.channel_subscribe;
                    break;
                case CLEAN_CHANNEL:
                default:
                    resId = R.string.empty;
                    break;
            }
            return context.getString(resId);
        }
    }

    public boolean isNextProgramVisible() {
        if (selectedChannel != null && selectedChannel.isAudioChannel()) {
            return false;
        } else if (selectedChBlockType == ParentalController.BlockType.BLOCK_ADULT_CHANNEL
            || selectedChBlockType == ParentalController.BlockType.BLOCK_CHANNEL) {
            return false;
        } else {
            return true;
        }
    }

    @BindingAdapter("android:layout_width")
    public static void setLayoutWidth(View view, float width) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.width = (int) width;
        view.setLayoutParams(layoutParams);
    }
}
