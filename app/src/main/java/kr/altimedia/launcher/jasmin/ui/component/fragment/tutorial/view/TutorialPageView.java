/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.tutorial.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.pager.NotifyKeyViewPager;
import kr.altimedia.launcher.jasmin.ui.view.presenter.ViewPagerPresenter;

public class TutorialPageView extends LinearLayout implements ViewPagerPresenter.PageRootView {
    private NotifyKeyViewPager mViewPager;
    private LinearLayout mIndicatorView = null;

    public TutorialPageView(Context context) {
        this(context, null);
    }

    public TutorialPageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TutorialPageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.view_tutorial_page, this);
        mIndicatorView = view.findViewById(R.id.indicatorView);
        mViewPager = view.findViewById(R.id.viewPager);
    }

    @Override
    public View getParentsView() {
        return getRootView();
    }

    public NotifyKeyViewPager getPager() {
        return mViewPager;
    }

    public View getIndicator() {
        return mIndicatorView;
    }

    @Override
    public View getActionView() {
        return null;
    }

    @Override
    public View getBlackScreen() {
        return null;
    }
}
