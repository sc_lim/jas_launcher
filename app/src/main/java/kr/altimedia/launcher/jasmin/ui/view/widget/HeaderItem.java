/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.widget;

import static androidx.leanback.widget.ObjectAdapter.NO_ID;

public class HeaderItem<T> {

    private final long mId;
    private final T mName;
    private CharSequence mDescription;
    private CharSequence mContentDescription;

    /**
     * Create a header item.  All fields are optional.
     */
    public HeaderItem(long id, T name) {
        mId = id;
        mName = name;
    }

    /**
     * Create a header item.
     */
    public HeaderItem(T name) {
        this(NO_ID, name);
    }

    /**
     * Returns a unique identifier for this item.
     */
    public final long getId() {
        return mId;
    }

    /**
     * Returns the name of this header item.
     */
    public final T getName() {
        return mName;
    }

    /**
     * Returns optional content description for the HeaderItem.  When it is null, {@link #getName()}
     * should be used for the content description.
     * @return Content description for the HeaderItem.
     */
    public CharSequence getContentDescription() {
        return mContentDescription;
    }

    /**
     * Sets optional content description for the HeaderItem.
     * @param contentDescription Content description sets on the HeaderItem.
     */
    public void setContentDescription(CharSequence contentDescription) {
        mContentDescription = contentDescription;
    }

    /**
     * Sets the description for the current header item. This will be visible when
     * the row receives focus.
     */
    public void setDescription(CharSequence description) {
        this.mDescription = description;
    }

    /**
     * Returns the description for the current row.
     */
    public CharSequence getDescription() {
        return mDescription;
    }
}