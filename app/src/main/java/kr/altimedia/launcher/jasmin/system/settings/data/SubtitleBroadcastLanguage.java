/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.system.settings.data;

import android.content.Context;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.common.SpinnerTextView;

import static kr.altimedia.launcher.jasmin.system.settings.SettingControl.LANG_ENG;
import static kr.altimedia.launcher.jasmin.system.settings.SettingControl.LANG_THAI;

public enum SubtitleBroadcastLanguage implements SpinnerTextView.SpinnerOptionType {
    THAI(0, R.string.subtitle_thai, LANG_THAI), ENG(1, R.string.subtitle_eng, LANG_ENG);

    private int type;
    private int title;
    private String key;

    SubtitleBroadcastLanguage(int type, int title, String key) {
        this.type = type;
        this.title = title;
        this.key = key;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public int getTitle() {
        return title;
    }

    @Override
    public String getKey() {
        return key;
    }

    public static SubtitleBroadcastLanguage findByName(Context context, String title) {
        SubtitleBroadcastLanguage[] types = values();

        title = title.toLowerCase();
        for (SubtitleBroadcastLanguage language : types) {
            String option = context.getResources().getString(language.title).toLowerCase();
            if (option.startsWith(title)) {
                return language;
            }
        }

        return THAI;
    }
}
