/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog.profile;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PROFILE;

public class ProfileDeleteNoticeDialogFragment extends DialogFragment {
    public static final String CLASS_NAME = ProfileDeleteNoticeDialogFragment.class.getName();
    private final String TAG = ProfileDeleteNoticeDialogFragment.class.getSimpleName();

    private DialogInterface.OnDismissListener mOnDismissListener = null;

    public ProfileDeleteNoticeDialogFragment() {
    }

    public static ProfileDeleteNoticeDialogFragment newInstance() {
        Bundle args = new Bundle();
        ProfileDeleteNoticeDialogFragment fragment = new ProfileDeleteNoticeDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return keyCode == KeyEvent.KEYCODE_BACK
                        || keyCode == KeyEvent.KEYCODE_ESCAPE;
            }
        });
        dialog.setOnDismissListener(this);
        return dialog;
    }


    public void setOnDismissListener(@NonNull DialogInterface.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_profile_delete_notice, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initButton(view);
    }

    private void initButton(View view) {
        TextView confirmButton = view.findViewById(R.id.confirmButton);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(dialog);
            mOnDismissListener = null;
        }
    }

//    private void showProfileSelectDialog() {
//        ProfileSelectDialogFragment profileSelectDialogFragment = ProfileSelectDialogFragment.newInstance();
//        profileSelectDialogFragment.setOnSelectProfileListener(new ProfileSelectDialogFragment.OnSelectProfileListener() {
//            @Override
//            public void onSelectProfile(ProfileInfo profileInfo) {
//                profileSelectDialogFragment.dismiss();
//
//                notifyProfileUpdate(profileInfo.getProfileId());
//            }
//        });
//        profileSelectDialogFragment.show(getFragmentManager(), ProfileSelectDialogFragment.CLASS_NAME);
//    }

}
