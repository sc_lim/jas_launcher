/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular;

import android.content.Context;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.ChannelRing;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ChannelRingStorage;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildLaidOutListener;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.tv.interactor.PopularChannelInteractor;
import kr.altimedia.launcher.jasmin.tv.interactor.TvViewInteractor;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.adapter.PopularListBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.data.OnDataLoadedListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.data.PopularChannel;
import kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.data.PopularChannelBuilder;
import kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.presenter.PopularItemPresenter;
import kr.altimedia.launcher.jasmin.ui.view.guide.ProgramGuide;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;


public class PopularChannelFragment extends Fragment implements OnDataLoadedListener, PopularChannelInteractor.PopularChannelDataListener {
    public static final String CLASS_NAME = PopularChannelFragment.class.getName();
    private final String TAG = PopularChannelFragment.class.getSimpleName();

    private final int COLUMN_SIZE = 3;

    private SIAsyncTask asyncTask;
    private ProgressBarManager progressBarManager;
    private final ArrayList<PopularChannel> channelList = new ArrayList<>();

    private RelativeLayout emptyVodLayer;
    private RelativeLayout contentLayer;
    private VerticalGridView gridView;
    private PopularListBridgeAdapter gridBridgeAdapter;
    private int gridHorSpacing;
    private int gridVerSpacing;

    private TvViewInteractor mTvViewInteractor;
    private PopularChannelInteractor mPopularChannelInteractor;
    private ProgramDataManager mProgramDataManager;

    private boolean isFistUpdated = true;
    private boolean isReady = false;
    private boolean isGridFocused = false;
    private boolean isDataUpdated = false;
    private int currentFocus = 0;
    private int currentRank = 0;

    private final int MUTE_CELL = 0;

    private ProgramGuide mProgramGuide;
    private ProgramGuide.ProgramGuideInteractor mProgramGuideInteractor;

    private final int MSG_PROGRAM_UPDATE = 0;
    private final int MSG_CELL_AUDIO_TRACK = 1;
    private final Handler updateHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (Log.INCLUDE) {
                Log.d(TAG, "handleMessage: what=" + msg.what);
            }
            if (msg.what == MSG_PROGRAM_UPDATE) {
                retrieveCurrentPrograms();
                showView(0);
            } else if (msg.what == MSG_CELL_AUDIO_TRACK) {
                if (mPopularChannelInteractor != null) {
                    int rank = msg.arg1;
                    if (currentRank == rank) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "handleMessage: setAudioTrack=" + rank);
                        }
                        mPopularChannelInteractor.setAudioTrack(rank);
                    }
                }
            }
        }
    };

    public static PopularChannelFragment newInstance(ProgramGuide programGuide, ProgramGuide.ProgramGuideInteractor programGuideInteractor) {
        PopularChannelFragment fragment = new PopularChannelFragment();
        fragment.setOnProgramGuideInteractor(programGuide, programGuideInteractor);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Log.INCLUDE) {
            Log.d(TAG, "onDestroy");
        }
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
        if (Log.INCLUDE) {
            Log.d(TAG, "onDestroyView");
        }
        if (asyncTask != null) {
            asyncTask.cancel(true);
        }

        if (progressBarManager != null) {
            progressBarManager.hide();
            progressBarManager.setRootView(null);
        }
    }

    @Override
    public void onDetach() {

        super.onDetach();

        if (Log.INCLUDE) {
            Log.d(TAG, "onDetach");
        }
        mTvViewInteractor = null;
        if (mPopularChannelInteractor != null) {
            mPopularChannelInteractor.setPopularChannelDataUpdateListener(null);
            mPopularChannelInteractor = null;
        }
        if (updateHandler != null) {
            updateHandler.removeMessages(MSG_CELL_AUDIO_TRACK);
            updateHandler.removeMessages(MSG_PROGRAM_UPDATE);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach");
        }
        isFistUpdated = true;
        if (context instanceof TvViewInteractor) {
            mTvViewInteractor = (TvViewInteractor) context;
        }
        if (context instanceof LiveTvActivity) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onAttach: setPopularChannelDataUpdateListener()");
            }
            mPopularChannelInteractor = ((LiveTvActivity) context).getPopularChannelInteractor();
            mPopularChannelInteractor.setPopularChannelDataUpdateListener(this);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onPopularChannelDataUpdated(String jsonData) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPopularChannelDataUpdated");
        }

        showProgress();

        currentFocus = 0;
        isDataUpdated = true;
        if (isFistUpdated) {
            isFistUpdated = false;
            isDataUpdated = false;
            setCellAudio(null, true, false);
        }

        if (asyncTask != null) {
            asyncTask.cancel(true);
        }
        asyncTask = new SIAsyncTask(jsonData);
        asyncTask.execute();
    }

    private void setOnProgramGuideInteractor(ProgramGuide programGuide, ProgramGuide.ProgramGuideInteractor programGuideInteractor) {
        mProgramGuide = programGuide;
        mProgramGuideInteractor = programGuideInteractor;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_channel_guide_popular_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated");
        }

        isReady = true;
        isGridFocused = false;
        currentFocus = 0;
        emptyVodLayer = view.findViewById(R.id.emptyVodLayer);
        contentLayer = view.findViewById(R.id.contentLayer);
        contentLayer.setVisibility(View.GONE);

        initLoadingBar(view);
        initGridView(view);

        showView(0);
    }


    @Override
    public void onResume() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onResume");
        }
        super.onResume();
        if (!mProgramGuide.isActive()) {
            return;
        }
        tunePopularChannel();
    }


    @Override
    public void onStart() {
        super.onStart();
        if (Log.INCLUDE) {
            Log.d(TAG, "onStart");
        }

    }

    private void initLoadingBar(View view) {
        progressBarManager = new ProgressBarManager();
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);
    }

    private void initGridView(View view) {

        gridView = view.findViewById(R.id.gridView);
        gridView.setNumColumns(COLUMN_SIZE);
        gridView.setClipToPadding(false);

        gridHorSpacing = (int) view.getResources().getDimension(R.dimen.popular_channel_cell_horizontal_space);
        gridVerSpacing = (int) view.getResources().getDimension(R.dimen.popular_channel_cell_vertical_space);
        setGridSpace();
    }

    private void setGridSpace() {
        gridView.setHorizontalSpacing(gridHorSpacing);
        gridView.setVerticalSpacing(gridVerSpacing);
    }

    @Override
    public void hideProgress() {
        if (progressBarManager != null) {
            progressBarManager.hide();
        }
    }

    @Override
    public void showProgress() {
        if (progressBarManager != null) {
            progressBarManager.setInitialDelay(10);
            progressBarManager.show();
        }
    }

    @Override
    public void showView(int type) {
        if (!isReady) {
            return;
        }

        setGridView();
    }

    private void setGridView() {
        if (Log.INCLUDE) {
            Log.d(TAG, "setGridView");
        }
        if (channelList != null && channelList.size() > 0) {

            PopularItemPresenter presenter = new PopularItemPresenter(new PopularItemPresenter.OnKeyListener() {
                @Override
                public boolean onKey(int keyAction, int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
//                    if (Log.INCLUDE) {
//                        Log.d(TAG, "onKey: keyAction=" + keyAction + ", keyCode=" + keyCode + ", index=" + index);
//                    }
                    if (keyAction == KeyEvent.ACTION_UP) {
                        currentFocus = index;
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            if (mProgramGuide != null) {
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "onKey: back to parent by back");
                                }
                                isGridFocused = false;
//                                currentFocus = 0;
                                setCellAudio(null, true, false);
                                mProgramGuide.requestBackPressed(null);
                                return true;
                            }
                        } else {
                            if (item instanceof PopularChannel) {
                                boolean isBlocked = ((PopularItemPresenter.ChannelItemViewHolder) viewHolder).isBlocked();
                                setCellAudio((PopularChannel) item, isBlocked, true);
                            }
                            return true;
                        }
                    } else if (keyAction == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            return true;
                        }
                    }

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER: {
                            if (item instanceof PopularChannel) {
                                onChannelSelected((PopularChannel) item);
                                return true;
                            }
                            break;
                        }

                        case KeyEvent.KEYCODE_DPAD_UP: {
                            int condition = 3 - (index + 1);
                            if (condition >= 0 && condition <= 2) { // most top item
                                int nextItemIndex = index + 6;
                                setCellFocus(nextItemIndex);
                                return true;
                            }
                            break;
                        }

                        case KeyEvent.KEYCODE_DPAD_DOWN: {
                            int size = (channelList != null ? channelList.size() : 0);
                            int condition = size - (index + 1);
                            if (condition >= 0 && condition <= 2) { // most bottom item
                                int nextItemIndex = index - 6;
                                boolean focused = setCellFocus(nextItemIndex);
                                if (!focused) {
                                    setCellFocus(channelList.size() - 1);
                                }
                                return true;
                            }
                            break;
                        }

                        case KeyEvent.KEYCODE_DPAD_RIGHT: {
                            int size = (channelList != null ? channelList.size() : 0);
                            int condition = (index + 1) % 3;
                            if (condition == 0 || index == size - 1) { // most right item
                                int nextItemIndex = index + 1;
                                if (nextItemIndex < size) {
                                    boolean focused = setCellFocus(nextItemIndex);
                                    if (!focused) {
                                        setCellFocus(0);
                                    }
                                } else {
                                    setCellFocus(0);
                                }
                                return true;
                            }
                            break;
                        }

                        case KeyEvent.KEYCODE_DPAD_LEFT: {
                            int size = (channelList != null ? channelList.size() : 0);
                            int condition = index % 3;
                            if (condition == 0 && (index + 1) != size) { // most left item
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "onKey: back to parent by left");
                                }
                                isGridFocused = false;
                                currentFocus = 0;
                                setCellAudio(null, true, false);
                            }
                            break;
                        }
                    }

                    return false;
                }
            }, new PopularItemPresenter.OnFocusListener() {
                @Override
                public void onFocus(boolean hasFocus, int index, Object item, boolean isBlocked) {
                    if (hasFocus) {
                        isGridFocused = true;
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFocus: currentFocus=" + currentFocus + ", index=" + index + ", isBlocked=" + isBlocked);
                        }
                    }
                }
            });

            ArrayObjectAdapter listObjectAdapter = new ArrayObjectAdapter(presenter);
            listObjectAdapter.addAll(0, channelList);

            if (gridBridgeAdapter != null) {
                gridBridgeAdapter.clear();
            } else {
                gridBridgeAdapter = new PopularListBridgeAdapter(new PopularListBridgeAdapter.OnBindListener() {
                    @Override
                    public void onBind(int index, PopularChannel channel, Presenter.ViewHolder viewHolder) {
                        if (isGridFocused && isDataUpdated) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onBind: currentFocus=" + currentFocus + ", index=" + index);
                            }
                            if (currentFocus == index) {
                                isDataUpdated = false;
                                boolean isBlocked = ((PopularItemPresenter.ChannelItemViewHolder) viewHolder).isBlocked();
                                setCellAudio(channel, isBlocked, false);
                            }
                        }
                    }
                });
            }

            gridBridgeAdapter.setAdapter(listObjectAdapter);
            gridView.setAdapter(gridBridgeAdapter);

            emptyVodLayer.setVisibility(View.GONE);
            contentLayer.setVisibility(View.VISIBLE);

            gridView.setOnChildLaidOutListener(new OnChildLaidOutListener() {
                @Override
                public void onChildLaidOut(ViewGroup parent, View view, int position, long id) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onChildLaidOut: isGridFocused=" + isGridFocused + ", currentFocus=" + currentFocus + ", position=" + position);
                    }
                    if (isGridFocused && currentFocus == position) {
                        setCellFocus(currentFocus);
                    }
                }
            });
        } else {
            contentLayer.setVisibility(View.GONE);
            emptyVodLayer.setVisibility(View.VISIBLE);
        }

        hideProgress();
    }

    private boolean setCellFocus(int index) {
        if (gridView != null) {
            try {
                int visibleSize = gridView.getAdapter().getItemCount(); // size of visible children
                if (index > visibleSize) {
                    index = visibleSize - 1;
                }
                RelativeLayout view = (RelativeLayout) gridView.getChildAt(index);
                if (Log.INCLUDE) {
                    Log.d(TAG, "setCellFocus: index=" + index + ", view =" + view);
                }
                if (view != null) {
                    gridView.setSelectedPosition(index);
                    view.requestFocus();
                    return true;
                }
            } catch (Exception e) {
            }
        }
        return false;
    }

    private void setCellAudio(PopularChannel popularChannel, boolean isMute, boolean isAsync) {
        try {
            if (isMute) {
                currentRank = MUTE_CELL;
            } else {
                currentRank = popularChannel.getViewingRank();
            }

            if (isAsync) {
                Message message = updateHandler.obtainMessage();
                message.what = MSG_CELL_AUDIO_TRACK;
                message.arg1 = currentRank;
                if (updateHandler != null) {
                    updateHandler.removeMessages(MSG_CELL_AUDIO_TRACK);
                    updateHandler.sendMessageDelayed(message, 200);
                }
            } else {
                if (mPopularChannelInteractor != null) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "setCellAudio: setAudioTrack=" + currentRank);
                    }
                    mPopularChannelInteractor.setAudioTrack(currentRank);
                }
            }
        } catch (Exception e) {
        }
    }

    private void tunePopularChannel() {
        try {
            TvSingletons mTvSingletons = TvSingletons.getSingletons(getContext());
            ChannelDataManager channelDataManager = mTvSingletons.getChannelDataManager();
            ChannelRing channels =
                    channelDataManager.getChannelRingStorage().getChannelRing(ChannelRingStorage.RING_MOSAIC);
            if (channels != null) {
                for (int i = 0; i < channels.size(); i++) {
                    if (mProgramGuideInteractor != null) {
                        Rect rect = new Rect();
                        getActivity().getWindowManager().getDefaultDisplay().getRectSize(rect);
                        mProgramGuideInteractor.tuneChannel(channels.get(i), rect);
                    }
                    return;
                }
            }
        } catch (Exception e) {
        }
    }

    private void onChannelSelected(PopularChannel channel) {
        if (mProgramGuideInteractor != null && channel != null && channel.getChannel() != null) {
            mProgramGuideInteractor.hideForTune(channel.getChannel());
        }
    }

    private Program getCurrentProgram(long channelId) {
        if (mProgramDataManager == null) {
            mProgramDataManager = TvSingletons.getSingletons(getContext()).getProgramDataManager();
        }

        try {
            long currentTimeMillis = JasmineEpgApplication.SystemClock().currentTimeMillis();
            List<Program> programs = mProgramDataManager.getPrograms(channelId, currentTimeMillis);
            for (Program program : programs) {
                program.getStartTimeUtcMillis();
                long startTime = program.getStartTimeUtcMillis();
                long endTime = program.getEndTimeUtcMillis();
                if (startTime <= currentTimeMillis && currentTimeMillis <= endTime) {
                    return program;
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    private void retrieveCurrentPrograms() {
        try {
            Channel channel;
            Program program;
            long endTime = 0;
            for (PopularChannel popularChannel : channelList) {
                channel = popularChannel.getChannel();
                if (channel != null) {
                    program = getCurrentProgram(channel.getId());
                    if(program != null) {
                        if (endTime <= 0) {
                            endTime = program.getEndTimeUtcMillis();
                        } else {
                            if (endTime > program.getEndTimeUtcMillis()) {
                                endTime = program.getEndTimeUtcMillis();
                            }
                        }
                    }
                } else {
                    program = null;
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "retrieveCurrentPrograms: program=" + program);
                }
                popularChannel.setProgram(program);
            }

            long delayTime = endTime - JasmineEpgApplication.SystemClock().currentTimeMillis();
            startProgramUpdateTimer(delayTime);
        } catch (Exception e) {
        }
    }

    private void startProgramUpdateTimer(long delayTime) {
        if (delayTime <= 0) return;

        if (Log.INCLUDE) {
            Log.d(TAG, "startProgramUpdateTimer: delayTime=" + delayTime);
        }

        if (updateHandler != null) {
            updateHandler.removeMessages(MSG_PROGRAM_UPDATE);
            updateHandler.sendEmptyMessageDelayed(MSG_PROGRAM_UPDATE, delayTime);
        }
    }

    private void parseMpdData(String jsonData) {
        try {
            jsonData = jsonData.replaceAll("'", "");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "parseMpdData : " + jsonData);
        }

        if (!channelList.isEmpty()) {
            channelList.clear();
        }

        try {
            List<PopularChannel> list = new PopularChannelBuilder().getPopularChannelList(jsonData);
            channelList.addAll(list);
            Channel channel;
            for (PopularChannel popularChannel : channelList) {
                channel = mPopularChannelInteractor.getChannel(popularChannel.getServiceId());
                popularChannel.setChannel(channel);
            }
            Collections.sort(channelList, new Comparator<PopularChannel>() {
                @Override
                public int compare(PopularChannel channel1, PopularChannel channel2) {
                    if (channel1.getViewingRank() > channel2.getViewingRank()) {
                        return 1;
                    } else if (channel1.getViewingRank() < channel2.getViewingRank()) {
                        return -1;
                    }
                    return 0;
                }
            });

            retrieveCurrentPrograms();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class SIAsyncTask extends AsyncTask<Void, Integer, Boolean> {

        private final String jsonData;

        SIAsyncTask(String jsonData) {
            this.jsonData = jsonData;
        }

        @Override
        protected Boolean doInBackground(Void... strings) {
            if (Log.INCLUDE) {
                Log.d(TAG, "doInBackground");
            }
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
            if (isCancelled()) {
                return true;
            }
            parseMpdData(jsonData);
            return true;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean s) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onPostExecute");
            }
            super.onPostExecute(s);

            showView(0);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled(Boolean s) {
            super.onCancelled(s);
        }
    }

}
