/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.installedAppList;

import android.os.AsyncTask;

import org.json.JSONException;

import java.io.IOException;

import kr.altimedia.launcher.jasmin.dm.ResponseException;

public class AppAsyncTask extends AsyncTask<Void, Void, Object> {
    private final AppTaskCallback mAppTaskCallback;

    public AppAsyncTask(AppTaskCallback mAppTaskCallback) {
        this.mAppTaskCallback = mAppTaskCallback;
    }

    @Override
    protected Object doInBackground(Void... voids) {
        try {
            return mAppTaskCallback.onTaskBackGround();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ResponseException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isCancelled()) {
            return mAppTaskCallback.onCanceled();
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Object appInfos) {
        super.onPostExecute(appInfos);
        if (isCancelled()) {
            this.mAppTaskCallback.onCanceled();
        } else {
            this.mAppTaskCallback.onTaskPostExecute(appInfos);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        mAppTaskCallback.onCanceled();
    }

    public interface AppTaskCallback<K> {
        Object onTaskBackGround() throws IOException, ResponseException, JSONException;

        void onTaskPostExecute(K result);

        Object onCanceled();
    }

}
