/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.media;

import kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackBaseControlGlue;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackGlueHost;

public abstract class VideoPlayerAdapter {
    private boolean isKeyBlock = false;

    public static class Callback {
        public void setIsPlaying(boolean playing) {

        }

        public void onReady(boolean ready) {

        }

        public void onBlockedStateChanged(boolean blocked) {

        }

        public void onPlayStateChanged(VideoPlayerAdapter adapter, VideoState state) {
        }

        public void onPreparedStateChanged(VideoPlayerAdapter adapter) {
        }

        public void onPlayCompleted(VideoPlayerAdapter adapter) {
        }

        public void onCurrentPositionChanged(VideoPlayerAdapter adapter) {
        }

        public void onBufferedPositionChanged(VideoPlayerAdapter adapter) {
        }

        public void onProgressInfoChanged(VideoPlayerAdapter adapter, long startTimeMs, long endTimeMs) {

        }

        public void onDurationChanged(VideoPlayerAdapter adapter) {
        }

        public void onVideoSizeChanged(VideoPlayerAdapter adapter, int width, int height) {
        }

        public void onError(VideoPlayerAdapter adapter, int errorCode, String errorMessage) {
        }

        public void onBufferingStateChanged(VideoPlayerAdapter adapter, boolean start) {
        }

        public void onMetadataChanged(VideoPlayerAdapter adapter) {
        }
    }

    Callback mCallback;

    public final void setCallback(Callback callback) {
        mCallback = callback;
    }

    public final Callback getCallback() {
        return mCallback;
    }

    public abstract void resume();

    public abstract void play();

    public abstract void pause();

    public abstract void stop();

    public void next() {
    }

    public void previous() {
    }

    public void fastForward() {
    }

    public void rewind() {
    }

    public void seekTo(long positionInMs) {
    }

    public void setProgressUpdatingEnabled(boolean enable) {
    }

    public void setShuffleAction(int shuffleActionIndex) {
    }

    public void setRepeatAction(int repeatActionIndex) {
    }

    public boolean isSeeking() {
        return false;
    }

    public boolean isPlaying() {
        return false;
    }

    public boolean isPaused() {
        return false;
    }

    public boolean isKeyBlock() {
        return isKeyBlock;
    }

    public void setKeyBlock(boolean keyBlock) {
        isKeyBlock = keyBlock;
    }


    public long getDuration() {
        return 0;
    }

    public long getCurrentPosition() {
        return 0;
    }

    public long getBufferedPosition() {
        return 0;
    }

    private VideoPlaybackGlueHost mHost;

    public void onAttachedToHost(VideoPlaybackGlueHost host) {
        this.mHost = host;
    }

    public VideoPlaybackGlueHost getHost() {
        return mHost;
    }

    public boolean isPrepared() {
        return true;
    }

    public boolean isReady() {
        return true;
    }

    public boolean whenPlayWithControlBar() {
        return true;
    }

    public long getSupportedActions() {
        return VideoPlaybackBaseControlGlue.ACTION_PLAY_PAUSE;
    }

    public void onDetachedFromHost() {
    }
}
