/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.external.proxy.multiview.data;

import android.os.Parcel;
import android.os.Parcelable;

public class ChannelInfo implements Parcelable {
    public static final int LOCAL = 0;
    public static final int CENTRAL = 1;

    public long mChannelId;
    public String mServiceId;
    public String mDisplayNumber;
    public String mDisplayName;
    public boolean mIsLocked;
    public boolean mIsPaidChannel;
    public boolean mIsAudioChannel;
    public boolean mIsFavoriteChannel;
    public String[] mUrl;
    public String mGenre;

    public ChannelInfo(long channelId,
                       String serviceId,
                       String displayNumber,
                       String displayName,
                       boolean isLocked,
                       boolean isPaidChannel,
                       boolean isAudioChannel,
                       boolean isFavoriteChannel,
                       String[] url,
                       String genre) {
        mChannelId = channelId;
        mServiceId = serviceId;
        mDisplayNumber = displayNumber;
        mDisplayName = displayName;
        mIsLocked = isLocked;
        mIsPaidChannel = isPaidChannel;
        mIsAudioChannel = isAudioChannel;
        mIsFavoriteChannel = isFavoriteChannel;
        mUrl = url;
        mGenre = genre;
    }

    protected ChannelInfo(Parcel in) {
        mChannelId = in.readLong();
        mServiceId = in.readString();
        mDisplayNumber = in.readString();
        mDisplayName = in.readString();
        mIsLocked = in.readByte() != 0;
        mIsPaidChannel = in.readByte() != 0;
        mIsAudioChannel = in.readByte() != 0;
        mIsFavoriteChannel = in.readByte() != 0;
        mUrl = in.createStringArray();
        mGenre = in.readString();
    }

    public String getLocalUrl() {
        return mUrl[LOCAL];
    }

    public String getCentralUrl() {
        return mUrl[CENTRAL];
    }

    public static final Creator<ChannelInfo> CREATOR = new Creator<ChannelInfo>() {
        @Override
        public ChannelInfo createFromParcel(Parcel in) {
            return new ChannelInfo(in);
        }

        @Override
        public ChannelInfo[] newArray(int size) {
            return new ChannelInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mChannelId);
        dest.writeString(mServiceId);
        dest.writeString(mDisplayNumber);
        dest.writeString(mDisplayName);
        dest.writeByte((byte) (mIsLocked ? 1 : 0));
        dest.writeByte((byte) (mIsPaidChannel ? 1 : 0));
        dest.writeByte((byte) (mIsAudioChannel ? 1 : 0));
        dest.writeByte((byte) (mIsFavoriteChannel ? 1 : 0));
        dest.writeStringArray(mUrl);
        dest.writeString(mGenre);
    }

    @Override
    public String toString() {
        return "ChannelInfo{" +
                "mChannelId=" + mChannelId +
                ", mServiceId=" + mServiceId +
                ", mDisplayNumber='" + mDisplayNumber + '\'' +
                ", mDisplayName='" + mDisplayName + '\'' +
                ", mIsLocked=" + mIsLocked +
                ", mIsPaidChannel=" + mIsPaidChannel +
                ", mIsAudioChannel=" + mIsAudioChannel +
                ", mIsFavoriteChannel=" + mIsFavoriteChannel +
                ", mUrl='" + java.util.Arrays.toString(mUrl) + '\'' +
                ", mGenre='" + mGenre + '\'' +
                '}';
    }

}
