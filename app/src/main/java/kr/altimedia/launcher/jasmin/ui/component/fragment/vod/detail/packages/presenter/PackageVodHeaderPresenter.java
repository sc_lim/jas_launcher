/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.packages.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.leanback.widget.RowHeaderView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.presenter.BaseHeaderPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;

public class PackageVodHeaderPresenter extends BaseHeaderPresenter {
    private int focusedIndex = 0;
    private int packageSize = 0;

    public PackageVodHeaderPresenter(int packageSize, int focusedIndex) {
        this.focusedIndex = focusedIndex;
        this.packageSize = packageSize;
    }

    @Override
    protected BaseHeaderPresenter.ViewHolder createHeaderViewHolder(ViewGroup var1) {
        View view = LayoutInflater.from(var1.getContext()).inflate(R.layout.presenter_package_vod_header, var1, false);
        return new PackageVodHeaderPresenterViewHolder(view, focusedIndex, packageSize);
    }

    @Override
    protected void onBindHeaderViewHolder(ViewHolder vh, Object item) {
        ListRow listRow = (ListRow) item;
        PackageVodHeaderPresenterViewHolder viewHolder = (PackageVodHeaderPresenterViewHolder) vh;

        String title = (String) listRow.getHeaderItem().getName();
        viewHolder.setData(title);
    }

    public static class PackageVodHeaderPresenterViewHolder extends ViewHolder {
        private int focusedIndex = 0;
        private int packageSize = 0;

        private RowHeaderView header;
        private TextView current;
        private RelativeLayout countLayout;

        public PackageVodHeaderPresenterViewHolder(View view, int focusedIndex, int packageSize) {
            super(view);

            this.focusedIndex = focusedIndex;
            this.packageSize = packageSize;

            initView(view);
        }

        @Override
        public TextView getHeaderTextView() {
            return null;
        }

        @Override
        public int getHeaderViewHeight() {
            return (int) view.getResources().getDimension(R.dimen.package_header_height);
        }

        private void initView(View view) {
            header = view.findViewById(R.id.row_header1);
            current = view.findViewById(R.id.current);
            countLayout = view.findViewById(R.id.count_layout);
            TextView total = view.findViewById(R.id.total);

            total.setText(getCount(packageSize));
            setCountVisibility(false);
        }

        public void setData(String head) {
            header.setText(head);
        }

        public void setFocusedIndex(int index) {
            String focusedIndex = getCount(index + 1);
            current.setText(focusedIndex);
            setCountVisibility(true);
        }

        public void setCountVisibility(boolean isVisible) {
            int visibility = isVisible ? View.VISIBLE : View.INVISIBLE;
            if (visibility != countLayout.getVisibility()) {
                countLayout.setVisibility(visibility);
            }
        }

        private String getCount(int count) {
            return count < 10 ? "0" + count : String.valueOf(count);
        }
    }
}
