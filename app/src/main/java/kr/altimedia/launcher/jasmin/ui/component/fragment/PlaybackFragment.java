/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Observable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.ui.OSDFragment;
import kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel;
import kr.altimedia.launcher.jasmin.tv.viewModel.OSDViewModel;
import kr.altimedia.launcher.jasmin.ui.component.activity.BaseActivity;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;


public class PlaybackFragment extends Fragment {
    public static final String CLASS_NAME = PlaybackFragment.class.getName();
    private static final String TAG = PlaybackFragment.class
            .getSimpleName();
    private final long HIDE_TIME = 3 * 1000;
    private final String KEY_TAG = "TAG";

    private TvOverlayManager mTvOverlayManager;

    private final Handler mFragmentHandler = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String tag = bundle.getString(KEY_TAG);
            removeFragment(tag);
        }
    };

    public PlaybackFragment() {
    }

    public static PlaybackFragment newInstance() {
        PlaybackFragment fragment = new PlaybackFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void setTvOverlayManager(TvOverlayManager mTvOverlayManager) {
        this.mTvOverlayManager = mTvOverlayManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_playback, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addViewDataCallback();
    }

    private void addViewDataCallback() {
        if (getEpgViewModel() != null && getEpgViewModel().currentChannel != null) {
            getEpgViewModel().currentChannel.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
                @Override
                public void onPropertyChanged(Observable sender, int propertyId) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "EpgViewModel.onPropertyChanged : " + propertyId);
                    }
                    if (!isShowingMiniGuideDialog()) {
                        mTvOverlayManager.showMiniGuideFragment();
                    }
                }
            });
        }
        if (getDCAViewModel() != null && getDCAViewModel().currentTvData != null) {
            getDCAViewModel().currentTvData.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
                @Override
                public void onPropertyChanged(Observable sender, int propertyId) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "DCAViewModel.onPropertyChanged : " + propertyId);
                    }
                    if (!isShowingFragment(OSDFragment.CLASS_NAME)) {
                        initDCALayer();
                    } else {
                        refreshTimer(OSDFragment.CLASS_NAME);
                    }
                }
            });
        }

    }

    private void initDCALayer() {
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.dcaRect, OSDFragment.newInstance(), OSDFragment.CLASS_NAME);
        ft.commit();
        onFragmentVisibilityChanged(OSDFragment.CLASS_NAME, true);
    }

    private void removeFragment(String tag) {
        FragmentManager fm = getChildFragmentManager();
        if (fm != null) {
            FragmentTransaction ft = fm.beginTransaction();
            Fragment fragment = fm.findFragmentByTag(tag);
            if (fragment != null) {
                ft.remove(fragment);
                ft.commitAllowingStateLoss();
            }
        }
    }

    private boolean isShowingMiniGuideDialog() {
        return mTvOverlayManager.isMiniGuideVisible();
    }

    private boolean isShowingFragment(String tag) {
        FragmentManager fm = getChildFragmentManager();
        return fm.findFragmentByTag(tag) != null;
    }

    private EpgViewModel getEpgViewModel() {
        return ((BaseActivity) getActivity()).getEpgViewModel();
    }

    private OSDViewModel getDCAViewModel() {
        return ((BaseActivity) getActivity()).getOSDViewModel();
    }

    public void hideChannelOSD() {
        removeFragment(OSDFragment.CLASS_NAME);
    }

    public void hideAll() {
        mTvOverlayManager.hideMiniGuideFragment();
        hideChannelOSD();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mFragmentHandler.removeMessages(FragmentType.DCA.getEventKey());
    }

    private void refreshTimer(String tag) {
        if (Log.INCLUDE) {
            Log.d(TAG, "refreshTimer() tag:" + tag);
        }
        if (!mFragmentHandler.hasMessages(FragmentType.findByTag(tag).getEventKey())) {
            return;
        }
        mFragmentHandler.removeMessages(FragmentType.findByTag(tag).getEventKey());
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TAG, tag);
        Message message = Message.obtain(mFragmentHandler, FragmentType.findByTag(tag).getEventKey());
        message.setData(bundle);
        if (FragmentType.findByTag(tag).isAutoHide()) {
            mFragmentHandler.sendMessageDelayed(message, HIDE_TIME);
        }
    }

    public void onFragmentVisibilityChanged(String tag, boolean show) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onFragmentVisibilityChanged() tag:" + tag + ", show:" + show);
        }
        if (show) {
            Bundle bundle = new Bundle();
            bundle.putString(KEY_TAG, tag);
            Message message = Message.obtain(mFragmentHandler, FragmentType.findByTag(tag).getEventKey());
            message.setData(bundle);
            if (FragmentType.findByTag(tag).isAutoHide()) {
                mFragmentHandler.sendMessageDelayed(message, HIDE_TIME);
            }
        }
    }
}
