/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.payment.type.ContentType;

public class SubscriptionInfo implements Parcelable {
    public static final Creator<SubscriptionInfo> CREATOR = new Creator<SubscriptionInfo>() {
        @Override
        public SubscriptionInfo createFromParcel(Parcel in) {
            return new SubscriptionInfo(in);
        }

        @Override
        public SubscriptionInfo[] newArray(int size) {
            return new SubscriptionInfo[size];
        }
    };

    private ContentType contentType;
    private String title;
    private String description;
    private String termsAndConditions;
    private String posterSourceUrl;
    private List<SubscriptionPriceInfo> subscriptionPriceInfoList;

    public SubscriptionInfo(ContentType contentType, String title, String description, String termsAndConditions, String posterSourceUrl, List<SubscriptionPriceInfo> subscriptionPriceInfoList) {
        this.contentType = contentType;
        this.title = title;
        this.description = description;
        this.termsAndConditions = termsAndConditions;
        this.posterSourceUrl = posterSourceUrl;
        this.subscriptionPriceInfoList = subscriptionPriceInfoList;
    }

    protected SubscriptionInfo(Parcel in) {
        contentType = ContentType.findByName(in.readString());
        title = in.readString();
        description = in.readString();
        termsAndConditions = in.readString();
        posterSourceUrl = in.readString();
        subscriptionPriceInfoList = in.createTypedArrayList(SubscriptionPriceInfo.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contentType.getType());
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(termsAndConditions);
        dest.writeString(posterSourceUrl);
        dest.writeTypedList(subscriptionPriceInfoList);
    }

    @Override
    public String toString() {
        return "SubscriptionInfo{" +
                "contentType=" + contentType +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", termsAndCondition='" + termsAndConditions + '\'' +
                ", posterSourceUrl='" + posterSourceUrl + '\'' +
                ", subscriptionPriceInfoList=" + subscriptionPriceInfoList +
                '}';
    }

    public ContentType getContentType() {
        return contentType;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public String getPosterSourceUrl() {
        return posterSourceUrl;
    }

    public List<SubscriptionPriceInfo> getSubscriptionPriceInfoList() {
        return subscriptionPriceInfoList;
    }
}
