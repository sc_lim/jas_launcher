/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.remote;

import com.altimedia.util.Log;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.MbsUtil;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.contents.api.ContentApi;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PackageContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonListInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContentInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.BannerListResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.ContentDetailResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.ContentsListResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.OTUResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.PackageDetailResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.SeasonListResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.response.SeriesListResponse;
import kr.altimedia.launcher.jasmin.ui.util.task.VodTaskManager;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by mc.kim on 11,05,2020
 */
public class ContentRemote {


    private static final String TAG = ContentRemote.class.getSimpleName();
    private final ContentApi mContentApi;
    private final int RETRY_COUNT = 3;

    public ContentRemote(ContentApi contentApi) {
        mContentApi = contentApi;
    }

    public List<Content> getContentsList(String language, String said, String profileId,
                                         String categoryID, int startNo, int contentCnt, String contentBannerFlag) throws IOException, ResponseException, AuthenticationException {
        Call<ContentsListResponse> call = mContentApi.getContentsList(language, said, profileId, categoryID, startNo, contentCnt, contentBannerFlag);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ContentsListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ContentsListResponse mContentsListResponse = response.body();
        List<Content> contentsList = mContentsListResponse.getContentList();
        for (Content content : contentsList) {
            content.setCategoryId(categoryID);
        }
        return contentsList;
    }

    public ContentDetailInfo getContentDetail(String language,
                                              String said, String profileId, String contentId, String categoryId) throws IOException, ResponseException, AuthenticationException {
        return getContentDetail(language, said, profileId, contentId, categoryId, null);
    }

    public ContentDetailInfo getContentDetail(String language,
                                              String said, String profileId, String contentId, String categoryId, SeriesContent seriesContent) throws IOException, ResponseException, AuthenticationException {
        Call<ContentDetailResponse> call = mContentApi.getContentDetail(language, said, profileId, contentId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ContentDetailResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ContentDetailResponse mContentDetailResponse = response.body();
        ContentDetailInfo detailInfo = mContentDetailResponse.getDetailInfo();
        if (seriesContent != null) {
            seriesContent.setCategoryId(categoryId);
        }
        detailInfo.setSeriesContent(seriesContent);
        detailInfo.setCategoryId(categoryId);
        return detailInfo;
    }

    public PackageContent getPackageDetail(String language,
                                           String said, String packageId, String profileId, String categoryId) throws IOException, ResponseException, AuthenticationException {
        Call<PackageDetailResponse> call = mContentApi.getPackageDetail(language, said, packageId, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<PackageDetailResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        PackageDetailResponse mContentDetailResponse = response.body();
        if (mContentDetailResponse.getPackageContent() != null) {
            mContentDetailResponse.getPackageContent().setCategoryId(categoryId);
        }
        return mContentDetailResponse.getPackageContent();
    }


    public List<Banner> getBannerList(String language, String said, String bannerPositionID, String categoryId)
            throws IOException, ResponseException, AuthenticationException {
        Call<BannerListResponse> call = mContentApi.getBannerList(language, said, bannerPositionID);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BannerListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BannerListResponse mBannerListResponse = response.body();
        List<Banner> bannerList = mBannerListResponse.getBannerList();
        for (Banner banner : bannerList) {
            banner.setCategoryId(categoryId);
        }
        return bannerList;
    }

    public List<Banner> getImageBannerList(String language, String said, String bannerPositionID, String categoryId)
            throws IOException, ResponseException, AuthenticationException {
        Call<BannerListResponse> call = mContentApi.getBannerList(language, said, bannerPositionID);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BannerListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BannerListResponse mBannerListResponse = response.body();
        List<Banner> bannerList = mBannerListResponse.getBannerList();
        List<Banner> filteredList = new ArrayList<>();
        for (Banner banner : bannerList) {
            if (banner.getBannerType() == Banner.IMAGE_TYPE) {
                filteredList.add(banner);
            }
            banner.setCategoryId(categoryId);
        }
        return filteredList;
    }


    public SeriesContentInfo getSeriesList(String language, String said, String profileID, String seriesAssetId, String categoryId, String contentId)
            throws IOException, ResponseException, AuthenticationException {
        Call<SeriesListResponse> call = mContentApi.getSeriesList(language, said, profileID, seriesAssetId, contentId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<SeriesListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        SeriesListResponse mSeriesListResponse = response.body();
        SeriesContentInfo seriesContentInfo = mSeriesListResponse.getSeriesContInfo();
        if (seriesContentInfo != null) {
            List<SeriesContent> seriesContents = seriesContentInfo.getSeriesList();
            for (SeriesContent content : seriesContents) {
                content.setCategoryId(categoryId);
            }
        }
        return seriesContentInfo;
    }

    public SeasonListInfo getSeasonList(String language, String said, String profileId, String seriesAssetId, String categoryId)
            throws IOException, ResponseException, AuthenticationException {
        Call<SeasonListResponse> call = mContentApi.getSeasonList(language, said, profileId, seriesAssetId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<SeasonListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        SeasonListResponse mSeasonListResponse = response.body();
        return mSeasonListResponse.getSeasonListResult();
    }

    public List<Content> getRecentWatchingList(String language, String said, String profileId,
                                               int contentCnt, String categoryId) throws IOException, ResponseException, AuthenticationException {
        Call<ContentsListResponse> call = mContentApi.getRecentWatchingList(language, said, profileId, contentCnt);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<ContentsListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        ContentsListResponse mContentsListResponse = response.body();
        List<Content> contentList = mContentsListResponse.getContentList();
        for (Content content : contentList) {
            content.setCategoryId(categoryId);
        }
        return contentList;
    }

    public boolean postVodLike(String said, String profileId, String contentId, VodTaskManager.VodLikeType type)
            throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("said", said);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("contentId", contentId);
        jsonObject.addProperty("type", type.toString());
        Call<BaseResponse> call = mContentApi.postVodLike(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + jsonObject.toString());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);

        BaseResponse mBaseResponse = response.body();
        return mBaseResponse != null && mBaseResponse.getReturnCode().equalsIgnoreCase("s");
    }

    public boolean deleteVodLike(String said, String profileId, String contentId, VodTaskManager.VodLikeType type)
            throws IOException, ResponseException, AuthenticationException {
        Call<BaseResponse> call = mContentApi.deleteVodLike(said, profileId, contentId, type.toString());
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<BaseResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        BaseResponse mBaseResponse = response.body();
        return mBaseResponse.getReturnCode().equalsIgnoreCase("s");
    }


    public OTUResponse getOneTimeUrl(String said, String profileId,
                                     String payYN, String contentType, String contentPath
            , String contentId, String resumeYN, String categoryId, boolean isPreview) throws IOException, ResponseException, AuthenticationException {
        Call<OTUResponse> call = mContentApi.getOneTimeUrl(said, profileId, payYN,
                contentType, contentPath, contentId, resumeYN, categoryId, isPreview ? "Y" : "N");
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<OTUResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.body().toString());
        }
        MbsUtil.checkValid(response);
        OTUResponse mContentsListResponse = response.body();
        if (mContentsListResponse.getOTUList() != null) {
            List<String> urlList = new ArrayList<>(mContentsListResponse.getOTUList().size() * RETRY_COUNT);
            for (String url : mContentsListResponse.getOTUList()) {
                for (int i = 0; i < RETRY_COUNT; i++) {
                    urlList.add(url);
                }
            }
            mContentsListResponse.setOTUList(urlList);
        }
        return mContentsListResponse;
    }
}
