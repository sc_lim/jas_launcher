/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatSeekBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;

/**
 * Created by mc.kim on 04,06,2020
 */
public class ProgressVodItemPresenter extends RowPresenter {

    private static final DateFormat sDateFormat = new SimpleDateFormat("MMM dd, yyyy");

    private VodItemPresenter.LoadWorkPosterListener mLoadWorkPosterListener;

    private HashMap<String, Integer> flagMap = new HashMap<>();

    public ProgressVodItemPresenter() {
        super();


        flagMap.put(Content.FLAG_EVENT, R.id.flagEvent);
        flagMap.put(Content.FLAG_HOT, R.id.flagHot);
        flagMap.put(Content.FLAG_NEW, R.id.flagNew);
        flagMap.put(Content.FLAG_PREMIUM, R.id.flagPremium);
        flagMap.put(Content.FLAG_UHD, R.id.flagUhd);
    }

    public ProgressVodItemPresenter(int headerLayoutId) {
        super(headerLayoutId);
    }

    public void setLoadWorkPosterListener(VodItemPresenter.LoadWorkPosterListener loadMoviePosterListener) {
        mLoadWorkPosterListener = loadMoviePosterListener;
    }

    @Override
    protected void onRowViewSelected(ViewHolder vh, boolean selected) {
        super.onRowViewSelected(vh, selected);


    }


    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        VodItemViewHolder mVodItemViewHolder = new VodItemViewHolder(inflater.inflate(R.layout.item_progress_vod_contents, null, false));
        return mVodItemViewHolder;
    }


    @Override
    protected void onBindRowViewHolder(ViewHolder viewHolder, Object item) {
        super.onBindRowViewHolder(viewHolder, item);
        viewHolder.view.setAlpha(1f);
        Content work = (Content) item;
        VodItemViewHolder cardView = (VodItemViewHolder) viewHolder;
        cardView.setText(work);
        cardView.setResumeProgress(work);
        initFlag(work, viewHolder.view);
        Glide.with(viewHolder.view.getContext())
                .load(work.getPosterSourceUrl(Content.mPosterDefaultRect.width(),
                        Content.mPosterDefaultRect.height())).diskCacheStrategy(DiskCacheStrategy.DATA).placeholder(R.drawable.vod_list_default).
                error(R.drawable.vod_list_default)
                .centerCrop()
                .into(cardView.getMainImageView());

        if (mLoadWorkPosterListener != null) {
            mLoadWorkPosterListener.onLoadWorkPoster(work, cardView.getMainImageView());
        }
    }

    private void initFlag(Content work, View view) {
        if (work.getEventFlag() == null) {
            removeFlag(view);
            return;
        }
        Iterator<String> keyIterator = flagMap.keySet().iterator();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            boolean hasFlag = work.getEventFlag().contains(key);
            View flagView = view.findViewById(flagMap.get(key));
            flagView.setVisibility(hasFlag ? View.VISIBLE : View.GONE);
        }
        View lockIcon = view.findViewById(R.id.iconVodLock);
        if (lockIcon != null) {
            boolean isLocked = PlaybackUtil.isVodOverRating(work.getRating());
            lockIcon.setVisibility(isLocked ? View.VISIBLE : View.GONE);
        }
    }


    private void removeFlag(View view) {
        Iterator<String> keyIterator = flagMap.keySet().iterator();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            View flagView = view.findViewById(flagMap.get(key));
            flagView.setVisibility(View.GONE);
        }
        View lockIcon = view.findViewById(R.id.iconVodLock);
        if (lockIcon != null) {
            lockIcon.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onUnbindRowViewHolder(ViewHolder viewHolder) {
        super.onUnbindRowViewHolder(viewHolder);

        VodItemViewHolder cardView = (VodItemViewHolder) viewHolder;
        removeFlag(cardView.view);
        cardView.removeBadgeImage();
        cardView.removeMainImage();
    }


    public interface LoadWorkPosterListener {

        void onLoadWorkPoster(Content work, ImageView imageView);

    }


    private static class VodItemViewHolder extends ViewHolder {
        private final String TAG = VodItemViewHolder.class.getSimpleName();
        private ImageView mainImageView = null;
        private TextView titleView = null;
        private AppCompatSeekBar resumeProgress = null;

        public VodItemViewHolder(View view) {
            super(view);
            mainImageView = view.findViewById(R.id.mainImage);
            titleView = view.findViewById(R.id.titleLayer);
            resumeProgress = view.findViewById(R.id.resume_progress);
        }

        public ImageView getMainImageView() {
            return mainImageView;
        }

        public void setText(Content title) {
            titleView.setText(title.getTitle());
        }

        public void setResumeProgress(Content title) {
            int maxProgress = (int) title.getRunTime() / 1000;
            int progress = (int) title.getResumeTime() / 1000;
            resumeProgress.setMax(maxProgress);
            resumeProgress.setProgress(progress);
        }

        public TextView getTitleView() {
            return titleView;
        }

        public void removeBadgeImage() {

        }

        public void removeMainImage() {
            mainImageView.setImageDrawable(null);
        }

    }


}