package kr.altimedia.launcher.jasmin.ui.view.common;

import android.content.Context;
import android.text.Spannable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.ui.view.util.TextScrollingMovementMethod;

public class ScrollDescriptionTextButton extends FontSateTextView {
    private final TextScrollingMovementMethod textScrollingMovementMethod = new TextScrollingMovementMethod();
    private TextView descriptionTextView;
    private Spannable spannable;
    private OnButtonKeyListener mOnButtonKeyListener;
    private final View.OnKeyListener onKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != KeyEvent.ACTION_DOWN) {
                return false;
            }

            if (descriptionTextView == null || spannable == null) {
                return false;
            }

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    textScrollingMovementMethod.scrollUp(descriptionTextView, spannable);
                    return true;
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    textScrollingMovementMethod.scrollDown(descriptionTextView, spannable);
                    return true;
            }

            if (mOnButtonKeyListener != null) {
                return mOnButtonKeyListener.onKey(v, keyCode, event);
            }

            return false;
        }
    };

    public ScrollDescriptionTextButton(Context context) {
        super(context);
    }

    public ScrollDescriptionTextButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ScrollDescriptionTextButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setDescriptionTextView(TextView descriptionTextView) {
        this.descriptionTextView = descriptionTextView;
        spannable = Spannable.Factory.getInstance().newSpannable(descriptionTextView.getText());
        setOnKeyListener(onKeyListener);
    }

    public void setOnButtonKeyListener(OnButtonKeyListener mOnButtonKeyListener) {
        this.mOnButtonKeyListener = mOnButtonKeyListener;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mOnButtonKeyListener = null;
        setOnKeyListener(null);
    }

    public interface OnButtonKeyListener {
        boolean onKey(View v, int keyCode, KeyEvent event);
    }
}
