package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;

public class SystemInfoPresenter extends Presenter {
    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_system_info, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        if (item == null) {
            viewHolder.view.setVisibility(View.INVISIBLE);
            return;
        }

        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((SystemInfo) item);
        viewHolder.view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    private static class ViewHolder extends Presenter.ViewHolder {
        private TextView title;
        private TextView info;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            title = view.findViewById(R.id.title);
            info = view.findViewById(R.id.info);
        }

        public void setData(SystemInfo mSystemInfo) {
            String systemTitle = view.getContext().getString(mSystemInfo.getTitle());
            title.setText(systemTitle);
            info.setText(mSystemInfo.getInfo());
        }
    }

    public static class SystemInfo {
        private int title;
        private String info;

        public SystemInfo(int title, String info) {
            this.title = title;
            this.info = info;
        }

        public int getTitle() {
            return title;
        }

        public String getInfo() {
            return info;
        }
    }
}
