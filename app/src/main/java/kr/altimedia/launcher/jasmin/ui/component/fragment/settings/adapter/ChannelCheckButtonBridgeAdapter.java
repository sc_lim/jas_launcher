/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter;

import android.view.KeyEvent;

import com.altimedia.util.Log;

import androidx.leanback.widget.ObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.ChannelCheckboxPresenter;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;

public class ChannelCheckButtonBridgeAdapter extends PageBridgeAdapter {
    private final String TAG = ChannelCheckButtonBridgeAdapter.class.getSimpleName();

    public static final int NUMBER_COLUMNS = 3;
    public static final int NUMBER_ROWS = 5;

    private PagingVerticalGridView verticalGridView;

    public ChannelCheckButtonBridgeAdapter(ObjectAdapter adapter, PagingVerticalGridView verticalGridView) {
        super(adapter, NUMBER_ROWS, NUMBER_COLUMNS);

        this.verticalGridView = verticalGridView;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(int keyCode, boolean isConsumed, ViewHolder viewHolder) {
                ChannelCheckboxPresenter.ChannelCheckboxViewHolder vh = (ChannelCheckboxPresenter.ChannelCheckboxViewHolder) viewHolder.getViewHolder();
                int index = (int) vh.view.getTag(R.id.KEY_INDEX);

                if (Log.INCLUDE) {
                    Log.d(TAG, "keyCode : " + keyCode + ", index : " + index);
                }

                switch (keyCode) {
                    case KeyEvent.KEYCODE_ENTER:
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        if (isConsumed) {
                            boolean isBlocked = !vh.checkBox.isChecked();
                            vh.checkBox.setChecked(isBlocked);
                        }

                        return false;
                    case KeyEvent.KEYCODE_DPAD_DOWN:
                        int size = getOriginalSize();
                        int moveIndex = index + NUMBER_COLUMNS;
                        int lastRow = verticalGridView.getRowIndex(size - 1);
                        int row = verticalGridView.getRowIndex(moveIndex);

                        if (row > lastRow) {
                            requestFocus(0);
                            return true;
                        } else if (moveIndex >= size) {
                            requestFocus(size - 1);
                            return true;
                        }

                        return false;
                }

                return false;
            }
        });

        super.onBind(viewHolder);
    }

    private void requestFocus(int index) {
        verticalGridView.scrollToPosition(index);
    }
}
