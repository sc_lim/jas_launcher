/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class ProfileInfo implements Parcelable {

    public static final int[] ICON_RSC_IDs = new int[]{
            R.drawable.profile_pic_01,
            R.drawable.profile_pic_02,
            R.drawable.profile_pic_03,
            R.drawable.profile_pic_04,
            R.drawable.profile_pic_05,
            R.drawable.profile_pic_06,
            R.drawable.profile_pic_07,
            R.drawable.profile_pic_08,
            R.drawable.profile_pic_09,
            R.drawable.profile_pic_10
    };

    public static final String[] ICON_FILE_NAMEs = new String[]{
            "profile_pic_01",
            "profile_pic_02",
            "profile_pic_03",
            "profile_pic_04",
            "profile_pic_05",
            "profile_pic_06",
            "profile_pic_07",
            "profile_pic_08",
            "profile_pic_09",
            "profile_pic_10"
    };

    public static final Creator<ProfileInfo> CREATOR = new Creator<ProfileInfo>() {
        @Override
        public ProfileInfo createFromParcel(Parcel in) {
            return new ProfileInfo(in);
        }

        @Override
        public ProfileInfo[] newArray(int size) {
            return new ProfileInfo[size];
        }
    };

    @Expose
    @SerializedName("profileId")
    private String profileId;
    @Expose
    @SerializedName("profileName")
    private String profileName;
    @Expose
    @SerializedName("profilePic")
    private String profilePic;
    @Expose
    @SerializedName("profileIconId")
    private String profileIconId;
    @Expose
    @SerializedName("profileIconPath")
    private String profileIconPath;
    @Expose
    @SerializedName("lockYN")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isLock;
    @Expose
    @SerializedName("viewRestriction")
    private int rating;
    @Expose
    @SerializedName("lastLoginYN")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean lastLoginYN; // available in login only

    protected ProfileInfo(Parcel in) {
        profileId = in.readString();
        profileName = in.readString();
        profilePic = in.readString();
        profileIconId = in.readString();
        profileIconPath = in.readString();
        isLock = in.readByte() != 0;
        rating = in.readInt();
        lastLoginYN = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(profileId);
        dest.writeString(profileName);
        dest.writeString(profilePic);
        dest.writeString(profileIconId);
        dest.writeString(profileIconPath);
        dest.writeValue((byte) (isLock ? 1 : 0));
        dest.writeInt(rating);
        dest.writeValue((byte) (lastLoginYN ? 1 : 0));
    }

    public String getProfileId() {
        return profileId;
    }

    public String getProfileName() {
        return profileName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getProfileIconId() {
        return profileIconId;
    }

    public String getProfileIconPath() {
        return profileIconPath;
    }

    public int getRating() {
        return rating;
    }
    public boolean isLock() {
        return isLock;
    }

    public boolean isLastLoginYN() {
        return lastLoginYN;
    }

    public Drawable getProfileIcon(Context context) {
        if (profilePic != null) {
            for (int i = 0; i < ICON_FILE_NAMEs.length; i++) {
                if (ICON_FILE_NAMEs[i].equalsIgnoreCase(profilePic)) {
                    return context.getResources().getDrawable(ICON_RSC_IDs[i], null);
                }
            }
        }
        return context.getResources().getDrawable(R.drawable.profile_default, null);
    }

    public void setProfileName(String name) {
        profileName = name;
    }

    public void setProfileIcon(String id, String path) {
        profileIconId = id;
        profileIconPath = path;
    }

    public void setProfileIcon(ImageView imageView) {
        if(imageView != null) {
            imageView.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        try {
                            Drawable defaultIcon = getProfileIcon(imageView.getContext());
                            Glide.with(imageView.getContext())
                                    .load(getProfileIconPath()).placeholder(defaultIcon).
                                    error(defaultIcon).diskCacheStrategy(DiskCacheStrategy.DATA)
                                    .centerCrop()
                                    .into(imageView);
                        } catch (Exception e) {
                        }
                    } catch (Exception e) {
                    }
                }
            });
        }
    }

    public void setLock(boolean lock) {
        isLock = lock;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setLastLoginYN(boolean value) {
        lastLoginYN = value;
    }

    @NonNull
    @Override
    public String toString() {
        return "ProfileInfo{" +
                "profileId=" + profileId +
                ", profileName=" + profileName +
                ", profilePic=" + profilePic +
                ", profileIconId=" + profileIconId +
                ", profileIconPath=" + profileIconPath +
                ", isLock=" + isLock +
                ", rating=" + rating +
                ", lastLoginYN=" + lastLoginYN +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfileInfo info = (ProfileInfo) o;

        return profileId != null ? profileId.equals(info.profileId) : info.profileId == null;
    }

    @Override
    public int hashCode() {
        return profileId != null ? profileId.hashCode() : 0;
    }
}
