/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.system;

import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.LinkedList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.system.manager.reboot.AutoRebootManager;
import kr.altimedia.launcher.jasmin.system.ota.SystemUpdateManager;
import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

//import leakcanary.LeakCanary;

public class SettingSystemSettingFragment extends SettingBaseFragment {
    public static final String CLASS_NAME = SettingSystemSettingFragment.class.getName();
    private static final String TAG = SettingSystemSettingFragment.class.getSimpleName();

    public SettingSystemSettingFragment() {
    }

    public static SettingSystemSettingFragment newInstance() {
        SettingSystemSettingFragment fragment = new SettingSystemSettingFragment();
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_system_setting;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        SystemUpdateManager.getInstance().resetHiddenKeyIndex();

        TextView button = view.findViewById(R.id.button);
        button.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                if (Log.INCLUDE) {
                    Log.d(TAG, "onKey, keyCode : " + keyCode);
                }

                setAutoRebootEnable(v.getContext(), keyCode);
                SystemUpdateManager.getInstance().setSystemUpdateEnable(keyCode);

                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        Intent intent = new Intent("android.settings.SETTINGS");
                        intent.setPackage("com.android.tv.settings");
//                        Intent intent = LeakCanary.INSTANCE.newLeakDisplayActivityIntent();
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getContext().startActivity(intent);
                        return true;
                    case KeyEvent.KEYCODE_DPAD_UP:
                    case KeyEvent.KEYCODE_DPAD_DOWN:
                        return true;
                }

                return false;
            }
        });
    }

    private final AutoRebootManager mAutoRebootManager = AutoRebootManager.getInstance();
    private final int[] AUTO_REBOOT_HIDDEN_KEYS = new int[]{
            KeyEvent.KEYCODE_MEDIA_REWIND, KeyEvent.KEYCODE_MEDIA_REWIND, KeyEvent.KEYCODE_MEDIA_REWIND, KeyEvent.KEYCODE_MEDIA_REWIND, KeyEvent.KEYCODE_MEDIA_REWIND, KeyEvent.KEYCODE_2};
    private LinkedList<Integer> inputKeyList = new LinkedList<>();

    public void setAutoRebootEnable(Context context, int keyCode) {
        inputKeyList.add(keyCode);
        int inputKeySize = inputKeyList.size();

        if (inputKeySize < AUTO_REBOOT_HIDDEN_KEYS.length) {
            return;
        }

        if (inputKeySize > AUTO_REBOOT_HIDDEN_KEYS.length) {
            inputKeyList.poll();
        }

        for (int i = 0; i < AUTO_REBOOT_HIDDEN_KEYS.length; i++) {
            int inputKey = inputKeyList.get(i);
            if (inputKey != AUTO_REBOOT_HIDDEN_KEYS[i]) {
                return;
            }
        }

        UserPreferenceManagerImp up = new UserPreferenceManagerImp(context);
        boolean isAutoRebootEnable = up.isAutoRebootEnable();
        up.setAutoRebootEnable(!isAutoRebootEnable);

        if (isAutoRebootEnable) {
            mAutoRebootManager.resetRebootTimer(context);
            JasminToast.makeToast(context, R.string.sys_auto_reboot_disabled);
        } else {
            mAutoRebootManager.setRebootTimer(context);
            JasminToast.makeToast(context, R.string.sys_auto_reboot_enabled);
        }
    }
}
