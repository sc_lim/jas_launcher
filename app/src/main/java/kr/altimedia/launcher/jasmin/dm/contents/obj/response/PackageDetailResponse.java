/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PackageContent;

/**
 * Created by mc.kim on 20,05,2020
 */
public class PackageDetailResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private PackageContent result;

    protected PackageDetailResponse(Parcel in) {
        super(in);
        result = (PackageContent) in.readValue(PackageContent.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(result);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public PackageContent getPackageContent() {
        return result;
    }

    public static final Creator<PackageDetailResponse> CREATOR = new Creator<PackageDetailResponse>() {
        @Override
        public PackageDetailResponse createFromParcel(Parcel in) {
            return new PackageDetailResponse(in);
        }

        @Override
        public PackageDetailResponse[] newArray(int size) {
            return new PackageDetailResponse[size];
        }
    };
}
