/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.graphics.drawable.Drawable;

import androidx.leanback.app.DetailsFragment;
import androidx.leanback.media.PlaybackGlue;
import androidx.leanback.widget.DetailsParallax;
import androidx.leanback.widget.Parallax;
import androidx.leanback.widget.ParallaxEffect;
import androidx.leanback.widget.ParallaxTarget;

public class DetailsBackgroundVideoHelper {
    private static final long BACKGROUND_CROSS_FADE_DURATION = 500;
    // Temporarily add CROSSFADE_DELAY waiting for video surface ready.
    // We will remove this delay once PlaybackGlue have a callback for videoRenderingReady event.
    private static final long CROSSFADE_DELAY = 1000;

    /**
     * Different states {@link DetailsFragment} can be in.
     */
    static final int INITIAL = 0;
    static final int PLAY_VIDEO = 1;
    static final int NO_VIDEO = 2;

    private final DetailsParallax mDetailsParallax;
    private ParallaxEffect mParallaxEffect;

    private int mCurrentState = INITIAL;

    @SuppressWarnings("WeakerAccess") /* synthetic access */
            ValueAnimator mBackgroundAnimator;
    @SuppressWarnings("WeakerAccess") /* synthetic access */
            Drawable mBackgroundDrawable;
    private PlaybackGlue mPlaybackGlue;
    private boolean mBackgroundDrawableVisible;

    /**
     * Constructor to setup a Helper for controlling video playback in VodDetailsFragment.
     * @param playbackGlue The PlaybackGlue used to control underlying player.
     * @param detailsParallax The DetailsParallax to add special parallax effect to control video
     *                        start/stop. Video is played when
     *                        {@link DetailsParallax#getOverviewRowTop()} moved bellow top edge of
     *                        screen. Video is stopped when
     *                        {@link DetailsParallax#getOverviewRowTop()} reaches or scrolls above
     *                        top edge of screen.
     * @param backgroundDrawable The drawable will change alpha to 0 when video is ready to play.
     */
    public DetailsBackgroundVideoHelper(
            PlaybackGlue playbackGlue,
            DetailsParallax detailsParallax,
            Drawable backgroundDrawable) {
        this.mPlaybackGlue = playbackGlue;
        this.mDetailsParallax = detailsParallax;
        this.mBackgroundDrawable = backgroundDrawable;
        mBackgroundDrawableVisible = true;
        mBackgroundDrawable.setAlpha(255);
        startParallax();
    }

    void startParallax() {
        if (mParallaxEffect != null) {
            return;
        }
        Parallax.IntProperty frameTop = mDetailsParallax.getOverviewRowTop();
        final float maxFrameTop = 1f;
        final float minFrameTop = 0f;
        mParallaxEffect = mDetailsParallax
                .addEffect(frameTop.atFraction(maxFrameTop), frameTop.atFraction(minFrameTop))
                .target(new ParallaxTarget() {
                    @Override
                    public void update(float fraction) {
                        if (fraction == maxFrameTop) {
                            updateState(NO_VIDEO);
                        } else {
                            updateState(PLAY_VIDEO);
                        }
                    }
                });
        // In case the VideoHelper is created after RecyclerView is created: perform initial
        // parallax effect.
        mDetailsParallax.updateValues();
    }

    public void stopParallax() {
        mDetailsParallax.removeEffect(mParallaxEffect);
    }

    public boolean isVideoVisible() {
        return mCurrentState == PLAY_VIDEO;
    }

    @SuppressWarnings("WeakerAccess") /* synthetic access */
    void updateState(int state) {
        if (state == mCurrentState) {
            return;
        }
        mCurrentState = state;
        applyState();
    }

    private void applyState() {
        switch (mCurrentState) {
            case PLAY_VIDEO:
                if (mPlaybackGlue != null) {
                    if (mPlaybackGlue.isPrepared()) {
                        internalStartPlayback();
                    } else {
                        mPlaybackGlue.addPlayerCallback(mControlStateCallback);
                    }
                } else {
                    crossFadeBackgroundToVideo(false);
                }
                break;
            case NO_VIDEO:
                crossFadeBackgroundToVideo(false);
                if (mPlaybackGlue != null) {
                    mPlaybackGlue.removePlayerCallback(mControlStateCallback);
                    mPlaybackGlue.pause();
                }
                break;
        }
    }

    public void setPlaybackGlue(PlaybackGlue playbackGlue) {
        if (mPlaybackGlue != null) {
            mPlaybackGlue.removePlayerCallback(mControlStateCallback);
        }
        mPlaybackGlue = playbackGlue;
        applyState();
    }

    @SuppressWarnings("WeakerAccess") /* synthetic access */
    void internalStartPlayback() {
        if (mPlaybackGlue != null) {
            mPlaybackGlue.play();
        }
        mDetailsParallax.getRecyclerView().postDelayed(new Runnable() {
            @Override
            public void run() {
                crossFadeBackgroundToVideo(true);
            }
        }, CROSSFADE_DELAY);
    }

    void crossFadeBackgroundToVideo(boolean crossFadeToVideo) {
        crossFadeBackgroundToVideo(crossFadeToVideo, false);
    }

    public void crossFadeBackgroundToVideo(boolean crossFadeToVideo, boolean immediate) {
        final boolean newVisible = !crossFadeToVideo;
        if (mBackgroundDrawableVisible == newVisible) {
            if (immediate) {
                if (mBackgroundAnimator != null) {
                    mBackgroundAnimator.cancel();
                    mBackgroundAnimator = null;
                }
                if (mBackgroundDrawable != null) {
                    mBackgroundDrawable.setAlpha(crossFadeToVideo ? 0 : 255);
                    return;
                }
            }
            return;
        }
        mBackgroundDrawableVisible = newVisible;
        if (mBackgroundAnimator != null) {
            mBackgroundAnimator.cancel();
            mBackgroundAnimator = null;
        }

        float startAlpha = crossFadeToVideo ? 1f : 0f;
        float endAlpha = crossFadeToVideo ? 0f : 1f;

        if (mBackgroundDrawable == null) {
            return;
        }
        if (immediate) {
            mBackgroundDrawable.setAlpha(crossFadeToVideo ? 0 : 255);
            return;
        }
        mBackgroundAnimator = ValueAnimator.ofFloat(startAlpha, endAlpha);
        mBackgroundAnimator.setDuration(BACKGROUND_CROSS_FADE_DURATION);
        mBackgroundAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                mBackgroundDrawable.setAlpha(
                        (int) ((Float) (valueAnimator.getAnimatedValue()) * 255));
            }
        });

        mBackgroundAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mBackgroundAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });

        mBackgroundAnimator.start();
    }

    private class PlaybackControlStateCallback extends PlaybackGlue.PlayerCallback {
        PlaybackControlStateCallback() {
        }

        @Override
        public void onPreparedStateChanged(PlaybackGlue glue) {
            if (glue.isPrepared()) {
                internalStartPlayback();
            }
        }
    }

    DetailsBackgroundVideoHelper.PlaybackControlStateCallback mControlStateCallback = new DetailsBackgroundVideoHelper.PlaybackControlStateCallback();
}
