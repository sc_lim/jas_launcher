/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.system.settings.data;

import android.content.Context;

import kr.altimedia.launcher.jasmin.R;

public enum OnOffValue {
    SET(0, R.string.set), OFF(1, R.string.off);
    private int type;
    private int title;

    OnOffValue(int type, int title) {
        this.type = type;
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public int getTitle() {
        return title;
    }

    public static OnOffValue findByName(Context context, String title) {
        OnOffValue[] types = values();

        title = title.toLowerCase();
        for (OnOffValue onOff : types) {
            String option = context.getResources().getString(onOff.title).toLowerCase();
            if (option.equalsIgnoreCase(title)) {
                return onOff;
            }
        }

        return OFF;
    }
}
