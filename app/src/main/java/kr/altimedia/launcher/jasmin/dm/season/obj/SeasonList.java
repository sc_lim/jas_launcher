/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.season.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SeasonList implements Parcelable {
    @SerializedName("list")
    @Expose
    private List<Season> seasonList;

    protected SeasonList(Parcel in) {
        in.readList(seasonList, Season.class.getClassLoader());
    }

    public static final Creator<SeasonList> CREATOR = new Creator<SeasonList>() {
        @Override
        public SeasonList createFromParcel(Parcel in) {
            return new SeasonList(in);
        }

        @Override
        public SeasonList[] newArray(int size) {
            return new SeasonList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(seasonList);
    }

    public int getSize() {
        return seasonList != null ? seasonList.size() : 0;
    }

    public List<Season> getSeasonList() {
        return seasonList;
    }
}
