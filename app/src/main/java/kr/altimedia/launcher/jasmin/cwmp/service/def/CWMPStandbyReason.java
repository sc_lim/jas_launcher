/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service.def;

public class CWMPStandbyReason {
    // 대기모드로 전환된 사유
    public static final String NO_ACTION = "no action"; // 리모콘 3시간 입력 없을 경우
    public static final String REMOTE_POWER = "remote power"; // 리모콘 전원 키에 의한 경우
    public static final String AUTO_POWER = "auto power"; // 자동대기 모드에 의한 경우
    public static final String HDMI_CEC = "hdmi cec"; // CEC 명령에 의한 경우
    public static final String LOW_POWER = "low power"; // 리모컨 저전력 키에 의한 경우
    public static final String APP_INTENT = "app_intent"; // (app) stb_state(Android request)로 요청되는 경우
}
