
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.tvmodule.util.StringUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class SearchVod implements Parcelable {
    public static final Creator<SearchVod> CREATOR = new Creator<SearchVod>() {
        @Override
        public SearchVod createFromParcel(Parcel in) {
            return new SearchVod(in);
        }

        @Override
        public SearchVod[] newArray(int size) {
            return new SearchVod[size];
        }
    };
    @Expose
    @SerializedName("CONST_ID")
    private String constId;
    @Expose
    @SerializedName("TITLE")
    private String title;
    @Expose
    @SerializedName("CATEGORY_ID")
    private String categoryId;
    @Expose
    @SerializedName("CATEGORY_NAME")
    private String categoryName;
    @Expose
    @SerializedName("CATEGORY_PULLNAME")
    private String categoryPullname;
    @Expose
    @SerializedName("SERIES_YN")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean seriesYn;
    @Expose
    @SerializedName("spellSrch")
    private String spellsrch;
    @Expose
    @SerializedName("RATING")
    private String rating;
    @Expose
    @SerializedName("NEW_HOT")
    private String newHot;
    @Expose
    @SerializedName("IMG_URL")
    private String imgUrl;
    @Expose
    @SerializedName("IMG_EXT")
    private String imgExt;
    @Expose
    @SerializedName("CMB_YN")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean cmbYn;
    @Expose
    @SerializedName("CHARGE_YN")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean chargeYn;
    @Expose
    @SerializedName("ACTOR")
    private String actor;
    @Expose
    @SerializedName("DIRECTOR")
    private String director;
    @Expose
    @SerializedName("RUNTIME")
    private String runtime;
    @Expose
    @SerializedName("HD_SD")
    private String hdSd;

    protected SearchVod(Parcel in) {
        constId = in.readString();
        title = in.readString();
        categoryId = in.readString();
        categoryName = in.readString();
        categoryPullname = in.readString();
        seriesYn = in.readByte() != 0;
        spellsrch = in.readString();
        rating = in.readString();
        newHot = in.readString();
        imgUrl = in.readString();
        imgExt = in.readString();
        cmbYn = in.readByte() != 0;
        chargeYn = in.readByte() != 0;
        actor = in.readString();
        director = in.readString();
        runtime = in.readString();
        hdSd = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(constId);
        parcel.writeString(title);
        parcel.writeString(categoryId);
        parcel.writeString(categoryName);
        parcel.writeString(categoryPullname);
        parcel.writeValue((byte) (seriesYn ? 1 : 0));
        parcel.writeString(spellsrch);
        parcel.writeString(rating);
        parcel.writeString(newHot);
        parcel.writeString(imgUrl);
        parcel.writeString(imgExt);
        parcel.writeValue((byte) (cmbYn ? 1 : 0));
        parcel.writeValue((byte) (chargeYn ? 1 : 0));
        parcel.writeString(actor);
        parcel.writeString(director);
        parcel.writeString(runtime);
        parcel.writeString(hdSd);
    }

    public String getConstId() {
        return constId;
    }

    public String getTitle() {
        return title;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getCategoryPullname() {
        return categoryPullname;
    }

    public boolean isSeries() {
        return seriesYn;
    }

    public String getSpellsrch() {
        return spellsrch;
    }

    public String getRating() {
        return rating;
    }

    public String getNewHot() {
        return newHot;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public boolean isCmb() {
        return cmbYn;
    }

    public boolean isCharge() {
        return chargeYn;
    }

    public String getActor() {
        return actor;
    }

    public String getDirector() {
        return director;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getHdSd() {
        return hdSd;
    }

    public String getPosterSourceUrl(int width, int height) {
        if(imgUrl != null && !imgUrl.isEmpty()) {
            String posterType = StringUtils.nullToEmpty(imgExt).isEmpty() ? "" : imgExt;
            return imgUrl + "_" + width + "x" + height + "." + posterType;
        }
        return "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @NonNull
    @Override
    public String toString() {
        return "SearchVod{" +
                "constId=" + constId +
                ", title=" + title +
                ", categoryId=" + categoryId +
                ", categoryName=" + categoryName +
                ", categoryPullname=" + categoryPullname +
                ", director=" + director +
                ", imgUrl=" + imgUrl +
                ", imgExt=" + imgExt +
                ", seriesYn=" + seriesYn +
                ", spellsrch=" + spellsrch +
                ", rating=" + rating +
                ", newHot=" + newHot +
                ", cmbYn=" + cmbYn +
                ", chargeYn=" + chargeYn +
                ", actor=" + actor +
                ", runtime=" + runtime +
                ", hdSd=" + hdSd +
                "}";
    }
}