/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import androidx.annotation.NonNull;
import androidx.loader.content.CursorLoader;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.ui.util.PermissionUtils;
import com.altimedia.util.Log;

import static kr.altimedia.launcher.jasmin.ui.view.common.NotificationIcon.NOTIFICATION_REGISTER;
import static kr.altimedia.launcher.jasmin.ui.view.common.NotificationIcon.NOTIFICATION_REGISTER_ACTION;

public class NotificationAlertService extends NotificationListenerService {
    public final static String TAG = NotificationAlertService.class.getSimpleName();
    public final static String SERVICE_COUNTING_ACTION = "alticast.intent.ACTION_NOTIFICATION_COUNT";
    public final static String SERVICE_COUNTING = "count";

    private BroadcastReceiver mRegisterReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Log.INCLUDE) {
                Log.d(TAG, "Action : " + intent.getAction());
            }

            boolean isRegister = (boolean) intent.getExtras().get(NOTIFICATION_REGISTER);
            if (isRegister) {
                updateCount();
            } else {
                LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mRegisterReceiver);
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mRegisterReceiver, new IntentFilter(NOTIFICATION_REGISTER_ACTION));
        updateCount();
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
        updateCount();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        updateCount();
    }

    private void updateCount() {
        if (!PermissionUtils.hasAccessPermission(getApplicationContext(),
                "android.permission.ACCESS_NOTIFICATIONS")) {
            if (Log.INCLUDE) {
                Log.d(TAG, "have not permission so return");
            }
            return;
        }

        NotificationCountLoader mNotificationCountLoader =
                new NotificationCountLoader(getApplicationContext(), new NotificationCursorCallback() {

                    @Override
                    public void onResult(ArrayList<String> notifications) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "count : " + notifications.size());
                        }

                        Intent intent = new Intent(SERVICE_COUNTING_ACTION);
                        intent.putExtra(SERVICE_COUNTING, notifications);

                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    }

                    @Override
                    public void onFail() {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFail");
                        }
                    }
                });
        mNotificationCountLoader.startLoading();
    }

    private interface NotificationCursorCallback {
        void onResult(ArrayList<String> notifications);

        void onFail();
    }

    private static class NotificationCountLoader extends CursorLoader {
        static final String COLUMN_SBN_KEY = "sbn_key";
        private static final Uri NOTIF_COUNT_CONTENT_URI = Uri.parse(
                "content://com.android.tv.notifications.NotificationContentProvider/" + "notifications");

        private final NotificationCursorCallback mNotificationCursorCallback;

        public NotificationCountLoader(Context context, @NonNull NotificationCursorCallback callback) {
            super(context, NOTIF_COUNT_CONTENT_URI, new String[]{COLUMN_SBN_KEY}, null, null, null);
            this.mNotificationCursorCallback = callback;
        }

        @Override
        public void deliverResult(Cursor cursor) {
            super.deliverResult(cursor);

            ArrayList<String> notifications = new ArrayList();
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    String sbnKey = cursor.getString(0);
                    notifications.add(sbnKey);

                    if (Log.INCLUDE) {
                        Log.d(TAG, ", sbnKey : " + sbnKey);
                    }
                } while (cursor.moveToNext());
            }

            if (cursor != null) {
                cursor.close();
            }

            mNotificationCursorCallback.onResult(notifications);
        }


        @Override
        public void onCanceled(Cursor cursor) {
            super.onCanceled(cursor);
            mNotificationCursorCallback.onFail();
        }
    }
}
