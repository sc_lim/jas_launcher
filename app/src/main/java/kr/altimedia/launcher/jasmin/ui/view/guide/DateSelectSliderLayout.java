/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

/**
 * Created by mc.kim on 05,06,2020
 */
public class DateSelectSliderLayout extends LinearLayout {
    private final String TAG = DateSelectSliderLayout.class.getSimpleName();

    public DateSelectSliderLayout(Context context) {
        super(context);
        initView(context);
    }

    public DateSelectSliderLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public DateSelectSliderLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private VerticalGridView dateList;
    private DateListAdapter mDateListAdapter;

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.layout_date_slider, this, true);
        dateList = view.findViewById(R.id.dateList);
        dateList.setVerticalSpacing(context.getResources().getDimensionPixelSize(R.dimen.program_guide_table_date_item_vertical_space));
        dateList.setWindowAlignmentOffsetPercent(50);
//        dateList.setWindowAlignmentOffset(context.getResources().getDimensionPixelOffset(R.dimen.program_guide_side_panel_row_align));

    }

    public void initialDate(Context context, ProgramManager programManager, ProgramGuide guide) {
        mDateListAdapter = new DateListAdapter(context, programManager, guide, dateList);
        dateList.setAdapter(mDateListAdapter);


    }

    public void initializeSelectedTime(long time) {
        mDateListAdapter.setLastRequestedDate(time);
    }


    private OnDateSelectSliderInterface mOnDateSelectSliderInterface;

    public void setOnDateSelectSliderInterface(OnDateSelectSliderInterface onDateSelectSliderInterface) {
        this.mOnDateSelectSliderInterface = onDateSelectSliderInterface;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                if (this.mOnDateSelectSliderInterface != null) {
                    this.mOnDateSelectSliderInterface.closeMenu();
                    return true;
                }
            }
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    public interface OnDateSelectSliderInterface {
        void openMenu();

        void closeMenu();
    }
}
