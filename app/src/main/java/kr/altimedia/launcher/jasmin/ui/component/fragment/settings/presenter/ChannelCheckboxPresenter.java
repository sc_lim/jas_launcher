/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.favorite.ChannelItem;
import com.altimedia.tvmodule.dao.Channel;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class ChannelCheckboxPresenter extends Presenter {
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_channel_checkbox, parent, false);
        return new ChannelCheckboxViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (item == null) {
            viewHolder.view.setVisibility(View.INVISIBLE);
            return;
        }

        ChannelCheckboxViewHolder vh = (ChannelCheckboxViewHolder) viewHolder;
        vh.view.setVisibility(View.VISIBLE);

        vh.setItem((ChannelItem) item);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }

    public static class ChannelCheckboxViewHolder extends Presenter.ViewHolder {
        public CheckBox checkBox;
        public TextView num;
        public TextView name;
        public ImageView logo;

        public ChannelCheckboxViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            checkBox = view.findViewById(R.id.checkbox);
            num = view.findViewById(R.id.num);
            name = view.findViewById(R.id.name);
        }

        public void setItem(ChannelItem channelItem) {
            Channel channel = channelItem.getChannel();
            boolean isLocked = channelItem.isChecked();

            checkBox.setChecked(isLocked);
            num.setText(getFormattedNumber(channel.getDisplayNumber()));

            String logoUri = channel.getLogoUri();
            if (logoUri != null && !logoUri.isEmpty()) {
                Glide.with(view.getContext())
                        .load(logoUri).diskCacheStrategy(DiskCacheStrategy.DATA)
                        .centerCrop()
                        .into(logo);
                logo.setVisibility(View.VISIBLE);
            } else {
                name.setText(channel.getDisplayName());
                name.setVisibility(View.VISIBLE);
            }
        }

        private String getFormattedNumber(String number) {
            int num = Integer.parseInt(number);
            return getFormattedNumber(num);
        }

        private String getFormattedNumber(int number) {
            if (number < 10) {
                return "00" + number;
            } else if (number < 100) {
                return "0" + number;
            } else {
                return String.valueOf(number);
            }
        }
    }
}
