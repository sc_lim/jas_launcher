/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.def.NormalDateTimeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;

public class CollectedContent implements Parcelable {
    public static final Creator<CollectedContent> CREATOR = new Creator<CollectedContent>() {
        @Override
        public CollectedContent createFromParcel(Parcel in) {
            return new CollectedContent(in);
        }

        @Override
        public CollectedContent[] newArray(int size) {
            return new CollectedContent[size];
        }
    };
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("containerType")
    private String containerType;
    @Expose
    @SerializedName("posterFileName")
    private String posterFileName;
    @Expose
    @SerializedName("contentGroupId")
    private String contentGroupId;
    @Expose
    @SerializedName("offerId")
    private String offerId;
    @Expose
    @SerializedName("purchaseId")
    private String purchaseId;
    @Expose
    @SerializedName("purchaseDateTime")
    @JsonAdapter(NormalDateTimeDeserializer.class)
    private Date purchaseDateTime;
    @SerializedName("rating")
    @Expose
    private int rating;
    @Expose
    @SerializedName("hiddenYn")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isHidden;
    @Expose
    @SerializedName("isSeries")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isSeries;
    @Expose
    @SerializedName("seriesAssetId")
    private String seriesAssetId;
    @Expose
    @SerializedName("isPackage")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isPackage;
    @Expose
    @SerializedName("packageId")
    private String packageId;

    protected CollectedContent(Parcel in) {
        title = in.readString();
        posterFileName = in.readString();
        containerType = in.readString();
        contentGroupId = in.readString();
        offerId = in.readString();
        purchaseId = in.readString();
        purchaseDateTime = ((Date) in.readValue((Date.class.getClassLoader())));
        rating = ((int) in.readValue((Integer.class.getClassLoader())));
        isHidden = (boolean) in.readValue(Boolean.class.getClassLoader());
        isSeries = in.readByte() != 0;
        seriesAssetId = in.readString();
        isPackage = in.readByte() != 0;
        packageId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(posterFileName);
        dest.writeString(containerType);
        dest.writeString(contentGroupId);
        dest.writeString(offerId);
        dest.writeString(purchaseId);
        dest.writeValue(purchaseDateTime);
        dest.writeValue(rating);
        dest.writeValue(isHidden);
        dest.writeByte((byte) (isSeries ? 1 : 0));
        dest.writeString(seriesAssetId);
        dest.writeByte((byte) (isPackage ? 1 : 0));
        dest.writeString(packageId);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosterFileName() {
        return posterFileName;
    }

    public String getContainerType() {
        return containerType;
    }

    public String getContentGroupId() {
        return contentGroupId;
    }

    public String getOfferId() {
        return offerId;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public Date getPurchaseDate() {
        return purchaseDateTime;
    }

    public int getRating() {
        return rating;
    }

    public String getPosterSourceUrl(int width, int height) {
        if(containerType == null || containerType.isEmpty()){
            containerType = "";
        }
        return posterFileName + "_" + width + "x" + height + "." + containerType;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public boolean isSeries() {
        return isSeries;
    }

    public String getSeriesAssetId() {
        return seriesAssetId;
    }

    public boolean isPackage() {
        return isPackage;
    }

    public String getPackageId() {
        return packageId;
    }

    @NonNull
    @Override
    public String toString() {
        return "CollectedContent{" +
                "title=" + title +
                ", posterFileName=" + posterFileName +
                ", containerType=" + containerType +
                ", contentGroupId=" + contentGroupId +
                ", offerId=" + offerId +
                ", purchaseId=" + purchaseId +
                ", purchaseDateTime=" + purchaseDateTime +
                ", rating=" + rating +
                ", isHidden=" + isHidden +
                ", isSeries=" + isSeries +
                ", seriesAssetId='" + seriesAssetId + '\'' +
                ", isPackage=" + isPackage +
                ", packageId='" + packageId + '\'' +
                "}";
    }
}
