package kr.altimedia.launcher.jasmin.system.manager;

import android.os.SystemClock;

import kr.altimedia.launcher.jasmin.cwmp.service.CWMPSystemInfoManager;

public class SystemInfoManager {
    private final String TAG = SystemInfoManager.class.getSimpleName();

    private static final SystemInfoManager instance = new SystemInfoManager();
    private final String INVALID_DATA = "invalid data";
    private final CWMPSystemInfoManager mCWMPSystemInfoManager;

    public SystemInfoManager() {
        mCWMPSystemInfoManager = new CWMPSystemInfoManager(INVALID_DATA);
    }

    public static SystemInfoManager getInstance() {
        return instance;
    }

    public String getFirmwareVersion() {
        return mCWMPSystemInfoManager.getFirmwareVersion();
    }

    public String getMainSoftwareVersion() {
        return mCWMPSystemInfoManager.getMainSoftwareVersion();
    }

    public String getSerialNumber() {
        return mCWMPSystemInfoManager.getSerialNumber();
    }

    public String getModelName() {
        return mCWMPSystemInfoManager.getModelName();
    }

    public String getManufacturerName() {
        return mCWMPSystemInfoManager.getManufacturerName();
    }

    public String getManufacturerOUI() {
        return mCWMPSystemInfoManager.getManufacturerOUI();
    }

    public String getHwVersion() {
        return mCWMPSystemInfoManager.getHwVersion();
    }

    public long getUpTime() {
        long var1 = SystemClock.uptimeMillis() / 1000L;
        return var1;
    }

    public String getManufactureYearMonth() {
        return mCWMPSystemInfoManager.getManufactureYearMonth();
    }

    public String getSocName() {
        return mCWMPSystemInfoManager.getSocName();
    }

    public String getDecoderName() {
        return mCWMPSystemInfoManager.getDecoderName();
    }

    public String getCodecSupport() {
        return mCWMPSystemInfoManager.getCodecSupport();
    }

    public String getBootLoaderVersion() {
        return mCWMPSystemInfoManager.getBootLoaderVersion();
    }

}
