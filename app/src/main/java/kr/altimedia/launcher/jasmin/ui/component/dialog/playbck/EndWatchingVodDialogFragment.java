/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog.playbck;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.playbck.presenter.VodEndActionPresenter;
import kr.altimedia.launcher.jasmin.ui.component.dialog.playbck.presenter.VodEndDataProvider;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Created by mc.kim on 04,03,2020
 */
public class EndWatchingVodDialogFragment extends SafeDismissDialogFragment
        implements VodEndDataProvider {
    private final String TAG = EndWatchingVodDialogFragment.class.getSimpleName();
    public static final String CLASS_NAME = EndWatchingVodDialogFragment.class.getName();
    private static final String KEY_MOVIE = "movie";
    private static final String KEY_NEXT_MOVIE = "nextMovie";
    private static final String KEY_EOF = "isEOF";
    private VodEndActionPresenter mVodEndActionPresenter;
    private Content mContent = null;
    private Content mNextContent = null;
    private boolean mIsEof = false;
    private OnVodWatchEndListener mOnVodWatchEndListener;
    private OnDismissListener mOnDismissListener;

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public EndWatchingVodDialogFragment() {
        this.mVodEndActionPresenter = new VodEndActionPresenter(this);
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.mOnDismissListener = onDismissListener;
    }

    public void setOnVodWatchEndListener(OnVodWatchEndListener onVodWatchEndListener) {
        this.mOnVodWatchEndListener = onVodWatchEndListener;
    }


    public static EndWatchingVodDialogFragment newInstance(Content content, Content nextContents, boolean isEOF) {
        EndWatchingVodDialogFragment f = new EndWatchingVodDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_MOVIE, content);
        bundle.putParcelable(KEY_NEXT_MOVIE, nextContents);
        bundle.putBoolean(KEY_EOF, isEOF);
        f.setArguments(bundle);
        return f;
    }

    private boolean isWatchable(Content content) {
        List<Product> productList = content.getProductList();
        if (productList == null) {
            return false;
        }
        boolean watchable = false;
        for (Product product : productList) {
            if (product.isPurchased() || product.getPrice() == 0) {
                watchable = true;
                break;
            }
        }
        return watchable;
    }

    @Override
    public boolean isPurchasedNextMovie() {
        return isWatchable(mNextContent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(requireContext(), R.style.Theme_TV_dialog_Fullscreen);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK){

                    if(event.getAction() == KeyEvent.ACTION_UP){
                        onAction(VodEndActionPresenter.Action.NOT_NOW);
                    }
                    return true;
                }
                return false;
            }
        });
        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        Content content = bundle.getParcelable(KEY_MOVIE);
        boolean isEOF = bundle.getBoolean(KEY_EOF);
        mContent = content;
        mNextContent = bundle.getParcelable(KEY_NEXT_MOVIE);
        mIsEof = isEOF;
        return mVodEndActionPresenter.generateView(inflater, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mVodEndActionPresenter.onViewCreated(view);
    }

    public void notifyRemainTime(long timeMillis) {
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyCurrentTime : " + timeMillis);
        }
        if (timeMillis < 6 * TimeUtil.SEC) {
            mVodEndActionPresenter.notifyCountDown(timeMillis);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public Content getContents() {
        if (mNextContent != null) {
            return mNextContent;
        } else {
            return mContent;
        }
    }

    @Override
    public boolean hasSeries() {
        return mContent.isSeries() && !StringUtils.nullToEmpty(mContent.getNextSeriesAssetId()).isEmpty();
    }

    @Override
    public boolean isEOF() {
        return mIsEof;
    }

    public void onFinishAction(boolean isAutoPlayOn) {
        if (isAutoPlayOn) {
            if(isPurchasedNextMovie()){
                onAction(VodEndActionPresenter.Action.WATCH_NOW);
            }else{
                onAction(VodEndActionPresenter.Action.NEXT_EPISODE);
            }
        } else {
            onAction(VodEndActionPresenter.Action.NOT_NOW);
        }
    }

    @Override
    public void onAction(VodEndActionPresenter.Action action) {

        if (mOnVodWatchEndListener == null) {
            return;
        }
        switch (action) {
            case WATCH_NOW:
            case NEXT_EPISODE:
                mOnVodWatchEndListener.requestFinish(this, mNextContent, action.getType());
                break;
            case EXIT:
            case NOT_NOW:
                mOnVodWatchEndListener.requestFinish(this, mContent, action.getType());
                break;
            case CANCEL:
                mOnVodWatchEndListener.requestCancel(this, action.getType());
                break;
        }
        mOnVodWatchEndListener = null;
    }

    public void dismissWithCallback() {
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss();
        }
        dismiss();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        mVodEndActionPresenter = null;
        mOnVodWatchEndListener = null;
        mOnDismissListener = null;

    }

    @Override
    public void dismissAllowingStateLoss() {
        super.dismissAllowingStateLoss();
        mVodEndActionPresenter = null;
        mOnVodWatchEndListener = null;
        mOnDismissListener = null;
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public interface OnVodWatchEndListener {
        void requestFinish(DialogFragment dialogFragment, Content content, VodEndActionPresenter.FinishType type);

        void requestCancel(DialogFragment dialogFragment, VodEndActionPresenter.FinishType type);
    }

    public interface OnDismissListener {
        void onDismiss();
    }
}
