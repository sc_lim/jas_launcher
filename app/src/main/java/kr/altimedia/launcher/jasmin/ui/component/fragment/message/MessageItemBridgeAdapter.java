/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.message;

import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;

import kr.altimedia.launcher.jasmin.dm.message.obj.Message;
import kr.altimedia.launcher.jasmin.ui.component.fragment.message.presenter.MessagePresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class MessageItemBridgeAdapter extends ItemBridgeAdapter {
    private MessagePresenter.onKeyListener onKeyListener;

    public MessageItemBridgeAdapter(ObjectAdapter adapter, VerticalGridView verticalGridView, int visibleCount) {
        super(adapter);

        initBinListForPaging(adapter, visibleCount);
    }

    private void initBinListForPaging(ObjectAdapter adapter, int visibleCount) {
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) adapter;
        int size = objectAdapter.size();

        int mod = size % visibleCount;
        if (mod != 0) {
            int binSize = visibleCount - mod;
            for (int i = 0; i < binSize; i++) {
                objectAdapter.add(null);
            }
        }
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        MessagePresenter presenter = (MessagePresenter) viewHolder.getPresenter();
        onKeyListener = presenter.getOnKeyListener();

        if (onKeyListener == null) {
            return;
        }

        viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                Message message = (Message) viewHolder.mItem;
                return onKeyListener.onKey(keyCode, message);
            }
        });
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
    }

}
