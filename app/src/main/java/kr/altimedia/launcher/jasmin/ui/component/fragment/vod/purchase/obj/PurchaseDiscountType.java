/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter.DiscountCheckBoxPresenter;

public enum PurchaseDiscountType implements DiscountCheckBoxPresenter.DiscountButton {
    COUPON_DISCOUNT(0, PaymentType.DISCOUNT_COUPON.getCode(), R.string.discount_coupon, 0, R.string.ea, false),
    COUPON_POINT(1, PaymentType.CASH_COUPON.getCode(), R.string.cash_coupon, 0, R.string.thb, false),
    MEMBERSHIP(2, PaymentType.MEMBERSHIP.getCode(), R.string.ttbb_point, 0, R.string.p, false);

    private int type;
    private String code;
    private int title;
    private int value;
    private int unit;
    private boolean isAvailablePayment;
    private boolean isChecked;
    private boolean isEnabled;
    private int icon;
    private int ratio;
    private int limitRate;

    PurchaseDiscountType(int type, String code, int title, int value, int unit, boolean isChecked) {
        this.type = type;
        this.code = code;
        this.title = title;
        this.value = value;
        this.unit = unit;
        this.isChecked = isChecked;
        this.isEnabled = value > 0;
    }

    @Override
    public float getValue() {
        return value;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public void setAvailablePayment(boolean availablePayment) {
        isAvailablePayment = availablePayment;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public void setEnabled(boolean enabled) {
        if (enabled && ratio > 0) {
            int valueRatio = (int) (value / ratio);
            if (valueRatio > 0) {
                isEnabled = enabled;
            }
        } else {
            isEnabled = enabled;
        }
    }

    public int getType() {
        return type;
    }

    public String getCode() {
        return code;
    }

    @Override
    public int getTitle() {
        return title;
    }

    @Override
    public int getUnit() {
        return unit;
    }

    @Override
    public int getDescription() {
        return (this == MEMBERSHIP) ? R.string.thb : -1;
    }

    public void setValue(int value) {
        this.value = value;
        setEnabled(value > 0);
    }

    @Override
    public int getIcon() {
        return icon;
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public boolean isEnabled() {
        return (isAvailablePayment && isEnabled) || isChecked;
    }

    @Override
    public int getRatio() {
        return ratio;
    }

    public void setRatio(int ratio) {
        this.ratio = ratio;
    }

    public int getLimitRate() {
        return limitRate;
    }

    public void setLimitRate(int limitRate) {
        this.limitRate = limitRate;
    }

    public static PurchaseDiscountType findByCode(String code) {
        PurchaseDiscountType[] types = values();
        for (PurchaseDiscountType purchaseDiscountType : types) {
            if (purchaseDiscountType.code.equalsIgnoreCase(code)) {
                return purchaseDiscountType;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return "PurchaseDiscountType{" +
                "type=" + type +
                ", code='" + code + '\'' +
                ", title=" + title +
                ", value=" + value +
                ", unit=" + unit +
                ", isAvailablePayment=" + isAvailablePayment +
                ", isChecked=" + isChecked +
                ", isEnabled=" + isEnabled +
                ", icon=" + icon +
                ", ratio=" + ratio +
                ", limitRate=" + limitRate +
                '}';
    }
}