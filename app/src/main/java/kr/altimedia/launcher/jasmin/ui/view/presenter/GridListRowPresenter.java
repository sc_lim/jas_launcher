/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.presenter;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.HorizontalHoverCardSwitcher;
import androidx.leanback.widget.OnChildSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import androidx.leanback.widget.ShadowOverlayHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewKeyListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.BaseGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.rowView.AppGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.FocusHighlightHelper;
import kr.altimedia.launcher.jasmin.ui.view.util.TransitionHelper;

public class GridListRowPresenter extends GridRowPresenter {
    private static final String TAG = "GridListRowPresenter";
    private static final boolean DEBUG = false;
    private static final int DEFAULT_RECYCLED_POOL_SIZE = 24;
    private int mRowHeight;
    private int mExpandedRowHeight;
    private PresenterSelector mHoverCardPresenterSelector;
    private int mFocusZoomFactor;
    private boolean mUseFocusDimmer;
    private boolean mShadowEnabled;
    private int mBrowseRowsFadingEdgeLength;
    private boolean mRoundedCornersEnabled;
    private boolean mKeepChildForeground;
    private HashMap<Presenter, Integer> mRecycledPoolSize;
    //    ShadowOverlayHelper mShadowOverlayHelper;
//    private ItemBridgeAdapter.Wrapper mShadowOverlayWrapper;
    private static int sSelectedRowTopPadding;
    private static int sExpandedSelectedRowTopPadding;
    private static int sExpandedRowNoHovercardBottomPadding;

    private RecyclerView.ItemDecoration decoration;

    private BaseOnItemViewKeyListener baseOnItemViewKeyListener;

    public GridListRowPresenter() {
        this(5);
    }


    public GridListRowPresenter(int focusZoomFactor) {
        this(focusZoomFactor, false);
    }

    public GridListRowPresenter(int focusZoomFactor, boolean useFocusDimmer) {
        this.mShadowEnabled = true;
        this.mBrowseRowsFadingEdgeLength = -1;
        this.mRoundedCornersEnabled = true;
        this.mKeepChildForeground = true;
        this.mRecycledPoolSize = new HashMap();
        if (!FocusHighlightHelper.isValidZoomIndex(focusZoomFactor)) {
            throw new IllegalArgumentException("Unhandled zoom factor");
        } else {
            this.mFocusZoomFactor = focusZoomFactor;
            this.mUseFocusDimmer = useFocusDimmer;
        }
    }

    private int mSpanCount = 5;

    public void setSpanCount(int count) {
        mSpanCount = count;
    }

    public void setRowHeight(int rowHeight) {
        this.mRowHeight = rowHeight;
    }

    public int getRowHeight() {
        return this.mRowHeight;
    }

    public void setExpandedRowHeight(int rowHeight) {
        this.mExpandedRowHeight = rowHeight;
    }

    public int getExpandedRowHeight() {
        return this.mExpandedRowHeight != 0 ? this.mExpandedRowHeight : this.mRowHeight;
    }

    public final int getFocusZoomFactor() {
        return this.mFocusZoomFactor;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public final int getZoomFactor() {
        return this.mFocusZoomFactor;
    }

    public final boolean isFocusDimmerUsed() {
        return this.mUseFocusDimmer;
    }


    public void setBaseOnItemViewKeyListener(BaseOnItemViewKeyListener baseOnItemViewKeyListener) {
        this.baseOnItemViewKeyListener = baseOnItemViewKeyListener;
    }

    protected void initializeRowViewHolder(GridRowPresenter.ViewHolder holder) {
        super.initializeRowViewHolder(holder);
        final GridListRowPresenter.ViewHolder rowViewHolder = (GridListRowPresenter.ViewHolder) holder;
        Context context = holder.view.getContext();
//        if (this.mShadowOverlayHelper == null) {
//            this.mShadowOverlayHelper = (new ShadowOverlayHelper.Builder()).needsOverlay(this.needsDefaultListSelectEffect()).needsShadow(this.needsDefaultShadow()).needsRoundedCorner(this.isUsingOutlineClipping(context) && this.areChildRoundedCornersEnabled()).preferZOrder(this.isUsingZOrder(context)).keepForegroundDrawable(this.mKeepChildForeground).options(this.createShadowOverlayOptions()).build(context);
//            if (this.mShadowOverlayHelper.needsWrapper()) {
//                this.mShadowOverlayWrapper = new ItemBridgeAdapterShadowOverlayWrapper(this.mShadowOverlayHelper);
//            }
//        }
        rowViewHolder.mItemBridgeAdapter = new GridListRowPresenter.GridListRowPresenterItemBridgeAdapter(rowViewHolder);
//        rowViewHolder.mItemBridgeAdapter.setWrapper(this.mShadowOverlayWrapper);
//        this.mShadowOverlayHelper.prepareParentForShadow(rowViewHolder.mViewPager);
        FocusHighlightHelper.setupBrowseItemFocusHighlight(rowViewHolder.mItemBridgeAdapter, this.mFocusZoomFactor, this.mUseFocusDimmer);
//        rowViewHolder.mViewPager.setFocusDrawingOrderEnabled(this.mShadowOverlayHelper.getShadowType() != 3);
        rowViewHolder.mGridView.setOnChildSelectedListener(new OnChildSelectedListener() {
            public void onChildSelected(ViewGroup parent, View view, int position, long id) {
                GridListRowPresenter.this.selectChildView(rowViewHolder, view, true);
            }
        });
        rowViewHolder.mGridView.setOnUnhandledKeyListener(new BaseGridView.OnUnhandledKeyListener() {
            @Override
            public boolean onUnhandledKey(KeyEvent event) {
                return rowViewHolder.getOnKeyListener() != null && rowViewHolder.getOnKeyListener().onKey(rowViewHolder.view, event.getKeyCode(), event);
            }
        });
    }


    final boolean needsDefaultListSelectEffect() {
        return this.isUsingDefaultListSelectEffect() && this.getSelectEffectEnabled();
    }

    public void setRecycledPoolSize(Presenter presenter, int size) {
        this.mRecycledPoolSize.put(presenter, size);
    }

    public int getRecycledPoolSize(Presenter presenter) {
        return this.mRecycledPoolSize.containsKey(presenter) ? (Integer) this.mRecycledPoolSize.get(presenter) : 24;
    }

    public final void setHoverCardPresenterSelector(PresenterSelector selector) {
        this.mHoverCardPresenterSelector = selector;
    }

    public final PresenterSelector getHoverCardPresenterSelector() {
        return this.mHoverCardPresenterSelector;
    }

    void selectChildView(GridListRowPresenter.ViewHolder rowViewHolder, View view, boolean fireEvent) {
        if (view != null) {
            if (rowViewHolder.mSelected) {
                ItemBridgeAdapter.ViewHolder ibh = (ItemBridgeAdapter.ViewHolder) rowViewHolder.mGridView.getChildViewHolder(view);
                if (this.mHoverCardPresenterSelector != null) {
//                    rowViewHolder.mHoverCardViewSwitcher.select(rowViewHolder.mGridView, view, ibh.mItem);
                }

                if (fireEvent && rowViewHolder.getOnItemViewSelectedListener() != null) {
                    rowViewHolder.getOnItemViewSelectedListener().onItemSelected(ibh.mHolder, ibh.mItem, rowViewHolder, rowViewHolder.mRow);
                }
            }
        } else {
            if (this.mHoverCardPresenterSelector != null) {
                rowViewHolder.mHoverCardViewSwitcher.unselect();
            }

            if (fireEvent && rowViewHolder.getOnItemViewSelectedListener() != null) {
                rowViewHolder.getOnItemViewSelectedListener().onItemSelected((Presenter.ViewHolder) null, (Object) null, rowViewHolder, rowViewHolder.mRow);
            }
        }

    }

    private static void initStatics(Context context) {
        if (sSelectedRowTopPadding == 0) {
//            sSelectedRowTopPadding = context.getResources().getDimensionPixelSize(R.dimen.cb_browse_selected_row_top_padding);
//            sExpandedSelectedRowTopPadding = context.getResources().getDimensionPixelSize(R.dimen.cb_browse_expanded_selected_row_top_padding);
//            sExpandedRowNoHovercardBottomPadding = context.getResources().getDimensionPixelSize(R.dimen.cb_browse_expanded_row_no_hovercard_bottom_padding);
        }
    }

    private int getSpaceUnderBaseline(GridListRowPresenter.ViewHolder vh) {
        BaseHeaderPresenter.ViewHolder headerViewHolder = vh.getHeaderViewHolder();
        if (headerViewHolder != null) {
            return this.getHeaderPresenter() != null ? this.getHeaderPresenter().getSpaceUnderBaseline(headerViewHolder) : headerViewHolder.view.getPaddingBottom();
        } else {
            return 0;
        }
    }

    private void setVerticalPadding(GridListRowPresenter.ViewHolder vh) {
        int paddingTop;
        int paddingBottom;


        if (vh.isExpanded()) {
            int headerSpaceUnderBaseline = this.getSpaceUnderBaseline(vh);
            paddingTop = (vh.isSelected() ? sExpandedSelectedRowTopPadding : vh.mPaddingTop) - headerSpaceUnderBaseline;
            paddingBottom = this.mHoverCardPresenterSelector == null ? sExpandedRowNoHovercardBottomPadding : vh.mPaddingBottom;
        } else if (vh.isSelected()) {
            paddingTop = sSelectedRowTopPadding - vh.mPaddingBottom;
            paddingBottom = sSelectedRowTopPadding;
        } else {
            paddingTop = 0;
            paddingBottom = vh.mPaddingBottom;
        }

        vh.getGridView().setPadding(vh.mPaddingLeft, paddingTop, vh.mPaddingRight, paddingBottom);
    }

    protected GridRowPresenter.ViewHolder createRowViewHolder(ViewGroup parent) {
        initStatics(parent.getContext());
        AppGridView rowView = new AppGridView(parent.getContext());
        this.setupFadingEffect(rowView);
        if (this.mRowHeight != 0) {
            rowView.setRowHeight(this.mRowHeight);
        }

        if (decoration != null) {
            rowView.getGridView().addItemDecoration(decoration);
        }


        return new GridListRowPresenter.ViewHolder(rowView, rowView.getGridView(), this);
    }

    public void addItemDecoration(RecyclerView.ItemDecoration decor) {
        this.decoration = decor;
    }

    protected void dispatchItemSelectedListener(GridRowPresenter.ViewHolder holder, boolean selected) {
        GridListRowPresenter.ViewHolder vh = (GridListRowPresenter.ViewHolder) holder;
        ItemBridgeAdapter.ViewHolder itemViewHolder = (ItemBridgeAdapter.ViewHolder) vh.mGridView.findViewHolderForPosition(vh.mGridView.getSelectedPosition());
        if (itemViewHolder == null) {
            super.dispatchItemSelectedListener(holder, selected);
        } else {
            if (selected && holder.getOnItemViewSelectedListener() != null) {
                holder.getOnItemViewSelectedListener().onItemSelected(itemViewHolder.getViewHolder(), itemViewHolder.mItem, vh, vh.getRow());
            }

        }
    }

    protected void onRowViewSelected(GridRowPresenter.ViewHolder holder, boolean selected) {
        super.onRowViewSelected(holder, selected);
        GridListRowPresenter.ViewHolder vh = (GridListRowPresenter.ViewHolder) holder;
        this.setVerticalPadding(vh);
        this.updateFooterViewSwitcher(vh);
    }

    private void updateFooterViewSwitcher(GridListRowPresenter.ViewHolder vh) {
        if (vh.mExpanded && vh.mSelected) {
            if (this.mHoverCardPresenterSelector != null) {
                vh.mHoverCardViewSwitcher.init((ViewGroup) vh.view, this.mHoverCardPresenterSelector);
            }

            ItemBridgeAdapter.ViewHolder ibh = (ItemBridgeAdapter.ViewHolder) vh.mGridView.findViewHolderForPosition(vh.mGridView.getSelectedPosition());
            this.selectChildView(vh, ibh == null ? null : ibh.itemView, false);
        } else if (this.mHoverCardPresenterSelector != null) {
            vh.mHoverCardViewSwitcher.unselect();
        }

    }

    private void setupFadingEffect(AppGridView rowView) {
        VerticalGridView gridView = rowView.getGridView();
        if (this.mBrowseRowsFadingEdgeLength < 0) {
            TypedArray ta = gridView.getContext().obtainStyledAttributes(R.styleable.LeanbackTheme);
            this.mBrowseRowsFadingEdgeLength = (int) ta.getDimension(R.styleable.LeanbackTheme_browseRowsFadingEdgeLength, 0.0F);
//            this.mBrowseRowsFadingEdgeLength = 0;
            ta.recycle();
        }
        gridView.setFadingEdgeLength(this.mBrowseRowsFadingEdgeLength);
//        gridView.setFadingLeftEdgeLength(this.mBrowseRowsFadingEdgeLength);
    }

    protected void onRowViewExpanded(GridRowPresenter.ViewHolder holder, boolean expanded) {
        super.onRowViewExpanded(holder, expanded);
        GridListRowPresenter.ViewHolder vh = (GridListRowPresenter.ViewHolder) holder;
        if (this.getRowHeight() != this.getExpandedRowHeight()) {
            int newHeight = expanded ? this.getExpandedRowHeight() : this.getRowHeight();
//            vh.setHeight(newHeight);
        }

        this.setVerticalPadding(vh);
        this.updateFooterViewSwitcher(vh);
    }

    private int calculateRowNum(int itemSize) {
        boolean needAddCount = itemSize % mSpanCount != 0;
        return needAddCount ? (itemSize / mSpanCount) + 1 : (itemSize / mSpanCount);
    }


    protected void onBindRowViewHolder(GridRowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);
        GridListRowPresenter.ViewHolder vh = (GridListRowPresenter.ViewHolder) holder;
        ListRow rowItem = (ListRow) item;
        vh.mItemBridgeAdapter.setAdapter(rowItem.getAdapter());
        vh.setBaseOnItemKeyListener(baseOnItemViewKeyListener);
        vh.mGridView.setAdapter(vh.mItemBridgeAdapter);

        if (vh.mItemBridgeAdapter != null) {
            int count = vh.mItemBridgeAdapter.getItemCount();
            int rowNum = calculateRowNum(count);
            vh.setHeight(rowNum * vh.getRowHeight());
            vh.mGridView.setNumColumns(mSpanCount);
        }

        vh.mGridView.setContentDescription(rowItem.getContentDescription());
    }

    protected void onUnbindRowViewHolder(GridRowPresenter.ViewHolder holder) {
        GridListRowPresenter.ViewHolder vh = (GridListRowPresenter.ViewHolder) holder;
        vh.mGridView.setAdapter((RecyclerView.Adapter) null);
        vh.setBaseOnItemKeyListener(null);
        vh.mItemBridgeAdapter.clear();
        super.onUnbindRowViewHolder(holder);
    }

    public final boolean isUsingDefaultSelectEffect() {
        return false;
    }

    public boolean isUsingDefaultListSelectEffect() {
        return true;
    }

    public boolean isUsingDefaultShadow() {
        return ShadowOverlayHelper.supportsShadow();
    }

    public boolean isUsingZOrder(Context context) {
        return false;
//        return !Settings.getInstance(context).preferStaticShadows();
    }

    public boolean isUsingOutlineClipping(Context context) {
        return false;
//        return !Settings.getInstance(context).isOutlineClippingDisabled();
    }

    public final void setShadowEnabled(boolean enabled) {
        this.mShadowEnabled = enabled;
    }

    public final boolean getShadowEnabled() {
        return this.mShadowEnabled;
    }

    public final void enableChildRoundedCorners(boolean enable) {
        this.mRoundedCornersEnabled = enable;
    }

    public final boolean areChildRoundedCornersEnabled() {
        return this.mRoundedCornersEnabled;
    }

    final boolean needsDefaultShadow() {
        return this.isUsingDefaultShadow() && this.getShadowEnabled();
    }

    public final void setKeepChildForeground(boolean keep) {
        this.mKeepChildForeground = keep;
    }

    public final boolean isKeepChildForeground() {
        return this.mKeepChildForeground;
    }

    protected ShadowOverlayHelper.Options createShadowOverlayOptions() {
        return ShadowOverlayHelper.Options.DEFAULT;
    }

    protected void onSelectLevelChanged(GridRowPresenter.ViewHolder holder) {
        super.onSelectLevelChanged(holder);
        GridListRowPresenter.ViewHolder vh = (GridListRowPresenter.ViewHolder) holder;
        int i = 0;

        for (int count = vh.mGridView.getChildCount(); i < count; ++i) {
            this.applySelectLevelToChild(vh, vh.mGridView.getChildAt(i));
        }

    }

    protected void applySelectLevelToChild(GridListRowPresenter.ViewHolder rowViewHolder, View childView) {
//        if (this.mShadowOverlayHelper != null && this.mShadowOverlayHelper.needsOverlay()) {
//            int dimmedColor = rowViewHolder.mColorDimmer.getPaint().getColor();
//            this.mShadowOverlayHelper.setOverlayColor(childView, dimmedColor);
//        }

    }

    public void freeze(GridRowPresenter.ViewHolder holder, boolean freeze) {
        GridListRowPresenter.ViewHolder vh = (GridListRowPresenter.ViewHolder) holder;
        vh.mGridView.setScrollEnabled(!freeze);
        vh.mGridView.setAnimateChildLayout(!freeze);
    }

    public void setEntranceTransitionState(GridRowPresenter.ViewHolder holder, boolean afterEntrance) {
        super.setEntranceTransitionState(holder, afterEntrance);
        ((GridListRowPresenter.ViewHolder) holder).mGridView.setChildrenVisibility(afterEntrance ? 0 : 4);
    }

    class GridListRowPresenterItemBridgeAdapter extends ItemBridgeAdapter {
        GridListRowPresenter.ViewHolder mRowViewHolder;

        GridListRowPresenterItemBridgeAdapter(GridListRowPresenter.ViewHolder rowViewHolder) {
            this.mRowViewHolder = rowViewHolder;
        }

        protected void onCreate(ItemBridgeAdapter.ViewHolder viewHolder) {
            if (viewHolder.itemView instanceof ViewGroup) {
                TransitionHelper.setTransitionGroup((ViewGroup) viewHolder.itemView, true);
            }

//            if (ListRowPresenter.this.mShadowOverlayHelper != null) {
//                ListRowPresenter.this.mShadowOverlayHelper.onViewCreated(viewHolder.itemView);
//            }

        }

        public void onBind(final ItemBridgeAdapter.ViewHolder viewHolder) {
            if (this.mRowViewHolder.getOnItemViewClickedListener() != null) {
                viewHolder.mHolder.view.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ItemBridgeAdapter.ViewHolder ibh = (ItemBridgeAdapter.ViewHolder) GridListRowPresenter.GridListRowPresenterItemBridgeAdapter.this.mRowViewHolder.mGridView.getChildViewHolder(viewHolder.itemView);
                        if (GridListRowPresenter.GridListRowPresenterItemBridgeAdapter.this.mRowViewHolder.getOnItemViewClickedListener() != null) {
                            GridListRowPresenter.GridListRowPresenterItemBridgeAdapter.this.mRowViewHolder.getOnItemViewClickedListener().onItemClicked(viewHolder.mHolder, ibh.mItem, GridListRowPresenter.GridListRowPresenterItemBridgeAdapter.this.mRowViewHolder, (ListRow) GridListRowPresenter.GridListRowPresenterItemBridgeAdapter.this.mRowViewHolder.mRow);
                        }

                    }
                });
            }

            if (mRowViewHolder.getBaseOnItemKeyListener() != null) {
                viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() != KeyEvent.ACTION_DOWN) {
                            return false;
                        }

                        ItemBridgeAdapter.ViewHolder ibh = (ItemBridgeAdapter.ViewHolder) GridListRowPresenter.GridListRowPresenterItemBridgeAdapter.this.mRowViewHolder.mGridView.getChildViewHolder(viewHolder.itemView);
                        return mRowViewHolder.getBaseOnItemKeyListener().onItemKey(keyCode, viewHolder.mHolder, ibh.mItem, GridListRowPresenter.GridListRowPresenterItemBridgeAdapter.this.mRowViewHolder, (ListRow) GridListRowPresenter.GridListRowPresenterItemBridgeAdapter.this.mRowViewHolder.mRow);
                    }
                });
            }

        }

        public void onUnbind(ItemBridgeAdapter.ViewHolder viewHolder) {
            if (this.mRowViewHolder.getOnItemViewClickedListener() != null) {
                viewHolder.mHolder.view.setOnClickListener((View.OnClickListener) null);
            }

            viewHolder.mHolder.view.setOnKeyListener(null);
        }

        public void onAttachedToWindow(ItemBridgeAdapter.ViewHolder viewHolder) {
            GridListRowPresenter.this.applySelectLevelToChild(this.mRowViewHolder, viewHolder.itemView);
            this.mRowViewHolder.syncActivatedStatus(viewHolder.itemView);
        }

        public void onAddPresenter(Presenter presenter, int type) {
            this.mRowViewHolder.getGridView().getRecycledViewPool().setMaxRecycledViews(type, GridListRowPresenter.this.getRecycledPoolSize(presenter));
        }
    }

    public static class SelectItemViewHolderTask extends ViewHolderTask {
        private int mItemPosition;
        private boolean mSmoothScroll = true;
        ViewHolderTask mItemTask;

        public SelectItemViewHolderTask(int itemPosition) {
            this.setItemPosition(itemPosition);
        }

        public void setItemPosition(int itemPosition) {
            this.mItemPosition = itemPosition;
        }

        public int getItemPosition() {
            return this.mItemPosition;
        }

        public void setSmoothScroll(boolean smoothScroll) {
            this.mSmoothScroll = smoothScroll;
        }

        public boolean isSmoothScroll() {
            return this.mSmoothScroll;
        }

        public ViewHolderTask getItemTask() {
            return this.mItemTask;
        }

        public void setItemTask(ViewHolderTask itemTask) {
            this.mItemTask = itemTask;
        }

        public void run(Presenter.ViewHolder holder) {
            if (holder instanceof GridListRowPresenter.ViewHolder) {
                VerticalGridView gridView = ((GridListRowPresenter.ViewHolder) holder).getGridView();
                androidx.leanback.widget.ViewHolderTask task = null;
                if (this.mItemTask != null) {
                    task = new androidx.leanback.widget.ViewHolderTask() {
                        final ViewHolderTask itemTask;

                        {
                            this.itemTask = GridListRowPresenter.SelectItemViewHolderTask.this.mItemTask;
                        }

                        public void run(RecyclerView.ViewHolder rvh) {
                            ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder) rvh;
                            this.itemTask.run(ibvh.getViewHolder());
                        }
                    };
                }

                if (this.isSmoothScroll()) {
                    gridView.setSelectedPositionSmooth(this.mItemPosition, task);
                } else {
                    gridView.setSelectedPosition(this.mItemPosition, task);
                }
            }

        }
    }

    public static class ViewHolder extends GridRowPresenter.ViewHolder {
        final GridListRowPresenter mListRowPresenter;
        final VerticalGridView mGridView;
        ItemBridgeAdapter mItemBridgeAdapter;
        final HorizontalHoverCardSwitcher mHoverCardViewSwitcher = new HorizontalHoverCardSwitcher();
        final int mPaddingTop;
        final int mPaddingBottom;
        final int mPaddingLeft;
        final int mPaddingRight;
        final View mRootView;

        public ViewHolder(View rootView, VerticalGridView gridView, GridListRowPresenter p) {
            super(rootView);
            this.mRootView = rootView;
            this.mGridView = gridView;
            this.mListRowPresenter = p;
            this.mPaddingTop = this.mGridView.getPaddingTop();
            this.mPaddingBottom = this.mGridView.getPaddingBottom();
            this.mPaddingLeft = this.mGridView.getPaddingLeft();
            this.mPaddingRight = this.mGridView.getPaddingRight();
        }

        public final GridListRowPresenter getListRowPresenter() {
            return this.mListRowPresenter;
        }

        public final VerticalGridView getGridView() {
            return this.mGridView;
        }

        public void setHeight(int height) {
            if (this.mGridView == null) {
                return;
            }
            ViewGroup.LayoutParams params = this.mGridView.getLayoutParams();
            params.height = height;
            this.mGridView.setLayoutParams(params);
        }

        public int getRowHeight() {
            if (this.mRootView == null) {
                return 0;
            }
            return ((AppGridView) mRootView).getRowHeight();
        }


        public final ItemBridgeAdapter getBridgeAdapter() {
            return this.mItemBridgeAdapter;
        }

        public int getSelectedPosition() {
            return this.mGridView.getSelectedPosition();
        }

        public Presenter.ViewHolder getItemViewHolder(int position) {
            ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder) this.mGridView.findViewHolderForAdapterPosition(position);
            return ibvh == null ? null : ibvh.getViewHolder();
        }

        public Presenter.ViewHolder getSelectedItemViewHolder() {
            return this.getItemViewHolder(this.getSelectedPosition());
        }

        public Object getSelectedItem() {
            ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder) this.mGridView.findViewHolderForAdapterPosition(this.getSelectedPosition());
            return ibvh == null ? null : ibvh.getItem();
        }
    }
}
