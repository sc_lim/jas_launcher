/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.favorite;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.tvmodule.dao.Channel;

public class ChannelItem implements Parcelable {
    private Channel channel;
    private boolean isChecked;

    public ChannelItem(Channel channel, boolean isChecked) {
        this.channel = channel;
        this.isChecked = isChecked;
    }

    protected ChannelItem(Parcel in) {
        channel = (Channel) in.readValue(Channel.class.getClassLoader());
        isChecked = in.readByte() != 0;
    }

    public static final Creator<ChannelItem> CREATOR = new Creator<ChannelItem>() {
        @Override
        public ChannelItem createFromParcel(Parcel in) {
            return new ChannelItem(in);
        }

        @Override
        public ChannelItem[] newArray(int size) {
            return new ChannelItem[size];
        }
    };

    public Channel getChannel() {
        return channel;
    }

    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(channel);
        dest.writeByte((byte) (isChecked ? 1 : 0));
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
