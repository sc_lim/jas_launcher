/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import kr.altimedia.launcher.jasmin.ui.app.AccountManager;

public class ChannelImageManager {
    private static SimpleDateFormat timeshift_dateformat;

    static {
        timeshift_dateformat = new SimpleDateFormat("YYYYMMddHHmm");
        timeshift_dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static String getChannelThumbnailUrl(Channel channel) {
        String baseUrl = AccountManager.getInstance().getBaseUrl();
        String serviceId = channel.getServiceId();
        String url = null;
        if (baseUrl != null) {
            url = baseUrl + "/channel/" + serviceId + ".jpg";
        }
//        if (Log.INCLUDE) {
//            Log.d("ChannelImageManager", "getChannelThumbnailUrl() serviceId:" + serviceId + ", url:" + url);
//        }
        return url;
    }

    public static String getTimeShiftThumbnailUrl(Channel channel, long seekTime) {
        String baseUrl = AccountManager.getInstance().getBaseUrl();
        String serviceId = channel.getServiceId();
        String url = null;
        if (baseUrl != null) {
            url = baseUrl + "/timeshift/" + serviceId + "/" + timeshift_dateformat.format(new Date(seekTime)) + "00.jpg";
        }
        if (Log.INCLUDE) {
            Log.d("ChannelImageManager", "getTimeShiftThumbnailUrl() serviceId:" + serviceId + ", seekTime:" + seekTime + ", url:" + url);
        }
        return url;
    }
}
