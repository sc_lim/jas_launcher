/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.system.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.system.manager.ExternalApplicationManager;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ChannelAlertSystemDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager;

/**
 * Created by mc.kim on 06,07,2020
 */
public class GlobalKeyReceiver extends BroadcastReceiver {
    private static final String ACTION_GLOBAL_BUTTON = "android.intent.action.GLOBAL_BUTTON";
    private final String TAG = GlobalKeyReceiver.class.getSimpleName();

    public GlobalKeyReceiver() {
        super();
        if (Log.INCLUDE) {
            Log.d(TAG, "GlobalKeyReceiver");
        }
    }

    private void showLiveTvBlockDialog(int keyCode, Context context) {

        if (TvSingletons.getSingletons(context).getBackendKnobs().getSystemDialogVisibility()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showBlockDialog but already showing  so return");
            }
            return;
        }
        String popuptitle = context.getString(R.string.notification);
        String errorTitle = context.getString(R.string.error_title_dbs);
        ChannelAlertSystemDialog mAlertSystemDialog = new ChannelAlertSystemDialog(context, keyCode, popuptitle, errorTitle);
        mAlertSystemDialog.show(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ACTION_GLOBAL_BUTTON.equals(intent.getAction())) {
            KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive: " + event);
            }
            int keyCode = event.getKeyCode();
            int action = event.getAction();

            if (action != KeyEvent.ACTION_UP) {
                return;
            }

            CWMPServiceProvider.getInstance().notifyHotKeyClicked(keyCode);
            if (keyCode == KeyEvent.KEYCODE_BUTTON_2) {
                /*MonoMax*/
                ExternalApplicationManager.getInstance().launchApp(context, ExternalApplicationManager.APP_MONOMAX, true);
            } else if (keyCode == KeyEvent.KEYCODE_BUTTON_3) {
                /*YouTube*/
                Bundle bundle = new Bundle();
                bundle.putBoolean("yt_remote_button", true);

                ExternalApplicationManager.getInstance().launchApp(context,
                        ExternalApplicationManager.APP_YOUTUBE,
                        true,
                        bundle);
            } else if (keyCode == KeyEvent.KEYCODE_TV || keyCode == KeyEvent.KEYCODE_GUIDE) {


                if (!NetworkUtil.hasNetworkConnection(context) || !AccountManager.getInstance().isAuthenticatedUser()) {
                    Intent brIntent = new Intent(PushMessageReceiver.ACTION_JUMP_MENU);
                    brIntent.putExtra(PushMessageReceiver.KEY_MENU_TYPE, keyCode == KeyEvent.KEYCODE_GUIDE ?
                            PushMessageReceiver.MenuType.Guide : PushMessageReceiver.MenuType.LiveTv);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(brIntent);
                    return;
                }

                if (!TvSingletons.getSingletons(context).getBackendKnobs().isEpgReady(true)) {
                    showLiveTvBlockDialog(keyCode, context);
                    return;
                }

                Intent tvIntent = new Intent(keyCode == KeyEvent.KEYCODE_GUIDE ?
                        PlaybackUtil.ACTION_START_GUIDE : PlaybackUtil.ACTION_START_LIVE);
                tvIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(tvIntent);
            }

            ReminderAlertManager.getInstance().clearAllDialog();
        }
    }
}
