/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.reminder;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.OnDataLoadedListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.reminder.adapter.BookedListBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.reminder.presenter.BookedItemPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.Reminder;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;

public class BookedListDialog extends SafeDismissDialogFragment implements OnDataLoadedListener, View.OnUnhandledKeyEventListener, ReminderAlertManager.OnReminderListener {
    public static final String CLASS_NAME = BookedListDialog.class.getName();
    private final String TAG = BookedListDialog.class.getSimpleName();

    private ProgressBarManager progressBarManager;

    private ArrayList<Reminder> bookedList = new ArrayList<>();
    private ArrayList<Integer> taskList = new ArrayList<>();
    private MbsDataTask dataTask;

    private TextView countView;
    private PagingVerticalGridView gridView;
    private ThumbScrollbar gridScrollbar;
    private final int VISIBLE_ROW_SIZE = 7;
    private BookedListBridgeAdapter gridBridgeAdapter;

    private LinearLayout listDataLayer;
    private LinearLayout emptyDataLayer;
    private View rootView;
    private TvOverlayManager mTvOverlayManager;

    public static BookedListDialog newInstance() {
        Bundle args = new Bundle();
        BookedListDialog fragment = new BookedListDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private void showSafeDialog(SafeDismissDialogFragment dialog) {
        if (mTvOverlayManager == null) {
            return;
        }
        mTvOverlayManager.showDialogFragment(dialog);
    }

    @Override
    public void onDestroyView() {

//        if (bookedList != null && !bookedList.isEmpty()) {
//            bookedList.clear();
//        }
        if (progressBarManager != null) {
            progressBarManager.hide();
            progressBarManager.setRootView(null);
        }
        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
        ReminderAlertManager.getInstance().removeOnReminderListener(this);

        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach: "+mTvOverlayManager);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_mymenu_booked_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        initProgressbar(view);
        initView(view);

        loadData();
    }

    private void initProgressbar(View view) {
        progressBarManager = new ProgressBarManager();
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);

        showProgress();
    }

    private void initView(View view) {

        countView = view.findViewById(R.id.count);
        gridView = view.findViewById(R.id.gridView);
        gridScrollbar = view.findViewById(R.id.gridScrollbar);

        int verticalSpacing = (int) view.getResources().getDimension(R.dimen.mymenu_menu_list_vertical_space);
        gridView.setVerticalSpacing(verticalSpacing);
        gridView.initVertical(VISIBLE_ROW_SIZE);

        listDataLayer = view.findViewById(R.id.listDataLayer);
        emptyDataLayer = view.findViewById(R.id.emptyDataLayer);

    }

    private void checkLoadList(Integer task, boolean added) {
        if (added) {
            if (taskList.contains(task)) {
                return;
            }
            taskList.add(task);
        } else {
            taskList.remove(task);
        }
        if (taskList.isEmpty()) {
            hideProgress();
        }
    }

    private void loadData() {
        List<Reminder> tmpList = ReminderAlertManager.getInstance().getReminderList();
        if(bookedList != null && !bookedList.isEmpty()) {
            bookedList.clear();
        }
        if(tmpList != null) {
            bookedList.addAll(tmpList);
        }

        ReminderAlertManager.getInstance().addOnReminderListener(this);

        showView(0);
    }

    @Override
    public void hideProgress() {
        if (progressBarManager != null) {
            progressBarManager.hide();
        }
    }

    @Override
    public void showProgress() {
        if (taskList.isEmpty()) {
            progressBarManager.show();
        }
    }

    @Override
    public void showView(int type) {
        completeTask(type);

        if (taskList.isEmpty()) {
            setGridView();
            hideProgress();
        }
    }

    private void completeTask(int type) {
        switch (type) {
            case 0:
                taskList.remove(dataTask);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "completeTask: removed type=" + type + ", current task size=" + taskList.size());
        }
    }

    private void setGridView() {
        if (bookedList != null && !bookedList.isEmpty()) {

            BookedItemPresenter presenter = new BookedItemPresenter(new BookedItemPresenter.OnKeyListener() {
                @Override
                public boolean onKey(int keyCode, int index, Object item) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onKey: keyCode=" + keyCode + ", index=" + index);
                    }
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                            if (item instanceof Reminder) {
                                onItemSelected(index, (Reminder) item);
                            }
                            return true;
                        case KeyEvent.KEYCODE_DPAD_RIGHT:
                        case KeyEvent.KEYCODE_DPAD_LEFT:
                            return true;
                        case KeyEvent.KEYCODE_DPAD_UP:
                            if(gridView != null) {
                                if (index == 0) {
                                    int size = bookedList.size(); // gridView.getAdapter().getItemCount();
                                    gridView.scrollToPosition(size - 1);
                                    return true;
                                }
                            }
                            break;
                        case KeyEvent.KEYCODE_DPAD_DOWN:
                            if(gridView != null) {
                                int size = bookedList.size(); // gridView.getAdapter().getItemCount();
                                if (index == (size - 1)) {
                                    gridView.scrollToPosition(0);
                                    return true;
                                }
                            }
                            break;
                    }
                    return false;
                }
            });
            ArrayObjectAdapter listObjectAdapter = new ArrayObjectAdapter(presenter);
            for (int i = 0; i < bookedList.size(); i++) {
                listObjectAdapter.add(bookedList.get(i));
            }
            gridBridgeAdapter = new BookedListBridgeAdapter(listObjectAdapter, VISIBLE_ROW_SIZE);
            gridBridgeAdapter.setAdapter(listObjectAdapter);
            gridView.setAdapter(gridBridgeAdapter);

            countView.setText("(" + bookedList.size() + ")");

            gridScrollbar.setList(bookedList.size(), VISIBLE_ROW_SIZE);
            gridView.setNumColumns(1);
            gridView.addOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {
                @Override
                public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                    super.onChildViewHolderSelected(parent, child, position, subposition);
                    gridScrollbar.moveFocus(position);
                }
            });

            gridScrollbar.setFocusable(false);

            emptyDataLayer.setVisibility(View.GONE);
            countView.setVisibility(View.VISIBLE);
            listDataLayer.setVisibility(View.VISIBLE);

        } else {
            countView.setVisibility(View.GONE);
            listDataLayer.setVisibility(View.GONE);
            emptyDataLayer.setVisibility(View.VISIBLE);
        }
    }

    private void onItemSelected(int index, Reminder item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onItemSelected");
        }
        BookedDeletePopup dialog = BookedDeletePopup.newInstance(item);
        dialog.setOnConfirmListener(new BookedDeletePopup.OnConfirmListener() {
            @Override
            public void onConfirmed(boolean confirmed) {
                dialog.dismiss();
                if (confirmed) {
                    boolean result = ReminderAlertManager.getInstance().requestRemoveReminder(getContext(), item);
                    if (result) {
                        showToast();
                        loadData();
                    }
                }
            }
        });
        showSafeDialog(dialog);
    }

    private void showToast() {
        Context context = getContext();
        JasminToast.makeToast(context, R.string.reminder_delete_toast_desc);
    }

    @Override
    public void onReminderListChange(List<Reminder> list, Reminder reminder) {
        loadData();
    }
}
