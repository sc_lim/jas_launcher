package kr.altimedia.launcher.jasmin.system.manager.reboot;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;

import com.altimedia.util.DeviceUtil;
import com.altimedia.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ProcessLifecycleOwner;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class AutoRebootService extends IntentService {
    private final String TAG = AutoRebootService.class.getSimpleName();
    private final long TIME_GAP = TimeUtil.HOUR;

    public AutoRebootService() {
        super(AutoRebootService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onHandleIntent");
        }

        Context context = getApplicationContext();
        long now = JasmineEpgApplication.getSingletons(context).getClock().currentTimeMillis();
        long rebootTime = intent.getLongExtra(AutoRebootManager.KEY_REBOOT_TIME, -1);
        if (Log.INCLUDE) {
            Log.d(TAG, "onHandleIntent, now : " + now + ", rebootTime : " + rebootTime);
        }

        if (now > rebootTime + TIME_GAP) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onHandleIntent, wrong time set, so return");
            }
            AutoRebootManager.getInstance().setRebootTimer(context);
            return;
        }

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isInteractive();

        if (Log.INCLUDE) {
            Log.d(TAG, "onHandleIntent, isScreenOn : " + isScreenOn);
        }

        //sleep mode시 무조건 reboot
        if (!isScreenOn) {
            DeviceUtil.rebootQuiescent(context);
            return;
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Lifecycle.State mState = ProcessLifecycleOwner.get().getLifecycle().getCurrentState();
                if (Log.INCLUDE) {
                    Log.d(TAG, "onHandleIntent, mState : " + mState);
                }

                //launcher app이 최 상위에 위치했을때만 reboot
                if (mState == Lifecycle.State.RESUMED) {
                    AutoRebootDialog mAutoRebootDialog = new AutoRebootDialog(context);
                    mAutoRebootDialog.show();
                    return;
                }

                setNextRebootTimer(context);
            }
        });
    }

    private void setNextRebootTimer(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setNextRebootTimer");
        }

        AutoRebootManager.getInstance().setRebootTimer(context);
    }
}
