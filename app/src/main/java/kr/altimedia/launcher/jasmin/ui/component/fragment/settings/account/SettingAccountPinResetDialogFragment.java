/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;

public class SettingAccountPinResetDialogFragment extends SettingBaseDialogFragment
        implements SettingAccountPinResetFragment.OnResetPinListener, SettingAccountPinResetSuccessFragment.OnClickChangeButtonListener {
    public static final String CLASS_NAME = SettingAccountPinResetDialogFragment.class.getName();
    private final String TAG = SettingAccountPinResetDialogFragment.class.getSimpleName();

    private SettingTaskManager settingTaskManager;

    private OnResetChangeListener mOnResetChangeListener;

    private SettingAccountPinResetDialogFragment() {
    }

    public static SettingAccountPinResetDialogFragment newInstance() {

        Bundle args = new Bundle();

        SettingAccountPinResetDialogFragment fragment = new SettingAccountPinResetDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnResetChangeListener(OnResetChangeListener mOnResetChangeListener) {
        this.mOnResetChangeListener = mOnResetChangeListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_setting_pin_reset, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        settingTaskManager = new SettingTaskManager(getFragmentManager(), mTvOverlayManager);
        initView(view);
    }

    private void initView(View view) {
        SettingAccountPinResetFragment settingAccountPinResetFragment = SettingAccountPinResetFragment.newInstance();
        settingAccountPinResetFragment.setOnResetPinListener(this);
        showFragment(settingAccountPinResetFragment, SettingAccountPinResetFragment.CLASS_NAME);
    }

    private void showFragment(Fragment fragment, String tag) {
        attachFragment(fragment, tag);
    }

    private void attachFragment(Fragment fragment, String tag) {
        getChildFragmentManager().beginTransaction().replace(R.id.frame_layout, fragment, tag).commit();
    }

    @Override
    public void onResetComplete(boolean isSuccess) {
        if (isSuccess) {
            SettingAccountPinResetSuccessFragment settingAccountPinResetSuccessFragment = SettingAccountPinResetSuccessFragment.newInstance();
            settingAccountPinResetSuccessFragment.setOnClickChangeButtonListener(this);
            showFragment(settingAccountPinResetSuccessFragment, SettingAccountPinResetSuccessFragment.CLASS_NAME);
        } else {
            dismiss();
        }
    }

    @Override
    public SettingTaskManager getSettingTaskManager() {
        return settingTaskManager;
    }

    @Override
    public void onClickChangeButton(boolean isNowChange) {
        if (isNowChange) {
            mOnResetChangeListener.onResetAndChangePin();
            showChangePinDialog();
        } else {
            mOnResetChangeListener.onRestPin();
            dismiss();
        }
    }

    private void showChangePinDialog() {
        SettingAccountPinChangeDialogFragment settingAccountPinChangeDialogFragment = SettingAccountPinChangeDialogFragment.newInstance();
        settingAccountPinChangeDialogFragment.setOnPinChangeResultCallback(new SettingAccountPinChangeDialogFragment.OnPinChangeResultCallback() {
            @Override
            public void onSuccess() {
                settingAccountPinChangeDialogFragment.dismiss();
                dismiss();
            }

            @Override
            public void onFail(int key) {
                FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getContext());
                failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
            }

            @Override
            public void onError(String errorCode, String message) {
                settingTaskManager.showErrorDialog(getContext(), TAG, errorCode, message);
            }

            @Override
            public void onCancel() {
                settingAccountPinChangeDialogFragment.dismiss();
                dismiss();
            }

            @Override
            public SettingTaskManager getSettingTaskManager() {
                return settingTaskManager;
            }
        });
        mTvOverlayManager.showDialogFragment(settingAccountPinChangeDialogFragment);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mOnResetChangeListener = null;
    }

    public interface OnResetChangeListener {
        void onRestPin();

        void onResetAndChangePin();
    }
}
