
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.tvmodule.util.StringUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.dm.def.ProductTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.RentalType;
import kr.altimedia.launcher.jasmin.dm.def.RentalTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class Product implements Parcelable {

    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("discountrate")
    @Expose
    private String discountRate;
    @SerializedName("endDateTime")
    @Expose
    private Date endDateTime;
    @SerializedName("isHidden")
    @Expose
    private boolean isHidden;
    @SerializedName("isPurchased")
    @Expose
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isPurchased;
    @SerializedName("purchaseDate")
    @Expose
    private long purchaseDate;
    @SerializedName("expiredDate")
    @Expose
    private long expiredDate;
    @SerializedName("period")
    @Expose
    private int period;
    @SerializedName("offerId")
    @Expose
    private String offerID;
    @SerializedName("onScreenYn")
    @Expose
    private String onScreenYn;
    @SerializedName("payment")
    @Expose
    private PaymentType payment;
    @SerializedName("price")
    @Expose
    private float price;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("productType")
    @Expose
    @JsonAdapter(ProductTypeDeserializer.class)
    private ProductType productType;
    @SerializedName("rentalType")
    @Expose
    @JsonAdapter(RentalTypeDeserializer.class)
    private RentalType rentalType;
    @SerializedName("startDateTime")
    @Expose
    private Date startDateTime;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("containerType")
    @Expose
    private String containerType;
    @SerializedName("thumbnailFileName")
    @Expose
    private String thumbnailFileName;
    //for package
    @SerializedName("rating")
    @Expose
    private int rating;
    //for subscribe
    @SerializedName("productDescription")
    @Expose
    private String description;

    public final static Creator<Product> CREATOR = new Creator<Product>() {

        @SuppressWarnings({
                "unchecked"
        })
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return (new Product[size]);
        }

    };

    protected Product(Parcel in) {

        this.currency = ((String) in.readValue((String.class.getClassLoader())));
        this.discountRate = ((String) in.readValue((String.class.getClassLoader())));
        this.endDateTime = ((Date) in.readValue((Date.class.getClassLoader())));
        this.isHidden = in.readByte() != 0;
        this.isPurchased = in.readByte() != 0;
        this.purchaseDate = (long) in.readValue(Long.class.getClassLoader());
        this.expiredDate = (long) in.readValue(Long.class.getClassLoader());
        this.period = (int) in.readValue(Integer.class.getClassLoader());
        this.offerID = ((String) in.readValue((String.class.getClassLoader())));
        this.onScreenYn = ((String) in.readValue((String.class.getClassLoader())));
        this.payment = ((PaymentType) in.readValue((PaymentType.class.getClassLoader())));
        this.price = (float) in.readValue((Float.class.getClassLoader()));
        this.productName = ((String) in.readValue((String.class.getClassLoader())));
        this.productType = (ProductType) in.readValue(ProductType.class.getClassLoader());
        this.rentalType = (RentalType) in.readValue(RentalType.class.getClassLoader());
        this.startDateTime = ((Date) in.readValue((Date.class.getClassLoader())));
        this.productId = ((String) in.readValue((String.class.getClassLoader())));
        this.duration = ((String) in.readValue((String.class.getClassLoader())));
        this.containerType = ((String) in.readValue((String.class.getClassLoader())));
        this.thumbnailFileName = ((String) in.readValue((String.class.getClassLoader())));
        this.rating = (int) in.readValue(Integer.class.getClassLoader());
        this.description = (String) in.readValue(String.class.getClassLoader());
    }

    public Product() {
    }

    public ProductType getProductType() {
        if (productType == null) {
            return ProductType.ERROR;
        }
        return productType;
    }

    public String getCurrency() {
        return currency;
    }


    public String getDiscountRate() {
        return discountRate;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public boolean isPurchased() {
        return isPurchased;
    }

    public long getPurchaseDate() {
        return purchaseDate;
    }

    public long getExpiredDate() {
        return expiredDate;
    }

    public int getPeriod() {
        return period;
    }

    public String getOfferID() {
        return offerID;
    }

    public String getOnScreenYn() {
        return onScreenYn;
    }

    public PaymentType getPayment() {
        return payment;
    }

    public float getPrice() {
        return price;
    }

    public String getProductName() {
        return productName;
    }

    public RentalType getRentalType() {
        if (rentalType == null) {
            return RentalType.ERROR;
        }
        return rentalType;
    }

    public void setRentalType(RentalType rentalType) {
        if (this.rentalType != null) {
            return;
        }
        this.rentalType = rentalType;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public int getRating() {
        return rating;
    }

    public String getDescription() {
        return description;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(currency);
        dest.writeValue(discountRate);
        dest.writeValue(endDateTime);
        dest.writeByte((byte) (isHidden ? 1 : 0));
        dest.writeByte((byte) (isPurchased ? 1 : 0));
        dest.writeValue(purchaseDate);
        dest.writeValue(expiredDate);
        dest.writeValue(period);
        dest.writeValue(offerID);
        dest.writeValue(onScreenYn);
        dest.writeValue(payment);
        dest.writeValue(price);
        dest.writeValue(productName);
        dest.writeValue(productType);
        dest.writeValue(rentalType);
        dest.writeValue(startDateTime);
        dest.writeValue(productId);
        dest.writeValue(duration);
        dest.writeValue(containerType);
        dest.writeValue(thumbnailFileName);
        dest.writeValue(rating);
        dest.writeValue(description);
    }

    public int describeContents() {
        return 0;
    }

    public String getProductId() {
        return productId;
    }

    public Duration getDuration() {
        return Duration.deserialize(duration);
    }

    public String getPosterSourceUrl(int width, int height) {
        String posterType = StringUtils.nullToEmpty(containerType).isEmpty() ? "" : containerType;
        return thumbnailFileName + "_" + width + "x" + height + "." + posterType;
    }

    @Override
    public String toString() {
        return "Product{" +
                "currency='" + currency + '\'' +
                ", discountRate='" + discountRate + '\'' +
                ", endDateTime=" + endDateTime +
                ", isHidden=" + isHidden +
                ", isPurchased=" + isPurchased +
                ", purchaseDate=" + purchaseDate +
                ", expiredDate=" + expiredDate +
                ", period=" + period +
                ", offerID='" + offerID + '\'' +
                ", onScreenYn='" + onScreenYn + '\'' +
                ", payment=" + payment +
                ", price='" + price + '\'' +
                ", productName='" + productName + '\'' +
                ", productType=" + productType +
                ", rentalType=" + rentalType +
                ", startDateTime=" + startDateTime +
                ", productId='" + productId + '\'' +
                ", duration='" + duration + '\'' +
                ", containerType='" + containerType + '\'' +
                ", thumbnailFileName='" + thumbnailFileName + '\'' +
                ", rating=" + rating +
                ", desc='" + description + '\'' +
                '}';
    }
}
