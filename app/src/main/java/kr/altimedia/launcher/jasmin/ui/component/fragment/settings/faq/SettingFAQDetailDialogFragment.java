/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.faq;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.FAQInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.ScrollDescriptionTextButton;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;

public class SettingFAQDetailDialogFragment extends SettingBaseDialogFragment {
    public static final String CLASS_NAME = SettingFAQDetailDialogFragment.class.getName();
    private final String TAG = SettingFAQDetailDialogFragment.class.getSimpleName();

    private static final String KEY_FAQ_INFO = "FAQ_INFO";
    private final int MAX_LINE_COUNT = 7;

    private SettingFAQDetailDialogFragment() {
    }

    public static SettingFAQDetailDialogFragment newInstance(FAQInfo faqInfo) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_FAQ_INFO, faqInfo);

        SettingFAQDetailDialogFragment fragment = new SettingFAQDetailDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_faq_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        initListener(view);
    }

    private void initView(View view) {
        FAQInfo faqInfo = getArguments().getParcelable(KEY_FAQ_INFO);

        TextView desc = view.findViewById(R.id.description);
        if (faqInfo != null) {
            ((TextView) view.findViewById(R.id.title)).setText(faqInfo.getFaqTitle());
            desc.setText(faqInfo.getFaqDescription());
        }

        ScrollDescriptionTextButton close = view.findViewById(R.id.close);
        close.setDescriptionTextView(desc);
        close.setOnButtonKeyListener(new ScrollDescriptionTextButton.OnButtonKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        dismiss();
                        return true;
                }
                return false;
            }
        });
    }

    private void initListener(View view) {
        TextView message = view.findViewById(R.id.description);
        ThumbScrollbar mThumbScrollbar = view.findViewById(R.id.scrollbar);
        mThumbScrollbar.setFocusable(false);

        message.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                mThumbScrollbar.setList(message.getLineCount(), MAX_LINE_COUNT);
            }
        });

        message.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                mThumbScrollbar.moveScroll((scrollY > oldScrollY));
            }
        });
    }
}
