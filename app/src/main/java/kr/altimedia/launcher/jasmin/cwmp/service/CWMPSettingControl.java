/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service;

import android.content.Context;

import af.system.JasHost;
import af.system.settings.JasSettings;
import af.system.settings.Settings;
import af.system.settings.SettingsEventListener;

public class CWMPSettingControl {
    private final String TAG = CWMPSettingControl.class.getSimpleName();
    private JasHost mJasHost;
    private Settings mSettings;
    public static final String KEY_SAID = JasSettings.KEY_SAID;
    public static final String KEY_PROFILE_ID = JasSettings.KEY_PROFILE_ID;
    public static final String KEY_LA_URL = JasSettings.KEY_LA_URL;
    public static final String KEY_LOGIN_TOKEN = JasSettings.KEY_LOGIN_TOKEN;
    public static final String KEY_SUBTITLE_FONT_SIZE = JasSettings.KEY_SUBTITLE_FONT_SIZE;
    public static final String KEY_REGION_CODE = JasSettings.KEY_REGION_CODE;
    public static final String KEY_DEBUG_INFORMATION = JasSettings.KEY_DEBUG_INFORMATION;
    public static final String KEY_COUNTRY_CODE = Settings.KEY_COUNTRY_CODE;
    public static final String KEY_SYSTEM_LANGUAGE_CODE = Settings.KEY_SYSTEM_LANGUAGE_CODE;
    public static final String KEY_SUBTITLE_LANGUAGE_CODE = Settings.KEY_SUBTITLE_LANGUAGE_CODE;
    public static final String KEY_SUBTITLE_ENABLE = Settings.KEY_SUBTITLE_ENABLE;
    public static final String KEY_AUDIO_LANGUAGE_CODE = Settings.KEY_AUDIO_LANGUAGE_CODE;
    public static final String KEY_AUDIO_VOLUME = Settings.KEY_AUDIO_VOLUME;
    public static final String KEY_STANDBY_LEVEL = Settings.KEY_STANDBY_LEVEL;
    public static final String KEY_TEXT_ENCODING = Settings.KEY_TEXT_ENCODING;
    public static final String KEY_TSR = Settings.KEY_TSR;
    public static final String KEY_TSR_INTERVAL = Settings.KEY_TSR_INTERVAL;
    public static final String KEY_AUTO_STANDBY = Settings.KEY_AUTO_STANDBY;
    public static final String KEY_CLOSEDCAPTION_ENABLE = Settings.KEY_CLOSEDCAPTION_ENABLE;
    public static final String KEY_CLOSEDCAPTION_FONT_SIZE = Settings.KEY_CLOSEDCAPTION_FONT_SIZE;
    public static final String KEY_AUDIO_DESCRIPTION = Settings.KEY_AUDIO_DESCRIPTION;
    public static final String KEY_AUDIO_DESCRIPTION_LANGUAGE_CODE = Settings.KEY_AUDIO_DESCRIPTION_LANGUAGE_CODE;
    public static final int VIDEO_RATIO_4_3_NORMAL = Settings.VIDEO_RATIO_4_3_NORMAL;
    public static final int VIDEO_RATIO_4_3_LETTERBOX = Settings.VIDEO_RATIO_4_3_LETTERBOX;
    public static final int VIDEO_RATIO_4_3_SQUEEZED = Settings.VIDEO_RATIO_4_3_SQUEEZED;
    public static final int VIDEO_RATIO_16_9_NORMAL = Settings.VIDEO_RATIO_16_9_NORMAL;
    public static final int VIDEO_RATIO_16_9_WIDE = Settings.VIDEO_RATIO_16_9_WIDE;
    public static final int VIDEO_RATIO_16_9_ZOOM = Settings.VIDEO_RATIO_16_9_ZOOM;
    public static final int VIDEO_RATIO_AUTO_SQUEEZED = Settings.VIDEO_RATIO_AUTO_SQUEEZED;
    public static final int AUDIO_OUTPUT_AC3 = Settings.AUDIO_OUTPUT_AC3;
    public static final int AUDIO_OUTPUT_PCM = Settings.AUDIO_OUTPUT_PCM;
    public static final int AUDIO_OUTPUT_NONE = Settings.AUDIO_OUTPUT_NONE;
    public static final int VIDEO_FORMAT_UNKNOWN = Settings.VIDEO_FORMAT_UNKNOWN;
    public static final int VIDEO_RESOLUTION_PAL = Settings.VIDEO_RESOLUTION_PAL;
    public static final int VIDEO_RESOLUTION_NTSC = Settings.VIDEO_RESOLUTION_NTSC;
    public static final int VIDEO_RESOLUTION_480p = Settings.VIDEO_RESOLUTION_480p;
    public static final int VIDEO_RESOLUTION_576p = Settings.VIDEO_RESOLUTION_576p;
    public static final int VIDEO_RESOLUTION_720p = Settings.VIDEO_RESOLUTION_720p;
    public static final int VIDEO_RESOLUTION_720p50hz = Settings.VIDEO_RESOLUTION_720p50hz;
    public static final int VIDEO_RESOLUTION_1080i = Settings.VIDEO_RESOLUTION_1080i;
    public static final int VIDEO_RESOLUTION_1080i50hz = Settings.VIDEO_RESOLUTION_1080i50hz;
    public static final int VIDEO_RESOLUTION_1080p = Settings.VIDEO_RESOLUTION_1080p;
    public static final int VIDEO_RESOLUTION_1080p24hz = Settings.VIDEO_RESOLUTION_1080p24hz;
    public static final int VIDEO_RESOLUTION_1080p50hz = Settings.VIDEO_RESOLUTION_1080p50hz;
    public static final int VIDEO_RESOLUTION_2160p30hz = Settings.VIDEO_RESOLUTION_2160p30hz;
    public static final int VIDEO_RESOLUTION_2160p60hz = Settings.VIDEO_RESOLUTION_2160p60hz;
    public static final int AUDIO_OUTPUT_DEVICE_STANDARD = Settings.AUDIO_OUTPUT_DEVICE_STANDARD;
    public static final int AUDIO_OUTPUT_DEVICE_BLUETOOTH = Settings.AUDIO_OUTPUT_DEVICE_BLUETOOTH;
    public static final int AUDIO_OUTPUT_DEVICE_BOTH = Settings.AUDIO_OUTPUT_DEVICE_BOTH;
    public static final String NETFLIX_ESN_KEY = Settings.NETFLIX_ESN_KEY;

    private final SettingsEventListener mSettingsEventListener = new SettingsEventListener() {
        @Override
        public void onSettingChanged(String s) {
            if (mCWMPSettingsEventListener != null) {
                mCWMPSettingsEventListener.onSettingChanged(s);
            }
        }
    };

    public CWMPSettingControl() {
        try {
            mJasHost = JasHost.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mSettings = Settings.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setPowerMode(Context context, POWER_MODE power_mode) {
        try {
            mJasHost.setPowerMode(context, power_mode.powerMode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void factoryReset(Context context) {
        try {
            mJasHost.factoryReset(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setVideoResolution(int var1) {
        try {
            mSettings.setVideoResolution(var1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String get(String key) {
        return get(key, "");
    }

    public String get(String key, String defaultValue) {
        try {
            return mSettings.get(key);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public void set(String key, String value) {
        try {
            mSettings.set(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setParentalRating(int rating) {
        try {
            mSettings.setParentalRating(rating);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public enum POWER_MODE {
        STANDBY(JasHost.POWER_MODE.STANDBY),
        RUNNING(JasHost.POWER_MODE.RUNNING);
        private final JasHost.POWER_MODE powerMode;

        POWER_MODE(JasHost.POWER_MODE power_mode) {
            this.powerMode = power_mode;
        }
    }


    private CWMPSettingsEventListener mCWMPSettingsEventListener;

    public void setSettingsEventListener(CWMPSettingsEventListener settingsEventListener) {
        mCWMPSettingsEventListener = settingsEventListener;
        try {
            mSettings.addSettingsEventListener(mSettingsEventListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void clearSettingsEventListener() {
        mCWMPSettingsEventListener = null;
        try {
            mSettings.removeSettingsEventListener(mSettingsEventListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public interface CWMPSettingsEventListener {
        void onSettingChanged(String var1);
    }

}
