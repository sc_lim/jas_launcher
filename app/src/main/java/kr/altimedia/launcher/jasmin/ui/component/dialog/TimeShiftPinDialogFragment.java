package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;

public class TimeShiftPinDialogFragment extends SafeDismissDialogFragment {
    public static final String DIALOG_TAG = TimeShiftPinDialogFragment.class.getName();

    private static final String TAG = "TimeShiftPinDialogFragment";
    private static final boolean DEBUG = false;

    /**
     * PIN code dialog for unlock channel
     */
    public static final int PIN_DIALOG_TYPE_UNLOCK_CHANNEL = 0;

    /**
     * PIN code dialog for unlock content.
     */
    public static final int PIN_DIALOG_TYPE_UNLOCK_PROGRAM = 1;

    private static final String ARGS_TYPE = "args_type";
    private static final String ARGS_RATING = "args_rating";

    private int mType;
    private String mRatingString;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private final UserDataManager mUserDataManager = new UserDataManager();

    private MbsDataTask checkPinTask;
    private boolean mPinChecked;
    private boolean mDismissSilently;

    public static TimeShiftPinDialogFragment create(int type) {
        return create(type, null);
    }

    public static TimeShiftPinDialogFragment create(int type, String rating) {
        TimeShiftPinDialogFragment fragment = new TimeShiftPinDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARGS_TYPE, type);
        args.putString(ARGS_RATING, rating);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), R.style.Theme_TV_dialog_Fullscreen);
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_livetv_inputpincode, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mType = getArguments().getInt(ARGS_TYPE, PIN_DIALOG_TYPE_UNLOCK_CHANNEL);
        mRatingString = getArguments().getString(ARGS_RATING);

        PasswordView password = view.findViewById(R.id.pin);
        password.requestFocus();

        password.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyInputChange(), isFull : " + isFull + ", input : " + input);
                }

                if (isFull) {
                    checkPinCode(input);
                }
            }
        });

        view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss(false);
            }
        });

        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void checkPinCode(String password) {
        checkPinTask = mUserDataManager.checkAccountPinCode(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), password,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onFailed");
                        }

                        setPinError();
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onSuccess, result : " + result);
                        }

                        if (result) {
                            Log.d(TAG, "ontifyInputChange() correct passwd");
                            getView().findViewById(R.id.description).setVisibility(View.GONE);
                            dismiss(true);
                        } else {
                            setPinError();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return TimeShiftPinDialogFragment.this.getContext();
                    }
                });
    }

    private void setPinError() {
        View view = getView();
        PasswordView password = view.findViewById(R.id.pin);
        TextView description = view.findViewById(R.id.description);

        password.clear();
        description.setVisibility(View.VISIBLE);

        if (Log.INCLUDE) {
            Log.d(TAG, "ontifyInputChange() wrong passwd");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (checkPinTask != null) {
            checkPinTask.cancel(true);
        }
    }

    private void dismiss(boolean onPinChecked) {
        mPinChecked = onPinChecked;
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (mOnPinCheckedListener != null) {
            mOnPinCheckedListener.onPinChecked(mPinChecked, mType, mRatingString);
        }
        mDismissSilently = false;
        mOnPinCheckedListener = null;
    }

    private OnPinCheckedListener mOnPinCheckedListener;

    public void setOnPinCheckedListener(OnPinCheckedListener mOnPinCheckedListener) {
        this.mOnPinCheckedListener = mOnPinCheckedListener;
    }

    public interface OnPinCheckedListener {
        void onPinChecked(boolean checked, int type, String rating);
    }
}
