/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.interactor;

import android.graphics.Rect;
import android.os.Handler;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;

import kr.altimedia.launcher.jasmin.tv.JasTvView;
import kr.altimedia.launcher.jasmin.tv.ui.ChannelPreviewIndicator;

public interface TvViewInteractor {
    static TuneResult convertTuneResult(JasTvView.TuningResult tuningResult) {
        switch (tuningResult) {
            case TUNING_BLOCKED_NOTSUPPORTED:
            case TUNING_BLOCKED_CHANNEL:
                return TuneResult.TUNE_BLOCKED_CHANNEL;
            case TUNING_BLOCKED_ADULT_CHANNEL:
                return TuneResult.TUNE_BLOCKED_ADULT_CHANNEL;
            case TUNING_BLOCKED_PROGRAM:
                return TuneResult.TUNE_BLOCKED_PROGRAM;
            case TUNING_BLOCKED_UNSUBSCRIBED:
                return TuneResult.TUNE_BLOCKED_UNSUBSCRIBED;
            case TUNING_NONE:
            case TUNING_REQUESTED:
            case TUNING_SUCCESS:
            case TUNING_SUCCESS_AUDIO:
            default:
                return TuneResult.TUNE_SUCCESS;
        }
    }

    TuneResult tuneChannel(Channel channel);

    void updateTuneResult(JasTvView.TuningResult tuningResult);

    void resizeTvView(Rect rect);

    void reserveProgram(Program program, Handler handler);

    void cancelReserveProgram(Program program, Handler handler);

    void setPreviewListener(ChannelPreviewIndicator.OnPreviewListener onPreviewListener);

    boolean isLockedChannel(Channel channel, Program program);

    boolean isLockedAdultChannel(Channel channel);

    boolean isUnLocked(Channel channel);

    enum TuneResult {
        TUNE_SUCCESS(false),
        TUNE_BLOCKED_CHANNEL(true),
        TUNE_BLOCKED_ADULT_CHANNEL(true),
        TUNE_BLOCKED_PROGRAM(true),
        TUNE_BLOCKED_UNSUBSCRIBED(true);
        private boolean blocked;

        TuneResult(boolean blocked) {
            this.blocked = blocked;
        }

        public boolean isBlocked() {
            return blocked;
        }

        @Override
        public String toString() {
            return "TuneResult{" +
                    "name=" + name() +
                    ", blocked=" + blocked +
                    '}';
        }
    }

    TuneResult tuneTimeShift(Channel channel, Program program, long startTime);
}
