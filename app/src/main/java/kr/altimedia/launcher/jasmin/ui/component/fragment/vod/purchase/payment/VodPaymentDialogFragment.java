/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Bank;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.BankResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.response.CreditCardResponse;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPaymentBankingFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PURCHASE_INFORM;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_MESSAGE_DESC;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_OFFER_ID;

public class VodPaymentDialogFragment extends SafeDismissDialogFragment implements VodPaymentGuideFragment.OnPaymentListener {
    public static final String CLASS_NAME = VodPaymentDialogFragment.class.getName();
    private static final String TAG = VodPaymentDialogFragment.class.getSimpleName();

    private static final String KEY_SENDER_ID = "SENDER_ID";
    private static final String KEY_PURCHASE_INFO = "PURCHASE_INFO";
    public static final String KEY_PURCHASE_OPTION_ITEM = "OPTION_LIST";

    public final String WORK_DETAIL_CALL = "DETAIL";
    public final String WORK_PURCHASE_CHECK = "PURCHASE_CHECK";
    private ArrayList<String> mWorkList = new ArrayList<>();

    private int mSenderId;
    private PurchaseInfo purchaseInfo;

    private OnPaymentCompleteListener onPaymentListener;

    public VodPaymentDialogFragment() {

    }

    public static VodPaymentDialogFragment newInstance(int senderId, PurchaseInfo purchaseInfo, Parcelable object) {
        Bundle args = new Bundle();
        args.putInt(KEY_SENDER_ID, senderId);
        args.putParcelable(KEY_PURCHASE_INFO, purchaseInfo);
        args.putParcelable(KEY_PURCHASE_OPTION_ITEM, object);

        VodPaymentDialogFragment fragment = new VodPaymentDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnPaymentListener(OnPaymentCompleteListener onPaymentListener) {
        this.onPaymentListener = onPaymentListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogBlackTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_vod_payment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSenderId = getArguments().getInt(KEY_SENDER_ID);
        purchaseInfo = getArguments().getParcelable(KEY_PURCHASE_INFO);
        PaymentType paymentType = purchaseInfo.getPaymentType();

        if (Log.INCLUDE) {
            Log.d(TAG, "PaymentType : " + paymentType);
        }

        if (paymentType == null) {
            showZeroFragment();
            return;
        }

        switch (paymentType) {
            case BILLING:
                showBillingFragment();
                break;
            case MEMBERSHIP_APP:
                showMembershipFragment();
                break;
            case AIR_PAY:
            case QR:
                showGuideFragment();
                break;
            case CREDIT_CARD:
                showCreditCardFragment();
                break;
            case INTERNET_MOBILE_BANK:
                showBankingFragment();
                break;
        }
    }

    public PurchaseInfo getPurchaseInfo() {
        return purchaseInfo;
    }

    private void showZeroFragment() {
        VodPaymentZeroFragment vodPaymentZeroFragment = VodPaymentZeroFragment.newInstance(purchaseInfo);
        attacheFragment(vodPaymentZeroFragment, VodPaymentZeroFragment.CLASS_NAME);
    }

    private void showBillingFragment() {
        VodPaymentBillingFragment vodPaymentBillingFragment = VodPaymentBillingFragment.newInstance(purchaseInfo);
        attacheFragment(vodPaymentBillingFragment, VodPaymentBillingFragment.CLASS_NAME);
    }

    private void showMembershipFragment() {
        VodPaymentMembershipFragment vodPaymentMembershipFragment = VodPaymentMembershipFragment.newInstance(purchaseInfo);
        attacheFragment(vodPaymentMembershipFragment, VodPaymentMembershipFragment.CLASS_NAME);
    }

    private void showGuideFragment() {
        VodPaymentGuideFragment vodPaymentGuideFragment = VodPaymentGuideFragment.newInstance(purchaseInfo);
        attacheFragment(vodPaymentGuideFragment, VodPaymentGuideFragment.CLASS_NAME);
    }

    private void showCreditCardFragment() {
        CreditCardResponse.CreditCardResult creditCardResult = getArguments().getParcelable(KEY_PURCHASE_OPTION_ITEM);
        if (creditCardResult == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showCreditCardFragment, creditCardResult is null, so do not show payment dialog");
            }

            dismiss();
            return;
        }

        ArrayList<CreditCard> otherCardList = (ArrayList<CreditCard>) creditCardResult.getOtherCardList();
        ArrayList<CreditCard> creditCardList = (ArrayList<CreditCard>) creditCardResult.getCreditCardList();

        VodPaymentCreditCardFragment vodPaymentCreditCardFragment = VodPaymentCreditCardFragment.newInstance(otherCardList, creditCardList);
        vodPaymentCreditCardFragment.setOnClickCreditCardListener(new VodPaymentCreditCardFragment.OnClickCreditCardListener() {
            @Override
            public void onClickCreditCard(CreditCard creditCard) {
                if (Log.INCLUDE) {
                    Log.d(TAG, ", onClickBank, creditCard : " + creditCard);
                }

                purchaseInfo.setCreditCard(creditCard);
                showGuideFragment();
            }
        });
        attacheFragment(vodPaymentCreditCardFragment, VodPaymentCreditCardFragment.CLASS_NAME);
    }

    private void showBankingFragment() {
        BankResponse.BankResult bankResult = getArguments().getParcelable(KEY_PURCHASE_OPTION_ITEM);
        if (bankResult == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showBankingFragment, bankResult is null, so do not show payment dialog");
            }

            dismiss();
            return;
        }

        ArrayList optionList = (ArrayList) bankResult.getBankList();
        VodPaymentBankingFragment vodPaymentBankingFragment = VodPaymentBankingFragment.newInstance(optionList);
        vodPaymentBankingFragment.setOnClickBankListener(new VodPaymentBankingFragment.OnClickBankListener() {
            @Override
            public void onClickBank(Bank bank) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onClickBank, bank : " + bank);
                }

                purchaseInfo.setBank(bank);
                showGuideFragment();
            }
        });
        attacheFragment(vodPaymentBankingFragment, VodPaymentBankingFragment.CLASS_NAME);
    }

    public void showPurchaseSuccessFragment(int senderId, String work) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showPurchaseSuccessFragment, mSenderId : " + mSenderId + ", senderId : " + senderId + ", work : " + work);
        }

        if (this.mSenderId != senderId || mWorkList.contains(work)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showPurchaseSuccessFragment, return");
            }
            return;
        }

        mWorkList.add(work);

        if (Log.INCLUDE) {
            Log.d(TAG, "showPurchaseSuccessFragment, work size : " + mWorkList.size());
        }

        if (mWorkList.size() == 2) {
            VodPaymentSuccessFragment vodPaymentSuccessFragment = VodPaymentSuccessFragment.newInstance(purchaseInfo);
            attacheFragment(vodPaymentSuccessFragment, VodPaymentSuccessFragment.CLASS_NAME);
            mWorkList.clear();
        }
    }

    private void showPurchaseFailFragment(String paymentErrorMessage) {
        VodPaymentFailFragment vodPaymentFailFragment = VodPaymentFailFragment.newInstance(purchaseInfo, paymentErrorMessage);
        attacheFragment(vodPaymentFailFragment, VodPaymentFailFragment.CLASS_NAME);
    }

    private void attacheFragment(Fragment fragment, String tag) {
        VodPaymentBaseFragment vodPaymentBaseFragment = (VodPaymentBaseFragment) fragment;
        vodPaymentBaseFragment.setOnPaymentListener(this);
        getChildFragmentManager().beginTransaction().replace(R.id.frame, vodPaymentBaseFragment, tag).commit();
    }

    private void onPurchasePushMessage() {
        Intent mIntent = new Intent(getContext(), PushMessageReceiver.class);
        mIntent.setAction(ACTION_UPDATED_PURCHASE_INFORM);
        mIntent.putExtra(KEY_MESSAGE_DESC, "Success");
        mIntent.putExtra(KEY_OFFER_ID, purchaseInfo.getProduct().getOfferID());
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(mIntent);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onPaymentListener = null;
    }

    @Override
    public void onPaymentComplete(boolean forcePush, boolean isSuccess) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPaymentComplete, isSuccess : " + isSuccess + ", forcePush : " + forcePush);
        }

        if (isSuccess) {
            if (forcePush) {
                onPurchasePushMessage();
            }

            showPurchaseSuccessFragment(mSenderId, WORK_PURCHASE_CHECK);
            if (onPaymentListener != null) {
                onPaymentListener.onComplete(true);
            }
        } else {
            if (Log.INCLUDE) {
                Log.d(TAG, "if payment is Fail, Server should give a error message by OnError!!!!");
            }

            String errorMessage = getString(R.string.payment_error);
            showPurchaseFailFragment(errorMessage);
        }
    }

    @Override
    public void onPaymentError(String paymentErrorMessage) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onPaymentError, onPaymentError, paymentErrorMessage " + paymentErrorMessage);
        }

        if (onPaymentListener != null) {
            onPaymentListener.onComplete(false);
        }

        showPurchaseFailFragment(paymentErrorMessage);
    }

    @Override
    public void onRequestWatch(boolean isRequest) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onRequestWatch, isRequest : " + isRequest);
        }

        onPaymentListener.onRequestWatch(isRequest);
    }

    @Override
    public void onDismissFragment() {
        dismiss();
    }

    @Override
    public void onCancelPayment(String cancelPurchaseId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCancelPayment");
        }

        onPaymentListener.onCancelPayment();
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public interface OnPaymentCompleteListener {
        void onComplete(boolean isSuccess);

        void onRequestWatch(boolean isRequest);

        void onCancelPayment();
    }
}
