/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import javax.inject.Named;
import javax.inject.Singleton;

import kr.altimedia.launcher.jasmin.dm.def.CouponType;
import kr.altimedia.launcher.jasmin.dm.def.CouponTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.NormalDateTimeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.PaymentTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.dm.user.api.UserApi;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static kr.altimedia.launcher.jasmin.dm.def.ServerConfig.MBS_URL;

/**
 * Created by mc.kim on 11,05,2020
 */
public class UserModule {
    public Retrofit provideUserModule(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(MBS_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(CouponType.class, new CouponTypeDeserializer());
        builder.registerTypeAdapter(PaymentType.class, new PaymentTypeDeserializer());
        builder.registerTypeAdapter(Date.class, new NormalDateTimeDeserializer());
        return builder.create();
    }

    @Singleton
    public UserApi provideUserApi(@Named("UserApi") Retrofit retrofit) {
        return retrofit.create(UserApi.class);
    }
}
