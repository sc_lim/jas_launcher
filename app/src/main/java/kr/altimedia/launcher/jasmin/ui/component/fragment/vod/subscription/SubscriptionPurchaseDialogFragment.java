/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment.SubscriptionPaymentDialogFragment;

public class SubscriptionPurchaseDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = SubscriptionPurchaseDialogFragment.class.getName();
    private static final String TAG = SubscriptionPurchaseDialogFragment.class.getSimpleName();

    public static final String KEY_SENDER_ID = "SENDER_ID";
    public static final String KEY_SUBSCRIPTION_INFO = "SUBSCRIPTION_INFO";

    private SubscriptionPaymentDialogFragment.OnCompleteSubscribePaymentListener onCompleteSubscribePaymentListener;

    public SubscriptionPurchaseDialogFragment() {

    }

    public static SubscriptionPurchaseDialogFragment newInstance(int mSenderId, SubscriptionInfo subscriptionInfo) {
        Bundle args = new Bundle();
        args.putInt(KEY_SENDER_ID, mSenderId);
        args.putParcelable(KEY_SUBSCRIPTION_INFO, subscriptionInfo);

        SubscriptionPurchaseDialogFragment fragment = new SubscriptionPurchaseDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnCompleteSubscribePaymentListener(SubscriptionPaymentDialogFragment.OnCompleteSubscribePaymentListener onCompleteSubscribePaymentListener) {
        this.onCompleteSubscribePaymentListener = onCompleteSubscribePaymentListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_subscription_purchase, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        attachView();
    }

    private void attachView() {
        Bundle args = getArguments();
        int mSenderId = args.getInt(KEY_SENDER_ID);
        SubscriptionInfo subscriptionInfo = args.getParcelable(KEY_SUBSCRIPTION_INFO);

        SubscriptionPurchaseFragment subscriptionPurchaseFragment = SubscriptionPurchaseFragment.newInstance(mSenderId, subscriptionInfo);
        subscriptionPurchaseFragment.setOnCompleteSubscribePaymentListener(onCompleteSubscribePaymentListener);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.layout, subscriptionPurchaseFragment, SubscriptionPurchaseFragment.CLASS_NAME).commit();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onCompleteSubscribePaymentListener = null;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }
}