/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.system.debug;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.BuildConfig;

public class StabilityDebugger extends BroadcastReceiver {

    private static final String TAG = StabilityDebugger.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onReceive, context=" + context + ", intent=" + intent);
        }

        if (BuildConfig.DEBUG) {
            if (intent.getAction().equals("kr.altimedia.jassystem.MEMORY_CHECK")) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "========== START MANUAL GC ==========");
                }

                System.gc();

                if (Log.INCLUDE) {
                    Log.d(TAG, "========== END GC ==========");
                }
            }
        }
    }

}
