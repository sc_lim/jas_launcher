/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.category.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import kr.altimedia.launcher.jasmin.dm.category.api.CategoryApi;
import kr.altimedia.launcher.jasmin.dm.def.CategoryType;
import kr.altimedia.launcher.jasmin.dm.def.CategoryTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mc.kim on 06,12,2019
 */
public class CategoryModule {

    public Retrofit provideCategoryModule(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(ServerConfig.MBS_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(CategoryType.class, new CategoryTypeDeserializer());
        return builder.create();
    }

    @Singleton
    public CategoryApi provideCategoryApi(@Named("Category") Retrofit retrofit) {
        return retrofit.create(CategoryApi.class);
    }
}
