/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;

import com.altimedia.util.Log;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.PurchasedContent;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.ComfirmPopup;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.ButtonItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.presenter.ButtonItemPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history.adapter.PurchasedListBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history.presenter.PurchasedItemPresenter;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PURCHASE_INFORM;

public class PurchasedEditFragment extends PurchasedBaseFragment {
    public static final String CLASS_NAME = PurchasedEditFragment.class.getName();
    private static final String TAG = PurchasedEditFragment.class.getSimpleName();

    private ArrayList<PurchasedContent> hiddenListOrigin = new ArrayList<>();
    private ArrayList<AsyncTask> taskList = new ArrayList<>();

    private ComfirmPopup comfirmPopup;
    private String currentProfileId;

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent == null) return;

            String action = intent.getAction();
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive: intent=" + intent.getAction());
            }
            if(action.equals(ACTION_UPDATED_PURCHASE_INFORM)) {
                ((PurchasedListDialog) PurchasedEditFragment.this.getParentFragment()).dismiss();
            }
        }
    };

    @NotNull
    public static PurchasedEditFragment newInstance(@NotNull ArrayList<PurchasedContent> pList, @NotNull ArrayList<PurchasedContent> hList) {
        PurchasedEditFragment fragment = new PurchasedEditFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(PurchasedListDialog.KEY_PURCHASED_LIST, pList);
        args.putParcelableArrayList(PurchasedListDialog.KEY_HIDDEN_LIST, hList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        currentProfileId = AccountManager.getInstance().getProfileId();
        setButtonListener(view);

        loadData(getArguments());

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_UPDATED_PURCHASE_INFORM);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, filter);
    }

    private void setButtonListener(View view) {
        try {
            btnSelectAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int totalHiddenSize = getTotalRemovableSize();
                    int currHiddenSize = hiddenList.size();
                    if (currHiddenSize != totalHiddenSize) {
                        hiddenList.clear();
                        if (currentProfileId != null) {
                            for (int i = 0; i < purchasedList.size(); i++) {
                                PurchasedContent purchasedItem = purchasedList.get(i);
                                if (currentProfileId.equals(purchasedItem.getProfileId())) {
                                    purchasedItem.setHidden(true);
                                    gridObjectAdapter.replace(i, purchasedItem);
                                    hiddenList.add(purchasedItem);
                                    totalHiddenSize++;
                                }
                            }
                        }
                    } else {
                        hiddenList.clear();
                        if (currentProfileId != null) {
                            for (int i = 0; i < purchasedList.size(); i++) {
                                PurchasedContent purchasedItem = purchasedList.get(i);
                                if (currentProfileId.equals(purchasedItem.getProfileId())) {
                                    purchasedItem.setHidden(false);
                                    gridObjectAdapter.replace(i, purchasedItem);
                                }
                            }
                        }
                    }

                    setEnableDeleteButton();
                }
            });
            btnSelectAll.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (hasFocus) {
                        infoLayer.setVisibility(View.GONE);
                    }
                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "BtnDelete: onClick");
                    }
                    comfirmPopup = ComfirmPopup.newInstance(getResources().getString(R.string.delete), new ButtonItemPresenter.OnKeyListener() {
                        @Override
                        public boolean onKey(int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "ButtonItemPresenter.onKey() keyCode=" + keyCode + ", index=" + index);
                            }
                            switch (keyCode) {
                                case KeyEvent.KEYCODE_ENTER:
                                case KeyEvent.KEYCODE_DPAD_CENTER: {
                                    if (item instanceof ButtonItem) {
                                        ButtonItem button = (ButtonItem) item;
                                        int type = button.getType();
                                        if (type == ButtonItem.TYPE_OK) {
                                            putPurchasedHiddenList(view);

                                        } else if (type == ButtonItem.TYPE_CANCEL) {
                                            if (comfirmPopup != null) {
                                                comfirmPopup.dismiss();
                                            }
                                        }
                                    }
                                }
                            }
                            return false;
                        }
                    });
                    String delete_confirm_desc1 = "";
                    if (hiddenList.size() == 1) {
                        PurchasedContent content = hiddenList.get(0);
                        delete_confirm_desc1 = content.getTitle();
                    } else {
                        delete_confirm_desc1 = hiddenList.size() + " " + getResources().getString(R.string.contents).toLowerCase();
                    }

                    comfirmPopup.setContent(delete_confirm_desc1, getResources().getString(R.string.purchased_delete_confirm_desc));
                    comfirmPopup.setButtonTitle(getResources().getString(R.string.delete), getResources().getString(R.string.cancel));
                    comfirmPopup.show(getChildFragmentManager(), ComfirmPopup.CLASS_NAME);
                }
            });
            btnDelete.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (hasFocus) {
                        infoLayer.setVisibility(View.GONE);
                    }
                }
            });

            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((PurchasedListDialog) getParentFragment()).backToFragment();
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void loadData(Bundle bundle) {
        if (bundle != null) {
            ArrayList pList = bundle.getParcelableArrayList(PurchasedListDialog.KEY_PURCHASED_LIST);
            if (pList != null) {
                purchasedList = pList;
            }
            ArrayList hList = bundle.getParcelableArrayList(PurchasedListDialog.KEY_HIDDEN_LIST);
            if (hList != null) {
                hiddenList = hList;
                hiddenListOrigin = hList;
            }

            setGridView();
        }
    }

    private void setGridView() {
        if (purchasedList != null && purchasedList.size() > 0) {
            PurchasedContent content = purchasedList.get(0);
            setContentInfo(content);

            PurchasedItemPresenter presenter = new PurchasedItemPresenter(true,
                    new PurchasedItemPresenter.OnKeyListener() {
                        @Override
                        public boolean onKey(int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "PurchasedItemPresenter: onKey: keyCode=" + keyCode + ", index=" + index);
                            }

                            switch (keyCode) {
                                case KeyEvent.KEYCODE_ENTER:
                                case KeyEvent.KEYCODE_DPAD_CENTER:
                                    if (item instanceof PurchasedContent) {
                                        PurchasedContent purchasedItem = (PurchasedContent) item;
                                        if (currentProfileId != null && currentProfileId.equals(purchasedItem.getProfileId())) {
                                            CheckBox checkBox = ((PurchasedItemPresenter.VodItemViewHolder) viewHolder).checkBox;
                                            updateGridView(purchasedItem, checkBox);
                                            updateHiddenList(purchasedItem);
                                        }
                                    }
                                    return true;

                                case KeyEvent.KEYCODE_DPAD_DOWN:
                                    int size = (purchasedList != null ? purchasedList.size() : 0);
                                    int moveIndex = index + PurchasedListDialog.NUMBER_COLUMNS;
                                    int lastRow = gridView.getRowIndex(size - 1);
                                    int row = gridView.getRowIndex(moveIndex);

                                    if (row > lastRow) {
                                        setFocus(0);
                                        return true;
                                    } else if (moveIndex >= size) {
                                        setFocus(size - 1);
                                        return true;
                                    }
                                    break;

                                case KeyEvent.KEYCODE_DPAD_RIGHT:
                                    if ((index + 1) % 5 == 0) {
                                        if (btnDelete.isEnabled()) {
                                            btnDelete.requestFocus();
                                        } else {
                                            btnSelectAll.requestFocus();
                                        }
                                    }
                                    break;
                            }

                            return false;
                        }
                    }, new PurchasedItemPresenter.OnFocusListener() {
                @Override
                public void onFocus(Object item) {
                    if (item instanceof PurchasedContent) {
                        PurchasedContent content = (PurchasedContent) item;
                        setContentInfo(content);
                    }
                }
            }
            );
            gridObjectAdapter = new ArrayObjectAdapter(presenter);
            gridObjectAdapter.addAll(0, purchasedList);

            gridBridgeAdapter = new PurchasedListBridgeAdapter(new PurchasedListBridgeAdapter.OnBindListener() {
                @Override
                public void onBind(int index) {
                }
            });

            gridBridgeAdapter.setAdapter(gridObjectAdapter);
            gridView.setAdapter(gridBridgeAdapter);

//            countView.setText("(" + purchasedList.size() + ")");

            emptyVodLayer.setVisibility(View.GONE);
            btnEdit.setVisibility(View.GONE);

//            countView.setVisibility(View.VISIBLE);
            btnGroupLayer.setVisibility(View.VISIBLE);
            infoLayer.setVisibility(View.VISIBLE);
            contentLayer.setVisibility(View.VISIBLE);

            setEnableDeleteButton();

        } else {
            countView.setVisibility(View.GONE);
            contentLayer.setVisibility(View.GONE);
            emptyVodLayer.setVisibility(View.VISIBLE);
        }
    }

    private boolean checkRemovalChanges() {
        if (hiddenList.size() != hiddenListOrigin.size()) {
            return true;
        } else {
            for (PurchasedContent candidateItem : hiddenList) {
                boolean existed = false;
                for (PurchasedContent currentItem : hiddenListOrigin) {
                    if (currentItem.getContentGroupId().equals(candidateItem.getContentGroupId()) &&
                            currentItem.getOfferId().equals(candidateItem.getOfferId())) {
                        existed = true;
                    }
                }
                if (existed) {
                    return true;
                }
            }
        }
        return false;
    }

    private int getTotalRemovableSize() {
        int totalSize = 0;
        if (currentProfileId != null) {
            for (int i = 0; i < purchasedList.size(); i++) {
                PurchasedContent purchasedItem = purchasedList.get(i);
                if (currentProfileId.equals(purchasedItem.getProfileId())) {
                    totalSize++;
                }
            }
        }
        return totalSize;
    }

    private void setEnableDeleteButton() {
        if (checkRemovalChanges()) {
            btnDelete.setEnabled(true);
        } else {
            btnDelete.setEnabled(false);
        }
    }

    private void updateGridView(PurchasedContent item, CheckBox checkBox) {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateGridView");
        }
        boolean isHidden = !checkBox.isChecked();
        checkBox.setChecked(isHidden);

        int idx = purchasedList.indexOf(item);
        if (idx >= 0) {
            item.setHidden(isHidden);
            gridObjectAdapter.replace(idx, item);
        }
    }

    private void updateHiddenList(PurchasedContent item) {
        PurchasedContent movie = item;
        if (hiddenList.contains(movie)) {
            hiddenList.remove(movie);
        } else {
//            if (hiddenList.size() >= LIMIT_SIZE) {
//                // show toast message
//                return false;
//            }
            hiddenList.add(movie);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "updateHiddenList: hiddenList=" + hiddenList.size());
        }

        setEnableDeleteButton();
    }

    private void putPurchasedHiddenList(View view) {
        String[] contentIds = new String[hiddenList.size()];
        int i = 0;
        for (PurchasedContent content : hiddenList) {
            contentIds[i] = content.getPurchaseId();
            i++;
        }

//        for(i=0; i<contentIds.length; i++){
//            Log.d(TAG, "putPurchasedHiddenList: contentIds["+i+"]="+contentIds[i]);
//        }

        UserDataManager userDataManager = new UserDataManager();
        userDataManager.putPurchaseListHidden(
            AccountManager.getInstance().getSaId(),
            AccountManager.getInstance().getProfileId(),
            contentIds,
            new MbsDataProvider<String, Boolean>() {
                @Override
                public void needLoading(boolean loading) {
                    if (loading) {
//                            showProgress();
                    } else {
//                            hideProgress();
                    }
                }

                @Override
                public void onFailed(int key) {

                }

                @Override
                public void onSuccess(String id, Boolean result) {
                    if (comfirmPopup != null) {
                        comfirmPopup.dismiss();
                    }
                    ((PurchasedListDialog) getParentFragment()).showView(PurchasedListDialog.TYPE_LIST, null, null);
                    JasminToast.makeToast(view.getContext(), R.string.purchased_delete_toast_desc);
                }

                @Override
                public void onError(String errorCode, String message) {
                    ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                    errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                }
            });
    }

}
