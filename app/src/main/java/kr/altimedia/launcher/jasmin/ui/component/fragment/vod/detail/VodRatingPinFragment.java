/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;
import com.altimedia.util.Log;

public class VodRatingPinFragment extends Fragment {
    public static final String CLASS_NAME = VodRatingPinFragment.class.getName();
    private final String TAG = VodRatingPinFragment.class.getSimpleName();
    private final AccountManager accountManager = AccountManager.getInstance();
    private final UserDataManager userDataManager = new UserDataManager();
    private TextView cancelButton;
    private PasswordView password;
    private MbsDataTask checkPinTask;

    private onCompleteCheckPinListener onCompleteCheckPinListener;

    public VodRatingPinFragment() {
        super(R.layout.fragment_vod_rating_pin);
    }

    public static VodRatingPinFragment newInstance() {
        return new VodRatingPinFragment();
    }

    public void setOnCompleteCheckPinListener(VodRatingPinFragment.onCompleteCheckPinListener onCompleteCheckPinListener) {
        this.onCompleteCheckPinListener = onCompleteCheckPinListener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        cancelButton = view.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onCompleteCheckPinListener.onCompleteCheckPin(false);
                    }
                });
        initPasswordView(view);
    }

    private void initPasswordView(View view) {
        password = view.findViewById(R.id.pin);
        password.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyInputChange, isFull : " + isFull + ", input : " + input);
                }

                if (isFull) {
                    checkPinCode(input);
                }
            }
        });
    }

    private void checkPinCode(String password) {
        String accountId = accountManager.getSaId();
        String profileId = accountManager.getProfileId();
        checkPinTask = userDataManager.checkAccountPinCode(
                accountId, profileId, password,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, needLoading, loading : " + loading);
                        }

                        onCompleteCheckPinListener.setProgress(loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onFailed");
                        }

                        setPinError();
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onSuccess, result : " + result);
                        }

                        if (result) {
                            onCompleteCheckPinListener.onCompleteCheckPin(true);
                        } else {
                            setPinError();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodRatingPinFragment.this.getContext();
                    }
                });
    }

    private void setPinError() {
        getView().findViewById(R.id.description).setVisibility(View.VISIBLE);
        password.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        onCompleteCheckPinListener = null;
        if (checkPinTask != null) {
            checkPinTask.cancel(true);
        }
    }

    public interface onCompleteCheckPinListener {
        void setProgress(boolean isShow);

        void onCompleteCheckPin(boolean isSuccess);
    }
}
