/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;


import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.leanback.widget.DetailsParallax;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;

import java.lang.ref.WeakReference;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.TransitionListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.util.StateMachine;
import kr.altimedia.launcher.jasmin.ui.view.util.TransitionHelper;


public class DetailsSupportFragment extends BaseSupportFragment {
    private static final String TAG = "DetailsSupportFragment";
    private static final boolean DEBUG = false;

    final StateMachine.State STATE_SET_ENTRANCE_START_STATE = new StateMachine.State("STATE_SET_ENTRANCE_START_STATE") {
        @Override
        public void run() {
            mRowsSupportFragment.setEntranceTransitionState(false);
        }
    };

    private final StateMachine.State STATE_ENTER_TRANSITION_INIT = new StateMachine.State("STATE_ENTER_TRANSIITON_INIT");

    private void switchToVideoBeforeVideoSupportFragmentCreated() {
        // if the video fragment is not ready: immediately fade out covering drawable,
        // hide title and mark mPendingFocusOnVideo and set focus on it later.
        mDetailsBackgroundController.switchToVideoBeforeCreate();
        showTitle(false);
        mPendingFocusOnVideo = true;
        slideOutGridView();
    }

    private final StateMachine.State STATE_SWITCH_TO_VIDEO_IN_ON_CREATE = new StateMachine.State("STATE_SWITCH_TO_VIDEO_IN_ON_CREATE",
            false, false) {
        @Override
        public void run() {
            switchToVideoBeforeVideoSupportFragmentCreated();
        }
    };

    private final StateMachine.State STATE_ENTER_TRANSITION_CANCEL = new StateMachine.State("STATE_ENTER_TRANSITION_CANCEL",
            false, false) {
        @Override
        public void run() {
            if (mWaitEnterTransitionTimeout != null) {
                mWaitEnterTransitionTimeout.mRef.clear();
            }
            // clear the activity enter/sharedElement transition, return transitions are kept.
            // keep the return transitions and clear enter transition
            if (getActivity() != null) {
                Window window = getActivity().getWindow();
                Object returnTransition = TransitionHelper.getReturnTransition(window);
                Object sharedReturnTransition = TransitionHelper
                        .getSharedElementReturnTransition(window);
                TransitionHelper.setEnterTransition(window, null);
                TransitionHelper.setSharedElementEnterTransition(window, null);
                TransitionHelper.setReturnTransition(window, returnTransition);
                TransitionHelper.setSharedElementReturnTransition(window, sharedReturnTransition);
            }
        }
    };

    private final StateMachine.State STATE_ENTER_TRANSITION_COMPLETE = new StateMachine.State("STATE_ENTER_TRANSIITON_COMPLETE",
            true, false);

    private final StateMachine.State STATE_ENTER_TRANSITION_ADDLISTENER = new StateMachine.State("STATE_ENTER_TRANSITION_PENDING") {
        @Override
        public void run() {
            Object transition = TransitionHelper.getEnterTransition(getActivity().getWindow());
            TransitionHelper.addTransitionListener(transition, mEnterTransitionListener);
        }
    };

    private final StateMachine.State STATE_ENTER_TRANSITION_PENDING = new StateMachine.State("STATE_ENTER_TRANSITION_PENDING") {
        @Override
        public void run() {
            if (mWaitEnterTransitionTimeout == null) {
                new WaitEnterTransitionTimeout(DetailsSupportFragment.this);
            }
        }
    };

    /**
     * Start this task when first DetailsOverviewRow is created, if there is no entrance transition
     * started, it will clear PF_ENTRANCE_TRANSITION_PENDING.
     */
    private static class WaitEnterTransitionTimeout implements Runnable {
        static final long WAIT_ENTERTRANSITION_START = 200;

        final WeakReference<DetailsSupportFragment> mRef;

        WaitEnterTransitionTimeout(DetailsSupportFragment f) {
            mRef = new WeakReference<>(f);
            f.getView().postDelayed(this, WAIT_ENTERTRANSITION_START);
        }

        @Override
        public void run() {
            DetailsSupportFragment f = mRef.get();
            if (f != null) {
                f.mStateMachine.fireEvent(f.EVT_ENTER_TRANSIITON_DONE);
            }
        }
    }

    final StateMachine.State STATE_ON_SAFE_START = new StateMachine.State("STATE_ON_SAFE_START") {
        @Override
        public void run() {
            onSafeStart();
        }
    };

    final StateMachine.Event EVT_ONSTART = new StateMachine.Event("onStart");

    final StateMachine.Event EVT_NO_ENTER_TRANSITION = new StateMachine.Event("EVT_NO_ENTER_TRANSITION");

    final StateMachine.Event EVT_DETAILS_ROW_LOADED = new StateMachine.Event("onFirstRowLoaded");

    final StateMachine.Event EVT_ENTER_TRANSIITON_DONE = new StateMachine.Event("onEnterTransitionDone");

    final StateMachine.Event EVT_SWITCH_TO_VIDEO = new StateMachine.Event("switchToVideo");

    @Override
    protected void createStateMachineStates() {
        super.createStateMachineStates();
        mStateMachine.addState(STATE_SET_ENTRANCE_START_STATE);
        mStateMachine.addState(STATE_ON_SAFE_START);
        mStateMachine.addState(STATE_SWITCH_TO_VIDEO_IN_ON_CREATE);
        mStateMachine.addState(STATE_ENTER_TRANSITION_INIT);
        mStateMachine.addState(STATE_ENTER_TRANSITION_ADDLISTENER);
        mStateMachine.addState(STATE_ENTER_TRANSITION_CANCEL);
        mStateMachine.addState(STATE_ENTER_TRANSITION_PENDING);
        mStateMachine.addState(STATE_ENTER_TRANSITION_COMPLETE);
    }

    @Override
    protected void createStateMachineTransitions() {
        super.createStateMachineTransitions();
        /**
         * Part 1: Processing enter transitions after fragment.onCreate
         */
        mStateMachine.addTransition(STATE_START, STATE_ENTER_TRANSITION_INIT, EVT_ON_CREATE);
        // if transition is not supported, skip to complete
        mStateMachine.addTransition(STATE_ENTER_TRANSITION_INIT, STATE_ENTER_TRANSITION_COMPLETE,
                COND_TRANSITION_NOT_SUPPORTED);
        // if transition is not set on Activity, skip to complete
        mStateMachine.addTransition(STATE_ENTER_TRANSITION_INIT, STATE_ENTER_TRANSITION_COMPLETE,
                EVT_NO_ENTER_TRANSITION);
        // if switchToVideo is called before EVT_ON_CREATEVIEW, clear enter transition and skip to
        // complete.
        mStateMachine.addTransition(STATE_ENTER_TRANSITION_INIT, STATE_ENTER_TRANSITION_CANCEL,
                EVT_SWITCH_TO_VIDEO);
        mStateMachine.addTransition(STATE_ENTER_TRANSITION_CANCEL, STATE_ENTER_TRANSITION_COMPLETE);
        // once after onCreateView, we cannot skip the enter transition, add a listener and wait
        // it to finish
        mStateMachine.addTransition(STATE_ENTER_TRANSITION_INIT, STATE_ENTER_TRANSITION_ADDLISTENER,
                EVT_ON_CREATEVIEW);
        // when enter transition finishes, go to complete, however this might never happen if
        // the activity is not giving transition options in startActivity, there is no API to query
        // if this activity is started in a enter transition mode. So we rely on a timer below:
        mStateMachine.addTransition(STATE_ENTER_TRANSITION_ADDLISTENER,
                STATE_ENTER_TRANSITION_COMPLETE, EVT_ENTER_TRANSIITON_DONE);
        // we are expecting app to start delayed enter transition shortly after details row is
        // loaded, so create a timer and wait for enter transition start.
        mStateMachine.addTransition(STATE_ENTER_TRANSITION_ADDLISTENER,
                STATE_ENTER_TRANSITION_PENDING, EVT_DETAILS_ROW_LOADED);
        // if enter transition not started in the timer, skip to DONE, this can be also true when
        // startActivity is not giving transition option.
        mStateMachine.addTransition(STATE_ENTER_TRANSITION_PENDING, STATE_ENTER_TRANSITION_COMPLETE,
                EVT_ENTER_TRANSIITON_DONE);

        /**
         * Part 2: modification to the entrance transition defined in BaseSupportFragment
         */
        // Must finish enter transition before perform entrance transition.
        mStateMachine.addTransition(STATE_ENTER_TRANSITION_COMPLETE, STATE_ENTRANCE_PERFORM);
        // Calling switch to video would hide immediately and skip entrance transition
        mStateMachine.addTransition(STATE_ENTRANCE_INIT, STATE_SWITCH_TO_VIDEO_IN_ON_CREATE,
                EVT_SWITCH_TO_VIDEO);
        mStateMachine.addTransition(STATE_SWITCH_TO_VIDEO_IN_ON_CREATE, STATE_ENTRANCE_COMPLETE);
        // if the entrance transition is skipped to complete by COND_TRANSITION_NOT_SUPPORTED, we
        // still need to do the switchToVideo.
        mStateMachine.addTransition(STATE_ENTRANCE_COMPLETE, STATE_SWITCH_TO_VIDEO_IN_ON_CREATE,
                EVT_SWITCH_TO_VIDEO);

        // for once the view is created in onStart and prepareEntranceTransition was called, we
        // could setEntranceStartState:
        mStateMachine.addTransition(STATE_ENTRANCE_ON_PREPARED,
                STATE_SET_ENTRANCE_START_STATE, EVT_ONSTART);

        /**
         * Part 3: onSafeStart()
         */
        // for onSafeStart: the condition is onStart called, entrance transition complete
        mStateMachine.addTransition(STATE_START, STATE_ON_SAFE_START, EVT_ONSTART);
        mStateMachine.addTransition(STATE_ENTRANCE_COMPLETE, STATE_ON_SAFE_START);
        mStateMachine.addTransition(STATE_ENTER_TRANSITION_COMPLETE, STATE_ON_SAFE_START);
    }

    private class SetSelectionRunnable implements Runnable {
        int mPosition;
        boolean mSmooth = true;

        SetSelectionRunnable() {
        }

        @Override
        public void run() {
            if (mRowsSupportFragment == null) {
                return;
            }
            mRowsSupportFragment.setSelectedPosition(mPosition, mSmooth);
        }
    }

    TransitionListener mEnterTransitionListener = new TransitionListener() {
        @Override
        public void onTransitionStart(Object transition) {
            if (mWaitEnterTransitionTimeout != null) {
                // cancel task of WaitEnterTransitionTimeout, we will clearPendingEnterTransition
                // when transition finishes.
                mWaitEnterTransitionTimeout.mRef.clear();
            }
        }

        @Override
        public void onTransitionCancel(Object transition) {
            mStateMachine.fireEvent(EVT_ENTER_TRANSIITON_DONE);
        }

        @Override
        public void onTransitionEnd(Object transition) {
            mStateMachine.fireEvent(EVT_ENTER_TRANSIITON_DONE);
        }
    };

    TransitionListener mReturnTransitionListener = new TransitionListener() {
        @Override
        public void onTransitionStart(Object transition) {
            onReturnTransitionStart();
        }
    };

    protected BrowseFrameLayout mRootView;
    View mBackgroundView;
    Drawable mBackgroundDrawable;
    protected Fragment mVideoSupportFragment;
    DetailsParallax mDetailsParallax;
    protected RowsSupportFragment mRowsSupportFragment;

    ObjectAdapter mAdapter;
    int mContainerListAlignTop;
    BaseOnItemViewSelectedListener mExternalOnItemViewSelectedListener;
    BaseOnItemViewClickedListener mOnItemViewClickedListener;
    protected DetailsSupportFragmentBackgroundController mDetailsBackgroundController;

    // A temporarily flag when switchToVideo() is called in onCreate(), if mPendingFocusOnVideo is
    // true, we will focus to VideoSupportFragment immediately after video fragment's view is created.
    boolean mPendingFocusOnVideo = false;

    WaitEnterTransitionTimeout mWaitEnterTransitionTimeout;

    Object mSceneAfterEntranceTransition;

    final SetSelectionRunnable mSetSelectionRunnable = new SetSelectionRunnable();

    final BaseOnItemViewSelectedListener<Object> mOnItemViewSelectedListener =
            new BaseOnItemViewSelectedListener<Object>() {
                @Override
                public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item,
                                           Presenter.ViewHolder rowViewHolder, Object row) {
                    int position = mRowsSupportFragment.getVerticalGridView().getSelectedPosition();
                    @SuppressLint("RestrictedApi") int subposition = mRowsSupportFragment.getVerticalGridView().getSelectedSubPosition();
                    if (DEBUG) Log.v(TAG, "row selected position " + position
                            + " subposition " + subposition);
                    onRowSelected(position, subposition);
                    if (mExternalOnItemViewSelectedListener != null) {
                        mExternalOnItemViewSelectedListener.onItemSelected(itemViewHolder, item,
                                rowViewHolder, row);
                    }
                }
            };

    /**
     * Sets the list of rows for the fragment.
     */
    public void setAdapter(ObjectAdapter adapter) {
        mAdapter = adapter;
/*        Presenter[] presenters = adapter.getPresenterSelector().getPresenters();
        if (presenters != null) {
            for (int i = 0; i < presenters.length; i++) {
                setupPresenter(presenters[i]);
            }
        } else {
            Log.e(TAG, "PresenterSelector.getPresenters() not implemented");
        }*/
        if (mRowsSupportFragment != null) {
            mRowsSupportFragment.setAdapter(adapter);
        }
    }

    /**
     * Returns the list of rows.
     */
    public ObjectAdapter getAdapter() {
        return mAdapter;
    }

    /**
     * Sets an item selection listener.
     */
    public void setOnItemViewSelectedListener(BaseOnItemViewSelectedListener listener) {
        mExternalOnItemViewSelectedListener = listener;
    }

    /**
     * Sets an item clicked listener.
     */
    public void setOnItemViewClickedListener(BaseOnItemViewClickedListener listener) {
        if (mOnItemViewClickedListener != listener) {
            mOnItemViewClickedListener = listener;
            if (mRowsSupportFragment != null) {
                mRowsSupportFragment.setOnItemViewClickedListener(listener);
            }
        }
    }

    protected void setRowDivider(int resourceId) {
        if (mRowsSupportFragment != null) {
            mRowsSupportFragment.setRowDivider(resourceId);
        }
    }

    /**
     * Returns the item clicked listener.
     */
    public BaseOnItemViewClickedListener getOnItemViewClickedListener() {
        return mOnItemViewClickedListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContainerListAlignTop = 0
        /*getResources().getDimensionPixelSize(R.dimen.lb_details_rows_align_top)*/;

        FragmentActivity activity = getActivity();
        if (activity != null) {
            Object transition = TransitionHelper.getEnterTransition(activity.getWindow());
            if (transition == null) {
                mStateMachine.fireEvent(EVT_NO_ENTER_TRANSITION);
            }
            transition = TransitionHelper.getReturnTransition(activity.getWindow());
            if (transition != null) {
                TransitionHelper.addTransitionListener(transition, mReturnTransitionListener);
            }
        } else {
            mStateMachine.fireEvent(EVT_NO_ENTER_TRANSITION);
        }
    }

    @Override
    public void onDestroy() {
        Object enterTransition = TransitionHelper.getEnterTransition(getActivity().getWindow());
        TransitionHelper.removeTransitionListener(enterTransition, mEnterTransitionListener);

        Object returnTransition  = TransitionHelper.getReturnTransition(getActivity().getWindow());
        TransitionHelper.removeTransitionListener(returnTransition, mReturnTransitionListener);

        super.onDestroy();
    }

    private int resourceId = R.layout.lb_details_fragment;

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    protected int getResourceId() {
        return resourceId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = (BrowseFrameLayout) inflater.inflate(
                getResourceId(), container, false);
        mBackgroundView = mRootView.findViewById(R.id.details_background_view);
        if (mBackgroundView != null) {
            mBackgroundView.setBackground(mBackgroundDrawable);
        }
        mRowsSupportFragment = (RowsSupportFragment) getChildFragmentManager().findFragmentById(
                R.id.details_rows_dock);
        if (mRowsSupportFragment == null) {
            mRowsSupportFragment = new RowsSupportFragment();
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.details_rows_dock, mRowsSupportFragment).commit();
        }
        installTitleView(inflater, mRootView, savedInstanceState);
        mRowsSupportFragment.setAdapter(mAdapter);
        mRowsSupportFragment.setOnItemViewSelectedListener(mOnItemViewSelectedListener);
        mRowsSupportFragment.setOnItemViewClickedListener(mOnItemViewClickedListener);


        mSceneAfterEntranceTransition = TransitionHelper.createScene(mRootView, new Runnable() {
            @Override
            public void run() {
                mRowsSupportFragment.setEntranceTransitionState(true);
            }
        });

        setupDpadNavigation();
        return mRootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        detachedDpadNavigation();
        detachedRowViewListener();


        mRootView = null;
        mBackgroundView = null;
        mSceneAfterEntranceTransition = null;
        mVideoSupportFragment = null;

        setTitleView(null);

        mRowsSupportFragment.setAdapter(null);
        mRowsSupportFragment = null;
    }

    /**
     * @deprecated override {@link #onInflateTitleView(LayoutInflater, ViewGroup, Bundle)} instead.
     */
    @Deprecated
    protected View inflateTitle(LayoutInflater inflater, ViewGroup parent,
                                Bundle savedInstanceState) {
        return super.onInflateTitleView(inflater, parent, savedInstanceState);
    }

    @Override
    public View onInflateTitleView(LayoutInflater inflater, ViewGroup parent,
                                   Bundle savedInstanceState) {
        return inflateTitle(inflater, parent, savedInstanceState);
    }

    void setVerticalGridViewLayout(VerticalGridView listview) {
        // align the top edge of item to a fixed position
/*        listview.setItemAlignmentOffset(-mContainerListAlignTop);
        listview.setItemAlignmentOffsetPercent(VerticalGridView.ITEM_ALIGN_OFFSET_PERCENT_DISABLED);
        listview.setWindowAlignmentOffset(0);
        listview.setWindowAlignmentOffsetPercent(VerticalGridView.WINDOW_ALIGN_OFFSET_PERCENT_DISABLED);
        listview.setWindowAlignment(VerticalGridView.WINDOW_ALIGN_NO_EDGE);*/
    }

    protected VerticalGridView getVerticalGridView() {
        return mRowsSupportFragment == null ? null : mRowsSupportFragment.getVerticalGridView();
    }

    /**
     * Gets embedded RowsSupportFragment showing multiple rows for DetailsSupportFragment.  If view of
     * DetailsSupportFragment is not created, the method returns null.
     *
     * @return Embedded RowsSupportFragment showing multiple rows for DetailsSupportFragment.
     */
    public RowsSupportFragment getRowsSupportFragment() {
        return mRowsSupportFragment;
    }

    /**
     * Setup dimensions that are only meaningful when the child Fragments are inside
     * DetailsSupportFragment.
     */
    private void setupChildFragmentLayout() {
        setVerticalGridViewLayout(mRowsSupportFragment.getVerticalGridView());
    }

    /**
     * Sets the selected row position with smooth animation.
     */
    public void setSelectedPosition(int position) {
        setSelectedPosition(position, true);
    }

    /**
     * Sets the selected row position.
     */
    public void setSelectedPosition(int position, boolean smooth) {
        mSetSelectionRunnable.mPosition = position;
        mSetSelectionRunnable.mSmooth = smooth;
        if (getView() != null && getView().getHandler() != null) {
            getView().getHandler().post(mSetSelectionRunnable);
        }
    }

    public void switchToVideo() {
        if (mVideoSupportFragment != null && mVideoSupportFragment.getView() != null) {
            mVideoSupportFragment.getView().requestFocus();
        } else {
            mStateMachine.fireEvent(EVT_SWITCH_TO_VIDEO);
        }
    }

    public void switchToRows() {
        mPendingFocusOnVideo = false;
        VerticalGridView verticalGridView = getVerticalGridView();
        if (verticalGridView != null && verticalGridView.getChildCount() > 0) {
            verticalGridView.requestFocus();
        }
    }

    public final Fragment findOrCreateVideoSupportFragment() {
        if (mVideoSupportFragment != null) {
            return mVideoSupportFragment;
        }
        Fragment fragment = getChildFragmentManager()
                .findFragmentById(R.id.video_surface_container);
        if (fragment == null && mDetailsBackgroundController != null) {
            FragmentTransaction ft2 = getChildFragmentManager().beginTransaction();
            ft2.add(androidx.leanback.R.id.video_surface_container,
                    fragment = mDetailsBackgroundController.onCreateVideoSupportFragment());
            ft2.commit();
            if (mPendingFocusOnVideo) {
                // wait next cycle for Fragment view created so we can focus on it.
                // This is a bit hack eventually we will do commitNow() which get view immediately.
                getView().post(new Runnable() {
                    @Override
                    public void run() {
                        if (getView() != null) {
                            switchToVideo();
                        }
                        mPendingFocusOnVideo = false;
                    }
                });
            }
        }
        mVideoSupportFragment = fragment;
        return mVideoSupportFragment;
    }

    @SuppressLint("RestrictedApi")
    void onRowSelected(int selectedPosition, int selectedSubPosition) {
        ObjectAdapter adapter = getAdapter();
        if ((mRowsSupportFragment != null && mRowsSupportFragment.getView() != null
                && mRowsSupportFragment.getView().hasFocus() && !mPendingFocusOnVideo)
                && (adapter == null || adapter.size() == 0
                || (getVerticalGridView().getSelectedPosition() == 0
                && getVerticalGridView().getSelectedSubPosition() == 0))) {
            showTitle(true);
        } else {
            showTitle(false);
        }
        if (adapter != null && adapter.size() > selectedPosition) {
            final VerticalGridView gridView = getVerticalGridView();
            final int count = gridView.getChildCount();
            if (count > 0) {
                mStateMachine.fireEvent(EVT_DETAILS_ROW_LOADED);
            }
            for (int i = 0; i < count; i++) {
                ItemBridgeAdapter.ViewHolder bridgeViewHolder = (ItemBridgeAdapter.ViewHolder)
                        gridView.getChildViewHolder(gridView.getChildAt(i));
                RowPresenter rowPresenter = (RowPresenter) bridgeViewHolder.getPresenter();
                onSetRowStatus(rowPresenter,
                        rowPresenter.getRowViewHolder(bridgeViewHolder.getViewHolder()),
                        bridgeViewHolder.getAdapterPosition(),
                        selectedPosition, selectedSubPosition);
            }
        }
    }

    /**
     * Called when onStart and enter transition (postponed/none postponed) and entrance transition
     * are all finished.
     */
    void onSafeStart() {
        if (mDetailsBackgroundController != null) {
            mDetailsBackgroundController.onStart();
        }
    }

    void onReturnTransitionStart() {
        if (mDetailsBackgroundController != null) {
            // first disable parallax effect that auto-start PlaybackGlue.
            boolean isVideoVisible = mDetailsBackgroundController.disableVideoParallax();
            // if video is not visible we can safely remove VideoSupportFragment,
            // otherwise let video playing during return transition.
            if (!isVideoVisible && mVideoSupportFragment != null) {
                FragmentTransaction ft2 = getChildFragmentManager().beginTransaction();
                ft2.remove(mVideoSupportFragment);
                ft2.commit();
                mVideoSupportFragment = null;
            }
        }
    }

    @Override
    public void onStop() {
        if (mDetailsBackgroundController != null) {
            mDetailsBackgroundController.onStop();
        }
        super.onStop();
    }

    protected void onSetRowStatus(RowPresenter presenter, RowPresenter.ViewHolder viewHolder, int
            adapterPosition, int selectedPosition, int selectedSubPosition) {
    }

    @Override
    public void onStart() {
        super.onStart();

        setupChildFragmentLayout();
        mStateMachine.fireEvent(EVT_ONSTART);
        if (mDetailsParallax != null) {
            mDetailsParallax.setRecyclerView(mRowsSupportFragment.getVerticalGridView());
        }
        if (mPendingFocusOnVideo) {
            slideOutGridView();
        } else if (!getView().hasFocus()) {
            initFocus();
        }
    }

    protected void initFocus() {
        Log.d(TAG, "init focus...");
        mRowsSupportFragment.getVerticalGridView().requestFocus();
    }

    @Override
    protected Object createEntranceTransition() {
        return TransitionHelper.loadTransition(getContext(),
                R.transition.lb_details_enter_transition);
    }

    @Override
    protected void runEntranceTransition(Object entranceTransition) {
        TransitionHelper.runTransition(mSceneAfterEntranceTransition, entranceTransition);
    }

    @Override
    protected void onEntranceTransitionEnd() {
        mRowsSupportFragment.onTransitionEnd();
    }

    @Override
    protected void onEntranceTransitionPrepare() {
        mRowsSupportFragment.onTransitionPrepare();
    }

    @Override
    protected void onEntranceTransitionStart() {
        mRowsSupportFragment.onTransitionStart();
    }

    public DetailsParallax getParallax() {
        if (mDetailsParallax == null) {
            mDetailsParallax = new DetailsParallax();
            if (mRowsSupportFragment != null && mRowsSupportFragment.getView() != null) {
                mDetailsParallax.setRecyclerView(mRowsSupportFragment.getVerticalGridView());
            }
        }

        return mDetailsParallax;
    }

    public void setBackgroundDrawable(Drawable drawable) {
        Log.d(TAG, "setBackgroundDrawable ... : " + drawable);
        if (mBackgroundView != null) {
            mBackgroundView.setBackground(drawable);
        }
        mBackgroundDrawable = drawable;
    }


    /**
     * This method does the following
     * <ul>
     * <li>sets up focus search handling logic in the root view to enable transitioning between
     * half screen/full screen/no video mode.</li>
     *
     * <li>Sets up the key listener in the root view to intercept events like UP/DOWN and
     * transition to appropriate mode like half/full screen video.</li>
     * </ul>
     */
    protected void setupDpadNavigation() {
        setupRequestFocusInDescendants();
        setupDispatchKeyListener();
        setupFocusSearchListener();
    }

    protected void detachedDpadNavigation(){
        mRootView.setOnChildFocusListener(null);
        mRootView.setOnDispatchKeyListener(null);
        mRootView.setOnFocusSearchListener(null);
    }

    protected void detachedRowViewListener(){
        mRowsSupportFragment.setOnItemViewSelectedListener(null);
        mRowsSupportFragment.setOnItemViewClickedListener(null);
    }

    protected void setupRequestFocusInDescendants() {
        mRootView.setOnChildFocusListener(new BrowseFrameLayout.OnChildFocusListener() {

            @Override
            public boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
                return true;
            }

            @Override
            public void onRequestChildFocus(View child, View focused) {
                if (child != mRootView.getFocusedChild()) {
                    if (child.getId() == R.id.details_fragment_root) {
                        if (!mPendingFocusOnVideo) {
                            slideInGridView();
                            showTitle(true);
                        }
                    } else if (child.getId() == R.id.video_surface_container) {
                        slideOutGridView();
                        showTitle(false);
                    } else {
                        showTitle(true);
                    }
                }
            }
        });
    }

    protected void setupDispatchKeyListener() {
        // If we press BACK on remote while in full screen video mode, we should
        // transition back to half screen video playback mode.
        mRootView.setOnDispatchKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // This is used to check if we are in full screen video mode. This is somewhat
                // hacky and relies on the behavior of the video helper class to update the
                // focusability of the video surface view.
                if (mVideoSupportFragment != null && mVideoSupportFragment.getView() != null
                        && mVideoSupportFragment.getView().hasFocus()) {
                    if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
                        if (getVerticalGridView().getChildCount() > 0) {
                            getVerticalGridView().requestFocus();
                            return true;
                        }
                    }
                }

                return false;
            }
        });
    }

    protected void setupFocusSearchListener() {
        mRootView.setOnFocusSearchListener(new BrowseFrameLayout.OnFocusSearchListener() {
            @Override
            public View onFocusSearch(View focused, int direction) {
                Log.d(TAG, "setOnFocusSearchListener, view : " + focused);
                if (mRowsSupportFragment.getVerticalGridView() != null
                        && mRowsSupportFragment.getVerticalGridView().hasFocus()) {
                    if (direction == View.FOCUS_UP) {
                        if (mDetailsBackgroundController != null
                                && mDetailsBackgroundController.canNavigateToVideoSupportFragment()
                                && mVideoSupportFragment != null && mVideoSupportFragment.getView() != null) {
                            return mVideoSupportFragment.getView();
                        } else if (getTitleView() != null && getTitleView().hasFocusable()) {
                            return getTitleView();
                        }
                    }
                } else if (getTitleView() != null && getTitleView().hasFocus()) {
                    if (direction == View.FOCUS_DOWN) {
                        if (mRowsSupportFragment.getVerticalGridView() != null) {
                            return mRowsSupportFragment.getVerticalGridView();
                        }
                    }
                }
                return focused;
            }
        });
    }

    /**
     * Slides vertical grid view (displaying media item details) out of the screen from below.
     */
    void slideOutGridView() {
        if (getVerticalGridView() != null) {
            getVerticalGridView().animateOut();
        }
    }

    void slideInGridView() {
        if (getVerticalGridView() != null) {
            getVerticalGridView().animateIn();
        }
    }
}

