/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service.control;

import android.content.Context;
import android.view.KeyEvent;

import com.altimedia.cwmplibrary.common.CWMPMultiObject;
import com.altimedia.cwmplibrary.common.CWMPParameter;
import com.altimedia.cwmplibrary.common.ModelObject;
import com.altimedia.cwmplibrary.common.method.InformCallback;
import com.altimedia.cwmplibrary.common.method.SetParameterListener;
import com.altimedia.cwmplibrary.ttbb.object.InternetGatewayDevice;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.deviceinfo.X_TTBB_RemoconHotKey;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.deviceinfo.X_TTBB_RemoconHotKeys;
import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.cwmp.service.CWMPController;

/**
 * HotKeyInformControl
 */
public class HotKeyInformControl {
    private final String CLASS_NAME = HotKeyInformControl.class.getName();
    private final String TAG = HotKeyInformControl.class.getSimpleName();

    private final String[] CWMP_HOT_KEYs = new String[]{
            "Home",
            "MIC",
            "EPG",
            "MonoMAX",
            "YouTube",
            "AppMenu"
    };

    private final int[] VK_HOT_KEYs = new int[]{
            KeyEvent.KEYCODE_HOME,
            -1,
            KeyEvent.KEYCODE_GUIDE,
            KeyEvent.KEYCODE_BUTTON_2,
            KeyEvent.KEYCODE_BUTTON_3,
            KeyEvent.KEYCODE_ALL_APPS
    };
    private final long DEFAULT_INTERVAL = 8*60*60*1000;

    private InternetGatewayDevice root;
    private Context context;

    private InformSender informSender;
    private Thread informSendThread;

    private boolean informEnabled = true; // default
    private long informInterval = DEFAULT_INTERVAL;

    public HotKeyInformControl(){
    }

    public void dispose() {
        stopInformSender();

        root = null;
        context = null;
    }

    public void init(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "init");
        }

        this.context = context;
    }

    public void start(InternetGatewayDevice root){
        if (Log.INCLUDE) {
            Log.d(TAG, "start");
        }
        this.root = root;

        if(this.root == null){
            if (Log.INCLUDE) {
                Log.d(TAG, "start: root is NOT available");
            }
            return;
        }

        // #01 index 고정형
        for(final ModelObject object : root.deviceInfo.getChilds()) {
            if (object instanceof CWMPMultiObject && object instanceof X_TTBB_RemoconHotKeys) {
                try {
                    X_TTBB_RemoconHotKeys x_ttbb_remoconHotKeys = ((X_TTBB_RemoconHotKeys) object);

                    informEnabled = x_ttbb_remoconHotKeys.RemoconHotKeyInformEnable.getValue() == 1;
                    informInterval = getInformInterval(x_ttbb_remoconHotKeys);

                    int size = x_ttbb_remoconHotKeys.getX_TTBB_RemoconHotKeyIndexes().size();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "start: informEnabled=" + informEnabled + ", informInterval=" + informInterval + ", hotKeyIndexes=" + size);
                    }
                    int index = 1;
                    for (final String keyCode : CWMP_HOT_KEYs) {
                        X_TTBB_RemoconHotKey x_ttbb_remoconHotKey = getX_TTBB_RemoconHotKey(x_ttbb_remoconHotKeys, index);
                        int count = x_ttbb_remoconHotKey.PushCount.getValue();
                        setHoKeyCount(x_ttbb_remoconHotKey, keyCode, count);
                        index++;
                    }

                    x_ttbb_remoconHotKeys.RemoconHotKeyInformEnable.addSetListener(new SetHotKeyInformEnableListener());
                    x_ttbb_remoconHotKeys.RemoconHotKeyInformInterval.addSetListener(new SetHotKeyInformIntervalListener());
                }catch (Exception e){
                }
                break;
            }
        }

        startInformSender();
    }

    private void startInformSender() {
        if(!informEnabled){
            if (Log.INCLUDE) {
                Log.d(TAG, "startInformSender: hot key inform is NOT enabled");
            }
            stopInformSender();
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "startInformSender");
        }
        if(informSendThread != null && !informSendThread.isInterrupted()) {
            if (Log.INCLUDE) {
                Log.w(TAG, "startInformSender: Already InformSender started");
            }
        } else {
            informSender = new InformSender();
            informSendThread = new Thread(informSender);
            informSendThread.start();
        }
    }

    private void stopInformSender() {
        if (Log.INCLUDE) {
            Log.d(TAG, "stopInformSender");
        }
        if(informSendThread != null && !informSendThread.isInterrupted()) {
            informSendThread.interrupt();
            informSendThread = null;
        }
    }

    /**
     * X_TTBB_RemoconHotKey inform 메시지를 전송합니다.
     *
     */
    public void notifyRemoconHotKey(){
        if(!informEnabled){
            if (Log.INCLUDE) {
                Log.d(TAG, "notifyRemoconHotKey: hot key inform is NOT enabled");
            }
            return;
        }

        CWMPController.getInstance().notifyRemoconHotKey(new InformCallback() {
            @Override
            public void onSuccess() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "sendInform: received SUCCESS informResponse for X_TTBB_RemoconHotKey");
                }
                // reset the click count for hot keys
                for(final ModelObject object : root.deviceInfo.getChilds()) {
                    if (object instanceof CWMPMultiObject && object instanceof X_TTBB_RemoconHotKeys) {
                        X_TTBB_RemoconHotKeys x_ttbb_remoconHotKeys = ((X_TTBB_RemoconHotKeys) object);
                        int size = x_ttbb_remoconHotKeys.getX_TTBB_RemoconHotKeyIndexes().size();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "notifyRemoconHotKey: InformCallback: x_ttbb_remoconHotKeys size=" + size);
                        }
                        int index = 1;

                        // #01 index 고정형
                        for(final String keyCode : CWMP_HOT_KEYs) {
                            X_TTBB_RemoconHotKey x_ttbb_remoconHotKey = getX_TTBB_RemoconHotKey(x_ttbb_remoconHotKeys, index);
                            setHoKeyCount(x_ttbb_remoconHotKey, keyCode, 0);

//                            x_ttbb_remoconHotKeys.removeX_TTBB_RemoconHotKey(index);
                            index++;
                        }

                        // #02 index 변동형
//                        for(index=1; index<=size; index++) {
//                            X_TTBB_RemoconHotKey x_ttbb_remoconHotKey = getX_TTBB_RemoconHotKey(x_ttbb_remoconHotKeys, index);
//                            if(x_ttbb_remoconHotKey != null) {
//                                setHoKeyCount(x_ttbb_remoconHotKey, "", 0);
//
//                                x_ttbb_remoconHotKeys.removeX_TTBB_RemoconHotKey(index);
//                            }
//                        }

                        if (Log.INCLUDE) {
                            Log.d(TAG, "notifyRemoconHotKey: InformCallback: x_ttbb_remoconHotKeys removed: " + x_ttbb_remoconHotKeys.getX_TTBB_RemoconHotKeyIndexes().size());
                        }
                        break;
                    }
                }
            }

            @Override
            public void onFail() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "sendInform: received FAIL informResponse for X_TTBB_RemoconHotKey");
                }
            }
        });
    }

    /**
     * 전달된 Hot key에 대한 클릭수를 집계합니다.
     *
     */
    public void onHotKeyClicked(int keyCode){
        if(!informEnabled){
            if (Log.INCLUDE) {
                Log.d(TAG, "onHotKeyClicked: hot key inform is NOT enabled, key="+keyCode);
            }
            return;
        }

        int index = 1;
        String targetHotKey = "";
        for(int i=0; i<VK_HOT_KEYs.length; i++) {
            if(keyCode == VK_HOT_KEYs[i]){
                targetHotKey = CWMP_HOT_KEYs[i];
                index = i + 1;
                break;
            }
        }
//        if (Log.INCLUDE) {
//            Log.d(TAG, "onHotKeyClicked: targetHotKey="+targetHotKey);
//        }

        for(final ModelObject object : root.deviceInfo.getChilds()) {
            if (object instanceof CWMPMultiObject && object instanceof X_TTBB_RemoconHotKeys) {
                X_TTBB_RemoconHotKeys x_ttbb_remoconHotKeys = ((X_TTBB_RemoconHotKeys) object);
                X_TTBB_RemoconHotKey x_ttbb_remoconHotKey = null;

                // #01 index 고정형
                x_ttbb_remoconHotKey = getX_TTBB_RemoconHotKey(x_ttbb_remoconHotKeys, index);
                int count = x_ttbb_remoconHotKey.PushCount.getValue() + 1;
                setHoKeyCount(x_ttbb_remoconHotKey, targetHotKey, count);

                // #02 index 변동형
//                int index = 1;
//                int size = x_ttbb_remoconHotKeys.getX_TTBB_RemoconHotKeyIndexes().size();
//                Log.d(TAG, "onHotKeyClicked: x_ttbb_remoconHotKeys size="+ size);
//                if(size > 0) {
//                    for (index = 1; index <= size; index++) {
//                        X_TTBB_RemoconHotKey tmp = x_ttbb_remoconHotKeys.getX_TTBB_RemoconHotKey(index);
//                        if (tmp != null) {
//                            String tmpHotKeyCode = tmp.HotKeyCode.getValue();
//                            if (targetHotKey.equals(tmpHotKeyCode)) {
//                                x_ttbb_remoconHotKey = tmp;
//                                break;
//                            }
//                        }
//                    }
//                }else{
//                    x_ttbb_remoconHotKey = x_ttbb_remoconHotKeys.getX_TTBB_RemoconHotKey(index);
//                }
//                Log.d(TAG, "onHotKeyClicked: x_ttbb_remoconHotKey's index="+ index);
//                if(x_ttbb_remoconHotKey == null) {
//                    x_ttbb_remoconHotKey = x_ttbb_remoconHotKeys.addX_TTBB_RemoconHotKey(new X_TTBB_RemoconHotKey(index));
//                }
//
//                int count = x_ttbb_remoconHotKey.PushCount.getValue() + 1;
//                setHoKeyCount(x_ttbb_remoconHotKey, targetHotKey, count);

                break;
            }
        }
    }

    private long getInformInterval(X_TTBB_RemoconHotKeys x_ttbb_remoconHotKeys){
        long interval = DEFAULT_INTERVAL;
        try {
            int intervalHr = x_ttbb_remoconHotKeys.RemoconHotKeyInformInterval.getValue();
            if (intervalHr > 0) {
                interval = intervalHr * 60 * 60 * 1000;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return interval;
    }

    private X_TTBB_RemoconHotKey getX_TTBB_RemoconHotKey(X_TTBB_RemoconHotKeys x_ttbb_remoconHotKeys, int index){
        X_TTBB_RemoconHotKey x_ttbb_remoconHotKey = null;

        // #01 index 고정형
        x_ttbb_remoconHotKey = x_ttbb_remoconHotKeys.getX_TTBB_RemoconHotKey(index);
        if(x_ttbb_remoconHotKey == null) {
            x_ttbb_remoconHotKey = x_ttbb_remoconHotKeys.addX_TTBB_RemoconHotKey(new X_TTBB_RemoconHotKey(index));
        }

        return x_ttbb_remoconHotKey;
    }

    private void setParamValue(CWMPParameter parameter, Object value){
        try{
            parameter.setValue(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setHoKeyCount(X_TTBB_RemoconHotKey x_ttbb_remoconHotKey, String hotKey, int count){
//        if (Log.INCLUDE) {
//            Log.d(TAG, "setHoKeyCount: hotKey=" + hotKey+", count="+count);
//        }
        try {
            x_ttbb_remoconHotKey.HotKeyCode.setValue(hotKey);
            x_ttbb_remoconHotKey.PushCount.setValue(count);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class SetHotKeyInformEnableListener implements SetParameterListener{
        @Override
        public void onSet(Object value) {
            if (Log.INCLUDE) {
                Log.d(TAG, "HotKeyInformEnable: onSet: value=" + value);
            }
            if(value instanceof Integer) {
                Integer command = (Integer) value;
                if(command == 0) { // 미전송
                    informEnabled = false;
                    stopInformSender();

                } else if(command == 1) { // 전송(default)
                    if(!informEnabled) {
                        informEnabled = true;
                        startInformSender();
                    }
                }
            }
        }
    }

    private class SetHotKeyInformIntervalListener implements SetParameterListener{
        @Override
        public void onSet(Object value) {
            if (Log.INCLUDE) {
                Log.d(TAG, "HotKeyInformInterval: onSet: value=" + value);
            }
            if(value instanceof Integer) {
                Integer intervalHr = (Integer) value;
                if(intervalHr > 0){
                    informInterval = intervalHr*60*60*1000;
                }
            }
        }
    }

    private class InformSender implements Runnable {

        InformSender() {
        }

        @Override
        public void run() {
            if (Log.INCLUDE) {
                Log.d(TAG, "InformSender: run");
            }
            try {
                while(!Thread.currentThread().isInterrupted()) {
                    Thread.sleep(informInterval);

                    notifyRemoconHotKey();
                }
            } catch (InterruptedException e) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "InformSender: exception occurred: " + e.toString());
                }
            } finally {
                if (Log.INCLUDE) {
                    Log.d(TAG, "InformSender: dead");
                }
            }
        }
    }

}

