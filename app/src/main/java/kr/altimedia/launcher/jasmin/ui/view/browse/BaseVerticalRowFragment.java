/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.browse;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import androidx.fragment.app.Fragment;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.leanback.widget.PresenterSelector;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;


/**
 * Created by mc.kim on 2018-01-10.
 */

public abstract class BaseVerticalRowFragment extends Fragment {
    private final String TAG = BaseVerticalRowFragment.class.getSimpleName();
    private static final String CURRENT_SELECTED_POSITION = "currentSelectedPosition";
    private ObjectAdapter mAdapter;
    protected VerticalGridView mVerticalGridView;
    private PresenterSelector mPresenterSelector;
    final ItemBridgeAdapter mBridgeAdapter = new ItemBridgeAdapter();
    int mSelectedPosition = -1;
    private boolean mPendingTransitionPrepare;
    private LateSelectionObserver mLateSelectionObserver = new LateSelectionObserver();

    public abstract int getLayoutResourceId();

    private final OnChildViewHolderSelectedListener mRowSelectedListener =
            new OnChildViewHolderSelectedListener() {
                @Override
                public void onChildViewHolderSelected(RecyclerView parent,
                                                      RecyclerView.ViewHolder view, int position, int subposition) {

                    if (Log.INCLUDE) {
                        Log.d(TAG, "onChildViewHolderSelected : " + position + ", sub position : " + subposition);
                    }
                    if (!mLateSelectionObserver.mIsLateSelection) {
                        mSelectedPosition = position;
                        onRowSelected(parent, view, position, subposition);
                    }
                }
            };

    void onRowSelected(RecyclerView parent, RecyclerView.ViewHolder view,
                       int position, int subposition) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onRowSelected : " + position + ", sub position : " + subposition);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResourceId(), container, false);
        mVerticalGridView = view.findViewById(R.id.gridView);
        if (mPendingTransitionPrepare) {
            mPendingTransitionPrepare = false;
            onTransitionPrepare();
        }
        return view;
    }

    VerticalGridView findGridViewFromRoot(View view) {
        return (VerticalGridView) view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mSelectedPosition = savedInstanceState.getInt(CURRENT_SELECTED_POSITION, -1);
        }
        setAdapterAndSelection();
        mVerticalGridView.setOnChildViewHolderSelectedListener(mRowSelectedListener);
    }

    protected VerticalGridView getItemGridView() {
        return mVerticalGridView;
    }

    /**
     * This class waits for the adapter to be updated before setting the selected
     * row.
     */
    private class LateSelectionObserver extends RecyclerView.AdapterDataObserver {
        boolean mIsLateSelection = false;

        LateSelectionObserver() {
        }

        @Override
        public void onChanged() {
            performLateSelection();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            performLateSelection();
        }

        void startLateSelection() {
            mIsLateSelection = true;
            mBridgeAdapter.registerAdapterDataObserver(this);
        }

        void performLateSelection() {
            clear();
            if (mVerticalGridView != null) {
                mVerticalGridView.setSelectedPosition(mSelectedPosition);
            }
        }

        void clear() {
            if (mIsLateSelection) {
                mIsLateSelection = false;
                mBridgeAdapter.unregisterAdapterDataObserver(this);
            }

            mBridgeAdapter.clear();
            mVerticalGridView.setAdapter(null);
        }
    }

    void setAdapterAndSelection() {
        mVerticalGridView.setAdapter(mBridgeAdapter);
        // We don't set the selected position unless we've data in the adapter.
        boolean lateSelection = mBridgeAdapter.getItemCount() == 0 && mSelectedPosition >= 0;
        if (lateSelection) {
            mLateSelectionObserver.startLateSelection();
        } else if (mSelectedPosition >= 0) {
            mVerticalGridView.setSelectedPosition(mSelectedPosition);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mLateSelectionObserver.clear();
        mVerticalGridView = null;
    }

    public boolean isLastSelection(){
        return mLateSelectionObserver.mIsLateSelection;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_SELECTED_POSITION, mSelectedPosition);
    }

    /**
     * Set the presenter selector used to create and bind views.
     */
    public final void setPresenterSelector(PresenterSelector presenterSelector) {
        mPresenterSelector = presenterSelector;
        updateAdapter();
    }

    /**
     * Get the presenter selector used to create and bind views.
     */
    public final PresenterSelector getPresenterSelector() {
        return mPresenterSelector;
    }

    /**
     * Sets the adapter for the fragment.
     */
    public final void setAdapter(ObjectAdapter rowsAdapter) {
        mAdapter = rowsAdapter;
        updateAdapter();
    }

    public void clearAdapter() {
        if (mBridgeAdapter != null) {
            getVerticalGridView().setAdapter(null);
            mBridgeAdapter.clear();
            mAdapter = null;
            updateAdapter();
        }
    }

    /**
     * Returns the list of rows.
     */
    public final ObjectAdapter getAdapter() {
        return mAdapter;
    }

    /**
     * Returns the bridge adapter.
     */
    final ItemBridgeAdapter getBridgeAdapter() {
        return mBridgeAdapter;
    }

    /**
     * Sets the selected row position with smooth animation.
     */
    public void setSelectedPosition(int position) {
        setSelectedPosition(position, true);
    }

    /**
     * Gets position of currently selected row.
     *
     * @return Position of currently selected row.
     */
    public int getSelectedPosition() {
        return mSelectedPosition;
    }

    /**
     * Sets the selected row position.
     */
    public void setSelectedPosition(int position, boolean smooth) {
        if (mSelectedPosition == position) {
            return;
        }
        mSelectedPosition = position;
        if (mVerticalGridView != null) {
            if (mLateSelectionObserver.mIsLateSelection) {
                return;
            }
            if (smooth) {
                mVerticalGridView.setSelectedPositionSmooth(position);
            } else {
                mVerticalGridView.setSelectedPosition(position);
            }
        }
    }

    public final VerticalGridView getVerticalGridView() {
        return mVerticalGridView;
    }

    void updateAdapter() {
        mBridgeAdapter.setAdapter(mAdapter);
        mBridgeAdapter.setPresenter(mPresenterSelector);
        if (mVerticalGridView != null) {
            setAdapterAndSelection();
        }
    }

    Object getItem(Row row, int position) {
        if (row instanceof ListRow) {
            return ((ListRow) row).getAdapter().get(position);
        } else {
            return null;
        }
    }

    public boolean onTransitionPrepare() {
        if (mVerticalGridView != null) {
            mVerticalGridView.setAnimateChildLayout(false);
            mVerticalGridView.setScrollEnabled(false);
            return true;
        }
        mPendingTransitionPrepare = true;
        return false;
    }

    public void onTransitionStart() {
        if (mVerticalGridView != null) {
            mVerticalGridView.setPruneChild(false);
            mVerticalGridView.setLayoutFrozen(true);
            mVerticalGridView.setFocusSearchDisabled(true);
        }
    }

    public void onTransitionEnd() {
        // be careful that fragment might be destroyed before header transition ends.
        if (mVerticalGridView != null) {
            mVerticalGridView.setLayoutFrozen(false);
            mVerticalGridView.setAnimateChildLayout(true);
            mVerticalGridView.setPruneChild(true);
            mVerticalGridView.setFocusSearchDisabled(false);
            mVerticalGridView.setScrollEnabled(true);
        }
    }

    public void setAlignment(int windowAlignOffsetTop) {
        if (mVerticalGridView != null) {
            // align the top edge of item
            mVerticalGridView.setItemAlignmentOffset(0);
            mVerticalGridView.setItemAlignmentOffsetPercent(
                    VerticalGridView.ITEM_ALIGN_OFFSET_PERCENT_DISABLED);

            // align to a fixed position from top
            mVerticalGridView.setWindowAlignmentOffset(windowAlignOffsetTop);
            mVerticalGridView.setWindowAlignmentOffsetPercent(
                    VerticalGridView.WINDOW_ALIGN_OFFSET_PERCENT_DISABLED);
            mVerticalGridView.setWindowAlignment(VerticalGridView.WINDOW_ALIGN_NO_EDGE);
        }
    }
}