/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;

import android.view.KeyEvent;

import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.OnMenuInteractor;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.menu.MenuSliderLayout;

/**
 * Created by mc.kim on 04,02,2020
 */
public class UserMenuRow extends Row {
    public enum UserMenu {
        USER, SEARCH, SETTINGS, NOTIFICATION
    }

    private final OnUserMenuClickListener mUserMenuClickListener;
    private OnMenuInteractor mInteractor;

    private MenuSliderLayout menuSliderLayout;

    public UserMenuRow(OnUserMenuClickListener userMenuClickListener) {
        this.mUserMenuClickListener = userMenuClickListener;
    }

    public OnUserMenuClickListener getMenuClickListener() {
        return mUserMenuClickListener;
    }

    public void setInteractor(OnMenuInteractor mInteractor) {
        this.mInteractor = mInteractor;
    }

    public void setMenuSlider(MenuSliderLayout mInteractor) {
        this.menuSliderLayout = mInteractor;
    }

    public OnMenuInteractor getInteractor() {
        return mInteractor;
    }

    public MenuSliderLayout getMenuSlider() {
        return menuSliderLayout;
    }


    public interface OnUserMenuClickListener {
        void onMenuClicked(UserMenu menu);

        boolean dispatchKey(KeyEvent keyEvent);
    }

}
