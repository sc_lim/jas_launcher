/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.view.common.ScrollDescriptionTextButton;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;

public class SubscriptionInfoDialogFragment extends SafeDismissDialogFragment {
    public static final String CLASS_NAME = SubscriptionInfoDialogFragment.class.getName();
    private static final String TAG = SubscriptionInfoDialogFragment.class.getSimpleName();

    private static final String KEY_PRODUCT = "PRODUCT";
    private Product product;

    private final int MAX_LINE_COUNT = 7;

    public OnSelectSubscriptionListener onSelectSubscriptionListener;

    private final ScrollDescriptionTextButton.OnButtonKeyListener mOnButtonKeyListener = new ScrollDescriptionTextButton.OnButtonKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    int id = v.getId();
                    if (id == R.id.next) {
                        onSelectSubscriptionListener.onSelect(product);
                    } else {
                        onSelectSubscriptionListener.onSelect(null);
                    }

                    return true;
            }

            return false;
        }
    };

    public SubscriptionInfoDialogFragment() {

    }

    public static SubscriptionInfoDialogFragment newInstance(Product product) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_PRODUCT, product);

        SubscriptionInfoDialogFragment fragment = new SubscriptionInfoDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    public void setOnSelectSubscriptionListener(OnSelectSubscriptionListener onSelectSubscriptionListener) {
        this.onSelectSubscriptionListener = onSelectSubscriptionListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_subscription_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        product = getArguments().getParcelable(KEY_PRODUCT);
        ((TextView)view.findViewById(R.id.title)).setText(product.getProductName());
        initButtons(view);
    }

    private void initButtons(View view) {
        TextView desc = view.findViewById(R.id.desc);
        desc.setText(product.getDescription());

        ThumbScrollbar scrollbar = view.findViewById(R.id.scrollbar);
        ScrollDescriptionTextButton next = view.findViewById(R.id.next);
        ScrollDescriptionTextButton cancel = view.findViewById(R.id.cancel);

        desc.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                scrollbar.setList(desc.getLineCount(), MAX_LINE_COUNT);
                if (scrollbar.getVisibility() == View.VISIBLE) {
                    desc.setMovementMethod(new ScrollingMovementMethod());
                }
            }
        });

        desc.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                boolean isMove = scrollY > oldScrollY;
                scrollbar.moveScroll(isMove);
            }
        });

        next.setDescriptionTextView(desc);
        cancel.setDescriptionTextView(desc);
        next.setOnButtonKeyListener(mOnButtonKeyListener);
        cancel.setOnButtonKeyListener(mOnButtonKeyListener);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onSelectSubscriptionListener = null;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public interface OnSelectSubscriptionListener {
        void onSelect(Product product);
    }
}
