/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;

/**
 * Created by mc.kim on 18,02,2020
 */
public enum SidePanelType {
    TIME_SHIFT("TimeShift",
            AccountManager.getInstance().getLocalLanguage().equalsIgnoreCase("EN") ?
                    R.raw.option_category_timeshift : R.raw.option_category_timeshift_th),

    LIVE("LiveTv",
            AccountManager.getInstance().getLocalLanguage().equalsIgnoreCase("EN") ?
                    R.raw.option_category_channel : R.raw.option_category_channel_th),
    VOD("VOD", AccountManager.getInstance().getLocalLanguage().equalsIgnoreCase("EN") ?
            R.raw.option_category_vod : R.raw.option_category_vod_th);

    final String type;
    final int resourceId;

    SidePanelType(String type, int resourceId) {
        this.type = type;
        this.resourceId = resourceId;
    }

    public int getResourceId() {
        return resourceId;
    }

    public String getType() {
        return type;
    }

    public static SidePanelType findByType(String type) {
        SidePanelType[] sdePanelTypes = values();
        for (SidePanelType panelType : sdePanelTypes) {
            if (panelType.type.equalsIgnoreCase(type)) {
                return panelType;
            }
        }
        return null;
    }

}
