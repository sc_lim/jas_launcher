package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import java.util.ArrayList;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentCreditCardFragment.LIST_NUMBER_COLUMNS;

public class CreditCardListPagerFragmentCreditCard extends CreditCardPageBaseFragment {
    public static final String KEY_PAGE_LIST = "PAGE_LIST";
    private final int NUMBER_ROWS = VodPaymentCreditCardFragment.LIST_NUMBER_ROWS;

    private ArrayList<CreditCard> creditCardList;
    private HorizontalGridView gridView;

    public static CreditCardListPagerFragmentCreditCard newInstance(ArrayList<CreditCard> list) {

        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_PAGE_LIST, list);

        CreditCardListPagerFragmentCreditCard fragment = new CreditCardListPagerFragmentCreditCard();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getResourceLayoutId() {
        return R.layout.fragment_credit_card_list_pager;
    }

    @Override
    protected void initView(View view) {
        creditCardList = getArguments().getParcelableArrayList(KEY_PAGE_LIST);
        initBankGridView(view);
    }

    private void initBankGridView(View view) {
        gridView = view.findViewById(R.id.gridView);
        gridView.setNumRows(NUMBER_ROWS);

        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(new CreditCardPresenter());
        objectAdapter.addAll(0, creditCardList);

        ItemBridgeAdapter bridgeAdapter = new ItemBridgeAdapter(objectAdapter);
        gridView.setAdapter(bridgeAdapter);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                gridView.setSelectedPosition(INIT_POSITION);
            }
        });

    }

    @Override
    public int getSize() {
        return creditCardList.size();
    }

    @Override
    public int getFocusedPosition() {
        return gridView.getChildAdapterPosition(gridView.getFocusedChild());
    }

    @Override
    public CreditCard getItem(int position) {
        return creditCardList.get(position);
    }

    @Override
    public void requestPosition(int position) {
        if (gridView != null) {
            gridView.setSelectedPosition(position);
            gridView.requestFocus();
        }
    }

    @Override
    public boolean isLastColumn(int position) {
        int size = creditCardList.size();
        int lastColumn = size / LIST_NUMBER_COLUMNS;
        if (size % LIST_NUMBER_COLUMNS > 0) {
            lastColumn++;
        }

        position += 1;
        int row = position / LIST_NUMBER_COLUMNS;
        if (position % NUMBER_ROWS > 0) {
            row++;
        }

        return row == lastColumn;
    }
}
