/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm;

import android.os.AsyncTask;

import com.altimedia.util.Log;
import com.altimedia.util.NetworkUtil;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;

/**
 * Created by mc.kim on 18,05,2020
 */
public class MbsDataTask<K> extends AsyncTask<K, Void, Object> {
    private final String TAG = MbsDataTask.class.getSimpleName();

    private final MbsTaskCallback mMbsTaskCallback;
    private final MbsDataProvider mMbsDataProvider;

    CountDownLatch latch = new CountDownLatch(1);
    private K mType = null;

    public MbsDataTask(@NonNull MbsDataProvider provider, @NonNull MbsTaskCallback mbsTaskCallback) {
        this.mMbsDataProvider = provider;
        this.mMbsTaskCallback = mbsTaskCallback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (Log.INCLUDE) {
            Log.d(TAG, "onPreExecute");
        }
        latch.countDown();
        this.mMbsTaskCallback.onPreExecute();
    }

    @Override
    protected Object doInBackground(K... integers) {

        if (Log.INCLUDE) {
            Log.d(TAG, "this.mMbsDataProvider.ignoreUserLogin() " + this.mMbsDataProvider.ignoreUserLogin());
        }
        if (!this.mMbsDataProvider.ignoreUserLogin()
                && !AccountManager.getInstance().isLoginUser()) {
            return new FailObject(MbsTaskCallback.REASON_LOGIN);
        }

        mType = integers[0];
        Object result = null;
        try {
            result = this.mMbsTaskCallback.onTaskBackGround(mType);
        } catch (IOException e) {
            e.printStackTrace();
            if (this.mMbsDataProvider.getTaskContext() != null) {
                boolean networkConnected = NetworkUtil.hasNetworkConnection(this.mMbsDataProvider.getTaskContext());
                if (Log.INCLUDE) {
                    Log.d(TAG, "networkConnected " + networkConnected);
                }
                if (!networkConnected) {
                    return new FailObject(MbsTaskCallback.REASON_NETWORK);
                } else {
                    return new ErrorObject(MbsTaskCallback.ERROR_IO, e.getMessage());
                }
            } else {
                return new ErrorObject(MbsTaskCallback.ERROR_IO, e.getMessage());
            }

        } catch (ResponseException e) {
            e.printStackTrace();

            if (this.mMbsDataProvider.getTaskContext() != null) {
                boolean networkConnected = NetworkUtil.hasNetworkConnection(this.mMbsDataProvider.getTaskContext());
                if (Log.INCLUDE) {
                    Log.d(TAG, "networkConnected " + networkConnected);
                }
                if (!networkConnected) {
                    return new FailObject(MbsTaskCallback.REASON_NETWORK);
                } else {
                    return new ErrorObject(e.getCode(), e.getMessage());
                }
            } else {
                return new ErrorObject(e.getCode(), e.getMessage());
            }
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
        if (isCancelled()) {
            return this.mMbsTaskCallback.onCanceled(mType);
        } else {
            return result;
        }
    }

    @Override
    protected void onPostExecute(Object params) {
        super.onPostExecute(params);
        if (Log.INCLUDE) {
            Log.d(TAG, "onPostExecute");
        }
        if (params instanceof ErrorObject) {
            ErrorObject errorResult = (ErrorObject) params;
            mMbsTaskCallback.onError(errorResult.getErrorCode(), errorResult.getErrorMessage());
        } else if (params instanceof FailObject) {
            mMbsTaskCallback.onTaskFailed(((FailObject) params).reason);
        } else {
            if (isCancelled()) {
                this.mMbsTaskCallback.onCanceled(mType);
            } else {
                if (params == null) {
                    mMbsTaskCallback.onTaskFailed(MbsTaskCallback.REASON_NULL_OBJECT);
                } else {
                    this.mMbsTaskCallback.onTaskPostExecute(mType, params);
                }
            }
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (Log.INCLUDE) {
            Log.d(TAG, "onCancelled");
        }
        this.mMbsTaskCallback.onCanceled(mType);
    }

    public boolean isRunning() {
        return getStatus() == Status.RUNNING;
    }

    private static class ErrorObject {
        final String errorCode;
        final String errorMessage;

        public ErrorObject(String errorCode, String errorMessage) {
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public String getErrorMessage() {
            return errorMessage;
        }
    }

    private static class FailObject {
        final int reason;

        public FailObject(int resume) {
            this.reason = resume;
        }

        public int getReason() {
            return reason;
        }
    }

}
