/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.PageBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.edit.SettingDeviceEditDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.DevicePresenter;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class SettingDeviceManageFragment extends SettingBaseFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton<UserDevice>,
        SettingDeviceEditDialogFragment.OnChangeDeviceListener {
    public static final String CLASS_NAME = SettingDeviceManageFragment.class.getName();
    private final String TAG = SettingDeviceManageFragment.class.getSimpleName();

    private final int VISIBLE_COUNT = 6;

    private ArrayList<UserDevice> deviceList;
    private ArrayObjectAdapter objectAdapter;

    private LinearLayout deviceListLayout;
    private LinearLayout noneLayout;

    private MbsDataTask deviceTask;

    public static SettingDeviceManageFragment newInstance() {
        return new SettingDeviceManageFragment();
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_device_manage;
    }

    @Override
    protected void initView(View view) {
        deviceListLayout = view.findViewById(R.id.device_list_layout);
        noneLayout = view.findViewById(R.id.device_none_layout);

        loadUserDeviceList();
    }

    private void loadUserDeviceList() {
        deviceTask = onFragmentChange.getSettingTaskManager().getDeviceList(
                new MbsDataProvider<String, List<UserDevice>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadUserDeviceList, needLoading, loading : " + loading);
                        }

                        onFragmentChange.showProgressbar(loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadUserDeviceList onFailed");
                        }

                        onFragmentChange.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, List<UserDevice> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadUserDeviceList, onSuccess, result : " + result);
                        }

                        showDeviceList((ArrayList<UserDevice>) result);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadUserDeviceList, onError");
                        }

                        onFragmentChange.getSettingTaskManager()
                                .showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingDeviceManageFragment.this.getContext();
                    }
                });
    }

    private void showDeviceList(ArrayList<UserDevice> list) {
        deviceList = list;

        if (list == null || list.size() == 0) {
            noneLayout.setVisibility(View.VISIBLE);
            return;
        }

        noneLayout.setVisibility(View.GONE);

        objectAdapter = new ArrayObjectAdapter(new DevicePresenter());
        objectAdapter.addAll(0, deviceList);

        PageBridgeAdapter mPageBridgeAdapter = new PageBridgeAdapter(objectAdapter, VISIBLE_COUNT);
        mPageBridgeAdapter.setListener(this);

        PagingVerticalGridView gridView = getView().findViewById(R.id.gridView);
        gridView.initVertical(VISIBLE_COUNT);
        gridView.setAdapter(mPageBridgeAdapter);
        int gap = (int) getResources().getDimension(R.dimen.setting_profile_vertical_gap);
        gridView.setVerticalSpacing(gap);

        deviceListLayout.setVisibility(View.VISIBLE);
        initScrollbar(list.size(), gridView);
    }

    private void initScrollbar(int size, PagingVerticalGridView gridView) {
        ThumbScrollbar scrollbar = getView().findViewById(R.id.scrollbar);
        scrollbar.setList(size, VISIBLE_COUNT);
        scrollbar.setFocusable(false);

        gridView.addOnChildViewHolderSelectedListener(new OnChildViewHolderSelectedListener() {
            @Override
            public void onChildViewHolderSelected(RecyclerView parent, RecyclerView.ViewHolder child, int position, int subposition) {
                super.onChildViewHolderSelected(parent, child, position, subposition);
                scrollbar.moveFocus(position);
            }
        });
    }

    @Override
    public boolean onClickButton(UserDevice item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        SettingDeviceEditDialogFragment settingDeviceEditDialogFragment = SettingDeviceEditDialogFragment.newInstance(item);
        settingDeviceEditDialogFragment.setOnChangeDeviceListener(this);
        onFragmentChange.getTvOverlayManager().showDialogFragment(settingDeviceEditDialogFragment);

        return true;
    }

    @Override
    public void onChangeDeviceName(UserDevice userDevice) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onChangeDeviceName, userDevice : " + userDevice);
        }

        int index = objectAdapter.indexOf(userDevice);
        objectAdapter.replace(index, userDevice);
    }

    @Override
    public void onDeleteDevice(UserDevice userDevice) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onDeleteDevice, userDevice : " + userDevice);
        }

        if (deviceList.size() == 1 && deviceList.indexOf(userDevice) != -1) {
            changeToNoneList();
        }

        deviceList.remove(userDevice);
        objectAdapter.remove(userDevice);

        if (deviceList.size() > 0) {
            VerticalGridView gridView = getView().findViewById(R.id.gridView);
            gridView.getChildAt(0).requestFocus();
        }
    }

    private void changeToNoneList() {
        getOnFragmentChange().onMenuFocus();

        deviceListLayout.setVisibility(View.GONE);
        noneLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        if (deviceTask != null) {
            deviceTask.cancel(true);
        }

        super.onDestroyView();
    }
}
