/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.response.ChangePinCodeResponse;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.PinPresenter;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account.SettingAccountPinErrorManager.CHANGE_MATCH;
import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account.SettingAccountPinErrorManager.CHANGE_SAME_ERROR;
import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.account.SettingAccountPinErrorManager.PIN_INCORRECT;

public class SettingAccountPinChangeDialogFragment extends SettingBaseDialogFragment implements AccountPinItemBridgeAdapter.OnButtonFocusRequestListener {
    public static final String CLASS_NAME = SettingAccountPinChangeDialogFragment.class.getName();
    private static final String TAG = SettingAccountPinChangeDialogFragment.class.getSimpleName();

    private static final String ERROR_PIN_INCORRECT = "MBS_0008";
    private static final String ERROR_PIN_EXISTING = "MBS_0039";

    private static final float SAVE_DEFAULT_ALPHA = 1.0f;
    private static final float SAVE_DIM_ALPHA = 0.2f;

    private TextView save;
    private TextView cancel;

    private VerticalGridView gridView;
    private OnPinChangeResultCallback mOnPinChangeResultCallback;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private OnKeyListener onKeyListener;

    private SettingAccountPinChangeDialogFragment() {
    }

    public static SettingAccountPinChangeDialogFragment newInstance() {
        SettingAccountPinChangeDialogFragment fragment = new SettingAccountPinChangeDialogFragment();
        return fragment;
    }

    public void setOnPinChangeResultCallback(@NonNull OnPinChangeResultCallback onPinChangeResultCallback) {
        this.mOnPinChangeResultCallback = onPinChangeResultCallback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        mOnPinChangeResultCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_pin_change, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        setPinList(view);
        setButtons(view);
        setSaveEnable(false);

        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private void setPinList(View view) {
        PinChangeItem.CURRENT.initPin();
        PinChangeItem.NEW.initPin();
        PinChangeItem.CONFIRM.initPin();

        ArrayList<PinChangeItem> pinList = new ArrayList<>(Arrays.asList(PinChangeItem.CURRENT, PinChangeItem.NEW, PinChangeItem.CONFIRM));
        ArrayObjectAdapter passwordAdapter = new ArrayObjectAdapter(new PinPresenter());
        passwordAdapter.addAll(0, pinList);

        gridView = view.findViewById(R.id.gridView);
        AccountPinItemBridgeAdapter itemBridgeAdapter = new AccountPinItemBridgeAdapter(passwordAdapter, gridView);
        itemBridgeAdapter.setOnButtonFocusRequestListener(this);
        gridView.setAdapter(itemBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        gridView.setVerticalSpacing(spacing);
    }

    private void setButtons(View view) {
        save = view.findViewById(R.id.saveButton);
        cancel = view.findViewById(R.id.cancelButton);
        TextView description = view.findViewById(R.id.description);

        onKeyListener = new OnKeyListener(mOnPinChangeResultCallback, mProgressBarManager);
        onKeyListener.setView(description, gridView);
        save.setOnKeyListener(onKeyListener);
        cancel.setOnKeyListener(onKeyListener);
    }

    @Override
    public void onSaveButtonRequestFocus() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSaveButtonRequestFocus");
        }

        setSaveEnable(true);
        save.requestFocus();
    }

    @Override
    public void onCancelButtonRequestFocus() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onCancelButtonRequestFocus");
        }

        cancel.requestFocus();
    }

    @Override
    public void onDisableSave() {
        setSaveEnable(false);
    }

    private void setSaveEnable(boolean isEnable) {
        if (save.isEnabled() != isEnable) {
            float alpha = isEnable ? SAVE_DEFAULT_ALPHA : SAVE_DIM_ALPHA;
            save.setAlpha(alpha);
            save.setEnabled(isEnable);
        }
    }

    @Override
    public void onDestroyView() {
        onKeyListener.destroyTask();
        save.setOnKeyListener(null);
        cancel.setOnKeyListener(null);

        super.onDestroyView();
    }

    protected enum PinChangeItem implements PinPresenter.PinButtonItem {
        CURRENT(0, R.string.current_pin, true), NEW(1, R.string.new_pin, false), CONFIRM(2, R.string.confirm_new_pin, false);

        private int type;
        private int title;
        private String password = "";
        private boolean isEnable;

        PinChangeItem(int type, int title, boolean isEnable) {
            this.type = type;
            this.title = title;
            this.isEnable = isEnable;
        }

        public void clearPassword() {
            password = "";
        }

        public void initPin() {
            this.isEnable = this == CURRENT;
            clearPassword();
        }

        @Override
        public int getType() {
            return type;
        }

        @Override
        public int getTitle() {
            return title;
        }

        @Override
        public String getPassword() {
            return password;
        }

        @Override
        public boolean isEnable() {
            return isEnable;
        }

        @Override
        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public void setEnable(boolean enable) {
            isEnable = enable;
        }
    }

    public interface OnPinChangeResultCallback {
        void onSuccess();

        void onFail(int key);

        void onError(String errorCode, String message);

        void onCancel();

        SettingTaskManager getSettingTaskManager();
    }

    private static class OnKeyListener implements View.OnKeyListener {
        private ProgressBarManager mProgressBarManager;
        private MbsDataTask changeTask;

        private TextView description;
        private VerticalGridView gridView;

        private OnPinChangeResultCallback mOnPinChangeResultCallback;

        private final SettingAccountPinErrorManager settingAccountPinErrorManager = new SettingAccountPinErrorManager();

        public OnKeyListener(OnPinChangeResultCallback mOnPinChangeResultCallback, ProgressBarManager progressBarManager) {
            this.mOnPinChangeResultCallback = mOnPinChangeResultCallback;
            this.mProgressBarManager = progressBarManager;
        }

        public void setView(TextView description, VerticalGridView gridView) {
            this.description = description;
            this.gridView = gridView;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != KeyEvent.ACTION_DOWN) {
                return false;
            }

            if (mOnPinChangeResultCallback == null) {
                return false;
            }

            int id = v.getId();

            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    gridView.requestFocus();

                    if (id == R.id.saveButton) {

                    } else if (id == R.id.cancelButton) {
                        ItemBridgeAdapter.ViewHolder nextVh = (ItemBridgeAdapter.ViewHolder) gridView.getChildViewHolder(gridView.getFocusedChild());
                        PinPresenter.ViewHolder next = (PinPresenter.ViewHolder) nextVh.getViewHolder();
                        next.clearPassword();
                    }
                    return true;
                case KeyEvent.KEYCODE_ENTER:
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    if (id == R.id.saveButton) {
                        int result = settingAccountPinErrorManager.getPinErrorType(PinChangeItem.CURRENT.getPassword(), PinChangeItem.NEW.getPassword(), PinChangeItem.CONFIRM.getPassword());

                        if (Log.INCLUDE) {
                            Log.d(TAG, "onClick, result : " + result);
                        }

                        if (result == CHANGE_MATCH) {
                            changePinCode(v);
                        } else {
                            incorrectPin(v, result);
                        }
                    } else if (id == R.id.cancelButton) {
                        mOnPinChangeResultCallback.onCancel();
                    }

                    return true;
            }

            return false;
        }

        private void incorrectPin(View v, int result) {
            String text = settingAccountPinErrorManager.getErrorText(description.getContext(), result);
            description.setText(text);
            description.setTextColor(v.getResources().getColor(R.color.color_FFA67C52));

            PinChangeItem.CURRENT.clearPassword();
            PinChangeItem.NEW.clearPassword();
            PinChangeItem.NEW.setEnable(false);
            PinChangeItem.CONFIRM.clearPassword();
            PinChangeItem.CONFIRM.setEnable(false);

            gridView.getAdapter().notifyDataSetChanged();
            gridView.setSelectedPosition(0);

            setSaveEnable((TextView) v, false);
        }

        private void setSaveEnable(TextView save, boolean isEnable) {
            if (save.isEnabled() != isEnable) {
                float alpha = isEnable ? SAVE_DEFAULT_ALPHA : SAVE_DIM_ALPHA;
                save.setAlpha(alpha);
                save.setEnabled(isEnable);
            }
        }

        private void changePinCode(View view) {
            String currentPin = PinChangeItem.CURRENT.getPassword();
            String newPin = PinChangeItem.NEW.getPassword();

            changeTask = mOnPinChangeResultCallback.getSettingTaskManager().changeAccountPinCode(
                    currentPin, newPin,
                    new MbsDataProvider<String, ChangePinCodeResponse>() {
                        @Override
                        public void needLoading(boolean loading) {
                            if (loading) {
                                mProgressBarManager.show();
                            } else {
                                mProgressBarManager.hide();
                            }
                        }

                        @Override
                        public void onFailed(int key) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onFailed");
                            }

                            mOnPinChangeResultCallback.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                        }

                        @Override
                        public void onSuccess(String id, ChangePinCodeResponse result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onSuccess, result : " + result);
                            }

                            ChangePinCodeResponse.PinCodeResult pinCodeResult = result.getResult();
                            boolean isSuccess = pinCodeResult.isSuccess();

                            if (isSuccess) {
                                mOnPinChangeResultCallback.onSuccess();
                            }
                        }

                        @Override
                        public void onError(String errorCode, String message) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onError, errorCode : " + errorCode + ", message : " + message);
                            }

                            showErrorText(view, errorCode, message);
                        }

                        @Override
                        public Context getTaskContext() {
                            return view.getContext();
                        }
                    });
        }

        private void showErrorText(View view, String errorCode, String message) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showErrorText, errorCode : " + errorCode);
            }

            if (errorCode.equalsIgnoreCase(ERROR_PIN_INCORRECT)) {
                incorrectPin(view, PIN_INCORRECT);
            } else if (errorCode.equalsIgnoreCase(ERROR_PIN_EXISTING)) {
                incorrectPin(view, CHANGE_SAME_ERROR);
            } else {
                mOnPinChangeResultCallback.onError(errorCode, message);
            }
        }

        public void destroyTask() {
            mOnPinChangeResultCallback = null;

            if (changeTask != null) {
                changeTask.cancel(true);
            }
        }
    }
}
