/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;

/**
 * Created by mc.kim on 25,05,2020
 */
public class VideoSidePanelOptionProvider extends SidePanelOptionProvider<Content> implements SidePanelOptionCallback {
    public VideoSidePanelOptionProvider(Content item) {
        super(item);
    }

    public String getOffString() {
        return "OFF";
    }

    @Override
    public Object getData(int key) {
        switch (key) {
            case KEY_SUBTITLE:
                return getSubtitleList();
            case KEY_AUDIO_LIST:
                return getAudioList();
        }

        return null;
    }

    @Override
    public ArrayList<String> getSubtitleList() {
        return new ArrayList<>();
    }

    @Override
    public ArrayList<String> getAudioList() {
        return new ArrayList<>();
    }

    @Override
    public void setSubtitle(String lang) {

    }

    @Override
    public void setAudio(String lang) {

    }

    @Override
    public String getCurrentSubtitleLanguage() {
        return getOffString();
    }

    @Override
    public String getCurrentAudioLanguage() {
        return getOffString();
    }

    @Override
    public boolean isFavoriteMenuVisible() {
        return true;
    }

    @Override
    public void requestFavorite(boolean isAdd) {
    }

    @Override
    public String getSubtitleStyle() {
        return "";
    }

    @Override
    public void setSubtitleStyle(String style) {

    }

    @Override
    public void reset() {

    }
}
