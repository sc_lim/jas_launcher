/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class DeviceItem implements Parcelable {
    public static final Creator<DeviceItem> CREATOR = new Creator<DeviceItem>() {
        @Override
        public DeviceItem createFromParcel(Parcel in) {
            return new DeviceItem(in);
        }

        @Override
        public DeviceItem[] newArray(int size) {
            return new DeviceItem[size];
        }
    };
    private String name;
    private Date loginDate;

    public DeviceItem(String name, Date loginDate) {
        this.name = name;
        this.loginDate = loginDate;
    }

    protected DeviceItem(Parcel in) {
        name = in.readString();
        loginDate = (Date) in.readValue(Date.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeValue(loginDate);
    }

    public String getName() {
        return name;
    }

    public Date getLoginDate() {
        return loginDate;
    }
}
