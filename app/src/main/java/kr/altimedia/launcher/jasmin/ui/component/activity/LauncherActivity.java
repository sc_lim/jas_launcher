/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.component.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileList;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.system.manager.ExternalApplicationManager;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.BootDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ChannelErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ProfileLoginNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.profile.ProfileDeleteNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.profile.ProfileSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.interactor.LaunchPath;
import kr.altimedia.launcher.jasmin.ui.component.fragment.interactor.OnAppLaunchListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.interactor.OnFragmentVisibilityInteractor;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home.MenuNavigationFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.Linker;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager;
import kr.altimedia.launcher.jasmin.ui.util.PermissionUtils;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PROFILE;


public class LauncherActivity extends BaseActivity implements OnFragmentVisibilityInteractor, View.OnFocusChangeListener, OnAppLaunchListener {

    private final String TAG = LauncherActivity.class.getSimpleName();
    TvOverlayManager mTvOverlayManager;
    ChannelDataManager mChannelDataManager;
    ProgramDataManager mProgramDataManager;
    TvInputManagerHelper mTvInputManagerHelper;
    public static int RESULT_CODE_SHOW_VOD_DETAIL = 200;
    public static int RESULT_CODE_WATCH_FINISHED = 201;
    public static int RESULT_CODE_USER_DELETED = 300;
    public static int RESULT_CODE_ERROR_READY = 400;
    public static int RESULT_CODE_LIVE_TV = 301;
    public static String KEY_VOD_DETAIL = "nextEpisode";
    public static String KEY_ERROR_CODE = "errorCode";
    final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Log.INCLUDE) {
                Log.d(TAG, "action : " + action);
            }
            switch (action) {
                case PushMessageReceiver.ACTION_UPDATED_MENU:
                    onMenuVersionChanged();
                    break;

                case PushMessageReceiver.ACTION_LOGIN:
                    onLoginInformationChanged();
                    break;
                case PushMessageReceiver.ACTION_UPDATED_PROFILE:
                    PushMessageReceiver.ProfileUpdateType typeData = (PushMessageReceiver.ProfileUpdateType) intent.getSerializableExtra(PushMessageReceiver.KEY_TYPE);
                    String id = intent.getStringExtra(PushMessageReceiver.KEY_PROFILE_ID);
                    if (typeData == null) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "ACTION_UPDATED_PROFILE not contain type so return");
                        }
                        return;
                    }
                    if (Log.INCLUDE) {
                        Log.d(TAG, "ACTION_UPDATED_PROFILE typeData : " + typeData);
                    }

                    switch (typeData) {
                        case login:
                        case rating:
                            onProfileInformationChanged(typeData, id);
                            break;
                        case userDeleted:
                            onProfileDeleted();
                            break;
                    }
                    break;
            }
        }
    };

    private void onProfileDeleted() {
        mTvOverlayManager.hideOverlays(TvOverlayManager.FLAG_HIDE_OVERLAYS_KEEP_MENU);
        notifyProfileUpdated();
        ProfileDeleteNoticeDialogFragment mProfileDeleteNoticeDialogFragment = ProfileDeleteNoticeDialogFragment.newInstance();
        mProfileDeleteNoticeDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                showProfileSelectedDialog();
            }
        });
        mProfileDeleteNoticeDialogFragment.show(getSupportFragmentManager(), ProfileDeleteNoticeDialogFragment.CLASS_NAME);
    }


    private void showProfileSelectedDialog() {
        UserDataManager userDataManager = new UserDataManager();
        userDataManager.getProfileList(AccountManager.getInstance().getSaId(), new MbsDataProvider<String, ProfileList>() {

            @Override
            public void needLoading(boolean loading) {
                if (loading) {
                    mProgressBarManager.show();
                } else {
                    mProgressBarManager.hide();
                }
            }

            @Override
            public void onFailed(int reason) {

                if (reason == MbsTaskCallback.REASON_NETWORK || reason == MbsTaskCallback.REASON_LOGIN) {
                    FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(reason, LauncherActivity.this);
                    failNoticeDialogFragment.show(mTvOverlayManager, getSupportFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    jumAppList();
                } else {
                    ProfileLoginNoticeDialogFragment mProfileLoginNoticeDialogFragment = ProfileLoginNoticeDialogFragment.newInstance(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            switch (v.getId()) {
                                case R.id.closeButton:
                                    jumAppList();
                                    break;
                                case R.id.okButton:
                                    showProfileSelectedDialog();
                                    break;
                            }
                        }
                    });
                    getTvOverlayManager().showDialogFragment(mProfileLoginNoticeDialogFragment);
                }
            }

            @Override
            public void onSuccess(String id, ProfileList result) {

                if (result == null || result.getProfileInfoList() == null || result.getProfileInfoList().isEmpty()) {
                    onFailed(MbsTaskCallback.REASON_LOGIN);
                    return;
                }
                ArrayList<ProfileInfo> profileInfos = (ArrayList<ProfileInfo>) result.getProfileInfoList();
                ProfileSelectDialogFragment profileSelectDialogFragment = ProfileSelectDialogFragment.newInstance(profileInfos);
                profileSelectDialogFragment.setOnSelectProfileListener(new ProfileSelectDialogFragment.OnSelectProfileListener() {
                    @Override
                    public void onSelectProfile(ProfileInfo profileInfo) {
                        profileSelectDialogFragment.dismiss();
                        notifyProfileUpdate(profileInfo.getProfileId());
                    }
                });
                profileSelectDialogFragment.show(getSupportFragmentManager(), ProfileSelectDialogFragment.CLASS_NAME);
            }

            @Override
            public void onError(String errorCode, String message) {
                ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                errorDialogFragment.show(getSupportFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                jumAppList();
//                ProfileLoginNoticeDialogFragment mProfileLoginNoticeDialogFragment = ProfileLoginNoticeDialogFragment.newInstance(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        switch (v.getId()) {
//                            case R.id.closeButton:
//                                jumAppList();
//                                break;
//                            case R.id.okButton:
//                                showProfileSelectedDialog();
//                                break;
//                        }
//                    }
//                });
//                getTvOverlayManager().showDialogFragment(mProfileLoginNoticeDialogFragment);
            }

            @Override
            public Context getTaskContext() {
                return LauncherActivity.this;
            }
        });
    }

    private void notifyProfileUpdated() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MenuNavigationFragment.CLASS_NAME);
        if (fragment == null || !fragment.isAdded()) {
            return;
        }
        MenuNavigationFragment mMenuNavigationFragment = (MenuNavigationFragment) fragment;
        mMenuNavigationFragment.mSidePanel.renewalProfileInfo();
    }

    private void jumAppList() {

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MenuNavigationFragment.CLASS_NAME);
        if (fragment == null || !fragment.isAdded()) {
            return;
        }
        MenuNavigationFragment mMenuNavigationFragment = (MenuNavigationFragment) fragment;
        mMenuNavigationFragment.jumpAppList();
    }


    private void notifyProfileUpdate(String profileId) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(PushMessageReceiver.KEY_TYPE, PushMessageReceiver.ProfileUpdateType.login);
        bundle.putString(PushMessageReceiver.KEY_PROFILE_ID, profileId);

        Intent intent = new Intent();
        intent.setAction(ACTION_UPDATED_PROFILE);
        intent.putExtras(bundle);

        if (Log.INCLUDE) {
            Log.d(TAG, "notifyProfileUpdate() intent=" + intent.getStringExtra(PushMessageReceiver.KEY_PROFILE_ID));
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_playback);

        mChannelDataManager = TvSingletons.getSingletons(this).getChannelDataManager();
        mProgramDataManager = TvSingletons.getSingletons(this).getProgramDataManager();
        mTvInputManagerHelper = TvSingletons.getSingletons(this).getTvInputManagerHelper();

        ViewGroup parentScene = findViewById(R.id.controllerFrame);
        mTvOverlayManager = new TvOverlayManager(this,
                parentScene,
                mChannelDataManager,
                mTvInputManagerHelper,
                mProgramDataManager);
        mTvOverlayManager.showBootDialog(BootDialogFragment.TYPE_BOOT, (b) -> authenticationListener(b));

        ViewGroup loadingView = (ViewGroup) LayoutInflater.from(this).inflate(R.layout.view_loading, findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);

    }

    @Override
    protected void onStart() {
        super.onStart();
        initBroadcast();
//        onStartProcess();
        int resultCode = AccountManager.getInstance().popResultCode();
        if (resultCode != RESULT_OK) {
            onActivityResult(RESULT_OK, resultCode, null);
        }
    }

    private void onResumeProcess() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onResumeProcess");
        }
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MenuNavigationFragment.CLASS_NAME);
        if (fragment == null || !fragment.isAdded()) {
            return;
        }
        MenuNavigationFragment mMenuNavigationFragment = (MenuNavigationFragment) fragment;
        Intent intent = getIntent();
        if (intent == null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onResumeProcess : intent == null");
            }
            if (TvSingletons.getSingletons(this).getBackendKnobs().needMenuUpdated(true)) {
                onMenuVersionChanged();
            } else {
                mMenuNavigationFragment.menuContainerRenewal();
            }
        } else {
            String action = intent.getAction();
            if (Log.INCLUDE) {
                Log.d(TAG, "onResumeProcess : action : " + action);
            }
            if (StringUtils.nullToEmpty(action).isEmpty()) {
                if (TvSingletons.getSingletons(this).getBackendKnobs().needMenuUpdated(true)) {
                    onMenuVersionChanged();
                } else {
                    mMenuNavigationFragment.openMenu(false);
                }
            } else {
                if (action.equalsIgnoreCase(Intent.ACTION_ALL_APPS)) {
                    ReminderAlertManager.getInstance().clearAllDialog();
                    CWMPServiceProvider.getInstance().notifyHotKeyClicked(KeyEvent.KEYCODE_ALL_APPS);
                    if (mTvOverlayManager != null) {
                        mTvOverlayManager.hideOverlays(TvOverlayManager.FLAG_HIDE_OVERLAYS_KEEP_MENU);
                    }
                    if (TvSingletons.getSingletons(this).getBackendKnobs().needMenuUpdated(true)) {
                        onMenuVersionChanged(action);
                    } else {
                        mMenuNavigationFragment.jumpAppList();
                    }
                } else if (action.equalsIgnoreCase(Intent.ACTION_VIEW)) {
                    if (!mMenuNavigationFragment.isInitialized()) {
                        mMenuNavigationFragment.jumpHome(hasStopped);
                    }
                } else if (action.equalsIgnoreCase(Intent.ACTION_MAIN)) {
                    if (!mMenuNavigationFragment.isInitialized()) {
                        ReminderAlertManager.getInstance().clearAllDialog();
                        if (mTvOverlayManager != null) {
                            mTvOverlayManager.hideOverlays(TvOverlayManager.FLAG_HIDE_OVERLAYS_KEEP_MENU);
                        }
                        if (TvSingletons.getSingletons(this).getBackendKnobs().needMenuUpdated(true)) {
                            onMenuVersionChanged(action);
                        } else {
                            mMenuNavigationFragment.jumpHome(hasStopped);
                        }
                    }
                } else if (action.equalsIgnoreCase(Linker.ACTION_START_MYMENU)) {
                    if (mTvOverlayManager != null) {
                        mTvOverlayManager.hideOverlays(TvOverlayManager.FLAG_HIDE_OVERLAYS_KEEP_MENU);
                    }
                    mMenuNavigationFragment.mSidePanel.showMyMenu(true);
                    if (TvSingletons.getSingletons(this).getBackendKnobs().needMenuUpdated(true)) {
                        onMenuVersionChanged(action);
                    }
                }
            }
        }
    }

    private boolean hasStopped = false;
    @Override
    protected void onStop() {
        super.onStop();
        hasStopped = true;
        deInitBroadcast();
    }

    private void initBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PushMessageReceiver.ACTION_LOGIN);
        intentFilter.addAction(PushMessageReceiver.ACTION_UPDATED_PROFILE);
        intentFilter.addAction(PushMessageReceiver.ACTION_UPDATED_MENU);
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private void deInitBroadcast() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }

    public void authenticationListener(boolean success) {
        if (Log.INCLUDE) {
            Log.d(TAG, "authenticationListener() success:" + success);
            Log.d(TAG, "authenticationListener() Said:" + AccountManager.getInstance().getSaId());
            Log.d(TAG, "authenticationListener() ProfileId:" + AccountManager.getInstance().getProfileId());
        }

        //initialize channel when authentication completed.
        JasChannelManager.getInstance().initMBS(success);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onKeyDown | keyCode : " + keyCode);
        }

        if (mTvOverlayManager.onKeyTvKey(keyCode, event)) {
            return true;
        }
        switch (keyCode) {

            case KeyEvent.KEYCODE_GUIDE:
            case KeyEvent.KEYCODE_TV:
            case KeyEvent.KEYCODE_BUTTON_1:
            case KeyEvent.KEYCODE_CHANNEL_UP:
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                if (!TvSingletons.getSingletons(this).getBackendKnobs().isEpgReady(true)) {
                    String errorTitle = getString(R.string.error_title_dbs);
                    TvSingletons tvSingletons = TvSingletons.getSingletons(this);
                    ChannelErrorDialogFragment dialogFragment = ChannelErrorDialogFragment.newInstance(
                            tvSingletons.getChannelDataManager(), keyCode, TAG, errorTitle, tvSingletons.getBackendKnobs());
                    dialogFragment.show(getSupportFragmentManager(), ChannelErrorDialogFragment.CLASS_NAME);

                    //check READ_TV_LISTINGS permission
                    if (!PermissionUtils.hasReadTvListings(this)) {
                        PermissionUtils.requestPermissions(this, PermissionUtils.PERMISSION_READ_TV_LISTINGS, 0);
                    }

                    break;
                }
                if (keyCode == KeyEvent.KEYCODE_GUIDE) {
                    CWMPServiceProvider.getInstance().notifyHotKeyClicked(keyCode);
                    Intent intent = new Intent(PlaybackUtil.ACTION_START_GUIDE);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else if (keyCode == KeyEvent.KEYCODE_TV) {
                    Intent intent = new Intent(PlaybackUtil.ACTION_START_LIVE);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else if (keyCode == KeyEvent.KEYCODE_BUTTON_1) {
//                    Intent intent = new Intent(PlaybackUtil.ACTION_START_FAV_GUIDE);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
                    Linker.gotoMyMenu(mTvOverlayManager, getSupportFragmentManager(), this);
                } else if (keyCode == KeyEvent.KEYCODE_CHANNEL_UP || keyCode == KeyEvent.KEYCODE_CHANNEL_DOWN) {
                    Channel currentChannel = mChannelDataManager.getGuideChannel();
                    Channel nextChannel = mChannelDataManager.getNextChannel(currentChannel, keyCode == KeyEvent.KEYCODE_CHANNEL_UP);
                    if (nextChannel == null) {
                        nextChannel = currentChannel;
                    }
                    PlaybackUtil.startLiveTvActivityWithChannel(this, nextChannel.getId());
                }

                return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    public TvOverlayManager getTvOverlayManager() {
        return mTvOverlayManager;
    }

    public List<Channel> getBrowsableChannelList() {
        return mChannelDataManager.getBrowsableChannelList();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        addFocusChangeListener(getWindow().getDecorView());

    }

    @Override
    public void show(String tag) {
    }

    @Override
    public void hide(String tag) {
    }


    protected void addFocusChangeListener(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup parentView = (ViewGroup) view;
            int viewSize = parentView.getChildCount();
            for (int i = 0; i < viewSize; i++) {
                View childView = ((ViewGroup) view).getChildAt(i);
                addFocusChangeListener(childView);
            }
        }
        view.setOnFocusChangeListener(this);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onFocusChange : " + view + ", b :" + b);
        }
    }

    public ChannelDataManager getChannelDataManager() {
        return mChannelDataManager;
    }

    @Override
    public void launchApp(LaunchPath path, String action) {
        if (Log.INCLUDE) {
            Log.d(TAG, "launchApp " + path + ", action : " + action);
        }

//        try {
//            Intent actionIntent = getPackageManager().getLeanbackLaunchIntentForPackage(action);
//            if (Log.INCLUDE) {
//                Log.d(TAG, "actionIntent : " + actionIntent);
//            }
//
//            if (actionIntent != null) {
//                actionIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(actionIntent);
//                return;
//            }
//        } catch (ActivityNotFoundException ex2) {
//            if (Log.INCLUDE) {
//                Log.d(TAG, "launchApp, ActivityNotFoundException, request to show google store, action : " + action);
//            }
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + action)));
//        }
        ExternalApplicationManager.getInstance().launchApp(this, action, true);
    }

    @Override
    public void startActivityFromFragment(@NonNull Fragment fragment, Intent intent, int requestCode) {
        super.startActivityFromFragment(fragment, intent, requestCode);
        if (Log.INCLUDE) {
            Log.d(TAG, "call startActivityFromFragment");
        }
    }

    @Override
    public void startActivityFromFragment(@NonNull Fragment fragment, Intent intent, int requestCode, @Nullable Bundle options) {
        super.startActivityFromFragment(fragment, intent, requestCode, options);
        if (Log.INCLUDE) {
            Log.d(TAG, "call startActivityFromFragment");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Log.INCLUDE) {
            Log.d(TAG, "onActivityResult : " + resultCode);
        }
        if (resultCode == RESULT_CODE_USER_DELETED) {
            onProfileDeleted();
            return;
        }
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MenuNavigationFragment.CLASS_NAME);
        if (resultCode == RESULT_CODE_LIVE_TV) {
            if (fragment != null && fragment.isAdded()) {
                MenuNavigationFragment menuNavigationFragment = (MenuNavigationFragment) fragment;
                menuNavigationFragment.notifyFocusLiveTv();
            }
            return;
        }

        boolean updateReady = TvSingletons.getSingletons(this).getBackendKnobs().needMenuUpdated(false);

        if (fragment != null && fragment.isAdded() && !updateReady) {
            MenuNavigationFragment menuNavigationFragment = (MenuNavigationFragment) fragment;
            menuNavigationFragment.notifyHomeUserContentsChanged();
        }
    }

    public void onProfileInformationChanged(PushMessageReceiver.ProfileUpdateType type, String id) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MenuNavigationFragment.CLASS_NAME);
        if (fragment != null && fragment.isAdded()) {
            MenuNavigationFragment menuNavigationFragment = (MenuNavigationFragment) fragment;
            menuNavigationFragment.notifyProfileStateChanged(type, id);
        }
    }

    public void onMenuVersionChanged() {
        onMenuVersionChanged(null);
    }

    public void onMenuVersionChanged(String action) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MenuNavigationFragment.CLASS_NAME);
        if (fragment != null && fragment.isAdded()) {
            MenuNavigationFragment menuNavigationFragment = (MenuNavigationFragment) fragment;
            menuNavigationFragment.notifyCheckMenuVersion(action, false);
        }
    }

    public void onLoginInformationChanged() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MenuNavigationFragment.CLASS_NAME);
        if (fragment != null && fragment.isAdded()) {
            MenuNavigationFragment menuNavigationFragment = (MenuNavigationFragment) fragment;
            menuNavigationFragment.notifyLoginStateChanged();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Log.INCLUDE) {
            Log.d(TAG, "called onPause");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Log.INCLUDE) {
            Log.d(TAG, "called onDestroy");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Log.INCLUDE) {
            Log.d(TAG, "called onResume");
        }
        onResumeProcess();
        hasStopped = false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (Log.INCLUDE) {
            Log.d(TAG, "called onResumeFragments");
        }
    }
}
