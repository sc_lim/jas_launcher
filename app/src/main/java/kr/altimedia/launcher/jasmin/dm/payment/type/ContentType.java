package kr.altimedia.launcher.jasmin.dm.payment.type;

public enum ContentType {
    VOD("vod"), CHANNEL("channel");

    private String type;

    ContentType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static ContentType findByName(String code) {
        ContentType[] types = values();
        for (ContentType contentType : types) {
            if (contentType.type.equalsIgnoreCase(code)) {
                return contentType;
            }
        }

        return VOD;
    }
}
