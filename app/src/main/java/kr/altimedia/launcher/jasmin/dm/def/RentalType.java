/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.def;

import kr.altimedia.launcher.jasmin.R;

public enum RentalType {
    RENT("Rent", R.string.vod_rent), BUY("Buy", R.string.vod_buy), SUBSCRIBE("Subscription", R.string.vod_subscribe),
    ERROR("ERROR", R.string.error_title_mbs);

    private final String type;
    private final int mName;

    RentalType(String type, int mName) {
        this.type = type;
        this.mName = mName;
    }

    public static RentalType findByName(String name) {
        RentalType[] types = values();
        for (RentalType rentalType : types) {
            if (rentalType.type.equalsIgnoreCase(name)) {
                return rentalType;
            }
        }

        return ERROR;
    }

    public int getName() {
        return mName;
    }
}
