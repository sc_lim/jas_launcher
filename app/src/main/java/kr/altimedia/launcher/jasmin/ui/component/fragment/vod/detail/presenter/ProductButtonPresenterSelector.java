/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Space;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.PresenterSelector;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PackageContent;
import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.VodProductType;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class ProductButtonPresenterSelector extends PresenterSelector {
    private final String TAG = ProductButtonPresenterSelector.class.getSimpleName();

    private final Presenter mOneLineProductPresenter = new OneLineProductPresenter();
    private final Presenter mTwoLineProductPresenter = new TwoLineProductPresenter();
    private final Presenter mSeriesProductPresenter = new SeriesProductPresenter();
    private final Presenter[] mPresenters = new Presenter[]{
            mOneLineProductPresenter, mTwoLineProductPresenter, mSeriesProductPresenter};

    private final ProductType productType;

    public ProductButtonPresenterSelector(ProductType productType) {
        this.productType = productType;
    }

    @Override
    public Presenter getPresenter(Object item) {
        if (item instanceof Content || item instanceof PackageContent) {
            return new MyListButtonPresenter();
        }

        if (item instanceof ProductButtonItem) {
            switch (productType) {
                case SINGLE:
                case PACKAGE:
                    return mTwoLineProductPresenter;
                case SERIES:
                    return mSeriesProductPresenter;
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "getPresenter, invalid item : " + item);
        }

        return null;
    }

    @Override
    public Presenter[] getPresenters() {
        return mPresenters;
    }

    static class ProductViewHolder extends Presenter.ViewHolder {
        TextView title;
        TextView value;
        TextView unit;
        Space space;

        int mLayoutDirection;

        public ProductViewHolder(View view, int layoutDirection) {
            super(view);
            title = view.findViewById(R.id.title);
            value = view.findViewById(R.id.value);
            unit = view.findViewById(R.id.unit);
            space = view.findViewById(R.id.space);
            mLayoutDirection = layoutDirection;
        }
    }

    abstract static class ProductPresenter extends Presenter {
        @Override
        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        }

        @Override
        public void onUnbindViewHolder(ViewHolder viewHolder) {
        }
    }

    class OneLineProductPresenter extends ProductPresenter {
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.vod_product_button, parent, false);
            return new ProductViewHolder(v, parent.getLayoutDirection());
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
            super.onBindViewHolder(viewHolder, item);
            ProductViewHolder vh = ((ProductViewHolder) viewHolder);

            ProductButtonItem productButtonItem = (ProductButtonItem) item;
            String type = vh.view.getContext().getString(productButtonItem.getProductType().getName());

            vh.title.setText(type);
        }

        @Override
        public void onUnbindViewHolder(ViewHolder viewHolder) {
            super.onUnbindViewHolder(viewHolder);
        }
    }

    public static class TwoLineProductPresenter extends ProductPresenter {
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.vod_product_button, parent, false);
            return new ProductViewHolder(v, parent.getLayoutDirection());
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
            super.onBindViewHolder(viewHolder, item);
            ProductViewHolder vh = (ProductViewHolder) viewHolder;

            ProductButtonItem productButtonItem = (ProductButtonItem) item;
            VodProductType productType = productButtonItem.getProductType();
            String type = vh.view.getContext().getString(productType.getName());
            String unit = productButtonItem.getUnit();
            float value = productButtonItem.getValue();

            vh.title.setText(type);
            if (value != -1) {
                vh.unit.setText(unit);
                vh.space.setVisibility(View.VISIBLE);
                if ((productType == VodProductType.SUBSCRIBE || productType == VodProductType.PACKAGE) && value > 1) {
                    vh.value.setText(String.valueOf((int) value));
                } else {
                    vh.value.setText(StringUtil.getFormattedPrice(value));
                }
            } else {
                vh.value.setText(unit);
            }

            //error case for expireDate
            String left = viewHolder.view.getContext().getString(R.string.left);
            if (value == -1 && unit.contains(left)) {
                vh.value.setText("-1");
            }
        }
    }

    public static class SeriesProductPresenter extends ProductPresenter {
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.series_vod_product_button, parent, false);
            return new ProductViewHolder(v, parent.getLayoutDirection());
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
            super.onBindViewHolder(viewHolder, item);
            ProductViewHolder vh = (ProductViewHolder) viewHolder;

            ProductButtonItem productButtonItem = (ProductButtonItem) item;
            VodProductType productType = productButtonItem.getProductType();
            String type = vh.view.getContext().getString(productType.getName());
            String unit = productButtonItem.getUnit();
            float value = productButtonItem.getValue();

            vh.title.setText(type);
            if (value != -1) {
                vh.unit.setText(unit);
                vh.space.setVisibility(View.VISIBLE);
                if ((productType == VodProductType.SUBSCRIBE || productType == VodProductType.PACKAGE) && value > 1) {
                    vh.value.setText(String.valueOf((int) value));
                } else {
                    vh.value.setText(StringUtil.getFormattedPrice(value));
                }
            } else {
                vh.value.setText(unit);
            }

            //error case for expireDate
            String left = viewHolder.view.getContext().getString(R.string.left);
            if (value == -1 && unit.contains(left)) {
                vh.value.setText("-1");
            }
        }
    }

    public static class MyListButtonPresenter extends Presenter {

        @Override
        public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_my_list_button, parent, false);
            return new MyListButtonPresenter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
            MyListButtonPresenter.ViewHolder vh = (MyListButtonPresenter.ViewHolder) viewHolder;
            vh.setData(item);
        }

        @Override
        public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
            viewHolder.view.setOnClickListener(null);
        }

        public class ViewHolder extends Presenter.ViewHolder {
            private CheckBox myListButton;

            public ViewHolder(View view) {
                super(view);
                initView(view);
            }

            private void initView(View view) {
                myListButton = view.findViewById(R.id.my_list_button);
            }

            private void setData(Object obj) {
                if (obj instanceof Content) {
                    Content content = (Content) obj;
                    myListButton.setChecked(content.isLike());
                } else if (obj instanceof PackageContent) {
                    PackageContent packageDetailData = (PackageContent) obj;
                    myListButton.setChecked(packageDetailData.isLike());
                }
            }

            public void toggleMyList(Object obj) {
                boolean isLike = false;

                if (obj instanceof Content) {
                    Content content = (Content) obj;
                    isLike = content.isLike();
                } else if (obj instanceof PackageContent) {
                    PackageContent packageDetailData = (PackageContent) obj;
                    isLike = packageDetailData.isLike();
                }

                myListButton.setChecked(isLike);
            }
        }
    }
}
