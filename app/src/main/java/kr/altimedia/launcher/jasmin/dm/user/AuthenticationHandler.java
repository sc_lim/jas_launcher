/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;

import androidx.annotation.NonNull;

/**
 * Created by mc.kim on 08,06,2020
 */
public class AuthenticationHandler extends Handler {
    final static int WHAT_START_ACTIVATION = 1;
    final static int WHAT_START_LOGIN_TOKEN = 2;
    final static int WHAT_START_SUBSCRIBER_INFO = 3;
    final static int WHAT_COMPLETE_ACTIVATION = 201;
    final static int WHAT_COMPLETE_LOGIN_TOKEN = 202;
    final static int WHAT_COMPLETE_SUBSCRIBER_INFO = 203;
    final static int WHAT_ERROR_ACTIVATION = 401;
    public final static int WHAT_ERROR_LOGIN_TOKEN = 402;
    final static int WHAT_ERROR_SUBSCRIBER_INFO = 403;

    public final static int WHAT_START_AUTHENTICATION = 0;
    public final static int WHAT_COMPLETE_AUTHENTICATION = 200;
    public final static int WHAT_ERROR_AUTHENTICATION = 400;
    public final static int WHAT_ERROR_AUTHENTICATION_NOT_CONNECTED = 500;


    public final static String FAIL_ERROR_CODE = "TimeOut";
    public final static String FAIL_ERROR_MESSAGE = "TimeOut";

    public final static String KEY_ACTIVATION = "ACTIVATION";
    public final static String KEY_LOGIN_INFO = "LOGIN_TOKEN";
    public final static String KEY_SUBSCRIBER_INFO = "SUBSCRIBER_INFO";

    public final static String KEY_ERROR_TYPE = "ERROR_TYPE";
    public final static String KEY_ERROR_CODE = "ERROR_CODE";
    public final static String KEY_ERROR_MESSAGE = "ERROR_MESSAGE";

    private AuthenticationHandler parentHandler;
    private Bundle authenticationBundle = new Bundle();

    public AuthenticationHandler(@NonNull Looper looper) {
        super(looper);
    }

    AuthenticationHandler getParentHandler() {
        return parentHandler;
    }

    void setParentHandler(AuthenticationHandler parentHandler) {
        this.parentHandler = parentHandler;
    }

    void putString(String key, String value) {
        authenticationBundle.putString(key, value);
    }

    String getString(String key) {
        return authenticationBundle.getString(key);
    }

    void putParcelable(String key, Parcelable value) {
        authenticationBundle.putParcelable(key, value);
    }

    Bundle getAuthenticationBundle() {
        return authenticationBundle;
    }
}
