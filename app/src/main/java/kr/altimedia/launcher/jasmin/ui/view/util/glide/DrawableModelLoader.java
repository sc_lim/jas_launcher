/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util.glide;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.signature.ObjectKey;

import kr.altimedia.launcher.jasmin.ui.view.util.InstalledAppUtil;


/**
 * Created by mc.kim on 10,12,2019
 */
public class DrawableModelLoader implements ModelLoader<InstalledAppUtil.AppInfo, Drawable> {
    private final Context mContext;

    DrawableModelLoader(Context context) {
        mContext = context;
    }

    @Nullable
    @Override
    public LoadData<Drawable> buildLoadData(@NonNull InstalledAppUtil.AppInfo appInfo, int width, int height, @NonNull Options options) {
        return new LoadData<>(new ObjectKey(appInfo),
                new DrawableDataFetcher(mContext, appInfo));
    }

    @Override
    public boolean handles(@NonNull InstalledAppUtil.AppInfo appInfo) {
        return false;
    }

}