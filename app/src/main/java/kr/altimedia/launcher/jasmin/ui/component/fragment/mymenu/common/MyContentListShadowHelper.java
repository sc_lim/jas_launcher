/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common;

import android.view.View;

import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class MyContentListShadowHelper implements VerticalGridView.OnScrollOffsetCallback {
    private final String TAG = MyContentListShadowHelper.class.getSimpleName();

    protected View parentView = null;
    private final View bottomShadow;

    public MyContentListShadowHelper(View bottomShadow) {
        super();

        this.bottomShadow = bottomShadow;
    }

    public void setParentView(View parentView) {
        this.parentView = parentView;
    }

    @Override
    public void onScrolledOffsetCallback(int offset, int remainScroll, int totalScroll) {
//        if(Log.INCLUDE) {
//            Log.d(TAG, "onScrolledOffsetCallback: offset="+offset+", remainScroll="+remainScroll+", totalScroll="+totalScroll);
//        }
        if (totalScroll == 0) {
            return;
        }
        if(remainScroll == 0 || (offset == totalScroll)) {
            bottomShadow.setVisibility(View.GONE);
        }else {
            bottomShadow.setVisibility(View.VISIBLE);
        }
    }
}
