/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.content.Context;
import android.media.tv.TvView;
import android.util.AttributeSet;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.tv.JasTvView;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.util.Log;

public class PipTvView extends JasTvView {
    private static final String TAG = PipTvView.class.getSimpleName();

    private ChannelPreviewIndicator pipPreviewIndicator;
    private PipBlockScreen pipBlockScreen;
    //channel info
    private TextView chNum;
    private TextView chName;
    private TextView progName;

    private AutoHider autoHider;

    public PipTvView(Context context) {
        this(context, null);
    }

    public PipTvView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PipTvView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public PipTvView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(getContext(), R.layout.pip_tv_view, this);
        TvView tvView = findViewById(R.id.tv_view_pip);

        pipPreviewIndicator = findViewById(R.id.pip_preview_indicator);
        pipPreviewIndicator.setTuneAction((ch) -> tuneChannel(ch, false));
        pipBlockScreen = findViewById(R.id.pip_block_screen);
        //pipBlockScreen.setVisible(true);
        chNum = findViewById(R.id.pip_ch_num);
        chName = findViewById(R.id.pip_ch_name);
        progName = findViewById(R.id.pip_program_name);

        autoHider = new AutoHider(5000, this::clearInfo);

        if (Log.INCLUDE) {
            Log.d(TAG, "chNum : " + chNum);
            Log.d(TAG, "chName : " + chName);
            Log.d(TAG, "progName : " + progName);
        }

        init(JasTvView.TYPE_PIP, tvView, pipBlockScreen);
    }

    public void initChannel(Channel ch) {
        if (tuningResult == null || tuningResult == TuningResult.TUNING_NONE) {
            Log.d(TAG, "initChannel() tune...");
            tuneChannel(ch);
        } else {
            Log.d(TAG, "initChannel() already tuned - ignore");
        }
    }

    public Channel getNextChannel(boolean up) {
        Channel nextChannel = mChannelDataManager.getNextChannel(currentChannel, up);
        return nextChannel;
    }

    public void updateInfo(TuningResult tuningResult) {
        Log.d(TAG, "updateText() tuningResult:" + tuningResult);
        if (currentChannel != null) {
            chNum.setText(StringUtil.getFormattedNumber(currentChannel.getDisplayNumber(), 3));
            chName.setText(currentChannel.getDisplayName());

            if (currentChannel.isLocked()) {
                progName.setText(getContext().getString(R.string.program_title_for_blocked_channel));
            } else {
                Program program = getCurrentProgram(currentChannel);
                progName.setText(program != null ? program.getTitle() : "");
            }

            autoHider.refreshTimer();
        } else {
            clearInfo();
        }
    }

    private void clearInfo() {
        chNum.setText("");
        chName.setText("");
        progName.setText("");

        autoHider.cancel();
    }

    @Override
    protected void setPreviewIndicator(Channel channel) {
        pipPreviewIndicator.updateChannelPreview(channel);
    }

    public void setBlockTextSize(int textSize) {
        pipBlockScreen.setBlockTextSize(textSize);
    }

    protected void updateMuting() {
        Log.d(TAG, "updateMuting() mute piptvview");
        tvView.setStreamVolume(0);
    }

    protected void updatePopularChannelData(String json) {
    }
}
