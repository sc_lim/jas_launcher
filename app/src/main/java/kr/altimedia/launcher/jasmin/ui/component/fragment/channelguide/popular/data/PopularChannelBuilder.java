/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.channelguide.popular.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.altimedia.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Iterator;
import java.util.List;

public class PopularChannelBuilder {
    private final String TAG = PopularChannelBuilder.class.getSimpleName();

    public Gson getGsonBuilder() {
        GsonBuilder builder = new GsonBuilder();
        return builder.create();
    }

    public List<PopularChannel> getPopularChannelList(String jsonChannelData) {
        List<PopularChannel> list = null;
        try {
            list = getGsonBuilder().fromJson(jsonChannelData, PopularChannelList.class).getPopularChannelList();

            if(list != null && !list.isEmpty()) {
                // validation#1: remove popular channel with wrong rank
                list = removeWrongRank(list);

                // validation#2: add popular channel with dummy rank
                list = addDummyRank(list);

                if (Log.INCLUDE) {
                    int index = 0;
                    for (PopularChannel pchannel : list) {
                        Log.d(TAG, "getPopularChannelList: ["+ (index++) +"] rank=" + pchannel.getViewingRank());
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    private List<PopularChannel> removeWrongRank(List<PopularChannel> list){
        int index = 0;
        for(Iterator<PopularChannel> it = list.iterator(); it.hasNext() ; ) {
            PopularChannel pchannel = it.next();
            if (pchannel.getViewingRank() <= 0) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "removeWrongRank: [" + index + "] rank=" + pchannel.getViewingRank());
                }
                it.remove();
            }
            index++;
        }

        return list;
    }

    private List<PopularChannel> addDummyRank(List<PopularChannel> list){
        boolean existed = false;
        for (int rank = 1; rank <= 9; rank++) {
            existed = false;
            for (PopularChannel pchannel : list) {
                if (rank == pchannel.getViewingRank()) {
                    existed = true;
                    break;
                }
            }
            if (!existed) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "addDummyRank: dummy rank=" + rank);
                }
                list.add(PopularChannel.buildPopularChannel(rank));
            }
        }
        return list;
    }
    /**
     * PopularChannelList
     */
    private class PopularChannelList implements Parcelable {

        @SerializedName("data")
        @Expose
        private List<PopularChannel> list = null;
        public final Creator<PopularChannelList> CREATOR = new Creator<PopularChannelList>() {
            @SuppressWarnings({
                    "unchecked"
            })
            public PopularChannelList createFromParcel(Parcel in) {
                return new PopularChannelList(in);
            }

            public PopularChannelList[] newArray(int size) {
                return (new PopularChannelList[size]);
            }
        };

        protected PopularChannelList(Parcel in) {
            in.readList(this.list, (PopularChannel.class.getClassLoader()));
        }

        public PopularChannelList() {
        }

        public List<PopularChannel> getPopularChannelList() {
            return list;
        }

        public void setPopularChannelList(List<PopularChannel> list) {
            this.list = list;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeList(list);
        }

        public int describeContents() {
            return 0;
        }
    }
}
