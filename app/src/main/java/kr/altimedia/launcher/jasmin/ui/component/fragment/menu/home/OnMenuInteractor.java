/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home;

import androidx.leanback.app.ProgressBarManager;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.dm.def.CategoryType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public interface OnMenuInteractor {

    void setSideMenuEnable(boolean enable);

    void menuSelected(Category menu, boolean force, boolean closeMenu);

    void renewalMenu(Category menu);

    boolean isMenuShowing();

    void openSideMenu(boolean withAnimation);

    void closeSideMenu(boolean withAnimation, boolean force);

    void onReadyToLoad(CategoryType type, MbsDataProvider<String, List<Category>> mbsDataProvider);

    void needMenuUpdate(MbsDataProvider<String, Boolean> mbsDataProvider);

    Category getCurrentCategory();

    ProgressBarManager getProgressBarManager();

    TvOverlayManager getTvOverlayManager();
}
