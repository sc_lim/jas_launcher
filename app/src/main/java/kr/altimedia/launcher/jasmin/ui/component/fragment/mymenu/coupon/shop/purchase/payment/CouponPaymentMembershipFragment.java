/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.coupon.CouponDataManager;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProduct;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseRequest;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseResult;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class CouponPaymentMembershipFragment extends CouponPaymentBaseFragment {
    public static final String CLASS_NAME = CouponPaymentMembershipFragment.class.getName();
    private final String TAG = CouponPaymentMembershipFragment.class.getSimpleName();

    private static final String KEY_PURCHASED_INFO = "PURCHASED_INFO";

    private TextView minView;
    private TextView secView;
    private CountDownTimer countDownTimer;

    private PurchaseInfo purchaseInfo;
    protected String purchaseId = null;

    private TvOverlayManager mTvOverlayManager = null;

    private CouponPaymentMembershipFragment() {
    }

    public static CouponPaymentMembershipFragment newInstance(PurchaseInfo purchaseInfo) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_PURCHASED_INFO, purchaseInfo);

        CouponPaymentMembershipFragment fragment = new CouponPaymentMembershipFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttached : context");
        }
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_coupon_shop_payment_membership;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        purchaseInfo = getArguments().getParcelable(KEY_PURCHASED_INFO);

        initAccountInfo(view);
        initButton(view);
        initTimer(view);

        requestPurchaseCoupon();
    }

    private void initAccountInfo(View view) {
        AccountManager accountManager = AccountManager.getInstance();

        TextView accountName = view.findViewById(R.id.account_name);
        TextView accountId = view.findViewById(R.id.account_id);

        accountName.setText(accountManager.getAccountName());
        accountId.setText(accountManager.getSaId());
    }

    private void initButton(View view) {
        TextView cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getOnPaymentListener() != null) {
                    getOnPaymentListener().onFragmentDismiss();
                }
            }
        });
    }

    private void initTimer(View view) {
        minView = view.findViewById(R.id.min_counting);
        secView = view.findViewById(R.id.sec_counting);
        setTime(TOTAL_COUNT_DOWN);

        String timeoutErrorMessage = getString(R.string.payment_error);
        countDownTimer = new CountDownTimer(TOTAL_COUNT_DOWN, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                int sec = setTime(millisUntilFinished);
                if (sec != 0 && sec % CHECK_PAYMENT_INTERVAL == 0) {
                    getPurchaseCouponResult();
                }
            }

            @Override
            public void onFinish() {
                if (getOnPaymentListener() != null) {
                    getOnPaymentListener().onPaymentError(timeoutErrorMessage);
                }
            }
        };
    }

    private int setTime(long millisUntilFinished) {
        long min = TimeUtil.getMin(millisUntilFinished);
        long sec = TimeUtil.getSec(millisUntilFinished);

        String minText = min < 10 ? "0" + min : String.valueOf(min);
        String secText = sec < 10 ? "0" + sec : String.valueOf(sec);
        minView.setText(minText);
        secView.setText(secText);

        return (int) sec;
    }

    private void requestPurchaseCoupon() {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestPurchaseCoupon");
        }

        CouponProduct couponProduct = purchaseInfo.getCouponProduct();
        PaymentType paymentType = purchaseInfo.getPaymentType();
        String paymentDetail = "";
        String creditCardType = "";
        String bankCode = "";

        CouponDataManager couponDataManager = new CouponDataManager();
        couponDataManager.requestPurchaseCoupon(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(),
                couponProduct.getId(), paymentType.getCode(), Integer.toString(couponProduct.getPrice()),
                paymentDetail, creditCardType, bankCode,
                new MbsDataProvider<String, CouponPurchaseRequest>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPurchaseCoupon: onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, CouponPurchaseRequest result) {
                        purchaseId = result.getPurchaseId();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPurchaseCoupon: onSuccess: purchasedId=" + purchaseId);
                        }

                        countDownTimer.start();
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPurchaseCoupon: onError: errorCode=" + errorCode + ", message=" + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return getContext();
                    }
                });
    }

    private void getPurchaseCouponResult() {
        if (Log.INCLUDE) {
            Log.d(TAG, "getPurchaseCouponResult: purchaseId=" + purchaseId);
        }

        if (resultTask != null) {
            resultTask.cancel(true);
        }

        if (purchaseId == null) {
            return;
        }

        CouponDataManager couponDataManager = new CouponDataManager();
        resultTask = couponDataManager.getPurchaseCouponResult( purchaseId,
                new MbsDataProvider<String, CouponPurchaseResult>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPurchaseCouponResult: onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, CouponPurchaseResult result) {
                        boolean isSuccess = result.isSuccess();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPurchaseCouponResult: onSuccess: isSuccess=" + isSuccess);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentComplete(isSuccess);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPurchaseCouponResult: onError: errorCode=" + errorCode + ", message=" + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return getContext();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        countDownTimer.cancel();
    }
}
