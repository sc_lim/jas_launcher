/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.recommend.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mc.kim on 01,06,2020
 */
public class RcmdCategory implements Parcelable {
    @Expose
    @SerializedName("MENU_TYPE")
    private String menuType;
    @Expose
    @SerializedName("ITEM_CNT")
    private int itemCnt;
    @Expose
    @SerializedName("MENU_ID")
    private String menuId;
    @Expose
    @SerializedName("MENU_NAME")
    private String menuName;

    protected RcmdCategory(Parcel in) {
        menuType = in.readString();
        itemCnt = in.readInt();
        menuId = in.readString();
        menuName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(menuType);
        dest.writeInt(itemCnt);
        dest.writeString(menuId);
        dest.writeString(menuName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RcmdCategory> CREATOR = new Creator<RcmdCategory>() {
        @Override
        public RcmdCategory createFromParcel(Parcel in) {
            return new RcmdCategory(in);
        }

        @Override
        public RcmdCategory[] newArray(int size) {
            return new RcmdCategory[size];
        }
    };

    @Override
    public String toString() {
        return "RcmdCategory{" +
                "menuType='" + menuType + '\'' +
                ", itemCnt=" + itemCnt +
                ", menuId='" + menuId + '\'' +
                ", menuName='" + menuName + '\'' +
                '}';
    }

    public String getMenuType() {
        return menuType;
    }

    public int getItemCnt() {
        return itemCnt;
    }

    public String getMenuId() {
        return menuId;
    }

    public String getMenuName() {
        return menuName;
    }
}
