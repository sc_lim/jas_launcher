/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.row;

import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;

import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelLayout;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelOptionProvider;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object.SideOptionCategory;

/**
 * Created by mc.kim on 18,02,2020
 */
public class SideOptionButtonRow extends SideOptionRow implements View.OnClickListener {
    private final SideOptionCategory mSideOptionCategory;
    private final SidePanelLayout.SideOptionKeyListener mSideOptionKeyListener;
    private final SidePanelOptionProvider mPanelOptionProvider;

    public SideOptionButtonRow(@NonNull SideOptionCategory sideOptionCategory,
                               @NonNull SidePanelLayout.SideOptionKeyListener sideOptionKeyListener,
                               @NonNull SidePanelOptionProvider panelOptionProvider) {
        this.mSideOptionCategory = sideOptionCategory;
        this.mSideOptionKeyListener = sideOptionKeyListener;
        this.mPanelOptionProvider = panelOptionProvider;
    }

    public SideOptionCategory getOptionType() {
        return mSideOptionCategory;
    }

    public SidePanelOptionProvider getPanelOptionProvider() {
        return mPanelOptionProvider;
    }

    @Override
    public void onClick(View v) {
        mSideOptionKeyListener.onItemSelected(mSideOptionCategory);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (super.onKey(v, keyCode, event)) {
            return true;
        }
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_BACK:
                mSideOptionKeyListener.onBackPressed(mSideOptionCategory);
                return true;

            case KeyEvent.KEYCODE_DPAD_CENTER:
                mSideOptionKeyListener.onItemSelected(mSideOptionCategory);
                return true;
        }
        return false;
    }
}
