/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.rowView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.pager.NotifyKeyViewPager;
import kr.altimedia.launcher.jasmin.ui.view.presenter.ViewPagerPresenter;

public class PageRowView extends LinearLayout implements ViewPagerPresenter.PageRootView {
    private RelativeLayout viewPagerLayer;
    private NotifyKeyViewPager mViewPager;
    private LinearLayout mIndicatorView = null;
    private View mActionButtonView = null;
    private View mBlackScreen = null;

    public PageRowView(Context context) {
        this(context, null);
    }

    public PageRowView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PageRowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.view_row_pager, this);
        viewPagerLayer = view.findViewById(R.id.viewPagerLayer);
        mIndicatorView = view.findViewById(R.id.indicatorView);
        mViewPager = view.findViewById(R.id.viewPager);
        mActionButtonView = view.findViewById(R.id.promotionButton);
        mBlackScreen = view.findViewById(R.id.blackScreen);
    }


    @Override
    public View getParentsView() {
        return viewPagerLayer;
    }

    @Override
    public NotifyKeyViewPager getPager() {
        return mViewPager;
    }

    @Override
    public View getIndicator() {
        return mIndicatorView;
    }

    @Override
    public View getActionView() {
        return mActionButtonView;
    }

    @Override
    public View getBlackScreen() {
        return mBlackScreen;
    }
}
