package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.content.Context;
import android.view.View;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;

/**
 * Created by mc.kim on 03,09,2020
 */
public class FailDialogFragment extends TwoButtonDialogFragment {
    public static final String CLASS_NAME = FailDialogFragment.class.getName();

    public FailDialogFragment(Context context, View.OnClickListener onClickListener) {
        super(context, onClickListener);
    }

    public static TwoButtonDialogFragment newInstance(Context context, View.OnClickListener onClickListener) {
        String title = context.getString(R.string.error_parse);
        String subtitle = ErrorMessageManager.getInstance().getErrorMessage(title);
        String description = "";
        return newInstance(context, title, subtitle, description, null, onClickListener);
    }
}
