/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 14,02,2020
 */
public class VideoPlaybackTransportRowView extends LinearLayout {

    public interface OnUnhandledKeyListener {
        /**
         * Returns true if the key event should be consumed.
         */
        boolean onUnhandledKey(KeyEvent event);
    }

    private VideoPlaybackTransportRowView.OnUnhandledKeyListener mOnUnhandledKeyListener;

    public VideoPlaybackTransportRowView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VideoPlaybackTransportRowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setOnUnhandledKeyListener(VideoPlaybackTransportRowView.OnUnhandledKeyListener listener) {
        mOnUnhandledKeyListener = listener;
    }

    VideoPlaybackTransportRowView.OnUnhandledKeyListener getOnUnhandledKeyListener() {
        return mOnUnhandledKeyListener;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (super.dispatchKeyEvent(event)) {
            return true;
        }
        return mOnUnhandledKeyListener != null && mOnUnhandledKeyListener.onUnhandledKey(event);
    }

    @Override
    protected boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
        final View focused = findFocus();
        if (focused != null && focused.requestFocus(direction, previouslyFocusedRect)) {
            return true;
        }
        View progress = findViewById(R.id.playback_progress);
        if (progress != null && progress.isFocusable()) {
            if (progress.requestFocus(direction, previouslyFocusedRect)) {
                return true;
            }
        }
        return super.onRequestFocusInDescendants(direction, previouslyFocusedRect);
    }

    @Override
    public View focusSearch(View focused, int direction) {
        // when focusSearch vertically, return the next immediate focusable child
        if (focused != null) {
            if (direction == View.FOCUS_UP) {
                int index = indexOfChild(getFocusedChild());
                for (index = index - 1; index >= 0; index--) {
                    View view = getChildAt(index);
                    if (view.hasFocusable()) {
                        return view;
                    }
                }
            } else if (direction == View.FOCUS_DOWN) {
                int index = indexOfChild(getFocusedChild());
                for (index = index + 1; index < getChildCount(); index++) {
                    View view = getChildAt(index);
                    if (view.hasFocusable()) {
                        return view;
                    }
                }
            } else if (direction == View.FOCUS_LEFT || direction == View.FOCUS_RIGHT) {
                if (getFocusedChild() instanceof ViewGroup) {
                    return FocusFinder.getInstance().findNextFocus(
                            (ViewGroup) getFocusedChild(), focused, direction);
                }
            }
        }
        return super.focusSearch(focused, direction);
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
