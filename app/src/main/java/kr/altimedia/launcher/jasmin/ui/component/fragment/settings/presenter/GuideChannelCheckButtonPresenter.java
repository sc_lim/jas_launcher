/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.common.CheckTextView;

public class GuideChannelCheckButtonPresenter extends Presenter {
    private final float CHECK_VIEW_DEFAULT_ALPHA = 1.0f;
    private final float CHECK_VIEW_DIM_ALPHA = 0.4f;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_check_button, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        CheckButtonItem buttonItem = (CheckButtonItem) item;
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData(buttonItem);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    private class ViewHolder extends Presenter.ViewHolder {
        private CheckTextView checkTextView;

        private ViewHolder(View view) {
            super(view);

            initView(view);
        }

        private void initView(View view) {
            checkTextView = view.findViewById(R.id.check_button);
        }

        public void setData(CheckButtonItem buttonItem) {
            Context context = view.getContext();

            String title = context.getString(buttonItem.getTitle());
            checkTextView.setText(title);

            checkTextView.setWidthTextView(R.dimen.setting_guide_channel_check_text_view_width);
            checkTextView.setCheckIconLayoutAlignment(RelativeLayout.ALIGN_PARENT_LEFT, R.dimen.setting_text_view_icon_left_margin, -1, R.dimen.setting_text_view_icon_right_margin, -1);
            checkTextView.setMaxLineTextView(1);
            checkTextView.setTextGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            checkTextView.setTextLayoutAlignment(RelativeLayout.ALIGN_PARENT_LEFT, R.dimen.setting_guide_channel_check_text_view_text_left_margin, -1, -1, -1);

            boolean isEnable = buttonItem.isEnabled();
            float alpha = isEnable ? CHECK_VIEW_DEFAULT_ALPHA : CHECK_VIEW_DIM_ALPHA;
            checkTextView.setEnabled(isEnable, alpha);

            if (buttonItem.isEnabledCheck()) {
                checkTextView.setChecked(buttonItem.isChecked());
            } else {
                checkTextView.setVisibilityCheckBox(View.INVISIBLE);
            }
        }
    }

    public interface CheckButtonItem {
        int getType();

        int getTitle();

        boolean isEnabled();

        boolean isEnabledCheck();

        boolean isChecked();

    }
}
