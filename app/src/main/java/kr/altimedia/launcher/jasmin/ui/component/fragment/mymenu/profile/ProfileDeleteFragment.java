/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfile;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

public class ProfileDeleteFragment extends Fragment {
    public static final String CLASS_NAME = ProfileDeleteFragment.class.getName();
    private static final String TAG = ProfileDeleteFragment.class.getSimpleName();

    private final String ERROR_OTHER_DEVICE_PROFILE = "MBS_1055";

    private TextView profileName;
    private TextView btnKeepProfile;
    private TextView btnDeleteProfile;

    private UserProfile profile;

    @NotNull
    public static ProfileDeleteFragment newInstance(UserProfile profile) {
        ProfileDeleteFragment fragment = new ProfileDeleteFragment();
        Bundle args = new Bundle();
        args.putParcelable(ProfileManageDialog.KEY_PROFILE, profile);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mymenu_profile_delete, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        if(bundle != null){
            profile = bundle.getParcelable(ProfileManageDialog.KEY_PROFILE);
        }

        initButton(view);
    }

    private void initButton(View view) {

        profileName = view.findViewById(R.id.profileName);
        btnKeepProfile = view.findViewById(R.id.btnKeepProfile);
        btnDeleteProfile = view.findViewById(R.id.btnDeleteProfile);

        if (profile != null) {
            profileName.setText(profile.getName());
        }

        btnKeepProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                            ((ProfileManageDialog)getParentFragment()).showProfileView(ProfileManageDialog.TYPE_LIST, null, null);
                ((ProfileManageDialog) getParentFragment()).backToFragment();
            }
        });

        btnDeleteProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProfile();
            }
        });

        btnKeepProfile.requestFocus();
    }

    private void deleteProfile() {
        if (profile == null) {
            return;
        }

        String profileId = profile.getProfileInfo().getProfileId();
        if (Log.INCLUDE) {
            Log.d(TAG, "deleteProfile: id=" + profileId);
        }
        String profileName = profile.getProfileInfo().getProfileName();
        UserDataManager userDataManager = new UserDataManager();
        userDataManager.deleteProfile(
                AccountManager.getInstance().getSaId(),
                profileId,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
//                        if (loading) {
//                            showProgress();
//                        } else {
//                            hideProgress();
//                        }
                    }

                    @Override
                    public void onFailed(int key) {
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        Context context = ProfileDeleteFragment.this.getContext();
                        JasminToast.makeToast(context, R.string.deleted);

                        notifyProfileDeleted(profileId);

                        ((ProfileManageDialog) getParentFragment()).hideProfileView(profileName);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if(ERROR_OTHER_DEVICE_PROFILE.equals(errorCode)) {
                            hideView();
                            ((ProfileManageDialog) getParentFragment()).showProfileView(ProfileManageDialog.TYPE_DELETE_ERROR, profile, null);

                        }else {
                            ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                            errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                        }
                    }
                });
    }

    private void notifyProfileDeleted(String id){
        Intent intent = new Intent();
        intent.setAction(ProfileManageDialog.ACTION_MY_PROFILE_DELETED);
        intent.putExtra(ProfileManageDialog.KEY_PROFILE_ID, id);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    private void hideView(){
        ProfileManageDialog manageDialog = ((ProfileManageDialog) getParentFragment());
//        if (manageDialog.isLinkFromMyMenu()) {
//            manageDialog.dismiss();
//        } else {
        manageDialog.backToFragment();
//        }
    }
}
