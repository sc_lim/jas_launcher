/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.view.common.ScrollDescriptionTextButton;
import kr.altimedia.launcher.jasmin.ui.view.common.ThumbScrollbar;

public class CouponTermsDialog extends SafeDismissDialogFragment implements View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = CouponTermsDialog.class.getName();
    private static final String TAG = CouponTermsDialog.class.getSimpleName();

    private static final String KEY_TERMS_CONDITION = "TERMS_CONDITION";

    private View rootView;
    private String termsAndConditions = "";

    private CouponTermsDialog() {
    }

    public static CouponTermsDialog newInstance(String termsAndConditions) {
        Bundle args = new Bundle();
        args.putString(KEY_TERMS_CONDITION, termsAndConditions);
        CouponTermsDialog fragment = new CouponTermsDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_coupon_shop_terms, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        initData();
        initView(view);
    }

    private void initData() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            termsAndConditions = arguments.getString(KEY_TERMS_CONDITION);
        }
    }

    private void initView(View view) {
        initScrollbar(view);
    }

    private void initScrollbar(View view) {
        TextView termsConditions = view.findViewById(R.id.termsConditions);
        ThumbScrollbar termsScrollbar = view.findViewById(R.id.termsScrollbar);

        if (termsAndConditions != null) {
            termsConditions.setText(termsAndConditions);
        }

        termsConditions.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                termsScrollbar.setList(termsConditions.getLineCount(), termsConditions.getMaxLines());
            }
        });
        termsConditions.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                boolean isMove = scrollY > oldScrollY;
                termsScrollbar.moveScroll(isMove);
            }
        });

        ScrollDescriptionTextButton btnClose = view.findViewById(R.id.btnClose);
        btnClose.setDescriptionTextView(termsConditions);
        btnClose.setOnButtonKeyListener(new ScrollDescriptionTextButton.OnButtonKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        dismiss();
                        return true;
                }
                return false;
            }
        });
    }
}
