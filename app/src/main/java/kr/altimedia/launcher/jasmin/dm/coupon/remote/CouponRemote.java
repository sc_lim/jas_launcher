/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.remote;


import com.altimedia.util.Log;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.List;

import kr.altimedia.launcher.jasmin.dm.AuthenticationException;
import kr.altimedia.launcher.jasmin.dm.MbsUtil;
import kr.altimedia.launcher.jasmin.dm.ResponseException;
import kr.altimedia.launcher.jasmin.dm.coupon.api.CouponApi;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponHomeInfo;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProductList;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseRequest;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseResult;
import kr.altimedia.launcher.jasmin.dm.coupon.object.MyCoupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.CouponHomeInfoResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.CouponProductListResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.CouponPurchaseRequestResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.CouponPurchaseResultResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.CouponRegisterResponse;
import kr.altimedia.launcher.jasmin.dm.coupon.object.response.MyCouponListResponse;
import retrofit2.Call;
import retrofit2.Response;

public class CouponRemote {
    private static final String TAG = CouponRemote.class.getSimpleName();
    private CouponApi mCouponApi;

    public CouponRemote(CouponApi couponApi) {
        mCouponApi = couponApi;
    }

    public CouponHomeInfo getCouponHomeInfo(String said, String profileId, boolean includeCoupons) throws IOException, ResponseException, AuthenticationException {
        Call<CouponHomeInfoResponse> call = mCouponApi.getCouponHomeInfo(said, profileId, includeCoupons);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<CouponHomeInfoResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        CouponHomeInfoResponse couponHomeInfoResponse = response.body();
        return couponHomeInfoResponse.getCouponHomeInfo();
    }

    public List<MyCoupon> getMyCouponList(String said, String profileId, String searchTarget, boolean includeHistory) throws IOException, ResponseException, AuthenticationException {
        Call<MyCouponListResponse> call = mCouponApi.getMyCouponList(said, profileId, searchTarget, includeHistory);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<MyCouponListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        MyCouponListResponse mListResponse = response.body();
        return mListResponse.getCouponList();
    }

    public MyCoupon postRegisterCoupon(String said, String profileId, String couponNumber) throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("couponNumber", couponNumber);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("said", said);
        Call<CouponRegisterResponse> call = mCouponApi.postRegisterCoupon(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<CouponRegisterResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        CouponRegisterResponse couponRegisterResponse = response.body();
        return couponRegisterResponse.getResult();
    }

    public CouponProductList getCouponProductList(String said, String profileId) throws IOException, ResponseException , AuthenticationException{
        Call<CouponProductListResponse> call = mCouponApi.getCouponProductList(said, profileId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<CouponProductListResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        CouponProductListResponse mListResponse = response.body();
        return mListResponse.getCouponProductList();
    }

    public CouponPurchaseRequest requestPurchaseCoupon(String said, String profileId, String cpnTypeId, String paymentMethod, String totalPrice, String paymentDetail, String creditCardType, String bankCode)
            throws IOException, ResponseException, AuthenticationException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("bankCode", bankCode);
        jsonObject.addProperty("creditCardType", creditCardType);
        jsonObject.addProperty("paymentDetail", paymentDetail);
        jsonObject.addProperty("totalPrice", totalPrice);
        jsonObject.addProperty("paymentMethod", paymentMethod);
        jsonObject.addProperty("cpnTypeId", cpnTypeId);
        jsonObject.addProperty("profileId", profileId);
        jsonObject.addProperty("said", said);
        Call<CouponPurchaseRequestResponse> call = mCouponApi.requestPurchaseCoupon(jsonObject);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<CouponPurchaseRequestResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        CouponPurchaseRequestResponse couponPurchaseRequestResponse = response.body();
        return couponPurchaseRequestResponse.getCouponPurchaseRequest();
    }

    public CouponPurchaseResult getPurchaseCouponResult(String purchaseId) throws IOException, ResponseException, AuthenticationException {
        Call<CouponPurchaseResultResponse> call = mCouponApi.getPurchaseCouponResult(purchaseId);
        if (Log.INCLUDE) {
            Log.d(TAG, "call : " + call.request().toString());
            Log.d(TAG, "call : " + call.request().url().toString());
            Log.d(TAG, "call : " + call.request().body());
        }
        Response<CouponPurchaseResultResponse> response = MbsUtil.checkConnectionValid(call);
        if (Log.INCLUDE) {
            Log.d(TAG, "response : " + response.isSuccessful());
            Log.d(TAG, "response : " + response.message());
            Log.d(TAG, "response : " + response.code());
            Log.d(TAG, "response : " + response.errorBody());
        }
        MbsUtil.checkValid(response);
        CouponPurchaseResultResponse couponPurchaseResultResponse = response.body();
        return couponPurchaseResultResponse.getResult();
    }
}
