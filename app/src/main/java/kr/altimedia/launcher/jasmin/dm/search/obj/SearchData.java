
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SearchData implements Parcelable {

    public static final Creator<SearchData> CREATOR = new Creator<SearchData>() {
        @Override
        public SearchData createFromParcel(Parcel in) {
            return new SearchData(in);
        }

        @Override
        public SearchData[] newArray(int size) {
            return new SearchData[size];
        }
    };
    @Expose
    @SerializedName("totalNum")
    private int totalNum = 0;
    @Expose
    @SerializedName("vodNum")
    private int vodNum = 0;
    @Expose
    @SerializedName("epgNum")
    private int epgNum = 0;
//    @Expose
//    @SerializedName("youtubeNum")
//    private int youtubeNum = 0;
    @Expose
    @SerializedName("monomaxNum")
    private int monomaxNum = 0;
    @Expose
    @SerializedName("keywordNum")
    private int keywordNum = 0;
    @Expose
    @SerializedName("VOD_LIST")
    private List<SearchVod> vodList;
    @Expose
    @SerializedName("REALTIME_VOD_LIST")
    private List<SearchChannel> realtimeVodList;
//    @Expose
//    @SerializedName("YOUTUBE_LIST")
//    private List<SearchYoutube> youtubeList;
    @Expose
    @SerializedName("MONOMAX_LIST")
    private List<SearchMonomax> monomaxList;
//    @Expose
//    @SerializedName("SRCH_OPT_LIST")
//    private List<SearchOption> srchOptList;

    protected SearchData(Parcel in) {
        this.totalNum = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.vodNum = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.epgNum = ((Integer) in.readValue((Integer.class.getClassLoader())));
//        this.youtubeNum = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.monomaxNum = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.keywordNum = ((Integer) in.readValue((Integer.class.getClassLoader())));

        this.vodList = new ArrayList<>();
        in.readList(this.vodList, SearchVod.class.getClassLoader());
        this.realtimeVodList = new ArrayList<>();
        in.readList(this.realtimeVodList, SearchChannel.class.getClassLoader());
//        this.youtubeList = new ArrayList<>();
//        in.readList(this.youtubeList, SearchYoutube.class.getClassLoader());
        this.monomaxList = new ArrayList<>();
        in.readList(this.monomaxList, SearchMonomax.class.getClassLoader());

//        srchOptList = in.createTypedArrayList(SearchOption.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(totalNum);
        dest.writeValue(vodNum);
        dest.writeValue(epgNum);
//        dest.writeValue(youtubeNum);
        dest.writeValue(monomaxNum);
        dest.writeValue(keywordNum);

        dest.writeList(vodList);
        dest.writeList(realtimeVodList);
//        dest.writeList(youtubeList);
        dest.writeList(monomaxList);
//        dest.writeList(srchOptList);
    }

    public int getTotalNum() {
        return totalNum;
    }

    public int getVodNum() {
        return vodNum;
    }

    public int getEpgNum() {
        return epgNum;
    }

//    public int getYoutubeNum() {
//        return youtubeNum;
//    }

    public int getMonomaxNum() {
        return monomaxNum;
    }

    public int getKeywordNum() {
        return keywordNum;
    }

    public List<SearchVod> getVodList() {
        return vodList;
    }

    public List<SearchChannel> getRealtimeVodList() {
        return realtimeVodList;
    }

//    public List<SearchYoutube> getYoutubeList() {
//        return youtubeList;
//    }

    public List<SearchMonomax> getMonomaxList() {
        return monomaxList;
    }

//    public List<SearchOption> getSrchOptList() {
//        return srchOptList;
//    }

    @Override
    public int describeContents() {
        return 0;
    }
}