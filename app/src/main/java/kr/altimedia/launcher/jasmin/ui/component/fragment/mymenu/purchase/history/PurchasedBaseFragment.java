/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.def.RentalType;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.dm.user.object.PurchasedContent;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history.adapter.PurchasedListBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class PurchasedBaseFragment extends Fragment {
    public static final String CLASS_NAME = PurchasedBaseFragment.class.getName();
    private static final String TAG = PurchasedBaseFragment.class.getSimpleName();

    protected ProgressBarManager progressBarManager;

    protected ArrayList<PurchasedContent> purchasedList = new ArrayList<>();
    protected ArrayList<PurchasedContent> hiddenList = new ArrayList<>();

    protected TextView countView;
    protected LinearLayout emptyVodLayer;
    protected LinearLayout contentLayer;
    protected PagingVerticalGridView gridView;
    protected PurchasedListBridgeAdapter gridBridgeAdapter;
    protected ArrayObjectAdapter gridObjectAdapter = null;

    protected LinearLayout infoLayer;
    protected TextView titleView;
    protected TextView dateView;
    protected TextView paymentView;
    protected TextView priceView;
    protected TextView priceUnitView;
    protected TextView periodView;
    protected TextView periodUnitView;

    protected TextView btnEdit;
    protected LinearLayout btnGroupLayer;
    protected TextView btnSelectAll;
    protected TextView btnDelete;
    protected TextView btnClose;

    protected TvOverlayManager mTvOverlayManager = null;

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        if (progressBarManager != null) {
            progressBarManager.hide();
            progressBarManager.setRootView(null);
        }
        super.onDestroyView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach: " + mTvOverlayManager);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mymenu_purchased_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
    }

    private void initView(View view) {
        initLoadingBar(view);
        initGridView(view);
        initInfoView(view);
    }

    private void initLoadingBar(View view) {
        progressBarManager = new ProgressBarManager();
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);
    }

    private void initGridView(View view) {

        countView = view.findViewById(R.id.count);

        emptyVodLayer = view.findViewById(R.id.emptyVodLayer);
        contentLayer = view.findViewById(R.id.contentLayer);

        gridView = view.findViewById(R.id.gridView);
        gridView.initVertical(PurchasedListDialog.NUMBER_ROW);
        gridView.setNumColumns(PurchasedListDialog.NUMBER_COLUMNS);
        gridView.setClipToPadding(false);

        int horizontalSpacing = (int) view.getResources().getDimension(R.dimen.mymenu_purchased_horizontal_space);
        int verticalSpacing = (int) view.getResources().getDimension(R.dimen.mymenu_purchased_vertical_space);
        gridView.setHorizontalSpacing(horizontalSpacing);
        gridView.setVerticalSpacing(verticalSpacing);
    }

    private void initInfoView(View view) {

        infoLayer = view.findViewById(R.id.infoLayer);

        titleView = view.findViewById(R.id.title);
        dateView = view.findViewById(R.id.date);
        paymentView = view.findViewById(R.id.payment);
        priceView = view.findViewById(R.id.price);
        priceUnitView = view.findViewById(R.id.priceUnit);
        periodView = view.findViewById(R.id.period);
        periodUnitView = view.findViewById(R.id.periodUnit);

        btnEdit = view.findViewById(R.id.btnEdit);
        btnGroupLayer = view.findViewById(R.id.btnGroupLayer);
        btnSelectAll = view.findViewById(R.id.btnSelectAll);
        btnDelete = view.findViewById(R.id.btnDelete);
        btnDelete.setEnabled(false);
        btnClose = view.findViewById(R.id.btnClose);
    }

    protected void setFocus(int index) {
        try {
            if (gridView != null) {
                gridView.setSelectedPosition(index);
            }
        }catch (Exception e){
        }
    }

    protected void setContentInfo(PurchasedContent content) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    titleView.setText(content.getTitle());
                    String formattedDate = "";
                    long purchasedDateTime = 0; // milliseconds
                    if (content.getPurchaseDateTime() != null) {
                        purchasedDateTime = content.getPurchaseDateTime().getTime();
                        formattedDate = TimeUtil.getModifiedDate(purchasedDateTime);
                    }
                    dateView.setText(formattedDate);
                    if (content.getPaymentType() != null) {
                        String code = content.getPaymentType().getCode();
                        if(PaymentType.DISCOUNT_COUPON.getCode().equals(code) ||
                           PaymentType.CASH_COUPON.getCode().equals(code) ||
                           PaymentType.MEMBERSHIP.getCode().equals(code)){
                            paymentView.setText(R.string.coupon_point);
                        }else {
                            try{
                                paymentView.setText(content.getPaymentType().getName());
                            }catch (Exception e){
                                paymentView.setText("");
                            }
                        }
                    }
                    long price = 0;
                    try{
                        price = Long.parseLong(content.getPrice());
                    }catch (Exception e){
                    }
                    priceView.setText(StringUtil.getFormattedNumber(price));
                    priceUnitView.setText(content.getCurrency());
                    if (content.getRentalType() == RentalType.RENT) {
                        int remainingHour = 0;
                        try {
                            remainingHour = content.getPeriod(); // hour
                        }catch (Exception e){
                        }
                        if(remainingHour > 0) {
                            int leftTime = remainingHour;
                            String leftUnit = "";
                            if (leftTime > 48) {
                                leftTime = remainingHour / 24;
                                if (leftTime == 1) {
                                    leftUnit = getResources().getText(R.string.day).toString();
                                }else {
                                    leftUnit = getResources().getText(R.string.days).toString();
                                }
                                leftUnit += " " + getResources().getText(R.string.left).toString();
                            } else {
                                if (leftTime <= 1) {
                                    leftUnit = getResources().getText(R.string.hour).toString();
                                } else {
                                    leftUnit = getResources().getText(R.string.hours).toString();
                                }
                                leftUnit += " " + getResources().getText(R.string.left).toString();
                            }
                            periodView.setText(Integer.toString(leftTime));
                            periodUnitView.setText(leftUnit);
                            periodUnitView.setVisibility(View.VISIBLE);
                        }else{
                            periodView.setText(getResources().getText(R.string.expired));
                            periodUnitView.setVisibility(View.GONE);
                        }
                    } else {
                        periodView.setText(getResources().getText(R.string.unlimited));
                        periodUnitView.setVisibility(View.GONE);
                    }

                    infoLayer.setVisibility(View.VISIBLE);
                }catch (Exception e){
                }
            }
        });
    }
}
