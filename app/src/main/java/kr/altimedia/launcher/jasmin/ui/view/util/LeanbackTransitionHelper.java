/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.content.Context;
import android.os.Build;
import android.view.animation.AnimationUtils;

import kr.altimedia.launcher.jasmin.R;


public class LeanbackTransitionHelper {
    public static Object loadTitleInTransition(Context context) {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            SlideKitkat slide = new SlideKitkat();
            slide.setSlideEdge(48);
            slide.setInterpolator(AnimationUtils.loadInterpolator(context, android.R.anim.decelerate_interpolator));
            slide.addTarget(R.id.browse_title_group);
            return slide;
        } else {
            return TransitionHelper.loadTransition(context, R.transition.lb_title_in);
        }
    }

    public static Object loadTitleOutTransition(Context context) {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            SlideKitkat slide = new SlideKitkat();
            slide.setSlideEdge(48);
            slide.setInterpolator(AnimationUtils.loadInterpolator(context, R.anim.lb_decelerator_4));
            slide.addTarget(R.id.browse_title_group);
            return slide;
        } else {
            return TransitionHelper.loadTransition(context, R.transition.lb_title_out);
        }
    }

    private LeanbackTransitionHelper() {
    }
}
