/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.user.object.FAQInfo;

public class FAQInfoResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private List<FAQInfo> faqInfoList;

    protected FAQInfoResponse(Parcel in) {
        super(in);
        in.readList(faqInfoList, FAQInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeList(faqInfoList);
    }

    public static final Creator<FAQInfoResponse> CREATOR = new Creator<FAQInfoResponse>() {
        @Override
        public FAQInfoResponse createFromParcel(Parcel in) {
            return new FAQInfoResponse(in);
        }

        @Override
        public FAQInfoResponse[] newArray(int size) {
            return new FAQInfoResponse[size];
        }
    };

    public List<FAQInfo> getFaqInfoList() {
        return faqInfoList;
    }
}
