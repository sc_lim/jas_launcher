/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase;

import android.os.Handler;
import android.os.Looper;
import android.view.View;

import androidx.leanback.widget.ObjectAdapter;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter.CouponPresenter;
import kr.altimedia.launcher.jasmin.ui.view.adapter.IndicatedItemAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;

public class CouponItemBridgeAdapter extends IndicatedItemAdapter {
    private final float ALIGNMENT = 87.8f;
    private HorizontalGridView mHorizontalGridView;
    private OnCouponListener onCouponListener;

    private Handler mHandler = new Handler(Looper.getMainLooper());
    private CouponInformUpdater mCouponInformUpdater;

    public CouponItemBridgeAdapter(ObjectAdapter adapter, HorizontalGridView mHorizontalGridView) {
        super(adapter);

        this.mHorizontalGridView = mHorizontalGridView;
        initAlignment();
    }

    private void initAlignment() {
        if (mHorizontalGridView == null) {
            return;
        }

        mHorizontalGridView.setWindowAlignmentOffsetPercent(ALIGNMENT);
    }

    public void setOnCouponListener(OnCouponListener onCouponListener) {
        this.onCouponListener = onCouponListener;
        mCouponInformUpdater = new CouponInformUpdater(this.onCouponListener);
    }

    @Override
    protected void onBind(IndicatedItemAdapter.ViewHolder viewHolder) {
        super.onBind(viewHolder);

        DiscountCoupon coupon = (DiscountCoupon) viewHolder.mItem;

        CouponPresenter.ViewHolder vh = (CouponPresenter.ViewHolder) viewHolder.mHolder;
        vh.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (mCouponInformUpdater != null) {
                        mCouponInformUpdater.setCoupon(coupon);
                        mHandler.post(mCouponInformUpdater);
                    }
                }
            }
        });

        vh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCouponListener != null) {
                    onCouponListener.onSelectCoupon(coupon);
                }
            }
        });
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);

        CouponPresenter.ViewHolder vh = (CouponPresenter.ViewHolder) viewHolder.mHolder;
        vh.setOnFocusChangeListener(null);
        vh.setOnClickListener(null);
    }

    public interface OnCouponListener {
        void onFocusCoupon(DiscountCoupon coupon);

        void onSelectCoupon(DiscountCoupon coupon);
    }

    private static class CouponInformUpdater implements Runnable {
        private final OnCouponListener mOnCouponListener;
        private DiscountCoupon mCoupon;


        public CouponInformUpdater(OnCouponListener onCouponListener) {
            this.mOnCouponListener = onCouponListener;
        }

        public void setCoupon(DiscountCoupon coupon) {
            this.mCoupon = coupon;
        }

        @Override
        public void run() {
            if (mOnCouponListener != null) {
                mOnCouponListener.onFocusCoupon(mCoupon);
            }
        }
    }
}
