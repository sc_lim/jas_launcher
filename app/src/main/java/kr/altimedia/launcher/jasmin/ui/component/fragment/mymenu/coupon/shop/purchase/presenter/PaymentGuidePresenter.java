/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.presenter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.PaymentGuideItem;

public class PaymentGuidePresenter extends Presenter {
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_payment_guide, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        PaymentGuideItem paymentGuideItem = (PaymentGuideItem) item;

        View view = viewHolder.view;
        TextView guide = view.findViewById(R.id.guide);
        ImageView image = view.findViewById(R.id.guide_image);
        ImageView guideIcon = view.findViewById(R.id.icon);

        String guideText = view.getContext().getString(paymentGuideItem.getGuide());
        guide.setText(guideText);

        setImage(image, paymentGuideItem.getGuideImage());
        setImage(guideIcon, paymentGuideItem.getGuideStepIcon());
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }

    public void setImage(ImageView view, int drawable) {
        Drawable d = view.getResources().getDrawable(drawable);
        view.setBackground(d);
    }
}
