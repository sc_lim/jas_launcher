/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.viewModel;


import android.app.Activity;
import android.app.Application;

import androidx.databinding.Observable;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private static ViewModelFactory ourInstance = null;
    private final String TAG = ViewModelFactory.class.getSimpleName();

    public static ViewModelFactory getInstance(Application application, Activity activity) {
        if (ourInstance == null) {
            synchronized (ViewModelFactory.class) {
                ourInstance = new ViewModelFactory(application, activity);
            }
        }
        return ourInstance;
    }

    public static ViewModelFactory getInstance(Application application, Activity activity, Observable.OnPropertyChangedCallback mOnPropertyChangedCallback) {
        if (ourInstance == null) {
            synchronized (ViewModelFactory.class) {
                ourInstance = new ViewModelFactory(application, activity, mOnPropertyChangedCallback);
            }
        }
        return ourInstance;
    }

    private Application mApplication = null;
    private Activity mActivity = null;
    private Observable.OnPropertyChangedCallback mPropertyChangedCallback = null;

    private ViewModelFactory(Application application, Activity activity) {
        mApplication = application;
        mActivity = activity;
    }

    public ViewModelFactory(Application mApplication, Activity mActivity, Observable.OnPropertyChangedCallback mPropertyChangedCallback) {
        this.mApplication = mApplication;
        this.mActivity = mActivity;
        this.mPropertyChangedCallback = mPropertyChangedCallback;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(EpgViewModel.class)) {
            if (mPropertyChangedCallback == null) {
                return (T) new EpgViewModel(mActivity, mApplication);
            } else {
                return (T) new EpgViewModel(mPropertyChangedCallback, mActivity, mApplication);
            }
        } else if (modelClass.isAssignableFrom(OSDViewModel.class)) {
            if (mPropertyChangedCallback == null) {
                return (T) new OSDViewModel(mActivity, mApplication);
            } else {
                return (T) new OSDViewModel(mPropertyChangedCallback, mActivity, mApplication);
            }
        } else if (modelClass.isAssignableFrom(TVGuideViewModel.class)) {
            if (mPropertyChangedCallback == null) {
                return (T) new TVGuideViewModel(mActivity, mApplication);
            } else {
                return (T) new TVGuideViewModel(mPropertyChangedCallback, mActivity, mApplication);
            }
        }

        return super.create(modelClass);
    }
}
