/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.def.NormalDateTimeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.ProductType;
import kr.altimedia.launcher.jasmin.dm.def.ProductTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.RentalType;
import kr.altimedia.launcher.jasmin.dm.def.RentalTypeDeserializer;
import kr.altimedia.launcher.jasmin.dm.def.YNBooleanDeserializer;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class PurchasedContent implements Parcelable {
    public static final Creator<PurchasedContent> CREATOR = new Creator<PurchasedContent>() {
        @Override
        public PurchasedContent createFromParcel(Parcel in) {
            return new PurchasedContent(in);
        }

        @Override
        public PurchasedContent[] newArray(int size) {
            return new PurchasedContent[size];
        }
    };
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("purchaseDateTime")
    @JsonAdapter(NormalDateTimeDeserializer.class)
    private Date purchaseDateTime;
    @Expose
    @SerializedName("price")
    private String price;
    @Expose
    @SerializedName("posterFileName")
    private String posterFileName;
    @Expose
    @SerializedName("containerType")
    private String containerType;
    @Expose
    @SerializedName("period")
    private int period;
    @Expose
    @SerializedName("productType")
    @JsonAdapter(ProductTypeDeserializer.class)
    private ProductType productType;
    @Expose
    @SerializedName("rentalType")
    @JsonAdapter(RentalTypeDeserializer.class)
    private RentalType rentalType;
    @Expose
    @SerializedName("paymentMethod")
    private PaymentType paymentType;
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("contentGroupId")
    private String contentGroupId;
    @Expose
    @SerializedName("offerId")
    private String offerId;
    @Expose
    @SerializedName("purchaseId")
    private String purchaseId;
    @Expose
    @SerializedName("hiddenYn")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isHidden;
    @SerializedName("rating")
    @Expose
    private int rating;
    @Expose
    @SerializedName("refundYn")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isRefundYn;
    @Expose
    @SerializedName("profileId")
    private String profileId;
    @Expose
    @SerializedName("isSeries")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isSeries;
    @Expose
    @SerializedName("seriesAssetId")
    private String seriesAssetId;
    @Expose
    @SerializedName("isPackage")
    @JsonAdapter(YNBooleanDeserializer.class)
    private boolean isPackage;
    @Expose
    @SerializedName("packageId")
    private String packageId;
    @Expose
    @SerializedName("episodeId")
    private String episodeId;

    protected PurchasedContent(Parcel in) {
        title = in.readString();
        purchaseDateTime = ((Date) in.readValue((Date.class.getClassLoader())));
        price = in.readString();
        posterFileName = in.readString();
        containerType = in.readString();
        period = ((Integer) in.readValue((Integer.class.getClassLoader())));
        productType = (ProductType) in.readValue(ProductType.class.getClassLoader());
        rentalType = (RentalType) in.readValue(RentalType.class.getClassLoader());
        paymentType = PaymentType.findByName((String) in.readValue(String.class.getClassLoader()));
        currency = in.readString();
        contentGroupId = in.readString();
        offerId = in.readString();
        purchaseId = in.readString();
        isHidden = (boolean) in.readValue(Boolean.class.getClassLoader());
        rating = ((int) in.readValue((Integer.class.getClassLoader())));
        isRefundYn = (boolean) in.readValue(Boolean.class.getClassLoader());
        profileId = in.readString();
        isSeries = in.readByte() != 0;
        seriesAssetId = in.readString();
        isPackage = in.readByte() != 0;
        packageId = in.readString();
        episodeId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeValue(purchaseDateTime);
        dest.writeString(price);
        dest.writeString(posterFileName);
        dest.writeString(containerType);
        dest.writeValue(period);
        dest.writeValue(productType);
        dest.writeValue(rentalType);
        dest.writeValue(paymentType.getCode());
        dest.writeString(currency);
        dest.writeString(contentGroupId);
        dest.writeString(offerId);
        dest.writeString(purchaseId);
        dest.writeValue(isHidden);
        dest.writeValue(rating);
        dest.writeValue(isRefundYn);
        dest.writeString(profileId);
        dest.writeByte((byte) (isSeries ? 1 : 0));
        dest.writeString(seriesAssetId);
        dest.writeByte((byte) (isPackage ? 1 : 0));
        dest.writeString(packageId);
        dest.writeString(episodeId);
    }

    public String getTitle() {
        return title;
    }

    public Date getPurchaseDateTime() {
        return purchaseDateTime;
    }

    public String getPrice() {
        return price;
    }

    public String getPosterFileName() {
        return posterFileName;
    }

    public String getContainerType() {
        return containerType;
    }

    public String getPosterSourceUrl(int width, int height) {
        if(containerType == null || containerType.isEmpty()){
            containerType = "";
        }
        return posterFileName + "_" + width + "x" + height + "." + containerType;
    }

    public int getPeriod() {
        return period;
    }

    public ProductType getProductType() {
        return productType;
    }

    public RentalType getRentalType() {
        return rentalType;
    }

    public void setRentalType(RentalType rentalType) {
        if (this.rentalType != null) {
            return;
        }
        this.rentalType = rentalType;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public String getCurrency() {
        return currency;
    }

    public String getContentGroupId() {
        return contentGroupId;
    }

    public String getOfferId() {
        return offerId;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean value) {
        isHidden = value;
    }

    public int getRating() {
        return rating;
    }

    public boolean isRefundable() {
        return isRefundYn;
    }

    public String getProfileId() {
        return profileId;
    }

    public boolean isSeries() {
        return isSeries;
    }

    public String getSeriesAssetId() {
        return seriesAssetId;
    }

    public boolean isPackage() {
        return isPackage;
    }

    public String getPackageId() {
        return packageId;
    }

    public String getEpisodeId() {
        return episodeId;
    }

    @NonNull
    @Override
    public String toString() {
        return "PurchasedContent{" +
                "title=" + title +
                ", purchaseDateTime=" + purchaseDateTime +
                ", price=" + price +
                ", posterFileName=" + posterFileName +
                ", containerType=" + containerType +
                ", period=" + period +
                ", productType=" + productType +
                ", rentalType=" + rentalType +
                ", paymentType=" + paymentType +
                ", currency=" + currency +
                ", contentGroupId=" + contentGroupId +
                ", offerId=" + offerId +
                ", purchaseId=" + purchaseId +
                ", isHidden=" + isHidden +
                ", rating=" + rating +
                ", isRefundYn=" + isRefundYn +
                ", profileId=" + profileId +
                ", isSeries=" + isSeries +
                ", seriesAssetId='" + seriesAssetId + '\'' +
                ", isPackage=" + isPackage +
                ", packageId='" + packageId + '\'' +
                ", episodeId='" + episodeId + '\'' +
                "}";
    }
}
