/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.OnChildSelectedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.ShadowOverlayHelper;
import androidx.recyclerview.widget.RecyclerView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.FocusHighlightHelper;
import kr.altimedia.launcher.jasmin.ui.view.util.TransitionHelper;


public class VerticalGridPresenter extends Presenter {
    private static final String TAG = "GridPresenter";
    private static final boolean DEBUG = false;
    private int mNumColumns;
    private int mFocusZoomFactor;
    private boolean mUseFocusDimmer;
    private boolean mShadowEnabled;
    private boolean mKeepChildForeground;
    private OnItemViewSelectedListener mOnItemViewSelectedListener;
    private OnItemViewClickedListener mOnItemViewClickedListener;
    private boolean mRoundedCornersEnabled;
//    ShadowOverlayHelper mShadowOverlayHelper;
//    private ItemBridgeAdapter.Wrapper mShadowOverlayWrapper;

    public VerticalGridPresenter() {
        this(4);
    }

    public VerticalGridPresenter(int focusZoomFactor) {
        this(focusZoomFactor, true);
    }

    public VerticalGridPresenter(int focusZoomFactor, boolean useFocusDimmer) {
        this.mNumColumns = -1;
        this.mShadowEnabled = true;
        this.mKeepChildForeground = true;
        this.mRoundedCornersEnabled = true;
        this.mFocusZoomFactor = focusZoomFactor;
        this.mUseFocusDimmer = useFocusDimmer;
    }

    public void setNumberOfColumns(int numColumns) {
        if (numColumns < 0) {
            throw new IllegalArgumentException("Invalid number of columns");
        } else {
            if (this.mNumColumns != numColumns) {
                this.mNumColumns = numColumns;
            }

        }
    }

    public int getNumberOfColumns() {
        return this.mNumColumns;
    }

    public final void setShadowEnabled(boolean enabled) {
        this.mShadowEnabled = enabled;
    }

    public final boolean getShadowEnabled() {
        return this.mShadowEnabled;
    }

    public boolean isUsingDefaultShadow() {
        return ShadowOverlayHelper.supportsShadow();
    }

    public final void enableChildRoundedCorners(boolean enable) {
        this.mRoundedCornersEnabled = enable;
    }

    public final boolean areChildRoundedCornersEnabled() {
        return this.mRoundedCornersEnabled;
    }

    public boolean isUsingZOrder(Context context) {
        return false;
//        return !Settings.getInstance(context).preferStaticShadows();
    }

    final boolean needsDefaultShadow() {
        return this.isUsingDefaultShadow() && this.getShadowEnabled();
    }

    public final int getFocusZoomFactor() {
        return this.mFocusZoomFactor;
    }

    public final boolean isFocusDimmerUsed() {
        return this.mUseFocusDimmer;
    }

    public final VerticalGridPresenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        VerticalGridPresenter.ViewHolder vh = this.createGridViewHolder(parent);
        vh.mInitialized = false;
        vh.mItemBridgeAdapter = new VerticalGridPresenter.VerticalGridItemBridgeAdapter();
        this.initializeGridViewHolder(vh);
        if (!vh.mInitialized) {
            throw new RuntimeException("super.initializeGridViewHolder() must be called");
        } else {
            return vh;
        }
    }

    protected VerticalGridPresenter.ViewHolder createGridViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical_grid, parent, false);
        return new VerticalGridPresenter.ViewHolder((VerticalGridView) root.findViewById(R.id.browse_grid));
    }

    protected void initializeGridViewHolder(final VerticalGridPresenter.ViewHolder vh) {
        if (this.mNumColumns == -1) {
            throw new IllegalStateException("Number of columns must be set");
        } else {
            vh.getGridView().setNumColumns(this.mNumColumns);
            vh.mInitialized = true;
            Context context = vh.mGridView.getContext();
//            if (this.mShadowOverlayHelper == null) {
//                this.mShadowOverlayHelper = (new ShadowOverlayHelper.Builder()).needsOverlay(this.mUseFocusDimmer).needsShadow(this.needsDefaultShadow()).needsRoundedCorner(this.areChildRoundedCornersEnabled()).preferZOrder(this.isUsingZOrder(context)).keepForegroundDrawable(this.mKeepChildForeground).options(this.createShadowOverlayOptions()).build(context);
//                if (this.mShadowOverlayHelper.needsWrapper()) {
//                    this.mShadowOverlayWrapper = new ItemBridgeAdapterShadowOverlayWrapper(this.mShadowOverlayHelper);
//                }
//            }
//
//            vh.mItemBridgeAdapter.setWrapper(this.mShadowOverlayWrapper);
//            this.mShadowOverlayHelper.prepareParentForShadow(vh.mViewPager);
            vh.getGridView().setFocusDrawingOrderEnabled(true);

            FocusHighlightHelper.setupBrowseItemFocusHighlight(vh.mItemBridgeAdapter, this.mFocusZoomFactor, false);
            vh.getGridView().setOnChildSelectedListener(new OnChildSelectedListener() {
                public void onChildSelected(ViewGroup parent, View view, int position, long id) {
                    VerticalGridPresenter.this.selectChildView(vh, view);
                }
            });
        }
    }

    public final void setKeepChildForeground(boolean keep) {
        this.mKeepChildForeground = keep;
    }

    public final boolean getKeepChildForeground() {
        return this.mKeepChildForeground;
    }

    protected ShadowOverlayHelper.Options createShadowOverlayOptions() {
        return ShadowOverlayHelper.Options.DEFAULT;
    }

    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        VerticalGridPresenter.ViewHolder vh = (VerticalGridPresenter.ViewHolder) viewHolder;
        vh.mItemBridgeAdapter.setAdapter((ObjectAdapter) item);
        vh.getGridView().setAdapter(vh.mItemBridgeAdapter);
    }

    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        VerticalGridPresenter.ViewHolder vh = (VerticalGridPresenter.ViewHolder) viewHolder;
        vh.mItemBridgeAdapter.setAdapter((ObjectAdapter) null);
        vh.getGridView().setAdapter((RecyclerView.Adapter) null);
    }

    public final void setOnItemViewSelectedListener(OnItemViewSelectedListener listener) {
        this.mOnItemViewSelectedListener = listener;
    }

    public final OnItemViewSelectedListener getOnItemViewSelectedListener() {
        return this.mOnItemViewSelectedListener;
    }

    public final void setOnItemViewClickedListener(OnItemViewClickedListener listener) {
        this.mOnItemViewClickedListener = listener;
    }

    public final OnItemViewClickedListener getOnItemViewClickedListener() {
        return this.mOnItemViewClickedListener;
    }

    void selectChildView(VerticalGridPresenter.ViewHolder vh, View view) {
        if (this.getOnItemViewSelectedListener() != null) {
            ItemBridgeAdapter.ViewHolder ibh = view == null ? null : (ItemBridgeAdapter.ViewHolder) vh.getGridView().getChildViewHolder(view);
            if (ibh == null) {
                this.getOnItemViewSelectedListener().onItemSelected(null, null, (RowPresenter.ViewHolder) null, null);
            } else {
                this.getOnItemViewSelectedListener().onItemSelected(ibh.mHolder, ibh.mItem, (RowPresenter.ViewHolder) null, null);
            }
        }

    }

    public void setEntranceTransitionState(VerticalGridPresenter.ViewHolder holder, boolean afterEntrance) {
        holder.mGridView.setChildrenVisibility(afterEntrance ? 0 : 4);
    }

    public static class ViewHolder extends Presenter.ViewHolder {
        ItemBridgeAdapter mItemBridgeAdapter;
        final VerticalGridView mGridView;
        boolean mInitialized;

        public ViewHolder(VerticalGridView view) {
            super(view);
            this.mGridView = view;
        }

        public VerticalGridView getGridView() {
            return this.mGridView;
        }

        public ItemBridgeAdapter getItemBridgeAdapter() {
            return mItemBridgeAdapter;
        }
    }

    class VerticalGridItemBridgeAdapter extends ItemBridgeAdapter {
        VerticalGridItemBridgeAdapter() {
        }

        protected void onCreate(ItemBridgeAdapter.ViewHolder viewHolder) {
            if (viewHolder.itemView instanceof ViewGroup) {
                TransitionHelper.setTransitionGroup((ViewGroup) viewHolder.itemView, true);
            }

//            if (VerticalGridPresenter.this.mShadowOverlayHelper != null) {
//                VerticalGridPresenter.this.mShadowOverlayHelper.onViewCreated(viewHolder.itemView);
//            }

        }

        public void onBind(final ItemBridgeAdapter.ViewHolder itemViewHolder) {
            if (VerticalGridPresenter.this.getOnItemViewClickedListener() != null) {
                View itemView = itemViewHolder.mHolder.view;
                itemView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (VerticalGridPresenter.this.getOnItemViewClickedListener() != null) {
                            VerticalGridPresenter.this.getOnItemViewClickedListener().onItemClicked(itemViewHolder.mHolder, itemViewHolder.mItem, null, null);
                        }

                    }
                });
            }

        }

        public void onUnbind(ItemBridgeAdapter.ViewHolder viewHolder) {
            if (VerticalGridPresenter.this.getOnItemViewClickedListener() != null) {
                viewHolder.mHolder.view.setOnClickListener((View.OnClickListener) null);
            }

        }

        public void onAttachedToWindow(ItemBridgeAdapter.ViewHolder viewHolder) {
            viewHolder.itemView.setActivated(true);
        }
    }
}
