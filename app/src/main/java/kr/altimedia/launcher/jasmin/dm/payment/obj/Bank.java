package kr.altimedia.launcher.jasmin.dm.payment.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bank implements Parcelable {
    @Expose
    @SerializedName("bankCode")
    private String bankCode;
    @Expose
    @SerializedName("bankThiName")
    private String bankThiName;
    @Expose
    @SerializedName("bankEngName")
    private String bankEngName;
    @Expose
    @SerializedName("bankLogo")
    private String bankLogo;


    protected Bank(Parcel in) {
        bankCode = in.readString();
        bankThiName = in.readString();
        bankEngName = in.readString();
        bankLogo = in.readString();
    }

    public static final Creator<Bank> CREATOR = new Creator<Bank>() {
        @Override
        public Bank createFromParcel(Parcel in) {
            return new Bank(in);
        }

        @Override
        public Bank[] newArray(int size) {
            return new Bank[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bankCode);
        dest.writeString(bankThiName);
        dest.writeString(bankEngName);
        dest.writeString(bankLogo);
    }

    public String getBankCode() {
        return bankCode;
    }

    public String getBankThiName() {
        return bankThiName;
    }

    public String getBankEngName() {
        return bankEngName;
    }

    public String getBankLogo() {
        return bankLogo;
    }
}
