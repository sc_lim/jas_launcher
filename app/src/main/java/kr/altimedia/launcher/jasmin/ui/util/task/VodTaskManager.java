/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.util.task;

import android.content.Context;
import android.os.AsyncTask;

import com.altimedia.util.Log;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PackageContent;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonListInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeriesContentInfo;
import kr.altimedia.launcher.jasmin.dm.recommend.remote.RecommendRemote;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;

public class VodTaskManager extends TaskManager {
    public enum TaskType {DETAIL, RELATE, PACKAGE, EPISODE, SEASON_LIST, CONTENT_FAVORITE}

    public enum VodLikeType {VOD, Series, Package}

    private static final String TAG = VodTaskManager.class.getName();

    private Context context;

    //common
    private MbsDataTask contentDetailTask;
    private MbsDataTask relatedContentsTask;
    private MbsDataTask favoriteContentTask;

    //package
    private MbsDataTask packageContentsTask;

    //series
    private MbsDataTask episodeContentsTask;
    private MbsDataTask seasonListTask;

    private OnCompleteContentFavoriteListener onCompleteContentFavoriteListener;
    private OnCompleteLoadContentDetailListener onCompleteLoadContentDetailListener;
    private OnCompleteLoadRelateListener onCompleteLoadRelateListener;
    private OnCompleteLoadPackageListener onCompleteLoadPackageListener;
    private OnCompleteLoadEpisodeListener onCompleteLoadEpisodeListener;
    private OnCompleteLoadSeasonListListener onCompleteLoadSeasonListListener;

    public VodTaskManager(Context context) {
        this.context = context;
    }

    public void setOnCompleteContentFavoriteListener(OnCompleteContentFavoriteListener onCompleteContentFavoriteListener) {
        this.onCompleteContentFavoriteListener = onCompleteContentFavoriteListener;
    }

    public void setOnCompleteLoadContentDetailListener(OnCompleteLoadContentDetailListener onCompleteLoadContentDetailListener) {
        this.onCompleteLoadContentDetailListener = onCompleteLoadContentDetailListener;
    }

    public void setOnCompleteLoadRelateListener(OnCompleteLoadRelateListener onCompleteLoadRelateListener) {
        this.onCompleteLoadRelateListener = onCompleteLoadRelateListener;
    }

    public void setOnCompleteLoadPackageListener(OnCompleteLoadPackageListener onCompleteLoadPackageListener) {
        this.onCompleteLoadPackageListener = onCompleteLoadPackageListener;
    }

    public void setOnCompleteLoadEpisodeListener(OnCompleteLoadEpisodeListener onCompleteLoadEpisodeListener) {
        this.onCompleteLoadEpisodeListener = onCompleteLoadEpisodeListener;
    }

    public void setOnCompleteLoadSeasonListListener(OnCompleteLoadSeasonListListener onCompleteLoadSeasonListListener) {
        this.onCompleteLoadSeasonListListener = onCompleteLoadSeasonListListener;
    }

    public void loadContentDetail(String contentId, String categoryId) {
        if (contentDetailTask != null && contentDetailTask.isRunning()) {
            cancelTask(contentDetailTask);
        }

        contentDetailTask = mContentDataManager.getContentDetail(AccountManager.getInstance().getLocalLanguage()
                , AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), contentId, categoryId, new MbsDataProvider<String, ContentDetailInfo>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (!loading) {
                            completeTask(contentDetailTask);
                        }

                        if (onCompleteLoadContentDetailListener != null) {
                            onCompleteLoadContentDetailListener.needLoading(loading);
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (onCompleteLoadContentDetailListener != null) {
                            onCompleteLoadContentDetailListener.onFailLoadContentDetail(key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, ContentDetailInfo result) {
                        if (onCompleteLoadContentDetailListener != null) {
                            onCompleteLoadContentDetailListener.onSuccessLoadContentDetail(id, result);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (onCompleteLoadContentDetailListener != null) {
                            onCompleteLoadContentDetailListener.onError(contentId, errorCode, message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });
        addTask(contentDetailTask);
    }

    public void loadRelatedList(String contentId, String categoryId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "loadRelatedList : " + contentId + ", categoryId : " + categoryId);
        }

        if (relatedContentsTask != null && relatedContentsTask.isRunning()) {
            cancelTask(relatedContentsTask);
        }

        relatedContentsTask = mRecommendDataManager.getRelatedContents(AccountManager.getInstance().getLocalLanguage()
                , AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), contentId, RecommendRemote.MAX_CNT,
                AccountManager.getInstance().getProductCode(), categoryId, AccountManager.getInstance().getCurrentRating(),
                new MbsDataProvider<String, List<Content>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (!loading) {
                            completeTask(relatedContentsTask);
                        }

                        if (onCompleteLoadRelateListener != null) {
                            onCompleteLoadRelateListener.needLoading(loading);
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (onCompleteLoadRelateListener != null) {
                            onCompleteLoadRelateListener.onFailLoadRelate(key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<Content> result) {
                        if (onCompleteLoadRelateListener != null) {
                            onCompleteLoadRelateListener.onSuccessLoadRelate(id, result);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (onCompleteLoadRelateListener != null) {
                            onCompleteLoadRelateListener.onError(contentId, errorCode, message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });
        addTask(relatedContentsTask);
    }

    public void loadPackageDetail(String packageId, String categoryId) {
        if (packageContentsTask != null && packageContentsTask.isRunning()) {
            cancelTask(packageContentsTask);
        }

        packageContentsTask = mContentDataManager.getPackageDetail(AccountManager.getInstance().getLocalLanguage()
                , AccountManager.getInstance().getSaId(), packageId,
                AccountManager.getInstance().getProfileId(), categoryId, new MbsDataProvider<String, PackageContent>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (!loading) {
                            completeTask(packageContentsTask);
                        }

                        if (onCompleteLoadPackageListener != null) {
                            onCompleteLoadPackageListener.needLoading(loading);
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (onCompleteLoadPackageListener != null) {
                            onCompleteLoadPackageListener.onFailLoadPackage(key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, PackageContent result) {
                        if (onCompleteLoadPackageListener != null) {
                            onCompleteLoadPackageListener.onSuccessLoadPackage(id, result);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (onCompleteLoadPackageListener != null) {
                            onCompleteLoadPackageListener.onError(packageId, errorCode, message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });
        addTask(packageContentsTask);

    }

    public void loadEpisodeList(String seriesAssetId, String categoryId, String contentId) {
        if (episodeContentsTask != null && episodeContentsTask.isRunning()) {
            cancelTask(episodeContentsTask);
        }

        episodeContentsTask = mContentDataManager.getSeriesList(AccountManager.getInstance().getLocalLanguage()
                , AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId()
                , seriesAssetId, categoryId, contentId, new MbsDataProvider<String, SeriesContentInfo>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (!loading) {
                            completeTask(episodeContentsTask);
                        }

                        if (onCompleteLoadEpisodeListener != null) {
                            onCompleteLoadEpisodeListener.needLoading(loading);
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (onCompleteLoadEpisodeListener != null) {
                            onCompleteLoadEpisodeListener.onFailLoadEpisode(key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, SeriesContentInfo result) {
                        if (onCompleteLoadEpisodeListener != null) {
                            onCompleteLoadEpisodeListener.onSuccessLoadEpisode(id, result);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (onCompleteLoadEpisodeListener != null) {
                            onCompleteLoadEpisodeListener.onError(seriesAssetId, errorCode, message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });

        addTask(episodeContentsTask);
    }

    public void loadSeasonList(String seriesAssetId, String categoryId) {
        if (seasonListTask != null && seasonListTask.isRunning()) {
            cancelTask(seasonListTask);
        }

        seasonListTask = mContentDataManager.getSeasonList(AccountManager.getInstance().getLocalLanguage(), AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(), seriesAssetId, categoryId, new MbsDataProvider<String, SeasonListInfo>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (!loading) {
                            completeTask(seasonListTask);
                        }

                        if (onCompleteLoadSeasonListListener != null) {
                            onCompleteLoadSeasonListListener.needLoading(loading);
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (onCompleteLoadSeasonListListener != null) {
                            onCompleteLoadSeasonListListener.onFailLoadSeasonList(key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, SeasonListInfo result) {
                        if (onCompleteLoadSeasonListListener != null) {
                            onCompleteLoadSeasonListListener.onSuccessLoadSeasonList(id, result);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (onCompleteLoadSeasonListListener != null) {
                            onCompleteLoadSeasonListListener.onError(seriesAssetId, errorCode, message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });

        addTask(seasonListTask);
    }

    public void onClickLikeContent(boolean isLike, String contentId, VodLikeType type) {
        if (isLike) {
            removeLikeContent(contentId, type);
        } else {
            addLikeContent(contentId, type);
        }
    }

    private void addLikeContent(String contentId, VodLikeType type) {
        favoriteContentTask = mContentDataManager.postFavoriteVod(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), contentId, type,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (!loading) {
                            completeTask(favoriteContentTask);
                        }

                        if (onCompleteContentFavoriteListener != null) {
                            onCompleteContentFavoriteListener.needLoading(loading);
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (onCompleteContentFavoriteListener != null) {
                            onCompleteContentFavoriteListener.onFailFavorite(key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (onCompleteContentFavoriteListener != null) {
                            onCompleteContentFavoriteListener.onSuccessAddFavorite(id, null);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (onCompleteContentFavoriteListener != null) {
                            onCompleteContentFavoriteListener.onError(contentId, errorCode, message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });

        addTask(favoriteContentTask);
    }

    private void removeLikeContent(String contentId, VodLikeType type) {
        favoriteContentTask = mContentDataManager.deleteFavoriteVod(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), contentId, type,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (!loading) {
                            completeTask(favoriteContentTask);
                        }

                        if (onCompleteContentFavoriteListener != null) {
                            onCompleteContentFavoriteListener.needLoading(loading);
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (onCompleteContentFavoriteListener != null) {
                            onCompleteContentFavoriteListener.onFailFavorite(key);
                        }
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (onCompleteContentFavoriteListener != null) {
                            onCompleteContentFavoriteListener.onSuccessRemoveFavorite(id, null);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (onCompleteContentFavoriteListener != null) {
                            onCompleteContentFavoriteListener.onError(contentId, errorCode, message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return context;
                    }
                });

        addTask(favoriteContentTask);
    }

    public void cancelTask(TaskType type) {
        AsyncTask task = null;

        switch (type) {
            case DETAIL:
                task = contentDetailTask;
                break;
            case RELATE:
                task = relatedContentsTask;
                break;
            case PACKAGE:
                task = packageContentsTask;
                break;
            case EPISODE:
                task = episodeContentsTask;
                break;
            case SEASON_LIST:
                task = seasonListTask;
                break;
            case CONTENT_FAVORITE:
                task = favoriteContentTask;
                break;
        }

        cancelTask(task);
    }

    public interface OnCompleteLoadContentDetailListener<K, V> {
        void needLoading(boolean isLoading);

        void onSuccessLoadContentDetail(K id, V result);

        void onFailLoadContentDetail(int key);

        void onError(K id, String errorCode, String message);
    }

    public interface OnCompleteLoadRelateListener<K, V> {
        void needLoading(boolean isLoading);

        void onSuccessLoadRelate(K id, V result);

        void onFailLoadRelate(int key);

        void onError(K id, String errorCode, String message);
    }

    public interface OnCompleteLoadPackageListener<K, V> {
        void needLoading(boolean isLoading);

        void onSuccessLoadPackage(K id, V result);

        void onFailLoadPackage(int key);

        void onError(K id, String errorCode, String message);
    }

    public interface OnCompleteLoadEpisodeListener<K, V> {
        void needLoading(boolean isLoading);

        void onSuccessLoadEpisode(K id, V result);

        void onFailLoadEpisode(int key);

        void onError(K id, String errorCode, String message);
    }

    public interface OnCompleteLoadSeasonListListener<K, V> {
        void needLoading(boolean isLoading);

        void onSuccessLoadSeasonList(K id, V result);

        void onFailLoadSeasonList(int key);

        void onError(K id, String errorCode, String message);
    }

    public interface OnCompleteContentFavoriteListener<K, V> {
        void needLoading(boolean isLoading);

        void onSuccessAddFavorite(K id, V result);

        void onSuccessRemoveFavorite(K id, V result);

        void onFailFavorite(int key);

        void onError(K id, String errorCode, String message);
    }
}
