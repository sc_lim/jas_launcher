/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseDiscountType;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class ContentPricePresenter extends Presenter {
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_content_price, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ListRow row = (ListRow) item;
        ArrayObjectAdapter arrayObjectAdapter = (ArrayObjectAdapter) row.getAdapter();
        Product product = (Product) arrayObjectAdapter.get(0);

        ViewHolder vh = (ViewHolder) viewHolder;
        vh.initData(product);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private TextView priceTextView;
        private TextView unitTextView;
        private TextView discount;
        private LinearLayout limitDescLayout;
        private TextView limitDesc;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            priceTextView = view.findViewById(R.id.price);
            unitTextView = view.findViewById(R.id.unit);
            discount = view.findViewById(R.id.discount);
            limitDescLayout = view.findViewById(R.id.limit_desc_layout);
            limitDesc = view.findViewById(R.id.limit_desc);
        }

        private void initData(Product product) {
            float price = product.getPrice();
            priceTextView.setText(StringUtil.getFormattedPrice(price));
            unitTextView.setText(view.getContext().getString(R.string.thb));
        }

        public void updateDiscountLimit(int index) {
            if (index == PurchaseDiscountType.MEMBERSHIP.getType()) {
                int rate = PurchaseDiscountType.MEMBERSHIP.getLimitRate();
                int ratio = PurchaseDiscountType.MEMBERSHIP.getRatio();
                String text = (String) limitDesc.getText();
                text = text.replace("%d", String.valueOf(rate));
                text = text.replace("%p", String.valueOf(ratio));
                limitDesc.setText(text);
                limitDescLayout.setVisibility(View.VISIBLE);
            } else {
                limitDescLayout.setVisibility(View.INVISIBLE);
            }
        }

        public void setDiscount(float value) {
            String str = value > 0 ? "- " + StringUtil.getFormattedPrice(value) : "0";
            discount.setText(str);
        }
    }
}
