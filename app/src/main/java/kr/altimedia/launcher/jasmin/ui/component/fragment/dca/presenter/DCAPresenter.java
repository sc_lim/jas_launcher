/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.dca.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import com.altimedia.tvmodule.dao.Channel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class DCAPresenter extends Presenter {
    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_dca, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        ViewHolder vh = (ViewHolder) viewHolder;
        vh.setData((Channel) item);
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {

    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private ImageView channelIcon;
        private TextView channelNum;
        private TextView channelName;
        private ImageView channelLogo;

        public ViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            channelIcon = view.findViewById(R.id.channel_icon);
            channelNum = view.findViewById(R.id.channel_number);
            channelName = view.findViewById(R.id.channel_name);
            channelLogo = view.findViewById(R.id.channel_logo);
        }

        public void setData(Channel channel) {
            String chNum = StringUtil.getFormattedNumber(channel.getDisplayNumber(), 3);
            channelNum.setText(chNum);
            setChannelTitle(channel);
            setChannelIcon(channel);
        }

        private void setChannelIcon(Channel channel) {
            int icon = -1;
            if (channel.isLocked()) {
                icon = R.drawable.selector_ch_icon_block;
            } else if (channel.isFavoriteChannel()) {
                icon = R.drawable.ch_icon_fav_f;
            }

            if (icon != -1) {
                channelIcon.setImageResource(icon);
                channelIcon.setVisibility(View.VISIBLE);
            } else {
                channelIcon.setVisibility(View.INVISIBLE);
            }
        }

        private void setChannelTitle(Channel channel) {
            String logo = channel.getLogoUri();

            if (logo != null && !logo.equals("")) {
                channelLogo.setVisibility(View.VISIBLE);
                Glide.with(view.getContext())
                        .load(logo).diskCacheStrategy(DiskCacheStrategy.DATA)
                        .centerCrop()
                        .into(channelLogo);
            } else {
                channelName.setVisibility(View.VISIBLE);
                channelName.setText(channel.getDisplayName());
            }
        }
    }
}
