/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service.control;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.RouteInfo;

import com.altimedia.cwmplibrary.common.CWMPMultiObject;
import com.altimedia.cwmplibrary.common.CWMPParameter;
import com.altimedia.cwmplibrary.common.ModelObject;
import com.altimedia.cwmplibrary.ttbb.object.InternetGatewayDevice;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.WANDevice;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.wandevice.WANConnectionDevice;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.wandevice.wanconnectiondevice.WANIPConnection;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.wandevice.wanconnectiondevice.WANIPConnections;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPController;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * NetworkInformControl
 */
public class NetworkInformControl {
    private final String CLASS_NAME = NetworkInformControl.class.getName();
    private final String TAG = NetworkInformControl.class.getSimpleName();

    private final int TYPE_ETHERNET = 1;
    private final int TYPE_WIFI = 2;

    private final String NAME_ETHERNET = "eth0";
    private final String NAME_WIFI = "wlan0";

    private final String STATIC = "Static";
    private final String DHCP = "DHCP";

    private ConnectivityManager.NetworkCallback ethernetNetworkCallback;
    private ConnectivityManager.NetworkCallback wifiNetworkCallback;

    private InternetGatewayDevice root;
    private Context context;

    private String ethernetIpAddress = "";
    private String wifiIpAddress = "";
    private String ethernetMacAddress = "";
    private String wifiMacAddress = "";

    public NetworkInformControl(){
    }

    public void dispose() {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
            if (cm != null) {
                if (ethernetNetworkCallback != null) {
                    cm.unregisterNetworkCallback(ethernetNetworkCallback);
                }
                if (wifiNetworkCallback != null) {
                    cm.unregisterNetworkCallback(wifiNetworkCallback);
                }
            }
        } catch (Exception e){
        }

        ethernetNetworkCallback = null;
        wifiNetworkCallback = null;

        root = null;
        context = null;
    }

    public void init(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "init");
        }

        this.context = context;

        // obtain ip address
        ethernetIpAddress = getEthernetIpAddress();
        wifiIpAddress = getWifiIpAddress();
    }

    public void start(InternetGatewayDevice root){
        if (Log.INCLUDE) {
            Log.d(TAG, "start");
        }
        this.root = root;

        if(this.root == null){
            if (Log.INCLUDE) {
                Log.d(TAG, "start: root is NOT available");
            }
            return;
        }

        createWanDevice(TYPE_ETHERNET); // ethernet
        createWanDevice(TYPE_WIFI); // wifi

        getMacAddress(":");
        setWanDeviceMacAddress(TYPE_ETHERNET, ethernetMacAddress);
        setWanDeviceMacAddress(TYPE_WIFI, wifiMacAddress);

        // check network connection
        setWanDeviceStatus(TYPE_ETHERNET, isEthernetConnected());
        setWanDeviceStatus(TYPE_WIFI, isWifiConnected());

        startEthernetMonitor();
        startWifiMonitor();
    }

    private void setParamValue(CWMPParameter parameter, Object value){
        try{
            parameter.setValue(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createWanDevice(int type){
        if (Log.INCLUDE) {
            Log.d(TAG, "createWanDevice: type="+type);
        }
        try {
            WANDevice wanDevice = root.wanDevices.getWanDevice(type);
            if (wanDevice == null) {
                wanDevice = root.wanDevices.addWanDevice(new WANDevice(type));
            }
            WANConnectionDevice wanConnectionDevice = wanDevice.wanConnectionDevices.getWANConnectionDevice(1);
            if (wanConnectionDevice == null) {
                wanConnectionDevice = wanDevice.wanConnectionDevices.addWANConnectionDevice(new WANConnectionDevice(1));
            }
            WANIPConnection wanIPConnection = wanConnectionDevice.wanipConnections.getWANIPConnection(1);
            if (wanIPConnection == null) {
                wanIPConnection = wanConnectionDevice.wanipConnections.addWANIPConnection(new WANIPConnection(1));
            }
            if (type == TYPE_ETHERNET) {
                wanIPConnection.Name.setValue(NAME_ETHERNET);
            } else if (type == TYPE_WIFI) {
                wanIPConnection.Name.setValue(NAME_WIFI);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void handleNetworkConnectivity(boolean wireless, String oldIpAddress, String newIpAddress){
        boolean urlChanged = CWMPController.getInstance().setConnectionRequestURL();
        if (Log.INCLUDE) {
            Log.d(TAG, "handleNetworkConnectivity: wireless="+wireless+", oldIpAddress="+oldIpAddress+", newIpAddress="+newIpAddress+"(UrlChanged="+urlChanged+")");
        }
        if(!oldIpAddress.equals("") && !oldIpAddress.equals(newIpAddress)){
            urlChanged = true;
        }
        CWMPController.getInstance().notifyNetworkConnected(wireless);
        if(urlChanged){
            CWMPController.getInstance().notifyIpAllocated();
        }
    }

    private void startEthernetMonitor(){
        if (Log.INCLUDE) {
            Log.d(TAG, "startEthernetMonitor");
        }
        ethernetNetworkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                super.onAvailable(network);

                String address = getEthernetIpAddress();
                setWanDeviceStatus(TYPE_ETHERNET, true);
//                setWanDeviceAPConnection(TYPE_ETHERNET);

                String tmpIpAddress = ethernetIpAddress;
                ethernetIpAddress = address;
                handleNetworkConnectivity(false, tmpIpAddress, ethernetIpAddress);
            }

            @Override
            public void onLost(Network network) {
                super.onLost(network);
                if (Log.INCLUDE) {
                    Log.d(TAG, "ETHERNET.onLost: ETHERNET disconnected");
                }
                setWanDeviceStatus(TYPE_ETHERNET, false);
                handleNetworkConnectivity(false, ethernetIpAddress, "");
            }

            @Override
            public void onLosing(@NonNull Network network, int maxMsToLive) {
                super.onLosing(network, maxMsToLive);
                if (Log.INCLUDE) {
                    Log.d(TAG, "ETHERNET.onLosing");
                }
            }

            @Override
            public void onUnavailable() {
                super.onUnavailable();
                if (Log.INCLUDE) {
                    Log.d(TAG, "ETHERNET.onUnavailable");
                }
            }

            @Override
            public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities networkCapabilities) {
                super.onCapabilitiesChanged(network, networkCapabilities);
                if (Log.INCLUDE) {
                    Log.d(TAG, "ETHERNET.onCapabilitiesChanged");
                }

//                String maxBitRate = "Auto";
//                if(networkCapabilities != null) {
//                    int downSpeed = networkCapabilities.getLinkDownstreamBandwidthKbps();
//                    int upSpeed = networkCapabilities.getLinkUpstreamBandwidthKbps();
//                    maxBitRate = Integer.toString(upSpeed);
//                }
//                if (Log.INCLUDE) {
//                    Log.d(TAG, "ETHERNET.onCapabilitiesChanged: maxBitRate="+maxBitRate);
//                }
            }

            @Override
            public void onLinkPropertiesChanged(@NonNull Network network, @NonNull LinkProperties linkProperties) {
                super.onLinkPropertiesChanged(network, linkProperties);
                if (Log.INCLUDE) {
                    Log.d(TAG, "ETHERNET.onLinkPropertiesChanged");
                }

                setWanDeviceIPConnection(TYPE_ETHERNET, linkProperties);
            }

            @Override
            public void onBlockedStatusChanged(@NonNull Network network, boolean blocked) {
                super.onBlockedStatusChanged(network, blocked);
                if (Log.INCLUDE) {
                    Log.d(TAG, "ETHERNET.onBlockedStatusChanged");
                }
            }
        };

        setNetworkCallback(NetworkCapabilities.TRANSPORT_ETHERNET, ethernetNetworkCallback);
    }

    private void startWifiMonitor(){
        if (Log.INCLUDE) {
            Log.d(TAG, "startWifiMonitor");
        }
        wifiNetworkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                super.onAvailable(network);

                String address = getWifiIpAddress();
                setWanDeviceStatus(TYPE_WIFI, true);
//                setWanDeviceAPConnection(TYPE_WIFI);

                String tmpIpAddress = wifiIpAddress;
                wifiIpAddress = address;
                handleNetworkConnectivity(true, tmpIpAddress, wifiIpAddress);
            }

            @Override
            public void onLost(Network network) {
                super.onLost(network);
                if (Log.INCLUDE) {
                    Log.d(TAG, "Wifi.onLost: WI-FI disconnected");
                }
                setWanDeviceStatus(TYPE_WIFI, false);
                handleNetworkConnectivity(true, wifiIpAddress, "");
            }

            @Override
            public void onLosing(@NonNull Network network, int maxMsToLive) {
                super.onLosing(network, maxMsToLive);
                if (Log.INCLUDE) {
                    Log.d(TAG, "Wifi.onLosing");
                }
            }

            @Override
            public void onUnavailable() {
                super.onUnavailable();
                if (Log.INCLUDE) {
                    Log.d(TAG, "Wifi.onUnavailable");
                }
            }

            @Override
            public void onCapabilitiesChanged(@NonNull Network network, @NonNull NetworkCapabilities networkCapabilities) {
                super.onCapabilitiesChanged(network, networkCapabilities);
                if (Log.INCLUDE) {
                    Log.d(TAG, "Wifi.onCapabilitiesChanged");
                }

//                String maxBitRate = "Auto";
//                if(networkCapabilities != null) {
//                    int downSpeed = networkCapabilities.getLinkDownstreamBandwidthKbps();
//                    int upSpeed = networkCapabilities.getLinkUpstreamBandwidthKbps();
//                    maxBitRate = Integer.toString(upSpeed);
//                }
//                if (Log.INCLUDE) {
//                    Log.d(TAG, "Wifi.onCapabilitiesChanged: maxBitRate="+maxBitRate);
//                }
            }

            @Override
            public void onLinkPropertiesChanged(@NonNull Network network, @NonNull LinkProperties linkProperties) {
                super.onLinkPropertiesChanged(network, linkProperties);
                if (Log.INCLUDE) {
                    Log.d(TAG, "Wifi.onLinkPropertiesChanged");
                }

                setWanDeviceIPConnection(TYPE_WIFI, linkProperties);
            }

            @Override
            public void onBlockedStatusChanged(@NonNull Network network, boolean blocked) {
                super.onBlockedStatusChanged(network, blocked);
                if (Log.INCLUDE) {
                    Log.d(TAG, "Wifi.onBlockedStatusChanged");
                }
            }
        };

        setNetworkCallback(NetworkCapabilities.TRANSPORT_WIFI, wifiNetworkCallback);
    }

    private void setNetworkCallback(int transportType, ConnectivityManager.NetworkCallback networkCallback){
        if (Log.INCLUDE) {
            Log.d(TAG, "setNetworkCallback: transportType=" + transportType);
        }
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkRequest.Builder builder = new NetworkRequest.Builder();
            builder.addTransportType(transportType);
            cm.registerNetworkCallback(builder.build(), networkCallback);
        } else{
            if (Log.INCLUDE) {
                Log.d(TAG, "setNetworkCallback: ConnectivityManager is NOT available");
            }
        }
    }

    private void setWanDeviceStatus(int type, boolean available){
        try {
            WANDevice wanDevice = root.wanDevices.getWanDevice(type);
            if (wanDevice == null) {
                wanDevice = root.wanDevices.addWanDevice(new WANDevice(type));
            }
            String status = "NoLink";
            if(available){
                status = "Up";
            }
            wanDevice.wanEthernetInterfaceConfig.Status.setValue(status);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setWanDeviceMacAddress(int type, String macAddress){
        try {
            WANDevice wanDevice = root.wanDevices.getWanDevice(type);
            if (wanDevice == null) {
                wanDevice = root.wanDevices.addWanDevice(new WANDevice(type));
            }
            wanDevice.wanEthernetInterfaceConfig.MACAddress.setValue(macAddress);

            WANConnectionDevice wanConnectionDevice = wanDevice.wanConnectionDevices.getWANConnectionDevice(1);
            if(wanConnectionDevice == null){
                wanConnectionDevice = wanDevice.wanConnectionDevices.addWANConnectionDevice(new WANConnectionDevice(1));
            }
            for(final ModelObject object : wanConnectionDevice.getChilds()) {
                if (object instanceof CWMPMultiObject && object instanceof WANIPConnections) {
                    WANIPConnection wanipConnection = ((WANIPConnections) object).getWANIPConnection(1);
                    if (wanipConnection == null) {
                        wanipConnection = ((WANIPConnections) object).addWANIPConnection(new WANIPConnection(1));
                    }

                    if (type == TYPE_ETHERNET) {
                        wanipConnection.MACAddress.setValue(ethernetMacAddress);
                    } else if (type == TYPE_WIFI) {
                        wanipConnection.MACAddress.setValue(wifiMacAddress);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setWanDeviceConfig(int type, String maxBitRate, String duplexMode){
        try {
            WANDevice wanDevice = root.wanDevices.getWanDevice(type);
            if (wanDevice == null) {
                wanDevice = root.wanDevices.addWanDevice(new WANDevice(type));
            }
//            wanDevice.wanEthernetInterfaceConfig.MaxBitRate.setValue(maxBitRate); // TODO
//            wanDevice.wanEthernetInterfaceConfig.DuplexMode.setValue(duplexMode); // TODO
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setWanDeviceIPConnection(int type, LinkProperties linkProperties){
        if (Log.INCLUDE) {
            Log.d(TAG, "setWanDeviceIPConnection: type="+type);
        }
        WANDevice wanDevice = root.wanDevices.getWanDevice(type);
        if(wanDevice == null) {
            wanDevice = root.wanDevices.addWanDevice(new WANDevice(type));
        }
        WANConnectionDevice wanConnectionDevice = wanDevice.wanConnectionDevices.getWANConnectionDevice(1);
        if(wanConnectionDevice == null){
            wanConnectionDevice = wanDevice.wanConnectionDevices.addWANConnectionDevice(new WANConnectionDevice(1));
        }
//            InternetGatewayDevice.WANDevice.{i}.WANConnectionDevice.{i}.WANIPConnection.{i}.X_TTBB_APConnection.xxx
        for(final ModelObject object : wanConnectionDevice.getChilds()) {
//                Log.d(TAG, "notifyEdidInfo: ModelObject >> "+ object.getFullName());
            if (object instanceof CWMPMultiObject && object instanceof WANIPConnections) {
//                    Log.d(TAG, "notifyEdidInfo: CWMPMultiObject >> "+ object.getFullName());
                WANIPConnection wanipConnection = ((WANIPConnections) object).getWANIPConnection(1);
                if(wanipConnection == null) {
                    wanipConnection = ((WANIPConnections) object).addWANIPConnection(new WANIPConnection(1));
                }

                if(type == TYPE_ETHERNET) {
//                    wanipConnection.Name.setValue(NAME_ETHERNET);
                    setParamValue(wanipConnection.MACAddress, ethernetMacAddress);
                    setParamValue(wanipConnection.ExternalIPAddress, ethernetIpAddress);
                }else if(type == TYPE_WIFI) {
//                    wanipConnection.Name, NAME_WIFI);
                    setParamValue(wanipConnection.MACAddress, wifiMacAddress);
                    setParamValue(wanipConnection.ExternalIPAddress, wifiIpAddress);
                }
                try {
                    int upTime = (int) System.currentTimeMillis() / 1000;
                    setParamValue(wanipConnection.Uptime, upTime);
                }catch (Exception e){
                    e.printStackTrace();
                }

                String addressType = getAddressType(type);
                setParamValue(wanipConnection.AddressingType, addressType);
                if(STATIC.equalsIgnoreCase(addressType)){
                    String defaultGateway = "";
                    for(RouteInfo routeInfo : linkProperties.getRoutes()){
                        try {
                            if(routeInfo.isDefaultRoute()){
                                defaultGateway = routeInfo.getGateway().getHostAddress();
                                break;
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    if (Log.INCLUDE) {
                        Log.d(TAG, "setWanDeviceIPConnection: defaultGateway="+defaultGateway);
                    }
                    setParamValue(wanipConnection.DefaultGateway, defaultGateway);
                }

                int prefixLength = 0;
                for(LinkAddress linkAddress : linkProperties.getLinkAddresses()){
                    if(linkAddress.getPrefixLength() <= 32){
                        prefixLength = linkAddress.getPrefixLength();
                        break;
                    }
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "setWanDeviceIPConnection: prefixLength="+prefixLength+", getSubnetMask="+getSubnetMask(prefixLength));
                }
                setParamValue(wanipConnection.SubnetMask, getSubnetMask(prefixLength));

                String dnsServers = "";
                if(linkProperties.getDnsServers() != null) {
                    InetAddress inetAddress;
                    String dnsAddress = "";
                    int dnsSize = linkProperties.getDnsServers().size();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "setWanDeviceIPConnection: dns size=" + dnsSize);
                    }
                    for (int i=0; i<dnsSize; i++) {
                        inetAddress = linkProperties.getDnsServers().get(i);
                        dnsAddress = inetAddress.getHostAddress();
                        if (inetAddress instanceof Inet4Address) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "setWanDeviceIPConnection: [" + i + "]dns=" + dnsAddress);
                            }
                            dnsServers += dnsAddress;
                            if(i < dnsSize-1){
                                dnsServers += ",";
                            }
                        }
                    }
                    if (Log.INCLUDE) {
                        Log.d(TAG, "setWanDeviceIPConnection: dnsServers=" + dnsServers);
                    }
                }
                setParamValue(wanipConnection.DNSServers, dnsServers);

//                wanipConnection.ConnectionTrigger.setValue(""); // TODO
//                wanipConnection.Option60.setValue(""); // TODO
//                wanipConnection.Option77.setValue(""); // TODO
//                wanipConnection.X_TTBB_Option60Enable.setValue(0); // TODO
//                wanipConnection.X_TTBB_Option77Enable.setValue(0); // TODO

                break;
            }
        }
    }

    private String getIpAddress(int type) throws Exception {
        for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
            NetworkInterface intf = en.nextElement();
            for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                InetAddress inetAddress = enumIpAddr.nextElement();
                if(inetAddress == null) continue;

                if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                    if(type == TYPE_ETHERNET && NAME_ETHERNET.equalsIgnoreCase(intf.getDisplayName())){
                        return StringUtils.nullToEmpty(inetAddress.getHostAddress());
                    }else if(type == TYPE_WIFI && NAME_WIFI.equalsIgnoreCase(intf.getDisplayName())){
                        return StringUtils.nullToEmpty(inetAddress.getHostAddress());
                    }
                }
                else {
                    // skip
                }
            }
        }
        return "";
    }

    /**
     * Ethernet IP 주소를 리턴합니다.
     *
     * @return
     */
    public String getEthernetIpAddress(){
        String strIpAddress ="";
        try{
            strIpAddress = getIpAddress(TYPE_ETHERNET);
        }catch (Exception e){
            if (Log.INCLUDE) {
                Log.d(TAG, "getEthernetIpAddress: exception occurred while getting ip address");
                Log.e(TAG, e.toString());
            }
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "getEthernetIpAddress: IP=" + strIpAddress);
        }
        return strIpAddress;
    }

    /**
     * WI-FI IP 주소를 리턴합니다.
     *
     * @return
     */
    public String getWifiIpAddress(){
        String strIpAddress = "";
        try {
            strIpAddress = getIpAddress(TYPE_WIFI);
        }catch (Exception e){
            if (Log.INCLUDE) {
                Log.d(TAG, "getWifiIpAddress: exception occurred while getting ip address");
                Log.e(TAG, e.toString());
            }
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "getWifiIpAddress: IP=" + strIpAddress);
        }
        return strIpAddress;
    }

    public String getMacAddress(String delimiter) {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
//                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    continue;
                }

                StringBuilder strMacAddress = new StringBuilder();
                for (byte b : macBytes) {
                    strMacAddress.append(String.format("%02X",b));
                    strMacAddress.append(delimiter);
                }

                if (strMacAddress.length() > 0) {
                    strMacAddress.deleteCharAt(strMacAddress.length() - 1);
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "getMacAddress: "+nif.getName()+": MAC address=" + strMacAddress.toString());
                }
                if (nif.getName().equalsIgnoreCase(NAME_WIFI)){
                    wifiMacAddress = strMacAddress.toString();
                }else if (nif.getName().equalsIgnoreCase(NAME_ETHERNET)){
                    ethernetMacAddress = strMacAddress.toString();
                }
            }
        } catch (Exception e) {
            if (Log.INCLUDE) {
                Log.d(TAG, "getMacAddress: exception occurred while getting mac address");
                Log.e(TAG, e.toString());
            }
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "getMacAddress: ethernetMac="+ethernetMacAddress+", wifiMac="+wifiMacAddress);
        }

        if(!ethernetMacAddress.isEmpty()){
            return ethernetMacAddress;
        }
        return wifiMacAddress;
    }

    private String getAddressType(int type){

        Process cmdProcess = null;
        BufferedReader reader = null;
        String line = "";
        try {
            cmdProcess = Runtime.getRuntime().exec("getprop persist.vendor.net.ipassignment");
            reader = new BufferedReader(new InputStreamReader(cmdProcess.getInputStream()));
            line = reader.readLine();
            if (Log.INCLUDE) {
                Log.d(TAG, "getAddressType(): type="+type+", assignment="+line);
            }
            if(line != null && (DHCP.equalsIgnoreCase(line) || STATIC.equalsIgnoreCase(line))) {
                return line;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
            }
            cmdProcess.destroy();
        }
        return DHCP;
    }

    private String getLocalDns(int type, int index){
        Process cmdProcess = null;
        BufferedReader reader = null;
        String line = "";
        try {
            cmdProcess = Runtime.getRuntime().exec("getprop net.dns"+index);
            reader = new BufferedReader(new InputStreamReader(cmdProcess.getInputStream()));
            line = reader.readLine();
            if (Log.INCLUDE) {
                Log.d(TAG, "getLocalDNS: type="+type+", dns="+line);
            }
            return line;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
            }
            cmdProcess.destroy();
        }
        return "";
    }

    private String getLocalMask(int type){
        Process cmdProcess = null;
        BufferedReader reader = null;
        String line = "";
        try {
            if(type == TYPE_ETHERNET){
                cmdProcess = Runtime.getRuntime().exec("getprop dhcp.eth0.mask");
            }else if(type == TYPE_WIFI){
                cmdProcess = Runtime.getRuntime().exec("getprop dhcp.wlan0.mask");
            }

            reader = new BufferedReader(new InputStreamReader(cmdProcess.getInputStream()));
            line = reader.readLine();
            if (Log.INCLUDE) {
                Log.d(TAG, "getLocalMask("+type+"): "+line);
            }
            return line;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
            }
            cmdProcess.destroy();
        }
        return "";
    }

    private String getLocalGateway(int type){
        Process cmdProcess = null;
        BufferedReader reader = null;
        String line = "";
        try {
            if(type == TYPE_ETHERNET){
                cmdProcess = Runtime.getRuntime().exec("getprop dhcp.eth0.gateway");
            }else if(type == TYPE_WIFI){
                cmdProcess = Runtime.getRuntime().exec("getprop dhcp.wlan0.gateway");
            }

            reader = new BufferedReader(new InputStreamReader(cmdProcess.getInputStream()));
            line = reader.readLine();
            if (Log.INCLUDE) {
                Log.d(TAG, "getLocalGateway("+type+"): "+line);
            }
            return line;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
            }
            cmdProcess.destroy();
        }
        return "";
    }

    private void getWifiStandard(int linkSpeed, int frequency){
        if (Log.INCLUDE) {
            Log.d(TAG, "getWifiStandard: linkSpeed="+linkSpeed+", frequency="+frequency);
        }
//        802.11ax	2019	600 to 2401 Mbps
//        802.11ac	2013	433 to 1733 Mbps
//        802.11n	2009	72 to 217 Mbps
//        802.11g	2003	54 Mbps	Wi-Fi 3
//        802.11a	1999	54 Mbps	Wi-Fi 2
//        802.11b	1999	11 Mbps	Wi-Fi 1
//        802.11	1997	2 Mbps	Wi-Fi 0 (beta)
        String standard = "";
        if(linkSpeed >= 800 && frequency >= 5000){

        } else if(linkSpeed > 150 && linkSpeed < 800 && frequency >= 5000){
            standard = "802.11ac";
        } else if(linkSpeed > 54 && linkSpeed <= 150 && (frequency >= 2400 || frequency >= 5000)){
            standard = "802.11n";
        } else if(linkSpeed > 11 && linkSpeed <= 54){
            if(frequency < 5800) {
                standard = "802.11a";
            }else if(frequency < 2400) {
                standard = "802.11g";
            }
        } else if(linkSpeed > 1.2 && linkSpeed <= 11 && frequency < 2400){
            standard = "802.11b";
        } else if(linkSpeed <= 1.2 && frequency < 2400){
            standard = "802.11(legacy)";
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "getWifiStandard: standard="+standard);
        }
    }

    private String convertIntToIp(int ipAddress)
    {
//        byte[] addressBytes = {
//                (byte) (0xff & ipAddress),
//                (byte) (0xff & (ipAddress >> 8)),
//                (byte) (0xff & (ipAddress >> 16)),
//                (byte) (0xff & (ipAddress >> 24))
//        };
//        try {
//            return InetAddress.getByAddress(addressBytes).toString();
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }
//        return "";
        return ( ipAddress & 0xFF) + "." +
               ((ipAddress >> 8 ) & 0xFF) + "." +
               ((ipAddress >> 16 ) & 0xFF) + "." +
               ((ipAddress >> 24 ) & 0xFF );

    }

    private String getSubnetMask(int prefixLength){
        try {
            int shift = 0xffffffff << (32 - prefixLength);
            int oct1 = ((byte) ((shift & 0xff000000) >> 24)) & 0xff;
            int oct2 = ((byte) ((shift & 0x00ff0000) >> 16)) & 0xff;
            int oct3 = ((byte) ((shift & 0x0000ff00) >> 8)) & 0xff;
            int oct4 = ((byte) (shift & 0x000000ff)) & 0xff;
            return oct1+"."+oct2+"."+oct3+"."+oct4;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Ethernet 연결 여부를 리턴합니다.
     *
     * @return
     */
    public boolean isEthernetConnected() {
        return isNetworkConnected(TYPE_ETHERNET);
    }

    /**
     * WI-FI 연결 여부를 리턴합니다.
     *
     * @return
     */
    public boolean isWifiConnected() {
        return isNetworkConnected(TYPE_WIFI);
    }

    private boolean isNetworkConnected(int type) {
        int transportType = -1;
        if(type == TYPE_ETHERNET){
            transportType = NetworkCapabilities.TRANSPORT_ETHERNET;
        }else if(type == TYPE_WIFI){
            transportType = NetworkCapabilities.TRANSPORT_WIFI;
        }
        boolean connected = false;
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(CONNECTIVITY_SERVICE);
        if (cm != null) {
            Network[] networks = cm.getAllNetworks();
            for (Network network : networks) {
                NetworkCapabilities capabilities = cm.getNetworkCapabilities(network);
                if (capabilities != null) {
                    if (capabilities.hasTransport(transportType)) {
                        connected = true;
                        break;
                    }
                }
            }
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "isNetworkConnected: type=" + type+", connected=" + connected);
        }
        return connected;
    }

    /**
     * 사설망인지 체크 하는 API
     * Class A : 10.0.0.0 ~ 10.255.255.255
     * Class B : 172.16.0.0 ~ 172.31.255.255
     * Class C : 192.168.0.0 ~ 192.168.255.255
     */
    public boolean isPrivateIpAddress(String ipAddress) {
        boolean ret = false;

        if(ipAddress == null) return false;

        int[] ip = new int[4];
        String[] ipSec = ipAddress.split("\\.");
        if(ipSec.length != 4) return ret;
        for (int i = 0; i < 4; i++) {
            ip[i] = Integer.parseInt(ipSec[i]);
        }

        if(ip[0] == 10) { // Class A
            if(ip[1] >= 0 && ip[1] <= 255 && ip[2] >= 0 && ip[2] <= 255 && ip[3] >= 0 && ip[3] <= 255) ret = true;
        }
        else if(ip[0] == 172) { // Class B
            if(ip[1] >= 16 && ip[1] <= 31 && ip[2] >= 0 && ip[2] <= 255 && ip[3] >= 0 && ip[3] <= 255) ret = true;
        }
        else if(ip[0] == 192) { // Class C
            if(ip[1] == 168 && ip[2] >= 0 && ip[2] <= 255 && ip[3] >= 0 && ip[3] <= 255) ret = true;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, ipAddress + " is " + (ret ? "private IP" : "public IP"));
        }
        return ret;
    }

    public boolean isValidIpAddress(String ipaddress) {
        String ipAddressRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
        Pattern p = Pattern.compile(ipAddressRegex);
        //Match the input string with the pattern
        Matcher m = p.matcher(ipaddress);
        return m.matches();
    }
}
