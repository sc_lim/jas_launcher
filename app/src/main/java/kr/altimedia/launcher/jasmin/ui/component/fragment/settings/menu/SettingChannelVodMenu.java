/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.menu;

import kr.altimedia.launcher.jasmin.R;

public enum SettingChannelVodMenu implements SettingMenu {
    SUBTITLES(R.string.subtitles, 0), SERIES_AUTO_PLAY(R.string.series_autoplay, 1);

    private static final int MENU_TITLE = R.string.setting_menu_channel_vod;

    private int menuName;
    private int menuId;

    SettingChannelVodMenu(int menuName, int menuId) {
        this.menuName = menuName;
        this.menuId = menuId;
    }

    public static int getMenuTitle() {
        return MENU_TITLE;
    }

    @Override
    public int getMenuName() {
        return menuName;
    }

    @Override
    public int getMenuId() {
        return menuId;
    }

    @Override
    public boolean isEnable() {
        return true;
    }
}
