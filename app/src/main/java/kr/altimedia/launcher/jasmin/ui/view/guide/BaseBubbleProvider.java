/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.guide;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by mc.kim on 03,07,2020
 */

abstract class BaseBubbleProvider extends RelativeLayout {

    protected OnInformationInteractor mOnInformationInteractor = new OnInformationInteractor() {
        @Override
        public void onItemUnSelected(View view) {
            onProgramUnSelected(view);

        }

        @Override
        public void onItemSelected(View view) {
            onProgramSelected(view);

        }
    };

    public BaseBubbleProvider(Context context) {
        this(context, null);
    }

    public BaseBubbleProvider(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseBubbleProvider(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public OnInformationInteractor getInformationInteractor() {
        return mOnInformationInteractor;
    }

    public abstract void onProgramSelected(View itemView);

    public abstract void onProgramUnSelected(View itemView);

    public interface OnInformationInteractor {
        void onItemSelected(View view);

        void onItemUnSelected(View view);
    }
}
