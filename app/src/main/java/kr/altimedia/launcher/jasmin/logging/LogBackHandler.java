/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.logging;

import android.content.Context;

import com.altimedia.util.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Vector;

import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.ChannelWatchLog;
import kr.altimedia.launcher.jasmin.dm.user.object.LoginResult;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * LogBackManager
 */
public class LogBackHandler {

    private final String CLASS_NAME = LogBackHandler.class.getName();
    private final String TAG = LogBackHandler.class.getSimpleName();

    private final long REPORTING_CYCLE_TIME = 5 * TimeUtil.MIN; // milliseconds

    private String deviceType;

    private LogSender logSender;
    private Thread logSendThread;

    private Vector channelLogQueue = null;
    private Context context;

    public void dispose() {
        stop();

        if (channelLogQueue != null) {
            channelLogQueue.removeAllElements();
            channelLogQueue = null;
        }
        context = null;
    }

    public LogBackHandler(Context context) {
        this.context = context;
        this.channelLogQueue = new Vector();
    }

    /**
     * LogBackManager를 시작합니다.
     */
    void start(String deviceType) {
        if (Log.INCLUDE) {
            Log.d(TAG, "start: deviceType=" + deviceType);
        }

        this.deviceType = deviceType;

        startLogSender();
    }

    /**
     * LogBackManager를 중지합니다.
     *
     * @return
     */
    boolean stop() {
        stopLogSender();

        return true;
    }

    /**
     * 채널 시청 로그를 큐에 적재합니다.
     *
     * @param channelId channel identifier
     * @param entryTime entry time for corresponding channel
     */
    public void addChannelLog(String channelId, long entryTime) {
        synchronized (channelLogQueue) {
            String formattedTime = TimeUtil.getModifiedDate(entryTime, "yyyy-MM-dd HH:mm:ss");
            channelLogQueue.add(new ChannelWatchLog(channelId, formattedTime));
            if (Log.INCLUDE) {
                Log.d(TAG, "addChannelLog: queue size=" + channelLogQueue.size());
            }
        }
    }

    private void sendChannelLog() {
        if (Log.INCLUDE) {
            Log.d(TAG, "sendChannelLog");
        }

        if (channelLogQueue == null || channelLogQueue.size() <= 0) {
            if (Log.INCLUDE) {
                Log.d(TAG, "sendChannelLog: empty queue");
            }
            return;
        }

        JsonArray jsonArray = new JsonArray();
        try {
            JsonObject jsonChild;
            ChannelWatchLog channelWatchLog;
            for (int i = 0; i < channelLogQueue.size(); i++) {
                channelWatchLog = (ChannelWatchLog) channelLogQueue.get(i);
                jsonChild = new JsonObject();
                jsonChild.addProperty("channelId", channelWatchLog.getChannelId());
                jsonChild.addProperty("entDt", channelWatchLog.getEntryTime());
                jsonArray.add(jsonChild);
            }
        } catch (Exception e) {
        }

        String said = AccountManager.getInstance().getSaId();
        String profileId = AccountManager.getInstance().getProfileId();
        UserDataManager userDataManager = new UserDataManager();
        userDataManager.postWatchingInfo(
                said,
                profileId,
                deviceType,
                jsonArray,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed");
                        }
                        channelLogQueue.removeAllElements();
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onSuccess");
                        }
                        channelLogQueue.removeAllElements();
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        channelLogQueue.removeAllElements();
                    }
                });
    }

    private void startLogSender() {
        stopLogSender();

        if (Log.INCLUDE) {
            Log.d(TAG, "startLogSender");
        }
        if (logSendThread != null && !logSendThread.isInterrupted()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "startLogSender: already started");
            }
        } else {
            logSender = new LogSender();
            logSendThread = new Thread(logSender);
            logSendThread.start();
        }
    }

    private void stopLogSender() {
        if (Log.INCLUDE) {
            Log.d(TAG, "stopLogSender");
        }
        if (logSendThread != null && !logSendThread.isInterrupted()) {
            logSendThread.interrupt();
            logSendThread = null;
        }
    }

    private long getReportingCycleTime() {
        long reportingCycleTime = REPORTING_CYCLE_TIME;
        try {
            LoginResult loginResult = (LoginResult) AccountManager.getInstance().getData(AccountManager.KEY_LOGIN_INFO);
            reportingCycleTime = Integer.parseInt(loginResult.getWatchingHistoryCycle()) * TimeUtil.MIN;
        } catch (Exception e) {
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "getReportingCycleTime: "+reportingCycleTime);
        }
        if(reportingCycleTime <= 0){
            return REPORTING_CYCLE_TIME;
        }else{
            return reportingCycleTime;
        }
    }

    private class LogSender implements Runnable {

        LogSender() {
        }

        @Override
        public void run() {
            if (Log.INCLUDE) {
                Log.d(TAG, "LogSender: run");
            }
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    Thread.sleep(getReportingCycleTime());

                    sendChannelLog();
                }
            } catch (InterruptedException e) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "LogSender: exception occurred: " + e.toString());
                }
            } finally {
                if (Log.INCLUDE) {
                    Log.d(TAG, "LogSender: dead");
                }
            }
        }
    }
}
