/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Duration;
import kr.altimedia.launcher.jasmin.dm.contents.obj.DurationUnit;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.def.RentalType;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentButton;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentWrapper;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseDiscountType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseTextButton;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter.ContentPricePresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter.DiscountPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter.PaymentPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter.PaymentTypePresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter.PurchaseTextButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.row.DiscountRow;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.row.PaymentRow;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPurchaseDialogFragment.KEY_DISCOUNT_TYPE_LIST;
import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.VodPurchaseDialogFragment.KEY_PAYMENT_TYPE_LIST;

public class VodPurchaseFragment extends Fragment
        implements DiscountPresenter.OnDiscountListener, PaymentPresenter.OnFocusPaymentListener,
        View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = VodPurchaseFragment.class.getName();
    private static final String TAG = VodPurchaseFragment.class.getSimpleName();

    public static final String KEY_CONTENT_TITLE = "CONTENT_TITLE";
    public static final String KEY_CONTENT_POSTER_URL = "CONTENT_POSTER_URL";
    public static final String KEY_PRODUCT = "PRODUCT";

    private final int PRICE_ROW = 0;
    private final int DISCOUNT_ROW = 1;
    private final int PAYMENT_ROW = 2;

    private final int NEXT_BUTTON = 0;
    private final int CANCEL_BUTTON = 1;

    private Product product;
    private RentalType rentalType;

    private PaymentWrapper paymentWrapper;
    private ArrayList<PaymentType> discountTypes = new ArrayList<>();
    private ArrayList<PaymentType> paymentTypes = new ArrayList<>();

    private VerticalGridView gridView;
    private HorizontalGridView buttonGridView;

    private ClassPresenterSelector mPresenterSelector;
    private ArrayObjectAdapter mAdapter;
    private PaymentItemBridgeAdapter paymentItemBridgeAdapter;

    private CouponSelectDialogFragment couponSelectDialogFragment;

    private PaymentItemBridgeAdapter.OnPaymentClickListener onPaymentClickListener;

    public VodPurchaseFragment() {

    }

    public static VodPurchaseFragment newInstance(Bundle bundle) {
        VodPurchaseFragment fragment = new VodPurchaseFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setOnPaymentClickListener(PaymentItemBridgeAdapter.OnPaymentClickListener onPaymentClickListener) {
        this.onPaymentClickListener = onPaymentClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vod_purchase, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        initData();
        initPurchaseInfo(view);
        initGirView(view);
        initButton(view);

        view.addOnUnhandledKeyEventListener(this);
    }

    private void initData() {
        this.product = getArguments().getParcelable(KEY_PRODUCT);
        this.rentalType = product.getRentalType();

        paymentWrapper = new PaymentWrapper(product.getPrice());
    }

    private void initPurchaseInfo(View view) {
        initPoster(view);

        String title = getArguments().getString(KEY_CONTENT_TITLE);
        ((TextView) view.findViewById(R.id.title)).setText(title);
        String type = view.getContext().getString(rentalType.getName());
        ((TextView) view.findViewById(R.id.purchase_type)).setText(type);

        setDuration(view);
    }

    private void setDuration(View view) {
        TextView durationView = view.findViewById(R.id.duration);
        TextView durationUnitView = view.findViewById(R.id.duration_unit);

        Duration duration = product.getDuration();
        DurationUnit durationUnit = duration.getUnit();
        int value = duration.getValue();

        int id;
        switch (durationUnit) {
            case UNIT_DAY:
                id = value > 1 ? R.string.days_upper : R.string.day_upper;
                break;
            case UNIT_MONTH:
                id = value > 1 ? R.string.months : R.string.month;
                break;
            case UNIT_YEAR:
                id = value > 1 ? R.string.years : R.string.year;
                break;
            default:
                String unit = getString(R.string.vod_watch_days_unlimited);
                durationUnitView.setText(unit);
                return;
        }

        String dur = String.valueOf(value);
        String unit = getString(id);
        durationView.setText(dur);
        durationUnitView.setText(unit);
    }

    private void initPoster(View view) {
        String posterUrl = getArguments().getString(KEY_CONTENT_POSTER_URL);
        ImageView poster = view.findViewById(R.id.poster);
        Glide.with(poster.getContext())
                .load(posterUrl).diskCacheStrategy(DiskCacheStrategy.DATA)
                .centerCrop()
                .placeholder(R.drawable.poster_default)
                .error(R.drawable.poster_default)
                .into(poster);
    }

    private void initButton(View view) {
        buttonGridView = view.findViewById(R.id.buttons_gridView);

        PurchaseTextButtonPresenter mPurchaseTextButtonPresenter = new PurchaseTextButtonPresenter();
        mPurchaseTextButtonPresenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = buttonGridView.indexOfChild(v);

                if (index == NEXT_BUTTON) {
                    PaymentButton mPaymentButton = new PaymentButton(null, true);
                    onPaymentClickListener.onPaymentClick(mPaymentButton);
                } else {
                    ((VodPurchaseDialogFragment) getParentFragment()).dismiss();
                }
            }
        });
        mPurchaseTextButtonPresenter.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    View priceRowView = getViewHolder(PRICE_ROW).view;
                    View discountRowView = getViewHolder(DISCOUNT_ROW).view;
                    View paymentRowView = getViewHolder(PAYMENT_ROW).view;

                    int unFocus = ContextCompat.getColor(getContext(), R.color.transparent);

                    priceRowView.setBackgroundColor(unFocus);
                    discountRowView.setBackgroundColor(unFocus);
                    paymentRowView.setBackgroundColor(unFocus);

//                    PaymentPresenter.ViewHolder paymentVh = (PaymentPresenter.ViewHolder) getRowViewHolder(PAYMENT_ROW).getViewHolder();
//                    paymentVh.setPromotion(false);
                }
            }
        });

        ArrayObjectAdapter mArrayObjectAdapter = new ArrayObjectAdapter(mPurchaseTextButtonPresenter);
        mArrayObjectAdapter.add(NEXT_BUTTON, new PurchaseTextButton(R.string.next, false));
        mArrayObjectAdapter.add(CANCEL_BUTTON, new PurchaseTextButton(R.string.cancel, true));

        ItemBridgeAdapter mItemBridgeAdapter = new ItemBridgeAdapter(mArrayObjectAdapter);
        buttonGridView.setAdapter(mItemBridgeAdapter);
    }

    private void initGirView(View view) {
        mPresenterSelector = new ClassPresenterSelector();
        mAdapter = new ArrayObjectAdapter(mPresenterSelector);

        initPriceRow();
        initDiscountRow();
        initPaymentRow();
        initGridView(view);
        initBrowseLayout(view);
    }

    private void initBrowseLayout(View view) {
        BrowseFrameLayout browseFrameLayout = view.findViewById(R.id.browse_layout);
        browseFrameLayout.setOnChildFocusListener(new BrowseFrameLayout.OnChildFocusListener() {
            @Override
            public boolean onRequestFocusInDescendants(int var1, Rect var2) {
                return false;
            }

            @Override
            public void onRequestChildFocus(View child, View focused) {
                if (gridView.getChildCount() < 3) {
                    return;
                }

                //for BG Focus GUI
                int selectedIndex = gridView.getSelectedPosition();
                ContentPricePresenter.ViewHolder contentPriceVh = (ContentPricePresenter.ViewHolder) getRowViewHolder(PRICE_ROW).getViewHolder();
                DiscountPresenter.ViewHolder discountVh = (DiscountPresenter.ViewHolder) getRowViewHolder(DISCOUNT_ROW).getViewHolder();
                View priceRowView = contentPriceVh.view;
                View discountRowView = discountVh.view;
                View paymentRowView = getViewHolder(PAYMENT_ROW).view;

                int focus = ContextCompat.getColor(getContext(), R.color.color_FF131313);
                int unFocus = ContextCompat.getColor(getContext(), R.color.transparent);

                if (selectedIndex == DISCOUNT_ROW) {
                    int index = discountVh.getSelectedPosition();
                    contentPriceVh.updateDiscountLimit(index);

                    priceRowView.setBackgroundColor(focus);
                    discountRowView.setBackgroundColor(focus);
                    paymentRowView.setBackgroundColor(unFocus);
                } else {
                    priceRowView.setBackgroundColor(unFocus);
                    discountRowView.setBackgroundColor(unFocus);
                    paymentRowView.setBackgroundColor(focus);
                    contentPriceVh.updateDiscountLimit(-1);
                }

//                if (selectedIndex != PAYMENT_ROW) {
//                    PaymentPresenter.ViewHolder paymentVh = (PaymentPresenter.ViewHolder) getRowViewHolder(PAYMENT_ROW).getViewHolder();
//                    paymentVh.setPromotion(false);
//                }
            }
        });
    }

    private Presenter.ViewHolder getViewHolder(int index) {
        View view = gridView.getChildAt(index);
        if (view == null) {
            return null;
        }

        ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) gridView.getChildViewHolder(view);
        return viewHolder.getViewHolder();
    }

    private void initPriceRow() {
        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter();
        arrayObjectAdapter.add(product);

        ListRow priceRow = new ListRow(arrayObjectAdapter);
        mAdapter.add(priceRow);
        mPresenterSelector.addClassPresenter(ListRow.class, new ContentPricePresenter());
    }

    private void initDiscountRow() {
        DiscountPresenter discountPresenter = new DiscountPresenter();
        discountPresenter.setOnDiscountListener(this);

        DiscountRow discountRow = getDiscountRow(discountPresenter);
        mAdapter.add(discountRow);
        mPresenterSelector.addClassPresenter(DiscountRow.class, discountPresenter);
    }

    private void initPaymentRow() {
        PaymentRow paymentRow = getPaymentRow();
        mAdapter.add(paymentRow);

        PaymentPresenter paymentPresenter = new PaymentPresenter();
        paymentPresenter.setOnPaymentClickListener(onPaymentClickListener);
        paymentPresenter.setOnFocusPaymentListener(this);
        mPresenterSelector.addClassPresenter(PaymentRow.class, paymentPresenter);
    }

    private void initGridView(View view) {
        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(mAdapter);
        gridView = view.findViewById(R.id.grid_view);
        gridView.setAdapter(itemBridgeAdapter);
    }

    private ItemBridgeAdapter.ViewHolder getRowViewHolder(int index) {
        View view = gridView.getChildAt(index);
        if (view != null) {
            ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) gridView.getChildViewHolder(view);
            return viewHolder;
        }

        return null;
    }

    private ItemBridgeAdapter.ViewHolder getButtonsViewHolder(int index) {
        View view = buttonGridView.getChildAt(index);
        if (view != null) {
            ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) buttonGridView.getChildViewHolder(view);
            return viewHolder;
        }

        return null;
    }

    private void updateDiscount(float discount) {
        ItemBridgeAdapter.ViewHolder viewHolder = getRowViewHolder(PRICE_ROW);
        if (viewHolder != null) {
            ContentPricePresenter.ViewHolder vh = (ContentPricePresenter.ViewHolder) viewHolder.getViewHolder();
            vh.setDiscount(discount);
        }
    }

    private void updatePayment(float totalPayment) {
        boolean isEnable = totalPayment > 0;
        boolean paymentEnable = false;

        ItemBridgeAdapter.ViewHolder rowVh = getRowViewHolder(PAYMENT_ROW);
        if (rowVh != null) {
            PaymentPresenter.ViewHolder vh = (PaymentPresenter.ViewHolder) rowVh.getViewHolder();
            paymentEnable = vh.updatePaymentEnable(isEnable);
        }

        ItemBridgeAdapter.ViewHolder buttonVh = getButtonsViewHolder(NEXT_BUTTON);
        if (buttonVh != null) {
            PurchaseTextButtonPresenter.ViewHolder vh = (PurchaseTextButtonPresenter.ViewHolder) buttonVh.getViewHolder();
            vh.setEnable(!isEnable && !paymentEnable);
            if (!paymentEnable) {
                buttonGridView.setSelectedPosition(NEXT_BUTTON);
            }
        }
    }

    private DiscountRow getDiscountRow(DiscountPresenter discountPresenter) {
        discountTypes = (ArrayList<PaymentType>) getArguments().getSerializable(KEY_DISCOUNT_TYPE_LIST);

        PurchaseDiscountType[] types = PurchaseDiscountType.values();
        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter();
        boolean isFocusableDiscounrRow = false;

        for (int i = 0; i < types.length; i++) {
            PurchaseDiscountType type = types[i];
            for (PaymentType paymentType : discountTypes) {
                if (type.getCode().equalsIgnoreCase(paymentType.getCode())) {
                    isFocusableDiscounrRow = true;
                    type.setAvailablePayment(true);
                    break;
                }
            }

            arrayObjectAdapter.add(type);
        }

        discountPresenter.setFocusableDiscountRow(isFocusableDiscounrRow);

        //handle key in presenter.viewHolder for update discount info
        DiscountRow discountRow = new DiscountRow(product, arrayObjectAdapter);
        return discountRow;
    }

    private PaymentRow getPaymentRow() {
        paymentTypes = (ArrayList<PaymentType>) getArguments().getSerializable(KEY_PAYMENT_TYPE_LIST);

        PaymentTypePresenter paymentTypePresenter = new PaymentTypePresenter();
        ArrayObjectAdapter paymentObjectAdapter = new ArrayObjectAdapter(paymentTypePresenter);

        for (PaymentType paymentType : paymentTypes) {
            paymentObjectAdapter.add(new PaymentButton(paymentType, true));
        }

        //handle key in bridgeAdapter
        paymentItemBridgeAdapter = new PaymentItemBridgeAdapter(paymentObjectAdapter);
        PaymentRow paymentRow = new PaymentRow(paymentItemBridgeAdapter);
        return paymentRow;
    }

    public PaymentWrapper getPaymentWrapper() {
        return paymentWrapper;
    }

    @Override
    public TvOverlayManager getTvOverlayManager() {
        return ((VodPurchaseDialogFragment) getParentFragment()).getTvOverlayManager();
    }

    @Override
    public void onChangeDiscountValue(PurchaseDiscountType purchaseDiscount, PaymentWrapper paymentWrapper) {
        if (purchaseDiscount == PurchaseDiscountType.COUPON_DISCOUNT && couponSelectDialogFragment != null) {
            couponSelectDialogFragment.dismiss();
            return;
        }

        this.paymentWrapper = paymentWrapper;

        onFocusPayment(null);
        updateDiscount(paymentWrapper.getTotalDiscount());
        updatePayment(paymentWrapper.getAccountPayment());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        onPaymentClickListener = null;
        couponSelectDialogFragment = null;
        getView().addOnUnhandledKeyEventListener(null);
    }

    @Override
    public void onFocusPayment(PaymentType paymentType) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onFocusPayment, paymentType : " + paymentType);
        }

        ItemBridgeAdapter.ViewHolder viewHolder = getRowViewHolder(DISCOUNT_ROW);
        if (viewHolder != null) {
            DiscountPresenter.ViewHolder vh = (DiscountPresenter.ViewHolder) viewHolder.getViewHolder();
            float total = paymentWrapper.getAccountPayment();
            boolean isIncludeVat = paymentWrapper.isIncludeVat(paymentType);
            vh.updateTotalPayment(total, isIncludeVat);
        }
    }


    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_DPAD_UP:

                DiscountPresenter.ViewHolder discountVh = (DiscountPresenter.ViewHolder) getViewHolder(DISCOUNT_ROW);
                PaymentPresenter.ViewHolder paymentVh = (PaymentPresenter.ViewHolder) getViewHolder(PAYMENT_ROW);

                int selectedIndex = gridView.getSelectedPosition();
                if (selectedIndex == DISCOUNT_ROW && keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                    if (!paymentVh.isEnabled()) {
                        buttonGridView.requestFocus();
                        return true;
                    }
                } else if (selectedIndex == PAYMENT_ROW && keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                    buttonGridView.requestFocus();
                    return true;
                } else if (buttonGridView.hasFocus() && keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                    if (paymentVh.isEnabled()) {
                        paymentVh.view.requestFocus();
                    } else {
                        discountVh.view.requestFocus();
                    }
                    return true;
                }

                break;
        }

        return false;
    }
}
