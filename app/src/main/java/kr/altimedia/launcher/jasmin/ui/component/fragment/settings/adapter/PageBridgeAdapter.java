/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter;

import com.altimedia.util.Log;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;

public class PageBridgeAdapter extends SettingBaseButtonItemBridgeAdapter {
    private final String TAG = PageBridgeAdapter.class.getSimpleName();
    private int originalSize = 0;

    public PageBridgeAdapter(ObjectAdapter adapter, int visibleCount) {
        this(adapter, visibleCount, 1);
    }

    public PageBridgeAdapter(ObjectAdapter adapter, int rowCount, int columnCount) {
        super(adapter);

        initBinListForPaging(adapter, rowCount, columnCount);
    }

    private void initBinListForPaging(ObjectAdapter adapter, int rowCount, int columnCount) {
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) adapter;
        originalSize = objectAdapter.size();

        int pageItems = rowCount * columnCount;
        int mod = originalSize > pageItems ? originalSize % pageItems : 0;

        if (Log.INCLUDE) {
            Log.d(TAG, "initBinListForPaging"
                    + ", originalSize : " + originalSize
                    + ", rowCount : " + rowCount
                    + ", columnCount : " + columnCount
                    + ", mod : " + mod);
        }

        if (mod != 0) {
            int binSize = pageItems - mod;
            Log.d(TAG, "initBinListForPaging, binSize : " + binSize);

            for (int i = 0; i < binSize; i++) {
                objectAdapter.add(null);
            }
        }
    }

    public int getOriginalSize() {
        return originalSize;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
    }
}
