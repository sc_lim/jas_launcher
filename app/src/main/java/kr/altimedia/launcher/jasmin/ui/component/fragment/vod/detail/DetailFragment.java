/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.leanback.widget.ObjectAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.payment.type.ContentType;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.product.ProductButtonManager2;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.listenter.OnVodViewChangeListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionInfoDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionPurchaseDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionSelectDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionPriceInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment.SubscriptionPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.util.task.VodTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

import static kr.altimedia.launcher.jasmin.dm.contents.obj.Content.mPosterPurchaseRect;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PURCHASE_INFORM;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_MESSAGE_DESC;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_OFFER_ID;

public class DetailFragment extends Fragment implements View.OnUnhandledKeyEventListener {
    private static final String TAG = DetailFragment.class.getSimpleName();

    protected VodTaskManager vodTaskManager;
    protected OnVodViewChangeListener mChangeVodDetailListener = null;
    private BaseOnItemViewClickedListener mOnItemViewClickedListener;
    protected BrowseFrameLayout mRootView;
    protected VodRowFragment mVodRowFragment;
    protected ObjectAdapter mAdapter;
    private int resourceId = R.layout.fragment_vod_details;

    private final Handler mHandler = new Handler();
    //    private Timer mBackgroundTimer;
//    protected BackgroundManager mBackgroundManager;
//    protected Drawable mDefaultBackground;
//    private DisplayMetrics mMetrics;
    private static final int BACKGROUND_UPDATE_DELAY = 1000;
//    protected String mBackgroundUri;

    protected LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver mPushMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive, Action : " + intent.getAction());
            }

            Bundle mBundle = intent.getExtras();
            if (mBundle != null) {
                boolean isPurchaseSuccess = mBundle.getString(KEY_MESSAGE_DESC).equalsIgnoreCase("Success");
                String offerId = mBundle.getString(KEY_OFFER_ID);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReceive, isPurchaseSuccess : " + isPurchaseSuccess + ", offerId : " + offerId);
                }

                if (isPurchaseSuccess) {
                    onPurchasePushMessage(offerId);
                }
            }
        }
    };

    protected final int mSenderId;

    public DetailFragment() {
        mSenderId = UUID.randomUUID().hashCode();
    }

    protected void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    protected int getResourceId() {
        return resourceId;
    }

    protected Rect getPadding() {
        return new Rect(0, 0, 0, 0);
    }

    private TvOverlayManager mTvOverlayManager = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        vodTaskManager = new VodTaskManager(context);
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = (BrowseFrameLayout) inflater.inflate(
                getResourceId(), container, false);
        mRootView.addOnUnhandledKeyEventListener(this);
        mVodRowFragment = (VodRowFragment) getChildFragmentManager().findFragmentById(
                R.id.details_rows_dock);
        if (mVodRowFragment == null) {
            mVodRowFragment = VodRowFragment.newInstance(getPadding());
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.details_rows_dock, mVodRowFragment).commit();
        }

        mVodRowFragment.setAdapter(mAdapter);
        mVodRowFragment.setOnItemViewClickedListener(mOnItemViewClickedListener);

        setupFocusSearchListener();
        return mRootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        prepareBackgroundManager(view);
        registerPurchaseReceiver();
    }

    protected VerticalGridView getVerticalGridView() {
        return mVodRowFragment == null ? null : mVodRowFragment.getVerticalGridView();
    }

    protected VodTaskManager getVodTaskManager() {
        return vodTaskManager;
    }

    protected void setupFocusSearchListener() {
        mRootView.setOnFocusSearchListener(new BrowseFrameLayout.OnFocusSearchListener() {
            @Override
            public View onFocusSearch(View focused, int direction) {
                return focused;
            }
        });
    }

    public void setAdapter(ObjectAdapter adapter) {
        mAdapter = adapter;
        if (mVodRowFragment != null) {
            mVodRowFragment.setAdapter(adapter);
        }
    }

    protected void setVerticalSpacing(int spacing) {
        if (mVodRowFragment != null) {
            mVodRowFragment.getVerticalGridView().setVerticalSpacing(spacing);
        }
    }

    public void setChangeVodDetailListener(OnVodViewChangeListener mChangeVodDetailListener) {
        this.mChangeVodDetailListener = mChangeVodDetailListener;
    }

    public void setOnItemViewClickedListener(BaseOnItemViewClickedListener listener) {
        if (mOnItemViewClickedListener != listener) {
            mOnItemViewClickedListener = listener;

            if (mVodRowFragment != null) {
                mVodRowFragment.setOnItemViewClickedListener(listener);
            }
        }
    }

    public void setOnScrollOffsetCallback(VerticalGridView.OnScrollOffsetCallback mOnScrollOffsetCallback) {
        mVodRowFragment.setOnScrollOffsetCallback(mOnScrollOffsetCallback);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        vodTaskManager.cancelAllTask();

        detachedDpadNavigation();
        detachedRowViewListener();
        mRootView.removeOnUnhandledKeyEventListener(this);
        mRootView = null;
        mVodRowFragment.setAdapter(null);
        mVodRowFragment = null;
        unRegisterPurchaseReceiver();
//        if (mBackgroundTimer != null) {
//            mBackgroundTimer.cancel();
//        }
    }

    protected void detachedDpadNavigation() {
        mRootView.setOnFocusSearchListener(null);
    }

    protected void detachedRowViewListener() {
        mChangeVodDetailListener = null;
        mVodRowFragment.setOnItemViewClickedListener(null);
    }

    private void prepareBackgroundManager(View view) {
//        mBackgroundManager = BackgroundManager.getInstance(getContext(), getChildFragmentManager());
//        mBackgroundManager.attach(view);
//        mDefaultBackground = new ColorDrawable(getResources().getColor(R.color.color_home_background_default, null));
//        mBackgroundManager.setDrawable(mDefaultBackground);
//        mMetrics = new DisplayMetrics();
//        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    public void showLoadFailDialog(boolean dismiss, int key) {
        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getContext());
        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);

        if (dismiss) {
            VodDetailsDialogFragment vodDetailsDialogFragment = (VodDetailsDialogFragment) getParentFragment();
            failNoticeDialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (vodDetailsDialogFragment != null) {
                        vodDetailsDialogFragment.dismiss();
                    }
                }
            });
        }
    }

    public void showLoadErrorDialog(boolean dismiss, String errorCode, String message) {
        ErrorDialogFragment dialogFragment =
                ErrorDialogFragment.newInstance(TAG, errorCode, message);
        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);

        if (dismiss) {
            VodDetailsDialogFragment vodDetailsDialogFragment = (VodDetailsDialogFragment) getParentFragment();
            dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (vodDetailsDialogFragment != null) {
                        vodDetailsDialogFragment.dismiss();
                    }
                }
            });
        }
    }

    private void registerPurchaseReceiver() {
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        mLocalBroadcastManager.registerReceiver(mPushMessageReceiver, new IntentFilter(ACTION_UPDATED_PURCHASE_INFORM));
    }

    private void unRegisterPurchaseReceiver() {
        mLocalBroadcastManager.unregisterReceiver(mPushMessageReceiver);
    }

    protected void updateDetail(Content content) {

    }

    protected void setContentLike(Content content) {
    }

    protected void onPurchasePushMessage(String offerId) {
    }

    protected boolean isContainsOfferId(String offerId, ArrayList<Product> products) {
        for (Product product : products) {
            if (product.getOfferID().equals(offerId)) {
                return true;
            }
        }

        return false;
    }

    protected void purchaseSubscription(ProductButtonManager2 productButtonManager, String termsAndCondition) {
        ArrayList<Product> mainSubscriptionList = productButtonManager.getSubscribeMainProductList();
        int size = mainSubscriptionList.size();

        if (Log.INCLUDE) {
            Log.d(TAG, "purchaseSubscription, size : " + size);
        }

        if (size > 1) {
            showSelectSubscriptionDialog(productButtonManager, termsAndCondition, mainSubscriptionList);
        } else if (size == 1) {
            Product product = mainSubscriptionList.get(0);
            SubscriptionInfoDialogFragment subscriptionInfoDialogFragment = SubscriptionInfoDialogFragment.newInstance(product);
            subscriptionInfoDialogFragment.setOnSelectSubscriptionListener(new SubscriptionInfoDialogFragment.OnSelectSubscriptionListener() {
                @Override
                public void onSelect(Product product) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onSelect, product : " + product);
                    }

                    subscriptionInfoDialogFragment.dismiss();
                    if (product == null) {
                        return;
                    }

                    ArrayList<Product> priceList = productButtonManager.getSubscribeProductList(product);
                    SubscriptionInfo subscriptionInfo = getSubscriptionInfo(termsAndCondition, priceList);
                    showPurchaseSubscriptionDialog(subscriptionInfo);
                }
            });

            mChangeVodDetailListener.getTvOverlayManager().showDialogFragment(subscriptionInfoDialogFragment);
        }
    }

    private SubscriptionInfo getSubscriptionInfo(String termsAndCondition, ArrayList<Product> productList) {
        ArrayList<SubscriptionPriceInfo> priceList = new ArrayList<>();

        String title = "";
        String description = "";
        String posterSourceUrl = "";
        for (Product product : productList) {
            title = product.getProductName();
            description = product.getDescription();
            posterSourceUrl = product.getPosterSourceUrl(mPosterPurchaseRect.width(), mPosterPurchaseRect.height());
            SubscriptionPriceInfo priceInfo = new SubscriptionPriceInfo(
                    product.getOfferID(), StringUtil.getFormattedPrice(product.getPrice()), product.getCurrency(), product.getDuration().getValue());
            priceList.add(priceInfo);
        }

        SubscriptionInfo subscriptionInfo = new SubscriptionInfo(ContentType.VOD, title, description, termsAndCondition, posterSourceUrl, priceList);
        if (Log.INCLUDE) {
            Log.d(TAG, "getSubscriptionInfo, subscriptionInfo : " + subscriptionInfo);
        }

        return subscriptionInfo;
    }

    private void showSelectSubscriptionDialog(ProductButtonManager2 productButtonManager, String termsAndCondition, ArrayList<Product> subscriptionList) {
        SubscriptionSelectDialogFragment subscriptionSelectDialogFragment
                = SubscriptionSelectDialogFragment.newInstance(subscriptionList);
        subscriptionSelectDialogFragment.setOnSelectSubscriptionListener(new SubscriptionInfoDialogFragment.OnSelectSubscriptionListener() {
            @Override
            public void onSelect(Product product) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "purchaseSubscription, onClick product : " + product);
                }

                if (product == null) {
                    return;
                }

                ArrayList<Product> priceList = productButtonManager.getSubscribeProductList(product);
                SubscriptionInfo subscriptionInfo = getSubscriptionInfo(termsAndCondition, priceList);
                showPurchaseSubscriptionDialog(subscriptionInfo);
            }
        });
        mChangeVodDetailListener.getTvOverlayManager().showDialogFragment(subscriptionSelectDialogFragment);
    }

    private void showPurchaseSubscriptionDialog(SubscriptionInfo getSubscriptionInfo) {
        SubscriptionPurchaseDialogFragment subscriptionPurchaseDialogFragment = SubscriptionPurchaseDialogFragment.newInstance(mSenderId, getSubscriptionInfo);
        subscriptionPurchaseDialogFragment.setOnCompleteSubscribePaymentListener(new SubscriptionPaymentDialogFragment.OnCompleteSubscribePaymentListener() {
            @Override
            public void onCompleteSubscribePayment(boolean isSuccess) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onCompleteSubscribePurchase, isSuccess : " + isSuccess);
                }
            }

            @Override
            public void onDismissPurchaseDialog() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onDismissPaymentDialog");
                }

                subscriptionPurchaseDialogFragment.dismiss();
            }
        });
        mChangeVodDetailListener.getTvOverlayManager().showDialogFragment(subscriptionPurchaseDialogFragment);
    }

    protected Fragment findFragmentByTag(String className) {
        return getActivity().getSupportFragmentManager().findFragmentByTag(className);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "call onStart");
//        mBackgroundManager.setDrawable(mDefaultBackground);
//        mBackgroundManager.onActivityStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "call onStop");
//        mBackgroundManager.onStop();
    }

//    private void updateBackground(String uri) {
//        if (getContext() == null) {
//            return;
//        }
//        int width = mMetrics.widthPixels;
//        int height = mMetrics.heightPixels;
//        Glide.with(getContext())
//                .load(uri)
//                .centerCrop()
//                .error(mDefaultBackground)
//                .into(new CustomTarget<Drawable>() {
//                    @Override
//                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                        Log.d(TAG, "onResourceReady");
//
//                        Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
//                        Palette palette = Palette.from(bitmap).generate();
//                        Palette.Swatch vibrant = palette.getVibrantSwatch();
//                        if (vibrant == null) {
//                            return;
//                        }
//                        float[] hsls = vibrant.getHsl();
//                        for (int i = 0; i < hsls.length; i++) {
//                            Log.d(TAG, "hsl | index : " + i + ", value : " + hsls[i]);
//                        }
//
//                        int backgroundColor = vibrant.getRgb();
//                        int hslColor = Color.HSVToColor(hsls);
//                        Log.d(TAG, "onResourceReady | backgroundColor : " + backgroundColor);
//                        Log.d(TAG, "onResourceReady | hslColor : " + hslColor);
////                        mBackgroundManager.setColor(backgroundColor);
//                    }
//
//                    @Override
//                    public void onLoadCleared(@Nullable Drawable placeholder) {
//                        Log.d(TAG, "onLoadCleared");
//                    }
//                });
//        mBackgroundTimer.cancel();
//    }


//    protected void startBackgroundTimer() {
//        if (null != mBackgroundTimer) {
//            mBackgroundTimer.cancel();
//        }
//        mBackgroundTimer = new Timer();
//        mBackgroundTimer.schedule(new UpdateBackgroundTask(), BACKGROUND_UPDATE_DELAY);
//    }

//    private class UpdateBackgroundTask extends TimerTask {
//
//        @Override
//        public void run() {
//            mHandler.post(new Runnable() {
//                @Override
//                public void run() {
//                    updateBackground(mBackgroundUri);
//                }
//            });
//        }
//    }
}

