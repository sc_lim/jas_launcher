/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.SeasonListInfo;

/**
 * Created by mc.kim on 08,05,2020
 */
public class SeasonListResponse extends BaseResponse {

    @Expose
    @SerializedName("data")
    private SeasonListResult seasonListResult;

    protected SeasonListResponse(Parcel in) {
        super(in);
        seasonListResult = in.readTypedObject(SeasonListResult.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(seasonListResult, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SeasonListResponse> CREATOR = new Creator<SeasonListResponse>() {
        @Override
        public SeasonListResponse createFromParcel(Parcel in) {
            return new SeasonListResponse(in);
        }

        @Override
        public SeasonListResponse[] newArray(int size) {
            return new SeasonListResponse[size];
        }
    };

    public SeasonListInfo getSeasonListResult() {
        return new SeasonListInfo(seasonListResult.getSeasonList(), seasonListResult.masterSeriesTitle, seasonListResult.seasonId);
    }

    @Override
    public String toString() {
        return "SeasonListResponse{" +
                "seasonListResult=" + seasonListResult +
                '}';
    }

    private static class SeasonListResult implements Parcelable {
        @Expose
        @SerializedName("sersList")
        private List<SeasonInfo> seasonList;
        @Expose
        @SerializedName("masterSeriesTitle")
        private String masterSeriesTitle;
        @Expose
        @SerializedName("seasonId")
        private String seasonId;


        protected SeasonListResult(Parcel in) {
            seasonList = in.createTypedArrayList(SeasonInfo.CREATOR);
            masterSeriesTitle = (String) in.readValue(String.class.getClassLoader());
            seasonId = (String) in.readValue(String.class.getClassLoader());
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(seasonList);
            dest.writeValue(masterSeriesTitle);
            dest.writeValue(seasonId);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<SeasonListResult> CREATOR = new Creator<SeasonListResult>() {
            @Override
            public SeasonListResult createFromParcel(Parcel in) {
                return new SeasonListResult(in);
            }

            @Override
            public SeasonListResult[] newArray(int size) {
                return new SeasonListResult[size];
            }
        };

        public List<SeasonInfo> getSeasonList() {
            return seasonList;
        }

        @Override
        public String toString() {
            return "SeasonListResult{" +
                    "seasonList=" + seasonList +
                    ", masterSeriesTitle=" + masterSeriesTitle +
                    ", seasonId=" + seasonId +
                    '}';
        }

    }
}
