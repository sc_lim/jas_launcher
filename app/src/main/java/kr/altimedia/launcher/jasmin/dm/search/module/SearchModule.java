/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import kr.altimedia.launcher.jasmin.dm.search.api.SearchApi;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchModule {

    public Retrofit provideSearchModule(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(ServerConfig.SEARCH_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    private Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
//        builder.setLenient();
        builder.setDateFormat("yyyy-MM-dd");
        return builder.create();
    }

    @Singleton
    public SearchApi provideSearchApi(@Named("SearchApi") Retrofit retrofit) {
        return retrofit.create(SearchApi.class);
    }
}
