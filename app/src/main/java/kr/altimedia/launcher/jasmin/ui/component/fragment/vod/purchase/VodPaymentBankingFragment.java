package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Bank;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.BankingListPagerFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentBaseFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.HandleViewPager;
import kr.altimedia.launcher.jasmin.ui.view.indicator.ImageIndicator;

public class VodPaymentBankingFragment extends VodPaymentBaseFragment implements ViewPager.OnPageChangeListener {
    public static final String CLASS_NAME = VodPaymentBankingFragment.class.getName();
    private final String TAG = VodPaymentBankingFragment.class.getSimpleName();

    private static final String KEY_BANK_LIST = "BANK_LIST";

    public static final int NUMBER_COLUMNS = 3;
    public static final int NUMBER_ROWS = 3;
    public static final int PAGE_ITEMS = NUMBER_COLUMNS * NUMBER_ROWS;

    private HandleViewPager viewPager;
    private PagerAdapter mPagerAdapter;
    private ImageIndicator leftIndicator;
    private ImageIndicator rightIndicator;

    private int totalPage = 0;
    private ArrayList<Bank> bankList;

    private OnClickBankListener mOnClickBankListener;

    public static VodPaymentBankingFragment newInstance(ArrayList<Bank> optionList) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_BANK_LIST, optionList);

        VodPaymentBankingFragment fragment = new VodPaymentBankingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnClickBankListener(OnClickBankListener mOnClickBankListener) {
        this.mOnClickBankListener = mOnClickBankListener;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_vod_payment_banking;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        bankList = getArguments().getParcelableArrayList(KEY_BANK_LIST);
        float mod = (float) (bankList.size() % PAGE_ITEMS);
        totalPage = mod > 0 ? bankList.size() / PAGE_ITEMS + 1 : bankList.size() / PAGE_ITEMS;

        BankingListPagerFragment.setSelectedPosition(0);
        initButton(view);
        initViewPage(view);
    }

    private void initButton(View view) {
        TextView cancelButton = view.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SafeDismissDialogFragment) getParentFragment()).dismiss();
            }
        });
    }

    private void initViewPage(View view) {
        viewPager = view.findViewById(R.id.viewPager);
        leftIndicator = view.findViewById(R.id.leftIndicator);
        rightIndicator = view.findViewById(R.id.rightIndicator);

        mPagerAdapter = new PagerAdapter(
                getFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, totalPage, bankList);
        viewPager.setAdapter(mPagerAdapter);
        viewPager.addOnPageChangeListener(this);
        viewPager.setOnKeyEventListener(new HandleViewPager.OnKeyEventListener() {
            @Override
            public boolean onKeyEvent(KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                BankingListPagerFragment currentFragment = (BankingListPagerFragment) getFragment(viewPager.getId(), mPagerAdapter.getItemId(viewPager.getCurrentItem()));
                int index = currentFragment.getFocusedPosition();
                int keyCode = event.getKeyCode();

                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        if (mOnClickBankListener != null) {
                            Bank bank = currentFragment.getItem(index);
                            mOnClickBankListener.onClickBank(bank);
                            return true;
                        }
                        return false;
                    case KeyEvent.KEYCODE_DPAD_LEFT:
                        if (index < NUMBER_COLUMNS) {
                            updateNextPageFocus(false, index);
                        }
                        return false;
                    case KeyEvent.KEYCODE_DPAD_RIGHT:
                        if (viewPager.getCurrentItem() == (totalPage - 1)) {
                            int lastIndex = currentFragment.getSize() - 1;
                            if (!currentFragment.isLastColumn(index) && index + NUMBER_ROWS > lastIndex) {
                                currentFragment.requestPosition(lastIndex);
                                return true;
                            }

                            return false;
                        }

                        if (index >= (NUMBER_COLUMNS * 2)) {
                            updateNextPageFocus(true, index);
                        }
                        return false;
                }

                return false;
            }
        });
    }

    private void updateNextPageFocus(boolean moveRight, int position) {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateNextPageFocus, position : " + position);
        }

        if (position < 0 || position >= PAGE_ITEMS) {
            return;
        }

        int currentPage = viewPager.getCurrentItem();
        BankingListPagerFragment nextFragment = (BankingListPagerFragment) getFragment(viewPager.getId(), mPagerAdapter.getItemId(moveRight ? currentPage + 1 : currentPage - 1));
        int moveFocusIndex = moveRight ? position - (NUMBER_COLUMNS * 2) : position + (NUMBER_COLUMNS * 2);

        //for focus sync
        BankingListPagerFragment.setSelectedPosition(moveFocusIndex);
        if (nextFragment != null) {
            nextFragment.requestPosition(moveFocusIndex);
        }
    }

    private Fragment getFragment(int viewId, long id) {
        String name = "android:switcher:" + viewId + ":" + id;
        return getFragmentManager().findFragmentByTag(name);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        leftIndicator.setArrowVisible(position == 0 ? View.INVISIBLE : View.VISIBLE);
        rightIndicator.setArrowVisible(position == (totalPage - 1) ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mOnClickBankListener = null;
        viewPager.removeOnPageChangeListener(this);
    }

    private static class PagerAdapter extends FragmentPagerAdapter {
        private int totalPage;
        private ArrayList<Bank> list;

        public PagerAdapter(@NonNull FragmentManager fm, int behavior, int totalPage, ArrayList<Bank> list) {
            super(fm, behavior);
            this.totalPage = totalPage;
            this.list = list;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            int start = position * 9;
            int end = (position + 1) * 9;

            ArrayList<Bank> pageList = new ArrayList<>();
            for (int i = start; i < end && i < list.size(); i++) {
                pageList.add(list.get(i));
            }

            return BankingListPagerFragment.newInstance(pageList);
        }

        @Override
        public int getCount() {
            return totalPage;
        }
    }

    public interface OnClickBankListener {
        void onClickBank(Bank bank);
    }
}
