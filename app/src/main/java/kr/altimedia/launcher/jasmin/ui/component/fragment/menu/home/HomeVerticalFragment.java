/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.menu.home;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.player.AltiMediaSource;
import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.category.MenuManager;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.dm.contents.ContentDataManager;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PlacementDecision;
import kr.altimedia.launcher.jasmin.dm.def.CategoryType;
import kr.altimedia.launcher.jasmin.dm.recommend.RecommendDataManager;
import kr.altimedia.launcher.jasmin.dm.recommend.remote.RecommendRemote;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter.ProgressVodItemPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter.PromotionPagerPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter.RecommendPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.presenter.VodItemPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.detail.VodDetailsDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.adapter.RepeatArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewKeyListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.LimitedIndexListRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.OnPresenterDispatchKeyListener;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RecommendRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.ViewPagerPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.CategoryListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.PagerRow;
import kr.altimedia.launcher.jasmin.ui.view.row.RecommendRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.verticalRow.VideoSupportVerticalRowFragment;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;


public class HomeVerticalFragment extends VideoSupportVerticalRowFragment {

    public static final String CLASS_NAME = HomeVerticalFragment.class.getName();
    private final String TAG = HomeVerticalFragment.class.getSimpleName();
    private OnMenuInteractor mOnMenuInteractor = null;
    private final ClassPresenterSelector mPresenterSelector;
    private final ArrayObjectAdapter mRowsAdapter;
    private final List<Banner> mPromotionBannerList = new ArrayList<>();
    private final List<WeakReference<AsyncTask>> taskList = new ArrayList<>();
    private final int CONTENTS_ALIGN_INDEX = 0;
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_NEED_ATTACHED = "attached";

    private final Handler mHandler = new Handler();
    private final AccountManager mAccountManager;
    private final ArrayList<Category> userBasedCategory = new ArrayList<>();

    private final int CONTENTS_VISIBLE_SIZE = 6;
    private final int PROMOTION_VISIBLE_SIZE = 2;
    public static final int LOAD_SIZE = 100;

    public HomeVerticalFragment() {
        // Required empty public constructor
        mPresenterSelector = new ClassPresenterSelector();
        mRowsAdapter = new ArrayObjectAdapter(mPresenterSelector);
        mAccountManager = AccountManager.getInstance();
    }

    private final TvOverlayManager.OnOverlayDialogQueueListener queueListener = new TvOverlayManager.OnOverlayDialogQueueListener() {
        @Override
        public void onSizeChanged(int size) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onSizeChanged : " + size);
            }
            if (size == 0) {
                resumePlayer(false);
            } else {
                stopPlayer();
                onDetachedPromotion();
            }
        }
    };

    public static HomeVerticalFragment newInstance(Category category) {
        HomeVerticalFragment fragment = new HomeVerticalFragment();
        fragment.setLayoutId(R.layout.fragment_home);
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_CATEGORY, category);
        fragment.setArguments(bundle);
        return fragment;
    }

    private TvOverlayManager mTvOverlayManager;

    @Override
    protected void onSurfaceDestroyed() {
        super.onSurfaceDestroyed();
        onDetachedPromotion();
    }

    @Override
    protected void onSurfaceCreated() {
        super.onSurfaceCreated();
        resumePlayer(false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }

        if (getParentFragment() instanceof OnMenuInteractor) {
            this.mOnMenuInteractor = (OnMenuInteractor) getParentFragment();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (Log.INCLUDE) {
            Log.d(TAG, "onSaveInstanceState");
        }
        outState.putBoolean(KEY_NEED_ATTACHED, true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private View mParentsView = null;


    private void showVodDetail(Content content) {
        if (mTvOverlayManager == null) {
            return;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "showVodDetail | content : " + content.toString());
        }
        if (content.isSeries()) {
            showSeriesDetail(content.getSeriesAssetId(), content.getCategoryId(), content.getContentGroupId());
        } else {
            mTvOverlayManager.showDialogFragment
                    (VodDetailsDialogFragment.newInstance(content, 1));
        }

    }

    private void showSeriesDetail(String seriesAssetId, String categoryId, String contentId) {
        if (mTvOverlayManager == null) {
            return;
        }

        mTvOverlayManager.showDialogFragment
                (VodDetailsDialogFragment.newInstance(seriesAssetId, categoryId, contentId, 1));
    }

    private void recoveryFromSaveInstance(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return;
        }
        boolean calledAttached = savedInstanceState.getBoolean(KEY_NEED_ATTACHED, false);
        if (calledAttached) {
            savedInstanceState.putBoolean(KEY_NEED_ATTACHED, false);
            if (getActivity() instanceof LauncherActivity) {
                mTvOverlayManager = ((LauncherActivity) getActivity()).getTvOverlayManager();
            } else if (getActivity() instanceof LiveTvActivity) {
                mTvOverlayManager = ((LiveTvActivity) getActivity()).getTvOverlayManager();
            }

            if (getParentFragment() instanceof OnMenuInteractor) {
                this.mOnMenuInteractor = (OnMenuInteractor) getParentFragment();
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated");
        }

        recoveryFromSaveInstance(savedInstanceState);
        mParentsView = view;
        mParentsView.addOnUnhandledKeyEventListener(new View.OnUnhandledKeyEventListener() {
            @Override
            public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                        mOnMenuInteractor.openSideMenu(true);
                        return true;
                    }
                }
                return false;
            }
        });
        VerticalGridView verticalGridView = view.findViewById(R.id.gridView);
        RecyclerView.RecycledViewPool mRecycledViewPool = new RecyclerView.RecycledViewPool() {
            @Override
            public void clear() {
                super.clear();
                if (Log.INCLUDE) {
                    Log.d(TAG, "RecycledViewPool | clear");
                }
            }

            @Override
            public void setMaxRecycledViews(int viewType, int max) {
                super.setMaxRecycledViews(viewType, max);
                if (Log.INCLUDE) {
                    Log.d(TAG, "RecycledViewPool | setMaxRecycledViews : " + viewType + ", max : " + max);
                }
            }

            @Override
            public int getRecycledViewCount(int viewType) {
                int viewCount = super.getRecycledViewCount(viewType);
                if (Log.INCLUDE) {
                    Log.d(TAG, "RecycledViewPool | getRecycledViewCount : " + viewCount);
                }
                return viewCount;
            }

            @Nullable
            @Override
            public RecyclerView.ViewHolder getRecycledView(int viewType) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "RecycledViewPool | getRecycledView : " + viewType);
                }
                return super.getRecycledView(viewType);
            }

            @Override
            public void putRecycledView(RecyclerView.ViewHolder scrap) {
                super.putRecycledView(scrap);
                if (Log.INCLUDE) {
                    Log.d(TAG, "RecycledViewPool | putRecycledView");
                }
            }
        };
        verticalGridView.setRecycledViewPool(mRecycledViewPool);
        verticalGridView.setVerticalSpacing(getContext().getResources().getDimensionPixelSize(R.dimen.home_vertical_space));
        dispatchScrollEvent(verticalGridView, R.id.viewPager);
        currentCategory = getArguments().getParcelable(KEY_CATEGORY);
        initCategory();
        ViewGroup loadingView = (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        getProgressBarManager().setProgressBarView(loadingView);

        setOnItemViewClickedListener(new BaseOnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder viewHolder, Object item, Presenter.ViewHolder rowViewHolder, Object row) {
                if (item == null) {
                    return;
                }

                if (Log.INCLUDE) {
                    Log.d(TAG, "viewHolder : " +
                            viewHolder + ", item : " +
                            item + ", rowViewHolder : " +
                            rowViewHolder + ", row : " +
                            row);
                }

                if (item instanceof Content) {
                    Content content = (Content) item;
                    showVodDetail(content);
                } else if (item instanceof Banner) {
                    onSelectedBannerAction(item);
                }
            }
        });
        setOnItemViewSelectedListener(new OnItemViewSelectedListener() {
            @Override
            public void onItemSelected(Presenter.ViewHolder viewHolder, Object item, Presenter.ViewHolder viewHolder2, Row row) {
                int selectedPosition = getVerticalGridView().getSelectedPosition();
                if (Log.INCLUDE) {
                    Log.d(TAG, "on Item Selected : " + selectedPosition);
                }
                if (!promotionContain) {
                    return;
                }
                if (item instanceof Content && selectedPosition == 1) {
                    Banner banner = Banner.buildBanner(((Content) item));
                    if (banner == null) {
                        return;
                    }
                    List<Banner> bannerList = new ArrayList<>(1);
                    bannerList.add(banner);
                    setPromotionLayer(0, bannerList, true, false);
                } else if (selectedPosition == 0) {
                    if (mPromotionBannerList.isEmpty()) {
                        return;
                    }
                    setPromotionLayer(0, mPromotionBannerList, true, true);
                }
            }
        });
        setOnItemViewKeyListener(new BaseOnItemViewKeyListener() {
            @Override
            public boolean onItemKey(int keyCode, Presenter.ViewHolder viewHolder, Object item, Presenter.ViewHolder rowViewHolder, Object row) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onItemKey, viewHolder : " +
                            viewHolder + ", item : " +
                            item + ", rowViewHolder : " +
                            rowViewHolder + ", row : " +
                            row);
                }

                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_RIGHT:
                        HorizontalGridView mHorizontalGridView = null;
                        if (rowViewHolder instanceof LimitedIndexListRowPresenter.ViewHolder) {
                            mHorizontalGridView = ((LimitedIndexListRowPresenter.ViewHolder) rowViewHolder).getGridView();
                        }

                        if (rowViewHolder instanceof RecommendRowPresenter.ViewHolder) {
                            mHorizontalGridView = ((RecommendRowPresenter.ViewHolder) rowViewHolder).getGridView();
                        }

                        if (mHorizontalGridView != null) {
                            int size = mHorizontalGridView.getAdapter().getItemCount();
                            int selectedPosition = mHorizontalGridView.getSelectedPosition();
                            if (selectedPosition == (size - 1)) {
                                mHorizontalGridView.scrollToPosition(0);
                                return true;
                            }
                        }

                        break;
                }

                return false;
            }
        });
    }


    private void tuneBannerAsset(Banner banner) {
        if (Log.INCLUDE) {
            Log.d(TAG, "tuneBannerAsset");
        }
        boolean autoPlay = mTvOverlayManager.getDialogQueueSize() == 0;
        int bannerType = banner.getBannerType();
        switch (bannerType) {
            case Banner.IMAGE_TYPE:
                requestTuneVod(autoPlay, banner.getBannerImageAsset().getPosterSourceUrl(Banner.PROMOTION_BANNER.width(), Banner.PROMOTION_BANNER.height()));
                break;

            case Banner.VIDEO_TYPE:

                PlaybackUtil.getPlayResource(getContext(), banner, new PlaybackUtil.PlayResourceCallback() {
                    @Override
                    public void onResult(AltiMediaSource[] mediaSource, List<PlacementDecision> placementDecisions) {
                        String loadingPoster = banner.getBannerClipAsset().getPosterSourceUrl(Banner.PROMOTION_BANNER.width(), Banner.PROMOTION_BANNER.height());
                        if (Log.INCLUDE) {
                            Log.d(TAG, "call onResult : " + loadingPoster + ", mediaSource : " + mediaSource.toString());
                        }
                        requestTuneVod(autoPlay, loadingPoster, mediaSource);
                    }

                    @Override
                    public void onError(String errorCode, String errorMessage) {

                    }

                });

                break;
        }
    }

    private boolean isSubCategoryLoading = false;

    @Override
    protected void notifyLoadMore() {
        super.notifyLoadMore();
        if (Log.INCLUDE) {
            Log.d(TAG, "call notifyLoadMore ");
        }
        if (isSubCategoryLoading) {
            if (Log.INCLUDE) {
                Log.d(TAG, "notifyLoadMore isLoading so return ");
            }
            return;
        }
        if (mRowsAdapter.size() < LOAD_SIZE) {
            if (Log.INCLUDE) {
                Log.d(TAG, "notifyLoadMore isLoading mRowsAdapter.size() < LOAD_SIZE " + mRowsAdapter.size());
            }
            return;
        }
        Category category = getArguments().getParcelable(KEY_CATEGORY);
        boolean canLoadMore = MenuManager.getInstance().canLoadMore(category.getCategoryID(), mRowsAdapter.size());
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyLoadMore canLoadMore : " + canLoadMore);
        }
        if (!canLoadMore) {
            return;
        }
        loadCategoryRow(category, mRowsAdapter.size());
    }

    private boolean hasPromotion(List<Category> result) {
        for (Category category : result) {
            if (category.getLinkType() == CategoryType.Promotion) {
                return true;
            }
        }
        return false;
    }

    private boolean promotionContain = false;

    private void loadCategoryRow(Category category, int startIndex) {
        clearData();
        isSubCategoryLoading = true;
        MenuManager.getInstance().loadSubCategory(getContext(), mAccountManager.getLocalLanguage(), mAccountManager.getSaId(), category.getCategoryID(),
                category.getCategoryVersion(), startIndex, LOAD_SIZE, new MbsDataProvider<String, List<Category>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(category, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed : " + key);
                        }
                        isSubCategoryLoading = false;
                        mOnMenuInteractor.setSideMenuEnable(false);
                        if (!isResumed()) {
                            return;
                        }
                        showNetworkCheckDialog(key);
                    }

                    @Override
                    public void onSuccess(String id, List<Category> result) {
                        promotionContain = hasPromotion(result);
                        if (getVerticalGridView() != null) {
                            getVerticalGridView().setItemViewCacheSize(result.size());
                        }
                        if (mOnMenuInteractor != null) {
                            mOnMenuInteractor.setSideMenuEnable(true);
                        }
                        isSubCategoryLoading = false;
                        for (Category category : result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onSuccess : Category " + category.toString());
                            }
                            switch (category.getLinkType()) {
                                case Promotion:
                                    loadPromotion(category);
                                    break;
                                case Banner:
                                    loadBanner(category);
                                    break;
                                case ContentsList:
                                    loadContentsData(category, promotionContain);
                                    break;
                                case RecentWatch:
                                    loadRecentWatchData(category);
                                    break;
                                case Recommend:
                                    loadRecommend(category);
                                    break;
                            }
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError category : " + errorCode + ", message : " + message);
                        }
                        isSubCategoryLoading = false;
                        if (!isResumed()) {
                            return;
                        }
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return HomeVerticalFragment.this.getContext();
                    }
                });
    }

    private Category currentCategory;

    private void initCategory() {
        userBasedCategory.clear();
        getProgressBarManager().show();
        setAdapter(mRowsAdapter);
        initPromotionPage();
        initBannerRow();
        initVodRow();
        loadCategoryRow(getArguments().getParcelable(KEY_CATEGORY), 0);
    }


    public Category getCurrentCategory() {
        return currentCategory;
    }

    private void loadBanner(Category category) {

        ContentDataManager contentDataManager = new ContentDataManager();
        AsyncTask task = contentDataManager.getImageBannerList(getContext(), mAccountManager.getLocalLanguage(), mAccountManager.getSaId(),
                category.getLinkInfo(), category.getCategoryID(), category.getCategoryVersion(), new MbsDataProvider<String, List<Banner>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(category, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed banner : " + key);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        showNetworkCheckDialog(key);
                    }

                    @Override
                    public void onSuccess(String id, List<Banner> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "banner response : " + result.size());
                        }
                        if (result.size() == 0) {
                            return;
                        }
                        RowPresenter moviePresenter = new RecommendPresenter();
                        RepeatArrayObjectAdapter listRowAdapter = new RepeatArrayObjectAdapter(moviePresenter, PROMOTION_VISIBLE_SIZE);
                        listRowAdapter.addAll(0, result);
                        HeaderItem header = new HeaderItem(category.getLinkType().getCategoryTypeLong(), category.getCategoryName());
                        RecommendRow listRow = new RecommendRow(category.getOrderNumber(), header, listRowAdapter);
//                        mRowsAdapter.add(listRow);
                        int addedIndex = addListRow(category, listRow);

                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError banner : " + errorCode + ", message : " + message);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return HomeVerticalFragment.this.getContext();
                    }
                });
        taskList.add(new WeakReference<>(task));
    }

    private boolean mCurrentPromotionPlay = false;

    private void setPromotionLayer(int orderNumber, List<Banner> bannerList, boolean replace, boolean promotionMode) {

        if (promotionMode && mCurrentPromotionPlay) {
            return;
        }
        mCurrentPromotionPlay = promotionMode;
        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter();
        for (int j = 0; j < bannerList.size(); j++) {
            listRowAdapter.add(bannerList.get(j));
        }
        if (Log.INCLUDE) {
            Log.d("ViewPagerPresenter", "listRowAdapter : " + listRowAdapter.size());
        }
        PagerRow listRow = new PagerRow(orderNumber, listRowAdapter);

        getVerticalGridView().post(new Runnable() {
            @Override
            public void run() {
                if (replace) {
                    mRowsAdapter.replace(0, listRow);
                } else {
                    mRowsAdapter.add(0, listRow);
                }
                tuneBannerAsset(bannerList.get(0));
            }
        });
    }

    private void loadPromotion(Category category) {

        ContentDataManager contentDataManager = new ContentDataManager();
        AsyncTask task = contentDataManager.getPromotionList(getContext(), mAccountManager.getLocalLanguage(), mAccountManager.getSaId(),
                category.getLinkInfo(), category.getCategoryID(), category.getCategoryVersion(), new MbsDataProvider<String, List<Banner>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(category, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed banner : " + key);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        showNetworkCheckDialog(key);
                    }

                    @Override
                    public void onSuccess(String id, List<Banner> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "promotion response : " + result.size());
                        }
                        if (result.size() == 0) {
                            promotionContain = false;
                            return;
                        }
                        mPromotionBannerList.clear();
                        mPromotionBannerList.addAll(result);
                        setPromotionLayer(category.getOrderNumber(), result, false, true);
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError banner : " + errorCode + ", message : " + message);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return HomeVerticalFragment.this.getContext();
                    }
                });
        taskList.add(new WeakReference<>(task));
    }

    private final List<Category> loadList = new ArrayList<>();


    private void checkLoadList(Category menu, boolean add) {
        if (add) {
            if (loadList.contains(menu)) {
                return;
            }
            loadList.add(menu);
        } else {
            loadList.remove(menu);
        }
        if (loadList.isEmpty()) {
            getProgressBarManager().hide();
        }
    }

    private int findIndexByHeaderId(Category category) {
        int size = mRowsAdapter.size();
        for (int i = 0; i < size; i++) {
            Object item = mRowsAdapter.get(i);
            if (item instanceof Row) {
                Row row = (Row) item;
                if (row.getId() == category.getOrderNumber()) {
                    return i;
                }
            }
        }
        return -1;
    }

    private int renewalWatchData(Category category) {

        final boolean needReplace;
        int itemIndex = findIndexByHeaderId(category);
        needReplace = itemIndex != -1;
        int index = itemIndex;
        ContentDataManager contentDataManager = new ContentDataManager();
        AsyncTask task = contentDataManager.getRecentWatchingList(mAccountManager.getLocalLanguage(), mAccountManager.getSaId(),
                mAccountManager.getProfileId(), 100, category.getCategoryID(), new MbsDataProvider<String, List<Content>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(category, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed : " + key);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        showNetworkCheckDialog(key);
                    }

                    @Override
                    public void onSuccess(String id, List<Content> result) {

                        getVerticalGridView().post(new Runnable() {
                            @Override
                            public void run() {
                                if (result == null || result.isEmpty()) {
                                    if (needReplace) {
                                        mRowsAdapter.removeItems(index, 1);
                                    }
                                    return;
                                }
                                RowPresenter moviePresenter = new ProgressVodItemPresenter();
                                RepeatArrayObjectAdapter listRowAdapter = new RepeatArrayObjectAdapter(moviePresenter, CONTENTS_VISIBLE_SIZE);
                                listRowAdapter.addAll(0, result);
                                HeaderItem header = new HeaderItem(category.getLinkType().getCategoryTypeLong(), category.getCategoryName());
                                CategoryListRow listRow = new CategoryListRow(category, category.getOrderNumber(), header, listRowAdapter);
                                if (needReplace) {
                                    if (index >= mRowsAdapter.size()) {
                                        mRowsAdapter.add(listRow);
                                    } else {
                                        mRowsAdapter.replace(index, listRow);
                                    }
                                } else {
                                    int addedIndex = addListRow(category, listRow);
                                    int index = userBasedCategory.indexOf(category);
                                    if (index != -1) {
                                        userBasedCategory.get(index).setOrderNumber(addedIndex);
                                    }
                                }
                            }
                        });

                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (!isResumed()) {
                            return;
                        }
                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return HomeVerticalFragment.this.getContext();
                    }
                });
        taskList.add(new WeakReference<>(task));
        return index;
    }

    private void loadRecentWatchData(final Category category) {
        userBasedCategory.add(category);
        ContentDataManager contentDataManager = new ContentDataManager();
        AsyncTask task = contentDataManager.getRecentWatchingList(mAccountManager.getLocalLanguage(), mAccountManager.getSaId(),
                mAccountManager.getProfileId(), 100, category.getCategoryID(), new MbsDataProvider<String, List<Content>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(category, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed : " + key);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        showNetworkCheckDialog(key);
                    }

                    @Override
                    public void onSuccess(String id, List<Content> result) {
                        if (result.size() == 0) {
                            return;
                        }
                        RowPresenter moviePresenter = new ProgressVodItemPresenter();
                        RepeatArrayObjectAdapter listRowAdapter = new RepeatArrayObjectAdapter(moviePresenter, CONTENTS_VISIBLE_SIZE);
                        listRowAdapter.addAll(0, result);
                        HeaderItem header = new HeaderItem(category.getLinkType().getCategoryTypeLong(), category.getCategoryName());
                        CategoryListRow listRow = new CategoryListRow(category, category.getOrderNumber(), header, listRowAdapter);


//                        int index = userBasedCategory.indexOf(category);
//                        if (index != -1) {
//                            userBasedCategory.get(index).setOrderNumber(mRowsAdapter.size() + 1);
//                        }
//                        mRowsAdapter.add(listRow);

                        int addedIndex = addListRow(category, listRow);
                        int index = userBasedCategory.indexOf(category);
                        if (index != -1) {
                            userBasedCategory.get(index).setOrderNumber(addedIndex);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (!isResumed()) {
                            return;
                        }
                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return HomeVerticalFragment.this.getContext();
                    }
                });
        taskList.add(new WeakReference<>(task));
    }

    public void showNetworkCheckDialog(int reason) {
        FailNoticeDialogFragment mFailNoticeDialogFragment = new FailNoticeDialogFragment(reason, getContext());
        mFailNoticeDialogFragment.show(mTvOverlayManager, getChildFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
    }

    private void loadContentsData(final Category category, boolean containPromotion) {
        ContentDataManager contentDataManager = new ContentDataManager();
        AsyncTask task = contentDataManager.getContentsList(getContext(), mAccountManager.getLocalLanguage(), mAccountManager.getSaId(),
                mAccountManager.getProfileId(), category.getCategoryID(), category.getCategoryVersion(),
                0, 100, category.getContentBannerFlag(), new MbsDataProvider<String, List<Content>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(category, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, " onFailed : " + key);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        showNetworkCheckDialog(key);
                    }

                    @Override
                    public void onSuccess(String id, List<Content> result) {
                        if (result.size() == 0) {
                            return;
                        }
                        RowPresenter moviePresenter = new VodItemPresenter();
                        RepeatArrayObjectAdapter listRowAdapter = new RepeatArrayObjectAdapter(moviePresenter, CONTENTS_VISIBLE_SIZE);
                        listRowAdapter.addAll(0, result);
                        HeaderItem header = new HeaderItem(category.getLinkType().getCategoryTypeLong(), category.getCategoryName());
                        CategoryListRow listRow = new CategoryListRow(category, category.getOrderNumber(), header, listRowAdapter);
                        if (containPromotion && category.isSupportedBanner() && mRowsAdapter.size() >= 1) {
                            mRowsAdapter.add(1, listRow);
                        } else {
                            int addedIndex = addListRow(category, listRow);
//                            int index = userBasedCategory.indexOf(category);
//                            if (index != -1) {
//                                userBasedCategory.get(index).setOrderNumber(addedIndex);
//                            }
//                            mRowsAdapter.add(listRow);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (!isResumed()) {
                            return;
                        }
                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return HomeVerticalFragment.this.getContext();
                    }
                });
        taskList.add(new WeakReference<>(task));
    }

    @Override
    protected void onRowSelected(RecyclerView parent, RecyclerView.ViewHolder viewHolder, int position, int subposition) {
        super.onRowSelected(parent, viewHolder, position, subposition);

        if (Log.INCLUDE) {
            Log.d(TAG, "onRowSelected : " + position + ", subPosition : " + subposition);
        }

    }

    private int renewalRecommend(Category category) {
        final boolean needReplace;
        int itemIndex = findIndexByHeaderId(category);
        needReplace = itemIndex != -1;
        final int index = itemIndex;
        RecommendDataManager recommendDataManager = new RecommendDataManager();
        AsyncTask task = recommendDataManager.getCurationContents(mAccountManager.getLocalLanguage(),
                mAccountManager.getSaId(), mAccountManager.getProfileId(),
                category.getCategoryID(), RecommendRemote.MAX_CNT, mAccountManager.getProductCode(), mAccountManager.getCurrentRating(), new MbsDataProvider<String, List<Content>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(category, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed : " + key);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        showNetworkCheckDialog(key);
                    }

                    @Override
                    public void onSuccess(String id, List<Content> result) {

                        getVerticalGridView().post(new Runnable() {
                            @Override
                            public void run() {
                                if (result == null || result.isEmpty()) {
                                    if (needReplace) {
                                        mRowsAdapter.removeItems(index, 1);
                                    }
                                    return;
                                }
                                RowPresenter moviePresenter = new VodItemPresenter();
                                RepeatArrayObjectAdapter listRowAdapter = new RepeatArrayObjectAdapter(moviePresenter, CONTENTS_VISIBLE_SIZE);
                                listRowAdapter.addAll(0, result);
                                HeaderItem header = new HeaderItem(category.getLinkType().getCategoryTypeLong(), category.getCategoryName());
                                CategoryListRow listRow = new CategoryListRow(category, category.getOrderNumber(), header, listRowAdapter);
                                if (needReplace) {
                                    if (index >= mRowsAdapter.size()) {
                                        mRowsAdapter.add(listRow);
                                    } else {
                                        mRowsAdapter.replace(index, listRow);
                                    }
                                } else {
                                    int addedIndex = addListRow(category, listRow);
                                    int index = userBasedCategory.indexOf(category);
                                    if (index != -1) {
                                        userBasedCategory.get(index).setOrderNumber(addedIndex);
                                    }
                                }
                            }
                        });

                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError : " + errorCode + ", message : " + message);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return HomeVerticalFragment.this.getContext();
                    }
                });
        taskList.add(new WeakReference(task));
        return index;
    }

    private final Runnable mFocusRunnable = new Runnable() {
        @Override
        public void run() {
            if (mVerticalGridView != null) {
                mVerticalGridView.setSelectedPosition(0);
            }
        }
    };
    private final Handler mFocusHandler = new Handler(Looper.getMainLooper());

    private int addListRow(Category category, Row listRow) {
        int index = category.getOrderNumber();
        if (mRowsAdapter.size() == 0) {
            mRowsAdapter.add(mRowsAdapter.size(), listRow);
            getVerticalGridView().postDelayed(mFocusRunnable, 500);
            if (Log.INCLUDE) {
                Log.d(TAG, "call selected position 0 : " + category.getCategoryName());
            }
            return 0;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "addListRow | mRowsAdapter.size() < index : " + (mRowsAdapter.size() < index));
        }
        if (mRowsAdapter.size() < index) {
            if (Log.INCLUDE) {
                Log.d(TAG, "addListRow : " + mRowsAdapter.size());
            }
            mRowsAdapter.add(mRowsAdapter.size(), listRow);
            getVerticalGridView().postDelayed(mFocusRunnable, 500);
            if (Log.INCLUDE) {
                Log.d(TAG, "call selected position 0 : " + category.getCategoryName());
            }
            return mRowsAdapter.size();
        } else {
            if (Log.INCLUDE) {
                Log.d(TAG, "addListRow : " + index);
            }
            mRowsAdapter.add(index - 1, listRow);
            getVerticalGridView().postDelayed(mFocusRunnable, 500);
            if (Log.INCLUDE) {
                Log.d(TAG, "call selected position 0 : " + category.getCategoryName());
            }
            return index;
        }

    }

    private void loadRecommend(final Category category) {
        userBasedCategory.add(category);
        RecommendDataManager recommendDataManager = new RecommendDataManager();
        AsyncTask task = recommendDataManager.getCurationContents(mAccountManager.getLocalLanguage(),
                mAccountManager.getSaId(), mAccountManager.getProfileId(), category.getCategoryID(), RecommendRemote.MAX_CNT,
                mAccountManager.getProductCode(), mAccountManager.getCurrentRating(), new MbsDataProvider<String, List<Content>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(category, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed : " + key);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        showNetworkCheckDialog(key);
                    }

                    @Override
                    public void onSuccess(String id, List<Content> result) {
                        if (result.size() == 0) {
                            return;
                        }
                        RowPresenter moviePresenter = new VodItemPresenter();
                        RepeatArrayObjectAdapter listRowAdapter = new RepeatArrayObjectAdapter(moviePresenter, CONTENTS_VISIBLE_SIZE);
                        listRowAdapter.addAll(0, result);
                        HeaderItem header = new HeaderItem(category.getLinkType().getCategoryTypeLong(), category.getCategoryName());
                        CategoryListRow listRow = new CategoryListRow(category, category.getOrderNumber(), header, listRowAdapter);

//                        int index = userBasedCategory.indexOf(category);
//                        if (index != -1) {
//                            userBasedCategory.get(index).setOrderNumber(mRowsAdapter.size() + 1);
//                        }
//                        mRowsAdapter.add(listRow);

                        int addedIndex = addListRow(category, listRow);
                        int index = userBasedCategory.indexOf(category);
                        if (index != -1) {
                            userBasedCategory.get(index).setOrderNumber(addedIndex);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError : " + errorCode + ", message : " + message);
                        }
                        if (!isResumed()) {
                            return;
                        }
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return HomeVerticalFragment.this.getContext();
                    }
                });
        taskList.add(new WeakReference<>(task));
    }


    private void onSelectedBannerAction(Object object) {
        if (!(object instanceof Banner)) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onSelectedBannerAction invalid itemSelected not banner");
            }
            return;
        }

        Banner selectedBanner = (Banner) object;
        if (Log.INCLUDE) {
            Log.d(TAG, "selectMovie : " + selectedBanner.toString());
        }
        String linkType = selectedBanner.getLinkType();
        if (linkType == null) {
            return;
        }
        if (linkType.equalsIgnoreCase(Banner.LINK_TYPE_CONTENTS)) {

            String id = selectedBanner.getLinkInfo();
            ContentDataManager dataManager = new ContentDataManager();
            dataManager.getContentDetail(mAccountManager.getLocalLanguage(),
                    mAccountManager.getSaId(), mAccountManager.getProfileId(),
                    id, selectedBanner.getCategoryId(), new MbsDataProvider<String, ContentDetailInfo>() {
                        @Override

                        public void needLoading(boolean loading) {
                            if (loading) {
                                getProgressBarManager().show();
                            } else {
                                getProgressBarManager().hide();
                            }
                        }

                        @Override
                        public void onFailed(int key) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onFailed : " + key);
                            }
                            if (!isResumed()) {
                                return;
                            }
                            showNetworkCheckDialog(key);
                        }

                        @Override
                        public void onSuccess(String id, ContentDetailInfo result) {
                            showVodDetail(result.getContent());
                        }

                        @Override
                        public void onError(String errorCode, String message) {
                            if (!isResumed()) {
                                return;
                            }
                            ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                            errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                        }

                        @Override
                        public Context getTaskContext() {
                            return HomeVerticalFragment.this.getContext();
                        }
                    });
        } else if (linkType.equalsIgnoreCase(Banner.LINK_TYPE_SERIES)) {
            String id = selectedBanner.getLinkInfo();
            showSeriesDetail(id, selectedBanner.getCategoryId(), "");
        } else if (linkType.equalsIgnoreCase(Banner.LINK_TYPE_CHANNEL)) {
            String id = selectedBanner.getLinkInfo();
            PlaybackUtil.startLiveTvActivityWithServiceId(getContext(), id);
        }
    }

    private void initPromotionPage() {
        PromotionPagerPresenter viewPagerPresenter = new PromotionPagerPresenter(true, R.layout.view_main_promotion_indicator, getFragmentManager(),
                new ViewPagerPresenter.OnPromotionChangedListener() {

                    @Override
                    public void onPromotionComplete(int position) {
                        showLastThumbnail();
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onPageScrollStateChanged : " + state);
                        }
                        if (state == ViewPager.SCROLL_STATE_SETTLING) {
                            stopPlayer();
                        }
                    }

                    @Override
                    public void onPromotionChanged(int position, Object object) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onPromotionChanged : " + position + ", object : " + object);
                        }
                        if (!(object instanceof Banner)) {
                            return;
                        }
                        Banner selectedBanner = (Banner) object;
                        tuneBannerAsset(selectedBanner);
                    }

                    @Override
                    public void onPromotionSelected(Object object) {
                        onSelectedBannerAction(object);
                    }

                    @Override
                    public boolean onNotifyPromotionOutEvent(int position, KeyEvent event) {
                        mOnMenuInteractor.openSideMenu(true);
                        return true;
                    }
                });
        setOnVideoEventCallback(viewPagerPresenter);
        mPresenterSelector.addClassPresenter(PagerRow.class, viewPagerPresenter);
    }

    private void initVodRow() {
        LimitedIndexListRowPresenter rowPresenter = new LimitedIndexListRowPresenter(5);
        rowPresenter.setFixedItemIndex(CONTENTS_ALIGN_INDEX);
        rowPresenter.setOnPresenterDispatchKeyListener(new OnPresenterDispatchKeyListener() {
            @Override
            public boolean notifyKey(int position, KeyEvent event) {
                switch (event.getKeyCode()) {
                    case KeyEvent.KEYCODE_DPAD_LEFT:
                        if (position == 0) {
                            if (event.getRepeatCount() > 0) {
                                return true;
                            }
                            mOnMenuInteractor.openSideMenu(true);
                            return true;
                        }
                        return false;
                    case KeyEvent.KEYCODE_BACK: {
                        mOnMenuInteractor.openSideMenu(true);
                        return true;
                    }
                }
                return false;
            }
        });
        mPresenterSelector.addClassPresenter(CategoryListRow.class, rowPresenter);
    }

    private void initBannerRow() {
        RecommendRowPresenter rowPresenter = new RecommendRowPresenter(4);
        rowPresenter.setOnPresenterDispatchKeyListener(new OnPresenterDispatchKeyListener() {
            @Override
            public boolean notifyKey(int position, KeyEvent event) {
                switch (event.getKeyCode()) {
                    case KeyEvent.KEYCODE_DPAD_LEFT:
                        if (position == 0) {
                            if (event.getRepeatCount() > 0) {
                                return true;
                            }
                            mOnMenuInteractor.openSideMenu(true);
                            return true;
                        }
                        return false;
                    case KeyEvent.KEYCODE_BACK: {
                        mOnMenuInteractor.openSideMenu(true);
                        return true;
                    }
                }
                return false;
            }
        });
        mPresenterSelector.addClassPresenter(RecommendRow.class, rowPresenter);
    }


    @Override
    public void onStart() {
        super.onStart();
        if (Log.INCLUDE) {
            Log.d(TAG, "lifeCycle | onStart");
        }
        if (mTvOverlayManager == null) {
            return;
        }
        if (mTvOverlayManager.getDialogQueueSize() == 0) {
            resumePlayer(true);
        }
        if (mTvOverlayManager != null) {
            mTvOverlayManager.addOnQueueListener(queueListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Log.INCLUDE) {
            Log.d(TAG, "lifeCycle | onStop");
        }
        stopPlayer();
        onDetachedPromotion();
        if (mTvOverlayManager != null) {
            mTvOverlayManager.removeOnQueueListener(queueListener);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Log.INCLUDE) {
            Log.d(TAG, "onPause");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Log.INCLUDE) {
            Log.d(TAG, "onResume");
        }
    }

    public void reloadList(Category category) {
        mCurrentPromotionPlay = false;
        if (category != null) {
            currentCategory = category;
            Bundle bundle = new Bundle();
            bundle.putParcelable(KEY_CATEGORY, category);
            setArguments(bundle);
        }
        loadCategoryRow(getArguments().getParcelable(KEY_CATEGORY), 0);

    }

    public void reloadList() {
        reloadList(null);
    }

    public void renewalList() {
        mCurrentPromotionPlay = false;
        Category category = getArguments().getParcelable(KEY_CATEGORY);
        isSubCategoryLoading = true;
        MenuManager.getInstance().loadSubCategory(getContext(), mAccountManager.getLocalLanguage(), mAccountManager.getSaId(),
                category.getCategoryID(), category.getCategoryVersion(), 0, LOAD_SIZE, new MbsDataProvider<String, List<Category>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(category, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onFailed : " + key);
                        }
                        isSubCategoryLoading = false;
                        mOnMenuInteractor.setSideMenuEnable(false);
                        if (!isResumed()) {
                            return;
                        }
                        showNetworkCheckDialog(key);
                    }

                    @Override
                    public void onSuccess(String id, List<Category> result) {
                        promotionContain = hasPromotion(result);
                        if (getVerticalGridView() != null) {
                            getVerticalGridView().setItemViewCacheSize(result.size());
                        }
                        if (mOnMenuInteractor != null) {
                            mOnMenuInteractor.setSideMenuEnable(true);
                        }
                        isSubCategoryLoading = false;
                        BackendKnobsFlags backendKnobsFlags = TvSingletons.getSingletons(getContext()).getBackendKnobs();
                        for (Category category : result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onSuccess : Category " + category.toString());
                            }

                            switch (category.getLinkType()) {
                                case Promotion:
                                    if (backendKnobsFlags.checkCategoryUpdated(category.getCategoryID(), true)) {
                                        mRowsAdapter.removeItems(0, 1);
                                        loadPromotion(category);
                                    }
                                    break;
                                case Banner:
                                    if (backendKnobsFlags.checkCategoryUpdated(category.getCategoryID(), true)) {
                                        mRowsAdapter.removeItems(category.getOrderNumber() - 1, 1);
                                        loadBanner(category);
                                    }
                                    break;
                                case ContentsList:
                                    if (backendKnobsFlags.checkCategoryUpdated(category.getCategoryID(), true)) {
                                        mRowsAdapter.removeItems(category.getOrderNumber() - 1, 1);
                                        loadContentsData(category, promotionContain);
                                    }
                                    break;


                                case RecentWatch:
                                    renewalWatchData(category);
                                    break;
                                case Recommend:
                                    renewalRecommend(category);
                                    break;
                            }
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onError category : " + errorCode + ", message : " + message);
                        }
                        isSubCategoryLoading = false;
                        if (!isResumed()) {
                            return;
                        }
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return HomeVerticalFragment.this.getContext();
                    }
                });

    }

    public void clearData() {
        clearPromotionVod();
        mRowsAdapter.clear();
    }

    private static class ScrollWithRunnable extends RecyclerView.OnScrollListener {
        private final String TAG = ScrollWithRunnable.class.getSimpleName();
        private Runnable mRunnable;
        private final Handler handler = new Handler(Looper.getMainLooper());
        private VerticalGridView mVerticalGridView;

        public void setRunnable(Runnable runnable, VerticalGridView verticalGridView) {
            this.mRunnable = runnable;
            this.mVerticalGridView = verticalGridView;
        }

        public void removeHandler() {
            if (mRunnable != null) {
                handler.removeCallbacks(mRunnable);
            }
        }

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (Log.INCLUDE) {
                Log.d(TAG, "onScrollStateChanged | newState : " + newState);
            }

        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (mRunnable != null) {
                handler.removeCallbacks(mRunnable);
            }
            handler.postDelayed(mRunnable, 200);
            mVerticalGridView.removeOnScrollListener(this);
        }
    }


    private final ScrollWithRunnable mScrollWithRunnable = new ScrollWithRunnable();

    public void notifyUpdateContentsAfterWatching(boolean contentsRefresh) {
        for (Category userCategory : userBasedCategory) {
            Category category = userCategory;
            if (Log.INCLUDE) {
                Log.d(TAG, "onSuccess : Category | " + userCategory.getOrderNumber() + ", category" + category.toString());
            }
            switch (category.getLinkType()) {
                case RecentWatch:
                    renewalWatchData(category);
                    break;
                case Recommend:
                    renewalRecommend(category);
                    break;
            }
        }
        if (contentsRefresh) {
            notifyUpdateContents();
        }
    }

    public void notifyUpdateUserContents(boolean contentsRefresh) {
        if (Log.INCLUDE) {
            Log.d(TAG, "userBasedCategory : " + userBasedCategory.size());
        }
        Runnable mRunnable = new Runnable() {
            @Override
            public void run() {
                for (Category userCategory : userBasedCategory) {
                    Category category = userCategory;
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onSuccess : Category | " + userCategory.getOrderNumber() + ", category" + category.toString());
                    }
                    int itemIndex = findIndexByHeaderId(category);
                    if (itemIndex != -1) {
                        mRowsAdapter.remove(itemIndex);
                        mBridgeAdapter.notifyItemRemoved(itemIndex);
                    }
                }


                for (Category userCategory : userBasedCategory) {
                    Category category = userCategory;
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onSuccess : Category | " + userCategory.getOrderNumber() + ", category" + category.toString());
                    }
                    switch (category.getLinkType()) {
                        case RecentWatch:
                            renewalWatchData(category);
                            break;
                        case Recommend:
                            renewalRecommend(category);
                            break;
                    }
                }
                if (contentsRefresh) {
                    notifyUpdateContents();
                }
            }
        };
        if (currentScrollOffset == 0) {
            new Handler(Looper.getMainLooper()).post(mRunnable);
        } else {
            mScrollWithRunnable.setRunnable(mRunnable, mVerticalGridView);
            mVerticalGridView.addOnScrollListener(mScrollWithRunnable);
            mVerticalGridView.smoothScrollToPosition(0);
        }


    }

    public void notifyUpdateContents() {
        mRowsAdapter.notifyArrayItemRangeChanged(1, mRowsAdapter.size());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (Log.INCLUDE) {
            Log.d(TAG, "onDestroyView");
        }
        if (getVerticalGridView() != null) {
            getVerticalGridView().removeCallbacks(mFocusRunnable);
        }
        mScrollWithRunnable.removeHandler();
        releasePlayer();
        setOnVideoEventCallback(null);
        for (WeakReference<AsyncTask> task : taskList) {
            if (task.get() != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "canceled");
                }
                task.get().cancel(true);
            }
        }
        taskList.clear();
    }

}
