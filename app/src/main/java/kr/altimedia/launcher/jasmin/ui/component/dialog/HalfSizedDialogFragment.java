/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.concurrent.TimeUnit;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

public class HalfSizedDialogFragment extends SafeDismissDialogFragment {
    public static final String DIALOG_TAG = HalfSizedDialogFragment.class.getName();
    public static final String TRACKER_LABEL = "Half sized dialog";

    private static final long AUTO_DISMISS_TIME_THRESHOLD_MS = TimeUnit.SECONDS.toMillis(30);

    private OnActionClickListener mOnActionClickListener;

    private Handler mHandler = new Handler();
    private Runnable mAutoDismisser =
            new Runnable() {
                @Override
                public void run() {
                    dismiss();
                }
            };

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.halfsized_dialog, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        mHandler.postDelayed(mAutoDismisser, AUTO_DISMISS_TIME_THRESHOLD_MS);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOnActionClickListener != null) {
            // Dismisses the dialog to prevent the callback being forgotten during
            // fragment re-creating.
            dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mHandler.removeCallbacks(mAutoDismisser);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnKeyListener(
                new DialogInterface.OnKeyListener() {
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent keyEvent) {
                        mHandler.removeCallbacks(mAutoDismisser);
                        mHandler.postDelayed(mAutoDismisser, AUTO_DISMISS_TIME_THRESHOLD_MS);
                        return false;
                    }
                });
        return dialog;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DCA;
    }

    @Override
    public int getTheme() {
        return R.style.Theme_TV_dialog_HalfSizedDialog;
    }

    /**
     * Sets {@link OnActionClickListener} for the dialog fragment. If listener is set, the dialog
     * will be automatically closed when it's paused to prevent the fragment being re-created by the
     * framework, which will result the listener being forgotten.
     */
    public void setOnActionClickListener(OnActionClickListener listener) {
        mOnActionClickListener = listener;
    }

    /**
     * Returns {@link OnActionClickListener} for sub-classes or any inner fragments.
     */
    protected OnActionClickListener getOnActionClickListener() {
        return mOnActionClickListener;
    }

    /**
     * An interface to provide callbacks for half-sized dialogs. Subclasses or inner fragments
     * should invoke {@link OnActionClickListener#onActionClick(long)} and provide the identifier of
     * the action user clicked.
     */
    public interface OnActionClickListener {
        void onActionClick(long actionId);
    }
}
