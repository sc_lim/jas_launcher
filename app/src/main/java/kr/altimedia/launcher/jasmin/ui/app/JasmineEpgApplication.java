/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.ui.app;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.PowerManager;
import android.provider.Settings;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.common.BackendKnobsFlags;
import com.altimedia.tvmodule.common.HasSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;
import com.altimedia.util.time.Clock;
import com.altimedia.util.time.OnTimeSetListener;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.BuildConfig;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.cwmp.service.def.CWMPEtcCode;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.dm.category.MenuManager;
import kr.altimedia.launcher.jasmin.dm.category.MenuUpdater;
import kr.altimedia.launcher.jasmin.dm.category.obj.Category;
import kr.altimedia.launcher.jasmin.dm.channel.ChannelLoader;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.dm.coupon.CouponNoticeManager;
import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import kr.altimedia.launcher.jasmin.logging.LogBackServiceProvider;
import kr.altimedia.launcher.jasmin.search.JasSearchActivity;
import kr.altimedia.launcher.jasmin.system.manager.SystemInfoManager;
import kr.altimedia.launcher.jasmin.system.manager.reboot.AutoRebootManager;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.module.DefaultBackendKnobsFlags;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.ReminderAlertManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.sleep.SleepModeReceiver;
import kr.altimedia.launcher.jasmin.ui.view.util.FontUtil;

public class JasmineEpgApplication extends EpgApplication implements HasSingletons<TvSingletons>,
        Application.ActivityLifecycleCallbacks {
    public static final String TAG = JasmineEpgApplication.class.getSimpleName();
    boolean isEpgReady = false;
    public static final String KEY_STOPPED = "isStopped";
    private final SleepModeReceiver mSleepModeReceiver = new SleepModeReceiver();
    private final BroadcastReceiver mConfigChangesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            switch (intent.getAction()) {
                case Intent.ACTION_LOCALE_CHANGED:
                    if (Log.INCLUDE) {
                        Log.d(TAG, "reset");
                    }
                    boolean isComplete = isUserSetupComplete(getApplicationContext());
                    if (Log.INCLUDE) {
                        Log.d(TAG, "isComplete : " + isComplete);
                    }
                    if (!isComplete) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "ACTION_LOCALE_CHANGED but setup wizard not complete so return");
                        }
                        break;
                    }
                    restartApp(getApplicationContext(), 100);
                    break;
            }
        }
    };

    private boolean isUserSetupComplete(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {

            boolean tvUserSetupComplete = Settings.Secure.getInt(
                    context.getContentResolver(), "tv_user_setup_complete", 0)
                    == 1;
            boolean userSetupComplete = Settings.Secure.getInt(
                    context.getContentResolver(), "user_setup_complete", 0)
                    == 1;

            if (Log.INCLUDE) {
                Log.d(TAG, "isUserSetupComplete | tvUserSetupComplete : " + tvUserSetupComplete);
                Log.d(TAG, "isUserSetupComplete | userSetupComplete : " + userSetupComplete);
            }

            return tvUserSetupComplete;
        } else {
            boolean provisioned = Settings.Secure.getInt(
                    context.getContentResolver(), Settings.Global.DEVICE_PROVISIONED, 0)
                    == 1;
            if (Log.INCLUDE) {
                Log.d(TAG, "isUserSetupComplete | provisioned : " + provisioned);
            }
            return provisioned;
        }
    }

    private void restartApp(Context context, int delay) {
        if (context == null) {
            return;
        }
        if (delay == 0) {
            delay = 1;
        }
//        Intent restartIntent = new Intent(context, LauncherActivity.class);
//        @SuppressLint("WrongConstant")
//        PendingIntent intent = PendingIntent.getActivity(context, 0, restartIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
//        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        manager.set(AlarmManager.RTC, System.currentTimeMillis() + delay, intent);
//        android.os.Process.killProcess(android.os.Process.myPid());
        if (mLauncherActivity != null) {
            mLauncherActivity.finish();
            mLauncherActivity = null;
        }
        if (mLiveTvActivity != null) {
            mLiveTvActivity.finish();
            mLiveTvActivity = null;
        }
        if (mVodActivity != null) {
            mVodActivity.finish();
            mVodActivity = null;
        }
        if (mSearchActivity != null) {
            mSearchActivity.finish();
            mSearchActivity = null;
        }
        Runtime.getRuntime().exit(0);
    }

    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;


    ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback() {
        @Override
        public void onAvailable(@NonNull Network network) {
            super.onAvailable(network);
            if (Log.INCLUDE) {
                Log.d(TAG, "onAvailable");
            }
            startCWMPService(getApplicationContext());

        }

        @Override
        public void onLost(@NonNull Network network) {
            super.onLost(network);
            if (Log.INCLUDE) {
                Log.d(TAG, "onLost");
            }
            CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_151);
        }

        @Override
        public void onUnavailable() {
            super.onUnavailable();
            if (Log.INCLUDE) {
                Log.d(TAG, "onUnavailable");
            }
        }
    };

    private final DefaultBackendKnobsFlags mBackendKnobsFlags = new DefaultBackendKnobsFlags() {


        @Override
        public void notifyEpgInitializeComplete() {
            if (Log.INCLUDE) {
                Log.d(TAG, "notifyEpgInitializeComplete");
            }
            ReminderAlertManager.getInstance().initReminderList(getApplicationContext());
        }

        @Override
        public boolean compiled() {
            return super.compiled();
        }

        @Override
        public long epgFetcherIntervalHour() {
            return super.epgFetcherIntervalHour();
        }

        @Override
        public long programGuideInitialFetchHours() {
            return super.programGuideInitialFetchHours();
        }

        @Override
        public long programGuideMaxHours() {
            return super.programGuideMaxHours();
        }

        @Override
        public boolean isEpgReady(boolean containUserData) {
            return JasChannelManager.getInstance().isLiveTvReady(containUserData);
        }

        @Override
        public long epgTargetChannelCount() {
            return super.epgTargetChannelCount();
        }

        @Override
        public boolean needMenuUpdated(boolean flush) {
            boolean needUpdated = mNeedMenuUpdated;
            if (flush) {
                setNeedMenuUpdated(false);
            }
            return needUpdated;
        }

        @Override
        public void notifyMenuVersionChecked() {
            super.notifyMenuVersionChecked();
            if (Log.INCLUDE) {
                Log.d(TAG, "notifyMenuVersionChecked");
            }
            if (mMenuUpdateHandler == null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyMenuVersionChecked but mMenuUpdateHandler == null so need to check");
                }
                return;
            }

            mMenuUpdateHandler.removeCallbacks(mMenuUpdateRunnable);
            if (Log.INCLUDE) {
                Log.d(TAG, "call notifyMenuVersionChecked : delayTime : " +
                        AccountManager.getInstance().getMenuUpdatePeriod());
            }
            mMenuUpdateHandler.postDelayed(mMenuUpdateRunnable, AccountManager.getInstance().getMenuUpdatePeriod());
        }
    };
    private ChannelLoader loader = null;
    private boolean mNeedMenuUpdated = false;

    @Override
    public BackendKnobsFlags getBackendKnobs() {
        return mBackendKnobsFlags;
    }

    private void printAppInfo() {
        android.util.Log.i(TAG, "APP_INFO ###########################################################");
        android.util.Log.i(TAG, "APP_INFO ## APPLICATION_ID: " + BuildConfig.APPLICATION_ID);
        android.util.Log.i(TAG, "APP_INFO ## VERSION_NAME: " + BuildConfig.VERSION_NAME);
        android.util.Log.i(TAG, "APP_INFO ## VERSION_CODE: " + BuildConfig.VERSION_CODE);
        android.util.Log.i(TAG, "APP_INFO ## FW_VERSION: " + SystemInfoManager.getInstance().getFirmwareVersion());
        android.util.Log.i(TAG, "APP_INFO ## ---------------------------------------------------");
        android.util.Log.i(TAG, "APP_INFO ## BUILD_TYPE: " + BuildConfig.BUILD_TYPE);
        android.util.Log.i(TAG, "APP_INFO ## DEBUG: " + BuildConfig.DEBUG);
        android.util.Log.i(TAG, "APP_INFO ## Log.INCLUDE: " + Log.INCLUDE);
        android.util.Log.i(TAG, "APP_INFO ## ---------------------------------------------------");
        android.util.Log.i(TAG, "APP_INFO ## FLAVOR: " + BuildConfig.FLAVOR);
        android.util.Log.i(TAG, "APP_INFO ## SERVER_TYPE: " + ServerConfig.SERVER_TYPE);
        android.util.Log.i(TAG, "APP_INFO ## MBS_URL: " + ServerConfig.MBS_URL);
        android.util.Log.i(TAG, "APP_INFO ## SEARCH_URL: " + ServerConfig.SEARCH_URL);
        android.util.Log.i(TAG, "APP_INFO ## AD_URL: " + ServerConfig.AD_URL);
        android.util.Log.i(TAG, "APP_INFO ## RECOMMEND_URL: " + ServerConfig.RECOMMEND_URL);
        android.util.Log.i(TAG, "APP_INFO ## LA_URL: " + ServerConfig.LA_URL);
        android.util.Log.i(TAG, "APP_INFO ###########################################################");
    }

    private HandlerThread mMenuUpdateThread = null;
    private Handler mMenuUpdateHandler = null;
    private MenuUpdater mMenuUpdateRunnable;

    private void initializeUpdateThread() {
        if (Log.INCLUDE) {
            Log.d(TAG, "initializeUpdateThread");
        }
        if (mMenuUpdateThread != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "initializeUpdateThread | mMenuUpdateThread != null  please check");
            }
            mMenuUpdateThread.quitSafely();
        }
        mMenuUpdateThread = new HandlerThread("MenuUpdateThread");
        mMenuUpdateThread.start();

        if (mMenuUpdateHandler != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "initializeUpdateThread | mMenuUpdateHandler != null  please check");
            }
        }
        mMenuUpdateHandler = new Handler(mMenuUpdateThread.getLooper());

        if (mMenuUpdateRunnable != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "initializeUpdateThread | mMenuUpdateRunnable != null  please check");
            }
        }
        mMenuUpdateRunnable = new MenuUpdater(getApplicationContext(),
                new MenuManager.OnMenuUpdateCallback() {
                    @Override
                    public void onMenuMenuUpdated(List<Category> result) {
                        setNeedMenuUpdated(true);
                    }

                    @Override
                    public void onCategoryUpdated(List<Category> updatedCategoryList) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onCategoryUpdated " + updatedCategoryList.size());
                        }
                        for (Category category : updatedCategoryList) {
                            mBackendKnobsFlags.putCategoryUpdated(category.getCategoryID());
                        }
                    }

                    @Override
                    public void onContentsCategoryUpdated(String categoryId, String version) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onContentsCategoryUpdated " + categoryId + ", version : " + version);
                        }
                        mBackendKnobsFlags.putCategoryUpdated(categoryId);
                    }

                    @Override
                    public void onFailed() {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "menu update fail so retry next time");
                        }
                        mBackendKnobsFlags.notifyMenuVersionChecked();
                    }
                });
    }

    private void clearUpdateThread() {
        if (Log.INCLUDE) {
            Log.d(TAG, "clearUpdateThread");
        }
        if (mMenuUpdateHandler != null) {
            mMenuUpdateHandler.removeCallbacks(mMenuUpdateRunnable);
        }
        if (mMenuUpdateThread != null) {
            mMenuUpdateThread.quitSafely();
        }
        if (mMenuUpdateRunnable != null) {
            mMenuUpdateRunnable.dispose();
        }
    }


    public void acquireWakeLock(Context context) {
        try {
            if (powerManager == null) {
                powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            }
            if (wakeLock == null) {
                wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
            }
            wakeLock.acquire();
            if (Log.INCLUDE) {
                Log.d(TAG, "acquireWakeLock");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copyVOConfig() throws IOException {
        String path = "/sdcard/android/data/" + getPackageName() + "/files/";
        String fileName = "config.json";

        if (Log.INCLUDE) {
            Log.d(TAG, "copyVOConfig, copy [" + fileName + "] to " + path);
        }

        OutputStream myOutput = new FileOutputStream(path + fileName);
        byte[] buffer = new byte[1024];
        int length;
        InputStream myInput = getAssets().open(fileName);

        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        myInput.close();
        myOutput.flush();
        myOutput.close();
    }

    @Override
    public void onCreate() {
        printAppInfo();
        super.onCreate();
        getSingletons(getApplicationContext());
        try {
            copyVOConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ErrorMessageManager.getInstance().initialize(getApplicationContext());
        acquireWakeLock(getApplicationContext());
        initializeUpdateThread();
        AccountManager.getInstance().initialize(getApplicationContext());
        FontUtil.getInstance().load(getApplicationContext());
        registerActivityLifecycleCallbacks(this);
        getApplicationContext().registerReceiver(mConfigChangesReceiver, new IntentFilter(Intent.ACTION_LOCALE_CHANGED));
        JasChannelManager.getInstance().init(this, getChannelDataManager());
        JasChannelManager.getInstance().registerJasChannelReceiver(this);

        registerAutoReboot();
        CouponNoticeManager.getInstance().init(this);
        loader = new ChannelLoader(getApplicationContext());
        loader.startSync(new ChannelLoader.SyncListener() {
            @Override
            public void onUpdated(List<Channel> channels) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onUpdated : " + channels.size());
                }
                isEpgReady = channels.size() != 0;
            }

            @Override
            public void onScanFinished(List<Channel> channels) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onScanFinished : " + channels.size());
                }

                isEpgReady = channels.size() != 0;
                mBackendKnobsFlags.removeInitializeWork(DefaultBackendKnobsFlags.WORK_EPG_SCANNING);
            }

            @Override
            public void onScanStart() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onScanStart");
                }
                isEpgReady = false;
            }

            @Override
            public void onScanError(int errorCode) {

                if (Log.INCLUDE) {
                    Log.d(TAG, "onScanError");
                }
                isEpgReady = false;
            }
        });

        startJasSevice();
        initNetworkChecker(getApplicationContext());
    }

    private void registerAutoReboot() {
        AutoRebootManager.getInstance().setRebootTimer(getApplicationContext());
        SystemClock().addClockUpdateListener(new OnTimeSetListener() {
            @Override
            public void notifyTimeSetChanged(long ntpTime) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "notifyTimeSetChanged");
                }
                SystemClock().removeClockUpdateListener(this);
                AutoRebootManager.getInstance().setRebootTimer(getApplicationContext());
            }
        });

    }


    private void initNetworkChecker(Context context) {
        ConnectivityManager connectivityManager = context.getSystemService(ConnectivityManager.class);
        connectivityManager.registerDefaultNetworkCallback(networkCallback);
    }

    private void deInitNetworkChecker(Context context) {
        ConnectivityManager connectivityManager = context.getSystemService(ConnectivityManager.class);
        connectivityManager.unregisterNetworkCallback(networkCallback);
    }

    public static void startCWMPService(Context context) {
        boolean initialized = CWMPServiceProvider.getInstance().init(context);
        if (!initialized) {
            if (Log.INCLUDE) {
                Log.d(TAG, "CWMP is already initialized so return");
            }
            return;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "CWMP is  initialized, call start ");
        }
        CWMPServiceProvider.getInstance().start();
    }

    private void startJasSevice() {
        LogBackServiceProvider.getInstance().init(getApplicationContext());
        LogBackServiceProvider.getInstance().start();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
//        PowerStatusManager.getInstance().releaseWakeLock();
        ReminderAlertManager.getInstance().clearAllDialog();
        TvSingletons.getSingletons(getApplicationContext()).getProgramDataManager().stop();
        clearUpdateThread();
        getApplicationContext().unregisterReceiver(mConfigChangesReceiver);
        JasChannelManager.getInstance().unregisterJasChannelReceiver(this);
        unregisterActivityLifecycleCallbacks(this);
        if (loader != null) {
            loader.clearSync();
        }
        deInitNetworkChecker(getApplicationContext());
    }

    public static Clock SystemClock() {
        return sSingletons.getClock();
    }

    @Override
    public TvSingletons singletons() {

        return this;
    }

    private Activity mLauncherActivity;
    private Activity mLiveTvActivity;
    private Activity mVodActivity;
    private Activity mSearchActivity;

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onActivityCreated : " + activity.getClass().getSimpleName());
        }
        if (activity instanceof LauncherActivity) {
            mLauncherActivity = activity;
        } else if (activity instanceof LiveTvActivity) {
            mLiveTvActivity = activity;
        } else if (activity instanceof VideoPlaybackActivity) {
            mVodActivity = activity;
        } else if (activity instanceof JasSearchActivity) {
            mSearchActivity = activity;
        }
        /*if (activity instanceof VideoPlaybackActivity || activity instanceof LiveTvActivity) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }*/
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onActivityStarted : " + activity.getClass().getSimpleName());
        }
        if (activity instanceof LauncherActivity) {
            CouponNoticeManager.getInstance().showCouponNotification(activity);
        }

        registerSleepModeReceiver(activity);
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {
        unRegisterSleepModeReceiver(activity);
    }


    private void registerSleepModeReceiver(Activity activity) {
        if (Log.INCLUDE) {
            Log.d(TAG, "called registerSleepModeReceiver : " + activity.getClass().getSimpleName());
        }

        IntentFilter mIntentFilter =new IntentFilter();
        mIntentFilter.addAction(SleepModeReceiver.ACTION_SCREEN_SAVER_MODE);
        mIntentFilter.addAction(SleepModeReceiver.ACTION_SLEEP_MODE);

        activity.registerReceiver(mSleepModeReceiver, mIntentFilter);
    }

    private void unRegisterSleepModeReceiver(Activity activity) {
        if (Log.INCLUDE) {
            Log.d(TAG, "called unRegisterSleepModeReceiver : " + activity.getClass().getSimpleName());
        }

        activity.unregisterReceiver(mSleepModeReceiver);
    }

    public void setNeedMenuUpdated(boolean mNeedMenuUpdated) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setNeedMenuUpdated : " + mNeedMenuUpdated);
        }
        this.mNeedMenuUpdated = mNeedMenuUpdated;
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onActivityResumed : " + activity.getClass().getSimpleName());
        }
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onActivityPaused : " + activity.getClass().getSimpleName());
        }
    }


    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onActivitySaveInstanceState : " + activity.getClass().getSimpleName());
        }
    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onActivityDestroyed : " + activity.getClass().getSimpleName());
        }
        if (activity instanceof LauncherActivity) {
            mLauncherActivity = null;
        } else if (activity instanceof LiveTvActivity) {
            mLiveTvActivity = null;
        } else if (activity instanceof VideoPlaybackActivity) {
            mVodActivity = null;
        } else if (activity instanceof JasSearchActivity) {
            mSearchActivity = null;
        }
    }

}
