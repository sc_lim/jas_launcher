/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.animation.TimeAnimator;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewParent;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

import androidx.leanback.graphics.ColorOverlayDimmer;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.ShadowOverlayContainer;
import androidx.leanback.widget.ShadowOverlayHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Slide;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.FocusHighlightHandler;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowHeaderPresenter;

public class FocusHighlightHelper {
    private static final String TAG = FocusHighlightHelper.class.getSimpleName();

    public static boolean isValidZoomIndex(int zoomIndex) {
        return zoomIndex == 0 || getResId(zoomIndex) > 0;
    }

    static int getResId(int zoomIndex) {
        if (Log.INCLUDE) {
            Log.d(TAG, "getResId : " + zoomIndex);
        }
        switch (zoomIndex) {
            case 1:
                return R.fraction.focus_zoom_factor_small;
            case 2:
                return R.fraction.focus_zoom_factor_medium;
            case 3:
                return R.fraction.focus_zoom_factor_large;
            case 4:
                return R.fraction.focus_zoom_factor_xsmall;
            case 5:
                return R.fraction.focus_zoom_factor_xlarge;
            default:
                return 0;
        }
    }

    public static void setupBrowseItemFocusHighlight(ItemBridgeAdapter adapter, int zoomIndex, boolean useDimmer) {
        adapter.setFocusHighlight(new FocusHighlightHelper.BrowseItemFocusHighlight(zoomIndex, useDimmer));
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static void setupHeaderItemFocusHighlight(VerticalGridView gridView) {
        setupHeaderItemFocusHighlight(gridView, true);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static void setupHeaderItemFocusHighlight(VerticalGridView gridView, boolean scaleEnabled) {
        if (gridView != null && gridView.getAdapter() instanceof ItemBridgeAdapter) {
            ((ItemBridgeAdapter) gridView.getAdapter()).setFocusHighlight(new FocusHighlightHelper.HeaderItemFocusHighlight(scaleEnabled));
        }

    }

    public static void setupHeaderItemFocusHighlight(ItemBridgeAdapter adapter) {
        setupHeaderItemFocusHighlight(adapter, true);
    }

    public static void setupHeaderItemFocusHighlight(ItemBridgeAdapter adapter, boolean scaleEnabled) {
        adapter.setFocusHighlight(new FocusHighlightHelper.HeaderItemFocusHighlight(scaleEnabled));
    }

    /**
     * @deprecated
     */
    @Deprecated
    public FocusHighlightHelper() {
    }

    static class HeaderItemFocusHighlight implements FocusHighlightHandler {
        private boolean mInitialized;
        private float mSelectScale;
        private int mDuration;
        boolean mScaleEnabled;

        HeaderItemFocusHighlight(boolean scaleEnabled) {
            this.mScaleEnabled = scaleEnabled;
        }

        void lazyInit(View view) {
            if (!this.mInitialized) {
                Resources res = view.getResources();
                TypedValue value = new TypedValue();
                if (this.mScaleEnabled) {
                    res.getValue(R.dimen.lb_browse_header_select_scale, value, true);
                    this.mSelectScale = value.getFloat();
                } else {
                    this.mSelectScale = 1.0F;
                }

                res.getValue(R.dimen.lb_browse_header_select_duration, value, true);
                this.mDuration = value.data;
                this.mInitialized = true;
            }

        }

        private void viewFocused(View view, boolean hasFocus) {
            this.lazyInit(view);
            view.setSelected(hasFocus);
            FocusHighlightHelper.FocusAnimator animator = (FocusHighlightHelper.FocusAnimator) view.getTag(R.id.lb_focus_animator);
            if (animator == null) {
                animator = new FocusHighlightHelper.HeaderItemFocusHighlight.HeaderFocusAnimator(view, this.mSelectScale, this.mDuration);
                view.setTag(R.id.lb_focus_animator, animator);
            }

            animator.animateFocus(hasFocus, false);
        }

        public void onItemFocused(View view, boolean hasFocus) {
            this.viewFocused(view, hasFocus);
        }

        public void onInitializeView(View view) {
        }

        static class HeaderFocusAnimator extends FocusHighlightHelper.FocusAnimator {
            ItemBridgeAdapter.ViewHolder mViewHolder;

            HeaderFocusAnimator(View view, float scale, int duration) {
                super(view, scale, false, duration);

                ViewParent parent;
                for (parent = view.getParent(); parent != null && !(parent instanceof RecyclerView); parent = parent.getParent()) {
                }

                if (parent != null) {
                    this.mViewHolder = (ItemBridgeAdapter.ViewHolder) ((RecyclerView) parent).getChildViewHolder(view);
                }

            }

            void setFocusLevel(float level) {
                Presenter presenter = this.mViewHolder.getPresenter();
                if (presenter instanceof RowHeaderPresenter) {
                    ((RowHeaderPresenter) presenter).setSelectLevel((RowHeaderPresenter.ViewHolder) this.mViewHolder.getViewHolder(), level);
                }

                super.setFocusLevel(level);
            }
        }
    }

    private static float getScale(Resources res, int scaleIndex) {
        return scaleIndex == 0 ? 1.0F : res.getFraction(FocusHighlightHelper.getResId(scaleIndex), 1, 1);
    }

    static class BrowseItemFocusHighlight implements FocusHighlightHandler {
        private static final int DURATION_MS = 150;
        private int mScaleIndex;
        private final boolean mUseDimmer;

        BrowseItemFocusHighlight(int zoomIndex, boolean useDimmer) {
            if (!FocusHighlightHelper.isValidZoomIndex(zoomIndex)) {
                throw new IllegalArgumentException("Unhandled zoom index");
            } else {
                this.mScaleIndex = zoomIndex;
                this.mUseDimmer = useDimmer;
            }
        }


        private float getScale(Resources res) {
            return FocusHighlightHelper.getScale(res, this.mScaleIndex);
        }


        public void onItemFocused(View view, boolean hasFocus) {
            view.setSelected(hasFocus);
            this.getOrCreateAnimator(view).animateFocus(hasFocus, false);
            if (view.findViewById(R.id.noneFocusLayer) != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "CHECK onItemFocused : " + hasFocus);
                }
                this.getOrCreateUnfocusAnimator(view.findViewById(R.id.noneFocusLayer), Gravity.LEFT).animateFocus(hasFocus, false);
            }

            if (view.findViewById(R.id.noneFocusRightLayer) != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "CHECK onItemFocused : " + hasFocus);
                }
                this.getOrCreateUnfocusAnimator(view.findViewById(R.id.noneFocusRightLayer), Gravity.RIGHT).animateFocus(hasFocus, false);
            }

            if (view.findViewById(R.id.noneFocusBottomLayer) != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "CHECK onItemFocused : " + hasFocus);
                }
                this.getOrCreateUnfocusAnimator(view.findViewById(R.id.noneFocusBottomLayer), Gravity.BOTTOM).animateFocus(hasFocus, false);
            }
        }

        public void onInitializeView(View view) {
            this.getOrCreateAnimator(view).animateFocus(false, true);
            if (view.findViewById(R.id.noneFocusLayer) != null) {
                this.getOrCreateUnfocusAnimator(view.findViewById(R.id.noneFocusLayer), Gravity.LEFT).animateFocus(false, true);
            }

            if (view.findViewById(R.id.noneFocusRightLayer) != null) {
                this.getOrCreateUnfocusAnimator(view.findViewById(R.id.noneFocusRightLayer), Gravity.RIGHT).animateFocus(false, true);
            }

            if (view.findViewById(R.id.noneFocusBottomLayer) != null) {
                this.getOrCreateUnfocusAnimator(view.findViewById(R.id.noneFocusBottomLayer), Gravity.BOTTOM).animateFocus(false, true);
            }
        }

        private FocusHighlightHelper.FocusAnimator getOrCreateAnimator(View view) {
            FocusHighlightHelper.FocusAnimator animator = (FocusHighlightHelper.FocusAnimator) view.getTag(R.id.focus_animator);
            if (animator == null) {
                animator = new FocusHighlightHelper.FocusAnimator(view, this.getScale(view.getResources()), this.mUseDimmer, 150);
                view.setTag(R.id.focus_animator, animator);
            }

            return animator;
        }


        private FocusHighlightHelper.UnFocusAnimator getOrCreateUnfocusAnimator(View view, @Slide.GravityFlag int gravity) {
            FocusHighlightHelper.UnFocusAnimator animator = (FocusHighlightHelper.UnFocusAnimator) view.getTag(R.id.unFocus_animator);
            if (animator == null) {
                animator = new FocusHighlightHelper.UnFocusAnimator(view, this.getScale(view.getResources()), this.mUseDimmer, 150, gravity);
                view.setTag(R.id.unFocus_animator, animator);
            }

            return animator;
        }
    }

    static class FocusAnimator implements TimeAnimator.TimeListener {
        private final View mView;
        private final int mDuration;
        private final ShadowOverlayContainer mWrapper;
        private final float mScaleDiff;
        private float mFocusLevel = 0.0F;
        private float mFocusLevelStart;
        private float mFocusLevelDelta;
        private final TimeAnimator mAnimator = new TimeAnimator();
        private final Interpolator mInterpolator = new AccelerateDecelerateInterpolator();
        private final ColorOverlayDimmer mDimmer;
        private final float mScale;

        void animateFocus(boolean select, boolean immediate) {
            this.endAnimation();
            float end = select ? 1.0F : 0.0F;
            if (immediate) {
                this.setFocusLevel(end);
            } else if (this.mFocusLevel != end) {
                this.mFocusLevelStart = this.mFocusLevel;
                this.mFocusLevelDelta = end - this.mFocusLevelStart;
                this.mAnimator.start();
            }
        }

        FocusAnimator(View view, float scale, boolean useDimmer, int duration) {
            this.mView = view;
            this.mDuration = duration;
            this.mScaleDiff = scale - 1.0F;
            this.mScale = scale;
            if (view instanceof ShadowOverlayContainer) {
                this.mWrapper = (ShadowOverlayContainer) view;
            } else {
                this.mWrapper = null;
            }

            this.mAnimator.setTimeListener(this);
            if (useDimmer) {
                this.mDimmer = ColorOverlayDimmer.createDefault(view.getContext());
            } else {
                this.mDimmer = null;
            }

        }

        void setFocusLevel(float level) {
            setFocusLevel(1.f, level);
        }

        void setFocusLevel(float scaleStart, float level) {
            this.mFocusLevel = level;
            float scale = scaleStart + this.mScaleDiff * level;

            this.mView.setScaleX(scale);
            this.mView.setScaleY(scale);
            if (this.mWrapper != null) {
                this.mWrapper.setShadowFocusLevel(level);
            } else {
                ShadowOverlayHelper.setNoneWrapperShadowFocusLevel(this.mView, level);
            }

            if (this.mDimmer != null) {
                this.mDimmer.setActiveLevel(level);
                int color = this.mDimmer.getPaint().getColor();
                if (this.mWrapper != null) {
                    this.mWrapper.setOverlayColor(color);
                } else {
                    ShadowOverlayHelper.setNoneWrapperOverlayColor(this.mView, color);
                }
            }
        }

        void endAnimation() {
            this.mAnimator.end();
        }

        public void onTimeUpdate(TimeAnimator animation, long totalTime, long deltaTime) {
            float fraction;
            if (totalTime >= (long) this.mDuration) {
                fraction = 1.0F;
                this.mAnimator.end();
            } else {
                fraction = (float) ((double) totalTime / (double) this.mDuration);
            }

            if (this.mInterpolator != null) {
                fraction = this.mInterpolator.getInterpolation(fraction);
            }

            this.setFocusLevel(this.mFocusLevelStart + fraction * this.mFocusLevelDelta);
        }
    }


    static class UnFocusAnimator implements TimeAnimator.TimeListener {
        private final View mView;
        private final int mDuration;
        private final ShadowOverlayContainer mWrapper;
        private final float mScaleDiff;
        private float mFocusLevel = 0.0F;
        private float mFocusLevelStart;
        private float mFocusLevelDelta;
        private final TimeAnimator mAnimator = new TimeAnimator();
        private final Interpolator mInterpolator = new AccelerateDecelerateInterpolator();
        private final ColorOverlayDimmer mDimmer;
        private int mGravity = Gravity.LEFT;
        private final float mScale;
        private final float mBaseScale;

        UnFocusAnimator(View view, float scale, boolean useDimmer, int duration, @Slide.GravityFlag int gravity) {
            this.mBaseScale = FocusHighlightHelper.getScale(view.getResources(), 5);
            this.mView = view;
            this.mDuration = duration;
            this.mScale = scale;
            this.mScaleDiff = scale - 1.0F;
            this.mGravity = gravity;
            if (view instanceof ShadowOverlayContainer) {
                this.mWrapper = (ShadowOverlayContainer) view;
            } else {
                this.mWrapper = null;
            }

            this.mAnimator.setTimeListener(this);
            if (useDimmer) {
                this.mDimmer = ColorOverlayDimmer.createDefault(view.getContext());
            } else {
                this.mDimmer = null;
            }

        }

        void animateFocus(boolean select, boolean immediate) {
            this.endAnimation();
            float end = select ? -0.6F : 0.0F;
            if (immediate) {
                this.setFocusLevel(end);
                this.setTranslateXLevel(this.mBaseScale, this.mScale, 1f, end);
                this.setTranslateYLevel(this.mBaseScale, this.mScale, 1f, end);
            } else if (this.mFocusLevel != end) {
                this.mFocusLevelStart = this.mFocusLevel;
                this.mFocusLevelDelta = end - this.mFocusLevelStart;
                this.mAnimator.start();
            }
        }

        void setFocusLevel(float level) {
            setFocusLevel(1.f, level);
        }

        void setFocusLevel(float scaleStart, float level) {
            this.mFocusLevel = level;
            float scale = scaleStart + this.mScaleDiff * level;
            this.mView.setScaleX(scale);
            this.mView.setScaleY(scale);
            if (this.mWrapper != null) {
                this.mWrapper.setShadowFocusLevel(level);
            } else {
                ShadowOverlayHelper.setNoneWrapperShadowFocusLevel(this.mView, level);
            }

            if (this.mDimmer != null) {
                this.mDimmer.setActiveLevel(level);
                int color = this.mDimmer.getPaint().getColor();
                if (this.mWrapper != null) {
                    this.mWrapper.setOverlayColor(color);
                } else {
                    ShadowOverlayHelper.setNoneWrapperOverlayColor(this.mView, color);
                }
            }
        }

        void setTranslateXLevel(float baseScale, float selectedScale, float translate, float level) {
            this.mFocusLevel = level;
            if (mGravity == Gravity.LEFT) {
                float scale = translate + (calculatedScaleRate(10, baseScale, selectedScale) * level);
                this.mView.setTranslationX(scale);
            } else if (mGravity == Gravity.RIGHT) {
                float scale = translate + (calculatedScaleRate(20, baseScale, selectedScale) * level);
                this.mView.setTranslationX(-scale);
            } else if (mGravity == Gravity.BOTTOM) {
                float scale = translate + (calculatedScaleRate(22, baseScale, selectedScale) * level);
                this.mView.setTranslationX(-scale);
            }
        }

        private float calculatedScaleRate(float value, float baseScale, float selectedScale) {
            if (Log.INCLUDE) {
                Log.d(TAG, "calculatedScaleRate | value : " + value);
                Log.d(TAG, "calculatedScaleRate | baseScale : " + baseScale);
                Log.d(TAG, "calculatedScaleRate | selectedScale :" + selectedScale);
            }
            float valueDiff = Math.abs(value - (value * (selectedScale / baseScale)));
            return value - (valueDiff * 6);
        }

        void setTranslateYLevel(float baseScale, float selectedScale, float translate, float level) {
            this.mFocusLevel = level;
            if (Log.INCLUDE) {
                Log.d(TAG, "scale : " + mScale);
                Log.d(TAG, "mScaleDiff : " + mScaleDiff);
            }
            if (mGravity == Gravity.LEFT) {
                float scale = translate + (calculatedScaleRate(10, baseScale, selectedScale) * level);
                this.mView.setTranslationY(scale);
            } else if (mGravity == Gravity.RIGHT) {
                float scale = translate + (calculatedScaleRate(10, baseScale, selectedScale) * level);
                this.mView.setTranslationY(scale);
            } else if (mGravity == Gravity.BOTTOM) {
                float scale = translate + (calculatedScaleRate(30, baseScale, selectedScale) * level);
                this.mView.setTranslationY(-scale);
            }
        }

        float getFocusLevel() {
            return this.mFocusLevel;
        }

        void endAnimation() {
            this.mAnimator.end();
        }

        public void onTimeUpdate(TimeAnimator animation, long totalTime, long deltaTime) {
            float fraction;
            if (totalTime >= (long) this.mDuration) {
                fraction = 1.0F;
                this.mAnimator.end();
            } else {
                fraction = (float) ((double) totalTime / (double) this.mDuration);
            }

            if (this.mInterpolator != null) {
                fraction = this.mInterpolator.getInterpolation(fraction);
            }

            this.setFocusLevel(this.mFocusLevelStart + fraction * this.mFocusLevelDelta);
            this.setTranslateXLevel(this.mBaseScale, this.mScale, 1f, this.mFocusLevelStart + fraction * this.mFocusLevelDelta);
            this.setTranslateYLevel(this.mBaseScale, this.mScale, 1f, this.mFocusLevelStart + fraction * this.mFocusLevelDelta);
        }
    }

}
