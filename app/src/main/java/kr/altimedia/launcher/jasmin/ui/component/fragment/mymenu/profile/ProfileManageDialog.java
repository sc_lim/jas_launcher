/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfile;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfileBuilder;

public class ProfileManageDialog extends SafeDismissDialogFragment implements View.OnUnhandledKeyEventListener{
    public static final String CLASS_NAME = ProfileManageDialog.class.getName();
    private static final String TAG = ProfileManageDialog.class.getSimpleName();

    public static final String ACTION_MY_PROFILE_ADDED = "kr.altimedia.intent.action.My.Profile.Added";
    public static final String ACTION_MY_PROFILE_UPDATED = "kr.altimedia.intent.action.My.Profile.Updated";
    public static final String ACTION_MY_PROFILE_DELETED = "kr.altimedia.intent.action.My.Profile.Deleted";
    public static final String KEY_PROFILE_ID = "KEY_PROFILE_ID";
    public static final String KEY_PROFILE_NAME = "KEY_PROFILE_NAME";
    public static final String KEY_PROFILE_ICON_ID = "KEY_PROFILE_ICON_ID";
    public static final String KEY_PROFILE_ICON_PATH = "KEY_PROFILE_ICON_PATH";

    public static final Integer MAX_PROFILE_SIZE = new Integer(6);

    public static final String KEY_DISABLE_HOT_KEY = "KEY_DISABLE_HOT_KEY";
    public static final String KEY_VIEW_TYPE = "KEY_VIEW_TYPE";
    public static final String KEY_PROFILE = "KEY_PROFILE";
    public static final String KEY_PROFILE_LIST = "KEY_PROFILE_LIST";
    public static final String KEY_PROFILE_UPDATED = "KEY_PROFILE_UPDATED";
    public static final String KEY_PROFILE_SIZE = "KEY_PROFILE_SIZE";
    public static final String KEY_PROFILE_INFO_LIST = "KEY_PROFILE_INFO_LIST";
    public static final String KEY_LINK_FROM_MY_MENU = "KEY_LINK_FROM_MY_MENU";

    public static final Integer TYPE_LIST = 0;
    public static final Integer TYPE_ADD = 1;
    public static final Integer TYPE_CHANGE = 2;
    public static final Integer TYPE_DELETE = 3;
    public static final Integer TYPE_EDIT = 4;
    public static final Integer TYPE_DELETE_ERROR = 5;

    private boolean created = false;

    private int currViewType;
    private int prevViewType;
    private int newViewType;
    private Fragment currentFragment;
    private String fragmentClassName;
    private View rootView;

    private ProfileUpdateListener profileUpdateListener;
    private boolean disableHotKey = false;
    private boolean linkFromMyMenu = false;

    ProfileManageDialog() {
        created = true;
    }

    public static ProfileManageDialog newInstance(int type, ArrayList<UserProfile> profileList) {
        if (Log.INCLUDE) {
            Log.d(TAG, "newInstance: type="+type);
        }
        Bundle args = new Bundle();
        ProfileManageDialog fragment = new ProfileManageDialog();
        args.putInt(KEY_VIEW_TYPE, type);
        args.putParcelableArrayList(KEY_PROFILE_LIST, profileList);
        args.putBoolean(KEY_DISABLE_HOT_KEY, false);
        fragment.setArguments(args);
        return fragment;
    }

    public static ProfileManageDialog newInstance(int type, ArrayList<UserProfile> profileList, UserProfile profile) {
        if (Log.INCLUDE) {
            Log.d(TAG, "newInstance: type="+type+", profile="+profile);
        }
        Bundle args = new Bundle();
        ProfileManageDialog fragment = new ProfileManageDialog();
        args.putInt(KEY_VIEW_TYPE, type);
        args.putParcelableArrayList(KEY_PROFILE_LIST, profileList);
        args.putParcelable(KEY_PROFILE, profile);
        args.putBoolean(KEY_DISABLE_HOT_KEY, false);
        fragment.setArguments(args);
        return fragment;
    }

    public static ProfileManageDialog newInstance(Context context, int type, ArrayList<ProfileInfo> profileList, boolean disableHotKey) {
        if (Log.INCLUDE) {
            Log.d(TAG, "newInstance: type="+type+", disableHotKey="+disableHotKey);
        }
        Bundle args = new Bundle();
        ProfileManageDialog fragment = new ProfileManageDialog();
        args.putInt(KEY_VIEW_TYPE, type);

        ArrayList<UserProfile> tmpList = new ArrayList<>();
        try {
            for (ProfileInfo profileInfo : profileList) {
                tmpList.add(UserProfileBuilder.createUserProfile(profileInfo));
            }
            if (!UserProfileBuilder.checkProfileOptionIncluded(tmpList, UserProfileBuilder.TYPE_ADD_OPTION)) {
                tmpList.add(UserProfileBuilder.createAddOption(context.getResources()));
            }
            if(type == TYPE_EDIT && profileList.size() == 1) {
                args.putParcelable(KEY_PROFILE, (UserProfile)tmpList.get(0));
            }
        }catch (Exception e){
        }
        args.putParcelableArrayList(ProfileManageDialog.KEY_PROFILE_LIST, tmpList);
        args.putBoolean(KEY_DISABLE_HOT_KEY, disableHotKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        if(disableHotKey){
            return false;
        }

        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new Dialog(requireContext(), getTheme()) {
            @Override
            public void onBackPressed() {
                boolean available = backToFragment();
                if (Log.INCLUDE) {
                    Log.d(TAG, "onBackPressed: back fragment available=" + available);
                }
                if (!available) {
                    super.onBackPressed();
                }
            }
        };
    }

    public boolean backToFragment() {
        FragmentManager manager = getChildFragmentManager();
        int stackCount = manager.getBackStackEntryCount();
        if (Log.INCLUDE) {
            Log.d(TAG, "backToFragment: back fragment size=" + stackCount);
        }
        if (stackCount > 0) {
            manager.popBackStack();
            return true;
        }
        return false;
    }

    public void removeAllBackFragments() {
        getChildFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_mymenu_common, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        Bundle arguments = getArguments();
        int type = TYPE_LIST;
        ArrayList profileList = null;
        disableHotKey = false;
        linkFromMyMenu = false;
        UserProfile profile = null;
        if (arguments != null) {
            type = arguments.getInt(KEY_VIEW_TYPE);
            disableHotKey = arguments.getBoolean(KEY_DISABLE_HOT_KEY);
            profileList = arguments.getParcelableArrayList(ProfileManageDialog.KEY_PROFILE_LIST);
            if(type == TYPE_EDIT){
                profile = arguments.getParcelable(KEY_PROFILE);
                linkFromMyMenu = true;
            }else if(type == TYPE_ADD){
                linkFromMyMenu = true;
            }
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated: type=" + type + ", linkFromMyMenu=" + linkFromMyMenu);
        }
        showProfileView(type, profile, profileList);
        showFragment(type);
        created = false;
    }

    private void showFragment(int type) {
        prevViewType = currViewType;
        currViewType = type;
        if (Log.INCLUDE) {
            Log.d(TAG, "showFragment: currViewType=" + currViewType + ", prevViewType=" + prevViewType);
        }
        if(this.currentFragment == null){
            return;
        }
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.mymenu_container, currentFragment, fragmentClassName);
        addToBackStack(ft);
        ft.commit();
    }

    private void addToBackStack(FragmentTransaction ft) {
        if (Log.INCLUDE) {
            Log.d(TAG, "addToBackStack: currViewType=" + currViewType + ", prevViewType=" + prevViewType);
        }
        if (!created) {
            ft.addToBackStack(null);
        }
    }

    public void hideProfileView() {
        if (Log.INCLUDE) {
            Log.d(TAG, "hideProfileView");
        }
        removeAllBackFragments();
        this.dismiss();
    }

    public void hideProfileView(String profileName) {
        if (Log.INCLUDE) {
            Log.d(TAG, "hideProfileView: currViewType=" + currViewType+", profileName="+profileName);
        }
        hideProfileView();

        if(profileUpdateListener != null){
            profileUpdateListener.onUpdated(profileName);
        }
    }

    public void showProfileView(int type, UserProfile profile, ArrayList<UserProfile> profileList) {
        newViewType = type;
        if (Log.INCLUDE) {
            Log.d(TAG, "showProfileView: newViewType=" + newViewType);
        }
        if (type == TYPE_LIST) {
            setProfileList(profileList, profile, false);
        } else if (type == TYPE_ADD) {
            setProfileAdd(profileList);
        } else if (type == TYPE_CHANGE) {
            setProfileChange(profile);
        } else if (type == TYPE_DELETE) {
            setProfileDelete(profile);
        } else if (type == TYPE_EDIT) {
            setProfileEdit(profile, (profileList != null ? profileList.size() : 0));
        } else if (type == TYPE_DELETE_ERROR) {
            setProfileDeleteError();
        }

        if (!created) {
            showFragment(type);
        }
    }

    private void setProfileList(ArrayList<UserProfile> profileList, UserProfile profile, boolean updated) {
        this.currentFragment = ProfileListFragment.newInstance(profileList, profile, updated);
        this.fragmentClassName = ProfileListFragment.CLASS_NAME;
    }

    public void setProfileAdd(ArrayList<UserProfile> profileList) {
        this.currentFragment = ProfileAddFragment.newInstance(profileList, disableHotKey);
        this.fragmentClassName = ProfileAddFragment.CLASS_NAME;
    }

    private void setProfileChange(UserProfile profile) {
        this.currentFragment = ProfileChangeFragment.newInstance(profile, disableHotKey);
        this.fragmentClassName = ProfileChangeFragment.CLASS_NAME;
    }

    private void setProfileDelete(UserProfile profile) {
        this.currentFragment = ProfileDeleteFragment.newInstance(profile);
        this.fragmentClassName = ProfileDeleteFragment.CLASS_NAME;
    }

    private void setProfileEdit(UserProfile profile, int profileSize) {
        this.currentFragment = ProfileEditFragment.newInstance(profile, profileSize);
        this.fragmentClassName = ProfileEditFragment.CLASS_NAME;
    }

    private void setProfileDeleteError() {
        this.currentFragment = ProfileDeleteErrorFragment.newInstance();
        this.fragmentClassName = ProfileDeleteErrorFragment.CLASS_NAME;
    }

    public boolean isLinkFromMyMenu(){
        return linkFromMyMenu;
    }

    public ProfileUpdateListener getProfileUpdateListener(){
        return profileUpdateListener;
    }

    public void setProfileUpdateListener(ProfileUpdateListener listener){
        profileUpdateListener = listener;
    }

    public interface ProfileUpdateListener{
        void onUpdated(String profileNewName);
    }
}
