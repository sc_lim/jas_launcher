
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.search.obj;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchOption implements Parcelable {
    public static final Creator<SearchOption> CREATOR = new Creator<SearchOption>() {
        @Override
        public SearchOption createFromParcel(Parcel in) {
            return new SearchOption(in);
        }

        @Override
        public SearchOption[] newArray(int size) {
            return new SearchOption[size];
        }
    };
    @Expose
    @SerializedName("SRCH_OPT")
    private String srchOpt;

    protected SearchOption(Parcel in) {
        srchOpt = in.readString();
    }

    public String getSrchOpt() {
        return srchOpt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(srchOpt);
    }
}