/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import kr.altimedia.launcher.jasmin.R;

public class MenuItem implements Parcelable {

    @SerializedName("type")
    @Expose
    private MenuType type;

    @SerializedName("title")
    private int title;

    public final static Creator<MenuItem> CREATOR = new Creator<MenuItem>() {
        @SuppressWarnings({
                "unchecked"
        })
        public MenuItem createFromParcel(Parcel in) {
            return new MenuItem(in);
        }

        public MenuItem[] newArray(int size) {
            return (new MenuItem[size]);
        }
    };

    protected MenuItem(Parcel in) {
        this.type = ((MenuType) in.readValue((MenuType.class.getClassLoader())));
        this.title = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public MenuItem() {
    }

    public MenuItem(MenuType type) {
        this.type = type;
    }

    public MenuType getType() {
        return type;
    }

    public int getTitle() {
        return title;
    }

    public int getIcon() {
        switch (type) {
            case PURCHASED:
                return R.drawable.my_purchased;
            case COLLECTION:
                return R.drawable.my_collection;
            case SUBSCRIPTION:
                return R.drawable.my_subscrip;
            case REMINDER:
                return R.drawable.my_reminder;
            case COUPON:
                return R.drawable.my_coupon;
        }
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeValue(type);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuItem menu_ = (MenuItem) o;
        return type == menu_.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "type='" + type + '\'' +
                ", title=" + title +
                '}';
    }
}