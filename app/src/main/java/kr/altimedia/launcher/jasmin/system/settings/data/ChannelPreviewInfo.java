/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.system.settings.data;

import android.text.format.DateFormat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Map;
import java.util.Set;

import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.AppConfig;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

import com.altimedia.util.Log;

public class ChannelPreviewInfo {
    private static final String TAG = ChannelPreviewInfo.class.getSimpleName();

    private static final long TIME_NOT_SET = -1;
    private long previewTime;
    private long previewResetTime;
    private Map<String, PreviewInfo> map;

    private static final boolean TEST_PREVIEW = AppConfig.TEST_CHANNEL_PREVIEW;

    public static class PreviewInfo {
        private long timesLeft;
        private long resetTime;

        public PreviewInfo(long timesLeft, long resetTime) {
            this.timesLeft = timesLeft;
            this.resetTime = resetTime;
        }

        public long getPreviewLeftTime(long previewTime, long previewResetTime) {
            if (timesLeft > 0) {
                if (timesLeft > previewTime) {
                    timesLeft = previewTime;
                    if (Log.INCLUDE) {
                        Log.d(TAG, "getPreviewLeftTime() 1.timesLeft corrected");
                    }
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "getPreviewLeftTime() 1.timesLeft:" + timesLeft);
                }
                return timesLeft;
            } else if (resetTime < JasmineEpgApplication.SystemClock().currentTimeMillis()) {
                //reset timesLeft
                timesLeft = previewTime;
                resetTime = TIME_NOT_SET;
                if (Log.INCLUDE) {
                    Log.d(TAG, "getPreviewLeftTime() 2.timesLeft:" + timesLeft);
                }
                return timesLeft;
            } else {
                if (resetTime > JasmineEpgApplication.SystemClock().currentTimeMillis() + previewResetTime) {
                    resetTime = JasmineEpgApplication.SystemClock().currentTimeMillis() + previewResetTime;
                    if (Log.INCLUDE) {
                        Log.d(TAG, "getPreviewLeftTime() 3.resetTime corrected");
                    }
                }

                if (Log.INCLUDE) {
                    Log.d(TAG, "getPreviewLeftTime() 3.timesLeft:" + 0 + ", resetTime:" + DateFormat.format("HH:mm:ss(MM/dd)", resetTime) + ", remains:"+getHHMMSS(resetTime-JasmineEpgApplication.SystemClock().currentTimeMillis()));
                }
                return 0;
            }
        }

        public void setPreviewLeftTime(long leftTime, long previewResetTime) {
            if (Log.INCLUDE) {
                Log.d(TAG, "setPreviewLeftTime() leftTime:" + leftTime);
            }
            if (leftTime > 0) {
                timesLeft = leftTime;
            } else {
                timesLeft = 0;
                this.resetTime = JasmineEpgApplication.SystemClock().currentTimeMillis() + previewResetTime;
            }
        }

        @Override
        public String toString() {
            return "PreviewInfo["
                    + "timesLeft:" + getHHMMSS(timesLeft)
                    + ", resetTime:" + (resetTime == TIME_NOT_SET ? TIME_NOT_SET : DateFormat.format("HH:mm:ss(MM/dd)", resetTime))
                    + ", now:" + DateFormat.format("HH:mm:ss(MM/dd)", JasmineEpgApplication.SystemClock().currentTimeMillis())
                    + "]";
        }
    }

    public ChannelPreviewInfo(String json) {
        long previewMinutes = AccountManager.getInstance().getPreviewTime();
        long previewResetMinutes = AccountManager.getInstance().getPreviewResetTime();
        if (Log.INCLUDE) {
            Log.d(TAG, "ChannelPreviewInfo() loginInfo.previewTime: " + previewMinutes + " minutes, loginInfo.resetTime: " + previewResetMinutes + " minutes");
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "ChannelPreviewInfo() now " + DateFormat.format("HH:mm:ss(MM/dd)", JasmineEpgApplication.SystemClock().currentTimeMillis()) + ", isTimeSet:" + JasmineEpgApplication.SystemClock().isTimeSet());
        }

        if (!TEST_PREVIEW) {
            previewTime = previewMinutes * TimeUtil.MIN;
            previewResetTime = previewResetMinutes * TimeUtil.MIN;
        } else {
            //for preview test
            previewTime = 30*1000L;
            previewResetTime = 10*1000L;
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "ChannelPreviewInfo() previewTime(HHMMSS): " + getHHMMSS(previewTime) + ", resetTime(HHMMSS): " + getHHMMSS(previewResetTime) + (TEST_PREVIEW ? " (for test)" : ""));
        }

        map = getDataFromJson(json);

        if (Log.INCLUDE) {
            Log.d(TAG, "ChannelPreviewInfo() loaded : "+this);
        }
    }

    private Map<String, PreviewInfo> getDataFromJson(String json) {
        if (Log.INCLUDE) {
            Log.d(TAG, "getDataFromJson() json:"+json);
        }
        Map<String, PreviewInfo> map = new Gson().fromJson(json, new TypeToken<Map<String, PreviewInfo>>() {}.getType());
        return map;
    }

    public String getDataAsJson() {
        String json = new Gson().toJson(map);
        if (Log.INCLUDE) {
            Log.d(TAG, "getDataAsJson() json:"+json);
        }
        return json;
    }

    private PreviewInfo getPreviewInfo(String serviceId) {
        PreviewInfo previewInfo = map.get(serviceId);
        if (previewInfo == null) {
            previewInfo = new PreviewInfo(previewTime, TIME_NOT_SET);
            map.put(serviceId, previewInfo);
        }
        return previewInfo;
    }

    public boolean isPreviewAvailable(String serviceId) {
        long timesLeft = getPreviewLeftTime(serviceId);
        return timesLeft > 0;
    }

    public long getPreviewLeftTime(String serviceId) {
        return getPreviewInfo(serviceId).getPreviewLeftTime(previewTime, previewResetTime);
    }

    public void setPreviewLeftTime(String serviceId, long leftTime) {
        getPreviewInfo(serviceId).setPreviewLeftTime(leftTime, previewResetTime);
    }

    @Override
    public String toString() {
        return "ChannelPreviewInfo["
                + "previewTime:" + getHHMMSS(previewTime)
                + ", previewResetTime:" + getHHMMSS(previewResetTime)
                + ", map:" + toString(map)
                + "]";
    }

    public static String toString(Map map) {
        if (map == null) {
            return null;
        } else {
            String str = "{";
            for (String key : (Set<String>)map.keySet()) {
                str += (key+":"+map.get(key)+",");
            }
            return str+"}";
        }
    }

    public static final String getHHMMSS(long time) {
        if (time == -1) {
            return "TIME_NOT_SET";
        }
        long sec = time / 1000L;
        long min = sec / 60;
        long hour = min / 60;

        int hh = (int) hour;
        int mm = (int) min % 60;
        int ss = (int) sec % 60;

        return String.format("%02d:%02d:%02d", hh, mm, ss);
    }
}
