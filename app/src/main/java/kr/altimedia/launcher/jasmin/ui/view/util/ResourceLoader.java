/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 26,05,2020
 */
public class ResourceLoader<K> {
    private final String TAG = ResourceLoader.class.getCanonicalName();

    public ResourceLoader() {
    }

    public void loadResource(Fragment fragment, K key, ResourceCallback callback) {
        if (fragment == null) {
            return;
        }
        if (!fragment.isAdded()) {
            return;
        }
        if (key instanceof String) {
            Glide.with(fragment).asDrawable().load(key).placeholder(R.drawable.topbanner_defaulte).
                    error(R.drawable.topbanner_defaulte).diskCacheStrategy(DiskCacheStrategy.DATA).into(new CustomTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onResourceReady");
                    }
                    callback.onLoaded(resource);
                }

                @Override
                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                    super.onLoadFailed(errorDrawable);
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onLoadFailed");
                    }
                    callback.onLoaded(errorDrawable);
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {
                }

                @Override
                public void onStop() {
                    super.onStop();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onStop");
                    }
                }

                @Override
                public void onDestroy() {
                    super.onDestroy();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onDestroy");
                    }
                }
            });
        } else if (key instanceof Integer) {
            callback.onLoaded(fragment.getResources().getDrawable((Integer) key, null));
        }
    }


    public void loadResource(Context context, ResourceListCallback callback, K... key) {
        int size = key.length;
        List<Pair<String, Drawable>> resourceList = new ArrayList<>();
        for (K url : key) {
            String targetUrl = (String) url;
            loadResource(context, targetUrl, new ResourceCallback() {
                @Override
                public void onLoaded(Drawable drawable) {
                    resourceList.add(new Pair<>(targetUrl, drawable));
                    if (resourceList.size() == size) {
                        callback.onLoaded(resourceList);
                    }
                }
            });
        }
    }

    private void loadResource(Context context, final String targetUrl, ResourceCallback callback) {
        Glide.with(context).asDrawable().load(targetUrl).
                diskCacheStrategy(DiskCacheStrategy.DATA).into(new CustomTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onResourceReady");
                }
                callback.onLoaded(resource);
            }

            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onLoadFailed");
                }
                callback.onLoaded(errorDrawable);

            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {
                callback.onLoaded(null);
            }
        });
    }


    public interface ResourceCallback {
        void onLoaded(Drawable drawable);
    }

    public interface ResourceListCallback {
        void onLoaded(List<Pair<String, Drawable>> loadResource);
    }
}
