/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.BonusType;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProduct;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class CouponPaymentSuccessFragment extends CouponPaymentBaseFragment {
    public static final String CLASS_NAME = CouponPaymentSuccessFragment.class.getName();
    private static final String TAG = CouponPaymentSuccessFragment.class.getSimpleName();

    private static final String KEY_COUPON = "COUPON";

    public static CouponPaymentSuccessFragment newInstance(CouponProduct couponItem) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_COUPON, couponItem);

        CouponPaymentSuccessFragment fragment = new CouponPaymentSuccessFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_coupon_shop_payment_success;
    }

    @Override
    protected void initView(View view) {
        initLayout(view);
    }

    private void initLayout(View view) {
        CouponProduct couponItem = getArguments().getParcelable(KEY_COUPON);

        TextView couponPrice = view.findViewById(R.id.couponPrice);
        LinearLayout bonusValueLayer = view.findViewById(R.id.bonusValueLayer);
        TextView bonusValue = view.findViewById(R.id.bonusValue);
        TextView bonusUnit = view.findViewById(R.id.bonusUnit);

        couponPrice.setText(StringUtil.getFormattedNumber(couponItem.getPrice()));
        if(couponItem.getBonusValue() > 0 && (couponItem.getBonusType() == BonusType.DC_RATE || couponItem.getBonusType() == BonusType.DC_AMOUNT)) {
            bonusValue.setText(StringUtil.getFormattedNumber(couponItem.getBonusValue()));
            if (couponItem.getBonusType() == BonusType.DC_RATE) {
                bonusUnit.setText("%");
            } else if (couponItem.getBonusType() == BonusType.DC_AMOUNT) {
                bonusUnit.setText(R.string.thb);
            }
            bonusValueLayer.setVisibility(View.VISIBLE);
        }else{
            bonusValueLayer.setVisibility(View.GONE);
        }

        TextView btnOk = view.findViewById(R.id.btnOk);
        btnOk.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                    case KeyEvent.KEYCODE_BACK:
                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().gotoHistoryListBySuccess();
                        }
                        break;
                }
                return false;
            }
        });
    }
}
