/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.object.SideOptionCategory;
import kr.altimedia.launcher.jasmin.ui.view.row.SideOptionSectionRow;

/**
 * Created by mc.kim on 06,04,2020
 */
public class SideOptionSectionPresenter extends Presenter {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_side_option_section, parent, false);
        return new ViewHolder(view);
    }

    public SideOptionSectionPresenter() {
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        View parentsView = viewHolder.view;
        TextView keyText = parentsView.findViewById(R.id.key);
        if (item instanceof SideOptionSectionRow) {
            SideOptionSectionRow sectionRow = (SideOptionSectionRow) item;
            SideOptionCategory type = sectionRow.getOptionType();
            keyText.setText(sectionRow.getOptionType().getOptionName());
        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {

    }
}
