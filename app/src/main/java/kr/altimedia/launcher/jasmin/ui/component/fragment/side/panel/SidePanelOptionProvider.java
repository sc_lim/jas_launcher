/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel;

/**
 * Created by mc.kim on 25,05,2020
 */
public abstract class SidePanelOptionProvider<T> {

    public static final int KEY_SUBTITLE = 0;
    public static final int KEY_AUDIO_LIST = 1;
    public static final int KEY_EXTRAS = 2;

    private final T item;

    public SidePanelOptionProvider(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public abstract Object getData(int key);

}
