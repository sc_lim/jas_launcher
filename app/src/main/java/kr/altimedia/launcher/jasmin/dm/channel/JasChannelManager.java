/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.dm.channel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.util.Log;

import java.util.List;
import java.util.function.Consumer;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.external.proxy.multiview.data.ChannelInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.system.settings.data.ChannelPreviewInfo;
import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;
import kr.altimedia.launcher.jasmin.tv.ParentalController;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_FAVORITE_CHANNEL;

public class JasChannelManager {
    private static final String TAG = JasChannelManager.class.getSimpleName();
    private static final boolean DEBUG = Log.INCLUDE;

    private static final JasChannelManager INSTANCE = new JasChannelManager();

    private final MbsDataHandler mMbsDataHandler = new MbsDataHandler();

    private AccountManager mAccountManager;
    private ChannelDataManager mChannelDataManager;
    private MbsChannelDataManager mMbsChannelDataManager;
    private UserDataManager mUserDataManager;
    private UserPreferenceManagerImp mUserPreferenceManager;
    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Log.INCLUDE) {
                Log.d(TAG, "BroadcastReceiver.onReceive() intent:" + intent);
            }
            String action = intent.getAction();
            if (PushMessageReceiver.ACTION_UPDATED_PROFILE.equalsIgnoreCase(action)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "BroadcastReceiver.onReceive() blocked channel updated...");
                }
                Object type = intent.getSerializableExtra(PushMessageReceiver.KEY_TYPE);
                if (Log.INCLUDE) {
                    Log.d(TAG, "BroadcastReceiver.onReceive(), type : " + type);
                }
                if (type == null) {
                    return;
                }
                PushMessageReceiver.ProfileUpdateType updateType = (PushMessageReceiver.ProfileUpdateType) type;
                if (updateType == PushMessageReceiver.ProfileUpdateType.blockedChannel ||
                        updateType == PushMessageReceiver.ProfileUpdateType.login) {
                    updateFavoriteChannelList();
                    updateBlockedChannelList();
                    ParentalController.updateParentalRating();
                } else if (updateType == PushMessageReceiver.ProfileUpdateType.rating) {
                    boolean currentUserChanged = intent.getBooleanExtra(PushMessageReceiver.KEY_CHANGED_LOGIN_USER_INFO, false);
                    if (Log.INCLUDE) {
                        Log.d(TAG, "BroadcastReceiver.onReceive() update rating : " + currentUserChanged);
                    }
                    if (currentUserChanged) {
                        ParentalController.updateParentalRating();
                    }
                }
            } else if (PushMessageReceiver.ACTION_UPDATED_FAVORITE_CHANNEL.equalsIgnoreCase(action)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "BroadcastReceiver.onReceive() favorite channel updated...");
                }
                updateFavoriteChannelList();
            } else if (PushMessageReceiver.ACTION_UPDATED_SUBSCRIBED_CHANNEL.equalsIgnoreCase(action)) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "BroadcastReceiver.onReceive() subscribed channel updated...");
                }
                updateSubscribedChannelList();
            } else {
                if (Log.INCLUDE) {
                    Log.d(TAG, "BroadcastReceiver.onReceive() undefined action received...");
                }
            }
        }
    };

    private ChannelPreviewInfo mChannelPreviewInfo;

    private boolean mIsInitialized = false;

    private JasChannelManager() {
    }

    public static JasChannelManager getInstance() {
        return INSTANCE;
    }

    public void init(Context context, ChannelDataManager channelDataManager) {
        if (Log.INCLUDE) {
            Log.d(TAG, "init()");
        }

        setIsInitialized(false);
        mChannelDataManager = channelDataManager;
        mAccountManager = AccountManager.getInstance();
        mUserPreferenceManager = new UserPreferenceManagerImp(context);
    }

    public void initMBS(boolean loginSuccess) {
        if (Log.INCLUDE) {
            Log.d(TAG, "initMBS() loginSuccess:"+loginSuccess);
            Log.d(TAG, "initMBS() accountManager, said:[" + mAccountManager.getSaId() + "], profileId:[" + mAccountManager.getProfileId() + "]");
        }

        initChannelUserPreference(mUserPreferenceManager);

        //initialize channel
        initMBSChannelTask(loginSuccess);
    }

    public void registerJasChannelReceiver(@NonNull Context context) {
        Log.d(TAG, "registerJasChannelReceiver() register broadcast receiver");
        //init broadcastReceiver
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PushMessageReceiver.ACTION_UPDATED_PROFILE);
        intentFilter.addAction(PushMessageReceiver.ACTION_UPDATED_FAVORITE_CHANNEL);
        intentFilter.addAction(PushMessageReceiver.ACTION_UPDATED_SUBSCRIBED_CHANNEL);
        LocalBroadcastManager.getInstance(context).registerReceiver(mBroadcastReceiver, intentFilter);
    }

    public void unregisterJasChannelReceiver(@NonNull Context context) {
        Log.d(TAG, "unregisterJasChannelReceiver() unregister broadcast receiver");
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mBroadcastReceiver);
    }

    public void updateChannelList() {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateChannelList()");
        }
        initMBSChannelTask(true);
    }

    public boolean isIsInitialized() {
        return mIsInitialized;
    }

    public void setIsInitialized(boolean isInitialized) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setIsInitialized : " + isInitialized);
        }
        if (mIsInitialized == isInitialized) {
            if (Log.INCLUDE) {
                Log.d(TAG, "return reason : " + (mIsInitialized == isInitialized));
            }
            return;
        }
        mIsInitialized = isInitialized;
        if (mIsInitialized) {
            mChannelDataManager.notifyUserBasedChannelListUpdated();
        }
    }

    public boolean isLiveTvReady(boolean containUserData) {
        if (DEBUG)
            Log.d(TAG, "isLiveTvReady() return : " + (isIsInitialized() && mChannelDataManager.getBrowsableChannelList().size() > 0));

        if (containUserData) {
            return isIsInitialized() && mChannelDataManager.getBrowsableChannelList().size() > 0;
        } else {
            return mChannelDataManager.getBrowsableChannelList().size() > 0;
        }
    }

    private void initChannelUserPreference(UserPreferenceManagerImp userPreferenceManagerImp) {
        int size = mChannelDataManager.getBrowsableChannelList().size();
        if (DEBUG) Log.d(TAG, "initChannelUserPreference() ch.size:"+size);

        //init Last Watched Channel
        String serviceId = userPreferenceManagerImp.getLastWatchedChannel();
        Channel channel = mChannelDataManager.getChannelByServiceId(serviceId);
        if (DEBUG)
            Log.d(TAG, "initChannelUserPreference() up.getLastWatchedChannel(), serviceId:" + serviceId + ", channel:" + channel);
        mChannelDataManager.setLastWatchedChannel(channel);

        //init Channel Preview Info
        mChannelPreviewInfo = userPreferenceManagerImp.getChannelPreviewInfo();
        if (DEBUG)
            Log.d(TAG, "initChannelUserPreference() mChannelPreviewInfo:" + mChannelPreviewInfo);
    }

    public Channel getLastWatchedChannel() {
        return mChannelDataManager.getLastWatchedChannel();
    }

    public Channel getChannel(String serviceId) {
        return mChannelDataManager.getChannelByServiceId(serviceId);
    }

    public void setLastWatchedChannel(Channel channel) {
        mUserPreferenceManager.setLastWatchedChannel((channel != null ? channel.getServiceId() : null));
        mChannelDataManager.setLastWatchedChannel(channel);
    }

    public boolean isPreviewAvailable(Channel channel) {
        if (channel != null && channel.isPaidChannel() && !channel.isAdultChannel()) {
            return mChannelPreviewInfo.isPreviewAvailable(channel.getServiceId());
        }
        return false;
    }

    public long getPreviewLeftTime(Channel channel) {
        if (channel != null && channel.isPaidChannel()) {
            return mChannelPreviewInfo.getPreviewLeftTime(channel.getServiceId());
        }
        return 0;
    }

    public void updatePreviewTime(String serviceId, long leftTime, boolean writeUP) {
        mChannelPreviewInfo.setPreviewLeftTime(serviceId, leftTime);
        if (DEBUG) Log.d(TAG, "updatePreviewTime() writeUP:"+writeUP+", mChannelPreviewInfo:" + mChannelPreviewInfo);
        if (writeUP) {
            mUserPreferenceManager.setChannelPreviewInfo(mChannelPreviewInfo);
        }
    }

    private void initMBSChannelTask(boolean loginSuccess) {
        if (DEBUG) Log.d(TAG, "initMBSChannelTask()");

        if (mUserDataManager == null) {
            mUserDataManager = new UserDataManager();
        }
        if (mMbsChannelDataManager == null) {
            mMbsChannelDataManager = new MbsChannelDataManager();
        }

        if (loginSuccess) {
            updateFavoriteChannelList();
            updateBlockedChannelList();
            updateSubscribedChannelList();

            ParentalController.updateParentalRating();
        }

        setIsInitialized(true);
    }

    public ChannelInfo[] getChannelInfo() {
        List<Channel> list = mChannelDataManager.getBrowsableChannelList();
        ChannelInfo[] channelInfos = new ChannelInfo[list.size()];
        Channel ch;
        for (int i = 0; list != null && i < list.size(); i++) {
            ch = list.get(i);
            channelInfos[i] = new ChannelInfo(ch.getId(),
                    ch.getServiceId(),
                    ch.getDisplayNumber(),
                    ch.getDisplayName(),
                    ch.isLocked(),
                    ch.isPaidChannel(),
                    ch.isAudioChannel(),
                    ch.isFavoriteChannel(),
                    new String[]{ch.getLocalUrl(), ch.getCentralUrl()},
                    ch.getGenre()
            );
        }

        return channelInfos;
    }

    public void setFavoriteChannel(Context context, Channel channel, boolean setFavorite) {
        mChannelDataManager.setFavoriteChannel(channel, setFavorite);
        if (setFavorite) {
            String message = context.getString(R.string.added_to_favorite_channel);
            JasminToast.makeToast(context, message);
        } else {
            String message = context.getString(R.string.remove_from_favorite_channel);
            JasminToast.makeToast(context, message);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "putFavoriteChannelList(said:[" + mAccountManager.getSaId() + "], profileId:[" + mAccountManager.getProfileId() + "], serviceId:[" + channel.getServiceId() + "]");
        }

        mMbsDataHandler.putFavoriteChannelList(channel.getServiceId());

        Intent mIntent = new Intent(context, PushMessageReceiver.class);
        mIntent.setAction(ACTION_UPDATED_FAVORITE_CHANNEL);
        LocalBroadcastManager.getInstance(context).sendBroadcast(mIntent);
    }

    private void updateFavoriteChannelList() {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateFavoriteChannelList()");
        }
        mMbsDataHandler.getFavoriteChannelList((List<String> list) -> mChannelDataManager.setFavoriteChannelList(list));
    }

    private void updateBlockedChannelList() {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateBlockedChannelList()");
        }
        mMbsDataHandler.getBlockedChannelList((List<String> list) -> mChannelDataManager.setBlockChannelList(list));
    }

    private void updateSubscribedChannelList() {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateSubscribedChannelList()");
        }
//        //for test
//        ArrayList<String> list = new ArrayList<>();
//        list.add("101");
//        mChannelDataManager.setSubscribedChannelList(list);

        mMbsDataHandler.getSubscribedChannelList((List<String> list) -> mChannelDataManager.setSubscribedChannelList(list));
    }

    private class MbsDataHandler {
        public void showErrorPopup(String errorCode, String message) {
//            String title = activity.getString(R.string.error_title_mbs);
//            ErrorDialogFragment dialogFragment =
//                    ErrorDialogFragment.newInstance(TAG, title, errorCode, message);
//            dialogFragment.show(activity.getSupportFragmentManager(), ErrorDialogFragment.CLASS_NAME);
            if (Log.INCLUDE) {
                Log.d(TAG, "showErrorPopup, errorCode:" + errorCode + ", message:" + message);
            }
        }

        private void putFavoriteChannelList(String channelId) {
            mUserDataManager.putFavoriteChannelList(
                    mAccountManager.getSaId(), mAccountManager.getProfileId(), channelId,
                    new MbsDataProvider<String, Boolean>() {
                        @Override
                        public void needLoading(boolean loading) {
                        }

                        @Override
                        public void onFailed(int key) {
                            Log.d(TAG, "putFavoriteChannelList, response : onFailed, key:" + key);
                        }

                        @Override
                        public void onSuccess(String id, Boolean response) {
                            Log.d(TAG, "putFavoriteChannelList, response, : " + response);
                        }

                        @Override
                        public void onError(String errorCode, String message) {
                            Log.d(TAG, "putFavoriteChannelList, response, onError - errorCode:" + errorCode + ", message:" + message);
                            showErrorPopup(errorCode, message);
                        }
                    });
        }

        private void putBlockedChannelList(Long channelId) {
            mUserDataManager.putBlockedChannelList(
                    mAccountManager.getSaId(), mAccountManager.getProfileId(), channelId,
                    new MbsDataProvider<String, Boolean>() {
                        @Override
                        public void needLoading(boolean loading) {
                        }

                        @Override
                        public void onFailed(int key) {
                            Log.d(TAG, "mPutFavoriteChannelTask, response : onFailed, key:" + key);
                        }

                        @Override
                        public void onSuccess(String id, Boolean response) {
                            Log.d(TAG, "mPutFavoriteChannelTask, response, : " + response);
                        }

                        @Override
                        public void onError(String errorCode, String message) {
                            Log.d(TAG, "mPutFavoriteChannelTask, response, onError - errorCode:" + errorCode + ", message:" + message);
                            showErrorPopup(errorCode, message);
                        }
                    });
        }

        private void getFavoriteChannelList(Consumer<List<String>> consumer) {
            mUserDataManager.getFavoriteChannelList(
                    mAccountManager.getSaId(), mAccountManager.getProfileId(),
                    new MbsDataProvider<String, List<String>>() {
                        @Override
                        public void needLoading(boolean loading) {
                        }

                        @Override
                        public void onFailed(int key) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "getFavoriteChannelList, response : onFailed, key:" + key);
                            }
                        }

                        @Override
                        public void onSuccess(String id, List<String> result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "getFavoriteChannelList, response : list.size:" + result.size());
                                for (int i = 0; result != null && i < result.size(); i++) {
                                    Log.d(TAG, "getFavoriteChannelList, response : list[" + i + "]:" + result.get(i));
                                }
                            }
                            consumer.accept(result);
                        }

                        @Override
                        public void onError(String errorCode, String message) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "getFavoriteChannelList, response, onError - errorCode:" + errorCode + ", message:" + message);
                            }
                            showErrorPopup(errorCode, message);
                        }
                    });
        }

        private void getBlockedChannelList(Consumer<List<String>> consumer) {
            mUserDataManager.getBlockedChannelList(
                    mAccountManager.getSaId(), mAccountManager.getProfileId(),
                    new MbsDataProvider<String, List<String>>() {
                        @Override
                        public void needLoading(boolean loading) {
                        }

                        @Override
                        public void onFailed(int key) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "getBlockedChannelList, response : onFailed, key:" + key);
                            }
                        }

                        @Override
                        public void onSuccess(String id, List<String> result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "getBlockedChannelList, response : list.size:" + result.size());
                                for (int i = 0; result != null && i < result.size(); i++) {
                                    Log.d(TAG, "getBlockedChannelList, response : list[" + i + "]:" + result.get(i));
                                }
                            }
                            consumer.accept(result);
                        }

                        @Override
                        public void onError(String errorCode, String message) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "getBlockedChannelList, response, onError - errorCode:" + errorCode + ", message:" + message);
                            }
                            showErrorPopup(errorCode, message);
                        }
                    });
        }

        private void getSubscribedChannelList(Consumer<List<String>> consumer) {
            mMbsChannelDataManager.getSubscribedChannelList(
                    mAccountManager.getSaId(), mAccountManager.getProfileId(),
                    new MbsDataProvider<String, List<String>>() {
                        @Override
                        public void needLoading(boolean loading) {
                        }

                        @Override
                        public void onFailed(int key) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "getSubscribedChannelList, response : onFailed, key:" + key);
                            }
                        }

                        @Override
                        public void onSuccess(String id, List<String> result) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "getSubscribedChannelList, response : list.size:" + result.size());
                                for (int i = 0; result != null && i < result.size(); i++) {
                                    Log.d(TAG, "getSubscribedChannelList, response : list[" + i + "]:" + result.get(i));
                                }
                            }
                            consumer.accept(result);
                        }

                        @Override
                        public void onError(String errorCode, String message) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "getSubscribedChannelList, response, onError - errorCode:" + errorCode + ", message:" + message);
                            }
                            showErrorPopup(errorCode, message);
                        }
                    });
        }
    }
}
