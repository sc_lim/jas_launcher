package kr.altimedia.launcher.jasmin.dm.adds.api;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by mc.kim on 04,08,2020
 */
public interface ADDSApi {
    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("reportADPlay")
    Call<BaseResponse> reportADPlay(@Query("domainId") String domainId, @Query("trackingId") String trackingId,
                                       @Query("subscriptionId") String subscriptionId, @Query("campaignId") String campaignId,
                                       @Query("userId") String userId,
                                       @Query("regionId") String regionId,
                                       @Query("advPlatformType") String advPlatformType,
                                       @Query("inventoryType") String inventoryType,
                                       @Query("opportunityType") String opportunityType,
                                       @Query("categoryId") String categoryId,
                                       @Query("assetId") String assetId,
                                       @Query("adPlayStartTime") String adPlayStartTime,
                                       @Query("adPlayEndTime") String adPlayEndTime);

}
