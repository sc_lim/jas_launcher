/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.message.obj.response;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.message.obj.Message;

/**
 * Created by mc.kim on 08,05,2020
 */
public class MessageDetailResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private Message result;

    protected MessageDetailResponse(Parcel in) {
        super(in);
        result = in.readTypedObject(Message.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedObject(result, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MessageDetailResponse> CREATOR = new Creator<MessageDetailResponse>() {
        @Override
        public MessageDetailResponse createFromParcel(Parcel in) {
            return new MessageDetailResponse(in);
        }

        @Override
        public MessageDetailResponse[] newArray(int size) {
            return new MessageDetailResponse[size];
        }
    };

    @Override
    public String toString() {
        return "MessageDetailResponse{" +
                "result=" + result +
                '}';
    }

    public Message getResult() {
        return result;
    }
}
