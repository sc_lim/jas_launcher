/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Payment;
import kr.altimedia.launcher.jasmin.dm.payment.obj.RequestVodPaymentResultInfo;
import kr.altimedia.launcher.jasmin.dm.payment.obj.VodPaymentResult;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentGuideItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentWrapper;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.presenter.PaymentGuidePresenter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager.KEY_CANCEL_PURCHASE_ID;

public class VodPaymentGuideFragment extends VodPaymentBaseFragment {
    public static final String CLASS_NAME = VodPaymentGuideFragment.class.getName();
    private final String TAG = VodPaymentGuideFragment.class.getSimpleName();

    private static final String KEY_PURCHASED_INFO = "PURCHASED_INFO";

    private final ArrayList<PaymentGuideItem> GUIDE_AIRPAY =
            new ArrayList<>(Arrays.asList(
                    new PaymentGuideItem(R.string.qr_airpay_step_one, R.drawable.qr_airpay_img_1, R.drawable.qr_info_1),
                    new PaymentGuideItem(R.string.qr_airpay_step_two, R.drawable.qr_airpay_img_2, R.drawable.qr_info_2),
                    new PaymentGuideItem(R.string.qr_airpay_step_three, R.drawable.qr_airpay_img_3, R.drawable.qr_info_3)));
    private final ArrayList<PaymentGuideItem> GUIDE_M_BANKING =
            new ArrayList<>(Arrays.asList(
                    new PaymentGuideItem(R.string.qr_m_banking_step_one, R.drawable.qr_mbanking_img_1, R.drawable.qr_info_1),
                    new PaymentGuideItem(R.string.qr_m_banking_step_two, R.drawable.qr_mbanking_img_2, R.drawable.qr_info_2),
                    new PaymentGuideItem(R.string.qr_m_banking_step_three, R.drawable.qr_mbanking_img_3, R.drawable.qr_info_3)));
    private final ArrayList<PaymentGuideItem> GUIDE_CREDIT_BANKING =
            new ArrayList<>(Arrays.asList(
                    new PaymentGuideItem(R.string.qr_credit_or_banking_step_one, R.drawable.qr_credit_bank_img_1, R.drawable.qr_info_1),
                    new PaymentGuideItem(R.string.qr_credit_or_banking_step_two, R.drawable.qr_credit_bank_img_2, R.drawable.qr_info_2)));

    private TextView minView;
    private TextView secView;
    private CountDownTimer countDownTimer;
    private ImageView qrImageView;

    private PurchaseInfo purchaseInfo;

    public VodPaymentGuideFragment() {
    }

    public static VodPaymentGuideFragment newInstance(PurchaseInfo purchaseInfo) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_PURCHASED_INFO, purchaseInfo);

        VodPaymentGuideFragment fragment = new VodPaymentGuideFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_vod_payment;
    }

    @Override
    protected void initView(View view) {
        initTitle(view);
        initButton(view);
        initGuide(view);
        initTimer(view);
        qrImageView = view.findViewById(R.id.qr_image);

        requestPayment();
    }

    private void initTitle(View view) {
        Context context = view.getContext();
        String title = "";

        purchaseInfo = getArguments().getParcelable(KEY_PURCHASED_INFO);
        PaymentType paymentType = purchaseInfo.getPaymentType();
        if (paymentType == PaymentType.AIR_PAY) {
            title = context.getString(R.string.payment_airpay);
        } else if (paymentType == PaymentType.QR) {
            title = context.getString(R.string.payment_qr);
        } else if (paymentType == PaymentType.CREDIT_CARD || paymentType == PaymentType.INTERNET_MOBILE_BANK) {
            title = context.getString(R.string.payment_qr_credit_or_banking);
        }

        ((TextView) view.findViewById(R.id.title)).setText(title);
    }

    private void initButton(View view) {
        TextView cancel = view.findViewById(R.id.cancel);
        cancel.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        showPaymentCancelDialog(purchaseInfo, purchaseId);
                        return true;
                }

                return false;
            }
        });
    }

    private void initGuide(View view) {
        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter(new PaymentGuidePresenter());
        arrayObjectAdapter.addAll(0, getGuide());

        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(arrayObjectAdapter);
        VerticalGridView verticalGridView = view.findViewById(R.id.guide_grid_view);
        verticalGridView.setAdapter(itemBridgeAdapter);
        int gap = (int) getResources().getDimension(R.dimen.payment_guide_spacing);
        verticalGridView.setVerticalSpacing(gap);

        PaymentType paymentType = purchaseInfo.getPaymentType();
        if (paymentType == PaymentType.CREDIT_CARD || paymentType == PaymentType.INTERNET_MOBILE_BANK) {
            ViewGroup.LayoutParams params = verticalGridView.getLayoutParams();
            params.height = (int) getResources().getDimension(R.dimen.payment_guide_credit_bank_height);
        }
    }

    private ArrayList<PaymentGuideItem> getGuide() {
        ArrayList<PaymentGuideItem> list = new ArrayList<>();

        PaymentType paymentType = purchaseInfo.getPaymentType();
        if (paymentType == PaymentType.AIR_PAY) {
            list.addAll(0, GUIDE_AIRPAY);
        } else if (paymentType == PaymentType.QR) {
            list.addAll(0, GUIDE_M_BANKING);
        } else if (paymentType == PaymentType.CREDIT_CARD || paymentType == PaymentType.INTERNET_MOBILE_BANK) {
            list.addAll(0, GUIDE_CREDIT_BANKING);
        }

        return list;
    }

    private void initTimer(View view) {
        minView = view.findViewById(R.id.min_counting);
        secView = view.findViewById(R.id.sec_counting);
        setTime(TOTAL_COUNT_DOWN);

        String timeOutErrorMessage = getString(R.string.payment_error);
        countDownTimer = new CountDownTimer(TOTAL_COUNT_DOWN, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                setTime(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                if (getOnPaymentListener() != null) {
                    getOnPaymentListener().onPaymentError(timeOutErrorMessage);
                }
            }
        };
    }

    private int setTime(long millisUntilFinished) {
        long min = TimeUtil.getMin(millisUntilFinished);
        long sec = TimeUtil.getSec(millisUntilFinished);

        String minText = min < 10 ? "0" + min : String.valueOf(min);
        String secText = sec < 10 ? "0" + sec : String.valueOf(sec);
        minView.setText(minText);
        secView.setText(secText);

        return (int) sec;
    }

    private void setQrCode(Bitmap qrCode) {
        Glide.with(getContext())
                .load(qrCode)
                .centerCrop()
                .into(qrImageView);
    }

    private TvOverlayManager mTvOverlayManager = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttached : context");
        }
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
    }

    @Override
    protected Payment getCreditCardPayment(Payment payment) {
        CreditCard creditCard = purchaseInfo.getCreditCard();
        payment.setCreditCardType(creditCard.getCreditCardType());
        payment.setPaymentDetail(creditCard.getCreditCardId());
        return payment;
    }

    @Override
    protected Payment getBankPayment(Payment payment) {
        payment.setBankCode(purchaseInfo.getBank().getBankCode());
        return payment;
    }

    private void requestPayment() {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestPayment");
        }

        Product product = purchaseInfo.getProduct();
        PaymentWrapper paymentWrapper = purchaseInfo.getPaymentWrapper();
        ArrayList<Payment> payments = getPaymentList(purchaseInfo);

        paymentRequestTask = vodPaymentDataManager.requestVodPayment(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), purchaseInfo.getContentType(),
                product.getOfferID(), payments, paymentWrapper.getPrice(),
                new MbsDataProvider<String, RequestVodPaymentResultInfo>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, RequestVodPaymentResultInfo result) {
                        purchaseId = result.getPurchaseId();
                        //call cancel vod payment for Home/back/cancel
                        getParentFragment().getArguments().putString(KEY_CANCEL_PURCHASE_ID, purchaseId);

                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onSuccess, purchasedId : " + purchaseId);
                        }

                        if (result.getQrCode() == null) {
                            return;
                        }

                        if (!StringUtils.nullToEmpty(purchaseId).isEmpty()) {
                            byte[] bytes = Base64.decode(result.getQrCode(), Base64.DEFAULT);
                            Bitmap qrCode = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            setQrCode(qrCode);

                            countDownTimer.start();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPaymentGuideFragment.this.getContext();
                    }
                });
    }

    @Override
    protected void getPaymentResult(String resultId) {
        String offerID = purchaseInfo.getProduct().getOfferID();
        if (Log.INCLUDE) {
            Log.d(TAG, "getPaymentResult, offerID : " + offerID);
        }

        if (purchaseId == null || !resultId.equalsIgnoreCase(offerID)) {
            return;
        }

        //do not cancel payment if getting product is purchased
        getParentFragment().getArguments().remove(KEY_CANCEL_PURCHASE_ID);

        if (paymentResultTask != null && paymentResultTask.isRunning()) {
            paymentResultTask.cancel(true);
        }

        paymentResultTask = vodPaymentDataManager.getVodPaymentResult(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), purchaseId,
                new MbsDataProvider<String, VodPaymentResult>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPaymentResult, onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, VodPaymentResult result) {
                        boolean isSuccess = result.isSuccess();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPaymentResult, onSuccess, isSuccess : " + isSuccess);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentComplete(false, isSuccess);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPaymentResult, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return VodPaymentGuideFragment.this.getContext();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        countDownTimer.cancel();
    }

//    private final String DUMMY = "iVBORw0KGgoAAAANSUhEUgAAAV4AAAFeCAYAAADNK3caAAAJtklEQVR42u3d0Y2jUBBEUfJP2hODpRHuqjo3ABZ43Yf5sfb5SJJe7fEKJAm8kgReSRJ4JQm8kiTwShJ4JUnglSTwShJ4JUnglSTwSpLAK0nglSSBV5LAK0nglSSBV5LAK0kCrySBV5IEXkkCrySBV5IEXkkCryQJvJIEXkkSeCUJvJIEXkkSeCUJvJIk8EoSeCVJ4JUk8EoSeCVJ4JUk8EqSwCtJ4JUkgVeSwPvtDQ+XeBaJs5H4fuwFeMELXvCCF7zgNWDgBa+9AK8BAy94wQte8Bow8ILXXoDXgIEXvPYCvOA1YOAFr70ArwEDL3jtBXjBC17wghe8g/BGHsQ/Pdeb12mF7s3nunYd+w5eAwZe8IIXvA4CvOAFr30HrwEDL3jtBXgdBHjBC177Dl7wghe84AWvgwAveMELXgcBXvCC176D14CBF7zgBe8gUBYwaykS4bUX4AUveMELXvCC14CBF7z2ArzgBS94wQte8Bow8ILXXoDXgIEXPuAFL3jBC17wghe8Bgy84LUX4AUveMELXvCC14AdX+Tle772fuwFeMELXvCCF7zgNWDgBa+9AK8BAy94wQte8Bow8ILXXoDXgIEXvPYCvOAFL3jBC17wGjDwgtdegBe84AUveMEL3qkBS4QlcUkTPzrgBa8BAy947QV4wQte8IIXvOA1YOAFr70AL3jBC17wghe8Bgy84LUX4DVg4AUveMELXvCCF7zgBa8BAy947QV4B+H1QXlnca7dc2L2ArwOArzgBa99B68BAy947QV4HQR4wQte+w5e8IIXvOAFr4MAL3jBC14HAV7wgte+g9eAgVfgBa+DAC94wWvfwWsBfwi462RdZ3kvwAte8LoOeMELXvC6DnjBC14DBhbXAS94wQte1wEveMFrwMDiOvYCvOAFr+uAF7zgBa/ruI69AC94wes64AVv5E2rawETn/3NezbVhXvqFYAXvOAVeAVe8IIXvAIveMEr8Aq84BV4BV7wglfgBS94wSvwCrzgBa/AC17wglfgfWu5ri1FK2LL+Cx/dJZ/lQZe8HrP4AUveIEAXvCCF7zgBS94wQte8IIXvOAFL3jBC17wghe84AUveMELXvCCF7zgBS94wQte7xm84AVv3DAD6s55LeOc+FFu3Xfwghe84AUveMELXvCCF7zgBS94wQte8IIXvOAFL3jBC17wghe84AUveMELXvCCF7zgBS94wQte8IIXvOCtHGb3434u/Hqr9Y8Nv1wDL1jcD3jBC16wuB/wghe84HU/7ge84AUvWNwPeMELXvC6H/CCF7zgBYv7AS94wQte9wNe8IIXvGBxP+AFL3iPLOm167SeBXzuzPPymYIXvOAFL3jBC17wghe84AUveMELXvCCF7zgBS94wQte8MLHs4MXvOAFL3jBC17wghe84AUveMELXvCCF7xxy56IYeIwty5y62wk7mDtHy3gBS94wQte8IIXvOAFL3jBC17wghe84LVc4AUveMELXvCCF7zgBS94wWs2wAte8IIXvOAFL3jBC17wghe8aUAtY3htkZc/Fonv+TMceMELBPCCF7zgBS94wQte8IIXvOAFL3jBC17wghe84AUveL1n8IIXvEAAL3jBC17wgtd7Bi94wQsE8IIXvJWH/gx37eOVeJ3lM20FHLzgBS94wQte8IIXvM4UvOAFL3jBC17wgteSghe84AUveMELXvCCF7zgBS94wQte8IIXvOAFL3jBC17wgncWVWjcWcDWZb/2fpbfM3jBC14ggBe84AUveMELXvCCF7zgBS94wQte8IIXvOAFL3jBC17wghe84AUveMELXvCC13sGL3jBCwTwgjcOw8T7WV4uH8q+64AXvOAFLzDBC17wghe84AUveMELXtcBL3jBC17wghe84AUveF0HvOAFL3hBB17wghe84HUd8IIXvOAFJnhn4X1zAa8t8rV/y0cwa1YT9xS84AUveMELXvCCF7zgBS94wQte8IIXvOAFL3jBC17wghe84AUveMELXvCCF7zgBS94wQte8IIXvOAFL3gr4b22pAZsE8PEmU+8DnjBC17wghe84AUveL0f8IIXvOAFL3jBC17/FnjBC17wwhC8FhC84AUveMELXvCCF7xgAS94wQte8IIXvOCdhRcIm8+V+Iu8VnjtF3gNBnjBC17wghe84AUveMFrMMALXvCCF7yeC7zgBa/AC17wghe8gPJc4AUveMELXvCC136B12CAF7zgBW/QAhqwO0Bdy/vpQx684AUveL0f8IIXvGABL3jBC17wghe84AUveMHi/YAXvOAFL3jBC17wghcs3g94wQte8IIXvOAFL3jB6/2A1y/X2g60FKjED3frBzdxDsEr8IIXvOAVeMELXvCCV+AFL3jBK/CCF7zgBa/AC17wglfgBS94wQte8IIXvOAFr8ALXvCC9wGLXwwlnGnr/Vw799aPIHjBC17wghe84AUveMELXvCCF7zgBS94wQte8IIXvOAFL3jBC17wghe84AUveMELXvCCF7zgBS94wQte8PqvfwafC3Sbz34NcPACCrzgBS94wQte+Hh28IIXvPDx7OAFL3jBa9k9O3jBC174eHb3A17wei74eHbwghe88PHs4AUvoMALXvCCF7xBg9H6i6HWX+1dA6H1LOAMXvCCF7zgBS94wQte8IIXvOAFL3jBC17wWnbwghe84AUveJ0FeMELXssOXvCCF7zgBa+zAC94wQte8IIXvOD9+XO5zubHtPXjBV7wgg684AUveMHrOuAFL3gNGOjAC17wghe8rgNe8IIXvK4DXnsBXvCCF7zgBS94wes64AUveMELOvCCF7zgHYT32rO3Lmnrs7sf8IIXvOAFL3jBC17wghe84AUveMELXvCCF7zgBS94wQte8IIXvOAFL3jBC17wghe84AUveMELXvCCF3TgLYN3+YOS+IFb/uiY+b7AC17wgtcugxe84AUveMHrsMALXvDaZfAaQvCC18yD12GBF7zgtcvgBS94wWuXweuwwAte8IIXvOAFL3jtMnhnF+camD5eWR+CxPsBL3jBC17wghe84DXM4AUveMELXvCC16yCF7yGGbzgBS94wQte8JpV8IIXvOAFL3jBa5jBC17wghe84AUveMErScWBV5LAK0nglSSBV5LAK0kCrySBV5IEXkkCrySBV5IEXkkCryQJvJIEXkkSeCUJvJIEXkkSeCUJvJIk8EoSeCVJ4JUk8EoSeCVJ4JUk8EqSwCtJ4JUkgVeSwCtJ4JUkgVeSwCtJAq8kgVeSBF5JAq8kgVeSBF5JAq8kCbySBF5JEnglCbyStNYfLBPZUL9tSp0AAAAASUVORK5CYII=";
}
