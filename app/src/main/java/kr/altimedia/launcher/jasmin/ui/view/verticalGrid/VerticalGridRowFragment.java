/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.verticalGrid;

import android.animation.TimeAnimator;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import androidx.fragment.app.Fragment;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.RowHeaderView;
import androidx.leanback.widget.ViewHolderTask;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewSelectedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.GridListRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.GridRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.verticalRow.BaseVerticalRowFragment;

import static androidx.leanback.widget.BaseGridView.WINDOW_ALIGN_HIGH_EDGE;
import static androidx.leanback.widget.BaseGridView.WINDOW_ALIGN_LOW_EDGE;

/**
 * Created by mc.kim on 12,12,2019
 */
public class VerticalGridRowFragment extends BaseVerticalRowFragment implements
        GridMainFragmentRowAdpaterProvider {

    private VerticalGridRowFragment.MainFragmentRowsAdapter mMainFragmentRowsAdapter;


    @Override
    public VerticalGridRowFragment.MainFragmentRowsAdapter getMainRowFragmentRowsAdapter() {
        if (mMainFragmentRowsAdapter == null) {
            mMainFragmentRowsAdapter = new VerticalGridRowFragment.MainFragmentRowsAdapter(this);
        }
        return mMainFragmentRowsAdapter;
    }

    /**
     * Internal helper class that manages row select animation and apply a default
     * dim to each row.
     */
    final class RowViewHolderExtra implements TimeAnimator.TimeListener {
        final GridRowPresenter mGridRowPresenter;
        final Presenter.ViewHolder mRowViewHolder;

        final TimeAnimator mSelectAnimator = new TimeAnimator();

        int mSelectAnimatorDurationInUse;
        Interpolator mSelectAnimatorInterpolatorInUse;
        float mSelectLevelAnimStart;
        float mSelectLevelAnimDelta;

        RowViewHolderExtra(ItemBridgeAdapter.ViewHolder ibvh) {
            mGridRowPresenter = (GridRowPresenter) ibvh.getPresenter();
            mRowViewHolder = ibvh.getViewHolder();
            mSelectAnimator.setTimeListener(this);
        }


        @Override
        public void onTimeUpdate(TimeAnimator animation, long totalTime, long deltaTime) {
            if (mSelectAnimator.isRunning()) {
                updateSelect(totalTime, deltaTime);
            }
        }

        void updateSelect(long totalTime, long deltaTime) {
            float fraction;
            if (totalTime >= mSelectAnimatorDurationInUse) {
                fraction = 1;

                mSelectAnimator.end();
            } else {
                fraction = (float) (totalTime / (double) mSelectAnimatorDurationInUse);
            }
            if (mSelectAnimatorInterpolatorInUse != null) {
                fraction = mSelectAnimatorInterpolatorInUse.getInterpolation(fraction);
            }
            float level = mSelectLevelAnimStart + fraction * mSelectLevelAnimDelta;
            mGridRowPresenter.setSelectLevel(mRowViewHolder, level);
        }

        void animateSelect(boolean select, boolean immediate) {
            mSelectAnimator.end();
            final float end = select ? 1 : 0;
            if (immediate) {
                mGridRowPresenter.setSelectLevel(mRowViewHolder, end);
            } else if (mGridRowPresenter.getSelectLevel(mRowViewHolder) != end) {
                mSelectAnimatorDurationInUse = mSelectAnimatorDuration;
                mSelectAnimatorInterpolatorInUse = mSelectAnimatorInterpolator;
                mSelectLevelAnimStart = mGridRowPresenter.getSelectLevel(mRowViewHolder);
                mSelectLevelAnimDelta = end - mSelectLevelAnimStart;
                mSelectAnimator.start();
            }
        }

    }

    static final String TAG = "VerticalGridRowFragment";
    static final boolean DEBUG = false;
    static final int ALIGN_TOP_NOT_SET = Integer.MIN_VALUE;
    static final int ALIGN_TOP = WINDOW_ALIGN_HIGH_EDGE;
    static final int ALIGN_BOTTOM = WINDOW_ALIGN_LOW_EDGE;

    ItemBridgeAdapter.ViewHolder mSelectedViewHolder;
    private int mSubPosition;
    boolean mExpand = true;
    boolean mViewsCreated;
    private int mAlignedTop = ALIGN_TOP_NOT_SET;
    boolean mAfterEntranceTransition = true;

    BaseOnItemViewSelectedListener mOnItemViewSelectedListener;
    BaseOnItemViewClickedListener mOnItemViewClickedListener;

    View.OnClickListener mOnHeaderItemViewClickedListener;

    // Select animation and interpolator are not intended to be
    // exposed at this moment. They might be synced with vertical scroll
    // animation later.
    int mSelectAnimatorDuration;
    Interpolator mSelectAnimatorInterpolator = new DecelerateInterpolator(2);

    private RecyclerView.RecycledViewPool mRecycledViewPool;
    private ArrayList<Presenter> mPresenterMapper;

    ItemBridgeAdapter.AdapterListener mExternalAdapterListener;

    @Override
    protected VerticalGridView findGridViewFromRoot(View view) {
        return (VerticalGridView) view.findViewById(R.id.gridView);
    }

    public void setOnItemViewClickedListener(BaseOnItemViewClickedListener listener) {
        mOnItemViewClickedListener = listener;
        if (mViewsCreated) {
            throw new IllegalStateException(
                    "Item clicked listener must be set before views are created");
        }
    }

    public void setOnHeaderItemViewClickedListener(View.OnClickListener listener) {
        mOnHeaderItemViewClickedListener = listener;
        if (mViewsCreated) {
            throw new IllegalStateException(
                    "Item clicked listener must be set before views are created");
        }
    }


    /**
     * Returns the item clicked listener.
     */
    public BaseOnItemViewClickedListener getOnItemViewClickedListener() {
        return mOnItemViewClickedListener;
    }

    public void setExpand(boolean expand) {
        mExpand = expand;
        VerticalGridView listView = getVerticalGridView();
        if (listView != null) {
            final int count = listView.getChildCount();
            if (DEBUG) Log.v(TAG, "setExpand " + expand + " count " + count);
            for (int i = 0; i < count; i++) {
                View view = listView.getChildAt(i);
                ItemBridgeAdapter.ViewHolder vh =
                        (ItemBridgeAdapter.ViewHolder) listView.getChildViewHolder(view);
                setRowViewExpanded(vh, mExpand);
            }
        }
    }

    /**
     * Sets an item selection listener.
     */
    public void setOnItemViewSelectedListener(BaseOnItemViewSelectedListener listener) {
        mOnItemViewSelectedListener = listener;
        VerticalGridView listView = getVerticalGridView();
        if (listView != null) {
            final int count = listView.getChildCount();
            Log.d(TAG, "count : " + count);
            for (int i = 0; i < count; i++) {
                View view = listView.getChildAt(i);
                ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder)
                        listView.getChildViewHolder(view);
                getRowViewHolder(ibvh).setOnItemViewSelectedListener(mOnItemViewSelectedListener);
            }
        }
    }


    /**
     * Returns an item selection listener.
     */
    public BaseOnItemViewSelectedListener getOnItemViewSelectedListener() {
        return mOnItemViewSelectedListener;
    }

    @Override
    protected void onRowSelected(RecyclerView parent, RecyclerView.ViewHolder viewHolder,
                                 int position, int subposition) {
        if (mSelectedViewHolder != viewHolder || mSubPosition != subposition) {
            if (DEBUG) Log.v(TAG, "new row selected position " + position + " subposition "
                    + subposition + " view " + viewHolder.itemView);
            mSubPosition = subposition;
            if (mSelectedViewHolder != null) {
                setRowViewSelected(mSelectedViewHolder, false, false);
            }
            mSelectedViewHolder = (ItemBridgeAdapter.ViewHolder) viewHolder;
            if (mSelectedViewHolder != null) {
                setRowViewSelected(mSelectedViewHolder, true, false);
            }
        }
        // When VerticalGridRowFragment is embedded inside a page fragment, we want to show
        // the title view only when we're on the first row or there is no data.
    }

    /**
     * Get row ViewHolder at adapter position.  Returns null if the row object is not in adapter or
     * the row object has not been bound to a row view.
     *
     * @param position Position of row in adapter.
     * @return Row ViewHolder at a given adapter position.
     */
    public GridRowPresenter.ViewHolder getRowViewHolder(int position) {
        VerticalGridView verticalView = getVerticalGridView();
        if (verticalView == null) {
            return null;
        }
        return getRowViewHolder((ItemBridgeAdapter.ViewHolder)
                verticalView.findViewHolderForAdapterPosition(position));
    }

    private int mLayoutId = R.layout.fragment_contents_browse;

    public void setLayoutId(int mLayoutId) {
        this.mLayoutId = mLayoutId;
    }

    @Override
    protected int getLayoutResourceId() {
        return this.mLayoutId;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSelectAnimatorDuration = getResources().getInteger(
                R.integer.lb_browse_rows_anim_duration);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (DEBUG) Log.v(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        // Align the top edge of child with id row_content.
        // Need set this for directly using VerticalGridRowFragment.
        getVerticalGridView().setItemAlignmentViewId(R.id.row_content);
        getVerticalGridView().setSaveChildrenPolicy(VerticalGridView.SAVE_LIMITED_CHILD);
        setAlignment(mAlignedTop);

        mRecycledViewPool = null;
        mPresenterMapper = null;

    }

    @Override
    public void onDestroyView() {
        mViewsCreated = false;
        super.onDestroyView();
    }

    void setExternalAdapterListener(ItemBridgeAdapter.AdapterListener listener) {
        mExternalAdapterListener = listener;
    }

    static void setRowViewExpanded(ItemBridgeAdapter.ViewHolder vh, boolean expanded) {
        ((GridRowPresenter) vh.getPresenter()).setRowViewExpanded(vh.getViewHolder(), true);
    }

    static void setRowViewSelected(ItemBridgeAdapter.ViewHolder vh, boolean selected,
                                   boolean immediate) {
        VerticalGridRowFragment.RowViewHolderExtra extra = (VerticalGridRowFragment.RowViewHolderExtra) vh.getExtraObject();
        extra.animateSelect(selected, immediate);
        ((GridRowPresenter) vh.getPresenter()).setRowViewSelected(vh.getViewHolder(), selected);
    }


    private final ItemBridgeAdapter.AdapterListener mBridgeAdapterListener =
            new ItemBridgeAdapter.AdapterListener() {
                @Override
                public void onAddPresenter(Presenter presenter, int type) {
                    if (mExternalAdapterListener != null) {
                        mExternalAdapterListener.onAddPresenter(presenter, type);
                    }
                }

                @Override
                public void onCreate(ItemBridgeAdapter.ViewHolder vh) {
                    VerticalGridView listView = getVerticalGridView();
                    if (listView != null) {
                        // set clip children false for slide animation
                        listView.setClipChildren(false);
                    }
                    setupSharedViewPool(vh);
                    mViewsCreated = true;
                    vh.setExtraObject(new VerticalGridRowFragment.RowViewHolderExtra(vh));
                    // selected state is initialized to false, then driven by grid view onChildSelected
                    // events.  When there is rebind, grid view fires onChildSelected event properly.
                    // So we don't need do anything special later in onBind or onAttachedToWindow.
                    setRowViewSelected(vh, false, true);
                    if (mExternalAdapterListener != null) {
                        mExternalAdapterListener.onCreate(vh);
                    }
                }

                @Override
                public void onAttachedToWindow(ItemBridgeAdapter.ViewHolder vh) {
                    if (DEBUG) Log.v(TAG, "onAttachToWindow");
                    // All views share the same mExpand value.  When we attach a view to grid view,
                    // we should make sure it pick up the latest mExpand value we set early on other
                    // attached views.  For no-structure-change update,  the view is rebound to new data,
                    // but again it should use the unchanged mExpand value,  so we don't need do any
                    // thing in onBind.
                    setRowViewExpanded(vh, mExpand);
                    GridRowPresenter gridRowPresenter = (GridRowPresenter) vh.getPresenter();
                    GridRowPresenter.ViewHolder rowVh = gridRowPresenter.getRowViewHolder(vh.getViewHolder());
                    rowVh.setOnItemViewSelectedListener(mOnItemViewSelectedListener);
                    rowVh.setOnItemViewClickedListener(mOnItemViewClickedListener);
                    rowVh.getHeaderViewHolder().view.setOnClickListener(mOnHeaderItemViewClickedListener);

                    gridRowPresenter.setEntranceTransitionState(rowVh, mAfterEntranceTransition);
                    if (mExternalAdapterListener != null) {
                        mExternalAdapterListener.onAttachedToWindow(vh);
                    }
                }

                @Override
                public void onDetachedFromWindow(ItemBridgeAdapter.ViewHolder vh) {
                    if (mSelectedViewHolder == vh) {
                        setRowViewSelected(mSelectedViewHolder, false, true);
                        mSelectedViewHolder = null;
                    }
                    if (mExternalAdapterListener != null) {
                        mExternalAdapterListener.onDetachedFromWindow(vh);
                    }
                }

                private void initHeaderInfo(RowHeaderView headerView) {
                    if (headerView != null) {
                        Log.d(TAG, "found row header : " + headerView.getText());
                        headerView.setVisibility(View.VISIBLE);
                        headerView.setContentDescription("#" + headerView.getText());
                        headerView.setClickable(true);
                        headerView.setActivated(true);
                        headerView.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_YES);
                    }
                }

                @Override
                public void onBind(ItemBridgeAdapter.ViewHolder vh) {

                    GridRowPresenter gridRowPresenter = (GridRowPresenter) vh.getPresenter();
                    GridRowPresenter.ViewHolder rowVh = gridRowPresenter.getRowViewHolder(vh.getViewHolder());
                    rowVh.getHeaderViewHolder().view.setTag(rowVh.getRow().getHeaderItem());
                    ViewGroup headerViewGroup = (ViewGroup) rowVh.getHeaderViewHolder().view;
                    initHeaderInfo(headerViewGroup.findViewById(R.id.row_header));
                    if (mExternalAdapterListener != null) {
                        mExternalAdapterListener.onBind(vh);
                    }
                }

                @Override
                public void onUnbind(ItemBridgeAdapter.ViewHolder vh) {
                    setRowViewSelected(vh, false, true);
                    if (mExternalAdapterListener != null) {
                        mExternalAdapterListener.onUnbind(vh);
                    }
                }
            };

    void setupSharedViewPool(ItemBridgeAdapter.ViewHolder bridgeVh) {
        GridRowPresenter gridRowPresenter = (GridRowPresenter) bridgeVh.getPresenter();
        GridRowPresenter.ViewHolder rowVh = gridRowPresenter.getRowViewHolder(bridgeVh.getViewHolder());
        Log.d(TAG, "setupSharedViewPool : " + bridgeVh.getViewHolder().getClass().getSimpleName());

        if (rowVh instanceof GridListRowPresenter.ViewHolder) {
            VerticalGridView view = ((GridListRowPresenter.ViewHolder) rowVh).getGridView();
            // Recycled view pool is shared between all list rows
            if (mRecycledViewPool == null) {
                mRecycledViewPool = view.getRecycledViewPool();
            } else {
                view.setRecycledViewPool(mRecycledViewPool);
            }

            ItemBridgeAdapter bridgeAdapter =
                    ((GridListRowPresenter.ViewHolder) rowVh).getBridgeAdapter();
            if (mPresenterMapper == null) {
                mPresenterMapper = bridgeAdapter.getPresenterMapper();
            } else {
                bridgeAdapter.setPresenterMapper(mPresenterMapper);
            }
        }
    }

    @Override
    protected void updateAdapter() {
        super.updateAdapter();
        mSelectedViewHolder = null;
        mViewsCreated = false;

        ItemBridgeAdapter adapter = getBridgeAdapter();
        if (adapter != null) {
            adapter.setAdapterListener(mBridgeAdapterListener);
        }
    }

    @Override
    public boolean onTransitionPrepare() {
        boolean prepared = super.onTransitionPrepare();
        if (prepared) {
            freezeRows(true);
        }
        return prepared;
    }

    @Override
    public void onTransitionEnd() {
        super.onTransitionEnd();
        freezeRows(false);
    }

    private void freezeRows(boolean freeze) {
        VerticalGridView verticalView = getVerticalGridView();
        if (verticalView != null) {
            final int count = verticalView.getChildCount();
            for (int i = 0; i < count; i++) {
                ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder)
                        verticalView.getChildViewHolder(verticalView.getChildAt(i));
                GridRowPresenter gridRowPresenter = (GridRowPresenter) ibvh.getPresenter();
                GridRowPresenter.ViewHolder vh = gridRowPresenter.getRowViewHolder(ibvh.getViewHolder());
                gridRowPresenter.freeze(vh, freeze);
            }
        }
    }

    /**
     * For rows that willing to participate entrance transition,  this function
     * hide views if afterTransition is true,  show views if afterTransition is false.
     */
    public void setEntranceTransitionState(boolean afterTransition) {
        mAfterEntranceTransition = afterTransition;
        VerticalGridView verticalView = getVerticalGridView();
        if (verticalView != null) {
            final int count = verticalView.getChildCount();
            for (int i = 0; i < count; i++) {
                ItemBridgeAdapter.ViewHolder ibvh = (ItemBridgeAdapter.ViewHolder)
                        verticalView.getChildViewHolder(verticalView.getChildAt(i));
                GridRowPresenter gridRowPresenter = (GridRowPresenter) ibvh.getPresenter();
                GridRowPresenter.ViewHolder vh = gridRowPresenter.getRowViewHolder(ibvh.getViewHolder());
                gridRowPresenter.setEntranceTransitionState(vh, mAfterEntranceTransition);
            }
        }
    }

    public void setSelectedPosition(int rowPosition, boolean smooth,
                                    final Presenter.ViewHolderTask rowHolderTask) {
        VerticalGridView verticalView = getVerticalGridView();
        if (verticalView == null) {
            return;
        }
        ViewHolderTask task = null;
        if (rowHolderTask != null) {
            // This task will execute once the scroll completes. Once the scrolling finishes,
            // we will get a success callback to update selected row position. Since the
            // update to selected row position happens in a post, we want to ensure that this
            // gets called after that.
            task = new ViewHolderTask() {
                @Override
                public void run(final RecyclerView.ViewHolder rvh) {
                    rvh.itemView.post(new Runnable() {
                        @Override
                        public void run() {
                            rowHolderTask.run(
                                    getRowViewHolder((ItemBridgeAdapter.ViewHolder) rvh));
                        }
                    });
                }
            };
        }

        if (smooth) {
            verticalView.setSelectedPositionSmooth(rowPosition, task);
        } else {
            verticalView.setSelectedPosition(rowPosition, task);
        }
    }

    static GridRowPresenter.ViewHolder getRowViewHolder(ItemBridgeAdapter.ViewHolder ibvh) {
        if (ibvh == null) {
            return null;
        }
        GridRowPresenter gridRowPresenter = (GridRowPresenter) ibvh.getPresenter();
        return gridRowPresenter.getRowViewHolder(ibvh.getViewHolder());
    }

    public boolean isScrolling() {
        if (getVerticalGridView() == null) {
            return false;
        }
        return getVerticalGridView().getScrollState() != HorizontalGridView.SCROLL_STATE_IDLE;
    }

    @Override
    public void setAlignment(int windowAlignOffsetFromTop) {
        final VerticalGridView gridView = getVerticalGridView();
        mAlignedTop = windowAlignOffsetFromTop;
        if (gridView == null) {
            return;
        }

        if (windowAlignOffsetFromTop == ALIGN_TOP_NOT_SET) {
            gridView.setItemAlignmentOffset(0);
            gridView.setItemAlignmentOffsetPercent(
                    0f);
            gridView.setItemAlignmentOffsetWithPadding(false);
            gridView.setWindowAlignmentOffset(0);
            // align to a fixed position from top
            gridView.setWindowAlignmentOffsetPercent(
                    50f);
            gridView.setWindowAlignment(3);
        } else {
            gridView.setItemAlignmentOffset(0);
            gridView.setItemAlignmentOffsetPercent(
                    VerticalGridView.ITEM_ALIGN_OFFSET_PERCENT_DISABLED);
            gridView.setItemAlignmentOffsetWithPadding(true);
            gridView.setWindowAlignmentOffset(mAlignedTop);
            // align to a fixed position from top
            gridView.setWindowAlignmentOffsetPercent(
                    VerticalGridView.WINDOW_ALIGN_OFFSET_PERCENT_DISABLED);
            gridView.setWindowAlignment(VerticalGridView.WINDOW_ALIGN_NO_EDGE);
        }
    }


//    @Override
//    public void setAlignment(int windowAlignOffsetFromTop) {
//        final VerticalGridView gridView = getVerticalGridView();
//        Log.d(TAG, "setAlignment  : " + windowAlignOffsetFromTop);
//        mAlignedTop = windowAlignOffsetFromTop;
//
//        if (gridView == null) {
//            return;
//        }
//
//        if (mAlignedTop == ALIGN_TOP_NOT_SET) {
//            gridView.setItemAlignmentOffset(0);
//            gridView.setItemAlignmentOffsetPercent(
//                    VerticalGridView.ITEM_ALIGN_OFFSET_PERCENT_DISABLED);
//            gridView.setItemAlignmentOffsetWithPadding(true);
//            gridView.setWindowAlignmentOffset(0);
//            // align to a fixed position from top
//            gridView.setWindowAlignmentOffsetPercent(
//                    VerticalGridView.WINDOW_ALIGN_OFFSET_PERCENT_DISABLED);
//            gridView.setWindowAlignment(WINDOW_ALIGN_LOW_EDGE);
//
//        } else {
//            gridView.setItemAlignmentOffset(0);
//            gridView.setItemAlignmentOffsetPercent(
//                    VerticalGridView.ITEM_ALIGN_OFFSET_PERCENT_DISABLED);
//            gridView.setItemAlignmentOffsetWithPadding(true);
//            gridView.setWindowAlignmentOffset(mAlignedTop);
//            // align to a fixed position from top
//            gridView.setWindowAlignmentOffsetPercent(
//                    VerticalGridView.WINDOW_ALIGN_OFFSET_PERCENT_DISABLED);
//            gridView.setWindowAlignment(VerticalGridView.WINDOW_ALIGN_NO_EDGE);
//        }
//
//    }

    /**
     * Find row ViewHolder by position in adapter.
     *
     * @param position Position of row.
     * @return ViewHolder of Row.
     */
    public GridRowPresenter.ViewHolder findRowViewHolderByPosition(int position) {
        if (mVerticalGridView == null) {
            return null;
        }
        return getRowViewHolder((ItemBridgeAdapter.ViewHolder) mVerticalGridView
                .findViewHolderForAdapterPosition(position));
    }


    public static class MainFragmentRowsAdapter
            extends VerticalGridRowFragment.BaseMainFragmentRowsAdapter<VerticalGridRowFragment> {
        public MainFragmentRowsAdapter(VerticalGridRowFragment fragment) {
            super(fragment);
        }

        @Override
        public void setAdapter(ObjectAdapter adapter) {
            getFragment().setAdapter(adapter);
        }

        /**
         * Sets an item clicked listener on the fragment.
         */
        @Override
        public void setOnItemViewClickedListener(OnItemViewClickedListener listener) {
            getFragment().setOnItemViewClickedListener(listener);
        }

        @Override
        public void setOnItemViewSelectedListener(OnItemViewSelectedListener listener) {
            getFragment().setOnItemViewSelectedListener(listener);
        }

        @Override
        public void setSelectedPosition(int rowPosition,
                                        boolean smooth,
                                        final Presenter.ViewHolderTask rowHolderTask) {
            getFragment().setSelectedPosition(rowPosition, smooth, rowHolderTask);
        }

        @Override
        public void setSelectedPosition(int rowPosition, boolean smooth) {
            getFragment().setSelectedPosition(rowPosition, smooth);
        }

        @Override
        public int getSelectedPosition() {
            return getFragment().getSelectedPosition();
        }

        @Override
        public GridRowPresenter.ViewHolder findRowViewHolderByPosition(int position) {
            return getFragment().findRowViewHolderByPosition(position);
        }
    }

    final ProgressBarManager mProgressBarManager = new ProgressBarManager();

    /**
     * Returns the {@link ProgressBarManager}.
     *
     * @return The {@link ProgressBarManager}.
     */
    public final ProgressBarManager getProgressBarManager() {
        return mProgressBarManager;
    }

    public static class BaseMainFragmentRowsAdapter<T extends Fragment> {
        private final T mFragment;

        public BaseMainFragmentRowsAdapter(T fragment) {
            if (fragment == null) {
                throw new IllegalArgumentException("Fragment can't be null");
            }
            this.mFragment = fragment;
        }


        public final T getFragment() {
            return mFragment;
        }

        /**
         * Set the visibility titles/hover of browse rows.
         */
        public void setAdapter(ObjectAdapter adapter) {
        }

        /**
         * Sets an item clicked listener on the fragment.
         */
        public void setOnItemViewClickedListener(OnItemViewClickedListener listener) {
        }

        /**
         * Sets an item selection listener.
         */
        public void setOnItemViewSelectedListener(OnItemViewSelectedListener listener) {
        }

        /**
         * Selects a Row and perform an optional task on the Row.
         */
        public void setSelectedPosition(int rowPosition,
                                        boolean smooth,
                                        final Presenter.ViewHolderTask rowHolderTask) {
        }

        /**
         * Selects a Row.
         */
        public void setSelectedPosition(int rowPosition, boolean smooth) {
        }

        /**
         * @return The position of selected row.
         */
        public int getSelectedPosition() {
            return 0;
        }

        /**
         * @param position Position of Row.
         * @return Row ViewHolder.
         */
        public GridRowPresenter.ViewHolder findRowViewHolderByPosition(int position) {
            return null;
        }
    }


}