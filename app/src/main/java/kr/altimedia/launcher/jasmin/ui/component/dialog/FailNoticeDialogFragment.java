/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;

import com.altimedia.util.Log;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;

/**
 * Created by mc.kim on 17,07,2020
 */
public class FailNoticeDialogFragment implements View.OnClickListener {
    public static final String CLASS_NAME = FailNoticeDialogFragment.class.getName();
    private final String TAG = FailNoticeDialogFragment.class.getSimpleName();
    private final TwoButtonDialogFragment twoButtonDialogFragment;
    private final int mReason;
    private final Context mContext;
    private FragmentManager mFragmentManager;

    public FailNoticeDialogFragment(int reason, Context context) {
        this.mReason = reason;
        this.mContext = context;
        if (reason == MbsTaskCallback.REASON_NETWORK) {
            twoButtonDialogFragment = NetworkConnectionNoticeDialogFragment.newInstance(context, this);
        } else if (reason == MbsTaskCallback.REASON_LOGIN) {
            twoButtonDialogFragment = LoginNoticeDialogFragment.newInstance(context, this);
        } else {
            twoButtonDialogFragment = FailDialogFragment.newInstance(context, this);
        }
    }

    private TvOverlayManager mTvOverlayManager;
    public boolean show(TvOverlayManager tvOverlayManager, FragmentManager fragmentManager, String tag) {
        mTvOverlayManager = tvOverlayManager;
        mFragmentManager = fragmentManager;
        if (twoButtonDialogFragment == null || tvOverlayManager == null) {
            return false;
        }

        tvOverlayManager.showDialogFragment(twoButtonDialogFragment);
        return true;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        twoButtonDialogFragment.setOnDismissListener(onDismissListener);
    }

    @Override
    public void onClick(View v) {
        if (mReason == MbsTaskCallback.REASON_NETWORK) {
            twoButtonDialogFragment.dismiss();
            Intent intent = new Intent("android.settings.WIFI_SETTINGS");
            intent.setPackage("com.android.tv.settings");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            v.getContext().startActivity(intent);
        } else {
            mTvOverlayManager.showBootDialog(BootDialogFragment.TYPE_RE_BOOT, new BootDialogFragment.BootProcessListener() {
                @Override
                public void onFinished(DialogFragment dialogFragment) {
                    dialogFragment.dismiss();
                    authenticationListener(true);
                    twoButtonDialogFragment.dismiss();
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(PushMessageReceiver.ACTION_LOGIN));
                }

                @Override
                public void onFinishedWithError(DialogFragment dialogFragment, int type, String errorCode, String errorMessage) {
                    dialogFragment.dismiss();
                    authenticationListener(false);
                    twoButtonDialogFragment.dismiss();
                    showError(type, errorCode, errorMessage);
                }
            });
        }
    }

    private void showError(int type, String errorCode, String errorMessage) {
        ErrorDialogFragment errorDialogFragment =
                ErrorDialogFragment.newInstance(TAG, errorCode, errorMessage);
        errorDialogFragment.show(mFragmentManager, ErrorDialogFragment.CLASS_NAME);
    }

    public void authenticationListener(boolean success) {
        if (Log.INCLUDE) {
            Log.d(TAG, "authenticationListener() success:" + success);
            Log.d(TAG, "authenticationListener() Said:" + AccountManager.getInstance().getSaId());
            Log.d(TAG, "authenticationListener() ProfileId:" + AccountManager.getInstance().getProfileId());
        }

        //initialize channel when authentication completed.
        JasChannelManager.getInstance().initMBS(success);
    }
}
