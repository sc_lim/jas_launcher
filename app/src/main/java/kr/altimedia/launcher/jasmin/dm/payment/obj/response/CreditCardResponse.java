package kr.altimedia.launcher.jasmin.dm.payment.obj.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;

public class CreditCardResponse extends BaseResponse {
    @Expose
    @SerializedName("data")
    private CreditCardResult result;

    protected CreditCardResponse(Parcel in) {
        super(in);
        result = in.readParcelable(CreditCardResult.class.getClassLoader());
    }

    public static final Creator<CreditCardResponse> CREATOR = new Creator<CreditCardResponse>() {
        @Override
        public CreditCardResponse createFromParcel(Parcel in) {
            return new CreditCardResponse(in);
        }

        @Override
        public CreditCardResponse[] newArray(int size) {
            return new CreditCardResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(result, flags);
    }

    public CreditCardResult getResult() {
        return result;
    }

    public static class CreditCardResult implements Parcelable {
        @Expose
        @SerializedName("cardList")
        private List<CreditCard> creditCardList;
        @Expose
        @SerializedName("cardTypeList")
        private List<CreditCard> otherCardList;
        @Expose
        @SerializedName("cardLogo")
        private List<CardLogo> cardLogoList;

        protected CreditCardResult(Parcel in) {
            creditCardList = in.createTypedArrayList(CreditCard.CREATOR);
            otherCardList = in.createTypedArrayList(CreditCard.CREATOR);
            cardLogoList = in.createTypedArrayList(CardLogo.CREATOR);
        }

        public static final Creator<CreditCardResult> CREATOR = new Creator<CreditCardResult>() {
            @Override
            public CreditCardResult createFromParcel(Parcel in) {
                return new CreditCardResult(in);
            }

            @Override
            public CreditCardResult[] newArray(int size) {
                return new CreditCardResult[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(creditCardList);
            dest.writeTypedList(otherCardList);
            dest.writeTypedList(cardLogoList);
        }

        private String getCardLogo(String cardType) {
            for (CardLogo logo : cardLogoList) {
                if (logo.getCreditCardType().equalsIgnoreCase(cardType)) {
                    return logo.getCreditCardLogo();
                }
            }

            return "";
        }

        public List<CreditCard> getCreditCardList() {
            for (CreditCard creditCard : creditCardList) {
                String logo = creditCard.getCreditCardLogo();
                if (logo == null || logo.isEmpty()) {
                    String matchLogo = getCardLogo(creditCard.getCreditCardType());
                    creditCard.setCreditCardLogo(matchLogo);
                }
            }

            return creditCardList;
        }

        public List<CreditCard> getOtherCardList() {
            for (CreditCard creditCard : otherCardList) {
                String logo = creditCard.getCreditCardLogo();
                if (logo == null || logo.isEmpty()) {
                    String matchLogo = getCardLogo(creditCard.getCreditCardType());
                    creditCard.setCreditCardLogo(matchLogo);
                }
            }

            return otherCardList;
        }
    }

    private static class CardLogo implements Parcelable {
        private String creditCardType;
        private String creditCardLogo;

        protected CardLogo(Parcel in) {
            creditCardType = in.readString();
            creditCardLogo = in.readString();
        }

        public static final Creator<CardLogo> CREATOR = new Creator<CardLogo>() {
            @Override
            public CardLogo createFromParcel(Parcel in) {
                return new CardLogo(in);
            }

            @Override
            public CardLogo[] newArray(int size) {
                return new CardLogo[size];
            }
        };

        public String getCreditCardType() {
            return creditCardType;
        }

        public String getCreditCardLogo() {
            return creditCardLogo;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(creditCardType);
            dest.writeString(creditCardLogo);
        }
    }
}
