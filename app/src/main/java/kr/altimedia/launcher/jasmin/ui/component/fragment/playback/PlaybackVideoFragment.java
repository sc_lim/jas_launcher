/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.playback;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.altimedia.player.AltiMediaSource;
import com.altimedia.player.subtitle.WebvttParser;
import com.altimedia.player.subtitle.WebvttSubtitle;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.ErrorMessageManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.adds.AddsReportManager;
import kr.altimedia.launcher.jasmin.dm.contents.ContentDataManager;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ContentDetailInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.OpportunityType;
import kr.altimedia.launcher.jasmin.dm.contents.obj.PlacementDecision;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Product;
import kr.altimedia.launcher.jasmin.dm.contents.obj.ThumbnailInfo;
import kr.altimedia.launcher.jasmin.dm.contents.obj.TrackingEvent;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.media.MediaPlayerAdapter;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.interactor.VideoPlaybackInteractor;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.playbck.EndWatchingVodDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.playbck.presenter.VodEndActionPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelLayout;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackSupportFragment;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackGlue;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackSupportFragmentGlueHost;
import kr.altimedia.launcher.jasmin.ui.view.playback.media.VideoPlaybackTransportControlGlue;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekDataProvider;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekUi;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

/**
 * Created by mc.kim on 06,01,2020
 */
public class PlaybackVideoFragment extends VideoPlaybackSupportFragment
        implements EndWatchingVodDialogFragment.OnVodWatchEndListener {
    private final String TAG = PlaybackVideoFragment.class.getSimpleName();


    static final int SURFACE_NOT_CREATED = 0;
    static final int SURFACE_CREATED = 1;

    RelativeLayout relativeLayout;
    SurfaceView mVideoSurface;
    SurfaceHolder.Callback mMediaPlaybackCallback;

    int mState = SURFACE_NOT_CREATED;
    private final int VISIBLE_SIZE = 6;
    private MediaPlayerAdapter mPlayerAdapter = null;
    private VideoPlaybackTransportControlGlue<MediaPlayerAdapter> mTransportControlGlue;
    private Content currentContent = null;
    private boolean isEndDialogShowing = false;
    private final List<PlacementDecision> mPlacementDecisions = new ArrayList<>();
    private final boolean isAddsEnable = true;

    public static PlaybackVideoFragment newInstance(Bundle bundle) {
        PlaybackVideoFragment fragment = new PlaybackVideoFragment();
        fragment.setArguments(bundle);
        fragment.setIsEnableUseVideoSetting(true);
        return fragment;
    }

    private SidePanelLayout.SidePanelActionListener mSidePanelActionListener = null;
    private VideoPlaybackInteractor mVideoPlaybackInteractor = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof SidePanelLayout.SidePanelActionListener) {
            mSidePanelActionListener = (SidePanelLayout.SidePanelActionListener) context;
        }
        if (context instanceof VideoPlaybackInteractor) {
            mVideoPlaybackInteractor = (VideoPlaybackInteractor) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private VideoPlaybackSeekDataProvider generateSeekDataProvider(Content content) {
        return generateSeekDataProvider(content, null);
    }


    private VideoPlaybackSeekDataProvider generateSeekDataProvider(Content content, WebvttSubtitle webvttSubtitle) {
        final boolean hasIntroSkipFunction = content.getMainStreamInfo().getIntroInfo() != null && content.getMainStreamInfo().getIntroInfo().getSkipIntroEndPosition()
                != 0 && content.getMainStreamInfo().getIntroInfo().getSkipIntroStartPosition() != 0;
        final long resumeTime = getArguments().getLong(VideoPlaybackActivity.KEY_START_TIME, 0);
        final boolean isSkipButtonShowing = hasIntroSkipFunction && content.getMainStreamInfo().getIntroInfo().getSkipIntroEndPosition() > resumeTime;
        mPlayerAdapter.setWhenPlayWithControlBar(isSkipButtonShowing);
        if (Log.INCLUDE) {
            Log.d(TAG, "hasIntroSkipFunction : " + hasIntroSkipFunction);
            Log.d(TAG, "hasIntroSkipFunction : " + content.getMainStreamInfo().getIntroInfo());
        }
        ThumbNailSeekPosition mThumbNailSeekPosition = new ThumbNailSeekPosition(webvttSubtitle);

        VideoPlaybackSeekDataProvider mPlaybackSeekDataProvider = new VideoPlaybackSeekDataProvider(VideoPlaybackSeekDataProvider.Type.VOD, VISIBLE_SIZE) {

            @Override
            public long getSkipTimeMillis() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call getSkipTimeMillis : " + hasIntroSkipFunction);
                }
                if (hasIntroSkipFunction) {
                    long skipPosition = content.getMainStreamInfo().getIntroInfo().getSkipIntroEndPosition();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "call getSkipTimeMillis : skipPosition " + skipPosition);
                    }
                    return skipPosition;
                } else {
                    return -1;
                }
            }

            @Override
            public long[] getSeekPositions() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call getSeekPosition");
                }
                return mThumbNailSeekPosition.getTimeMillisList();
            }

            @Override
            public void getThumbnail(long seekTime, ResultCallback callback) {
                super.getThumbnail(seekTime, callback);
                if (Log.INCLUDE) {
                    Log.d(TAG, "getThumbnail | seekTime : " + seekTime);
                }
                if (seekTime == -1) {
                    return;
                }
                final int index = mThumbNailSeekPosition.getEventIndex(seekTime);
                if (Log.INCLUDE) {
                    Log.d(TAG, "getThumbnail | index : " + index);
                }
                if (index == -1) {
                    return;
                }
                String fileName = mThumbNailSeekPosition.getFileName(index);
                if (fileName.isEmpty()) {
                    return;
                }

                String thumbFilePath = content.getMainStreamInfo().getThumbnailImage().getThumbnailFolder();
                Glide.with(getContext())
                        .load(thumbFilePath + fileName).diskCacheStrategy(DiskCacheStrategy.DATA)
                        .placeholder(R.drawable.detail_series_default).error(R.drawable.detail_series_default)
                        .into(new SimpleTarget<Drawable>() {
                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                callback.onThumbnailLoaded(resource, seekTime);
                            }
                        });
            }

            @Override
            public void reset() {
                super.reset();
            }
        };
        return mPlaybackSeekDataProvider;
    }

    private void startVodPlay(Content content) {
        this.startVodPlay(content, null);
    }

    private void initSeekClient(VideoPlaybackSeekDataProvider mPlaybackSeekDataProvider) {
        setPlaybackSeekUiClient(new VideoPlaybackSeekUi.Client() {

            @Override
            public boolean isSeekEnabled() {
                return true;
            }

            @Override
            public void onSeekStarted() {
                super.onSeekStarted();
            }

            @Override
            public VideoPlaybackSeekDataProvider getPlaybackSeekDataProvider() {
                return mPlaybackSeekDataProvider;
            }

            @Override
            public void onSeekPositionChanged(long pos) {
                super.onSeekPositionChanged(pos);
            }

            @Override
            public void onSeekFinished(boolean cancelled) {
                super.onSeekFinished(cancelled);
            }
        });
        mTransportControlGlue.setSeekProvider(mPlaybackSeekDataProvider);
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mTransportControlGlue != null) {
            mTransportControlGlue.pause();
        }
    }


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        relativeLayout = (RelativeLayout) LayoutInflater.from(getContext()).inflate(
                R.layout.layout_vo_surface, root, false);
        mVideoSurface = relativeLayout.findViewById(R.id.video_surface);
        root.addView(relativeLayout, 0);
        mPlayerAdapter = new MediaPlayerAdapter(getContext(), relativeLayout, mVideoSurface);
        mPlayerAdapter.setVideoInfoView(relativeLayout.findViewById(R.id.videoInfo));
        mPlayerAdapter.insertSubtitleView(relativeLayout.findViewById(R.id.subtitleview_container));
        mVideoSurface.getHolder().setFormat(PixelFormat.RGB_888);
        mVideoSurface.getHolder().addCallback(mPlayerAdapter.getVideoPlayerSurfaceHolderCallback());
        mVideoSurface.getHolder().addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceCreated(holder);
                }
                mState = SURFACE_CREATED;
                mPlayerAdapter.setDisplay(holder);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceChanged(holder, format, width, height);
                }
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (mMediaPlaybackCallback != null) {
                    mMediaPlaybackCallback.surfaceDestroyed(holder);
                }
                mState = SURFACE_NOT_CREATED;
                mPlayerAdapter.setDisplay(null);
            }
        });
        setBlackScreenVisible(true);
        setBackgroundType(VideoPlaybackSupportFragment.BG_NONE);
        return root;
    }

    private long mPlayStartTimeMillis = 0;
    private final int WHAT_FINISH = 1;
    private final long CHECK_PAUSE_TIME = 30 * TimeUtil.MIN;
    private final Handler mPauseHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == WHAT_FINISH) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "call finish");
                }
                onBackPressed();
            }
        }
    };

    private static class SeriesPlayerCallback extends VideoPlaybackGlue.PlayerCallback {
        private final String TAG = SeriesPlayerCallback.class.getSimpleName();
        private static final long REMAIN_TIME = 30 * TimeUtil.SEC;
        private final PlaybackVideoFragment mFragment;
        private final UserPreferenceManagerImp userPreferenceManagerImp;
        private final boolean isOn;

        public SeriesPlayerCallback(PlaybackVideoFragment fragment) {
            super(REMAIN_TIME);
            mFragment = fragment;
            userPreferenceManagerImp = new UserPreferenceManagerImp(mFragment.getContext());
            isOn = userPreferenceManagerImp.getAutoPlay();
        }


        @Override
        public void onReady(boolean ready) {
            super.onReady(ready);
            if (ready) {
                mFragment.getProgressBarManager().hide();
            } else {
                mFragment.getProgressBarManager().show();
            }
        }

        @Override
        public void onPreparedStateChanged(VideoPlaybackGlue glue) {
            super.onPreparedStateChanged(glue);
            if (Log.INCLUDE) {
                Log.d(TAG, "onPrepareStateChanged");
            }
            mFragment.mPlayStartTimeMillis = JasmineEpgApplication.SystemClock().currentTimeMillis();
            mFragment.initializeSubtitleData();
        }

        @Override
        public void onPlayStateChanged(VideoPlaybackGlue glue) {
            super.onPlayStateChanged(glue);
            if (Log.INCLUDE) {
                Log.d(TAG, "onPlayStateChanged");
            }
            mFragment.mPauseHandler.removeMessages(mFragment.WHAT_FINISH);
            if (!glue.isPlaying()) {
                mFragment.mPauseHandler.sendEmptyMessageDelayed(mFragment.WHAT_FINISH,
                        mFragment.CHECK_PAUSE_TIME);
            }
        }

        @Override
        public void onPlayCompleted(VideoPlaybackGlue glue) {
            super.onPlayCompleted(glue);
            if (Log.INCLUDE) {
                Log.d(TAG, "onPlayCompleted");
            }
            glue.pause();

            if (mFragment.checkHasAdvertisement(OpportunityType.PostRoll)) {
                mFragment.startAdvertisement(mFragment.currentContent.getTitle(), mFragment.mPlayerAdapter.getCurrentPosition(),
                        mFragment.currentContent.getRunTime(), mFragment.getAdvertisement(OpportunityType.PostRoll),
                        new AdvertisementVideoFragment.OnAdvertisementCallback() {
                            @Override
                            public void onComplete() {

                                if (mFragment.mEndWatchingVodDialogFragment != null && mFragment.isVisible()) {
                                    mFragment.mEndWatchingVodDialogFragment.onFinishAction(isOn);
                                } else {
                                    mFragment.onPlayCompleteAction(isOn);
                                }
                            }
                        });
            } else {
                UserPreferenceManagerImp userPreferenceManagerImp = new UserPreferenceManagerImp(mFragment.getContext());
                boolean isOn = userPreferenceManagerImp.getAutoPlay();
                if (mFragment.mEndWatchingVodDialogFragment != null && mFragment.isVisible()) {
                    mFragment.mEndWatchingVodDialogFragment.onFinishAction(isOn);
                } else {
                    mFragment.onPlayCompleteAction(isOn);
                }
            }
        }

        @Override
        public void onAlertTime(VideoPlaybackGlue glue) {
            super.onAlertTime(glue);
            if (Log.INCLUDE) {
                Log.d(TAG, "onAlertTime");
            }
            if (!mFragment.isEndDialogShowing) {
                mFragment.showSeriesEndWatchingVodDialog();
            }
        }

        @Override
        public void onUpdateProgress(VideoPlaybackGlue glue, long time, long duration) {
            super.onUpdateProgress(glue, time, duration);
            if (mFragment.mEndWatchingVodDialogFragment != null && isOn) {
                mFragment.mEndWatchingVodDialogFragment.notifyRemainTime(duration - time);
            }
        }
    }

    private static class SinglePlayerCallback extends VideoPlaybackGlue.PlayerCallback {
        private final String TAG = SinglePlayerCallback.class.getSimpleName();
        private final PlaybackVideoFragment mFragment;

        public SinglePlayerCallback(PlaybackVideoFragment fragment) {
            super();
            mFragment = fragment;
        }


        @Override
        public void onPreparedStateChanged(VideoPlaybackGlue glue) {
            super.onPreparedStateChanged(glue);
            if (Log.INCLUDE) {
                Log.d(TAG, "onPrepareStateChanged");
            }
            mFragment.mPlayStartTimeMillis = JasmineEpgApplication.SystemClock().currentTimeMillis();
            mFragment.initializeSubtitleData();
        }

        @Override
        public void onReady(boolean ready) {
            super.onReady(ready);
            if (ready) {
                mFragment.getProgressBarManager().hide();
            } else {
                mFragment.getProgressBarManager().show();
            }
        }


        @Override
        public void onPlayStateChanged(VideoPlaybackGlue glue) {
            super.onPlayStateChanged(glue);
            if (Log.INCLUDE) {
                Log.d(TAG, "onPlayStateChanged | playing : " + glue.isPlaying());
                Log.d(TAG, "mPauseHandler.removeMessages(WHAT_FINISH)");
            }
            mFragment.mPauseHandler.removeMessages(mFragment.WHAT_FINISH);
            if (!glue.isPlaying()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onPlayStateChanged | playing : " + glue.isPlaying());
                    Log.d(TAG, "mPauseHandler.sendEmptyMessageDelayed");
                }
                mFragment.mPauseHandler.sendEmptyMessageDelayed(mFragment.WHAT_FINISH,
                        mFragment.CHECK_PAUSE_TIME);
            }
        }

        @Override
        public void onPlayCompleted(VideoPlaybackGlue glue) {
            super.onPlayCompleted(glue);
            if (mFragment.checkHasAdvertisement(OpportunityType.PostRoll)) {
                mFragment.startAdvertisement(mFragment.currentContent.getTitle(), mFragment.mPlayerAdapter.getCurrentPosition(), mFragment.currentContent.getRunTime(), mFragment.getAdvertisement(OpportunityType.PostRoll),
                        new AdvertisementVideoFragment.OnAdvertisementCallback() {
                            @Override
                            public void onComplete() {
                                mFragment.sendResumeTimeComplete(new Handler(Looper.getMainLooper()) {
                                    @Override
                                    public void handleMessage(@NonNull Message msg) {
                                        super.handleMessage(msg);
                                        mFragment.mVideoPlaybackInteractor.requestVodWatched(mFragment.currentContent);
                                    }
                                });
                            }
                        });
            } else {
                mFragment.sendResumeTimeComplete(new Handler(Looper.getMainLooper()) {
                    @Override
                    public void handleMessage(@NonNull Message msg) {
                        super.handleMessage(msg);
                        mFragment.mVideoPlaybackInteractor.requestVodWatched(mFragment.currentContent);
                    }
                });
            }
        }
    }

    private void startVodPlay(Content content, String thumbNailData) {
        mVideoPlaybackInteractor.setSideOptionProvider(content, mPlayerAdapter);
        final long resumeTime = getArguments().getLong(VideoPlaybackActivity.KEY_START_TIME, 0);
        VideoPlaybackSeekDataProvider mPlaybackSeekDataProvider = null;
        if (thumbNailData != null) {
            try {
                WebvttParser mWebvttParser = new WebvttParser();
                InputStream inputStream = new ByteArrayInputStream(thumbNailData.getBytes(StandardCharsets.UTF_8));
                WebvttSubtitle mWebvttSubtitle = mWebvttParser.parse(inputStream, "UTF-8", 0);
                mPlaybackSeekDataProvider = generateSeekDataProvider(content, mWebvttSubtitle);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            mPlaybackSeekDataProvider = generateSeekDataProvider(content);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "content : " + content.toString());
        }

        VideoPlaybackSupportFragmentGlueHost glueHost =
                new VideoPlaybackSupportFragmentGlueHost(PlaybackVideoFragment.this);

        mTransportControlGlue = new VideoPlaybackTransportControlGlue<>(getContext(), mPlayerAdapter);
        mTransportControlGlue.setSeekEnabled(true);
        if (mPlaybackSeekDataProvider != null) {
            initSeekClient(mPlaybackSeekDataProvider);
        }
        boolean hasNextSeries = currentContent.isSeries() &&
                !StringUtils.nullToEmpty(currentContent.getNextSeriesAssetId()).isEmpty();

        mTransportControlGlue.addPlayerCallback(hasNextSeries ? new SeriesPlayerCallback(this) : new SinglePlayerCallback(this));
        mTransportControlGlue.setHost(glueHost);
        mTransportControlGlue.setTitle(content.getTitle());
        mTransportControlGlue.setSubtitle(content.getSynopsis());

        PlaybackUtil.getPlayResource(getContext(), content, resumeTime, new PlaybackUtil.PlayResourceCallback() {

            @Override
            public void needLoading(boolean loading) {
                super.needLoading(loading);
                if (loading) {
                    getProgressBarManager().show();
                } else {
                    getProgressBarManager().hide();
                }
            }

            @Override
            public void onResult(AltiMediaSource[] mediaSource, List<PlacementDecision> placementDecisions) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "url : " + mediaSource.toString());
                }
                mPlacementDecisions.clear();
                if (isAddsEnable) {
                    mPlacementDecisions.addAll(placementDecisions);
                }
                mPlayerAdapter.setDataSource(mediaSource, resumeTime, !checkHasAdvertisement(OpportunityType.PreRoll));
                if (checkHasAdvertisement(OpportunityType.PreRoll)) {
                    startAdvertisement(content.getTitle(), resumeTime, content.getRunTime(), getAdvertisement(OpportunityType.PreRoll), new AdvertisementVideoFragment.OnAdvertisementCallback() {
                        @Override
                        public void onComplete() {
                            setBlackScreenVisible(false);
                            mPlayerAdapter.requestPlayWhenPause();
                        }
                    });
                } else {
                    setBlackScreenVisible(false);
                }
            }

            @Override
            public void onFail(int reason) {

            }

            @Override
            public void onError(String errorCode, String errorMessage) {
                ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, errorMessage);
                dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        mVideoPlaybackInteractor.onErrorWhenReady(currentContent, errorCode);
                    }
                });
            }
        });
    }


    private void onPlayCompleteAction(boolean isAutoPlay) {
        sendResumeTimeComplete(new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                endWatchingSeriesVodDialog(isAutoPlay);
            }
        });
    }

    private void startAdvertisement(String title, long currentTime, long duration, PlacementDecision placementDecision, AdvertisementVideoFragment.OnAdvertisementCallback onAdvertisementCallback) {
        if (Log.INCLUDE) {
            Log.d(TAG, "startAdvertisement : " + title + ", duration : " + duration);
        }
        if (mPlayerAdapter.isKeyBlock()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "startAdvertisement already started so return");
            }
            return;
        }

        Fragment fragment = getFragmentManager().findFragmentByTag(AdvertisementVideoFragment.CLASS_NAME);
        if (fragment != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "startAdvertisement already showing so return");
            }
            return;
        }

        AdvertisementVideoFragment advertisementVideoFragment = AdvertisementVideoFragment.newInstance(title, currentTime, duration, placementDecision, new AdvertisementVideoFragment.OnAdvertisementListener() {
            @Override
            public void started(PlacementDecision.Placement placement) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "startAdvertisement | started " + placement.toString());
                }
                PlacementDecision.Tracking tracking = placement.getTracking(TrackingEvent.start);
                if (tracking == null) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "startAdvertisement | started but not have start Event so return");
                    }
                    return;
                }
                String url = tracking.getUrl();
                sendLog(url);
            }

            @Override
            public void finished(PlacementDecision.Placement placement, boolean endOfAd, long startTimeMillis) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "startAdvertisement | finished " + placement.toString() + ", endOfAd : " + endOfAd + ", startTimeMillis : " + startTimeMillis);
                }
                PlacementDecision.Tracking complete = placement.getTracking(TrackingEvent.complete);
                if (complete != null) {
                    String url = complete.getUrl();
                    url = url + "&adPlayStartTime" + startTimeMillis + "&adPlayEndTime" + JasmineEpgApplication.SystemClock().currentTimeMillis();
                    sendLog(url);
                }
                PlacementDecision.Tracking impression = placement.getTracking(TrackingEvent.impression);
                if (impression != null) {
                    String url = impression.getUrl();
                    sendLog(url);
                }
            }
        });
        advertisementVideoFragment.setOnAdsListener(new AdvertisementVideoFragment.OnAdsListener() {
            @Override
            public void advertisementShow() {
                mPlayerAdapter.setKeyBlock(true);
            }

            @Override
            public void advertisementComplete(boolean eof) {
                mPlayerAdapter.setKeyBlock(false);
                if (eof) {
                    if (onAdvertisementCallback != null) {
                        onAdvertisementCallback.onComplete();
                    }
                } else {
                    onBackPressed();
                }
            }
        });
        advertisementVideoFragment.show(getFragmentManager(), AdvertisementVideoFragment.CLASS_NAME);
    }

    private void sendLog(String url) {
        if (Log.INCLUDE) {
            Log.d(TAG, "sendLog : " + url);
        }
        AddsReportManager reportManager = new AddsReportManager();
        reportManager.reportAdPlay(url, new MbsDataProvider<String, String>() {
            @Override
            public void needLoading(boolean loading) {

            }

            @Override
            public void onFailed(int reason) {

            }

            @Override
            public void onSuccess(String id, String result) {

            }

            @Override
            public void onError(String errorCode, String message) {

            }
        });
    }

    private void initializeSubtitleData() {
        SettingControl control = SettingControl.getInstance();
        String subtitleFontSize = control.getSubtitleLanguageFontSize();
        mPlayerAdapter.setSubtitleStyleFontCode(subtitleFontSize);
        boolean isOn = control.isSubtitleEnable();
        if (Log.INCLUDE) {
            Log.d(TAG, "initializeSubtitleData : " + isOn);
        }
        if (!isOn) {
            return;
        }
        String subtitleLanguageCode = control.getSubtitleLanguageCode();
        mPlayerAdapter.setSelectedSubtitleTrackByLanguageCode(subtitleLanguageCode);

    }

    private boolean checkHasAdvertisement(OpportunityType type) {
        for (PlacementDecision placementDecision : mPlacementDecisions) {
            if (placementDecision.getOpportunityType() == type) {

                return placementDecision.getPlacement() != null && !placementDecision.getPlacement().isEmpty();
            }
        }
        return false;
    }

    private PlacementDecision getAdvertisement(OpportunityType type) {
        for (PlacementDecision placementDecision : mPlacementDecisions) {
            if (placementDecision.getOpportunityType() == type) {
                return placementDecision;
            }
        }
        return null;
    }

    private void loadThumbnail(Content content) {
        ThumbnailInfo thumbnailInfo = content.getMainStreamInfo().getThumbnailImage();
        if (thumbnailInfo == null) {
            startVodPlay(content);
            return;
        }
        ContentDataManager contentDataManager = new ContentDataManager();
        contentDataManager.getThumbnailData(thumbnailInfo.getThumbnailRootUrl(), new MbsDataProvider<String, String>() {
            @Override
            public void needLoading(boolean loading) {
            }

            @Override
            public void onFailed(int key) {
                startVodPlay(content);
            }

            @Override
            public void onSuccess(String id, String result) {
                startVodPlay(content, result);
            }

            @Override
            public void onError(String errorCode, String message) {
                startVodPlay(content);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isEndDialogShowing = false;
        final Content content =
                getArguments().getParcelable(VideoPlaybackActivity.KEY_MOVIE);
        currentContent = content;
        loadThumbnail(content);
    }

    private static class ThumbNailSeekPosition {
        private final ArrayList<Long> realTimeMicro;
        private final int length;
        private final WebvttSubtitle mWebvttSubtitle;
        private long[] timeMillis;
        private ArrayList<Long> seekPosition;
        private final long TIME_DIFFER_MICRO_MILLIS = TimeUtil.MIN * 1000;

        public ThumbNailSeekPosition(WebvttSubtitle webvttSubtitle) {
            length = webvttSubtitle == null ? 0 : webvttSubtitle.getEventTimeCount();
            mWebvttSubtitle = webvttSubtitle;
            realTimeMicro = new ArrayList<>();

            initialData(webvttSubtitle);
        }

        private void initialData(WebvttSubtitle webvttSubtitle) {
            for (int i = 0; i < length; i++) {
                long eventTime = webvttSubtitle.getEventTime(i);
                if (!needAddSeekTime(eventTime)) {
                    continue;
                }
                realTimeMicro.add(eventTime);
            }

            timeMillis = new long[realTimeMicro.size()];
            seekPosition = new ArrayList<>(realTimeMicro.size());
            for (int i = 0; i < realTimeMicro.size(); i++) {
                timeMillis[i] = realTimeMicro.get(i) / 1000;
                seekPosition.add(timeMillis[i]);
            }

        }

        private boolean needAddSeekTime(long microMillis) {
            int lastIndex = realTimeMicro.size() - 1;
            if (lastIndex < 0) {
                return true;
            }
            long differTime = Math.abs(realTimeMicro.get(lastIndex) - microMillis);
            return differTime >= TIME_DIFFER_MICRO_MILLIS;
        }

        public long[] getTimeMillisList() {
            return timeMillis;
        }

        public long getTimeMillis(int index) {
            return timeMillis[index];
        }

        public int getLength() {
            return length;
        }

        public long getEventTime(int index) {
            return realTimeMicro.get(index);
        }

        public int getEventIndex(long seekTime) {
            return seekPosition.indexOf(seekTime);
        }

        public String getFileName(int index) {
            long eventTime = getEventTime(index);
            String eventText = mWebvttSubtitle.getText(eventTime);
            if (StringUtils.nullToEmpty(eventText).isEmpty()) {
                return "";
            }
            if (eventText.split("#") == null || eventText.split("#").length == 0) {
                return "";
            }
            String fileLocate = eventText.split("#")[0];
            return fileLocate;
        }
    }


    @Override
    protected boolean isPlaying() {
        return mPlayerAdapter.isPlaying();
    }

    @Override
    protected boolean isPrepared() {
        return mPlayerAdapter.isPrepared();
    }

    /**
     * Adds {@link SurfaceHolder.Callback} to {@link android.view.SurfaceView}.
     */

    @Override
    public void onVideoSizeChanged(int width, int height) {
        int screenWidth = getView().getWidth();
        int screenHeight = getView().getHeight();

        ViewGroup.LayoutParams p = mVideoSurface.getLayoutParams();
        if (screenWidth * height > width * screenHeight) {
            // fit in screen height
            p.height = screenHeight;
            p.width = screenHeight * width / height;
        } else {
            // fit in screen width
            p.width = screenWidth;
            p.height = screenWidth * height / width;
        }
        mVideoSurface.setLayoutParams(p);
    }

    /**
     * Returns the surface view.
     */
    public SurfaceView getSurfaceView() {
        return mVideoSurface;
    }

    @Override
    public void onDestroyView() {
        if (mTransportControlGlue != null) {
            mTransportControlGlue.clearPlayerCallback();
        }
        mPlayerAdapter.release();
        mVideoSurface = null;
        mState = SURFACE_NOT_CREATED;
        mPauseHandler.removeMessages(WHAT_FINISH);
        super.onDestroyView();

        for (WeakReference<AsyncTask> task : taskList) {
            if (task.get() != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "canceled");
                }
                task.get().cancel(true);
            }
        }
        taskList.clear();
        if (mEndWatchingVodDialogFragment != null) {
            mEndWatchingVodDialogFragment.dismiss();
        }
    }

    private void getContentsDetail(String id, String categoryId, MbsDataProvider<String, ContentDetailInfo> provider) {
        AccountManager accountManager = AccountManager.getInstance();
        ContentDataManager contentDataManager = new ContentDataManager();
        contentDataManager.getContentDetail(accountManager.getLocalLanguage(), accountManager.getSaId(),
                accountManager.getProfileId(), id, categoryId, provider);
    }

    private void showSeriesEndWatchingVodDialog() {
        isEndDialogShowing = true;
        boolean hasNextSeries = currentContent.isSeries() &&
                !StringUtils.nullToEmpty(currentContent.getNextSeriesAssetId()).isEmpty();
        if (!hasNextSeries) {
            return;
        }
        getContentsDetail(currentContent.getNextSeriesAssetId(), currentContent.getCategoryId(), new MbsDataProvider<String, ContentDetailInfo>() {
            @Override
            public void needLoading(boolean loading) {
                if (loading) {
                    getProgressBarManager().show();
                } else {
                    getProgressBarManager().hide();
                }
            }

            @Override
            public void onFailed(int key) {

            }

            @Override
            public void onSuccess(String id, ContentDetailInfo result) {
                Content nextContent = result.getContent();
                requestTryNextContents(currentContent, nextContent, true);
            }

            @Override
            public void onError(String errorCode, String message) {

            }
        });
    }

    private boolean isWatchable(Content content) {
        List<Product> productList = content.getProductList();
        if (productList == null) {
            return false;
        }
        boolean watchable = false;
        for (Product product : productList) {
            if (product.isPurchased() || product.getPrice() == 0) {
                watchable = true;
                break;
            }
        }
        return watchable;
    }

    private void endWatchingSeriesVodDialog(boolean isAutoPlay) {
        boolean hasNextSeries = currentContent.isSeries() &&
                !StringUtils.nullToEmpty(currentContent.getNextSeriesAssetId()).isEmpty();
        if (!hasNextSeries) {
            return;
        }
        mPlayerAdapter.pause();
        getContentsDetail(currentContent.getNextSeriesAssetId(), currentContent.getCategoryId(), new MbsDataProvider<String, ContentDetailInfo>() {
            @Override
            public void needLoading(boolean loading) {
                if (loading) {
                    getProgressBarManager().show();
                } else {
                    getProgressBarManager().hide();
                }
            }

            @Override
            public void onFailed(int key) {
                mVideoPlaybackInteractor.requestVodWatched(currentContent);
            }

            @Override
            public void onSuccess(String id, ContentDetailInfo result) {
                Content nextContent = result.getContent();
                if (isAutoPlay) {
                    if (isWatchable(nextContent)) {
                        mVideoPlaybackInteractor.requestTuneVod(nextContent);
                    } else {
                        mVideoPlaybackInteractor.requestShowVodDetail(nextContent);
                    }
                } else {
                    mVideoPlaybackInteractor.requestVodWatched(currentContent);
                }
            }

            @Override
            public void onError(String errorCode, String message) {
                mVideoPlaybackInteractor.requestVodWatched(currentContent);
            }
        });
    }


    private EndWatchingVodDialogFragment mEndWatchingVodDialogFragment = null;

    private void requestTryNextContents(Content content, Content nextContents,
                                        boolean isEof) {
        mEndWatchingVodDialogFragment =
                EndWatchingVodDialogFragment.newInstance(content, nextContents, isEof);
        mEndWatchingVodDialogFragment.setOnVodWatchEndListener(this);
        mEndWatchingVodDialogFragment.setOnDismissListener(new EndWatchingVodDialogFragment.OnDismissListener() {
            @Override
            public void onDismiss() {
                isEndDialogShowing = false;
                mEndWatchingVodDialogFragment = null;
            }
        });
        mEndWatchingVodDialogFragment.show(getFragmentManager(), EndWatchingVodDialogFragment.CLASS_NAME);
    }

    private void sendResumeTimeComplete(Handler handler) {
        sendResumeTime(0, handler);
    }

    private final List<WeakReference<AsyncTask>> taskList = new ArrayList<>();

    private void sendResumeTime(long time, Handler handler) {
        if (!taskList.isEmpty()) {
            return;
        }
        if (time < 0) {
            if (handler != null) {
                handler.sendEmptyMessage(0);
            }
            return;
        }
        AccountManager accountManager = AccountManager.getInstance();
        UserDataManager userDataManager = new UserDataManager();
        AsyncTask mbsDataTask = userDataManager.postResumeTime(accountManager.getSaId(),
                accountManager.getProfileId(),
                currentContent.getContentGroupId(),
                time, mPlayStartTimeMillis, JasmineEpgApplication.SystemClock().currentTimeMillis(), new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            getProgressBarManager().show();
                        } else {
                            getProgressBarManager().hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {

                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "result : " + result.toString());
                        }
                        if (handler != null) {
                            handler.sendEmptyMessage(0);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (handler != null) {
                            handler.sendEmptyMessage(0);
                        }
                    }
                });
        taskList.add(new WeakReference<>(mbsDataTask));
    }

    public void onBackPressed() {
        long currentPosition = mPlayerAdapter.getCurrentPosition();
        sendResumeTime(currentPosition, new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                mVideoPlaybackInteractor.requestVodWatched(currentContent);
            }
        });
    }

    @Override
    public void requestCancel(DialogFragment dialogFragment, VodEndActionPresenter.FinishType type) {
        EndWatchingVodDialogFragment endWatchingVodDialogFragment =
                (EndWatchingVodDialogFragment) dialogFragment;
        endWatchingVodDialogFragment.dismissWithCallback();
    }

    @Override
    public void requestFinish(DialogFragment dialogFragment, Content targetContent, VodEndActionPresenter.FinishType type) {
        mPlayerAdapter.pause();
        sendResumeTimeComplete(new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                dialogFragment.dismiss();
                switch (type) {
                    case Exit:
                        mVideoPlaybackInteractor.requestVodWatched(currentContent);
                        break;
                    case NextEpisodeDetail:
                        targetContent.setLike(currentContent.isLike());
                        mVideoPlaybackInteractor.requestShowVodDetail(targetContent);
                        break;
                    case TuneNextEpisode:
                        targetContent.setLike(currentContent.isLike());
                        mVideoPlaybackInteractor.requestTuneVod(targetContent);
                        break;
                }
            }
        });
    }

    @Override
    public void onError(int errorCode, CharSequence errorMessage) {
        super.onError(errorCode, errorMessage);

        String title = ErrorMessageManager.getInstance().getPlayerErrorCode(getContext(), errorCode);
        ErrorDialogFragment dialogFragment = ErrorDialogFragment.newInstance(TAG, title,
                ErrorMessageManager.getInstance().getErrorMessage(title));
        dialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
        dialogFragment.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mVideoPlaybackInteractor.requestVodWatched(currentContent);
            }
        });
    }

    @Override
    protected boolean checkEnableSidePanel() {
//        ArrayList<String> subtitleList = mPlayerAdapter.getSubtitleList();
//        ArrayList<String> audioList = mPlayerAdapter.getAudioList();
//        if (Log.INCLUDE) {
//            Log.d(TAG, "checkEnableSidePanel : subtitleList : " + subtitleList.size());
//            Log.d(TAG, "checkEnableSidePanel : audioList : " + audioList.size());
//        }
//        boolean disable = subtitleList.isEmpty() && audioList.isEmpty();
//        return !disable;
        return true;
    }

    @Override
    protected boolean isKeyBlock() {
        return !mPlayerAdapter.isReady();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}