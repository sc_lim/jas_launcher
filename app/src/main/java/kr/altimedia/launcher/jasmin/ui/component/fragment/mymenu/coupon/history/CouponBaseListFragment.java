/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history;

import android.view.KeyEvent;
import android.view.View;

import com.altimedia.util.Log;

import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.adapter.CouponListRowBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.detail.CouponDetailDialog;
import kr.altimedia.launcher.jasmin.ui.view.common.PagingVerticalGridView;

public class CouponBaseListFragment extends Fragment implements CouponListRowBridgeAdapter.OnKeyListener {
    private final String TAG = CouponBaseListFragment.class.getSimpleName();

    protected PagingVerticalGridView gridView;
    protected OnVisibleCompleteListener onVisibleCompleteListener;
    private OnFocusRequestListener onFocusRequestListener;

    public CouponBaseListFragment() {
    }

    public void setOnFocusRequestListener(OnFocusRequestListener onFocusRequestListener) {
        this.onFocusRequestListener = onFocusRequestListener;
    }

    public void setOnVisibleCompleteListener(OnVisibleCompleteListener onVisibleCompleteListener) {
        this.onVisibleCompleteListener = onVisibleCompleteListener;
    }

    protected int getSelectedPosition() {
        return 0;
    }

    protected int getTotalSize() {
        return 0;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onFocusRequestListener = null;
    }

    @Override
    public boolean onKey(int keyCode, Coupon coupon) {
        int selectedIndex = getSelectedPosition();
        if(Log.INCLUDE){
            Log.d(TAG, "onKey: keyCode="+keyCode+", selectedIndex="+selectedIndex);
        }
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                return true;
            case KeyEvent.KEYCODE_DPAD_UP: {
                if (gridView != null) {
                    if (selectedIndex == 0) {
                        if (onFocusRequestListener != null) {
                            onFocusRequestListener.onFocusRequest(View.FOCUS_UP);
                        }
                        return true;
                    }
                }
                break;
            }
            case KeyEvent.KEYCODE_DPAD_DOWN: {
                if (gridView != null) {
                    int size = getTotalSize();
                    if (selectedIndex == (size - 1)) {
                        gridView.scrollToPosition(0);
                        return true;
                    }
                }
                break;
            }
            case KeyEvent.KEYCODE_DPAD_CENTER:
                CouponDetailDialog couponDetailDialog = CouponDetailDialog.newInstance(coupon);
                couponDetailDialog.show(getFragmentManager(), CouponDetailDialog.CLASS_NAME);
                return true;
        }

        return false;
    }

    public interface OnFocusRequestListener {
        void onFocusRequest(int direction);
    }

    public interface OnVisibleCompleteListener {
        void onVisibleComplete();
    }
}
