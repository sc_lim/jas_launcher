/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj;

import android.os.Parcel;
import android.os.Parcelable;

import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;

public class PaymentButton implements Parcelable {
    private PaymentType type;
    private boolean isEnable = true;

    public PaymentButton(PaymentType type, boolean isEnable) {
        this.type = type;
        this.isEnable = isEnable;
    }

    protected PaymentButton(Parcel in) {
        type = (PaymentType) in.readValue(PaymentType.class.getClassLoader());
        isEnable = in.readByte() != 0;
    }

    public static final Creator<PaymentButton> CREATOR = new Creator<PaymentButton>() {
        @Override
        public PaymentButton createFromParcel(Parcel in) {
            return new PaymentButton(in);
        }

        @Override
        public PaymentButton[] newArray(int size) {
            return new PaymentButton[size];
        }
    };

    public PaymentType getType() {
        return type;
    }

    public boolean isEnable() {
        return isEnable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(type);
        dest.writeByte((byte) (isEnable ? 1 : 0));
    }
}
