package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.os.Bundle;
import android.view.View;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentCreditCardFragment.KEY_OTHER_CARD_LIST;
import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentCreditCardFragment.LIST_NUMBER_COLUMNS;
import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentCreditCardFragment.LIST_PAGE_ITEMS;
import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment.VodPaymentCreditCardFragment.MAIN_PAGE_ITEMS;

public class CreditCardMainPagerFragment extends CreditCardPageBaseFragment {
    private final String TAG = CreditCardMainPagerFragment.class.getSimpleName();

    public static final String KEY_PAGE_LIST = "PAGE_LIST";
    private static final int NUMBER_ROWS = VodPaymentCreditCardFragment.LIST_NUMBER_ROWS;

    private ArrayList<CreditCard> otherCardList;
    private ArrayList<CreditCard> creditCardList;
    private HorizontalGridView gridView;
    private VerticalGridView otherGridView;

    public static CreditCardMainPagerFragment newInstance(ArrayList<CreditCard> otherCardList, ArrayList<CreditCard> creditCardList) {

        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_OTHER_CARD_LIST, otherCardList);
        args.putParcelableArrayList(KEY_PAGE_LIST, creditCardList);

        CreditCardMainPagerFragment fragment = new CreditCardMainPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getResourceLayoutId() {
        return R.layout.fragment_credit_card_main_pager;
    }

    @Override
    protected void initView(View view) {
        otherCardList = getArguments().getParcelableArrayList(KEY_OTHER_CARD_LIST);
        creditCardList = getArguments().getParcelableArrayList(KEY_PAGE_LIST);

        if (Log.INCLUDE) {
            Log.d(TAG, "otherCardList : " + otherCardList.size() + ", creditCardList : " + creditCardList.size());
        }

        initSideCardButtons(view);
        initCreditCardGridView(view);
    }

    private void initSideCardButtons(View view) {
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(new CreditCardOtherPresenter());
        objectAdapter.addAll(0, otherCardList);
        ItemBridgeAdapter bridgeAdapter = new ItemBridgeAdapter(objectAdapter);

        otherGridView = view.findViewById(R.id.side_gridView);
        otherGridView.setAdapter(bridgeAdapter);
    }

    private void initCreditCardGridView(View view) {
        ArrayObjectAdapter objectAdapter = new ArrayObjectAdapter(new CreditCardPresenter());
        objectAdapter.addAll(0, creditCardList);

        gridView = view.findViewById(R.id.gridView);
        gridView.setNumRows(NUMBER_ROWS);

        ItemBridgeAdapter bridgeAdapter = new ItemBridgeAdapter(objectAdapter);
        gridView.setAdapter(bridgeAdapter);

        if (creditCardList.size() <= 0) {
            gridView.setFocusable(false);
            gridView.setFocusableInTouchMode(false);
            view.findViewById(R.id.registered_layout).setVisibility(View.INVISIBLE);
        }
    }

    public int getOtherCardFocusedPosition() {
        View view = otherGridView.getFocusedChild();
        return view != null ? otherGridView.getChildAdapterPosition(view) : -1;
    }

    @Override
    public int getSize() {
        return creditCardList.size();
    }

    @Override
    public int getFocusedPosition() {
        View view = gridView.getFocusedChild();
        return view != null ? gridView.getChildAdapterPosition(view) : -1;
    }

    @Override
    public CreditCard getItem(int position) {
        int index = getOtherCardFocusedPosition();
        if (index != -1) {
            return otherCardList.get(index);
        }

        index = getFocusedPosition();
        if (index != -1) {
            return creditCardList.get(position);
        }

        return null;
    }

    @Override
    public void requestPosition(int position) {
        position -= (LIST_PAGE_ITEMS - MAIN_PAGE_ITEMS);

        if (gridView != null) {
            gridView.setSelectedPosition(position);
            gridView.requestFocus();
        }
    }

    @Override
    public boolean isLastColumn(int position) {
        int size = creditCardList.size();
        int lastColumn = size / LIST_NUMBER_COLUMNS;
        if (size % LIST_NUMBER_COLUMNS > 0) {
            lastColumn++;
        }

        position += 1;
        int row = position / LIST_NUMBER_COLUMNS;
        if (position % NUMBER_ROWS > 0) {
            row++;
        }

        return row == lastColumn;
    }
}
