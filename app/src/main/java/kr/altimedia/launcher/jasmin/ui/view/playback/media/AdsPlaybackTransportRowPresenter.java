/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.ColorInt;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.leanback.widget.Action;
import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.OnActionClickedListener;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoControlBarPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackTransportRowView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;

/**
 * Created by mc.kim on 29,04,2020
 */
public class AdsPlaybackTransportRowPresenter extends VideoPlaybackRowPresenter {
    private final String TAG = AdsPlaybackTransportRowPresenter.class.getSimpleName();
    public static final long SEEK_DIFFER_TIME = 10 * 1000;

    static class BoundData extends VideoPlaybackControlsPresenter.BoundData {
        ViewHolder mRowViewHolder;
    }

    protected void onProgressBarKey(ViewHolder vh, KeyEvent keyEvent) {

    }


    /**
     * A ViewHolder for the PlaybackControlsRow supporting seek UI.
     */
    public class ViewHolder extends VideoPlaybackRowPresenter.ViewHolder {
        final Presenter.ViewHolder mDescriptionViewHolder;
        final View mRootView;
        final ImageView mImageView;
        final ViewGroup mDescriptionDock;
        final ViewGroup mControlsDock;
        final ViewGroup mSecondaryControlsDock;
        final TextView mTotalTime;
        final TextView mCurrentTime;
        final AppCompatSeekBar mProgressBar;
        long mTotalTimeInMs = Long.MIN_VALUE;
        long mCurrentTimeInMs = Long.MIN_VALUE;
        long mSecondaryProgressInMs;
        final StringBuilder mTempBuilder = new StringBuilder();
        VideoControlBarPresenter.ViewHolder mControlsVh;
        VideoControlBarPresenter.ViewHolder mSecondaryControlsVh;
        BoundData mControlsBoundData = new BoundData();
        BoundData mSecondaryBoundData = new BoundData();
        Presenter.ViewHolder mSelectedViewHolder;
        Object mSelectedItem;
        VideoPlaybackControlsRow.PlayPauseAction mPlayPauseAction;
        final VideoPlaybackControlsRow.OnPlaybackProgressCallback mListener =
                new VideoPlaybackControlsRow.OnPlaybackProgressCallback() {

                    @Override
                    public void onReadyStateChanged(VideoPlaybackControlsRow row, boolean ready) {
                        super.onReadyStateChanged(row, ready);
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onReadyStateChanged : " + ready);
                        }
                        mRootView.setVisibility(ready ? View.VISIBLE : View.INVISIBLE);
                    }

                    @Override
                    public void onCurrentPositionChanged(VideoPlaybackControlsRow row, long ms, boolean seeking) {
//                        setCurrentPosition(ms);
                    }

                    @Override
                    public void onDurationChanged(VideoPlaybackControlsRow row, long ms) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "onDurationChanged  : " + ms);
                        }

//                        setTotalTime(ms);
                    }

                    @Override
                    public void onBufferedPositionChanged(VideoPlaybackControlsRow row, long ms) {
                        setBufferedPosition(ms);
                    }
                };


        boolean onForward() {
            long seekTimeInMs = mCurrentTimeInMs + SEEK_DIFFER_TIME;
            if (seekTimeInMs > mTotalTimeInMs) {
                seekTimeInMs = mTotalTimeInMs;
            }
            seekTime(seekTimeInMs);
            return true;
        }

        boolean onBackward() {
            long seekTimeInMs = mCurrentTimeInMs - SEEK_DIFFER_TIME;
            if (seekTimeInMs < 0) {
                seekTimeInMs = 0;
            }
            seekTime(seekTimeInMs);
            return true;
        }

        public void setCurrentTimeInMs(long currentTimeInMs) {
            this.mCurrentTimeInMs = currentTimeInMs;
        }

        public ViewHolder(View rootView, Presenter descriptionPresenter) {
            super(rootView);
            mRootView = rootView;
            mImageView = rootView.findViewById(R.id.image);
            mDescriptionDock = rootView.findViewById(R.id.description_dock);
            mCurrentTime = rootView.findViewById(R.id.current_time);
            mTotalTime = rootView.findViewById(R.id.total_time);
            mProgressBar = rootView.findViewById(R.id.playback_progress);
            mProgressBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onProgressBarClicked(ViewHolder.this);
                }
            });
            OnProgressKeyListener mOnProgressKeyListener = new OnProgressKeyListener(this);
            mProgressBar.setOnKeyListener(mOnProgressKeyListener);
            mProgressBar.setMax(Integer.MAX_VALUE); //current progress will be a fraction of this
            mControlsDock = rootView.findViewById(R.id.controls_dock);
            mSecondaryControlsDock =
                    rootView.findViewById(R.id.secondary_controls_dock);
            mDescriptionViewHolder = descriptionPresenter == null ? null :
                    descriptionPresenter.onCreateViewHolder(mDescriptionDock);
            if (mDescriptionViewHolder != null) {
                mDescriptionDock.addView(mDescriptionViewHolder.view);
            }
            mRootView.setVisibility(View.INVISIBLE);
        }


        public final Presenter.ViewHolder getDescriptionViewHolder() {
            return mDescriptionViewHolder;
        }


        boolean onProgressbarKeyPressed(ViewHolder vh, KeyEvent keyEvent) {
            onProgressBarKey(vh, keyEvent);
            return true;
        }

        private void seekTime(long time) {
            onItemSelected(time);
        }

        public void onItemSelected(long time) {
        }


        void dispatchItemSelection() {
            if (!isSelected()) {
                return;
            }
            if (mSelectedViewHolder == null) {
                if (getOnItemViewSelectedListener() != null) {
                    getOnItemViewSelectedListener().onItemSelected(null, null,
                            ViewHolder.this, getRow());
                }
            } else {
                if (getOnItemViewSelectedListener() != null) {
                    getOnItemViewSelectedListener().onItemSelected(mSelectedViewHolder,
                            mSelectedItem, ViewHolder.this, getRow());
                }
            }
        }

        Presenter getPresenter(boolean primary) {
            ObjectAdapter adapter = primary
                    ? ((VideoPlaybackControlsRow) getRow()).getPrimaryActionsAdapter()
                    : ((VideoPlaybackControlsRow) getRow()).getSecondaryActionsAdapter();
            if (adapter == null) {
                return null;
            }
            if (adapter.getPresenterSelector() instanceof ControlButtonPresenterSelector) {
                ControlButtonPresenterSelector selector =
                        (ControlButtonPresenterSelector) adapter.getPresenterSelector();
                return selector.getPrimaryPresenter();
            }
            return adapter.getPresenter(adapter.size() > 0 ? adapter.get(0) : null);
        }

        /**
         * Returns the TextView that showing total time label. This method might be used in
         * {@link #onSetDurationLabel}.
         *
         * @return The TextView that showing total time label.
         */
        public final TextView getDurationView() {
            return mTotalTime;
        }

        /**
         * Called to update total time label. Default implementation updates the TextView
         * {@link #getDurationView()}. Subclass might override.
         *
         * @param totalTimeMs Total duration of the media in milliseconds.
         */
        protected void onSetDurationLabel(long totalTimeMs) {
            if (mTotalTime != null) {
                formatTime(totalTimeMs, mTempBuilder);
                mTotalTime.setText(mTempBuilder.toString());
            }
        }

        void setTotalTime(long totalTimeMs) {
            if (mTotalTimeInMs != totalTimeMs) {
                mTotalTimeInMs = totalTimeMs;
                onSetDurationLabel(totalTimeMs);
            }
        }

        /**
         * Returns the TextView that showing current position label. This method might be used in
         * {@link #onSetCurrentPositionLabel}.
         *
         * @return The TextView that showing current position label.
         */
        public final TextView getCurrentPositionView() {
            return mCurrentTime;
        }

        /**
         * Called to update current time label. Default implementation updates the TextView
         * {@link #getCurrentPositionView}. Subclass might override.
         *
         * @param currentTimeMs Current playback position in milliseconds.
         */
        protected void onSetCurrentPositionLabel(long currentTimeMs) {
            if (mCurrentTime != null) {
                formatTime(currentTimeMs, mTempBuilder);
                mCurrentTime.setText(mTempBuilder.toString());
            }
        }

        void setCurrentPosition(long currentTimeMs) {
            boolean needToUpdate = false;
            if (currentTimeMs != mCurrentTimeInMs) {
                needToUpdate = true;
                setCurrentTimeInMs(currentTimeMs);
            }

            if (!needToUpdate) {
                return;
            }
            if (currentTimeMs == -1) {
                return;
            }
            onSetCurrentPositionLabel(currentTimeMs);
            int progressRatio = 0;
            if (mTotalTimeInMs > 0) {
                // Use ratio to represent current progres
                double ratio = (double) mCurrentTimeInMs / mTotalTimeInMs;     // Range: [0, 1]
                progressRatio = (int) (ratio * Integer.MAX_VALUE);  // Could safely cast to int
            }
            mProgressBar.setProgress(progressRatio);
            mProgressBar.setSecondaryProgress(progressRatio);
        }

        void setBufferedPosition(long progressMs) {
            mSecondaryProgressInMs = progressMs;
            // Solve the progress bar by using ratio
            double ratio = (double) progressMs / mTotalTimeInMs;           // Range: [0, 1]
            double progressRatio = ratio * Integer.MAX_VALUE;   // Could safely cast to int
        }
    }


    static void formatTime(long ms, StringBuilder sb) {
        sb.setLength(0);
        if (ms < 0) {
            sb.append("--");
            return;
        }
        long seconds = ms / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        seconds -= minutes * 60;
        minutes -= hours * 60;

        if (hours < 10) {
            sb.append('0');
        }
        sb.append(hours).append(':');

        if (minutes < 10) {
            sb.append('0');
        }

        sb.append(minutes).append(':');
        if (seconds < 10) {
            sb.append('0');
        }
        sb.append(seconds);
    }

    float mDefaultSeekIncrement = 0.01f;
    int mProgressColor = Color.TRANSPARENT;
    int mSecondaryProgressColor = Color.TRANSPARENT;
    boolean mProgressColorSet;
    boolean mSecondaryProgressColorSet;
    Presenter mDescriptionPresenter;
    VideoControlBarPresenter mPlaybackControlsPresenter;
    VideoControlBarPresenter mSecondaryControlsPresenter;
    OnActionClickedListener mOnActionClickedListener;

    private final VideoControlBarPresenter.OnControlSelectedListener mOnControlSelectedListener =
            new VideoControlBarPresenter.OnControlSelectedListener() {
                @Override
                public void onControlSelected(Presenter.ViewHolder itemViewHolder, Object item,
                                              VideoControlBarPresenter.BoundData data) {
                    ViewHolder vh = ((BoundData) data).mRowViewHolder;
                    if (vh.mSelectedViewHolder != itemViewHolder || vh.mSelectedItem != item) {
                        vh.mSelectedViewHolder = itemViewHolder;
                        vh.mSelectedItem = item;
                        vh.dispatchItemSelection();
                    }
                }
            };

    private final VideoControlBarPresenter.OnControlClickedListener mOnControlClickedListener =
            new VideoControlBarPresenter.OnControlClickedListener() {
                @Override
                public void onControlClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                             VideoControlBarPresenter.BoundData data) {
                    ViewHolder vh = ((BoundData) data).mRowViewHolder;
                    if (vh.getOnItemViewClickedListener() != null) {
                        vh.getOnItemViewClickedListener().onItemClicked(itemViewHolder, item,
                                vh, vh.getRow());
                    }
                    if (mOnActionClickedListener != null && item instanceof Action) {
                        mOnActionClickedListener.onActionClicked((Action) item);
                    }
                }
            };
    private final long mCurrentTime;
    private final long mDuration;

    public AdsPlaybackTransportRowPresenter(long currentTime, long duration) {
        setHeaderPresenter(null);
        setSelectEffectEnabled(false);
        mCurrentTime = currentTime;
        mDuration = duration;
        mPlaybackControlsPresenter = new VideoControlBarPresenter(R.layout.widget_video_control_bar);
        mPlaybackControlsPresenter.setDefaultFocusToMiddle(false);
        mSecondaryControlsPresenter = new VideoControlBarPresenter(R.layout.widget_video_control_bar);
        mSecondaryControlsPresenter.setDefaultFocusToMiddle(false);

        mPlaybackControlsPresenter.setOnControlSelectedListener(mOnControlSelectedListener);
        mSecondaryControlsPresenter.setOnControlSelectedListener(mOnControlSelectedListener);
        mPlaybackControlsPresenter.setOnControlClickedListener(mOnControlClickedListener);
        mSecondaryControlsPresenter.setOnControlClickedListener(mOnControlClickedListener);
    }

    /**
     * @param descriptionPresenter Presenter for displaying item details.
     */
    public void setDescriptionPresenter(Presenter descriptionPresenter) {
        mDescriptionPresenter = descriptionPresenter;
    }

    public void setOnActionClickedListener(OnActionClickedListener listener) {
        mOnActionClickedListener = listener;
    }

    /**
     * Returns the listener for {@link Action} click events.
     */
    public OnActionClickedListener getOnActionClickedListener() {
        return mOnActionClickedListener;
    }

    /**
     * Sets the primary color for the progress bar.  If not set, a default from
     * the theme will be used.
     */
    public void setProgressColor(@ColorInt int color) {
        mProgressColor = color;
        mProgressColorSet = true;
    }

    /**
     * Returns the primary color for the progress bar.  If no color was set, transparent
     * is returned.
     */
    @ColorInt
    public int getProgressColor() {
        return mProgressColor;
    }

    /**
     * Sets the secondary color for the progress bar.  If not set, a default from
     * the theme {@link R.attr#playbackProgressSecondaryColor} will be used.
     *
     * @param color Color used to draw secondary progress.
     */
    public void setSecondaryProgressColor(@ColorInt int color) {
        mSecondaryProgressColor = color;
        mSecondaryProgressColorSet = true;
    }

    /**
     * Returns the secondary color for the progress bar.  If no color was set, transparent
     * is returned.
     */
    @ColorInt
    public int getSecondaryProgressColor() {
        return mSecondaryProgressColor;
    }

    @Override
    public void setEnable(RowPresenter.ViewHolder rowViewHolder, boolean enable) {

    }

    @Override
    public void onReappear(RowPresenter.ViewHolder rowViewHolder) {
        ViewHolder vh = (ViewHolder) rowViewHolder;
        if (vh.view.hasFocus()) {
            vh.mDescriptionDock.requestFocus();
        }
    }


    private static int getDefaultProgressColor(Context context) {
        TypedValue outValue = new TypedValue();
        if (context.getTheme()
                .resolveAttribute(R.attr.playbackProgressPrimaryColor, outValue, true)) {
            return context.getResources().getColor(outValue.resourceId);
        }
        return context.getResources().getColor(R.color.lb_playback_progress_color_no_theme);
    }

    private static int getDefaultSecondaryProgressColor(Context context) {
        TypedValue outValue = new TypedValue();
        if (context.getTheme()
                .resolveAttribute(R.attr.playbackProgressSecondaryColor, outValue, true)) {
            return context.getResources().getColor(outValue.resourceId);
        }
        return context.getResources().getColor(
                R.color.lb_playback_progress_secondary_color_no_theme);
    }

    @Override
    protected RowPresenter.ViewHolder createRowViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_video_ads_transport_controls_row, parent, false);
        ViewHolder vh = new ViewHolder(v, mDescriptionPresenter);
        initRow(vh);
        return vh;
    }

    private void initRow(final ViewHolder vh) {
//        vh.mProgressBar.setProgressColor(mProgressColorSet ? mProgressColor
//                : getDefaultProgressColor(vh.mControlsDock.getContext()));
        vh.mControlsVh = (VideoControlBarPresenter.ViewHolder) mPlaybackControlsPresenter
                .onCreateViewHolder(vh.mControlsDock);
//        vh.mProgressBar.setSecondaryProgressColor(mSecondaryProgressColorSet
//                ? mSecondaryProgressColor
//                : getDefaultSecondaryProgressColor(vh.mControlsDock.getContext()));
        vh.mControlsDock.addView(vh.mControlsVh.view);

        vh.mSecondaryControlsVh = (VideoControlBarPresenter.ViewHolder) mSecondaryControlsPresenter
                .onCreateViewHolder(vh.mSecondaryControlsDock);
        vh.mSecondaryControlsDock.addView(vh.mSecondaryControlsVh.view);
        ((VideoPlaybackTransportRowView) vh.view.findViewById(R.id.transport_row))
                .setOnUnhandledKeyListener(new VideoPlaybackTransportRowView.OnUnhandledKeyListener() {
                    @Override
                    public boolean onUnhandledKey(KeyEvent event) {
                        if (vh.getOnKeyListener() != null) {
                            return vh.getOnKeyListener().onKey(vh.view, event.getKeyCode(), event);
                        }

                        return false;
                    }
                });
    }


    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);

        ViewHolder vh = (ViewHolder) holder;
        VideoPlaybackControlsRow row = (VideoPlaybackControlsRow) vh.getRow();

        if (row.getItem() == null) {
            vh.mDescriptionDock.setVisibility(View.GONE);
        } else {
            vh.mDescriptionDock.setVisibility(View.VISIBLE);
            if (vh.mDescriptionViewHolder != null) {
                mDescriptionPresenter.onBindViewHolder(vh.mDescriptionViewHolder, row.getItem());
            }
        }

        if (row.getImageDrawable() == null) {
            vh.mImageView.setVisibility(View.GONE);
        } else {
            vh.mImageView.setVisibility(View.VISIBLE);
        }
        vh.mImageView.setImageDrawable(row.getImageDrawable());

        vh.mControlsBoundData.adapter = row.getPrimaryActionsAdapter();
        vh.mControlsBoundData.presenter = vh.getPresenter(true);
        vh.mControlsBoundData.mRowViewHolder = vh;
        mPlaybackControlsPresenter.onBindViewHolder(vh.mControlsVh, vh.mControlsBoundData);

        vh.mSecondaryBoundData.adapter = row.getSecondaryActionsAdapter();
        vh.mSecondaryBoundData.presenter = vh.getPresenter(false);
        vh.mSecondaryBoundData.mRowViewHolder = vh;
        mSecondaryControlsPresenter.onBindViewHolder(vh.mSecondaryControlsVh,
                vh.mSecondaryBoundData);

        vh.setTotalTime(mDuration);
        vh.setCurrentPosition(mCurrentTime);
        vh.setBufferedPosition(row.getBufferedPosition());
        row.setOnPlaybackProgressChangedListener(vh.mListener);
    }

    @Override
    protected void onUnbindRowViewHolder(RowPresenter.ViewHolder holder) {
        ViewHolder vh = (ViewHolder) holder;
        VideoPlaybackControlsRow row = (VideoPlaybackControlsRow) vh.getRow();

        if (vh.mDescriptionViewHolder != null) {
            mDescriptionPresenter.onUnbindViewHolder(vh.mDescriptionViewHolder);
        }


        mPlaybackControlsPresenter.onUnbindViewHolder(vh.mControlsVh);
        mSecondaryControlsPresenter.onUnbindViewHolder(vh.mSecondaryControlsVh);
        row.setOnPlaybackProgressChangedListener(null);

        super.onUnbindRowViewHolder(holder);
    }

    /**
     * Client of progress bar is clicked, default implementation delegate click to
     * PlayPauseAction.
     *
     * @param vh ViewHolder of PlaybackTransportRowPresenter
     */
    protected void onProgressBarClicked(ViewHolder vh) {
        if (vh != null) {
            if (vh.mPlayPauseAction == null) {
                vh.mPlayPauseAction = new VideoPlaybackControlsRow.PlayPauseAction(vh.view.getContext());
            }

            if (vh.getOnItemViewClickedListener() != null) {
                vh.getOnItemViewClickedListener().onItemClicked(vh, vh.mPlayPauseAction,
                        vh, vh.getRow());
            }
            if (mOnActionClickedListener != null) {
                mOnActionClickedListener.onActionClicked(vh.mPlayPauseAction);
            }
        }
    }

    public void setDefaultSeekIncrement(float ratio) {
        mDefaultSeekIncrement = ratio;
    }

    public float getDefaultSeekIncrement() {
        return mDefaultSeekIncrement;
    }

    @Override
    protected void onRowViewSelected(RowPresenter.ViewHolder vh, boolean selected) {
        super.onRowViewSelected(vh, selected);
        if (selected) {
            ((ViewHolder) vh).dispatchItemSelection();
        }
    }

    @Override
    protected void onRowViewAttachedToWindow(RowPresenter.ViewHolder vh) {
        super.onRowViewAttachedToWindow(vh);
        if (mDescriptionPresenter != null) {
            mDescriptionPresenter.onViewAttachedToWindow(
                    ((ViewHolder) vh).mDescriptionViewHolder);
        }


    }

    @Override
    protected void onRowViewDetachedFromWindow(RowPresenter.ViewHolder vh) {
        super.onRowViewDetachedFromWindow(vh);
        if (mDescriptionPresenter != null) {
            mDescriptionPresenter.onViewDetachedFromWindow(
                    ((ViewHolder) vh).mDescriptionViewHolder);
        }

    }


    private static class OnProgressKeyListener implements View.OnKeyListener {
        final ViewHolder mViewHolder;

        public OnProgressKeyListener(ViewHolder mViewHolder) {
            this.mViewHolder = mViewHolder;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent keyEvent) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    return true;
                case KeyEvent.KEYCODE_MINUS:
                case KeyEvent.KEYCODE_MEDIA_REWIND:
                case KeyEvent.KEYCODE_PLUS:
                case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                case KeyEvent.KEYCODE_DPAD_CENTER:
                case KeyEvent.KEYCODE_ENTER:
                case KeyEvent.KEYCODE_BACK:
                case KeyEvent.KEYCODE_ESCAPE:
                    return false;
            }
            return false;
        }
    }

}
