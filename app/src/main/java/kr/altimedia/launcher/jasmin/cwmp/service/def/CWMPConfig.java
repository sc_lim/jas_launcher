/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service.def;

public class CWMPConfig {
    // {LIVE, TESTBED, KT-Mokdong}
    public static final String[] MGT_SVR_MAIN_URLs = new String[]{
            "https://iptv.cems.3bbtv.com:8443/device/tr-069",
            "https://iptvstaging.cems.3bbtv.com:8443/device/tr-069",
            "https://222.122.207.42:8443/device/tr-069"
    };
    public static final String[] MGT_SVR_SUB_URLs = new String[]{
            "https://iptvstaging.cems.3bbtv.com:8443/device/tr-069",
            "https://222.122.207.42:8443/device/tr-069",
            "https://iptvstaging.cems.3bbtv.com:8443/device/tr-069"
    };
    public static final String[] MGT_SVR_CONN_REQ_USERs = new String[]{
            "ttbbdms",
            "ttbbdms",
            "ttbbdms"
    }; // for certification of acs->cpe
    public static final String[] MGT_SVR_CONN_REQ_PWs = new String[]{
            "^ttbb-069^",
            "^ttbb-069^",
            "^ttbb-069^"
    }; // for certification of acs->cpe
    public static final String[] FW_UPDATE_URLs = new String[]{
            "https://iptv.cems.3bbtv.com:9443/device/fwupdateinfo",
            "https://iptvstaging.cems.3bbtv.com:9443/device/fwupdateinfo",
            "https://222.122.207.42:9443/device/fwupdateinfo"
    };
    public static final String[] APK_META_URLs = new String[]{
            "https://iptv.cems.3bbtv.com:9443/device/apkmetainfo",
            "https://iptvstaging.cems.3bbtv.com:9443/device/apkmetainfo",
            "https://222.122.207.42:9443/device/apkmetainfo"
    };
    public static final String[] DEFAULT_SETTING_URLs = new String[]{
            "https://iptv.cems.3bbtv.com:9443/device/defaultsetting",
            "https://iptvstaging.cems.3bbtv.com:9443/device/defaultsetting",
            "https://222.122.207.42:9443/device/defaultsetting"
    };

    public static final int TCP_SRC_PORT = 7547;
    public static final int UDP_SRC_PORT = 7447;
    public static final int UDP_DST_PORT = 7557;
}
