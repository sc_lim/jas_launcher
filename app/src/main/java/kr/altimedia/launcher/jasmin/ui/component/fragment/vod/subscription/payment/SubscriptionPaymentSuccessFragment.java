/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionInfo;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionPurchaseDialogFragment.KEY_SUBSCRIPTION_INFO;

public class SubscriptionPaymentSuccessFragment extends SubscriptionPaymentBaseFragment {
    public static final String CLASS_NAME = SubscriptionPaymentSuccessFragment.class.getName();

    private TextView confirm;

    public static SubscriptionPaymentSuccessFragment newInstance(SubscriptionInfo subscriptionInfo) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_SUBSCRIPTION_INFO, subscriptionInfo);

        SubscriptionPaymentSuccessFragment fragment = new SubscriptionPaymentSuccessFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_subscription_payment_success;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        SubscriptionInfo subscriptionInfo = getArguments().getParcelable(KEY_SUBSCRIPTION_INFO);
        String title = subscriptionInfo.getTitle();
        ((TextView) view.findViewById(R.id.title)).setText(title);

        confirm = view.findViewById(R.id.confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnPaymentListener onPaymentListener = getOnPaymentListener();
                if (onPaymentListener != null) {
                    onPaymentListener.onDismissPurchaseDialog();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        confirm.setOnClickListener(null);
    }
}
