/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.dialog;

import android.content.Context;
import android.view.View;

import kr.altimedia.launcher.jasmin.R;

/**
 * Created by mc.kim on 17,07,2020
 */
public class LoginNoticeDialogFragment extends TwoButtonDialogFragment {
    public static final String CLASS_NAME = LoginNoticeDialogFragment.class.getName();
    private final String TAG = LoginNoticeDialogFragment.class.getSimpleName();

    public LoginNoticeDialogFragment(Context context, View.OnClickListener onClickListener) {
        super(context, onClickListener);
    }

    public static TwoButtonDialogFragment newInstance(Context context, View.OnClickListener onClickListener) {
        String title = context.getString(R.string.error);
        String subtitle = context.getString(R.string.error_login_title);
        String description = context.getString(R.string.error_login_description);
        String buttonTitle = context.getString(R.string.button_retry);
        TwoButtonDialogFragment dialogFragment = newInstance(context, title, subtitle, description, buttonTitle, onClickListener);
        return dialogFragment;
    }

}
