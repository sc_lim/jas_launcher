/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.subtitles;

import android.view.View;

import com.altimedia.util.Log;

import androidx.leanback.widget.ArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.system.settings.data.OnOffValue;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.CheckButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.SettingBaseButtonItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.CheckButtonPresenter;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class SettingSubtitlesFragment extends SettingBaseFragment
        implements SettingBaseButtonItemBridgeAdapter.OnClickButton<SettingSubtitlesFragment.SubtitleCheckButtonButton>, SettingControl.SettingsEventListener {
    public static final String CLASS_NAME = SettingSubtitlesFragment.class.getName();
    private static final String TAG = SettingSubtitlesFragment.class.getSimpleName();

    private final SettingControl settingControl = SettingControl.getInstance();

    private ArrayObjectAdapter objectAdapter;
    private CheckButtonBridgeAdapter checkButtonBridgeAdapter;

    private SubtitleCheckButtonButton btnOn;
    private SubtitleCheckButtonButton btnOff;

    private SettingSubtitlesFragment() {

    }

    public static SettingSubtitlesFragment newInstance() {
        return new SettingSubtitlesFragment();
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_subtitles;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        initList(view);
        settingControl.addSettingsEventListener(this);
    }

    private void initList(View view) {
        boolean isEnable = settingControl.isSubtitleEnable();
        btnOn = new SubtitleCheckButtonButton(OnOffValue.SET.getType(), isEnable);
        btnOff = new SubtitleCheckButtonButton(OnOffValue.OFF.getType(), !isEnable);

        objectAdapter = new ArrayObjectAdapter(new CheckButtonPresenter());
        objectAdapter.add(btnOn);
        objectAdapter.add(btnOff);

        VerticalGridView gridView = view.findViewById(R.id.gridView);
        checkButtonBridgeAdapter = new CheckButtonBridgeAdapter(objectAdapter);
        checkButtonBridgeAdapter.setListener(this);
        gridView.setAdapter(checkButtonBridgeAdapter);

        int spacing = (int) getResources().getDimension(R.dimen.setting_text_view_vertical_spacing);
        gridView.setVerticalSpacing(spacing);
    }

    @Override
    public boolean onClickButton(SubtitleCheckButtonButton item) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onClickButton, item : " + item);
        }

        int type = item.getType();
        if (type == OnOffValue.SET.getType()) {
            SettingSubtitlesSetupDialogFragment settingSubtitlesSetupDialogFragment = SettingSubtitlesSetupDialogFragment.newInstance();
            settingSubtitlesSetupDialogFragment.setOnSubtitlesChangeResultCallback(new SettingSubtitlesSetupDialogFragment.OnSubtitlesChangeResultCallback() {
                @Override
                public void onChangeSubtitles(boolean isSuccess) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onChangeSubtitles, isSuccess : " + isSuccess);
                    }

                    if (isSuccess) {
                        toastSave();
                    }

                    settingSubtitlesSetupDialogFragment.dismiss();
                }
            });
            onFragmentChange.getTvOverlayManager().showDialogFragment(settingSubtitlesSetupDialogFragment);
        } else if (type == OnOffValue.OFF.getType()) {
            settingControl.setSubtitleEnable(false);
        }

        return true;
    }

    private void setButtonChecked(boolean isOn) {
        btnOn.setChecked(isOn);
        btnOff.setChecked(!isOn);

        objectAdapter.replace(OnOffValue.SET.getType(), btnOn);
        objectAdapter.replace(OnOffValue.OFF.getType(), btnOff);
    }

    private void toastSave() {
        JasminToast.makeToast(getContext(), R.string.saved);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        checkButtonBridgeAdapter.setListener(null);
        settingControl.removeSettingsEventListener(this);
    }

    @Override
    public void onSettingChanged(String s) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSettingChanged, s : " + s);
        }

        if (s.equalsIgnoreCase(SettingControl.KEY_SUBTITLE_ENABLE)) {
            boolean isEnable = settingControl.isSubtitleEnable();
            setButtonChecked(isEnable);
        }
    }

    protected class SubtitleCheckButtonButton implements CheckButtonPresenter.CheckButtonItem {

        private OnOffValue onOff;
        private boolean isChecked;

        SubtitleCheckButtonButton(int type, boolean isChecked) {
            onOff = (type == OnOffValue.SET.getType()) ? OnOffValue.SET : OnOffValue.OFF;
            this.isChecked = isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        @Override
        public int getType() {
            return onOff.getType();
        }

        @Override
        public int getTitle() {
            return onOff.getTitle();
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public boolean isChecked() {
            return isChecked;
        }

        @Override
        public boolean isEnabledCheck() {
            return true;
        }

        public OnOffValue getOnOff() {
            return onOff;
        }
    }
}
