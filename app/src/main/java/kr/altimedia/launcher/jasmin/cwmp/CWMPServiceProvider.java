/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.RemoteException;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.BuildConfig;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPCallback;
import kr.altimedia.launcher.jasmin.cwmp.service.CWMPService;
import kr.altimedia.launcher.jasmin.cwmp.service.def.CWMPStandbyReason;
import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import kr.altimedia.launcher.jasmin.system.ota.SystemUpdateManager;
import kr.altimedia.launcher.jasmin.system.settings.SettingControl;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;

/**
 * CWMPServiceProvider
 */
public class CWMPServiceProvider {
    private static CWMPServiceProvider instance;
    private final String CLASS_NAME = CWMPServiceProvider.class.getName();
    private final String TAG = CWMPServiceProvider.class.getSimpleName();

    private final int BOOT_INFORM_NOT_READY = 0;
    private final int BOOT_INFORM_COMPLETE = 2;

    private Context context;
    private ServiceConnection serviceConnection;
    private CWMPService cwmpService;
    private CWMPEventHandler cwmpEventHandler;
    private PowerStatusReceiver powerStatusReceiver;
    private Channel watchingChannel = null;
    private String watchingVod = null;

    private boolean authorized = false;
    private String authDescription = "";
    private boolean isInitialized = false;
    private int bootInformStatus = -1;

    public static void removeInstance() {
        if (instance != null) {
            instance.dispose();
            instance = null;
        }
    }

    public static CWMPServiceProvider getInstance() {
        if (instance == null) {
            instance = new CWMPServiceProvider();
        }
        return instance;
    }

    private void dispose() {
        if (cwmpEventHandler != null) {
            cwmpEventHandler.dispose();
            cwmpEventHandler = null;
        }
        if(powerStatusReceiver != null){
            powerStatusReceiver.dispose();
            powerStatusReceiver = null;
        }
        stop();

        context = null;
    }

    /**
     * CWMPServiceProvider를 초기화합니다.
     *
     * @param context
     */
    public boolean init(Context context) {
        if (Log.INCLUDE) {
            Log.d(TAG, "init");
        }

        if (!isInitialized) {
            this.isInitialized = true;
            this.context = context;
            this.cwmpEventHandler = new CWMPEventHandler(this.context);
            this.powerStatusReceiver = new PowerStatusReceiver();
        }else{
            if (cwmpService != null) {
                return false;
            }

            // exceptional case which service not started for
            if (Log.INCLUDE) {
                Log.d(TAG, "CWMP is already initialized, but need to bind service");
            }
        }
        return true;
    }

    /**
     * CWMPServiceProvider를 시작합니다.
     */
    public void start() {
        if (Log.INCLUDE) {
            Log.d(TAG, "start");
        }

        connect();
    }

    /**
     * CWMPServiceProvider를 중지합니다.
     */
    public void stop() {
        if (Log.INCLUDE) {
            Log.d(TAG, "stop");
        }
        if (context != null && serviceConnection != null) {
            context.unbindService(serviceConnection);
        }
    }

    /**
     * CWMPService에 연결합니다.
     */
    private void connect() {
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onServiceConnected");
                }
                CWMPService.CWMPServiceBinder binder = (CWMPService.CWMPServiceBinder) service;
                cwmpService = binder.getService();
                try {
                    cwmpService.registerCWMPEventListener(cwmpEventHandler);
                } catch (NullPointerException | RemoteException e) {
                    e.printStackTrace();
                }

                startPowerStatusListener();

                // exceptional case which service not started for
                if(bootInformStatus == BOOT_INFORM_NOT_READY){
                    notifyBooting(authorized, authDescription);
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onServiceDisconnected");
                }
                try {
                    if (cwmpService != null) {
                        cwmpService.unregisterCWMPEventListener(cwmpEventHandler);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                cwmpService = null;
            }
        };

        try {
            Intent intent = new Intent(context, CWMPService.class);
            int targetSignal = CWMPService.TYPE_LIVE;
            if (ServerConfig.FLAVOR_TYPE_KR.equals(BuildConfig.FLAVOR)) {
                targetSignal = CWMPService.TYPE_KT_MOKDONG;
            } else if (ServerConfig.FLAVOR_TYPE_STAGING.equals(BuildConfig.FLAVOR)) {
                targetSignal = CWMPService.TYPE_TESTBED;
            }
            intent.putExtra(CWMPService.KEY_TARGET_SIGNAL, targetSignal);
            intent.putExtra(CWMPService.KEY_SAID, StringUtils.nullToEmpty(SettingControl.getInstance().getSaId()));
            intent.putExtra(CWMPService.KEY_REGISTRATION_SERVER, ServerConfig.MBS_URL);
            intent.putExtra(CWMPService.KEY_LAUNCHER_VERSION, BuildConfig.VERSION_NAME);
            context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void startPowerStatusListener(){
        if(powerStatusReceiver != null) {
            powerStatusReceiver.start(context, new CWMPServiceProvider.OnPowerStatusListener() {
                @Override
                public void onScreenOn() {
                    CWMPServiceProvider.this.notifyPowerState(false, "");
                }

                @Override
                public void onScreenOff() {
                    CWMPServiceProvider.this.notifyPowerState(true, CWMPStandbyReason.REMOTE_POWER);
                }
            });
        }
    }

    public long getNormalPeriod() {
        if (cwmpService == null) {
            return 0;
        }
        try {
            return cwmpService.getNormalPeriod();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public Channel getWatchingChannel() {
        return watchingChannel;
    }

    public String getWatchingVod() {
        return watchingVod;
    }

    public boolean isScreenOff() {
        if(powerStatusReceiver != null) {
            return powerStatusReceiver.isScreenOff();
        }
        return false;
    }

    public void setWatchingChannel(Channel channel) {
        watchingChannel = channel;
    }

    public void setWatchingVod(String contentName) {
        if (Log.INCLUDE) {
            Log.d(TAG, "setWatchingVod : " + (contentName == null ? "null" : contentName));
        }
        watchingVod = contentName;
    }

    public void notifyBooting(boolean authorized, String description) {
        if (cwmpService == null){
            bootInformStatus = BOOT_INFORM_NOT_READY;
            if (Log.INCLUDE) {
                Log.d(TAG, "notifyBooting: cwmp service is not available, so try to start service");
            }

            // exceptional case which service not started for
            this.authorized = authorized;
            this.authDescription = description;
            if(isInitialized) {
                start();
            }

            return;
        }

        try {
            if(bootInformStatus == BOOT_INFORM_COMPLETE){
                if(this.authorized == authorized) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "notifyBooting: already sent(no changes), so just return");
                    }
                    return;
                }
            }

            this.authorized = authorized;

            bootInformStatus = BOOT_INFORM_COMPLETE;
            cwmpService.notifyBooting(authorized, StringUtils.nullToEmpty(SettingControl.getInstance().getSaId()), description);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try{
            cwmpService.requestDefaultSetting(new CWMPCallback() {
                @Override
                public void onSuccess() {
                    SystemUpdateManager.getInstance().init(context);
                }
                @Override
                public void onFailure() {
                    SystemUpdateManager.getInstance().init(context);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            SystemUpdateManager.getInstance().init(context);
        }
    }

    public void notifyPowerState(boolean standby, String reason) {
        if (cwmpService == null) return;
        try {
            cwmpService.notifyPowerState(standby, reason);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void notifyEtcError(String errorCode) {
        if (cwmpService == null) return;
        try {
            long currTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
            cwmpService.notifyEtcError(errorCode, null, currTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void notifyWatchAuthority() {
        if (cwmpService == null) return;
        try {
            cwmpService.notifyWatchAuthority();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void notifyHotKeyClicked(int keyCode) {
        if (cwmpService == null) return;
        try {
            cwmpService.onHotKeyClicked(keyCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    interface OnPowerStatusListener {
        void onScreenOn();
        void onScreenOff();
    }

    class PowerStatusReceiver extends BroadcastReceiver {
        private final String SCREEN_ON = Intent.ACTION_SCREEN_ON;
        private final String SCREEN_OFF = Intent.ACTION_SCREEN_OFF;

        private boolean isScreenOff;
        private Context context;
        private OnPowerStatusListener listener;

        void dispose() {
            try {
                if (context != null) {
                    context.unregisterReceiver(this);
                }
            } catch (Exception e) {
            }
            context = null;
        }

        void start(Context context, OnPowerStatusListener listener) {
            if (Log.INCLUDE) {
                Log.d(TAG, "start");
            }
            this.context = context;
            this.listener = listener;
            PowerManager powerManager = null;
            if (powerManager == null) {
                powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                isScreenOff = !powerManager.isInteractive();
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "start: isScreenOff="+isScreenOff);
            }
            try {
                IntentFilter filter = new IntentFilter();
                filter.addAction(Intent.ACTION_SCREEN_ON);
                filter.addAction(Intent.ACTION_SCREEN_OFF);
                context.registerReceiver(this, filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * 대기모드 여부를 리턴합니다.
         *
         * @return
         */
        boolean isScreenOff() {
            return isScreenOff;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                boolean value = false;
                if (action.equals(SCREEN_ON)) {
                    value = false;
                } else if (action.equals(SCREEN_OFF)) {
                    value = true;
                }
                if(value == isScreenOff) return;
                isScreenOff = value;
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReceive: intent="+intent);
                }
                if(listener != null){
                    if(isScreenOff) {
                        listener.onScreenOff();
                    }else {
                        listener.onScreenOn();
                    }
                }
            }
        }
    }
}
