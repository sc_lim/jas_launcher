/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.block;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.altimedia.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseProfileDialogFragment;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.SettingProfileDialogFragment.KEY_PROFILE;

public class SettingProfileChannelBlockedResetDialogFragment extends SettingBaseProfileDialogFragment {
    public static final String CLASS_NAME = SettingProfileChannelBlockedResetDialogFragment.class.getName();
    public final String TAG = SettingProfileChannelBlockedResetDialogFragment.class.getSimpleName();

    private OnResetBlockedChannelResultCallback onResetBlockedChannelResultCallback;

    private SettingProfileChannelBlockedResetDialogFragment() {

    }

    public static SettingProfileChannelBlockedResetDialogFragment newInstance(ProfileInfo profileInfo) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_PROFILE, profileInfo);

        SettingProfileChannelBlockedResetDialogFragment fragment = new SettingProfileChannelBlockedResetDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnResetBlockedChannelResultCallback(OnResetBlockedChannelResultCallback onResetBlockedChannelResultCallback) {
        this.onResetBlockedChannelResultCallback = onResetBlockedChannelResultCallback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_block_reset, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        TextView confirm = view.findViewById(R.id.confirm);
        TextView cancel = view.findViewById(R.id.cancel);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResetBlockedChannelResultCallback.onChangeResetBlockedChannel(true);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResetBlockedChannelResultCallback.onChangeResetBlockedChannel(false);
            }
        });
    }

    @Override
    protected ProfileInfo getProfileInfo() {
        return getArguments().getParcelable(KEY_PROFILE);
    }

    @Override
    protected void updateProfile(PushMessageReceiver.ProfileUpdateType type) {
        super.updateProfile(type);

        if (Log.INCLUDE) {
            Log.d(TAG, "updateProfile, type : " + type);
        }

        dismiss();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onResetBlockedChannelResultCallback = null;
    }

    public interface OnResetBlockedChannelResultCallback {
        void onChangeResetBlockedChannel(boolean isReset);
    }
}
