/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.parental.rating;

import kr.altimedia.launcher.jasmin.system.settings.data.ParentalRating;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.CheckButtonPresenter;

public class RatingLockItem implements CheckButtonPresenter.CheckButtonItem {

    private ParentalRating pr;
    private boolean isChecked;

    public RatingLockItem(int rating) {
        if (rating == ParentalRating.LIMIT_3.getRating()) {
            pr = ParentalRating.LIMIT_3;
        } else if (rating == ParentalRating.LIMIT_6.getRating()) {
            pr = ParentalRating.LIMIT_6;
        } else if (rating == ParentalRating.LIMIT_13.getRating()) {
            pr = ParentalRating.LIMIT_13;
        } else if (rating == ParentalRating.LIMIT_18.getRating()) {
            pr = ParentalRating.LIMIT_18;
        } else {
            pr = ParentalRating.NO_LIMIT;
        }
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getRating() {
        return pr.getRating();
    }

    @Override
    public int getType() {
        return pr.getType();
    }

    @Override
    public int getTitle() {
        return pr.getTitle();
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public boolean isEnabledCheck() {
        return true;
    }

    public ParentalRating findByName(String rating) {
        ParentalRating[] types = ParentalRating.values();

        for (ParentalRating parentalRating : types) {
            String type = parentalRating.toString();
            if (type.equalsIgnoreCase(rating)) {
                return parentalRating;
            }
        }

        return ParentalRating.NO_LIMIT;
    }

    public ParentalRating getParentalRating() {
        return pr;
    }
}
