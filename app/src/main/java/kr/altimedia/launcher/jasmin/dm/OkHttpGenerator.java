/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import kotlin.Pair;
import kr.altimedia.launcher.jasmin.dm.def.ServerConfig;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class OkHttpGenerator {
    private static final String TAG = OkHttpGenerator.class.getSimpleName();

    private final static String KEY_TRANSACTION_ID = "transactionId";
    private final static String KEY_RETURN_CODE = "returnCode";
    private final static String VALUE_RETURN_CODE_SUCCESS = "S";
    private final static String KEY_ERROR_CODE = "errorCode";
    private final static String KEY_ERROR_MESSAGE = "errorMessage";


    public static OkHttpClient getUnsafeOkHttpClient(String systemPassword, String loginToken, String uuid, String fcmToken, String languageType, String deviceId) {
        try {
            // https://futurestud.io/tutorials/glide-module-example-accepting-self-signed-https-certificates
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier((hostname, session) -> true);
            builder.addInterceptor(chain -> {
                Request.Builder headBuild = chain.request().newBuilder();
                if (!StringUtils.nullToEmpty(systemPassword).isEmpty()) {
                    headBuild.addHeader("Authorization", "Basic " + systemPassword.trim());
                }
                if (!StringUtils.nullToEmpty(uuid).isEmpty()) {
                    headBuild.addHeader("iptv-tx-id", uuid.trim());
                }
                if (!StringUtils.nullToEmpty(loginToken).isEmpty()) {
                    headBuild.addHeader("iptv-login-token", loginToken.trim());
                }
                if (!StringUtils.nullToEmpty(languageType).isEmpty()) {
                    headBuild.addHeader("iptv-lang-type", languageType.trim());
                }
                if (!StringUtils.nullToEmpty(fcmToken).isEmpty()) {
                    headBuild.addHeader("Device-Token", fcmToken.trim());
                }
                if (!StringUtils.nullToEmpty(deviceId).isEmpty()) {
                    headBuild.addHeader("Device-id", deviceId.trim());
                }
                Request request = headBuild.build();
                if (Log.INCLUDE) {
                    Headers headers = request.headers();
                    Iterator<Pair<String, String>> header = headers.iterator();
                    while (header.hasNext()) {
                        Pair<String, String> he = header.next();
                        Log.d(TAG, "header : " + he.getFirst() + ", second : " + he.getSecond());
                    }
                }
                return chain.proceed(request);
            });
            OkHttpClient okHttpClient = builder.
                    addNetworkInterceptor(new LogInterceptor(uuid)).
                    connectTimeout(ServerConfig.CONNECT_TIMEOUT_SECOND, TimeUnit.SECONDS).
                    readTimeout(ServerConfig.READ_TIMEOUT_SECOND, TimeUnit.SECONDS).
                    writeTimeout(ServerConfig.WRITE_TIMEOUT_SECOND, TimeUnit.SECONDS).build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static class LogInterceptor implements Interceptor {
        private final String transactionId;

        public LogInterceptor(String transactionId) {
            this.transactionId = transactionId;
        }

        @NotNull
        @Override
        public Response intercept(@NotNull Chain chain) throws IOException {
            Request request = chain.request();
            Response response = chain.proceed(request);
//
            if (response.isSuccessful()) {
                String json = new String(response.body().bytes());
                if (Log.INCLUDE) {
                    Log.d(TAG, "json : " + json);
                }
                try {
                    JsonElement element = new JsonParser().parse(json.trim());
                    final JsonObject object = element.getAsJsonObject();
                    object.addProperty(KEY_TRANSACTION_ID, transactionId);
                    return response.newBuilder().body(ResponseBody.create(object.toString(), response.body().contentType())).build();
                } catch (Exception e) {
                    return response.newBuilder().body(ResponseBody.create(json, response.body().contentType())).build();
                }
            }


//            if (response.isSuccessful()) {
//                int responseCode = response.code();
//                String errorBodyMessage = response.body().string();
//                JsonObject errorJson = new Gson().fromJson(errorBodyMessage, JsonObject.class);
//                if (errorJson != null && errorJson.has(KEY_RETURN_CODE) && !errorJson.get(KEY_RETURN_CODE).isJsonNull()) {
//                    String resultCode = errorJson.get(KEY_RETURN_CODE).getAsString();
//                    if (!resultCode.equalsIgnoreCase(VALUE_RETURN_CODE_SUCCESS)) {
//                        JsonObject errorJsonWithUrl = new JsonObject();
//                        String errorCode = "";
//                        if (errorJson.has(KEY_ERROR_CODE) && !errorJson.get(KEY_ERROR_CODE).isJsonNull()) {
//                            errorCode = errorJson.get(KEY_ERROR_CODE).getAsString() + request.url().encodedPath();
//                        } else {
//                            errorCode = responseCode + "" + request.url().encodedPath();
//                        }
//                        String errorMessage = "";
//                        if (errorJson.has(KEY_ERROR_MESSAGE) && !errorJson.get(KEY_ERROR_MESSAGE).isJsonNull()) {
//                            errorMessage = errorJson.get(KEY_ERROR_MESSAGE).getAsString();
//                        }
//                        errorJsonWithUrl.addProperty(KEY_RETURN_CODE, resultCode);
//                        errorJsonWithUrl.addProperty(KEY_ERROR_CODE, errorCode);
//                        errorJsonWithUrl.addProperty(KEY_ERROR_MESSAGE, errorMessage);
//
//
//                        MediaType contentType = response.body().contentType();
//                        ResponseBody body = ResponseBody.create(errorJsonWithUrl.toString(), contentType);
//                        return response.newBuilder().body(body).build();
//                    }
//                } else {
//                    MediaType contentType = response.body().contentType();
//                    JsonObject errorJsonObject = new JsonObject();
//                    errorJsonObject.addProperty(KEY_RETURN_CODE, "F");
//                    errorJsonObject.addProperty(KEY_ERROR_CODE, response.code() + request.url().encodedPath());
//                    errorJsonObject.addProperty(KEY_ERROR_MESSAGE, errorBodyMessage);
//                    ResponseBody body = ResponseBody.create(errorJsonObject.toString(), contentType);
//                    return response.newBuilder().body(body).build();
//                }
//            }
            return response;
        }
    }
}
