/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.edit;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.object.UserDevice;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.data.CustomInputEditText;
import kr.altimedia.launcher.jasmin.ui.util.task.SettingTaskManager;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.settings.device.edit.SettingDeviceEditDialogFragment.KEY_DEVICE;

public class SettingDeviceEditNameFragment extends SettingDeviceEditBaseFragment implements View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = SettingDeviceEditNameFragment.class.getName();
    private final String TAG = SettingDeviceEditNameFragment.class.getSimpleName();

    private final float DEFAULT_ALPHA = 1.0f;
    private final float DIM_ALPHA = 0.5f;

    private UserDevice deviceItem;

    private CustomInputEditText editText;
    private TextView saveButton;

    private MbsDataTask editNameTask;

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = String.valueOf(s);
            boolean isSaveEnable = !text.isEmpty();
            setEnableButton(isSaveEnable);
        }
    };

    private SettingDeviceEditNameFragment() {
    }

    public static SettingDeviceEditNameFragment newInstance(Bundle bundle) {
        SettingDeviceEditNameFragment fragment = new SettingDeviceEditNameFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getResourceId() {
        return R.layout.fragment_chnage_device_name;
    }

    @Override
    protected void initView(View view) {
        deviceItem = getArguments().getParcelable(KEY_DEVICE);
        TextView deviceName = view.findViewById(R.id.device_name);
        deviceName.setText(deviceItem.getDeviceName());

        initButton(view);
        initInputView(view);
    }

    private void initInputView(View view) {
        TextView cancelButton = view.findViewById(R.id.cancelButton);
        editText = view.findViewById(R.id.intputText);
        editText.addTextChangedListener(textWatcher);
        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_DOWN:
                        if (saveButton.isEnabled()) {
                            saveButton.requestFocus();
                            return true;
                        }
                }
                return false;
            }
        });
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    editText.showKeyboard();
                }
            }
        });
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onEditorAction");
                }

                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (editText.getText().length() > 0) {
                        saveButton.requestFocus();
                    } else {
                        cancelButton.requestFocus();
                    }
                }

                return false;
            }
        });

        editText.addOnUnhandledKeyEventListener(this);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                editText.requestFocus();
            }
        });
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        int keyCode = event.getKeyCode();
        if (keyCode >= KeyEvent.KEYCODE_0 && keyCode <= KeyEvent.KEYCODE_9) {
            int keyNum = keyCode - 7;
            String stringNum = Integer.toString(keyNum);

            editText.append(stringNum);
        }

        return false;
    }

    private boolean isRequest = false;
    private void initButton(View view) {
        saveButton = view.findViewById(R.id.saveButton);
        TextView cancelButton = view.findViewById(R.id.cancelButton);

        saveButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isRequest) {
                            return;
                        }

                        isRequest = true;
                        putDeviceName();
                    }
                });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOnChangeFragmentListener().onBackFragment();
            }
        });

        setEnableButton(false);
    }

    private void setEnableButton(boolean isEnable) {
        if (isEnable != saveButton.isEnabled()) {
            float alpha = isEnable ? DEFAULT_ALPHA : DIM_ALPHA;
            saveButton.setAlpha(alpha);
            saveButton.setEnabled(isEnable);
        }
    }

    private void putDeviceName() {
        String editedName = String.valueOf(editText.getText());
        OnChangeFragmentListener onChangeFragmentListener = getOnChangeFragmentListener();
        SettingTaskManager settingTaskManager = onChangeFragmentListener.getSettingTaskManager();

        editNameTask = settingTaskManager.putDevice(
                deviceItem.getDeviceId(), editedName,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "putDeviceName, needLoading, loading : " + loading);
                        }

                        onChangeFragmentListener.showProgressbar(loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "putDeviceName, onFailed");
                        }

                        isRequest = false;
                        onChangeFragmentListener.getSettingTaskManager().showFailDialog(getTaskContext(), key);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "putDeviceName, onSuccess, resut : " + result);
                        }

                        if (result) {
                            onChangeFragmentListener.onEditName(deviceItem, editedName);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "deleteDevice, onError");
                        }

                        isRequest = false;
                        settingTaskManager.showErrorDialog(getTaskContext(), TAG, errorCode, message);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SettingDeviceEditNameFragment.this.getContext();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        editText.removeOnUnhandledKeyEventListener(this);
        editText.removeTextChangedListener(textWatcher);
        if (editNameTask != null) {
            editNameTask.cancel(true);
        }
    }
}
