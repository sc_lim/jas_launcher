/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.message.api;

import com.google.gson.JsonObject;

import kr.altimedia.launcher.jasmin.dm.BaseResponse;
import kr.altimedia.launcher.jasmin.dm.message.obj.response.MessageListResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by mc.kim on 08,05,2020
 */
public interface MessageApi {
    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("messagelist")
    Call<MessageListResponse> getMessageList(@Query("language") String language,
                                             @Query("said") String said,
                                             @Query("profileId") String profileId);

    @Headers({
            "Accept: application/json; charset=UTF-8",
            "Content-Type: application/json",
    })
    @POST("messageview")
    Call<BaseResponse> postMessageView(@Body JsonObject pinCode);
}
