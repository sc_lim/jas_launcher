package kr.altimedia.launcher.jasmin.dm.user.object;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import kr.altimedia.launcher.jasmin.dm.def.ObjectBundleDeserializer;

public class ExternalIntentData implements Parcelable {
    public static final Creator<ExternalIntentData> CREATOR = new Creator<ExternalIntentData>() {
        @Override
        public ExternalIntentData createFromParcel(Parcel in) {
            return new ExternalIntentData(in);
        }

        @Override
        public ExternalIntentData[] newArray(int size) {
            return new ExternalIntentData[size];
        }
    };

    @Expose
    @SerializedName("packageId")
    private String packageId;


    @Expose
    @SerializedName("intentData")
    @JsonAdapter(ObjectBundleDeserializer.class)
    private Bundle intentData;

    protected ExternalIntentData(Parcel in) {
        packageId = in.readString();
        intentData = in.readBundle();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(packageId);
        dest.writeBundle(intentData);
    }

    public String getPackageId() {
        return packageId;
    }

    public Bundle getIntentData() {
        return intentData;
    }
}
