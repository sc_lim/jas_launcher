
/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import kr.altimedia.launcher.jasmin.dm.def.CouponType;

public class DiscountCoupon extends Coupon implements Parcelable {
    public static final Creator<DiscountCoupon> CREATOR = new Creator<DiscountCoupon>() {
        @Override
        public DiscountCoupon createFromParcel(Parcel in) {
            return new DiscountCoupon(in);
        }

        @Override
        public DiscountCoupon[] newArray(int size) {
            return new DiscountCoupon[size];
        }
    };

    @Expose
    @SerializedName("dcType")
    @JsonAdapter(DiscountTypeDeserializer.class)
    private DiscountType dcType; // 할인유형
    @Expose
    @SerializedName("cpnDcRate")
    private int rate; // 쿠폰할인율
    @Expose
    @SerializedName("maxDcAmt")
    private int maxAmount; // 최대할인금액
    @Expose
    @SerializedName("minVodAmt")
    private String minVodAmount;

    protected DiscountCoupon(Parcel in) {
        super(in);

        dcType = (DiscountType) in.readValue(DiscountType.class.getClassLoader());
        rate = in.readInt();
        maxAmount = in.readInt();
        minVodAmount = in.readString();
    }

    protected DiscountCoupon(Coupon coupon, DiscountType dcType, int amount, int rate, int maxAmount, String minVodAmount) {
        this.id = coupon.id;
        this.name = coupon.name;
        this.registerDate = coupon.registerDate;
        this.expireDate = coupon.expireDate;
        this.couponStatus = coupon.couponStatus;
        this.couponType = coupon.couponType;
        if (this.couponType == null) {
            this.couponType = CouponType.DISCOUNT;
        }
        this.expiringYn = coupon.expiringYn;
        this.dcType = dcType;
        this.rate = rate;
        this.maxAmount = maxAmount;
        this.minVodAmount = minVodAmount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeValue(dcType);
        dest.writeInt(rate);
        dest.writeInt(maxAmount);
        dest.writeString(minVodAmount);
    }

    public DiscountType getDiscountType() {
        return dcType;
    }

    public int getRate() {
        return rate;
    }

    public int getMaxAmount() {
        return maxAmount;
    }

    public String getMinVodAmount() {
        return minVodAmount;
    }

    @NonNull
    @Override
    public String toString() {
        return "DiscountCoupon{" +
                "id='" + id + '\'' +
                "name='" + name + '\'' +
                "registerDate='" + registerDate + '\'' +
                "expireDate='" + expireDate + '\'' +
                "couponStatus='" + couponStatus + '\'' +
                "couponType='" + couponType + '\'' +
                "dcType='" + dcType + '\'' +
                "rate='" + rate + '\'' +
                "maxAmount='" + maxAmount + '\'' +
                "minVodAmount='" + minVodAmount + '\'' +
                "}";
    }
}
