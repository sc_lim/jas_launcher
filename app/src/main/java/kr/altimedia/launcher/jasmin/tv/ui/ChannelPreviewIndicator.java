/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.tv.ui;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Consumer;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.channel.JasChannelManager;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;

public class ChannelPreviewIndicator extends FrameLayout {
    private static final String TAG = ChannelPreviewIndicator.class.getSimpleName();

    public interface OnPreviewListener {
        void setVisibility(int visibility);
        void updateTime(String time, String unit);
    }

    private Consumer<Channel> consumer;
    private ImageView channel_preview_progress;
    private TextView channel_preview_time;
    private TextView channel_preview_time_unit;

    private OnPreviewListener onPreviewListener;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message message) {
            updateTime();
        }
    };

    public ChannelPreviewIndicator(Context context) {
        this(context, null);
    }

    public ChannelPreviewIndicator(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChannelPreviewIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ChannelPreviewIndicator(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();

        channel_preview_progress = findViewById(R.id.channel_preview_progress);
        channel_preview_time = findViewById(R.id.channel_preview_time);
        channel_preview_time_unit = findViewById(R.id.channel_preview_time_unit);
    }

    public void setTuneAction(Consumer<Channel> consumer) {
        this.consumer = consumer;
    }

    public void updateChannelPreview(Channel channel) {
        long timesLeft = JasChannelManager.getInstance().getPreviewLeftTime(channel);
        if (timesLeft != 0) {
            setVisibility(View.VISIBLE);
            if (onPreviewListener != null) {
                onPreviewListener.setVisibility(View.VISIBLE);
            }
            startTimer(timesLeft, channel);
        } else {
            stopTimer();
            setVisibility(View.INVISIBLE);
            if (onPreviewListener != null) {
                onPreviewListener.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onVisibilityChanged() visible:" + (visibility == View.VISIBLE));
        }
        super.onVisibilityChanged(changedView, visibility);
        if (visibility != View.VISIBLE) {
            stopTimer();
        }
    }

    public void setPreviewListener(OnPreviewListener onPreviewListener) {
        if (onPreviewListener == null) {
            this.onPreviewListener.setVisibility(View.INVISIBLE);
        }
        this.onPreviewListener = onPreviewListener;
    }

    private void updateTime() {
        long timesLeft = Math.max(0, endTime - JasmineEpgApplication.SystemClock().currentTimeMillis());
        updateTime(timesLeft);
    }

    private void updateTime(long timesLeft) {
        int sec = (int)(timesLeft / 1000L);
        int min = (sec-1) / 60;//correct display number (61~120:1min, 121~180:2min, 241~300:5min)

        if (channel_preview_progress != null) {
            int resid;
            if (sec < 1) {//0s : preview_05
                resid = R.drawable.preview_05;
            } else if (min < 1) {//01s~60s : preview_04
                resid = R.drawable.preview_04;
            } else if (min < 2) {//1m~2m : preview_03
                resid = R.drawable.preview_03;
            } else if (min < 3) {//2m~3m : preview_02
                resid = R.drawable.preview_02;
            } else if (min < 4) {//3~4m : preview_01
                resid = R.drawable.preview_01;
            } else {//4m~ : none
                resid = 0;
            }
            channel_preview_progress.setBackgroundResource(resid);
        }

        if (channel_preview_time != null && channel_preview_time_unit != null) {
            if (min > 0) {
                channel_preview_time.setText("" + (min + 1));
                channel_preview_time_unit.setText(R.string.channel_preview_time_min);

                if (onPreviewListener != null) {
                    onPreviewListener.updateTime("" + (min + 1), getContext().getString(R.string.channel_preview_time_min));
                }
            } else {
                channel_preview_time.setText("" + sec);
                channel_preview_time_unit.setText(R.string.channel_preview_time_sec);

                if (onPreviewListener != null) {
                    onPreviewListener.updateTime("" + sec, getContext().getString(R.string.channel_preview_time_sec));
                }
            }
        }
    }

    private long endTime;
    private String serviceId;
    private Timer timer;

    private void startTimer(long timesLeft, final Channel channel) {
        stopTimer();
        if (Log.INCLUDE) {
            Log.d(TAG, "startTimer() timesLeft:" + timesLeft + ", serviceId:" + serviceId);
        }
        endTime = JasmineEpgApplication.SystemClock().currentTimeMillis() + timesLeft;
        this.serviceId = channel.getServiceId();
        updateTime(timesLeft);

        synchronized (this) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                public void run() {
                    long timesLeft = Math.max(0, endTime - JasmineEpgApplication.SystemClock().currentTimeMillis());

                    int sec = (int) (timesLeft / 1000L);
                    boolean updateNeeded = (sec % 10 == 0);
                    if (Log.INCLUDE) {
                        Log.d(TAG, "tick() timesLeft:" + timesLeft + ", serviceId:" + serviceId + ", updateNeeded:" + updateNeeded);
                    }

                    mHandler.sendEmptyMessage(0);
                    if (sec == 0) {
                        stopTimer();
                        tuneChannel(channel);
                    } else if (sec % 10 == 0) {
                        //update with write up
                        updateUp(serviceId, timesLeft, true);
                    } else {
                        //update without write up
                        updateUp(serviceId, timesLeft, false);
                    }
                }
            }, 900L, 1000L);
        }
    }

    private void stopTimer() {
        boolean isCanceled = false;
        synchronized (this) {
            if (timer != null) {
                timer.cancel();
                timer = null;
                isCanceled = true;
            }
        }
        if (isCanceled) {
            long timesLeft = Math.max(0, endTime - JasmineEpgApplication.SystemClock().currentTimeMillis());
            if (Log.INCLUDE) {
                Log.d(TAG, "stopTimer() timesLeft:" + timesLeft + ", serviceId:" + serviceId);
            }
            updateUp(serviceId, timesLeft, true);
            serviceId = null;
        }
    }

    private void updateUp(String serviceId, long timesLeft, boolean writeUP) {
        long recordTime = ((int)(timesLeft/1000L) * 1000L);
        if (Log.INCLUDE) {
            Log.d(TAG, "updateUp() timesLeft:" + timesLeft + ", recordTime:" + recordTime + ", serviceId:" + serviceId+", writeUp:"+writeUP);
        }
        JasChannelManager.getInstance().updatePreviewTime(serviceId, recordTime, writeUP);
    }

    private void tuneChannel(Channel channel) {
        if (Log.INCLUDE) {
            Log.d(TAG, "tuneChannel() channel:" + channel+", serviceId:"+serviceId);
        }
        Context context = getContext();
        if ((context instanceof LiveTvActivity == false) && context instanceof ContextWrapper) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (context instanceof LiveTvActivity) {
            LiveTvActivity liveTvActivity = (LiveTvActivity) context;
            liveTvActivity.runOnUiThread(new Runnable() {
                public void run() {
                    consumer.accept(channel);
                }
            });
        }
    }
}
