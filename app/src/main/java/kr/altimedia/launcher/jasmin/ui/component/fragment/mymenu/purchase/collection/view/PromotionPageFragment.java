/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.collection.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Banner;

public class PromotionPageFragment extends Fragment {
    public static final String CLASS_NAME = PromotionPageFragment.class.getName();
    private final String TAG = PromotionPageFragment.class.getSimpleName();

    private static final String KEY_ITEM = "PROMOTION_ITEM";

    public static PromotionPageFragment newInstance(Banner item) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_ITEM, item);

        PromotionPageFragment fragment = new PromotionPageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mymenu_promotion_pager, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        Banner promotionItem = getArguments().getParcelable(KEY_ITEM);

        Log.d(TAG, "initView: promotionItem=" + promotionItem);

//        TextView title = view.findViewById(R.id.title);
//        TextView desc = view.findViewById(R.id.desc);
        ImageView image = view.findViewById(R.id.image);

        Context context = view.getContext();
//        title.setText(promotionItem.getTitle());
//        desc.setText(promotionItem.getDesc());
        Glide.with(context)
                .load(promotionItem.getBannerImageAsset().getFileName())
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .centerCrop()
                .placeholder(R.drawable.default_background)
                .error(R.drawable.default_background)
                .into(image);
    }
}
