/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.coupon.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import androidx.annotation.NonNull;

public class CouponProductList implements Parcelable {
    @Expose
    @SerializedName("result")
    public List<CouponProduct> couponProductList;
    @Expose
    @SerializedName("termsAndConditions")
    public String termsAndConditions;


    public static final Creator<CouponProductList> CREATOR = new Creator<CouponProductList>() {
        @Override
        public CouponProductList createFromParcel(Parcel in) {
            return new CouponProductList(in);
        }
        @Override
        public CouponProductList[] newArray(int size) {
            return new CouponProductList[size];
        }
    };

    protected CouponProductList(Parcel in) {
        in.readList(couponProductList, CouponProduct.class.getClassLoader());
        termsAndConditions = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(couponProductList);
        dest.writeString(termsAndConditions);
    }

    public List<CouponProduct> getCouponList() {
        return couponProductList;
    }

    public String getTermsAndConditions(){
        return termsAndConditions;
    }

    @NonNull
    @Override
    public String toString() {
        return "CouponProductList{" +
                "couponProductList='" + couponProductList + '\'' +
                ", termsAndConditions='" + termsAndConditions + '\'' +
                '}';
    }
}
