/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 *
 */

package kr.altimedia.launcher.jasmin.tv;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.TimingLogger;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.tvmodule.dao.Program;
import com.altimedia.tvmodule.dao.ProgramImpl;
import com.altimedia.tvmodule.manager.ChannelDataManager;
import com.altimedia.tvmodule.manager.ChannelRingStorage;
import com.altimedia.tvmodule.manager.ProgramDataManager;
import com.altimedia.tvmodule.util.TvInputManagerHelper;
import com.altimedia.util.Log;

import java.util.Collections;
import java.util.List;

import androidx.annotation.MainThread;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.BuildConfig;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.system.manager.ExternalApplicationManager;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.tv.interactor.PopularChannelInteractor;
import kr.altimedia.launcher.jasmin.tv.interactor.TvViewInteractor;
import kr.altimedia.launcher.jasmin.tv.manager.KeyEventManager;
import kr.altimedia.launcher.jasmin.tv.manager.MiniEpgDataManager;
import kr.altimedia.launcher.jasmin.tv.ui.AutoHider;
import kr.altimedia.launcher.jasmin.tv.ui.ChannelPreviewIndicator;
import kr.altimedia.launcher.jasmin.tv.ui.MainTvView;
import kr.altimedia.launcher.jasmin.tv.ui.PipContainerWindow;
import kr.altimedia.launcher.jasmin.tv.ui.TimeShiftFragment;
import kr.altimedia.launcher.jasmin.tv.viewModel.EpgViewModel;
import kr.altimedia.launcher.jasmin.tv.viewModel.OSDViewModel;
import kr.altimedia.launcher.jasmin.tv.viewModel.TVGuideViewModel;
import kr.altimedia.launcher.jasmin.tv.viewModel.ViewModelFactory;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.app.AppConfig;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.activity.BaseActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.PinDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SubscribeChannelDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.interactor.LaunchPath;
import kr.altimedia.launcher.jasmin.ui.component.fragment.interactor.OnAppLaunchListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.interactor.OnFragmentVisibilityInteractor;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.Linker;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.Reminder;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelLayout;
import kr.altimedia.launcher.jasmin.ui.component.fragment.side.panel.SidePanelType;
import kr.altimedia.launcher.jasmin.ui.util.PermissionUtils;
import kr.altimedia.launcher.jasmin.ui.view.common.JasminToast;
import kr.altimedia.launcher.jasmin.ui.view.drawer.EdgeDrawerLayout;
import kr.altimedia.launcher.jasmin.ui.view.guide.ProgramGuide;
import kr.altimedia.launcher.jasmin.ui.view.util.GenreUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class LiveTvActivity extends BaseActivity implements
        OnFragmentVisibilityInteractor, View.OnFocusChangeListener,
        OnAppLaunchListener, TvViewInteractor, PinDialogFragment.OnPinCheckedListener, TimeShiftFragment.OnTimeShiftInteractor,
        PipContainerWindow.OnPIPInteractor, LifecycleOwner {
    public final static String ACTION_PIP_SETTINGS = "altimedia.intent.action.PipSettings";
    public final static String ACTION_CHANNEL_GUIDE = "altimedia.intent.action.ChannelGuide";
    public final static String ACTION_CHANNEL_CHANGE = "altimedia.intent.action.ChannelChange";
    public final static String KEY_CHANNEL_NUM = "channelNumber";

    private final int WHAT_BASE_RESULT_CODE = 1;
    private static final boolean DEBUG = Log.INCLUDE;
    private static final String TAG = LiveTvActivity.class.getSimpleName();

    public static final boolean TIMESHIFT_DISABLED = !AppConfig.FEATURE_TIME_SHIFT_ENABLED;

    private static final void log(String msg) {
        if (DEBUG) {
            Log.d(TAG, msg);
        }
    }

    //TvView
    private MainTvView mainTvView;

    //tvmodule
    private TvOverlayManager mTvOverlayManager;
    private ChannelDataManager mChannelDataManager;
    private ProgramDataManager mProgramDataManager;
    private TvInputManagerHelper mTvInputManagerHelper;

    //ViewModel
    private MiniEpgViewModelManager mViewModelManager;

    //side panel
    private SidePanelManager mSidePanelManager;

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            log("onReceive() intent:" + intent.getAction());
            // Get extra data included in the Intent
            log("Action : " + intent.getAction());
            String action = intent.getAction();
            if (ACTION_PIP_SETTINGS.equalsIgnoreCase(action)) {
                //pip settings
                mSidePanelManager.closeMenu();
                showPipOption();
            } else if (ACTION_CHANNEL_GUIDE.equalsIgnoreCase(action)) {
                //channel guide
                mSidePanelManager.closeMenu();
//                if (mPipTvViewContainer.isVisible()) {
//                    mPipTvViewContainer.resetPip();
//                }
                mTvOverlayManager.showProgramGuide();
            } else if (ACTION_CHANNEL_CHANGE.equalsIgnoreCase(action)) {
                //change change
                String channelNumber = intent.getStringExtra(KEY_CHANNEL_NUM);
                Channel ch = mChannelDataManager.getChannelByDisplayNumber(channelNumber);
                if (Log.INCLUDE) {
                    Log.d(TAG, "BroadcastReceiver.onReceive() action:" + intent.getAction() + ", ch:" + ch);
                }
                if (ch != null) {
                    tuneChannel(ch);
                }
            } else if (PushMessageReceiver.ACTION_UPDATED_PROFILE.equalsIgnoreCase(action)) {
                PushMessageReceiver.ProfileUpdateType typeData = (PushMessageReceiver.ProfileUpdateType) intent.getSerializableExtra(PushMessageReceiver.KEY_TYPE);
                if (typeData == null) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "ACTION_UPDATED_PROFILE not contain type so return");
                    }
                    return;
                }
                if (Log.INCLUDE) {
                    Log.d(TAG, "ACTION_UPDATED_PROFILE typeData : " + typeData);
                }

                switch (typeData) {
                    case userDeleted:
                        AccountManager.getInstance().putResultCode(LauncherActivity.RESULT_CODE_USER_DELETED);
                        finish();
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        log("onCreate()");

        TimingLogger timings = new TimingLogger(TAG, "onCreate()");
        timings.addSplit("start");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_livetv);
        mainTvView = findViewById(R.id.main_tv_view);
        mainTvView.setOnChangedListener(new JasTvView.OnChangedListener() {
            @Override
            public void onChannelChanged(Channel channel, boolean selfChanged) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onChannelChanged : " + selfChanged + ", channel : " + channel.getDisplayNumber());
                }
                if (!selfChanged) {
                    getEpgViewModel().setWatchingChannel(channel);
                }
            }
        });
        ViewGroup parentScene = findViewById(R.id.controllerFrame);

        mChannelDataManager = TvSingletons.getSingletons(this).getChannelDataManager();
        mProgramDataManager = TvSingletons.getSingletons(this).getProgramDataManager();
//        mProgramDataManager.start();
        mTvInputManagerHelper = TvSingletons.getSingletons(this).getTvInputManagerHelper();
        mTvOverlayManager = new TvOverlayManager(this,
                parentScene,
                mChannelDataManager,
                mTvInputManagerHelper,
                mProgramDataManager);
        mTvOverlayManager.readyProgramGuide(new ProgramGuide.OnProgramGuideListener() {
            @Override
            public void onProgramGuideShow() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onProgramGuideShow");
                }
                mTvOverlayManager.hideMiniGuideFragment();
            }

            @Override
            public void onProgramGuideDismiss() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onProgramGuideDismiss : " + mTvOverlayManager.isProgramGuideVisible());
                }
                if (isTimeShiftMode()) {
                    return;
                }
                showMiniGuide();
                mViewModelManager.showChannelOSD();
            }
        });
        mSidePanelManager = new SidePanelManager();

        timings.addSplit("done");
        timings.dumpToLog();
    }

    public PopularChannelInteractor getPopularChannelInteractor() {
        return mainTvView;
    }

    public ChannelDataManager getChannelDataManager() {
        return mChannelDataManager;
    }

    public TvOverlayManager getTvOverlayManager() {
        return mTvOverlayManager;
    }

    public void onPaidChannelPurchased(String serviceId) {
        Channel ch = getChannelDataManager().getChannelByServiceId(serviceId);
        if (Log.INCLUDE) {
            Log.d(TAG, "onPaidChannelPurchased ch:" + (ch != null ? ch.getDisplayNumber() : null));
        }
        if (ch != null) {
            ch.setSubscribedChannel(true);
            tuneChannel(ch, false);
        }
    }

    public void setMute(boolean isMute) {
        mainTvView.setMute(isMute);
    }

    //implements View.OnFocusChangeListener
    public void onFocusChange(View view, boolean b) {
        log("onFocusChange : " + view + ", b :" + b);
    }

    //implements OnAppLaunchListener
    public void launchApp(LaunchPath path, String action) {
        log("launchApp " + path + ", action : " + action);

//        try {
//            Intent actionIntent = getPackageManager().getLeanbackLaunchIntentForPackage(action);
//            log("actionIntent : " + actionIntent);
//
//            if (actionIntent != null) {
//                startActivity(actionIntent);
//                return;
//            }
//        } catch (ActivityNotFoundException ex2) {
//            log("launchApp, ActivityNotFoundException, request to show google store, action : " + action);
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + action)));
//        }
        ExternalApplicationManager.getInstance().launchApp(this, action, true);
    }

    //implements OnFragmentVisibilityInteractor
    public void show(String tag) {
        log("show() tag:" + tag);
    }

    //implements OnFragmentVisibilityInteractor
    public void hide(String tag) {
        log("hide() tag:" + tag);
    }

    public void hideProgramGuide() {
        log("hideProgramGuide");
        mTvOverlayManager.hideProgramGuide();
    }

    private void setFocusAllow(ViewGroup viewGroup, boolean focus) {
        viewGroup.setDescendantFocusability(focus ? ViewGroup.FOCUS_AFTER_DESCENDANTS : ViewGroup.FOCUS_BLOCK_DESCENDANTS);
    }

    protected void addFocusChangeListener(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup parentView = (ViewGroup) view;
            int viewSize = parentView.getChildCount();
            for (int i = 0; i < viewSize; i++) {
                View childView = ((ViewGroup) view).getChildAt(i);
                addFocusChangeListener(childView);
            }
        }
        view.setOnFocusChangeListener(this);
    }

    protected void onPostCreate(Bundle savedInstanceState) {
        TimingLogger timings = new TimingLogger(TAG, "onPostCreate()");
        timings.addSplit("start");
        log("onPostCreate()");

        super.onPostCreate(savedInstanceState);
        addFocusChangeListener(getWindow().getDecorView());

        mTvOverlayManager.initPlaybackController();
        mViewModelManager = new MiniEpgViewModelManager(this);

        timings.addSplit("done");
        timings.dumpToLog();
    }

    protected void onDestroy() {
        log("onDestroy()");
        TimingLogger timings = new TimingLogger(TAG, "onDestroy()");
        timings.addSplit("start");

        super.onDestroy();
//        mProgramDataManager.stop();
        mainTvView.clearListener();
        mViewModelManager.dispose();
        mViewModelManager = null;

        timings.addSplit("done");
        timings.dumpToLog();
    }

    @Override
    protected void onStart() {
        log("onStart()");
        TimingLogger timings = new TimingLogger(TAG, "onStart()");
        timings.addSplit("start");

        super.onStart();

//        mainTvView.setVisibility(View.VISIBLE);
        mTvOverlayManager.hidePipOverlay();

        IntentFilter intentFilter = new IntentFilter(ACTION_CHANNEL_GUIDE);
        intentFilter.addAction(ACTION_PIP_SETTINGS);
        intentFilter.addAction(ACTION_CHANNEL_CHANGE);
        intentFilter.addAction(PushMessageReceiver.ACTION_UPDATED_PROFILE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, intentFilter);

        //check READ_TV_LISTINGS permission
        if (!PermissionUtils.hasReadTvListings(this)) {
            PermissionUtils.requestPermissions(this, PermissionUtils.PERMISSION_READ_TV_LISTINGS, 0);
        }
        timings.addSplit("done");
        timings.dumpToLog();

        mainTvView.attachChannelListListener();
    }

    @Override
    protected void onStop() {
        log("onStop()");
        super.onStop();
        mTvOverlayManager.hideMiniGuideFragment();
        mTvOverlayManager.stopTimeShift();
//        TimingLogger timings = new TimingLogger(TAG, "onStop()");
//        timings.addSplit("start");
//        mainTvView.setVisibility(View.GONE);
        mainTvView.reset();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        mainTvView.detachChannelListListener();
//        timings.addSplit("done");
//        timings.dumpToLog();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        log("onResume()");
        TimingLogger timings = new TimingLogger(TAG, "onResume()");
        timings.addSplit("start");

        super.onResume();

        //start live tv activity with channel tune
        Intent intent = getIntent();
        long channelId = PlaybackUtil.getChannelIdFromIntent(intent, -1);
        boolean tuneNeeded = true;

        if (mSidePanelManager != null && mSidePanelManager.isOpenMenu()) {
            mSidePanelManager.closeMenu();
        }

        if (intent != null &&
                intent.getAction() != null) {
            if (Log.INCLUDE) {
                Log.d(TAG, "intent.getAction() : " + intent.getAction());
            }
            if (intent.getAction().equalsIgnoreCase(PlaybackUtil.ACTION_START_GUIDE)) {
                mTvOverlayManager.showProgramGuide();
                if (mainTvView.getCurrentChannel() != null) {
                    //ignore liveTv key when liveTv activity is running.
                    tuneNeeded = false;
                }
            } else if (intent.getAction().equalsIgnoreCase(PlaybackUtil.ACTION_START_FAV_GUIDE)) {
                mTvOverlayManager.showProgramGuide(GenreUtil.getId(GenreUtil.FAVORITE_CHANNELS));
            } else if (intent.getAction().equalsIgnoreCase(PlaybackUtil.ACTION_START_LIVE)) {
                if (mTvOverlayManager.isProgramGuideVisible()) {
                    mTvOverlayManager.hideProgramGuide();
                }

                if (mainTvView.getCurrentChannel() != null) {
                    //ignore liveTv key when liveTv activity is running.
                    tuneNeeded = false;
                }

                if (mTvOverlayManager.isTimeShiftActive()) {
                    TimeShiftFragment shiftFragment = mTvOverlayManager.getTimeShiftFragment();
                    if (shiftFragment != null) {
                        Channel channel = shiftFragment.getCurrentChannel();
                        if (channel != null) {
                            channelId = channel.getId();
                        }
                    }
                    mTvOverlayManager.stopTimeShift();
                    mainTvView.reset();
                    tuneNeeded = true;
                }
            }
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "onResume() tuneNeeded : " + tuneNeeded);
        }
        if (tuneNeeded) {
            tuneDefaultChannel(channelId);
        }

        setIntent(null);

        timings.addSplit("done");
        timings.dumpToLog();
    }

    @Override
    public TuneResult tuneTimeShift(Channel channel, Program program, long startTime) {
        if (Log.INCLUDE) {
            Log.d(TAG, "tuneTimeShift() channel:" + channel);
            Log.d(TAG, "tuneTimeShift() program:" + program);
        }
        if (TIMESHIFT_DISABLED) {
            if (Log.INCLUDE) {
                Log.d(TAG, "tuneTimeShift() timeShift disabled!!!");
            }
            return null;
        }
        if (mTvOverlayManager.isMiniGuideVisible()) {
            mTvOverlayManager.hideMiniGuideFragment();
        }
        mTvOverlayManager.startTimeShift(channel, program, startTime, mainTvView);
        return null;
    }

    private boolean isTimeShiftMode() {
        return mTvOverlayManager.isTimeShiftActive();
    }

    @Override
    public void stopTimeShift(Channel channel) {
        if (Log.INCLUDE) {
            Log.d(TAG, "tuneLiveTv() channel:" + channel);
        }
        if (TIMESHIFT_DISABLED) {
            if (Log.INCLUDE) {
                Log.d(TAG, "tuneLiveTv() timeShift disabled!!!");
            }
            return;
        }

        mainTvView.resetTimeShift();
        tuneChannel(channel);
    }

    //implements TvViewInteractor
    public void resizeTvView(Rect rect) {
        mainTvView.resizeTvView(rect);
        log("resizeTvView() rect:" + rect);
    }

    private void toggleReserveProgram(Program program) {
        if (program != null) {
            boolean isReserved = reminderAlertManager.isReserved(program);
            if (!isReserved) {
                boolean isSuccess = reminderAlertManager.requestAddReminder(this, (ProgramImpl) program.toParcelable());
                if (isSuccess) {
                    reminderAlertManager.toastReminderSuccess(getApplicationContext());
                }
            } else {
                boolean isSuccess = reminderAlertManager.requestRemoveReminder(this, (ProgramImpl) program.toParcelable());
                if (isSuccess) {
                    reminderAlertManager.toastReminderCancel(getApplicationContext());
                }
            }
        }
    }

    //implements TvViewInteractor
    public void reserveProgram(Program program, Handler handler) {
        log("reserveProgram() program:" + program);
        boolean isSuccess = reminderAlertManager.requestAddReminder(this, (ProgramImpl) program.toParcelable());
        if (isSuccess) {
            reminderAlertManager.toastReminderSuccess(getApplicationContext());
        }
        handler.sendEmptyMessage(WHAT_BASE_RESULT_CODE);
//        long channelId = program.getChannelId();
//        tuneTimeShift(mChannelDataManager.getChannel(channelId), program);
    }

    //implements TvViewInteractor
    public void cancelReserveProgram(Program program, Handler handler) {
        log("cancelReserveProgram() program:" + program);
        boolean isSuccess = reminderAlertManager.requestRemoveReminder(this, (ProgramImpl) program.toParcelable());
        if (isSuccess) {
            reminderAlertManager.toastReminderCancel(getApplicationContext());
        }
        handler.sendEmptyMessage(WHAT_BASE_RESULT_CODE);
    }

    //implements TvViewInteractor
    public void setPreviewListener(ChannelPreviewIndicator.OnPreviewListener onPreviewListener) {
        log("setPreviewListener() onPreviewListener:" + onPreviewListener);
        mainTvView.setPreviewListener(onPreviewListener);
    }

    @Override
    protected void reminderConflict(Program program, Reminder conflictedReminder) {
        super.reminderConflict(program, conflictedReminder);
        reminderAlertManager.showReminderConflictDialog(getApplicationContext(), getSupportFragmentManager(), program, conflictedReminder);
    }

    @Override
    protected void reminderListChange(List<Reminder> list) {
        super.reminderListChange(list);
//        Channel epgChannel = mViewModelManager.getMiniEpgChannel();
//        if (epgChannel != null) {
//            mViewModelManager.updateEpgViewModel(epgChannel);
//        }
    }

    //    public void swapMainAndPipChannel() {
//        JasTvView.swap(mainTvView, pipTvView);
//    }

    private TuneResult tuneDefaultChannel(long channelId) {
        log("launcher version : v" + BuildConfig.VERSION_NAME + "/" + BuildConfig.FLAVOR + "/" + BuildConfig.BUILD_TYPE);
        log("tuneDefaultTune(), channelId=" + channelId);
        TimingLogger timings = new TimingLogger(TAG, "tuneDefaultTune()");
        timings.addSplit("start");

//        if (DEBUG) {
//            List<Channel> list = mChannelDataManager.getChannelList();
//            printChannelList("tuneDefaultTune() ch", list);
//        }

        Channel channel = null;
        if (channelId != -1) {
            //get channel from all channel ring
            channel = mChannelDataManager.getChannelRingStorage().getChannelRing(ChannelRingStorage.RING_ALL).getChannel(channelId);
        }
        if (channel == null) {
            channel = mChannelDataManager.getGuideChannel();
        }

        if (DEBUG) log("tuneDefaultChannel() channel:" + channel);
        TuneResult tuneResult = tuneChannel(channel);

//        mViewModelManager.showMiniGuide();

        timings.addSplit("done");
        timings.dumpToLog();
        return tuneResult;
    }

    //implements TvViewInteractor
    public TuneResult tuneChannel(Channel channel) {
        log("tuneChannel() channel:" + channel);
        mTvOverlayManager.stopTimeShift();
        TimingLogger timings = new TimingLogger(TAG, "tuneChannel(channel)");
        timings.addSplit("start");

        TuneResult result = tuneChannel(channel, true);
        log("tuneChannel() result:" + result);

        timings.addSplit("done");
        timings.dumpToLog();
        return result;
    }

    public TuneResult tuneChannel(Channel channel, boolean checkBlockNeeded) {
        log("tuneChannel() channel:" + channel);
        TimingLogger timings = new TimingLogger(TAG, "tuneChannel(channel, checkBlockNeeded)");
        timings.addSplit("start");

        if (channel == null) {
            log("tuneChannel() channel is null");
            return TuneResult.TUNE_SUCCESS;
        }

        mChannelDataManager.initEpgChannelRing(channel);

        //update program
        Program program = mViewModelManager.updateAndGetProgram(channel);

        log("tuneChannel() channel=[" + channel.getDisplayNumber() + "]" + channel.getDisplayName() + ", id:" + channel.getId() + ", inputId:" + channel.getInputId());
        timings.addSplit("before tuneChannel()");

        if (channel.isUHDChannel() && mTvOverlayManager.isPipShowing()) {
            mTvOverlayManager.hidePipOverlay();
            showPipUHDErrorToast();
        }

        JasTvView.TuningResult tuningResult = mainTvView.tuneChannel(channel, program, checkBlockNeeded, true);
        timings.addSplit("after tuneChannel()");
        TuneResult tuneResult = TvViewInteractor.convertTuneResult(tuningResult);

        mViewModelManager.updateOSDViewModel(channel);
        mViewModelManager.updateEpgViewModel(channel);
        mViewModelManager.updateTVGuideViewModel(channel, program, tuningResult);
        mProgramDataManager.notifyChannelTuned(channel.getId());
        timings.addSplit("done");
        timings.dumpToLog();
        return tuneResult;
    }

    public void updateTuneResult(JasTvView.TuningResult tuningResult) {
        if (mViewModelManager != null) {
            mViewModelManager.updateEpgViewModel(tuningResult);
            mViewModelManager.updateTVGuideViewModel(tuningResult);
        }
    }

    public Program updateAndGetProgram(Channel channel) {
        return mViewModelManager.updateAndGetProgram(channel);
    }

    public void hideOverlaysForTune() {
        log("hideOverlaysForTune()");
        mTvOverlayManager.hideOverlays(TvOverlayManager.FLAG_HIDE_OVERLAYS_KEEP_PROGRAM_GUIDE);
    }

    @Override
    @MainThread
    public void onBackPressed() {
    }

    private boolean isKeyEventBlocked(int keyCode, KeyEvent event) {
        boolean isKeyDown = event.getAction() == KeyEvent.ACTION_DOWN;
        if (isKeyDown ? mTvOverlayManager.onKeyDown(keyCode, event)
                : mTvOverlayManager.onKeyUp(keyCode, event)) {
            return true;
        }


        if (mSidePanelManager.isOpenMenu()) {
            boolean consumed = true;
            switch (keyCode) {
                case KeyEvent.KEYCODE_INFO:
                    break;
                case KeyEvent.KEYCODE_CHANNEL_UP:
                case KeyEvent.KEYCODE_CHANNEL_DOWN:
                case KeyEvent.KEYCODE_GUIDE:
                case KeyEvent.KEYCODE_0:
                case KeyEvent.KEYCODE_1:
                case KeyEvent.KEYCODE_2:
                case KeyEvent.KEYCODE_3:
                case KeyEvent.KEYCODE_4:
                case KeyEvent.KEYCODE_5:
                case KeyEvent.KEYCODE_6:
                case KeyEvent.KEYCODE_7:
                case KeyEvent.KEYCODE_8:
                case KeyEvent.KEYCODE_9:
                    mSidePanelManager.closeMenu();
                    consumed = false;
                    break;
            }

            return consumed;
        }
        return false;
    }

    private boolean isKeyNum(int keyCode, KeyEvent event) {
//        if (KeyEventManager.isNumberKey(keyCode) && mTvOverlayManager.isProgramGuideVisible()) {
//            return true;
//        }

        return mTvOverlayManager.onKeyNum(keyCode, event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        TimingLogger timings = new TimingLogger(TAG, "onKeyDown()");
        timings.addSplit("start");

        keyCode = KeyEventManager.correctKeyCode(keyCode, event);
        log("call onKeyDown() : " + KeyEvent.keyCodeToString(keyCode) + ", keyCode:" + keyCode + ", scancode:" + event.getScanCode() + ", repeatCount:" + event.getRepeatCount());

        if (isKeyEventBlocked(keyCode, event)) {
            log("call onKeyDown() - isKeyEventBlocked! : " + KeyEvent.keyCodeToString(keyCode) + ", keyCode:" + keyCode);
            return super.onKeyDown(keyCode, event);
        }
        if (isKeyNum(keyCode, event)) {
            log("call onKeyDown() - isKeyNum! : " + KeyEvent.keyCodeToString(keyCode) + ", keyCode:" + keyCode);
            return super.onKeyDown(keyCode, event);
        }


        if (mViewModelManager.prefetchKeyEvent(keyCode, event)) {
            log("call onKeyDown() - mViewModelManager.prefetchKeyEvent() : true");
            return true;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (!TIMESHIFT_DISABLED && mTvOverlayManager.isTimeShiftActive() == true) {
                    //do not anything
                } else {
                    AccountManager.getInstance().putResultCode(LauncherActivity.RESULT_CODE_LIVE_TV);
                    super.onBackPressed();
                }
                break;
            case KeyEvent.KEYCODE_TV:
                if (mTvOverlayManager.isProgramGuideVisible()) {
                    mTvOverlayManager.hideProgramGuide();
                }
                break;
            case KeyEvent.KEYCODE_GUIDE://KeyGuide
                CWMPServiceProvider.getInstance().notifyHotKeyClicked(keyCode);
                mTvOverlayManager.showProgramGuide();
                break;
            case KeyEvent.KEYCODE_BUTTON_1://Favorite Key, go to programGuide.favorite
//                mTvOverlayManager.showProgramGuide(GenreUtil.getId(GenreUtil.FAVORITE_CHANNELS));
                Linker.gotoMyMenu(mTvOverlayManager, getSupportFragmentManager(), this);
                break;
            case KeyEvent.KEYCODE_INFO://Set key, show side option menu
                mSidePanelManager.openMenu();
                break;
            case KeyEvent.KEYCODE_CHANNEL_UP:
                mViewModelManager.tuneChannel(event.getRepeatCount() == 0, true);
                break;
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                mViewModelManager.tuneChannel(event.getRepeatCount() == 0, false);
                break;
            case KeyEvent.KEYCODE_MEDIA_REWIND:
                if (TIMESHIFT_DISABLED) {
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onKeyDown() timeShift disabled!!!");
                    }
                    break;
                }
                if (mTvOverlayManager.isTimeShiftActive()) {
                    break;
                }
                mViewModelManager.startTimeShift(
                        JasmineEpgApplication.getSingletons(this).getClock().currentTimeMillis() - (11 * TimeUtil.SEC));
                break;
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                if (mTvOverlayManager.isTimeShiftActive() && !TIMESHIFT_DISABLED) {
                } else {
                    mViewModelManager.showMiniGuide();
                }
                break;
        }

        timings.addSplit("done");
        timings.dumpToLog();
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        keyCode = KeyEventManager.correctKeyCode(keyCode, event);
        log("call onKeyUp() : " + KeyEvent.keyCodeToString(keyCode) + ", keyCode:" + keyCode + ", scancode:" + event.getScanCode() + ", repeatCount:" + event.getRepeatCount());
        if (isKeyEventBlocked(keyCode, event)) {
            log("call onKeyDown() - isKeyEventBlocked! : " + KeyEvent.keyCodeToString(keyCode) + ", keyCode:" + keyCode);
            return super.onKeyUp(keyCode, event);
        }
//        if (isKeyNum(keyCode, event)) {
//            log("call onKeyDown() - isKeyNum! : " + KeyEvent.keyCodeToString(keyCode) + ", keyCode:" + keyCode);
//            return super.onKeyUp(keyCode, event);
//        }
        switch (keyCode) {
            case KeyEvent.KEYCODE_CHANNEL_UP:
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                mViewModelManager.finishTuneChannel();
                break;
            case KeyEvent.KEYCODE_DPAD_UP:
            case KeyEvent.KEYCODE_DPAD_DOWN:
                mViewModelManager.finishShowChannel();
                break;
        }
        return super.onKeyUp(keyCode, event);
    }

    //implements PinDialogFragment.OnPinCheckedListener.onPinChecked()
    public void onPinChecked(boolean checked, int type, String rating) {
        log("onPinChecked() checked:" + checked + ", type:" + type + ", rating:" + rating);
        if (checked && mainTvView.isBlocked()) {
            Channel blockedChannel = mainTvView.getCurrentChannel();
            tuneChannel(blockedChannel, false);
        }
    }

    private void showPinDialogFragment() {
        PinDialogFragment dialog = PinDialogFragment.create(PinDialogFragment.PIN_DIALOG_TYPE_UNLOCK_CHANNEL);
        mTvOverlayManager.showDialogFragment(PinDialogFragment.DIALOG_TAG, dialog, false);
    }

    private void showSubscribeChannelDialogFragment(Channel currentChannel) {
        if (currentChannel == null) {
            return;
        }
        SubscribeChannelDialogFragment dialog = SubscribeChannelDialogFragment.create(currentChannel.getServiceId());
        mTvOverlayManager.showDialogFragment(SubscribeChannelDialogFragment.DIALOG_TAG, dialog, false);
    }

    private void showPipOption() {
        if (mTvOverlayManager.isPipShowing()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "pressed showPipOption but already showing please check it");
            }
            return;
        }
        Channel currentChannel = mainTvView.getCurrentChannel();
        if (currentChannel != null && currentChannel.isUHDChannel()) {
            showPipUHDErrorToast();
            return;
        }

        if (mTvOverlayManager.isMiniGuideVisible()) {
            mTvOverlayManager.hideMiniGuideFragment();
        }
        mTvOverlayManager.showPipOverlay(mViewModelManager.getCurrentChannel(), this);
    }

    void showPipUHDErrorToast() {
        JasminToast.makeToast(this, R.string.pip_not_available);
    }

    public void checkAndShowMiniGuide() {
        if (mViewModelManager.isMiniGuideFixed() && !isTimeShiftMode()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "checkAndShowMiniGuide() showMiniGuide()");
            }
            mViewModelManager.showMiniGuide();
        } else {
            if (Log.INCLUDE) {
                Log.d(TAG, "checkAndShowMiniGuide() don't showMiniGuide()");
            }
        }
    }

    public void showMiniGuide() {
        mViewModelManager.showMiniGuide();
    }

    private void printChannel(String msg, Channel channel) {
        if (channel == null) {
            log("[" + msg + "] channel : null");
        } else {
            log("[" + msg + "] channel No:" + channel.getDisplayNumber()
                    + ", Name:" + channel.getDisplayName()
                    + ", inputId:" + channel.getInputId()
                    + ", isLocked:" + channel.isLocked());
        }
    }

    public void printChannelList(String msg, List<Channel> channels) {
        log("[" + msg + "] channels.size : " + (channels != null ? channels.size() : "null"));
        Channel ch;
        for (int i = 0; channels != null && i < channels.size(); i++) {
            ch = channels.get(i);
            log("[" + msg + "]   channel[" + i + "]=[" + ch.getDisplayNumber() + "]" + ch.getDisplayName() + ", id:" + ch.getId() + ", inputId:" + ch.getInputId() + ", isLocked:" + ch.isLocked());
        }
        log("[" + msg + "] done");
    }

    private class MiniEpgViewModelManager implements MiniEpgDataManager.MiniEpgDataListener {
        private final String TAG = MiniEpgViewModelManager.class.getSimpleName();

        //Channel & Program data manager
        private MiniEpgDataManager mMiniEpgDataManager;
        private final MiniEpgDataManager.TableEntry[] mProgramEntries = new MiniEpgDataManager.TableEntry[2];

        //ViewModel
        private OSDViewModel mOSDViewModel = null;
        private EpgViewModel mEpgViewModel = null;
        private TVGuideViewModel mTVGuideViewModel = null;

        //auto hider
        private final AutoHider autoHider;

        //status
        private boolean stateTuneChannel = false;
        private boolean stateShowChannel = false;

        //cached program
        private Channel requestedChannel;
        private int selectedIndex;
        private List<MiniEpgDataManager.TableEntry> cachedEntries = Collections.emptyList();

        private MiniEpgViewModelManager(FragmentActivity activity) {
            if (Log.INCLUDE) {
                Log.d(TAG, "MiniEpgViewModelManager() created...");
            }
            TimingLogger timings = new TimingLogger(TAG, "MiniEpgViewModelManager()");
            timings.addSplit("start");

            mMiniEpgDataManager = new MiniEpgDataManager(mTvInputManagerHelper, mChannelDataManager, mProgramDataManager, this);
            timings.addSplit("create MiniEpgDataManager");
            mMiniEpgDataManager.updateChannels(true);
            timings.addSplit("update Channels");

            final ViewModelFactory viewModelFactory = ViewModelFactory.getInstance(getApplication(), activity);
            mOSDViewModel = ViewModelProviders.of(activity, viewModelFactory).get(OSDViewModel.class);
            mEpgViewModel = ViewModelProviders.of(activity, viewModelFactory).get(EpgViewModel.class);
            mTVGuideViewModel = ViewModelProviders.of(activity, viewModelFactory).get(TVGuideViewModel.class);

            // Defect #526 : 3secs -> 5.5secs
            // Defect #1432 : 5.5secs -> 10secs
            autoHider = new AutoHider(10 * 1000, () -> {
                if (isMiniGuideVisible()) {
                    if (isMiniGuideFixed()) {
//                        refreshMiniEpg();
                    } else {
                        hideMiniGuide();
                    }
                }
            });
            timings.addSplit("done");
            timings.dumpToLog();
        }

        private boolean isMiniGuideFixed() {
            boolean fixed;
            JasTvView.TuningResult result = mainTvView.getTuningResult();
            switch (result) {
                case TUNING_BLOCKED_CHANNEL:
                case TUNING_BLOCKED_ADULT_CHANNEL:
                case TUNING_BLOCKED_PROGRAM:
                case TUNING_BLOCKED_UNSUBSCRIBED:
                    fixed = true;
                    break;
                default:
                    fixed = false;
                    break;
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "isMiniGuideFixed() fixed:" + fixed+", (result:"+result+")");
            }
            return fixed;
        }

        private void dispose() {
            if (Log.INCLUDE) {
                Log.d(TAG, "MiniEpgViewModelManager.dispose()");
            }
            TimingLogger timings = new TimingLogger(TAG, "dispose()");
            timings.addSplit("start");

            autoHider.cancel();

            mMiniEpgDataManager.dispose();
            mMiniEpgDataManager = null;
            mOSDViewModel = null;
            mEpgViewModel = null;
            mTVGuideViewModel = null;
            timings.addSplit("done");
            timings.dumpToLog();
        }

        //implements MiniEpgDataManager.MiniEpgDataListener.channelUpdated()
        public void channelUpdated() {
            if (Log.INCLUDE) {
                Log.d(TAG, "channelUpdated()");
            }
            if (!LiveTvActivity.this.isActivityStarted()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "channelUpdated() but activity state not started so return");
                }
                return;
            }

            //If the EPG channel ring does not contain the watching channel, add it.
            Channel watchingChannel = mEpgViewModel.watchingChannel != null ? mEpgViewModel.watchingChannel.getCurrentChannel() : null;
            if (!mChannelDataManager.containsEpgChannelRing(watchingChannel)) {
                mChannelDataManager.addChannelToEpgChannelRing(watchingChannel);
            }

            if (mEpgViewModel != null && mEpgViewModel.currentChannel != null) {
                Channel currentChannel = mEpgViewModel.currentChannel.getCurrentChannel();
                if (!mChannelDataManager.getChannelRingStorage().getChannelRing(ChannelRingStorage.RING_MINIEPG).contains(currentChannel)) {

                    Channel newChannel = mEpgViewModel.watchingChannel != null ? mEpgViewModel.watchingChannel.getCurrentChannel() : null;
                    if (!mChannelDataManager.getChannelRingStorage().getChannelRing(ChannelRingStorage.RING_MINIEPG).contains(newChannel)) {
                        newChannel = mChannelDataManager.getLowestChannel();
                    }

                    if (Log.INCLUDE) {
                        Log.d(TAG, "channelUpdated() selected channel is gone. newch:"+newChannel);
                    }
                    showChannel(true, newChannel);
                }
            }
        }

        //implements MiniEpgDataManager.MiniEpgDataListener.programUpdated()
        public void programUpdated() {
            if (Log.INCLUDE) {
                Log.d(TAG, "programUpdated() ch:[" + (requestedChannel != null ? requestedChannel.getDisplayNumber() : null) + "]");
            }
            if (!LiveTvActivity.this.isActivityStarted()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "programUpdated() but activity state not started so return");
                }
                return;
            }
            TimingLogger timings = new TimingLogger(TAG, "programUpdated()");
            timings.addSplit("start");

            if (stateTuneChannel || stateShowChannel) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "programUpdated() - do not update when changing channel");
                }
                return;
            }

            if (requestedChannel != null && (requestedChannel == mEpgViewModel.currentChannel.getCurrentChannel())) {
                MiniEpgDataManager.TableEntry previousEntry = getCurrentEntry();
                cachedEntries = mMiniEpgDataManager.getTableEntry(requestedChannel.getId());
                if (Log.INCLUDE) {
                    Log.d(TAG, "programUpdated() update program size:" + cachedEntries.size());
                }
                MiniEpgDataManager.TableEntry currentEntry = getCurrentEntry();
                if (previousEntry != currentEntry) {
                    selectedIndex = 0;
                }

                if (Log.INCLUDE) {
                    Log.d(TAG, "programUpdated() selected index:" + selectedIndex);
                }

                updateEpgViewModel(requestedChannel);
            }
            timings.addSplit("done");
            timings.dumpToLog();
        }

        private void retrieveProgram(Channel channel) {
            if (Log.INCLUDE) {
                Log.d(TAG, "retrieveProgram() - channel:[" + channel.getDisplayNumber() + "]");
            }
            TimingLogger timings = new TimingLogger(TAG, "retrieveProgram()");
            timings.addSplit("start");

            //retrieve program info
            mMiniEpgDataManager.onChannelTuned(channel.getId());
            //get cached program
            updatePrograms(channel);
            timings.addSplit("done");
            timings.dumpToLog();
        }

        private void updatePrograms(Channel channel) {
            if (Log.INCLUDE) {
                Log.d(TAG, "updatePrograms() - channel:[" + channel.getDisplayNumber() + "]");
            }
            TimingLogger timings = new TimingLogger(TAG, "updatePrograms()");
            timings.addSplit("start");

            requestedChannel = channel;
            cachedEntries = mMiniEpgDataManager.getTableEntry(requestedChannel.getId());
            if (cachedEntries.size() > 0) {
                selectedIndex = 0;
            } else {
                selectedIndex = 0;
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "updatePrograms() - channel:[" + channel.getDisplayNumber() + "], [" + selectedIndex + "/" + cachedEntries.size() + "]");
            }
            timings.addSplit("done");
            timings.dumpToLog();
        }

        private Program getCurrentProgram() {
            if (selectedIndex < cachedEntries.size()) {
                return cachedEntries.get(selectedIndex).getProgram();
            } else {
                return null;
            }
        }

        private MiniEpgDataManager.TableEntry getCurrentEntry() {
            if (0 <= selectedIndex && selectedIndex < cachedEntries.size()) {
                return cachedEntries.get(selectedIndex);
            } else {
                return null;
            }
        }

        private MiniEpgDataManager.TableEntry getNextEntry() {
            if (0 <= selectedIndex + 1 && selectedIndex + 1 < cachedEntries.size()) {
                return cachedEntries.get(selectedIndex + 1);
            } else {
                return null;
            }
        }

        private Program updateAndGetProgram(Channel channel) {
            if (Log.INCLUDE) {
                Log.d(TAG, "updateAndGetProgram() - channel:" + channel.getDisplayNumber());
            }
            TimingLogger timings = new TimingLogger(TAG, "updateAndGetProgram()");
            timings.addSplit("start");
            //retrieve program info
            retrieveProgram(channel);
            timings.addSplit("done");
            timings.dumpToLog();
            return getCurrentProgram();
        }

        private boolean isMiniGuideVisible() {
            return mTvOverlayManager.isMiniGuideVisible();
        }

        private void hideMiniGuide() {
            mTvOverlayManager.hideMiniGuideFragment();
        }

        private boolean prefetchKeyEvent(int keyCode, KeyEvent event) {
            if (Log.INCLUDE) {
                Log.d(TAG, "prefetchKeyEvent(), isMiniGuideVisible:" + isMiniGuideVisible());
                Log.d(TAG, "prefetchKeyEvent(), isTimeShiftActive:" + mTvOverlayManager.isTimeShiftActive());

            }
            if (isMiniGuideVisible()) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        if (isMiniGuideFixed()) {
                            return false;
                        } else {
                            mTvOverlayManager.hideMiniGuideFragment();
                        }
                        return true;
                    case KeyEvent.KEYCODE_DPAD_UP:
                        showChannel(event.getRepeatCount() == 0, true);
                        return true;
                    case KeyEvent.KEYCODE_DPAD_DOWN:
                        showChannel(event.getRepeatCount() == 0, false);
                        return true;
                    case KeyEvent.KEYCODE_DPAD_LEFT:
                        keyLeft();
                        return true;
                    case KeyEvent.KEYCODE_DPAD_RIGHT:
                        keyRight();
                        return true;
                    case KeyEvent.KEYCODE_DPAD_CENTER: {
                        keyOk();
                        return true;
                    }
                }
                return false;
            } else if (TIMESHIFT_DISABLED && mTvOverlayManager.isTimeShiftActive()) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        if (Log.INCLUDE) {
                            Log.d(TAG, "stopTimeShift()");
                        }
//                        Channel channel = getCurrentChannel();
//                        tuneLiveTv(channel);
                        return false;
                }
                return false;
            } else {
                return false;
            }
        }

        private void keyOk() {
            if (Log.INCLUDE) {
                Log.d(TAG, "keyOk()");
            }
            TimingLogger timings = new TimingLogger(TAG, "keyOk()");
            timings.addSplit("start");

            //mini guide visible
            MiniEpgDataManager.TableEntry entry = getCurrentEntry();
            if (entry != null && entry.isFutureProgram()) {
                Program program = getCurrentProgram();
                toggleReserveProgram(program);
                updateEpgViewModel(requestedChannel);//update remind icon in miniepg
            } else {
                Channel currentChannel = getCurrentChannel();
                Channel epgCh = mEpgViewModel.currentChannel.getCurrentChannel();
                if (epgCh != currentChannel) {
                    //tune selected channel
                    updateEpgViewModel(epgCh);
                    LiveTvActivity.this.tuneChannel(epgCh);
                } else if (mainTvView.isBlocked() || mainTvView.isPreview()) {
                    //unlock channel or subscribe channel
                    ParentalController.BlockType blockType = mainTvView.getBlockType();
                    if (blockType == ParentalController.BlockType.BLOCK_UNSUBSCRIBED
                            || blockType == ParentalController.BlockType.PREVIEW_CHANNEL
                            || (blockType == ParentalController.BlockType.BLOCK_ADULT_CHANNEL && currentChannel.isPaidChannel())) {
                        showSubscribeChannelDialogFragment(currentChannel);
                    } else {
                        showPinDialogFragment();
                    }
                } else {
                    //hide mini guide
                    hideMiniGuide();
                }
            }
            timings.addSplit("done");
            timings.dumpToLog();
        }

        private void keyLeft() {
            if (Log.INCLUDE) {
                Log.d(TAG, "keyLeft()");
            }
            if (isChannelBlocked(requestedChannel)) {
                return;
            }

            if (selectedIndex > 0) {
                selectedIndex--;
                updateEpgViewModel(requestedChannel);
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "keyLeft() index:" + selectedIndex + " of " + cachedEntries.size());
            }
        }

        private void keyRight() {
            if (Log.INCLUDE) {
                Log.d(TAG, "keyRight()");
            }
            if (isChannelBlocked(requestedChannel)) {
                return;
            }

            int newIndex = selectedIndex + 1;
            if (newIndex < cachedEntries.size()) {
                selectedIndex = newIndex;
                updateEpgViewModel(requestedChannel);
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "keyRight() index:" + selectedIndex + " of " + cachedEntries.size());
            }
        }

        private Channel getCurrentChannel() {
            return mOSDViewModel.currentTvData.getCurrentChannel();
        }

        private Channel getMiniEpgChannel() {
            return mEpgViewModel.getCurrentChannel();
        }

        private void showMiniGuide() {
            Channel channel = getCurrentChannel();
            showChannel(true, channel);
        }

        private void showChannelOSD() {
            mOSDViewModel.setValue(getCurrentChannel());
        }

        private void updateOSDViewModel(Channel channel) {
            if (Log.INCLUDE) {
                Log.d(TAG, "updateOSDViewModel() channel:" + channel);
            }
            mOSDViewModel.setValue(channel);
            mEpgViewModel.setWatchingChannel(channel);
        }

        private void updateEpgViewModel(JasTvView.TuningResult tuningResult) {
            if (tuningResult == JasTvView.TuningResult.TUNING_BLOCKED_PROGRAM) {
                mEpgViewModel.setBlockType(ParentalController.BlockType.BLOCK_PROGRAM);
            }
            if (!isMiniGuideFixed()) {
                autoHider.refreshTimer();
            } else {
                autoHider.cancel();
            }
        }

        private ParentalController.BlockType getTunedChannelBlockType(Channel channel) {
            Channel currentChannel = getCurrentChannel();
            if (currentChannel == channel) {
                return mainTvView.getBlockType();
            }
            return ParentalController.BlockType.CLEAN_CHANNEL;
        }

        private ParentalController.BlockType getSelectedChannelBlockType(Channel channel) {
            return !isChannelBlocked(channel) ? ParentalController.BlockType.CLEAN_CHANNEL
                    : channel.isAdultChannel() ? ParentalController.BlockType.BLOCK_ADULT_CHANNEL : ParentalController.BlockType.BLOCK_CHANNEL;
        }

        private boolean isChannelBlocked(Channel channel) {
            if (Log.INCLUDE) {
                Log.d(TAG, "isChannelBlocked() ch:[" + channel.getDisplayNumber() + "]");
            }
            if (channel.isLocked() || channel.isAdultChannel()) {
                if (channel == getCurrentChannel()) {
                    return mainTvView.isBlocked();
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }

        private int getFlags(Channel channel) {
            //init flags
            int flags = EpgViewModel.FLAG_DEFAULT;
            if (mEpgViewModel.getWatchingChannel() == channel) {
                flags = EpgViewModel.setFlag(flags, EpgViewModel.FLAG_IS_WATCHING_CHANNEL);
            }
            if (mainTvView.isUnblockedChannel(channel)) {
                flags = EpgViewModel.setFlag(flags, EpgViewModel.FLAG_IS_UNBLOCKED_CHANNEL);
            }
            if (getCurrentEntry() != null && getCurrentEntry().getProgram() != null) {
                flags = EpgViewModel.setFlag(flags, EpgViewModel.FLAG_HAS_PROGRAM);
            }
            if (selectedIndex == 0) {
                flags = EpgViewModel.setFlag(flags, EpgViewModel.FLAG_IS_CURRENT_PROGRAM);
            }
            if (mTvOverlayManager.isPipShowing()) {
                flags = EpgViewModel.setFlag(flags, EpgViewModel.FLAG_PIP_VISIBLE);
            }
            return flags;
        }

        private void updateEpgViewModel(Channel channel) {
            if (Log.INCLUDE) {
                Log.d(TAG, "updateEpgViewModel() {");
                Log.d(TAG, "updateEpgViewModel()   ch:[no:" + channel.getDisplayNumber() + "/id:"+channel.getId()+"], prog:[" + selectedIndex + "/" + cachedEntries.size() + "]");
                Log.d(TAG, "updateEpgViewModel()   selectedChBlockType:"+getSelectedChannelBlockType(channel));
                Log.d(TAG, "updateEpgViewModel()   tunedChBlockType:"+getTunedChannelBlockType(channel));
                Log.d(TAG, "updateEpgViewModel()   flag:"+EpgViewModel.flagToString(getFlags(channel)));
                Log.d(TAG, "updateEpgViewModel()   currentEntry:"+getCurrentEntry());
                Log.d(TAG, "updateEpgViewModel()   nextEntry:"+getNextEntry());
                Log.d(TAG, "updateEpgViewModel() }");
            }
            if (mSidePanelManager.isOpenMenu()) {
                Log.d(TAG, "updateEpgViewModel() mSidePanelManager.isOpened() do not update EpgViewModel!!!");
                return;
            }
            mEpgViewModel.setValue(channel, getCurrentEntry(), getNextEntry(), getSelectedChannelBlockType(channel), getTunedChannelBlockType(channel), getFlags(channel));
            if (!isMiniGuideFixed()) {
                autoHider.refreshTimer();
            } else {
                autoHider.cancel();
            }
        }

        private void updateTVGuideViewModel(JasTvView.TuningResult tuningResult) {
            mTVGuideViewModel.setValue(TvViewInteractor.convertTuneResult(tuningResult));
        }

        private void updateTVGuideViewModel(Channel channel, Program program, JasTvView.TuningResult tuningResult) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showEPG() channel:" + channel
                        + ", program=" + program);
            }

            mTVGuideViewModel.setValue(channel);
            mTVGuideViewModel.setValue(program);
            mTVGuideViewModel.setValue(TvViewInteractor.convertTuneResult(tuningResult));
        }

        private void tuneChannel(boolean requestTune, boolean up) {
            if (Log.INCLUDE) {
                Log.d(TAG, "tuneChannel() requestTune:" + requestTune);
            }
            if (mTvOverlayManager.isProgramGuideVisible()) {
                mTvOverlayManager.hideProgramGuide();
            }

            TimingLogger timings = new TimingLogger(TAG, "tuneChannel()");
            timings.addSplit("start");
            Channel currentChannel = getCurrentChannel();
            Channel nextChannel = mChannelDataManager.getNextEpgChannel(currentChannel, up);
            tuneChannel(requestTune, nextChannel);
            timings.addSplit("done");
            timings.dumpToLog();
        }

        private void tuneChannel(boolean requestTune, Channel channel) {
            TimingLogger timings = new TimingLogger(TAG, "tuneChannel(requestTune, channel)");
            timings.addSplit("start");
            if (requestTune) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "tuneChannel() tune actually !!!!!!!!!!!!!");
                }
                stateTuneChannel = false;
                LiveTvActivity.this.tuneChannel(channel);
//                updateEpgViewModel(channel, null, null, getBlockType(channel));
            } else {
                stateTuneChannel = true;
                updateOSDViewModel(channel);
                updateEpgViewModel(channel);
            }
            timings.addSplit("done");
            timings.dumpToLog();
        }

        private void finishTuneChannel() {
            if (Log.INCLUDE) {
                Log.d(TAG, "finishTuneChannel() stateTuneChannel:" + stateTuneChannel);
            }
            if (stateTuneChannel) {
                Channel currentChannel = getCurrentChannel();
                tuneChannel(true, currentChannel);
            }
        }

        private void showChannel(boolean requestUpdate, boolean up) {
            if (Log.INCLUDE) {
                Log.d(TAG, "showChannelUp() requestUpdate:" + requestUpdate);
            }
            Channel currentChannel = mEpgViewModel.currentChannel.getCurrentChannel();
            Channel nextChannel = mChannelDataManager.getNextEpgChannel(currentChannel, up);
            showChannel(requestUpdate, nextChannel);
        }

        private void showChannel(boolean requestUpdate, Channel channel) {
            TimingLogger timings = new TimingLogger(TAG, "showChannel(requestUpdate, channel)");
            timings.addSplit("start");

            if (requestUpdate) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "showChannel() requestUpdate !!!!!!!!!!!!!");
                }
                stateShowChannel = false;
                retrieveProgram(channel);
            } else {
                stateShowChannel = true;
            }

            updateEpgViewModel(channel);
            timings.addSplit("done");
            timings.dumpToLog();
        }

        private void finishShowChannel() {
            if (Log.INCLUDE) {
                Log.d(TAG, "finishShowChannel() stateShowChannel:" + stateShowChannel);
            }
            if (stateShowChannel) {
                Channel currentChannel = mEpgViewModel.currentChannel.getCurrentChannel();
                showChannel(true, currentChannel);
            }
        }


        private void startTimeShift(long startTimeMs) {
            if (Log.INCLUDE) {
                Log.d(TAG, "startTimeShift()");
            }
            Channel currentChannel = mEpgViewModel.currentChannel.getCurrentChannel();

            if (currentChannel != getCurrentChannel()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "startTimeShift() - do not start timeshift - timeshift can start on the currently being watched channel");
                }
                return;
            } else if (mainTvView.isBlocked() || mainTvView.isPreview()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "startTimeShift() - do not start timeshift - current state is locked");
                }
                return;
            }

            Program currentProgram = null;
            MiniEpgDataManager.TableEntry entry = getCurrentEntry();
            if (entry != null && !entry.isFutureProgram()) {
                currentProgram = getCurrentProgram();
            }
            if (currentProgram != null && isTimeShiftEnabled(currentChannel, currentProgram)) {
                tuneTimeShift(currentChannel, currentProgram, startTimeMs);
            } else {
                if (Log.INCLUDE) {
                    Log.d(TAG, "startTimeShift() - there is no program.");
                }
            }
        }

        private boolean isTimeShiftEnabled(Channel channel, Program program) {
            if (program == null) {
                return false;
            }
            long timeShiftTimeMillis = channel != null ? channel.getTimeshiftService() : 0;

            if (timeShiftTimeMillis == 0) {
                return false;
            }
            if (Log.INCLUDE) {
                Log.d(TAG, "isTimeShiftEnabled | timeShiftTimeMillis : " + timeShiftTimeMillis);
            }
            long currentTime = JasmineEpgApplication.SystemClock().currentTimeMillis();
            long availableTime = currentTime - timeShiftTimeMillis;
            return program.getEndTimeUtcMillis() > availableTime;
        }
    }

    private class SidePanelManager implements SidePanelLayout.SidePanelActionListener {
        //options
        private ViewGroup mContainerView = null;
        private EdgeDrawerLayout mDrawer = null;
        private SidePanelLayout mSidePanel = null;
        private boolean isOpend;

        private SidePanelManager() {
            mContainerView = findViewById(R.id.controllerFrame);
            mDrawer = findViewById(R.id.drawer_layout);
            mSidePanel = findViewById(R.id.sidePanel);
            mSidePanel.setSidePanelActionListener(this);
            mDrawer.addDrawerListener(mSimpleDrawerListener);
            mDrawer.setClosedPaddingRate(0f);
            isOpend = false;
        }

        public boolean isOpenMenu() {
            if (Log.INCLUDE) {
                Log.d(TAG, "EdgeDrawerLayout isOpenMenu:" + isOpend);
            }
            return isOpend;
        }

        //implements SidePanelActionListener.openMenu
        public void openMenu() {
            if (Log.INCLUDE) {
                Log.d(TAG, "EdgeDrawerLayout openMenu");
            }
            if (isOpenMenu()) {
                return;
            }
            if (mainTvView.getCurrentChannel().isHiddenChannel()) {
                return;
            }
            boolean isTimeShiftEnable = getTvOverlayManager().isTimeShiftActive();
            boolean isUhdChannel = mainTvView.getCurrentChannel().isUHDChannel();
            LiveSidePanelOptionProvider sidePanelOptionProvider = new LiveSidePanelOptionProvider(getApplicationContext(), mainTvView);
            mSidePanel.setSidePanelType(getApplicationContext(), (isTimeShiftEnable || isUhdChannel) ? SidePanelType.TIME_SHIFT : SidePanelType.LIVE, sidePanelOptionProvider);
            mSidePanel.renewalPanelData(getApplicationContext());
            mDrawer.openDrawer(mSidePanel);
            isOpend = true;

            mTvOverlayManager.hideMiniGuideFragment();
        }

        //implements SidePanelActionListener.closeMenu
        public void closeMenu() {
            if (Log.INCLUDE) {
                Log.d(TAG, "EdgeDrawerLayout closeMenu");
            }
            if (!isOpenMenu()) {
                return;
            }
            mSidePanel.clearPanelData();
            mDrawer.closeDrawer(mSidePanel);
            isOpend = false;
            //show mini epg when current channel is blocked.
            checkAndShowMiniGuide();
        }

        private boolean isEnabled() {
            return mSidePanel.isEnabled();
        }

        private final EdgeDrawerLayout.SimpleDrawerListener mSimpleDrawerListener = new EdgeDrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setFocusAllow(mContainerView, false);
                setFocusAllow(((ViewGroup) drawerView), true);
                drawerView.requestFocus();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                setFocusAllow(mContainerView, true);
                setFocusAllow(((ViewGroup) drawerView), false);
                mContainerView.requestFocus();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };
    }

    @Override
    public boolean swapChannel(JasTvView pipTvView) {
        Channel pipChannel = pipTvView.getCurrentChannel();
        boolean resetPip = JasTvView.swapChannel(mainTvView, pipTvView);

        Channel channel = mainTvView.getCurrentChannel();
        Program program = mainTvView.getCurrentProgram();
        JasTvView.TuningResult tuningResult = mainTvView.getTuningResult();
        mViewModelManager.updateOSDViewModel(pipChannel);
        mViewModelManager.updateTVGuideViewModel(channel, program, tuningResult);

        if (resetPip) {
            showMiniGuide();
        }
        return resetPip;
    }

    @Override
    public boolean onPipKeyEvent(int keyCode, KeyEvent keyEvent) {
        if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
            return onKeyDown(keyCode, keyEvent);
        } else {
            return onKeyDown(keyCode, keyEvent);
        }
    }

    @Override
    public boolean needPassKeyEvent(int keyCode, KeyEvent keyEvent) {
        return mTvOverlayManager.isMiniGuideVisible();
    }

    @Override
    public boolean isLockedChannel(Channel channel, Program program) {
        if (channel == null) {
            return false;
        }
        return channel.isLocked() && !mainTvView.isUnblockedChannel(channel);
    }

    @Override
    public boolean isLockedAdultChannel(Channel channel) {
        if (channel == null) {
            return false;
        }
        boolean isAdultChannel = channel.isAdultChannel();
        return isAdultChannel && !mainTvView.isUnblockedChannel(channel);
    }

    @Override
    public boolean isUnLocked(Channel channel) {
        return mainTvView.isUnblockedChannel(channel);
    }
}
