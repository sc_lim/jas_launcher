/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.widget;

import android.graphics.drawable.Drawable;

import java.util.List;

import kr.altimedia.launcher.jasmin.dm.contents.obj.StreamInfo;

/**
 * Created by mc.kim on 06,05,2020
 */
public class TrailerDataProvider {
    private final int mVisibleSize;

    public TrailerDataProvider(int visibleSize) {
        this.mVisibleSize = visibleSize;
    }

    public int getVisibleSize() {
        return mVisibleSize;
    }

    public static class ResultCallback {
        public void onThumbnailLoaded(Drawable drawable) {
        }
    }

    public List<StreamInfo> getTrailerDataList() {
        return null;
    }

    public void getThumbnail(StreamInfo info, ResultCallback callback) {
    }

    public void onSelectedThumbnail(StreamInfo info) {

    }

    public boolean isCurrentPlayingVideo(StreamInfo info) {
        return false;
    }

    public String getThumbnailTitle(StreamInfo info) {
        return "";
    }

    public void reset() {
    }

}
