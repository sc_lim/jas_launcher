/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.BonusType;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.PointCoupon;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class CouponCashBonusDetailFragment extends Fragment {
    public static final String CLASS_NAME = CouponCashBonusDetailFragment.class.getName();
    private final String TAG = CouponCashBonusDetailFragment.class.getSimpleName();

    private static final String KEY_ITEM = "ITEM";
    private final String UNLIMITED = "31.12.9999";

    private CouponCashBonusDetailFragment() {
    }

    public static CouponCashBonusDetailFragment newInstance(Coupon item) {

        Bundle args = new Bundle();
        args.putParcelable(KEY_ITEM, item);

        CouponCashBonusDetailFragment fragment = new CouponCashBonusDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_coupon_history_detail_cash_bonus, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Coupon item = getArguments().getParcelable(KEY_ITEM);

        initView(view, item);
    }

    private void initView(View view, Coupon item) {
        TextView couponPrice = view.findViewById(R.id.couponPrice);
//        LinearLayout bonusValueLayer = view.findViewById(R.id.bonusValueLayer);
//        TextView bonusValue = view.findViewById(R.id.bonusValue);
//        TextView bonusUnit = view.findViewById(R.id.bonusUnit);
        TextView couponBalance = view.findViewById(R.id.couponBalance);
        TextView chargedCouponPrice = view.findViewById(R.id.chargedCouponPrice);
        LinearLayout chargedBonusValueLayer = view.findViewById(R.id.chargedBonusValueLayer);
        TextView chargedBonusValue = view.findViewById(R.id.chargedBonusValue);
        TextView chargedBonusUnit = view.findViewById(R.id.chargedBonusUnit);
        TextView expirationDate = view.findViewById(R.id.expirationDate);
        TextView paymentMethod = view.findViewById(R.id.paymentMethod);
        RelativeLayout couponBgImage = view.findViewById(R.id.couponBgImage);

        PointCoupon pc = (PointCoupon)item;
        couponBgImage.setBackground(view.getResources().getDrawable(pc.getBgImage()));
        couponPrice.setText(StringUtil.getFormattedNumber(pc.getChargedAmount()));
        couponBalance.setText(StringUtil.getFormattedNumber(pc.getBalance()));
        int chargedAmt = pc.getChargedAmount(); // + pc.getBonusAmount();
        chargedCouponPrice.setText(StringUtil.getFormattedNumber(chargedAmt));
        if(pc.getBonusValue() > 0 && (pc.getBonusType() == BonusType.DC_RATE || pc.getBonusType() == BonusType.DC_AMOUNT)) {
//            bonusValue.setText(StringUtil.getFormattedNumber(pc.getBonusValue()));
            chargedBonusValue.setText(StringUtil.getFormattedNumber(pc.getBonusValue()));
            if(pc.getBonusType() != null) {
                if (pc.getBonusType() == BonusType.DC_RATE) {
//                    bonusUnit.setText("%");
                    chargedBonusUnit.setText("%");
                } else if (pc.getBonusType() == BonusType.DC_AMOUNT) {
//                    bonusUnit.setText(R.string.thb);
                    chargedBonusUnit.setText(R.string.thb);
                }
//                bonusValueLayer.setVisibility(View.VISIBLE);
                chargedBonusValueLayer.setVisibility(View.VISIBLE);
            }
        }else{
//            bonusValueLayer.setVisibility(View.GONE);
            chargedBonusValueLayer.setVisibility(View.GONE);
        }
        if(pc.getExpireDate() != null) {
            String dateStr = TimeUtil.getModifiedDate(pc.getExpireDate().getTime());
            if(UNLIMITED.equals(dateStr)){
                expirationDate.setText(R.string.unlimited);
            }else {
                expirationDate.setText(dateStr);
            }
        }
        if(pc.getPaymentType() != null) {
            try {
                paymentMethod.setText(view.getResources().getString(pc.getPaymentType().getName()));
            }catch (Exception e){
            }
        }

        TextView close = view.findViewById(R.id.btnClose);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CouponDetailDialog) getParentFragment()).dismiss();
            }
        });
    }
}
