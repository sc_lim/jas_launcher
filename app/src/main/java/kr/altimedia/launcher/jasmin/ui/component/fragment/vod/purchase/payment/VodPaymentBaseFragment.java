/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.payment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.payment.VodPaymentDataManager;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Payment;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PaymentWrapper;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseDiscountType;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.purchase.obj.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PURCHASE_INFORM;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.KEY_OFFER_ID;

public abstract class VodPaymentBaseFragment extends Fragment {
    private final String TAG = VodPaymentBaseFragment.class.getSimpleName();

    protected final long TOTAL_COUNT_DOWN = 5 * TimeUtil.MIN;
    protected final long INTERVAL = TimeUtil.SEC;

    protected final VodPaymentDataManager vodPaymentDataManager = new VodPaymentDataManager();
    protected MbsDataTask paymentRequestTask;
    protected MbsDataTask paymentResultTask;

    protected String purchaseId = null;

    protected int layoutResourceId = -1;

    private OnPaymentListener onPaymentListener;

    private LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver mPushMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive, Action : " + intent.getAction());
            }

            Bundle mBundle = intent.getExtras();
            if (mBundle != null) {
                String offerId = intent.getExtras().getString(KEY_OFFER_ID);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReceive, offerId : " + offerId);
                }

                getPaymentResult(offerId);
            }
        }
    };

    public void setOnPaymentListener(OnPaymentListener onPaymentListener) {
        this.onPaymentListener = onPaymentListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layoutResourceId = initLayoutResourceId();
        return inflater.inflate(layoutResourceId, container, false);
    }

    protected abstract int initLayoutResourceId();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerPurchaseIdReceiver();
        initView(view);
    }

    protected void initView(View view) {
    }

    private void registerPurchaseIdReceiver() {
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        mLocalBroadcastManager.registerReceiver(mPushMessageReceiver, new IntentFilter(ACTION_UPDATED_PURCHASE_INFORM));
    }

    private void unRegisterPurchaseIdReceiver() {
        mLocalBroadcastManager.unregisterReceiver(mPushMessageReceiver);
    }

    protected void getPaymentResult(String resultId) {
    }

    protected Payment getBankPayment(Payment payment) {
        return payment;
    }

    protected Payment getCreditCardPayment(Payment payment) {
        return payment;
    }

    public OnPaymentListener getOnPaymentListener() {
        return onPaymentListener;
    }

    protected ArrayList<Payment> getPaymentList(PurchaseInfo purchaseInfo) {
        PaymentWrapper paymentWrapper = purchaseInfo.getPaymentWrapper();
        PaymentType paymentType = purchaseInfo.getPaymentType();

        int accountPayment = paymentWrapper.getAccountPayment();
        DiscountCoupon discountCoupon = paymentWrapper.getDiscountCoupon();
        int couponDiscount = paymentWrapper.getCouponDiscountValue();
        int couponPoint = (int) paymentWrapper.getCouponPoint();
        int membershipPointTHB = (int) paymentWrapper.getMembershipPoint();

        if (Log.INCLUDE) {
            Log.d(TAG, "getPaymentList, paymentType : " + paymentType
                    + ", totalPayment : " + accountPayment
                    + ", discountCoupon : " + discountCoupon
                    + ", couponDiscount : " + couponDiscount
                    + ", couponPoint : " + couponPoint
                    + ", membershipPointTHB : " + membershipPointTHB);
        }

        ArrayList<Payment> payments = new ArrayList<>();

        if (paymentType != null) {
            Payment payment = new Payment(null, accountPayment, paymentType.getCode(), null);

            if (paymentType == PaymentType.CREDIT_CARD) {
                payment = getCreditCardPayment(payment);
            } else if (paymentType == PaymentType.INTERNET_MOBILE_BANK) {
                payment = getBankPayment(payment);
            }

            payments.add(payment);
        }

        if (discountCoupon != null || couponDiscount > 0) {
            payments.add(new Payment(discountCoupon.getId(), couponDiscount, PaymentType.DISCOUNT_COUPON.getCode(), String.valueOf(discountCoupon.getRate())));
        }

        if (couponPoint > 0) {
            payments.add(new Payment(null, couponPoint, PaymentType.CASH_COUPON.getCode(), null));
        }

        if (membershipPointTHB > 0) {
            int membershipPoint = membershipPointTHB * PurchaseDiscountType.MEMBERSHIP.getRatio();
            if (Log.INCLUDE) {
                Log.d(TAG, "getPaymentList, membershipPointTHB : " + membershipPointTHB + "THB (" + membershipPoint + "P)");
            }

            payments.add(new Payment("", membershipPointTHB, PaymentType.MEMBERSHIP.getCode(), String.valueOf(membershipPoint)));
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "getPaymentList, payments : " + payments);
        }

        return payments;
    }

    protected void showPaymentCancelDialog(PurchaseInfo purchaseInfo, String purchaseId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "showPaymentCancelDialog, purchaseId : " + purchaseId);
        }

        SafeDismissDialogFragment dialogFragment = (SafeDismissDialogFragment) getParentFragment();
        TvOverlayManager tvOverlayManager = dialogFragment.getTvOverlayManager();

        VodPaymentCancelDialogFragment vodPaymentCancelDialogFragment = VodPaymentCancelDialogFragment.newInstance(purchaseInfo);
        vodPaymentCancelDialogFragment.setOnClickButtonListener(new VodPaymentCancelDialogFragment.OnClickButtonListener() {
            @Override
            public void onClickCancelPayment() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onClickCancelPayment");
                }

                cancelPayment(purchaseId);
                vodPaymentCancelDialogFragment.dismiss();
            }

            @Override
            public void onclickDoNotCancelPayment() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onclickDoNotCancelPayment");
                }

                vodPaymentCancelDialogFragment.dismiss();
            }
        });

        tvOverlayManager.showDialogFragment(vodPaymentCancelDialogFragment);
    }

    private void cancelPayment(String cancelPurchaseId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "cancelPayment");
        }

        if (onPaymentListener != null) {
            onPaymentListener.onCancelPayment(cancelPurchaseId);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (paymentRequestTask != null) {
            paymentRequestTask.cancel(true);
        }

        if (paymentResultTask != null) {
            paymentResultTask.cancel(true);
        }

        unRegisterPurchaseIdReceiver();
        onPaymentListener = null;
    }

    public interface OnPaymentListener {
        void onPaymentComplete(boolean forcePush, boolean isSuccess);

        void onPaymentError(String paymentErrorCode);

        void onRequestWatch(boolean isRequest);

        void onDismissFragment();

        void onCancelPayment(String cancelPurchaseId);
    }
}
