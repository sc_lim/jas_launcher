/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.leanback.app.ProgressBarManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.cwmp.CWMPServiceProvider;
import kr.altimedia.launcher.jasmin.cwmp.service.def.CWMPEtcCode;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.payment.VodPaymentDataManager;
import kr.altimedia.launcher.jasmin.dm.payment.obj.Payment;
import kr.altimedia.launcher.jasmin.dm.payment.obj.RequestVodPaymentResultInfo;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionPriceInfo;
import kr.altimedia.launcher.jasmin.ui.view.common.PasswordView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionPurchaseDialogFragment.KEY_SUBSCRIPTION_INFO;

public class SubscriptionPaymentFragment extends SubscriptionPaymentBaseFragment {
    public static final String CLASS_NAME = SubscriptionPaymentFragment.class.getName();
    private final String TAG = SubscriptionPaymentFragment.class.getSimpleName();

    private static final String KEY_PRICE = "PRICE";

    private PasswordView passwordView;
    private TextView pinTextView;
    private TextView cancel;

    private final ProgressBarManager mProgressBarManager = new ProgressBarManager();
    private final UserDataManager mUserDataManager = new UserDataManager();
    private MbsDataTask checkPinTask;
    private MbsDataTask paymentRequestTask;

    private SubscriptionInfo subscriptionInfo;
    private SubscriptionPriceInfo priceInfo;

    private String purchaseId = null;

    public SubscriptionPaymentFragment() {
    }

    public static SubscriptionPaymentFragment newInstance(SubscriptionInfo subscriptionInfo, SubscriptionPriceInfo price) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_SUBSCRIPTION_INFO, subscriptionInfo);
        args.putParcelable(KEY_PRICE, price);

        SubscriptionPaymentFragment fragment = new SubscriptionPaymentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_subscription_payment;
    }

    @Override
    protected void initView(View view) {
        initText(view);
        initPasswordView(view);
        initButton(view);

        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        mProgressBarManager.setProgressBarView(loadingView);
    }

    private TvOverlayManager mTvOverlayManager = null;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttached : context");
        }
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
    }

    private void initText(View view) {
        Bundle bundle = getArguments();
        subscriptionInfo = bundle.getParcelable(KEY_SUBSCRIPTION_INFO);
        priceInfo = bundle.getParcelable(KEY_PRICE);
        pinTextView = view.findViewById(R.id.pin_text);
        TextView duration = view.findViewById(R.id.duration);

        int dur = priceInfo.getDuration();
        if (dur > 1) {
            duration.setText(String.valueOf(priceInfo.getDuration()));
        } else {
            duration.setText("");
            ((TextView) view.findViewById(R.id.duration_unit)).setText(getString(R.string.monthly));
        }

        TextView title = view.findViewById(R.id.title);
        TextView payment = view.findViewById(R.id.payment);

        title.setText(subscriptionInfo.getTitle());
        payment.setText(priceInfo.getPrice());
    }

    private void initPasswordView(View view) {
        passwordView = view.findViewById(R.id.password);
        passwordView.setOnPasswordComplete(new PasswordView.OnPasswordComplete() {
            @Override
            public void notifyInputChange(boolean isFull, String input) {
                if (isFull) {
                    checkPinCode(input);
                }
            }
        });
    }

    private void initButton(View view) {
        cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getOnPaymentListener() != null) {
                    getOnPaymentListener().onDismissPaymentDialog();
                }
            }
        });
    }

    private void checkPinCode(String password) {
        checkPinTask = mUserDataManager.checkAccountPinCode(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), password,
                new MbsDataProvider<String, Boolean>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        } else {
                            mProgressBarManager.hide();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, Boolean result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onSuccess, result : " + result);
                        }

                        if (result) {
                            requestPayment();
                        } else {
                            setPinError();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "checkPinCode, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        ErrorDialogFragment dialogFragment =
                                ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return SubscriptionPaymentFragment.this.getContext();
                    }
                });
    }

    private void setPinError() {
        String text = getResources().getString(R.string.incorrect_pin_desc);
        pinTextView.setText(text);
        passwordView.clear();

        CWMPServiceProvider.getInstance().notifyEtcError(CWMPEtcCode.ERROR_CODE_207);
    }

    private void requestPayment() {
        int price = Integer.parseInt(priceInfo.getPrice());
        ArrayList<Payment> payments = new ArrayList<>();
        payments.add(new Payment(null, price, PaymentType.BILLING.getCode(), null));

        VodPaymentDataManager vodPaymentDataManager = new VodPaymentDataManager();
        paymentRequestTask = vodPaymentDataManager.requestVodPayment(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(), subscriptionInfo.getContentType(),
                priceInfo.getOfferId(), payments, price,
                new MbsDataProvider<String, RequestVodPaymentResultInfo>() {
                    @Override
                    public void needLoading(boolean loading) {
                        if (loading) {
                            mProgressBarManager.show();
                        }
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onSuccess, onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, RequestVodPaymentResultInfo result) {
                        purchaseId = result.getPurchaseId();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onSuccess, purchasedId : " + purchaseId);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentSubscription(true);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPayment, onError, errorCode : " + errorCode + ", message : " + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return SubscriptionPaymentFragment.this.getContext();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (checkPinTask != null) {
            checkPinTask.cancel(true);
        }

        if (paymentRequestTask != null) {
            paymentRequestTask.cancel(true);
        }

        passwordView.setOnPasswordComplete(null);
        cancel.setOnClickListener(null);
        mProgressBarManager.hide();
    }
}
