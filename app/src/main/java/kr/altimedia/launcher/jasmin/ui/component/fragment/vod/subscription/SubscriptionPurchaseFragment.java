/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.util.Log;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.obj.SubscriptionPriceInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.payment.SubscriptionPaymentDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.presenter.SubscriptionDurationPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.presenter.SubscriptionPaymentPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.presenter.SubscriptionTermsAgreePresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.row.DurationRow;
import kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.row.TermsRow;
import kr.altimedia.launcher.jasmin.ui.view.browse.BrowseFrameLayout;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.vod.subscription.SubscriptionPurchaseDialogFragment.KEY_SUBSCRIPTION_INFO;

public class SubscriptionPurchaseFragment extends Fragment
        implements SubscriptionDurationPresenter.OnSelectSubscriptionDurationListener, SubscriptionTermsAgreePresenter.OnClickTermsButtonListener,
        SubscriptionPaymentPresenter.OnClickPaymentButtonListener, View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = SubscriptionPurchaseFragment.class.getName();
    private static final String TAG = SubscriptionPurchaseFragment.class.getSimpleName();

    private final int DURATION_ROW = 0;
    private final int TERMS_ROW = 1;
    private final int PAYMENT_ROW = 2;

    private SubscriptionInfo subscriptionInfo;
    private SubscriptionPriceInfo selectedPrice;

    private VerticalGridView gridView;
    private ClassPresenterSelector mPresenterSelector;
    private ArrayObjectAdapter mAdapter;

    private SubscriptionPaymentDialogFragment.OnCompleteSubscribePaymentListener onCompleteSubscribePaymentListener;

    public SubscriptionPurchaseFragment() {

    }

    public static SubscriptionPurchaseFragment newInstance(int mSenderId, SubscriptionInfo subscriptionInfo) {

        Bundle args = new Bundle();
        args.putInt(SubscriptionPurchaseDialogFragment.KEY_SENDER_ID, mSenderId);
        args.putParcelable(SubscriptionPurchaseDialogFragment.KEY_SUBSCRIPTION_INFO, subscriptionInfo);

        SubscriptionPurchaseFragment fragment = new SubscriptionPurchaseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setOnCompleteSubscribePaymentListener(SubscriptionPaymentDialogFragment.OnCompleteSubscribePaymentListener onCompleteSubscribePaymentListener) {
        this.onCompleteSubscribePaymentListener = onCompleteSubscribePaymentListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_subscription_purchase, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        Bundle args = getArguments();
        subscriptionInfo = args.getParcelable(KEY_SUBSCRIPTION_INFO);

        initPurchaseInfo(view);
        initPoster(view);
        initRow(view);
        initBrowseLayout(view);
    }

    private void initPurchaseInfo(View view) {
        initPoster(view);
        ((TextView) view.findViewById(R.id.title)).setText(subscriptionInfo.getTitle());
    }

    private void initPoster(View view) {
        ImageView poster = view.findViewById(R.id.poster);
        Glide.with(poster.getContext())
                .load(subscriptionInfo.getPosterSourceUrl()).diskCacheStrategy(DiskCacheStrategy.DATA)
                .centerCrop()
                .placeholder(R.drawable.sample_banner)
                .error(R.drawable.sample_banner)
                .into(poster);
    }

    private void initRow(View view) {
        mPresenterSelector = new ClassPresenterSelector();
        mAdapter = new ArrayObjectAdapter(mPresenterSelector);
        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(mAdapter);

        gridView = view.findViewById(R.id.grid_view);
        gridView.setAdapter(itemBridgeAdapter);

        initDurationRow();
        initTermsRow();
        initPaymentRow();
    }

    private void initDurationRow() {
        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter();
        arrayObjectAdapter.add(subscriptionInfo);

        DurationRow durationRow = new DurationRow(arrayObjectAdapter);
        mAdapter.add(durationRow);

        SubscriptionDurationPresenter subscriptionDurationPresenter = new SubscriptionDurationPresenter();
        subscriptionDurationPresenter.setOnSelectSubscriptionDurationListener(this);
        mPresenterSelector.addClassPresenter(DurationRow.class, subscriptionDurationPresenter);
    }

    private void initTermsRow() {
        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter();
        arrayObjectAdapter.add(subscriptionInfo.getTermsAndConditions());

        TermsRow termsRow = new TermsRow(arrayObjectAdapter);
        mAdapter.add(termsRow);

        SubscriptionTermsAgreePresenter subscriptionTermsAgreePresenter = new SubscriptionTermsAgreePresenter();
        subscriptionTermsAgreePresenter.setOnClickTermsButtonListener(this);
        mPresenterSelector.addClassPresenter(TermsRow.class, subscriptionTermsAgreePresenter);
    }

    private void initPaymentRow() {
        mAdapter.add(subscriptionInfo.getSubscriptionPriceInfoList().get(0));
        SubscriptionPaymentPresenter subscriptionPaymentPresenter = new SubscriptionPaymentPresenter();
        subscriptionPaymentPresenter.setOnClickPaymentButtonListener(this);
        mPresenterSelector.addClassPresenter(SubscriptionPriceInfo.class, subscriptionPaymentPresenter);
    }

    private void initBrowseLayout(View view) {
        BrowseFrameLayout browseFrameLayout = view.findViewById(R.id.browse_layout);
        browseFrameLayout.setOnChildFocusListener(new BrowseFrameLayout.OnChildFocusListener() {
            @Override
            public boolean onRequestFocusInDescendants(int var1, Rect var2) {
                return false;
            }

            @Override
            public void onRequestChildFocus(View child, View focused) {
                if (gridView.getChildCount() < 3) {
                    return;
                }

                //for BG Focus GUI
                int selectedIndex = gridView.getSelectedPosition();

                View priceRowView = getViewHolder(DURATION_ROW).view;
                View termsRowView = getViewHolder(TERMS_ROW).view;

                TextView durationTitle = priceRowView.findViewById(R.id.title);
                TextView termsTitle = termsRowView.findViewById(R.id.title);
                TextView termsDescTitle = termsRowView.findViewById(R.id.terms_description);

                int focus = ContextCompat.getColor(getContext(), R.color.color_FF131313);
                int unFocus = ContextCompat.getColor(getContext(), R.color.transparent);
                int focusTitle = ContextCompat.getColor(getContext(), R.color.color_FFFFFFFF);
                int unFocusTitle = ContextCompat.getColor(getContext(), R.color.color_66E6E6E6);

                if (selectedIndex == DURATION_ROW) {
                    priceRowView.setBackgroundColor(focus);
                    termsRowView.setBackgroundColor(unFocus);
                    durationTitle.setTextColor(focusTitle);
                    termsTitle.setTextColor(unFocusTitle);
                    termsDescTitle.setTextColor(unFocusTitle);
                } else if (selectedIndex == TERMS_ROW) {
                    priceRowView.setBackgroundColor(unFocus);
                    termsRowView.setBackgroundColor(focus);
                    durationTitle.setTextColor(unFocusTitle);
                    termsTitle.setTextColor(focusTitle);
                    termsDescTitle.setTextColor(focusTitle);
                } else {
                    priceRowView.setBackgroundColor(unFocus);
                    termsRowView.setBackgroundColor(unFocus);
                    durationTitle.setTextColor(unFocusTitle);
                    termsTitle.setTextColor(unFocusTitle);
                    termsDescTitle.setTextColor(unFocusTitle);
                }
            }
        });

        view.addOnUnhandledKeyEventListener(this);
    }

    private void showSubscriptionPaymentDialogFragment() {
        int mSenderId = getArguments().getInt(SubscriptionPurchaseDialogFragment.KEY_SENDER_ID);
        SubscriptionPaymentDialogFragment subscriptionPaymentDialogFragment = SubscriptionPaymentDialogFragment.newInstance(mSenderId, subscriptionInfo, selectedPrice);
        subscriptionPaymentDialogFragment.setOnCompleteSubscribePaymentListener(onCompleteSubscribePaymentListener);
        ((SubscriptionPurchaseDialogFragment) getParentFragment()).getTvOverlayManager().showDialogFragment(subscriptionPaymentDialogFragment);
    }

    private void updateAgreeByTermsDialog(boolean isAgree) {
        View view = gridView.getChildAt(TERMS_ROW);
        ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) gridView.getChildViewHolder(view);
        SubscriptionTermsAgreePresenter.ViewHolder vh = (SubscriptionTermsAgreePresenter.ViewHolder) viewHolder.getViewHolder();

        vh.setCheckTermsAgree(isAgree);
        updateSubscribeButton(isAgree);
    }

    private void updateAgreeByDuration(boolean isEnable) {
        View view = gridView.getChildAt(TERMS_ROW);
        ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) gridView.getChildViewHolder(view);
        SubscriptionTermsAgreePresenter.ViewHolder vh = (SubscriptionTermsAgreePresenter.ViewHolder) viewHolder.getViewHolder();

        vh.updateTermsButtons(isEnable, false);
        if (!isEnable) {
            updateSubscribeButton(false);
        }
    }

    private void updateSubscribeButton(boolean isAgree) {
        SubscriptionPaymentPresenter.ViewHolder vh = (SubscriptionPaymentPresenter.ViewHolder) getViewHolder(PAYMENT_ROW);
        vh.updateSubscribeButton(isAgree);
    }

    private void updatePaymentRow(SubscriptionPriceInfo subscriptionPriceInfo) {
        SubscriptionPaymentPresenter.ViewHolder vh = (SubscriptionPaymentPresenter.ViewHolder) getViewHolder(PAYMENT_ROW);
        if (vh != null) {
            SubscriptionPriceInfo visiblePrice = selectedPrice == null ? subscriptionPriceInfo : selectedPrice;
            vh.setData(visiblePrice);
        }
    }

    private Presenter.ViewHolder getViewHolder(int index) {
        View view = gridView.getChildAt(index);
        if (view == null) {
            return null;
        }

        ItemBridgeAdapter.ViewHolder viewHolder = (ItemBridgeAdapter.ViewHolder) gridView.getChildViewHolder(view);
        return viewHolder.getViewHolder();
    }

    @Override
    public void onFocusSubscriptionDuration(SubscriptionPriceInfo subscriptionPriceInfo) {
        updatePaymentRow(subscriptionPriceInfo);
    }

    @Override
    public void onSelectSubscriptionDuration(SubscriptionPriceInfo subscriptionPriceInfo) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onSelectSubscriptionDuration, price : " + subscriptionPriceInfo);
        }

        selectedPrice = subscriptionPriceInfo;

        boolean isUpdate = subscriptionPriceInfo != null;
        updateAgreeByDuration(isUpdate);

        if (isUpdate) {
            updatePaymentRow(selectedPrice);
        }
    }

    @Override
    public void onClickAgree(boolean isAgree) {
        updateSubscribeButton(isAgree);
    }

    @Override
    public void onClickDetail() {
        String desc = subscriptionInfo.getTermsAndConditions();
        SubscriptionTermsDialogFragment subscriptionTermsDialogFragment = SubscriptionTermsDialogFragment.newInstance(desc);
        subscriptionTermsDialogFragment.setOnClickListener(new SubscriptionTermsDialogFragment.OnClickListener() {
            @Override
            public void isAgree(boolean isAgree) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onClick, isAgree : " + isAgree);
                }

                subscriptionTermsDialogFragment.dismiss();
                updateAgreeByTermsDialog(isAgree);
            }
        });

        ((SubscriptionPurchaseDialogFragment) getParentFragment()).getTvOverlayManager().showDialogFragment(subscriptionTermsDialogFragment);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getView().removeOnUnhandledKeyEventListener(this);
        onCompleteSubscribePaymentListener = null;
    }

    @Override
    public void onClickSubscribe() {
        if (selectedPrice != null) {
            showSubscriptionPaymentDialogFragment();
        }
    }

    @Override
    public void onClickCancel() {
        if (onCompleteSubscribePaymentListener != null) {
            onCompleteSubscribePaymentListener.onDismissPurchaseDialog();
        }
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return false;
        }

        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_DPAD_UP:

                SubscriptionDurationPresenter.ViewHolder durVh = (SubscriptionDurationPresenter.ViewHolder) getViewHolder(DURATION_ROW);
                SubscriptionTermsAgreePresenter.ViewHolder termsVh = (SubscriptionTermsAgreePresenter.ViewHolder) getViewHolder(TERMS_ROW);
                SubscriptionPaymentPresenter.ViewHolder subsVh = (SubscriptionPaymentPresenter.ViewHolder) getViewHolder(PAYMENT_ROW);

                int selectedIndex = gridView.getSelectedPosition();
                if (selectedIndex == DURATION_ROW && keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                    if (!termsVh.isEnabled()) {
                        subsVh.view.requestFocus();
                        return true;
                    }
                } else if (selectedIndex == PAYMENT_ROW && keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                    if (!termsVh.isEnabled()) {
                        durVh.view.requestFocus();
                        return true;
                    }
                }

                break;
        }


        return false;
    }
}
