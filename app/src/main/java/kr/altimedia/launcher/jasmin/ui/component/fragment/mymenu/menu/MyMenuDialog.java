/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.app.ProgressBarManager;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ClassPresenterSelector;
import androidx.leanback.widget.OnChildLaidOutListener;
import androidx.leanback.widget.Presenter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsTaskCallback;
import kr.altimedia.launcher.jasmin.dm.contents.obj.Content;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.dm.user.object.FavoriteContent;
import kr.altimedia.launcher.jasmin.dm.user.object.Membership;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileInfo;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileList;
import kr.altimedia.launcher.jasmin.dm.user.object.ProfileLoginResult;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.ErrorDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.dialog.profile.ProfileDeleteNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.MyContentListFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.Linker;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.OnDataLoadedListener;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfile;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.UserProfileBuilder;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.presenter.MyContentListPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.history.CouponHistoryDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.adapter.MenuBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.adapter.ProfileBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.data.MenuItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.data.MenuItemBuilder;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.presenter.FavoriteChannelPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.presenter.MenuItemPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.presenter.ProfilePresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.presenter.VodContentPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.ProfileManageDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.profile.ProfilePinCheckDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.collection.CollectionListDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.history.PurchasedListDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.purchase.subscription.SubscriptionListDialog;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.reminder.BookedListDialog;
import kr.altimedia.launcher.jasmin.ui.util.task.UserTaskManager;
import kr.altimedia.launcher.jasmin.ui.view.adapter.RepeatArrayObjectAdapter;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.BaseOnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.browse.listener.OnItemViewClickedListener;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.row.ListRow;
import kr.altimedia.launcher.jasmin.ui.view.row.Row;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.widget.HeaderItem;

import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_FAVORITE_CHANNEL;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATED_PROFILE;
import static kr.altimedia.launcher.jasmin.system.service.PushMessageReceiver.ACTION_UPDATE_VOD_FAVORITE;

public class MyMenuDialog extends SafeDismissDialogFragment implements OnDataLoadedListener, UserTaskManager.OnUpdateUserDiscountListener, View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = MyMenuDialog.class.getName();
    private final String TAG = MyMenuDialog.class.getSimpleName();

    private final Integer TYPE_PROFILE = new Integer(0);
    private final Integer TYPE_MENU = new Integer(1);
    private final Integer TYPE_FAV_CH = new Integer(2);
    private final Integer TYPE_FAV_VOD = new Integer(3);

    private static final String KEY_HOT_KEY = "KEY_HOT_KEY";

    private ArrayList<Integer> taskList = new ArrayList<>();
    private ProgressBarManager progressBarManager;

    private ArrayList<UserProfile> profileList = new ArrayList<>();
    private ArrayList<UserProfile> profileListWithOptions = new ArrayList<>();
    private HorizontalGridView profileGridView;
    private ArrayObjectAdapter profileObjectAdapter = null;
    private String currentProfileID = "";
    private int maxProfileSize = ProfileManageDialog.MAX_PROFILE_SIZE;

    private List<MenuItem> menuList;
    private HorizontalGridView menuGridView = null;
    private ArrayObjectAdapter menuObjectAdapter = null;

    private LinearLayout favContentEmptyLayer;

    private ArrayList<Channel> channelList = new ArrayList<>();
    private LinearLayout favChLayer;
    private RelativeLayout favChListLayer;
    private MyContentListFragment favChView;
    private ClassPresenterSelector favChPresenterSelector;
    private ArrayObjectAdapter favChRowsAdapter;
    private BaseOnItemViewClickedListener favChOnItemViewClickedListener;
    private LinearLayout favChEmptyLayer;

    private List<FavoriteContent> contentList = new ArrayList<>();
    private LinearLayout favVodLayer;
    private RelativeLayout favVodListLayer;
    private MyContentListFragment favVodView;
    private ClassPresenterSelector favVodPresenterSelector;
    private ArrayObjectAdapter favVodRowsAdapter;
    private BaseOnItemViewClickedListener favVodOnItemViewClickedListener;
    private LinearLayout favVodEmptyLayer;

    private LinearLayout menuAreaLayer;
    private LinearLayout favAreaLayer;

    private final int CONTENTS_VISIBLE_SIZE = 6;
    //    private MbsDataTask dataTask;
    private List<WeakReference<AsyncTask>> mbsDataTaskList = new ArrayList<>();

    private TextView membershipPoint = null;
    private TextView cashCoupon = null;
    private TextView discountCoupon = null;

    private boolean profileReloaded;
    private String updatedProfileName;
    private int currentMenuFocusIndex = 0;

    private View rootView;

    private boolean favChReloaded;
    private boolean favVodReloaded;
    private boolean favChLoaded;
    private boolean favVodLoaded;

    private final UserTaskManager userTaskManager = UserTaskManager.getInstance();
    private TvOverlayManager mTvOverlayManager;

    private boolean isHotKey;

//    private LiveTvActivity liveTvActivity;

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) return;

            String action = intent.getAction();
            if (Log.INCLUDE) {
                Log.d(TAG, "onReceive: intent=" + intent.getAction());
            }
            if (action.equals(ACTION_UPDATED_FAVORITE_CHANNEL)) {
                reloadFavCh();

            } else if (action.equals(ACTION_UPDATE_VOD_FAVORITE)) {
                reloadFavVod();

            } else if (action.equals(ACTION_UPDATED_PROFILE)) {
                PushMessageReceiver.ProfileUpdateType typeData =
                        (PushMessageReceiver.ProfileUpdateType) intent.getSerializableExtra(PushMessageReceiver.KEY_TYPE);
                if (typeData == null) {
                    return;
                }
                switch (typeData) {
                    case add:
                    case delete:
                        reloadProfile(ACTION_UPDATED_PROFILE);
                        break;
                }

            } else if (action.equals(ProfileManageDialog.ACTION_MY_PROFILE_ADDED)) {
                final String name = intent.getStringExtra(ProfileManageDialog.KEY_PROFILE_NAME);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReceive: name=" + name);
                }
                reloadProfile(name);

            } else if (action.equals(ProfileManageDialog.ACTION_MY_PROFILE_UPDATED)) {
                final String id = intent.getStringExtra(ProfileManageDialog.KEY_PROFILE_ID);
                final String name = intent.getStringExtra(ProfileManageDialog.KEY_PROFILE_NAME);
                final String iconId = intent.getStringExtra(ProfileManageDialog.KEY_PROFILE_ICON_ID);
                final String iconPath = intent.getStringExtra(ProfileManageDialog.KEY_PROFILE_ICON_PATH);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReceive: id=" + id + ", name=" + name + ", icon=" + iconId + ", icon path=" + iconPath);
                }

                reloadProfile(null);

                new Thread() {
                    public void run() {
                        ArrayList<ProfileInfo> list = (ArrayList<ProfileInfo>) AccountManager.getInstance().getData(AccountManager.KEY_PROFILE_LIST);
                        ProfileInfo changedProfileInfo = null;
                        for (ProfileInfo profileInfo : list) {
                            if (profileInfo.getProfileId().equals(id)) {
                                changedProfileInfo = profileInfo;
                                changedProfileInfo.setProfileName(name);
                                changedProfileInfo.setProfileIcon(iconId, iconPath);
                                break;
                            }
                        }

                        AccountManager.getInstance().updateProfileList(list);

                        if (changedProfileInfo != null && changedProfileInfo.isLastLoginYN()) {
                            notifyProfileUpdate(id);
                        }
                    }
                }.start();

            } else if (action.equals(ProfileManageDialog.ACTION_MY_PROFILE_DELETED)) {
                final String id = intent.getStringExtra(ProfileManageDialog.KEY_PROFILE_ID);
                if (Log.INCLUDE) {
                    Log.d(TAG, "onReceive: id=" + id);
                }

                reloadProfile(null);

                new Thread() {
                    public void run() {
                        ArrayList<ProfileInfo> list = (ArrayList<ProfileInfo>) AccountManager.getInstance().getData(AccountManager.KEY_PROFILE_LIST);
                        for (ProfileInfo profileInfo : list) {
                            if (profileInfo.getProfileId().equals(id)) {
                                list.remove(profileInfo);
                                break;
                            }
                        }
                        AccountManager.getInstance().updateProfileList(list);
                    }
                }.start();

            }
        }
    };

    public static MyMenuDialog newInstance() {
        Bundle args = new Bundle();
        MyMenuDialog fragment = new MyMenuDialog();
        fragment.setArguments(args);
        return fragment;
    }

    public static MyMenuDialog newInstance(boolean isHotKey) {
        Bundle args = new Bundle();
        MyMenuDialog fragment = new MyMenuDialog();
        args.putBoolean(KEY_HOT_KEY, isHotKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    private void showSafeDialog(SafeDismissDialogFragment dialog) {
        if (mTvOverlayManager == null) {
            return;
        }
        mTvOverlayManager.showDialogFragment(dialog);
    }

    private void clearTaskList(){
        if(mbsDataTaskList == null){
            return;
        }

        for (WeakReference<AsyncTask> task : mbsDataTaskList) {
            if (task.get() != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "canceled");
                }
                task.get().cancel(true);
            }
        }
        taskList.clear();
    }

    @Override
    public void onDestroyView() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onDestroyView");
        }
//        if (liveTvActivity != null) {
//            liveTvActivity.setMute(false);
//            liveTvActivity = null;
//        }

//        if (menuList != null && !menuList.isEmpty()) {
//            menuList.clear();
//        }
//        if (contentList != null && !contentList.isEmpty()) {
//            contentList.clear();
//        }

        clearTaskList();

        if (rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }
        if (userTaskManager != null) {
            userTaskManager.removeUserDiscountListener(this);
        }
        if (progressBarManager != null) {
            progressBarManager.hide();
            progressBarManager.setRootView(null);
        }

        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mBroadcastReceiver);

        rootView = null;

        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (keyCode == KeyEvent.KEYCODE_BUTTON_1) {
            return true;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttach: " + mTvOverlayManager);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        favVodView = (MyContentListFragment) getChildFragmentManager().findFragmentById(
                R.id.favVodView);
        if (favVodView == null) {
            Rect padding = new Rect(30, 0, 0, 0);
            favVodView = MyContentListFragment.newInstance(padding);
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.favVodView, favVodView).commit();
        }

        favChView = (MyContentListFragment) getChildFragmentManager().findFragmentById(
                R.id.favChView);
        if (favChView == null) {
            Rect padding = new Rect(30, 0, 0, 0);
            favChView = MyContentListFragment.newInstance(padding);
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.favChView, favChView).commit();
        }

        return inflater.inflate(R.layout.dialog_mymenu_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated");
        }

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        Bundle arguments = getArguments();
        if (arguments != null) {
            isHotKey = arguments.getBoolean(KEY_HOT_KEY);
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "onViewCreated: isHotKey=" + isHotKey);
        }

        currentProfileID = AccountManager.getInstance().getProfileId();
        profileReloaded = false;
        updatedProfileName = null;
        currentMenuFocusIndex = 0;

        favVodReloaded = false;

        menuAreaLayer = view.findViewById(R.id.menuAreaLayer);
        favContentEmptyLayer = view.findViewById(R.id.favContentEmptyLayer);
        favAreaLayer = view.findViewById(R.id.favAreaLayer);

        initLoadingBar(view);
        initCouponInfo(view);
        initProfile(view);
        initMenu(view);
        initFavCh(view);
        initFavVod(view);

        loadCouponSummary();
        loadProfile();
        loadMenu();
        loadFavCh();
        loadFavVod();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_UPDATED_FAVORITE_CHANNEL);
        filter.addAction(ACTION_UPDATE_VOD_FAVORITE);
        filter.addAction(ACTION_UPDATED_PROFILE);
        filter.addAction(ProfileManageDialog.ACTION_MY_PROFILE_ADDED);
        filter.addAction(ProfileManageDialog.ACTION_MY_PROFILE_UPDATED);
        filter.addAction(ProfileManageDialog.ACTION_MY_PROFILE_DELETED);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mBroadcastReceiver, filter);
    }

    @Override
    public void onResume() {
        if (Log.INCLUDE) {
            Log.d(TAG, "onResume: favChLoaded=" + favChLoaded + ", favVodLoaded=" + favVodLoaded);
        }
        super.onResume();
    }

    public boolean isShowing() {
        if (rootView != null) {
            return rootView.getVisibility() == View.VISIBLE;
        }
        return false;
    }

    private void initLoadingBar(View view) {
        progressBarManager = new ProgressBarManager();
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View loadingView = inflater.inflate(R.layout.view_loading, view.findViewById(R.id.progressRoot));
        progressBarManager.setProgressBarView(loadingView);
        showProgress();
    }

    private void initCouponInfo(View view) {
        membershipPoint = view.findViewById(R.id.membershipPoint);
        cashCoupon = view.findViewById(R.id.cashCoupon);
        discountCoupon = view.findViewById(R.id.discountCoupon);
    }

    private void initProfile(View view) {
        profileGridView = view.findViewById(R.id.profileGridView);
        profileGridView.setNumRows(1);
        profileGridView.setClipToPadding(false);

        int horizontalSpacing = (int) view.getResources().getDimension(R.dimen.mymenu_profile_horizontal_space);
        profileGridView.setHorizontalSpacing(horizontalSpacing);
    }

    private void initMenu(View view) {
        menuGridView = view.findViewById(R.id.menuGridView);

        int horizontalSpacing = (int) view.getResources().getDimension(R.dimen.mymenu_menu_horizontal_space);
        menuGridView.setHorizontalSpacing(horizontalSpacing);
    }

    private void initFavCh(View view) {
        favChLayer = view.findViewById(R.id.favChLayer);
        favChListLayer = view.findViewById(R.id.favChListLayer);
        favChEmptyLayer = view.findViewById(R.id.favChEmptyLayer);
    }

    private void initFavVod(View view) {
        favVodLayer = view.findViewById(R.id.favVodLayer);
        favVodListLayer = view.findViewById(R.id.favVodListLayer);
        favVodEmptyLayer = view.findViewById(R.id.favVodEmptyLayer);
    }

    private void checkLoadList(Integer task, boolean added) {
        if (added) {
            if (taskList.contains(task)) {
                return;
            }
            taskList.add(task);
        } else {
            taskList.remove(task);
        }
        if (taskList.isEmpty()) {
            hideProgress();
        }
    }

    private void loadCouponSummary() {
        userTaskManager.addUserDiscountListener(this);
        userTaskManager.loadUserDiscountInfo(getContext());
    }

    public void reloadProfile(String updatedProfileName) {
        if (Log.INCLUDE) {
            Log.d(TAG, "reloadProfile: name=" + updatedProfileName);
        }
        this.profileReloaded = true;
        this.updatedProfileName = updatedProfileName;

        this.loadProfile();
    }

    private void loadProfile() {

        UserDataManager userDataManager = new UserDataManager();
        AsyncTask dataTask = userDataManager.getProfileList(
                AccountManager.getInstance().getSaId(),
                new MbsDataProvider<String, ProfileList>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(TYPE_PROFILE, loading);
                    }

                    @Override
                    public void onFailed(int reason) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfile: onFailed: profileReloaded=" + profileReloaded);
                        }
                        try {
                            addProfileOptions();
                            showView(TYPE_PROFILE);

                            if (reason == MbsTaskCallback.REASON_LOGIN) {
                                showNetworkErrorPopup(reason);
                            }
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onSuccess(String key, ProfileList result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadProfile: onSuccess: result=" + result);
                        }
                        try {
                            if (!profileList.isEmpty()) {
                                profileList.clear();
                            }
                            List<ProfileInfo> tmpList = result.getProfileInfoList();
                            for (ProfileInfo profileInfo : tmpList) {
                                profileList.add(UserProfileBuilder.createUserProfile(profileInfo));
                            }
                            maxProfileSize = result.getMaxProfileCount();

                            buildProfileList();

                            if (profileReloaded && updatedProfileName != null && !updatedProfileName.equals("")) {
                                updateAccountProfileList();
                            }

                            showView(TYPE_PROFILE);
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        addProfileOptions();
                        showView(TYPE_PROFILE);

                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return MyMenuDialog.this.getContext();
                    }
                });

        mbsDataTaskList.add(new WeakReference<>(dataTask));
    }

    private void buildProfileList() {
        if (!profileListWithOptions.isEmpty()) {
            profileListWithOptions.clear();
        }
        profileListWithOptions.addAll(profileList);

//        boolean profileDeleted = true;
        if (currentProfileID != null) {
            int idx = 0;
            for (UserProfile profile : profileListWithOptions) {
                if (profile != null && profile.getId() != null && profile.getId().equals(currentProfileID)) {
                    profile.setSelected(true);
                    profileListWithOptions.set(idx, profile);
//                    profileDeleted = false;
                }
                idx++;
            }
        }

        addProfileOptions();

//        if(profileDeleted){
//            showProfileDeleteNotice();
//        }
    }

    private void addProfileOptions() {
        if (profileListWithOptions.size() < maxProfileSize) {
            if (!UserProfileBuilder.checkProfileOptionIncluded(profileListWithOptions, UserProfileBuilder.TYPE_ADD_OPTION)) {
                profileListWithOptions.add(UserProfileBuilder.createAddOption(getResources()));
            }
        }
        if (!UserProfileBuilder.checkProfileOptionIncluded(profileListWithOptions, UserProfileBuilder.TYPE_MANAGE_OPTION)) {
            profileListWithOptions.add(UserProfileBuilder.createManageOption(getResources()));
        }
    }

    private void loadMenu() {
        if (menuList != null && !menuList.isEmpty()) {
            menuList.clear();
        }

        checkLoadList(TYPE_MENU, true);

        menuList = new MenuItemBuilder().getMenuList();
        checkLoadList(TYPE_MENU, false);

        showView(TYPE_MENU);
    }

    public void reloadFavCh() {
        if (Log.INCLUDE) {
            Log.d(TAG, "reloadFavCh");
        }
        this.favChReloaded = true;
        this.loadFavCh();
    }

    private void loadFavCh() {
        favChLoaded = false;

        UserDataManager userDataManager = new UserDataManager();
        AsyncTask dataTask = userDataManager.getFavoriteChannelList(
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                new MbsDataProvider<String, List<String>>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int reason) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadFavCh: onFailed: reason=" + reason);
                        }
                        favChLoaded = true;
                        try {
                            showView(TYPE_FAV_CH);

                            if (reason == MbsTaskCallback.REASON_NETWORK) {
                                showNetworkErrorPopup(reason);
                            }
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<String> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadFavCh: onSuccess: result=" + result);
                        }
                        favChLoaded = true;
                        try {
                            if (channelList != null && !channelList.isEmpty()) {
                                channelList.clear();
                            }
                            for (String serviceId : result) {
                                TvSingletons mTvSingletons = TvSingletons.getSingletons(getContext());
                                Channel channel = mTvSingletons.getChannelDataManager().getChannelByServiceId(serviceId);
                                if (channel != null && !channel.isHiddenChannel()) {
                                    channelList.add(channel);
                                }
                            }

                            showView(TYPE_FAV_CH);
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        favChLoaded = true;

                        showView(TYPE_FAV_CH);

                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }
                });

        mbsDataTaskList.add(new WeakReference<>(dataTask));
    }

    public void reloadFavVod() {
        if (Log.INCLUDE) {
            Log.d(TAG, "reloadFavVod");
        }
        this.favVodReloaded = true;
        this.loadFavVod();
    }

    private void loadFavVod() {
        favVodLoaded = false;

        UserDataManager userDataManager = new UserDataManager();
        AsyncTask dataTask = userDataManager.getFavoriteVodList(
                AccountManager.getInstance().getLocalLanguage(),
                AccountManager.getInstance().getSaId(),
                AccountManager.getInstance().getProfileId(),
                new MbsDataProvider<String, List<FavoriteContent>>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(TYPE_FAV_VOD, loading);
                    }

                    @Override
                    public void onFailed(int reason) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadFavVod: onFailed: reason=" + reason);
                        }
                        favVodLoaded = true;
                        try {
                            showView(TYPE_FAV_VOD);

                            if (reason == MbsTaskCallback.REASON_NETWORK) {
                                showNetworkErrorPopup(reason);
                            }
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<FavoriteContent> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "loadFavVod: onSuccess: result=" + result);
                        }
                        favVodLoaded = true;
                        try {
                            if (contentList != null && !contentList.isEmpty()) {
                                contentList.clear();
                            }
                            contentList = result;
                            showView(TYPE_FAV_VOD);
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        favVodLoaded = true;

                        showView(TYPE_FAV_VOD);

                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public Context getTaskContext() {
                        return MyMenuDialog.this.getContext();
                    }
                });

        mbsDataTaskList.add(new WeakReference<>(dataTask));
    }

    @Override
    public void hideProgress() {
        if (progressBarManager != null) {
            progressBarManager.hide();
        }
    }

    @Override
    public void showProgress() {
        if (taskList.isEmpty()) {
            progressBarManager.show();
        }
    }

    private void showNetworkErrorPopup(int reason) {
        if (reason == MbsTaskCallback.REASON_NETWORK || reason == MbsTaskCallback.REASON_LOGIN) {
            FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(reason, getContext());
            failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
        }
    }

    @Override
    public void showView(int type) {
        if (type == TYPE_PROFILE) {
            setProfileView();

        } else if (type == TYPE_MENU) {
            currentMenuFocusIndex = 0;
            setMenuView();

        } else if (type == TYPE_FAV_CH) {
            setFavChView();

        } else if (type == TYPE_FAV_VOD) {
            setVodView();
        }


    }

    private void setCouponView() {
        long totalPointCoupon = userTaskManager.getPointCoupon();
        int size = userTaskManager.getDiscountCoupons().size();

        cashCoupon.setText(StringUtil.getFormattedNumber(totalPointCoupon));
        discountCoupon.setText(StringUtil.getFormattedNumber(size));
    }

    private void setProfileView() {
        if (profileListWithOptions != null && profileListWithOptions.size() > 0) {
            ProfilePresenter presenter = new ProfilePresenter(new ProfilePresenter.OnKeyListener() {
                @Override
                public boolean onKey(int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                        case KeyEvent.KEYCODE_DPAD_CENTER: {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "ProfilePresenter: onKey: keyCode=" + keyCode + ", index=" + index);
                            }
                            if (item instanceof UserProfile) {
                                UserProfile user = (UserProfile) item;
                                onProfileSelected(user);
                                return true;
                            }
                            break;
                        }
                        case KeyEvent.KEYCODE_DPAD_DOWN: {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "ProfilePresenter: onKey: keyCode=" + keyCode + ", index=" + index);
                            }
                            setMenuFocus(currentMenuFocusIndex);
                            return true;
                        }
                        case KeyEvent.KEYCODE_DPAD_LEFT: {
                            int size = profileGridView.getAdapter().getItemCount();
                            if (index == 0) {
                                profileGridView.scrollToPosition(size - 1);
                                return true;
                            }
                            break;
                        }
                        case KeyEvent.KEYCODE_DPAD_RIGHT: {
                            int size = profileGridView.getAdapter().getItemCount();
                            if (index == (size - 1)) {
                                profileGridView.scrollToPosition(0);
                                return true;
                            }
                            break;
                        }
                        case KeyEvent.KEYCODE_BACK: {
                            dismiss();
                            return true;
                        }
                    }
                    return false;
                }
            }, new ProfilePresenter.OnFocusListener() {
                @Override
                public void onFocus(boolean hasFocus, View view, int index, Object item) {
                }
            });
            if (profileObjectAdapter != null) {
                profileObjectAdapter.clear();
            }
            profileObjectAdapter = new ArrayObjectAdapter(presenter);
            profileObjectAdapter.addAll(0, profileListWithOptions);
            ProfileBridgeAdapter bridgeAdapter = new ProfileBridgeAdapter();
            bridgeAdapter.setAdapter(profileObjectAdapter);
            profileGridView.setAdapter(bridgeAdapter);

            if (profileReloaded) {
                profileGridView.setOnChildLaidOutListener(new OnChildLaidOutListener() {
                    @Override
                    public void onChildLaidOut(ViewGroup parent, View view, int position, long id) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "ProfilePresenter: onChildLaidOut: position=" + position);
                        }
                        setSmartProfileFocus(position);
                    }
                });
                profileReloaded = false;
            }
        }
    }

    private void setMenuView() {
        MenuItemPresenter presenter = new MenuItemPresenter(
                new MenuItemPresenter.OnKeyListener() {
                    @Override
                    public boolean onKey(int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
                        switch (keyCode) {
                            case KeyEvent.KEYCODE_ENTER:
                            case KeyEvent.KEYCODE_DPAD_CENTER:
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "MenuItemPresenter: onKey: keyCode=" + keyCode + ", index=" + index);
                                }
                                if (item instanceof MenuItem) {
                                    MenuItem menu = (MenuItem) item;
                                    onMenuSelected(menu);
                                    return true;
                                }
                                break;
                            case KeyEvent.KEYCODE_DPAD_UP:
                                if (Log.INCLUDE) {
                                    Log.d(TAG, "MenuItemPresenter: onKey: keyCode=" + keyCode + ", index=" + index);
                                }
                                if (item instanceof MenuItem) {
                                    return setDefaultProfileFocus();
                                }
                                break;
                            case KeyEvent.KEYCODE_DPAD_DOWN: {
                                if (isFavChAvailable() || isFavVodAvailable()) {
                                    menuAreaLayer.setVisibility(View.GONE);
                                    return true;
                                }
                                break;
                            }
                            case KeyEvent.KEYCODE_DPAD_LEFT: {
                                int size = menuGridView.getAdapter().getItemCount();
                                if (index == 0) {
                                    menuGridView.scrollToPosition(size - 1);
                                    return true;
                                }
                                break;
                            }
                            case KeyEvent.KEYCODE_DPAD_RIGHT: {
                                int size = menuGridView.getAdapter().getItemCount();
                                if (index == (size - 1)) {
                                    menuGridView.scrollToPosition(0);
                                    return true;
                                }
                                break;
                            }
                        }
                        return false;
                    }
                },
                new MenuItemPresenter.OnFocusListener() {
                    @Override
                    public void onFocus(int index, Object item) {
                        if (item instanceof MenuItem) {
                            currentMenuFocusIndex = index;
//                            if (Log.INCLUDE) {
//                                Log.d(TAG, "MenuItemPresenter: onFocus: currentMenuFocusIndex=" + currentMenuFocusIndex);
//                            }
                        }
                    }
                });

        menuObjectAdapter = new ArrayObjectAdapter(presenter);
        menuObjectAdapter.addAll(0, menuList);

        MenuBridgeAdapter bridgeAdapter = new MenuBridgeAdapter();
        bridgeAdapter.setAdapter(menuObjectAdapter);

        menuGridView.setAdapter(bridgeAdapter);

        menuGridView.setOnChildLaidOutListener(new OnChildLaidOutListener() {
            @Override
            public void onChildLaidOut(ViewGroup parent, View view, int position, long id) {
                if (currentMenuFocusIndex == position) {
                    setMenuFocus(currentMenuFocusIndex);
                }
            }
        });
    }

    private void setFavChView() {
        if (isFavChAvailable()) {
            RowPresenter contentPresenter = new FavoriteChannelPresenter();
            RepeatArrayObjectAdapter listRowAdapter = new RepeatArrayObjectAdapter(contentPresenter, CONTENTS_VISIBLE_SIZE);
            listRowAdapter.addAll(0, channelList);
            HeaderItem header = new HeaderItem(0, "");
            ListRow listRow = new ListRow(header, listRowAdapter);

            favChPresenterSelector = new ClassPresenterSelector();
            if (favChRowsAdapter == null) {
                favChRowsAdapter = new ArrayObjectAdapter(favChPresenterSelector);
            } else {
                favChRowsAdapter.clear();
            }
            favChRowsAdapter.add(listRow);

            favChOnItemViewClickedListener = new ChannelItemClickedListener();
            if (favChView != null) {
                favChView.setOnItemViewClickedListener(favChOnItemViewClickedListener);
            }
            MyContentListPresenter rowPresenter = new MyContentListPresenter(0);
            rowPresenter.setFixedItemIndex(0);
            rowPresenter.setOnPresenterDispatchKeyListener(new MyContentListPresenter.OnPresenterDispatchKeyListener() {
                @Override
                public boolean notifyKey(KeyEvent event, Presenter.ViewHolder viewHolder, int position) {
                    int keyCode = event.getKeyCode();
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_UP: {
//                            if (Log.INCLUDE) {
//                                Log.d(TAG, "FavoriteChannelListPresenter: notifyKey: keyCode=" + keyCode + ", position=" + position);
//                            }
                            setMenuFocus(currentMenuFocusIndex);
                            return true;
                        }
                        case KeyEvent.KEYCODE_DPAD_LEFT: {
                            HorizontalGridView hGridView = ((MyContentListPresenter.ViewHolder) viewHolder).getGridView();
                            if (hGridView != null) {
                                int size = hGridView.getAdapter().getItemCount();
//                                if (Log.INCLUDE) {
//                                    Log.d(TAG, "FavoriteChannelListPresenter: notifyKey: keyCode=" + keyCode + ", position=" + position + ", size=" + size);
//                                }
                                if (position == 0 && size <= CONTENTS_VISIBLE_SIZE) {
                                    hGridView.scrollToPosition((size - 1));
                                    return true;
                                }
                            }
                            break;
                        }
                        case KeyEvent.KEYCODE_DPAD_RIGHT: {
                            HorizontalGridView hGridView = ((MyContentListPresenter.ViewHolder) viewHolder).getGridView();
                            if (hGridView != null) {
                                int size = hGridView.getAdapter().getItemCount();
//                                if (Log.INCLUDE) {
//                                    Log.d(TAG, "FavoriteChannelListPresenter: notifyKey: keyCode=" + keyCode + ", position=" + position + ", size=" + size);
//                                }
                                if (position == (size - 1) && size <= CONTENTS_VISIBLE_SIZE) {
                                    hGridView.scrollToPosition(0);
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                }
            });
            favChPresenterSelector.addClassPresenter(ListRow.class, rowPresenter);

            if (favChView != null) {
                favChView.setAdapter(favChRowsAdapter);
            }


            favChEmptyLayer.setVisibility(View.GONE);
            favChListLayer.setVisibility(View.VISIBLE);
            favChLayer.setVisibility(View.VISIBLE);

            if (isHotKey) {
                setFavLayerFocus();
            }

        } else {
            favChEmptyLayer.setVisibility(View.VISIBLE);
            favChListLayer.setVisibility(View.GONE);
            favChLayer.setVisibility(View.VISIBLE);

            if (isHotKey) {
                setFavLayerFocus();
            } else {
                setEmptyLayer();
            }
        }
    }

    private void setVodView() {
        if (isFavVodAvailable()) {
            RowPresenter contentPresenter = new VodContentPresenter();
            RepeatArrayObjectAdapter listRowAdapter = new RepeatArrayObjectAdapter(contentPresenter, CONTENTS_VISIBLE_SIZE);
            listRowAdapter.addAll(0, contentList);
            HeaderItem header = new HeaderItem(0, "");
            ListRow listRow = new ListRow(header, listRowAdapter);

            favVodPresenterSelector = new ClassPresenterSelector();
            if (favVodRowsAdapter == null) {
                favVodRowsAdapter = new ArrayObjectAdapter(favVodPresenterSelector);
            } else {
                favVodRowsAdapter.clear();
            }
            favVodRowsAdapter.add(listRow);

            favVodOnItemViewClickedListener = new VodItemClickedListener();
            if (favVodView != null) {
                favVodView.setOnItemViewClickedListener(favVodOnItemViewClickedListener);
            }
            MyContentListPresenter rowPresenter = new MyContentListPresenter();
            rowPresenter.setFixedItemIndex(0);
            rowPresenter.setOnPresenterDispatchKeyListener(new MyContentListPresenter.OnPresenterDispatchKeyListener() {
                @Override
                public boolean notifyKey(KeyEvent event, Presenter.ViewHolder viewHolder, int position) {
                    int keyCode = event.getKeyCode();
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_UP: {
//                            if (Log.INCLUDE) {
//                                Log.d(TAG, "VodContentListPresenter: notifyKey: keyCode=" + keyCode + ", position=" + position);
//                            }
                            if (!isFavChAvailable()) {
                                setMenuFocus(currentMenuFocusIndex);
                                return true;
                            }
                            break;
                        }
                        case KeyEvent.KEYCODE_DPAD_LEFT: {
                            HorizontalGridView hGridView = ((MyContentListPresenter.ViewHolder) viewHolder).getGridView();
                            if (hGridView != null) {
                                int size = hGridView.getAdapter().getItemCount();
//                                if (Log.INCLUDE) {
//                                    Log.d(TAG, "VodContentListPresenter: notifyKey: keyCode=" + keyCode + ", position=" + position + ", size=" + size);
//                                }
                                if (position == 0 && size <= CONTENTS_VISIBLE_SIZE) {
                                    hGridView.scrollToPosition((size - 1));
                                    return true;
                                }
                            }
                            break;
                        }
                        case KeyEvent.KEYCODE_DPAD_RIGHT: {
                            HorizontalGridView hGridView = ((MyContentListPresenter.ViewHolder) viewHolder).getGridView();
                            if (hGridView != null) {
                                int size = hGridView.getAdapter().getItemCount();
//                                if (Log.INCLUDE) {
//                                    Log.d(TAG, "VodContentListPresenter: notifyKey: keyCode=" + keyCode + ", position=" + position + ", size=" + size);
//                                }
                                if (position == (size - 1) && size <= CONTENTS_VISIBLE_SIZE) {
                                    hGridView.scrollToPosition(0);
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                }
            });
            favVodPresenterSelector.addClassPresenter(ListRow.class, rowPresenter);

            if (favVodView != null) {
                favVodView.setAdapter(favVodRowsAdapter);
            }

            favVodEmptyLayer.setVisibility(View.GONE);
            favVodListLayer.setVisibility(View.VISIBLE);
            favVodLayer.setVisibility(View.VISIBLE);

            if (isHotKey) {
                setFavLayerFocus();
            }

        } else {
            favVodEmptyLayer.setVisibility(View.VISIBLE);
            favVodListLayer.setVisibility(View.GONE);
            favVodLayer.setVisibility(View.VISIBLE);

            if (isHotKey) {
                setFavLayerFocus();
            } else {
                setEmptyLayer();
            }
        }
    }

    private void setEmptyLayer() {
        if (favChLoaded && favVodLoaded) {
            if (!isFavChAvailable() && !isFavVodAvailable()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "setEmptyLayer");
                }
                favChLayer.setVisibility(View.GONE);
                favVodLayer.setVisibility(View.GONE);
                favContentEmptyLayer.setVisibility(View.VISIBLE);

                currentMenuFocusIndex = 0;
                setMenuFocus(currentMenuFocusIndex);
            }

            isHotKey = false;
        }
    }

    private void setFavLayerFocus() {
        if (Log.INCLUDE) {
            Log.d(TAG, "setFavLayerFocus: favChLoaded=" + favChLoaded + ", favVodLoaded=" + favVodLoaded);
        }
        if (favChLoaded && favVodLoaded) {
            if (isFavChAvailable()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "setFavLayerFocus: favChView");
                }

                menuAreaLayer.setVisibility(View.GONE);
                favChView.resetFocus();
            } else if (isFavVodAvailable()) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "setFavLayerFocus: favVodView");
                }
                menuAreaLayer.setVisibility(View.GONE);
                favVodView.resetFocus();
            } else {
                setEmptyLayer();
            }
            isHotKey = false;
        }
    }

    private boolean isFavChAvailable() {
        return channelList != null && channelList.size() > 0;
    }

    private boolean isFavVodAvailable() {
        return contentList != null && contentList.size() > 0;
    }

    private void setMenuFocus(int index) {
        setProfileActivated(false);

        menuGridView.requestFocus();
        View child = menuGridView.getChildAt(index);
        if (child != null) {
            menuGridView.setSelectedPosition(index);
            child.requestFocus();
            menuAreaLayer.setVisibility(View.VISIBLE);
            if (Log.INCLUDE) {
                Log.d(TAG, "setMenuFocus: index=" + index);
            }
        }
    }

    private void setProfileActivated(boolean activated) {
        try {
            if (activated) {
                for (int i = 0; i < profileGridView.getChildCount(); i++) {
                    View child = profileGridView.getChildAt(i);
                    if (child != null) {
                        child.setActivated(true);
                    }
                }
            } else {
                for (int i = 0; i < profileGridView.getChildCount(); i++) {
                    View child = profileGridView.getChildAt(i);
                    if (child != null) {
                        child.setActivated(false);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    private boolean setDefaultProfileFocus() {
        setProfileActivated(true);

//        if (Log.INCLUDE) {
//            Log.d(TAG, "setDefaultProfileFocus");
//        }
        if (profileObjectAdapter != null) {
            for (int i = 0; i < profileObjectAdapter.size(); i++) {
                UserProfile profile = (UserProfile) profileObjectAdapter.get(i);
                if (profile.getId().equals(currentProfileID)) {
                    setProfileFocus(i);
                    return true;
                }
            }
        }
        return false;
    }

    private boolean setSmartProfileFocus(int position) {
//        if (Log.INCLUDE) {
//            Log.d(TAG, "setSmartProfileFocus: position="+position);
//        }
        if (profileObjectAdapter != null) {
            int size = profileObjectAdapter.size();
            if (position < size) {
                View child = profileGridView.getChildAt(position);
                if (child != null) {
                    child.setActivated(true);
                }

                if (updatedProfileName != null && !updatedProfileName.equals("")) {
                    if (position == size - 1) {
                        for (int i = 0; i < size; i++) {
                            UserProfile profile = (UserProfile) profileObjectAdapter.get(i);
                            try {
                                if (updatedProfileName.equals(profile.getName())) {
                                    if (Log.INCLUDE) {
                                        Log.d(TAG, "setSmartProfileFocus: index=" + i + ", profile name=" + profile.getName());
                                    }
                                    setProfileFocus(i);
                                    return true;
                                }
                            } catch (Exception e) {
                            }
                        }
                    }
                } else {
                    setProfileFocus(size - 1);
                }
            }
        }
        return false;
    }

    private boolean setProfileFocus(int index) {
        try {
            profileGridView.setSelectedPosition(index);
            profileGridView.requestFocus();
            View child = profileGridView.getLayoutManager().findViewByPosition(index);
            if (child != null) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "setProfileFocus: index=" + index);
                }
                child.requestFocus();
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public void notifyUserDiscountChange(int pointCouponSize, int discountCouponSize, ArrayList<DiscountCoupon> discountCoupons, Membership membership) {
        setCouponView();
        setMembershipPoint();
    }

    @Override
    public void notifyFail(int key) {
        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getContext());
        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
    }

    @Override
    public void notifyError(String errorCode, String message) {
        ErrorDialogFragment dialogFragment =
                ErrorDialogFragment.newInstance(TAG, errorCode, message);
        dialogFragment.show(getFragmentManager(), ErrorDialogFragment.CLASS_NAME);
    }

    private void setMembershipPoint() {
        Membership membership = userTaskManager.getMembership();
        if (membership != null) {
            int point = membership.getMembershipPoint();
            membershipPoint.setText(StringUtil.getFormattedNumber(point));
        }
    }

    private void onProfileSelected(UserProfile profile) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onProfileSelected: profile=" + profile);
        }
        if (profile.isAddOption() || profile.isManageOption()) {
            ArrayList<UserProfile> list = new ArrayList();
            list.addAll(profileListWithOptions);
            for (UserProfile p : list) {
                if (p.isManageOption()) {
                    list.remove(p);
                    break;
                }
            }
            int viewType = ProfileManageDialog.TYPE_ADD;
            if (profile.isManageOption()) {
                viewType = ProfileManageDialog.TYPE_LIST;
                if (profileList.size() == 1) {
                    viewType = ProfileManageDialog.TYPE_EDIT;
                    showProfileDialog(viewType, profileList, profileList.get(0));
                    return;
                }
            }
            showProfileDialog(viewType, list, null);

        } else {
            if (!profile.getId().equals(currentProfileID)) {
                if (profile.getProfileInfo().isLock()) {
                    ProfilePinCheckDialog profilePinCheckDialog = ProfilePinCheckDialog.newInstance(profile.getProfileInfo(), new ProfilePinCheckDialog.OnPinCheckResultListener() {
                        @Override
                        public void onPinCheckResult(boolean correct, Object object) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onPinCheckResult: correct=" + correct);
                            }
                            if (correct) {
                                loginNewProfile(profile);
                            }
                        }
                    });
                    showSafeDialog(profilePinCheckDialog);

                } else {
                    loginNewProfile(profile);
                }
            } else {
//                showProfileDialog(ProfileManageDialog.TYPE_EDIT, profileList, profile);
            }
        }
    }

    private void showProfileDialog(int viewType, ArrayList<UserProfile> list, UserProfile profile) {
        try {
            ProfileManageDialog profileManageDialog = null;
            if (profile != null) {
                profileManageDialog = ProfileManageDialog.newInstance(viewType, list, profile);
            } else {
                profileManageDialog = ProfileManageDialog.newInstance(viewType, list);
            }
            showSafeDialog(profileManageDialog);
        } catch (Exception e) {
        }
    }

    private void showProfileDeleteNotice() {
        ProfileDeleteNoticeDialogFragment dialogFragment = ProfileDeleteNoticeDialogFragment.newInstance();
        dialogFragment.show(getChildFragmentManager(), ProfileDeleteNoticeDialogFragment.CLASS_NAME);
    }

    private void loginNewProfile(UserProfile profile) {
        Log.d(TAG, "loginNewProfile");
        AccountManager accountMgr = AccountManager.getInstance();
        UserDataManager userDataManager = new UserDataManager();
        AsyncTask task = userDataManager.requestProfileLogin(
                accountMgr.getLoginId(),
                accountMgr.getSaId(),
                profile.getId(),
                new MbsDataProvider<String, ProfileLoginResult>() {
                    @Override
                    public void needLoading(boolean loading) {
                        checkLoadList(0, loading);
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "profileLogin: onFailed");
                        }
                    }

                    @Override
                    public void onSuccess(String key, ProfileLoginResult result) {
                        String newProfileId = profile.getId();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "profileLogin: onSuccess: newProfileId=" + newProfileId + ", result=" + result);
                        }
                        AccountManager.getInstance().onNewProfileLogin(newProfileId, result);

                        notifyProfileUpdate(newProfileId);

                        MyMenuDialog.this.dismiss();
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(TAG, errorCode, message);
                        errorDialogFragment.show(getChildFragmentManager(), ErrorDialogFragment.CLASS_NAME);
                    }
                });
    }

    private void notifyProfileUpdate(String profileId) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(PushMessageReceiver.KEY_TYPE, PushMessageReceiver.ProfileUpdateType.login);
        bundle.putString(PushMessageReceiver.KEY_PROFILE_ID, profileId);

        Intent intent = new Intent();
        intent.setAction(ACTION_UPDATED_PROFILE);
        intent.putExtras(bundle);

        if (Log.INCLUDE) {
            Log.d(TAG, "notifyProfileUpdate() intent=" + intent.getStringExtra(PushMessageReceiver.KEY_PROFILE_ID));
        }
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    private void updateAccountProfileList() {
        if (Log.INCLUDE) {
            Log.d(TAG, "updateAccountProfileList");
        }
        new Thread() {
            public void run() {
                ArrayList<ProfileInfo> list = (ArrayList<ProfileInfo>) AccountManager.getInstance().getData(AccountManager.KEY_PROFILE_LIST);
                boolean added = false;
                for (UserProfile userProfile : profileList) {
                    boolean existed = false;
                    for (ProfileInfo profileInfo : list) {
                        if (userProfile.getId().equals(profileInfo.getProfileId())) {
                            existed = true;
                            break;
                        }
                    }
                    if (!existed) {
                        added = true;
                        list.add(userProfile.getProfileInfo());
                    }
                }

                if (added) {
                    AccountManager.getInstance().updateProfileList(list);
                }
            }
        }.start();
    }

    private void onMenuSelected(MenuItem menu) {
        if (Log.INCLUDE) {
            Log.d(TAG, "onMenuSelected() menu=" + menu);
        }
        switch (menu.getType()) {
            case PURCHASED:
                PurchasedListDialog purchasedListDialog = PurchasedListDialog.newInstance();
                showSafeDialog(purchasedListDialog);
                break;

            case COLLECTION:
                CollectionListDialog collectionListDialog = CollectionListDialog.newInstance();
                showSafeDialog(collectionListDialog);
                break;

            case SUBSCRIPTION:
                SubscriptionListDialog subscriptionListDialog = SubscriptionListDialog.newInstance();
                showSafeDialog(subscriptionListDialog);
                break;

            case REMINDER:
                BookedListDialog bookedListDialog = BookedListDialog.newInstance();
                showSafeDialog(bookedListDialog);
                break;

            case COUPON:
                CouponHistoryDialog couponHistoryDialog = CouponHistoryDialog.newInstance();
                showSafeDialog(couponHistoryDialog);
                break;
        }
    }

    private final class ChannelItemClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder var, Row row) {
            if (Log.INCLUDE) {
                Log.d(TAG, "ChannelItemClickedListener: onItemClicked: item=" + item + ", row=" + row);
            }
            if (item == null) {
                return;
            }
            if (item instanceof Channel) {
                Channel channel = (Channel) item;
                try {
                    PlaybackUtil.startLiveTvActivityWithChannel(getActivity().getApplicationContext(), channel.getId());
                } catch (Exception e) {
                }
            }
        }
    }

    private final class VodItemClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, Presenter.ViewHolder var, Row row) {
            if (Log.INCLUDE) {
                Log.d(TAG, "VodItemClickedListener: onItemClicked: item=" + item + ", row=" + row);
            }
            if (item == null) {
                return;
            }
            if (item instanceof FavoriteContent) {
                FavoriteContent content = (FavoriteContent) item;
                if (content.isPackage()) {
                    Linker.goToPackageVodDetail(mTvOverlayManager, content.getPackageId(), content.getRating());
                } else {
//                    Linker.goToVodDetail(mTvOverlayManager, getChildFragmentManager(), content.getContentGroupId());
                    Linker.goToVodDetail(mTvOverlayManager, Content.buildFavoriteContent(content));
                }
            }
        }
    }
}
