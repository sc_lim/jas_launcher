/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.common;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.system.settings.preference.UserPreferenceManagerImp;

import static kr.altimedia.launcher.jasmin.ui.view.util.NotificationAlertService.SERVICE_COUNTING;
import static kr.altimedia.launcher.jasmin.ui.view.util.NotificationAlertService.SERVICE_COUNTING_ACTION;

public class NotificationIcon extends RelativeLayout {
    public static final String CLASS_NAME = NotificationIcon.class.getName();
    private static final String TAG = NotificationIcon.class.getSimpleName();

    private static final String NOTIFICATION_ACTION = "com.android.tv.action.OPEN_NOTIFICATIONS_PANEL";
    private static final String NOTIFICATION_PACKAGE = "com.google.android.tvrecommendations";

    public static final String NOTIFICATION_REGISTER_ACTION = "alticast.intent.ACTION_NOTIFICATION_REGISTER";
    public static final String NOTIFICATION_REGISTER = "NOTIFICATION_REGISTER";

    private RelativeLayout relatedLayout = null;
    private ImageView bgLayout = null;
    private ImageView iconLayout = null;

    private RelativeLayout countLayout = null;
    private ImageView countBg = null;
    private TextView countTextView = null;

    private LocalBroadcastManager mLocalBroadcastManager;
    private UserPreferenceManagerImp userPreferenceManagerImp;

    private ArrayList<String> notifications;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            if (Log.INCLUDE) {
                Log.d(TAG, "Action : " + intent.getAction());
            }

            notifications = (ArrayList<String>) intent.getExtras().get(SERVICE_COUNTING);
            setNotifications();
        }
    };

    public NotificationIcon(Context context) {
        this(context, null);
    }

    public NotificationIcon(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NotificationIcon(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public NotificationIcon(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
        getAttrs(context, attrs, defStyleAttr, defStyleRes);
    }

    private void getAttrs(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.NotificationIcon, defStyleAttr, defStyleRes);
        setTypeArray(context, typedArray, attrs, defStyleAttr, defStyleRes);
    }

    @SuppressLint("ResourceAsColor")
    private void setTypeArray(Context context, TypedArray typedArray, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        int bgDrawable = typedArray.getResourceId(R.styleable.NotificationIcon_noti_bg_drawable, R.drawable.selector_notification_bg);
        int iconDrawable = typedArray.getResourceId(R.styleable.NotificationIcon_noti_icon_drawable, R.drawable.icon_util_alarm);
        int countBgDrawable = typedArray.getResourceId(R.styleable.NotificationIcon_noti_count_bg_drawable, R.drawable.selector_notification_icon_bg);
        int countColor = typedArray.getResourceId(R.styleable.NotificationIcon_noti_count_color, R.color.color_FF222535);
        int fontFamily = typedArray.getResourceId(R.styleable.NotificationIcon_noti_badge_font, R.font.font_prompt_semibold);

        bgLayout.setBackgroundResource(bgDrawable);
        iconLayout.setBackgroundResource(iconDrawable);
        countBg.setBackgroundResource(countBgDrawable);
        countTextView.setTextColor(getResources().getColorStateList(countColor));

        if (fontFamily != -1) {
            countTextView.setTypeface(getResources().getFont(fontFamily));
        }

        countLayout.setVisibility(View.INVISIBLE);

        typedArray.recycle();
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.common_notification_icon, this, false);
        addView(v);

        setDescendantFocusability(FOCUS_AFTER_DESCENDANTS);

        relatedLayout = v.findViewById(R.id.relatedLayout);
        bgLayout = v.findViewById(R.id.bgLayout);
        iconLayout = v.findViewById(R.id.iconLayout);

        countLayout = v.findViewById(R.id.count_layout);
        countBg = v.findViewById(R.id.count_bg);
        countTextView = v.findViewById(R.id.count);
    }

    @Override
    public void setOnKeyListener(OnKeyListener l) {
        relatedLayout.setOnKeyListener(l);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        relatedLayout.setOnClickListener(l);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();

        if (action != KeyEvent.ACTION_DOWN) {
            return false;
        }

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
                saveNotifications();

                Intent intent = new Intent(NOTIFICATION_ACTION);
                intent.setPackage(NOTIFICATION_PACKAGE);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(intent);

                return true;
        }

        return super.dispatchKeyEvent(event);
    }

    private void saveNotifications() {
        if (Log.INCLUDE) {
            Log.d(TAG, "saveNotifications");
        }

        countBg.setActivated(false);
        userPreferenceManagerImp.setNotifications(notifications);
    }

    private void setNotifications() {
        int newSize = notifications.size();
        if (Log.INCLUDE) {
            Log.d(TAG, "count : " + newSize);
        }
        countTextView.setText("" + newSize);

        List<String> oldNotifications = userPreferenceManagerImp.getNotifications();
        int oldSize = oldNotifications != null ? oldNotifications.size() : 0;

        if (Log.INCLUDE) {
            Log.d(TAG, "setNotifications, oldSize : " + oldSize + ", newSize : " + newSize);
        }

        int visibility = newSize == 0 ? View.INVISIBLE : View.VISIBLE;
        countLayout.setVisibility(visibility);

        if (isShowNotificationPanel()) {
            if (Log.INCLUDE) {
                Log.d(TAG, "setNotifications, isShowNotificationPanel == true");
            }

            saveNotifications();
            return;
        }

        boolean isChanged = false;
        if (oldNotifications == null || oldSize < newSize) {
            isChanged = true;
            if (newSize > 0) {
                countBg.setActivated(true);
            }
        } else if (oldSize > newSize) {
            isChanged = true;
            saveNotifications();
        } else if (oldSize == newSize) {
            for (String old : oldNotifications) {
                isChanged = true;
                for (String neww : notifications) {
                    if (old.equalsIgnoreCase(neww)) {
                        isChanged = false;
                        break;
                    }
                }

                if (isChanged) {
                    break;
                }
            }

            countBg.setActivated(isChanged);
        }

        if (Log.INCLUDE) {
            Log.d(TAG, "setNotifications, isChanged : " + isChanged);
        }
    }

    private boolean isShowNotificationPanel() {
        ActivityManager mActivityManager = (ActivityManager) getContext().getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> list = mActivityManager.getRunningAppProcesses();

        for (ActivityManager.RunningAppProcessInfo rap : list) {
            if (rap.importance == rap.IMPORTANCE_FOREGROUND) {
                if (NOTIFICATION_PACKAGE.equalsIgnoreCase(rap.processName)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        userPreferenceManagerImp = new UserPreferenceManagerImp(getContext());
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(getContext());

        mLocalBroadcastManager.registerReceiver(mMessageReceiver, new IntentFilter(SERVICE_COUNTING_ACTION));
        setNotificationRegister(true);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mLocalBroadcastManager.unregisterReceiver(mMessageReceiver);
        setNotificationRegister(false);
    }

    private void setNotificationRegister(boolean isRegister) {
        Intent intent = new Intent(NOTIFICATION_REGISTER_ACTION);
        intent.putExtra(NOTIFICATION_REGISTER, isRegister);

        mLocalBroadcastManager.sendBroadcast(intent);
    }
}
