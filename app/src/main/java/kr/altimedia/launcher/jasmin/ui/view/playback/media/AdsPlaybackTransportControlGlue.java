/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.playback.media;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;

import com.altimedia.util.Log;

import java.lang.ref.WeakReference;

import androidx.leanback.widget.Action;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import kr.altimedia.launcher.jasmin.media.VideoPlayerAdapter;
import kr.altimedia.launcher.jasmin.ui.view.playback.VideoPlaybackRowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.AdsDetailsDescriptionPresenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackControlsRow;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;

/**
 * Created by mc.kim on 29,04,2020
 */
public class AdsPlaybackTransportControlGlue<T extends VideoPlayerAdapter>
        extends VideoPlaybackBaseControlGlue<T> {

    static final String TAG = "AdsPlaybackTransportControlGlue";
    static final boolean DEBUG = false;

    static final int MSG_UPDATE_PLAYBACK_STATE = 100;
    static final int MSG_HIDE_CONTROL_STATE = 101;
    static final int MSG_HIDE_PLAYBACK_STATE = 102;
    static final int MSG_HIDE_PLAY_CONTROL_STATE = 103;

    static final int HIDE_CONTROL_STATE_DELAY_MS = 1500;
    static final int HIDE_PLAYBACK_STATE_DELAY_MS = 3500;
    static final int HIDE_PLAY_CONTROLLER_STATE_DELAY_MS = HIDE_CONTROL_STATE_DELAY_MS + HIDE_PLAYBACK_STATE_DELAY_MS;

    boolean mSeekEnabled;

    private class UpdatePlaybackStateHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_UPDATE_PLAYBACK_STATE:
                    AdsPlaybackTransportControlGlue glue =
                            ((WeakReference<AdsPlaybackTransportControlGlue>) msg.obj).get();
                    if (glue != null) {
                        glue.onUpdatePlaybackState();
                    }
                    sendHandlerMsg(MSG_HIDE_PLAY_CONTROL_STATE);
                    break;
                case MSG_HIDE_CONTROL_STATE:
                    getHost().hideStateOverlay(true);
                    if (getHost().isControlsOverlayVisible()) {
                        sendHandlerMsg(MSG_HIDE_PLAYBACK_STATE);
                    } else {
                        if (isPlaying()) {
                            getHost().hideIconView(true);
                        } else {
                            getHost().showIconView(true);
                        }
                    }
                    break;
                case MSG_HIDE_PLAYBACK_STATE:
                    getHost().hideControlsOverlay(true);
                    break;

                case MSG_HIDE_PLAY_CONTROL_STATE:
                    getHost().hideStateOverlay(true);
                    getHost().hideControlsOverlay(true);
                    break;
            }

        }
    }

    final Handler sHandler = new UpdatePlaybackStateHandler();

    final WeakReference<VideoPlaybackBaseControlGlue> mGlueWeakReference = new WeakReference(this);

    /**
     * Constructor for the glue.
     *
     * @param context
     * @param impl    Implementation to underlying media player.
     */

    public AdsPlaybackTransportControlGlue(Context context, T impl, long currentTime, long fakeDuration) {
        super(context, impl);
        this.mCurrentTime = currentTime;
        this.mFakeDuration = fakeDuration;
    }

    @Override
    public void setControlsRow(VideoPlaybackControlsRow controlsRow) {
        super.setControlsRow(controlsRow);
        clearSHandler();
        onUpdatePlaybackState();
    }

    private void clearSHandler() {
        sHandler.removeMessages(MSG_HIDE_PLAY_CONTROL_STATE, mGlueWeakReference);
        sHandler.removeMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference);
        sHandler.removeMessages(MSG_HIDE_PLAYBACK_STATE, mGlueWeakReference);
        sHandler.removeMessages(MSG_HIDE_CONTROL_STATE, mGlueWeakReference);
    }

    @Override
    protected void onCreatePrimaryActions(ArrayObjectAdapter primaryActionsAdapter) {
        primaryActionsAdapter.add(mPlayPauseAction =
                new VideoPlaybackControlsRow.PlayPauseAction(getContext()));
    }

    private long mCurrentTime = -1;
    private long mFakeDuration = -1;

    @Override
    protected void onTimerStop() {
        super.onTimerStop();
        clearSHandler();
    }

    @Override
    protected VideoPlaybackRowPresenter onCreateRowPresenter() {

        final AdsDetailsDescriptionPresenter detailsPresenter =
                new AdsDetailsDescriptionPresenter() {
                    @Override
                    protected void onBindDescription(ViewHolder
                                                             viewHolder, Object obj) {
                        viewHolder.getTitle().setText(mTitle);
                    }
                };

        AdsPlaybackTransportRowPresenter rowPresenter = new AdsPlaybackTransportRowPresenter(mCurrentTime, mFakeDuration) {
            @Override
            protected void onBindRowViewHolder(RowPresenter.ViewHolder vh, Object item) {
                super.onBindRowViewHolder(vh, item);
                vh.setOnKeyListener(AdsPlaybackTransportControlGlue.this);
            }

            @Override
            protected void onUnbindRowViewHolder(RowPresenter.ViewHolder vh) {
                super.onUnbindRowViewHolder(vh);
                vh.setOnKeyListener(null);
            }

            @Override
            protected void onProgressBarKey(ViewHolder vh, KeyEvent keyEvent) {
                super.onProgressBarKey(vh, keyEvent);
            }

            @Override
            protected void onProgressBarClicked(ViewHolder vh) {
                super.onProgressBarClicked(vh);
            }
        };
        rowPresenter.setDescriptionPresenter(detailsPresenter);
        return rowPresenter;
    }

    @Override
    protected void onAttachedToHost(VideoPlaybackGlueHost host) {
        super.onAttachedToHost(host);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttachedToHost");
        }
    }

    @Override
    protected void onDetachedFromHost() {
        super.onDetachedFromHost();
        clearSHandler();
    }


    @Override
    public void onActionClicked(Action action) {
        dispatchAction(action, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE));
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
            case KeyEvent.KEYCODE_ESCAPE:
            case KeyEvent.KEYCODE_DPAD_LEFT:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
            case KeyEvent.KEYCODE_MEDIA_REWIND:
            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                return false;
        }
        if (mControlsRow == null) {
            return false;
        }
        if (isDPADKeyEvent(event)) {

            return true;
        }

        final ObjectAdapter primaryActionsAdapter = mControlsRow.getPrimaryActionsAdapter();
        Action action = mControlsRow.getActionForKeyCode(primaryActionsAdapter, keyCode);
        if (action == null) {
            action = mControlsRow.getActionForKeyCode(mControlsRow.getSecondaryActionsAdapter(),
                    keyCode);
        }

        if (action != null) {
            dispatchAction(action, event);
            return true;
        }


        sendHandlerMsg(MSG_HIDE_CONTROL_STATE);
        return false;
    }


//    void onUpdatePlaybackStatusAfterUserAction() {
//        onUpdatePlaybackStatusAfterUserAction(HIDE_CONTROL_STATE_DELAY_MS);
//    }


    @Override
    protected void onUpdatePlaybackStatusAfterUserAction(final int msgWhat) {
        updatePlaybackState(mIsPlaying);
        sendHandlerMsg(msgWhat);
    }


    void sendHandlerMsg(int msgWhat) {
        clearSHandler();
        switch (msgWhat) {
            case MSG_UPDATE_PLAYBACK_STATE:
                sHandler.sendMessage(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference));
                break;

            case MSG_HIDE_CONTROL_STATE:
                sHandler.sendMessageDelayed(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference), HIDE_CONTROL_STATE_DELAY_MS);
                break;

            case MSG_HIDE_PLAYBACK_STATE:
                sHandler.sendMessageDelayed(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference), HIDE_PLAYBACK_STATE_DELAY_MS);
                break;

            case MSG_HIDE_PLAY_CONTROL_STATE:
                sHandler.sendMessageDelayed(sHandler.obtainMessage(msgWhat,
                        mGlueWeakReference), HIDE_PLAY_CONTROLLER_STATE_DELAY_MS);
                break;
        }
    }


    void onUpdatePlaybackStatusAfterUserAction(int index, int msgWhat) {
        updatePlaybackState(mIsPlaying, index);
        sendHandlerMsg(msgWhat);
    }


    boolean isDPADKeyEvent(KeyEvent keyEvent) {


        boolean isCenter = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER;
        boolean up = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP;
        boolean down = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN;
        boolean right = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT;
        boolean left = keyEvent == null
                || keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT;
        if (isCenter) {
            if (getHost().isControlsOverlayVisible()) {
                return false;
            }
            if (keyEvent.getAction() == KeyEvent.ACTION_UP) {
                return true;
            }

            if (isPlaying()) {
                pause();
                mIsPlaying = false;
            } else {
                play();
                mIsPlaying = true;
            }
            onUpdatePlaybackStatusAfterUserAction(MSG_HIDE_CONTROL_STATE);
            getHost().showStateOverlay(true);
            getHost().showControlsOverlay(true);
            return true;
        } else if (up || down || left || right) {
            if (getHost().isControlsOverlayVisible()) {
                return false;
            }
            getHost().showControlsOverlay(true);
            sendHandlerMsg(MSG_UPDATE_PLAYBACK_STATE);
            return true;
        }
        return false;
    }

    /**
     * Called when the given action is invoked, either by click or keyevent.
     */
    boolean dispatchAction(Action action, KeyEvent keyEvent) {
        boolean handled = false;
        if (keyEvent == null) {
            return false;
        }
        if (mPlayerAdapter.isKeyBlock()) {
            return false;
        }

        if (!mPlayerAdapter.isReady()) {
            return false;
        }
        if (action instanceof VideoPlaybackControlsRow.PlayPauseAction) {

            boolean canPlay = keyEvent == null
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY;
            boolean canPause = keyEvent == null
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                    || keyEvent.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PAUSE;
            //            PLAY_PAUSE    PLAY      PAUSE
            // playing    paused                  paused
            // paused     playing       playing
            // ff/rw      playing       playing   paused

            if (canPause && mIsPlaying) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                    return true;
                }
                pause();
                mIsPlaying = false;
                onUpdatePlaybackStatusAfterUserAction(MSG_HIDE_CONTROL_STATE);
                getHost().showStateOverlay(true);
                getHost().showControlsOverlay(true);
                handled = true;
            } else if (canPlay && !mIsPlaying) {
                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                    return true;
                }
                play();
                mIsPlaying = true;
                onUpdatePlaybackStatusAfterUserAction(MSG_HIDE_CONTROL_STATE);
                getHost().showStateOverlay(true);
                getHost().showControlsOverlay(true);
                handled = true;
            }
//            getHost().showStateOverlay(true);
//            handled = true;
        } else if (action instanceof VideoPlaybackControlsRow.SkipNextAction) {
            next();
            handled = true;
        } else if (action instanceof VideoPlaybackControlsRow.SkipPreviousAction) {
            previous();
            handled = true;
        }
        return handled;
    }


    @Override
    protected long getDuration() {
        return mFakeDuration;
    }

    @Override
    protected long getCurrentPosition() {
        return super.getCurrentPosition();
    }

    boolean onForward() {
        long seekTimeInMs = getCurrentPosition() + AdsPlaybackTransportRowPresenter.SEEK_DIFFER_TIME;
        if (seekTimeInMs > getDuration()) {
            seekTimeInMs = getDuration();
        }
        seekTo(seekTimeInMs);
        return true;
    }

    boolean onBackward() {
        long seekTimeInMs = getCurrentPosition() - AdsPlaybackTransportRowPresenter.SEEK_DIFFER_TIME;
        if (seekTimeInMs < 0) {
            seekTimeInMs = 0;
        }
        seekTo(seekTimeInMs);
        return true;
    }

    @Override
    protected void onPlayStateChanged() {
        if (DEBUG) Log.v(TAG, "onStateChanged");
        if (!isReady()) {
            return;
        }
        if (sHandler.hasMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference)) {
            sHandler.removeMessages(MSG_UPDATE_PLAYBACK_STATE, mGlueWeakReference);
            if (mPlayerAdapter.isPlaying() != mIsPlaying) {
                if (DEBUG) Log.v(TAG, "Status expectation mismatch, delaying update");
                sHandler.sendMessageDelayed(sHandler.obtainMessage(MSG_UPDATE_PLAYBACK_STATE,
                        mGlueWeakReference), HIDE_CONTROL_STATE_DELAY_MS);
            } else {
                if (DEBUG) Log.v(TAG, "Update state matches expectation");
                onUpdatePlaybackState();
            }
        } else {
            onUpdatePlaybackState();
        }

        super.onPlayStateChanged();
    }

    void onUpdatePlaybackState() {
        mIsPlaying = mPlayerAdapter.isPlaying();
        updatePlaybackState(mIsPlaying);
    }

    private void updatePlaybackState(boolean isPlaying) {
        int index = !isPlaying
                ? VideoPlaybackControlsRow.PlayPauseAction.INDEX_PLAY
                : VideoPlaybackControlsRow.PlayPauseAction.INDEX_PAUSE;
        updatePlaybackState(isPlaying, index);
        if (Log.INCLUDE) {
            Log.d(TAG, "updatePlaybackState : " + isPlaying);
        }
        if (isPlaying) {
            getHost().hideIconView(true);
        }
    }


    private void updatePlaybackState(boolean isPlaying, int iconIndex) {
        if (Log.INCLUDE) {
            Log.d(TAG, " updatePlaybackState : " + isPlaying + ", iconIndex : " + iconIndex);
        }
        if (mControlsRow == null) {
            return;
        }

        if (!isPlaying) {
            onUpdateProgress();
            mPlayerAdapter.setProgressUpdatingEnabled(true);
        } else {
            mPlayerAdapter.setProgressUpdatingEnabled(true);
        }

        if (mFadeWhenPlaying && getHost() != null) {
            getHost().setControlsOverlayAutoHideEnabled(isPlaying);
        }

        if (mPlayPauseAction != null) {
            int index = iconIndex;
            int mPlayPauseActionIndex = mPlayPauseAction.getIndex();
            if (mPlayPauseActionIndex != index
                    || mPlayPauseActionIndex == VideoPlaybackControlsRow.PlayPauseAction.INDEX_REWIND
                    || mPlayPauseActionIndex == VideoPlaybackControlsRow.PlayPauseAction.INDEX_FAST_FORWARD) {
                mPlayPauseAction.setIndex(index);
                notifyItemChanged((ArrayObjectAdapter) getControlsRow().getPrimaryActionsAdapter(),
                        mPlayPauseAction);
            }
        }
    }

    public final void setSeekEnabled(boolean seekEnabled) {
        mSeekEnabled = seekEnabled;
    }

    public final boolean isSeekEnabled() {
        return mSeekEnabled;
    }

}
