/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.register;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.coupon.object.Coupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.DiscountCoupon;
import kr.altimedia.launcher.jasmin.dm.coupon.object.PointCoupon;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class CouponRegistrationCompleteFragment extends Fragment {
    public static final String CLASS_NAME = CouponRegistrationCompleteFragment.class.getName();
    private final String TAG = CouponRegistrationCompleteFragment.class.getSimpleName();

    private static final String KEY_COUPON = "COUPON";

    private TextView couponName;
    private TextView couponPrice;
    private TextView couponUnit;
    private TextView btnOk;

    private Coupon coupon;

    private CouponRegistrationCompleteFragment() {
    }

    public static CouponRegistrationCompleteFragment newInstance(Coupon coupon) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_COUPON, coupon);

        CouponRegistrationCompleteFragment fragment = new CouponRegistrationCompleteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_coupon_registration_complete, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
    }

    private void initView(View view) {
        Bundle bundle = getArguments();
        coupon = bundle.getParcelable(KEY_COUPON);

        couponName = view.findViewById(R.id.couponName);
        couponPrice = view.findViewById(R.id.couponPrice);
        couponUnit = view.findViewById(R.id.couponUnit);

        if(coupon != null) {
            if(coupon instanceof PointCoupon){
                PointCoupon pc = (PointCoupon)coupon;
                couponName.setText(R.string.cash_coupon);
                couponPrice.setText(StringUtil.getFormattedNumber(pc.getPointPrice()));
                couponUnit.setText(R.string.thb);
            }else if(coupon instanceof DiscountCoupon){
                DiscountCoupon dc = (DiscountCoupon)coupon;
                couponName.setText(R.string.discount_coupon);
                couponPrice.setText(StringUtil.getFormattedNumber(dc.getRate()));
                couponUnit.setText("%");
            }
        }

        btnOk = view.findViewById(R.id.btnOk);
        btnOk.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if(event.getAction() != KeyEvent.ACTION_DOWN) {
                    return true;
                }
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                    case KeyEvent.KEYCODE_BACK:
                        CouponRegistrationDialog dialog = ((CouponRegistrationDialog) getParentFragment());
                        CouponRegistrationDialog.CouponRegisterListener listener = dialog.getCouponRegisterListener();
                        if (listener != null) {
                            listener.onSuccess(coupon);
                        }
                        dialog.dismiss();
                        break;
                }
                return false;
            }
        });
    }
}
