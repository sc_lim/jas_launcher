/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.cwmp.service;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import com.alticast.af.system.RJasSysInfoEdidInfo;
import com.altimedia.cwmplibrary.common.CWMPMultiObject;
import com.altimedia.cwmplibrary.common.CWMPParameter;
import com.altimedia.cwmplibrary.common.ModelObject;
import com.altimedia.cwmplibrary.common.method.InformCallback;
import com.altimedia.cwmplibrary.ttbb.inform.TTBBInform;
import com.altimedia.cwmplibrary.ttbb.object.InternetGatewayDevice;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.deviceinfo.X_TTBB_EDID;
import com.altimedia.cwmplibrary.ttbb.object.internetgatewaydevice.deviceinfo.X_TTBB_EDIDs;
import com.altimedia.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import af.system.JasSysInfoEdidEventListener;
import kr.altimedia.launcher.jasmin.cwmp.service.control.AcsStbControl;
import kr.altimedia.launcher.jasmin.cwmp.service.control.AcsXTTBBControl;
import kr.altimedia.launcher.jasmin.cwmp.service.control.HotKeyInformControl;
import kr.altimedia.launcher.jasmin.cwmp.service.control.NetworkInformControl;
import kr.altimedia.launcher.jasmin.cwmp.service.def.CWMPConfig;
import kr.altimedia.launcher.jasmin.cwmp.service.def.CWMPEtcCode;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;

/**
 * CWMPController
 */
public class CWMPController {
    private final String CLASS_NAME = CWMPController.class.getName();
    private final String TAG = CWMPController.class.getSimpleName();

    private final String MGT_SVR_PARAM_KEY = "unSet";
    private final String DEV_INFO_PRODUCT_CLS = "T01";
    private final String DEV_INFO_SPEC_VER = "0.9.29"; //""T01-1.0";
    private final String NOT_REGISTERED = "Not-Registered";

    private static CWMPController instance;

    private int targetAcs = 0; // live or testbed
    private String registrationServer = "";
    private String launcherVersion = "Not Supported";

    private CWMPCPEManager cpeManager;
    private InternetGatewayDevice root;

    private NetworkInformControl networkInformControl;
    private AcsXTTBBControl acsXTTBBControl;
    private AcsStbControl acsStbControl;
    private HotKeyInformControl hotKeyInformControl;

    private CWMPEventListener cwmpEventListener;
    private CWMPSystemInfoManager jasSystemManager;

    private boolean bootCompleted = false;
    private String provisioningCode = NOT_REGISTERED;
    private TTBBInform.Command lastBootInform;
    private Date firstUseDate = null;
    private Context context;

    private HashMap<String, TTBBInform.Command> bootInformMap = new HashMap<String, TTBBInform.Command>();

    public static void removeInstance() {
        if(instance != null){
            instance.dispose();
            instance = null;
        }
    }

    public static CWMPController getInstance() {
        if(instance == null){
            instance = new CWMPController();
        }
        return instance;
    }

    private void dispose() {
        stop();

        if(networkInformControl != null){
            networkInformControl.dispose();
            networkInformControl = null;
        }
        if(acsXTTBBControl != null){
            acsXTTBBControl.dispose();
            acsXTTBBControl = null;
        }
        if(acsStbControl != null){
            acsStbControl.dispose();
            acsStbControl = null;
        }
        if(hotKeyInformControl != null){
            hotKeyInformControl.dispose();
            hotKeyInformControl = null;
        }
        cpeManager = null;
        root = null;
        context = null;

    }

    /**
     * CWMPController를 초기화합니다.
     *
     * @param context
     */
    void init(Context context, int targetAcs, String said, String registrationServer, String launcherVersion) {
        if (Log.INCLUDE) {
            Log.d(TAG, "init: targetAcs=" + targetAcs+", registrationServer="+registrationServer+", said="+said);
        }
        this.context = context;
        this.targetAcs = targetAcs;
        this.registrationServer = registrationServer;
        this.launcherVersion = launcherVersion;

        jasSystemManager = new CWMPSystemInfoManager();

        networkInformControl = new NetworkInformControl();
        networkInformControl.init(context);

        acsXTTBBControl = new AcsXTTBBControl();
        acsXTTBBControl.init(context, CWMPConfig.FW_UPDATE_URLs[this.targetAcs], CWMPConfig.APK_META_URLs[this.targetAcs], CWMPConfig.DEFAULT_SETTING_URLs[this.targetAcs]);

        acsStbControl = new AcsStbControl();
        acsStbControl.init(context);

        hotKeyInformControl = new HotKeyInformControl();
        hotKeyInformControl.init(context);

        cpeManager = CWMPCPEManager.getInstance();
        cpeManager.init(context, acsXTTBBControl);

        root = cpeManager.getRoot();

        setBasicInfo(said);
    }

    /**
     * CWMPController를 시작합니다.
     */
    void start() {
        if (Log.INCLUDE) {
            Log.d(TAG, "start: CWMP is stared with (" + CWMPConfig.TCP_SRC_PORT + ", " + CWMPConfig.UDP_SRC_PORT + ", " + CWMPConfig.UDP_DST_PORT + ")");
        }

        boolean ret = cpeManager.start(CWMPConfig.TCP_SRC_PORT, CWMPConfig.UDP_SRC_PORT, CWMPConfig.UDP_DST_PORT);

        if (Log.INCLUDE) {
            if (ret) Log.d(TAG, "start: success");
            else Log.d(TAG, "start: failure");
        }

        networkInformControl.start(root);
        acsXTTBBControl.start(root);
        acsStbControl.start(root);
        hotKeyInformControl.start(root);
    }

    /**
     * CWMPController를 중지합니다.
     *
     * @return
     */
    boolean stop() {
        boolean ret = cpeManager.stop();
        if (Log.INCLUDE) {
            if (ret) Log.d(TAG, "stop: success");
            else Log.d(TAG, "stop: failure");
        }
        return ret;
    }

    private void setParamValue(CWMPParameter parameter, Object value){
        try{
            parameter.setValue(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setBasicInfo(String said) {
        String mainAcsUrl = CWMPConfig.MGT_SVR_MAIN_URLs[this.targetAcs];
        String subAcsUrl = CWMPConfig.MGT_SVR_SUB_URLs[this.targetAcs];
        String conReqUsername = CWMPConfig.MGT_SVR_CONN_REQ_USERs[this.targetAcs];
        String conReqPassword = CWMPConfig.MGT_SVR_CONN_REQ_PWs[this.targetAcs];
        String manufacturer = jasSystemManager.getManufacturerName(); // Build.MANUFACTURER;
        String manufacturerOUI = jasSystemManager.getManufacturerOUI();
        String macAddress = networkInformControl.getMacAddress(":");
        String serialNumber = macAddress;
        String serialNumberOrigin = "";
        try {
            // 개인정보보호정책에 따라 Q버전에서는 Build.SERIAL 호출 제한
//            serialNumberOrigin = (String) Build.class.getField("SERIAL").get(null);
            serialNumberOrigin = Settings.Secure.getString(this.context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
        }
        String hwVersion = jasSystemManager.getHwVersion(); // Build.HARDWARE;
        String swVersion = jasSystemManager.getFirmwareVersion();
        int upTime = 0;
        try {
            upTime = (int) jasSystemManager.getUpTime();
        }catch (Exception e){
        }
        String manufactureYearMonth = jasSystemManager.getManufactureYearMonth();
        String osVersion = "Android " + Build.VERSION.RELEASE;
        String socName = jasSystemManager.getSocName();
        String decoderName = jasSystemManager.getDecoderName();
        String codecSupport = jasSystemManager.getCodecSupport();
        String bootLoaderVersion = jasSystemManager.getBootLoaderVersion();
        String model = jasSystemManager.getModelName(); // Build.MODEL;
        String acsPassword = DEV_INFO_PRODUCT_CLS + "-" + serialNumber;
        String acsUsername = manufacturerOUI + "-" + acsPassword;

        if (Log.INCLUDE) {
            Log.d(TAG, "CWMP_INFO: flavor=" + targetAcs);
            Log.d(TAG, "CWMP_INFO: mac address=" + macAddress);
            Log.d(TAG, "CWMP_INFO: manufacturer=" + manufacturer);
            Log.d(TAG, "CWMP_INFO: manufacturer OUI=" + manufacturerOUI);
            Log.d(TAG, "CWMP_INFO: serial number(mac)=" + serialNumber);
            Log.d(TAG, "CWMP_INFO: serial number(origin)=" + serialNumberOrigin);
            Log.d(TAG, "CWMP_INFO: hardware version=" + hwVersion);
            Log.d(TAG, "CWMP_INFO: software version=" + swVersion);
            Log.d(TAG, "CWMP_INFO: manufacture year month=" + manufactureYearMonth);
            Log.d(TAG, "CWMP_INFO: osVersion=" + osVersion);
            Log.d(TAG, "CWMP_INFO: socName=" + socName);
            Log.d(TAG, "CWMP_INFO: decoderName=" + decoderName);
            Log.d(TAG, "CWMP_INFO: codecSupport=" + codecSupport);
            Log.d(TAG, "CWMP_INFO: epgVersion=" + launcherVersion);
            Log.d(TAG, "CWMP_INFO: bootLoaderVersion=" + bootLoaderVersion);
            Log.d(TAG, "CWMP_INFO: model=" + model);
            Log.d(TAG, "CWMP_INFO: username=" + acsUsername);
            Log.d(TAG, "CWMP_INFO: password=" + acsPassword);
        }

        setAcsUrl(root.managementServer.URL, mainAcsUrl);
        setAcsUrl(root.managementServer.SubURL, subAcsUrl);

        setParamValue(root.managementServer.Username, acsUsername);
        setParamValue(root.managementServer.Password, acsPassword);
        setParamValue(root.managementServer.ConnectionRequestUsername, conReqUsername);
        setParamValue(root.managementServer.ConnectionRequestPassword, conReqPassword);
        setParamValue(root.managementServer.ParameterKey, MGT_SVR_PARAM_KEY);

        setParamValue(root.deviceInfo.Manufacturer, manufacturer);
        setParamValue(root.deviceInfo.ManufacturerOUI, manufacturerOUI);
        setParamValue(root.deviceInfo.ProductClass, DEV_INFO_PRODUCT_CLS);
        setParamValue(root.deviceInfo.SerialNumber, serialNumber);
        setParamValue(root.deviceInfo.HardwareVersion, hwVersion);
        setParamValue(root.deviceInfo.SoftwareVersion, swVersion);
        setParamValue(root.deviceInfo.SpecVersion, cpeManager.getSpecVersion());
        setParamValue(root.deviceInfo.UpTime, upTime);
        setParamValue(root.deviceInfo.ModelName, model);
        setParamValue(root.deviceInfo.AdditionalSoftwareVersion, launcherVersion);
        setParamValue(root.deviceInfo.x_ttbb_additionalInfo.SerialNumber, serialNumberOrigin);
        setParamValue(root.deviceInfo.x_ttbb_additionalInfo.ManufactureYearMonth, manufactureYearMonth);
        setParamValue(root.deviceInfo.x_ttbb_additionalInfo.OsVersion, osVersion);
        setParamValue(root.deviceInfo.x_ttbb_additionalInfo.SoCName, socName);
        setParamValue(root.deviceInfo.x_ttbb_additionalInfo.DecoderName, decoderName);
        setParamValue(root.deviceInfo.x_ttbb_additionalInfo.CodecSupport, codecSupport);
        setParamValue(root.deviceInfo.x_ttbb_additionalInfo.EPGVersion, launcherVersion);
        setParamValue(root.deviceInfo.x_ttbb_additionalInfo.BootLoaderVer, bootLoaderVersion);

        setLinkStatus();
        setConnectionRequestURL();

        setProvisioningCode(said);
        setDeviceSummary();

        setEdidListener();

        if(!bootInformMap.containsKey(TTBBInform.Command.X_TTBB_RemoconHotKey)) {
            bootInformMap.put(TTBBInform.Command.X_TTBB_RemoconHotKey, new TTBBInform.Command(TTBBInform.Command.X_TTBB_RemoconHotKey));
        }
    }

    public boolean setConnectionRequestURL(){
        boolean urlChanged = false;
        boolean ethAvailable = networkInformControl.isEthernetConnected();
        boolean wlanAvailable = networkInformControl.isWifiConnected();
        String localIpAddress = "";
        if(ethAvailable){
            localIpAddress = networkInformControl.getEthernetIpAddress();
        } else if(wlanAvailable){
            localIpAddress = networkInformControl.getWifiIpAddress();
        }
        if(localIpAddress != null && !localIpAddress.isEmpty()) {
            int port = CWMPConfig.TCP_SRC_PORT;
            if(networkInformControl.isPrivateIpAddress(localIpAddress)){
                port = CWMPConfig.UDP_SRC_PORT;
            }
            String oldConnReqUrl = root.managementServer.ConnectionRequestURL.getValue();
            String newConnReqUrl = "http://" + localIpAddress + ":" + port;
            if (Log.INCLUDE) {
                Log.d(TAG, "CWMP_INFO: old ConnectionRequestURL=" + oldConnReqUrl);
                Log.d(TAG, "CWMP_INFO: new ConnectionRequestURL=" + newConnReqUrl+"(ethernet="+ethAvailable+", wifi="+wlanAvailable+")");
            }
            if(oldConnReqUrl != null && !oldConnReqUrl.equalsIgnoreCase(newConnReqUrl)){
                urlChanged = true;
            }
            setParamValue(root.managementServer.ConnectionRequestURL, newConnReqUrl);

            if(firstUseDate == null) {
                firstUseDate = new Date(JasmineEpgApplication.SystemClock().currentTimeMillis());
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.ms");
                setParamValue(root.deviceInfo.FirstUseDate, dateFormat.format(firstUseDate));
                if (Log.INCLUDE) {
                    Log.d(TAG, "CWMP_INFO: FirstUseDate="+root.deviceInfo.FirstUseDate.getValue());
                }
            }
        }else{
            if (Log.INCLUDE) {
                Log.d(TAG, "CWMP_INFO: local ip is not available");
            }
        }
        return urlChanged;
    }

    private String setAcsUrl(CWMPParameter param, String defaultUrl){
        try {
            if (Log.INCLUDE) {
                Log.d(TAG, "setAcsUrl: " + param.getFullName() + " >> value:" + (param.getValue()!=null?param.getValue().toString():"") + ", isDefault:" + param.isDefault());
            }
            Object object = param.getValue();
            String value = "";
            if (object != null && object instanceof String) {
                value = (String) object;
                if (value.isEmpty() || param.isDefault()) {
                    param.setValue(defaultUrl);
                } else {
                    defaultUrl = value;
                }
            }else{
                param.setValue(defaultUrl);
            }

            if (Log.INCLUDE) {
                Log.d(TAG, "setAcsUrl: " + param.getFullName() + " >> value:" + param.getValue().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defaultUrl;
    }

    private void setProvisioningCode(String said){
        if(root.deviceInfo.ProvisioningCode.isDefault()){
            if(said != null && !said.isEmpty()) {
                provisioningCode = said;
            }
        }else{
            provisioningCode = root.deviceInfo.ProvisioningCode.getValue();
            if(NOT_REGISTERED.equals(provisioningCode)){
                if(said != null && !said.isEmpty()) {
                    provisioningCode = said;
                }
            }else{
                if(provisioningCode != null && said != null  && !said.isEmpty() && !provisioningCode.equals(said)){
                    provisioningCode = said;
                }
            }
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "CWMP_INFO: provisioningCode=" + provisioningCode);
        }
        setParamValue(root.deviceInfo.ProvisioningCode, provisioningCode);
    }

    private void setDeviceSummary(){
        String manufacturerOUI = jasSystemManager.getManufacturerOUI();
        String macAddress = networkInformControl.getMacAddress(":");
        String serialNumber = macAddress;
        String model = jasSystemManager.getModelName(); // Build.MODEL;
        String swVersion = jasSystemManager.getFirmwareVersion(); //Build.VERSION.RELEASE;

        StringBuilder deviceSummary = new StringBuilder();
        deviceSummary.append(manufacturerOUI).append("-");
        deviceSummary.append(DEV_INFO_PRODUCT_CLS).append("-");
        deviceSummary.append(serialNumber).append(",");
        deviceSummary.append(macAddress).append(",");
        deviceSummary.append(model).append(",");
        deviceSummary.append(swVersion).append(",");
        deviceSummary.append(provisioningCode);
        if (Log.INCLUDE) {
            Log.d(TAG, "CWMP_INFO: deviceSummary=" + deviceSummary.toString());
        }
        setParamValue(root.DeviceSummary, deviceSummary.toString());
    }

    private void setLinkStatus(){
        boolean lanAvailable = networkInformControl.isEthernetConnected();
        boolean wlanAvailable = networkInformControl.isWifiConnected();
        int status = 0;
        if(lanAvailable && wlanAvailable){
            status = 6;
        }else if(!lanAvailable && wlanAvailable){
            status = 4;
        }else if(lanAvailable && !wlanAvailable){
            status = 2;
        }
        setParamValue(root.deviceInfo.LinkStatus, new Integer(status));
        if (Log.INCLUDE) {
            Log.d(TAG, "CWMP_INFO: LinkStatus=" + status);
        }
    }

    private void setEdidListener(){
        if (Log.INCLUDE) {
            Log.d(TAG, "setEdidListener");
        }

        for(final ModelObject object : root.deviceInfo.getChilds()) {
            if (object instanceof CWMPMultiObject && object instanceof X_TTBB_EDIDs) {
                X_TTBB_EDID x_ttbb_edid = ((X_TTBB_EDIDs) object).getX_TTBB_EDID(1);
                if (x_ttbb_edid == null) {
                    x_ttbb_edid = ((X_TTBB_EDIDs) object).addX_TTBB_EDID(new X_TTBB_EDID(1));
                }
                break;
            }
        }

        jasSystemManager.addSysInfoEdidEventListener(new JasSysInfoEdidEventListener() {
            @Override
            public void onChangedEdidInfo(RJasSysInfoEdidInfo edidInfo) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "onChangedEdidInfo: edidInfo="+edidInfo);
                }
                try {
                    setEdidInfo(
                            edidInfo.version,
                            edidInfo.revision,
                            edidInfo.resolution,
                            edidInfo.screenSize,
                            edidInfo.manufacturer,
                            edidInfo.model,
                            edidInfo.DOTNo);

                    notifyEdidInfo();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public void setEdidInfo(String version,
                               String revision,
                               String resolution,
                               String screenSize,
                               String manufacturer,
                               String model,
                               String dotNo) {
        if (!isCpeAvailable()) {
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "setEdidInfo");
        }

//        InternetGatewayDevice.DeviceInfo.X_TTBB_EDID.{i}.xxx
        for(final ModelObject object : root.deviceInfo.getChilds()) {
//            Log.d(TAG, "notifyEdidInfo: ModelObject >> "+ object.getFullName());
            if (object instanceof CWMPMultiObject && object instanceof X_TTBB_EDIDs) {
//                Log.d(TAG, "notifyEdidInfo: CWMPMultiObject >> "+ object.getFullName());
                X_TTBB_EDID x_ttbb_edid = ((X_TTBB_EDIDs) object).getX_TTBB_EDID(1);
                if(x_ttbb_edid == null) {
                    x_ttbb_edid = ((X_TTBB_EDIDs) object).addX_TTBB_EDID(new X_TTBB_EDID(1));
                }

                setParamValue(x_ttbb_edid.Supported, 1);
                setParamValue(x_ttbb_edid.Version, version);
                setParamValue(x_ttbb_edid.Revision, revision);
                setParamValue(x_ttbb_edid.Resolution, resolution);
                setParamValue(x_ttbb_edid.ScreenSize, screenSize);
                setParamValue(x_ttbb_edid.Manufacturer, manufacturer);
                setParamValue(x_ttbb_edid.Model, model);
                setParamValue(x_ttbb_edid.DOTNo, dotNo);
                break;
            }
        }
    }

    private boolean isCpeAvailable(){
        if(root == null ||cpeManager == null){
            if (Log.INCLUDE) {
                Log.d(TAG, "CPE manager is NOT available");
            }
            return false;
        }
        return true;
    }

    public boolean isBootCompleted() {
        return bootCompleted;
    }

    public long getNormalPeriod() {
        return acsXTTBBControl.getNormalPeriod();
    }

    public void requestDefaultSetting(CWMPCallback cwmpCallback){
        acsXTTBBControl.requestDefaultSetting(cwmpCallback);
    }

    private void sendInform(TTBBInform.Command command) {
        final String informCommand = command.toString();
        if (Log.INCLUDE) {
            Log.d(TAG, "sendInform: command=" + informCommand);
        }
        boolean ret = cpeManager.sendInform(command, new InformCallback() {
            @Override
            public void onSuccess() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "sendInform: received SUCCESS informResponse for " + informCommand);
                }
            }

            @Override
            public void onFail() {
                if (Log.INCLUDE) {
                    Log.d(TAG, "sendInform: received FAIL informResponse for " + informCommand);
                }
            }
        });
        if (Log.INCLUDE) {
            if (ret) Log.d(TAG, "sendInform: success");
            else Log.d(TAG, "sendInform: failure");
        }
    }

    private void sendInform(TTBBInform.Command command, InformCallback callback) {
        final String informCommand = command.toString();
        if (Log.INCLUDE) {
            Log.d(TAG, "sendInform: command=" + informCommand);
        }

        boolean ret = cpeManager.sendInform(command, callback);

        if (Log.INCLUDE) {
            if (ret) Log.d(TAG, "sendInform: success");
            else Log.d(TAG, "sendInform: failure");
        }
    }

    /**
     * BOOTSTRAP 혹은 BOOT inform 메시지를 전송합니다.
     * @param authorized 개통 혹은 미개통 여부
     */
    public void notifyBooting(boolean authorized, String said, String description){
        if(!isCpeAvailable()){
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyBooting: authorized=" + authorized+", said="+said);
        }

        setConnectionRequestURL();
        setProvisioningCode(said);
        setDeviceSummary();

        if(acsStbControl != null) {
            acsStbControl.setRegistration(registrationServer, authorized, description);
        }

        bootCompleted = true;
        String registerInform = TTBBInform.Command.X_TTBB_Registered;
        if(authorized){
            lastBootInform = new TTBBInform.Command(TTBBInform.Command.BOOT);
        }else{
            lastBootInform = new TTBBInform.Command(TTBBInform.Command.BOOTSTRAP);
            registerInform = TTBBInform.Command.X_TTBB_Unregistered;
        }
        if(!bootInformMap.containsKey(registerInform)) {
            bootInformMap.put(registerInform, new TTBBInform.Command(registerInform));
        }

        sendInform(lastBootInform);

        if(bootInformMap != null && !bootInformMap.isEmpty()){
            if (Log.INCLUDE) {
                Log.d(TAG, "notifyBooting: bootInformMap=" + bootInformMap.size());
            }
            Iterator<String> iter = bootInformMap.keySet().iterator();
            while(iter.hasNext()) {
                String key = iter.next();
                TTBBInform.Command inform = bootInformMap.get(key);
                if(inform.equalsName(TTBBInform.Command.X_TTBB_RemoconHotKey)){
                    notifyRemoconHotKey();
                }else {
                    sendInform(inform);
                }
            }
            bootInformMap.clear();
        }
    }

    public void notifyBooting(){
        if(!isCpeAvailable()){
            return;
        }
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyBooting: lastBootInform=" + lastBootInform);
        }
        if(lastBootInform != null) {
            sendInform(lastBootInform);
        }
    }

    /**
     * X_TTBB_WarmStart or X_TTBB_StandBy inform 메시지를 전송합니다.
     *
     * @param standby STB standby 여부
     * @param reason STB standby 진입 사유
     */
    public void notifyPowerState(boolean standby, String reason){
        if (!isCpeAvailable() || !isBootCompleted()) {
            return;
        }

        if(standby){
            setParamValue(root.x_ttbb_control.StandByReason, reason);
            sendInform(new TTBBInform.Command(TTBBInform.Command.X_TTBB_StandBy));

        }else{
            sendInform(new TTBBInform.Command(TTBBInform.Command.X_TTBB_WarmStart));
        }
    }

    /**
     * X_TTBB_LinkUp or X_TTBB_WlanLinkUp inform 메시지를 전송합니다.
     *
     * @param wireless 무선 인터넷 여부
     */
    public void notifyNetworkConnected(boolean wireless){
        if (!isCpeAvailable()) {
            return;
        }

        setLinkStatus();

        if (!isBootCompleted()) {
//            if(wireless) {
//                if(!bootInformMap.containsKey(TTBBInform.Command.X_TTBB_WlanLinkUp)) {
//                    bootInformMap.put(TTBBInform.Command.X_TTBB_WlanLinkUp, new TTBBInform.Command(TTBBInform.Command.X_TTBB_WlanLinkUp));
//                }
//            }
            return;
        }

        if(wireless){
//            sendInform(new TTBBInform.Command(TTBBInform.Command.X_TTBB_WlanLinkUp));

        }else{
            sendInform(new TTBBInform.Command(TTBBInform.Command.X_TTBB_LinkUp));
        }
    }

    /**
     * X_TTBB_IpAllocation inform 메시지를 전송합니다.
     *
     */
    public void notifyIpAllocated(){
        if (!isCpeAvailable() || !isBootCompleted()) {
            return;
        }

        sendInform(new TTBBInform.Command(TTBBInform.Command.X_TTBB_IpAllocation));
    }

    /**
     * 전달된 X_TTBB_EdidInfo inform 메시지를 전송합니다.
     *
     */
    public void notifyEdidInfo(String version,
                               String revision,
                               String resolution,
                               String screenSize,
                               String manufacturer,
                               String model,
                               String dotNo) {
        if (!isCpeAvailable()) {
            return;
        }

        setEdidInfo(version,
                    revision,
                    resolution,
                    screenSize,
                    manufacturer,
                    model,
                    dotNo);

        notifyEdidInfo();
    }

    /**
     * 저장된 X_TTBB_EdidInfo inform 메시지를 전송합니다.
     *
     */
    public void notifyEdidInfo() {
        if (!isCpeAvailable()) {
            return;
        }

        if (!isBootCompleted()) {
            if(!bootInformMap.containsKey(TTBBInform.Command.X_TTBB_EdidInfo)) {
                bootInformMap.put(TTBBInform.Command.X_TTBB_EdidInfo, new TTBBInform.Command(TTBBInform.Command.X_TTBB_EdidInfo));
            }
            return;
        }

        int size = root.deviceInfo.X_TTBB_EDIDNumberOfEntries.getValue();
        if (Log.INCLUDE) {
            Log.d(TAG, "notifyEdidInfo: X_TTBB_EDIDNumberOfEntries=" + size);
        }
        if(size > 0) {
            sendInform(new TTBBInform.Command(TTBBInform.Command.X_TTBB_EdidInfo));
        }
    }

    /**
     * X_TTBB_EtcError inform 메시지를 전송합니다.
     *
     * @param errorCode 에러코드
     * @param errorDesc 에러설명
     * @param errorTime 에러발생시간
     */
    public void notifyEtcError(String errorCode, String errorDesc, long errorTime) {
        if (!isCpeAvailable() || !isBootCompleted()) {
            return;
        }

        try {
            if(errorDesc == null || errorDesc.isEmpty()) {
                errorDesc = CWMPEtcCode.getErrorDescription(errorCode);
            }
            cpeManager.addEtcError(errorCode, errorDesc);
        }catch (Exception|Error e){
        }
    }

    /**
     * X_TTBB_WatchAuthority inform 메시지를 전송합니다.
     *
     */
    public void notifyWatchAuthority(){
        if (!isCpeAvailable() || !isBootCompleted()) {
            return;
        }

        sendInform(new TTBBInform.Command(TTBBInform.Command.X_TTBB_WatchAuthority));
    }

    /**
     * X_TTBB_RemoconHotKey inform 메시지를 전송합니다.
     *
     */
    public void notifyRemoconHotKey(InformCallback informCallback){
        if (!isCpeAvailable() || !isBootCompleted()) {
            return;
        }

        sendInform(new TTBBInform.Command(TTBBInform.Command.X_TTBB_RemoconHotKey), informCallback);
    }

    public void notifyRemoconHotKey(){
        if(hotKeyInformControl != null) {
            hotKeyInformControl.notifyRemoconHotKey();
        }
    }

    /**
     * 전달된 Hot key에 대한 클릭수를 집계합니다.
     *
     */
    public void onHotKeyClicked(int keyCode){
        if (!isCpeAvailable() || !isBootCompleted()) {
            return;
        }

        hotKeyInformControl.onHotKeyClicked(keyCode);
    }

    public CWMPEventListener getCWMPEventListener() {
        return cwmpEventListener;
    }

    public void registerCWMPEventListener(CWMPEventListener listener) {
        cwmpEventListener = listener;
    }

    public void unregisterCWMPEventListener(CWMPEventListener listener) {
        cwmpEventListener = null;
    }
}
