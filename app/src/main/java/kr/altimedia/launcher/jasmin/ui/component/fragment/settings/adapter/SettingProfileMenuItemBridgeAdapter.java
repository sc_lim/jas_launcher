/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter;

import android.view.KeyEvent;
import android.view.View;

import androidx.leanback.widget.ObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter.SettingMenuPresenter;
import kr.altimedia.launcher.jasmin.ui.view.browse.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;

public class SettingProfileMenuItemBridgeAdapter extends ItemBridgeAdapter {
    private VerticalGridView verticalGridView;
    private SettingProfileMenuItemBridgeAdapter.OnClickMenuListener onClickMenuListener;

    public SettingProfileMenuItemBridgeAdapter(ObjectAdapter adapter, VerticalGridView verticalGridView) {
        super(adapter);

        this.verticalGridView = verticalGridView;
    }

    public void setOnClickMenuListener(SettingProfileMenuItemBridgeAdapter.OnClickMenuListener onClickMenuListener) {
        this.onClickMenuListener = onClickMenuListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);
        Presenter presenter = viewHolder.getPresenter();

        if (presenter instanceof SettingMenuPresenter) {
            SettingMenuPresenter.ViewHolder vh = (SettingMenuPresenter.ViewHolder) viewHolder.getViewHolder();
            View view = vh.view;
            int size = getAdapter().size();

            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    int index = verticalGridView.getSelectedPosition();
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                            if (onClickMenuListener != null) {
                                onClickMenuListener.onClickMenu();
                                return true;
                            }

                            return false;
                        case KeyEvent.KEYCODE_DPAD_UP:
                            v.setActivated(false);

                            if (index == 0) {
                                verticalGridView.scrollToPosition(size - 1);
                                return true;
                            }

                            return false;
                        case KeyEvent.KEYCODE_DPAD_DOWN:
                            v.setActivated(false);

                            if (index == (size - 1)) {
                                verticalGridView.scrollToPosition(0);
                                return true;
                            }

                            return false;
                    }
                    return false;
                }
            });

            view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        v.setActivated(true);
                        vh.setParentFocus(true);
                    }
                }
            });
        }
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);

        Presenter.ViewHolder vh = viewHolder.getViewHolder();
        vh.view.setOnKeyListener(null);
        vh.view.setOnFocusChangeListener(null);
    }

    public interface OnClickMenuListener {
        void onClickMenu();
    }
}
