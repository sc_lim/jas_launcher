/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.menu.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.tvmodule.dao.Channel;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.view.presenter.RowPresenter;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;

public class FavoriteChannelPresenter extends RowPresenter {

    public FavoriteChannelPresenter() {
        super();
    }

    @Override
    public ViewHolder createRowViewHolder(ViewGroup parent) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        ChannelItemViewHolder mVodItemViewHolder = new ChannelItemViewHolder(inflater.inflate(R.layout.item_mymenu_favorite_channel, null, false));
        return mVodItemViewHolder;
    }

    @Override
    public void onBindRowViewHolder(ViewHolder viewHolder, Object item) {
        super.onBindRowViewHolder(viewHolder, item);

        ChannelItemViewHolder itemViewHolder = (ChannelItemViewHolder) viewHolder;
        Channel channel = (Channel) item;

        itemViewHolder.setItem(channel);
    }

    @Override
    public void onUnbindRowViewHolder(ViewHolder viewHolder) {
        super.onUnbindRowViewHolder(viewHolder);

        ChannelItemViewHolder itemViewHolder = (ChannelItemViewHolder) viewHolder;
        itemViewHolder.dispose();
    }

    public class ChannelItemViewHolder extends ViewHolder {
        private final String TAG = ChannelItemViewHolder.class.getSimpleName();

        private Channel channel;

        private TextView channelNumber;
        private TextView channelName;
        private ImageView channelLogo;

        public ChannelItemViewHolder(View view) {
            super(view);

            channelNumber = view.findViewById(R.id.channelNumber);
            channelName = view.findViewById(R.id.channelName);
            channelLogo = view.findViewById(R.id.channelLogo);
        }

        public void dispose() {

        }

        public void setItem(Channel channel) {
            if(channel == null) {
                this.channelNumber.setText("000");
                return;
            }

            this.channel = channel;

            String formattedNum = StringUtil.getFormattedNumber(channel.getDisplayNumber(), 3);
            this.channelNumber.setText(formattedNum);

            this.channelLogo.setVisibility(View.GONE);
            this.channelName.setVisibility(View.VISIBLE);
            this.channelName.setText(channel.getDisplayName());

//            this.channelName.setVisibility(View.GONE);
//            this.channelLogo.setVisibility(View.VISIBLE);
//            this.channelLogo.setImageResource(R.drawable.dummy_channel_logo_000); // TODO set corresponding logo
//            this.channelLogo.setImageAlpha(ALPHA_NORMAL);
        }
    }
}