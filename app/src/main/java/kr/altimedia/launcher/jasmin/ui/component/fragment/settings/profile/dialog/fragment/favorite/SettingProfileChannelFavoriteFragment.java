/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.profile.dialog.fragment.favorite;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.MbsDataTask;
import kr.altimedia.launcher.jasmin.dm.user.UserDataManager;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.SettingBaseFragment;
import com.altimedia.util.Log;

public class SettingProfileChannelFavoriteFragment extends SettingBaseFragment {
    public static final String CLASS_NAME = SettingProfileChannelFavoriteFragment.class.getName();
    private static final String TAG = SettingProfileChannelFavoriteFragment.class.getSimpleName();

    private TextView favoriteButton;

    private List<String> favoriteList = new ArrayList<>();
    private final UserDataManager mUserDataManager = new UserDataManager();
    private MbsDataTask favoriteTask;

    private SettingProfileChannelFavoriteFragment() {
    }

    public static SettingProfileChannelFavoriteFragment newInstance() {
        SettingProfileChannelFavoriteFragment fragment = new SettingProfileChannelFavoriteFragment();
        return fragment;
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_channel_favorite;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        initButton(view);
        initData();
    }

    private void initData() {
        favoriteTask = mUserDataManager.getFavoriteChannelList(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(),
                new MbsDataProvider<String, List<String>>() {
                    @Override
                    public void needLoading(boolean loading) {
                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "initData, onFailed");
                        }
                    }

                    @Override
                    public void onSuccess(String id, List<String> result) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "initData, size : " + result.size());
                        }

                        favoriteList.clear();
                        favoriteList.addAll(0, result);

                        setCount();
                    }

                    @Override
                    public void onError(String errorCode, String message) {

                    }
                });
    }

    private void initButton(View view) {
        favoriteButton = view.findViewById(R.id.favorite);
        OnKeyListener onKeyListener = new OnKeyListener(getFragmentManager());
        onKeyListener.setFavoriteList(favoriteList);

        favoriteButton.setOnKeyListener(onKeyListener);
    }

    private void setCount() {
        int size = favoriteList.size();
        Context context = getContext();
        String text = size > 0 ? context.getString(R.string.blocked_channels) + " : " + size : context.getString(R.string.no_blocked_channels);

        favoriteButton.setText(text);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (favoriteTask != null) {
            favoriteTask.cancel(true);
        }
    }

    private static class OnKeyListener implements View.OnKeyListener {
        private FragmentManager fragmentManager;
        private List<String> favoriteList = new ArrayList<>();

        public OnKeyListener(FragmentManager fragmentManager) {
            this.fragmentManager = fragmentManager;
        }

        public void setFavoriteList(List<String> favoriteList) {
            this.favoriteList = favoriteList;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() != KeyEvent.ACTION_DOWN) {
                return false;
            }

            switch (keyCode) {
                case KeyEvent.KEYCODE_ENTER:
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    SettingProfileChannelFavoriteSetupDialogFragment settingProfileChannelFavoriteSetupDialogFragment = SettingProfileChannelFavoriteSetupDialogFragment.newInstance((ArrayList<String>) favoriteList);
                    settingProfileChannelFavoriteSetupDialogFragment.setOnFavoriteChannelChangeResultCallback(new SettingProfileChannelFavoriteSetupDialogFragment.OnFavoriteChannelChangeResultCallback() {
                        @Override
                        public void onChangeFavoriteChannel(boolean isSuccess, int favoriteSize) {
                            if (Log.INCLUDE) {
                                Log.d(TAG, "onChangeFavoriteChannel, isSuccess : " + isSuccess + ", favoriteSize : " + favoriteSize);
                            }

                            settingProfileChannelFavoriteSetupDialogFragment.dismiss();
                            if (isSuccess) {
                                setCount((TextView) v, favoriteSize);
                            }
                        }
                    });
                    settingProfileChannelFavoriteSetupDialogFragment.show(fragmentManager, SettingProfileChannelFavoriteSetupDialogFragment.CLASS_NAME);
                    return true;
                case KeyEvent.KEYCODE_DPAD_UP:
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    return true;
            }

            return false;
        }

        private void setCount(TextView textView, int size) {
            Context context = textView.getContext();
            String text = size > 0 ? context.getString(R.string.blocked_channels) + " : " + size : context.getString(R.string.no_blocked_channels);
            textView.setText(text);
        }
    }
}
