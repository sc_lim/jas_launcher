/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.playback;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.ui.view.playback.widget.VideoPlaybackSeekDataProvider;

/**
 * Created by mc.kim on 03,03,2020
 */
public class ThumbnailObjectAdapter extends ArrayObjectAdapter {
    private final String TAG = ThumbnailObjectAdapter.class.getSimpleName();
    private VideoPlaybackSeekDataProvider mVideoPlaybackSeekDataProvider = null;

    public ThumbnailObjectAdapter(Presenter presenter) {
        super(presenter);
    }

    public ThumbnailObjectAdapter(Presenter presenter, VideoPlaybackSeekDataProvider seekDataProvider) {
        super(presenter);
        this.mVideoPlaybackSeekDataProvider = seekDataProvider;
    }

    public void setVideoPlaybackSeekDataProvider(VideoPlaybackSeekDataProvider videoPlaybackSeekDataProvider) {
        this.mVideoPlaybackSeekDataProvider = videoPlaybackSeekDataProvider;
    }

    public int getSeekableSize() {
        if (mVideoPlaybackSeekDataProvider == null) {
            return 0;
        }
        long[] seekPositions = mVideoPlaybackSeekDataProvider.getSeekPositions();
        if (seekPositions == null) {
            return 0;
        }
        return seekPositions.length;
    }

    @Override
    public int size() {
        if (mVideoPlaybackSeekDataProvider != null) {
            long[] seekPositions = mVideoPlaybackSeekDataProvider.getSeekPositions();
            int visibleSize = mVideoPlaybackSeekDataProvider.getVisibleSize();
            int seekLength = seekPositions.length;
            boolean hasMargin = seekLength % visibleSize != 0;
            if (hasMargin) {
                int computeSize = ((seekLength / visibleSize) + 1) * visibleSize;
                return computeSize;
            } else {
                return seekLength;
            }
        } else {
            return 0;
        }
    }

    @Override
    public Object get(int position) {
        long[] seekPositions = mVideoPlaybackSeekDataProvider.getSeekPositions();
        if(seekPositions.length>position){
            return seekPositions[position];
        }else{
            return -1L;
        }
    }
}
