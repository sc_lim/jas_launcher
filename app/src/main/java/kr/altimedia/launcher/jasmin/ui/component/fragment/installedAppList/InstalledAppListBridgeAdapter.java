/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.installedAppList;

import android.view.KeyEvent;
import android.view.View;

import java.util.ArrayList;

import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ObjectAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.adapter.PageBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.view.presenter.AppPresenter;
import kr.altimedia.launcher.jasmin.ui.view.util.InstalledAppUtil;
import com.altimedia.util.Log;

public class InstalledAppListBridgeAdapter extends PageBridgeAdapter {
    private static final String TAG = InstalledAppListBridgeAdapter.class.getSimpleName();
    private OnBindListener onBindListener;

    public InstalledAppListBridgeAdapter(ObjectAdapter adapter, int rowCount, int columnCount, OnBindListener onBindListener) {
        super(adapter, rowCount, columnCount);
        this.onBindListener = onBindListener;
    }

    @Override
    protected void onBind(ViewHolder viewHolder) {
        super.onBind(viewHolder);

        AppPresenter presenter = (AppPresenter) viewHolder.mPresenter;
        AppPresenter.OnKeyListener onKeyListener = presenter.getOnKeyListener();

        int index = (int) viewHolder.itemView.getTag(R.id.KEY_INDEX);
        if (onKeyListener != null) {
            viewHolder.mHolder.view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    InstalledAppUtil.AppInfo appInfo = ((InstalledAppUtil.AppInfo) viewHolder.mItem);
                    String title = appInfo.getTitle();
                    if (Log.INCLUDE) {
                        Log.d(TAG, "onKey, item : " + title + ", index : " + index);
                    }

                    return onKeyListener.onKey(event, index, appInfo);
                }
            });
        }

        onBindListener.onBind(index);
    }

    @Override
    protected void onUnbind(ViewHolder viewHolder) {
        super.onUnbind(viewHolder);
        viewHolder.mHolder.view.setOnKeyListener(null);
    }

    public void updateList(ArrayList<InstalledAppUtil.AppInfo> list) {
        ArrayObjectAdapter originAdapter = (ArrayObjectAdapter) getAdapter();
        originAdapter.setItems(list, null);
    }

    public void onItemMove(int from, int to) {
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) getAdapter();
        objectAdapter.move(from, to);

        int startRange = from < to ? from : to;
        notifyItemRangeChanged(startRange, objectAdapter.size());
    }

    public ArrayList<InstalledAppUtil.AppInfo> getList() {
        ArrayList<InstalledAppUtil.AppInfo> list = new ArrayList<>();
        ArrayObjectAdapter objectAdapter = (ArrayObjectAdapter) getAdapter();
        for (int i = 0; i < objectAdapter.size(); i++) {
            InstalledAppUtil.AppInfo appInfo = (InstalledAppUtil.AppInfo) objectAdapter.get(i);
            list.add(appInfo);
        }

        return list;
    }

    public interface OnBindListener {
        void onBind(int index);
    }
}
