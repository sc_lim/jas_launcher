package kr.altimedia.launcher.jasmin.ui.view.guide.listener;

import android.view.KeyEvent;

import androidx.leanback.widget.VerticalGridView;

import com.altimedia.util.Log;

import kr.altimedia.launcher.jasmin.ui.view.guide.ProgramGuide;


/**
 * Created by mc.kim on 03,09,2020
 */
public class OnRowRepeatedKeyInterceptListener implements VerticalGridView.OnKeyInterceptListener {
    private static final String TAG = "OnRowRepeatedKeyInterceptListener";
    private final ProgramGuide mProgramGuide;

    public OnRowRepeatedKeyInterceptListener(ProgramGuide programGuide) {
        mProgramGuide = programGuide;
    }

    @Override
    public boolean onInterceptKeyEvent(KeyEvent event) {
        if (mProgramGuide.isTimeLineScrollState() || event.getRepeatCount() % 2 == 1) {
            if( Log.INCLUDE){
                Log.d(TAG,"onInterceptKeyEvent | mProgramGuide.isTimeLineScrollState() : "+mProgramGuide.isTimeLineScrollState());
                Log.d(TAG,"onInterceptKeyEvent | event.getRepeatCount() : "+event.getRepeatCount());
            }
            return true;
        }

        return false;
    }
}
