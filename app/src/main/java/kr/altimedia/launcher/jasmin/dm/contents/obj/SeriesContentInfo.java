/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.dm.contents.obj;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class SeriesContentInfo implements Parcelable {
    private String focusedEpisode;
    private int rating;
    private List<SeriesContent> seriesList = new ArrayList<>();

    public SeriesContentInfo(String focusedEpisode, int rating, List<SeriesContent> seriesList) {
        this.focusedEpisode = focusedEpisode;
        this.rating = rating;
        this.seriesList = seriesList;
    }

    protected SeriesContentInfo(Parcel in) {
        focusedEpisode = in.readString();
        rating = in.readInt();
        in.readList(seriesList, SeriesContent.class.getClassLoader());
    }

    public static final Creator<SeriesContentInfo> CREATOR = new Creator<SeriesContentInfo>() {
        @Override
        public SeriesContentInfo createFromParcel(Parcel in) {
            return new SeriesContentInfo(in);
        }

        @Override
        public SeriesContentInfo[] newArray(int size) {
            return new SeriesContentInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(focusedEpisode);
        dest.writeInt(rating);
        dest.writeList(seriesList);
    }


    public String getFocusedEpisode() {
        return focusedEpisode;
    }

    public int getRating() {
        return rating;
    }

    public List<SeriesContent> getSeriesList() {
        return seriesList;
    }
}
