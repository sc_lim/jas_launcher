/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.reminder;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.OnChildLaidOutListener;
import androidx.leanback.widget.Presenter;

import com.altimedia.tvmodule.TvSingletons;
import com.altimedia.tvmodule.dao.Channel;
import com.altimedia.util.Log;

import java.util.ArrayList;
import java.util.Calendar;

import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.media.PlaybackUtil;
import kr.altimedia.launcher.jasmin.ui.app.JasmineEpgApplication;
import kr.altimedia.launcher.jasmin.ui.component.dialog.SafeDismissDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.adapter.ButtonBridgeAdapter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.data.ButtonItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.common.presenter.ButtonItemPresenter;
import kr.altimedia.launcher.jasmin.ui.component.fragment.reminder.Reminder;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.HorizontalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.StringUtil;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

public class BookedDeletePopup extends SafeDismissDialogFragment implements View.OnUnhandledKeyEventListener {
    public static final String CLASS_NAME = BookedDeletePopup.class.getName();
    private final String TAG = BookedDeletePopup.class.getSimpleName();

    private String title;

    private HorizontalGridView buttonGridView;
    private ArrayObjectAdapter buttonObjectAdapter = null;

    private OnConfirmListener onConfirmListener;

    private Reminder bookedItem;
    private View rootView;

    private BookedDeletePopup(Reminder item) {
        this.bookedItem = item;
    }

    public static BookedDeletePopup newInstance(Reminder item) {
        BookedDeletePopup fragment = new BookedDeletePopup(item);
        return fragment;
    }

    @Override
    public int getOverlayType() {
        return TvOverlayManager.OVERLAY_TYPE_DIALOG;
    }

    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        this.onConfirmListener = onConfirmListener;
    }

    @Override
    public void onDestroyView() {
        if(rootView != null) {
            rootView.removeOnUnhandledKeyEventListener(this);
        }

        super.onDestroyView();
    }

    @Override
    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_ESCAPE) {
            return false;
        }
        if (getActivity() == null) {
            return false;
        }
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            return getActivity().onKeyDown(keyCode, event);
        } else {
            return getActivity().onKeyUp(keyCode, event);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DefaultDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_mymenu_booked_delete, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view;
        rootView.addOnUnhandledKeyEventListener(this);

        initView(view);
    }

    private void initView(View view) {
        if (Log.INCLUDE) {
            Log.d(TAG, "initView");
        }
        buttonGridView = view.findViewById(R.id.buttonGridView);

        long channelId = -1;
        if (bookedItem != null) {
            try {
                channelId = bookedItem.getChannelId();
                Channel channel = TvSingletons.getSingletons(view.getContext()).getChannelDataManager().getChannel(channelId);
                if (Log.INCLUDE) {
                    Log.d(TAG, "initView: channelId=" + channelId + ", channel=" + channel);
                }
                String chNum = "";
                String chName = "";
                boolean isLocked = false;
                if (channel != null) {
                    chNum = StringUtil.getFormattedNumber(channel.getDisplayNumber(), 3);
                    chName = channel.getDisplayName();
                    isLocked = channel.isLocked();
                }
                String pgmName = bookedItem.getTitle();

                int diffDay = getDiffDay(bookedItem.getStartTime());
                String startDay = "Today";
                if (diffDay == 0) {
                    startDay = getString(R.string.today2);
                } else if (diffDay == 1) {
                    startDay = getString(R.string.tomorrow);
                } else {
                    startDay = TimeUtil.getModifiedDate(bookedItem.getStartTime(), "E dd MMM");
                }
                String startTime = StringUtil.getFormattedHour(Calendar.HOUR_OF_DAY, bookedItem.getStartTime())
                        + ":" + StringUtil.getFormattedMinute(bookedItem.getStartTime());
    //            String endTime = StringUtil.getFormattedHour(bookedItem.getEndTimeUtcMillis())
    //                    + ":" + StringUtil.getFormattedMinute(bookedItem.getEndTimeUtcMillis());

                TextView channelNumber = view.findViewById(R.id.channelNumber);
                TextView channelName = view.findViewById(R.id.channelName);
                TextView programName = view.findViewById(R.id.programName);
                TextView programStartDay = view.findViewById(R.id.programStartDay);
                TextView programStartTime = view.findViewById(R.id.programStartTime);

                channelNumber.setText(chNum);
                channelName.setText(chName);
                programName.setText(pgmName);
                programStartDay.setText(startDay);
                programStartTime.setText(startTime);

            }catch (Exception e){
            }
        }

        ArrayList<ButtonItem> buttonItems = new ArrayList<>();
        buttonItems.add(new ButtonItem(ButtonItem.TYPE_WATCH_CHANNEL, getResources().getString(R.string.watch_channel)));
        buttonItems.add(new ButtonItem(ButtonItem.TYPE_DELETE, getResources().getString(R.string.delete)));
        buttonItems.add(new ButtonItem(ButtonItem.TYPE_CLOSE, getResources().getString(R.string.close)));

        final long selectedChannelId = channelId;
        ButtonItemPresenter presenter = new ButtonItemPresenter(false, new ButtonItemPresenter.OnKeyListener() {
            @Override
            public boolean onKey(int keyCode, int index, Object item, Presenter.ViewHolder viewHolder) {
                if (Log.INCLUDE) {
                    Log.d(TAG, "ButtonItemPresenter.onKey() keyCode=" + keyCode + ", index=" + index);
                }
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER: {
                        if (item instanceof ButtonItem) {
                            ButtonItem button = (ButtonItem) item;
                            int type = button.getType();
                            if (type == ButtonItem.TYPE_WATCH_CHANNEL) {
                                if(selectedChannelId >= 0) {
                                    onConfirmListener.onConfirmed(false);
                                    tuneToChannel(selectedChannelId);
                                }
                            } else if (type == ButtonItem.TYPE_DELETE) {
                                onConfirmListener.onConfirmed(true);
                            } else if (type == ButtonItem.TYPE_CLOSE) {
                                onConfirmListener.onConfirmed(false);
                            }
                        }
                    }
                }
                return false;
            }
        });
        presenter.setSize(R.dimen.mymenu_booked_button_width, R.dimen.mymenu_booked_button_height);
        buttonObjectAdapter = new ArrayObjectAdapter(presenter);
        buttonObjectAdapter.addAll(0, buttonItems);
        ButtonBridgeAdapter bridgeAdapter = new ButtonBridgeAdapter();
        bridgeAdapter.setAdapter(buttonObjectAdapter);
        buttonGridView.setAdapter(bridgeAdapter);

        int horizontalSpacing = (int) view.getResources().getDimension(R.dimen.mymenu_button_horizontal_space);
        buttonGridView.setHorizontalSpacing(horizontalSpacing);

        setFocus();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);

        onConfirmListener = null;
    }

    private void setFocus(){
        buttonGridView.setOnChildLaidOutListener(new OnChildLaidOutListener() {
            @Override
            public void onChildLaidOut(ViewGroup parent, View view, int position, long id) {
                if(ButtonItem.TYPE_DELETE == position) {
                    View child = buttonGridView.getChildAt(position);
                    if (child != null) {
                        buttonGridView.setSelectedPosition(position);
                    }
                }
            }
        });
    }

    private int getDiffDay(long targetTime){
        Calendar currCal = Calendar.getInstance();
        currCal.setTimeInMillis(JasmineEpgApplication.SystemClock().currentTimeMillis());
        int currYear = currCal.get(Calendar.YEAR);
        int currMonth = currCal.get(Calendar.MONTH) + 1;
        int currDay = currCal.get(Calendar.DAY_OF_MONTH);

        Calendar targetCal = Calendar.getInstance();
        targetCal.setTimeInMillis(targetTime);
        int targetYear = targetCal.get(Calendar.YEAR);
        int targetMonth = targetCal.get(Calendar.MONTH) + 1;
        int targetDay = targetCal.get(Calendar.DAY_OF_MONTH);

        int currDate = currYear * 12 + currMonth + currDay;
        int targetDate = targetYear * 12 + targetMonth + targetDay;
        return targetDate - currDate;
    }

    private void tuneToChannel(long channelId) {
        if (Log.INCLUDE) {
            Log.d(TAG, "tuneToChannel: channelId=" + channelId);
        }
        try {
            PlaybackUtil.startLiveTvActivityWithChannel(getActivity().getApplicationContext(), channelId);
        }catch (Exception e){
        }
    }

    public interface OnConfirmListener {
        void onConfirmed(boolean confirmed);
    }
}
