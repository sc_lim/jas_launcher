/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.payment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.altimedia.tvmodule.util.StringUtils;
import com.altimedia.util.Log;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.ItemBridgeAdapter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.dm.MbsDataProvider;
import kr.altimedia.launcher.jasmin.dm.coupon.CouponDataManager;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponProduct;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseRequest;
import kr.altimedia.launcher.jasmin.dm.coupon.object.CouponPurchaseResult;
import kr.altimedia.launcher.jasmin.dm.payment.obj.CreditCard;
import kr.altimedia.launcher.jasmin.dm.payment.type.PaymentType;
import kr.altimedia.launcher.jasmin.tv.LiveTvActivity;
import kr.altimedia.launcher.jasmin.ui.app.AccountManager;
import kr.altimedia.launcher.jasmin.ui.component.activity.LauncherActivity;
import kr.altimedia.launcher.jasmin.ui.component.activity.VideoPlaybackActivity;
import kr.altimedia.launcher.jasmin.ui.component.dialog.FailNoticeDialogFragment;
import kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.PaymentGuideItem;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.data.PurchaseInfo;
import kr.altimedia.launcher.jasmin.ui.component.fragment.mymenu.coupon.shop.purchase.presenter.PaymentGuidePresenter;
import kr.altimedia.launcher.jasmin.ui.view.layoutManager.grid.VerticalGridView;
import kr.altimedia.launcher.jasmin.ui.view.util.TimeUtil;

import static kr.altimedia.launcher.jasmin.ui.component.fragment.menu.TvOverlayManager.KEY_CANCEL_PURCHASE_ID;

public class CouponPaymentGuideFragment extends CouponPaymentBaseFragment {
    public static final String CLASS_NAME = CouponPaymentGuideFragment.class.getName();
    private final String TAG = CouponPaymentGuideFragment.class.getSimpleName();

    private static final String KEY_PURCHASED_INFO = "PURCHASED_INFO";

    private final ArrayList<PaymentGuideItem> GUIDE_AIRPAY =
            new ArrayList<>(Arrays.asList(
                    new PaymentGuideItem(R.string.qr_airpay_step_one, R.drawable.qr_airpay_img_1, R.drawable.qr_info_1),
                    new PaymentGuideItem(R.string.qr_airpay_step_two, R.drawable.qr_airpay_img_2, R.drawable.qr_info_2),
                    new PaymentGuideItem(R.string.qr_airpay_step_three, R.drawable.qr_airpay_img_3, R.drawable.qr_info_3)));
    private final ArrayList<PaymentGuideItem> GUIDE_M_BANKING =
            new ArrayList<>(Arrays.asList(
                    new PaymentGuideItem(R.string.qr_m_banking_step_one, R.drawable.qr_mbanking_img_1, R.drawable.qr_info_1),
                    new PaymentGuideItem(R.string.qr_m_banking_step_two, R.drawable.qr_mbanking_img_2, R.drawable.qr_info_2),
                    new PaymentGuideItem(R.string.qr_m_banking_step_three, R.drawable.qr_mbanking_img_3, R.drawable.qr_info_3)));
    private final ArrayList<PaymentGuideItem> GUIDE_CREDIT_BANKING =
            new ArrayList<>(Arrays.asList(
                    new PaymentGuideItem(R.string.qr_credit_or_banking_step_one, R.drawable.qr_credit_bank_img_1, R.drawable.qr_info_1),
                    new PaymentGuideItem(R.string.qr_credit_or_banking_step_two, R.drawable.qr_credit_bank_img_2, R.drawable.qr_info_2)));

    private TextView minView;
    private TextView secView;
    private CountDownTimer countDownTimer;
    private ImageView qrImageView;

    private PurchaseInfo purchaseInfo;
    private TvOverlayManager mTvOverlayManager = null;

    public static CouponPaymentGuideFragment newInstance(PurchaseInfo purchaseInfo) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_PURCHASED_INFO, purchaseInfo);

        CouponPaymentGuideFragment fragment = new CouponPaymentGuideFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (Log.INCLUDE) {
            Log.d(TAG, "onAttached : context");
        }
        if (context instanceof LauncherActivity) {
            mTvOverlayManager = ((LauncherActivity) context).getTvOverlayManager();
        } else if (context instanceof LiveTvActivity) {
            mTvOverlayManager = ((LiveTvActivity) context).getTvOverlayManager();
        } else if (context instanceof VideoPlaybackActivity) {
            mTvOverlayManager = ((VideoPlaybackActivity) context).getTvOverlayManager();
        }
    }

    @Override
    protected int initLayoutResourceId() {
        return R.layout.fragment_coupon_shop_payment;
    }

    @Override
    protected void initView(View view) {
        purchaseInfo = getArguments().getParcelable(KEY_PURCHASED_INFO);

        initTitle(view);
        initButton(view);
        initGuide(view);
        initTimer(view);
        qrImageView = view.findViewById(R.id.qrCodeImage);

        requestPurchaseCoupon();
    }

    private void initTitle(View view) {
        Context context = view.getContext();
        String title = "";

        PaymentType paymentType = purchaseInfo.getPaymentType();
        if (paymentType == PaymentType.AIR_PAY) {
            title = context.getString(R.string.payment_airpay);
        } else if (paymentType == PaymentType.QR) {
            title = context.getString(R.string.payment_qr);
        } else if (paymentType == PaymentType.CREDIT_CARD || paymentType == PaymentType.INTERNET_MOBILE_BANK) {
            title = context.getString(R.string.payment_qr_credit_or_banking);
        }

        ((TextView) view.findViewById(R.id.title)).setText(title);
    }

    private void initButton(View view) {
        TextView cancel = view.findViewById(R.id.cancel);
        cancel.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                        showPaymentCancelDialog(purchaseInfo, purchaseId);
                        return true;
                }

                return false;
            }
        });
    }

    private void initGuide(View view) {
        ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter(new PaymentGuidePresenter());
        arrayObjectAdapter.addAll(0, getGuide());

        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(arrayObjectAdapter);
        VerticalGridView verticalGridView = view.findViewById(R.id.guide_grid_view);
        verticalGridView.setAdapter(itemBridgeAdapter);
        int gap = (int) getResources().getDimension(R.dimen.payment_guide_spacing);
        verticalGridView.setVerticalSpacing(gap);

        PaymentType paymentType = purchaseInfo.getPaymentType();
        if (paymentType == PaymentType.CREDIT_CARD || paymentType == PaymentType.INTERNET_MOBILE_BANK) {
            ViewGroup.LayoutParams params = verticalGridView.getLayoutParams();
            params.height = (int) getResources().getDimension(R.dimen.payment_guide_credit_bank_height);
        }
    }

    private ArrayList<PaymentGuideItem> getGuide() {
        ArrayList<PaymentGuideItem> list = new ArrayList<>();

        PurchaseInfo purchaseInfo = getArguments().getParcelable(KEY_PURCHASED_INFO);
        PaymentType paymentType = purchaseInfo.getPaymentType();
        if (paymentType == PaymentType.AIR_PAY) {
            list.addAll(0, GUIDE_AIRPAY);
        } else if (paymentType == PaymentType.QR) {
            list.addAll(0, GUIDE_M_BANKING);
        } else if (paymentType == PaymentType.CREDIT_CARD || paymentType == PaymentType.INTERNET_MOBILE_BANK) {
            list.addAll(0, GUIDE_CREDIT_BANKING);
        }

        return list;
    }

    private void initTimer(View view) {
        minView = view.findViewById(R.id.min_counting);
        secView = view.findViewById(R.id.sec_counting);
        setTime(TOTAL_COUNT_DOWN);

        String timeoutErrorMessage = getString(R.string.payment_error);
        countDownTimer = new CountDownTimer(TOTAL_COUNT_DOWN, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                int sec = setTime(millisUntilFinished);
                if (sec != 0 && sec % CHECK_PAYMENT_INTERVAL == 0) {
                    getPurchaseCouponResult();
                }
            }

            @Override
            public void onFinish() {
                if (getOnPaymentListener() != null) {
                    getOnPaymentListener().onPaymentError(timeoutErrorMessage);
                }
            }
        };
    }

    private int setTime(long millisUntilFinished) {
        long min = TimeUtil.getMin(millisUntilFinished);
        long sec = TimeUtil.getSec(millisUntilFinished);

        String minText = min < 10 ? "0" + min : String.valueOf(min);
        String secText = sec < 10 ? "0" + sec : String.valueOf(sec);
        minView.setText(minText);
        secView.setText(secText);

        return (int) sec;
    }

    private void setQrCode(Bitmap qrCode) {
        Glide.with(getContext())
                .load(qrCode)
                .centerCrop()
                .into(qrImageView);
    }

    private void requestPurchaseCoupon() {
        if (Log.INCLUDE) {
            Log.d(TAG, "requestPurchaseCoupon");
        }

        CouponProduct couponProduct = purchaseInfo.getCouponProduct();
        PaymentType paymentType = purchaseInfo.getPaymentType();
        String paymentDetail = "";
        String creditCardType = "";
        String bankCode = "";
        try {
            if (paymentType == PaymentType.CREDIT_CARD) {
                CreditCard card = purchaseInfo.getCreditCard();
                if(card != null) {
                    if(card.getCreditCardId() != null) {
                        paymentDetail = card.getCreditCardId();
                    }
                    if(card.getCreditCardType() != null) {
                        creditCardType = card.getCreditCardType();
                    }
                }
            } else if (paymentType == PaymentType.INTERNET_MOBILE_BANK) {
                if(purchaseInfo.getBank().getBankCode() != null) {
                    bankCode = purchaseInfo.getBank().getBankCode();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        CouponDataManager couponDataManager = new CouponDataManager();
        couponDataManager.requestPurchaseCoupon(
                AccountManager.getInstance().getSaId(), AccountManager.getInstance().getProfileId(),
                couponProduct.getId(), paymentType.getCode(), Integer.toString(couponProduct.getPrice()),
                paymentDetail, creditCardType, bankCode,
                new MbsDataProvider<String, CouponPurchaseRequest>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPurchaseCoupon: onFailed");
                        }

                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, CouponPurchaseRequest result) {
                        purchaseId = result.getPurchaseId();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPurchaseCoupon: onSuccess: purchasedId=" + purchaseId);
                        }

                        try {
                            getParentFragment().getArguments().putString(KEY_CANCEL_PURCHASE_ID, purchaseId);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if (result.getQrCode() == null) {
                            return;
                        }

                        if (!StringUtils.nullToEmpty(purchaseId).isEmpty()) {
                            byte[] bytes = Base64.decode(result.getQrCode(), Base64.DEFAULT);
                            Bitmap qrCode = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            setQrCode(qrCode);

                            countDownTimer.start();
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "requestPurchaseCoupon: onError: errorCode=" + errorCode + ", message=" + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return getContext();
                    }
                });
    }

    private void getPurchaseCouponResult() {
        if (Log.INCLUDE) {
            Log.d(TAG, "getPurchaseCouponResult: purchaseId=" + purchaseId);
        }

        if (resultTask != null) {
            resultTask.cancel(true);
        }

        if (purchaseId == null) {
            return;
        }

        CouponDataManager couponDataManager = new CouponDataManager();
        resultTask = couponDataManager.getPurchaseCouponResult( purchaseId,
                new MbsDataProvider<String, CouponPurchaseResult>() {
                    @Override
                    public void needLoading(boolean loading) {

                    }

                    @Override
                    public void onFailed(int key) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPurchaseCouponResult: onFailed");
                        }
                        FailNoticeDialogFragment failNoticeDialogFragment = new FailNoticeDialogFragment(key, getTaskContext());
                        failNoticeDialogFragment.show(mTvOverlayManager, getFragmentManager(), FailNoticeDialogFragment.CLASS_NAME);
                    }

                    @Override
                    public void onSuccess(String id, CouponPurchaseResult result) {
                        boolean isSuccess = result.isSuccess();
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPurchaseCouponResult: onSuccess: isSuccess=" + isSuccess);
                        }

                        if(isSuccess){
                            try {
                                //do not cancel payment if product is purchased
                                getParentFragment().getArguments().remove(KEY_CANCEL_PURCHASE_ID);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentComplete(isSuccess);
                        }
                    }

                    @Override
                    public void onError(String errorCode, String message) {
                        if (Log.INCLUDE) {
                            Log.d(TAG, "getPurchaseCouponResult: onError: errorCode=" + errorCode + ", message=" + message);
                        }

                        if (getOnPaymentListener() != null) {
                            getOnPaymentListener().onPaymentError(message);
                        }
                    }

                    @Override
                    public Context getTaskContext() {
                        return getContext();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        countDownTimer.cancel();
    }
}
