/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.component.fragment.settings.presenter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.leanback.widget.Presenter;
import kr.altimedia.launcher.jasmin.R;
import kr.altimedia.launcher.jasmin.ui.component.fragment.settings.subtitles.SettingSubtitlesSetupDialogFragment;
import kr.altimedia.launcher.jasmin.ui.view.common.SpinnerTextView;

public class SettingSpinnerPresenter extends SpinnerTextView.SpinnerPresenter {

    public SettingSpinnerPresenter() {
    }

    @Override
    protected ViewHolder onCreateSpinnerViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.presenter_setting_spinner, parent, false);
        return new SpinnerViewHolder(view);
    }

    @Override
    protected void onBindSpinnerViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        SpinnerViewHolder vh = (SpinnerViewHolder) viewHolder;
        vh.setData((SpinnerTextView.SpinnerPresenterItem) item);
    }

    @Override
    protected void onUnbindSpinnerViewHolder(Presenter.ViewHolder viewHolder) {
        SpinnerViewHolder vh = (SpinnerViewHolder) viewHolder;
        vh.view.setOnClickListener(null);
    }

    private class SpinnerViewHolder extends SpinnerTextView.SpinnerPresenter.ViewHolder {
        private TextView title;
        private CheckBox checkBox;

        public SpinnerViewHolder(View view) {
            super(view);
            initView(view);
        }

        private void initView(View view) {
            title = view.findViewById(R.id.spinnerTitle);
            checkBox = view.findViewById(R.id.checkBox);
        }

        private void setData(SpinnerTextView.SpinnerPresenterItem item) {
            SettingSubtitlesSetupDialogFragment.SubtitleOptionButton mSubtitleOptionButton =
                    SettingSubtitlesSetupDialogFragment.SubtitleOptionButton.values()[item.getParentType()];
            boolean isChecked = mSubtitleOptionButton.getCurrentOption().getType() == item.getType();

            String name = item.getTitle();
            title.setText(name);
            checkBox.setChecked(isChecked);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SpinnerTextView.OnSpinnerClickListener onSpinnerClickListener = getOnSpinnerClickListener();
                    if (onSpinnerClickListener != null) {
                        onSpinnerClickListener.onClick(item);
                    }
                }
            });
        }
    }
}