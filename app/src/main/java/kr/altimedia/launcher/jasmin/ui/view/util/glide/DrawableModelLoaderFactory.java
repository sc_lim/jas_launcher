/*
 * Copyright (C) 2020 Altimedia Corporation. All Rights Reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimedia Corporation. You may not use or distribute
 * this software except in compliance with the terms and conditions
 * of any applicable license agreement in writing between Altimedia
 * Corporation and you.
 */

package kr.altimedia.launcher.jasmin.ui.view.util.glide;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;

import kr.altimedia.launcher.jasmin.ui.view.util.InstalledAppUtil;

/**
 * Created by mc.kim on 10,12,2019
 */
public class DrawableModelLoaderFactory implements ModelLoaderFactory<InstalledAppUtil.AppInfo, Drawable> {

    private final Context mContext;

    DrawableModelLoaderFactory(Context context) {
        mContext = context;
    }

    @NonNull
    @Override
    public ModelLoader<InstalledAppUtil.AppInfo, Drawable> build(@NonNull MultiModelLoaderFactory multiFactory) {
        return new DrawableModelLoader(mContext);
    }


    @Override
    public void teardown() {
        // Empty Implementation.
    }
}