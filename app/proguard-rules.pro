#####################
# Android Support
#####################

#####################
# GSON
-keepattributes Signature
-keepattributes *Annotation*
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.examples.android.model.** { *; }
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer
-dontnote sun.misc.Unsafe
-dontnote com.google.gson.internal.UnsafeAllocator
#####################

#####################
# OkHttp3
-dontwarn okhttp3.**
-dontwarn okio.**
-dontnote okhttp3.**
#####################

#####################
# Retrofit 2.X
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions
-keepclasseswithmembers class * {
       @retrofit2.http.* <methods>;
}
#####################

#####################
# RxJava
-keep class rx.schedulers.Schedulers {
    public static <methods>;
}
-keep class rx.schedulers.ImmediateScheduler {
    public <methods>;
}
-keep class rx.schedulers.TestScheduler {
    public <methods>;
}
-keep class rx.schedulers.Schedulers {
    public static ** test();
}
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
    long producerIndex;
    long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    long producerNode;
    long consumerNode;
}
-dontwarn rx.internal.**
-dontnote rx.internal.**
#####################

#####################
# This is due to legacy API katniss is referencing. ==>
-dontwarn com.google.android.volley.**
-dontwarn com.google.android.common.**
#####################

#####################
# SimpleXML
-keep public class org.simpleframework.** { *; }
-keep class org.simpleframework.xml.** { *; }
-keep class org.simpleframework.xml.core.** { *; }
-keep class org.simpleframework.xml.util.** { *; }
#####################

#####################
# Launcher
-keep class kr.altimedia.launcher.jasmin.ui.view.util.NotificationAlertService { *; }
-keep class kr.altimedia.launcher.jasmin.cwmp.service.CWMPSettingControl { *; }
-keep class kr.altimedia.launcher.jasmin.cwmp.service.CWMPSystemInfoManager { *; }
-keep class kr.altimedia.launcher.jasmin.cwmp.service.CWMPSystemUpdateManager { *; }
#####################

#####################
# cwmp library
-keep class com.alticast.** { *; }
-keep class af.** { *; }
-keep class org.dslforum.** { *; }
-keep class com.altimedia.cwmplibrary.** { *; }
-keep class com.altimedia.update.** { *; }
-keep class kr.altimedia.jassystem.** { *; }
#####################

#####################
# kr.altimedia.jasmine.tis library
-keep class kr.altimedia.agent.epg.data.** { *; }
-keep class kr.altimedia.jasmine.tis.util.** { *; }
#####################

#####################
# fpinserter library
-keep class com.coretrust.** { *; }
#####################

#####################
# visualon library
-keep class com.google.android.exoplayer2.** { *; }
-keep class com.visualon.** { *; }
#####################

#####################
# others
-keepclasseswithmembernames,includedescriptorclasses class * {
    native <methods>;
}
-keep class androidx.** { *; }
-keep class org.xmlpull.** { *; }
-keep class org.jibx.runtime.** { *; }

-keepparameternames
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod,MethodParameters,LocalVariableTable,LocalVariableTypeTable
-dontwarn android.support.v4.**,org.slf4j.**,com.google.android.gms.**,com.google.common.**
-dontskipnonpubliclibraryclasses

-keepclassmembers class * {
  public static <fields>;
  public *;
}
#####################
